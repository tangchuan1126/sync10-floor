package com.cwc.test.wcr;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/com/cwc/test/wcr/applicationContext.xml")
public abstract class BaseTestCase {
	
}