package com.cwc.test.wcr;

import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorAppointmentMgr;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;
import com.cwc.db.PageCtrl;

public class DemoTestCase1 extends BaseTestCase {
	
	@Resource
	private FloorProductStoreMgr productStoreMgr;
	
	@Resource
	private FloorAppointmentMgr floorAppointmentMgr;
	
	@Resource
	DBUtilAutoTran_Optm dbUtilAutoTran;
	
	@Before
	public void init() throws SQLException {
		ConfigBean.putBeans("wms_appointment_work", "wms_appointment_work");
		ConfigBean.putBeans("wms_appointment_base", "wms_appointment_base");
		ConfigBean.putBeans("wms_appointment_excep", "wms_appointment_excep");
		ConfigBean.putBeans("product_storage_catalog","product_storage_catalog");
		ConfigBean.putBeans("wms_appointment", "wms_appointment");
		ConfigBean.putBeans("wms_appointment_work_total", "wms_appointment_work_total");
		DBRow[] jetLags = dbUtilAutoTran.selectMutliple("select id,jet_lag from "+ConfigBean.getStringValue("product_storage_catalog"));
		
		Map<Long,Long> dateUtilJetLags = new HashMap<Long,Long>();
		for (DBRow dbRow : jetLags) 
		{
			dateUtilJetLags.put(dbRow.get("id",0l),dbRow.get("jet_lag",0l));
		}
		//DateUtil.setStroageJetLeg(dateUtilJetLags);
	}
	//@Ignore
	//@Test
	public void test1() throws Exception 
	{

		//Map<Long,Long> dateUtilJetLags = new HashMap<Long,Long>();
		
		//DateUtil.setStroageJetLeg(dateUtilJetLags);
		//7474
		Set<String> groupBy = new HashSet<String>();
		//groupBy.add("pc_id");
		groupBy.add("title_id");
		long[] title_id = new long[0];
		long[] catalog_id  = new long[0];
		//title_id[0]=18;
		
		DBRow[] b = productStoreMgr.productCount(0, 0, 0, 0, title_id, 1092966, "", catalog_id, 0, groupBy);
		System.out.println(DBRowUtils.dbRowArrayAsJSON(b));
		assertTrue(b.length!=0);
	}
	//@Ignore
	//@Test
	public void test2() throws Exception {
		
		//java.text.SimpleDateFormat sf = new java.text.SimpleDateFormat("MM/dd/YYYY");
		///Date date = sf.parse("03/02/2015"); //"03/02/2015"
		DBRow[] b = floorAppointmentMgr.sumWorkByDayTotal("2015-02-12", "2015-02-12", 1000005);
				//floorAppointmentMgr.sumWorkByDay("2015-03-02", 1000005);
		System.out.println(DBRowUtils.dbRowArrayAsJSON(b));
	}
	
	@Test
	public void test3() throws Exception {
		
		DBRow[] b = floorAppointmentMgr.sumBaseByDayTotal("2015-02-08", "2015-02-14", 1000005);
		//floorAppointmentMgr.sumWorkByDay("2015-03-02", 1000005);
		System.out.println(DBRowUtils.dbRowArrayAsJSON(b));
	}
	
	//@Test
	public void test4() throws Exception {
		
		DBRow[] b = floorAppointmentMgr.getBaseDefault(1000005);
		System.out.println(DBRowUtils.dbRowArrayAsJSON(b));
	}
	//@Test
	public void test5() throws Exception {
		
		DBRow[] b = floorAppointmentMgr.getCustomerExcept("2015-01-01", "2015-03-12", null, 1000005, null);
		System.out.println(DBRowUtils.dbRowArrayAsJSON(b));
	}
	//@Test
	public void test6() throws Exception {
		
		DBRow item = new DBRow();
		//item.add("bid",3);
		item.add("storage_id", 1000006);
		item.add("appointment_type", "outbound");
		item.add("start_hour", "1");
		item.add("end_hour", "23");
		item.add("def_cnt", "0");
		floorAppointmentMgr.addOrUpdateBase(item);
		
		DBRow item1 = new DBRow();
		item1.add("storage_id", 1000006);
		item1.add("appointment_type", "inbound");
		item1.add("start_hour", "1");
		item1.add("end_hour", "23");
		item1.add("def_cnt", "0");
		floorAppointmentMgr.addOrUpdateBase(item1);
	}
	//@Test
	public void test7() throws Exception {
		
		DBRow data = new DBRow();
		//item.add("bid",3);
		
		data.add("storage_id", 1000005);
		//data.add("appointment_type", "inbound");
		data.add("work_date", "2015-02-12");
		data.add("`hour`", "3");
		data.add("`limit`", "5");
		//addOrUpdateCustomWork
		floorAppointmentMgr.addOrUpdateWorkTotal(data);
	}
	//@Test
	public void test8() throws Exception {
		
		DBRow data = new DBRow();
		//item.add("bid",3);
		
		data.add("storage_id", 1000005);
		data.add("appointment_type", "outbound");
		data.add("work_date", "2015-02-12");
		data.add("`hour`", "3");
		data.add("`limit`", "0");
		data.add("customer_id", "2726");
		//addOrUpdateCustomWork
		floorAppointmentMgr.addOrUpdateCustomWork(data);
	}
	//@Test
	public void test9()throws Exception {
	
		DBRow data = new DBRow();
		//item.add("bid",3);
		
		data.add("storage_id", 1000005);
		data.add("appointment_type", "outbound");
		data.add("work_date", "2015-02-12");
		data.add("`hour`", "3");
		data.add("`limit`", "0");
		data.add("customer_id", "2726");
		//addOrUpdateCustomWork
		
		assertTrue(floorAppointmentMgr.sumOneWorkOut("2015-02-12", 3, 1000005, "2726",true));
	}
	
	//@Test
	public void test10()throws Exception {
		assertTrue(floorAppointmentMgr.sumOneWorkIn("2015-02-12", 3, 1000005, "2726", true));
	}
	
	//@Test
	public void test11()throws Exception {
		assertTrue(floorAppointmentMgr.subOneWorkOut("2015-02-12", 3, 1000005, "2726", false));
	}
	//@Test
	public void test12()throws Exception {
		assertTrue(floorAppointmentMgr.subOneWorkIn("2015-02-12", 3, 1000005, "2726",false));
	}
	//@Test
	public void test13()throws Exception {
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(1);
		pc.setPageSize(20);
		floorAppointmentMgr.getAppointmentByPage("all", "", "03/02/2015", null, "1000005","inbound", pc);
	}
	//@Test
	public void test14()throws Exception {

		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH");
		Date date1 = format1.parse("2015-02-12"+" 00");
		Date date2 = new Date(format1.parse("2015-02-12"+" 00").getTime()+24*3600*1000);
		
		System.out.println(DBRowUtils.dbRowArrayAsJSON(floorAppointmentMgr.sumWorkByDayTotal(DateUtil.showUTCTime(date1, 1000005), DateUtil.showUTCTime(date2, 1000005), 1000005)));
		
	}
	
	//@Test
	public void test15()throws Exception {

		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH");
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date1 = format1.parse("2015-02-01"+" 00");
		Date date2 = new Date(format1.parse("2015-03-15"+" 00").getTime()+24*3600*1000);
		//floorAppointmentMgr.sumWorkByStartEnd(start, end, storage_id, hour);
		long storage_id = 1000005;
		DBRow[] rows = floorAppointmentMgr.sumWorkByStartEndTotal(DateUtil.showUTCTime(date1, storage_id), DateUtil.showUTCTime(date2, storage_id), storage_id, DateUtil.getSubHour( DateUtil.showUTCTime(date1, storage_id),format2.format(date1)) );
		System.out.println(DBRowUtils.dbRowArrayAsJSON(rows));
	}
	//@Test
	public void test16()throws Exception {
		DBRow row = new DBRow();
		row.put("storage_id", 1000005);
		row.put("appointment_type", "inbound");
		row.put("`hour`", 0);
		row.put("`limit`", 6);
		row.add("role_id", 1);
		DBRow row2 = floorAppointmentMgr.addOrUpdateDefaultValue(row);
		System.out.println(row2);
	}
	//@Test
	public void test17()throws Exception {
		DBRow row = floorAppointmentMgr.addOrUpdateRole("2015-03-01","03/31/2015","other");
		System.out.println(row.get("bid", 0L));
		assertTrue(row.get("bid", 0L) !=0);
	}
	
	//@Test
	public void test18()throws Exception {
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date1 = format2.parse("2015-04-01 08:00:00");
		
		//int it = floorAppointmentMgr.getLimitDefault(date1,"2013-04-01",8, 1000005, "outbound");
		//System.out.println(it);
		//assertTrue(it>0);
		
	}
}