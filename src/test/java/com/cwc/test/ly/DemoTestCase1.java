package com.cwc.test.ly;

import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.floor.api.zj.FloorCountryMgrZJ;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class DemoTestCase1 extends BaseTestCase {
	
	@Resource(name="floorCountryMgr")
	private FloorCountryMgrZJ floorCountryMgrZJ;
	
	@Test
	public void test_1() throws Exception {
		DBRow[] admins = floorCountryMgrZJ.getAllCountryCode();
		assertTrue(admins != null && admins.length>0);
		System.out.println(DBRowUtils.dbRowArrayAsJSON(admins).toString(4));
	}

}
