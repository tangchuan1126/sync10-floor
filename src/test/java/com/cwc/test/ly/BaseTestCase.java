package com.cwc.test.ly;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ResourceUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cwc.app.util.ConfigBean;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/com/cwc/test/ly/applicationContext.xml")
public abstract class BaseTestCase {
	
	@Before
	public void setUp() throws Exception 
	{
		initTableConfig();
	}	
	
	private void initTableConfig() throws ParserConfigurationException, SAXException, IOException 
	{
		File cfgFile = ResourceUtils.getFile("classpath:com/cwc/test/ly/config.xml"); 
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
		DocumentBuilder builder = factory.newDocumentBuilder(); 
		Document doc = builder.parse(cfgFile); 
		NodeList nodes = doc.getElementsByTagName("tag"); 
		Node node = null;
		NamedNodeMap map = null;
		for(int i =0 ;i < nodes.getLength();i++) 
		{
			node = nodes.item(i);
			map = node.getAttributes();
			ConfigBean.putBeans(map.getNamedItem("name").getNodeValue(), map.getNamedItem("value").getNodeValue());
		}
		
	}
}
