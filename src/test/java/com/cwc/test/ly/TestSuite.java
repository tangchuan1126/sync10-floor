package com.cwc.test.ly;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DemoTestCase1.class,DemoTestCase2.class })
public class TestSuite {
	
}

