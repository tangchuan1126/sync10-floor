package com.cwc.test.zyj.titles;

import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class TitlesTestCase1 extends BaseTestCase {
	
	@Resource
	private FloorProprietaryMgrZyj floorProprietaryMgrZyj;
	
	@Before
	public void setUp() throws Exception {
		
	}
	
	@Test
	public void test_1() throws Exception {
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(1);
		pc.setPageSize(100);
		Long[] pcLine = new Long[1];
		pcLine[0] = 1000064L;
		DBRow[] admins = floorProprietaryMgrZyj.findProductCatagoryParentsIdAndNameByTitleId(0L, pcLine, new Long[0],new Long[0], new Long[0], 0, 0, null);
		assertTrue(admins != null && admins.length>0);
			System.out.println(DBRowUtils.dbRowArrayAsJSON(admins).toString(4));
	}

}
