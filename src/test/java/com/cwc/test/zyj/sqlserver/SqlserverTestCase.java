package com.cwc.test.zyj.sqlserver;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.cwc.app.floor.api.zj.FloorSQLServerMgrZJ;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;

public class SqlserverTestCase extends BaseTestCase {
	
	@Resource
	private FloorSQLServerMgrZJ floorSQLServerMgrZJ;
	
	@Before
	public void setUp() throws Exception {
		
	}
	
	@Test
	public void test_1() throws Exception {
		String c = floorSQLServerMgrZJ.checkFnOrderSerialnoFinalStatus(200934, "W16");
		System.out.println(c);
	}
	
	
	
	public void findMasterBolsByLoadNo()throws Exception
	{
		DBRow[] rows = floorSQLServerMgrZJ.findMasterBolsByLoadNo(
				"TRANSFER- 3".replace(" ", ""), "", "", new String[0]);
		assertTrue(rows.length > 0 || rows.length == 0);
			System.out.println(DBRowUtils.dbRowArrayAsJSON(rows).toString(4));
	}
	
	
	
	public void findBillOfLadingTemplateByLoadMaster()throws Exception
	{
		DBRow[] rows = floorSQLServerMgrZJ.findBillOfLadingTemplateByLoadMaster(
				"TRANSFER- 3".replace(" ", ""), "", "", new String[0]);
		assertTrue(rows.length > 0 || rows.length == 0);
			System.out.println(DBRowUtils.dbRowArrayAsJSON(rows).toString(4));
	}
	
	public void test1() throws Exception
	{
		DBRow[] rows = floorSQLServerMgrZJ.findMasterBolsByLoadNo("3475181", "", "", new String[0]);
		assertTrue(rows.length > 0);
			System.out.println(DBRowUtils.dbRowArrayAsJSON(rows).toString(4));
	}
	
	public void findOrdersNoteByMasterBolNos() throws Exception
	{
		Long[] masterBols = new Long[1];
		masterBols[0]= 24251L;
		String companyID = "W12";
		String customerID = "VIZIO";
		DBRow[] rows = floorSQLServerMgrZJ.findOrdersNoteByMasterBolNos(masterBols, companyID, customerID);
		DBRow max = null;
		for (int i = 0; i < rows.length; i++) 
		{
			String note = rows[i].getString("BOLNote").replace(" ", "").toUpperCase();
			if(note.contains("DELIVERYWINDOW"))
			{
				int deliveryWindowStart = note.indexOf("DELIVERYWINDOW")+15;
				String str1 = note.substring(deliveryWindowStart, deliveryWindowStart+22);
				String[] dateArr = str1.split("TO");
				if(dateArr.length > 0)
				{
					rows[i].add("first", dateArr[0]);
					if(dateArr.length == 2)
					{
						rows[i].add("end", dateArr[1]);
					}
				}
			}
			if(max == null)
			{
				max = rows[i];
			}
			else
			{
				if(DateUtil.createDate(max.getString("first"),DateUtil.defaultDateFormat).after(DateUtil.createDate(rows[i].getString("first"),DateUtil.defaultDateFormat)))
				{
					max = rows[i];
				}
			}
		}
		
		assertTrue(rows.length > 0);
			System.out.println(DBRowUtils.dbRowArrayAsJSON(rows).toString(4));
			System.out.println(max.getString("first")+","+max.getString("BOLNote"));
	}

}
