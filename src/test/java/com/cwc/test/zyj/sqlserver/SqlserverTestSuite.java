package com.cwc.test.zyj.sqlserver;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SqlserverTestCase.class })
public class SqlserverTestSuite {
	
}

