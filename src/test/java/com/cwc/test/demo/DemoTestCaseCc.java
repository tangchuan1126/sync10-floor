package com.cwc.test.demo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Ignore;
import org.junit.Test;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.FloorProductCategoryMgr;
import com.cwc.app.floor.api.FloorProductCategoryMgr.NodeType;
import com.cwc.app.floor.api.FloorProductCategoryMgr.RelationType;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.app.invent.BaseController;

public class DemoTestCaseCc extends BaseTestCase {
	
	@Resource
	FloorProductStoreMgr productStoreMgr;
	@Resource
	FloorProductCategoryMgr productCategoryMgr;
	@Resource
	FloorGoogleMapsMgrCc googleMap;
	@Resource
	BaseController bcontroller;
	
	@Ignore
	@Test
	public void parentContainerTree() throws Exception{
		Map<String, Object> map = productStoreMgr.parentContainerTree(1000005, 2001108);
		System.out.println(map.toString());
	}
	
	//@Test
	public void test_parentContainerTree() throws Exception{
		long ps_id =1000005;
		long con_id = 2001105;
		Map<String, Object> map= productStoreMgr.parentContainerTree(ps_id,con_id);
		System.out.println(map.toString());
		
	}
	@Ignore
	@Test
	public void test_wholeContainerTree() throws Exception{
		/*
		JSONObject	parentTree =new JSONObject(productStoreMgr.parentContainerTree(1000005,2001105));
		JSONObject childTree =productStoreMgr.containerTree(1000005, 2002503, null, 0l, 0l, null, null);
		
		System.out.println(parentTree.toString(4));
		System.out.println("===============================");
		System.out.println(childTree.toString(4));
		*/
		
		productStoreMgr.wholeContainerTree(1000005,2001105);
	}
	
	@Ignore
	@Test
	public void test_removeSlcNodes() throws Exception{
		long ps_id =1000002;
		long[] slc_ids = new long [] {1015246,1015247};
		long slc_type =1;
		productStoreMgr.removeSlcNodes(ps_id, slc_ids, slc_type, true);
		
	}
	@Ignore
	@Test
	public void test_deleteLocationById() throws Exception{
		long[] slc_ids = new long [] {1015246,1015247};
		long ps_id =1000002;
		googleMap.deleteLocationById(ps_id, slc_ids);
		
	}
	
	
/*	@Ignore
	@Test
	public void test_queryProductLine() throws Exception{
		Map<String, Object> slc = new HashMap<String, Object>();
		long ps_id =1000005;
		Set<Long> set= productStoreMgr.queryProductLine(ps_id, null,0l,null,null);
		
	}*/
	@Ignore
	@Test
	public void test_getParkingDocksOccupancy() throws Exception{
		long ps_id =1000005;
		googleMap.getParkingDocksOccupancy(ps_id);
	}
	@Ignore
	@Test
	public void test_deleteContainer() throws Exception{
		long ps_id =1000005;
		long con_id =2002506;
		bcontroller.deleteContainer(ps_id,con_id);
	}
/*	@Ignore
	@Test
	public void test_queryHasInventLocations() throws Exception{
		long ps_id=1000005;
		Long[] title_ids = new Long[]{166l};
		long product_line=0l;
		long catalog=356;
		String[] p_names=null;
		String[] lot_numbers=null;
		int container_type=0;
		long type_id =0;
		int union_flag =-1;
		DBRow[] trees = productStoreMgr.queryHasInventLocations(ps_id,title_ids, product_line,catalog,p_names, lot_numbers, container_type, type_id,union_flag);
		System.out.println(DBRowUtils.dbRowArrayAsJSON(trees).toString(4));
		
	}*/
	
	
	@Ignore
	@Test
	public void productcatalogs() {
		String[] sypher = new String[]{
				//"===== 创建 Product",
				"create (n:Product {pc_id:1003003, p_name:'SE24FY10', catalogs:[354], title_id:166, Product_line:1000017, union_flag:0}) return n",
				"create (n:Product {pc_id:1003004, p_name:'SE24HE03', catalogs:[356], title_id:166, Product_line:1000017, union_flag:0}) return n",
				"create (n:Product {pc_id:1003005, p_name:'SE26HQ02', catalogs:[392], title_id:166, Product_line:1000012, union_flag:0}) return n",
				"create (n:Product {pc_id:1003006, p_name:'SE26HQ04', catalogs:[392], title_id:16, Product_line:1000012, union_flag:0}) return n",
				"create (n:Product {pc_id:1003007, p_name:'SE29HY34', catalogs:[397], title_id:166, Product_line:1000017, union_flag:0}) return n",
				"create (n:Product {pc_id:1003008, p_name:'SE32HY10', catalogs:[356], title_id:166, Product_line:1000017, union_flag:0}) return n",
				"create (n:Product {pc_id:1003039, p_name:'WD32HB1120', catalogs:[356], title_id:16, Product_line:1000017, union_flag:0}) return n",
				"create (n:Product {pc_id:1003009, p_name:'SE39FH03', catalogs:[392], title_id:166, Product_line:1000012, union_flag:0}) return n",
				"create (n:Product {pc_id:1003010, p_name:'SE39HE02', catalogs:[397], title_id:166, Product_line:1000012, union_flag:0}) return n",
				"create (n:Product {pc_id:1003037, p_name:'DWM48F1Y1', catalogs:[354], title_id:16, Product_line:1000017, union_flag:0}) return n",
				"create (n:Product {pc_id:1003038, p_name:'DWM55F2Y1', catalogs:[356], title_id:16, Product_line:1000017, union_flag:0}) return n",

				//"===== 创建 StorageLocationCatalog",


				//"====== 创建CLP <-[CONTAINS]- (Container) -[LOCATES]->",
				"match (p:Product{pc_id:1003003}), (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot1001', time_number:0}]-(c:Container {con_id:2001000, container:'CLP2001000' ,contain_type:1, type_id:68, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003003}), (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot1001', time_number:0}]-(c:Container {con_id:2001001, container:'CLP2001001' ,contain_type:1, type_id:68, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003004}), (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot1002', time_number:0}]-(c:Container {con_id:2001002, container:'CLP2001002' ,contain_type:1, type_id:69, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003004}), (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot1002', time_number:0}]-(c:Container {con_id:2001003, container:'CLP2001003' ,contain_type:1, type_id:69, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003005}), (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot1003', time_number:0}]-(c:Container {con_id:2001004, container:'CLP2001004' ,contain_type:1, type_id:70, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003005}), (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot1003', time_number:0}]-(c:Container {con_id:2001005, container:'CLP2001005' ,contain_type:1, type_id:70, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003006}), (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot1004', time_number:0}]-(c:Container {con_id:2001006, container:'CLP2001006' ,contain_type:1, type_id:71, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003006}), (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot1004', time_number:0}]-(c:Container {con_id:2001007, container:'CLP2001007' ,contain_type:1, type_id:71, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003007}), (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot1005', time_number:0}]-(c:Container {con_id:2001008, container:'CLP2001008' ,contain_type:1, type_id:72, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003007}), (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot1005', time_number:0}]-(c:Container {con_id:2001009, container:'CLP2001009' ,contain_type:1, type_id:72, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003008}), (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot1001', time_number:0}]-(c:Container {con_id:2001010, container:'CLP2001010' ,contain_type:1, type_id:73, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003008}), (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot1001', time_number:0}]-(c:Container {con_id:2001011, container:'CLP2001011' ,contain_type:1, type_id:73, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003037}), (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot10011', time_number:0}]-(c:Container {con_id:2001012, container:'CLP2001012' ,contain_type:1, type_id:74, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003037}), (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot10011', time_number:0}]-(c:Container {con_id:2001013, container:'CLP2001013' ,contain_type:1, type_id:74, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003038}), (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot10012', time_number:0}]-(c:Container {con_id:2001014, container:'CLP2001014' ,contain_type:1, type_id:75, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003038}), (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot10012', time_number:0}]-(c:Container {con_id:2001015, container:'CLP2001015' ,contain_type:1, type_id:75, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003008}), (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot1001', time_number:0}]-(c:Container {con_id:2001016, container:'CLP2001016' ,contain_type:1, type_id:73, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003008}), (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot1001', time_number:0}]-(c:Container {con_id:2001017, container:'CLP2001017' ,contain_type:1, type_id:73, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003004}), (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot1002', time_number:0}]-(c:Container {con_id:2001018, container:'CLP2001018' ,contain_type:1, type_id:69, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003004}), (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot1002', time_number:0}]-(c:Container {con_id:2001019, container:'CLP2001019' ,contain_type:1, type_id:69, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003009}), (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot1008', time_number:0}]-(c:Container {con_id:2001020, container:'CLP2001020' ,contain_type:1, type_id:76, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003009}), (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot1008', time_number:0}]-(c:Container {con_id:2001021, container:'CLP2001021' ,contain_type:1, type_id:76, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003010}), (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot1003', time_number:0}]-(c:Container {con_id:2001022, container:'CLP2001022' ,contain_type:1, type_id:77, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",
				"match (p:Product{pc_id:1003010}), (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot1003', time_number:0}]-(c:Container {con_id:2001023, container:'CLP2001023' ,contain_type:1, type_id:77, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return s,l,c,r,p",

				//"====== 创建CLP <-[CONTAINS]- (Container)",
				"match (p:Product{pc_id:1003003})  create unique(c:Container {con_id:2001100, container:'CLP2001100' ,contain_type:1, type_id:68, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (p:Product{pc_id:1003003})  create unique(c:Container {con_id:2001101, container:'CLP2001101' ,contain_type:1, type_id:68, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (p:Product{pc_id:1003005})  create unique(c:Container {con_id:2001102, container:'CLP2001102' ,contain_type:1, type_id:70, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (p:Product{pc_id:1003005})  create unique(c:Container {con_id:2001103, container:'CLP2001103' ,contain_type:1, type_id:70, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (p:Product{pc_id:1003039})  create unique(c:Container {con_id:2001104, container:'CLP2001104' ,contain_type:1, type_id:78, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (p:Product{pc_id:1003009})  create unique(c:Container {con_id:2001105, container:'CLP2001105' ,contain_type:1, type_id:76, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (p:Product{pc_id:1003009})  create unique(c:Container {con_id:2001106, container:'CLP2001106' ,contain_type:1, type_id:76, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (p:Product{pc_id:1003007})  create unique(c:Container {con_id:2001107, container:'CLP2001107' ,contain_type:1, type_id:72, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (p:Product{pc_id:1003007})  create unique(c:Container {con_id:2001108, container:'CLP2001108' ,contain_type:1, type_id:72, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (p:Product{pc_id:1003037})  create unique(c:Container {con_id:2001109, container:'CLP2001109' ,contain_type:1, type_id:74, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (p:Product{pc_id:1003037})  create unique(c:Container {con_id:2001110, container:'CLP2001110' ,contain_type:1, type_id:74, is_has_sn:1, is_full:3})-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",

				//"====== 创建TLP (Container) -[LOCATES]->",
				"match (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot001', time_number:0}]-(c:Container {con_id:2002500, container:'TLP2002500' ,contain_type:3, type_id:2, is_has_sn:1, is_full:3}) return s,l,c",
				"match (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot003', time_number:0}]-(c:Container {con_id:2002501, container:'TLP2002501' ,contain_type:3, type_id:3, is_has_sn:1, is_full:3}) return s,l,c",
				"match (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot007', time_number:0}]-(c:Container {con_id:2002502, container:'TLP2002502' ,contain_type:3, type_id:8, is_has_sn:1, is_full:3}) return s,l,c",
				"match (s:StorageLocationCatalog{slc_id:1005116}) create unique s<-[l:LOCATES{lot_number:'lot008', time_number:0}]-(c:Container {con_id:2002503, container:'TLP2002503' ,contain_type:3, type_id:9, is_has_sn:1, is_full:3}) return s,l,c",
				"match (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot005', time_number:0}]-(c:Container {con_id:2002504, container:'TLP2002504' ,contain_type:3, type_id:2, is_has_sn:1, is_full:3}) return s,l,c",
				"match (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot0011', time_number:0}]-(c:Container {con_id:2002505, container:'TLP2002505' ,contain_type:3, type_id:3, is_has_sn:1, is_full:3}) return s,l,c",
				"match (s:StorageLocationCatalog{slc_id:1005113}) create unique s<-[l:LOCATES{lot_number:'lot0012', time_number:0}]-(c:Container {con_id:2002506, container:'TLP2002506' ,contain_type:3, type_id:8, is_has_sn:1, is_full:3}) return s,l,c",

				//"===== 创建TLP包含CLP关系",
				"match (c1:Container{con_id:2002500}), (c2:Container{con_id:2001100}) create unique c1-[r:CONTAINS]->c2 return c1,r,c2",
				"match (c1:Container{con_id:2002500}), (c2:Container{con_id:2001101}) create unique c1-[r:CONTAINS]->c2 return c1,r,c2",
				"match (c1:Container{con_id:2002501}), (c2:Container{con_id:2001102}) create unique c1-[r:CONTAINS]->c2 return c1,r,c2",
				"match (c1:Container{con_id:2002501}), (c2:Container{con_id:2001103}) create unique c1-[r:CONTAINS]->c2 return c1,r,c2",
				"match (c1:Container{con_id:2002502}), (c2:Container{con_id:2001104}) create unique c1-[r:CONTAINS]->c2 return c1,r,c2",
				"match (c1:Container{con_id:2002503}), (c2:Container{con_id:2001105}) create unique c1-[r:CONTAINS]->c2 return c1,r,c2",
				"match (c1:Container{con_id:2002503}), (c2:Container{con_id:2001106}) create unique c1-[r:CONTAINS]->c2 return c1,r,c2",
				"match (c1:Container{con_id:2002504}), (c2:Container{con_id:2001107}) create unique c1-[r:CONTAINS]->c2 return c1,r,c2",
				"match (c1:Container{con_id:2002504}), (c2:Container{con_id:2001108}) create unique c1-[r:CONTAINS]->c2 return c1,r,c2",
				"match (c1:Container{con_id:2002505}), (c2:Container{con_id:2001109}) create unique c1-[r:CONTAINS]->c2 return c1,r,c2",
				"match (c1:Container{con_id:2002505}), (c2:Container{con_id:2001110}) create unique c1-[r:CONTAINS]->c2 return c1,r,c2",


				//"===== 创建TLP包含Product关系",
				"match (c:Container{con_id:2002500}), (p:Product{pc_id:1003003}) create unique c-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (c:Container{con_id:2002501}), (p:Product{pc_id:1003005}) create unique c-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (c:Container{con_id:2002502}), (p:Product{pc_id:1003039}) create unique c-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (c:Container{con_id:2002503}), (p:Product{pc_id:1003009}) create unique c-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (c:Container{con_id:2002505}), (p:Product{pc_id:1003037}) create unique c-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p",
				"match (c:Container{con_id:2002506}), (p:Product{pc_id:1003038}) create unique c-[r:CONTAINS{quantity:100, locked_quantity:0}]->p return c,r,p"

				//"===== 添加LOCATES上的pc_id和title_id",
				//"match (s:StorageLocationCatalog)<-[l:LOCATES]-()-[:CONTAINS*0..]->(p:Product) set l.pc_id=p.pc_id, l.title_id=p.title_id return s,l,p",
		};
				
		for (String s : sypher) {
			try {
				//productStoreMgr.tx(1000005, productStoreMgr.statement(new HashMap<String, Object>(), s));
			} catch (Exception e) {
				System.out.println(s);
				e.printStackTrace();
			}
		}
	}
	/*@Ignore
	@Test
	public void searchRelationNodes() throws Exception{
		Map searchFor = new HashMap();
		searchFor.put("pc_id", 1002703);
		
		productCategoryMgr.searchRelationNodes(1, NodeType.Product, searchFor, RelationType.CONTAINS, false);
	}
	@Ignore
	@Test
	public void removeRelationNode() throws Exception{
		Map searchFor = new HashMap();
		searchFor.put("pc_id", 1002681);
		
		Map delFor = new HashMap();
		delFor.put("con_id", 100);
		
		productCategoryMgr.removeRelationNode(1, NodeType.Product, searchFor, RelationType.CONTAINS, false, NodeType.Category, delFor);
	}*/
	/*@Ignore
	@Test
	public void addProperties() throws Exception{
		Map searchFor = new HashMap();
		searchFor.put("total_weight", 2963);
		
		Map addFor = new HashMap();
		addFor.put("test1", 444);
		addFor.put("test2", 555);
		addFor.put("test3", 666);
		productCategoryMgr.addProperties(1, RelationType.CONTAINS,searchFor, addFor);
	}
*/
	@Test
	public void wholeContainerTree() throws Exception{
		long ps_id=1000005;
		long con_id =200 ;
	 JSONObject json=productStoreMgr.wholeContainerTree(ps_id,con_id);
	 System.out.println(json.toString(4));
	}
	@Ignore
	@Test
	public void test_locationTree()throws Exception{
		long ps_id=1000005;
		Map<String,Object>  searchForSlc = new  HashMap<String, Object>();
		searchForSlc.put("slc_id", 1005116);
		searchForSlc.put("slc_type", 1);
		long customer_id =166;
		long product_line =1000017;
		Long[] titles_id =new Long[]{166l};
		
	}
	@Ignore
	@Test
	public void test_searchContainerTree()throws Exception{
		
		long ps_id=1000005;
		Map<String,Object>  searchFor = new  HashMap<String, Object>();
		searchFor.put("con_id", 2012502);
		JSONObject json= productStoreMgr.searchContainerTree(ps_id,searchFor);
		System.out.println(json.toString(4));
		
	}
	
/*	@Test
	public void test_searchContainer()throws Exception{
		
		long ps_id=1000005;
		Map<String,Object>  searchFor = new  HashMap<String, Object>();
		searchFor.put("con_id", 2012502);
		JSONArray jsonarray= productStoreMgr.searchContainer(ps_id,searchFor,0l,0l,0l);
		System.out.println(jsonarray.toString(4));
		
	}*/
	@Ignore
	@Test
	public void test_searchContainer()throws Exception{
		long ps_id=1000005;
	
		long con_id= 2001015;
		
		 Map<String ,Object> search =new HashMap<String, Object>();
		 search.put("con_id", con_id);
//		 JSONArray  ret=	 productStoreMgr.searchContainer(ps_id, search, null, 0l, null);
		
//		 System.out.println(ret.getJSONObject(0).get("lot_number"));
		
	}
	@Test
	public void test_copyTree()throws Exception{
		long ps_id=1000005;
		
		long con_id= 2001015;
		/*
		 * {
        "con_props": {
            "con_id": 3032999,
            "container": "3055168",
            "container_type": 3,
            "customer_id": 2726,
            "damage": 2,
            "is_damage": 2,
            "is_full": 3,
            "is_has_sn": 1,
            "location_id": 1005775,
            "location_type": 1,
            "lot_no": "10243070221",
            "pc_id": 1003688,
            "quantity": 1,
            "receipt_no": 31395,
            "title_id": 4,
            "type_id": 19
        },
        "locates_props": {
            "lot_number": "10243070221",
            "pc_id": "1003688",
            "time_number": 1435844382339,
            "title_id": "4"
        },
        "product_props": {
            "catalogs": 0,
            "customer_id": 2726,
            "lot_no": "10243070221",
            "model_number": "M50-C1",
            "p_name": "M50-C1/10243070221",
            "pc_id": 1003688,
            "product_line": 0,
            "title_id": 4,
            "union_flag": 0
        },
        "ps_id": 1000005,
        "rel_props": {
            "quantity": 1
        },
        "seal_number": [
            "NA",
            ""
        ],
        "slc_props": {
            "slc_id": 1,
            "slc_type": 1
        }
    }
		 */
		List<Map<String,Object>>	list =new ArrayList<Map<String,Object>>();
		Map<String ,Object> map =new HashMap<String, Object>();
		Map<String ,Object> con_props =new HashMap<String, Object>();
		con_props.put("con_id", 3032999);
		con_props.put("container_type", 3);
		con_props.put("is_damage", 2);
		con_props.put("is_full", 3);
		con_props.put("is_has_sn", 1);
		con_props.put("con_id", 3032999);
		con_props.put("type_id", 19);
		Map<String ,Object> locates_props =new HashMap<String, Object>();
		locates_props.put("time_number", 1435844382339L);
		locates_props.put("lot_number", "10243070221");
		locates_props.put("pc_id", 1003688);
		locates_props.put("title_id", 4);
		Map<String ,Object> rel_props =new HashMap<String, Object>();
		rel_props.put("quantity",1);
		
		Map<String ,Object> slc_props =new HashMap<String, Object>();
		slc_props.put("slc_id", 1);
		slc_props.put("slc_type", 1);
		
		Map<String ,Object> product_props =new HashMap<String, Object>();
		product_props.put("pc_id", 1003688);
		product_props.put("catalogs", 0);
		product_props.put("model_number", "M50-C1");
		product_props.put("title_id", 4);
		product_props.put("customer_id", 2726);
		map.put("con_props", con_props);
		map.put("locates_props", locates_props);
		map.put("rel_props", rel_props);
		map.put("slc_props", slc_props);
		map.put("product_props", product_props);
		list.add(map);
		bcontroller.copyTree(ps_id, list);
	
	}
	
	
}
