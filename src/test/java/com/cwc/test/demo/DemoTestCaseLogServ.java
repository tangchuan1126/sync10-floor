package com.cwc.test.demo;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import us.monoid.json.JSONObject;
import us.monoid.web.Content;
import us.monoid.web.JSONResource;
import us.monoid.web.Resty;

import com.cwc.app.beans.log.SerialNumberLogBean;
import com.cwc.app.floor.api.FloorLogMgr;



public class DemoTestCaseLogServ extends BaseTestCase {
	static Logger log = Logger.getLogger("JAVAPS");
	
	@Resource
	FloorLogMgr logMgr;
	
	
	@Before
	public void setUp() throws Exception {
		
	}
	
	@Test
	public void test_addLog() throws Exception {
		SerialNumberLogBean logBean = new SerialNumberLogBean();
		logBean.setPc_id(100000);
		logBean.setSerialNumber("111-111-111");
		logBean.setSupplier_id(1);
		logBean.setTitle_id(18);
		
		logMgr.addLog(logBean, false);
	}
	@Test
	public void test_addLog_async() throws Exception {
		SerialNumberLogBean logBean = new SerialNumberLogBean();
		logBean.setPc_id(100000);
		logBean.setSerialNumber("111-111-111");
		logBean.setSupplier_id(1);
		logBean.setTitle_id(18);
		
		logMgr.addLog(logBean, true);
	}
	
	@Test
	public void test_queryLog() throws Exception {
		Resty client = new Resty();
		JSONObject query = new JSONObject()
							.put("fields", new JSONObject().put("serialNumber", "111-111-111"))
							.put("page_ctrl", new JSONObject().put("pageNo", 1).put("pageSize", 20))
							.put("start_date", "2015-07-01T00:00:00")
							.put("end_date", "2015-08-01T00:00:00");
		JSONResource result = client.json(logMgr.getRestBaseUrl()+"/log_filter/SerialNumberLog", new Content("application/json", query.toString().getBytes("UTF-8")));
		System.out.println(result.object().toString(4));
	}
	
}
