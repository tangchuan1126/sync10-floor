package com.cwc.test.demo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.FloorProductStoreMgr;

public class DemoTestCaseNeo4j extends BaseTestCase {
	static Logger log = Logger.getLogger("JAVAPS");
	
	@Resource
	FloorProductStoreMgr productStoreMgr;
	
	@Before
	public void setUp() throws Exception {
		
	}
	
	//@Test
	public void test_locationTreeOld() throws Exception {
		//long ps_id = 1006883;
		long ps_id = 1000005;
		Map<String,Object> searchForSlc = new HashMap<String,Object>();
		Long[] title_ids = null;
		long product_line = 0;
		long catalog = 0;
		String[] p_names = null;
		String[] lot_numbers = null;
		int container_type = 0;
		long type_id = 0;
		String[] model_number = null;
		
		long tm = System.currentTimeMillis();
		List<Map<String,Object>> result = productStoreMgr.locationTreeOld(ps_id, 
				searchForSlc, 
				title_ids, 
				product_line, 
				catalog, 
				p_names, 
				lot_numbers, 
				container_type, 
				type_id, 
				model_number);
		
		System.out.println("COST: "+(System.currentTimeMillis() - tm)/1000.0 +
				" secs;  locations="+result.size()+
				"; data="+new JSONArray(result).toString().getBytes("UTF-8").length/1024.0 + " KB");
		log.info(new JSONArray(result).toString(4));
	}
	
	//@Test
	public void test_locationTree() throws Exception {
		long ps_id = 1000005;
		Map<String,Object> searchForSlc = new HashMap<String,Object>();
		//searchForSlc.put("slc_id", 1005113L);
		searchForSlc.put("slc_id", 1005116L);
		Long[] title_ids = null;
		long product_line = 0;
		long catalog = 0;
		String[] p_names = null;
		String[] lot_numbers = null;
		int container_type = 0;
		//long type_id = 74; //68;
		long type_id = 0; 
		String[] model_number =  new String[]{"SE32HY10"};   
		//String[] model_number =  null;  
		Long[] con_ids = null; 
		
		long tm = System.currentTimeMillis();
		JSONArray result = productStoreMgr.locationTree(ps_id, 
				searchForSlc, 
				title_ids, 
				product_line, 
				catalog, 
				p_names, 
				lot_numbers, 
				container_type, 
				type_id, 
				model_number, con_ids);
		
		System.out.println("COST: "+(System.currentTimeMillis() - tm)/1000.0 +
				" secs;  locations="+result.length()+
				"; data="+result.toString().getBytes("UTF-8").length/1024.0 + " KB");
		log.info(result.toString(4));
	}
	
	//@Test
	public void test_containerTree() throws Exception {
		long ps_id = 1000005;
		long con_id = 2002504; 
		//long con_id = 2002501;
		Long[] title_ids = null;  //new Long[]{166L};
		long product_line = 0;  //1000017L;
		long catalog = 0; //354;
		String[] p_names = null;
		String[] model_numbers = null;
		
		long tm = System.currentTimeMillis();
		JSONObject result = productStoreMgr.containerTree(ps_id, con_id,title_ids, product_line, catalog, p_names, model_numbers);
		System.out.println("containerTree "+con_id+" cost: " + (System.currentTimeMillis()-tm)/1000.0 + " secs");
		if(result!=null) log.info(result.toString(4));
		
	}
	
	@Test
	public void test_productCatalogs() throws Exception {
		long ps_id = 1000005;
		System.out.println(new JSONArray(productStoreMgr.productCatalogs(ps_id, null, 0)).toString(4));
	}
}
