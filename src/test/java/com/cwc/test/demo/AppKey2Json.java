package com.cwc.test.demo;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ClassUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import us.monoid.json.JSONObject;

@RunWith(Suite.class)
@SuiteClasses(AppKey2Json.AppKey2JsonWorker.class)
public class AppKey2Json {
	
	public static boolean actuallySameType(Class a, Class b){
		return ClassUtils.primitiveToWrapper(a).equals(ClassUtils.primitiveToWrapper(b));
	}
	public static void putWithLocale(Map<String,Object> m, String key, String[] locales, Object... values){
		Map<String,Object> valOfLocale = new HashMap<String,Object>();
		for(int i=0; i<locales.length; i++){
			valOfLocale.put(locales[i], i<values.length ? values[i]: values[0]);
		}
		m.put(key, valOfLocale);
	}

	public static class AppKey2JsonWorker {
		@Test
		public void appKey2Json() throws Exception {
			Properties sysProps = System.getProperties();
			String appKey2JsonOutput = sysProps.getProperty("appKey2JsonOutput", null);
			
			for (Object k : sysProps.keySet()) {
				String key = k.toString();
				if(! key.startsWith("com.cwc.app.key.")) continue;
				String[] locales = sysProps.getProperty(key).split(",");
				Class  keyBeanClass = Class.forName(key);
				Object keyBean = keyBeanClass.newInstance();
				Field[] fields = keyBeanClass.getDeclaredFields();
				Method[] methods = keyBeanClass.getDeclaredMethods();
				
				Map<String,Object> fieldsMap = new HashMap<String,Object>(); 
				for(Field f: fields) {
					if(! java.lang.reflect.Modifier.isStatic(f.getModifiers())) continue;
					
					Object keyVal = f.get(null);
					
					Object keyLabelByStr = null;
					boolean found = false;
					for(Method m: methods){
						Class<?>[] paramTypes = m.getParameterTypes();
						if(m.getReturnType().equals(String.class) && 
								paramTypes.length == 1 &&
								actuallySameType(paramTypes[0], keyVal.getClass())
						){
							Object keyLabel = m.invoke(keyBean, keyVal);
							putWithLocale(fieldsMap,keyVal.toString(), locales, new Object[]{keyLabel});
							found = true;
						}
						else if(m.getReturnType().equals(String.class) && 
								paramTypes.length == 1 &&
								paramTypes[0].equals(String.class) ){
							keyLabelByStr = m.invoke(keyBean, keyVal.toString());
						}
					}
					if(!found){
						if(keyLabelByStr == null) {
							System.out.println("Cannot convert KeyBean: "+key);
							continue;
						}
						putWithLocale(fieldsMap,keyVal.toString(), locales, new Object[]{keyLabelByStr});
					}
				}
				
				JSONObject jo = new JSONObject(fieldsMap);
				System.out.printf("%s=%s\n",key, jo.toString(4) );
				if(appKey2JsonOutput!=null){ //输出包装为requireJS模块的JSON文件
					FileUtils.write(
					new File(appKey2JsonOutput+File.separator+keyBeanClass.getSimpleName()+".js"), 
							"define("+jo.toString(4)+");", 
							"UTF-8");
					
				}
			}
		}
	}
}
