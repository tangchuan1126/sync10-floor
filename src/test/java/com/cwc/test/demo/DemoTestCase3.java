package com.cwc.test.demo;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.Neo4jPathAnalyser;
import com.cwc.app.util.Neo4jPathAnalyser2;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class DemoTestCase3 extends BaseTestCase {
	@Resource
	FloorProductStoreMgr productStoreMgr;
    @Resource
    FloorGoogleMapsMgrCc googleMapsMgrcc;
    @Resource
    DBUtilAutoTran dbUtilAutoTran;
   @Before
	public void setUp() throws Exception {
		long ps_id = 1000005;
		ConfigBean.putBeans("storage_load_unload_location", "storage_load_unload_location");
		ConfigBean.putBeans("storage_door", "storage_door");
		ConfigBean.putBeans("storage_warehouse_base", "storage_warehouse_base");
		ConfigBean.putBeans("product_storage_catalog", "product_storage_catalog");
		ConfigBean.putBeans("storage_location_catalog", "storage_location_catalog");
		ConfigBean.putBeans("storage_location_area", "storage_location_area");
		ConfigBean.putBeans("storage_kml", "storage_kml");
	}

//	@Test
	public void test_repeat_node() throws Exception{
		Map<String,Object> containerNode = new HashMap<String, Object>();
		Map<String,Object> productNode = new HashMap<String, Object>();
		Map<String,Object> rel = new HashMap<String, Object>();
		
		productNode.put("title_id","");
		productNode.put("p_name","W1-2");		
		productNode.put("unit_name","ITEM");
		productNode.put("unit_price","1.0");
		productNode.put("catalog_id","0");
		productNode.put("orignal_pc_id", "0");
		productNode.put("width","12.0");
		productNode.put("weight","12.0");
		productNode.put("heigth", "12.0");
		productNode.put("length", "12.0");
		productNode.put("length_nom","4");
		productNode.put("weight_nom","2");
		productNode.put("price_uom","2");
		productNode.put("union_flag","0");
		productNode.put("alive","1");
		
		rel.put("quantity",6);
		rel.put("stack_length_qty",1);
		rel.put("stack_width_qty",2);
		rel.put("stack_height_qty",3);
		rel.put("total_weight",73.44);
		rel.put("weight_nom",2);
		
		containerNode.put("con_id",1000);
		containerNode.put("type_id",1000);			
		containerNode.put("pc_id",1002484);
		containerNode.put("sort",1);
		containerNode.put("basic_type_id",12);
		containerNode.put("inner_pc_or_lp",0);
		containerNode.put("stack_length_qty", 1);
		containerNode.put("stack_width_qty", 2);
		containerNode.put("stack_height_qty", 3);			
		containerNode.put("is_has_sn",2);
		containerNode.put("inner_total_lp",6);
		containerNode.put("inner_total_pc",6);
		containerNode.put("length_uom",4);
		containerNode.put("weight_uom",2);		
		containerNode.put("container_type",1);
		containerNode.put("lp_name","CLP1*2*3[t1-2]");
		containerNode.put("active",1);
		containerNode.put("ibt_length",1.11);
		containerNode.put("ibt_width",1.22);
		containerNode.put("ibt_height",1.33);
		containerNode.put("ibt_weight",1.44);
		containerNode.put("total_weight",73.44); 
		containerNode.put("level", 1);
		
		
		
		
		//productStoreMgr.addNode(1,NodeType.Container,containerNode);//添加容器节点
		
		//productStoreMgr.addRelation(1,NodeType.Container,NodeType.Product,containerNode,productNode,rel);
		
		productStoreMgr.addNode(1,NodeType.Container,containerNode);//添加容器节点
	}
	
//	@Test
	public Map<String, Object> props(Object... kvs) {
		Map<String, Object> props = new HashMap<String, Object>();
		for (int i = 0; i < kvs.length; i += 2) {
			props.put(kvs[i].toString(), kvs[i + 1]);
		}
		return props;
	}
	@Ignore
	@Test
	public void test_productCatalogs() throws Exception {
		long ps_id = 1000005;
		System.out.println(new JSONArray(
				//productStoreMgr.searchLocations(ps_id, new long[]{18}, 1000066, 436, new String[]{"10212120021/E221-A1"}, new String[]{""}, 0)
		).toString(4));
	}
	@Ignore
	@Test
	public void test_productLine() throws Exception {
		int ps_id = 1000005;
		DBRow  rows =googleMapsMgrcc.getStorageKmlByPsId(ps_id);
		System.out.println(DBRowUtils.dbRowAsString(rows));
		
	}
	@Ignore
	@Test
	public void test_lp_type_nodes() throws Exception {
		long ps_id = 1;

		productStoreMgr.addNode(ps_id, NodeType.Container,
				props("con_id", 9001L, "type_id", 9001L));
		
		productStoreMgr.addNode(ps_id, NodeType.Container,
				props("con_id", 90011L, "type_id", 9002L));
		productStoreMgr.addNode(ps_id, NodeType.Container,
				props("con_id", 90012L, "type_id", 9002L));
		
		productStoreMgr.addNode(ps_id, NodeType.Container,
				props("con_id", 900111L, "type_id", 9003L));
		
		productStoreMgr.addNode(ps_id, NodeType.Container,
				props("con_id", 900121L, "type_id", 9003L));
		
		productStoreMgr.addNode(ps_id, NodeType.Product,
				props("pc_id", 800001L, "title_id", 8001L));
		productStoreMgr.addNode(ps_id, NodeType.Product,
				props("pc_id", 800001L, "title_id", 8002L));
		
		productStoreMgr.addRelation(ps_id, NodeType.Container, NodeType.Container, 
				props("con_id",9001L),props("con_id",90011L),props("quantity",10));
		productStoreMgr.addRelation(ps_id, NodeType.Container, NodeType.Container, 
				props("con_id",9001L),props("con_id",90012L),props("quantity",10));
		productStoreMgr.addRelation(ps_id, NodeType.Container, NodeType.Container, 
				props("con_id",90011L),props("con_id",900111L),props("quantity",10));
		productStoreMgr.addRelation(ps_id, NodeType.Container, NodeType.Container, 
				props("con_id",90012L),props("con_id",900121L),props("quantity",10));
		productStoreMgr.addRelation(ps_id, NodeType.Container, NodeType.Product, 
				props("con_id",900111L),props("pc_id",800001L,"title_id",8001L),props("quantity",30));
		productStoreMgr.addRelation(ps_id, NodeType.Container, NodeType.Product, 
				props("con_id",900121L),props("pc_id",800001L,"title_id",8002L),props("quantity",20));
		
		productStoreMgr.addNode(ps_id, NodeType.Container,
				props("con_id", 9111L, "type_id", 9001L));
		productStoreMgr.addRelation(ps_id, NodeType.Container, NodeType.Container, 
				props("con_id",9111L),props("con_id",90011L),props("quantity",20));
		
		//查询出以上包装结构，获得树状结构
		JSONObject result = productStoreMgr.searchContainerTree(ps_id, props("type_id", 9001L));
		System.out.println(result.toString(4));
	}

	@Ignore
	@Test
	public void test_lp_type() throws Exception {
		long ps_id = 1;
		Map<String, Object> fromNode = new HashMap<String, Object>();

		fromNode.put("container_type", 1000);

		productStoreMgr.addNode(ps_id, NodeType.Container, fromNode);

		Map<String, Object> fromNode2 = new HashMap<String, Object>();
		fromNode2.put("container_type", 2000);
		productStoreMgr.addNode(ps_id, NodeType.Container, fromNode2);

		Map<String, Object> toNode = new HashMap<String, Object>();
		toNode.put("type_id", 1001);
		productStoreMgr.addNode(ps_id, NodeType.Container, toNode);

		Map<String, Object> relProps = new HashMap<String, Object>();
		relProps.put("quantity", 100);

		productStoreMgr.addRelation(ps_id, NodeType.Container,
				NodeType.Container, fromNode, toNode, relProps);

		relProps.put("quantity", 50);
		productStoreMgr.addRelation(ps_id, NodeType.Container,
				NodeType.Container, fromNode2, toNode, relProps);
	}

	//同步mysql和图数据库数据
	//@Test
	public void test_addNodeLocation() throws Exception {
		long ps_id = 1000005;
		//插入一个ghost 位置
		Map<String , Object>  props=new HashMap<String, Object>();
		props.put("slc_id",1);
		props.put("slc_type",1);
		props.put("ps_id", ps_id);
		props.put("slc_position", "ghost");
		productStoreMgr.addNode(ps_id, NodeType.StorageLocationCatalog, props);	
		DBRow[] rows =	googleMapsMgrcc.getLocationByPsId(ps_id);
		if(rows.length>0){
			for(int i=0;i<rows.length;i++){
				props.clear();
				DBRow row=rows[i];
				props.put("slc_id", Long.parseLong(row.getString("obj_id")));
				props.put("ps_id", ps_id);
				props.put("slc_area", Long.parseLong(row.getString("area_id")));
				props.put("is_three_dimensional", row.get("is_three_dimensional",0));
				props.put("slc_position", row.getString("obj_name"));
				props.put("slc_type", 1);
				productStoreMgr.addNode(ps_id, NodeType.StorageLocationCatalog, props);	
			}
		}
		DBRow[] stagings =	googleMapsMgrcc.getStagingInfoByPsId(ps_id);
		if(stagings.length>0){
			for(int i=0;i<stagings.length;i++){
				props.clear();
				DBRow row=stagings[i];
				props.put("slc_id", Long.parseLong(row.getString("obj_id")));
				//props.put("slc_area", row.getString("area_id"));
				props.put("ps_id", ps_id);
				props.put("is_three_dimensional", 0);
				props.put("slc_position", row.getString("obj_name"));
				props.put("slc_type", 2);
				productStoreMgr.addNode(ps_id, NodeType.StorageLocationCatalog, props);	
			}
		}
	}
	
	/*@Test
	public void cleanNeo4jData() throws Exception{
		long ps_id = 1000005;		
		DBRow[] rows =dbUtilAutoTran.select("product");
		long[] pc_id=new long[rows.length];
		for(int i=0;i<rows.length;i++){
			pc_id[i]=rows[i].get("pc_id", 0l);			
		}
		JSONObject params = new JSONObject();
		params.put("pc_id", pc_id);
		String  queryLines =" match (p:Product) where not (p.pc_id in {pc_id}) return p";
		JSONArray	 jsonArray=productStoreMgr.query(ps_id, params, queryLines);
		long[] _pc_id=new long[jsonArray.length()];
		for(int i=0;i<jsonArray.length();i++){
			_pc_id[i]=jsonArray.getJSONArray(i).getJSONObject(0).getJSONObject("data").getLong("pc_id");			
		}
		JSONArray statements = new JSONArray();
	
		String  deleteLines1 =" match (slc: StorageLocationCatalg)<-[l]-(c: Container)-[*]->(p:Product)"
				+ " where p.pc_id in {pc_id} delete l";
		String  deleteLines2 =" match (c)-[r:CONTAINS*]->(p:Product) where p.pc_id in {pc_id}"
				+ " with  [x in r |  [ startnode(x), x, endnode(x) ] ] as r2 delete r2[0],r2[1],r2[2]";
		JSONObject delparams = new JSONObject();
		statements
		.put( new JSONObject()
		.put("parameters",delparams)
		.put("statement",deleteLines1))
		.put(new JSONObject()
		.put("parameters",delparams)
		.put("statement",deleteLines2));
		delparams.put("pc_id", _pc_id); 
		productStoreMgr.transaction(ps_id, statements);
	}
	*/
	
	@Ignore
	@Test
	public void query_addNodes() throws Exception {
		long ps_id = 1000005;
		Map<String , Object>  con_props=new HashMap<String, Object>();
		con_props.put("con_id", 101013);
		con_props.put("container", "TLP101013");
		con_props.put("container_type", 3);
		con_props.put("is_full", 1);
		con_props.put("is_has_sn", 1);
		con_props.put("type_id", 0);
		productStoreMgr.addNode(ps_id, NodeType.Container, con_props);
		Map<String , Object>  product_props=new HashMap<String, Object>();
		product_props.put("pc_id", 1011222);
		product_props.put("title_id", 60);
		product_props.put("product_line", 100212);
		product_props.put("catalogs",new int[]{354});
		product_props.put("p_name", "100212/esld");
		product_props.put("union_flag", 0);
		productStoreMgr.addNode(ps_id, NodeType.Product, product_props);
		Map<String , Object>  relProps=new HashMap<String, Object>();
		relProps.put("locked_quantity", 0);
		relProps.put("quantity", 55);
		Map<String , Object>  slc_props=new HashMap<String, Object>();
		slc_props.put("slc_id", 87);
		slc_props.put("slc_type", 2);
		Map<String , Object>  slc_relprops=new HashMap<String, Object>();
		slc_relprops.put("pc_id", 1011222);
		slc_relprops.put("title_id", 60);
		slc_relprops.put("lot_number", 1011);
		slc_relprops.put("time_number", System.currentTimeMillis());
		productStoreMgr.addRelation(ps_id, NodeType.Container, NodeType.Product, con_props, product_props, relProps);
		productStoreMgr.addRelation(ps_id, NodeType.Container, NodeType.StorageLocationCatalog, con_props, slc_props, slc_relprops);
		//DBRow[] nodes =productStoreMgr.searchNodes(ps_id, NodeType.StorageLocationCatalog, props);
		
	}
	@Ignore
	@Test
	public void test_compareTree() throws Exception {
		long ps_id =1000005;
		Map<String , Object>  con_props=new HashMap<String, Object>();
		con_props.put("con_id", 100011);
		Map<String , Object> toNodeProps =new HashMap<String, Object>();
		Map<String , Object> relProps =new HashMap<String, Object>();
		productStoreMgr.removeRelations(ps_id, NodeType.Container, NodeType.StorageLocationCatalog, con_props, toNodeProps, relProps);
		
	}

	@Ignore
	@Test
	public void test_sumNodes() throws Exception {
		Map<String, Object> searchFor = new HashMap<String, Object>();
		// 测试极端情况，条件不满足，且字段不存在
		searchFor.put("container_type", 0);
		System.out.println(productStoreMgr.sumNodes(0, NodeType.Container,
				searchFor, "non_exists"));
		// 测试正常情况，条件满足
		searchFor.put("container_type", 2);
		searchFor.put("is_has_sn", 2);
		System.out.println(productStoreMgr.sumNodes(0, NodeType.Container,
				searchFor, "is_full", "is_has_sn"));
	}

	@Ignore
	@Test
	public void test_sumContainers() throws Exception {
		Map<String, Object> searchForSlc = new HashMap<String, Object>();
		Map<String, Object> searchForCon = new HashMap<String, Object>();

		// searchForSlc.put("slc_id", 1019477);
		searchForSlc.put("slc_area", 1000771);
		// searchForCon.put("con_id", 9999999);
		// searchForCon.put("con_id", 1001712);
		searchForCon.put("is_full", 1);
		searchForCon.put("is_has_sn", 2);
		System.out.println(productStoreMgr.sumContainers(0, searchForSlc,
				searchForCon, "is_full", "is_has_sn"));
	}

	@Ignore
	@Test
	public void test_titlesHavePhysical() throws Exception {
		long ps_id = 1000006;
		//System.out.println(productStoreMgr.titlesHavePhysical(ps_id));
	}

	@Ignore
	@Test
	public void test_locationsContainsTitle() throws Exception {
		long ps_id = 1000005;
		long title_id = 0;
		DBRow[] result = productStoreMgr
				.locationsContainsTitle(ps_id, title_id);
		System.out.println(result.length);
		System.out.println(DBRowUtils.dbRowArrayAsJSON(result).toString(4));
	}
	@Ignore
	@Test
	public void test_containerTree() throws Exception {
		long ps_id = 1000005;
		long con_id = 101011;
		JSONObject result = productStoreMgr.containerTree(ps_id, con_id, null, 0l, 0l, null, null);
		System.out.println(result.toString(4));
	}
	@Ignore
	@Test
	public void test_copyTree() throws Exception {
		long src_ps_id = 1000005;
		long dest_ps_id = 1000005;
		long con_id = 101011;
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("slc_id", 1005111);
		map.put("slc_type", 1);
		productStoreMgr.copyTree(src_ps_id, dest_ps_id, con_id, map);
		
	}

	@Ignore
	@Test
	public void test_analyzeTree() throws Exception {
		long ps_id = 0;
		long con_id = 1305227;
		Neo4jPathAnalyser a1 = productStoreMgr.analyzeTree(ps_id, con_id);

		System.out.println(a1);

		Neo4jPathAnalyser a2 = new Neo4jPathAnalyser2(
				productStoreMgr.containerTree(ps_id, con_id, null, 0l, 0l, null, null));
		System.out.println(a2);

		assertTrue(a1.getCckeys().equals(a2.getCckeys()));
		assertTrue(a1.getCpkeys().equals(a2.getCpkeys()));
		assertTrue(a1.getPathkeys().equals(a2.getPathkeys()));

		Set<String> a1Paths = new HashSet<String>();
		Set<String> a2Paths = new HashSet<String>();

		JSONArray a1PathsArray = a1.getPaths();
		JSONArray a2PathsArray = a2.getPaths();

		for (int i = 0; i < a1PathsArray.length(); i++)
			a1Paths.add(a1PathsArray.getJSONArray(i).toString());

		for (int i = 0; i < a2PathsArray.length(); i++)
			a2Paths.add(a2PathsArray.getJSONArray(i).toString());

		assertTrue(a1Paths.equals(a2Paths));
	}

	@Ignore
	@Test
	public void test_copyTree_compareTree() throws Exception {
		long src_ps_id = 0;
		long dest_ps_id_1 = 1000006;
		long dest_ps_id_2 = 100043;
		long con_id = 1305227;

		productStoreMgr.copyTree(src_ps_id, dest_ps_id_1, con_id, null);
		productStoreMgr.copyTree(
				productStoreMgr.containerTree(src_ps_id, con_id, null, 0l, 0l, null, null), dest_ps_id_2,
				null);

		// compare the two copies
		JSONObject t1 = productStoreMgr.containerTree(dest_ps_id_1, con_id, null, 0l, 0l, null, null);
		JSONObject t2 = productStoreMgr.containerTree(dest_ps_id_2, con_id, null, 0l, 0l, null, null);

		System.out.println(t1.toString(4));
		System.out.println(t2.toString(4));
		assertTrue(t1.toString().equals(t2.toString()));
	}

/*	@Ignore
	@Test
	public void test_searchLocates() throws Exception {
		long ps_id = 0;
		Map<String, Object> searchFor = new HashMap<String, Object>();
		searchFor.put("slc_id", 1019477);
		// searchFor.put("slc_area", 1000229);

		//System.out.println(new JSONArray(productStoreMgr.locationTree(ps_id,
		//		searchFor, new Long[]{18L, 19L},0l,0l,null,null, 3, 1)).toString(4));
		
		System.out.println(new JSONArray(productStoreMgr.conTypeIds(ps_id, 1019477, 1, null, 0, 0, null, null)).toString(4));

//		JSONObject ct = productStoreMgr.containerTree(ps_id, 1304652, 19L);
//		System.out.println(ct == null ? "null" : ct.toString(4));
	}*/
	@Ignore
	@Test
	public void test_LocationTree() throws Exception {
		long ps_id = 1000005;
		Map<String, Object> searchForSlc = new HashMap<String, Object>();
		searchForSlc.put("slc_id", 1005113);
		searchForSlc.put("slc_type", 1);
		//	List<Map<String,Object>>lists=productStoreMgr.locationTree(ps_id, searchForSlc, null, 0l,0l,  null, null,0,0l) ;
//		JSONObject ct = productStoreMgr.containerTree(ps_id, 1304652, 19L);
//		System.out.println(ct == null ? "null" : ct.toString(4));
		/*	for(int i=0;i<lists.size();i++){
				Map<String,Object> map =lists.get(i);
			}*/
	}
	
	@Ignore
	@Test
	public void test_productCount() throws Exception {
		long ps_id = 1000005;
		Set<String> groupBy = new HashSet<String>();
		groupBy.add("slc_id");
		groupBy.add("pc_id");
		groupBy.add("slc_type");
		//DBRow[] loc = productStoreMgr.productCount(ps_id, 0,1, 0, 0, null, 0, null, null, 0, groupBy);
		//System.out.println(DBRowUtils.dbRowArrayAsJSON(loc).toString(4));
		DBRow[] stag = productStoreMgr.productCount(ps_id,0, 2,0, 0, null, 0, null, null, 0, groupBy);
		System.out.println(DBRowUtils.dbRowArrayAsJSON(stag).toString(4));
	}
	@Ignore
	@Test
	public void test_putAway()throws Exception{
		//101011
		long ps_id = 1000005;
		Map<String,Object> slc_props =new HashMap<String, Object>();
		slc_props.put("slc_id", 1005093);
		slc_props.put("slc_type",1);
        Long[] con_ids =new Long[]{101011l,101012l,101013l};
		productStoreMgr.putAway(ps_id, slc_props,con_ids);
	}
	@Ignore
	@Test
	public void test_copyTree_2() throws Exception{
		long ps_id =1000002;
		JSONObject con =new JSONObject();
		JSONArray products =new JSONArray();
		JSONObject product =new JSONObject();
		con.put("con_id", 100023);
		con.put("container", 100020);
		con.put("container_type", 3);
		con.put("is_full", 1);
		con.put("is_has_sn", 0);
		con.put("type_id", 123);
		product.put("catalogs",new long[]{354});
		product.put("p_name", "10023021/Ewer");
		product.put("pc_id", 10023025);
		product.put("product_line", 213);
		product.put("quantity", 100);
		product.put("title_id", 57);
		products.put(product);
		con.put("products", products);
		System.out.println(con.toString(4));
		Map<String,Object>	locProps =new HashMap<String,Object>();
		locProps.put("slc_id",1017186);
		locProps.put("slc_type", 1);
		locProps.put("pc_id", 10023025);
		locProps.put("title_id", 57);
		locProps.put("lot_number", 232);
		locProps.put("time_number", System.currentTimeMillis());
		productStoreMgr.copyTree(con, ps_id, locProps);
		
	}
}
