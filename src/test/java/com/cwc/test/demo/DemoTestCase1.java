package com.cwc.test.demo;

import static org.junit.Assert.assertTrue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class DemoTestCase1 extends BaseTestCase {
	
	@Resource
	private FloorAdminMgr floorAdminMgr;
	
//	@Resource
//	private  FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;
	
	@BeforeClass public static void dbRowConfig(){
		//将以下两行注释掉，测试一样可以成功，说明兼容以前的行为。
//		DBRow.setKeyUseUpper(false); 
//		DBRow.setKeyIgnoreCase(true);
	} 
	
	@Before
	public void setUp() throws Exception {
		ConfigBean.putBeans("admin", "admin");
	}
	
	public static  class TestBean implements Serializable{
		private int id;
		private String name="";
		
		TestBean(int id){
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
		
		
	}
	
	@Test
	public void test_001() throws Exception {
		List<TestBean> ltb = new ArrayList<TestBean>();
		for(int i=0; i<10; i++)
			ltb.add(new TestBean(i));
		
		System.out.println(new JSONArray(ltb).toString(4));
	}
	
//	@Test
//	public void test_floor() throws Exception {
//		int pageSize = 0;
//		int pageNo = 0;
//		long ps_id = 1000005;
//		
//		PageCtrl pc =new PageCtrl();
//		pc.setPageSize(pageSize==0 ? 4 : pageSize);
//		pc.setPageNo(pageNo==0 ? 1 : pageNo);
//		Map<String,Object> result= new HashMap<String,Object>();
//		result.put("data",floorGoogleMapsMgrCc.getPrintServerByPsId(ps_id,pc));
//		result.put("pageCtrl", pc);
//		System.out.println(new JSONObject(result).toString(4));
//
//	}
	
	@Ignore
	@Test
	public void test_1() throws Exception {
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(1);
		pc.setPageSize(10);
		DBRow[] admins = floorAdminMgr.getAllAdmin(pc);
		assertTrue(admins != null && admins.length>0);
		System.out.println(new JSONArray(admins).toString(4));
		System.out.println(new JSONObject(pc).toString(4));
		
		DBRow t = new DBRow();
		Map<String,Object> m = new HashMap<String,Object>();
		m.put("CamelCase", 1);
		m.put("lowercase", "a");
		m.put("UPPERCASE", "A");
		
		t.putAll(m);
		t.put("camelCase", 2);  //覆盖原值
		
		System.out.println(new JSONObject(t).toString(4));
		
		assertTrue(t.get("camelcase").equals(new Integer(2)));
		assertTrue(t.get("CAMELCASE").equals(new Integer(2)));
		assertTrue(t.get("CamelCase").equals(new Integer(2)));
		assertTrue(t.get("lowercase").equals("a"));
		assertTrue(t.get("LOWERCASE").equals("a"));
		assertTrue(t.get("uppercase").equals("A"));
		assertTrue(t.get("UPPERCASE").equals("A"));
	
		
		
		
		for(DBRow admin: admins){
			for(Object key: admin.getFieldNames()) {
				String k = (String)key;
				String ku = k.toUpperCase();
				String kl = k.toLowerCase();
				
				System.out.println(k);
				
				assertTrue( admin.getValue(k).equals(admin.getValue(ku)) 
						&& admin.getValue(k).equals(admin.getValue(kl)));
				assertTrue( admin.get(k,null).equals(admin.get(ku, null)) 
						&& admin.get(k, null).equals(admin.get(kl, null)) );
				assertTrue( admin.get(k).equals(admin.get(ku))
						&& admin.get(k).equals(admin.get(kl)) );
			}
		}
	}

}
