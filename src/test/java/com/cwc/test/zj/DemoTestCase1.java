package com.cwc.test.zj;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.cwc.app.api.mgr.YMSMgrAPI;
import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.floor.api.zj.FloorCheckInMgrZJ;
import com.cwc.app.floor.api.zj.FloorOutListDetailMgrZJ;
import com.cwc.app.floor.api.zj.FloorSpaceResourcesRelationMgr;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class DemoTestCase1 extends BaseTestCase {
	
	@Resource
	private FloorCheckInMgrZJ floorCheckInMgrZJ;
	@Resource
	DBUtilAutoTran dbUtilAutoTran;
	
	@Test
	public void test_2() 
			throws Exception 
	{
		ConfigBean.putBeans("space_resources_relation", "space_resources_relation");
		ConfigBean.putBeans("storage_door", "storage_door");
		ConfigBean.putBeans("storage_yard_control", "storage_yard_control");
		ConfigBean.putBeans("entry_equipment", "entry_equipment");
		ConfigBean.putBeans("config_search_warehouse_location", "config_search_warehouse_location");
		ConfigBean.putBeans("appointment_temp", "appointment_temp");
		ConfigBean.putBeans("product_storage_catalog","product_storage_catalog");
		ConfigBean.putBeans("door_or_location_occupancy_details","door_or_location_occupancy_details");
		ConfigBean.putBeans("door_or_location_occupancy_main","door_or_location_occupancy_main");
		ConfigBean.putBeans("entry_equipment","entry_equipment");
		ConfigBean.putBeans("schedule","schedule");
		ConfigBean.putBeans("schedule_sub","schedule_sub");
		ConfigBean.putBeans("admin","admin");
		ConfigBean.putBeans("storage_door","storage_door");
		ConfigBean.putBeans("wms_load_order","wms_load_order");
		ConfigBean.putBeans("wms_load_order_pallet_type_count","wms_load_order_pallet_type_count");
		DBRow[] jetLags = dbUtilAutoTran.selectMutliple("select id,jet_lag from "+ConfigBean.getStringValue("product_storage_catalog"));
		
		Map<Long,Long> dateUtilJetLags = new HashMap<Long,Long>();
		for (DBRow dbRow : jetLags) 
		{
			dateUtilJetLags.put(dbRow.get("id",0l),dbRow.get("jet_lag",0l));
		}
		
		//DateUtil.setStroageJetLeg(dateUtilJetLags);
		
//		DBRow[] c = floorSpaceResourcesRelationMgr.findResourceByEquipment(1000005,"","YUANXINYU-01");
		long ps_id = 1000005;
		String start  = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime("2015-01-12 00:00:00", ps_id));
		String end  = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime("2015-01-12 23:59:59", ps_id));
//		DBRow[] c = floorCheckInMgrZJ.equipmentReport(ps_id,"2015-01-01 00:00:00","2015-01-14 23:59:59");
//		DBRow[] c = floorCheckInMgrZJ.gateActivityReport(ps_id,"2015-01-01 00:00:00","2015-01-14 23:59:59");
//		DBRow[] c = floorCheckInMgrZJ.noTaskEntry(ps_id,null);
		DBRow[] b = floorCheckInMgrZJ.taskActivityReport(ps_id,start,end);
//		DBRow[] a = floorCheckInMgrZJ.workingOnWarehouse(ps_id, null);
//		floorCheckInMgrZJ.noTaskLabor(ps_id, new long[]{1000002}, new long[]{0},null);
		
//		System.out.println(DBRowUtils.dbRowArrayAsJSON(c));
		
		System.out.println(DBRowUtils.dbRowArrayAsJSON(b));
	}
}
