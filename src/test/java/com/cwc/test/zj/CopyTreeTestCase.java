package com.cwc.test.zj;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.app.floor.api.FloorProductStoreMgr;

public class CopyTreeTestCase extends BaseTestCase {
	
	@Autowired
	private FloorProductStoreMgr floorProductStoreMgr;
	
	@Test
	public void test1() throws Exception
	{
		long ps_id = 100000;
		long slc_id = 1019752;
		long title_id = 18;
		long pc_id = 1093152;
		long con_id = 1301091;
		String lot_number = "";
		String time_number = "";
		
		Map<String,Object> locProps = new HashMap<String, Object>();
		locProps.put("slc_id",slc_id);
		locProps.put("pc_id",pc_id);
		locProps.put("title_id",title_id);
		locProps.put("lot_number",lot_number);
		locProps.put("time_number",time_number);
		
		floorProductStoreMgr.copyTree(0,ps_id,con_id,locProps);
	}
}
