package com.cwc.test.zj;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.app.api.mgr.YMSMgrAPI;
import com.cwc.app.floor.api.zj.FloorSQLServerMgrZJ;
import com.cwc.app.floor.api.zj.FloorStorageApproveMgrZJ;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class DemoTestCase2 extends BaseTestCase {
	
	@Resource
	YMSMgrAPI ymsMgrAPI;
	@Resource
	DBUtilAutoTran dbUtilAutoTran;
	
	@Test
	public void test_1() 
		throws Exception 
	{
		ConfigBean.putBeans("config_search_warehouse_location", "config_search_warehouse_location");
		ConfigBean.putBeans("appointment_temp", "appointment_temp");
		ConfigBean.putBeans("product_storage_catalog","product_storage_catalog");
		ConfigBean.putBeans("door_or_location_occupancy_details","door_or_location_occupancy_details");
		ConfigBean.putBeans("door_or_location_occupancy_main","door_or_location_occupancy_main");
		ConfigBean.putBeans("entry_equipment","entry_equipment");
		ConfigBean.putBeans("schedule","schedule");
		ConfigBean.putBeans("schedule_sub","schedule_sub");
		ConfigBean.putBeans("admin","admin");
		ConfigBean.putBeans("storage_door","storage_door");
		ConfigBean.putBeans("wms_load_order","wms_load_order");
		ConfigBean.putBeans("wms_load_order_pallet_type_count","wms_load_order_pallet_type_count");
//		ConfigBean.putBeans("","");
//		ConfigBean.putBeans("","");
		
		DBRow[] jetLags = dbUtilAutoTran.selectMutliple("select id,jet_lag from "+ConfigBean.getStringValue("product_storage_catalog") +" where id in(1000005,1000007,1000018)");
		
		Map<Long,Long> dateUtilJetLags = new HashMap<Long,Long>();
		for (DBRow dbRow : jetLags) 
		{
			dateUtilJetLags.put(dbRow.get("id",0l),dbRow.get("jet_lag",0l));
//			DateUtil.setStroageJetLeg(dateUtilJetLags);
			ymsMgrAPI.getAppointment(dbRow.get("id",0l),"2015-02-04","2015-02-08");
			
		}
		
		
		long ps_id = 1000005;
		String start = "2015-01-03 00:00:00";
		String end = "2015-01-03 23:59:59";
		
		
//		ymsMgrAPI.getAppointment(1000005,"2015-01-17","2015-01-17");
//		System.out.println(DBRowUtils.dbRowArrayAsJSON(ymsMgrAPI.scordOutBoundReport(ps_id, start, end, null)));
//		System.out.println(DBRowUtils.dbRowArrayAsJSON(ymsMgrAPI.scordInBoundReport(ps_id, start, end, null)));
//		System.out.println("scheduleInBound:"+DBRowUtils.dbRowArrayAsJSON(ymsMgrAPI.scheduleInBound(ps_id, start,end,null)));
//		System.out.println("scheduleOutBound:"+DBRowUtils.dbRowArrayAsJSON(ymsMgrAPI.scheduleOutBound(ps_id, start,end,null)));
//		System.out.println("equipmentReport:"+DBRowUtils.dbRowArrayAsJSON(ymsMgrAPI.equipmentReport(ps_id, start, end)));
//		System.out.println("taskActivityReport:"+DBRowUtils.dbRowArrayAsJSON(ymsMgrAPI.taskActivityReport(ps_id, start, end)));
	}
}
