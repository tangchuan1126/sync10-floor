package com.cwc.test.zj;

import java.util.HashMap;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.key.ContainerHasSnTypeKey;
import com.cwc.app.key.ContainerProductState;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;

public class CreateNORMTLPTestCase extends BaseTestCase {
	@Autowired
	private FloorProductStoreMgr floorProductStoreMgr;
	
	@Ignore
	@Test
	public void createTLPNode()
		throws Exception
	{
		long ps_id = 0;
		long con_id = System.currentTimeMillis()/1000;
		
		DBRow[] rows = new DBRow[10];
		
		for (int i = 0; i < rows.length; i++) 
		{
			 //像图数据库添加节点
			 rows[i] = new DBRow();
			 rows[i].add("con_id",con_id+i);
			 rows[i].add("container_type",ContainerTypeKey.TLP);
			 rows[i].add("type_id",-1);
			 rows[i].add("container","TLP");
			 rows[i].add("is_full",ContainerProductState.FULL);
			 rows[i].add("is_has_sn",ContainerHasSnTypeKey.NOSN);
			 rows[i].add("inner_product","papaer");
			 rows[i].add("volume",i);
			 rows[i].add("volume_unit","LBS");
		}		
		 
		floorProductStoreMgr.addNodes(ps_id,NodeType.Container,rows);
	}
	
	@Test
	public void getPapaerTLP()
		throws Exception
	{
		Map<String,Object> searchFor = new HashMap<String, Object>();
		searchFor.put("inner_product","papaer");
//		searchFor.put("volume",2);
		searchFor.put("volume_unit","LBS");
		DBRow[] rows = floorProductStoreMgr.searchNodes(0, NodeType.Container, searchFor);
		
		System.out.println(DBRowUtils.dbRowArrayAsJSON(rows));
	}
}
