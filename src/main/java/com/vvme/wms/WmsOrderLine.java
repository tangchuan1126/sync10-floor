package com.vvme.wms;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.cwc.db.DBRow;

@SuppressWarnings("serial")
public class WmsOrderLine implements Serializable {
	final static String ENDUSER = "END USER";
	private Long orderNo;
	private Date orderedDate;
	private String customerID;
	private String HUB;
	private String DN = "";
	private String SO;
	private String PO;
	
	private int printed;
	
	private String title;
	private String model;
	private String lotNO;
	private int orderQty;
	private float totalWeight;
	private float masterCartonQty;
	private float totalPallets;
	private String linearUnit;
	private String weightUnit;
	private String itemShipInstruction;
	private Integer lineNO;
	
	private String shipToType;
	private String shipToPartyName;
	private String shipToAddress1;
	private String ShipToAddress2;
	private String shipToCity;
	private String shipToState;
	private String shipToZipCode;
	private String shipToCountry;
	
	private Date shipNotBefore;
	private Date shipNoLater;
	private Date reqShipDate;
	private Date MABD;
	private String SCAC;
	private String freightTerms;
	private String billTo;
	private String soldToName1;
	private String soldToName2;
	private String loadNO;
	private String freightType;
	private String notes;
	private String trackingNO;
	private String shippingInstruction;
	private String pickUpDateAppt;
	private String pickUpTimeAppt;
	private int status = 1;
	
	//辅助字段
	private Long shipFromId;
	private String companyId;
	
	public Long getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}
	public Date getOrderedDate() {
		return orderedDate;
	}
	public void setOrderedDate(Date orderedDate) {
		this.orderedDate = orderedDate;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getHUB() {
		return HUB;
	}
	public void setHUB(String hUB) {
		HUB = hUB;
	}
	public String getDN() {
		return DN;
	}
	public void setDN(String dN) {
		if (dN==null) dN = "";
		DN = dN;
	}
	public String getSO() {
		return SO;
	}
	public void setSO(String sO) {
		SO = sO;
	}
	public String getPO() {
		return PO;
	}
	public void setPO(String pO) {
		PO = pO;
	}
	
	
	public int getPrinted() {
		return printed;
	}
	public void setPrinted(int printed) {
		this.printed = printed;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getLotNO() {
		return lotNO;
	}
	public void setLotNO(String lotNO) {
		this.lotNO = lotNO;
	}
	public int getOrderQty() {
		return orderQty;
	}
	public void setOrderQty(int orderQty) {
		this.orderQty = orderQty;
	}
	public float getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(float totalWeight) {
		this.totalWeight = totalWeight;
	}
	public float getMasterCartonQty() {
		return masterCartonQty;
	}
	public void setMasterCartonQty(float masterCartonQty) {
		this.masterCartonQty = masterCartonQty;
	}
	public float getTotalPallets() {
		return totalPallets;
	}
	public void setTotalPallets(float totalPallets) {
		this.totalPallets = totalPallets;
	}
	public String getLinearUnit() {
		return linearUnit;
	}
	public void setLinearUnit(String linearUnit) {
		this.linearUnit = linearUnit;
	}
	public String getWeightUnit() {
		return weightUnit;
	}
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}
	public String getItemShipInstruction() {
		return itemShipInstruction;
	}
	public void setItemShipInstruction(String itemShipInstruction) {
		this.itemShipInstruction = itemShipInstruction;
	}
	public Integer getLineNO() {
		return lineNO;
	}
	public void setLineNO(Integer lineNO) {
		this.lineNO = lineNO;
	}
	public String getShipToType() {
		return shipToType;
	}
	public void setShipToType(String shipToType) {
		//B2C
		if (ENDUSER.equalsIgnoreCase(shipToType))  {
			this.shipToType = ENDUSER;
		} else if("Retailer".equalsIgnoreCase(shipToType)) {
			//EDI 取soldToName1对应值。
			this.shipToType = "";
		} else {
			this.shipToType = shipToType;
		}
	}
	public String getShipToPartyName() {
		return shipToPartyName;
	}
	public void setShipToPartyName(String shipToPartyName) {
		this.shipToPartyName = shipToPartyName;
	}
	public String getShipToAddress1() {
		return shipToAddress1;
	}
	public void setShipToAddress1(String shipToAddress1) {
		this.shipToAddress1 = shipToAddress1;
	}
	public String getShipToAddress2() {
		return ShipToAddress2;
	}
	public void setShipToAddress2(String shipToAddress2) {
		ShipToAddress2 = shipToAddress2;
	}
	public String getShipToCity() {
		return shipToCity;
	}
	public void setShipToCity(String shipToCity) {
		this.shipToCity = shipToCity;
	}
	public String getShipToState() {
		return shipToState;
	}
	public void setShipToState(String shipToState) {
		this.shipToState = shipToState;
	}
	public String getShipToZipCode() {
		return shipToZipCode;
	}
	public void setShipToZipCode(String shipToZipCode) {
		this.shipToZipCode = shipToZipCode;
	}
	public String getShipToCountry() {
		return shipToCountry;
	}
	public void setShipToCountry(String shipToCountry) {
		this.shipToCountry = shipToCountry;
	}
	public Date getShipNotBefore() {
		return shipNotBefore;
	}
	public void setShipNotBefore(Date shipNotBefore) {
		this.shipNotBefore = shipNotBefore;
	}
	public Date getShipNoLater() {
		return shipNoLater;
	}
	public void setShipNoLater(Date shipNoLater) {
		this.shipNoLater = shipNoLater;
	}
	public Date getReqShipDate() {
		return reqShipDate;
	}
	public void setReqShipDate(Date reqShipDate) {
		this.reqShipDate = reqShipDate;
	}
	public Date getMABD() {
		return this.MABD;
	}
	public void setMABD(Date mABD) {
		this.MABD = mABD;
	}
	public String getSCAC() {
		return this.SCAC;
	}
	public void setSCAC(String sCAC) {
		this.SCAC = sCAC;
	}
	public String getFreightTerms() {
		return freightTerms;
	}
	public void setFreightTerms(String freightTerms) {
		this.freightTerms = freightTerms;
	}
	public String getBillTo() {
		return billTo;
	}
	public void setBillTo(String billTo) {
		this.billTo = billTo;
	}
	public String getSoldToName1() {
		return soldToName1;
	}
	public void setSoldToName1(String soldToName1) {
		this.soldToName1 = soldToName1;
	}
	public String getSoldToName2() {
		return soldToName2;
	}
	public void setSoldToName2(String soldToName2) {
		this.soldToName2 = soldToName2;
	}
	public String getLoadNO() {
		return loadNO;
	}
	public void setLoadNO(String loadNO) {
		this.loadNO = loadNO;
	}
	public String getFreightType() {
		return freightType;
	}
	public void setFreightType(String freightType) {
		this.freightType = freightType;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getTrackingNO() {
		return trackingNO;
	}
	public void setTrackingNO(String trackingNO) {
		this.trackingNO = trackingNO;
	}
	public String getShippingInstruction() {
		return shippingInstruction;
	}
	public void setShippingInstruction(String shippingInstruction) {
		this.shippingInstruction = shippingInstruction;
	}
	public String getPickUpDateAppt() {
		return pickUpDateAppt;
	}
	public void setPickUpDateAppt(String pickUpDateAppt) {
		this.pickUpDateAppt = pickUpDateAppt;
	}
	public String getPickUpTimeAppt() {
		return pickUpTimeAppt;
	}
	public void setPickUpTimeAppt(String pickUpTimeAppt) {
		this.pickUpTimeAppt = pickUpTimeAppt;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Long getShipFromId() {
		return shipFromId;
	}
	public void setShipFromId(Long shipFromId) {
		this.shipFromId = shipFromId;
	}
	
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
	public WmsOrderLine() {
		
	}
	
	public WmsOrderLine(DBRow rs, Long shipFromId, String companyId) throws Exception {
		this(rs);
		this.setShipFromId(shipFromId);
		this.setCompanyId(companyId);
		//this.setCustomerID(companyId);
	}
	
	private WmsOrderLine(DBRow rs) throws SQLException, ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		this.setOrderNo(rs.get("OrderNo", 0L));
		if (rs.getString("OrderedDate")!=null) this.setOrderedDate(sdf.parse(rs.get("OrderedDate","")));
		this.setHUB(rs.get("HUB",""));
		this.setCustomerID(rs.get("CustomerID",""));
		this.setDN(rs.get("DN",""));
		this.setSO(rs.get("SO",""));
		this.setPO(rs.get("PONo",""));
		
		this.setPrinted("true".equalsIgnoreCase(rs.getString("ShippingLabelPrinted"))?1:0);
		
		this.setTitle(rs.get("title",""));
		this.setModel(rs.get("Model",""));
		this.setLotNO(rs.get("LotNo",""));
		this.setOrderQty(rs.get("OrderedQty",0));
		this.setTotalWeight(rs.get("Weight",0f));
		this.setMasterCartonQty(rs.get("CartonQty",0f));
		this.setTotalPallets(rs.get("PalletQty",0f));
		
		this.setShipToType(rs.get("ShipToType",""));
		this.setShipToPartyName(rs.get("ShipToName",""));
		this.setShipToAddress1(rs.get("ShipToAddress1",""));
		this.setShipToAddress2("");
		this.setShipToCity(rs.get("ShipToCity",""));
		this.setShipToState(rs.get("ShipToState",""));
		this.setShipToZipCode(rs.get("ShipToZipCode",""));
		this.setShipToCountry(rs.get("ShipToCountry",""));
		this.setShipNotBefore((Date)rs.get("ShipNotBefore"));
		this.setShipNoLater((Date)rs.get("ShipNoLater"));
		if (rs.getString("RequestedDate")!=null)
			this.setReqShipDate(sdf.parse(rs.get("RequestedDate","")));
		if (rs.getString("MABD")!=null)
			this.setMABD(sdf.parse(rs.get("MABD","")));
		
		//this.setMABD(rs.getDate("MABD"));
		this.setSCAC(rs.get("SCAC",""));
		this.setFreightTerms(rs.get("FreightTerm",""));
		this.setBillTo(rs.get("BillToID",""));
		this.setLoadNO(rs.get("LoadNo",""));
		this.setFreightType(rs.get("FreightType",""));
		this.setNotes(rs.get("Note",""));
		if ("Picking".equalsIgnoreCase(rs.get("Status", "")) || "Picked".equalsIgnoreCase(rs.get("Status", ""))) {
			this.setStatus(2);
		}
	}
	
	public WmsOrderLine(ResultSet rs, Long shipFromId, String vId) throws SQLException, ParseException {
		this.setShipFromId(shipFromId);
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			if (rs.getString("OrderedDate")!=null) this.setOrderedDate(sdf.parse(rs.getString("OrderedDate")));
			this.setHUB(rs.getString("HUB"));
			this.setCustomerID(rs.getString("CustomerID"));
			this.setDN(rs.getString("DN"));
			this.setSO(rs.getString("SO"));
			this.setPO(rs.getString("PONo"));
			
			this.setTitle(rs.getString("title"));
			this.setModel(rs.getString("Model"));
			this.setLotNO(rs.getString("LotNo"));
			this.setOrderQty(rs.getInt("OrderedQty"));
			this.setTotalWeight(rs.getFloat("Weight"));
			this.setMasterCartonQty(rs.getFloat("CartonQty"));
			this.setTotalPallets(rs.getFloat("PalletQty"));
			
			this.setShipToType(rs.getString("ShipToType"));
			this.setShipToPartyName(rs.getString("ShipToName"));
			this.setShipToAddress1(rs.getString("ShipToAddress1"));
			this.setShipToAddress2("");
			this.setShipToCity(rs.getString("ShipToCity"));
			this.setShipToState(rs.getString("ShipToState"));
			this.setShipToZipCode(rs.getString("ShipToZipCode"));
			this.setShipToCountry(rs.getString("ShipToCountry"));
			this.setShipNotBefore(rs.getDate("ShipNotBefore"));
			this.setShipNoLater(rs.getDate("ShipNoLater"));
			if (rs.getString("RequestedDate")!=null)
				this.setReqShipDate(sdf.parse(rs.getString("RequestedDate")));
			if (rs.getString("MABD")!=null)
				this.setMABD(sdf.parse(rs.getString("MABD")));
			//this.setMABD(rs.getDate("MABD"));
			this.setSCAC(rs.getString("SCAC"));
			this.setFreightTerms(rs.getString("FreightTerm"));
			this.setBillTo(rs.getString("BillToID"));
			this.setLoadNO(rs.getString("LoadNo"));
			this.setFreightType(rs.getString("FreightType"));
			this.setNotes(rs.getString("Note"));
			if ("Picking".equalsIgnoreCase(rs.getString("Status")) || "Picked".equalsIgnoreCase(rs.getString("Status"))) {
				this.setStatus(2);
			}
	}
}