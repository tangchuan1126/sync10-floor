package com.vvme.wms;

public interface ReceiptWriter {

	/**
	 * 保存RN-》RO
	 * 
	 * @param order
	 * @return
	 * @throws Exception
	 */
	public long write(Receipt order) throws Exception;
}