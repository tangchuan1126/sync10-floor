package com.vvme.wms;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;

public class Receipt implements Serializable {
	private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private static final long serialVersionUID = 1L;
	
	protected Long id = 0L;
	
	protected Long psId = 0L;
	
	protected String hub = "";

	protected String companyId="";

	protected Long receiptNo = 0L;

	protected String status="Imported";

	protected Long customerKey;

	protected String customerId;

	protected Long supplierKey;

	protected String supplierId;

	protected Date enteredDate;

	protected String warehouseId;

	protected String referenceNo="";

	protected String poNo;

	protected String bolNo;

	protected String containerNo;

	protected String seals;

	protected String scac;

	protected String carrierId="";

	protected Date scheduleDate;

	protected Date appointmentDate;

	protected Date inYardDate;

	protected Date devannerDate;

	protected Date lastFreeDate;

	protected String receiptType ="receipt";

	protected String note;

	protected Boolean cartonLabelPrinted=false;

	protected Boolean platePrinted = false;

	protected Boolean receiptTicketPrinted =false;

	protected Boolean tallySheetPrinted = false;

	protected Boolean receiptPrinted = false;

	protected String linkSequenceNo;

	protected String reControlNo;

	protected Boolean ediOrder = false;

	protected Boolean send944 = false;

	protected Boolean send947 = false;

	protected Date dateCreated;

	protected String userCreated;

	protected Date dateUpdated = new Date();

	protected String userUpdated;

	protected Long detailId = null;

	protected Date printDate;

	protected Long printUser = null;

	protected Integer printNumber;

	protected Long closeUser;

	Set<ReceiptLine> lines = new HashSet<ReceiptLine>();
	
	

	public Set<ReceiptLine> getLines() {
		return lines;
	}
	public void setLines(Set<ReceiptLine> lines) {
		this.lines = lines;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPsId() {
		return psId;
	}

	public void setPsId(Long psId) {
		this.psId = psId;
	}
	
	public String getHub() {
		return hub;
	}
	public void setHub(String hub) {
		this.hub = hub;
	}

	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public Long getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(Long receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getCustomerKey() {
		return customerKey;
	}

	public void setCustomerKey(Long customerKey) {
		this.customerKey = customerKey;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Long getSupplierKey() {
		return supplierKey;
	}

	public void setSupplierKey(Long supplierKey) {
		this.supplierKey = supplierKey;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public Date getEnteredDate() {
		return enteredDate;
	}
	
	public void setEnteredDate(Date enteredDate) {
		this.enteredDate = enteredDate;
	}

	public String getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(String warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public String getBolNo() {
		return bolNo;
	}

	public void setBolNo(String bolNo) {
		this.bolNo = bolNo;
	}

	public String getContainerNo() {
		return containerNo;
	}

	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}

	public String getSeals() {
		return seals;
	}

	public void setSeals(String seals) {
		this.seals = seals;
	}

	public String getScac() {
		return scac;
	}

	public void setScac(String scac) {
		this.scac = scac;
	}

	public String getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}

	public Date getScheduleDate() {
		return scheduleDate;
	}

	public void setScheduleDate(Date scheduleDate) {
		this.scheduleDate = scheduleDate;
	}
	
	public Date getAppointmentDate() {
		return appointmentDate;
	}
	
	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public Date getInYardDate() {
		return inYardDate;
	}
	
	public void setInYardDate(Date inYardDate) {
		this.inYardDate = inYardDate;
	}

	public Date getDevannerDate() {
		return devannerDate;
	}
	
	public void setDevannerDate(Date devannerDate) {
		this.devannerDate = devannerDate;
	}

	public Date getLastFreeDate() {
		return lastFreeDate;
	}
	
	public void setLastFreeDate(Date lastFreeDate) {
		this.lastFreeDate = lastFreeDate;
	}

	public String getReceiptType() {
		return receiptType;
	}

	public void setReceiptType(String receiptType) {
		this.receiptType = receiptType;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Boolean getCartonLabelPrinted() {
		return cartonLabelPrinted;
	}

	public void setCartonLabelPrinted(Boolean cartonLabelPrinted) {
		this.cartonLabelPrinted = cartonLabelPrinted;
	}

	public Boolean getPlatePrinted() {
		return platePrinted;
	}

	public void setPlatePrinted(Boolean platePrinted) {
		this.platePrinted = platePrinted;
	}

	public Boolean getReceiptTicketPrinted() {
		return receiptTicketPrinted;
	}

	public void setReceiptTicketPrinted(Boolean receiptTicketPrinted) {
		this.receiptTicketPrinted = receiptTicketPrinted;
	}

	public Boolean getTallySheetPrinted() {
		return tallySheetPrinted;
	}

	public void setTallySheetPrinted(Boolean tallySheetPrinted) {
		this.tallySheetPrinted = tallySheetPrinted;
	}

	public Boolean getReceiptPrinted() {
		return receiptPrinted;
	}

	public void setReceiptPrinted(Boolean receiptPrinted) {
		this.receiptPrinted = receiptPrinted;
	}

	public String getLinkSequenceNo() {
		return linkSequenceNo;
	}

	public void setLinkSequenceNo(String linkSequenceNo) {
		this.linkSequenceNo = linkSequenceNo;
	}

	public String getReControlNo() {
		return reControlNo;
	}

	public void setReControlNo(String reControlNo) {
		this.reControlNo = reControlNo;
	}

	public Boolean getEdiOrder() {
		return ediOrder;
	}

	public void setEdiOrder(Boolean ediOrder) {
		this.ediOrder = ediOrder;
	}

	public Boolean getSend944() {
		return send944;
	}

	public void setSend944(Boolean send944) {
		this.send944 = send944;
	}

	public Boolean getSend947() {
		return send947;
	}

	public void setSend947(Boolean send947) {
		this.send947 = send947;
	}

	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		if (dateUpdated==null) {
			this.dateUpdated = new Date();
		} else {
			this.dateUpdated = dateUpdated;
		}
	}

	public String getUserUpdated() {
		return userUpdated;
	}

	public void setUserUpdated(String userUpdated) {
		this.userUpdated = userUpdated;
	}

	public Long getDetailId() {
		return detailId;
	}

	public void setDetailId(Long detailId) {
		this.detailId = detailId;
	}

	public Date getPrintDate() {
		return printDate;
	}
	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	public Long getPrintUser() {
		return printUser;
	}

	public void setPrintUser(Long printUser) {
		this.printUser = printUser;
	}

	public Integer getPrintNumber() {
		return printNumber;
	}

	public void setPrintNumber(Integer printNumber) {
		this.printNumber = printNumber;
	}

	public Long getCloseUser() {
		return closeUser;
	}
	public void setCloseUser(Long closeUser) {
		this.closeUser = closeUser;
	}
	
	public Receipt() {}
	
	public Receipt(final WmsReceiptLine wms, DBRow pc, long psid) {
		this.setEnteredDate(wms.getEnteredDate());
		this.setPsId(psid);
		
		
		this.setCustomerId(wms.getCustomerID());

		this.setCompanyId(wms.getWarehouseID());
		this.setWarehouseId(wms.getWarehouseID());
		
		this.setHub(wms.getHUB());
		this.setSupplierId(wms.getTitle());
		
		this.setReferenceNo(wms.getRN());
		this.setLinkSequenceNo(wms.getRN());

		this.setPoNo(wms.getPO());
		this.setBolNo(wms.getBOLNo());
		this.setContainerNo(wms.getContainerNo());
		this.setCarrierId(wms.getSCAC());
		
		this.setScheduleDate(wms.getScheduledDate());
		this.setInYardDate(wms.getInYardDate());
		this.setDevannerDate(wms.getDevannedDate());
		this.setNote(wms.getNotes());
		this.setEdiOrder(true);
		
		this.setUserCreated("HON");
		this.setDateCreated(new Date());
		
		this.addLine(wms, pc, this);
		
	}
	public Receipt(final DBRow wms, DBRow pc, long psid) {
		this.setEnteredDate(getDate(wms.get("ReceiptDate"),psid));
		this.setPsId(psid);
		
		this.setCustomerId(wms.get("Customer", ""));
		this.setHub(wms.get("HUB", ""));
		this.setCompanyId(wms.get("Company", ""));
		this.setWarehouseId(wms.get("Warehouse", ""));
		
		
		
		this.setSupplierId(wms.get("Title", ""));
		
		this.setReferenceNo(wms.get("RN", ""));
		this.setLinkSequenceNo(wms.get("RN", ""));

		this.setPoNo(wms.get("PO", ""));
		this.setBolNo(wms.get("BOLNO", ""));
		this.setContainerNo(wms.get("ContainerNo", ""));
		this.setCarrierId(wms.get("Carrier", ""));
		
		this.setScheduleDate(getDate(wms.get("ScheduleDate"),psid));
		//this.setInYardDate(getDate(wms.get("InYardDate"),psid));
		//this.setDevannerDate(getDate(wms.get("DevannerDate"),psid));
		this.setNote(wms.get("Notes", ""));
		this.setEdiOrder(true);
		
		//wms.get("ExpectedCartons", 0);
		
		this.setUserCreated("EDI001");
		Date now = new Date();
		this.setDateCreated(now);
		this.setUserUpdated("EDI001");
		this.setDateUpdated(now);
		
		this.addLine(wms, pc, this);
	}
	
	private static Date getDate(Object obj,long psid) {
		if (obj==null|| "".equals(obj)) return null;
		try {
			//System.out.println(DateUtil.showUTCTime(String.valueOf(obj),psid));
			//System.out.println(DateUtil.StringToDate(DateUtil.showUTCTime(String.valueOf(obj),psid)));
			return DateUtil.StringToDate(DateUtil.showUTCTime(String.valueOf(obj),psid));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void addLine(WmsReceiptLine wms, DBRow pc, Receipt order) {
		ReceiptLine line = new ReceiptLine();
		//line.setReceipt(order);
		line.setCompanyId(this.getCompanyId());
		line.setWarehouseId(this.getWarehouseId());
		
		line.setLocationId(this.getWarehouseId());
		
		line.setSupplier(wms.getTitle());
		line.setItemId(wms.getModel());
		line.setLotNo(wms.getLotNO());
		line.setExpectedQty(wms.getExpectedQty());
		line.setPallets(wms.getTotalPallets());
		line.setNote(wms.getItemInstruction());
		line.setPoNo(wms.getPO());
		line.setPoLineNo(1);
		line.setLineNo(this.lines.size()+1);
		
		if (pc!=null) {
			line.setQtyPerPallet(pc.get("qty_per_pallet", 0));
			line.setPcId(pc.get("pc_id", 0L));
			line.setSnSize(pc.get("sn_size", 0));
		}
		line.setWmsItem(wms.getModel());
		line.setWmsLotNo(wms.getLotNO());
		line.setWmsQty(wms.getExpectedQty());
		this.lines.add(line);
	}
	
	public void addLine(DBRow wms, DBRow pc, Receipt order) {
		ReceiptLine line = new ReceiptLine();
		line.setCompanyId(this.getCompanyId());
		line.setWarehouseId(this.getWarehouseId());
		
		line.setLocationId(this.getWarehouseId());
		
		line.setSupplier(wms.get("Title", ""));
		line.setItemId(wms.get("Model", ""));
		line.setLotNo(wms.get("LotNO", ""));
		line.setExpectedQty(wms.get("ExpectedQty", 0));
		line.setPallets(wms.get("Pallets", 0D));
		line.setNote(wms.get("ItemShipInstruction", ""));
		line.setPoNo(wms.get("PO", ""));
		line.setPoLineNo(1);
		line.setLineNo(this.lines.size()+1);
		
		if (pc!=null) {
			line.setQtyPerPallet(pc.get("qty_per_pallet", 1));
			line.setPcId(pc.get("pc_id", 0L));
			line.setSnSize(pc.get("sn_size", 0));
		}
		line.setWmsItem(wms.get("Model", ""));
		line.setWmsLotNo(wms.get("LotNO", ""));
		line.setWmsQty(wms.get("ExpectedQty", 0));
		this.lines.add(line);
	}
	
	public DBRow toDBRow() {
		DBRow rn = new DBRow();
		rn.add("id", this.getId());
		rn.add("companyId", this.getCompanyId());
		rn.add("receiptNo", this.getReceiptNo());
		rn.add("status", "Imported");
		rn.add("customerId", this.getCustomerId());
		rn.add("supplierId", this.getSupplierId());
		if (this.getEnteredDate()!=null) {
			rn.add("enteredDate", df.format(this.getEnteredDate()));
		} else {
			rn.addNull("enteredDate");
		}
		rn.add("warehouseId", this.getWarehouseId());
		rn.add("referenceNo", this.getReferenceNo());
		
		rn.add("poNo", this.getPoNo());
		rn.add("bolNo", this.getBolNo());
		rn.add("containerNo", this.getContainerNo());
		
		rn.add("seals", this.getSeals());//
		rn.add("carrierId", this.getCarrierId());
		if (this.getScheduleDate()!=null) {
			rn.add("scheduleDate", df.format(this.getScheduleDate()));
		} else {
			rn.addNull("scheduleDate");
		}
		if (this.getAppointmentDate()!=null) {
			rn.add("appointmentDate", df.format(this.getAppointmentDate()));
		} else {
			rn.addNull("appointmentDate");
		}
		if (this.getInYardDate()!=null) {
			rn.add("inYardDate", df.format(this.getInYardDate()));
		} else {
			rn.addNull("inYardDate");
		}
		if (this.getDevannerDate()!=null) {
			rn.add("devannerDate", df.format(this.devannerDate));
		} else {
			rn.addNull("devannerDate");
		}
		
		if (this.getLastFreeDate()!=null) {
			rn.add("lastFreeDate", df.format(this.getLastFreeDate()));
		} else {
			rn.addNull("lastFreeDate");
		}
		
		rn.add("receiptType", this.getReceiptType());
		rn.add("note", this.getNote());
		
		rn.add("linkSequenceNo", this.getLinkSequenceNo());
		rn.add("reControlNo", this.getReControlNo());
		
		rn.add("ediOrder", this.getEdiOrder()?1:0);
		rn.add("send944", this.getSend944()?1:0);
		rn.add("send947", this.getSend947()?1:0);
		
		rn.add("dateCreated", df.format(this.getDateCreated()));
		rn.add("userCreated", this.getUserCreated());
		
		rn.add("dateUpdated", df.format(this.getDateUpdated()));
		rn.add("userUpdated", this.getUserUpdated());
		return rn;
	}
	
	public DBRow[] toLines() {
		List<DBRow> lns = new ArrayList<DBRow>();
		for (ReceiptLine line: lines) {
			lns.add(line.toDBRow());
		}
		return lns.toArray( new DBRow[0]);
	}
}
