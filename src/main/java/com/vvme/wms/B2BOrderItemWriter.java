package com.vvme.wms;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;


public class B2BOrderItemWriter implements B2BOrderWriter {
	
	private DBUtilAutoTran_Optm dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public long write(B2BOrder order) throws Exception  {
		//log.info(order.getDN());
		long id = writeMain(order);
		writeLine(order.getLines(), id, order.getHUB());
		return id;
	}
	
	public long write(B2BOrder order, long customer_id) throws Exception  {
		long id = writeMain(order, customer_id);
		writeLine(order.getLines(), id, order.getHUB());
		return id;
	}
	
	public void deleteOrders(String WXX) throws SQLException {
		System.out.println("deleteOrders");
		dbUtilAutoTran.executeSQL("delete from b2b_order where source_type=2 and warehouse_id ='"+ WXX+"'");
		dbUtilAutoTran.executeSQL("delete from b2b_order_item where warehouse_id='"+ WXX+"'");
		//jdbcSync.execute("delete from b2b_order where source_type=2");
		//jdbcSync.execute("delete from b2b_order_item where warehouse_id = 'W12'");
		//jdbcSync.execute("delete from b2b_order_log where b2ber_id=0");
	}
	
	/**
	 * 删除明细
	 * 
	 * @param oId
	 * @throws SQLException
	 */
	public void deleteOrderLines(long oId) {
		try {
			//DBRow[] rows = dbUtilAutoTran.selectMutliple("select b2b_detail_id from b2b_order_item where b2b_oid="+ oId);
			//if (rows!=null && rows.length>0) {
				dbUtilAutoTran.executeSQL("delete from b2b_order_item where b2b_oid="+ oId);
			//}
		} catch (Exception e) {}
	}
	
	/**
	 * 保存order,指定客户ID
	 * 
	 * @param order
	 * @param customer_id
	 * @return
	 * @throws Exception
	 */
	private long writeMain(final B2BOrder order, long customer_id) throws Exception {
		//final String sql = "INSERT INTO `b2b_order`(customer_dn,retail_po,ship_to_party_name,deliver_house_number, deliver_city,address_state_deliver,deliver_zip_code,send_psid,receive_psid,company_id,order_type,customer_id,freight_carrier,b2b_order_status,purchase_id,put_status,freight_term,source_type,bill_to_id,bill_to_name ,deliver_pro_id, deliver_ccid, account_id, b2b_order_address,b2b_order_linkman,b2b_order_linkman_phone,b2b_order_date,create_account,create_account_id,requested_date,customer_order_date,mabd) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		DBRow data = new DBRow();
		data.add("customer_dn", order.getDN());
		data.add("retail_po", order.getPO());
		data.add("b2b_order_number", order.getOrderNumber());
		
		data.add("ship_to_party_name",order.getShipToPartyName());
		data.add("deliver_house_number",order.getShipToAddress1());
		
		data.add("deliver_city",order.getShipToCity());
		data.add("address_state_deliver",order.getShipToState());
		data.add("deliver_zip_code",order.getShipToZipCode());
		data.add("send_psid",order.getShipFromId());
		data.add("receive_psid",order.getShipToId());
		
		data.add("company_id",order.getCompanyId());
		
		data.add("order_type",order.getShipToType());
		data.add("customer_id", customer_id);
		data.add("tag",order.getPrinted());
		
		data.add("freight_carrier",order.getFreightType());
		data.add("b2b_order_status", order.getStatus());
		data.add("purchase_id", 0);//purchase_id
		data.add("put_status", 1);//put_status
		data.add("freight_term",order.getFreightTerms());
		data.add("source_type", order.getSourceType());//source_type:2
		data.add("bill_to_id", order.getBillToId());//bill_to_id
		data.add("bill_to_name", order.getBillTo());
		data.add("deliver_pro_id", order.getShipToStateId());
		data.add("deliver_ccid",  order.getShipToCountryId());
		data.add("account_id", order.getAccountId());
		
		data.add("b2b_order_address", order.getShipToAddress1());//辅助字段 b2b_order_address
		
		data.add("b2b_order_linkman", "");//b2b_order_linkman
		data.add("b2b_order_linkman_phone", "");//b2b_order_linkman_phone
		data.add("b2b_order_date", new Date());
		data.add("create_account", "");
		data.add("create_account_id", 0);
		data.add("requested_date", order.getReqShipDate());
		data.add("customer_order_date", order.getOrderedDate());
		data.add("mabd", order.getMABD());
		data.add("warehouse_id", order.getHUB());//W??
		
		data.add("updatedate", new Date());
		//同步打印状态
		return dbUtilAutoTran.insertOrUpdateReturnId("b2b_order"," updatedate=VALUES(updatedate), tag=VALUES(tag)", data);
	}
	
	private long writeMain(final B2BOrder order) throws Exception {
		//final String sql = "INSERT INTO `b2b_order`(customer_dn,retail_po,ship_to_party_name,deliver_house_number, deliver_city,address_state_deliver,deliver_zip_code,send_psid,receive_psid,company_id,order_type,customer_id,freight_carrier,b2b_order_status,purchase_id,put_status,freight_term,source_type,bill_to_id,bill_to_name ,deliver_pro_id, deliver_ccid, account_id, b2b_order_address,b2b_order_linkman,b2b_order_linkman_phone,b2b_order_date,create_account,create_account_id,requested_date,customer_order_date,mabd) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		if (order.getCustomerID().equalsIgnoreCase("VZB")) {
			return writeMain(order, 5L);
		} else {
			return writeMain(order, 4L);
		}
		/*
		DBRow data = new DBRow();
		data.add("customer_dn", order.getDN());
		data.add("retail_po", order.getPO());
		data.add("b2b_order_number", order.getOrderNumber());
		
		data.add("ship_to_party_name",order.getShipToPartyName());
		data.add("deliver_house_number",order.getShipToAddress1());
		
		data.add("deliver_city",order.getShipToCity());
		data.add("address_state_deliver",order.getShipToState());
		data.add("deliver_zip_code",order.getShipToZipCode());
		data.add("send_psid",order.getShipFromId());
		data.add("receive_psid",order.getShipToId());
		data.add("company_id",order.getCompanyId());
		data.add("order_type",order.getShipToType());
		if (order.getCustomerID().equalsIgnoreCase("VZB")) {
			data.add("customer_id", 5L);
		} else {
			data.add("customer_id", 4L);
		}
		data.add("tag",order.getPrinted());
		
		
		data.add("freight_carrier",order.getFreightType());
		data.add("b2b_order_status", order.getStatus());
		data.add("purchase_id", 0);//purchase_id
		data.add("put_status", 1);//put_status
		data.add("freight_term",order.getFreightTerms());
		data.add("source_type", 2);//source_type
		data.add("bill_to_id", 0);//bill_to_id
		data.add("bill_to_name", order.getBillTo());
		data.add("deliver_pro_id", order.getShipToStateId());
		data.add("deliver_ccid",  order.getShipToCountryId());
		data.add("account_id", order.getAccountId());
		
		data.add("b2b_order_address",order.getShipToAddress1());//辅助字段
		data.add("b2b_order_linkman", "");//b2b_order_linkman
		data.add("b2b_order_linkman_phone", "");//b2b_order_linkman_phone
		data.add("b2b_order_date", new Date());
		data.add("create_account", "");
		data.add("create_account_id", 0);
		data.add("requested_date", order.getReqShipDate());
		data.add("customer_order_date", order.getOrderedDate());
		data.add("mabd", order.getMABD());
		data.add("warehouse_id", order.getHUB());
		
		data.add("updatedate", new Date());
		
		return dbUtilAutoTran.insertOrUpdateReturnId("b2b_order"," updatedate=VALUES(updatedate)", data);
		*/
	}
	
	private void writeLine(List<B2BOrderLine> lines, Long orderId, String HUB) throws Exception {
		if (lines==null || lines.size()==0) throw new Exception("No Orderline");
		this.deleteOrderLines(orderId);
		for (int i=0;i<lines.size();i++) {
			this.writeLine(lines.get(i), orderId, HUB, i+1);
		}
	}
	
	private void writeLine(final B2BOrderLine line, final long orderId, String HUB, final int idx) throws Exception {
		//final String sql = "insert b2b_order_item (line_no,title,b2b_pc_id,material_number,lot_number,blp_type_id,clp_type_id,b2b_volume,b2b_normal_count,b2b_union_count,b2b_p_code,b2b_delivery_count,b2b_send_count,b2b_reap_count,b2b_p_name,item_id,b2b_count,b2b_wait_count,b2b_weight,total_weight,pallet_spaces,boxes,note,b2b_oid,warehouse_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		DBRow data = new DBRow();
		data.add("line_no",(idx+1));
		data.add("title",line.getTitle());
		data.add("b2b_pc_id",0);
		data.add("material_number",line.getLotNO());
		data.add("lot_number",line.getLotNO());
		data.add("blp_type_id",0);//blp_type_id
		data.add("clp_type_id",0);//clp_type_id
		data.add("b2b_volume",0);//b2b_volume
		data.add("b2b_normal_count",0);//b2b_normal_count
		data.add("b2b_union_count",0);//b2b_union_count
		data.add("b2b_p_code",line.getPcode());
		data.add("b2b_delivery_count",line.getOrderQty());
		data.add("b2b_send_count",0);//b2b_send_count
		data.add("b2b_reap_count",0);//b2b_reap_count
		data.add("b2b_p_name",line.getPname());
		data.add("item_id",line.getModel());
		data.add("b2b_count",line.getOrderQty());
		data.add("b2b_wait_count",line.getOrderQty());
		data.add("b2b_weight",0);
		data.add("total_weight",line.getTotalWeight());
		data.add("pallet_spaces",line.getTotalPallets());
		data.add("boxes",line.getMasterCartonQty());
		data.add("note",line.getItemShipInstruction());
		data.add("b2b_oid",orderId);
		data.add("warehouse_id",HUB);
		data.add("so", line.getSO());
		data.add("retail_po", line.getPO());
		dbUtilAutoTran.insertOrUpdateReturnId("b2b_order_item", " b2b_count=VALUES(b2b_count), b2b_wait_count=VALUES(b2b_wait_count), total_weight=VALUES(total_weight), pallet_spaces=VALUES(pallet_spaces), note=VALUES(note)", data);
	}
}
