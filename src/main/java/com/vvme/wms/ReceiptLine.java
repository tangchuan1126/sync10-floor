package com.vvme.wms;

import java.io.Serializable;
import java.util.Date;

import com.cwc.db.DBRow;

public class ReceiptLine implements Serializable {
	private static final long serialVersionUID = 1L;
	
	protected Long id = 0L;

	Receipt receipt;

	protected String companyId;

	protected Long receiptNo = 0L;

	protected Integer lineNo;

	protected String itemId;

	protected String warehouseId;

	protected String zoneId;

	protected String locationId;

	protected String palletSize;

	protected String poNo;

	protected Integer poLineNo;

	protected String lotNo;

	protected Integer goodsTotal;

	protected Integer damageTotal;

	protected Integer expectedQty;

	protected Double pallets;

	protected String note;

	protected Integer qtyPerPallet;

	protected Boolean isCheckSn = true;

	protected Integer snSize;

	protected Boolean isCheckLotNo = false;

	protected String wmsItem;

	protected String wmsLotNo;

	protected Integer wmsQty;

	protected String supplier;

	protected Integer status = 0;

	protected Boolean isSkuType = true;

	protected Long pcId = 0L;

	protected Date closeDate;

	protected Long closeUser;

	protected Long detailId;

	protected Date unloadingStartTime;

	protected Date unloadingFinishTime;

	protected Long countedUser;

	protected Long scanStartAdid;

	protected Date scanStartTime;

	protected Long scanEndAdid;

	protected Date scanEndTime;

	protected Long closeAdid;

	protected Date closeTime;

	protected Date assignTime;

	protected Date ccStartTime;

	protected Date ccFinishTime;

	protected Long ccUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Receipt getReceipt() {
		return receipt;
	}

	public void setReceipt(Receipt receipt) {
		this.receipt = receipt;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public Long getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(Long receiptNo) {
		this.receiptNo = receiptNo;
	}

	public Integer getLineNo() {
		return lineNo;
	}

	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(String warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getPalletSize() {
		return palletSize;
	}

	public void setPalletSize(String palletSize) {
		this.palletSize = palletSize;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public Integer getPoLineNo() {
		return poLineNo;
	}

	public void setPoLineNo(Integer poLineNo) {
		this.poLineNo = poLineNo;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public Integer getGoodsTotal() {
		return goodsTotal;
	}

	public void setGoodsTotal(Integer goodsTotal) {
		this.goodsTotal = goodsTotal;
	}

	public Integer getDamageTotal() {
		return damageTotal;
	}

	public void setDamageTotal(Integer damageTotal) {
		this.damageTotal = damageTotal;
	}

	public Integer getExpectedQty() {
		return expectedQty;
	}

	public void setExpectedQty(Integer expectedQty) {
		this.expectedQty = expectedQty;
	}

	public Double getPallets() {
		return pallets;
	}

	public void setPallets(Double pallets) {
		this.pallets = pallets;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getQtyPerPallet() {
		return qtyPerPallet;
	}

	public void setQtyPerPallet(Integer qtyPerPallet) {
		this.qtyPerPallet = qtyPerPallet;
	}

	public Boolean getIsCheckSn() {
		return isCheckSn;
	}

	public void setIsCheckSn(Boolean isCheckSn) {
		this.isCheckSn = isCheckSn;
	}

	public Integer getSnSize() {
		return snSize;
	}

	public void setSnSize(Integer snSize) {
		this.snSize = snSize;
	}

	public Boolean getIsCheckLotNo() {
		return isCheckLotNo;
	}

	public void setIsCheckLotNo(Boolean isCheckLotNo) {
		this.isCheckLotNo = isCheckLotNo;
	}

	public String getWmsItem() {
		return wmsItem;
	}

	public void setWmsItem(String wmsItem) {
		this.wmsItem = wmsItem;
	}

	public String getWmsLotNo() {
		return wmsLotNo;
	}

	public void setWmsLotNo(String wmsLotNo) {
		this.wmsLotNo = wmsLotNo;
	}

	public Integer getWmsQty() {
		return wmsQty;
	}

	public void setWmsQty(Integer wmsQty) {
		this.wmsQty = wmsQty;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Boolean getIsSkuType() {
		return isSkuType;
	}

	public void setIsSkuType(Boolean isSkuType) {
		this.isSkuType = isSkuType;
	}

	public Long getPcId() {
		return pcId;
	}

	public void setPcId(Long pcId) {
		this.pcId = pcId;
	}

	public Date getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	public Long getCloseUser() {
		return closeUser;
	}

	public void setCloseUser(Long closeUser) {
		this.closeUser = closeUser;
	}

	public Long getDetailId() {
		return detailId;
	}

	public void setDetailId(Long detailId) {
		this.detailId = detailId;
	}

	public Date getUnloadingStartTime() {
		return unloadingStartTime;
	}

	public void setUnloadingStartTime(Date unloadingStartTime) {
		this.unloadingStartTime = unloadingStartTime;
	}

	public Date getUnloadingFinishTime() {
		return unloadingFinishTime;
	}
	public void setUnloadingFinishTime(Date unloadingFinishTime) {
		this.unloadingFinishTime = unloadingFinishTime;
	}

	public Long getCountedUser() {
		return countedUser;
	}
	public void setCountedUser(Long countedUser) {
		this.countedUser = countedUser;
	}

	public Long getScanStartAdid() {
		return scanStartAdid;
	}

	public void setScanStartAdid(Long scanStartAdid) {
		this.scanStartAdid = scanStartAdid;
	}

	public Date getScanStartTime() {
		return scanStartTime;
	}
	public void setScanStartTime(Date scanStartTime) {
		this.scanStartTime = scanStartTime;
	}

	public Long getScanEndAdid() {
		return scanEndAdid;
	}

	public void setScanEndAdid(Long scanEndAdid) {
		this.scanEndAdid = scanEndAdid;
	}

	public Date getScanEndTime() {
		return scanEndTime;
	}
	public void setScanEndTime(Date scanEndTime) {
		this.scanEndTime = scanEndTime;
	}

	public Long getCloseAdid() {
		return closeAdid;
	}

	public void setCloseAdid(Long closeAdid) {
		this.closeAdid = closeAdid;
	}

	public Date getCloseTime() {
		return closeTime;
	}
	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}

	public Date getAssignTime() {
		return assignTime;
	}
	public void setAssignTime(Date assignTime) {
		this.assignTime = assignTime;
	}

	public Date getCcStartTime() {
		return ccStartTime;
	}
	public void setCcStartTime(Date ccStartTime) {
		this.ccStartTime = ccStartTime;
	}

	public Date getCcFinishTime() {
		return ccFinishTime;
	}
	
	public void setCcFinishTime(Date ccFinishTime) {
		this.ccFinishTime = ccFinishTime;
	}

	public Long getCcUser() {
		return ccUser;
	}

	public void setCcUser(Long ccUser) {
		this.ccUser = ccUser;
	}
	
	public DBRow toDBRow() {
		DBRow line = new DBRow();
		line.add("companyId", this.getCompanyId());
		line.add("receiptNo", this.getReceiptNo());
		
		line.add("lineNo", this.getLineNo());
		
		line.add("itemId", this.getItemId());
		
		line.add("warehouseId", this.getWarehouseId());
		
		line.add("zoneId", this.getZoneId());
		line.add("locationId", this.getLocationId());
		
		
		line.add("palletSize", this.getPalletSize());
		line.add("poNo", this.getPoNo());
		line.add("poLineNo", this.getPoLineNo());
		
		line.add("lotNo", this.getLotNo());
		line.add("expectedQty", this.getExpectedQty());
		
		line.add("pallets", this.getPallets());
		line.add("note", this.getNote());
		return line;
	}
}