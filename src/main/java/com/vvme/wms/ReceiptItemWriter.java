package com.vvme.wms;

import java.util.Set;

import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

public class ReceiptItemWriter implements ReceiptWriter {
	
	private DBUtilAutoTran_Optm dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public long write(Receipt order) throws Exception {
		long id = writeMain(order);
		writeLine(order.getLines(), id, order.getCustomerId(), order.getPsId());
		return id;
	}
	
	private long writeMain(Receipt order) throws Exception {
		//INSERT INTO `RECEIVE_ORDER`.`receipts` (`ps_id`, `company_id`, `receipt_no`, `status`, `customer_key`, `customer_id`, `supplier_key`, `supplier_id`, `entered_date`, `warehouse_id`, `reference_no`, `po_no`, `bol_no`, `container_no`, `seals`, `scac`, `carrier_id`, `schedule_date`, `appointment_date`, `in_yard_date`, `devanner_date`, `last_free_date`, `receipt_type`, `note`, `carton_label_printed`, `plate_printed`, `receipt_ticket_printed`, `tally_sheet_printed`, `receipt_printed`, `link_sequence_no`, `re_control_no`, `edi_order`, `send944`, `send947`, `date_created`, `user_created`, `date_updated`, `user_updated`, `detail_id`, `print_date`, `print_user`, `print_number`, `close_user`) VALUES ;
		DBRow data = new DBRow();
		Long psId = order.getPsId();
		
		data.add("ps_id", order.getPsId());
		data.add("hub", order.getHub());
		data.add("company_id", order.getCompanyId());
		data.add("receipt_no", order.getReceiptNo());
		data.add("status", order.getStatus());
		
		data.add("customer_key",order.getCustomerKey());
		data.add("customer_id",order.getCustomerId());
		data.add("supplier_key",order.getSupplierKey());
		data.add("supplier_id",order.getSupplierId());
		if (order.getEnteredDate()==null) {
			data.add("entered_date", null);//
		} else {
			data.add("entered_date",order.getEnteredDate());//
		}
		
		data.add("warehouse_id",order.getWarehouseId());
		data.add("reference_no",order.getReferenceNo());
		data.add("po_no",order.getPoNo());
		data.add("bol_no",order.getBolNo());
		data.add("container_no",order.getContainerNo());
		data.add("seals",order.getSeals());
		data.add("scac",order.getScac());
		data.add("carrier_id",order.getCarrierId());
		
		if (order.getScheduleDate()==null) {
			data.add("schedule_date", null);//
		} else {
			data.add("schedule_date",order.getScheduleDate());//
		}
		if (order.getAppointmentDate()==null) {
			data.add("appointment_date", null);//
		} else {
			data.add("appointment_date",order.getAppointmentDate());//
		}
		if (order.getInYardDate()==null) {
			data.add("in_yard_date",null);
		} else {
			data.add("in_yard_date",order.getInYardDate());//
		}
		if (order.getDevannerDate()==null) {
			data.add("devanner_date",null);
		} else {
			data.add("devanner_date", order.getDevannerDate());//
		}
		if (order.getLastFreeDate()==null) {
			data.add("last_free_date",null);
		} else {
			data.add("last_free_date", order.getLastFreeDate());
		}
		
		data.add("receipt_type",order.getReceiptType());
		data.add("note",order.getNote());
		data.add("carton_label_printed",order.getCartonLabelPrinted()?1:0);
		data.add("plate_printed",order.getPlatePrinted()?1:0);
		data.add("receipt_ticket_printed",order.getReceiptTicketPrinted()?1:0);
		data.add("tally_sheet_printed",order.getTallySheetPrinted()?1:0);
		data.add("receipt_printed",order.getReceiptPrinted()?1:0);
		data.add("link_sequence_no",order.getLinkSequenceNo());
		data.add("re_control_no",order.getReControlNo());
		data.add("edi_order",order.getEdiOrder()?1:0);
		data.add("send944",order.getSend944()?1:0);
		data.add("send947",order.getSend947()?1:0);
		if (order.getDateCreated()==null) {
			data.add("date_created",null);
		} else {
			data.add("date_created", order.getDateCreated());//
		}
		data.add("user_created",order.getUserCreated());
		if (order.getDateUpdated()==null) {
			data.add("date_updated",null);
		} else {
			data.add("date_updated",order.getDateUpdated());//
		}
		data.add("user_updated",order.getUserUpdated());
		if (order.getDetailId()!=null) {
			data.add("detail_id",order.getDetailId());
		}
		if (order.getPrintDate()==null) {
			data.add("print_date",null);
		} else {
			data.add("print_date", order.getPrintDate());//
		}
		if (order.getPrintUser()!=null) {
			data.add("print_user",order.getPrintUser());
		}
		if (order.getPrintNumber()!=null) {
			data.add("print_number",order.getPrintNumber());
		}
		if (order.getCloseUser()!=null) {
			data.add("close_user",order.getCloseUser());
		}
		
		return dbUtilAutoTran.insertReturnId("receipts", data);
	}
	
	private void writeLine(Set<ReceiptLine> lines, long id, String hub, long psId) throws Exception {
		if (lines==null || lines.size()==0) throw new Exception("No Orderline");
		int i = 0;
		for (ReceiptLine line: lines) {
			i++;
			if (!checkOrderLines(id, i)) {
				throw new Exception("Line is pass");
			}
			writeLine(line, id, hub, psId, i);
		}
	}
	
	private boolean checkOrderLines(long id, int lineNo) {
		//TODO 是否已经处理。
		return true;
	}
	
	private long writeLine(final ReceiptLine order, final long orderId, String HUB, long psId, final int idx) throws Exception {
		//INSERT INTO `RECEIVE_ORDER`.`receipt_lines` (`receipt_id`, `company_id`, `receipt_no`, `line_no`, `item_id`, `warehouse_id`, `zone_id`, `location_id`, `pallet_size`, `po_no`, `po_line_no`, `lot_no`, `goods_total`, `damage_total`, `expected_qty`, `pallets`, `note`, `qty_per_pallet`, `is_check_sn`, `sn_size`, `is_check_lot_no`, `wms_item`, `wms_lot_no`, `wms_qty`, `supplier`, `status`, `is_sku_type`, `pc_id`, `close_date`, `close_user`, `detail_id`, `unloading_start_time`, `unloading_finish_time`, `counted_user`, `scan_start_adid`, `scan_start_time`, `scan_end_adid`, `scan_end_time`, `close_adid`, `close_time`, `assign_time`, `cc_start_time`, `cc_finish_time`, `cc_user`) VALUES ;
		DBRow data = new DBRow();
		data.add("receipt_id", orderId);
		data.add("company_id",order.getCompanyId());
		data.add("receipt_no",order.getReceiptNo());
		data.add("line_no",order.getLineNo());
		data.add("item_id",order.getItemId());
		data.add("warehouse_id",order.getWarehouseId());
		data.add("zone_id",order.getZoneId());
		data.add("location_id",order.getLocationId());
		data.add("pallet_size",order.getPalletSize());
		data.add("po_no",order.getPoNo());
		data.add("po_line_no",order.getPoLineNo());
		data.add("lot_no",order.getLotNo());
		if (order.getGoodsTotal()!=null) {
			data.add("goods_total",order.getGoodsTotal());
		}
		if (order.getDamageTotal()!=null) {
			data.add("damage_total",order.getDamageTotal());
		}
		data.add("expected_qty",order.getExpectedQty());
		data.add("pallets",order.getPallets());
		data.add("note",order.getNote());
		data.add("qty_per_pallet",order.getQtyPerPallet());
		data.add("is_check_sn",order.getIsCheckSn()?1:0);
		data.add("sn_size",order.getSnSize());
		data.add("is_check_lot_no",order.getIsCheckLotNo()?1:0);
		data.add("wms_item",order.getWmsItem());
		data.add("wms_lot_no",order.getWmsLotNo());
		data.add("wms_qty",order.getWmsQty());
		data.add("supplier",order.getSupplier());
		data.add("status",order.getStatus());
		data.add("is_sku_type",order.getIsSkuType()?1:0);
		data.add("pc_id",order.getPcId());
		if (order.getCloseDate()==null) {
			data.add("close_date", null);//
		} else {
			data.add("close_date", order.getCloseDate());//
		}
		if (order.getCloseUser()!=null) {
			data.add("close_user",order.getCloseUser());
		}
		if (order.getDetailId()!=null) {
			data.add("detail_id",order.getDetailId());
		}
		if (order.getUnloadingStartTime()==null) {
			data.add("unloading_start_time", null);//
		} else {
			data.add("unloading_start_time",order.getUnloadingStartTime());//
		}
		if (order.getUnloadingFinishTime()==null) {
			data.add("unloading_finish_time", null);//
		} else {
			data.add("unloading_finish_time",order.getUnloadingFinishTime());//
		}
		if (order.getCountedUser()!=null) {
			data.add("counted_user",order.getCountedUser());
		}
		if (order.getScanStartAdid()!=null) {
			data.add("scan_start_adid",order.getScanStartAdid());
		}
		if (order.getScanStartTime()==null) {
			data.add("scan_start_time", null);//
		} else {
			data.add("scan_start_time",order.getScanStartTime());//
		}
		if (order.getScanEndAdid()!=null) {
			data.add("scan_end_adid",order.getScanEndAdid());
		}
		
		if (order.getScanEndTime()==null) {
			data.add("scan_end_time", null);//
		} else {
			data.add("scan_end_time",order.getScanEndTime());//
		}
		if (order.getCloseAdid()!=null) {
			data.add("close_adid",order.getCloseAdid());
		}
		if (order.getCloseTime()==null) {
			data.add("close_time", null);//
		} else {
			data.add("close_time",order.getCloseTime());//
		}
		
		if (order.getAssignTime()==null) {
			data.add("assign_time", null);//
		} else {
			data.add("assign_time",order.getAssignTime());//
		}
		if (order.getCcStartTime()==null) {
			data.add("cc_start_time", null);//
		} else {
			data.add("cc_start_time",order.getCcStartTime());//
		}
		if (order.getCcFinishTime()==null) {
			data.add("cc_finish_time", null);
		} else {
			data.add("cc_finish_time",order.getCcFinishTime());//
		}
		if (order.getCcUser()!=null) {
			data.add("cc_user",order.getCcUser());
		}
		
		
		return dbUtilAutoTran.insertReturnId("receipt_lines", data);
		//return 0L;
	}
}