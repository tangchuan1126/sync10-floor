package com.vvme.wms;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class WmsReceiptLine implements Serializable {
	
	private Date enteredDate;
	
	private String customerID;
	private String HUB;
	
	private String warehouseID;//辅助字段
	
	private String RN = "";
	
	private String PO;
	private String BOLNo;
	private String containerNo;
	private String SCAC;
	
	private String soldToName1;//辅助字段
	private String soldToName2;//辅助字段
	
	private Date scheduledDate;//ETA
	private Date inYardDate;
	private Date devannedDate;
	private String receiptType;
	
	private String notes;
	
	private String title;
	private String model;
	private String lotNO;
	private int expectedQty;
	private double masterCartonQty;
	private double totalPallets;
	private String itemInstruction;
	
	private double totalWeight;
	private String weightUnit;
	
	public Date getEnteredDate() {
		return enteredDate;
	}
	public void setEnteredDate(Date enteredDate) {
		this.enteredDate = enteredDate;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getHUB() {
		return HUB;
	}
	public void setHUB(String hUB) {
		HUB = hUB;
	}
	public String getWarehouseID() {
		return warehouseID;
	}
	public void setWarehouseID(String warehouseID) {
		this.warehouseID = warehouseID;
	}
	public String getRN() {
		return RN;
	}
	public void setRN(String rN) {
		RN = rN;
	}
	public String getPO() {
		return PO;
	}
	public void setPO(String pO) {
		PO = pO;
	}
	public String getBOLNo() {
		return BOLNo;
	}
	public void setBOLNo(String bOLNo) {
		BOLNo = bOLNo;
	}
	public String getContainerNo() {
		return containerNo;
	}
	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}
	public String getSCAC() {
		return SCAC;
	}
	public void setSCAC(String sCAC) {
		SCAC = sCAC;
	}
	public String getSoldToName1() {
		return soldToName1;
	}
	public void setSoldToName1(String soldToName1) {
		this.soldToName1 = soldToName1;
	}
	public String getSoldToName2() {
		return soldToName2;
	}
	public void setSoldToName2(String soldToName2) {
		this.soldToName2 = soldToName2;
	}
	public Date getScheduledDate() {
		return scheduledDate;
	}
	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}
	public Date getInYardDate() {
		return inYardDate;
	}
	public void setInYardDate(Date inYardDate) {
		this.inYardDate = inYardDate;
	}
	public Date getDevannedDate() {
		return devannedDate;
	}
	public void setDevannedDate(Date devannedDate) {
		this.devannedDate = devannedDate;
	}
	public String getReceiptType() {
		return receiptType;
	}
	public void setReceiptType(String receiptType) {
		this.receiptType = receiptType;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getLotNO() {
		return lotNO;
	}
	public void setLotNO(String lotNO) {
		this.lotNO = lotNO;
	}
	public int getExpectedQty() {
		return expectedQty;
	}
	public void setExpectedQty(int expectedQty) {
		this.expectedQty = expectedQty;
	}
	public double getMasterCartonQty() {
		return masterCartonQty;
	}
	public void setMasterCartonQty(double masterCartonQty) {
		this.masterCartonQty = masterCartonQty;
	}
	public double getTotalPallets() {
		return totalPallets;
	}
	public void setTotalPallets(double totalPallets) {
		this.totalPallets = totalPallets;
	}
	public String getItemInstruction() {
		return itemInstruction;
	}
	public void setItemInstruction(String itemInstruction) {
		this.itemInstruction = itemInstruction;
	}
	
	public double getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(double totalWeight) {
		this.totalWeight = totalWeight;
	}
	
	public String getWeightUnit() {
		return weightUnit;
	}
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}
}