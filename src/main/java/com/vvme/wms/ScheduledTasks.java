package com.vvme.wms;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTranSQLServer;

public class ScheduledTasks {
	private static final Log log = LogFactory.getLog(ScheduledTasks.class);
	
	
	private WMSLineItemReader b2b_reader;
	
	private B2BOrderWriter b2b_writer;
	
	private ShipToNameValidate shipTo;
	
	private DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer;
	
	public ScheduledTasks() {
		
	}
	
	public ScheduledTasks(ScheduledTasks task) {
		this.b2b_reader = task.b2b_reader;
		this.b2b_writer = task.b2b_writer;
		this.shipTo = task.shipTo;
		this.dbUtilAutoTranSQLServer = task.dbUtilAutoTranSQLServer;
	}
	
	public void init2() {
	}
	
	public void setB2b_reader(WMSLineItemReader b2b_reader) {
		this.b2b_reader = b2b_reader;
	}
	public void setB2b_writer(B2BOrderWriter b2b_writer) {
		this.b2b_writer = b2b_writer;
	}
	public void setShipTo(ShipToNameValidate shipTo) {
		this.shipTo = shipTo;
	}
	public void setDbUtilAutoTranSQLServer(DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer) {
		this.dbUtilAutoTranSQLServer = dbUtilAutoTranSQLServer;
	}

	public int run(String... strings) throws Exception {
		this.init();
		log.info("Run Task..");
		shipTo.init();
		
		//b2b_writer.deleteOrders("");
		//System.out.println("del");
		
		List<B2BOrder> orders = this.step1(1000005L);
		write(orders);
		//System.out.println("step1");
		
		List<B2BOrder> orders2 = this.step2(1000018L);
		write(orders2);
		//System.out.println("step2");
		
		List<B2BOrder> orders3 = this.step3(1000006L);
		write(orders3);
		//System.out.println("step3");
		log.info("Run Task Finish.");
		return 1;
	}
	
	public List<Long> run (String key) throws Exception {
		List<Long> rt = new ArrayList<Long>();
		long ps_id = Long.valueOf(key);
		synchronized(key) {
			shipTo.init();
			log.info("Run Task..");
			if (ps_id==1000005) {
				//b2b_writer.deleteOrders("W12");
				List<B2BOrder> orders = this.step1(ps_id);
				rt = write(orders);
			} else if (ps_id==1000018) {
				//b2b_writer.deleteOrders("W14");
				List<B2BOrder> orders2 = this.step2(ps_id);
				rt = write(orders2);
			} else if (ps_id==1000006) {
				//b2b_writer.deleteOrders("W29");
				List<B2BOrder> orders3 = this.step3(ps_id);
				rt = write(orders3);
			} else {
				
			}
			log.info("Run Task Finish.");
		}
		return rt;
	}
	
	public List<Long> write(List<B2BOrder> datas) {
		if (datas==null) {
			log.warn("no list");
			return null;
		}
		List<String> ErrorDN = new ArrayList<String>();
		List<Long> ids = new ArrayList<Long>();
		//log.info("call write");
		for (B2BOrder order : datas) {
			try {
				ids.add(b2b_writer.write(order));
			} catch (Exception e) {
				ErrorDN.add(order.getDN());
				log.warn(e);
			}
		}
		return ids;
	}
	
	public List<B2BOrder> step1(long ps) throws Exception {
		DBRow[] results = dbUtilAutoTranSQLServer.selectMutliple("select m.OrderNo,CONVERT(bit, (CONVERT(int,m.CartonLabelPrinted)+CONVERT(int,m.PalletLabelPrinted)+CONVERT(int,m.PickTicketPrinted)+CONVERT(int,m.UCCLabelPrinted)+CONVERT(int,m.ShippingLabelPrinted)+CONVERT(int,m.TallySheetPrinted)+CONVERT(int,m.PackingListPrinted)+CONVERT(int,m.PackingSerialNosPrinted)+CONVERT(int,m.BillOfLadingPrinted))) as ShippingLabelPrinted, CONVERT(VARCHAR(10),m.OrderedDate,101) as [OrderedDate], m.CustomerID, m.CompanyID as [HUB], m.ReferenceNo as [DN], '' as [SO], d.PONo ,d.SupplierID as [title], d.ItemID as [Model], d.LotNo, d.OrderedQty, d.Pallets*d.PalletWeight as [Weight],"
				+ " d.PrePackQty as CartonQty, d.Pallets as [PalletQty], case when m.AccountID = m.CustomerID then 'END USER' else (select RetailerID from CustomerAccounts as ca where ca.CompanyID = m.CompanyID and ca.CustomerID= m.CustomerID and ca.AccountID = m.AccountID ) end as [ShipToType], "
				+ " m.ShipToName, m.ShipToAddress1,m.ShipToCity,m.ShipToState, m.ShipToZipCode, case when ISNULL(m.ShipToCountry, '')='' then 'United States' else m.ShipToCountry end as [ShipToCountry], null as [ShipNotBefore], null as [ShipNoLater], CONVERT(VARCHAR(10),m.RequestedDate,101) as [RequestedDate], "
				+ " CONVERT(VARCHAR(10),m.RequestedDate,101) as [MABD], '' as [SCAC], m.FreightTerm, m.BillToID, '' as [LoadNo], case when m.CarrierID = 'CONSOL' then 'CONSOL' when m.CarrierID ='IMDL' then 'IMDL' else '' END as [FreightType], m.Note, null as PickUpDateAppt, null as PickUpTimeAppt, null as ShipDate, m.Status as [Status], null as [BOL] "
				+ " from Orders m left join OrderLines d on m.CompanyID = d.CompanyID and m.OrderNo = d.OrderNo where m.Status in ('Imported','Open','Picking','Picked') and (m.CompanyID ='W12' AND m.CustomerID in ('VIZIO','VZB')) AND d.SupplierID !='VIZIORET' order by m.OrderNo");
		List<WmsOrderLine> wline = new ArrayList<WmsOrderLine>();
		//log.info("step1_");
		for (DBRow row:results) {
			wline.add(new WmsOrderLine(row, ps, "V102"));
		}
		//log.info("step1");
		return b2b_reader.read(wline);
	}
	
	public List<B2BOrder> step2(long ps) throws Exception {
		DBRow[] results = dbUtilAutoTranSQLServer.selectMutliple("select m.OrderNo,CONVERT(bit, (CONVERT(int,m.CartonLabelPrinted)+CONVERT(int,m.PalletLabelPrinted)+CONVERT(int,m.PickTicketPrinted)+CONVERT(int,m.UCCLabelPrinted)+CONVERT(int,m.ShippingLabelPrinted)+CONVERT(int,m.TallySheetPrinted)+CONVERT(int,m.PackingListPrinted)+CONVERT(int,m.PackingSerialNosPrinted)+CONVERT(int,m.BillOfLadingPrinted))) as ShippingLabelPrinted, CONVERT(VARCHAR(10),m.OrderedDate,101) as [OrderedDate], m.CustomerID, m.CompanyID as [HUB], m.ReferenceNo as [DN], '' as [SO], d.PONo ,d.SupplierID as [title], d.ItemID as [Model], d.LotNo, d.OrderedQty, d.Pallets*d.PalletWeight as [Weight],"
					+ "d.PrePackQty as CartonQty, d.Pallets as [PalletQty], case when m.AccountID = m.CustomerID then 'END USER' else (select RetailerID from CustomerAccounts as ca where ca.CompanyID = m.CompanyID and ca.CustomerID= m.CustomerID and ca.AccountID = m.AccountID ) end as [ShipToType], "
					+ "m.ShipToName, m.ShipToAddress1,m.ShipToCity,m.ShipToState, m.ShipToZipCode, case when ISNULL(m.ShipToCountry, '')='' then 'United States' else m.ShipToCountry end as [ShipToCountry], null as [ShipNotBefore], null as [ShipNoLater], CONVERT(VARCHAR(10),m.RequestedDate,101) as [RequestedDate], "
					+ "CONVERT(VARCHAR(10),m.RequestedDate,101) as [MABD], '' as [SCAC], m.FreightTerm, m.BillToID, '' as [LoadNo], case when m.CarrierID = 'CONSOL' then 'CONSOL' when m.CarrierID ='IMDL' then 'IMDL' else '' END as [FreightType], m.Note, null as PickUpDateAppt, null as PickUpTimeAppt, null as ShipDate, m.Status as [Status], null as [BOL] "
					+ "from Orders m left join OrderLines d on m.CompanyID = d.CompanyID and m.OrderNo = d.OrderNo where m.Status in ('Imported','Open','Picking','Picked') and (m.CompanyID ='W14' AND m.CustomerID ='VZO') AND d.SupplierID !='VIZIORET' order by m.OrderNo");
		List<WmsOrderLine> wline = new ArrayList<WmsOrderLine>();
		//log.info("step2_");
		for (DBRow row:results) {
			wline.add(new WmsOrderLine(row, ps, "V105"));
		}
		//log.info("step2");
		return b2b_reader.read(wline);
	}
	
	public List<B2BOrder> step3(long ps) throws Exception {
		DBRow[] results = dbUtilAutoTranSQLServer.selectMutliple("select m.OrderNo,CONVERT(bit, (CONVERT(int,m.CartonLabelPrinted)+CONVERT(int,m.PalletLabelPrinted)+CONVERT(int,m.PickTicketPrinted)+CONVERT(int,m.UCCLabelPrinted)+CONVERT(int,m.ShippingLabelPrinted)+CONVERT(int,m.TallySheetPrinted)+CONVERT(int,m.PackingListPrinted)+CONVERT(int,m.PackingSerialNosPrinted)+CONVERT(int,m.BillOfLadingPrinted))) as ShippingLabelPrinted, CONVERT(VARCHAR(10),m.OrderedDate,101) as [OrderedDate], m.CustomerID, m.CompanyID as [HUB], m.ReferenceNo as [DN], '' as [SO], d.PONo ,d.SupplierID as [title], d.ItemID as [Model], d.LotNo, d.OrderedQty, d.Pallets*d.PalletWeight as [Weight],"
					+ "d.PrePackQty as CartonQty, d.Pallets as [PalletQty],case when m.AccountID = m.CustomerID then 'END USER' else (select RetailerID from CustomerAccounts as ca where ca.CompanyID = m.CompanyID and ca.CustomerID= m.CustomerID and ca.AccountID = m.AccountID ) end as [ShipToType], "
					+ "m.ShipToName,m.ShipToAddress1,m.ShipToCity,m.ShipToState, m.ShipToZipCode, case when ISNULL(m.ShipToCountry, '')='' then 'United States' else m.ShipToCountry end as [ShipToCountry], null as [ShipNotBefore], null as [ShipNoLater], CONVERT(VARCHAR(10),m.RequestedDate,101) as [RequestedDate], "
					+ "CONVERT(VARCHAR(10),m.RequestedDate,101) as [MABD], '' as [SCAC], m.FreightTerm, m.BillToID, '' as [LoadNo], case when m.CarrierID = 'CONSOL' then 'CONSOL' when m.CarrierID ='IMDL' then 'IMDL' else '' END as [FreightType], m.Note, null as PickUpDateAppt, null as PickUpTimeAppt, null as ShipDate, m.Status as [Status], null as [BOL] "
					+ "from Orders m left join OrderLines d on m.CompanyID = d.CompanyID and m.OrderNo = d.OrderNo where m.Status in ('Imported','Open','Picking','Picked') and (m.CompanyID ='W29' AND m.CustomerID = 'VIZIO3' ) AND d.SupplierID !='VIZIORET' order by m.OrderNo");
		List<WmsOrderLine> wline = new ArrayList<WmsOrderLine>();
		//log.info("step3_");
		for (DBRow row:results) {
			wline.add(new WmsOrderLine(row, ps, "V110"));
		}
		//log.info("step3");
		return b2b_reader.read(wline);
	}
	
	public void init() {}
}