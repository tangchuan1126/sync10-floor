package com.vvme.wms;

import java.sql.SQLException;

public interface B2BOrderWriter {
	/**
	 * 保存order
	 * 
	 * @param order
	 * @throws Exception
	 */
	public long write(B2BOrder order) throws Exception;
	
	/**
	 * 保存order,指定客户ID
	 * 
	 * @param order
	 * @param customer_id
	 * @throws Exception
	 */
	public long write(B2BOrder order, long customer_id) throws Exception;
	
	/**
	 * 清除同步单据
	 * 
	 * @throws SQLException
	 */
	public void deleteOrders(String WXX) throws SQLException;
}