package com.vvme.wms;

import java.util.ArrayList;
import java.util.List;







//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.db.DBRow;
import com.vvme.wms.ShipToNameValidate;

@Component("reader")
public class WMSLineItemReader {
	
	//@Autowired
	private ShipToNameValidate ShipTo;
	
	private FloorProductMgr productMgr;
	
	private FloorProprietaryMgrZyj proprietaryMgr;
	
	public void setShipTo(ShipToNameValidate shipTo) {
		ShipTo = shipTo;
	}
	
	public void setProductMgr(FloorProductMgr productMgr) {
		this.productMgr = productMgr;
	}
	
	public void setProprietaryMgr(FloorProprietaryMgrZyj proprietaryMgr) {
		this.proprietaryMgr = proprietaryMgr;
	}

	public List<B2BOrder> read(List<WmsOrderLine> lines) throws Exception {
		List<B2BOrder> orders = new ArrayList<B2BOrder>();
		B2BOrder order = new B2BOrder();
		for (WmsOrderLine line: lines) {
			if (order.getDN().equals(line.getDN())) {
				order.addLine(line);
			} else {
				order = new B2BOrder(line);
				order.setShipToId(ShipTo.validate(order.getShipFromId(), order.getShipToType(), order.getShipToPartyName()));
				order.setShipToCountryId(ShipTo.getNation(order.getShipFromId(), order.getShipToType(), order.getShipToPartyName()));
				order.setShipToStateId(ShipTo.getPro(order.getShipFromId(), order.getShipToType(), order.getShipToPartyName()));
				order.setAccountId(ShipTo.getAccountId(order.getShipFromId(), order.getShipToType(), order.getShipToPartyName()));
				if (order.getShipToStateId()==0) {
					if (ShipTo.usas.containsKey(order.getShipToState())) {
						order.setShipToStateId(ShipTo.usas.get(order.getShipToState()));
						order.setShipToCountryId(11036);
					}
				}
				if (order.getShipToStateId()==0) {
					if (ShipTo.canadas.containsKey(order.getShipToState())) {
						order.setShipToStateId(ShipTo.canadas.get(order.getShipToState()));
						order.setShipToCountryId(11007);
					}
				}
				orders.add(order);
			}
		}
		return orders;
	}
	
	public List<B2BOrder> readEdi(List<DBRow> lines, String companyId, String wmsCompanyId, long shipFromId, long customerId) throws Exception {
		List<B2BOrder> orders = new ArrayList<B2BOrder>();
		B2BOrder order = new B2BOrder();
		for (DBRow line: lines) {
			if (order.getDN().equals(line.getString("DN", ""))) {
				order.addLine(line, shipFromId);
				//order.setCompanyId(companyId);
			} else {
				order = new B2BOrder(line, shipFromId);
				order.setShipToId(ShipTo.validate(order.getShipFromId(), order.getShipToType(), order.getShipToPartyName()));
				order.setShipFromId(shipFromId);
				order.setCompanyId(companyId);
				order.setHUB(wmsCompanyId);
				
				order.setShipToCountryId(ShipTo.getNation(order.getShipFromId(), order.getShipToType(), order.getShipToPartyName()));
				order.setShipToStateId(ShipTo.getPro(order.getShipFromId(), order.getShipToType(), order.getShipToPartyName()));
				order.setAccountId(ShipTo.getAccountId(order.getShipFromId(), order.getShipToType(), order.getShipToPartyName()));
				
				if (order.getShipToStateId()==0) {
					if (ShipTo.usas.containsKey(order.getShipToState())) {
						order.setShipToStateId(ShipTo.usas.get(order.getShipToState()));
						order.setShipToCountryId(11036);
					}
				}
				if (order.getShipToStateId()==0) {
					if (ShipTo.canadas.containsKey(order.getShipToState())) {
						order.setShipToStateId(ShipTo.canadas.get(order.getShipToState()));
						order.setShipToCountryId(11007);
					}
				}
				orders.add(order);
			}
		}
		return orders;
	}
	
	public List<Receipt> readRN(List<WmsReceiptLine> lines) throws Exception {
		List<Receipt> orders = new ArrayList<Receipt>();
		Receipt order = new Receipt();
		for (WmsReceiptLine line: lines) {
			DBRow pc = getPC(line);
			if (order.getReferenceNo().equals(line.getRN())) {
				order.addLine(line, pc, order);
			} else {
				order = new Receipt(line, pc, getPsId(line));
				order.setCustomerKey(getCustomerId(line));
				order.setSupplierKey(getSupplierKey(line));
				orders.add(order);
			}
		}
		return orders;
	}
	public List<Receipt> readRN(List<WmsReceiptLine> lines, Long customerId) throws Exception {
		List<Receipt> orders = new ArrayList<Receipt>();
		Receipt order = new Receipt();
		for (WmsReceiptLine line: lines) {
			DBRow pc = getPC(line);
			if (order.getReferenceNo().equals(line.getRN())) {
				order.addLine(line, pc, order);
			} else {
				order = new Receipt(line, pc, getPsId(line));
				order.setCustomerKey(customerId);
				order.setSupplierKey(getSupplierKey(line));
				orders.add(order);
			}
		}
		return orders;
	}
	private long getCustomerId(WmsReceiptLine line) {
		return 0L;
	}
	private long getSupplierKey(WmsReceiptLine line) {
		return 0L;
	}
	private DBRow getPC(WmsReceiptLine line) {
		//TODO by Model+LotNO   CustomerId Title?
		String pname ="";
		if (line.getLotNO()==null || "".equals(line.getLotNO())) {
			pname = line.getModel();
		} else {
			pname = line.getModel()+"/"+line.getLotNO();
		}
		try {
			//TODO qty_per_pallet, sn_size, is_check_sn,is_check_lot_no, is_sku_type 信息暂无
			return productMgr.getDetailProductByPname(pname);
		} catch (Exception e) {
		}
		return new DBRow();
	}
	private long getPsId(WmsReceiptLine line) {
		//TODO by HUB+CustomerID
		return 0L;
	}
	private DBRow getPC(DBRow line) {
		String pname ="";
		if ("".equals(line.get("LotNO", ""))) {
			pname = line.get("Model","");
		} else {
			pname = line.get("Model","")+"/"+line.get("LotNO","");
		}
		try {
			//TODO qty_per_pallet, sn_size, is_check_sn,is_check_lot_no, is_sku_type 信息暂无
			return productMgr.getDetailProductByPname(pname);
		} catch (Exception e) {
		}
		return new DBRow();
	}
	private long getSupplierKey(DBRow line) {
		try {
			DBRow row = proprietaryMgr.findProprietaryByTitleName(line.get("Title", ""));
			if (row!=null) {
				return row.get("title_id", 0L);
			}
		} catch (Exception e) {
		}
		return 0L;
	}
	
	public List<Receipt> readRNEdi(List<DBRow> lines, String companyId, String wmsCompanyId, long psId, long customerId) throws Exception {
		List<Receipt> orders = new ArrayList<Receipt>();
		Receipt order = new Receipt();
		for (DBRow line: lines) {
			DBRow pc = getPC(line);
			if (order.getReferenceNo().equals(line.get("RN", ""))) {
				order.addLine(line, pc, order);
			} else {
				line.put("Warehouse", wmsCompanyId);
				line.put("Company", wmsCompanyId);
				order = new Receipt(line, pc, psId);
				order.setCustomerKey(customerId);
				order.setSupplierKey(getSupplierKey(line));
				orders.add(order);
			}
		}
		return orders;
	}
}