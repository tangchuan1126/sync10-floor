package com.vvme.wms;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.stereotype.Component;



import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

public class ShipToNameValidate {
	
	private DBUtilAutoTran_Optm dbUtilAutoTran;
	
	public DBUtilAutoTran_Optm getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}
	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public Map<String,Long> usas = new HashMap<String,Long>();
	
	public Map<String,Long> canadas = new HashMap<String,Long>();
	
	Map<String,Map<String,Object>> shiptos = new HashMap<String,Map<String,Object>>();
	
	//Map<String,Map<String,Object>> nations = new HashMap<String,Map<String,Object>>();

	public void init() throws SQLException {
		
		shiptos = new HashMap<String,Map<String,Object>>();
		
		DBRow[] shiptoall = dbUtilAutoTran.selectMutliple("select id, title, ship_to_id, t.deliver_nation, t.deliver_pro_id from product_storage_catalog t where t.storage_type=5 and t.ship_to_id is not null");
		for (DBRow item: shiptoall) {
			String key = item.get("title","");
			shiptos.put(key, item);
		}
		
		usas = new HashMap<String,Long>();
		DBRow[] usap = dbUtilAutoTran.selectMutliple("select pro_id, p_code from country_province t where t.nation_id = 11036");
		for (DBRow item: usap) {
			usas.put(item.get("p_code",""), item.get("pro_id",0L));
		}
		
		canadas = new HashMap<String,Long>();
		DBRow[] canadap = dbUtilAutoTran.selectMutliple("select pro_id, p_code from country_province t where t.nation_id = 11007");
		for (DBRow item: canadap) {
			canadas.put(item.get("p_code",""), item.get("pro_id",0L));
		}
	}
	
	public void setShiptos(Map<String, Map<String, Object>> shiptos) {
		this.shiptos = shiptos;
	}

	
	public long validate(long shipFromId, String shipToType, String shipToPartyName) {
		if (shipToPartyName==null) return 0;
		if ("END USER".equalsIgnoreCase(shipToType)) {
			return shipFromId;
		} else {
			if(shiptos.containsKey(shipToPartyName.replaceAll(" ", "_"))) {
				return Long.parseLong(shiptos.get(shipToPartyName.replaceAll(" ", "_")).get("id").toString());
			} else {
				return 0;
			}
		}
	}
	
	public long getAccountId(long shipFromId, String shipToType, String shipToPartyName) {
		if (shipToPartyName==null) return 0;
		if ("END USER".equalsIgnoreCase(shipToType)) {
			return -1;
		} else {
			if(shiptos.containsKey(shipToPartyName.replaceAll(" ", "_"))) {
				return Long.parseLong(shiptos.get(shipToPartyName.replaceAll(" ", "_")).get("ship_to_id").toString());
			} else {
				return 0;
			}
		}
	}
	
	public long getNation(long shipFromId, String shipToType, String shipToPartyName) {
		if (shipToPartyName==null) return 0;
		
		if ("END USER".equalsIgnoreCase(shipToType)) {
			return 0;
		} else {
			if(shiptos.containsKey(shipToPartyName.replaceAll(" ", "_"))) {
				return Long.parseLong(shiptos.get(shipToPartyName.replaceAll(" ", "_")).get("deliver_nation").toString());
			} else {
				return 0;
			}
		}
	}
	
	public long getPro(long shipFromId, String shipToType, String shipToPartyName) {
		if (shipToPartyName==null) return 0;
		if ("END USER".equalsIgnoreCase(shipToType)) {
			return 0;
		} else {
			if(shiptos.containsKey(shipToPartyName.replaceAll(" ", "_"))) {
				return Long.parseLong(shiptos.get(shipToPartyName.replaceAll(" ", "_")).get("deliver_pro_id").toString());
			} else {
				return 0;
			}
		}
	}
}