package com.vvme.wms;

import java.io.Serializable;

@SuppressWarnings("serial")
public class B2BOrderLine implements Serializable {
	private Long pkid;
	private long oid;
	private Integer lineNO =1;
	
	private String title;
	
	private long PCID=0;
	
	private String model="";
	private String lotNO="";
	
	private int orderQty;
	private float totalWeight;
	private float masterCartonQty;
	private float totalPallets;
	
	private String linearUnit;
	private String weightUnit;
	private String itemShipInstruction;
	
	private String SO = "";
	private String PO = "";
	
	private long conID = 0;//容器ID
	
	public Long getPkid() {
		return pkid;
	}
	public void setPkid(Long pkid) {
		this.pkid = pkid;
	}
	
	public Long getOid() {
		return oid;
	}
	public void setOid(Long oid) {
		this.oid = oid;
	}
	
	public Integer getLineNO() {
		return lineNO;
	}
	public void setLineNO(Integer lineNO) {
		this.lineNO = lineNO;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public long getPCID() {
		return PCID;
	}
	public void setPCID(long pCID) {
		PCID = pCID;
	}
	
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		if (model==null) this.model ="";
		this.model = model;
	}
	
	public String getLotNO() {
		return lotNO;
	}
	public void setLotNO(String lotNO) {
		if (lotNO==null) this.lotNO = "";
		this.lotNO = lotNO;
	}
	
	public String getPcode() {
		if (this.lotNO.equals("")) {
			return this.model.toUpperCase();
		} else {
			return this.model.toUpperCase() +"/" + this.lotNO;
		}
	}
	
	public String getPname() {
		if (this.lotNO.equals("")) {
			return this.model.toUpperCase();
		} else {
			return this.model.toUpperCase() +"/" + this.lotNO;
		}
	}

	public int getOrderQty() {
		return orderQty;
	}
	public void setOrderQty(int orderQty) {
		this.orderQty = orderQty;
	}
	
	public float getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(float totalWeight) {
		this.totalWeight = totalWeight;
	}
	
	public float getMasterCartonQty() {
		return masterCartonQty;
	}
	public void setMasterCartonQty(float masterCartonQty) {
		this.masterCartonQty = masterCartonQty;
	}
	
	public float getTotalPallets() {
		return totalPallets;
	}
	public void setTotalPallets(float totalPallets) {
		this.totalPallets = totalPallets;
	}
	
	public String getLinearUnit() {
		return linearUnit;
	}
	public void setLinearUnit(String linearUnit) {
		this.linearUnit = linearUnit;
	}
	
	public String getWeightUnit() {
		return weightUnit;
	}
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}
	
	public String getItemShipInstruction() {
		return itemShipInstruction;
	}
	public void setItemShipInstruction(String itemShipInstruction) {
		this.itemShipInstruction = itemShipInstruction;
	}
	
	public String getSO() {
		return SO;
	}
	public void setSO(String sO) {
		SO = sO;
	}
	
	public String getPO() {
		return PO;
	}
	public void setPO(String pO) {
		PO = pO;
	}
	
	public long getConID() {
		return conID;
	}
	public void setConID(long conID) {
		this.conID = conID;
	}
}