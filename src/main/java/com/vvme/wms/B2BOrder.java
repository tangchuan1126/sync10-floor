package com.vvme.wms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;

@SuppressWarnings("serial")
public class B2BOrder implements Serializable {
	
	private Long pkid;
	
	private String DN="";
	
	private String customerID;
	private String HUB;
	
	private String SO = "";
	private String PO = "";
	
	private int printed = 0;
	
	private Date orderedDate;
	
	private List<B2BOrderLine> lines = new ArrayList<B2BOrderLine>();
	private String linearUnit;
	private String weightUnit;
	
	private long shipFromId;
	private long shipToId;
	
	private String shipToType;
	private String shipToPartyName;
	private String shipToAddress1;
	private String ShipToAddress2;
	private String shipToCity;
	private String shipToState;
	private String shipToZipCode;
	private String shipToCountry;
	
	private Date shipNotBefore;
	private Date shipNoLater;
	private Date reqShipDate;
	private Date MABD;
	private String SCAC;
	private String freightTerms;
	
	private String billTo;
	private String soldToName1;
	private String soldToName2;
	private String loadNO;
	private String freightType;
	private String notes;
	private String trackingNO;
	private String shippingInstruction;
	private String pickUpDateAppt;
	private String pickUpTimeAppt;
	
	private String companyId;
	
	private long shipToStateId =0;
	private long shipToCountryId =0;
	private long accountId =0;
	private String orderNumber ="";
	
	private int status=1;
	
	private int billToId=0;
	private int sourceType = 2; //来源，默认为sync
	
	
	
	public B2BOrder() {}
	
	public B2BOrder(final WmsOrderLine wms) {
		this.setOrderNumber(String.valueOf(wms.getOrderNo()));
		this.setDN(wms.getDN());
		this.setCustomerID(wms.getCustomerID());
		this.setShipFromId(wms.getShipFromId());
		this.setCompanyId(wms.getCompanyId());
		this.setHUB(wms.getHUB());
		this.setSO(wms.getSO());
		this.setPO(wms.getPO());
		this.setPrinted(wms.getPrinted());
		try {
			this.setOrderedDate(DateUtil.StringToDate(DateUtil.showUTCTime(wms.getOrderedDate(),this.getShipFromId())));
		} catch (Exception e) {
		}
		
		try {
			this.setReqShipDate(DateUtil.StringToDate(DateUtil.showUTCTime(wms.getReqShipDate(),this.getShipFromId())));
		} catch (Exception e) {
		}
		try {
			this.setMABD(DateUtil.StringToDate(DateUtil.showUTCTime(wms.getMABD(),this.getShipFromId())));
		} catch (Exception e) {
		}
		
		this.setFreightType(wms.getFreightType());
		this.setFreightTerms(wms.getFreightTerms());
		
		this.setShipToType(wms.getShipToType());
		
		this.setShipToPartyName(wms.getShipToPartyName());
		this.setShipToAddress1(wms.getShipToAddress1());
		this.setShipToAddress2(wms.getShipToAddress2());
		this.setShipToCity(wms.getShipToCity());
		this.setShipToState(wms.getShipToState());
		this.setShipToZipCode(wms.getShipToZipCode());
		this.setShipToCountry(wms.getShipToCountry());
		
		this.setShipNotBefore(wms.getShipNotBefore());
		this.setShipNoLater(wms.getShipNoLater());
		
		
		this.setBillTo(wms.getBillTo());
		this.setSoldToName1(wms.getSoldToName1());
		this.setSoldToName2(wms.getSoldToName2());
		this.setLoadNO(wms.getLoadNO());
		
		if ("".equals(getShipToType()) && !"".equals(getSoldToName1())) {
			//ShipToType 可为 End User or retailerid
			this.setShipToType(getSoldToName1());
		}
		
		this.setNotes(wms.getNotes());
		this.setTrackingNO(wms.getTrackingNO());
		this.setShippingInstruction(wms.getShippingInstruction());
		this.setPickUpDateAppt(wms.getPickUpDateAppt());
		this.setPickUpTimeAppt(wms.getPickUpTimeAppt());
		this.setStatus(wms.getStatus());
		this.addLine(wms);
	}
	
	public B2BOrder(final DBRow wms, long psid) {
		this.setShipFromId(psid);
		this.setOrderedDate(getDate(wms.get("OrderDate"),psid));
		this.setCustomerID(wms.get("Customer", ""));
		this.setHUB(wms.get("HUB", ""));
		this.setDN(wms.get("DN", ""));
		this.setSO(wms.get("SO", ""));
		this.setPO(wms.get("PO", ""));
		
		this.setReqShipDate(getDate(wms.get("ReqShipDate"),psid));
		
		this.setMABD(getDate(wms.get("MABD"),psid));
		
		
		this.setFreightType(wms.get("FreightType", ""));
		this.setFreightTerms(wms.get("FreightTerms", ""));
		
		this.setShipToType(wms.get("ShipToType", ""));
		this.setShipToPartyName(wms.get("ShipToPartyName", ""));
		this.setShipToAddress1(wms.get("ShipToAddress1", ""));
		this.setShipToAddress2(wms.get("ShipToAddress2", ""));
		this.setShipToCity(wms.get("ShipToCity", ""));
		this.setShipToState(wms.get("ShipToState", ""));
		this.setShipToZipCode(wms.get("ShipToZipCode", ""));
		this.setShipToCountry(wms.get("Country", ""));
		
		this.setShipNotBefore(getDate(wms.get("ShipNotBefore"),psid));
		this.setShipNoLater(getDate(wms.get("ShipNoLater"),psid));
		
		
		this.setBillTo(wms.get("BillTo", ""));
		this.setSoldToName1(wms.get("SoldToName1", ""));
		this.setSoldToName2(wms.get("SoldToName2", ""));
		this.setLoadNO(wms.get("LoadNO", ""));
		
		this.setNotes(wms.get("Notes", ""));
		this.setTrackingNO(wms.get("TrackingNO", ""));
		this.setShippingInstruction(wms.get("ShippingInstruction", ""));
		this.setPickUpDateAppt(null);
		this.setPickUpTimeAppt(null);
		//this.setStatus(1);
		this.addLine(wms, psid);
	}
	
	private static Date getDate(Object obj,long psid) {
		if (obj==null|| "".equals(obj)) return null;
		try {
			return DateUtil.StringToDate(DateUtil.showUTCTime(String.valueOf(obj),psid));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public long getAccountId() {
		return accountId;
	}
	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public long getShipToCountryId() {
		return shipToCountryId;
	}
	public void setShipToCountryId(long shipToCountryId) {
		this.shipToCountryId = shipToCountryId;
	}

	public long getShipToStateId() {
		return shipToStateId;
	}
	public void setShipToStateId(long shipToStateId) {
		this.shipToStateId = shipToStateId;
	}

	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public Long getPkid() {
		return pkid;
	}
	public void setPkid(Long pkid) {
		this.pkid = pkid;
	}
	
	public String getDN() {
		return DN;
	}
	public void setDN(String dN) {
		if (dN==null) this.DN = "";
		DN = dN;
	}
	
	public int getPrinted() {
		return printed;
	}
	public void setPrinted(int printed) {
		this.printed = printed;
	}

	public Date getOrderedDate() {
		return orderedDate;
	}
	public void setOrderedDate(Date orderedDate) {
		this.orderedDate = orderedDate;
	}
	
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	
	public String getHUB() {
		return HUB;
	}
	public void setHUB(String hUB) {
		HUB = hUB;
		if (hUB==null) {
			this.HUB = "";
		}
	}
	
	public String getSO() {
		return SO;
	}
	public void setSO(String sO) {
		SO = sO;
	}
	
	public String getPO() {
		return PO;
	}
	public void setPO(String pO) {
		PO = pO;
	}
	
	public List<B2BOrderLine> getLines() {
		return lines;
	}
	public void setLines(List<B2BOrderLine> lines) {
		this.lines = lines;
	}
	
	public String getLinearUnit() {
		return linearUnit;
	}
	public void setLinearUnit(String linearUnit) {
		this.linearUnit = linearUnit;
	}
	
	public String getWeightUnit() {
		return weightUnit;
	}
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}
	
	public long getShipFromId() {
		return shipFromId;
	}
	public void setShipFromId(long shipFromId) {
		this.shipFromId = shipFromId;
	}
	
	public long getShipToId() {
		return shipToId;
	}
	public void setShipToId(long shipToId) {
		this.shipToId = shipToId;
	}
	
	public String getShipToType() {
		return shipToType;
	}
	public void setShipToType(String shipToType) {
		if (shipToType!=null) {
			this.shipToType = shipToType.toUpperCase();
		} else {
			this.shipToType = null;
		}
	}
	
	public String getShipToPartyName() {
		return shipToPartyName;
	}
	public void setShipToPartyName(String shipToPartyName) {
		this.shipToPartyName = shipToPartyName;
	}
	
	public String getShipToAddress1() {
		return shipToAddress1;
	}
	public void setShipToAddress1(String shipToAddress1) {
		this.shipToAddress1 = shipToAddress1;
	}
	
	public String getShipToAddress2() {
		return ShipToAddress2;
	}
	public void setShipToAddress2(String shipToAddress2) {
		ShipToAddress2 = shipToAddress2;
	}
	
	public String getShipToCity() {
		return shipToCity;
	}
	public void setShipToCity(String shipToCity) {
		this.shipToCity = shipToCity;
	}
	
	public String getShipToState() {
		return shipToState;
	}
	public void setShipToState(String shipToState) {
		this.shipToState = shipToState;
	}
	
	public String getShipToZipCode() {
		return shipToZipCode;
	}
	public void setShipToZipCode(String shipToZipCode) {
		this.shipToZipCode = shipToZipCode;
	}
	
	public String getShipToCountry() {
		return shipToCountry;
	}
	public void setShipToCountry(String shipToCountry) {
		this.shipToCountry = shipToCountry;
	}
	
	public Date getShipNotBefore() {
		return shipNotBefore;
	}
	public void setShipNotBefore(Date shipNotBefore) {
		this.shipNotBefore = shipNotBefore;
	}
	
	public Date getShipNoLater() {
		return shipNoLater;
	}
	public void setShipNoLater(Date shipNoLater) {
		this.shipNoLater = shipNoLater;
	}
	
	public Date getReqShipDate() {
		return reqShipDate;
	}
	public void setReqShipDate(Date reqShipDate) {
		this.reqShipDate = reqShipDate;
	}
	
	public Date getMABD() {
		return MABD;
	}
	public void setMABD(Date mABD) {
		MABD = mABD;
	}
	
	public String getSCAC() {
		return SCAC;
	}
	public void setSCAC(String sCAC) {
		SCAC = sCAC;
	}
	
	public String getFreightTerms() {
		return freightTerms;
	}
	public void setFreightTerms(String freightTerms) {
		this.freightTerms = freightTerms;
	}
	
	public String getBillTo() {
		return billTo;
	}
	public void setBillTo(String billTo) {
		this.billTo = billTo;
	}
	
	public String getSoldToName1() {
		return soldToName1;
	}
	public void setSoldToName1(String soldToName1) {
		this.soldToName1 = soldToName1;
	}
	
	public String getSoldToName2() {
		return soldToName2;
	}
	public void setSoldToName2(String soldToName2) {
		this.soldToName2 = soldToName2;
	}
	
	public String getLoadNO() {
		return loadNO;
	}
	public void setLoadNO(String loadNO) {
		this.loadNO = loadNO;
	}
	
	public String getFreightType() {
		return freightType;
	}
	public void setFreightType(String freightType) {
		this.freightType = freightType;
	}
	
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	public String getTrackingNO() {
		return trackingNO;
	}
	public void setTrackingNO(String trackingNO) {
		this.trackingNO = trackingNO;
	}
	
	public String getShippingInstruction() {
		return shippingInstruction;
	}
	public void setShippingInstruction(String shippingInstruction) {
		this.shippingInstruction = shippingInstruction;
	}
	
	public String getPickUpDateAppt() {
		return pickUpDateAppt;
	}
	public void setPickUpDateAppt(String pickUpDateAppt) {
		this.pickUpDateAppt = pickUpDateAppt;
	}
	
	public String getPickUpTimeAppt() {
		return pickUpTimeAppt;
	}
	
	public void setPickUpTimeAppt(String pickUpTimeAppt) {
		this.pickUpTimeAppt = pickUpTimeAppt;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public int getBillToId() {
		return billToId;
	}
	public void setBillToId(int billToId) {
		this.billToId = billToId;
	}

	public int getSourceType() {
		return sourceType;
	}
	public void setSourceType(int sourceType) {
		this.sourceType = sourceType;
	}

	public void addLine(WmsOrderLine line) {
		B2BOrderLine nline = new B2BOrderLine();
		nline.setTitle(line.getTitle());
		nline.setModel(line.getModel());
		nline.setLotNO(line.getLotNO());
		nline.setOrderQty(line.getOrderQty());
		nline.setTotalWeight(line.getTotalWeight());
		nline.setMasterCartonQty(line.getMasterCartonQty());
		nline.setTotalPallets(line.getTotalPallets());
		nline.setLinearUnit(line.getLinearUnit());
		nline.setWeightUnit(line.getWeightUnit());
		nline.setItemShipInstruction(line.getItemShipInstruction());
		nline.setPO(line.getPO());
		nline.setSO(line.getSO());
		this.lines.add(nline);
	}
	
	public void addLine(DBRow line, long psid) {
		B2BOrderLine nline = new B2BOrderLine();
		nline.setTitle(line.get("Title", ""));
		nline.setModel(line.get("Model", ""));
		nline.setLotNO(line.get("LotNO", ""));
		nline.setOrderQty(line.get("OrderQty", 0));
		nline.setTotalWeight(line.get("TotalWeight", 0f));
		nline.setMasterCartonQty(line.get("MasterCartonQty",0));
		nline.setTotalPallets(line.get("TotalPallets", 0f));
		nline.setLinearUnit(line.get("LinearUnit", ""));
		nline.setWeightUnit(line.get("WeightUnit", ""));
		nline.setItemShipInstruction(line.get("ItemShipInstruction", ""));
		nline.setPO(line.get("PO", ""));
		nline.setSO(line.get("SO", ""));
		this.lines.add(nline);
	}
}