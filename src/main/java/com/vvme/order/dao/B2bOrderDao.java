package com.vvme.order.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

public class B2bOrderDao {
	private DBUtilAutoTran_Optm dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	/**
	 * 添加订单
	 * 
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long add(DBRow dbrow) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("b2b_order"), dbrow);
		} catch (Exception e) {
			throw new Exception("add(DBRow dbrow) error:" + e);
		}
	}
	
	/**
	 * 修改订单
	 * 
	 * @param b2b_order_id
	 * @param para
	 * @throws Exception
	 */
	public void updateById(long b2b_oid, DBRow para) throws Exception {
		try {
			dbUtilAutoTran.update("where b2b_oid = " + b2b_oid, ConfigBean.getStringValue("b2b_order"), para);
		} catch (Exception e) {
			throw new Exception("updateById(long b2b_oid, DBRow para) error:" + e);
		}
	}
	
	/**
	 * 根据订单ID获得订单
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public DBRow get(long b2b_oid) throws Exception {
		try {
			String sql = "select a.*,b.carrier,"
					+ "(select sum(i.total_weight) from b2b_order_item  i where i.b2b_oid= a.b2b_oid) total_weight,"
					+ "(select sum(i.boxes) from b2b_order_item i where i.b2b_oid= a.b2b_oid) total_boxes,"
					+ "(select sum(i.pallet_spaces) from b2b_order_item  i where i.b2b_oid= a.b2b_oid) total_pallets"
					+ " from " + ConfigBean.getStringValue("b2b_order") + " a left join carrier_scac_mcdot b on a.carriers=b.scac where b2b_oid = ?";
			
			DBRow para = new DBRow();
			para.add("b2b_oid", b2b_oid);
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} catch (Exception e) {
			throw new Exception("get(long b2b_oid) error:" + e);
		}
	}
	
	/**
	 * 插入前的检查
	 * 
	 * @param dn
	 * @param customerid
	 * @param importer
	 * @return
	 * @throws Exception
	 */
	public int checkOrder(String dn, String customerid, long importer) throws Exception {
		try {
			String sql = "select count(1) as cnt from b2b_order where customer_dn='" + dn + "' and customer_id='"
					+ customerid + "'";
			DBRow row = this.dbUtilAutoTran.selectSingle(sql);
			
			int cnt = Integer.parseInt(row.getString("cnt"));
			return cnt;
		} catch (Exception e) {
			throw new Exception("checkOrder(String dn,String customerid,long importer) error:" + e);
		}
	}
	
	/**
	 * 查找整条明细
	 * 
	 * @param customerId
	 * @param dn
	 * @param itemId
	 * @param lot
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findB2bLines(String customerId, String dn, String itemId, String lot) throws Exception {
		try {
			String sql = "select a.send_psid,a.company_id,a.customer_dn,b.retail_po as retail_po,b.so as order_number,a.customer_order_date,a.freight_term,"
					+ "a.ship_to_party_name,a.deliver_house_number,a.deliver_city,a.address_state_deliver,a.deliver_zip_code,a.deliver_ccid,"
					+ "a.mabd,a.etd,a.load_no,a.pickup_appointment,a.delivery_appointment,a.deliveryed_date,a.bol_tracking,a.carriers,a.ship_not_before,a.ship_not_later,a.requested_date,a.bill_to_name,a.bill_to_id,a.freight_carrier,"
					+ "b.lot_number,b.b2b_count,b.item_id,b.total_weight,b.pallet_spaces,b.boxes from b2b_order a,b2b_order_item b where a.b2b_oid=b.b2b_oid "
					+ "and a.customer_id='" + customerId + "' and a.customer_dn='" + dn + "' and b.item_id='" + itemId+"' and lot_number='"+lot+"'";
	
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("findB2bLines(String customerId,String dn,String itemId,String lot) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param params
	 * @param fields
	 * @return
	 * @throws Exception
	 * @author: ye
	 */
	public DBRow[] find(DBRow para, String[] fields) throws Exception {
		if (null == fields || fields.length <= 0)
			return find(para);
		
		try {
			StringBuilder sb = new StringBuilder("select ").append(StringUtils.join(fields, ",")).append(" from ")
					.append(ConfigBean.getStringValue("b2b_order"));
			
			@SuppressWarnings("unchecked")
			ArrayList<String> list = para.getFieldNames();
			
			if (list.size() > 0) {
				sb.append(" where ");
				for (String fn : list) {
					sb.append(fn).append("=? and ");
				}
			}
			
			String sql = sb.toString().substring(0, sb.toString().length() - 5);
			
			return this.dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("find(DBRow para, String[] fields) error:" + e);
		}
	}
	
	/**
	 * 查找b2b_order
	 * 
	 * @param para
	 * @return
	 * @throws Exception
	 * @author: ye
	 */
	public DBRow[] find(DBRow para) throws Exception {
		try {
			StringBuilder sb = new StringBuilder("select * from " + ConfigBean.getStringValue("b2b_order"));
			
			@SuppressWarnings("unchecked")
			ArrayList<String> list = para.getFieldNames();
			
			if (list.size() > 0) {
				sb.append(" where ");
				for (String fn : list) {
					sb.append(fn).append("=? and ");
				}
			}
			
			String sql = sb.toString().substring(0, sb.toString().length() - 5);
			
			return this.dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("find(DBRow para) error:" + e);
		}
	}
	
	/**
	 * 查找出order表多余的dn数据
	 * 
	 * @param cid
	 * @param carriers
	 * @param dns
	 * @return
	 * @throws Exception
	 */
	
	public DBRow[] findLeftOrder(String cid, long importer) throws Exception {
		try {
			String sql = "select a.customer_dn from ";
			sql = sql + " b2b_order a LEFT JOIN  (select * from wms_order_lines_tmp where importer=? and customer_id='"
					+ cid + "') b ";
			sql = sql + " on a.customer_dn=b.dn and a.customer_id=b.customer_id ";
			sql += " where a.customer_id=? and ( a.b2b_order_status>=2 and a.b2b_order_status<10 ) and b.dn is null ";
			DBRow row = new DBRow();
			row.add("importer", importer);
			row.add("customer_id", cid);
			return this.dbUtilAutoTran.selectPreMutliple(sql, row);
		} catch (Exception e) {
			throw new Exception("findLeftOrder(String cid,String carriers) error:" + e);
		}
	}
	
	/**
	 * 根据dn查找出order及item明细
	 * 
	 * @param dn
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderAndDetail(String dns, String cid) throws Exception {
		try {
			StringBuilder sql = new StringBuilder()
					.append("select a.customer_order_date as a0,(select brand_name from customer_brand where cb_id='"+cid+"') as a1,a.company_id as a2,a.customer_dn as a3,"
							+ "a.order_number as a4,a.retail_po as a5,b.title as a6,")
					.append("b.item_id as a7,b.material_number as a8,b.b2b_delivery_count as a9,b.total_weight as a10,b.boxes as a11,b.pallet_spaces as a12,a.order_type as a13"
							+ ",a.ship_to_party_name as a14,a.b2b_order_address as a15,a.deliver_city as a16,")
					.append("a.address_state_deliver as a17,a.deliver_zip_code as a18,c.c_country as a19,a.ship_not_before as a20,a.ship_not_later as a21,a.requested_date as a22"
							+ ",a.mabd as a23,a.carriers as a24,a.freight_term as a25,a.bill_to_id as a26,")
					.append("a.load_no as a27,a.freight_carrier as a28,b.note as a29,a.delivery_appointment as a30,a.pickup_appointment as a31,")
					.append("a.deliveryed_date as a32,a.b2b_order_status as a33,a.bol_tracking as a34,a.send_psid ")
					.append(" from b2b_order a,b2b_order_item b,country_code c where c.ccid=a.deliver_ccid and a.b2b_oid=b.b2b_oid and a.customer_dn in (")
					.append(dns).append(") and b2b_order_status<>6 and customer_id='").append(cid).append("' order by a.load_no");
			DBRow[] rows = this.dbUtilAutoTran.selectMutliple(sql.toString());
			
			final Map<String,String> ORDER_STATUS=new HashMap<String,String>();
			
			ORDER_STATUS.put("1", "Imported");
			ORDER_STATUS.put("2", "Committed");
			ORDER_STATUS.put("3", "Picking");
			ORDER_STATUS.put("4", "Packing");
			ORDER_STATUS.put("5", "Staging");
			ORDER_STATUS.put("6", "Staged");
			ORDER_STATUS.put("7", "Loading");
			ORDER_STATUS.put("8", "Closed");
			ORDER_STATUS.put("9", "Cancel");
			ORDER_STATUS.put("10", "Open");
			
			DBRow para=new DBRow();
			for (DBRow row : rows) {
				int ps_id = (Integer) row.getValue("send_psid");
				
				Date a0 = (Date) row.getValue("a0");
				if (a0 != null)
					row.put("a0", DateUtil.showLocationTime(a0, ps_id,"MM/dd/yyyy HH:mm:ss").substring(0, 10));
				
				Date a20 = (Date) row.getValue("a20");
				if (a20 != null)
					row.put("a20", DateUtil.showLocationTime(a20, ps_id,"MM/dd/yyyy HH:mm:ss").substring(0, 10));
				
				Date a21 = (Date) row.getValue("a21");
				if (a21 != null)
					row.put("a21", DateUtil.showLocationTime(a21, ps_id,"MM/dd/yyyy HH:mm:ss").substring(0, 10));
				
				Date a22 = (Date) row.getValue("a22");
				if (a22 != null)
					row.put("a22", DateUtil.showLocationTime(a22, ps_id,"MM/dd/yyyy HH:mm:ss").substring(0, 10));
				
				Date a23 = (Date) row.getValue("a23");
				if (a23 != null)
					row.put("a23", DateUtil.showLocationTime(a23, ps_id,"MM/dd/yyyy HH:mm:ss").substring(0, 10));
				
				Date a31 = (Date) row.getValue("a31");
				if (a31 != null){
					row.put("a30", DateUtil.showLocationTime(a31, ps_id,"MM/dd/yyyy HH:mm:ss").substring(0, 10));
					row.put("a31", DateUtil.showLocationTime(a31, ps_id,"MM/dd/yyyy HH:mm:ss").substring(11, 16));
				}else{
					String a30 = row.getString("a30");
					row.put("a30", a30);
				}
				
				Date a32 = (Date) row.getValue("a32");
				if (a32 != null)
					row.put("a32", DateUtil.showLocationTime((Date) a32, ps_id,"MM/dd/yyyy HH:mm:ss").substring(0, 10));
				
				String order_status=row.getString("a33");
				row.put("a33", ORDER_STATUS.get(order_status));
			}
			
			return rows;
		} catch (Exception e) {
			throw new Exception("findOrderAndDetail(String dns, String cid) error:" + e);
		}
	}
	
	/**
	 * 导入load 时 更新carrier和freight type
	 * @param cid
	 * @param importer
	 * @throws Exception
	 */
	public void updateCarrierAndFreightType(String cid,long importer) throws Exception{
		try{
			String sql="update b2b_order a inner join (select distinct dn,customer_id,b.carrier,upper(freight_type) as freight_type,c.carrier as carrname from wms_order_lines_tmp b left join carrier_scac_mcdot c on b.carrier=c.scac where correct in (15,24,19,41,42) and  customer_id='"+cid+"' and importer="+importer+") b on a.customer_dn=b.dn and a.customer_id=b.customer_id"+ 
					" set carriers=b.carrier,freight_carrier=b.freight_type,b2b_order_waybill_name=b.carrname ";
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("updateCarrierAndFreightType(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param loadno
	 * @param cid
	 * @param app
	 * @throws Exception
	 */
	public void updateAppointment(String loadno,String cid,String app) throws Exception{
		try{
			String whereSql = "where load_no='" + loadno +"' and customer_id='"+cid+"'";
			DBRow para=new DBRow();
			para.add("pickup_appointment", app);
			this.dbUtilAutoTran.update(whereSql, ConfigBean.getStringValue("b2b_order"), para);
		}catch(Exception e){
			throw new Exception("updateAppointment(String loadno,String cid,String app) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findExpOrder(DBRow param) throws Exception{
		try {
			StringBuilder sql = new StringBuilder()
					.append("select a.customer_order_date as a0,(select brand_name from customer_brand where cb_id=a.customer_id) as a1,a.company_id as a2,a.customer_dn as a3,"
							+ "b.so as a4,b.retail_po as a5,b.title as a6,")
					.append("b.item_id as a7,b.material_number as a8,b.b2b_delivery_count as a9,b.total_weight as a10,b.boxes as a11,b.pallet_spaces as a12,a.order_type as a13"
							+ ",a.ship_to_party_name as a14,a.deliver_house_number as a15,a.deliver_city as a16,")
					.append("a.address_state_deliver as a17,a.deliver_zip_code as a18,c.c_country as a19,a.ship_not_before as a20,a.ship_not_later as a21,a.requested_date as a22"
							+ ",a.mabd as a23,a.carriers as a24,a.freight_term as a25,a.bill_to_name as a26,")
					.append("a.load_no as a27,a.freight_carrier as a28,b.note as a29,a.delivery_appointment as a30,a.pickup_appointment as a31,")
					.append("a.deliveryed_date as a32,a.b2b_order_status as a33,a.bol_tracking as a34,a.send_psid ")
					.append(" from b2b_order a,b2b_order_item b,country_code c where c.ccid=a.deliver_ccid and a.b2b_oid=b.b2b_oid ");
			
			long psId=Long.parseLong(param.get("send_psid", ""));
			
			if (!param.get("send_psid", "").equals("")) {
				sql.append(" and a.send_psid=" + param.get("send_psid", ""));
			}
			if (!param.get("receive_psid", "").equals("")) {
				sql.append(" and a.receive_psid=" + param.get("receive_psid", ""));
			}
			if (!param.get("b2b_order_status", "").equals("")) {
				sql.append(" and a.b2b_order_status=" + param.get("b2b_order_status", ""));
			}
			if (!param.get("source_type", "").equals("")) {
				sql.append(" and a.source_type=" + param.get("source_type", ""));
			}
			if (!param.get("freight_term", "").equals("")) {
				sql.append(" and a.freight_term='" + param.get("freight_term", "") + "'");
			}
			if (!param.get("customer_id", "").equals("")) {
				sql.append(" and a.customer_id='" + param.get("customer_id", "") + "'");
			}
			if (!param.get("load_no", "").equals("")) {
				sql.append(" and a.load_no='" + param.get("load_no", "") + "'");
			}
			if (param.get("load_no_flag", "").equals("1")) {
				sql.append(" and a.load_no is not null");
			}
			if (param.get("load_no_flag", "").equals("0")) {
				sql.append(" and a.load_no is  null");
			}
			if (!param.get("customer_dn", "").equals("")) {
				sql.append(" and a.customer_dn='" + param.get("customer_dn", "") + "'");
			}
			if (!param.get("retail_po", "").equals("")) {
				sql.append(" and a.retail_po='" + param.get("retail_po", "") + "'");
			}
			if (!param.get("create_account_id", "").equals("") && !param.get("create_account_id", "").equals("0")) {
				sql.append(" and a.create_account_id=" + param.get("create_account_id", ""));
			}
			if (!param.get("retailer", "").equals("")) {
				sql.append(" and a.receive_psid in (select id from product_storage_catalog where storage_type=5 and storage_type_id = "+param.get("retailer", 0)+")");
			}
			
			String timeType=param.get("timeType", "");
			if (!timeType.equals("")) {
				Date t = new Date();
				String start=param.getString("startTime");
				if(!StringUtils.isEmpty(start)){
					t=DateUtils.parseDate(start+" 00:00:00", new String[]{"yyyy-MM-dd HH:mm:ss"});
					String utcStart=DateUtil.showUTCTime(t, psId);
					t=DateUtils.parseDate(utcStart, new String[]{"yyyy-MM-dd HH:mm:ss"});
				}else{
					t.setTime(0l);
				}
				String end=param.getString("endTime");
				Date e=new Date();
				if(!StringUtils.isEmpty(end)){
					e=DateUtils.parseDate(end+" 00:00:00", new String[]{"yyyy-MM-dd HH:mm:ss"});
					String utcEnd=DateUtil.showUTCTime(e, psId);
					e=DateUtils.parseDate(utcEnd, new String[]{"yyyy-MM-dd HH:mm:ss"});
				}else{
					e.setTime(0l);
				}
				switch(timeType){
					case "mabd":
						if(t.getTime()>0)
							sql.append(" and a.mabd>='" + DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss", t) + "'");
						if(e.getTime()>0)
							sql.append(" and a.mabd<='" + DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss", e) + "'");
						break;
					case "orderDate":
						if(t.getTime()>0)
							sql.append(" and a.customer_order_date>='" + DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss", t) + "'");
						if(e.getTime()>0)
							sql.append(" and a.customer_order_date<='" + DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss", e) + "'");
						break;
					case "reqShipDate":
						if(t.getTime()>0)
							sql.append(" and a.requested_date>='" + DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss", t) + "'");
						if(e.getTime()>0)
							sql.append(" and a.requested_date<='" + DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss", e) + "'");
						break;
				}
			}
			
			sql.append(" order by a.load_no,a.customer_order_date,a.mabd,a.pickup_appointment ");
			 
			return this.dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception("findExpOrder(DBRow param) error:" + e);
		}
	}
}
