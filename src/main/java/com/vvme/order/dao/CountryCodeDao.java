package com.vvme.order.dao;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

/**
 * 
 * @ClassName: CountryCodeDao
 * @Description:
 * @author yetl
 * @date 2015年4月27日
 *
 */
public class CountryCodeDao {
	private DBUtilAutoTran_Optm dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	/**
	 * 根据国家名称查询国家ID
	 * 
	 * @param country
	 * @return
	 * @throws Exception
	 */
	public DBRow findCcid(String country) throws Exception {
		try {
			String sql = "select ccid from "
					+ ConfigBean.getStringValue("country_code")
					+ " where c_country='" + country + "'";
			return this.dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("findCcid(String country) error:" + e);
		}
	}

	/**
	 * 根据国家ID查找国家名称
	 * 
	 * @param ccid
	 * @return
	 * @throws Exception
	 */
	public DBRow findCountry(int ccid) throws Exception {
		try {
			String sql = "select c_country from "
					+ ConfigBean.getStringValue("country_code")
					+ " where ccid=" + ccid;
			return this.dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("findCcid(String country) error:" + e);
		}
	}

	public DBRow getByCcid(int ccid) throws Exception {
		try {
			String sql = "select * from "
					+ ConfigBean.getStringValue("country_code")
					+ " where ccid = ? ";

			DBRow para = new DBRow();
			para.add("ccid", ccid);

			return dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (Exception e) {
			throw new Exception("getByCcid(int ccid) error:" + e);
		}
	}
}
