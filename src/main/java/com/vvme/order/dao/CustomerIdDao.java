package com.vvme.order.dao;

/**
 * 
 * @ClassName: CustomerIdDao
 * @Description: 
 * @author yetl
 * @date 2015年7月10日
 *
 */
public class CustomerIdDao extends AbstractBaseDao{
	
	protected String getTableName() {
		return "customer_id";
	}
	
}
