package com.vvme.order.dao;

import java.sql.SQLException;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

public class CompanyDicDao extends AbstractBaseDao{
	
	/**
	 * 查映射
	 * @param delivery_plant
	 * @return
	 * @throws Exception
	 */
	public DBRow getCompanyId(String delivery_plant,String customerId) throws Exception{
		try {
			String sql="select * from wms_company_dic where delivery_plant=? and customer_id=?";
			DBRow para=new DBRow();
			para.put("delivery_plant", delivery_plant);
			para.put("customer_id", customerId);
			return this.dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (SQLException e) {
			throw new Exception("getCompanyId(String delivery_plant,String customerId) error:" + e);
		}
	}

	protected String getTableName() {
		return "wms_company_dic";
	}
}
