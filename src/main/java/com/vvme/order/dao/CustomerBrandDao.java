package com.vvme.order.dao;

import com.cwc.db.DBRow;

/**
 * 
 * @ClassName: CustomerBrandDao
 * @Description: 
 * @author yetl
 * @date 2015年4月28日
 *
 */
public class CustomerBrandDao extends AbstractBaseDao{
	
	/**
	 * 获取customer
	 * @param customer
	 * @return
	 */
	public DBRow getByCustomer(String customer) throws Exception{
		try{
			String sql="select * from customer_brand where brand_name='"+customer+"'";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("getByCustomer(String customer) error:" + e);
		}
	}

	protected String getTableName() {
		return "customer_brand";
	}
}
