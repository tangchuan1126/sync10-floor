package com.vvme.order.dao;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.init.StorageJetLagInit;

public class ProductStorageCatalogDao extends AbstractBaseDao{
	private StorageJetLagInit storageJetLagInit;

	public void setStorageJetLagInit(StorageJetLagInit storageJetLagInit) {
		this.storageJetLagInit = storageJetLagInit;
	}

	public long add(DBRow row) throws Exception {
		try {
			long ps_id = dbUtilAutoTran.insertReturnId(
					ConfigBean.getStringValue("product_storage_catalog"), row);
			storageJetLagInit.init(dbUtilAutoTran);
			return ps_id;
		} catch (Exception e) {
			throw new Exception("add(DBRow row) error:" + e);
		}
	}

	/**
	 * 
	 * @param shipToPartyName
	 * @param shipToCity
	 * @param shipToZipCode
	 * @param retailerid
	 * @param shipToAddress
	 * @return
	 * @throws Exception
	 */
	public DBRow[] find(String shipToPartyName, String shipToCity,
			String shipToZipCode, String retailerid, String shipToAddress)
			throws Exception {
		try {
			String sql = "select * from product_storage_catalog where REPLACE(title,' ','')=REPLACE('"
					+ shipToPartyName
					+ "',' ','') "
					+ " and replace(deliver_city,' ','') like concat('%','"
					+ shipToCity
					+ "','%') and deliver_zip_code='"
					+ shipToZipCode
					+ "' and deliver_street like REPLACE('"
					+ shipToAddress
					+ "',' ','')" + " and storage_type=5 and storage_type_id=" + retailerid;
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					"find(String shipToPartyName,String shipToCity,String shipToZipCode,String retailerid,String shipToAddress) error:"
							+ e);
		}
	}

	/**
	 * 
	 * @param shipToPartyName
	 * @param shipToCity
	 * @return
	 * @throws Exception
	 */
	public DBRow[] find(String shipToPartyName, String shipToCity)
			throws Exception {
		try {
			String sql = "select * from product_storage_catalog where REPLACE(title,' ','')=REPLACE('"
					+ shipToPartyName
					+ "',' ','') "
					+ " and replace(deliver_city,' ','') like concat('%','"
					+ shipToCity
					+ "','%') and storage_type=5 ";
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					"find(String shipToPartyName,String shipToCity) error:" + e);
		}
	}

	/**
	 * 
	 * @param shipToPartyName
	 * @param shipToCity
	 * @param shipToZipCode
	 * @return
	 * @throws Exception
	 */
	public DBRow[] find(String shipToPartyName, String shipToCity,
			String shipToZipCode) throws Exception {
		try {
			String sql = "select * from product_storage_catalog where REPLACE(title,' ','')=REPLACE('"
					+ shipToPartyName
					+ "',' ','') "
					+ " and replace(deliver_city,' ','') like concat('%','"
					+ shipToCity
					+ "','%') and deliver_zip_code='" + shipToZipCode + "' and storage_type=5 ";
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					"find(String shipToPartyName,String shipToCity,String shipToZipCode) error:"
							+ e);
		}
	}

	/**
	 * 
	 * @param shipToPartyName
	 * @param shipToCity
	 * @param shipToZipCode
	 * @param retailerid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] find(String shipToPartyName, String shipToCity,
			String shipToZipCode, String retailerid) throws Exception {
		try {
			String sql = "select * from product_storage_catalog where REPLACE(title,' ','')=REPLACE('"
					+ shipToPartyName
					+ "',' ','') "
					+ " and replace(deliver_city,' ','') like concat('%','"
					+ shipToCity
					+ "','%') and deliver_zip_code='"
					+ shipToZipCode
					+ "' and storage_type=5 and storage_type_id=" + retailerid;
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					"find(String shipToPartyName,String shipToCity,String shipToZipCode,String retailerid) error:"
							+ e);
		}
	}

	/**
	 * 
	 * @param title
	 * @return
	 * @throws Exception
	 */
	public DBRow getByTitle(String title,int shipToId) throws Exception {
		try {
			String sql = "select * from "
					+ ConfigBean.getStringValue("product_storage_catalog")
					+ " where REPLACE(title,' ','')=? and storage_type=5 and storage_type_id=?";
			DBRow para = new DBRow();
			para.put("title", title.replaceAll("\\s*", ""));
			para.put("storage_type_id", shipToId);
			return this.dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (Exception e) {
			throw new Exception("getByTitle(String title) error:" + e);
		}
	}
	
	/**
	 * 获取psid
	 * 
	 * @param shipToName
	 * @return
	 * @throws Exception
	 */
	public DBRow find(String shipToName, String shipToAddress,
			String shipToCity, long pro_id,String storageTypeId) throws Exception {
		try {
			String sql = "select * from "
					+ ConfigBean.getStringValue("product_storage_catalog")
					+ " where REPLACE(title,' ','')=? and replace(deliver_house_number,' ','')=? and replace(deliver_city,' ','')=? and deliver_pro_id=? "
					+ " and storage_type=5 and storage_type_id=?";
			DBRow row = new DBRow();

			row.add("title", shipToName.replaceAll("\\s*", ""));
			row.add("deliver_house_number", shipToAddress.replaceAll("\\s*", ""));
			row.add("deliver_city", shipToCity.replaceAll("\\s*", ""));
			row.add("deliver_pro_id", pro_id);
			row.add("storage_type_id", storageTypeId);

			return this.dbUtilAutoTran.selectPreSingle(sql, row);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(
					"find(String shipToName, String shipToAddress,String shipToCity, long pro_id) error:"
							+ e);
		}
	}

	public DBRow getById(long id) throws Exception {
		try {

			DBRow para = new DBRow();
			para.add("id", id);
			return (dbUtilAutoTran.selectPreSingle("select * from "
					+ ConfigBean.getStringValue("product_storage_catalog")
					+ " psc," + ConfigBean.getStringValue("country_code")
					+ " cc where id=? and psc.native=cc.ccid", para));
		} catch (Exception e) {
			throw new Exception(
					"getById(long id) error:"
							+ e);
		}
	}
	
	/**
	 * 获得仓库信息
	 * 
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow get(long id) throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where id = ?";
			
			DBRow para = new DBRow();
			para.add("id", id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} catch (Exception e) {
			throw new Exception("get(long id):" + e);
		}
	}

	protected String getTableName() {
		return "product_storage_catalog";
	}
	
}
