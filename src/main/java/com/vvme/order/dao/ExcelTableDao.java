package com.vvme.order.dao;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

/**
 * 
 * @ClassName: ExcelTableDao
 * @Description: 
 * @author yetl
 * @date 2015年4月27日
 *
 */
public class ExcelTableDao {
	private DBUtilAutoTran_Optm dbUtilAutoTran;
	private String table_schema;
	
	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public void setTable_schema(String table_schema) {
		this.table_schema = table_schema;
	}

	/**
	 * 获取order_lines_tmp表字段与excel对应关系相关信息
	 * @return
	 * @throws Exception
	 */
	public DBRow [] getFieldInfo() throws Exception{
		try{
			String sql="select a.is_nullable as isnil,a.column_name as cl ,a.data_type as dt ,a.column_type as ct,a.CHARACTER_MAXIMUM_LENGTH as cml,a.NUMERIC_PRECISION as np,"+
					"a.NUMERIC_SCALE as ns,b.excel_sn as sn,b.excel_title as title,b.format as format,b.order_column_name as ocn from "+
					"information_schema.`COLUMNS` a INNER JOIN "+
					"wms_xls_map b on a.column_name=b.column_name  where "+
					 "a.TABLE_NAME='wms_order_lines_tmp' and b.table_name='wms_order_lines_tmp' and b.enable=1 and b.excel_sn is not null and a.table_schema='"+table_schema+"'";
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			return rows;
		}catch(Exception e){
			throw new Exception("getFieldInfo() error:"+ e);
		}
	}
	
	/**
	 * 
	 * @param mid
	 * @return
	 * @throws Exception
	 */
	public DBRow [] getFieldInfo(int mid) throws Exception{
		try{
			String sql="select a.is_nullable as isnil,a.column_name as cl ,a.data_type as dt ,a.column_type as ct,a.CHARACTER_MAXIMUM_LENGTH as cml,a.NUMERIC_PRECISION as np,"+
					"a.NUMERIC_SCALE as ns,b.excel_sn as sn,b.excel_title as title,b.format as format,b.order_column_name as ocn from "+
					"information_schema.`COLUMNS` a INNER JOIN "+
					"wms_xls_map b on a.column_name=b.column_name  where "+
					 "a.TABLE_NAME='wms_order_lines_tmp' and b.table_name='wms_order_lines_tmp' and b.enable=1 and b.excel_sn is not null and a.table_schema='"+table_schema+"' and b.mid="+mid;
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			return rows;
		}catch(Exception e){
			throw new Exception("getFieldInfo() error:"+ e);
		}
	}
}
