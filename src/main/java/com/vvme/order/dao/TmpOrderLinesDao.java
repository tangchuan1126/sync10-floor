package com.vvme.order.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

/**
 * 
 * @ClassName: TmpOrderLinesDao
 * @Description: 订单导入临时表DAO
 * @author yetl
 * @date 2015年4月27日
 *
 */
public class TmpOrderLinesDao {
	private DBUtilAutoTran_Optm dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	/**
	 * 
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public boolean save(DBRow row) throws Exception{
	     return this.dbUtilAutoTran.insert(ConfigBean.getStringValue("wms_order_lines_tmp"), row);
	}
	
	/**
	 * 
	 * @param row
	 * @param wherecond
	 * @return
	 * @throws Exception
	 */
	public int updateByDn(DBRow row,String dn,String cid,long importer) throws Exception{
		return this.dbUtilAutoTran.update("where dn='"+dn+"' and customer_id='"+cid+"' and importer="+importer,  ConfigBean.getStringValue("wms_order_lines_tmp"), row);
	}
	
	/**
	 * 更新临时表记录校验结果
	 * @param dn
	 * @param customerId
	 * @param importer
	 * @param correct
	 * @throws Exception
	 */
	public void updateSign(String dn, String cid, long importer,int correct) throws Exception{
	   try
	    {
		   DBRow para=new DBRow();
		   para.put("correct", correct);
		   updateByDn(para,dn,cid,importer);
	    }
	    catch (Exception e)
	    {
	      throw new Exception("updateSign(String dn, String cid, long importer,int correct) error:" + e);
	    }
	}
	
	/**
	 * 
	 * @param para
	 * @return
	 * @throws Exception
	 */
	public DBRow[] find(DBRow para) throws Exception {
		try {
			StringBuilder sb = new StringBuilder("select * from " + ConfigBean.getStringValue("wms_order_lines_tmp"));
			
			@SuppressWarnings("unchecked")
			ArrayList<String> list = para.getFieldNames();
			
			if (list.size() > 0) {
				sb.append(" where ");
				for (String fn : list) {
					sb.append(fn).append("=? and ");
				}
			}
			
			String sql = sb.toString().substring(0, sb.toString().length() - 5);
			
			return this.dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("find(DBRow para) error:" + e);
		}
	}
	
	/**
	 * 查找b2b_order_item
	 * 
	 * @param para
	 * @return
	 * @throws Exception
	 * @author: ye
	 */
	public DBRow[] find(DBRow para, String[] fields) throws Exception {
		if (null == fields || fields.length <= 0)
			return find(para);
		
		try {
			StringBuilder sb = new StringBuilder("select ").append(StringUtils.join(fields, ",")).append(" from ")
					.append(ConfigBean.getStringValue("wms_order_lines_tmp"));
			
			@SuppressWarnings("unchecked")
			ArrayList<String> list = para.getFieldNames();
			
			if (list.size() > 0) {
				sb.append(" where ");
				for (String fn : list) {
					sb.append(fn).append("=? and ");
				}
			}
			
			String sql = sb.toString().substring(0, sb.toString().length() - 5);
			
			return this.dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("find(DBRow para, String[] fields) error:" + e);
		}
	}
	
	/**
	 * 导入dn时查询校验dn下某些列必须相同
	 * @param dn
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findByDn(String dn,String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="SELECT delivery_plant,dn,order_date,ship_to_party_name,"+
						"ship_to_address,ship_to_city,ship_to_state,"+
						"ship_to_zip_code,country,mabd,req_ship_date,loadno,pickup_appointment_date,pickup_appointment_time,"+
						"ship_date,bol_tracking,carrier,bill_to,ft,row FROM wms_order_lines_tmp where dn='"+dn+"' and customer_id='"+p_customer_id+"' and importer="+p_importer;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findByDn(String dn,String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 导入dn时查询校验dn下某些列必须相同
	 * @param dn
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findByDn0(String dn,String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="SELECT delivery_plant,dn,order_date,ship_to_party_name,"+
						"ship_to_address,ship_to_city,ship_to_state,order_number,"+
						"ship_to_zip_code,country,mabd,req_ship_date,loadno,pickup_appointment_date,pickup_appointment_time,"+
						"ship_date,bol_tracking,carrier,bill_to,ft,row FROM wms_order_lines_tmp where dn='"+dn+"' and customer_id='"+p_customer_id+"' and importer="+p_importer;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findByDn0(String dn,String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 根据b2boid更新
	 * @param b2boid
	 * @param importer
	 * @param correct
	 * @throws Exception
	 */
	public void updateCorrect(String b2boid,long importer,int correct) throws Exception{
	   try
	    {
		   String sql="update "+ConfigBean.getStringValue("wms_order_lines_tmp") +" a inner join b2b_order b"+
				   " on a.customer_id=b.customer_id and a.dn=b.customer_dn set a.correct="+correct+" where  a.correct=19 and b.b2b_oid="+b2boid+" and a.importer="+importer;
	      this.dbUtilAutoTran.executeSQL(sql);
	    }
	    catch (Exception e)
	    {
	      throw new Exception("updateCorrect(String b2boid,long importer,int correct) error:" + e);
	    }
	}
	
	public void updateCorrectByLoadno(String loadno,String p_customer_id, long p_importer,int correct) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct="+correct+" where customer_id='"+p_customer_id+"' "
											+ " and importer="+p_importer+" and loadno='"+loadno+"' and correct=19";
			
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("updateCorrectByLoadno(String loadno,String p_customer_id, long p_importer,int correct) error:" + e);
		}
	}
	
	/**
	 * 找出0的没判断的数据
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findCorrect0(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select DISTINCT a.customer_dn from wms_order_lines_tmp b INNER JOIN b2b_order a on a.customer_dn=b.dn "+ 
						"and a.customer_id=b.customer_id where b.customer_id='"+p_customer_id+"' and importer="+p_importer+" and b.correct=0 ";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findCorrect0(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 找出需要校验错误为2的dn
	 * @param cid
	 * @param importer
	 * @throws Exception
	 */
	public DBRow [] findSign2(String cid,long importer) throws Exception{
		try{
			String sql="select dn from "+ConfigBean.getStringValue("wms_order_lines_tmp")+" where  customer_id=? and importer=? and correct=0 group by dn having count(1)>1";
			DBRow para=new DBRow();
			para.put("customer_id", cid);
			para.put("importer", importer);
			
			return this.dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch(Exception e){
			throw new Exception("findSign2(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 把不存在的dn标记成4
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign4(String cid,long importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp a left join b2b_order b on a.dn=b.customer_dn and a.customer_id=b.customer_id set correct=4 where a.importer="+importer+" and a.customer_id='"+cid+"' and b.b2b_oid is null";
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findSign4(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 找出错误类型为5的错误并更新
	 * @param p_customer_id
	 * @param p_importer
	 * @param dn
	 * @throws Exception
	 */
	public void findSign5(String cid,long importer) throws Exception{
		try{
			DBRow [] rows=findCorrect0(cid,importer);
			
			for(DBRow r:rows){
				String dn=r.getString("customer_dn");
				
				String sql="select count(*) as cnt from (select DISTINCT item_id,ifnull(material_number,'') as material_number,title from b2b_order_item where b2b_oid="+
		 					"(select b2b_oid from b2b_order where customer_dn='"+dn+"' and customer_id='"+cid+"')) a left join "+
		 				"(select DISTINCT model_number,ifnull(material_number,'') as material_number,title from wms_order_lines_tmp where customer_id='"+cid+"' and dn='"+dn+"' and importer="+importer
		 				+" and correct=0) b on a.item_id=b.model_number and a.material_number=b.material_number and a.title=b.title where item_id is null;";
				
				DBRow row=this.dbUtilAutoTran.selectSingle(sql);
				int cnt=Integer.parseInt(row.getString("cnt"));
				
				if(cnt>0){//先左连接查询 
					DBRow para=new DBRow();
					para.put("correct", 5);
					this.updateByDn(para,dn,cid,importer);
				}else{
						//右连接查询
						sql="select count(*) as cnt from (select DISTINCT item_id,ifnull(material_number,'') as material_number,title from b2b_order_item where b2b_oid="+
			 					"(select b2b_oid from b2b_order where customer_dn='"+dn+"' and customer_id='"+cid+"')) a right join "+
			 				"(select DISTINCT model_number,ifnull(material_number,'') as material_number,title from wms_order_lines_tmp where customer_id='"+cid+"' and dn='"+dn+"' and importer="+importer
			 				+" and correct=0) b on a.item_id=b.model_number and a.material_number=b.material_number and a.title=b.title where item_id is null ;";
						
						row=this.dbUtilAutoTran.selectSingle(sql);
						cnt=Integer.parseInt(row.getString("cnt"));
						
						if(cnt>0){
							updateSign(dn,cid,importer,5);
						}else{
							sql="select count(*) as cnt from (select DISTINCT item_id,ifnull(material_number,'') as material_number,title from b2b_order_item where b2b_oid="+
				 					"(select b2b_oid from b2b_order where customer_dn='"+dn+"' and customer_id='"+cid+"')) a inner join "+
				 				"(select DISTINCT model_number,ifnull(material_number,'') as material_number,title from wms_order_lines_tmp where customer_id='"+cid+"' and dn='"+dn+"' and importer="+importer
				 				+" and correct=0) b where a.item_id=b.model_number and a.material_number=b.material_number and a.title=b.title;";
							
							row=this.dbUtilAutoTran.selectSingle(sql);
							cnt=Integer.parseInt(row.getString("cnt"));
							
							if(cnt==0){
								updateSign(dn,cid,importer,5);
							}
						}
//					}
					
				}
			}
		}catch(Exception e){
			throw new Exception("findSign5(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 仓库信息是否查找到
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public DBRow [] findSign8(String cid,long importer) throws Exception{
		try{
			String sql="select distinct dn,delivery_plant from wms_order_lines_tmp where customer_id='"+cid+"' and importer="+importer;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findSign8(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 找出错误类型为9的错误并更新
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign9(String cid,long importer) throws Exception{
		try{
			String sql="select DISTINCT a.customer_dn from wms_order_lines_tmp b INNER JOIN b2b_order a on a.customer_dn=b.dn "+
						"and a.customer_id=b.customer_id where b.customer_id='"+cid+"' and importer="+importer+" and b.correct=0 and a.b2b_order_status=8";
			
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			if(rows.length>0){
				String dns="'";
				for(DBRow r:rows){
					String dn=r.getString("customer_dn");
					
					dns+=dn+"','";
				}
				dns+="0'";
				
				sql="update "+ConfigBean.getStringValue("wms_order_lines_tmp")+" set correct=9 where dn in ("+dns+") and "+
						" customer_id='"+cid+"' and importer="+importer;
				this.dbUtilAutoTran.executeSQL(sql);
			}
			
		}catch(Exception e){
			throw new Exception("findSign9(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 查找出已经存在的dn
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign11(String cid,long importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp a left join b2b_order b on a.dn=b.customer_dn and a.customer_id=b.customer_id set a.correct=11 where a.importer="+importer+" and a.customer_id='"+cid+"' and b.b2b_oid is not null";
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findSign11(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 找出完全一样的dn
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect19(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select DISTINCT a.dn,b.b2b_oid,a.correct from wms_order_lines_tmp a left join b2b_order b on a.dn=b.customer_dn and a.customer_id=b.customer_id "+
						"and a.ship_to_city=b.deliver_city and a.ship_to_state=b.address_state_deliver and REPLACE(a.ship_to_address,' ','')=b.deliver_house_number and "+ 
						"REPLACE(a.ship_to_party_name,' ','_')=b.ship_to_party_name and a.retail_po=b.retail_po and a.order_number=b.order_number and ifnull(a.carrier,'')=b.carriers "+
						"and ifnull(a.bol_tracking,'')=b.bol_tracking "+
						"and ifnull(a.delivery_appointment,'')=b.delivery_appointment where a.importer="+p_importer+" and a.customer_id='"+p_customer_id+"' and a.correct in (0,14,11)";
			
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			for(DBRow r:rows){
				String b2b_oid=r.getString("b2b_oid");
				String dn=r.getString("dn");
				String correct=r.getString("correct");
				
				//0的需要判断是否有相同dn的其他Line有问题
				if(correct.equals("0")){
					sql="select count(1) as cnt from wms_order_lines_tmp where correct<>0 and dn='"+dn+"' and customer_id='"+p_customer_id+"' and importer="+p_importer;
					
					DBRow db=this.dbUtilAutoTran.selectSingle(sql);
					
					//去掉 一个dn对应多条明细部分sku能对应上,部分sku对应不上的问题 这种明细不处理只标记出来
					if(Integer.parseInt(db.getString("cnt"))>0){
						continue;
					}
				}
				
				if(StringUtils.isEmpty(b2b_oid)){
					updateSign(dn,p_customer_id,p_importer,21);
					continue;
				}
				
				sql="select count(1) as cnt from (select  item_id,sum(b2b_delivery_count) as a1 ,sum(total_weight) as a2,sum(pallet_spaces) as a3,sum(trailer_feet) as a4,sum(config_change) as a5,"+
						"sum(new_multiplier) as a6,sum(internal_cost) as a7,sum(boxes) as a8 from b2b_order_item where b2b_oid="+b2b_oid+" group by item_id,material_number,sloc) a "+
						"left join (select model_number,sum(ceil(order_qty)) as a1,sum(weight) as a2,sum(pallet_spaces) as a3,sum(trailer_feet) as a4,sum(config_change) as a5,"+
						"sum(new_multiplier) as a6,sum(internal_cost) as a7,sum(boxes) as a8 "+
						"from wms_order_lines_tmp where dn='"+dn+"' and customer_id='"+p_customer_id+"' and importer="+p_importer+
					    " group by material_number,model_number,sloc) b on a.a1=b.a1 and a.a2=b.a2 and a.a3=b.a3 and a.a4=b.a4 "+ 
					    "and a.a5=b.a5 and a.a6=b.a6 and a.a7=b.a7 and a.a8=b.a8 where b.model_number is null";
				
				DBRow db=this.dbUtilAutoTran.selectSingle(sql);
				
				if(Integer.parseInt(db.getString("cnt"))>0){
					updateSign(dn,p_customer_id,p_importer,21);
					continue;
				}else{
					updateSign(dn,p_customer_id,p_importer,19);
				}
			}
		}catch(Exception e){
			throw new Exception("findCorrect19(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 找出sku错误的数据
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign20(String cid,long importer) throws Exception{
		try{
//			String sql="update "+ConfigBean.getStringValue("wms_order_lines_tmp")+" LEFT JOIN product on p_name=CONCAT(model_number,'/',material_number) set correct=20 where "+
//			" customer_id="+cid+" and importer="+importer+" and pc_id is null and correct=0;";
			
			String sql="update "+ConfigBean.getStringValue("wms_order_lines_tmp")+" LEFT JOIN product on p_name=CONCAT(model_number,case  when material_number is null then '' else concat('/',material_number) end) set correct=20 where "+
					" customer_id="+cid+" and importer="+importer+" and pc_id is null and correct=0;";
			
//			String sql="update "+ConfigBean.getStringValue("wms_order_lines_tmp")+" LEFT JOIN product on p_name=model_number set correct=20 where "+
//					" customer_id="+cid+" and importer="+importer+" and pc_id is null and correct=0;";
			
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findSign20(String cid,long importer) error:" + e);
		}
	}
		
	/**
	 * 校验相同的loadno下的pickup appointment必须相同
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findSign26(String cid,long importer) throws Exception{
		try{
			String sql="select dn,row,pickup_appointment_date,pickup_appointment_time,delivery_plant,carrier,req_ship_date,loadno from "+ConfigBean.getStringValue("wms_order_lines_tmp")+" where loadno in ("
					+"select loadno from (select distinct loadno,pickup_appointment_date,pickup_appointment_time,delivery_plant,carrier,req_ship_date from wms_order_lines_tmp "+
					" where  customer_id='"+cid+"' and importer="+importer+" and correct=0) a group by loadno having count(1)>1) "
							+ "and customer_id='"+cid+"' and importer="+importer+" and correct=0";
			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findSign26(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 查找错误类型31的数据
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign31(String cid,long importer) throws Exception{
		try{
			String sql="select a.dn,a.loadno,b.b2b_oid from wms_order_lines_tmp a left join b2b_order b on a.dn=b.customer_dn "
					+ "and a.customer_id=b.customer_id where a.correct=19  and a.customer_id='"+cid+"' "
										+ " and a.importer="+importer;
					
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			String loadnoSql="select order_id from wms_load_order_master where load_no=?";
			
			Map<String,Set<String>> map=new HashMap<String,Set<String>>();
			Map<String,String> dnOrder=new HashMap<String,String>();
			for(DBRow row:rows){
				String dn=row.getString("dn");
				String loadno=row.getString("loadno");
				String b2boid=row.getString("b2b_oid");
				
				dnOrder.put(b2boid, dn);
				
				if(!map.containsKey(loadno)){
					Set<String> set=new HashSet<String>();
					set.add(b2boid);
					
					map.put(loadno, set);
				}else{
					map.get(loadno).add(b2boid);
				}
			}
			
			Iterator<Entry<String, Set<String>>> it=map.entrySet().iterator();
			DBRow para=new DBRow();
			
			while(it.hasNext()){
				Entry<String,Set<String>> entry=(Entry<String,Set<String>>)it.next();
				
				String loadno=entry.getKey();
				Set<String> oids=entry.getValue();
				
				para.put("load_no", loadno);
				
				DBRow [] datas=this.dbUtilAutoTran.selectPreMutliple(loadnoSql, para);
				
				if(datas.length==0 || null==datas)
					continue;
				
				Set<String> orderIds=new HashSet<String>();
				
				for(DBRow r:datas){
					String order_id=r.getString("order_id");
					orderIds.add(order_id);
				}
				
				if(oids.size()!=orderIds.size()){
					for(String oid:oids)
						updateCorrect(oid,importer,31);
				}else{
					oids.removeAll(orderIds);
					
					if(!oids.isEmpty()){
						for(String oid:oids)
							updateCorrect(oid,importer,31);
					}
				}
				
			}
			
		}catch(Exception e){
			throw new Exception("findSign31(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param cid
	 * @param importer
	 * @throws Exception
	 */
	public void findSign32(String cid,long importer) throws Exception{
		try{
			String sql="select distinct loadno from wms_order_lines_tmp where correct=19 and customer_id='"+cid+"' "
										+ " and importer="+importer;
			
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			String osql="select distinct correct from wms_order_lines_tmp where loadno=? and correct<>19 and customer_id='"+cid+"' "
										+ " and importer="+importer;
			
			DBRow para=new DBRow();
			for(DBRow r:rows){
				String loadno=r.getString("loadno");
				
				if(!StringUtils.isEmpty(loadno)){
					para.put("loadno", loadno);
					DBRow [] datas=this.dbUtilAutoTran.selectPreMutliple(osql, para);
					
					if(datas.length>0){
						updateCorrectByLoadno(loadno,cid,importer,32);
					}
				}
						
			}
		}catch(Exception e){
			throw new Exception("findSign32(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 查找REQ.SHIP DATE时间已过的
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public DBRow [] findSign35(String cid,long importer) throws Exception{
		try{
//			String sql="update wms_order_lines_tmp set correct=35 where concat(date_format(req_ship_date,'%Y-%m-%d '),'23:59:59')<NOW() and correct=0 and customer_id='"+cid+"' and importer="+importer;
//			this.dbUtilAutoTran.executeSQL(sql);
			String sql="select dn,req_ship_date,(select ps_id from wms_company_dic a where a.delivery_plant=wms_order_lines_tmp.delivery_plant) as ps_id  from wms_order_lines_tmp where correct=0 and customer_id='"+cid+"' and importer="+importer;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findSign35(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 查找MABD时间已过的
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public DBRow [] findSign36(String cid,long importer) throws Exception{
		try{
//			String sql="update wms_order_lines_tmp set correct=36 where mabd<NOW() and correct=0 and customer_id='"+cid+"' and importer="+importer;
//			this.dbUtilAutoTran.executeSQL(sql);
			String sql="select dn,mabd,(select ps_id from wms_company_dic a where a.delivery_plant=wms_order_lines_tmp.delivery_plant) as ps_id from wms_order_lines_tmp where  correct=0 and customer_id='"+cid+"' and importer="+importer;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findSign36(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 查找pickupappointment时间已过的
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign37(String cid,long importer) throws Exception{
		try{
//			String sql="update wms_order_lines_tmp set correct=37 where date_format(pickup_appointment_date,concat('%Y-%m-%d ',ifnull(date_format(pickup_appointment_time,'%H:%i:%s'),'23:59:59')))<=NOW() and correct=0 and customer_id='"+cid+"' and importer="+importer;
			
			String sql="select distinct dn,pickup_appointment_date,pickup_appointment_time from wms_order_lines_tmp where correct=0 and customer_id='"+cid+"' and importer="+importer+" and pickup_appointment_date is not null";
			String psSql="select send_psid from b2b_order where customer_dn=?";
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			for(DBRow row:rows){
				String dn=row.getString("dn");
				String pickup_appointment_date=row.getString("pickup_appointment_date");
				String pickup_appointment_time=row.getString("pickup_appointment_time");
				
				if(StrUtil.isBlank(pickup_appointment_date))
					continue;
				
				if(StrUtil.isBlank(pickup_appointment_time))
					pickup_appointment_time="23:59:59";
				
				pickup_appointment_date=pickup_appointment_date+" "+pickup_appointment_time;
				
				DBRow para=new DBRow();
				para.put("customer_dn", dn);
				DBRow psRow=this.dbUtilAutoTran.selectPreSingle(psSql, para);
				String psid=psRow.getString("send_psid");
				long ps_id=0;
				if(!StrUtil.isBlank(psid))
					ps_id=Long.parseLong(psid);
				pickup_appointment_date=DateUtil.showUTCTime(pickup_appointment_date,ps_id);
				Date pk=DateUtil.formateDate(pickup_appointment_date);
				if(pk.before(new Date()))
					updateSign(dn,cid,importer,37);
			}
		}catch(Exception e){
			throw new Exception("findSign37(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 找出错误类型为38的错误并更新
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign38(String cid,long importer) throws Exception{
		try{
			String sql="select DISTINCT a.customer_dn from wms_order_lines_tmp b INNER JOIN b2b_order a on a.customer_dn=b.dn "+
						"and a.customer_id=b.customer_id where b.customer_id='"+cid+"' and importer="+importer+" and b.correct=0 and (a.b2b_order_status=1 or a.b2b_order_status=10 )";
			
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			if(rows.length>0){
				String dns="'";
				for(DBRow r:rows){
					String dn=r.getString("customer_dn");
					
					dns+=dn+"','";
				}
				dns+="0'";
				
				sql="update "+ConfigBean.getStringValue("wms_order_lines_tmp")+" set correct=38 where dn in ("+dns+") and "+
						" customer_id='"+cid+"' and importer="+importer;
				this.dbUtilAutoTran.executeSQL(sql);
			}
			
		}catch(Exception e){
			throw new Exception("findSign38(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 更新load时,有loadno值carrier必填
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign40(String cid,long importer) throws Exception{
		try{
			String sql="update  wms_order_lines_tmp a left join carrier_scac_mcdot b on a.carrier=b.scac set correct=40 where  a.importer="+importer+" and a.customer_id='"+cid+"' and correct=0 and loadno is not null and b.carrier is null";
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findSign40(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 相同DN下的lines不能出现重复的SKU
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign43(String cid,long importer) throws Exception{
		try{

			String sql="select dn from wms_order_lines_tmp where "+
					" customer_id="+cid+" and importer="+importer+" and correct=0 group by dn,model_number,material_number,title having count(1)>1;";
			
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			for(DBRow row:rows){
				String dn=row.getString("dn");
				updateSign(dn,cid,importer,43);
			}
		}catch(Exception e){
			throw new Exception("findSign43(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 查找REQ.SHIP DATE时间大于mabd时间
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign44(String cid,long importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct=44 where req_ship_date>mabd and correct=0 and customer_id='"+cid+"' and importer="+importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findSign44(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 找出Carrier不存在
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign45(String cid,long importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp a left join carrier_scac_mcdot b on a.carrier=b.scac set correct=45 where "+
					" customer_id="+cid+" and importer="+importer+" and correct=0 and a.carrier is not null and b.id is null;";
			
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findSign45(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 标记title不存在的错误
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign46(String cid,long importer) throws Exception{
		try{
			String sql="update "+ConfigBean.getStringValue("wms_order_lines_tmp")+" LEFT JOIN title on title_name=title set correct=46 where "+
					" customer_id="+cid+" and importer="+importer+" and correct=0 and title_id is null;";
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findSign46(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 查找pickupappointment时间大于mabd
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign47(String cid,long importer) throws Exception{ 
		try{
			String sql="update wms_order_lines_tmp set correct=47 where pickup_appointment_date>=mabd and correct=0 and customer_id='"+cid+"' and importer="+importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findSign47(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 一个order是能FTL一个load
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign53(String cid,long importer) throws Exception{
		try{
			String sql="select loadno from wms_order_lines_tmp where correct=0 and freight_type='ftl' and loadno is not null and importer="+importer+" and customer_id='"+cid+"'";
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			String pre="select distinct dn,loadno from wms_order_lines_tmp where correct=0 and importer="+importer+" and customer_id='"+cid+"' and loadno=?";
			DBRow para=new DBRow();
			for(DBRow row:rows){
				String loadno=row.getString("loadno");
				para.put("loadno", loadno);
				DBRow [] dbs=this.dbUtilAutoTran.selectPreMutliple(pre, para);
				if(dbs.length>1){
					for(DBRow r:dbs){
						String dn=r.getString("dn");
						updateSign(dn,cid,importer,53);
					}
				}
			}
		}catch(Exception e){
			throw new Exception("findSign53(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * ship_not_before
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign54(String cid,long importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct=54 where ship_not_before is not null and concat(date_format(ship_not_before,'%Y-%m-%d '),'23:59:59')<NOW() and correct=0 and customer_id='"+cid+"' and importer="+importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findSign54(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * ship_not_before
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign55(String cid,long importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct=55 where ship_not_later is not null and concat(date_format(ship_not_later,'%Y-%m-%d '),'23:59:59')<NOW() and correct=0 and customer_id='"+cid+"' and importer="+importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findSign55(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * ship_not_before 大于 req_ship_date
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findSign56(String cid,long importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct=56 where ship_not_before is not null and concat(date_format(ship_not_before,'%Y-%m-%d '),'23:59:59')>concat(date_format(req_ship_date,'%Y-%m-%d '),'23:59:59') and correct=0 and customer_id='"+cid+"' and importer="+importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findSign56(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * ship_not_later 小于 req_ship_date
	 * @param cid
	 * @param importer
	 * @throws Exception
	 */
	public void findSign57(String cid,long importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct=57 where ship_not_later is not null and concat(date_format(ship_not_later,'%Y-%m-%d '),'23:59:59')<concat(date_format(req_ship_date,'%Y-%m-%d '),'23:59:59') and correct=0 and customer_id='"+cid+"' and importer="+importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findSign57(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findSignOther(String cid,long importer) throws Exception{
		try{	
			String sql=" select b.dn,b.pro_id_cc,b.product_id,b.isright,c.b2b_oid,b.ship_to_party_name,b.ship_to_city,b.country,b.ship_to_zip_code,b.order_type,"
					+ "b.ship_to_address,b.ft,b.bill_to,b.retailerid,b.customer_id,b.customer_key from (select a.ft,a.ship_to_address,a.order_type,a.ship_to_city,a.country,"
					+ "a.ship_to_zip_code,a.ship_to_party_name,a.bill_to,a.dn,a.customer_id,a.delivery_plant,"+
					"(select ship_to_id from ship_to where ship_to_name=a.order_type) as retailerid,"+
					"(select customer_key from customer_brand where cb_id=a.customer_id) as customer_key,"+
					"(select pro_id from country_province where p_code=a.ship_to_state and nation_id=(select ccid from country_code where c_country=a.country)) as pro_id_cc,"+
//					"(select id from product_storage_catalog where title=REPLACE(a.ship_to_party_name,' ','_')) as product_id,"+
					"(select id from product_storage_catalog where REPLACE(title,' ','')=REPLACE(a.ship_to_party_name,' ','') and deliver_pro_id=pro_id_cc and deliver_nation=(select ccid from country_code where c_country=a.country) and replace(deliver_house_number,' ','')=REPLACE(a.ship_to_address,' ','')  and storage_type_id=retailerid and storage_type=5) as product_id,"+
					"(select id from product_storage_catalog where REPLACE(title,' ','')=REPLACE(a.ship_to_party_name,' ','') and "+
					"deliver_pro_id=pro_id_cc and REPLACE(deliver_city,' ','') like concat('%',REPLACE(a.ship_to_city,' ',''),'%') and deliver_zip_code=a.ship_to_zip_code and replace(deliver_house_number,' ','')=REPLACE(a.ship_to_address,' ','') and storage_type_id=retailerid and storage_type=5) as isright "+
					"from wms_order_lines_tmp a where correct=0 and importer="+importer+" and customer_id='"+cid+"') b left join b2b_order c "+
					"on b.customer_id=c.customer_id and b.dn=c.customer_dn";
			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findSignOther(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param dn
	 * @param cid
	 * @param importer
	 * @return
	 * @throws Exception
	 */
	public DBRow findDNOtherErr(String dn,String cid,long importer) throws Exception{
		try {
			String sql="select count(1) as cnt from wms_order_lines_tmp where correct<>0 and dn='"+dn+"' and customer_id='"+cid+"' and importer="+importer;
			return this.dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("findDNOtherErr(String dn,String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 插叙出要插入或者更新的dn
	 * @param importer
	 * @param customer_id
	 * @return
	 * @throws Exception
	 */
	public DBRow [] getCanImportOrders(long importer,String cid) throws Exception{
		try{
			String sql="select distinct dn,loadno,correct,country from "+ConfigBean.getStringValue("wms_order_lines_tmp")+" where correct in (4,13,34) and importer="+importer+" and customer_id='"+cid+"'";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getCanImportOrders(long importer,String customer_id) error:" + e);
		}
	}
	
	/**
	 * 根据dn查询order临时表的关于主order的信息
	 * @param dn
	 * @return
	 * @throws Exception
	 */
	public DBRow [] getOrderTmpByDn(String dn,String cid,long uid) throws Exception{
		try{
			String sql="select distinct ship_not_before,ship_not_later,ship_to_zip_code,delivery_plant,dn,order_number,retail_po,order_date,ship_to_party_name,ship_to_address,ship_to_city," +
					"ship_to_state,mabd,req_ship_date,bill_to,loadno,customer_id,freight_type,pickup_appointment_date,pickup_appointment_time,bol_tracking,carrier,country,ft,order_type,"
					+ "d.id as bill_to_id,d.title as bill_to_name,d.deliver_house_number as bill_to_address1,d.deliver_street as bill_to_address2,d.deliver_city as bill_to_city,"
					+ "e.p_code as bill_to_state,f.c_country as bill_to_country,d.deliver_zip_code as bill_to_zip_code,d.deliver_contact as bill_to_contact,d.deliver_phone as bill_to_phone  " +
					"from wms_order_lines_tmp left join product_storage_catalog d on d.title=bill_to "
					+ "left join country_province e on e.pro_id=d.deliver_pro_id left join country_code f on f.ccid=d.deliver_nation where dn='"+dn+"' and customer_id='"+cid+"' and importer="+uid;
			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getOrderTmpByDn(String dn,String cid,long uid) error:"+e);
		}
	}
	
	
	public void updateSign12(String dn,String cid,long importer) throws Exception{
		try{
			String sql = "update " + ConfigBean.getStringValue("wms_order_lines_tmp") + " set correct=12 where dn='" + dn
					+ "' and customer_id='" + cid + "' and importer=" + importer + " and correct in (4,13)";
			
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("updateSign12(String dn,String cid,long importer) error:"+e);
		}
	}
	
	/**
	 * 获取明细
	 * @param dn
	 * @param cid
	 * @param importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findOrderLines(String dn, String cid, long importer) throws Exception{
		try{
			StringBuilder sb=new StringBuilder();
			sb.append("select order_number,retail_po,title,ifnull(pc_id,0) as pc_id,material_number,material_number,sum(ceil(order_qty)) as delivery_count,"
					+ "case when (material_number='' or material_number is null) then model_number else concat(model_number,'/',material_number) end as b2b_p_name,model_number,sum(ceil(order_qty)) as b2b_count,sum(ceil(order_qty)) as b2b_wait_count,"
					+ "sum(wms_order_lines_tmp.weight)/sum(order_qty) as b2b_weight,sum(wms_order_lines_tmp.weight) as total_weight,sum(pallet_spaces) as pallet_spaces,sum(boxes) as boxes,note")
			.append(" from wms_order_lines_tmp left join product on p_name=CONCAT(model_number,case when material_number is null then '' else concat('/',material_number) end)"
					+ " where dn='").append(dn)
			.append("' and customer_id='").append(cid).append("' and importer=").append(importer)
			.append(" group by material_number,model_number,title;");
			return this.dbUtilAutoTran.selectMutliple(sb.toString());
		}catch(Exception e){
			throw new Exception("findOrderLines(String dn, String cid, long importer) error:"+e);
		}
	}
	
	/**
	 * 根据customer_id transport_id删除临时表记录
	 * @param cid
	 * @param tid
	 * @throws Exception
	 */
	public void delLinesTmp(String cid,long importer) throws Exception{
		try{
			this.dbUtilAutoTran.delete("where customer_id='"+cid+"' and importer="+importer, ConfigBean.getStringValue("wms_order_lines_tmp"));
		}catch(Exception e){
			throw new Exception("delLinesTmp(String cid,long importer) error:"+e);
		}
	}
	
	/**
	 * 插叙出要插入或者更新的dn在excel中的行号
	 * @param importer
	 * @param customer_id
	 * @return
	 * @throws Exception
	 */
	public DBRow [] getCorrectDataRow(long importer,String customer_id,Integer... correct) throws Exception{
		try{
			String sql="select row from "+ConfigBean.getStringValue("wms_order_lines_tmp")+" where correct in ("+StringUtils.join(correct,",")+") and importer="+importer+" and customer_id='"+customer_id+"' order by row";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getCorrectDataRow(DBRow para) error:" + e);
		}
	}
	
	/**
	 * 查询导入load时需要校验dn下某些列必须相同
	 * @param dn
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findLoadAppByDn(String dn,String cid,long importer) throws Exception{
		try{
			String sql="SELECT dn,loadno,pickup_appointment_date,pickup_appointment_time,freight_type,carrier,"+
						"row FROM wms_order_lines_tmp where dn='"+dn+"' and customer_id='"+cid+"' and importer="+importer;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findLoadAppByDn(String dn,String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 查询导入load时需要校验dn下某些列必须相同
	 * @param dn
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findFreightTermsBillByDn(String dn,String cid,long importer) throws Exception{
		try{
			String sql="SELECT dn,bill_to,ft,"+
						"row FROM wms_order_lines_tmp where dn='"+dn+"' and customer_id='"+cid+"' and importer="+importer;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findFreightTermsBillByDn(String dn,String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 查找都有的dn
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] findCommonDn(String cid,long importer) throws Exception{
		try{
			String sql="select distinct dn from wms_order_lines_tmp where customer_id='"+cid+
					"' and importer="+importer+" and correct in (14,11)";
			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findCommonDn(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 查找完全一样的dn后在进行load比较
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findLoadApp(String cid,long importer,Set<Integer> correct) throws Exception{
		try{
			String sql="select distinct dn,loadno,pickup_appointment_date,pickup_appointment_time,correct,carrier,ft,freight_type,bill_to from "+ConfigBean.getStringValue("wms_order_lines_tmp")+" where correct in ("+StringUtils.join(correct, ",")+") and customer_id='"+cid+"' and importer="+importer;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findLoadApp(String cid,long importer,Set<Integer> correct) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param cid
	 * @param importer
	 * @param correct
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findSO(String cid,long importer,int correct) throws Exception{
		try{
			String sql="select b.b2b_oid,b.so as so,b.b2b_detail_id,a.order_number,a.dn,a.row from wms_order_lines_tmp a left join "
					+ "(select c.lot_number,c.item_id,c.title,c.b2b_detail_id,c.so,d.customer_dn,d.customer_id,d.b2b_oid from b2b_order_item c inner join b2b_order d on c.b2b_oid=d.b2b_oid where d.customer_id='"+cid+"') as b "
					+ " on b.lot_number=a.material_number and b.item_id=a.model_number "
					+ " and b.title=a.title and b.customer_dn=a.dn and b.customer_id=a.customer_id and a.correct="+correct+" and a.customer_id='"+cid+"' and a.importer="+importer +" where b.b2b_oid is not null";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findSO(String cid,long importer) error:" + e);
		}
	}
	
}
