package com.vvme.order.dao;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

/**
 * 
 * @ClassName: FileDao
 * @Description: 
 * @author yetl
 * @date 2015年4月29日
 *
 */
public class FileDao {
	private DBUtilAutoTran_Optm dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public void updateById(long id , DBRow row) throws Exception {
		try{
			dbUtilAutoTran.update("where file_id=" + id,  ConfigBean.getStringValue("file"), row);	
		}catch (Exception e) {
			throw new Exception("updateById(long id , DBRow row):"+e);
		}
	}
}
