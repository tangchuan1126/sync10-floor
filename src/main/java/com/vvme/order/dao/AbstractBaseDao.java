package com.vvme.order.dao;

import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

/**
 * 
 * @ClassName: AbstractBaseDao
 * @Description: 
 * @author yetl
 * @date 2015年7月3日
 *
 */
public abstract class AbstractBaseDao {
	protected final String tableName;
	
	protected DBUtilAutoTran_Optm dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public AbstractBaseDao(){
		this.tableName=getTableName();
	}
	
	/**
	 * 
	 * @param params
	 * @param fields
	 * @return
	 * @throws Exception
	 * @author: ye
	 */
	public final DBRow[] find(DBRow para, String[] fields) throws Exception {
		if (null == fields || fields.length <= 0)
			return find(para);
		
		try {
			StringBuilder sb = new StringBuilder("select ").append(StringUtils.join(fields, ",")).append(" from ")
					.append(tableName);
			
			@SuppressWarnings("unchecked")
			ArrayList<String> list = para.getFieldNames();
			
			if (list.size() > 0) {
				sb.append(" where ");
				for (String fn : list) {
					sb.append(fn).append("=? and ");
				}
			}
			
			String sql = sb.toString().substring(0, sb.toString().length() - 5);
			
			return this.dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("find(DBRow para, String[] fields) error:" + e);
		}
	}
	
	/**
	 * 查找b2b_order
	 * 
	 * @param para
	 * @return
	 * @throws Exception
	 * @author: ye
	 */
	public final DBRow[] find(DBRow para) throws Exception {
		try {
			StringBuilder sb = new StringBuilder("select * from " + tableName);
			
			if(para!=null){
				@SuppressWarnings("unchecked")
				ArrayList<String> list = para.getFieldNames();
				
				if (list.size() > 0) {
					sb.append(" where ");
					for (String fn : list) {
						sb.append(fn).append("=? and ");
					}
					String sql = sb.toString().substring(0, sb.toString().length() - 5);
					
					return this.dbUtilAutoTran.selectPreMutliple(sql, para);
				}
			}
			
			return this.dbUtilAutoTran.selectMutliple(sb.toString());
		} catch (Exception e) {
			throw new Exception("find(DBRow para) error:" + e);
		}
	}
	
	protected abstract String getTableName();
}
