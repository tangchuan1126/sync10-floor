package com.vvme.order.dao;

import com.cwc.db.DBRow;

public class CarrierScacMcdotDao extends AbstractBaseDao{
	
	/**
	 * 查询运输公司
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getByScac(String scac) throws Exception{
		try{
			String sql="select carrier from carrier_scac_mcdot where scac='"+scac+"'";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("getByScac(String id) error:"+e);
		}
	}

	protected String getTableName() {
		return "carrier_scac_mcdot";
	}
}
