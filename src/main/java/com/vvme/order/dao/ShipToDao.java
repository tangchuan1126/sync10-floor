package com.vvme.order.dao;

import com.cwc.db.DBRow;

public class ShipToDao extends AbstractBaseDao{
	/**
	 * 
	 * @param shipTo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getShipToId(String shipTo) throws Exception{
		try{
			String sql="select * from ship_to where '"+shipTo+"'=ship_to_name";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getShipToId(String shipTo) error:" + e);
		}
	}

	protected String getTableName() {
		return "ship_to";
	}
}
