package com.vvme.order.dao;

import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

public class B2bOrderItemDao {
	private DBUtilAutoTran_Optm dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	/**
	 * 添加订单详细
	 * 
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long add(DBRow dbrow) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("b2b_order_item"), dbrow);
		} catch (Exception e) {
			throw new Exception("add error:" + e);
		}
	}
	
	/**
	 * 
	 * 根据b2b_oid 获取明细列表
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-24下午5:49:39<br>
	 * @author: cuicong
	 */
	public DBRow[] getByOid(long b2b_oid) throws Exception {
		try {
			// String sql = "select * from " + ConfigBean.getStringValue("b2b_order_item") + " where b2b_oid = ? ";
			// 加入商品 upc信息 wangcr 2015.1.29
			String sql = "select td.*, pcode.p_code, p.length, p.width, p.heigth, p.freight_class, p.nmfc_code, '' lp_name from "
					+ ConfigBean.getStringValue("b2b_order_item") + " as td left join "
					+ ConfigBean.getStringValue("product") + " as p on p.pc_id = td.b2b_pc_id left join "
					+ ConfigBean.getStringValue("product_code")
					+ " as pcode on pcode.pc_id = p.pc_id and pcode.code_type= " + CodeTypeKey.UPC
					//+ " left join license_plate_type a on a.pc_id=p.pc_id "
					+ " where td.b2b_oid = ? order by td.b2b_detail_id";
			DBRow para = new DBRow();
			para.add("b2b_oid", b2b_oid);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("getByOid(long b2b_oid) error:" + e);
		}
	}
	
	/**
	 * 修改订单
	 * 
	 * @param b2b_order_id
	 * @param para
	 * @throws Exception
	 */
	public void updateById(long id, DBRow para) throws Exception {
		try {
			dbUtilAutoTran.update("where b2b_detail_id = " + id, ConfigBean.getStringValue("b2b_order_item"), para);
		} catch (Exception e) {
			throw new Exception("updateById(long id, DBRow para) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findByOid(long b2b_oid) throws Exception {
		try {
			String sql = "select td.* from "
					+ ConfigBean.getStringValue("b2b_order_item") + " as td where td.b2b_oid = ? order by td.b2b_detail_id";
			DBRow para = new DBRow();
			para.add("b2b_oid", b2b_oid);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("findByOid(long b2b_oid) error:" + e);
		}
	}
}
