package com.vvme.order.dao;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

/**
 * 
 * @ClassName: CountryProvinceDao
 * @Description:
 * @author yetl
 * @date 2015年4月27日
 *
 */
public class CountryProvinceDao {
	private DBUtilAutoTran_Optm dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public DBRow getDetailCountryProvinceByProvinceCode(String p_code,
			long nation_id) throws Exception {
		try {
			String sql = "select * from "
					+ ConfigBean.getStringValue("country_province")
					+ " where p_code = ? and nation_id = ? ";

			DBRow para = new DBRow();
			para.add("p_code", p_code);
			para.add("nation_id", nation_id);

			return dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (Exception e) {
			throw new Exception("getDetailCountryProvinceByProvinceCode error:"
					+ e);
		}
	}

	public DBRow getDetailProByProId(int pro_id) throws Exception {
		try {
			String sql = "select * from "
					+ ConfigBean.getStringValue("country_province")
					+ " where pro_id = ? ";

			DBRow para = new DBRow();
			para.add("pro_id", pro_id);

			return dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (Exception e) {
			throw new Exception("getDetailProByProId error:" + e);
		}
	}
}
