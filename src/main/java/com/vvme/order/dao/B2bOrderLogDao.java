package com.vvme.order.dao;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

public class B2bOrderLogDao {
	private DBUtilAutoTran_Optm dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	/**
	 * 添加订单日志
	 * 
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long add(DBRow row) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("b2b_order_log"), row);
		} catch (Exception e) {
			throw new Exception("add(DBRow row):" + e);
		}
	}
}
