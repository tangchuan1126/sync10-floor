package com.vvme.order.dao;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

public class XlsExportDao {
	private DBUtilAutoTran_Optm dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	/**
	 * 导出模板
	 * @return
	 * @throws Exception
	 */
	public DBRow [] getExcelExport() throws Exception{
		try{
			String sql="select * from wms_xls_export";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getExcelExport() error:" + e);
		}
	}	
	
}
