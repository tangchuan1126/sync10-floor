package com.cwc.transaction;

/**
 * 提供给需要跟数据库事务同步类实现的接口
 * 有些事件必须跟数据库事务同步提交，所以必须实现该接口，把事件加入到事务流程中，在最后跟数据库事务一起提交
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public interface TransactionActionIFace 
{
	//事务需要实现方法
	public void perform() throws Exception;
}
