/*
 * Copyright 2002-2007 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cwc.transaction;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Properties;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;
import org.springframework.aop.support.AopUtils;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.interceptor.TransactionAttribute;
import org.springframework.transaction.interceptor.TransactionAttributeSource;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import org.springframework.transaction.interceptor.TransactionProxyFactoryBean;
import org.springframework.transaction.support.CallbackPreferringPlatformTransactionManager;
import org.springframework.transaction.support.TransactionCallback;



/**
 * AOP Alliance MethodInterceptor for declarative transaction
 * management using the common Spring transaction infrastructure
 * ({@link org.springframework.transaction.PlatformTransactionManager}).
 *
 * <p>Derives from the {@link TransactionAspectSupport} class which
 * contains the integration with Spring's underlying transaction API.
 * TransactionInterceptor simply calls the relevant superclass methods
 * such as {@link #createTransactionIfNecessary} in the correct order.
 *
 * <p>TransactionInterceptors are thread-safe.
 *
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @see TransactionProxyFactoryBean
 * @see org.springframework.aop.framework.ProxyFactoryBean
 * @see org.springframework.aop.framework.ProxyFactory
 */
public class SpringTransactionInterceptor extends TransactionInterceptor implements MethodInterceptor, Serializable {
	static Logger log = Logger.getLogger("PLATFORM");
	
	/**
	 * Create a new TransactionInterceptor.
	 * <p>Transaction manager and transaction attributes still need to be set.
	 * @see #setTransactionManager
	 * @see #setTransactionAttributes(java.util.Properties)
	 * @see #setTransactionAttributeSource(TransactionAttributeSource)
	 */
	public SpringTransactionInterceptor() {
	}

	/**
	 * Create a new TransactionInterceptor.
	 * @param ptm the transaction manager to perform the actual transaction management
	 * @param attributes the transaction attributes in properties format
	 * @see #setTransactionManager
	 * @see #setTransactionAttributes(java.util.Properties)
	 */
	public SpringTransactionInterceptor(PlatformTransactionManager ptm, Properties attributes) {
		setTransactionManager(ptm);
		setTransactionAttributes(attributes);
	}

	/**
	 * Create a new TransactionInterceptor.
	 * @param ptm the transaction manager to perform the actual transaction management
	 * @param tas the attribute source to be used to find transaction attributes
	 * @see #setTransactionManager
	 * @see #setTransactionAttributeSource(TransactionAttributeSource)
	 */
	public SpringTransactionInterceptor(PlatformTransactionManager ptm, TransactionAttributeSource tas) {
		setTransactionManager(ptm);
		setTransactionAttributeSource(tas);
	}

	//事务拦截器的拦截方法  
	public Object invoke(final MethodInvocation invocation) throws Throwable {
        //通过AOP获取事务的目标类  
        Class<?> targetClass = (invocation.getThis() != null ? AopUtils.getTargetClass(invocation.getThis()) : null);  
        //通过事务属性源TransactionAttributeSource读取事务的属性配置，即调用上面名称匹配  
        //事务属性源NameMatchTransactionAttributeSource的方法  

		final TransactionAttribute txAttr =
				getTransactionAttributeSource().getTransactionAttribute(invocation.getMethod(), targetClass);
		//获取Spring事务管理IoC容器配置的事务处理器  
		final PlatformTransactionManager tm = determineTransactionManager(txAttr);  
		
        //获取目标类指定方法的事务连接点  
        final String joinpointIdentification = methodIdentification(invocation.getMethod(), targetClass);  

		//区分不同类型的PlatformTransactionManager事务处理器，不同类型的事务处理器调用方式不同。
		//对CallbackPreferringPlatformTransactionManager，需要回调函数来实现事务的创建和提交，
		//对非CallbackPreferringPlatformTransactionManager来说，则不需要使用回调函数来实现事务处理。  
        
		//非CallbackPreferringPlatformTransactionManager类型的事务处理器 
        if (txAttr == null || !(tm instanceof CallbackPreferringPlatformTransactionManager)) {

        //创建事务，将当前事务状态和信息保存到TransactionInfo对象中			
		TransactionInfo txInfo = createTransactionIfNecessary (tm, txAttr, joinpointIdentification);
		Object retVal = null;
			try {
                //沿着拦截器链调用处理，使得最后目标对象的方法得到调用  
				//增加事务标记
				//log.info("== start ===>"+invocation.getMethod());

				//Echo写的代码，目的是为了提供非数据库操作时的事务功能。需要检查
				retVal = invocation.proceed();
			}
			catch (Throwable ex) {
                //在调用拦截器拦过程中出现异常，则根据事务配置进行提交或回滚处理  
				//Echo写的代码，目的是为了提供非数据库操作时的事务功能。需要检查
				//log.info("EX 回滚:"+ex.getClass()+" - "+invocation.getMethod().getName());
				completeTransactionAfterThrowing(txInfo, ex);
				throw ex;
			}
			    
			    //清除与当前线程绑定的事务信息
			finally {
				//log.info("finally 清除事务-"+invocation.getMethod().getName());
				cleanupTransactionInfo(txInfo);
			}
			
            //通过事务处理器来对事务进行提交  
			//log.info("关闭连接-"+invocation.getMethod().getName());
			commitTransactionAfterReturning(txInfo);
			//正常完成，清理事务
			//log.info("== end ===>"+invocation.getMethod());
			return retVal;
		}
		  //CallbackPreferringPlatformTransactionManager类型的事务处理器  
		  //It's a CallbackPreferringPlatformTransactionManager: pass a TransactionCallback in.
		else {
          //通过回调函数对事务进行处理  
			try {
                //执行实现TransactionCallback接口的doInTransaction回调方法  
                Object result = ((CallbackPreferringPlatformTransactionManager) tm).execute(txAttr,  
                        new TransactionCallback<Object>() {  
                	//实现TransactionCallback接口匿名内部类的回调方法  
					public Object doInTransaction(TransactionStatus status) 
					{
					    //创建和准备事务  
						TransactionInfo txInfo = prepareTransactionInfo(tm, txAttr, joinpointIdentification, status);
								try {
									//沿着拦截器拦调用
									return invocation.proceed();
								}
                                //拦截器链处理过程中产生异常  
								catch (Throwable ex) {
                                    //如果事务对异常进行回滚处理  
									if (txAttr.rollbackOn(ex)) {
                                        //如果异常时运行时异常，则事务回滚处理  										if (ex instanceof RuntimeException) {
										if (ex instanceof RuntimeException) {  
                                            throw (RuntimeException) ex;  
                                        }  
                                        //如果不是运行时异常，则提交处理  
										else {
											throw new ThrowableHolderException(ex);
										}
									}
                                    //如果事务对异常不进行回滚处理  
									else {
                                        //提交处理  
										return new ThrowableHolder(ex);
									}
								}
                                //清除当前线程绑定的事务信息  
								finally {
									cleanupTransactionInfo(txInfo);
								}
							}
						});
              //对调用结果异常进行处理。  
              //如果是ThrowableHolder类型的异常，则转换为Throwable抛出  
				if (result instanceof ThrowableHolder) {
					throw ((ThrowableHolder) result).getThrowable();
				}
                //如果不是ThrowableHolder类型的异常，则异常不做处理直接抛出  
				else {
					return result;
				}
			}
			catch (ThrowableHolderException ex) {
				throw ex.getCause();
			}
		}
	}


	//---------------------------------------------------------------------
	// Serialization support
	//---------------------------------------------------------------------

	private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
		// Rely on default serialization, although this class itself doesn't carry state anyway...
		ois.defaultReadObject();

		// Serialize all relevant superclass fields.
		// Superclass can't implement Serializable because it also serves as base class
		// for AspectJ aspects (which are not allowed to implement Serializable)!
		setTransactionManager((PlatformTransactionManager) ois.readObject());
		setTransactionAttributeSource((TransactionAttributeSource) ois.readObject());
	}

	private void writeObject(ObjectOutputStream oos) throws IOException {
		// Rely on default serialization, although this class itself doesn't carry state anyway...
		oos.defaultWriteObject();

		// Deserialize superclass fields.
		oos.writeObject(getTransactionManager());
		oos.writeObject(getTransactionAttributeSource());
	}


	/**
	 * Internal holder class for a Throwable, used as a return value
	 * from a TransactionCallback (to be subsequently unwrapped again).
	 */
	private static class ThrowableHolder {

		private final Throwable throwable;

		public ThrowableHolder(Throwable throwable) {
			this.throwable = throwable;
		}

		public final Throwable getThrowable() {
			return this.throwable;
		}
	}


	/**
	 * Internal holder class for a Throwable, used as a RuntimeException to be
	 * thrown from a TransactionCallback (and subsequently unwrapped again).
	 */
	private static class ThrowableHolderException extends RuntimeException {

		public ThrowableHolderException(Throwable throwable) {
			super(throwable);
		}

		public String toString() {
			return getCause().toString();
		}
	}

}
