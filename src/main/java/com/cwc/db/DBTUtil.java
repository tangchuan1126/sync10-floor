package com.cwc.db;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;


/**
 * 使用事务DB操作类（不再使用）
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class DBTUtil
{
	static Logger log = Logger.getLogger("DB");
    private DBConnectionManager ll11l1111l1l1l1l1l11ll1l1llll111ll1l1l1l1ll11;
    private Connection ll11l1l1l1llll111ll1l1l1l1ll11;
    private DBConfigure ll11l11l1l11ll1l1llll111ll1l1l1l1ll11 = new DBConfigure(); 

    /*
     *
     *@param
     *@return
     */
    public DBTUtil()
            throws Exception
    {
        ll11l1111l1l1l1l1l11ll1l1llll111ll1l1l1l1ll11 = DBConnectionManager.getInstance();
    }

     
    public boolean insert(String ll11l1l1l1llll111ll1l1l1l1ll1, DBRow ll111ll11ll1ll111ll1l1l1ll1, Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
            throws Exception
    {
        boolean ok = false;
        if(ll111ll11ll1ll111ll1l1l1ll1 == null)
            return ok;
        ArrayList ll11l11ll111ll11ll1ll1ll1l1l1ll1 = ll111ll11ll1ll111ll1l1l1ll1.getFieldNames();
        if(ll11l11ll111ll11ll1ll1ll1l1l1ll1 == null)
            return ok;
        int ll11l1111l1l11l1l11ll1l1llll111ll1l1l1l1ll11 = ll11l11ll111ll11ll1ll1ll1l1l1ll1.size();
        if(ll11l1111l1l11l1l11ll1l1llll111ll1l1l1l1ll11 == 0)
            return ok;
        //sql begin
        StringBuffer ll11111ll11ll1ll111ll1l1l1ll1 = new StringBuffer("(");
        StringBuffer ll1111ll111ll11ll1ll1ll1l1l1ll1 = new StringBuffer("values(");
        boolean started = false;
        for(int l11l1111l11l1llll111ll1l1l1l1ll111 = 0; l11l1111l11l1llll111ll1l1l1l1ll111 < ll11l1111l1l11l1l11ll1l1llll111ll1l1l1l1ll11; l11l1111l11l1llll111ll1l1l1l1ll111++)
        {
            String ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1 = (String)ll11l11ll111ll11ll1ll1ll1l1l1ll1.get(l11l1111l11l1llll111ll1l1l1l1ll111);
            if(started)
            {
                ll11111ll11ll1ll111ll1l1l1ll1.append(",");
                ll1111ll111ll11ll1ll1ll1l1l1ll1.append(",");
            }
            ll11111ll11ll1ll111ll1l1l1ll1.append(ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1);
            ll1111ll111ll11ll1ll1ll1l1l1ll1.append("?");
            started = true;
        }

        ll11111ll11ll1ll111ll1l1l1ll1.append(")");
        ll1111ll111ll11ll1ll1ll1l1l1ll1.append(")");
       //sql l11l11lll1l11l1llll111ll1l1l1l1ll111

        PreparedStatement ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11 = null;
        StringBuffer ll111ll111ll11ll1ll1ll1l1l1ll1 = new StringBuffer("INSERT INTO ");
        ll111ll111ll11ll1ll1ll1l1l1ll1.append(ll11l1l1l1llll111ll1l1l1l1ll1).append(" ");
        ll111ll111ll11ll1ll1ll1l1l1ll1.append(ll11111ll11ll1ll111ll1l1l1ll1.toString()).append(" ");
        ll111ll111ll11ll1ll1ll1l1l1ll1.append(ll1111ll111ll11ll1ll1ll1l1l1ll1.toString());
        //debug
        //System.out.println("sql=" + ll111ll111ll11ll1ll1ll1l1l1ll1.toString());
        try
            {
                if( ll1llll11ll11ll1ll11l11111ll1l1l1ll1 == null)
                {
                    ll11l1l1l1llll111ll1l1l1l1ll11 = ll11l1111l1l1l1l1l11ll1l1llll111ll1l1l1l1ll11.getConnection();
                }
                else
                {
                    ll11l1l1l1llll111ll1l1l1l1ll11 = ((Transaction)ll1llll11ll11ll1ll11l11111ll1l1l1ll1).getConnection();
                }
                ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11 = ll11l1l1l1llll111ll1l1l1l1ll11.prepareStatement(ll111ll111ll11ll1ll1ll1l1l1ll1.toString());
                //set value begin
                if(ll111ll11ll1ll111ll1l1l1ll1 != null)
                {
                    for(int l11l1111l11l1llll111ll1l1l1l1ll111 = 0; l11l1111l11l1llll111ll1l1l1l1ll111 < ll11l1111l1l11l1l11ll1l1llll111ll1l1l1l1ll11; l11l1111l11l1llll111ll1l1l1l1ll111++)
                    {
                        String ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1 = (String)ll11l11ll111ll11ll1ll1ll1l1l1ll1.get(l11l1111l11l1llll111ll1l1l1l1ll111);
                        DBObjectType ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1 = ll111ll11ll1ll111ll1l1l1ll1.getField(ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1);

                        if(ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1 != null)
                        {
                            if(ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.getFieldType() == 4)
                            {
                                byte ll1l11ll1l1l1ll11ll1111l1l1l1l11111l1l1l1ll111l1l1l1l1[] = null;
                                ByteArrayOutputStream ll1l11ll1l1l1ll11ll1111l1ll11111l1l1l1ll111l1l1l1l1 =
                                        new ByteArrayOutputStream();
                                //StreamUtil.putObjectIntoOutputStream(ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.getFieldValue(), ll1l11ll1l1l1ll11ll1111l1ll11111l1l1l1ll111l1l1l1l1);
                                ll1l11ll1l1l1ll11ll1111l1l1l1l11111l1l1l1ll111l1l1l1l1 = ll1l11ll1l1l1ll11ll1111l1ll11111l1l1l1ll111l1l1l1l1.toByteArray();
                                try
                                {
                                    if(ll1l11ll1l1l1ll11ll1111l1ll11111l1l1l1ll111l1l1l1l1 != null)
                                    {
                                        ll1l11ll1l1l1ll11ll1111l1ll11111l1l1l1ll111l1l1l1l1.close();
                                    }
                                }
                                catch(IOException e){}

                                ByteArrayInputStream ll1l11ll1l1l1ll11ll1111l1111111l1l1ll111l1l1l1l1 =
                                        new ByteArrayInputStream(ll1l11ll1l1l1ll11ll1111l1l1l1l11111l1l1l1ll111l1l1l1l1);
                                ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.setBinaryStream(l11l1111l11l1llll111ll1l1l1l1ll111 + 1, ll1l11ll1l1l1ll11ll1111l1111111l1l1ll111l1l1l1l1, ll1l11ll1l1l1ll11ll1111l1l1l1l11111l1l1l1ll111l1l1l1l1.length);
                            }
                            else
                            {
								ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.setObject(l11l1111l11l1llll111ll1l1l1l1ll111 + 1, ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.getFieldValue());
                            }
                        }
                        else
                        {
                            ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.setObject(l11l1111l11l1llll111ll1l1l1l1ll111, null);
                        }
                    }

                }

                //set value l11l11lll1l11l1llll111ll1l1l1l1ll111

                ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.executeUpdate();
                ok = true;
            }
            catch(SQLException e1)
            {
            	log.error("DBTUtil.insert error:" + e1);
            	log.error("Error SQL:"+ll111ll111ll11ll1ll1ll1l1l1ll1.toString());
                throw new SQLException("DBTUtil.insert error:" + e1);

            }


        return ok;

    }

     public int update(String l11ll1l1l1ll11ll1111l1l1l1l111l1l1l111l1l1l, String l11ll1l1l1ll11ll1111l1l1l11l1l1l111l1l1l, DBRow ll111ll11ll1ll111ll1l1l1ll1, Object l11ll1l1l1ll11ll11111l1l1l11l1l1l111l1l1l)
        throws Exception
    {
        int lines = 0;
        if(ll111ll11ll1ll111ll1l1l1ll1 == null)
            return lines;
        StringBuffer l11ll1l1l1ll11ll111l1l1l111l1l1l111l1l1l = new StringBuffer();
        l11ll1l1l1ll11ll111l1l1l111l1l1l111l1l1l.append("update ");
        l11ll1l1l1ll11ll111l1l1l111l1l1l111l1l1l.append(l11ll1l1l1ll11ll1111l1l1l11l1l1l111l1l1l);
        l11ll1l1l1ll11ll111l1l1l111l1l1l111l1l1l.append(" set ");
        ArrayList l11ll1l1l1ll111ll111l1l1l111l1l1l111l1l1l = ll111ll11ll1ll111ll1l1l1ll1.getFieldNames();
        if(l11ll1l1l1ll111ll111l1l1l111l1l1l111l1l1l == null || l11ll1l1l1ll111ll111l1l1l111l1l1l111l1l1l.size() == 0)
            return 0;
        int l11ll1l1l1ll11ll11l1l1l111l1l1l111l1l1l = l11ll1l1l1ll111ll111l1l1l111l1l1l111l1l1l.size();
        DBObjectType ll1l11ll1l1l1ll11ll1111l1l1l1l111l1l1l111l1l1l1ll1[] = new DBObjectType[l11ll1l1l1ll11ll11l1l1l111l1l1l111l1l1l];
        ArrayList l11ll1l1l1ll11ll1111ll1l1l111l1l1l111l1l1l = new ArrayList();
        for(int l11l1111l11l1llll111ll1l1l1l1ll111 = 0; l11l1111l11l1llll111ll1l1l1l1ll111 < l11ll1l1l1ll11ll11l1l1l111l1l1l111l1l1l; l11l1111l11l1llll111ll1l1l1l1ll111++)
        {
            String l11ll1l1l1ll11l1l1l111l1l1l111l1l1l = (String)l11ll1l1l1ll111ll111l1l1l111l1l1l111l1l1l.get(l11l1111l11l1llll111ll1l1l1l1ll111);
            DBObjectType ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1 = ll111ll11ll1ll111ll1l1l1ll1.getField(l11ll1l1l1ll11l1l1l111l1l1l111l1l1l);
            if(ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1 != null)
            {
                ll1l11ll1l1l1ll11ll1111l1l1l1l111l1l1l111l1l1l1ll1[l11l1111l11l1llll111ll1l1l1l1ll111] = ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1;
                Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1 = ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.getFieldValue();
                l11ll1l1l1ll11ll111l1l1l111l1l1l111l1l1l.append(l11ll1l1l1ll11l1l1l111l1l1l111l1l1l);
                l11ll1l1l1ll11ll111l1l1l111l1l1l111l1l1l.append("=");
                if(ll1llll11ll11ll1ll11l11111ll1l1l1ll1 == null)
                {
                    l11ll1l1l1ll11ll111l1l1l111l1l1l111l1l1l.append("null ");
                } else
                {
                    l11ll1l1l1ll11ll111l1l1l111l1l1l111l1l1l.append("? ");
                    l11ll1l1l1ll11ll1111ll1l1l111l1l1l111l1l1l.add(l11ll1l1l1ll11l1l1l111l1l1l111l1l1l);
                }
                if(l11l1111l11l1llll111ll1l1l1l1ll111 < l11ll1l1l1ll11ll11l1l1l111l1l1l111l1l1l - 1)
                    l11ll1l1l1ll11ll111l1l1l111l1l1l111l1l1l.append(", ");
            }
        }

        l11ll1l1l1ll11ll111l1l1l111l1l1l111l1l1l.append(l11ll1l1l1ll11ll1111l1l1l1l111l1l1l111l1l1l);

        PreparedStatement ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11 = null;
        l11ll1l1l1ll11ll11l1l1l111l1l1l111l1l1l = l11ll1l1l1ll11ll1111ll1l1l111l1l1l111l1l1l.size();
        String updateStr[];
        if(l11ll1l1l1ll11ll11l1l1l111l1l1l111l1l1l > 0)
        {
            updateStr = (String[])l11ll1l1l1ll11ll1111ll1l1l111l1l1l111l1l1l.toArray(new String[0]);
        }
        else
        {
            updateStr = new String[0];
        }
        try
        {
            if( l11ll1l1l1ll11ll11111l1l1l11l1l1l111l1l1l == null)
            {
                ll11l1l1l1llll111ll1l1l1l1ll11 = ll11l1111l1l1l1l1l11ll1l1llll111ll1l1l1l1ll11.getConnection();
            }
            else
            {
                ll11l1l1l1llll111ll1l1l1l1ll11 = ((Transaction)l11ll1l1l1ll11ll11111l1l1l11l1l1l111l1l1l).getConnection();
            }
            ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11 = ll11l1l1l1llll111ll1l1l1l1ll11.prepareStatement(l11ll1l1l1ll11ll111l1l1l111l1l1l111l1l1l.toString());
            for(int l11l1111l11l1llll111ll1l1l1l1ll111 = 0; l11l1111l11l1llll111ll1l1l1l1ll111 < l11ll1l1l1ll11ll11l1l1l111l1l1l111l1l1l; l11l1111l11l1llll111ll1l1l1l1ll111++)
            {
                String tmp = updateStr[l11l1111l11l1llll111ll1l1l1l1ll111];
                DBObjectType ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1 = ll111ll11ll1ll111ll1l1l1ll1.getField(tmp);
                Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1 = ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.getFieldValue();
                if(ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.getFieldType() == 4)
                {
                    byte ll1l11ll1l1l1ll11ll1111l1l1l1l11111l1l1l1ll111l1l1l1l1[] = null;
                    ByteArrayOutputStream ll1l11ll1l1l1ll11ll1111l1ll11111l1l1l1ll111l1l1l1l1 = new ByteArrayOutputStream();
                    //StreamUtil.putObjectIntoOutputStream(ll1llll11ll11ll1ll11l11111ll1l1l1ll1, ll1l11ll1l1l1ll11ll1111l1ll11111l1l1l1ll111l1l1l1l1);
                    ll1l11ll1l1l1ll11ll1111l1l1l1l11111l1l1l1ll111l1l1l1l1 = ll1l11ll1l1l1ll11ll1111l1ll11111l1l1l1ll111l1l1l1l1.toByteArray();
                    try
                    {
                        if(ll1l11ll1l1l1ll11ll1111l1ll11111l1l1l1ll111l1l1l1l1 != null)
                        {
                            ll1l11ll1l1l1ll11ll1111l1ll11111l1l1l1ll111l1l1l1l1.close();
                        }
                    }
                    catch(IOException e){}
                    ByteArrayInputStream ll1l11ll1l1l1ll11ll1111l1111111l1l1ll111l1l1l1l1 = new ByteArrayInputStream(ll1l11ll1l1l1ll11ll1111l1l1l1l11111l1l1l1ll111l1l1l1l1);
                    ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.setBinaryStream(l11l1111l11l1llll111ll1l1l1l1ll111 + 1, ll1l11ll1l1l1ll11ll1111l1111111l1l1ll111l1l1l1l1, ll1l11ll1l1l1ll11ll1111l1l1l1l11111l1l1l1ll111l1l1l1l1.length);
                } else
                {
                    ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.setObject(l11l1111l11l1llll111ll1l1l1l1ll111 + 1, ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
                }
            }

            lines = ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.executeUpdate();
            if(ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11!=null)
            {
                ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.close();
            }
        }
        catch(SQLException e)
        {
        	log.error("DBTUtil.update error:" + e);
        	log.error("DBTUtil.update errorSql:" + l11ll1l1l1ll11ll111l1l1l111l1l1l111l1l1l.toString());
            throw new SQLException("DBTUtil.update error:" + e);
        }

        
        return lines;

    }

    public int delete(String l11ll1l1l1ll11ll1111l1l1l1l111l1l1l111l1l1l, String ll11l1l1l1llll111ll1l1l1l1ll1, Object l11ll1l1l1ll11ll11111l1l1l11l1l1l111l1l1l)
        throws Exception
    {
        int num = 0;
        String sql=null;
		try
		{
			if(ll11l1l1l1llll111ll1l1l1l1ll1 == null)
			{
			    return 0;
			}
			sql = "delete from " + ll11l1l1l1llll111ll1l1l1l1ll1 + " "
			        + (l11ll1l1l1ll11ll1111l1l1l1l111l1l1l111l1l1l==null?"":l11ll1l1l1ll11ll1111l1l1l1l111l1l1l111l1l1l);
			if( l11ll1l1l1ll11ll11111l1l1l11l1l1l111l1l1l == null)
			{
			    ll11l1l1l1llll111ll1l1l1l1ll11 = ll11l1111l1l1l1l1l11ll1l1llll111ll1l1l1l1ll11.getConnection();
			}
			else
			{
			    ll11l1l1l1llll111ll1l1l1l1ll11 = ((Transaction)l11ll1l1l1ll11ll11111l1l1l11l1l1l111l1l1l).getConnection();
			}
			num = 0;
			num = ll11l1l1l1llll111ll1l1l1l1ll11.createStatement().executeUpdate(sql);
		} 
		catch (SQLException e)
		{
			log.error("DBTUtil.delete error:" + e);
			log.error("errorSql:" + sql);
			throw new SQLException("delete error:" + e);
		}    
		

        return num;
    }

    /*
     * get Result
     *@param ll1l11ll11ll1ll11l11111ll1l1l1ll1: ResultSet
     *@param PageCtrl:
     *@return  DBRow[]
     */

    private DBRow[] l11l1111l1l1111ll1l1llll111ll1l1l1l1ll111(ResultSet ll1l11ll11ll1ll11l11111ll1l1l1ll1, PageCtrl ll1l11ll11ll1ll11l11111ll1l1l1ll111)
          throws Exception
      {
          int l11l1111l1l11ll1l1llll111ll1l1l1l1ll111 = 0;
          int l11l1111l1l11l1llll111ll1l1l1l1ll111 = 0;
          int l11l11lll1l11l1llll111ll1l1l1l1ll111 = 0;
          int l11l1111l11l1llll111ll1l1l1l1ll111 = 0;
          int l11l111111l1llll111ll1l1l1l1ll111 = 0;
          int l11l11ll11l1llll111ll1l1l1l1ll111 = 0;
          if(ll1l11ll11ll1ll11l11111ll1l1l1ll111 != null)
          {
              l11l111111l1llll111ll1l1l1l1ll111 = ll1l11ll11ll1ll11l11111ll1l1l1ll111.getPageSize();
              l11l11ll11l1llll111ll1l1l1l1ll111 = ll1l11ll11ll1ll11l11111ll1l1l1ll111.getPageNo();
              if(l11l111111l1llll111ll1l1l1l1ll111 <= 0 || l11l11ll11l1llll111ll1l1l1l1ll111 < 1)
                  throw new Exception("PageCtrl data is error");
              l11l1111l1l11l1llll111ll1l1l1l1ll111 = ( l11l11ll11l1llll111ll1l1l1l1ll111 - 1) * l11l111111l1llll111ll1l1l1l1ll111;
              l11l11lll1l11l1llll111ll1l1l1l1ll111 = l11l11ll11l1llll111ll1l1l1l1ll111 * l11l111111l1llll111ll1l1l1l1ll111;
          }
          else
          {

          }

          int l1111l11ll11l1llll111ll1l1l1l1ll111 = 0;
          DBRow l111l11l1llll111ll1l1l1l1ll111[] = null;
          DBObjectType l111l11l1llll111ll1l1l1l1ll1111l1l1l1l1l[] = ll1l11ll1l1l1ll11ll1111l1l1l1l111l1l1l111l1l(ll1l11ll11ll1ll11l11111ll1l1l1ll1);
          if(l111l11l1llll111ll1l1l1l1ll1111l1l1l1l1l != null)
              l1111l11ll11l1llll111ll1l1l1l1ll111 = l111l11l1llll111ll1l1l1l1ll1111l1l1l1l1l.length;
          ArrayList list = new ArrayList();

          while(ll1l11ll11ll1ll11l11111ll1l1l1ll1 != null && ll1l11ll11ll1ll11l11111ll1l1l1ll1.next())
          {
              l11l1111l1l11ll1l1llll111ll1l1l1l1ll111++;
              if( (++l11l1111l11l1llll111ll1l1l1l1ll111 > l11l1111l1l11l1llll111ll1l1l1l1ll111 && l11l1111l11l1llll111ll1l1l1l1ll111 <= l11l11lll1l11l1llll111ll1l1l1l1ll111 ) || (l11l1111l1l11l1llll111ll1l1l1l1ll111 == 0 && l11l11lll1l11l1llll111ll1l1l1l1ll111 == 0) )
              {
                  DBRow ll111ll11ll1ll111ll1l1l1ll1 = new DBRow();
                  for(int i = 0; i < l1111l11ll11l1llll111ll1l1l1l1ll111; i++)
                  {
                      Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1 = null;
                      DBObjectType ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1 = l111l11l1llll111ll1l1l1l1ll1111l1l1l1l1l[i];
                      if(ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1 != null)
                      {
                          String l11ll1l1l1ll11l1l1l111l1l1l111l1l1l = ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.getFieldName();
                          DBObjectType l111l11l1llll11111ll1l1l1l1ll1111l1l1l1l1l = (DBObjectType)ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.clone();
                          try
                          {
                              if(l111l11l1llll11111ll1l1l1l1ll1111l1l1l1l1l.getFieldType() == 4)
                              {
                              } else
                              {
                                  ll1llll11ll11ll1ll11l11111ll1l1l1ll1 = ll1l11ll11ll1ll11l11111ll1l1l1ll1.getObject(l11ll1l1l1ll11l1l1l111l1l1l111l1l1l);
                                  l111l11l1llll11111ll1l1l1l1ll1111l1l1l1l1l.setFieldValue(ll1llll11ll11ll1ll11l11111ll1l1l1ll1);

                              }
                          }
                          catch(Exception e)
                          {
                          	 log.error("DBTUtil.l11l1111l1l1111ll1l1llll111ll1l1l1l1ll111 error:read from " + l11ll1l1l1ll11l1l1l111l1l1l111l1l1l + "  encountere error");
                          }
                          if(l111l11l1llll11111ll1l1l1l1ll1111l1l1l1l1l != null && l111l11l1llll11111ll1l1l1l1ll1111l1l1l1l1l.getFieldValue() != null)
                              ll111ll11ll1ll111ll1l1l1ll1.addField(l111l11l1llll11111ll1l1l1l1ll1111l1l1l1l1l);
                      }
                  }

                  list.add(ll111ll11ll1ll111ll1l1l1ll1);
              }
          }
          if(list.size() > 0)
          {
              l111l11l1llll111ll1l1l1l1ll111 = (DBRow[])list.toArray(new DBRow[0]);
          }
          else
          {
              l111l11l1llll111ll1l1l1l1ll111 = new DBRow[0];
          }
          if(ll1l11ll11ll1ll11l11111ll1l1l1ll111 != null)
          {
              int ll1l11ll11ll1ll11l11111ll1l1l1ll11111 = ((l11l1111l1l11ll1l1llll111ll1l1l1l1ll111 + l11l111111l1llll111ll1l1l1l1ll111) - 1) / l11l111111l1llll111ll1l1l1l1ll111;
              if( ll1l11ll11ll1ll11l11111ll1l1l1ll11111 < l11l11ll11l1llll111ll1l1l1l1ll111)
              {
                  l11l11ll11l1llll111ll1l1l1l1ll111 = ll1l11ll11ll1ll11l11111ll1l1l1ll11111;
              }

              ll1l11ll11ll1ll11l11111ll1l1l1ll111.setPageNo(l11l11ll11l1llll111ll1l1l1l1ll111);
              ll1l11ll11ll1ll11l11111ll1l1l1ll111.setPageSize(l11l111111l1llll111ll1l1l1l1ll111);
              ll1l11ll11ll1ll11l11111ll1l1l1ll111.setPageCount(ll1l11ll11ll1ll11l11111ll1l1l1ll11111);
              ll1l11ll11ll1ll11l11111ll1l1l1ll111.setAllCount(l11l1111l1l11ll1l1llll111ll1l1l1l1ll111);
              ll1l11ll11ll1ll11l11111ll1l1l1ll111.setRowCount(l111l11l1llll111ll1l1l1l1ll111.length);
          }

          return l111l11l1llll111ll1l1l1l1ll111;
      }


    private DBRow selectSingle(String ll1llll11l1l11ll1ll11l11111ll1l1l1ll1,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
        throws SQLException
    {
        DBRow ll1llll111l11ll1ll11l11111ll1l1l1ll1[] = selectMutliple(ll1llll11l1l11ll1ll11l11111ll1l1l1ll1,null,ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
        if(ll1llll111l11ll1ll11l11111ll1l1l1ll1 == null || ll1llll111l11ll1ll11l11111ll1l1l1ll1.length == 0)
            return(null);
        if(ll1llll111l11ll1ll11l11111ll1l1l1ll1.length > 1)
        {
        	log.error("DBTUtil.selectSingle error:Select Multiple data but only one is ok");
        	log.error("Error SQL:"+ll1llll11l1l11ll1ll11l11111ll1l1l1ll1);
        	throw new SQLException("DBTUtil.selectSingle error:Select Multiple data but only one is ok");
        }           
        else
        {
            return ll1llll111l11ll1ll11l11111ll1l1l1ll1[0];        	
        }
    }   
    

    private DBRow[] selectMutliple(String ll1llll11l1l11ll1ll11l11111ll1l1l1ll1,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
        throws SQLException
    {
        return selectMutliple(ll1llll11l1l11ll1ll11l11111ll1l1l1ll1, null,ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
    }

    /*
     *
     *@param
     *@return
     */

    private DBRow[] selectMutliple(String ll1llll11l1l11ll1ll11l11111ll1l1l1ll1, PageCtrl ll1l11ll11ll1ll11l11111ll1l1l1ll111,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
        throws SQLException
    {
        DBRow l111l11l1llll111ll1l1l1l1ll111[] = null;
        if( (ll1llll11l1l11ll1ll11l11111ll1l1l1ll1==null) || (ll1llll11l1l11ll1ll11l11111ll1l1l1ll1.length() == 0) )
            return null;

        try
        {
            PreparedStatement ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11 = null;
            ResultSet ll1l11ll11ll1ll11l11111ll1l1l1ll1 = null;
            ll11l1l1l1llll111ll1l1l1l1ll11 = ((Transaction)ll1llll11ll11ll1ll11l11111ll1l1l1ll1).getConnection();
            ((Transaction)ll1llll11ll11ll1ll11l11111ll1l1l1ll1).commit();
            try
            {
                ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11 = ll11l1l1l1llll111ll1l1l1l1ll11.prepareStatement(ll1llll11l1l11ll1ll11l11111ll1l1l1ll1);
                ll1l11ll11ll1ll11l11111ll1l1l1ll1 = ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.executeQuery();
                l111l11l1llll111ll1l1l1l1ll111 = l11l1111l1l1111ll1l1llll111ll1l1l1l1ll111(ll1l11ll11ll1ll11l11111ll1l1l1ll1, ll1l11ll11ll1ll11l11111ll1l1l1ll111);
            }
            catch(Exception e1)
            {
                e1.printStackTrace();
                throw new SQLException(e1.getMessage());
            }
        }
        catch(Exception e)
        {
        	log.error("DBTUtil.selectMutliple error:" + e);
        	log.error("Error SQL:"+ll1llll11l1l11ll1ll11l11111ll1l1l1ll1);
            throw new SQLException("DBTUtil.selectMutliple error:" + e);
        }
        return l111l11l1llll111ll1l1l1l1ll111;
    }

    
    private DBRow selectPreSingle(String ll1llll11l1l11ll1ll11l11111ll1l1l1ll1,DBRow ll1llll11l1l11ll1ll11l11111ll1l1l1ll11,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
	    throws SQLException
	{
	    DBRow ll1llll111l11ll1ll11l11111ll1l1l1ll1[] = selectPreMutliple(ll1llll11l1l11ll1ll11l11111ll1l1l1ll1,ll1llll11l1l11ll1ll11l11111ll1l1l1ll11,null,ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
	    if(ll1llll111l11ll1ll11l11111ll1l1l1ll1 == null || ll1llll111l11ll1ll11l11111ll1l1l1ll1.length == 0)
	        return(null);
	    if(ll1llll111l11ll1ll11l11111ll1l1l1ll1.length > 1)
	    {
	    	log.error("DBTUtil.selectSingle(prepare) error:Select Multiple data but only one is ok");
	    	log.error("Error SQL:"+ll1llll11l1l11ll1ll11l11111ll1l1l1ll1);
	    	throw new SQLException("DBTUtil.selectSingle(prepare) error:Select Multiple data but only one is ok");
	    }           
	    else
	    {
	        return ll1llll111l11ll1ll11l11111ll1l1l1ll1[0];        	
	    }
	}
    
    private DBRow[] selectPreMutliple(String ll1llll11l1l11ll1ll11l11111ll1l1l1ll1,DBRow ll1llll11l1l11ll1ll11l11111ll1l1l1ll11,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
	    throws SQLException
	{
	    return selectPreMutliple(ll1llll11l1l11ll1ll11l11111ll1l1l1ll1,ll1llll11l1l11ll1ll11l11111ll1l1l1ll11,null,ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
	}

    private DBRow[] selectPreMutliple(String ll1llll11l1l11ll1ll11l11111ll1l1l1ll1,DBRow ll1llll11l1l11ll1ll11l11111ll1l1l1ll11,PageCtrl ll1l11ll11ll1ll11l11111ll1l1l1ll111,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
	    throws SQLException
	{
    	ArrayList ll11l11ll111ll11ll1ll1ll1l1l1ll1 = ll1llll11l1l11ll1ll11l11111ll1l1l1ll11.getFieldNames();
    	if ( ll11l11ll111ll11ll1ll1ll1l1l1ll1==null||ll11l11ll111ll11ll1ll1ll1l1l1ll1.size()==0 )
    	{
    		return(null);
    	}
    	int ll11l1111l1l11l1l11ll1l1llll111ll1l1l1l1ll11 = ll11l11ll111ll11ll1ll1ll1l1l1ll1.size();
    	
    	
	    DBRow l111l11l1llll111ll1l1l1l1ll111[] = null;
	    if( (ll1llll11l1l11ll1ll11l11111ll1l1l1ll1==null) || (ll1llll11l1l11ll1ll11l11111ll1l1l1ll1.length() == 0) )
	    {
	    	return(null);
	    }	        
	
	    try
	    {
	        PreparedStatement ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11 = null;
	        ResultSet ll1l11ll11ll1ll11l11111ll1l1l1ll1 = null;
            ll11l1l1l1llll111ll1l1l1l1ll11 = ((Transaction)ll1llll11ll11ll1ll11l11111ll1l1l1ll1).getConnection();
            ((Transaction)ll1llll11ll11ll1ll11l11111ll1l1l1ll1).commit();
	        try
	        {
	        	ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11 = ll11l1l1l1llll111ll1l1l1l1ll11.prepareStatement(ll1llll11l1l11ll1ll11l11111ll1l1l1ll1);
	        	for(int l11l1111l11l1llll111ll1l1l1l1ll111 = 0; l11l1111l11l1llll111ll1l1l1l1ll111 < ll11l1111l1l11l1l11ll1l1llll111ll1l1l1l1ll11; l11l1111l11l1llll111ll1l1l1l1ll111++)
	        	{
                    String ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1 = (String)ll11l11ll111ll11ll1ll1ll1l1l1ll1.get(l11l1111l11l1llll111ll1l1l1l1ll111);
                    DBObjectType ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1 = ll1llll11l1l11ll1ll11l11111ll1l1l1ll11.getField(ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1);
                    
                    if(ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1 != null)
                    {
                        if(ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.getFieldType() == 4)
                        {
//                            byte ll1l11ll1l1l1ll11ll1111l1l1l1l11111l1l1l1ll111l1l1l1l1[] = null;
//                            ByteArrayOutputStream ll1l11ll1l1l1ll11ll1111l1ll11111l1l1l1ll111l1l1l1l1 = new ByteArrayOutputStream();
//                            ll1l11ll1l1l1ll11ll1111l1l1l1l11111l1l1l1ll111l1l1l1l1 = ll1l11ll1l1l1ll11ll1111l1ll11111l1l1l1ll111l1l1l1l1.toByteArray();
//                            try
//                            {
//                                if(ll1l11ll1l1l1ll11ll1111l1ll11111l1l1l1ll111l1l1l1l1 != null)
//                                {
//                                    ll1l11ll1l1l1ll11ll1111l1ll11111l1l1l1ll111l1l1l1l1.close();
//                                }
//                            }
//                            catch(IOException e){}
//
//                            ByteArrayInputStream ll1l11ll1l1l1ll11ll1111l1111111l1l1ll111l1l1l1l1 = new ByteArrayInputStream(ll1l11ll1l1l1ll11ll1111l1l1l1l11111l1l1l1ll111l1l1l1l1);
//                            ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.setBinaryStream(l11l1111l11l1llll111ll1l1l1l1ll111 + 1, ll1l11ll1l1l1ll11ll1111l1111111l1l1ll111l1l1l1l1, ll1l11ll1l1l1ll11ll1111l1l1l1l11111l1l1l1ll111l1l1l1l1.length);
                        	log.error("DBTUtil.selectPreMutliple(ll1llll11l1l11ll1ll11l11111ll1l1l1ll11) error:ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.getFieldType()=4!");
                        }
                        else
                        {
							ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.setObject(l11l1111l11l1llll111ll1l1l1l1ll111 + 1,ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.getFieldValue());
                        }
                    }
                    else
                    {
//                        ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.setObject(l11l1111l11l1llll111ll1l1l1l1ll111, null);
                    	log.error("DBTUtil.selectPreMutliple(ll1llll11l1l11ll1ll11l11111ll1l1l1ll11) error:ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1=null!");
                    }
	        	}        	
	            ll1l11ll11ll1ll11l11111ll1l1l1ll1 = ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.executeQuery();
	            l111l11l1llll111ll1l1l1l1ll111 = l11l1111l1l1111ll1l1llll111ll1l1l1l1ll111(ll1l11ll11ll1ll11l11111ll1l1l1ll1, ll1l11ll11ll1ll11l11111ll1l1l1ll111);
	        }
	        catch(Exception e1)
	        {
		    	log.error("DBTUtil.selectMutliple(prepare) error:" + e1);
		    	log.error("Error SQL:"+ll1llll11l1l11ll1ll11l11111ll1l1l1ll1);
		        throw new SQLException("DBTUtil.selectMutliple(prepare) error:" + e1);
	        }
	    }
	    catch(Exception e)
	    {
	    	log.error("DBTUtil.selectMutliple(prepare) error:" + e);
	    	log.error("Error SQL:"+ll1llll11l1l11ll1ll11l11111ll1l1l1ll1);
	        throw new SQLException("DBTUtil.selectMutliple(prepare) error:" + e);
	    }
	    return l111l11l1llll111ll1l1l1l1ll111;
	}

    /*
     *
     *@param
     *@return
     */

    private DBRow[] select(String ll1l11ll11ll1ll11l11111ll1l1l1ll111ll, String ll1l11ll11ll1ll11l11111ll1l1l1ll111ll1, PageCtrl ll1l11ll11ll1ll11l11111ll1l1l1ll111,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
        throws SQLException
    {
        String orderByStr = "order by";
        if(isBlank(ll1l11ll11ll1ll11l11111ll1l1l1ll111ll1))
            return selectMutliple("select * from " + ll1l11ll11ll1ll11l11111ll1l1l1ll111ll, ll1l11ll11ll1ll11l11111ll1l1l1ll111,ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
        if(ll1l11ll11ll1ll11l11111ll1l1l1ll111ll1.length() > orderByStr.length())
        {
            String head = ll1l11ll11ll1ll11l11111ll1l1l1ll111ll1.substring(0, orderByStr.length());
            if(head.equalsIgnoreCase(orderByStr))
                return selectMutliple("select * from " + ll1l11ll11ll1ll11l11111ll1l1l1ll111ll + " " + ll1l11ll11ll1ll11l11111ll1l1l1ll111ll1, ll1l11ll11ll1ll11l11111ll1l1l1ll111);
        }
        return selectMutliple("select * from " + ll1l11ll11ll1ll11l11111ll1l1l1ll111ll + " " + orderByStr + " " + ll1l11ll11ll1ll11l11111ll1l1l1ll111ll1, ll1l11ll11ll1ll11l11111ll1l1l1ll111,ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
    }

    private DBRow[] select(String ll1l11ll11ll1ll11l11111ll1l1l1ll111ll, String ll1l11ll11ll1ll11l11111ll1l1l1ll111ll1,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
        throws SQLException
    {
        return select(ll1l11ll11ll1ll11l11111ll1l1l1ll111ll, ll1l11ll11ll1ll11l11111ll1l1l1ll111ll1, null,ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
    }


    private DBRow[] select(String ll1l11ll11ll1ll11l11111ll1l1l1ll111ll,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
        throws SQLException
    {
        return select(ll1l11ll11ll1ll11l11111ll1l1l1ll111ll, null, null,ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
    }

    private DBObjectType[] ll1l11ll1l1l1ll11ll1111l1l1l1l111l1l1l111l1l(ResultSet ll1l11ll11ll1ll11l11111ll1l1l1ll1)
        throws Exception
      {
          if(ll1l11ll11ll1ll11l11111ll1l1l1ll1 == null)
              return null;
          DBObjectType ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l1l111l1l1l1ll1[] = null;
          try
          {
              ResultSetMetaData ll1l1111ll11ll1ll11l11111ll1l1l1ll111 = ll1l11ll11ll1ll11l11111ll1l1l1ll1.getMetaData();
              int l1111l11ll11l1llll111ll1l1l1l1ll111 = ll1l1111ll11ll1ll11l11111ll1l1l1ll111.getColumnCount();
              ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l1l111l1l1l1ll1 = new DBObjectType[l1111l11ll11l1llll111ll1l1l1l1ll111];
              for(int i = 1; i <= l1111l11ll11l1llll111ll1l1l1l1ll111; i++)
              {
                  DBObjectType ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1 = new DBObjectType();
                  ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.setFieldName(ll1l1111ll11ll1ll11l11111ll1l1l1ll111.getColumnName(i));
                  int ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l = ll1l1111ll11ll1ll11l11111ll1l1l1ll111.getColumnType(i);
                  ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.setFieldType(ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l1l11(ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l));
                  ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l1l111l1l1l1ll1[i - 1] = ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1;
              }

          }
          catch(SQLException e)
          {
          	  log.error("DBTUtil.ll1l11ll1l1l1ll11ll1111l1l1l1l111l1l1l111l1l error:not get metadata" + e);
              throw new Exception("DBTUtil.ll1l11ll1l1l1ll11ll1111l1l1l1l111l1l1l111l1l error:not get metadata" + e);
          }
          return ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l1l111l1l1l1ll1;
      }


    public DBObjectType[] ll1l11ll1l1l1ll11ll1111l1l1l1l111l1l1l111l1l(String ll11l1l1l1llll111ll1l1l1l1ll1,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
        throws Exception
      {
          if(ll11l1l1l1llll111ll1l1l1l1ll1 == null)
          {
              return null;
          }
          DBObjectType ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l1l111l1l1l1ll1[] = null;
          
          ResultSet ll1l11ll11ll1ll11l11111ll1l1l1ll1 = null;
          Statement stmt = null;
          try
          {
              this.ll11l1l1l1llll111ll1l1l1l1ll11 = ((Transaction)ll1llll11ll11ll1ll11l11111ll1l1l1ll1).getConnection();
              stmt = ll11l1l1l1llll111ll1l1l1l1ll11.createStatement();
              String sql = "select * from " + ll11l1l1l1llll111ll1l1l1l1ll1;
              ll1l11ll11ll1ll11l11111ll1l1l1ll1 = stmt.executeQuery(sql);

              if( ll1l11ll11ll1ll11l11111ll1l1l1ll1 == null )
              {
                   return null;
              }

              ResultSetMetaData ll1l1111ll11ll1ll11l11111ll1l1l1ll111 = ll1l11ll11ll1ll11l11111ll1l1l1ll1.getMetaData();
              int l1111l11ll11l1llll111ll1l1l1l1ll111 = ll1l1111ll11ll1ll11l11111ll1l1l1ll111.getColumnCount();
              ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l1l111l1l1l1ll1 = new DBObjectType[l1111l11ll11l1llll111ll1l1l1l1ll111];
              for(int i = 1; i <= l1111l11ll11l1llll111ll1l1l1l1ll111; i++)
              {
                  DBObjectType ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1 = new DBObjectType();
                  ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.setFieldName(ll1l1111ll11ll1ll11l11111ll1l1l1ll111.getColumnName(i));
                  int ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l = ll1l1111ll11ll1ll11l11111ll1l1l1ll111.getColumnType(i);

                  ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1.setFieldType( ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l );
                  ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l1l111l1l1l1ll1[i - 1] = ll1l11ll1l1l1ll11ll1111l1l1l1l1111l1l1l1ll111l1l1l1l1;
              }
          }
          catch(SQLException e)
          {
          	  log.error("DBTUtil.ll1l11ll1l1l1ll11ll1111l1l1l1l111l1l1l111l1l error:not get metadata" + e);
              throw new Exception("not get metadata", e);
          }
          
          return ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l1l111l1l1l1ll1;
      }

    private int ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l1l11(int ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l)
       {
           int l1ll1l11l1ll11ll1111l1l1l1l111l1 = -1;
           switch(ll1l11ll1l11l1ll11ll1111l1l1l1l111l1l)
           {
           case -7:
           case -6:
           case -5:
           case 2: 
           case 3: 
           case 4: 
           case 5: 
           case 6:
           case 7: 
           case 8:
               l1ll1l11l1ll11ll1111l1l1l1l111l1 = 1;
               break;

           case -1:
           case 1: 
           case 12: 
               l1ll1l11l1ll11ll1111l1l1l1l111l1 = 2;
               break;

           case 91:
           case 92: 
           case 93:
               l1ll1l11l1ll11ll1111l1l1l1l111l1 = 8;
               break;

           case -4:
           case -3:
           case -2:
           case 1111:
           case 2000:
           case 2003:
           case 2004:
           case 2005:
               l1ll1l11l1ll11ll1111l1l1l1l111l1 = 4;
               break;

           case 0: 
           case 16:
           case 2001:
           case 2002:
           case 2006:
               l1ll1l11l1ll11ll1111l1l1l1l111l1 = -1;
               break;
           }
           return l1ll1l11l1ll11ll1111l1l1l1l111l1;
       }



    public DBRow l1ll1l11l1ll11ll1111l1l1l1l(HashMap hashMap, String ll11l1l1l1llll111ll1l1l1l1ll1,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
        throws Exception
    {
        if( hashMap == null )
        {
            return new DBRow();
        }
        else
        {
            DBObjectType[] l111l11l1llll11111ll1l1l1l1ll1111l1l1l1l1l = ll1l11ll1l1l1ll11ll1111l1l1l1l111l1l1l111l1l( ll11l1l1l1llll111ll1l1l1l1ll1,ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
            DBRow ll111ll11ll1ll111ll1l1l1ll1 =new DBRow();
            Iterator iterator = hashMap.keySet().iterator();
            while( iterator.hasNext() )
            {
                String key = (String)iterator.next();
                int type = this.l1ll1l11l1ll11ll1111l1l1l1l111l1l1l1l111l1l1l1lll11lll(key, l111l11l1llll11111ll1l1l1l1ll1111l1l1l1l1l );
                switch(type)
                {
                    case 1: // int
                        ll111ll11ll1ll111ll1l1l1ll1.add(key,
                           Integer.parseInt(String.valueOf(hashMap.get(key))));
                        break;
                    case 2:  //long
                        ll111ll11ll1ll111ll1l1l1ll1.add(key,
                           Long.parseLong(String.valueOf(hashMap.get(key))));
                        break;

                    case 3: //float
                        ll111ll11ll1ll111ll1l1l1ll1.add(key,
                           Long.parseLong(String.valueOf(hashMap.get(key))));
                        break;

                    case 4: //double
                        ll111ll11ll1ll111ll1l1l1ll1.add(key,
                           Long.parseLong(String.valueOf(hashMap.get(key))));
                        break;
                    case 5: //String
                        ll111ll11ll1ll111ll1l1l1ll1.add(key,
                           Long.parseLong(String.valueOf(hashMap.get(key))));
                        break;
                    case 6: // object
                        ll111ll11ll1ll111ll1l1l1ll1.add(key, hashMap.get(key) );
                        break;
                }

            }

            return ll111ll11ll1ll111ll1l1l1ll1;
        }
    }

    private int l1ll1l11l1ll11ll1111l1l1l1l111l1l1l1l111l1l1l1lll11lll(String fieldName, DBObjectType[] l111l11l1llll11111ll1l1l1l1ll1111l1l1l1l1l)
    {
        int FieldType = 0;
        if( (fieldName == null ) || (l111l11l1llll11111ll1l1l1l1ll1111l1l1l1l1l == null) )
        {
            return FieldType;
        }

        if( l111l11l1llll11111ll1l1l1l1ll1111l1l1l1l1l.length == 0 )
        {
            return FieldType;
        }

        int size = l111l11l1llll11111ll1l1l1l1ll1111l1l1l1l1l.length;

        for( int i = 0; i < size; i ++ )
        {
            if( l111l11l1llll11111ll1l1l1l1ll1111l1l1l1l1l[i].getFieldName().equals( fieldName ) )
            {
                FieldType = l111l11l1llll11111ll1l1l1l1ll1111l1l1l1l1l[i].getFieldType();
                i = size;
            }
        }

        return FieldType;

    }
    
    public void update(String ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll111,String ll11l1111l1l11l1111ll1l1llll111ll1l1l1l1ll111,String l11ll1l1l1ll11ll1111l1l1l11l1l1l111l1l1l,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
    	throws Exception
    {
		PreparedStatement ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11 = null;
		String sql=null;
		
		try
		{
			sql = "update "+l11ll1l1l1ll11ll1111l1l1l11l1l1l111l1l1l+" set "+ll11l1111l1l11l1111ll1l1llll111ll1l1l1l1ll111+" " + ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll111;
			ll11l1l1l1llll111ll1l1l1l1ll11 = ((Transaction)ll1llll11ll11ll1ll11l11111ll1l1l1ll1).getConnection();
			ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11 = ll11l1l1l1llll111ll1l1l1l1ll11.prepareStatement(sql);
			ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.executeUpdate();
			if(ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11!=null)
			{
				ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll11.close();
			}
			
		}
		catch (Exception e)
		{
			log.error("DBTUtil.update error:" + e);
			throw new Exception("DBTUtil.update error:" + e);
		}
    }
    
    /**
     * �õ�������ݿ��еı�?
     * @return
     * @throws Exception
     */
	public ArrayList ll1llll11ll1111ll1ll11l11111ll1l1l1ll1(Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
		throws Exception
	{
		try
		{
			ArrayList tablesAl = new ArrayList(); 
			
			ll11l1l1l1llll111ll1l1l1l1ll11 = ((Transaction)ll1llll11ll11ll1ll11l11111ll1l1l1ll1).getConnection();
			DatabaseMetaData ll1llll11ll11l11ll1ll11l11111ll1l1l1ll1 = ll11l1l1l1llll111ll1l1l1l1ll11.getMetaData();
			ResultSet ll1l11ll11ll1ll11l11111ll1l1l1ll1 = ll1llll11ll11l11ll1ll11l11111ll1l1l1ll1.getTables("","","",null);
			while ( ll1l11ll11ll1ll11l11111ll1l1l1ll1.next() )
			{
				tablesAl.add(ll1l11ll11ll1ll11l11111ll1l1l1ll1.getString("TABLE_NAME"));
			}			
				
			return(tablesAl);			
		}
		catch (Exception e)
		{
			log.error("DBTUtil.getTableNames error:" + e);
			throw new Exception("DBTUtil.getTableNames error:" + e);
		}

	}
	
//	public void createTable(Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
//		throws Exception
//	{
//		ArrayList tablesAl = getTableNames(ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
//		for ( int i=0; i<tablesAl.size(); i++ )
//		{
//			update("drop l11ll1l1l1ll11ll1111l1l1l11l1l1l111l1l1l " + tablesAl.get(i) + ";",ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
//		}
//	}
	
	/**
	 * get next id
	 * @param tablename
	 * @return
	 * @throws Exception
	 */
	public long getSequance(String tablename,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
		throws Exception
	{
		synchronized(tablename)
		{
			if ( sequanceIsExist(tablename,ll1llll11ll11ll1ll11l11111ll1l1l1ll1) )
			{
				return(increaseSequance(tablename,ll1llll11ll11ll1ll11l11111ll1l1l1ll1));
			}
			else
			{
				return(createSequance(tablename,ll1llll11ll11ll1ll11l11111ll1l1l1ll1));
			}	
		}
	}

	private boolean sequanceIsExist(String tablename,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
		throws Exception
	{
		try
		{
			DBRow ll111ll11ll1ll111ll1l1l1ll1 = selectSingle("select * from " + ll11l11l1l11ll1l1llll111ll1l1l1l1ll11.getSeqTableName() + " ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll111 tablename='" + tablename + "'",ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
			if ( ll111ll11ll1ll111ll1l1l1ll1 != null )
			{
				return(true);
			}
			else
			{
				return(false);
			}
		}
		catch (SQLException e)
		{
			log.error("DBTUtil.sequanceIsExist error:" + e);
			throw new Exception("DBTUtil.sequanceIsExist error:" + e);
		}
	}
	
	/**
	 * ��sequance��һ
	 * @param tablename
	 * @return
	 * @throws Exception
	 */
	private long increaseSequance(String tablename,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
		throws Exception
	{
		try
		{
			//update("update " + ll11l11l1l11ll1l1llll111ll1l1l1l1ll11.getSeqTableName() + " set seq=seq+1 ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll111 tablename='" + tablename + "'",ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
			update("ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll111 tablename='" + tablename + "'","seq=seq+1",ll11l11l1l11ll1l1llll111ll1l1l1l1ll11.getSeqTableName(),ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
			DBRow ll111ll11ll1ll111ll1l1l1ll1 = selectSingle("select * from " + ll11l11l1l11ll1l1llll111ll1l1l1l1ll11.getSeqTableName() + " ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll111 tablename='" + tablename + "'",ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
			return(ll111ll11ll1ll111ll1l1l1ll1.get("seq",0));
		}
		catch (Exception e)
		{
			log.error("DBTUtil.increaseSequance error:" + e);
			throw new Exception("DBTUtil.increaseSequance error:" + e);
		}
	}
	
	/**
	 * �½�һ��sequance
	 * @param tablename
	 * @return
	 * @throws Exception
	 */
	private long createSequance(String tablename,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
		throws Exception
	{
		try
		{
			DBRow ll111ll11ll1ll111ll1l1l1ll1 = new DBRow();
			ll111ll11ll1ll111ll1l1l1ll1.add("tablename",tablename);
			ll111ll11ll1ll111ll1l1l1ll1.add("seq",ll11l11l1l11ll1l1llll111ll1l1l1l1ll11.getInitSeqValue());
			insert(ll11l11l1l11ll1l1llll111ll1l1l1l1ll11.getSeqTableName(),ll111ll11ll1ll111ll1l1l1ll1,ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
			return(ll11l11l1l11ll1l1llll111ll1l1l1l1ll11.getInitSeqValue());
		}
		catch (Exception e)
		{
			log.error("DBTUtil.createSequance error:" + e);
			throw new Exception("DBTUtil.createSequance error:" + e);
		}
	}

	public String getDatabaseType()
		throws Exception
	{
		String databaseType = "";
		try
		{			
			ll11l1l1l1llll111ll1l1l1l1ll11 = ll11l1111l1l1l1l1l11ll1l1llll111ll1l1l1l1ll11.getConnection();
			DatabaseMetaData ll1llll11ll11l11ll1ll11l11111ll1l1l1ll1 = ll11l1l1l1llll111ll1l1l1l1ll11.getMetaData();
			databaseType = ll1llll11ll11l11ll1ll11l11111ll1l1l1ll1.getDatabaseProductName();
			return(databaseType);
		}
		catch (Exception e)
		{
			log.error("DBTUtil.getDatabaseType error:" + e);
			throw new Exception("DBTUtil.getDatabaseType error:" + e);
		}
	}
	
	/**
	 * ����ֶ�ֵ�Ƿ�Ϊ���ڸ��?
	 * �����oracle�������⴦��
	 * @return
	 */
	private boolean isDataRule(String val)
	{
		String t[] = val.split(" ");
		String ymd[] = t[0].split("-");
		
		if ( (ymd.length != 3) || (ymd[0].length() != 4) || (ymd[1].length() != 2) || (ymd[2].length() != 2) )
		{
			return(false);
		}

		try
		{
			Integer.parseInt(ymd[0]);
		}
		catch (NumberFormatException e)
		{
			return(false);
		}
		try
		{
			Integer.parseInt(ymd[1]);
		}
		catch (NumberFormatException e)
		{
			return(false);
		}
		try
		{
			Integer.parseInt(ymd[2]);
		}
		catch (NumberFormatException e)
		{
			return(false);
		}
		
		return(true);
	}
	
    /*
    *
    *@param
    *@return
    */
  private boolean isBlank(String str)
   {
       if( (str == null) || (str.trim().length() == 0))
       {
           return true;
       }
       else
       {
           return false;
       }
   }
  
	public void increaseFieldVal(String l11ll1l1l1ll11ll1111l1l1l11l1l1l111l1l1l,String ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll111,String ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1,int val,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
		throws Exception
	{
		try 
		{
			//String sql = "update "+l11ll1l1l1ll11ll1111l1l1l11l1l1l111l1l1l+" set "+ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1+"="+ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1+"+"+val+" "+ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll111;
			update(ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll111,ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1+"="+ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1+"+"+val,l11ll1l1l1ll11ll1111l1l1l11l1l1l111l1l1l,ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
		} 
		catch (Exception e) 
		{
			log.error("DBTUtil.increaseFieldVal error:" + e);
			throw new Exception("DBTUtil.increaseFieldVal error:" + e);
		}
	}
	
	public void decreaseFieldVal(String l11ll1l1l1ll11ll1111l1l1l11l1l1l111l1l1l,String ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll111,String ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1,int val,Object ll1llll11ll11ll1ll11l11111ll1l1l1ll1)
		throws Exception
	{
		try
		{
			//String sql = "update "+l11ll1l1l1ll11ll1111l1l1l11l1l1l111l1l1l+" set "+ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1+"="+ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1+"-"+val+" "+ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll111;
			update(ll11l1111l1l11l111ll1l1llll111ll1l1l1l1ll111,ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1+"="+ll11l11ll111ll11ll1ll1ll1l1l1ll1111l1l1l1l1ll1l1l1+"-"+val,l11ll1l1l1ll11ll1111l1l1l11l1l1l111l1l1l,ll1llll11ll11ll1ll11l11111ll1l1l1ll1);
		}
		catch (Exception e)
		{
			log.error("DBTUtil.decreaseFieldVal error:" + e);
			throw new Exception("DBTUtil.decreaseFieldVal error:" + e);
		}
	}

}











