
package com.cwc.db;

import com.cwc.db.DBConnectionManager;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;

import java.util.Vector;

/**
 * 没有使用
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class DBHandler{

    private DBConnectionManager connMgr;
    private Connection conn;
    private Vector v_stmt = new Vector();
    private Vector v_rs = new Vector();

    public DBHandler() throws Exception
    {
            connMgr = DBConnectionManager.getInstance();

            this.conn = connMgr.getConnection();

    }

  /**
   * get Connection
   */
    public Connection getConnection()
    {
 		return this.conn;
 	}

    /*
     *get Statement
     *@return
     */

    public Statement getStatement()
    {
        try
        {
            return this.conn.createStatement();
        }
        catch(SQLException e)
        {
            return null;
        }
    }

    /*
     *set commit mode
     *@param flag
     *@return
     */

    public void setAutoCommit(boolean flag) throws SQLException
    {
    	this.getConnection().setAutoCommit(flag);
    }

    /*
     * get commid mode
     *@param
     *@return boolean
     */

    public boolean getAutoCommit() throws SQLException
    {
     	return this.getConnection().getAutoCommit();
    }


    /*
     *
     *@param
     *@return
     */

     public void commit() throws SQLException
     {
     	if( this.getAutoCommit() )
        {
             throw new SQLException("now is autocommit mode!");
        }
     	else
        {
             this.getConnection().commit();
        }
     }

     /*
      *
      *@param
      *@return
      */


     public void rollback() throws SQLException
     {
     	if( this.getAutoCommit() )
         {
             throw new SQLException("now is autocommit mode!");
         }
     	else
         {
             this.getConnection().rollback();
         }
     }


    /*
     *executeQuery sql
     *@param sql
     *@return ResultSet
     */

    public ResultSet executeQuery( String sql ) throws SQLException
    {
   		this.checkSql(sql);

	    ResultSet rs = null;

	    Statement stmt = this.conn.createStatement();

        rs = stmt.executeQuery( sql );

        this.v_rs.add(rs);

        this.v_stmt.add(stmt);

	    return rs;

   }


    /*
     *executeUpdate sql
     *@param  sql
     *@return  int
     */

     public int executeUpdate(String sql) throws SQLException
     {
	    this.checkSql(sql);

		Statement stmt = this.conn.createStatement();
	    int affectRow = stmt.executeUpdate(sql);

        this.v_stmt.add( stmt );

	    return affectRow;

  	}


	/*
	 *check sql is null
	 *@param sql
	 *@return
	 */

	 private void checkSql(String sql) throws SQLException
    {
		if ( ( sql == null ) || ( sql.length() == 0 ))
        {
		    throw new SQLException(" Error:sql is null");
        }
	}


	/*
	 *close all connection,stmt,rs
	 *@param
	 *@return
	 */

	public void close()
    {
		try
        {
            int i = 1;

            if( this.v_rs != null )
            {
                for( i = 1; i < this.v_rs.size(); i ++ )
                {
                    ((ResultSet)this.v_rs.get(i)).close();
                    v_rs.remove(i);
                }
            }

            if( this.v_stmt != null)
            {
                for( i = 1; i < this.v_stmt.size(); i ++ )
                {
                    ((Statement)this.v_stmt.get(i)).close();
                    v_stmt.remove(i);
                }
            }
            connMgr.close(conn);
        }
        catch(SQLException e)
        {
            e.printStackTrace();
		}
	}


 }