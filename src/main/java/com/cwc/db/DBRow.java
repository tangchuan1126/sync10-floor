package com.cwc.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 结果集封装类
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class DBRow
    implements Serializable,Cloneable,Map<String,Object>
{
	enum BooleanFinal { 
        FALSE_UNSET(false), 
        TRUE_UNSET(true), 
        FALSE(false), 
        TRUE(true);
        
        private boolean trueOrFalse;
        BooleanFinal(boolean tf){ this.trueOrFalse = tf; }
        public boolean bool() { return this.trueOrFalse; }
    }
	private static  BooleanFinal keyUseUpper = BooleanFinal.TRUE_UNSET;
    public static  boolean getKeyUseUpper() { return keyUseUpper.bool(); }
    public static  void setKeyUseUpper(boolean tf) { 
        switch(keyUseUpper) {
            case FALSE_UNSET: 
            case TRUE_UNSET: keyUseUpper = tf ? BooleanFinal.TRUE : BooleanFinal.FALSE; break;
            default: throw new UnsupportedOperationException("keyUseUpper开关不能二次赋值！");
        }
    }
    private static  BooleanFinal keyIgnoreCase = BooleanFinal.FALSE_UNSET;
    public static  boolean getKeyIgnoreCase() { return keyIgnoreCase.bool(); }
    public static  void setKeyIgnoreCase(boolean tf) { 
    	switch(keyIgnoreCase) {
	        case FALSE_UNSET: 
	        case TRUE_UNSET: keyIgnoreCase = tf ? BooleanFinal.TRUE : BooleanFinal.FALSE; break;
	        default: throw new UnsupportedOperationException("keyIgnoreCase开关不能二次赋值！");
	    }
    }
	
    private HashMap fieldValues = null;		//字段值，存放dbfield
    private ArrayList fieldNms = null;			//字段名

    private void init()
    {
		fieldValues = new HashMap();
		fieldNms = new ArrayList();
    }
    
    public DBRow()
    {
    	init();
    }

	/**
	 * 加入一个DBFIEL
	 * @param dbField
	 */
    void addField(DBObjectType dbField)
    {
        if(dbField == null) return;
        
        String fieldName = dbField.getFieldName();
        
        if(isBlank(fieldName)) return;
        
        fieldName = keyUseUpper.bool() ? fieldName.trim().toUpperCase() : fieldName.trim().toLowerCase(); //by Frank,  要么全大写、要么全小写

        if(fieldNms != null && fieldValues != null)
        {
            if(!existFiled(fieldName)) fieldNms.add(fieldName);
            fieldValues.put(fieldName, dbField);
        }
    }

    /**
     * 判断字段是否存在
     * @param fieldName
     * @return
     */
    private boolean existFiled(String fieldName)
    {
        if(fieldNms == null || isBlank(fieldName))
            return false;
        
        fieldName = fieldName.trim();
        
        if(keyUseUpper.bool())
        	return fieldNms.contains(fieldName.toUpperCase()) || 
        			(keyIgnoreCase.bool() && fieldNms.contains(fieldName)) ;
        
        else
        	return  fieldNms.contains(fieldName) || 
    			(keyIgnoreCase.bool() && 
    					(fieldNms.contains(fieldName.toLowerCase()) || fieldNms.contains(fieldName.toUpperCase()))) ;
        
        
    }

    /**
     * 清空dbrow
     *
     */
    public void clear()
    {
        if(fieldNms == null)
        {
            fieldNms = new ArrayList();        	
        }
        else 
        {
        	fieldNms.clear();
        }
        
        if(fieldValues == null)
        {
        	fieldValues = new HashMap();
        }
        else
        {
        	fieldValues.clear();
        }
    }
    
    /**
     * 销毁DBROW
     *
     */
    public void destroy()
    {
        if(fieldNms != null)
        {
        	fieldNms.clear();
        	fieldNms = null;
        }

        
        if(fieldValues != null)
	    {
        	fieldValues.clear();
        	fieldValues = null;
	    }

    }

    /**
     * 获得一个DBROW中所有字段名字
     * @return
     */
    public ArrayList getFieldNames()
    {
        return fieldNms;
    }

    /**
     * 获得一个DBFIELD
     * @param fieldNm
     * @return
     */
    DBObjectType getField(String fieldNm)
    {
        if(existFiled(fieldNm) && fieldValues != null)
        {
        	fieldNm = keyUseUpper.bool() ? fieldNm.trim().toUpperCase() : fieldNm.trim().toLowerCase(); //要么全大写，要么全小写，不允许混合
            DBObjectType v = (DBObjectType) fieldValues.get(fieldNm);
            if(v == null && keyIgnoreCase.bool() ) v = (DBObjectType) fieldValues.get(fieldNm.toLowerCase());
            if(v == null && keyIgnoreCase.bool() ) v = (DBObjectType) fieldValues.get(fieldNm.toUpperCase());
            return v;
        } else
        {
            return null;
        }
    }
    
    /**
     * 增加整型值对
     * @param fieldNm
     * @param v
     */
    private void addIntField(String fieldNm, int v)
    {
        addField(new DBObjectType(fieldNm, DBObjectType.DECIMAL, new Integer(v)));
    }

    /**
     * 增加长整型值对
     * @param fieldNm
     * @param v
     */
    private void addLongField(String fieldNm, long v)
    {
        addField(new DBObjectType(fieldNm, DBObjectType.DECIMAL, new Long(v)));
    }

    /**
     * 增加字符串值对
     * @param fieldNm
     * @param v
     */
    private void addStrField(String fieldNm, String v)
    {
        addField(new DBObjectType(fieldNm, DBObjectType.STRING, v));
    }

    /**
     * 增加浮点值对
     * @param fieldNm
     * @param v
     */
    private void addFloatField(String fieldNm, float v)
    {
        addField(new DBObjectType(fieldNm, DBObjectType.STRING, new Float(v)));
    }

    /**
     * 增加浮点值对
     * @param fieldNm
     * @param v
     */
    private void addDoubleField(String fieldNm, double v)
    {
        addField(new DBObjectType(fieldNm, DBObjectType.STRING, new Double(v)));
    }

    /**
     * 增加对象值对
     * @param fieldNm
     * @param v
     */
    private void addObjectField(String fieldNm, Object obj)
    {
    	int objectType;
    	if(obj instanceof String)
    	{
    		objectType = DBObjectType.STRING;
    	}
    	else if(obj instanceof Integer||obj instanceof Long ||obj instanceof Short||obj instanceof Double||obj instanceof Float)
    	{
    		objectType = DBObjectType.DECIMAL;
    	}
    	else if (obj instanceof java.util.Date||obj instanceof java.sql.Date) 
    	{
			objectType = DBObjectType.TIME;
		}
    	else
    	{
    		objectType =  DBObjectType.OBJECT;
    	}
        addField(new DBObjectType(fieldNm,objectType, obj));
    }
    
    /**
     * 增加DBRow对象值对
     * @param fieldNm
     * @param v
     */
    private void addDBRowObjectField(String fieldNm, DBRow obj)
    {
        addField(new DBObjectType(fieldNm, DBObjectType.DBROW_OBJECT, obj));
    }

    /**
     * 增加NULL值对
     * @param fieldNm
     * @param v
     */
    public void addNull(String fieldNm)
    {
        addField(new DBObjectType(fieldNm, DBObjectType.STRING, null));
    }

    /**
     * 获得一个对象值
     * @param fieldNm
     * @return
     * @throws Exception
     */
    public Object getValue(String fieldNm)
        throws Exception
    {
        DBObjectType df = getField(fieldNm);
        if(df != null)
            return df.getFieldValue();
        else
            return null;
    }

    /**
     * 获得字符串值
     * @param fieldNm
     * @return
     */
    public String getString(String fieldNm)
    {
        return getString(fieldNm, "");
    }

    /**
     * 获得字符串值，可设置默认返回值
     * @param fieldNm
     * @param defaulteV
     * @return
     */
    public String getString(String fieldNm, String defaulteV)
    {
        return fieldToString(fieldNm, defaulteV);
    }

    /**
     * 获得数值型值
     * @param fieldNm
     * @return
     * @throws Exception
     */
    private Object getDecimalValue(String fieldNm)
        throws Exception
    {
        DBObjectType df = getField(fieldNm);

        if(df == null || df.getFieldValue() == null) return null;
        
//        if(df.getFieldType() != DBObjectType.DECIMAL)
//        {
//            throw new Exception("data formatted error for serialize " + fieldNm + "(" + df.getFieldType() + ") to decimal");        	
//        }
//        else
//        {
			return df.getFieldValue();
//        }
    }

    /**
     * 获得int型值，可设置默认返回值
     * @param fieldNm
     * @param defaultV
     * @return
     * @throws Exception
     */
    private int getIntValue(String fieldNm, int defaultV)
        throws Exception
    {
        Object obj = getDecimalValue(fieldNm);
        if(obj == null)
            return defaultV;
        else
            return getIntFromStr(obj.toString(), defaultV);
    }

    /**
     * 获得long型值，可设置默认返回值
     * @param fieldNm
     * @param defaultV
     * @return
     * @throws Exception
     */
    private long getLongValue(String fieldNm, long defaultV)
        throws Exception
    {
        Object obj = getDecimalValue(fieldNm);
        if(obj == null)
            return defaultV;
        else
            return getLongFromStr(obj.toString(), defaultV);
    }

    /**
     * 获得float型值，可设置默认返回值
     * @param fieldNm
     * @param defaultV
     * @return
     * @throws Exception
     */
    private float getFloatValue(String fieldNm, float defaultV)
        throws Exception
    {
        Object obj = getDecimalValue(fieldNm);
        if(obj == null)
            return defaultV;
        else
            return getFloatFromStr(obj.toString(), defaultV);
    }

    /**
     * 获得boolean型值，可设置默认返回值
     * @param fieldNm
     * @param defaultV
     * @return
     * @throws Exception
     */
    private boolean getBooleanValue(String fieldNm, boolean defaultV)
        throws Exception
    {
        Object obj = getValue(fieldNm);
        if(obj == null)
            return defaultV;
        else
            return getBooleanFromStr(obj.toString(), defaultV);
    }

    /**
     * 把字段值转换成字符串
     * @param fieldNm
     * @return
     */
    public String fieldToString(String fieldNm)
    {
        return fieldToString(fieldNm, "");
    }

    /**
     * 把字段值转换成字符串，看设置默认返回值
     * @param fieldNm
     * @param defaultV
     * @return
     */
    public String fieldToString(String fieldNm, String defaultV)
    {
        try
        {
            DBObjectType df = getField(fieldNm);
            
            if(df == null) return defaultV;
            
            String val = df.toString();
            
            if(val == null) return defaultV;
            else  return val;
        }
        catch(Exception e)
        {
            return defaultV;
        }
    }

    /**
     * 获得double型值，可设置默认返回值
     * @param fieldNm
     * @param defaultV
     * @return
     * @throws Exception
     */
    private double getDoubleValue(String fieldNm, double defaultV)
        throws Exception
    {
        Object obj = getDecimalValue(fieldNm);
        if(obj == null)
            return defaultV;
        else
            return getDoubleFromStr(obj.toString(), defaultV);
    }

    /**
     * 获得String型值，可设置默认返回值
     * @param fieldNm
     * @param defaultV
     * @return
     * @throws Exception
     */
    private String getStrValue(String fieldNm, String defaultV)
        throws Exception
    {
        DBObjectType df = getField(fieldNm);
//        if(df.getFieldType() != DBObjectType.STRING)
//            throw new Exception("data formatted error for serialize "
//                    + fieldNm + "(" + df.getFieldType() + ") to String");
//        else
            return strValue(df.getFieldValue(), defaultV);
    }

    /**
     * 获得对象值
     * @param fieldNm
     * @return
     * @throws Exception
     */
    private Object getObjectValue(String fieldNm)
        throws Exception
    {
        DBObjectType df = getField(fieldNm);
        
        if(df == null)
            return null;
        Object obj = df.getFieldValue();
        return obj;
    }

    /**
     * 增加int型值对
     * @param fieldNm
     * @param v
     */
    public void add(String fieldNm, int v)
    {
        addIntField(fieldNm, v);
    }

    /**
     * 增加long型值对
     * @param fieldNm
     * @param v
     */
    public void add(String fieldNm, long v)
    {
    	addLongField(fieldNm, v);
    }

    /**
     * 增加String型值对
     * @param fieldNm
     * @param v
     */
    public void add(String fieldNm, String v)
    {
    	addStrField(fieldNm, v);
    }

    /**
     * 增加Object型值对
     * @param fieldNm
     * @param v
     */
    public void add(String fieldNm, Object v)
    {
    	addObjectField(fieldNm, v);
    }
    
    /**
     * 增加DBRow型值对
     * @param fieldNm
     * @param v
     */
//    public void add(String fieldNm, DBRow v)
//    {
//    	addObjectField(fieldNm, v);
//    }

    /**
     * 增加DBRow型值对
     * @param fieldNm
     * @param fieldValue
     */
    public  void addDBRowObject(String fieldNm, DBRow fieldValue)
    {
    	addDBRowObjectField(fieldNm, fieldValue);
    }
    
    /**
     * 增加Object型值对
     * @param fieldNm
     * @param fieldValue
     */
    public  void addObject(String fieldNm, Object fieldValue)
    {
    	addObjectField(fieldNm, fieldValue);
    }

    /**
     * 增加float型值对
     * @param fieldNm
     * @param v
     */
    public void add(String fieldNm, float v)
    {
    	addFloatField(fieldNm, v);
    }

    /**
     * 增加double型值对
     * @param fieldNm
     * @param v
     */
    public void add(String fieldNm, double v)
    {
    	addDoubleField(fieldNm, v);
    }

    /**
     * 获得int型值对
     * @param fieldNm
     * @param defaultV
     * @return
     */
    public int get(String fieldNm, int defaultV)
    {
        try
        {
            return getIntValue(fieldNm, defaultV);
        }catch(Exception e)
        {
            return defaultV;
        }
    }

    /**
     * 获得long型值对
     * @param fieldNm
     * @param defaultV
     * @return
     */
    public long get(String fieldNm, long defaultV)
    {
        try
        {
            return getLongValue(fieldNm, defaultV);
        }
        catch(Exception e)
        {
            return defaultV;
        }
    }

    /**
     * 获得float型值对
     * @param fieldNm
     * @param defaultV
     * @return
     */
    public float get(String fieldNm, float defaultV)
    {
        try
        {
            return getFloatValue(fieldNm, defaultV);
        }
        catch(Exception e)
        {
            return defaultV;
        }
    }

    /**
     * 获得double型值对
     * @param fieldNm
     * @param defaultV
     * @return
     */
    public double get(String fieldNm, double defaultV)
    {
        try
        {
            return getDoubleValue(fieldNm, defaultV);
        }
        catch(Exception e)
        {
            return defaultV;
        }
    }

    /**
     * 获得boolean型值对
     * @param fieldNm
     * @param defaultV
     * @return
     */
    public boolean get(String fieldNm, boolean defaultV)
    {
        try
        {
            return getBooleanValue(fieldNm, defaultV);
        }
        catch(Exception e)
        {
            return defaultV;
        }
    }

    /**
     * 获得String型值对
     * @param fieldNm
     * @param defaultV
     * @return
     */
    public String get(String fieldNm, String defaultV)
    {
        try
        {
            return getStrValue(fieldNm, defaultV);
        }catch(Exception e)
        {
            return defaultV;
        }
    }

    /**
     * 移除某个字段
     * @param fieldNm
     */
    public void remove(String fieldNm)
    {
    	fieldNm = keyUseUpper.bool() ? fieldNm.toUpperCase() : fieldNm.toLowerCase();  //要么全大写，要么全小写，不允许混合
        if(fieldNms == null) fieldNms = new ArrayList();
        else {
        	fieldNms.remove(fieldNm);
        	if(keyIgnoreCase.bool()) {
        		fieldNms.remove(fieldNm.toUpperCase());
        		fieldNms.remove(fieldNm.toLowerCase());
        	}
        }
        if(fieldValues == null) fieldValues = new HashMap();
        else {
        	fieldValues.remove(fieldNm);
        	if(keyIgnoreCase.bool()) {
        		fieldValues.remove(fieldNm.toUpperCase());
        		fieldValues.remove(fieldNm.toLowerCase());
        	}
        }
    }

    /**
     * 获得对象
     * @param fieldNm
     * @param defaultV
     * @return
     * @throws Exception
     */
    public Object get(String fieldNm, Object defaultV)
        throws Exception
    {
        Object obj = getObjectValue(fieldNm);
        if(obj == null)
            obj = defaultV;
        return obj;
    }

    /**
     * 判断字符串是否为空
     * @param str
     * @return
     */
    private boolean isBlank(String str)
    {
        if( (str == null) || (str.trim().length() == 0))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * 把字符串转换为int
     * @param str
     * @param def
     * @return
     */
    private int getIntFromStr(String str, int def)
    {
        try
        {
            int i = Integer.parseInt(str);
            return i;
        }
        catch(Exception e)
        {
            return def;
        }
    }

    /**
     * 把字符串转换为long
     * @param str
     * @param def
     * @return
     */
    private long getLongFromStr(String str, long def)
    {
        try
        {
            return Long.parseLong(str);
        }
        catch(Exception e)
        {
            return def;
        }
    }

    /**
     * 把字符串转换为float
     * @param str
     * @param def
     * @return
     */
   private float getFloatFromStr(String str, float def)
    {
        try
        {
            return Float.parseFloat(str);
        }
        catch(Exception e)
        {
            return def;
        }
    }

   /**
    * 把字符串转换为double
    * @param str
    * @param def
    * @return
    */
    private double getDoubleFromStr(String str, double def)
     {
         try
         {
             return Double.parseDouble(str);
         }
         catch(Exception e)
         {
             return def;
         }
     }

    /**
     * 把字符串转换为boolean
     * @param str
     * @param def
     * @return
     */
    private boolean getBooleanFromStr(String str, boolean def)
    {
        try
        {
            return Boolean.getBoolean(str);
        }
        catch(Exception e)
        {
            return def;
        }
    }

    /**
     * 把obj转换为string
     * @param obj
     * @param def
     * @return
     */
    private String strValue(Object obj,String def)
    {
        if( obj == null)
        {
            return def;
        }
        else
        {
            return obj.toString();
        }

    }
    
    /**
     * 把一个DBROW复制给另外一个DBROW
     * @param data
     * @return
     * @throws Exception
     */
    public DBRow append(DBRow data) 
    	throws Exception
    {
    	String fieldname;
		ArrayList al = data.getFieldNames();
		for (int i=0; i<al.size(); i++)
		{
			fieldname = al.get(i).toString();
			add( fieldname,data.get(fieldname,new Object()) );
			getField(fieldname).setFieldType(data.getField(fieldname).getFieldType());
		}
		
		return(this);
    }
    
    /**
     * 
     * @param fields
     * @param data
     * @return
     * @throws Exception
     */
    public DBRow append(String fields,DBRow data) 
		throws Exception
	{
    	ArrayList fieldAl = new ArrayList();
    	String field[] = fields.split(",");
    	for (int i=0; i<field.length; i++)
    	{
    		fieldAl.add(field[i]);
    	}
    	
		String inDataFieldName;
		ArrayList al = data.getFieldNames();
		for (int i=0; i<al.size(); i++)
		{
			inDataFieldName = al.get(i).toString();
			
			if ( fieldAl.contains(inDataFieldName.toLowerCase()) )
			{
				add( inDataFieldName,data.get(inDataFieldName,new Object()) );
				getField(inDataFieldName).setFieldType(data.getField(inDataFieldName).getFieldType());	
			}
		}
		return(this);
	}
    
    public Object clone()
    	throws CloneNotSupportedException
    {
    	DBRow newDBRow = (DBRow)super.clone();
    	
    	ArrayList fields = this.getFieldNames();
    	for (int i=0; i<fields.size(); i++)
    	{
    		newDBRow.add(fields.get(i).toString(), this.getString(fields.get(i).toString()));
    	}
    	
        return(newDBRow);
    }
    
    

    public static void main(String args[])
    	throws Exception
    {
    	DBRow r = new DBRow();
    	r.add("a","a");
    	
    	DBRow t = new DBRow();
    	t.add("b","b");
    	
    	DBRow tt = r.append(t);
    	
    	//System.out.println(tt.getString("b"));
    }

	@Override
	public int size() {
		return this.fieldNms.size();
	}

	@Override
	public boolean isEmpty() {
		return this.fieldNms.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		String k = (String)key;
		String ku = k.toUpperCase();
		String kl = k.toLowerCase();
		
		if(keyUseUpper.bool())
			return this.fieldNms.contains(ku) || ( keyIgnoreCase.bool() && this.fieldNms.contains(k) ) ;
		
		return this.fieldNms.contains(k) || ( keyIgnoreCase.bool() &&  (this.fieldNms.contains(ku) || this.fieldNms.contains(kl)) );
				
	}

	@Override
	public boolean containsValue(Object value) {
		return this.fieldValues.containsValue(value);
	}

	@Override
	public Object get(Object key) {
		try {
			return this.get((String)key, (Object)null);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Object put(String key, Object value) {
		this.add( key, value);
		return this.get(key);
	}

	@Override
	public Object remove(Object key) {
		Object v = this.get(key);
		this.remove((String)key);
		return v;
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> m) {
		for(Map.Entry e: m.entrySet()){
			String k = (String)e.getKey();
			Object v = e.getValue();
			this.put(k, v);
		}
	}

	@Override
	public Set<String> keySet() {
		return this.fieldValues.keySet();
	}

	@Override
	public Collection<Object> values() {
		List<Object> vs = new ArrayList<Object>();
		for(Object k: this.fieldNms){
			try {
				vs.add(this.getValue((String)k));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return vs;
	}

	@Override
	public Set<java.util.Map.Entry<String, Object>> entrySet() {
		Map<String,Object> m = new HashMap<String,Object>();
		for(Object k: this.fieldNms){
			try {
				m.put((String)k, this.getValue((String)k));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return m.entrySet();
	}

}

