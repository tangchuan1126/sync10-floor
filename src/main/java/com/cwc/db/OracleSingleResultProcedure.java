package com.cwc.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

public class OracleSingleResultProcedure
{
	private Connection con = null;
	private CallableStatement proc = null;
	private DBConnectionManager dbm = null;
	private DBRow result = new DBRow();
	private ArrayList resultIndex = new ArrayList();
	
	public OracleSingleResultProcedure(String procName)
		throws Exception
	{
		dbm = DBConnectionManager.getInstance();
		con = dbm.getConnection();
		
		proc = con.prepareCall(procName);		
	}
	
	
	public void setIntProcPara(int index,int val)
		throws SQLException
	{
		proc.setInt(index, val);
	}
	
	public void setLongProcPara(int index,long val)
		throws SQLException
	{
		proc.setLong(index, val);
	}
	
	public void setStringProcPara(int index,String val)
		throws SQLException
	{
		proc.setString(index, val);
	}
	
	public void setFloatProcPara(int index,float val)
		throws SQLException
	{
		proc.setFloat(index, val);
	}
	
	public void setDoubleProcPara(int index,double val)
		throws SQLException
	{
		proc.setDouble(index, val);
	}
	////////////////

	public void setStringOutProcPara(int index)
		throws SQLException
	{
		resultIndex.add(index);
		proc.registerOutParameter(index, Types.VARCHAR);
	}
	
	public void setIntOutProcPara(int index)
		throws SQLException
	{
		resultIndex.add(index);
		proc.registerOutParameter(index, Types.INTEGER);
	}
	
	
	//////////////
	
	public String getStringResult(int index)
		throws SQLException
	{
		return( result.getString( String.valueOf(index) ) );
	}
	
	public int getIntResult(int index)
		throws SQLException
	{
		return( Integer.parseInt(result.getString( String.valueOf(index) )) );
	}
	
	public void execute() 
		throws SQLException
	{
		try 
		{
			proc.execute();
			
			for (int i=0; i<resultIndex.size(); i++)
			{
				result.add(resultIndex.get(i).toString() , proc.getString( Integer.parseInt(resultIndex.get(i).toString()) ) );
			}
		} 
		catch (SQLException e) 
		{
			throw new SQLException(e.toString());
		}
		finally
		{
			release();
		}
	}
	
	public void release()
	{
		try
		{
			dbm.close(con);
			if (proc!=null)
			{
				proc.close();
				proc = null;
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
}

/**
 * 调用示例
 * 
 * 
 * 
 * 	public DBRow tbCustinfo(long ID,String name,String password,String PhPass,String Paypass,String mailaddress,String mobile,String reg_no,String vip_flag,String flag)
		throws Exception
	{
		try
		{
	        OracleSingleResultProcedure osrp = new OracleSingleResultProcedure("{ call TbCustinfo(?,?,?,?,?,?,?,?,?,?,?) }");
	        
	        osrp.setLongProcPara(1, ID);
	        osrp.setStringProcPara(2, name);
	        osrp.setStringProcPara(3, password);
	        osrp.setStringProcPara(4, PhPass);
	        osrp.setStringProcPara(5, Paypass);
	        osrp.setStringProcPara(6, mailaddress);
	        osrp.setStringProcPara(7, mobile);
	        osrp.setStringProcPara(8, reg_no);
	        osrp.setStringProcPara(9, vip_flag);
	        osrp.setStringProcPara(10, flag);
	        
	        osrp.setStringOutProcPara(11);
	        osrp.execute();
	        
	        DBRow result = new DBRow();
	        result.add("result",osrp.getStringResult(11));
	        
	        return( result );
		}
		catch (Exception e)
		{
			throw new Exception("tbCustinfo(row) error:" + e);
		}
	}
	
 */





