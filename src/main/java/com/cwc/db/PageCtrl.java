package com.cwc.db;

/**
 * 计算分页封装类
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class PageCtrl
{

    public PageCtrl()
    {
    }

    /**
     * 获得每页显示记录数
     * @return
     */
    public int getPageSize()
    {
        if(pageSize <= 0)
            return defaultPageSite;		//默认数
        else
            return pageSize;
    }

    /**
     * 设置每页显示记录数
     * @param pageSize
     */
    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }

    public int getPageNo()
    {
        if(pageNo <= 0)
        {
            return 1;
        }
        else
        {
            return pageNo;
        }

    }

    /**
     * 设置当前翻到第几页
     * @param pageNo
     */
    public void setPageNo(int pageNo)
    {
        this.pageNo = pageNo;
    }

    /**
     * 获得所有记录数
     * @return
     */
    public int getAllCount()
    {
        return allCount;
    }

    /**
     * 设置所有记录
     * @param allCount
     */
    public void setAllCount(int allCount)
    {
        this.allCount = allCount;
    }

    /**
     * 获得总页数
     * @return
     */
    public int getPageCount()
    {
        return pageCount;
    }

    /**
     * 设置总页数
     * @param pageCount
     */
    public void setPageCount(int pageCount)
    {
        this.pageCount = pageCount;
    }

    /**
     * 获得记录行数
     * @return
     */
    public int getRowCount()
    {
        return rowCount;
    }

    /**
     * 设置记录行数
     * @param rowCount
     */
    public void setRowCount(int rowCount)
    {
        this.rowCount = rowCount;
    }

    /**
     * 判断是否有下一页
     * @return
     */
    public boolean isNext()
    {
        return ((this.pageCount > 1 ) && ( this.pageNo < this.pageCount) );
    }

    /**
     * 判断是否有前一页
     * @return
     */
    public boolean isFornt()
    {
        return ((this.pageCount > 1 ) && ( this.pageNo > 1) );
    }
    
    /**
     * 判断是否第一页
     * @return
     */
    public boolean isFirst()
    {
        return ((this.pageCount > 1 ) && ( this.pageNo > 1) );
    }

    /**
     * 判断是否最后一页
     * @return
     */
    public boolean isLast()
    {
       return ( (this.pageCount > 1) && ( this.pageNo != this.pageCount) );
    }


    private int pageSize;
    private int pageNo;
    private int allCount;
    private int pageCount;
    private int rowCount;
    private static final int defaultPageSite = 10;


}
