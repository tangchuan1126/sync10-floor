
package com.cwc.db;

import java.io.RandomAccessFile;
import java.sql.SQLException;

/**
 * 不再使用
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class DBsetup
{
    public DBsetup()
    {

    }

    /*
     * init DataBase
     *@param  file: file name
     *@return
     */
    public void initDB(String file)
        throws Exception
    {
            DBHandler dbHandler = new DBHandler();
            RandomAccessFile rf = new RandomAccessFile(file, "r");
            String line = null;
            while( (line = rf.readLine()) != null)
            {
               // System.out.println(line);
                try
                {
                    dbHandler.executeUpdate(line);
                }
                catch(SQLException e)
                {}

            }
            rf.close();
    }
    /*
     * setup database
     *@param
     *@return
     */
    public void setupDB(String file)
        throws Exception
    {
        DBHandler dbHandler = new DBHandler();
        RandomAccessFile rf = new RandomAccessFile( file, "r");
        String sql = null;
        while( (sql = getSQL( rf )) != null)
        {
            try
            {
            	sql = new String(sql.getBytes("ISO8859_1"),"GBK");
               // System.out.println(sql);
                dbHandler.executeUpdate( sql );
            }
            catch(SQLException e)
            {
                e.printStackTrace();
            }
        }
        rf.close();
    }


    /*
     *get sql
     *@param
     *@return
     */
    private String getSQL(RandomAccessFile rf)
        throws Exception
    {
        StringBuffer buf = new StringBuffer();
        String line = null;
        while( (line = rf.readLine()) != null)
        {

            if( (line.length() == 0) || (line.charAt(0)=='#') )
            {
               continue;
            }
            else
            {
                buf.append( line.trim() );
                if(buf.charAt( buf.length()-1)==';')
                {
                    //return buf.substring(0, buf.length()-1).toString();
                    return buf.toString();
                }

            }
        }

        return null;

    }

    /*
     * test
     *@param
     *@return
     */

/*    public static void main(String[] args)
    {
        try
        {

            DBsetup setup = new DBsetup();
            String initFile="D:/sncel/prj/news/jsp/WEB-INF/db/init.sql";
            String setupFile="D:/sncel/prj/news/jsp/WEB-INF/db/setup.sql";
            String dataFile="D:/sncel/prj/news/jsp/WEB-INF/db/data.sql";
            setup.initDB(initFile);
            setup.setupDB(setupFile);
            setup.setupDB(dataFile);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

    }
    */
}
