package com.cwc.db;

import java.sql.Connection;

import org.apache.log4j.Logger;

/**
 * 对现有JDBC事务封装
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class Transaction
{
	static Logger log = Logger.getLogger("DB");
	
    private Connection conn;
    
    
    public Transaction()
    	throws Exception
    {
        init();
    }

    private void init()
    	throws Exception
    {
        try
        {
            conn = DBConnectionManager.getInstance().getConnection();	//获得数据库连接
            conn.setAutoCommit(false);									//打开事务
        }
        catch(Exception e)
        {
        	log.error("Transaction.init() error:" + e);
        	throw new Exception("Transaction init() error:" + e);
        }

    }

    /**
     * 提交事务
     * @throws Exception
     */
    public void commit()
        throws Exception
    {
        try 
		{
			conn.commit();
		}
        catch (Exception e)
		{
        	log.error("Transaction.commit() error:" + e);
        	throw new Exception("Transaction commit() error:" + e);
		}
    }

    /**
     * 回滚事务
     * @throws Exception
     */
    public void rollback()
    	throws Exception
    {
        try
        {
            conn.rollback();
        }
        catch(Exception e)
        {
        	log.error("Transaction.rollback() error:" + e);
        	throw new Exception("Transaction rollback() error:" + e);
        }
        finally
		{
        	close();
		}
    }

    /**
     * 获得数据库链接
     * @return
     */
    public Connection getConnection()
    {
        return(conn);
    }

    /**
     * 关闭事务
     * @throws Exception
     */
    public void close()
    	throws Exception
    {
        try
        {
            if ( conn!=null&&!conn.isClosed() )
            {
            	this.conn.setAutoCommit(true);		//设置自动提交
                conn.close();            			//关闭连接
            }
        }
        catch(Exception e)
        {
        	log.error("Transaction.close() error:" + e);
        	throw new Exception("Transaction close() error:" + e);
        }

    }

}
