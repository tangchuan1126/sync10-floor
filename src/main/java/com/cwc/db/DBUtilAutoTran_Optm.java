package com.cwc.db;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.cwc.transaction.TransactionThread;

public class DBUtilAutoTran_Optm extends DBUtilAutoTran {
	static Logger log = Logger.getLogger("JAVAPS");
	private ThreadLocal<Long> stopwatch = new ThreadLocal<Long>();

	public DBUtilAutoTran_Optm() throws Exception {
		super();
	}

	public boolean insert(String tablename, DBRow data) throws Exception {
		return super.insert(tablename, data);
	}
	
	/**
	 * 仅适用mysql.
	 * 插入行后如果导致在一个UNIQUE索引或PRIMARY KEY中出现重复值，则执行旧行UPDATE。
	 * 
	 * @param tablename
	 * @param updateSQL
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public long insertOrUpdateReturnId(String tablename, String updateSQL, DBRow data) throws Exception {
		long id = 0;
		
		Connection conn = null;
		//数据为空返回失败
		if(data == null) return 0;
		
		ArrayList fieldNames = data.getFieldNames();
		//字段名为NULL返回失败
		if(fieldNames == null) return 0;
		
		//字段数量
		int fieldnamessize = fieldNames.size();
		
		//字段数为0返回失败
		if(fieldnamessize == 0) return 0;
		
		//组织SQL
		StringBuffer paraSql = new StringBuffer("(");					//组织参数
		StringBuffer paraValueSql = new StringBuffer("values(");		//组织值
		
		boolean started = false;
		for(int i = 0; i < fieldnamessize; i++)
		{
			String fieldName = (String)fieldNames.get(i);
			if(started)
			{
				paraSql.append(",");
				paraValueSql.append(",");
			}
			paraSql.append(fieldName);
			paraValueSql.append("?");
			started = true;
		}
		paraSql.append(")");
		paraValueSql.append(")");
		
		PreparedStatement pstmt = null;
		StringBuffer sql = new StringBuffer("INSERT INTO ");
		sql.append(tablename).append(" ");
		sql.append(paraSql.toString()).append(" ");
		sql.append(paraValueSql.toString());
		sql.append(" ON DUPLICATE KEY UPDATE ");
		sql.append(updateSQL);
		try 
		{
			//当处在事务中时，数据库连接从事务中的线程变量获得，否则从连接池获得
			conn = DataSourceUtils.getConnection(dataSource);
			pstmt = conn.prepareStatement(sql.toString(),PreparedStatement.RETURN_GENERATED_KEYS);
			if (log.isDebugEnabled()) log.debug(sql.toString());
			if(data != null)
			{
				for(int i = 0; i < fieldnamessize; i++)
				{
					String fieldName = (String)fieldNames.get(i);	//字段名
					DBObjectType dbField = data.getField(fieldName);	//字段对象
					if(dbField != null)
					{
						//判断字段类型是否为对象类型
						if(dbField.getFieldType() == DBObjectType.OBJECT)		//对象类型
						{
							byte ll1l11llll11l1l1ll11ll1111l1l1l1l1[] = null;
							ByteArrayOutputStream ll1111l11l1l1ll11ll1111l1l1l1l1 =
									new ByteArrayOutputStream();
							//StreamUtil.putObjectIntoOutputStream(ll1l11ll1l1l1ll11ll1111l1l1l1l1.getFieldValue(), ll1111l11l1l1ll11ll1111l1l1l1l1);
							ll1l11llll11l1l1ll11ll1111l1l1l1l1 = ll1111l11l1l1ll11ll1111l1l1l1l1.toByteArray();
							try
							{
								if(ll1111l11l1l1ll11ll1111l1l1l1l1 != null)
								{
									ll1111l11l1l1ll11ll1111l1l1l1l1.close();
								}
							}
							catch(IOException e){}
							ByteArrayInputStream ll1lllll11l1l1ll11ll1111l1l1l1l1 =
									new ByteArrayInputStream(ll1l11llll11l1l1ll11ll1111l1l1l1l1);
							pstmt.setBinaryStream(i + 1, ll1lllll11l1l1ll11ll1111l1l1l1l1, ll1l11llll11l1l1ll11ll1111l1l1l1l1.length);
						}
						else
						{
							pstmt.setObject(i + 1, dbField.getFieldValue());		//设置预备声明变量
						}
					}
					else
					{
						pstmt.setObject(i, null);
					}
				}
			}
			int cnt = pstmt.executeUpdate();
			if (log.isDebugEnabled()) log.debug(cnt);
			ResultSet rs = pstmt.getGeneratedKeys();
			//id = rs.getLong(1);
			if (rs.next()) 
			{
				if (rs.first()) {
					id = rs.getLong(1);
				}
				//id = rs.getLong(1);
			}
		}
		catch(SQLException e1)
		{
			log.error("DBUtilAutoTran.insert error:" + e1);
			log.error("Error SQL:"+sql.toString());
			throw new SQLException("DBUtilAutoTran.insert error:" + e1);
		}
		finally
		{
				closeConn(null, pstmt, conn);
		}
		//把修改过的表记录在事务中 
		if (TransactionThread.isInTransaction())
		{
			TransactionThread.addTransactionUpdateTable(tablename);			
		}
		return id;
	}

	public int update(String wherecond, String tablename, DBRow data)
			throws Exception {
		return super.update(wherecond, tablename, data);
	}

	public int update(String wherecond, String tablename, DBRow data,
			boolean clean) throws Exception {
		return super.update(wherecond, tablename, data, clean);
	}
	
	/**
	 * 非事物更新
	 * 
	 * @param sql
	 * @param para
	 * @param tablename
	 * @return
	 */
	public int updatePreNoTx(String sql,DBRow para, String tablename) {
		Connection conn = null;
	    int num = 0;
	    PreparedStatement pstmt;
		try
		{
			if(tablename == null || "".equals(tablename))
			{
			    return 0;
			}
			
			if (sql==null||(!sql.toLowerCase().startsWith("update "))) {
				return 0;
			}
			if (!sql.toLowerCase().contains(" "+tablename.trim().toLowerCase()+" set")) {
				return 0;
			}
			/*
			if (false)
			{
				log.info(sql);
			}
			*/
			logSql(sql);
			
			conn = DataSourceUtils.getConnection(dataSource);
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(sql);
			
			ArrayList fieldList = para.getFieldNames();
			for (int i=0; i<fieldList.size(); i++)
			{
				//区分数据类型，全部用字符串获得
				pstmt.setObject(i+1, para.getString( fieldList.get(i).toString() ));
			}
			
			num = pstmt.executeUpdate();
			conn.commit();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
			log.error("DBUtilAutoTran.deletePre error:" + e);
			log.error("errorSql:" + sql);
			//把参数打印一下
			for (int i=0; i<para.getFieldNames().size(); i++)
			{
				log.error(para.getFieldNames().get(i)+" : "+para.getString( para.getFieldNames().get(i).toString() ));
			}
		}
	    finally
		{
				closeConn(null, null, conn);         
		}
		return num;
	}
	
	/**
     * 使用sql更行表，解决无法在dbrow 自增
     * 
     * @param sql
     * @param para
     * @param tablename 必须与sql中表名一致。
     * @return
     * @throws Exception
     * @author wangcr 2015-02-12
     */
	public int updatePre(String sql,DBRow para, String tablename) throws Exception {
		Connection conn = null;
	    int num = 0;
	    PreparedStatement pstmt;
		try
		{
			if(tablename == null || "".equals(tablename))
			{
			    return 0;
			}
			
			if (sql==null||(!sql.toLowerCase().startsWith("update "))) {
				return 0;
			}
			if (!sql.toLowerCase().contains(" "+tablename.trim().toLowerCase()+" set")) {
				return 0;
			}
			/*
			if (false)
			{
				log.info(sql);
			}
			*/
			logSql(sql);
			
			conn = DataSourceUtils.getConnection(dataSource);
			pstmt = conn.prepareStatement(sql);
			
			ArrayList fieldList = para.getFieldNames();
			for (int i=0; i<fieldList.size(); i++)
			{
				//区分数据类型，全部用字符串获得
				pstmt.setObject(i+1, para.getString( fieldList.get(i).toString() ));
			}
			
			num = pstmt.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
			log.error("DBUtilAutoTran.deletePre error:" + e);
			log.error("errorSql:" + sql);
			//把参数打印一下
			for (int i=0; i<para.getFieldNames().size(); i++)
			{
				log.error(para.getFieldNames().get(i)+" : "+para.getString( para.getFieldNames().get(i).toString() ));
			}
			
			throw new SQLException("deletePre error:" + e);
		}
	    finally
		{
				closeConn(null, null, conn);         
		}        

		//把修改过的表记录在事务中 
		if (TransactionThread.isInTransaction())
		{
			TransactionThread.addTransactionUpdateTable(tablename);			
		}
	    
	    return num;
	}

	public int delete(String wherecond, String tablename) throws Exception {
		return super.delete(wherecond, tablename);
	}
	
	/**
	 * 非进程事务删除
	 * 
	 * @param wherecond
	 * @param tablename
	 * @return
	 * @throws Exception
	 */
	public int deleteNoTx(String wherecond, String tablename) throws Exception {
		Connection conn = null;
		int num = 0;
		String sql =null;
		try {
			if(tablename == null) {
				return 0;
			}
			sql = "delete from " + tablename + " " + (wherecond==null?"":wherecond);
			conn = DataSourceUtils.getConnection(dataSource);
			num = conn.createStatement().executeUpdate(sql);
		} catch (SQLException e) {
			log.error("DBUtilAutoTran.delete error:" + e);
			log.error("errorSql:" + sql);
			throw new SQLException("delete error:" + e);
		} finally {
			closeConn(null, null, conn);
		}
		return num;
	}

//	public DBRow selectSingleCache(String sql, String tablenames[])
//			throws Exception {
//		logSql(sql, (Object) tablenames);
//		DBRow r = super.selectSingleCache(sql, tablenames);
//		logTime(sql);
//		return r;
//	}

	public DBRow selectSingle(String sql) throws SQLException {
		logSql(sql);
		DBRow r = super.selectSingle(sql);
		logTime(sql);
		return r;
	}

//	public DBRow selectPreSingleCache(String sql, DBRow para,
//			String tablenames[]) throws Exception {
//		logSql(sql, para, tablenames);
//		DBRow r = super.selectPreSingleCache(sql, para, tablenames);
//		logTime(sql);
//		return r;
//	}

	public DBRow selectPreSingle(String sql, DBRow para) throws SQLException {
		logSql(sql, para);
		DBRow r = super.selectPreSingle(sql, para);
		logTime(sql);
		return r;
	}

//	public DBRow[] selectPreMutlipleCache(String sql, DBRow para,
//			String tablenames[]) throws Exception {
//		logSql(sql, para, tablenames);
//		DBRow[] r = super.selectPreMutlipleCache(sql, para, tablenames);
//		logTime(sql);
//		return r;
//	}

	public DBRow[] selectPreMutliple(String sql, DBRow para)
			throws SQLException {
		logSql(sql, para);
		DBRow[] r = super.selectPreMutliple(sql, para);
		logTime(sql);
		return r;
	}

	public DBRow[] selectPreMutlipleCache(String sql, DBRow para, PageCtrl pc,
			String tablenames[]) throws Exception {
		logSql(sql, para, pc, tablenames);

		int pageNo = -1;
		if (pc != null) {
			sql = convertSql(sql, pc); // 转换SQL为带有select SQL_CALC_FOUND_ROWS ...
										// limit offset,pageSize 格式
			pageNo = pc.getPageNo(); // 缓存真正的页数
			pc.setPageNo(1); // 让它觉得好像是第一页，骗它填充rowCount属性、pageSize属性
		}

		DBRow[] r = super.selectPreMutliple(sql, para, pc); // Bypass Cache

		countPages(pageNo, pc); // 统计总数并填充到pc对象pageCount,
								// allCount属性，并恢复真正的pageNo属性，

		logTime(sql);
		return r;
	}

	public DBRow[] selectPreMutliple(String sql, DBRow para, PageCtrl pc)
			throws SQLException {
		logSql(sql, para, pc);

		int pageNo = -1;
		if (pc != null) {
			sql = convertSql(sql, pc); // 转换SQL为带有select SQL_CALC_FOUND_ROWS ...
										// limit offset,pageSize 格式
			pageNo = pc.getPageNo(); // 缓存真正的页数
			pc.setPageNo(1); // 让它觉得好像是第一页，骗它填充rowCount属性、pageSize属性
		}

		DBRow[] r = super.selectPreMutliple(sql, para, pc);
		
		countPages(pageNo, pc); // 统计总数并填充到pc对象pageCount,
								// allCount属性，并恢复真正的pageNo属性，

		logTime(sql);

		return r;
	}

//	public DBRow[] selectMutlipleCache(String sql, String tablenames[])
//			throws Exception {
//		logSql(sql, (Object) tablenames);
//		DBRow[] r = super.selectMutlipleCache(sql, tablenames);
//		logTime(sql);
//		return r;
//	}

	public DBRow[] selectMutliple(String sql) throws SQLException {
		logSql(sql);
		DBRow[] r = super.selectMutliple(sql);
		logTime(sql);
		return r;
	}

	public DBRow[] selectMutlipleCache(String sql, PageCtrl pc,
			String tablenams[]) throws Exception {
		logSql(sql, pc, tablenams);

		int pageNo = -1;
		if (pc != null) {
			sql = convertSql(sql, pc); // 转换SQL为带有select SQL_CALC_FOUND_ROWS ...
										// limit offset,pageSize 格式
			pageNo = pc.getPageNo(); // 缓存真正的页数
			pc.setPageNo(1); // 让它觉得好像是第一页，骗它填充rowCount属性、pageSize属性
		}

		DBRow[] r = super.selectMutliple(sql, pc); // Bypass Cache
		
		countPages(pageNo, pc); // 统计总数并填充到pc对象pageCount,
								// allCount属性，并恢复真正的pageNo属性，

		logTime(sql);
		return r;
	}

//	public DBRow[] selectMutlipleCacheByCount(String sql, int count,
//			String tablenames[]) throws Exception {
//		logSql(sql, count, tablenames);
//		DBRow[] r = super.selectMutlipleCacheByCount(sql, count, tablenames);
//		logTime(sql);
//		return r;
//	}

//	public DBRow[] selectPreMutlipleCacheByCount(String sql, DBRow para,
//			int count, String tablenames[]) throws Exception {
//		logSql(sql, para, count, tablenames);
//		DBRow[] r = super.selectPreMutlipleCacheByCount(sql, para, count,
//				tablenames);
//		logTime(sql);
//		return r;
//	}

	public DBRow[] selectMutliple(String sql, PageCtrl pc) throws SQLException {
		logSql(sql, pc);

		int pageNo = -1;
		if (pc != null) {
			sql = convertSql(sql, pc); // 转换SQL为带有select SQL_CALC_FOUND_ROWS ...
										// limit offset,pageSize 格式
			pageNo = pc.getPageNo(); // 缓存真正的页数
			pc.setPageNo(1); // 让它觉得好像是第一页，骗它填充rowCount属性、pageSize属性
		}

		DBRow[] r = super.selectMutliple(sql, pc);
		
		countPages(pageNo, pc); // 统计总数并填充到pc对象pageCount,
								// allCount属性，并恢复真正的pageNo属性，

		logTime(sql);
		return r;
	}

	public DBRow[] selectPreMutlipleByCount(String sql, DBRow para, int count)
			throws SQLException {
		logSql(sql, para, count);
		DBRow[] r = super.selectPreMutlipleByCount(sql, para, count);
		logTime(sql);
		return r;
	}

	public DBRow[] selectMutlipleByCount(String sql, int count)
			throws SQLException {
		logSql(sql, count);
		DBRow[] r = super.selectMutlipleByCount(sql, count);
		logTime(sql);
		return r;
	}

	public void update(String where, String msql, String table)
			throws Exception {
		super.update(where, msql, table);
	}

	public void update(String where, String msql, String table, boolean clean)
			throws Exception {
		super.update(where, msql, table, clean);
	}

	public long getSequance(String tablename) throws Exception {
		return super.getSequance(tablename);
	}

	public void increaseFieldVal(String table, String where, String field,
			long val, boolean clean) throws Exception {
		super.increaseFieldVal(table, where, field, val, clean);
	}

	public void decreaseFieldVal(String table, String where, String field,
			long val, boolean clean) throws Exception {
		super.decreaseFieldVal(table, where, field, val, clean);
	}

	public void batchUpdate(String sql[]) throws Exception {
		super.batchUpdate(sql);
	}

	public void increaseFieldVal(String table, String where, String field,
			double val, boolean clean) throws Exception {
		super.increaseFieldVal(table, where, field, val, clean);
	}

	public String getDBVersion() throws Exception {
		return super.getDBVersion();
	}

	public String getMysqlCreateTableSQL(String tbName) throws Exception {
		return super.getMysqlCreateTableSQL(tbName);
	}

	public ArrayList getAllTables() throws Exception {
		return super.getAllTables();
	}

	public void updateInstallSQL(String sql) throws Exception {
		super.updateInstallSQL(sql);
	}

	private void logSql(String sql, Object... args) {
		log.info(sql);
		for (Object arg : args) {
			if (arg == null) {
				// PASS
			} else if (arg instanceof String[]) { // tablenames
				log.info("tablenames=" + Arrays.asList((String[]) arg));
			} else if (arg instanceof Integer) {
				log.info("count=" + arg);
			} else if (arg instanceof PageCtrl) {
				log.info("pageCtrl=" + pageCtrlAsString((PageCtrl) arg));
			} else if (arg instanceof DBRow) {
				log.info("para=" + dbRowAsString((DBRow) arg));
			} else {
				log.error("unknown arg=" + arg);
			}
		}
		this.stopwatch.set(System.currentTimeMillis());
	}

	private void logTime(String sql) {
		log.info(sql + "\n[" + (System.currentTimeMillis() - stopwatch.get())
				+ " millisecs]");
	}

	private String pageCtrlAsString(PageCtrl pc) {
		StringBuilder sb = new StringBuilder();
		sb.append("allCount=").append(pc.getAllCount()).append(",")
				.append("pageCount=").append(pc.getPageCount()).append(",")
				.append("pageNo=").append(pc.getPageNo()).append(",")
				.append("pageSize=").append(pc.getPageSize()).append(",")
				.append("rowCount=").append(pc.getRowCount());

		return sb.toString();
	}

	private String dbRowAsString(DBRow para) {
		StringBuilder sb = new StringBuilder();
		sb.append("{\n");
		String indent = "    ";
		for (Object field : para.getFieldNames()) {
			try {
				sb.append(indent).append(field).append("=")
						.append(para.getValue(field.toString())).append("\n");
			} catch (Exception e) {
				log.fatal(indent + "field=" + field, e);
			}
		}
		sb.append("}");
		return sb.toString();
	}

	private String convertSql(String sql, PageCtrl pc) {
		int pageNo = pc.getPageNo();
		int pageSize = pc.getPageSize();
		int offset = (pageNo-1) * pageSize;
		sql = sql.replaceFirst("^\\s*(select|SELECT)\\s+",
				"select SQL_CALC_FOUND_ROWS ")
				+ " limit "
				+ offset
				+ ","
				+ pageSize;
		log.info(sql);
		return sql;
	}

	private void countPages(int pageNo, PageCtrl pc) {
		if(pc==null) return;
		
		Connection conn = null;
		Statement stat = null;
		ResultSet rs = null;
		try {
			conn = DataSourceUtils.getConnection(dataSource);
			stat = conn.createStatement();
			rs = stat.executeQuery("SELECT found_rows()");
			if (rs.next()) {
				int allCount = rs.getInt(1);

				int pageSize = pc.getPageSize();
				// 计算总页数
				int pageCount = ((allCount + pageSize) - 1) / pageSize;

				if (pageCount < pageNo) {
					pageNo = pageCount;
				}

				pc.setAllCount(allCount);
				pc.setPageCount(pageCount);
				pc.setPageNo(pageNo);

				log.info(pageCtrlAsString(pc));
			} else {
				throw new SQLException("没有能够成功获取 found_rows()！");
			}

		} catch (SQLException e) {
			log.error("获取 found_rows() 产生异常！", e);
		} finally {
			closeConn(rs, stat, conn);
		}

	}
}
