package com.cwc.db;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

import com.cwc.app.util.ConfigBean;

/**
 * 内部数据源
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class DBCPPool
{
	static Logger log = Logger.getLogger("PLATFORM");
	
	private static BasicDataSource ds = null;
	
	/**
	 * 初始化数据源
	 *
	 */
	public static void intitDataSource() 
	{
		ds = new BasicDataSource();
		ds.setDriverClassName( ConfigBean.getStringValue("jdbc_driver") );
		ds.setUsername( ConfigBean.getStringValue("jdbc_username") );
		ds.setPassword( ConfigBean.getStringValue("jdbc_password") );
		ds.setUrl( ConfigBean.getStringValue("jdbc_url") );
		ds.setMaxActive( ConfigBean.getIntValue("pool_max_active") );
		ds.setMaxIdle( ConfigBean.getIntValue("pool_max_idle") );
		ds.setMaxWait( ConfigBean.getIntValue("pool_max_wait") );
		ds.setMinIdle( ConfigBean.getIntValue("pool_min_idle") );
	}

	/**
	 * 获得数据源
	 * @return
	 */
	public synchronized static DataSource getDataSource()
	{
		if ( ds==null )
		{
			intitDataSource();
		}
		
		//System.out.println("--- NumActive: " + ds.getNumActive() + ","+ "NumIdle: " + ds.getNumIdle() );
		//检测链接使用情况
		
		return(ds);
	}
	
	/**
	 * 释放数据源（系统关闭时被调用）
	 *
	 */
	public static void closeDataSource()
	{
		log.info("closeDataSource successful!");
		
		try
		{
			if (ds != null) {
				ds.close();
			}
		}
		catch (Exception e)
		{
			log.error("DBCPPool.closeDataSource error:"+e);
		}		
	}
}
