package com.cwc.db;

import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.HashMap;

/**
 * 不再使用
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class DBSequence
{
    private Statement stmt = null;
    private ResultSet rs = null;
    private String sql = null;
    private String seqTable = "DB_SEQ";
    private String SEQNM_COL = "TABLE_NAME";
    private String SEQVALUE_COL = "SEQ_VALUE";
    private int DefaultSeq = 1;
    private HashMap hashMap ;

    public DBSequence()
        throws Exception
    {
     if( hashMap == null )
     {
         hashMap = new HashMap();
         init();
     }
    }

    /*
     *
     *@param
     *@return
     */

    private void init()
        throws Exception
    {
        //System.out.println(" init");
        Connection conn = DBConnectionManager.getInstance().getConnection();

        if ( ( conn == null ) || ( conn.isClosed() ) )
        {
            throw new SQLException("database connection fail!");
        }

        /*
        rs = conn.getMetaData().getTables(null, null,null, new String[] {"TABLE"});
        while (rs.next())
        {
            if( rs.getString("TABLE_NAME").equalsIgnoreCase( seqTable ) )
            {
                System.out.println("database cotain this table:" + seqTable);
                flag = false;
                break;
            }
        }

        Close(rs, null, null);
        */

        try
        {
            sql = "SELECT " + SEQNM_COL + "," + SEQVALUE_COL + " FROM "
                + seqTable;

            //System.out.println(sql);
            stmt = conn.createStatement();

            rs = stmt.executeQuery( sql );

            while( rs.next() )
            {
                // set seq into hashmap
                hashMap.put(rs.getString(SEQNM_COL), new Long(rs.getLong( SEQVALUE_COL)) );
            }

            Close( rs, stmt, conn );
        }
        catch(SQLException e)
        {
            //e.printStackTrace();
            createSEQTable( conn );
        }




    }

    private void createSEQTable( Connection conn)
    {
        try
        {
            //System.out.println("create seq table");

            StringBuffer buf = new StringBuffer();
            buf.append( "create table " + seqTable );
            buf.append(" (");
            buf.append( SEQNM_COL + " varchar(50),");
            buf.append( SEQVALUE_COL + " bigint ");
            buf.append(" );");
            //debug
            //System.out.println("sql=" + buf.toString());

            stmt = conn.createStatement();

            stmt.executeUpdate(buf.toString() );

            Close( null, stmt, conn );

        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }

    }


     /**
      * get current seq
      * @param tableName: seq table name
      * @return int
      */

     public long getCurrentSeq( String tableName)
        throws SQLException
    {
         if( tableName == null )
         {
            throw new SQLException("tableName is null");
         }

        if( hashMap == null)
        {
            throw new SQLException(" sequence table is not create.");
        }

        Long seq = (Long)hashMap.get( tableName);
        if( seq == null )
        {
            throw new SQLException("Sequence(" + tableName + ") have now establish.");
        }

        synchronized(seq)
        {
            return seq.longValue();
        }

    }

     /**
      * get next seq
      * @param tableName: seq table name
      * reutrn int
      */


     public long getNextSeq( String tableName)
             throws Exception
     {
        if(hashMap == null)
        {
            throw new SQLException("Seqence Table is not create.");
        }

        Long seq = (Long)( hashMap.get( tableName ));

        long retValue = 0;

        if(seq == null)
        {
            this.insertSeq( tableName );
            hashMap.put( tableName , new Long(DefaultSeq));
            retValue = 1;
        }
         else
        {
            synchronized(seq)
            {
                this.updateSeq(tableName);
                retValue = seq.longValue() + 1;
                hashMap.put(tableName, new Long(retValue));
            }
        }

         return retValue;
     }

    /*
     *
     *@param
     *@return
     */

    private void insertSeq(String tableName)
        throws Exception
    {
        Connection conn = DBConnectionManager.getInstance().getConnection();

        if ( ( conn == null ) || ( conn.isClosed() ) )
        {
            throw new SQLException("database connection fail!");
        }
        stmt = conn.createStatement();

        sql = "INSERT INTO  " + seqTable
                +" (" + SEQNM_COL + "," + SEQVALUE_COL +") VALUES " +
                "('" + tableName + "'," + DefaultSeq + ")";

         //System.out.println("sql=" + sql);
         stmt.executeUpdate( sql );

        Close(null, stmt, conn);
    }
     /**
      * update seqTable
      * @param tableName: update seq's table name
      * return
      */
     private void updateSeq(String tableName)
         throws Exception
     {
            Connection conn = DBConnectionManager.getInstance().getConnection();

            if ( ( conn == null ) || ( conn.isClosed() ) )
            {
                throw new SQLException("database connection fail!");
            }
            stmt = conn.createStatement();

            sql = "UPDATE " + seqTable
                    + " SET " + SEQVALUE_COL + "  = " + SEQVALUE_COL + " + 1"
                    + " WHERE " + SEQNM_COL + "='" + tableName + "'";

            stmt.executeUpdate( sql );

            Close( null, stmt, conn);
     }

     private void Close(ResultSet rs, Statement stmt, Connection conn)
     {
        try
        {
            if( rs != null)
            {
                rs.close();
            }

            if( stmt != null)
            {
                stmt.close();
            }

            DBConnectionManager.getInstance().close(conn);

        }
         catch(Exception e)
        {
            e.printStackTrace();
        }
     }


}
