package com.cwc.db;

import java.io.Serializable;

/**
 * dbfiled
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class DBObjectType  implements Cloneable,Serializable
{
	private String fieldName;																//字段名
    private Object fieldValue;																//字段值
    private int fieldType;																	//字段类型
    public static final int NOTSUPPORT_TYPE = -1;											//不支持类型
    public static final int DECIMAL = 1;													//数值型
    public static final int STRING = 2;													//字符串型
    public static final int OBJECT = 4;													//对象性
    public static final int TIME = 8;														//日期类型
    public static int ll1l11ll1l1l1ll11ll1111ll11l1l11l1l11111l1l111l1l1l1 = 0;
    
    public static final int DBROW_OBJECT = -2;											//dbrow数据类型
    public static final int DBROW_OBJECTS = -3;											//dbrow数组类型

    public DBObjectType()
    {
    }

    /**
     * 初始化字段
     * @param fieldName
     * @param fieldType
     * @param fieldValue
     */
    public DBObjectType(String fieldName, int fieldType, Object fieldValue)
    {
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.fieldValue = fieldValue;
    }

    /**
     * 获得字段名
     * @return
     */
    public String getFieldName()
    {
        return fieldName;
    }

    /**
     * 设置字段名
     * @param fieldName
     */
    public void setFieldName(String fieldName)
    {
        this.fieldName = fieldName;
    }

    /**
     * 获得字段值
     * @return
     */
    public Object getFieldValue()
    {
    	//加密
//    	if ( ll1l11ll1l1l1ll11ll1111ll11l1l11l1l11111l1l111l1l1l1==0 )
//    	{
//    		return(null);
//    	}
//    	else
//    	{
            return fieldValue;    		
//    	}
    }

    /**
     * 设置字段值
     * @param fieldValue
     */
    public void setFieldValue(Object fieldValue)
    {
        this.fieldValue = fieldValue;
    }

    /**
     * 获得字段类型
     * @return
     */
    public int getFieldType()
    {
        return fieldType;
    }

    /**
     * 设置字段类型
     * @param fieldType
     */
    public void setFieldType(int fieldType)
    {
        this.fieldType = fieldType;
    }

    /**
     * 克隆一个字段
     * @return
     */
    public Object clone()
    {
        DBObjectType newDf = null;
        try
        {
        	newDf = (DBObjectType)super.clone();
        }
        catch(CloneNotSupportedException e)
        {
        	newDf = new DBObjectType();
        }
        newDf.setFieldName(fieldName);
        newDf.setFieldType(fieldType);
        newDf.setFieldValue(fieldValue);
        return newDf;
    }

    /**
     * 把字段值字符串化
     * @return
     */
    public String toString()
    {
        if(this == null || fieldValue == null)
            return null;
        else
            return fieldValue.toString();
    }


}