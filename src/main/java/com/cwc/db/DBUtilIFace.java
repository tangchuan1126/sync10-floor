package com.cwc.db;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * D类B操作需要实现的接口
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public interface DBUtilIFace 
{
    public boolean insert(String tablename, DBRow data)  throws Exception;
    
    public long insertReturnId(String tablename, DBRow data)
	throws Exception;
    
    public int update(String wherecond, String tablename, DBRow data) throws Exception;
    
    public int update(String wherecond, String tablename, DBRow data,boolean clean) throws Exception;
    
    public int delete(String wherecond, String tablename) throws Exception;
    
//    public DBRow selectSingleCache(String sql,String tablenames[]) throws Exception;
    
    public DBRow selectSingle(String sql) throws SQLException;
    
//    public DBRow selectPreSingleCache(String sql,DBRow para,String tablenames[]) throws Exception;
    
    public DBRow selectPreSingle(String sql,DBRow para) throws SQLException;
    
//    public DBRow[] selectPreMutlipleCache(String sql,DBRow para,String tablenames[]) throws Exception;
    
    public DBRow[] selectPreMutliple(String sql,DBRow para) throws SQLException;
    
//    public DBRow[] selectPreMutlipleCache(String sql,DBRow para,PageCtrl pc,String tablenames[]) throws Exception;
    
    public DBRow[] selectPreMutliple(String sql,DBRow para,PageCtrl pc) throws SQLException;
    
//    public DBRow[] selectMutlipleCache(String sql,String tablenames[]) throws Exception;
    
    public DBRow[] selectMutliple(String sql) throws SQLException;
    
//    public DBRow[] selectMutlipleCache(String sql,PageCtrl pc,String tablenams[]) throws Exception;
    
//    public DBRow[] selectMutlipleCacheByCount(String sql,int count,String tablenames[]) throws Exception;
  
//	public DBRow[] selectPreMutlipleCacheByCount(String sql,DBRow para,int count,String tablenames[])  throws Exception;
    
    public DBRow[] selectMutliple(String sql, PageCtrl pc) throws SQLException;
    
    public DBRow[] selectPreMutlipleByCount(String sql,DBRow para,int count)  throws SQLException;
    
    public DBRow[] selectMutlipleByCount(String sql,int count)  throws SQLException;
    
    //public DBRow[] select(String table, String orderBy, PageCtrl pc)  throws SQLException;
    
    //public DBRow[] select(String sql, String orderBy)  throws SQLException;
    
    //public DBRow[] select(String sql)  throws SQLException;
    
    public void update(String where,String msql,String table) throws Exception;
	
    public void update(String where,String msql,String table,boolean clean) throws Exception;
	
	public long getSequance(String tablename) throws Exception;
	
	public void increaseFieldVal(String table,String where,String field,long val,boolean clean) throws Exception;
	
	public void decreaseFieldVal(String table,String where,String field,long val,boolean clean) throws Exception;
	
    public void batchUpdate(String sql[]) throws Exception;
	
	public void increaseFieldVal(String table,String where,String field,double val,boolean clean) throws Exception;
	
    public String getDBVersion() throws Exception;
	
	public String getMysqlCreateTableSQL(String tbName) throws Exception;
    
    public ArrayList getAllTables() 	throws Exception;
    
    public void updateInstallSQL(String sql) throws Exception;
}
