package com.cwc.db;

import com.cwc.app.util.ConfigBean;

/**
 * 初始化数据库参数
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class DBConfigure
{
    private static DBConfigure singleton;
    private int minsize;
    private int maxsize;
    private String url = "";
    private String user = "";
    private String password = "";
    private String driver;
    private String sequenceSqlClass = "";
    private String datasource = "";
	private String poolmandatasource = "";
    private String seqTableNm = "";
    private long initSeqValue;
    private final int DEFAULT_MIN = 1;
    private final int DEFAULT_MAX = 5;
    private final int DEFAULT_SEQ_INITVALUE = 1000;

    /*
     *  get only DBConfigure
     *@param
     *@return
     */
    public synchronized static DBConfigure getInstance()
    {
    	if ( singleton==null )
    	{
    		singleton = new DBConfigure();
    	}
    	
        return(singleton);
    }

    DBConfigure()
    {
        init();
    }

    /*
     * get min size of connection
     *@param
     *@return
     */
    public int getMinsize()
    {
        return minsize;
    }

    /*
     * get seq table name
     *@param
     *@return
     */
    public String getSeqTableName()
    {
        return seqTableNm;
    }

    /*
     * set seq table name
     *@param seqTableNm:
     *@return
     */
    public void setSeqTableName(String seqTableNm)
    {
        this.seqTableNm = seqTableNm;
    }

    public long getInitSeqValue()
    {
        return initSeqValue;
    }

    public void setInitSeqValue(long initSeqValue)
    {
        this.initSeqValue = initSeqValue;
    }

    public String getSequenceSqlClass()
    {
        return sequenceSqlClass;
    }

    public int getMaxsize()
    {
        return maxsize;
    }

    public String getUrl()
    {
        return url;
    }

    public String getDriver()
    {
        return driver;
    }

    public String getUserName()
    {
        return user;
    }

    public String getPassword()
    {
        return password;
    }


    public String getDatasource()
    {
        return datasource;
    }
    
	public String getPoolmanDatasource()
	{
		return poolmandatasource;
	}

    public void setDatasource(String datasource)
    {
        this.datasource = datasource;
    }


    private void init()
    {
        try
        {
            datasource = ConfigBean.getStringValue("data_source");
			poolmandatasource = ConfigBean.getStringValue("");
            user = ConfigBean.getStringValue("jdbc_username");
            password = ConfigBean.getStringValue("jdbc_password");
            url = ConfigBean.getStringValue("jdbc_url");
            driver = ConfigBean.getStringValue("jdbc_driver");
            sequenceSqlClass = ConfigBean.getStringValue("");
            seqTableNm = ConfigBean.getStringValue("sequence_table");
            initSeqValue = ConfigBean.getIntValue("org_sequence");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
