package com.cwc.db;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.cwc.app.util.ConfigBean;

/**
 * 数据库连接管理
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class DBConnectionManager
{
	static Logger log = Logger.getLogger("DB");
	
    private static DBConnectionManager instance;

	DBConfigure dbConfigure;
    String jdbc;
    String username;
    String password;
    String driver;
    String DataSourceName;
    String PoolManDataSourceName;
    String url;
    
    /**
     * 初始化链接参数，加载数据库驱动
     * @throws Exception
     */
    private DBConnectionManager() throws Exception
    {
    	try {
			dbConfigure = DBConfigure.getInstance();
			
			jdbc = dbConfigure.getUrl();
			username = dbConfigure.getUserName();
			password = dbConfigure.getPassword();
			driver = dbConfigure.getDriver();
			url =  dbConfigure.getUrl();
			DataSourceName = dbConfigure.getDatasource();
			PoolManDataSourceName = dbConfigure.getPoolmanDatasource();
			
			registDriver();
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
    }

     public static synchronized  DBConnectionManager getInstance()
             throws Exception
     {
        if (instance == null)
        {
            instance = new DBConnectionManager();
        }

        return instance;

    }

     public void registDriver()
     	throws Exception
     {
        try
        {
            if( driver.length() == 0)
            {
                throw new SQLException("please set driver!");
            }
            Driver dbdriver = (Driver)Class.forName(driver).newInstance();

            DriverManager.registerDriver(dbdriver);
        }
        catch(java.lang.ClassNotFoundException e)
        {
        	log.error("DBConnectionManager.createPools error:driver [" + driver + "] not found " + e);
            throw new Exception(driver + " :driver no found!");
        }
        catch(Exception e)
        {
        	log.error("DBConnectionManager.createPools error:" + e);
        	throw new Exception("DBConnectionManager.createPools error:" + e);
        }
     }

     /**
      * 从外部数据源获取数据库连接
      * @param DataSourceName
      * @return
      * @throws Exception
      */
    private DataSource datasourceConnection(String DataSourceName)
    	throws Exception
    {
        try
        {
        	DataSource  ds;

        	if ( ConfigBean.getStringValue("jdbc_internal_pool").equals("true") )
        	{
        		//内部数据源
				ds = DBCPPool.getDataSource();
        	}
        	else
        	{
//        		Hashtable env = new Hashtable(); 
//        		env.put("java.naming.factory.initial","org.apache.naming.java.javaURLContextFactory");

        		InitialContext ctx=new InitialContext();
        		
        		ctx.lookup("java:comp/env");       		
        		ds=(DataSource)ctx.lookup(DataSourceName);
        	}
        	
        	return(ds);
        }
        catch(Exception e)
        {
        	log.error("DBConnectionManager.datasourceConnection("+DataSourceName+") error:" + e);
        	throw new Exception("DBConnectionManager.datasourceConnection error:" + e);
        }
    }

	/**
	 * 获得数据库连接
	 * @return
	 * @throws SQLException
	 */
    public synchronized Connection getConnection() 
    	throws SQLException
    {
        try
        {
        	Connection conn;
        	
        	if ( ConfigBean.getStringValue("jdbc_internal_pool").equals("direct") )		//直接连接数据库
        	{
        		conn = DriverManager.getConnection(ConfigBean.getStringValue("jdbc_url"),ConfigBean.getStringValue("jdbc_username"),ConfigBean.getStringValue("jdbc_password"));
        	}
        	else if ( ConfigBean.getStringValue("jdbc_internal_pool").equals("odbc") )		//通过ODBC链接
        	{
        		conn = DriverManager.getConnection(ConfigBean.getStringValue("jdbc_url"));
        	}
        	else
        	{
        		//通过数据源获得
           		conn = datasourceConnection(DataSourceName).getConnection();        		
        	}

            if( (conn == null) || (conn.isClosed()) )
            {
                 throw new SQLException("getConnection() connection is " + conn);
            }
            else
            {
                return conn;
            }
        }
        catch(Exception e)
        {
        	log.error("DBConnectionManager.Connection error:" + e);
            throw new SQLException("DBConnectionManager.Connection error:" + e);
        }
    }

    //关闭数据库连接
    public final void close(Connection conn)
    	throws SQLException
    {
		try
		{
			if( (conn != null) && !conn.isClosed() )
			{
				conn.close();
			}
		}
		catch (SQLException e)
		{
			log.error("DBConnectionManager.close error:" + e);
            throw new SQLException("DBConnectionManager.close error:" + e);
		}
    }
    
    public static void main()
    {
    	//System.out.println("Hello, it's: " +  new Date());        //print available locales        Locale list[] = DateFormat.getAvailableLocales();        System.out.println("======System available locales:======== ");        for (int i = 0; i < list.length; i++) {            System.out.println(list[i].toString() + "\t" + list[i].getDisplayName());        }        //print JVM default properties        System.out.println("======System property======== ");        System.getProperties().list(System.out);
    }

}


