package com.cwc.init;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class StorageJetLagInit {

	/**
	 * 初始化加载 时差信息
	 * @param dbUtilAutoTran
	 * @throws SQLException
	 */
	public void init(DBUtilAutoTran dbUtilAutoTran) 
		throws SQLException
	{
		cleanStorageTimeZoneId();
		String sql = "SELECT ps.id, cp.timezone_id FROM "+ConfigBean.getStringValue("product_storage_catalog")+" ps "
				+ " join country_province cp on cp.pro_id = ps.pro_id"
				+ " where cp.timezone_id is not null";
		DBRow[] timeZoneIds = dbUtilAutoTran.selectMutliple(sql);
		
		Map<Long,String> tzMap = new HashMap<Long,String>();
		for (DBRow dbRow : timeZoneIds) 
		{
			tzMap.put(dbRow.get("id",0l),dbRow.getString("timezone_id"));
		}
		
		DateUtil.setStroageTimeZoneId(tzMap);
	}
	public void cleanStorageTimeZoneId()
	{
		DateUtil.setStroageTimeZoneId(null);
	}
	
}
