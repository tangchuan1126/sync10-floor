package com.cwc.app.key;

import com.cwc.app.util.Environment;

public class LocalKey
{
	public static String getLocalKey(String org)
	{
		if (org==null)
		{
			return(null);
		}
		else
		{
			return(org.concat("_").concat(Environment.getLocale().toString()));			
		}
	}
}
