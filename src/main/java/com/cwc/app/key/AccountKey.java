package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class AccountKey
{
	DBRow row;
	public static int SUPPLIER = 1;	//供应商
	public static int APPLI_MONEY = 2 ; //资金申请
	public static int PRE_APPLY_MONEY = 3 ; //预申请
	public static int APPLY_TRANSFER = 4 ; //转账申请
 
	public AccountKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(AccountKey.SUPPLIER), "供应商");
		row.add(String.valueOf(AccountKey.APPLI_MONEY), "资金申请");
		row.add(String.valueOf(AccountKey.PRE_APPLY_MONEY), "预申请");
		row.add(String.valueOf(AccountKey.APPLY_TRANSFER), "转账申请");
		
	}
	
	public ArrayList getAccountKeys()
	{
		return(row.getFieldNames());
	}

	public String getAccountKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
