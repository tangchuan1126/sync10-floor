package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProductEditTypeKey
{
	DBRow row;

	public static int ADD = 1;
	public static int UPDATE = 2;
	public static int DELETE = 3;
	
	

	public ProductEditTypeKey() 
	{
		row = new DBRow();
		
		row.add(String.valueOf(ProductEditTypeKey.ADD),"添加");
		row.add(String.valueOf(ProductEditTypeKey.UPDATE),"修改");
		row.add(String.valueOf(ProductEditTypeKey.DELETE),"删除");
		
	}

	public ArrayList getProductEditType()
	{
		return(row.getFieldNames());
	}

	public String getProductEditTypeId(String id)
	{
		return(row.getString(id));
	}
}
