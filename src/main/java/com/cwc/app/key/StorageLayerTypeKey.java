package com.cwc.app.key;

import java.util.ArrayList;
import java.util.List;

import com.cwc.db.DBRow;

public class StorageLayerTypeKey {
	
	DBRow dbRowEn;
	
	public static int LOCATION = 1;// 
	public static int STAGING = 2;// 
	public static int DOCK = 3;// 
	public static int PARKING = 4;// 
	public static int ZONE = 5;// 
	public static int WEBCAM = 6;// 
	public static int PRINT = 7;// 

	public StorageLayerTypeKey() {
		
		dbRowEn = new DBRow();
		dbRowEn.add(String.valueOf(StorageLayerTypeKey.LOCATION), "LOCATION");
		dbRowEn.add(String.valueOf(StorageLayerTypeKey.STAGING), "STAGING");
		dbRowEn.add(String.valueOf(StorageLayerTypeKey.DOCK), "DOCK");
		dbRowEn.add(String.valueOf(StorageLayerTypeKey.PARKING), "PARKING");
		dbRowEn.add(String.valueOf(StorageLayerTypeKey.ZONE), "ZONE");
		dbRowEn.add(String.valueOf(StorageLayerTypeKey.WEBCAM), "WEBCAM");
		dbRowEn.add(String.valueOf(StorageLayerTypeKey.PRINT), "PRINT");
	}

	public ArrayList<?> getStorageTypeKeys() {
		return (dbRowEn.getFieldNames());
	}

	public String getStorageTypeKeyName(String id) {
		return (dbRowEn.getString(id));
	}

	public String getStorageTypeKeyName(int id) {
		return (dbRowEn.getString(String.valueOf(id)));
	}

	public String getStorageTypeKeyNameEn(String id) {
		return (dbRowEn.getString(id));
	}

	public String getStorageTypeKeyNameEn(int id) {
		return (dbRowEn.getString(String.valueOf(id)));
	}
	
}
