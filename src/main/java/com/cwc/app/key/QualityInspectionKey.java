package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class QualityInspectionKey {
	
	DBRow row;
	
	public static int NO_NEED_QUALITY = 1;	//不需要质检
	public static int NEED_QUALITY = 2;//需要质检
//	public static int QUALITYING = 3;//质检中
	public static int FINISH = 3;//质检完成
	
	public QualityInspectionKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(QualityInspectionKey.NO_NEED_QUALITY), "不需要质检要求");
		row.add(String.valueOf(QualityInspectionKey.NEED_QUALITY), "质检要求上传中");
//		row.add(String.valueOf(QualityInspectionKey.QUALITYING), "质检要求上传中");
		row.add(String.valueOf(QualityInspectionKey.FINISH), "质检要求完成");
	}
	
	public ArrayList<String> getQualityInspection()
	{
		return(row.getFieldNames());
	}

	public String getQualityInspectionById(String id)
	{
		return(row.getString(id));
	}
	
	public String getQualityInspectionById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
