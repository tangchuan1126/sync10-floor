package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class PurchaseLogTypeKey {
	
	DBRow row;
	public static int CREATE = 1;
	public static int FINANCIAL = 2;
	public static int UPDATE = 3;
	public static int INVOICE = 4;
	public static int DRAWBACK = 5;
	public static int PRICE = 6;
	public static int TRANSPORT = 7;
	public static int NEED_TAG = 8;
	public static int PRODUCT_TAG = 12;
	public static int QUALITY = 13;
	public static int PRODUCT_MODEL = 23;
	public static int THIRD_TAG = 33; 			//采购单第三方标签
	
	
	public PurchaseLogTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(PurchaseLogTypeKey.CREATE),"创建记录");
		row.add(String.valueOf(PurchaseLogTypeKey.FINANCIAL),"财务记录");
		row.add(String.valueOf(PurchaseLogTypeKey.UPDATE),"修改记录");
		row.add(String.valueOf(PurchaseLogTypeKey.INVOICE),"发票跟进");
		row.add(String.valueOf(PurchaseLogTypeKey.DRAWBACK),"退税跟进");
		row.add(String.valueOf(PurchaseLogTypeKey.PRICE),"价格跟进");
		row.add(String.valueOf(PurchaseLogTypeKey.TRANSPORT),"交货跟进");
		row.add(String.valueOf(PurchaseLogTypeKey.NEED_TAG),"内部标签跟进");
		row.add(String.valueOf(PurchaseLogTypeKey.PRODUCT_TAG),"商品标签");
		row.add(String.valueOf(PurchaseLogTypeKey.QUALITY),"质检要求");
		row.add(String.valueOf(PurchaseLogTypeKey.PRODUCT_MODEL),"商品范例");
		row.add(String.valueOf(PurchaseLogTypeKey.THIRD_TAG),"第三方标签");
	}
	
	public ArrayList PurchaseLogType()
	{
		return(row.getFieldNames());
	}

	public String getPurchaseLogTypeById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
