package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class PurchaseArriveKey {
	DBRow row;
	
	public static int NOARRIVE = 1;
	public static int PARTARRIVE = 2;
	public static int ALLARRIVE = 3;
	public static int ARRIVEAPPROVE = 4;
	public static int WAITAPPROVE = 8;
	

	public PurchaseArriveKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(PurchaseArriveKey.NOARRIVE),"未到货");
		row.add(String.valueOf(PurchaseArriveKey.PARTARRIVE),"部分到货");
		row.add(String.valueOf(PurchaseArriveKey.ALLARRIVE),"全部到货");
		row.add(String.valueOf(PurchaseArriveKey.ARRIVEAPPROVE),"多到货");
		row.add(String.valueOf(PurchaseArriveKey.WAITAPPROVE),"<font color=blue>审核中</font>");
		
		
	}
	
	public ArrayList getQuoteStatus()
	{
		return(row.getFieldNames());
	}

	public String getQuoteStatusById(String id)
	{
		return(row.getString(id));
	}
}
