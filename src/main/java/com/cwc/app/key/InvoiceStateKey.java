package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

//Invoice key 主要是创建,发送
public class InvoiceStateKey {

	DBRow row;
	
	public DBRow getRow() {
		return row;
	}

	public static int NewAdd = 1;					// 新创建没有Invoice
	public static int CreateInvoiceed = 2;			//已经创建Invoice
	public static int SendInvoiceed = 3;			//已经发送Invoice
	public static int NotUpdateBillInvoice = 4;		//>3 的时候修改了bill需要更新Invoice
 
	public InvoiceStateKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(InvoiceStateKey.NewAdd), "NewAdd");
		row.add(String.valueOf(InvoiceStateKey.CreateInvoiceed), "CreateInvoiceed");
		row.add(String.valueOf(InvoiceStateKey.SendInvoiceed), "SendInvoiceed");
		row.add(String.valueOf(InvoiceStateKey.NotUpdateBillInvoice), "NotUpdateBillInvoice");
		 
	}

	public ArrayList<String> getStatus()
	{
		return((ArrayList<String>)row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
