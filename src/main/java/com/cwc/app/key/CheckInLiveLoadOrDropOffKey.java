package com.cwc.app.key;

import java.util.List;

import com.cwc.db.DBRow;

public class CheckInLiveLoadOrDropOffKey {
 	 
	
	DBRow row;
	public final static int LIVE = 1;
	public final static int DROP = 2;
	public final static int SWAP = 3;
	public final static int PICK_UP = 4;
	public final static int TMS = 5;
	
	public CheckInLiveLoadOrDropOffKey()
	{
		row = new DBRow();
		row.add(String.valueOf(CheckInLiveLoadOrDropOffKey.LIVE), "Live Load");
		row.add(String.valueOf(CheckInLiveLoadOrDropOffKey.DROP), "Drop Off");
		row.add(String.valueOf(CheckInLiveLoadOrDropOffKey.SWAP), "Swap CTNR");
		row.add(String.valueOf(CheckInLiveLoadOrDropOffKey.PICK_UP), "Pickup CTNR");
		row.add(String.valueOf(CheckInLiveLoadOrDropOffKey.TMS), "TMS");
	}
	
	public List getCheckInLiveLoadOrDropOffKey()
	{
		return row.getFieldNames();
	}
	
	public String getCheckInLiveLoadOrDropOffKey(int id)
	{
		return row.getString(String.valueOf(id));
	}
	
	public String getCheckInLiveLoadOrDropOffKey(String id)
	{
		return row.getString(id);
	}
}

