package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class CheckInSpotStatusTypeKey{

	DBRow row;
	public static int RELEASED = 1;
	public static int OCUPIED = 2;
 
	
	
	public CheckInSpotStatusTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(CheckInSpotStatusTypeKey.RELEASED), "Released"); 	//释放 也就是Free	
 		row.add(String.valueOf(CheckInSpotStatusTypeKey.OCUPIED), "Occupied");		//占用
	}

	public ArrayList getContainerTypeKeys() {
		return (row.getFieldNames());
	}

	public String getContainerTypeKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getContainerTypeKeyValue(String id) {
		return (row.getString(id));
	}

}
