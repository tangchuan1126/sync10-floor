package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class OrderTaskKey
{
	DBRow reason;
	DBRow operation;
	DBRow status;

	public static int T_REASON_MERGE = 1;
	public static int T_REASON_RECORD = 2;
	public static int T_REASON_MODIFY_ADDRESS = 3;
	public static int T_REASON_CNACLE = 4;
	public static int T_REASON_UNMERGE = 5;
	public static int T_AGAIN_INSTORE = 6;//缺货已打印订单，再次补货

	public static int T_OPERATION_DESTROY = 1;
	public static int T_OPERATION_RE_PRINT = 2;

	public static int T_STATUS_WAITING = 1;
	public static int T_STATUS_COMPLETE = 2;
	public static int T_STATUS_UNCOMPLETE = 3;

	public OrderTaskKey() 
	{
		reason = new DBRow();
		reason.add(String.valueOf(T_REASON_MERGE),"订单合并");
		reason.add(String.valueOf(T_REASON_RECORD),"重新抄单");
		reason.add(String.valueOf(T_REASON_MODIFY_ADDRESS),"修改地址");
		reason.add(String.valueOf(T_REASON_CNACLE),"取消订单");
		reason.add(String.valueOf(T_REASON_UNMERGE),"订单解除合并");
		reason.add(String.valueOf(T_AGAIN_INSTORE),"缺货补货");

		operation = new DBRow();
		operation.add(String.valueOf(T_OPERATION_DESTROY),"撕毁原单");
		operation.add(String.valueOf(T_OPERATION_RE_PRINT),"重新打印");

		status = new DBRow();
		status.add(String.valueOf(T_STATUS_WAITING),"<span style='color:#FF0000;font-weight:bold'>待处理</span>");
		status.add(String.valueOf(T_STATUS_COMPLETE),"<span style='color:#33CC00;font-weight:bold'>已执行</span>");
		status.add(String.valueOf(T_STATUS_UNCOMPLETE),"<span style='color:#0000FF;font-weight:bold'>不能执行</span>");
	}

	public ArrayList getAllReasonKeys()
	{
		return( this.reason.getFieldNames() );
	}
	
	public String getReasonById(int id)
	{
		return( this.reason.getString(String.valueOf(id)) );
	}
	
	public ArrayList getAllOperationKeys()
	{
		return( this.operation.getFieldNames() );
	}
	
	public String getOperationById(int id)
	{
		return( this.operation.getString(String.valueOf(id)) );
	}
	
	public ArrayList getAllStatusKeys()
	{
		return( this.status.getFieldNames() );
	}
	
	public String getStatusById(int id)
	{
		return( this.status.getString(String.valueOf(id)) );
	}
}


