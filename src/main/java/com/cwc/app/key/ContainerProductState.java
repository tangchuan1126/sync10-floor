package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ContainerProductState {


	DBRow row;
	public static int EMPTY = 1 ; //空箱子
	public static int DEPART = 2 ; // 部分
	public static int FULL = 3 ; // 满箱子
  
	public ContainerProductState() 
	{
		row = new DBRow();
		row.add(String.valueOf(ContainerProductState.EMPTY), "EMPTY");
		row.add(String.valueOf(ContainerProductState.DEPART), "DEPART");
		row.add(String.valueOf(ContainerProductState.FULL), "FULL");
  
	}
	
	public ArrayList getContainerProductStateKeys()
	{
		return(row.getFieldNames());
	}

	public String getContainerProductStateValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getContainerProductStateValue(String id)
	{
		return(row.getString(id));
	}
}
