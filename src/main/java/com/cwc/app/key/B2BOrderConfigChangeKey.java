package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderConfigChangeKey {

	DBRow row;
	
	public static int NotNeed = 1;			//不需要
	public static int Can = 2;				//可以
	public static int CanNot = 3;			//不可以
	
	public B2BOrderConfigChangeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderConfigChangeKey.NotNeed),"<font color=\"green\">不需要</font>");
		row.add(String.valueOf(B2BOrderConfigChangeKey.Can),"<font color=\"blue\">可以</font>");
		row.add(String.valueOf(B2BOrderConfigChangeKey.CanNot),"<font color=\"red\">不可以</font>");
	}

	public ArrayList getB2BOrderStatusKeys()
	{
		return(row.getFieldNames());
	}

	public String getB2BOrderStatusKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
