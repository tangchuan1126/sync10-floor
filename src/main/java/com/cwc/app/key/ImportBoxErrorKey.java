package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ImportBoxErrorKey {

	DBRow row;
	
	public static String PRODUCTNOTFOUND = "PRODUCTNOTFOUND";//商品不存在
	public static String QUANTITYERROR = "HARDWOARID";//盒子容量计算有误
	public static String VOLUMEERROR = "VOLUMEERROR";//容器体积不正确
	public static String WEIGHTERROR = "WEIGHTERROR" ; //盒子重量输入有误.
	public static String BOXTYPENAME = "BOXTYPENAME" ;	//BOXTYPENAME输入有误
	public static String CONTAINERTYPE = "CONTAINERTYPE" ;	//盒子容器类型有误
	public ImportBoxErrorKey(){
		
		row = new DBRow();
		row.add(ImportBoxErrorKey.PRODUCTNOTFOUND,"<font color=red>商品不存在&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>");
		row.add(ImportBoxErrorKey.QUANTITYERROR,"<font color=red>盒子装货数有误.&nbsp;&nbsp;&nbsp;</font>");
		row.add(ImportBoxErrorKey.VOLUMEERROR,"<font color=red>盒子体积计算有误.</font>");
		row.add(ImportBoxErrorKey.WEIGHTERROR,"<font color=red>盒子重量输入有误.</font>");
		row.add(ImportBoxErrorKey.BOXTYPENAME,"<font color=red>盒子类型输入有误.</font>");
		row.add(ImportBoxErrorKey.CONTAINERTYPE,"<font color=red>盒子容器类型有误.</font>");
	}
	public ArrayList getContainerErrorMessage()
	{
		return(row.getFieldNames());
	}

	public String getContainerErrorMessageById(String id)
	{
		return(row.getString(id));
	}
}
