package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class TransactionType {

	DBRow row;
	
	public DBRow getRow() {
		return row;
	}

	public static int Payment = 1;			 
	public static int Complain = 2;		 
	public static int Warranty = 3;		 
	public static int Compensate = 4; // pay for order
 
	public TransactionType() 
	{
		row = new DBRow();
		row.add(String.valueOf(TransactionType.Payment), "付款类");
		row.add(String.valueOf(TransactionType.Complain), "投诉类");
		row.add(String.valueOf(TransactionType.Warranty), "质保类");
		row.add(String.valueOf(TransactionType.Compensate), "补偿类");
		 
	}

	public ArrayList<String> getStatus()
	{
		return((ArrayList<String>)row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
