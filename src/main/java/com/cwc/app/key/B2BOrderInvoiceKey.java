package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderInvoiceKey {
	DBRow row;
	
	public static int NOINVOICE = 1;//不需要发票
	public static int INVOICE = 2;//需要发票
	public static int INVOICING = 3;//开票中
	public static int FINISH = 4;//开票完成
	
	public B2BOrderInvoiceKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderInvoiceKey.NOINVOICE), "不需要发票");
		row.add(String.valueOf(B2BOrderInvoiceKey.INVOICE), "需要发票");
		row.add(String.valueOf(B2BOrderInvoiceKey.INVOICING), "开票中");
		row.add(String.valueOf(B2BOrderInvoiceKey.FINISH), "开票完成");
	}
	
	public ArrayList getB2BOrderInvoiceKeys()
	{
		return(row.getFieldNames());
	}

	public String getB2BOrderInvoiceKeyById(String id)
	{
		return(row.getString(id));
	}
	
	public String getB2BOrderInvoiceKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
