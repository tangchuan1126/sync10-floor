package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WmsOrderTypeKey {
	DBRow row;
	
	public static int LOAD_NO = 1;
	public static int MASTER_BOL_NO = 2;
	public static int ORDER = 3;
	public static int RECEIPTS = 4;
	public static int PICKUP = 5;
	public static int DELIVERY = 6;
	
	public WmsOrderTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WmsOrderTypeKey.LOAD_NO), "LoadNo");
		row.add(String.valueOf(WmsOrderTypeKey.MASTER_BOL_NO), "MasterBOLNo");
		row.add(String.valueOf(WmsOrderTypeKey.ORDER), "Order");
		row.add(String.valueOf(WmsOrderTypeKey.RECEIPTS), "Receipts");
		row.add(String.valueOf(WmsOrderTypeKey.PICKUP), "PickUp");
		row.add(String.valueOf(WmsOrderTypeKey.DELIVERY), "Delivery");
	}
	
	public ArrayList getWmsOrderTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getWmsOrderTypeKeyyByKey(String id)
	{
		return(row.getString(id));
	}
	
}
