package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class NoticeTypeKey {

	DBRow dbRow;
	public static int MAIL = 1;
	public static int MESSAGE = 2;
	public static int PAGE = 3;
	
	public NoticeTypeKey()
	{
		dbRow = new DBRow();
		dbRow.add(String.valueOf(NoticeTypeKey.MAIL), "邮件");
		dbRow.add(String.valueOf(NoticeTypeKey.MESSAGE), "短信");
		dbRow.add(String.valueOf(NoticeTypeKey.PAGE), "页面");
	}
	
	public ArrayList getNoticeTypeKeys()
	{
		return(dbRow.getFieldNames());
	}

	public String getNoticeTypeKeyName(String id)
	{
		return(dbRow.getString(id));
	}
	
	public String getNoticeTypeKeyName(int id)
	{
		return(dbRow.getString(String.valueOf(id)));
	}
}
