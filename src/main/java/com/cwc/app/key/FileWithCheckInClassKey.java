package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class FileWithCheckInClassKey {
	DBRow row;
	
	public final static int PhotoGateCheckIn = 1 ;    //Gate Check In   web页面		//sort 10
	public final static int PhotoDockCheckIn = 2 ;    //Dock Check In  手持设备		//sort 40
	public final static int PhotoWindowCheckIn = 3 ;  //Window Check In web页面		//sort 20
	public final static int PhotoGateCheckOut = 4;    //check out  both				//sort 70
	public final static int PhotoDockClose = 5 ;      //dock close 					//sort 60	
	public final static int PhotoWindowCheckOut = 6 ; //window Check Out 不知道是什么 问徐佳//sort 30
	public final static int PhotoLoad = 7 ;			//Load 的时候使用					//sort 50	
	public final static int PhotoReceive = 8 ; 		//Receive 的时候使用				//sort 50
	public final static int PhotoWarehouseCheckIn  = 9 ; //Photo WarehouseCheckIn 同dock Check In //sort 40
	public final static int PhotoTaskProcessing = 10 ; 	//任务处理中				//sort 50
	public final static int PhotoCountingSheet = 11 ;//CountSheet
	public final static int PhotoTMSException = 12 ; 	//TMS异常
	public final static int PhotoNoGroup = 0 	; 	//无分组 显示的时候用的				//sort 0
	 
	
	/**
	 * 将以前的FileWithClass 合并
	 * @param file_with_class
	 * @return
	 * @author zhangrui
	 * @Date   2014年12月15日
	 */
	public static int getFixFileWithClass(int file_with_class){
		if(file_with_class == PhotoLoad ||  
				file_with_class == PhotoReceive  
				){
			return PhotoTaskProcessing ;
		}
		if(file_with_class == PhotoWarehouseCheckIn ||
				file_with_class == PhotoDockCheckIn){
			return PhotoWarehouseCheckIn ;
		}
		return file_with_class ;
	}
	/**
	 * 按照Entry的流程排序Dir
	 * @param file_with_class
	 * @return
	 * @author zhangrui
	 * @Date   2014年12月15日
	 */
	public static int getFixSortFileWithClass(int file_with_class){ 
		int returnInt = 0 ;
		switch (file_with_class) {
			case PhotoGateCheckIn: returnInt = 10 ; break;
			case PhotoDockCheckIn: returnInt = 40 ; break;
			case PhotoWindowCheckIn: returnInt = 20 ; break;
			case PhotoGateCheckOut: returnInt = 70 ; break;
			case PhotoDockClose: returnInt = 60 ; break;
			case PhotoWindowCheckOut: returnInt = 30 ; break;
			case PhotoLoad: returnInt = 50 ; break;
			case PhotoReceive: returnInt = 50 ; break;
 			case PhotoTaskProcessing: returnInt = 50 ; break;
			case PhotoNoGroup: returnInt =  0 ; break;
			case PhotoWarehouseCheckIn : returnInt = 40; break ;
			case PhotoCountingSheet : returnInt = 55; break ;
 		}
		return returnInt ;
	}
	public FileWithCheckInClassKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(FileWithCheckInClassKey.PhotoGateCheckIn), "GateCheckIn");
		row.add(String.valueOf(FileWithCheckInClassKey.PhotoDockCheckIn), "DockCheckIn");
		row.add(String.valueOf(FileWithCheckInClassKey.PhotoWindowCheckIn), "WindowCheckIn");
		row.add(String.valueOf(FileWithCheckInClassKey.PhotoGateCheckOut), "GateCheckOut");
		row.add(String.valueOf(FileWithCheckInClassKey.PhotoDockClose), "DockClose");
		row.add(String.valueOf(FileWithCheckInClassKey.PhotoWindowCheckOut), "WindowCheckOut");
		row.add(String.valueOf(FileWithCheckInClassKey.PhotoLoad), "Load");
		row.add(String.valueOf(FileWithCheckInClassKey.PhotoReceive), "Receive");	
		row.add(String.valueOf(FileWithCheckInClassKey.PhotoWarehouseCheckIn), "DockCheckIn");
		row.add(String.valueOf(FileWithCheckInClassKey.PhotoTaskProcessing), "TaskProcessing");
		row.add(String.valueOf(FileWithCheckInClassKey.PhotoCountingSheet), "CountSheet");  // zhangrui 2015-03-04 添加
		row.add(String.valueOf(FileWithCheckInClassKey.PhotoNoGroup), "No Group");		//显示的时候用的

		
	}
	public ArrayList getFileWithType()
	{
		return(row.getFieldNames());
	}

	public String getFileWithTypeId(int id)
	{
		return(row.getString(String.valueOf(getFixFileWithClass(id))));
	}
	 
}
