package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class PalletInventoryKey
{
	DBRow row;
	
	public static int Pallet1 = 1;		//gate check in 图片
	public static int Pallet2 = 2;	//WINDOW CHECK IN 图片
	
	public static int Pallet3 = 3;//WARE HOUSE CHECK IN 图片
	public static int Pallet4 = 4;//GATE CHECK OUT 图片
	public static int Pallet5 = 5;//GATE CHECK OUT 图片  android
	public static int  Pallet6 = 6;// WINDOW CHECK OUT 图片
	
	
	public PalletInventoryKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(this.Pallet1), "Pallet1");
		row.add(String.valueOf(this.Pallet2), "Pallet2");
		row.add(String.valueOf(this.Pallet3) , "Pallet3");
		row.add(String.valueOf(this.Pallet4) , "Pallet4");
		row.add(String.valueOf(this.Pallet5) , "Pallet5");
		row.add(String.valueOf(this.Pallet6) , "Pallet6");
	}

	public String getPalletInventoryName(int para)
	{
		return(row.get(String.valueOf(para), ""));
	}
	public int getLength()
	{
		return(row.size());
	}
	
	public ArrayList<String> getPalletInventoryName(){
		return row.getFieldNames();
	}

	
}
