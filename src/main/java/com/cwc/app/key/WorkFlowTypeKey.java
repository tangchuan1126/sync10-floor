package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WorkFlowTypeKey {

	DBRow row;
	
	public static int ORDER_ASSESS = 1;	 //订单评价
	public static int DRWABACK = 2 ; 	 //申请退款
	public static int RETAIL_PRICE = 3 ; //零售报价
	
	public WorkFlowTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WorkFlowTypeKey.ORDER_ASSESS), "订单评价");
		row.add(String.valueOf(WorkFlowTypeKey.DRWABACK), "申请退款");
		row.add(String.valueOf(WorkFlowTypeKey.RETAIL_PRICE), "零售报价");
	}
	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
