package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class CheckInSearchPriorityKey {
	DBRow row;
	
	public static int LOCAL = 1;
	public static int WMS = 2;
	
	
	public CheckInSearchPriorityKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(CheckInSearchPriorityKey.LOCAL), "Local");
		row.add(String.valueOf(CheckInSearchPriorityKey.WMS), "WMS");
	}
	
	public ArrayList getCheckInSearchPriorityKeys()
	{
		return(row.getFieldNames());
	}

	public String getCheckInSearchPriorityKey(String id)
	{
		return(row.getString(id));
	}
	
	public String getCheckInSearchPriorityKey(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
}
