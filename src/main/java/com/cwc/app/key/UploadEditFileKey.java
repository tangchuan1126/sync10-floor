package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class UploadEditFileKey
{
	DBRow row;
	public static int CommodityData = 1;	//商品数据
	public static int UnionRelation = 2;	//组合关系
	public static int ProductFile = 3;		//商品文件


	public UploadEditFileKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(UploadEditFileKey.CommodityData),"商品数据");
		row.add(String.valueOf(UploadEditFileKey.UnionRelation),"套装关系");
		row.add(String.valueOf(UploadEditFileKey.ProductFile),"商品图片");
	}

	public ArrayList getUploadEditFile()
	{
		return(row.getFieldNames());
	}

	public String getUploadEditFileById(String id)
	{
		return(row.getString(id));
	}
}
