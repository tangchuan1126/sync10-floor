package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ReturnProductItemMatchKey
{
	DBRow row;
	
	public static int NOT_RELATION = 1;	//新创建,还没有关联上商品
	public static int RELATION = 2 ; //关联上商品
	public static int NOT_MATCH = 3 ; //上传的相片不符合商品。
	public ReturnProductItemMatchKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnProductItemMatchKey.NOT_RELATION), "未关联");
		row.add(String.valueOf(ReturnProductItemMatchKey.RELATION), "关联商品");
		row.add(String.valueOf(ReturnProductItemMatchKey.NOT_MATCH), "图片不符");
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
