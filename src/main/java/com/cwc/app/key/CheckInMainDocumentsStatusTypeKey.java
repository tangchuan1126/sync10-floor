package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class CheckInMainDocumentsStatusTypeKey {

	DBRow row;
/**	public static int PICK_UP_UNPROCESS = 1;
	public static int PICK_UP_PROCESSING = 2;
	public static int PICK_UP_INYARD = 3;
	public static int PICK_UP_LEAVING = 4;
	public static int PICK_UP_LEFT = 5;
	public static int DELIVERY_UNPROCESS = 6;
	public static int DELIVERY_PROCESSING = 7;
	public static int DELIVERY_INYARD = 8;
	public static int DELIVERY_LEAVING = 9;
	public static int DELIVERY_LEFT = 10;
	public static int NONE = 11;
	public static int PICK_UP_CONTAINER_INYARD = 12;
	public static int DELIVERY_CONTAINER_INYARD = 13;
//	public static int TR_PICK_UP_LEFT = 14;
//	public static int TR_DELIVERY_LEFT = 15;*/
	public static int UNPROCESS = 1;
	public static int PROCESSING = 2;
	public static int INYARD = 3;
	public static int LEAVING = 4;
	public static int LEFT = 5;
	public CheckInMainDocumentsStatusTypeKey() 
	{
		row = new DBRow();
//		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_UNPROCESS), "取货未处理");
//		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_PROCESSING), "取货处理中");
//		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_INYARD), "取货完成  InYard");
//		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_LEAVING), "取货完成  Leaving");
//		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_LEFT), "取货完成  Left");
//		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_UNPROCESS), "送货未处理");
//		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_PROCESSING), "送货处理中");
//		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_INYARD), "送货完成  InYard");
//		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_LEAVING), "送货完成  Leaving");
//		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_LEFT), "送货完成   Left");
/**		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_UNPROCESS), "Pick up:Open ");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_PROCESSING), "Pick up:Processing");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_INYARD), "Pick up:Closed  InYard");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_LEAVING), "Pick up:Closed  Leaving");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_LEFT), "Pick up:Closed  Left");
//		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.TR_PICK_UP_LEFT), "Pick up:Left");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_UNPROCESS), "Delivery:Open");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_PROCESSING), "Delivery:Processing");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_INYARD), "Delivery:Closed  InYard");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_LEAVING), "Delivery:Closed  Leaving");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_LEFT), "Delivery:Closed  Left");
//		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.TR_DELIVERY_LEFT), "Delivery:Left");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.NONE), "");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.PICK_UP_CONTAINER_INYARD), "Pick up:Trailer InYard");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.DELIVERY_CONTAINER_INYARD), "Delivery:Trailer InYard");*/
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.UNPROCESS), "Unprocess");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.PROCESSING), "Processing");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.INYARD), "Processed");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.LEAVING), "Leaving");
		row.add(String.valueOf(CheckInMainDocumentsStatusTypeKey.LEFT), "Left");
 
	}

	public ArrayList getContainerTypeKeys() {
		return (row.getFieldNames());
	}

	public String getContainerTypeKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getContainerTypeKeyValue(String id) {
		return (row.getString(id));
	}

}
