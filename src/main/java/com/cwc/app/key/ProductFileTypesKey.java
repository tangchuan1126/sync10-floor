package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProductFileTypesKey {
	
	DBRow row;
	public static int packet	= 1 ; //商品包装
	public static int self		= 2 ; //商品本身
	public static int bottom	= 3 ; //商品底贴
	public static int weight	= 4 ; //商品称重
	public static int tags		= 5 ; //商品标签
 
	public ProductFileTypesKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ProductFileTypesKey.packet), "商品包装");
		row.add(String.valueOf(ProductFileTypesKey.self), "商品本身");
		row.add(String.valueOf(ProductFileTypesKey.bottom), "商品底贴");
		row.add(String.valueOf(ProductFileTypesKey.weight), "商品称重");
		row.add(String.valueOf(ProductFileTypesKey.tags), "商品标签");
	}
	
	public ArrayList getProductFileTypesKeys()
	{
		return(row.getFieldNames());
	}

	public String getProductFileTypesKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getProductFileTypesKeyValue(String id)
	{
		return(row.getString(id));
	}
}
