package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class StorageReturnProductStatusKey
{
	DBRow row;
	
	public static int NONE_FINISH = 1;	//新创建
	public static int WAITING = 2	; //等待退货
	public static int LITTLE_PRODUCT = 3; //部分退货
	public static int RETURN_PRODUCT_HANDLE_FINISH = 4 ; //退件处理完成
	public static int FINISH  = 5 ; // 退货完成.
 
	public StorageReturnProductStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(StorageReturnProductStatusKey.NONE_FINISH), "新创建");
		row.add(String.valueOf(StorageReturnProductStatusKey.WAITING), "等待退货");
		row.add(String.valueOf(StorageReturnProductStatusKey.LITTLE_PRODUCT), "部分退货");
		row.add(String.valueOf(StorageReturnProductStatusKey.RETURN_PRODUCT_HANDLE_FINISH), "退件处理完成");
		row.add(String.valueOf(StorageReturnProductStatusKey.FINISH), "退货完成");
 
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
