package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;
//优先保留原则
public class FirstOutTypeKey
{
	DBRow row;
	public static int FIFO = 1;	//先进先出
	public static int SNFO = 2 ; //序列号优先
 
	public FirstOutTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(FirstOutTypeKey.FIFO), "FIFO");
		row.add(String.valueOf(FirstOutTypeKey.SNFO), "SNFO");
		
	}
	
	public ArrayList getFirstOutTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getFirstOutTypeKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
