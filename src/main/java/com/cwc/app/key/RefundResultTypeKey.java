package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;
/**
 * 退款的状态(1:申请.2同意退款.3驳回.4否决)
 * @author Administrator
 *
 */
public class RefundResultTypeKey {

	DBRow row;
	
	public static int APPLY = 1;	 	 //退款申请中
	public static int AGREE = 2 ; 		 //退款同意
	public static int REJECT = 3 ; 		 //否决
	public static int VETO = 4 ; 		 //否决
	
	 
	
	public RefundResultTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(RefundResultTypeKey.APPLY), "申请退款中");
		row.add(String.valueOf(RefundResultTypeKey.AGREE), "同意退款");
		row.add(String.valueOf(RefundResultTypeKey.REJECT), "否决退款");
		row.add(String.valueOf(RefundResultTypeKey.VETO), "否决退款");
	 
	}
	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
