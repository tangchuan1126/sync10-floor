package com.cwc.app.key;

import java.util.ArrayList;
import com.cwc.db.DBRow;

public class HandStatusleKey
{
	public static int NORMAL = 0;
	public static int DOUBT = 1;
	public static int ARCHIVE = 2;
	public static int CANCEL = 4;
	
	public static int DOUBT_ADDRESS = 5;
	public static int DOUBT_PAY = 7;
	public static int HANG_UP = 8;
	public static int PRINT_DOUBT = 9;
	public static int ADDITION_MONEY = 10;


    public HandStatusleKey()
    {
        row = new DBRow();
        row.add(String.valueOf(HandStatusleKey.NORMAL), "正常");
        row.add(String.valueOf(HandStatusleKey.DOUBT), "<font color=red>疑问订单</font>");
        row.add(String.valueOf(HandStatusleKey.ARCHIVE), "<font color=blue>归档</font>");
        row.add(String.valueOf(HandStatusleKey.CANCEL), "<font color=red>取消</font>");
        
        row.add(String.valueOf(HandStatusleKey.DOUBT_ADDRESS), "<font color=red>疑问地址</font>");
        row.add(String.valueOf(HandStatusleKey.DOUBT_PAY), "<font color=red>疑问付款</font>");
        row.add(String.valueOf(HandStatusleKey.HANG_UP), "<font color=red>挂起</font>");
        row.add(String.valueOf(HandStatusleKey.PRINT_DOUBT), "<font color=red>打印疑问</font>");
        row.add(String.valueOf(HandStatusleKey.ADDITION_MONEY), "<font color=red>补钱</font>");
    }

    public ArrayList getOrderHandle()
    {
        return row.getFieldNames();
    }

    public String getOrderHandleById(String id)
    {
        return row.getString(id);
    }

    DBRow row;

    
    
}
