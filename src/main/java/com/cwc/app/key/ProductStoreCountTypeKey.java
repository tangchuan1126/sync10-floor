package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProductStoreCountTypeKey {
DBRow row;
	   // 拆分中
	public static int STORECOUNT = 1;
	public static int DAMAGED = 2;
	
	public ProductStoreCountTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ProductStoreCountTypeKey.STORECOUNT),"正常商品");			
		row.add(String.valueOf(ProductStoreCountTypeKey.DAMAGED),"残损商品");
	}

	public ArrayList getProductStoreCountTypeKey()
	{
		return(row.getFieldNames());
	}

	public String getProductStoreCountTypeKeyId(String id)
	{
		return(row.getString(id));
	}
}
