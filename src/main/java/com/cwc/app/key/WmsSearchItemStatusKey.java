package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WmsSearchItemStatusKey {

DBRow row;
	
	public static int ITEM_UNIT_CORRECT = 1;
	public static int ITEM_UNIT_NOT_EXIST = 2;
	public static int ITEM_UNIT_MORE = 3;	
	
	public WmsSearchItemStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WmsSearchItemStatusKey.ITEM_UNIT_CORRECT), "A CustomerId is only one configuration of the ItemId");
		row.add(String.valueOf(WmsSearchItemStatusKey.ITEM_UNIT_NOT_EXIST), "A CustomerId does not exist Configuration Information of the ItemId");
		row.add(String.valueOf(WmsSearchItemStatusKey.ITEM_UNIT_MORE), "A CustomerId has more than one configuration of the ItemId");
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}

}
