package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class SpaceRelationTypeKey
{
	DBRow row;

	public static int Equipment = 1;
	public static int Task = 2;
	

	public SpaceRelationTypeKey() 
	{
		row = new DBRow();
		
		row.add(String.valueOf(SpaceRelationTypeKey.Equipment),"设备");
		row.add(String.valueOf(SpaceRelationTypeKey.Task),"任务");
	}

	public ArrayList getOrderHandle()
	{
		return(row.getFieldNames());
	}

	public String getOrderHandleById(String id)
	{
		return(row.getString(id));
	}
}
