package com.cwc.app.key;

import java.util.List;

import com.cwc.db.DBRow;

public class ProdurePeriodStatusKey {

	DBRow row;
	public static int NOT_NEED = 1;
	public static int NEED = 2;
	public static int MIDDLE = 3;
	public static int FINISH = 4;
	
	public ProdurePeriodStatusKey()
	{
		row = new DBRow();
		row.add(String.valueOf(ProdurePeriodStatusKey.NOT_NEED), "无需");
		row.add(String.valueOf(ProdurePeriodStatusKey.NEED), "需要");
		row.add(String.valueOf(ProdurePeriodStatusKey.MIDDLE), "中");
		row.add(String.valueOf(ProdurePeriodStatusKey.FINISH), "完成");
	}
	
	public List getProdureNoticeStatusKeys()
	{
		return row.getFieldNames();
	}
	
	public String getProdureNoticeStatusKeyName(int id)
	{
		return row.getString(String.valueOf(id));
	}
	
	public String getProdureNoticeStatusKeyName(String id)
	{
		return row.getString(id);
	}
}
