package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProductRelatesKey {
	DBRow row;
	public static int PC_LINE = 1;
	public static int PC_CATAGORY = 2;
	public static int PC_ADMIN = 3;
	
	public ProductRelatesKey()
	{
		row = new DBRow();
		row.add(String.valueOf(ProductRelatesKey.PC_LINE), "产品线");
		row.add(String.valueOf(ProductRelatesKey.PC_CATAGORY), "产品分类");
		row.add(String.valueOf(ProductRelatesKey.PC_ADMIN), "产品所属用户");
	}
	
	public ArrayList getProductRelatesKeys()
	{
		return(row.getFieldNames());
	}

	public String getProductRelatesKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
