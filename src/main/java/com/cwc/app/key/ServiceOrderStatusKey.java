package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ServiceOrderStatusKey {
	
	DBRow row;
	
	public static int WAITING = 8;			//等待处理   
	public static int WAITING_RETURN = 3;	//等待退货   
	public static int VIRTUAL_RETURN = 10;	//虚拟退货   
	public static int NOT_NEED_RETURN = 11;	//无需退货   
	public static int FINISH_HANDLE	=1;		//退货处理完成
	public static int LITTLE_PRODUCT = 7;	 //部分退货
	public static int FINISH_RETURN = 4;	//完成退货
	public static int WAITING_PAY = 5;		//等待付款   
	public static int CREATEORDER = 6;		//创建账单时，售后承担，创建订单
	public static int FINISH_PAY = 12;		//付款完成
	public static int FINISH = 9;			//完成
	public static int CANCEL = 2;			//取消
	
	public ServiceOrderStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ServiceOrderStatusKey.WAITING),"<font color='red'><strong>等待处理</strong></font>");
		row.add(String.valueOf(ServiceOrderStatusKey.WAITING_RETURN),"<font color='#ff6600'><strong>等待退货</strong></font>");
		row.add(String.valueOf(ServiceOrderStatusKey.VIRTUAL_RETURN),"<font color=green><strong>虚拟退货</strong></font>");
		row.add(String.valueOf(ServiceOrderStatusKey.NOT_NEED_RETURN),"<font color=green><strong>无需退货</strong></font>");
		row.add(String.valueOf(ServiceOrderStatusKey.FINISH_HANDLE), "退货处理完成");
		row.add(String.valueOf(ServiceOrderStatusKey.LITTLE_PRODUCT), "部分退货");
		row.add(String.valueOf(ServiceOrderStatusKey.FINISH_RETURN),"<font color=blue><strong>完成退货</strong></font>");
		row.add(String.valueOf(ServiceOrderStatusKey.WAITING_PAY),"<font color='#000099'><strong>等待付款</strong></font>");
		row.add(String.valueOf(ServiceOrderStatusKey.CREATEORDER), "售后承担");
		row.add(String.valueOf(ServiceOrderStatusKey.FINISH_PAY),"<font color='#cc3300'><strong>完成付款</strong></font>");
		row.add(String.valueOf(ServiceOrderStatusKey.FINISH),"完成");
		row.add(String.valueOf(ServiceOrderStatusKey.CANCEL),"取消");
	}
	
	public ArrayList getServiceOrderStatus()
	{
		return(row.getFieldNames());
	}

	public String getServiceOrderStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}

	public String getServiceOrderStatusById(String id)
	{
		return(row.getString(id));
	}
}
