package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WaybillTrackeTypeKey
{
	DBRow row;
	
	public static int StockoutTrack = 1;
	public static int OverdueSendTrack = 2;
	public static int PackingTrack = 3;

	public WaybillTrackeTypeKey()
	{
		row = new DBRow();
		row.add(String.valueOf(WaybillTrackeTypeKey.StockoutTrack), "缺货跟进");
		row.add(String.valueOf(WaybillTrackeTypeKey.OverdueSendTrack), "超期未发货跟进");
		row.add(String.valueOf(WaybillTrackeTypeKey.PackingTrack), "包装跟进");
	}

	public ArrayList getWaybillTrackeType()
	{
		return(row.getFieldNames());
	}

	public String getWaybillTrackeType(String id)
	{
		return(row.getString(id));
	}
}
