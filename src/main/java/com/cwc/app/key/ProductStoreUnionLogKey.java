package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProductStoreUnionLogKey
{
	DBRow row;
	public static int Combination = 1;	//组合套装
	public static int Split = 2 ; //拆散套装
 
	public ProductStoreUnionLogKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ProductStoreUnionLogKey.Combination), "供应商");
		row.add(String.valueOf(ProductStoreUnionLogKey.Split), "资金申请");
	}
	
	public ArrayList getProductStoreUnionLogKeys()
	{
		return(row.getFieldNames());
	}

	public String getProductStoreUnionLogKey(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
