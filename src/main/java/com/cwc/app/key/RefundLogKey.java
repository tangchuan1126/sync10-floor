package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class RefundLogKey {
	DBRow row;
	
	public static int YES = 1;
	public static int NO = 2;

	public RefundLogKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(RefundLogKey.YES),"退款成功");
		row.add(String.valueOf(RefundLogKey.NO),"<font color='#4a8ebc'>退款失败</font>");
		
	}
	
	public ArrayList getQuoteStatus()
	{
		return(row.getFieldNames());
	}

	public String getQuoteStatusById(String id)
	{
		return(row.getString(id));
	}
}
