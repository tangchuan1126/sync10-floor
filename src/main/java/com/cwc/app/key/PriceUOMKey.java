package com.cwc.app.key;

import java.util.ArrayList;
import java.util.List;

import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;

public class PriceUOMKey {

	DBRow row;

	public static int RMB = 1;
	public static int USD = 2;

	public PriceUOMKey() {
		row = new DBRow();
		row.add(String.valueOf(PriceUOMKey.USD), "USD");
		row.add(String.valueOf(PriceUOMKey.RMB), "RMB");

	}
	
	public DBRow[] getKey() {
		
		List<?> list = this.getMoneyUOMKeys();
		
		DBRow[] rowArray = new DBRow[list.size()];
		
		for(int i = 0;i<list.size();i++){
			
			DBRow dbrow = new DBRow();
			dbrow.put("id", Integer.valueOf(list.get(i).toString()));
			dbrow.put("name", this.getMoneyUOMKey(Integer.valueOf(list.get(i).toString())));
			
			rowArray[i] = dbrow;
		}
		
		return rowArray;
	}

	public ArrayList<?> getMoneyUOMKeys() {
		return (row.getFieldNames());
	}

	public String getMoneyUOMKey(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public int getMoneyUOMKeyByValue(String value) {
		
		int reInt = 0;
		
		if (!StrUtil.isBlank(value)) {
			
			ArrayList<?> names = this.getMoneyUOMKeys();
			
			for (Object object : names) {
				
				int key = Integer.parseInt(String.valueOf(object));
				String values = this.getMoneyUOMKey(key);
				if (value.toUpperCase().equals(values.toUpperCase())) {
					reInt = key;
					break;
				}
			}
		}
		return reInt;
	}
}
