package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class DocLevelKey {
	DBRow row;
	
	public static int LEVEL1 = 1;
	public static int LEVEL2 = 2;
	public static int LEVEL3 = 3;
	
	public DocLevelKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(DocLevelKey.LEVEL1), "1");
		row.add(String.valueOf(DocLevelKey.LEVEL2), "2");
		row.add(String.valueOf(DocLevelKey.LEVEL3), "3");
	}
	
	public ArrayList getDocLevelKeyKeys()
	{
		return(row.getFieldNames());
	}

	public String getDocLevelKeyByKey(String id)
	{
		return(row.getString(id));
	}
	
	public String getDocLevelKeyByKey(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
}
