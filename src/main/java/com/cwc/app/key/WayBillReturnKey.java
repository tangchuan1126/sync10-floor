package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WayBillReturnKey {
DBRow row;
	
	public static int ADDRESSERROR = 1;		//地址错误
	public static int WEIGHTERROR = 2;		//重量错误
	public static int CUSTOMSRETURN = 3;	//海关退货
	public static int CALLOFF = 4;			//我方截货
	public static int DELIVERYDAMAGED = 5;	//运输残损
	public static int DELIVERYLOST = 6;		//运输丢失
	
	public WayBillReturnKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WayBillReturnKey.ADDRESSERROR), "待打印");
		row.add(String.valueOf(WayBillReturnKey.WEIGHTERROR), "已打印");
		row.add(String.valueOf(WayBillReturnKey.CUSTOMSRETURN), "已发货");
		row.add(String.valueOf(WayBillReturnKey.CALLOFF), "取消");
		row.add(String.valueOf(WayBillReturnKey.DELIVERYDAMAGED), "运输残损");
		row.add(String.valueOf(WayBillReturnKey.DELIVERYLOST), "运输丢失");
	}

	public ArrayList getWayBillReturnKey()
	{
		return(row.getFieldNames());
	}

	public String getWayBillReturnKey(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
