package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ReturnProductSourceKey
{
	DBRow row;
	
	public static int Amazon = 1;				//amozon
	public static int CUSTOMER_POST_BACK = 2 ; //客户退回
	public static int CUSTOMER_REJECTION = 3 ; //客户拒收
	public static int STORAGE_CLEAN = 4 	; //仓库清理
	public static int NON_DELIVERY = 5;		// 无法投递
 
	
	public ReturnProductSourceKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnProductSourceKey.Amazon), "Amazon");
		row.add(String.valueOf(ReturnProductSourceKey.CUSTOMER_POST_BACK), "客户退回");
		row.add(String.valueOf(ReturnProductSourceKey.CUSTOMER_REJECTION), "客户拒收");
		row.add(String.valueOf(ReturnProductSourceKey.STORAGE_CLEAN), "仓库清理");
		row.add(String.valueOf(ReturnProductSourceKey.NON_DELIVERY), "无法投递");
		
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
