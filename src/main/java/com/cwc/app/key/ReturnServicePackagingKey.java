package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ReturnServicePackagingKey {
	
	DBRow row;
	public static int ORIGINAL_BOX = 1;	//Original Box
	public static int ORIGINAL_CUSHION  = 2 ; //Original Cushion
 
	public ReturnServicePackagingKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnServicePackagingKey.ORIGINAL_BOX), "Original Box");
		row.add(String.valueOf(ReturnServicePackagingKey.ORIGINAL_CUSHION), "Original Cushion");
		
	}
	
	public ArrayList getReturnServicePackagingKeys()
	{
		return(row.getFieldNames());
	}

	public String getReturnServicePackagingKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getReturnServicePackagingKeyById(String id)
	{
		return(row.getString(id));
	}
}
