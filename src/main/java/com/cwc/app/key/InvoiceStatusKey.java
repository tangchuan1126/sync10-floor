package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class InvoiceStatusKey {
	    DBRow row;
	    public static int CLOSED = 3;
	    public static int OPEN = 2;
	    public static int IMPORTED = 1;

	    public InvoiceStatusKey()
	    {
	        row = new DBRow();
	        row.add(String.valueOf(IMPORTED), "IMPORTED");
	        row.add(String.valueOf(OPEN), "OPEN");
	        row.add(String.valueOf(CLOSED), "CLOSED");
	    }

	    public ArrayList<?> getStatus()
	    {
	        return row.getFieldNames();
	    }

	    public String getStatusById(int id)
	    {
	        return row.getString(String.valueOf(id));
	    }
         
	    public DBRow getRow()
	    {
	        return row;
	    }
	   


}
