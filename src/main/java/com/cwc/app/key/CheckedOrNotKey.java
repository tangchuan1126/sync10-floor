package com.cwc.app.key;

import java.util.List;

import com.cwc.db.DBRow;

public class CheckedOrNotKey {

	DBRow row;
	public static int CHECKED = 1;
	public static int NOT_CHECKED = 2;
	
	public CheckedOrNotKey()
	{
		row = new DBRow();
		row.add(String.valueOf(CheckedOrNotKey.CHECKED), "选中");
		row.add(String.valueOf(CheckedOrNotKey.NOT_CHECKED), "不选中");
	}
	
	public List getCheckedOrNotKeys()
	{
		return row.getFieldNames();
	}
	
	public String getCheckedOrNotKeyName(int id)
	{
		return row.getString(String.valueOf(id));
	}
	
	public String getCheckedOrNotKeyName(String id)
	{
		return row.getString(id);
	}
}
