package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ImportErrorKey {
	DBRow row;
	
	public static int PCODEHAS = 1;
	public static int PNAMEHAS = 2;
	public static int SELFPCODEHAS = 3;
	public static int SELFPNAMEHAS = 4;
	public static int UNITNAMENULL = 5;
	public static int GROSSPROFITNULL = 6;
	public static int GROSSPROFITERROR = 7;
	public static int WEIGHTNULL = 8;
	public static int WEIGHTERROR = 9;
	public static int PRICENULL = 10;
	public static int PRICENERROR = 11;
	public static int PRODUCTNULL = 12;
	public static int SELFPRODUCTHAS = 13;
	public static int ACCESSORIESNULL= 14;
	public static int QUANTITYNULL = 15;
	public static int QUANTITYERROR = 16;
	public static int CANNOTACCESSORIES = 17;
	public static int CANNOTPRODUCT = 18;
	public static int PCODEEMPTY = 19;
	public static int PNAMEEMPTY = 20;
	public static int PRODUCTEMPTY = 21;
	public static int ACCESSORIESEMPTY = 22;
	public static int CATALOGERROR = 23;
	public static int CATALOGTYPEERROR = 24;
	public static int QUANTIZERO = 25;
	public static int WEIGHTZERO = 26;
	public static int PRICEZERO = 27;
	public static int GROSSPROFITZERO = 28;
	public static int VOLUMENULL = 29;
	public static int VOLUMEZERO = 30;
	public static int VOLUMEERROR = 31;
	public static int ALIVENULL = 32;
	public static int ALIVEERROR = 33;
	public static int PRODUCTSAMEACCESSORIES = 34;
	public static int ACCESSORIESNOTUPDATE = 35;
	public static int PRODUCTNOTUPDATE = 36;
	

	public ImportErrorKey() 
	{
		row = new DBRow();
		
		//套装、散件公用
		row.add(String.valueOf(ImportErrorKey.PCODEHAS),"<font color=red>此商品条码已被使用</font>");
		row.add(String.valueOf(ImportErrorKey.PNAMEHAS),"<font color=red>此商品名已被使用</font>");
		row.add(String.valueOf(ImportErrorKey.SELFPCODEHAS),"<font color=red>与文件内商品条码重复</font>");
		row.add(String.valueOf(ImportErrorKey.SELFPNAMEHAS),"<font color=red>与文件内商品名称重复</font>");
		row.add(String.valueOf(ImportErrorKey.UNITNAMENULL),"<font color=red>该商品单位为空</font>");
		row.add(String.valueOf(ImportErrorKey.CATALOGERROR),"<font color=red>系统无此产品分类，请对照商品分类填写</font>");
		row.add(String.valueOf(ImportErrorKey.CATALOGTYPEERROR),"<font color=red>产品分类ID格式不正确，只可填写数字</font>");
		row.add(String.valueOf(ImportErrorKey.PCODEEMPTY),"<font color=red>商品条码没有填写</font>");
		row.add(String.valueOf(ImportErrorKey.PNAMEEMPTY),"<font color=red>商品名称没有填写</font>");
		row.add(String.valueOf(ImportErrorKey.VOLUMENULL),"<font color=red>商品的体积没有填写</font>");
		row.add(String.valueOf(ImportErrorKey.VOLUMEZERO),"<font color=red>商品的体积小于0</font>");
		row.add(String.valueOf(ImportErrorKey.VOLUMEERROR),"<font color=red>商品的体积格式不正确</font>");
		
		row.add(String.valueOf(ImportErrorKey.ALIVENULL),"<font color=red>必须填写商品是否可抄单</font>");
		row.add(String.valueOf(ImportErrorKey.ALIVEERROR),"<font color=red>填写值不正确(1可抄单/0不可抄单)</font>");
		
		
		//散件独有检查
		row.add(String.valueOf(ImportErrorKey.GROSSPROFITNULL),"<font color=red>该商品毛利率为空</font>");
		row.add(String.valueOf(ImportErrorKey.GROSSPROFITERROR),"<font color=red>该商品毛利率格式不正确</font>");
		row.add(String.valueOf(ImportErrorKey.GROSSPROFITZERO),"<font color=red>该商品毛利率小于0</font>");
		row.add(String.valueOf(ImportErrorKey.WEIGHTNULL),"<font color=red>该商品的重量为空</font>");
		row.add(String.valueOf(ImportErrorKey.WEIGHTERROR),"<font color=red>该商品的重量格式不正确</font>");
		row.add(String.valueOf(ImportErrorKey.WEIGHTZERO),"<font color=red>该商品的重量为无效值（0或负数）</font>");
		row.add(String.valueOf(ImportErrorKey.PRICENULL),"<font color=red>该商品的单价必须填写</font>");
		row.add(String.valueOf(ImportErrorKey.PRICENERROR),"<font color=red>商品的单价格式不正确</font>");
		row.add(String.valueOf(ImportErrorKey.PRICEZERO),"<font color=red>该商品的单价不大于0</font>");
		
		//关系检查
		row.add(String.valueOf(ImportErrorKey.PRODUCTNULL),"<font color=red>该套装条码不属于任何商品</font>");
		row.add(String.valueOf(ImportErrorKey.SELFPRODUCTHAS),"<font color=red>与文件内其他套装条码重复</font>");
		row.add(String.valueOf(ImportErrorKey.ACCESSORIESNULL),"<font color=red>该配件条码不属于任何商品</font>");
		row.add(String.valueOf(ImportErrorKey.QUANTITYNULL),"<font color=red>组合所需数量不可不填</font>");
		row.add(String.valueOf(ImportErrorKey.QUANTITYERROR),"<font color=red>组合所需数量格式不正确</font>");
		row.add(String.valueOf(ImportErrorKey.QUANTIZERO),"<font color=red>组合所需数量为0</font>");
		row.add(String.valueOf(ImportErrorKey.CANNOTACCESSORIES),"<font color=red>套装商品不可作为其他商品的配件</font>");
		row.add(String.valueOf(ImportErrorKey.CANNOTPRODUCT),"<font color=red>此商品为其他商品配件，不可组合成套装</font>");
		row.add(String.valueOf(ImportErrorKey.PRODUCTEMPTY),"<font color=red>套装条码没有填写</font>");
		row.add(String.valueOf(ImportErrorKey.ACCESSORIESEMPTY),"<font color=red>配件条码没有填写</font>");
		
		row.add(String.valueOf(ImportErrorKey.PRODUCTSAMEACCESSORIES),"<font color=red>套装内以有此配件</font>");
		row.add(String.valueOf(ImportErrorKey.ACCESSORIESNOTUPDATE),"<font color=red>配件条码将被修改，请修改套装关系内的配件条码</font>");
		row.add(String.valueOf(ImportErrorKey.PRODUCTNOTUPDATE),"<font color=red>套装条码将被修改，请修改套装关系内的套装条码</font>");
		
		
		
	}
	
	public ArrayList getErrorMessage()
	{
		return(row.getFieldNames());
	}

	public String getErrorMessageById(String id)
	{
		return(row.getString(id));
	}
}
