package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ClearanceKey {
	DBRow row;
	
	public static int NOCLEARANCE = 1;	//无清关
	public static int CLEARANCE = 2;//清关
	public static int CLEARANCEING = 3;//清关中
	public static int FINISH = 4;//清关完成
	public static int CHECKCARGO=5; // 查货中;
	public ClearanceKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ClearanceKey.NOCLEARANCE), "无需清关");
		row.add(String.valueOf(ClearanceKey.CLEARANCE), "需要清关");
		row.add(String.valueOf(ClearanceKey.CLEARANCEING), "清关中");
		row.add(String.valueOf(ClearanceKey.CHECKCARGO), "查货中");
		row.add(String.valueOf(ClearanceKey.FINISH), "清关完成");
	}
	
	public ArrayList<String> getStatuses()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
