package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ReturnProductCheckItemTypeKey {

	DBRow row;
	
	public static int OUTCHECK = 1;	//无需测试
	public static int CHECK = 2; //测试中
	public static int CHECK_FINISH  = 3 ; //测试完成
	public ReturnProductCheckItemTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnProductCheckItemTypeKey.OUTCHECK), "无需测试");
		row.add(String.valueOf(ReturnProductCheckItemTypeKey.CHECK), "测试中");
		row.add(String.valueOf(ReturnProductCheckItemTypeKey.CHECK_FINISH), "测试完成");
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
