package com.cwc.app.key;

import com.cwc.db.DBRow;

public class PurchaseTransportPageTypesKey {
	
	DBRow row;
	public static int SEND_ADDRESS = 1;
	public static int DELIVER_ADDRESS = 2;
	public static int OTHERS = 3;
	public static int TRANSPORT_DETAILS = 4;
	public static int PROCESS_INFO = 5;
	public static int FREGHT_COMPANY = 6;
	public static int FREGHT_COST = 7;
	
	public PurchaseTransportPageTypesKey()
	{
		row = new DBRow();
		row.add(String.valueOf(PurchaseTransportPageTypesKey.SEND_ADDRESS), "提货地址");
		row.add(String.valueOf(PurchaseTransportPageTypesKey.DELIVER_ADDRESS), "收货地址");
		row.add(String.valueOf(PurchaseTransportPageTypesKey.OTHERS), "其他");
		row.add(String.valueOf(PurchaseTransportPageTypesKey.TRANSPORT_DETAILS), "货物列表");
		row.add(String.valueOf(PurchaseTransportPageTypesKey.PROCESS_INFO), "流程指派");
		row.add(String.valueOf(PurchaseTransportPageTypesKey.FREGHT_COMPANY), "运输设置");
		row.add(String.valueOf(PurchaseTransportPageTypesKey.FREGHT_COST), "运费设置");
	}

	public String getPurchaseTransportPageTypesKey(int id){
		return row.getString(String.valueOf(id));
	}
}
