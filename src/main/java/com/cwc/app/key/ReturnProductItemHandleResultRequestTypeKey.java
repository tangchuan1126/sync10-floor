package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**
 * 退货商品的处理方式
 * @author Administrator
 *
 */
public class ReturnProductItemHandleResultRequestTypeKey {

	DBRow row;
	
	public static int Direct_StockIn = 1; //直接入库
	public static int Destroy = 2 ; //直接销毁
	public static int StockInAfterTest = 3 ; //测试后入库
	public static int StockInAfterTestOk = 4 ; // 测试好后入库
	public static int DestroyAfterTestBad = 5 ; //测试坏销毁
	public static int DestoryAfterTest = 6 ; //测试后销毁
	 
	public ReturnProductItemHandleResultRequestTypeKey() 
	{
	 
		row = new DBRow();
		row.add(String.valueOf(ReturnProductItemHandleResultRequestTypeKey.Direct_StockIn), "直接入库");
		row.add(String.valueOf(ReturnProductItemHandleResultRequestTypeKey.Destroy), "直接销毁");
		row.add(String.valueOf(ReturnProductItemHandleResultRequestTypeKey.StockInAfterTest), "测试后入库");
		row.add(String.valueOf(ReturnProductItemHandleResultRequestTypeKey.StockInAfterTestOk), "测试好后入库");
		row.add(String.valueOf(ReturnProductItemHandleResultRequestTypeKey.DestroyAfterTestBad), "测试坏销毁");
		row.add(String.valueOf(ReturnProductItemHandleResultRequestTypeKey.DestoryAfterTest), "测试后销毁");
	 
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
