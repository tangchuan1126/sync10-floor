package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class CrmTraceTypeKey
{
	DBRow row;
	
	public static int EMAIL = 1;
	public static int PHONE = 2;
	public static int IM = 3;
	public static int QUOTE = 4;
	public static int SPECIAL = 5;
	public static int GIVEUP = 6;
	public static int CANCEL_SPECIAL = 7;  
	
	public CrmTraceTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(CrmTraceTypeKey.EMAIL), "邮件跟进");
		row.add(String.valueOf(CrmTraceTypeKey.PHONE), "电话跟进");
		row.add(String.valueOf(CrmTraceTypeKey.IM), "即时通讯跟进");
		row.add(String.valueOf(CrmTraceTypeKey.QUOTE), "报价");
		row.add(String.valueOf(CrmTraceTypeKey.SPECIAL), "加入<font color='red'>特别关注</font>");
		row.add(String.valueOf(CrmTraceTypeKey.GIVEUP), "放弃");
		row.add(String.valueOf(CrmTraceTypeKey.CANCEL_SPECIAL), "取消<font color='red'>特别关注</font>");
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
