package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class TransferAccountsKey {
	
	DBRow row;
	
	public static int NOT_TRANSFER 	= 0;	//未转账
	public static int FINISH_TRANSFER	= 2;	//转账完成
	
	public TransferAccountsKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(TransferAccountsKey.NOT_TRANSFER), "未转账");
		row.add(String.valueOf(TransferAccountsKey.FINISH_TRANSFER), "转账完成");
	}
	
	public ArrayList getFTransferAccountsKey()
	{
		return(row.getFieldNames());
	}

	public String getTransferAccountsKeyNameById(String status)
	{
		return(row.getString(status));
	}

}
