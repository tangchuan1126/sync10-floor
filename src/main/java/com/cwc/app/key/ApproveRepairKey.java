package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ApproveRepairKey {
	DBRow row;
	
	public static int WAITAPPROVE = 1;		//待审核
	public static int APPROVE = 2;	//已审核
	
	public ApproveRepairKey() 
	{
		row = new DBRow();
	}

	public ArrayList getApproveRepairStatus()
	{
		return(row.getFieldNames());
	}

	public String getApproveRepairStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
