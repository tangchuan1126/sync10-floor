package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class FileWithTypeKey {
	DBRow row;
	
	public final static int ProductReturn = 1;//服务请求
	public final static int delivery = 2;
	public final static int transport = 3;
	public final static int transport_certificate = 4;//转运单单证凭证
	public final static int transport_clearance = 5;	//转运单清关凭证
	public final static int transport_declaration = 6 ;//转运单报关凭证
	public final static int product_file =  7 ;		//商品文件
	public final static int PURCHASE_DRAWBACK = 8;//采购单的退税凭证
	public final static int PURHCASE_QUALITY_INSPECTION = 9;//采购单的质检要求
	public final static int PURCHASE_INVOICE = 11;//采购单的发票凭证
	public final static int PRODUCT_TAG_FILE = 12;//采购单的标签文件
	public final static int PURCHASE_TAG_FILE = 13;//采购单的内部标签文件
	public final static int TRANSPORT_STOCKINSET = 14 ; //转运单运费
	public final static int TRANSPORT_QUALITYINSPECTION = 15 ; //转运单质检
	public final static int TRANSPORT_PRODUCT_TAG_FILE = 16;//转运单的标签文件
	public final static int PURCHASE_PRODUCT_FILE = 17 ; //采购单商品图片
	public final static int TRANSPORT_TAG = 18 ; //转运单内部标签
	public final static int assets_receive =19 ;//资产收货凭证
	public final static int REPAIR_CLEARANCE = 20;	//返修单清关凭证
	public final static int REPAIR_DECLARATION = 21 ;//返修单报关凭证
	public final static int REPAIR_STOCKINSET = 22 ; //返修单运费
	public final static int REPAIR_QUALITYINSPECTION = 23 ; //返修单质检
	public final static int REPAIR_TAG = 24 ; //返修单制签
	public final static int REPAIR_PRODUCT_FILE = 25;//返修单商品文件
	public final static int REPAIR_PRODUCT_TAG_FILE = 26;//返修单的标签文件
	
	public final static int APPLY_FUNDS_APPROVE = 27;//资金申请的凭证
	public final static int REPAIR_CERTIFICATE = 28;//返修单单证凭证
	public final static int KNOWLEDGE = 29;			//问题
	public final static int ANSWER = 30;				//答案
	
	public final static int PRODUCT_SELF_FILE = 31 ; //商品信息的图片
	public final static int PURCHASE_THIRD_TAG_FILE = 32;//采购单的第三方标签文件
	public final static int TRANSPORT_THIRD_TAG_FILE = 33;//转运单的第三方标签文件
	public final static int DOUBTGOODS = 34;				//未知商品图片
	
	public final static int RETURN_PRODUCT_FILE = 35 ; //退货单邮寄包装
	public final static int RETURN_PRODUCT_DETAIL = 36 ; //退货单详细图片
	public final static int RETURN_CHECK_PRODUCT = 37 ;
	
	public final static int RETURN_VIRTUAL = 38;//退货单虚拟退货的图片
	public final static int TRANSPORT_RECEIVE = 39 ; //transport 收货
	public final static int TRANSPORT_OUTBOUND = 40 ; // transport 装货图片.
	
	//B2B订单
	public final static int B2B_ORDER_PRODUCT_FILE = 41;//B2B订单商品文件
	public final static int B2B_ORDER_PRODUCT_TAG_FILE = 42;//B2B订单商品标签文件
	public final static int B2B_ORDER_CERTIFICATE = 43;//B2B订单单证凭证
	public final static int B2B_ORDER_CLEARANCE = 44;	//B2B订单清关凭证
	public final static int B2B_ORDER_DECLARATION = 45 ;//B2B订单报关凭证
	public final static int B2B_ORDER_STOCKINSET = 46;//B2B订单运费流程文件
	public final static int B2B_ORDER_TAG = 47;//B2B订单内部标签文件
	public final static int B2B_ORDER_QUALITYINSPECTION = 48;//B2B订单质检文件
	public final static int B2B_ORDER_THIRD_TAG_FILE = 49;//B2B订单第三方标签文件
	
	public final static int B2B_ORDER_RECEIVE = 50 ; //B2B_ORDER 收货
	public final static int B2B_ORDER_OUTBOUND = 51 ; // B2B_ORDER 装货图片.
	public final static int OCCUPANCY_MAIN = 52 ; // 司机签到上传照片.
	
	
	//TMS图片
	public final static int TMS_SORTINT = 58;// 分拣时的图片
	public final static int TMS_LONDING = 56;//装货/装载
	public final static int TMS_UNLONDING = 57;//接收/卸货
	
	//BOL签字的图片
	public final static int BOL_SIGNATURE=60;//签字图片
	public FileWithTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(FileWithTypeKey.ProductReturn), "服务请求");
		row.add(String.valueOf(FileWithTypeKey.delivery), "交货单文件");
		row.add(String.valueOf(FileWithTypeKey.transport), "转运单文件");
		row.add(String.valueOf(FileWithTypeKey.transport_certificate), "转运单单证");
		row.add(String.valueOf(FileWithTypeKey.transport_clearance), "转运单清关凭证");
		row.add(String.valueOf(FileWithTypeKey.transport_declaration), "转运单报关凭证");
		row.add(String.valueOf(FileWithTypeKey.PURCHASE_INVOICE), "采购单的发票凭证");
		row.add(String.valueOf(FileWithTypeKey.PURCHASE_DRAWBACK), "采购单的退税凭证");
		row.add(String.valueOf(FileWithTypeKey.PURHCASE_QUALITY_INSPECTION), "采购单的质检要求");
		row.add(String.valueOf(FileWithTypeKey.product_file), "商品文件");
		row.add(String.valueOf(FileWithTypeKey.PRODUCT_TAG_FILE), "商品的标签文件");
		row.add(String.valueOf(FileWithTypeKey.PURCHASE_TAG_FILE), "采购单的内部标签文件");
		row.add(String.valueOf(FileWithTypeKey.TRANSPORT_STOCKINSET), "转运单运费");
		row.add(String.valueOf(FileWithTypeKey.TRANSPORT_QUALITYINSPECTION), "转运单质检");
		row.add(String.valueOf(FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE), "转运单的标签文件");
		row.add(String.valueOf(FileWithTypeKey.PURCHASE_PRODUCT_FILE), "采购单商品图片");
		row.add(String.valueOf(FileWithTypeKey.TRANSPORT_TAG), "转运单内部标签");
		row.add(String.valueOf(FileWithTypeKey.assets_receive), "固定资产到货凭证");	
		row.add(String.valueOf(FileWithTypeKey.REPAIR_CERTIFICATE), "返修单单证凭证");
		row.add(String.valueOf(FileWithTypeKey.REPAIR_CLEARANCE), "返修单清关凭证");
		row.add(String.valueOf(FileWithTypeKey.REPAIR_DECLARATION), "返修单报关凭证");
		row.add(String.valueOf(FileWithTypeKey.REPAIR_STOCKINSET), "返修单运费");
		row.add(String.valueOf(FileWithTypeKey.REPAIR_QUALITYINSPECTION), "返修单质检");
		row.add(String.valueOf(FileWithTypeKey.REPAIR_TAG), "返修单制签");
		row.add(String.valueOf(FileWithTypeKey.REPAIR_PRODUCT_FILE), "返修单商品文件");
		row.add(String.valueOf(FileWithTypeKey.REPAIR_PRODUCT_TAG_FILE), "返修单商品标签文件");
		
		row.add(String.valueOf(FileWithTypeKey.APPLY_FUNDS_APPROVE), "资金申请的凭证");
		row.add(String.valueOf(FileWithTypeKey.KNOWLEDGE), "问题");
		row.add(String.valueOf(FileWithTypeKey.ANSWER), "答案");
		row.add(String.valueOf(FileWithTypeKey.PRODUCT_SELF_FILE), "商品信息的图片");
		row.add(String.valueOf(FileWithTypeKey.PURCHASE_THIRD_TAG_FILE), "采购单的第三方标签文件");
		row.add(String.valueOf(FileWithTypeKey.TRANSPORT_THIRD_TAG_FILE), "转运单的第三方标签文件");
		row.add(String.valueOf(FileWithTypeKey.DOUBTGOODS), "未知商品图片");
		row.add(String.valueOf(FileWithTypeKey.RETURN_PRODUCT_FILE), "退货单邮寄包装");
		row.add(String.valueOf(FileWithTypeKey.RETURN_PRODUCT_DETAIL), "退货单详细图片");
		row.add(String.valueOf(FileWithTypeKey.RETURN_CHECK_PRODUCT), "退货商品测试报告");
		row.add(String.valueOf(FileWithTypeKey.RETURN_VIRTUAL), "服务单虚拟退货的图片");
		row.add(String.valueOf(FileWithTypeKey.TRANSPORT_RECEIVE), "转运单收货");
		row.add(String.valueOf(FileWithTypeKey.TRANSPORT_OUTBOUND), "转运单装货");
		row.add(String.valueOf(FileWithTypeKey.B2B_ORDER_PRODUCT_FILE), "B2B订单商品文件");
		row.add(String.valueOf(FileWithTypeKey.B2B_ORDER_PRODUCT_TAG_FILE), "B2B订单商品标签文件");
		row.add(String.valueOf(FileWithTypeKey.B2B_ORDER_CERTIFICATE), "B2B订单单证凭证");
		row.add(String.valueOf(FileWithTypeKey.B2B_ORDER_CLEARANCE), "B2B订单清关凭证");
		row.add(String.valueOf(FileWithTypeKey.B2B_ORDER_DECLARATION), "B2B订单报关凭证");
		row.add(String.valueOf(FileWithTypeKey.B2B_ORDER_STOCKINSET), "B2B订单运费流程文件");
		row.add(String.valueOf(FileWithTypeKey.B2B_ORDER_TAG), "B2B订单内部标签文件");
		row.add(String.valueOf(FileWithTypeKey.B2B_ORDER_QUALITYINSPECTION), "B2B订单质检文件");
		row.add(String.valueOf(FileWithTypeKey.B2B_ORDER_THIRD_TAG_FILE), "B2B订单第三方标签文件");
		row.add(String.valueOf(FileWithTypeKey.OCCUPANCY_MAIN), "司机签到上传照片");
		row.add(String.valueOf(FileWithTypeKey.BOL_SIGNATURE), "bol签字的照片");
	}
	
	public ArrayList getFileWithType()
	{
		return(row.getFieldNames());
	}

	public String getFileWithTypeId(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
