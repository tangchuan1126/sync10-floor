package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class DifferentKey
{
	DBRow row;
	public static int Equal = 1;	//相等
	public static int More = 2 ; //多
	public static int Lack = 3 ; //少
 
	public DifferentKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(DifferentKey.Equal), "数量等");
		row.add(String.valueOf(DifferentKey.More), "数量多");
		row.add(String.valueOf(DifferentKey.Lack), "数量少");
		
	}
	
	public ArrayList getDifferentKeys()
	{
		return(row.getFieldNames());
	}

	public String getDifferentKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
