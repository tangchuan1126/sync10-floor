package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderDeclarationKey {
	DBRow row;
	
	public static int NODELARATION = 1;	//无报关
	public static int DELARATION = 2;//有报关
	public static int DELARATING = 3;//报关中
	public static int FINISH = 4;//报关完成
	
	public B2BOrderDeclarationKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderDeclarationKey.NODELARATION), "无需报关");
		row.add(String.valueOf(B2BOrderDeclarationKey.DELARATION), "需要报关");
		row.add(String.valueOf(B2BOrderDeclarationKey.DELARATING), "报关中");
		row.add(String.valueOf(B2BOrderDeclarationKey.FINISH), "报关完成");
	}
	
	public ArrayList<String> getB2BOrderDeclarationKeys()
	{
		return(row.getFieldNames());
	}

	public String getB2BOrderDeclarationKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
