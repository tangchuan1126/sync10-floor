package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ApplyStatusKey 
{
	DBRow row;
	
	public static String NO_APPLY = "0";	//未付款
	public static String PARTIAL_PAYMENT = "1";//部分付款
	public static String FINISH = "2";//已付款
	public static String CANCEL="3";//取消
	public static String CANNOT_TRANSFERS="4";//不可转款，表示尚未进行资金申请
	public static String APPLYED="5";
	public static String NOAPPLICATION="6";
	public static String NORMAL="7";
	public ApplyStatusKey() 
	{
		row = new DBRow();
		row.add(ApplyStatusKey.NO_APPLY, "已申请");
		row.add(ApplyStatusKey.PARTIAL_PAYMENT, "<font color='red'>部分付款</font>");
		row.add(ApplyStatusKey.FINISH, "<font color='green'>已付款</font>");
		row.add(ApplyStatusKey.CANCEL, "<font color='blue'>取消</font>");
		row.add(ApplyStatusKey.CANNOT_TRANSFERS, "未申请");
		row.add(ApplyStatusKey.APPLYED, "申请");
		row.add(ApplyStatusKey.NOAPPLICATION, "未申请");
		row.add(ApplyStatusKey.NORMAL, "正常");
	}

	public ArrayList getApplyTypeStatus()
	{
		return(row.getFieldNames());
	}

	public String getApplyTypeStatusById(String status)
	{
		return(row.getString(status));
	}
	
	public static void main(String[] args) 
	{
	   ApplyStatusKey key=new ApplyStatusKey();
	}
}
