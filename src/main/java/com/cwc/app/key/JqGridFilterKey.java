package com.cwc.app.key;

import java.util.HashMap;
import java.util.List;

import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.beans.jqgrid.RuleBean;


public class JqGridFilterKey
{
	public static HashMap<String,String> map = new HashMap<String, String>();
	
	public JqGridFilterKey() 
	{
		map.put("eq"," = ");
		map.put("ne"," <> ");
		map.put("lt"," < ");
		map.put("le"," <= ");
		map.put("gt"," > ");
		map.put("ge"," >= ");
		
		map.put("cn"," LIKE '%key%'");
		map.put("nc"," NOT LIKE '%key%'");
		
		map.put("bw"," LIKE 'key%'");
		map.put("bn"," NOT LIKE 'key%'");
		
		map.put("ew"," LIKE '%key'");
		map.put("en"," NOT LIKE '%key'");
		
		
		map.put("ni"," NOT IN (key)");
		map.put("in"," IN (key)");
		map.put("nu"," IS NULL");
		map.put("nn"," <> NULL");
	}
	
	public String filterSQL(FilterBean filterBean)
		throws Exception
	{
		try 
		{
			List<RuleBean> list = filterBean.getRules();
			StringBuffer sql = new StringBuffer();
			String on;
			for (int i = 0; i < list.size(); i++) 
			{
				if(i==0)
				{
					on = "and (";
				}
				else
				{
					on = filterBean.getGroupOp();
				}
				if(map.get(list.get(i).getOp()).split("LIKE").length>1)
				{
					if(!list.get(i).getData().equals(""))
					{
						String value = map.get(list.get(i).getOp()).replaceAll("key",list.get(i).getData());//有like的
						
						sql.append(" "+on+" "+list.get(i).getField()+value);
					}
				}
				else if(map.get(list.get(i).getOp()).contains("NULL"))
				{
					sql.append(" "+on+" "+list.get(i).getField()+map.get(list.get(i).getOp()));//不用附加值的
				}
				else
				{
					if(!list.get(i).getData().equals(""))
					{
						sql.append(" "+on+" "+list.get(i).getField()+map.get(list.get(i).getOp())+"'"+list.get(i).getData()+"'");
					}
				}
			}
			sql.append(")");
			if(sql.toString().equals(")"))
			{
				sql = new StringBuffer("");
			}
			return (sql.toString());
		} 
		catch (Exception e) 
		{
			throw new Exception("JqGridFilterKey fillterSQL error"+e);
		}
	}
}
