package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class LableTemplateKey
{
	DBRow row;
	
	public static int PRODUCTLABEL= 1;	//商品标签
	public static int LOCATIONLABEL= 2;//位置标签
	public static int ASSETSLABEL = 3;//资产标签
	public static int OUTSIZELABEL= 4;//外箱商品标签
	public static int SEQUENCELABEL = 5;//序列号标签
	public static int DAMAGEDLABEL = 6;	//残损标签
	public static int DOUBTGOODLABEL = 7;	//未知标签
	public static int SPLITGOODLABEL = 8;	//拆散套装标签
	public static int CHECK_IN_BOL = 9;	//拆散套装标签
	
	
	public LableTemplateKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(LableTemplateKey.PRODUCTLABEL), "商品标签");
		row.add(String.valueOf(LableTemplateKey.LOCATIONLABEL), "位置标签");
		row.add(String.valueOf(LableTemplateKey.ASSETSLABEL), "资产标签");
		row.add(String.valueOf(LableTemplateKey.OUTSIZELABEL), "外箱商品标签");
		row.add(String.valueOf(LableTemplateKey.SEQUENCELABEL), "序列号标签");
		row.add(String.valueOf(LableTemplateKey.DAMAGEDLABEL), "残损件标签");
		row.add(String.valueOf(LableTemplateKey.DOUBTGOODLABEL), "未知商品标签");
		row.add(String.valueOf(LableTemplateKey.SPLITGOODLABEL), "拆散套装标签");
		row.add(String.valueOf(LableTemplateKey.CHECK_IN_BOL), "check in bol 标签");
	}

	public ArrayList getLableTemplates()
	{
		return(row.getFieldNames());
	}

	public String getLableTemplateNameById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
