package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**
 * 目前只给签字用的关联
 * @author zhangrui
 *
 */
public class RelativeFileKey {

	DBRow row;
	
	public static int BiLLOfLoadingShipper = 1;	//billOfLoadingShipper签字
	public static int BillOfLoadingCarrier = 2; //billOfLoadingCarrier签字
	public static int BillOfLoadingInitial = 3;  //billOfLoadingLoadingInitial签字
 

	
	public RelativeFileKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(RelativeFileKey.BiLLOfLoadingShipper), "BiLLOfLoadingShipper");
		row.add(String.valueOf(RelativeFileKey.BillOfLoadingCarrier), "BillOfLoadingCarrier");
		row.add(String.valueOf(RelativeFileKey.BillOfLoadingInitial), "BillOfLoadingInitial");

	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
