package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProductTypeKey
{
	DBRow row;
	//商品类型
	public static int NORMAL = 1; 
	public static int UNION_STANDARD = 2; 
	public static int UNION_STANDARD_MANUAL = 3; 
	public static int UNION_CUSTOM = 4; 
 
	public ProductTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ProductTypeKey.NORMAL), "普通商品");
		row.add(String.valueOf(ProductTypeKey.UNION_STANDARD), "普通套装");
		row.add(String.valueOf(ProductTypeKey.UNION_STANDARD_MANUAL), "手工拼装");
		row.add(String.valueOf(ProductTypeKey.UNION_CUSTOM), "定制套装");
		
	}
	
	public ArrayList getProductTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getProductTypeKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
