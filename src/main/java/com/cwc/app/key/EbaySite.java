package com.cwc.app.key;

import java.util.ArrayList;
import java.util.List;

import com.cwc.db.DBRow;

public class EbaySite {

	DBRow row;
	
	public DBRow getRow() {
		return row;
	}

	public static int EBAY_AU = 15;			 
	public static int EBAY_MOTOR = 100;		 
	public static int EBAY_GB = 3;		 
	public static int EBAY_US = 0; 
 
	public EbaySite() 
	{
		row = new DBRow();
		row.add(String.valueOf(EbaySite.EBAY_AU), "EBAY-AU,eBay Australia");
		row.add(String.valueOf(EbaySite.EBAY_MOTOR), "EBAY-MOTOR,eBay Motors");
		row.add(String.valueOf(EbaySite.EBAY_GB), "EBAY-GB,eBay UK");
		row.add(String.valueOf(EbaySite.EBAY_US), "EBAY-US,eBay United States");
		 
	}

	public ArrayList<String> getStatus()
	{
		return((ArrayList<String>)row.getFieldNames());
	}

	public String getEbaySiteById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	public static void main(String[] args) {
		EbaySite site = new EbaySite();
		List<String> list = site.getStatus();
		for(String s : list){
			 
		}
	}
}
