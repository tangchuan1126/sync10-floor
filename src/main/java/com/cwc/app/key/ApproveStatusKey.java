package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class ApproveStatusKey
{
	DBRow row;
	
	public static int WAITAPPROVE = 1;		//待审核
	public static int APPROVE = 2;	//已审核
	
	public ApproveStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ApproveStatusKey.WAITAPPROVE), "待审核");
		row.add(String.valueOf(ApproveStatusKey.APPROVE), "已审核");
	}

	public ArrayList getApproveStatus()
	{
		return(row.getFieldNames());
	}

	public String getApproveStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
