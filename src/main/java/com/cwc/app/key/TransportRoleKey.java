package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class TransportRoleKey 
{
	DBRow row;
	/**
	 * 发货方 1
	 */
	public static int TRANSPORT_ROLE_SEND = 1;	
	
	/**
	 * 收货方 2
	 */
	public static int TRANSPORT_ROLE_RECEIVE = 2;
		
	public TransportRoleKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(TransportRoleKey.TRANSPORT_ROLE_SEND), "发货方");
		row.add(String.valueOf(TransportRoleKey.TRANSPORT_ROLE_RECEIVE), "收货方");
	}
		
	public ArrayList<String> getRoles()
	{
		return(row.getFieldNames());
	}

	public String getRoleById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}

}
