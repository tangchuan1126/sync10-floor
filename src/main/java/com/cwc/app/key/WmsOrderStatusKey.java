package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WmsOrderStatusKey {
	DBRow row;
	
	public static String CLOSED = "Closed";
	public static String IMPORTED = "Imported";
	public static String OPEN = "Open";
	public static String PICKED = "Picked";
	public static String PICKING = "Picking";
	public static String SHIPPED = "Shipped";
	public static String RECEIVED = "Received";
	public static String STAGING = "Staging";

	
	public WmsOrderStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WmsOrderStatusKey.CLOSED), "Closed");
		row.add(String.valueOf(WmsOrderStatusKey.IMPORTED), "Imported");
		row.add(String.valueOf(WmsOrderStatusKey.OPEN), "Open");
		row.add(String.valueOf(WmsOrderStatusKey.PICKED), "Picked");
		row.add(String.valueOf(WmsOrderStatusKey.PICKING), "Picking");
		row.add(String.valueOf(WmsOrderStatusKey.SHIPPED), "Shipped");
		row.add(String.valueOf(WmsOrderStatusKey.RECEIVED), "Received");
		row.add(String.valueOf(WmsOrderStatusKey.STAGING), "Staging");
	}
	
	public ArrayList getWmsOrderStatusKeys()
	{
		return(row.getFieldNames());
	}

	public String getWmsOrderStatusKeyByKey(String id)
	{
		return(row.getString(id));
	}
	
}
