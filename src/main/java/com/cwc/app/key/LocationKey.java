package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class LocationKey
{
	DBRow row;
	
	
	public LocationKey() 
	{
		row = new DBRow();
		row.add("R","货架——R");
		row.add("T","通道——T");
		row.add("P","托盘——P");
		row.add("N","不规则——N");
	}

	public ArrayList getLocationsType()
	{
		return(row.getFieldNames());
	}

	public String getLoationType(String type)
	{
		return(row.getString(type));
	}
}
