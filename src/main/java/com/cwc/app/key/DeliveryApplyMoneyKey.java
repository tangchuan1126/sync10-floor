package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class DeliveryApplyMoneyKey {
	DBRow row;
	
	public static int NOT_APPLY  = 1;	//未申请
	public static int APPLY = 2;//已申请
	public static int PARTIAL_PAYMENT = 3;//部分付款
	public static int PAYMENT = 4;//全部付款
	
	public DeliveryApplyMoneyKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(DeliveryApplyMoneyKey.NOT_APPLY), "未申请");
		row.add(String.valueOf(DeliveryApplyMoneyKey.APPLY), "已申请");
		row.add(String.valueOf(DeliveryApplyMoneyKey.PARTIAL_PAYMENT), "部分付款");
		row.add(String.valueOf(DeliveryApplyMoneyKey.PAYMENT), "全部付款");
	}
	
	public ArrayList<String> getStatuses()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
