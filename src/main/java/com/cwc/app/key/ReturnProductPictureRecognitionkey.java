package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ReturnProductPictureRecognitionkey {

	DBRow row;
	
	public static int RecognitionNoneFinish = 1;	//图片识别未完成
	public static int RecognitionFinish = 2 ; //图片识别完成
	
	public ReturnProductPictureRecognitionkey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnProductPictureRecognitionkey.RecognitionNoneFinish), "识别未完成");
		row.add(String.valueOf(ReturnProductPictureRecognitionkey.RecognitionFinish), "识别完成");
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
