package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class RepairClearanceKey {
	DBRow row;
	
	public static int NOCLEARANCE = 1;	//无清关
	public static int CLEARANCE = 2;//清关
	public static int CLEARANCEING = 3;//清关中
	public static int FINISH = 4;//清关完成
	public static int CHECKCARGO=5; // 查货中;
	public RepairClearanceKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(RepairClearanceKey.NOCLEARANCE), "无需清关");
		row.add(String.valueOf(RepairClearanceKey.CLEARANCE), "需要清关");
		row.add(String.valueOf(RepairClearanceKey.CLEARANCEING), "清关中");
		row.add(String.valueOf(RepairClearanceKey.CHECKCARGO), "查货中");
		row.add(String.valueOf(RepairClearanceKey.FINISH), "清关完成");
	}
	
	public ArrayList<String> getStatuses()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
