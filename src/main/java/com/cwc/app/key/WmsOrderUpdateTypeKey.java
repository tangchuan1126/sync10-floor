package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**  
 * @author Liang Jie
 * Created on 2015年4月8日
 * 
 */
public class WmsOrderUpdateTypeKey {
	private DBRow row = new DBRow();
	
	public static final int COMMIT = 1;
	public static final int CANCEL = 2;
	public static final int LOADAPPT = 3;
	public static final int EDIT = 4;
	public static final int SO = 5;
	
	
	public WmsOrderUpdateTypeKey() {
		row = new DBRow();
		row.add(String.valueOf(COMMIT), "Commit Inventory");
		row.add(String.valueOf(CANCEL), "Cancel");
		row.add(String.valueOf(LOADAPPT), "Load&Appointment");
		row.add(String.valueOf(EDIT), "Edit");
		row.add(String.valueOf(SO), "Edit SO");
	}

	@SuppressWarnings("rawtypes")
	public ArrayList getValueList()
	{
		return(row.getFieldNames());
	}

	public String getLabelById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public static boolean isValid(int id)
	{
		String label = new WmsOrderUpdateTypeKey().getLabelById(id);
		
		return label != null;
	}
}
