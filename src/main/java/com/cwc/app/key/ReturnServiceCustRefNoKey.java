package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ReturnServiceCustRefNoKey {
	
	DBRow row;
	public static int BL = 1;	//BL
	public static int DOC = 2 ; //DOC
	public static int CLAIM = 3 ; //CLAIM
	public static int OTHER = 4 ; //OTHER
 
	public ReturnServiceCustRefNoKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnServiceCustRefNoKey.BL), "BL");
		row.add(String.valueOf(ReturnServiceCustRefNoKey.DOC), "DOC");
		row.add(String.valueOf(ReturnServiceCustRefNoKey.CLAIM), "CLAIM");
		row.add(String.valueOf(ReturnServiceCustRefNoKey.OTHER), "OTHER");
		
	}
	
	public ArrayList getReturnServiceCustRefNoKeys()
	{
		return(row.getFieldNames());
	}

	public String getReturnServiceCustRefNoKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getReturnServiceCustRefNoKeyById(String id)
	{
		return(row.getString(id));
	}
}
