package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WaybillInternalOperKey
{
	DBRow row;
	
	public static int Create = 1;
	public static int Print = 2;
	public static int Cancel = 3;
	public static int Send = 4;
	public static int Recover = 5;
	public static int ArtificialModification = 6;

	public WaybillInternalOperKey()
	{
		row = new DBRow();
		row.add(String.valueOf(WaybillInternalOperKey.Create), "创建运单");
		row.add(String.valueOf(WaybillInternalOperKey.Print), "打印运单");
		row.add(String.valueOf(WaybillInternalOperKey.Cancel), "取消运单");
		row.add(String.valueOf(WaybillInternalOperKey.Send), "运单发货");
		row.add(String.valueOf(WaybillInternalOperKey.Recover), "恢复运单");
		row.add(String.valueOf(WaybillInternalOperKey.ArtificialModification), "人工修改");
		
	}

	public ArrayList getWaybillInternalOper()
	{
		return(row.getFieldNames());
	}

	public String getWaybillInternalOper(String id)
	{
		return(row.getString(id));
	}
}
