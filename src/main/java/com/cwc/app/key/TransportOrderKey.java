package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class TransportOrderKey
{
	DBRow row;
	
	public static int READY = 1;			//备货中
	public static int INTRANSIT = 2;		//正常运送
	public static int APPROVEING = 3;		//审核中
	public static int FINISH = 4;			//完成
	public static int PACKING = 5;			//装箱中
	public static int NOFINISH = 7;			//未完成
	public static int AlREADYARRIAL = 8;	//已到货
	public static int RECEIVEING = 9;		//收货中
	public static int AlREADYRECEIVE = 10;	//已收货
	public static int CANINSTORE = 11;		//条码机可下载
	public static int CONFIGCHANGEING = 12;	//配置更改中
	
	public TransportOrderKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(TransportOrderKey.READY), "备货中");
		row.add(String.valueOf(TransportOrderKey.PACKING), "<font color='green'>装货中</font>");
		row.add(String.valueOf(TransportOrderKey.INTRANSIT), "<font color='blue'>运输中</font>");
		row.add(String.valueOf(TransportOrderKey.APPROVEING), "<font color='red'>审核中</font>");
		row.add(String.valueOf(TransportOrderKey.FINISH), "已入库");
		row.add(String.valueOf(TransportOrderKey.AlREADYARRIAL), "已到货");
		row.add(String.valueOf(TransportOrderKey.RECEIVEING), "收货中");
		row.add(String.valueOf(TransportOrderKey.NOFINISH), "未完成");
		row.add(String.valueOf(TransportOrderKey.AlREADYRECEIVE),"已收货");
		row.add(String.valueOf(TransportOrderKey.CONFIGCHANGEING),"<font color='#5B4B00'>配置更改中</font>");
	}

	public ArrayList getTransportOrderStatus()
	{
		return(row.getFieldNames());
	}

	public String getTransportOrderStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
