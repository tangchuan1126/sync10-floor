package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProductShapeTypeKey {
	DBRow row;
	
	
	public static int SPLIT = 1;
	public static int ASSEMBLY = 2;
	public static int RENOVATE = 3;
	public static int DAMAGED = 4;
	public static int CONVERT = 5;
	
	
	public ProductShapeTypeKey() 
	{
		row = new DBRow();
		
		row.add(String.valueOf(ProductShapeTypeKey.SPLIT), "拆散");
		row.add(String.valueOf(ProductShapeTypeKey.ASSEMBLY), "拼装");
		row.add(String.valueOf(ProductShapeTypeKey.RENOVATE), "翻新");
		row.add(String.valueOf(ProductShapeTypeKey.DAMAGED), "残损");
		row.add(String.valueOf(ProductShapeTypeKey.CONVERT), "改货");
		
	}
	
	public ArrayList getContainerTypeKeys() {
		return (row.getFieldNames());
	}

	public String getContainerTypeKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getContainerTypeKeyValue(String id) {
		return (row.getString(id));
	}
}
