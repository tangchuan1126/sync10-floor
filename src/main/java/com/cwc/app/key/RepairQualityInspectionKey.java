package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class RepairQualityInspectionKey {
	
	DBRow row;
	
	public static int NO_NEED_QUALITY = 1;	//不需要质检
	public static int NEED_QUALITY = 2;//需要质检
	 
	public static int FINISH = 3;//报告完成
	
	public RepairQualityInspectionKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(RepairQualityInspectionKey.NO_NEED_QUALITY), "无需质检报告");
		row.add(String.valueOf(RepairQualityInspectionKey.NEED_QUALITY), "质检报告中");
		 
		row.add(String.valueOf(RepairQualityInspectionKey.FINISH), "质检报告完成");
	}
	
	public ArrayList<String> getStatuses()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(String id)
	{
		return(row.getString(id));
	}
}
