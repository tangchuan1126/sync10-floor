package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderLogTypeKey {
	
	DBRow row;
	
	public static int Create = 1;	
	public static int Financial = 2;
	public static int Update = 3;
	public static int Goods = 4;
	public static int Freight = 5;
	public static int ImportCustoms = 6;
	public static int ExportCustoms = 7;
	public static int Label = 8;
	public static int Document = 9;
	public static int ProductShot = 10;
	public static int QualityControl = 11;
	public static int ThirdLabel = 12;
	public static int STORAGEPERSON = 13;
	public static int THIRD_TAG = 14;
	public static int LOAD_REGISTER = 15;
	public static int UNLOAD_REGISTER = 16;
	
	public B2BOrderLogTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderLogTypeKey.Create),"创建记录");
		row.add(String.valueOf(B2BOrderLogTypeKey.Financial),"财务记录");
		row.add(String.valueOf(B2BOrderLogTypeKey.Update),"修改记录");
		row.add(String.valueOf(B2BOrderLogTypeKey.Goods),"货物状态");
		row.add(String.valueOf(B2BOrderLogTypeKey.Freight),"运费流程");
		row.add(String.valueOf(B2BOrderLogTypeKey.ImportCustoms),"进口清关");
		row.add(String.valueOf(B2BOrderLogTypeKey.ExportCustoms),"出口报关");
		row.add(String.valueOf(B2BOrderLogTypeKey.Label),"实物标签");
		row.add(String.valueOf(B2BOrderLogTypeKey.Document),"单证流程");
		row.add(String.valueOf(B2BOrderLogTypeKey.ProductShot),"实物图片流程");
		row.add(String.valueOf(B2BOrderLogTypeKey.QualityControl),"质检流程");
		row.add(String.valueOf(B2BOrderLogTypeKey.ThirdLabel),"内部标签");
		row.add(String.valueOf(B2BOrderLogTypeKey.STORAGEPERSON),"到货派送");
		row.add(String.valueOf(B2BOrderLogTypeKey.THIRD_TAG),"第三方标签");
		row.add(String.valueOf(B2BOrderLogTypeKey.LOAD_REGISTER), "缷货任务");
		row.add(String.valueOf(B2BOrderLogTypeKey.UNLOAD_REGISTER), "装货任务");
	}
	
	public ArrayList getB2BOrderLogTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getB2BOrderLogTypeKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
