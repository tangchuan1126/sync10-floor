package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class PurchaseProductModelKey {
	DBRow dbRow;
	public static int NOT_PRODUCT_MODEL = 1;//无需商品范例
	public static int NEED_PRODUCT_MODEL = 2;//商品范例上传中
	public static int FINISH = 3 ; //商品范例上传完成
	
	public PurchaseProductModelKey() {
		dbRow = new DBRow();
		dbRow.add(String.valueOf(PurchaseProductModelKey.NOT_PRODUCT_MODEL), "无需范例");
		dbRow.add(String.valueOf(PurchaseProductModelKey.NEED_PRODUCT_MODEL), "范例上传中");
		dbRow.add(String.valueOf(PurchaseProductModelKey.FINISH), "范例上传完成");
	}
	
	public ArrayList getProductModelKeyIds()
	{
		return(dbRow.getFieldNames());
	}

	public String getProductModelKeyName(String id)
	{
		return(dbRow.getString(id));
	}

}
