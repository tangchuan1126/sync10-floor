package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class TransportRegistrationTypeKey {

	DBRow row;
	public static int SEND = 1;//装货司机签到
	public static int DELEIVER = 2;//缷货司机签到
	
	public TransportRegistrationTypeKey()
	{
		row = new DBRow();
		row.add(String.valueOf(TransportRegistrationTypeKey.SEND), "装货司机签到");
		row.add(String.valueOf(TransportRegistrationTypeKey.DELEIVER), "缷货司机签到");
	}
	
	public ArrayList getTransportRegistrationTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getTransportRegistrationTypeKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getTransportRegistrationTypeKeyValue(String id)
	{
		return(row.getString(id));
	}
}
