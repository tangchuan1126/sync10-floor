package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WmsItemUnitKey {
	DBRow row;
	
	public static String PIECE = "Piece";
	public static String PALLET = "Pallet";
	public static String PACKAGE = "Package";
	public static String CONTAINER = "Container";
	public static String INNER_CASE = "Inner Case";
	
	public WmsItemUnitKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WmsItemUnitKey.PIECE), "Pcs");
		row.add(String.valueOf(WmsItemUnitKey.PALLET), "PLT");
		row.add(String.valueOf(WmsItemUnitKey.PACKAGE), "PKG");
		row.add(String.valueOf(WmsItemUnitKey.CONTAINER), "CNTR");
		row.add(String.valueOf(WmsItemUnitKey.INNER_CASE), "INNER");
	}
	
	public ArrayList getWmsItemUnitKeys()
	{
		return(row.getFieldNames());
	}

	public String getWmsItemUnitKeyByKey(String id)
	{
		return(row.getString(id));
	}
	
}
