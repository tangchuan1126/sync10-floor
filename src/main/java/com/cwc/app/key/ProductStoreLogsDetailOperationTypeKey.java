package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProductStoreLogsDetailOperationTypeKey
{
	DBRow row;
	
	public static int InStore = 1;
	public static int OutStore = 2;
	public static int RebackStore = 3;
	

	public ProductStoreLogsDetailOperationTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ProductStoreLogsDetailOperationTypeKey.InStore),"进库");
		row.add(String.valueOf(ProductStoreLogsDetailOperationTypeKey.OutStore),"出库");
		row.add(String.valueOf(ProductStoreLogsDetailOperationTypeKey.RebackStore),"已回退");
		
	}

	public ArrayList getProductStoreLogsDetailOperationType()
	{
		return(row.getFieldNames());
	}

	public String getProductStoreLogsDetailOperationTypeById(String id)
	{
		return(row.getString(id));
	}
}
