package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class CheckInLogTypeKey {

	DBRow row;
	public static int GATEIN = 1;
	public static int WINDOW = 2;
	public static int WAREHOUSE = 3;
	public static int GATEOUT = 4;
	public static int UPDATE_WINDOW = 5;
	public static int ANDROID_GATEOUT = 6;
	public static int PATROL = 7;
	public static int ANDROID_PATROL = 8;
	public static int DOCK_CLOSE = 9;
	public static int ANDROID_DOCK_CLOSE = 10;
	public static int ANDROID_SHUTTLE = 11;
	public static int ANDROID_DOCK_CHECKIN = 12;
	public static int ANDROID_SUMSANG_RECEIVE = 13 ;		//三星提交收货
	public static int ANDROID_SUMSANG_RECEIVED = 14 ;		//三星收货完成
	public static int ANDROID_LOAD = 15 ;
	public static int ANDROID_LOAD_EXCEPTION = 16 ;			//Load Exception;
	
	public static int SpotLog  = 17 ;					//有关Spot的日志 zhangrui
	public static int DoorLog =  18 ; 				 	//有关Door的日志 zhangrui
	
	public static int TRACTOROUT =  19 ; 				 	//checkout 区分车头zhouweibin
	public static int TRAILEROUT =  20 ; 				 	//checkout 区分车尾zhouweibin
	public static int USERESOURCE = 21;					//使用资源(包括占用，预约)
	public static int AndroidAssignTask = 22 ; 				//分配任务的时候
	public static int AndroidTaskProcessing = 23 ;				//任务处理的时候
	public static int AndroidCloseTask = 24 ;					//close task
	public static int AndroidStartTask = 25 ;					//start task
	public static int AndroidExceptionTask = 26 ;			//ExceptionTask
	public static int AndroidPartiallyTask = 27 ;			//Partially Task 
	public static int AndroidTakeOver = 28 ;			//Take Over 
	public static int ANDROID_LOAD_CONSOLIDATE = 29;			//Consolidate
	public static int RECEIPT = 30;							//收货日志

	public CheckInLogTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(CheckInLogTypeKey.GATEIN), "Gate Check In");
		row.add(String.valueOf(CheckInLogTypeKey.WINDOW), "Window Check In");
		row.add(String.valueOf(CheckInLogTypeKey.WAREHOUSE), "Warehouse Check In");
		row.add(String.valueOf(CheckInLogTypeKey.GATEOUT), "Gate Check Out");
		row.add(String.valueOf(CheckInLogTypeKey.UPDATE_WINDOW), "Update Window Check In");
		row.add(String.valueOf(CheckInLogTypeKey.ANDROID_GATEOUT), "Android Check Out");
		row.add(String.valueOf(CheckInLogTypeKey.PATROL), "Patrol");
		row.add(String.valueOf(CheckInLogTypeKey.ANDROID_PATROL), "Android Patrol");
		row.add(String.valueOf(CheckInLogTypeKey.DOCK_CLOSE), "Dock Close");
		row.add(String.valueOf(CheckInLogTypeKey.ANDROID_DOCK_CLOSE), "Android Dock Close");
		row.add(String.valueOf(CheckInLogTypeKey.ANDROID_SHUTTLE), "Android Shuttle");
		row.add(String.valueOf(CheckInLogTypeKey.ANDROID_DOCK_CHECKIN), "Android Dock Check In");
		row.add(String.valueOf(CheckInLogTypeKey.ANDROID_SUMSANG_RECEIVE), "Android SumSang Receive");
		row.add(String.valueOf(CheckInLogTypeKey.ANDROID_SUMSANG_RECEIVED), "Android SumSang Received");
		row.add(String.valueOf(CheckInLogTypeKey.ANDROID_LOAD), "Android Load");
		row.add(String.valueOf(CheckInLogTypeKey.ANDROID_LOAD_EXCEPTION), "Android Load Exception");
		row.add(String.valueOf(CheckInLogTypeKey.SpotLog), "Spot");
		row.add(String.valueOf(CheckInLogTypeKey.DoorLog), "Door");
		
		row.add(String.valueOf(CheckInLogTypeKey.TRACTOROUT), "Tractor Check Out");
		row.add(String.valueOf(CheckInLogTypeKey.TRAILEROUT), "Trailer Check Out");
		row.add(String.valueOf(CheckInLogTypeKey.AndroidAssignTask), "Assign Task");
		row.add(String.valueOf(CheckInLogTypeKey.AndroidTaskProcessing), "Task Processing");
		row.add(String.valueOf(CheckInLogTypeKey.AndroidCloseTask), "Close Task");
		row.add(String.valueOf(CheckInLogTypeKey.AndroidStartTask), "Start Task");

		row.add(String.valueOf(CheckInLogTypeKey.AndroidExceptionTask), "Exception Task");
		row.add(String.valueOf(CheckInLogTypeKey.AndroidPartiallyTask), "Partially Task");
		row.add(String.valueOf(CheckInLogTypeKey.AndroidTakeOver), "Take Over Task");
		row.add(String.valueOf(CheckInLogTypeKey.ANDROID_LOAD_CONSOLIDATE), "Load Consolidate");
		row.add(String.valueOf(CheckInLogTypeKey.RECEIPT), "Receipt");

	}

	public ArrayList getCheckInLogTypeKeys() {
		return (row.getFieldNames());
	}

	public String getCheckInLogTypeKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getCheckInLogTypeKeyValue(String id) {
		return (row.getString(id));
	}

}
