package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class RepairTagKey {

	DBRow row;
	
	public DBRow getRow() {
		return row;
	}

	public static int NOTAG = 1;			 
	public static int TAG = 2;		 
	public static int FINISH = 3;		 
 
 
	public RepairTagKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(RepairTagKey.NOTAG), "无需制签");
		row.add(String.valueOf(RepairTagKey.TAG), "需要制签");
		row.add(String.valueOf(RepairTagKey.FINISH), "制签完成");
	 
		 
	}

	public ArrayList<String> getRepairTags()
	{
		return((ArrayList<String>)row.getFieldNames());
	}

	public String getRepairTagById(String id)
	{
		return(row.getString(id));
	}
	
	public String getRepairTagById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
