package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class GoodsCaseKey {
	
	DBRow row;
	public static String intact = "1"; //物品完好
	public static String function_damage = "2";//功能残损
	public static String facade_damage = "3";//外观残损
	public static String scrap = "4"; //报废
	
	public GoodsCaseKey() {

		row = new DBRow();
		row.add(GoodsCaseKey.intact, "完好");
		row.add(GoodsCaseKey.function_damage, "功能残损");
		row.add(GoodsCaseKey.facade_damage, "外观残损");
		row.add(GoodsCaseKey.scrap, "报废");
	} 
	public ArrayList getGoodsCase(){
		
		return (row.getFieldNames());
	}
	
	public String getGoodsCaseById(String id){
		return (row.getString(id));
	}
	
}
