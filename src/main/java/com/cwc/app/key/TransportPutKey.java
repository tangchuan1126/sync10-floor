package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class TransportPutKey {

	DBRow row;
	public static int CanNotPut = 1;	//不可放货
	public static int Puting = 2;		//放货中
	public static int Puted = 3;		//放货完成
	
	public TransportPutKey()
	{
		row = new DBRow();
		row.add(String.valueOf(TransportPutKey.CanNotPut), "不可放货");
		row.add(String.valueOf(TransportPutKey.Puting), "放货中");
		row.add(String.valueOf(TransportPutKey.Puted), "放货完成");
	}
	
	public ArrayList getTransportPutKeys()
	{
		return(row.getFieldNames());
	}

	public String getTransportPutKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getTransportPutKeyValue(String id)
	{
		return(row.getString(id));
	}
}
