package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProductStorageTypeKey
{
	DBRow row;
	public static int OwnWarehouse = 1;		//自有仓库	
	public static int TransitWarehouse = 2;	//转运仓库
	public static int SupplierWarehouse = 3;	//供应商仓库
	public static int ThirdPartyWarehouse = 4;	//第三方仓库
	public static int CustomerWarehouse = 5;	//客户仓库
	public static int RepairWarehouse = 6;		//返修仓库

	
	
	

	public ProductStorageTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ProductStorageTypeKey.OwnWarehouse), "自有仓库");
		row.add(String.valueOf(ProductStorageTypeKey.TransitWarehouse), "转运仓库");
		row.add(String.valueOf(ProductStorageTypeKey.SupplierWarehouse), "供应商仓库");
		row.add(String.valueOf(ProductStorageTypeKey.ThirdPartyWarehouse), "第三方仓库");
		row.add(String.valueOf(ProductStorageTypeKey.CustomerWarehouse), "客户仓库");
		row.add(String.valueOf(ProductStorageTypeKey.RepairWarehouse), "返修仓库");
	}

	public ArrayList getProductStorageType()
	{
		return(row.getFieldNames());
	}

	public String getProductStorageTypeById(String id)
	{
		return(row.getString(id));
	}
}
