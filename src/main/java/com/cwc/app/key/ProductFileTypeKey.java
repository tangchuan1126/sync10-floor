package com.cwc.app.key;

import java.util.ArrayList;
import java.util.List;

import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;

public class ProductFileTypeKey {

	DBRow row;
	public static final int packet = 1; // 商品包装
	public static final int self = 2; // 商品本身
	public static final int bottom = 3; // 商品底贴
	public static final int weight = 4; // 商品称重
	public static final int tags = 5; // 商品标签

	public ProductFileTypeKey() {
		row = new DBRow();
		row.add(String.valueOf(ProductFileTypeKey.self), "SKU");// SKU
		row.add(String.valueOf(ProductFileTypeKey.packet), "PKG");// PKG
		row.add(String.valueOf(ProductFileTypeKey.bottom), "BTM");// BTM
		row.add(String.valueOf(ProductFileTypeKey.tags), "LBL");// LBL
		row.add(String.valueOf(ProductFileTypeKey.weight), "WGT");// WGT
	}
	
	public DBRow[] getKey() {
		
		List<?> list = this.getProductFileTypesKeys();
		
		DBRow[] rowArray = new DBRow[list.size()];
		
		for(int i = 0;i<list.size();i++){
			
			DBRow dbrow = new DBRow();
			dbrow.put("id", Integer.valueOf(list.get(i).toString()));
			dbrow.put("name", this.getProductFileTypesKeyValue(Integer.valueOf(list.get(i).toString())));
			
			rowArray[i] = dbrow;
		}
		
		return rowArray;
	}

	@SuppressWarnings("rawtypes")
	public ArrayList getProductFileTypesKeys() {
		return (row.getFieldNames());
	}

	public String getProductFileTypesKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getProductFileTypesKeyValue(String id) {
		return (row.getString(id));
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ArrayList getProductFileTypeValue() {
		ArrayList<String> fieldList = row.getFieldNames();
		ArrayList<String> valueList = new ArrayList<String>();
		for (int i = 0; i < fieldList.size(); i++) {
			valueList.add(row.getString(fieldList.get(i)));
		}
		return valueList;
	}

	public String[] getProductFileDescription(int id) {
		String str = "";
		switch (id) {
		case self:
			str = "The photos where has words or jack on the product, 3~6";
			break;
		case packet:
			str = "The photos where has words on the product package surface, 3~6";
			break;
		case bottom:
			str = "The photos where has words,icons,logo,model that pastes on the bottom of product, 3~6";
			break;
		case tags:
			str = "The photos where pastes the labels of your company's code, 3~6";
			break;
		case weight:
			str = "The photos of the weight when put the product on the electronic scale, 3~6";
			break;
		default:
			break;
		}
		String[] strr = new String[1];
		strr[0] = str;
		return strr;
	}

	public String[] getProductFileDescription(String idStr) {
		int id = StrUtil.getInt(idStr);
		String str = "";
		switch (id) {
		case self:
			str = "The photos where has words or jack on the product, 3~6";
			break;
		case packet:
			str = "The photos where has words on the product package surface, 3~6";
			break;
		case bottom:
			str = "The photos where has words,icons,logo,model that pastes on the bottom of product, 3~6";
			break;
		case tags:
			str = "The photos where pastes the labels of your company's code, 3~6";
			break;
		case weight:
			str = "The photos of the weight when put the product on the electronic scale, 3~6";
			break;
		default:
			break;
		}
		String[] strr = new String[1];
		strr[0] = str;
		return strr;
	}
}
