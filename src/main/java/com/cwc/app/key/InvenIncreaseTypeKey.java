package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class InvenIncreaseTypeKey
{
	DBRow row;
	public static int PLUS = 1;	//加
	public static int MINUS = 2 ; //减
 
	public InvenIncreaseTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(InvenIncreaseTypeKey.PLUS), "加");
		row.add(String.valueOf(InvenIncreaseTypeKey.MINUS), "减");
		
	}
	
	public ArrayList getInvenOperateTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getInvenOperateTypeKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
