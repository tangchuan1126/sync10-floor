package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ConfigChangeStoreStatusKey {

	DBRow row;
	
	public static int Enough = 1;			//足够
	public static int NotEnough = 2;		//不够
	
	public ConfigChangeStoreStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ConfigChangeStoreStatusKey.Enough),"<font color=\"green\">足够</font>");
		row.add(String.valueOf(ConfigChangeStoreStatusKey.NotEnough),"<font color=\"red\">不够</font>");
	}

	public ArrayList getConfigChangeStoreStatusKeys()
	{
		return(row.getFieldNames());
	}

	public String getConfigChangeStoreStatusKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
