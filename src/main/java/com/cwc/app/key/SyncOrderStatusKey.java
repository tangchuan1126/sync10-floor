package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class SyncOrderStatusKey {
	DBRow row;
	
	public static int OPEN = 1;
	
	public SyncOrderStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(SyncOrderStatusKey.OPEN), "Open");
	}
	
	public ArrayList getSyncOrderStatusKeys()
	{
		return(row.getFieldNames());
	}

	public String getSyncOrderStatusKeyByKey(String id)
	{
		return(row.getString(id));
	}
	
	public String getSyncOrderStatusKeyByKey(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public int getSyncOrderStatusKeyByValue(String value)
	{
		int a = 0;
		ArrayList<String> keys = getSyncOrderStatusKeys();
		for (int i = 0; i < keys.size(); i++)
		{
			int b = Integer.parseInt(keys.get(i));
			if(getSyncOrderStatusKeyByKey(b).equals(value))
			{
				a = b;
			}
		}
		return a;
	}
	
}
