package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**
 * 系统中 模块的编号
 * @author win7zr
 *
 */
public class ModuleKey {

	DBRow dbRow;
	public static int PURCHASE_ORDER = 1;//采货单
	public static int TRANSPORT_ORDER = 2;//转货单
	public static int SCHEDULE = 3 ; //任务
	public static int APPLY_MONEY =4 ; //资金
	public static int APPLY_TRANSFER_MONEY = 5;//转账
	public static int REPAIR_ORDER = 6;//返修单
	public static int WORK_FLOW_REFUND = 7 ; // 申请退款
	public static int WORK_FLOW_RETAIL_PRICE = 8 ; //调整零售报价
	public static int B2B_ORDER = 9;
	public static int CHECK_IN_LOAD = 10;   			 //load(这些是checkin子单据的类型,和这个ModuleKey没有关系)
	public static int CHECK_IN_ORDER = 11;   			 //order(这些是checkin子单据的类型,和这个ModuleKey没有关系)
	public static int CHECK_IN_CTN = 12;    			 //ctnr(这些是checkin子单据的类型,和这个ModuleKey没有关系)
	public static int CHECK_IN_BOL = 13; 				 //bol(这些是checkin子单据的类型,和这个ModuleKey没有关系)
	public static int CHECK_IN_DELIVERY_ORTHERS = 14;	 //delivery other(这些是checkin子单据的类型,和这个ModuleKey没有关系)
	public static int CHECK_IN_WAITING =15;
	public static int CHECK_IN_ANDROID =16;
	public static int CHECK_IN_PONO = 17; 				 //pono(这些是checkin子单据的类型,和这个ModuleKey没有关系)
	public static int CHECK_IN_PICKUP_ORTHERS = 18;		 //pickup other(这些是徐佳子单据的类型,和这个ModuleKey没有关系)
	public static int CHECK_IN_ENTRY = 19;//checkin 主单据
	public static final int CHECK_IN_ENTRY_DETAIL = 20;
	public static final int CHECK_IN_SAMSUNG_PLATE = 21;//checkin 三星收托盘
	
	public static final int CHECK_IN = 22 ; 		//Check in模块
	public static final int Finance = 23 ;  		//上海财务模块
	public static final int Inventory = 24 ;  		//Inventory 模块
	
	public static final int SMALL_PARCEL = 25 ;  		//small parcel(这些是checkin子单据的类型,和这个ModuleKey没有关系)
	
	public static final int OUTBOUND = 56 ; 		//TMS单据
	public static final int INBOUND = 57 ; 		//TMS单据

	public static final int SPECIAL_TASK = 60 ; 		//special_task
  	public ModuleKey() {
		dbRow = new DBRow();
		dbRow.add(String.valueOf(ModuleKey.PURCHASE_ORDER), "采货单");
		dbRow.add(String.valueOf(ModuleKey.TRANSPORT_ORDER), "转货单");
		dbRow.add(String.valueOf(ModuleKey.SCHEDULE), "任务");
		dbRow.add(String.valueOf(ModuleKey.APPLY_MONEY), "资金");
		dbRow.add(String.valueOf(ModuleKey.APPLY_TRANSFER_MONEY), "转账");
		dbRow.add(String.valueOf(ModuleKey.REPAIR_ORDER), "返修单");
		dbRow.add(String.valueOf(ModuleKey.WORK_FLOW_REFUND), "申请退款");
		dbRow.add(String.valueOf(ModuleKey.WORK_FLOW_RETAIL_PRICE), "调整零售报价");
		dbRow.add(String.valueOf(ModuleKey.B2B_ORDER), "B2B订单");
		dbRow.add(String.valueOf(ModuleKey.CHECK_IN_LOAD), "LOAD");
		dbRow.add(String.valueOf(ModuleKey.CHECK_IN_ORDER), "ORDER");
		dbRow.add(String.valueOf(ModuleKey.CHECK_IN_CTN), "CTNR");
		dbRow.add(String.valueOf(ModuleKey.CHECK_IN_BOL), "BOL");
		dbRow.add(String.valueOf(ModuleKey.CHECK_IN_DELIVERY_ORTHERS), "Delivery");
		dbRow.add(String.valueOf(ModuleKey.CHECK_IN_PICKUP_ORTHERS), "Pick Up");
		dbRow.add(String.valueOf(ModuleKey.CHECK_IN_WAITING), "check in waiting");
		dbRow.add(String.valueOf(ModuleKey.CHECK_IN_ANDROID), "check in android warehouse");
		dbRow.add(String.valueOf(ModuleKey.CHECK_IN_PONO), "PO");
		dbRow.add(String.valueOf(ModuleKey.Finance), "Finance");
		dbRow.add(String.valueOf(ModuleKey.OUTBOUND), "Outbound");
		dbRow.add(String.valueOf(ModuleKey.INBOUND), "Inbound");
		dbRow.add(String.valueOf(ModuleKey.SMALL_PARCEL), "Small");
		dbRow.add(String.valueOf(ModuleKey.SPECIAL_TASK), "Special Task");
	}
	
	public ArrayList getModuleIds()
	{
		return(dbRow.getFieldNames());
	}

	public String getModuleName(String id)
	{
		return(dbRow.getString(id));
	}
	
	public String getModuleName(int id)
	{
		return(dbRow.getString(String.valueOf(id)));
	}
	
}
