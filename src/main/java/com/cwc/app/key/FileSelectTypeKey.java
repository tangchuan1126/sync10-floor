package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class FileSelectTypeKey {

	DBRow row;
	
	public static int PURCHASE = 1;	//采购单
	public static int TRANSPORT = 2;//转运单
	public static int PRODUCT = 3;//商品
	
	public FileSelectTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(FileSelectTypeKey.PURCHASE), "采购单");
		row.add(String.valueOf(FileSelectTypeKey.TRANSPORT), "转运单");
		row.add(String.valueOf(FileSelectTypeKey.PRODUCT), "商品");
	}
	
	public ArrayList<String> getFileSelectTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getFileSelectTypeKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getFileSelectTypeKeyById(String id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
