package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class TransportThirdTagKey {

	DBRow row;
	
	public DBRow getRow() {
		return row;
	}

	public static int NOTAG = 1;			 
	public static int TAG = 2;		 
	public static int FINISH = 3;
	public static int FINISH_PURCHASE = 4;
 
	public TransportThirdTagKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(TransportThirdTagKey.NOTAG), "无需打签");
		row.add(String.valueOf(TransportThirdTagKey.TAG), "需要打签");
		row.add(String.valueOf(TransportThirdTagKey.FINISH), "打签完成");
	}

	public ArrayList<String> getTransportThirdTags()
	{
		return((ArrayList<String>)row.getFieldNames());
	}

	public String getTransportThirdTagById(String id)
	{
		return(row.getString(id));
	}
	
	public String getTransportThirdTagById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
