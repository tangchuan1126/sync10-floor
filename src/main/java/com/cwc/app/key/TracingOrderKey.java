package com.cwc.app.key;

import java.net.URLEncoder;
import java.util.ArrayList;
import com.cwc.db.DBRow;

public class TracingOrderKey
{
	DBRow typeWrap;
	
	public static int OTHERS = 0;
	public static int PAYPAL = 1;

	public static int RECORD_NORMAL_ORDER = 2;
	public static int RECORD_LACKING_ORDER = 3;
	public static int RECORD_DOUBT_ORDER = 4;
	public static int RECORD_DOUBT_ADDRESS_ORDER = 5;
	public static int RECORD_DOUBT_PAY_ORDER = 6;
	public static int DOUBT_TRACE = 7;
	public static int DOUBT_ADDRESS_TRACE = 8;
	public static int DOUBT_PAY_TRACE = 9;
	public static int LACKING_TRACE = 10;
	public static int ORDER_COST_VERIFY = 11;
	public static int CREDIT_VERIFY = 12;
	public static int PRINT_DOUBT = 13;

	//售后问题跟踪
	public static int NORMAL_REFUNDING = 14;
	public static int DISPUTE_REFUNDING = 15;
	public static int PART_REFUNDED = 16;
	public static int ALL_REFUNDED = 17;
	public static int NORMAL_WARRANTYING = 18;
	public static int DISPUTE_WARRANTYING = 19;
	public static int WARRANTIED = 20;
	public static int OTHER_DISPUTING = 21;
	public static int CANCEL_REFUND = 22;
	public static int CANCEL_WARRANTY = 23;
	public static int CANCEL_DISPUTE = 24;
	public static int CREATE_WARRANTY = 38;

	public static int FREE_WARRANTIED = 25;
	public static int FAILURE_REFUND = 26;
	public static int FAILURE_WARRANTY = 27;
	public static int FAILURE_DISPUTING = 28;

	public static int BAD_REFUND = 29;//退款差评
	public static int BAD_WARRANTY = 30;//质保差评
	public static int BAD_DISPUTING = 31;//差评
	public static int FREE_SHIPPING_FEE_WARRANTY = 32;//免运费质保
	
	public static int RETURN_PRODUCT = 33;
	
	public static int RECORD_UPDATE_ORDER = 34;//抄单修改订单
	public static int RECORD_NORMAL_WAYBILL = 35;//抄单生成正常运单
	public static int RECORD_STOREOUT_WAYBILL = 36;//生产缺货运单
	public static int WAYBILL_REPLENISH = 37;	//运单补货
	
	public static int WAYBILL_TRACKING = 39;	//运单追踪
	public static int WAYBILL_OPERATION = 40;	//运单操作
	public static int CANCEL_WAYBILL = 41;		//取消运单
	
	public static int NOT_NEED_RETURN	= 42;	//删除退货

	public TracingOrderKey() 
	{
		typeWrap = new DBRow();
		
		
		DBRow row1 = new DBRow();
		row1.add(String.valueOf(TracingOrderKey.OTHERS),"其他问题跟进");
		row1.add(String.valueOf(TracingOrderKey.PAYPAL),"PayPal");
		row1.add(String.valueOf(TracingOrderKey.RECORD_NORMAL_ORDER),"正常抄单");
		row1.add(String.valueOf(TracingOrderKey.RECORD_LACKING_ORDER),"缺货抄单");
		row1.add(String.valueOf(TracingOrderKey.RECORD_DOUBT_ORDER),"疑问订单抄单");
		row1.add(String.valueOf(TracingOrderKey.RECORD_DOUBT_ADDRESS_ORDER),"疑问地址抄单");
		row1.add(String.valueOf(TracingOrderKey.RECORD_DOUBT_PAY_ORDER),"疑问付款抄单");
		row1.add(String.valueOf(TracingOrderKey.DOUBT_TRACE),"疑问订单跟进");
		row1.add(String.valueOf(TracingOrderKey.DOUBT_ADDRESS_TRACE),"疑问地址跟进");
		row1.add(String.valueOf(TracingOrderKey.DOUBT_PAY_TRACE),"疑问付款跟进");
		row1.add(String.valueOf(TracingOrderKey.LACKING_TRACE),"缺货跟进");//pass
		row1.add(String.valueOf(TracingOrderKey.RECORD_UPDATE_ORDER),"抄单修改");
		row1.add(String.valueOf(TracingOrderKey.RECORD_NORMAL_WAYBILL),"生成有货运单");
		row1.add(String.valueOf(TracingOrderKey.RECORD_STOREOUT_WAYBILL),"生产缺货运单");
		row1.add(String.valueOf(TracingOrderKey.CANCEL_WAYBILL),"取消运单");
		row1.add(String.valueOf(TracingOrderKey.WAYBILL_REPLENISH),"运单补货");
		typeWrap.add("抄单跟进", row1);
		
		DBRow row2 = new DBRow();
		row2.add(String.valueOf(TracingOrderKey.ORDER_COST_VERIFY),"成本审核");
		row2.add(String.valueOf(TracingOrderKey.CREDIT_VERIFY),"信用卡验证");
		row2.add(String.valueOf(TracingOrderKey.PRINT_DOUBT),"递送地址验证");
		typeWrap.add("审核跟进", row2);
		
		DBRow row3 = new DBRow();
		row3.add(String.valueOf(TracingOrderKey.CREATE_WARRANTY),"申请服务");
		row3.add(String.valueOf(TracingOrderKey.NORMAL_WARRANTYING),"确定退货商品");
		row3.add(String.valueOf(TracingOrderKey.DISPUTE_WARRANTYING),"争议质保中");
		row3.add(String.valueOf(TracingOrderKey.BAD_WARRANTY),"质保差评");
		row3.add(String.valueOf(TracingOrderKey.FAILURE_WARRANTY),"质保退款");
		row3.add(String.valueOf(TracingOrderKey.FREE_WARRANTIED),"无偿质保");
		row3.add(String.valueOf(TracingOrderKey.WARRANTIED),"有偿质保");
		row3.add(String.valueOf(TracingOrderKey.FREE_SHIPPING_FEE_WARRANTY),"免运费质保");
		row3.add(String.valueOf(TracingOrderKey.CANCEL_WARRANTY),"撤销质保");
		row3.add(String.valueOf(TracingOrderKey.RETURN_PRODUCT),"退货进库");
		row3.add((String.valueOf(TracingOrderKey.NOT_NEED_RETURN)), "无需退货");
		typeWrap.add("质保跟进", row3);
		
		DBRow row4 = new DBRow();
		row4.add(String.valueOf(TracingOrderKey.NORMAL_REFUNDING),"正常退款中");
		row4.add(String.valueOf(TracingOrderKey.DISPUTE_REFUNDING),"争议退款中");
		row4.add(String.valueOf(TracingOrderKey.PART_REFUNDED),"已部分退款");
		row4.add(String.valueOf(TracingOrderKey.ALL_REFUNDED),"已全部退款");
		row4.add(String.valueOf(TracingOrderKey.CANCEL_REFUND),"撤销退款");
		row4.add(String.valueOf(TracingOrderKey.FAILURE_REFUND),"退款失败");
		row4.add(String.valueOf(TracingOrderKey.BAD_REFUND),"退款差评");
		typeWrap.add("退款跟进", row4);
		
		DBRow row5 = new DBRow();
		row5.add(String.valueOf(TracingOrderKey.OTHER_DISPUTING),"其他争议");
		row5.add(String.valueOf(TracingOrderKey.CANCEL_DISPUTE),"撤销争议");
		row5.add(String.valueOf(TracingOrderKey.BAD_DISPUTING),"争议差评");
		row5.add(String.valueOf(TracingOrderKey.FAILURE_DISPUTING),"争议退款");
		typeWrap.add("争议跟进", row5);
		
		DBRow row6 = new DBRow();
		row6.add(String.valueOf(TracingOrderKey.WAYBILL_TRACKING),"运单追踪");
		row6.add(String.valueOf(TracingOrderKey.WAYBILL_OPERATION),"运单操作");
		typeWrap.add("运单跟进",row6);
	}

	public ArrayList getTracingOrderKeys(DBRow datas)
	{
		return(datas.getFieldNames());
	}

	public String getTracingOrderKeyById(String id) 
		throws Exception
	{
		ArrayList typeWrapList = typeWrap.getFieldNames();

		for (int i=0; i<typeWrapList.size(); i++)
		{
			DBRow data = (DBRow)typeWrap.get(typeWrapList.get(i).toString(), new Object());
			if (data.getString(id).equals("")==false)
			{
				return( data.getString(id) );				
			}
		}
		
		return("");
	}
	
	public ArrayList getTracingOrderKeysType()
	{
		return(typeWrap.getFieldNames());
	}
	
	public DBRow getTracingOrderType(String key)
		throws Exception
	{
		return((DBRow)typeWrap.get(key, new Object()));
	}
	
	public static void main(String args[]) throws Exception
	{
		String url = "http://www.google.com/ig/calculator?hl=en&q=1RMB"+URLEncoder.encode("转","utf-8")+"AUD";
	}
	
	
}



