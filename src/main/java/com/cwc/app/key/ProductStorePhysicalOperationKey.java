package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProductStorePhysicalOperationKey
{
	DBRow row;
	public static int LayUp = 1;	//放货
	public static int PickUp = 2 ; //拣货
	public static int PutBack = 3 ; //放回
	public static int PickException = 4; //拣货异常
 
	public ProductStorePhysicalOperationKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ProductStorePhysicalOperationKey.LayUp), "放货");
		row.add(String.valueOf(ProductStorePhysicalOperationKey.PickUp), "拣货");
		row.add(String.valueOf(ProductStorePhysicalOperationKey.PutBack), "放回");
		row.add(String.valueOf(ProductStorePhysicalOperationKey.PickException), "放回");
	}
	
	public ArrayList getProductStorePhysicalOperations()
	{
		return(row.getFieldNames());
	}

	public String getProductStorePhysicalOperationValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
