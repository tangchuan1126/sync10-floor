package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;
/**
 * 退款的状态(1:申请.2同意退款.3驳回.4否决)
 * @author Administrator
 *
 */
public class RetailPriceResultTypeKey {

	DBRow row;
	
	public static int APPLY = 1;	 //调价申请中
	public static int AGREE = 2 ; 	 //调价同意
	public static int NOT_AGREE = 3 ; 	 //调价不同意
	
	 
	
	public RetailPriceResultTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(RetailPriceResultTypeKey.APPLY), "调价申请中");
		row.add(String.valueOf(RetailPriceResultTypeKey.AGREE), "调价同意");
		row.add(String.valueOf(RetailPriceResultTypeKey.NOT_AGREE), "调价不同意");
	 
	}
	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
