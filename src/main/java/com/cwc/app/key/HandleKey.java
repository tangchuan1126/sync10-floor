package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class HandleKey
{
	DBRow row;

	public static int WAIT4_RECORD = 1;
	public static int WAIT4_PRINT = 2;
	public static int PRINTED = 3;
	public static int DELIVERIED = 4;
	public static int VERIFY_COST = 5;
	public static int WAITING_DELIVERY = 33;//全部出库
	
	public static int WAIT4_OUTBOUND = 2;//待出库
	public static int OUTBOUNDING = 3;//出库中
	public static int ALLOUTBOUND = 33;//全部出库
	

	public HandleKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(HandleKey.WAIT4_RECORD),"<font color=red>待抄单</font>");
		row.add(String.valueOf(HandleKey.WAIT4_PRINT),"<font color=blue>待出库</font>");
		row.add(String.valueOf(HandleKey.PRINTED),"<font color=green>出库中</font>");
		row.add(String.valueOf(HandleKey.DELIVERIED),"<font color=black>已发货</font>");
		row.add(String.valueOf(HandleKey.VERIFY_COST),"<font color=red>成本审核</font>");
		
		row.add(String.valueOf(HandleKey.WAITING_DELIVERY),"全部出库");
	}

	public ArrayList getOrderHandle()
	{
		return(row.getFieldNames());
	}

	public String getOrderHandleById(String id)
	{
		return(row.getString(id));
	}
}
