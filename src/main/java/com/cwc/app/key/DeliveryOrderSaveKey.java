package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class DeliveryOrderSaveKey
{
	DBRow row;
	
	public static int NOSAVE = 1;	//未保存
	public static int HASSAVE = 2;//已保存
	
	public DeliveryOrderSaveKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(DeliveryOrderSaveKey.NOSAVE), "未保存");
		row.add(String.valueOf(DeliveryOrderSaveKey.HASSAVE), "已保存");
	}

	public ArrayList getDeliveryOrderStatus()
	{
		return(row.getFieldNames());
	}

	public String getDeliveryOrderStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
