package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProductStoreOperationKey {
	DBRow inStore;
	DBRow outStore;
	DBRow inStoreDamaged;
	DBRow outStoreDamaged;
	DBRow row;
	   // 拆分中
	public static int IN_STORE_MANUAL = 1;						//手工   补充库存
	public static int OUT_STORE_MANUAL = 2;						//手工   提取库存
	public static int IN_OUT_STORE_FILE = 3;					//文件操作库存
	public static int OUT_STORE_TRANSPORT = 4;					//转运单   装箱
	public static int OUT_STORE_TRANSPORT_AUDIT = 5;			//转运审核    出库
	public static int OUT_STORE_WAYBILL = 6;					//运单    提取库存
	public static int IN_STORE_DAMAGED_SPLIT_NORMAL = 7;		//残损套装拆散   补充库存
	public static int IN_STORE_TRANSPORT = 8;					//转运进库
	public static int IN_STORE_TRANSPORT_RE = 9;				//转运装箱中止
	public static int IN_STORE_DELIVERY = 10;					//交货单进库
	public static int IN_OUT_STORE_REVISE = 11;					//校正库存
	public static int IN_STORE_STOREOUT_WAYBILL = 12;			//缺货运单   回退库存
	public static int OUT_STORE_STOREOUT_WAYBILL = 13;			//缺货运单   重新提取库存
	public static int IN_STORE_RETURN = 14;						//退货   补充库存
	public static int OUT_STORE_CONVER_PRODUCT = 15;			//转换商品    提取库存
	public static int IN_STORE_CONVER_PRODUCT = 16;				//转换商品    补充库存
	public static int OUT_STORE_DAMAGED = 17;					//残损件    提取库存
	public static int IN_STORE_RENEW = 18;						//商品翻新进库
	public static int IN_STORE_COMBINATION_UNION_STANDARD = 19;	//预拼装进库
	public static int OUT_STORE_COMBINATION_NORMAL = 20;		//预拼装出库
	public static int OUT_STORE_SPLIT_UNION_STANDARD = 21;		//拆散套装出库
	public static int IN_STORE_SPLIT_NORMAL = 22;				//拆散散件进库
	public static int IN_STORE_WAYBILL_CANCEL = 23;				//运单取消   回退库存
	public static int OUT_STORE_SPLIT_DAMDGED_UNION_STANDARD = 24;//拆散残损套装
	public static int IN_STORE_SPLIT_DAMDGED_NORMAL = 25;		//拆散残损套装    入库
	public static int IN_STORE_SPLIT_DAMDGED_DAMDGED_NORMAL = 26;//拆散残损套装      残损入库
	public static int OUT_STORE_DAMAGED_RENEW = 27;				//残损返修    扣残损库存
	public static int OUT_STORE_DAMAGED_REPAIR = 28;			//残损返修单装箱	残损出库
	public static int OUT_STORE_DAMAGED_REPAIR_AUDIT = 29;		//残损返修单审核   残损出库
	public static int IN_STORE_DAMAGED_RETURN = 30;				//退货	补充残损件
	public static int IN_STORE_DELIVERY_AUDIT = 31;				//交货单审核进库
	public static int IN_STORE_TRANSPORT_DIFFERENT = 32;		//转运装箱差异，退回库存
	public static int OUT_STORE_TRANSPORT_DIFFERENT = 33;		//转运装箱差异，重新扣库存
	public static int IN_STORE_SYSTEM_COMBINATION = 34;			//系统预拼	正常商品	  入库
	public static int OUT_STORE_SYSTEM_COMBINATION = 35;		//系统预拼	正常商品	 出库
	public static int IN_STORE_SYSTEM_SPLIT_UNION_STANDARD = 36;				//系统取消预拼	正常商品	 入库
	public static int OUT_STORE_SYSTEM_SPLIT_UNION_STANDARD = 37;				//系统取消预拼	正常商品	 出库
	public static int IN_STORE_DAMAGED_SPLIT_PRODUCT = 38;			//返修残损商品入库
	public static int IN_STORE_NORMAL_DAMAGED_SPLIT_DEC_PRODUCT = 39;	//返修残损商品成为正常商品，残损商品减少数
	public static int IN_STORE_DAMAGED_NORMAL_SPLIT_ADD_PRODUCT = 40;	//返修残损商品成为正常商品，正常商品增加数
	public static int OUT_STORE_DAMAGED_SPLIT_PRODUCT	= 41;	//返修单残损商品装箱 出库
	public static int IN_STORE_DAMAGED_SPLIT_RE = 42;	//终止装箱
	public static int OUT_STORE_DAMAGED_SPLIT_DESTROY = 43;	//返修残损商品被销毁，残损商品减少数
	public static int IN_STORE_DAMAGED_SPLIT_PRODUCT_OUTSTORAGE = 44;//返修单出库时，出库数少于计划装箱数，库存残损商品增加数
	public static int OUT_STROE_DAMAGED_SPLIT_PRODUCT_OUTSTRAGE = 45;//返修单出库时，出库数大于计划装箱数，库存残损商品减少数
	public static int REBACK = 46;									 //业务单据取消回退
	public static int IN_STORE_TRANSPORT_AUDIT = 47;			//转运单审核进库
	public static int OUT_STORE_BACK_UP_COMBINATION = 48;		//拼装非完整套装散件出
	public static int IN_STORE_BACK_UP_COMBINATION = 49;		//拼装非完整套装散件入
	public static int OUT_STORE_BACK_UP_SPLIT = 50;				//拆散非完整套装非完整套装出库
	public static int IN_STORE_BACK_UP_SPLIT = 51;				//拆散非完整套装散件入库
	public static int OUT_STORE_PICKUP_EXCEPTION = 52;			//拣货单拣货异常（数量不够）
	
	public ProductStoreOperationKey() 
	{
		row = new DBRow();
		
		inStore = new DBRow();
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_MANUAL),"手工  	正常商品      补充库存");	
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_OUT_STORE_FILE),"文件操作库存");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_DAMAGED_SPLIT_NORMAL),"残损套装拆散     正常商品      补充库存");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_TRANSPORT),"转运     正常商品     进库");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_TRANSPORT_RE),"转运装箱中止     正常商品     回退库存");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_DELIVERY),"交货单     正常商品     进库");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_OUT_STORE_REVISE),"校正库存     正常商品");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_STOREOUT_WAYBILL),"缺货运单     正常商品     回退库存");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_RETURN),"退货     正常商品     补充库存");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_CONVER_PRODUCT),"转换商品     正常商品     补充库存");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_RENEW),"商品翻新     正常商品     进库");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_COMBINATION_UNION_STANDARD),"预拼装     正常商品     进库");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_SPLIT_NORMAL),"拆散     正常商品     散件进库");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_WAYBILL_CANCEL),"运单取消     正常商品     回退库存");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_SPLIT_DAMDGED_NORMAL),"拆散残损件     正常商品     散件入库");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_DELIVERY_AUDIT),"交货单审核      正常商品     进库");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_TRANSPORT_AUDIT),"转运单审核      正常商品     进库");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_TRANSPORT_DIFFERENT),"转运单装箱差异      正常商品     进库");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_SYSTEM_COMBINATION),"系统预拼装     正常商品     进库");
		inStore.add(String.valueOf(ProductStoreOperationKey.IN_STORE_SYSTEM_SPLIT_UNION_STANDARD),"系统取消预拼装      正常商品     进库");
		
		
		outStore = new DBRow();
		outStore.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_MANUAL),"手工  	正常商品     提取库存");
		outStore.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_TRANSPORT),"转运单     正常商品      装箱");
		outStore.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_TRANSPORT_AUDIT),"转运审核     正常商品     出库");
		outStore.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_WAYBILL),"运单     正常商品     提取库存");
		outStore.add(String.valueOf(ProductStoreOperationKey.IN_OUT_STORE_REVISE),"校正库存     正常商品");
		outStore.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_STOREOUT_WAYBILL),"缺货运单     正常商品     重新提取库存");
		outStore.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_CONVER_PRODUCT),"转换商品     正常商品     提取库存");
		outStore.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_DAMAGED),"残损件     正常商品     提取库存");
		outStore.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_COMBINATION_NORMAL),"预拼装     正常商品     出库");
		outStore.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_SPLIT_UNION_STANDARD),"拆散套装     正常商品     出库");
		outStore.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_TRANSPORT_DIFFERENT),"转运单装箱差异      正常商品    出库");
		outStore.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_SYSTEM_COMBINATION),"系统预拼装     正常商品     出库");
		outStore.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_SYSTEM_SPLIT_UNION_STANDARD),"系统取消预拼装      正常商品     出库");
		
		
		inStoreDamaged = new DBRow();
		inStoreDamaged.add(String.valueOf(ProductStoreOperationKey.IN_STORE_SPLIT_DAMDGED_DAMDGED_NORMAL),"拆散残损件     残损商品     残损入库");
		inStoreDamaged.add(String.valueOf(ProductStoreOperationKey.IN_STORE_DAMAGED_RETURN),"退货     残损商品     残损进库");
		
		
		outStoreDamaged = new DBRow();
		outStoreDamaged.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_DAMAGED_RENEW),"残损件翻新     残损商品     残损出库");
		outStoreDamaged.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_DAMAGED_REPAIR),"残损返修单出库     残损商品     残损出库");
		outStoreDamaged.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_DAMAGED_REPAIR_AUDIT),"残损返修单审核     残损商品     残损出库");
		
		outStoreDamaged.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_SPLIT_DAMDGED_UNION_STANDARD),"拆散残损件     残损商品     出库");
		
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_MANUAL),"手工  	正常商品      补充库存");			
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_MANUAL),"手工  	正常商品     提取库存");
		row.add(String.valueOf(ProductStoreOperationKey.IN_OUT_STORE_FILE),"进库     正常商品     补充库存");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_TRANSPORT),"转运单     正常商品      装箱");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_TRANSPORT_AUDIT),"转运审核     正常商品     出库");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_WAYBILL),"运单     正常商品     提取库存");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_DAMAGED_SPLIT_NORMAL),"残损套装拆散     正常商品      补充库存");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_TRANSPORT),"转运     正常商品     进库");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_TRANSPORT_RE),"转运装箱中止     正常商品     回退库存");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_DELIVERY),"交货单     正常商品     进库");
		row.add(String.valueOf(ProductStoreOperationKey.IN_OUT_STORE_REVISE),"校正库存     正常商品");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_STOREOUT_WAYBILL),"缺货运单     正常商品     回退库存");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_STOREOUT_WAYBILL),"缺货运单     正常商品     重新提取库存");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_RETURN),"退货     正常商品     补充库存");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_CONVER_PRODUCT),"转换商品     正常商品     提取库存");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_CONVER_PRODUCT),"转换商品     正常商品     补充库存");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_DAMAGED),"残损件     正常商品     提取库存");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_RENEW),"商品翻新     正常商品     进库");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_COMBINATION_UNION_STANDARD),"预拼装     正常商品     进库");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_COMBINATION_NORMAL),"预拼装     正常商品     出库");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_SPLIT_UNION_STANDARD),"拆散套装     正常商品     出库");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_SPLIT_NORMAL),"拆散     正常商品     散件进库");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_WAYBILL_CANCEL),"运单取消     正常商品     回退库存");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_SPLIT_DAMDGED_UNION_STANDARD),"拆散残损件     残损商品     出库");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_SPLIT_DAMDGED_NORMAL),"拆散残损件     正常商品     散件入库");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_SPLIT_DAMDGED_DAMDGED_NORMAL),"拆散残损件     残损商品     残损入库");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_DAMAGED_RENEW),"残损件翻新     残损商品     残损出库");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_DAMAGED_REPAIR),"残损返修单出库     残损商品     残损出库");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_DAMAGED_REPAIR_AUDIT),"残损返修单审核     残损商品     残损出库");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_DAMAGED_RETURN),"退货     残损商品     残损进库");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_DELIVERY_AUDIT),"交货单审核      正常商品     进库");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_TRANSPORT_AUDIT),"转运单审核      正常商品     进库");
		
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_TRANSPORT_DIFFERENT),"转运单装箱差异      正常商品     进库");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_TRANSPORT_DIFFERENT),"转运单装箱差异      正常商品    出库");
		
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_SYSTEM_COMBINATION),"系统预拼      正常商品     进库");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_SYSTEM_COMBINATION),"系统预拼      正常商品    出库");
		
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_SYSTEM_SPLIT_UNION_STANDARD),"系统取消预拼	正常商品     入库");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_SYSTEM_SPLIT_UNION_STANDARD),"系统取消预拼	正常商品     出库");
		
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_DAMAGED_SPLIT_PRODUCT),"返修残损商品入库");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_NORMAL_DAMAGED_SPLIT_DEC_PRODUCT),"返修残损商品成为正常商品，残损商品减少数");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_DAMAGED_NORMAL_SPLIT_ADD_PRODUCT),"返修残损商品成为正常商品，正常商品增加数");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_DAMAGED_SPLIT_PRODUCT),"返修单残损商品装箱 出库");
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_DAMAGED_SPLIT_RE),"返修单残损商品终止装箱");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STORE_DAMAGED_SPLIT_DESTROY),"返修残损商品被销毁，残损商品减少数");
		
		row.add(String.valueOf(ProductStoreOperationKey.IN_STORE_DAMAGED_SPLIT_PRODUCT_OUTSTORAGE),"返修单出库时，出库数少于计划装箱数，库存残损商品增加数");
		row.add(String.valueOf(ProductStoreOperationKey.OUT_STROE_DAMAGED_SPLIT_PRODUCT_OUTSTRAGE),"返修单出库时，出库数大于计划装箱数，库存残损商品减少数");
		
		row.add(String.valueOf(ProductStoreOperationKey.REBACK),"业务单据库存操作回退");
		
	}

	public ArrayList getProductStoreOperation()
	{
		return(row.getFieldNames());
	}
	
	public ArrayList getProdcutInStoreOperation()
	{
		return (inStore.getFieldNames());
	}
	
	public ArrayList getProductOutStoreOperation()
	{
		return (outStore.getFieldNames());
	}
	
	public ArrayList getProductInStoreDamagedOperation()
	{
		return (inStoreDamaged.getFieldNames());
	}
	
	public ArrayList getProductOutStoreDamagedOperation()
	{
		return (outStoreDamaged.getFieldNames());
	}

	public String getProductStoreOperationId(String id)
	{
		return(row.getString(id));
	}
}
