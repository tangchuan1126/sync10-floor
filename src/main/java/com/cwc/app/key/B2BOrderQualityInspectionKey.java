package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderQualityInspectionKey {
	
	DBRow row;
	
	public static int NO_NEED_QUALITY = 1;	//不需要质检
	public static int NEED_QUALITY = 2;//需要质检
	public static int FINISH = 3;//报告完成
	
	public B2BOrderQualityInspectionKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderQualityInspectionKey.NO_NEED_QUALITY), "无需质检报告");
		row.add(String.valueOf(B2BOrderQualityInspectionKey.NEED_QUALITY), "质检报告中");
		row.add(String.valueOf(B2BOrderQualityInspectionKey.FINISH), "质检报告完成");
	}
	
	public ArrayList<String> getB2BQualityInspectionKeys()
	{
		return(row.getFieldNames());
	}

	public String getB2BQualityInspectionKeyById(String id)
	{
		return(row.getString(id));
	}
}
