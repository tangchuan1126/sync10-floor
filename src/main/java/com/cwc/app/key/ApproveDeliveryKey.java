package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class ApproveDeliveryKey
{
	DBRow row;
	
	public static int WAITAPPROVE = 0;		//待审核
	public static int APPROVE = 1;	//已审核
	
	public ApproveDeliveryKey() 
	{
		row = new DBRow();
	}

	public ArrayList getApproveDeliveryStatus()
	{
		return(row.getFieldNames());
	}

	public String getApproveDeliveryStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
