package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class SystemTrackingKey {
DBRow row;
	
	public static int ALL = 0;							//全部
	public static int InOrder = 1;						//进单
	
	public static int NeedTrackingOrder = 2;			//需跟进订单
	public static int DoubtOrder = 21;					//需跟进疑问订单
	public static int DoubtAddress = 22;				//需跟进疑问地址
	public static int DoubtPay = 23;					//需跟进付款
	
	public static int NeedTrackingShipping = 3;			//需跟进快递
	public static int DeliveryException = 31;			//需跟进派送例外
	public static int CustomsException = 32;			//需跟进清关例外
	public static int NotYetDeliveryed = 33;			//需跟进尚未妥投
	
	public static int NeedWayBill = 4;        			//需跟进发货
	public static int Stockout = 41;					//需跟进缺货
	public static int Overdue = 42;						//需跟进逾期未发货
	
	public static int PurchaseTrack = 5;				//需跟进采购单
	
	public static int DeliveryTrack = 6;				//需跟进工厂交货
	
	public static int StorageSendTrack = 7;				//需跟进仓库发货
	
	public static int StorageReciveTrack = 8;			//需跟进仓库收货
	
	public static int OceanShippingTrack = 9;			//需跟进海运
	
	public SystemTrackingKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(SystemTrackingKey.ALL), "全部");
		row.add(String.valueOf(SystemTrackingKey.InOrder), "进单");
		row.add(String.valueOf(SystemTrackingKey.NeedTrackingOrder),"订单");
		row.add(String.valueOf(SystemTrackingKey.NeedTrackingShipping),"快递");
		row.add(String.valueOf(SystemTrackingKey.NeedWayBill), "发货");
	}

	public ArrayList getSystemTracking()
	{
		return(row.getFieldNames());
	}

	public String getSystemTrackingById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
