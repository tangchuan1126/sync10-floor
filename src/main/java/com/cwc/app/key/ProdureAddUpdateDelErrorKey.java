package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProdureAddUpdateDelErrorKey 
{
	DBRow dbRow;
	public static int PROCESS_NAME_EXIST = 1;
	public static int PROCESS_KEY_EXIST = 2;
	public static int COLUMN_NAME_EXIST = 3;
	public static int COLUMN_NAME_PAGE_EXIST = 4;
	public static int ACTIVITY_NAME_REPEAT = 5;
	public static int ACTIVITY_VAL_REPEAT = 6;
	public static int NOTICE_NAME_REPEAT = 7;
	public static int NOTICE_COLUMN_REPEAT = 8;
	
	public ProdureAddUpdateDelErrorKey()
	{
		dbRow = new DBRow();
		dbRow.add(String.valueOf(ProdureAddUpdateDelErrorKey.PROCESS_NAME_EXIST), "流程名在单据内已存在");
		dbRow.add(String.valueOf(ProdureAddUpdateDelErrorKey.PROCESS_KEY_EXIST), "流程key在单据内已存在");
		dbRow.add(String.valueOf(ProdureAddUpdateDelErrorKey.COLUMN_NAME_EXIST), "流程字段名在单据内已存在");
		dbRow.add(String.valueOf(ProdureAddUpdateDelErrorKey.COLUMN_NAME_PAGE_EXIST), "流程字段页面使用名在单据内已存在");
		dbRow.add(String.valueOf(ProdureAddUpdateDelErrorKey.ACTIVITY_NAME_REPEAT), "流程阶段名重复");
		dbRow.add(String.valueOf(ProdureAddUpdateDelErrorKey.ACTIVITY_VAL_REPEAT), "流程阶段值重复");
		dbRow.add(String.valueOf(ProdureAddUpdateDelErrorKey.NOTICE_NAME_REPEAT), "流程通知名重复");
		dbRow.add(String.valueOf(ProdureAddUpdateDelErrorKey.NOTICE_COLUMN_REPEAT), "流程通知字段名重复");
	}
	
	public ArrayList getProdureAddUpdateDelErrorKeys()
	{
		return(dbRow.getFieldNames());
	}

	public String getProdureAddUpdateDelErrorKey(String id)
	{
		return(dbRow.getString(id));
	}
	
	public String getProdureAddUpdateDelErrorKey(int id)
	{
		return(dbRow.getString(String.valueOf(id)));
	}
}
