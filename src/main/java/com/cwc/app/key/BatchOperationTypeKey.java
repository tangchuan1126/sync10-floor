package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class BatchOperationTypeKey {
	DBRow row;
	
	public static int NORMAL = 1;//服务请求
	public static int SPLIT = 2;
	public static int ASSEMBLY = 3;
	public BatchOperationTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(BatchOperationTypeKey.NORMAL), "普通");
		row.add(String.valueOf(BatchOperationTypeKey.SPLIT), "拆分");
		row.add(String.valueOf(BatchOperationTypeKey.ASSEMBLY), "拼装");
	}
	
	public ArrayList getBatchOperationType()
	{
		return(row.getFieldNames());
	}

	public String getBatchOperationTypeById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
