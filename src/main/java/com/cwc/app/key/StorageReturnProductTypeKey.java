package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class StorageReturnProductTypeKey
{
	
	DBRow row;
	
	public static int HAS_PACK = 1;	//有包裹 
	public static int NOT_PACK = 2;	//无包裹 
	public StorageReturnProductTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(StorageReturnProductTypeKey.HAS_PACK), "有包裹");
		row.add(String.valueOf(StorageReturnProductTypeKey.NOT_PACK), "无包裹"); 
	}

	public  ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
