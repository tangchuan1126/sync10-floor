package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ImportAssetsErrorKey {
	
	DBRow row;
	public static int A_NAME = 1;
	public static int A_CATEGORY = 2;
	public static int CATEGORYTYPEERROR = 3;
	public static int A_CODE = 4;
	public static int A_PRICE = 5;
	public static int A_PRICENERROR = 6;
	public static int PRICENULL = 7;
	public static int CATEGORYNULL = 8;
	public static int LOCATIONTYPERROR = 9;
	public static int EXISTSLOCATION = 10;
	public static int GOODSSTATENULL=11;
	public static int STATENULL=12;
	public static int CREATERNULL=13;
	public static int PRODUCTLINENOTEXIST=14;
	public static int LOCATIONERROR=15;
	public static int APPLYSTATEERROR=16;
	public static int CATEGORYERROR=17;
	public static int SAMPLEERROR=18;
	public static int MATCHNAME=19;
	
	public ImportAssetsErrorKey()
	{
		row = new DBRow();
		row.add(String.valueOf(ImportAssetsErrorKey.A_NAME), "<font color=red>此资产名称不能为空</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.A_CATEGORY), "<font color=red>该资产类别不存在，请您核实资产类别表再导入</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.CATEGORYTYPEERROR),"<font color=red>资产类别ID格式不正确，只可填写数字</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.A_CODE), "<font color=red>资产的条码不能为空</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.A_PRICENERROR),"<font color=red>资产的单价格式不正确</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.A_PRICE),"<font color=red>资产的单价不大于0</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.PRICENULL), "<font color=red>资产的单价不能为空</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.CATEGORYNULL),"<font color=red>资产类别不能为空</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.LOCATIONTYPERROR),"<font color=red>资产存放位置的id格式不正确，只可填写数字</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.EXISTSLOCATION),"<font color=red>该办公地点不存在，请您核实资产存放位置表再导入</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.GOODSSTATENULL),"<font color=red>货物状态应在未到货、已到货、已出售、已退货、已换货范围之内</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.STATENULL),"<font color=red>好坏情况应在完好、功能残损、外观残损、报废范围内</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.CREATERNULL),"<font color=red>申请人不存在，请核实申请人的名字是否正确</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.PRODUCTLINENOTEXIST),"<font color=red>产品线不存在，请核实产品线是否输入正确</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.LOCATIONERROR),"<font color=red>存放地点不正确，请您核实资产存放位置后再导入</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.APPLYSTATEERROR),"<font color=red>资金状态应在无需申请、未申请、已申请范围内</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.CATEGORYERROR),"<font color=red>您导入的数据类型是样品数据，请检查资产类别ID是否正确</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.SAMPLEERROR),"<font color=red>您导入的数据类型不是样品数据，请检查资产类别ID是否正确</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.MATCHNAME),"<font color=red>您导入的资产名称列的数据格式不正确，格式应该是  名称/型号   多个名称/型号中间用逗号分隔</font>");
	}
	
	public ArrayList getErrorMessage()
	{
		return(row.getFieldNames());
	}

	public String getErrorMessageById(String id)
	{
		return(row.getString(id));
	}
}
