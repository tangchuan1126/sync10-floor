package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WeightDifferentKey {
DBRow row;
	
	public static int DIFFERENCE = 1;//差异
	public static int BIG = 2;	//大于
	public static int SMAIL = 3;//小于
	
	public WeightDifferentKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WeightDifferentKey.DIFFERENCE), "差异");
		row.add(String.valueOf(WeightDifferentKey.BIG), "大于");
		row.add(String.valueOf(WeightDifferentKey.SMAIL), "小于");
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
