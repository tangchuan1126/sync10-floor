package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ContainerLocationDifferentTypeKey {

	DBRow row;
	
	public static int Different = 1;//只是容器树有不同
	public static int Missing = 2;	//系统内有位置与此容器树关系，盘点没有
	public static int Exceed = 3;	//系统内无位置与次容器树关系，盘点有

	public ContainerLocationDifferentTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ContainerLocationDifferentTypeKey.Different), "不同");
		row.add(String.valueOf(ContainerLocationDifferentTypeKey.Missing), "缺失");
		row.add(String.valueOf(ContainerLocationDifferentTypeKey.Exceed),"超出");
		
	}

	public ArrayList ContainerLocationDifferentTypeKey()
	{
		return(row.getFieldNames());
	}

	public String getContainerLocationDifferentTypeKey(String status)
	{
		return(row.getString(status));
	}
	
	public String getContainerLocationDifferentTypeKey(int status)
	{
		return(row.getString(String.valueOf(status)));
	}
}
