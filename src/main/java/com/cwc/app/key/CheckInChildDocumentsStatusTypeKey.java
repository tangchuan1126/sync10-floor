package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class CheckInChildDocumentsStatusTypeKey {

	DBRow row;
	public final static int UNPROCESS = 1;
	public final static int PROCESSING = 2;
	public final static int CLOSE = 3;
	public final static int EXCEPTION = 4;
	public final static int PARTIALLY = 5;
	
	public CheckInChildDocumentsStatusTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(CheckInChildDocumentsStatusTypeKey.UNPROCESS), "Unprocess");
		row.add(String.valueOf(CheckInChildDocumentsStatusTypeKey.PROCESSING), "Processing");
		row.add(String.valueOf(CheckInChildDocumentsStatusTypeKey.CLOSE), "Closed");
		row.add(String.valueOf(CheckInChildDocumentsStatusTypeKey.EXCEPTION), "Exception");
		row.add(String.valueOf(CheckInChildDocumentsStatusTypeKey.PARTIALLY), "Partially");
	
 
	}

	public static Integer[] getAllStatus(){
		return new Integer[]{UNPROCESS,PROCESSING,CLOSE,EXCEPTION,PARTIALLY};
	}
	public ArrayList getContainerTypeKeys() {
		return (row.getFieldNames());
	}

	public String getContainerTypeKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getContainerTypeKeyValue(String id) {
		return (row.getString(id));
	}

}
