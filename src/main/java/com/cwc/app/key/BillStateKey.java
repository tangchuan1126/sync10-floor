package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class BillStateKey {
	DBRow row;
	
	public DBRow getRow() {
		return row;
	}

	public static int quote = 1;			 
	public static int bargain = 2;		 
	public static int cancel = 3;		 
 
 
	public BillStateKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(BillStateKey.quote), "报价中");
		row.add(String.valueOf(BillStateKey.bargain), "成交");
		row.add(String.valueOf(BillStateKey.cancel), "取消");
	 
		 
	}

	public ArrayList<String> getStatus()
	{
		return((ArrayList<String>)row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
