package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;
//-1:未打印,1.打印成功.2打印失败
public class PrintTaskKey
{
	DBRow row;
	public static int ToPrint = -1;	//待打印
	public static int Success = 1 ; //打印成功
	public static int Failed = 2 ; //打印失败
	public static int Cancel = 3 ; //取消
 
	
	public PrintTaskKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(PrintTaskKey.ToPrint), "待打印");
		row.add(String.valueOf(PrintTaskKey.Success), "打印成功");
		row.add(String.valueOf(PrintTaskKey.Failed), "打印失败");
		row.add(String.valueOf(PrintTaskKey.Cancel), "取消");
	}
	
	public ArrayList getAccountKeys()
	{
		return(row.getFieldNames());
	}

	public String getAccountKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
