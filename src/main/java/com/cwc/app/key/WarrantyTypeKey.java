package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WarrantyTypeKey {
	
	DBRow row;
	public static int ProductQuality= 1;	//产品质量问题
	public static int DistributionError = 2;//配货错误
	public static int ClientError = 3;		//客户买错
	public static int NotSfatisfied = 4;	//对产品不满意
	public static int DeliveryDamage = 5;	//运输破损
	public static int CustomsReturned = 6;	//海关退回
	public static int CustomersToReturn = 7;	//客户自行退回
	public static int SellersCutCargo = 8;	//卖家截货物
	public static int ParcelLost = 9;	//包裹丢失
	public static int AccessoriesIncomplete = 10;	//配件不完整
	
	
	
	
	public WarrantyTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WarrantyTypeKey.ProductQuality), "质量问题");
		row.add(String.valueOf(WarrantyTypeKey.DistributionError), "配货错误");
		row.add(String.valueOf(WarrantyTypeKey.ClientError), "客户买错");
		row.add(String.valueOf(WarrantyTypeKey.NotSfatisfied), "不满意产品");
		row.add(String.valueOf(WarrantyTypeKey.DeliveryDamage), "运输破损");
		row.add(String.valueOf(WarrantyTypeKey.CustomsReturned), "海关退回");
		row.add(String.valueOf(WarrantyTypeKey.CustomersToReturn), "客户自行退回");
		row.add(String.valueOf(WarrantyTypeKey.SellersCutCargo), "卖家截货物");
		row.add(String.valueOf(WarrantyTypeKey.ParcelLost), "包裹丢失");
		row.add(String.valueOf(WarrantyTypeKey.AccessoriesIncomplete), "配件不完整");
		
	}

	public ArrayList getWarrantyType()
	{
		return(row.getFieldNames());
	}

	public String getWarrantyTypeById(String id)
	{
		return(row.getString(id));
	}
	
	public String getWarrantyTypeById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
