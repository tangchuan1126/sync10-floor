package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

//是否包含SN. 1.包含SN .2.不包含SN
public class ContainerHasSnTypeKey {

	DBRow row;
	public static int HASSN = 1 ; //包含SN
	public static int NOSN = 2 ; // 不包含SN
	
 
  
	public ContainerHasSnTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ContainerHasSnTypeKey.HASSN), "HASSN");
		row.add(String.valueOf(ContainerHasSnTypeKey.NOSN), "NOSN");
   
	}
	
	public ArrayList getContainerHasSnTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getContainerHasSnTypeKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getContainerHasSnTypeKeyValue(String id)
	{
		return(row.getString(id));
	}
}
