package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderImportDetailErrorKey {
	DBRow row;
	
	public static String ProductNotExist = "ProductNotExist";
	public static String CountNoOne	= "CountNoOne";
	public static String SerialNumberRepeat = "SerialNumberRepeat";
	public static String ProductNotInPlate = "ProductNotInPlate";
	

	public B2BOrderImportDetailErrorKey() 
	{
		row = new DBRow();
		row.add(B2BOrderImportDetailErrorKey.ProductNotExist,"<font color='red'>商品不存在</font>");
		row.add(B2BOrderImportDetailErrorKey.CountNoOne,"<font color='red'>序列号只能标识唯一商品</font>");
		row.add(B2BOrderImportDetailErrorKey.SerialNumberRepeat,"<font color='red'>序列号重复</font>");
		row.add(B2BOrderImportDetailErrorKey.ProductNotInPlate,"<font color='red'>货物没放在托盘</font>");
		
	}
	
	public ArrayList getB2BOrderImportDetailErrorKeys()
	{
		return(row.getFieldNames());
	}

	public String getB2BOrderImportDetailErrorKeyById(String id)
	{
		return(row.getString(id));
	}
}
