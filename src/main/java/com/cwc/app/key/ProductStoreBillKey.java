package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProductStoreBillKey {
	DBRow row;
	DBRow acronym;
	   // 拆分中
	public static int DELIVERY_ORDER = 1;	//交货单
	public static int TRANSPORT_ORDER = 2;	//转运单
	public static int WAYBILL_ORDER = 3;	//运单
	public static int RETURN_ORDER = 4;		//退货单
	public static int DAMAGED_REPAIR = 5;	//残损返修单
	public static int ORDER = 6;			//订单
	public static int STORAGE_APPROVE = 7;	//库存审核
	public static int PICK_UP_ORDER = 8;	//拣货单
	public static int CONFIG_CHANGE = 9;	//配置更改

	public ProductStoreBillKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ProductStoreBillKey.DELIVERY_ORDER), "交货单");
		row.add(String.valueOf(ProductStoreBillKey.TRANSPORT_ORDER), "转运单");
		row.add(String.valueOf(ProductStoreBillKey.WAYBILL_ORDER), "运单");
		row.add(String.valueOf(ProductStoreBillKey.RETURN_ORDER), "退货单");
		row.add(String.valueOf(ProductStoreBillKey.DAMAGED_REPAIR), "残损返修单");
		row.add(String.valueOf(ProductStoreBillKey.ORDER),"订单");
		row.add(String.valueOf(ProductStoreBillKey.STORAGE_APPROVE),"库存审核单");
		row.add(String.valueOf(ProductStoreBillKey.CONFIG_CHANGE),"配置更改单");
		
		acronym = new DBRow();
		acronym.add(String.valueOf(ProductStoreBillKey.DELIVERY_ORDER), "D");
		acronym.add(String.valueOf(ProductStoreBillKey.TRANSPORT_ORDER), "T");
		acronym.add(String.valueOf(ProductStoreBillKey.WAYBILL_ORDER), "W");
		acronym.add(String.valueOf(ProductStoreBillKey.RETURN_ORDER), "R");
		acronym.add(String.valueOf(ProductStoreBillKey.DAMAGED_REPAIR), "DR");
		acronym.add(String.valueOf(ProductStoreBillKey.ORDER),"O");
		acronym.add(String.valueOf(ProductStoreBillKey.STORAGE_APPROVE),"SA");
		acronym.add(String.valueOf(ProductStoreBillKey.CONFIG_CHANGE),"CC");
	}

	public ArrayList getProductStoreBill()
	{
		return(row.getFieldNames());
	}

	public String getProductStoreBillId(String id)
	{
		return(row.getString(id));
	}
	
	public ArrayList getProductStoreBillAcronym()
	{
		return(acronym.getFieldNames());
	}

	public String getProductStoreBillAcronymId(int id)
	{
		return(acronym.getString(String.valueOf(id)));
	}
	
	public String getProductStoreBillAcronymId(String id)
	{
		return(acronym.getString(id));
	}
}
