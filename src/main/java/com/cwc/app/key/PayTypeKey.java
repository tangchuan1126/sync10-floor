package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class PayTypeKey
{
	DBRow row;
	
	public static int PayPal = 0;	//paypal
	public static int CreditCard = 1;//信用卡
	public static int Wire = 2;//银行转帐
	public static int Cash = 3;//现金
	public static int Check = 4;//支票
	public static int ParentOrder = 5;//父订单
	public static int Free = 6;//父订单
	
	public PayTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(PayTypeKey.PayPal), "PayPal");
		row.add(String.valueOf(PayTypeKey.CreditCard), "CreditCard");
		row.add(String.valueOf(PayTypeKey.Wire), "Wire");
		row.add(String.valueOf(PayTypeKey.Cash), "Cash");
		row.add(String.valueOf(PayTypeKey.Check), "Check");
		row.add(String.valueOf(PayTypeKey.ParentOrder), "ParentOrder");
		row.add(String.valueOf(PayTypeKey.Free), "Free");
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
