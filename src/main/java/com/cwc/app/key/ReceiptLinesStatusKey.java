package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ReceiptLinesStatusKey {

	DBRow dbRow;
	public static int UNCOUNT = 0;
	public static int COUNTED = 2;
	public static int START_SCAN = 3;
	public static int FINISH_SCAN = 4;


	
	public ReceiptLinesStatusKey()
	{
		dbRow = new DBRow();
		dbRow.add(String.valueOf(ReceiptLinesStatusKey.UNCOUNT), "Unloading");
		dbRow.add(String.valueOf(ReceiptLinesStatusKey.COUNTED), "Counted");
		dbRow.add(String.valueOf(ReceiptLinesStatusKey.START_SCAN), "Scaning");
		dbRow.add(String.valueOf(ReceiptLinesStatusKey.FINISH_SCAN), "Finshed Scan");
	}
	
	public ArrayList getReceiptLinesStatusKeys()
	{
		return(dbRow.getFieldNames());
	}

	public String getReceiptLinesStatusKeyName(String id)
	{
		return(dbRow.getString(id));
	}
	
	public String getReceiptLinesStatusKeyName(int id)
	{
		return(dbRow.getString(String.valueOf(id)));
	}
}
