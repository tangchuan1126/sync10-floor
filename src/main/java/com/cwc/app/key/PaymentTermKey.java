package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class PaymentTermKey {
	DBRow dbRowEn;
	public static int COD = 1;
	public static int N15 = 2;
	public static int NET30 = 3;
	public static int NET40 = 4;
	public static int NET45 = 5;
	public static int NET60 = 6;

	public PaymentTermKey()
	{
		dbRowEn = new DBRow();
	    dbRowEn.add(String.valueOf(COD), "COD");
	    dbRowEn.add(String.valueOf(N15), "N15");
	    dbRowEn.add(String.valueOf(NET30), "NET30");
	    dbRowEn.add(String.valueOf(NET40), "NET40");
	    dbRowEn.add(String.valueOf(NET45), "NET45");
	    dbRowEn.add(String.valueOf(NET60), "NET60");
	}
	
	public ArrayList getPaymentTermKeys() {
	    return dbRowEn.getFieldNames();
	  }
	  
	  public String getPaymentTermName(String id) {
	    return dbRowEn.getString(id);
	  }
	  
	  public String getPaymentTermName(int id) {
	    return dbRowEn.getString(String.valueOf(id));
	  }
	  
//	  public String getPaymentTermNameEn(String id) {
//	    return dbRowEn.getString(id);
//	  }
//	  
//	  public String getPaymentTermNameEn(int id) {
//	    return dbRowEn.getString(String.valueOf(id));
//	  }
//	  
	  public DBRow[] getEnPaymentTerm()
	  {
	    DBRow[] result = new DBRow[6];
	    
	    DBRow cod = new DBRow();
	    DBRow n15 = new DBRow();
	    DBRow net30 = new DBRow();
	    DBRow net40 = new DBRow();
	    DBRow net45 = new DBRow();
	    DBRow net60 = new DBRow();
	    
	    cod.add("id", String.valueOf(COD));
	    cod.add("name", "COD");
	    
	    n15.add("id", String.valueOf(N15));
	    n15.add("name", "N15");
	    
	    net30.add("id", String.valueOf(NET30));
	    net30.add("name", "NET30");
	    
	    net40.add("id", String.valueOf(NET40));
	    net40.add("name", "NET40");
	    
	    net45.add("id", String.valueOf(NET45));
	    net45.add("name", "NET45");
	    
	    net60.add("id", String.valueOf(NET60));
	    net60.add("name", "NET60");
	    
	    result[0] = cod;
	    result[1] = n15;
	    result[2] = net30;
	    result[3] = net40;
	    result[4] = net45;
	    result[5] = net60;
	    
	    return result;
	  }
	
	
	
}
