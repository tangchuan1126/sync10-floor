package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class PageTagTypeKey
{
	DBRow dbRow;
	public static int TEXT = 1;//文本
	public static int TEXT_FIELD = 2;//文本框
	public static int TEXT_AREA = 3;//文本域
	public static int RADIO = 4;//单选框
	public static int CHECK_BOX = 5;//复选框
	public static int DROP_LIST = 6;//下拉列表
	
	public PageTagTypeKey()
	{
		dbRow = new DBRow();
		dbRow.add(String.valueOf(PageTagTypeKey.TEXT), "Text");
		dbRow.add(String.valueOf(PageTagTypeKey.TEXT_FIELD), "TextField");
		dbRow.add(String.valueOf(PageTagTypeKey.TEXT_AREA), "TextArea");
		dbRow.add(String.valueOf(PageTagTypeKey.RADIO), "Radio");
		dbRow.add(String.valueOf(PageTagTypeKey.CHECK_BOX), "CheckBox");
		dbRow.add(String.valueOf(PageTagTypeKey.DROP_LIST), "DropList");
	}
	
	public ArrayList getPageTagTypeKeyKeys()
	{
		return(dbRow.getFieldNames());
	}

	public String getPageTagTypeKeyName(String id)
	{
		return(dbRow.getString(id));
	}
	
	public String getPageTagTypeKeyName(int id)
	{
		return(dbRow.getString(String.valueOf(id)));
	}
}
