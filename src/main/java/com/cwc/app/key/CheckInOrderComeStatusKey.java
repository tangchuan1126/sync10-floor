package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class CheckInOrderComeStatusKey
{
	DBRow row;
	public static int OnTime = 0;		
	public static int TooEarly = 1 ; 	
	public static int TooLate = 2 ; 	
	public static int NoAppointment = 3 ; 
 
	public CheckInOrderComeStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(CheckInOrderComeStatusKey.OnTime), "On Time");
		row.add(String.valueOf(CheckInOrderComeStatusKey.TooEarly), "Too Early");
		row.add(String.valueOf(CheckInOrderComeStatusKey.TooLate), "Too Late");
		row.add(String.valueOf(CheckInOrderComeStatusKey.NoAppointment),"No Appointment");		
	}
	
	public ArrayList getCheckInOrderComeStatusKeys()
	{
		return(row.getFieldNames());
	}

	public String geCheckInOrderComeStatusKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
