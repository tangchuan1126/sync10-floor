package com.cwc.app.key;

import java.io.IOException;
import java.util.ArrayList;

import com.cwc.db.DBRow;

public class DamagedRepairKey {
	DBRow row;
	
	public static int READY = 1;	//准备中
	public static int PACKING = 2;//装箱中
	public static int INTRANSIT = 3;//正常运送
	public static int ERRORINTRANSIT = 4;//差异运输 
	public static int ALLINTRANSIT = 11;//所有运输

	public DamagedRepairKey() 
	{
		row = new DBRow();
		
		//散件
		row.add(String.valueOf(DamagedRepairKey.READY), "准备中");
		row.add(String.valueOf(DamagedRepairKey.PACKING), "<font color='green'>装箱中</font>");
		row.add(String.valueOf(DamagedRepairKey.INTRANSIT), "<font color='blue'>正常运输<font>");
		row.add(String.valueOf(DamagedRepairKey.ERRORINTRANSIT), "<font color='red'>差异运输</font>");
	}
	
	public ArrayList getDamagedRepairStatus()
	{
		return(row.getFieldNames());
	}

	public String getDamagedRepairStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
