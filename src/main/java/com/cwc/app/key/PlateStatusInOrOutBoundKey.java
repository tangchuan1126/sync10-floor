package com.cwc.app.key;

import java.util.ArrayList;
import com.cwc.db.DBRow;

public class PlateStatusInOrOutBoundKey {
	
	DBRow dbRow;
	public static int ON_HAND = 1;
	public static int SHIPPED = 2;
	
	public PlateStatusInOrOutBoundKey()
	{
		dbRow = new DBRow();
		dbRow.add(String.valueOf(PlateStatusInOrOutBoundKey.ON_HAND), "OnHand");
		dbRow.add(String.valueOf(PlateStatusInOrOutBoundKey.SHIPPED), "Shipped");
	}
	
	public ArrayList getPlateStatusInOrOutBoundKeys()
	{
		return(dbRow.getFieldNames());
	}

	public String getPlateStatusInOrOutBoundKeyName(String id)
	{
		return(dbRow.getString(id));
	}
	
	public String getPlateStatusInOrOutBoundKeyName(int id)
	{
		return(dbRow.getString(String.valueOf(id)));
	}
}
