package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class SealWithStatusKey
{
	DBRow row;
	
	public static int IMPORT = 1;		//import 状态
	public static int USED = 2;			//used 状态
	public static int RETURN = 3;		//return 状态
	
	
	public SealWithStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(this.IMPORT), "Unused");
		row.add(String.valueOf(this.USED), "Used");
		row.add(String.valueOf(this.RETURN) , "Return");
	}

	public String getSealWithStatusName(int para)
	{
		return(row.get(String.valueOf(para), ""));
	}
	public int getLength()
	{
		return(row.size());
	}
	
	public ArrayList<String> getSealWithStatusNames(){
		return row.getFieldNames();
	}

	
}
