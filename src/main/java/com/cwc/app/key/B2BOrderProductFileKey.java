package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderProductFileKey {

	DBRow row;
	
	public static int NOPRODUCTFILE = 1;	//无需商品文件
	public static int PRODUCTFILE = 2;//商品文件上传中
 
	public static int FINISH = 3;//商品文件上传完成
 
	public B2BOrderProductFileKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderProductFileKey.NOPRODUCTFILE), "无需实物图片");
		row.add(String.valueOf(B2BOrderProductFileKey.PRODUCTFILE), "采集实物图片中");
		row.add(String.valueOf(B2BOrderProductFileKey.FINISH), "实物图片采集完成");
	
	}
	
	public ArrayList<String> getB2BOrderProductFileKeys()
	{
		return(row.getFieldNames());
	}

	public String getB2BOrderProductFileKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
}
