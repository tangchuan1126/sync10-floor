package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ReturnServiceAccessoriesKey {
	
	DBRow row;
	public static int STAND_BASE = 1;
	public static int AC_ADAPTER = 2 ;
	public static int REMOTE = 3 ;
	public static int HDM_CABLE = 4;
	public static int D3_GLASS = 5 ;
	public static int RCA_CABLE = 6;
	
 
	public ReturnServiceAccessoriesKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnServiceAccessoriesKey.STAND_BASE), "Stand/Base");
		row.add(String.valueOf(ReturnServiceAccessoriesKey.AC_ADAPTER), "AC Adapter");
		row.add(String.valueOf(ReturnServiceAccessoriesKey.REMOTE), "Remote");
		row.add(String.valueOf(ReturnServiceAccessoriesKey.HDM_CABLE), "HDM/Cable");
		row.add(String.valueOf(ReturnServiceAccessoriesKey.D3_GLASS), "3D Glass");
		row.add(String.valueOf(ReturnServiceAccessoriesKey.RCA_CABLE), "RCA Cable");
		
	}
	
	public ArrayList getReturnServiceAccessoriesKeys()
	{
		return(row.getFieldNames());
	}

	public String getReturnServiceAccessoriesKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getReturnServiceAccessoriesKeyById(String id)
	{
		return(row.getString(id));
	}
}
