package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class SupplierErrorKey {
	
	DBRow dbRow;
	public static int SUPPLIER_ID_REPEAT = 1;
	public static int SUPPLIER_NAME_REPEAT = 2;
	
	public SupplierErrorKey()
	{
		dbRow = new DBRow();
		dbRow.add(String.valueOf(SupplierErrorKey.SUPPLIER_ID_REPEAT), "SupplierID Repeat.");
		dbRow.add(String.valueOf(SupplierErrorKey.SUPPLIER_NAME_REPEAT), "Supplier Name Repeat.");
	}
	
	public ArrayList getSupplierErrorKeys()
	{
		return(dbRow.getFieldNames());
	}

	public String getSupplierErrorKeyName(String id)
	{
		return(dbRow.getString(id));
	}
	
	public String getSupplierErrorKeyName(int id)
	{
		return(dbRow.getString(String.valueOf(id)));
	}
}
