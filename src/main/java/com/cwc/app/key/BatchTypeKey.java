package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class BatchTypeKey {
	DBRow row;
	
	public static int INIT = 1;	
	public static int DELIVERY = 2;
	public static int TRANSPORTOUT = 3;
	//public static int TRANSPORTIN = 4;
	public static int SPLIT = 5;
	public static int ASSEMBLY = 6;
	public static int RENOVATE = 7;
	public static int DAMAGED = 8;
	public static int CONVERT = 9;
	public static int APPROVE = 10;
	public static int WAYBILL = 11;
	public static int RETURN = 12;
	public static int SPILTUNIONWHOLE = 13;
	
	public BatchTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(BatchTypeKey.INIT), "初始化批次库存");
		row.add(String.valueOf(BatchTypeKey.DELIVERY), "交货单入库");
		row.add(String.valueOf(BatchTypeKey.TRANSPORTOUT), "转运单");
		//row.add(String.valueOf(BatchTypeKey.TRANSPORTIN), "转运单入库");
		row.add(String.valueOf(BatchTypeKey.SPLIT), "正常拆分");
		row.add(String.valueOf(BatchTypeKey.ASSEMBLY), "拼装");
		row.add(String.valueOf(BatchTypeKey.RENOVATE), "翻新");
		row.add(String.valueOf(BatchTypeKey.DAMAGED), "残损");
		row.add(String.valueOf(BatchTypeKey.CONVERT), "改货");
		row.add(String.valueOf(BatchTypeKey.APPROVE), "盘点审核");
		row.add(String.valueOf(BatchTypeKey.WAYBILL), "运单出库");
		row.add(String.valueOf(BatchTypeKey.RETURN), "退件");
		row.add(String.valueOf(BatchTypeKey.SPILTUNIONWHOLE), "残损拆分");
	}
	
	public ArrayList<String> getStatuses()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
