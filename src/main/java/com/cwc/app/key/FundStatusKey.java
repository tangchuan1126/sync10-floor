package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**
 * 为也兼容以前的数据，
 * 未申请转账，部分付款，付款完成，取消申请和原来Key值一致
 * @author Administrator
 *
 */
public class FundStatusKey {

	DBRow row;
	
	public static int NO_APPLY_TRANSFER 	= 0;	//未申请转账，原存数据库的状态
	public static int HAD_APPLY_TRANSFER	= 5;	//已申请转账
	public static int PART_PAYMENT			= 1;	//部分付款，原存数据库的状态
	public static int FINISH_PAYMENT		= 2;	//付款完成，原存数据库的状态
	public static int CANCEL				= 3;	//取消申请，原存数据库的状态
	public static int CANNOT_APPLY_TRANSFER	= 4;	//凭证未提交，此状态未存数据库
	
	
	public FundStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(FundStatusKey.NO_APPLY_TRANSFER), "未申请转账");
		row.add(String.valueOf(FundStatusKey.HAD_APPLY_TRANSFER), "已申请转账");
		row.add(String.valueOf(FundStatusKey.PART_PAYMENT), "<font color='red'>部分付款</font>");
		row.add(String.valueOf(FundStatusKey.FINISH_PAYMENT), "<font color='green'>付款完成</font>");
		row.add(String.valueOf(FundStatusKey.CANCEL), "<font color='blue'>取消申请</font>");
		row.add(String.valueOf(FundStatusKey.CANNOT_APPLY_TRANSFER), "凭证未提交");
	}
	
	public ArrayList getFundStatusKey()
	{
		return(row.getFieldNames());
	}

	public String getFundStatusKeyNameById(String status)
	{
		return(row.getString(status));
	}
	
}
