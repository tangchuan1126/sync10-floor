package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProcessKey {

	DBRow row;
	
	public DBRow getRow() {
		return row;
	}
 
	public final static  int FOLLOW_UP = 1 ;					//跟进记录
 	public final static  int TRANSPORT = 2 ; 					//转账记录
 	public final static  int UPDATE_PURCHASE_DETAIL = 3; 		//修改采购单详细
	public final static  int INVOICE = 4;						//发票
	public final static  int DRAWBACK = 5 ; 					//退税
	public final static  int PURCHASETAG = 8; 					//采购单内部标签
	public final static  int PRODUCTTAG = 12 ; 				//商品标签
	public final static  int QUALITY_INSPECTION = 13;		 	//质检		 
	public final static  int PURCHASE_PURCHASERS = 14;			//采购员
	public final static  int PURCHASE_DISPATCHER = 15;			//调度员
	public final static  int CLEARANCE = 16	;				//清关流程
	public final static  int DECLARATION = 17	;				//报关流程
	public final static  int CERTIFICATE = 18 ;				//单证流程
	public final static  int PRODUCTFILE= 19	;				//商品图片流程
	public final static  int TRANSPORT_TAG = 20;				//转运单内部标签流程
	public final static  int TRANSPORT_STOCK = 21;				//转运单运费流程
	public final static  int TRANSPORT_PRODUCT_TAG = 22;		//转运单商品标签
	public final static  int PURCHASE_RODUCTMODEL = 23;		//采购单的商品范例
	public final static  int PURCHASE_PRODUCTFILE = 24;		//采购单的商品图片
	public final static  int PURCHASE_EXPECT_APPLY_FUNDS = 25;		//采购定金申请
	public final static  int TRANSPORT_EXPECT_APPLY_FUNDS = 26;		//转运单资金申请
	public final static  int PURCHASE_FRONT_MONEY = 27;       //预申请
	public final static  int TRANSPORT_STOCK_APPLY_FUNDS = 28;//转运单运费资金
	public final static  int PURCHASE_EXPECT_TRANSFER_MONEY = 29;		//采购定金转账
	public final static  int TRANSPORT_EXPECT_TRANSFER_MONEY = 30;		//转运单资金转账
	public final static  int TRANSPORT_STOCK_TRANSFER_MONEY = 31;//转运单运费转账
	public final static  int REPAIR_STOCK_APPLY_FUNDS	= 32;//返修单申请运费资金
	public final static  int REPAIR_STOCK_TRANSFER_MONEY	= 32;//返修单运费资金转账
	public final static  int PURCHASE_THIRD_TAG = 33; 			//采购单第三方标签
	public final static  int TRANSPORT_THIRD_TAG = 34;			//转运单第三方标签
	
	public final static  int APPLY_FUNDS_WAGE = 35;					//运营费用的人员工资
	public final static  int APPLY_FUNDS_SOCIAL_SECURITY = 36; 		//运营费用的社保
	public final static  int APPLY_FUNDS_LIFE_INCIDENTALS = 37;		//运营费用的生活日杂
	public final static  int APPLY_FUNDS_OFFICE_COSTS = 38; 			//运营费用的办公费用
	public final static  int APPLY_FUNDS_RENT = 39;					//运营费用的房租
	public final static  int APPLY_FUNDS_COMMUNICATION = 40; 			//运营费用的通讯费
	public final static  int APPLY_FUNDS_OTHER_FREIGHT = 41;			//运营费用的其他运费
	public final static  int APPLY_FUNDS_TRAVEL_COSTS = 42; 			//运营费用的差旅
	public final static  int APPLY_FUNDS_VEHICLE_COSTS = 43;			//运营费用的车辆费用
	public final static  int APPLY_FUNDS_DECLARATION_DOLLARS = 44; 	//运营费用的报关美金
	
	public final static  int APPLY_FUNDS_DECLARATION = 45; 			//采购货款的报关
	public final static  int APPLY_FUNDS_CLEARANCE = 46;				//采购货款的清关
	public final static  int APPLY_FUNDS_DRAWBACK = 47;				//采购货款的退税
	public final static  int APPLY_FUNDS_INVOICED_AMOUNT = 48; 		//采购货款的开票金额
	
	public final static  int APPLY_FUNDS_SAMPLE_PROCUREMENT = 49; 		//固定资产与样品的样品采购
	public final static  int APPLY_FUNDS_FIXED_ASSETS = 50;			//固定资产与样品的固定资产
	public final static  int APPLY_FUNDS_TEST_MARKETING_PROCUREMENT = 51;	//资金申请的货物测试销售期采购
	public final static  int CHECK_IN_WINDOW = 52;							//Widow Check In Notify Warehouse Mgr 
	public final static  int CHECK_IN_WAREHOUSE = 53;						//Assign Task Notitfy Warehouse Loader 
	public final static  int CHECK_IN_ANDROID_WAREHOUSE = 53;				//Assign Task Notitfy Warehouse Loader 
	public final static  int CHECK_IN_ANDROID_PARROL = 55;					//Patrol Notify Warehouse Mgr
	public final static  int GateHasTaskNotifyWindow = 56;	    //替换原来的Gate_check_in	            //Gate Check In Notify Window mgr
	
	public final static  int GateNotifyWareHouse = 57 ;					//Gate Check In Notify WareHouse mgr
	
	
	public final static  int FinanceApplyPayment = 58 ;		
	public final static  int GateNoTaskNotifyWindow	 = 59 ;					//Gate No Task Notify Window		

	public final static int HANDLING_SOP = 61;	//Handling Sop
	

	public final static  int CongfigChangeTask	=  62 ;			//assign supervisor
	public final static  int ScanTask	=  63 ;					//assign scan
	public final static  int MovementTask	=  64 ;					//Movement Task
	

	public final static int SPECIAL_TASK = 70;	//Special Task
	public final static int SPECIAL_INVOICE = 71;	//Special Invoice
	public final static int INVENTORYCYCLECOUNTTASK = 60;					//CYCLECOUNTTASK Inventory 		
	public final static int INVENTORYVERIFICATION  = 72;					//VERIFICATION Inventory 	
	public final static int INVENTORYFINDTASK = 73;							//FINDTASK Inventory 	
	public ProcessKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ProcessKey.PURCHASE_FRONT_MONEY), "预申请");
		row.add(String.valueOf(ProcessKey.FOLLOW_UP), "跟进记录");
		row.add(String.valueOf(ProcessKey.TRANSPORT), "转账记录");
		row.add(String.valueOf(ProcessKey.UPDATE_PURCHASE_DETAIL), "修改采购单详细");
		row.add(String.valueOf(ProcessKey.INVOICE), "发票");
		row.add(String.valueOf(ProcessKey.DRAWBACK), "退税");
		row.add(String.valueOf(ProcessKey.PURCHASETAG), "采购单内部标签");
		row.add(String.valueOf(ProcessKey.PRODUCTTAG), "商品标签");
		row.add(String.valueOf(ProcessKey.QUALITY_INSPECTION), "质检	");
		row.add(String.valueOf(ProcessKey.PURCHASE_PURCHASERS), "采购员");
		row.add(String.valueOf(ProcessKey.PURCHASE_DISPATCHER), "调度员");
		row.add(String.valueOf(ProcessKey.PURCHASE_RODUCTMODEL), "采购单的商品范例");
		row.add(String.valueOf(ProcessKey.PURCHASE_PRODUCTFILE), "采购单的商品图片");
		row.add(String.valueOf(ProcessKey.PURCHASE_EXPECT_APPLY_FUNDS), "采购单定金申请");
		 
		row.add(String.valueOf(ProcessKey.CLEARANCE), "清关流程");
		row.add(String.valueOf(ProcessKey.DECLARATION), "报关流程");
		row.add(String.valueOf(ProcessKey.CERTIFICATE), "单证流程");
		row.add(String.valueOf(ProcessKey.PRODUCTFILE), "商品图片流程");
		row.add(String.valueOf(ProcessKey.TRANSPORT_TAG), "转运单内部标签流程");
		row.add(String.valueOf(ProcessKey.TRANSPORT_STOCK), "转运单运费流程");
		row.add(String.valueOf(ProcessKey.TRANSPORT_PRODUCT_TAG), "转运单商品标签流程");
		row.add(String.valueOf(ProcessKey.TRANSPORT_EXPECT_APPLY_FUNDS), "转运单资金申请");
		row.add(String.valueOf(ProcessKey.TRANSPORT_STOCK_APPLY_FUNDS), "转运单运费的资金申请");
		row.add(String.valueOf(ProcessKey.PURCHASE_EXPECT_TRANSFER_MONEY), "采购定金转账");
		row.add(String.valueOf(ProcessKey.TRANSPORT_EXPECT_TRANSFER_MONEY), "转运单资金转账");
		row.add(String.valueOf(ProcessKey.TRANSPORT_STOCK_TRANSFER_MONEY), "转运单运费转账");
		
		row.add(String.valueOf(ProcessKey.REPAIR_STOCK_APPLY_FUNDS), "返修单申请运费资金");
		row.add(String.valueOf(ProcessKey.REPAIR_STOCK_TRANSFER_MONEY), "返修单运费资金转账");
		
		row.add(String.valueOf(ProcessKey.PURCHASE_THIRD_TAG), "采购单第三方标签");
		row.add(String.valueOf(ProcessKey.TRANSPORT_THIRD_TAG), "转运单第三方标签");
		
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_WAGE), "运营费用的人员工资");
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_SOCIAL_SECURITY), "运营费用的社保");
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_LIFE_INCIDENTALS), "运营费用的生活日杂");
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_OFFICE_COSTS), "运营费用的办公费用");
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_RENT), "运营费用的房租");
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_COMMUNICATION), "运营费用的通讯费");
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_OTHER_FREIGHT), "运营费用的其他运费");
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_TRAVEL_COSTS), "运营费用的差旅");
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_VEHICLE_COSTS), "运营费用的车辆费用");
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_DECLARATION_DOLLARS), "运营费用的报关美金");
		
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_DECLARATION), "采购货款的报关");
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_CLEARANCE), "采购货款的清关");
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_DRAWBACK), "采购货款的退税");
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_INVOICED_AMOUNT), "采购货款的开票金额");
		
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_SAMPLE_PROCUREMENT), "固定资产与样品的样品采购");
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_FIXED_ASSETS), "固定资产与样品的固定资产");
		row.add(String.valueOf(ProcessKey.APPLY_FUNDS_TEST_MARKETING_PROCUREMENT), "资金申请的货物测试销售期采购");
		row.add(String.valueOf(ProcessKey.CHECK_IN_WINDOW), "window 签到");
		row.add(String.valueOf(ProcessKey.CHECK_IN_WAREHOUSE), "warehouse司机签到");
		row.add(String.valueOf(ProcessKey.CHECK_IN_ANDROID_WAREHOUSE), "android warehouse 通知");
		row.add(String.valueOf(ProcessKey.CHECK_IN_ANDROID_PARROL), "android 巡逻通知");
		row.add(String.valueOf(ProcessKey.GateHasTaskNotifyWindow), "gate check in");
		row.add(String.valueOf(ProcessKey.GateNoTaskNotifyWindow), "No Task Notify Window");
		row.add(String.valueOf(ProcessKey.FinanceApplyPayment), "FinanceApplyPayment");

		row.add(String.valueOf(ProcessKey.HANDLING_SOP), "Handling Sop");
		
		row.add(String.valueOf(ProcessKey.CongfigChangeTask), "CC Task");
		row.add(String.valueOf(ProcessKey.ScanTask), "Scan Task");
		row.add(String.valueOf(ProcessKey.SPECIAL_TASK), "Special Task");
		row.add(String.valueOf(ProcessKey.SPECIAL_INVOICE), "Special Invoice");
	}

	public ArrayList<String> getStatus()
	{
		return((ArrayList<String>)row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	public String getStatusById(String id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
