package com.cwc.app.key;

public class LogServModelKey {
	public static String TMS_TRACK = "TmsTrack";
	public static String TRANSPORT = "Transport";
	public static String SERIAL_NUMBER_LOG = "SerialNumberLog";
	public static String PRODUCT_STORE_PHYSICAL_LOG = "ProductStorePhysicalLog";
	public static String PRODUCT_STORE_LOG = "ProductStoreLog";
	public static String PRODUCT_STORE_ALLOCATE_LOG = "ProductStoreAllocateLog";
	public static String PRODUCT_STORAGE_TITLE_BACKUP = "ProductStorageTitleBackUp";
	public static String PRODUCT_STORAGE_LOCATION_BACKUP = "ProductStorageLocationBackUp";
	public static String PRODUCT_STORAGE_CONTAINER_BACKUP = "ProductStorageContainerBackUp";
	public static String PRODUCT_STORAG_EBACKUP = "ProductStorageBackUp";
	public static String ACCESS_LOG = "AccessLog";
	public static String BASIC_DATA_SETUP = "BasicDataSetup";
	public static String STORE_RECEIVE_LOG = "StoreReceiveLog";
	public static String SPECIAL_TASK_LOG = "SpecialTaskLog";
}
