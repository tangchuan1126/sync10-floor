package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**
 * 进出库日志类型
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class InOutStoreKey
{
	DBRow row;
	
	public static int OUT_STORE_UPLOAD = 0;				//上传出库日志
	public static int OUT_STORE_CONVERT_PRODUCT = 1;	//改商品出库
	public static int OUT_STORE_DAMAGE = 2;				//残损件出库
	public static int OUT_STORE_TRANSPORT = 8;			//转运出库
	
	public static int IN_STORE_UPLOAD = 3;				//上传进库日志
	public static int IN_STORE_CONVERT_PRODUCT = 4;		//改商品进库
	public static int IN_STORE_RETURN = 5;				//退件进库
	public static int IN_STORE_PURCHASE = 6;				//采购进库
	public static int IN_STORE_TRANSPORT = 7;			//转运入库
	public static int IN_STORE_TRANSPORT_RE = 9; 		//转运装箱取消
	public static int IN_STORE_DAMAGED_SPILT = 10; 		//残损套装拆散
	public static int IN_STORE_CANCEL_WAYBILL = 11;		//运单取消回退库存
	

	public InOutStoreKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(InOutStoreKey.OUT_STORE_UPLOAD),"普通出库");
		row.add(String.valueOf(InOutStoreKey.OUT_STORE_CONVERT_PRODUCT),"改商品出库");
		row.add(String.valueOf(InOutStoreKey.OUT_STORE_DAMAGE),"残损件出库");
		row.add(String.valueOf(InOutStoreKey.OUT_STORE_TRANSPORT),"采购进库");
		
		row.add(String.valueOf(InOutStoreKey.IN_STORE_UPLOAD),"普通入库");
		row.add(String.valueOf(InOutStoreKey.IN_STORE_CONVERT_PRODUCT),"改商品进库");
		row.add(String.valueOf(InOutStoreKey.IN_STORE_RETURN),"退货进库");
		row.add(String.valueOf(InOutStoreKey.IN_STORE_PURCHASE),"采购进库");
		row.add(String.valueOf(InOutStoreKey.IN_STORE_TRANSPORT),"转运进库");
		row.add(String.valueOf(InOutStoreKey.IN_STORE_TRANSPORT_RE),"转运中止");
		row.add(String.valueOf(InOutStoreKey.IN_STORE_CANCEL_WAYBILL),"运单取消回退库存");
		
	}

	public ArrayList getKeys()
	{
		return(row.getFieldNames());
	}

	public String getVal(String key)
	{
		return(row.getString(key));
	}
}

