package com.cwc.app.key;

import java.util.List;

import com.cwc.db.DBRow;

public class CheckInEntryPriorityKey {

	DBRow row;
	public static int NO = 0;
	public static int HIGH = 1;
	public static int MIDDLE = 2;
	public static int LOW = 3;
	
	public CheckInEntryPriorityKey()
	{
		row = new DBRow();
		row.add(String.valueOf(CheckInEntryPriorityKey.NO), "NA");
		row.add(String.valueOf(CheckInEntryPriorityKey.HIGH), "High");
		row.add(String.valueOf(CheckInEntryPriorityKey.MIDDLE), "Middle");
		row.add(String.valueOf(CheckInEntryPriorityKey.LOW), "Low");
	}
	
	public List getCheckInEntryPriorityKey()
	{
		return row.getFieldNames();
	}
	
	public String getCheckInEntryPriorityKey(int id)
	{
		return row.getString(String.valueOf(id));
	}
	
	public String getCheckInEntryPriorityKey(String id)
	{
		return row.getString(id);
	}
}
