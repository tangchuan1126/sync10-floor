package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class GoodsStatusKey {
	
	DBRow row;
	
	public static String no_arrival = "1"; //未到货
	public static String arrival = "2";//已到货
	public static String sell = "3";//已出售
	public static String retreat = "4";//已退货
	public static String barter = "5"; //已换货
	
	public GoodsStatusKey(){
		
		row = new DBRow();
		row.add(GoodsStatusKey.no_arrival, "未到货");
		row.add(GoodsStatusKey.arrival, "已到货");
		row.add(GoodsStatusKey.sell, "已出售");
		row.add(GoodsStatusKey.retreat, "已退货");
		row.add(GoodsStatusKey.barter, "已换货");
	}
	
	public ArrayList getGoodsStatus(){
		
		return (row.getFieldNames());
	}
	
	public String getGoodsStatusById(String status){
		
		return (row.getString(status));
	}

}
