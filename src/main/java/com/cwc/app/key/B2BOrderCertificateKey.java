package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderCertificateKey {

DBRow row;
	
	public DBRow getRow() {
		return row;
	}

	public static int NOCERTIFICATE= 1;			 
	public static int CERTIFICATE = 2;		 
	public static int FINISH = 3;		 
 
 
	public B2BOrderCertificateKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderCertificateKey.NOCERTIFICATE), "无需单证");
		row.add(String.valueOf(B2BOrderCertificateKey.CERTIFICATE), "单证采集中");
		row.add(String.valueOf(B2BOrderCertificateKey.FINISH), "单证采集完成");
	 
		 
	}

	public ArrayList<String> getB2BOrderCertificateKeys()
	{
		return((ArrayList<String>)row.getFieldNames());
	}

	public String getB2BOrderCertificateKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
