package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class LoadUnloadOccupancyTypeKey {
	
	DBRow dbRow;
	public static int DOOR = 1;//门
	public static int LOCATION = 2;//位置
	
	public LoadUnloadOccupancyTypeKey()
	{
		dbRow = new DBRow();
		dbRow.add(String.valueOf(LoadUnloadOccupancyTypeKey.DOOR), "门");
		dbRow.add(String.valueOf(LoadUnloadOccupancyTypeKey.LOCATION), "位置");
	}
	
	public ArrayList getLoadUnloadOccupancyTypes()
	{
		return(dbRow.getFieldNames());
	}

	public String getLoadUnloadOccupancyTypeName(String id)
	{
		return(dbRow.getString(id));
	}
	
	public String getLoadUnloadOccupancyTypeName(int id)
	{
		return(dbRow.getString(String.valueOf(id)));
	}
}
