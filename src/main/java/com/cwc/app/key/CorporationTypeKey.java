package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class CorporationTypeKey {
	
	DBRow row;
	public static final int CUSTOMER		= 1 ; //Customer
	public static final int CARRIER			= 2 ; //Carrier
	public static final int LOGISTICS_TEAM	= 3 ; //Logistics Team
	public static final int TITLE			= 4 ; //Title
	public static final int SHIP_TO			= 5 ; //Ship To
 
	public CorporationTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(CorporationTypeKey.CUSTOMER), "Customer");//Customer
		row.add(String.valueOf(CorporationTypeKey.CARRIER), "Carrier");//Carrier
		row.add(String.valueOf(CorporationTypeKey.LOGISTICS_TEAM), "Logistics Team");//Logistics Team
		row.add(String.valueOf(CorporationTypeKey.TITLE), "Title");//Title
		row.add(String.valueOf(CorporationTypeKey.SHIP_TO), "Ship To");//Ship To
	}
	
	public ArrayList getCorporationTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getCorporationTypeKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getCorporationTypeKeyValue(String id)
	{
		return(row.getString(id));
	}
	
	public ArrayList getCorporationTypeValues()
	{
		ArrayList<String> fieldList = row.getFieldNames();
		ArrayList<String> valueList = new ArrayList<String>();
		for (int i = 0; i < fieldList.size(); i++) 
		{
			valueList.add(row.getString(fieldList.get(i)));
		}
		return valueList;
	}
	

}
