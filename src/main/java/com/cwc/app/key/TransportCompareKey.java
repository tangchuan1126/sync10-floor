package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class TransportCompareKey
{
	DBRow row;
	public static int Consistent = 1;	//一致
	public static int SnDifferent = 2 ; //序列号有不同
	public static int CountDifferent = 3 ; //数量不等
	public static int AllDifferent = 4 ; //都不同
 
	public TransportCompareKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(TransportCompareKey.Consistent), "一致");
		row.add(String.valueOf(TransportCompareKey.SnDifferent), "序列号不同");
		row.add(String.valueOf(TransportCompareKey.CountDifferent), "数量不等");
		row.add(String.valueOf(TransportCompareKey.AllDifferent), "都有差异");
	}
	
	public ArrayList TransportReceiveCompareKey()
	{
		return(row.getFieldNames());
	}

	public String TransportReceiveCompareKey(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
