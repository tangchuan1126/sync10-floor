package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class InvenOperateTypeKey
{
	DBRow row;
	public static int CREATE = 1;	//
	public static int UPDATE = 2 ; //
	public static int DELETE = 3 ; //
 
	public InvenOperateTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(InvenOperateTypeKey.CREATE), "增");
		row.add(String.valueOf(InvenOperateTypeKey.UPDATE), "删");
		row.add(String.valueOf(InvenOperateTypeKey.DELETE), "改");
		
	}
	
	public ArrayList getInvenOperateTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getInvenOperateTypeKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
