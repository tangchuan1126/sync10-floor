package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class CheckInEntryWaitingOrNotKey{

	DBRow row;
	public static int WAITING = 1;
	
	public CheckInEntryWaitingOrNotKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(CheckInEntryWaitingOrNotKey.WAITING), "Waiting"); 	//entry waiting
	}

	public ArrayList getCheckInEntryWaitingOrNotKeys() {
		return (row.getFieldNames());
	}

	public String getCheckInEntryWaitingOrNotKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getCheckInEntryWaitingOrNotKeyValue(String id) {
		return (row.getString(id));
	}

}
