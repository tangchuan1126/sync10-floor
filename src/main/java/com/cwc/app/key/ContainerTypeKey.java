package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ContainerTypeKey {
 
	DBRow row;
	public static final int Original = 0;
	public static final int CLP = 1 ; //CLP   商品托盘
//	public static final int BLP = 2 ; //BLP	主箱子
	public static final int TLP = 3 ; //TLP	临时托盘
//	public static final int ILP = 4;	//ILP	内箱
 
	public ContainerTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ContainerTypeKey.Original),"Original");
		row.add(String.valueOf(ContainerTypeKey.CLP), "CLP");
//		row.add(String.valueOf(ContainerTypeKey.BLP), "BLP");
		row.add(String.valueOf(ContainerTypeKey.TLP), "TLP");
//		row.add(String.valueOf(ContainerTypeKey.ILP), "ILP");
	}
	
	public ArrayList getContainerTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getContainerTypeKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getContainerTypeKeyValue(String id)
	{
		return(row.getString(id));
	}
}
