package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ReturnReceiveFromTypeKey {

	DBRow row;
	public static int WALMART = 1;
	public static int COSTCO = 2;
	public static int BEST_BUY = 3;
	public static int AMAZON = 4;
	public static int AARONS = 5;
	public static int BJ_WHOLESALE = 6;
	
	public static int DBLINGRAM_MICRO = 7;
	public static int MEIJER = 8;
	public static int NFM = 9;
	public static int MICROSOFT = 10;
	public static int MITO = 11;
	public static int TIGER_DIRECT = 12;
	public static int TECH_DATA = 13;
	
	public ReturnReceiveFromTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnReceiveFromTypeKey.WALMART), "Wal-Mart");
		row.add(String.valueOf(ReturnReceiveFromTypeKey.COSTCO), "Costco");
		row.add(String.valueOf(ReturnReceiveFromTypeKey.BEST_BUY), "Best Buy");
		row.add(String.valueOf(ReturnReceiveFromTypeKey.AMAZON), "Amazon");
		row.add(String.valueOf(ReturnReceiveFromTypeKey.AARONS), "Aarons");
		row.add(String.valueOf(ReturnReceiveFromTypeKey.BJ_WHOLESALE), "BJ\'s Wholesale");
		
		row.add(String.valueOf(ReturnReceiveFromTypeKey.DBLINGRAM_MICRO), "DBL-Ingram Micro");
		row.add(String.valueOf(ReturnReceiveFromTypeKey.MEIJER), "Meijer");
		row.add(String.valueOf(ReturnReceiveFromTypeKey.NFM), "NFM");
		row.add(String.valueOf(ReturnReceiveFromTypeKey.MICROSOFT), "Microsoft");
		row.add(String.valueOf(ReturnReceiveFromTypeKey.MITO), "Mito");
		row.add(String.valueOf(ReturnReceiveFromTypeKey.TIGER_DIRECT), "NFM");
		row.add(String.valueOf(ReturnReceiveFromTypeKey.TECH_DATA), "Tech Data");
	}

	public ArrayList getReturnReceiveFromTypeKeys() {
		return (row.getFieldNames());
	}

	public String getReturnReceiveFromTypeKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getReturnReceiveFromTypeKeyValue(String id) {
		return (row.getString(id));
	}

}
