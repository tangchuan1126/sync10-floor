package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class LoadUnloadRelationTypeKey {
	
	DBRow dbRow;
	public static int TRANSPORT = 1;//转运单
	
	public LoadUnloadRelationTypeKey()
	{
		dbRow = new DBRow();
		dbRow.add(String.valueOf(LoadUnloadRelationTypeKey.TRANSPORT), "转运单");
	}
	
	public ArrayList getLoadUnloadRelationTypes()
	{
		return(dbRow.getFieldNames());
	}

	public String getLoadUnloadRelationTypeName(String id)
	{
		return(dbRow.getString(id));
	}
	
	public String getLoadUnloadRelationTypeName(int id)
	{
		return(dbRow.getString(String.valueOf(id)));
	}
}
