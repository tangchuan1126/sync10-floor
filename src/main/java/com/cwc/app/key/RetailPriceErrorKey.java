package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**
 * 零售报价导入错误
 * @author Administrator
 *
 */
public class RetailPriceErrorKey
{
	DBRow row;
	
	public static int NOT_NUMBER = 1;
	public static int ZERO = 2;
	public static int LOST_MONEY = 3;
	public static int FORMAT_ERRO = 4;
	
	public RetailPriceErrorKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(RetailPriceErrorKey.NOT_NUMBER),"非数字");
		row.add(String.valueOf(RetailPriceErrorKey.ZERO),"不能为0");
		row.add(String.valueOf(RetailPriceErrorKey.LOST_MONEY),"亏本");
		row.add(String.valueOf(RetailPriceErrorKey.FORMAT_ERRO),"存在错误数据");
	}

	public ArrayList getKeys()
	{
		return(row.getFieldNames());
	}

	public String getRetailPriceErrorById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
