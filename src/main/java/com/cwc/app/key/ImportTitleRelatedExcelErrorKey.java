package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ImportTitleRelatedExcelErrorKey 
{
	DBRow row;
	public static int ADMIN_IS_BLANK = 1;//account为空
	public static int ADMIN_NOT_EXIST = 2;//account不存在
	public static int TITLE_IS_BLANK = 3;//title为空
	public static int TITLE_NOT_EXIST = 4;//title不存在
	public static int TITLE_SORT_BLANK = 5;//title优先级为空
	public static int TITLE_SORT_NOT_NUMBER = 6;//title优先级只能为数字
	public static int TITLE_SORT_REPEAT = 7;//title优先级重复
	
	public static int ADMIN_TITLE_REPEAT = 9;//adminTitle重复
	public static int PRODUCT_IS_BLANK = 10;//商品名为空
	public static int PRODUCT_NOT_EXIST = 11;//商品不存在
	public static int PRODUCT_TITLE_REPEAT = 12;//商品title重复
	
	public static int ADMIN_TITLE_NOT_EXIST = 13;//此用户无此title
	
	public ImportTitleRelatedExcelErrorKey(){
		
		row = new DBRow();
		row.add(String.valueOf(ImportTitleRelatedExcelErrorKey.ADMIN_IS_BLANK), "account为空");
		row.add(String.valueOf(ImportTitleRelatedExcelErrorKey.ADMIN_NOT_EXIST),"account不存在");
		row.add(String.valueOf(ImportTitleRelatedExcelErrorKey.TITLE_IS_BLANK), "title为空");
		row.add(String.valueOf(ImportTitleRelatedExcelErrorKey.TITLE_NOT_EXIST),"title不存在");
		row.add(String.valueOf(ImportTitleRelatedExcelErrorKey.TITLE_SORT_BLANK),"title优先级为空");
		row.add(String.valueOf(ImportTitleRelatedExcelErrorKey.TITLE_SORT_NOT_NUMBER),"title优先级只能为数字");
		row.add(String.valueOf(ImportTitleRelatedExcelErrorKey.TITLE_SORT_REPEAT),"同用户内title优先级重复");
		
		row.add(String.valueOf(ImportTitleRelatedExcelErrorKey.ADMIN_TITLE_REPEAT),"用户title重复");
		row.add(String.valueOf(ImportTitleRelatedExcelErrorKey.PRODUCT_IS_BLANK),"商品条码为空");
		row.add(String.valueOf(ImportTitleRelatedExcelErrorKey.PRODUCT_NOT_EXIST),"商品不存在");
		row.add(String.valueOf(ImportTitleRelatedExcelErrorKey.PRODUCT_TITLE_REPEAT),"同商品内title重复");
		row.add(String.valueOf(ImportTitleRelatedExcelErrorKey.ADMIN_TITLE_NOT_EXIST),"此用户无此title");
		
	}
	
	public ArrayList getImportTitleRelatedExcelErrorKeys()
	{
		return(row.getFieldNames());
	}

	public String getImportTitleRelatedExcelErrorKeyById(String id)
	{
		return(row.getString(id));
	}
	
	public String getImportTitleRelatedExcelErrorKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
}
