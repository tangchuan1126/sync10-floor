package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**
 * 取自以下表
 * apply_money_association_type
 * @author Administrator
 *
 */
public class FinanceApplyTypeKey {
	
	DBRow dbRow;
	
	public static int NO_ASOCIATE		= 1;//无关联
	public static int FIXED_ASSETS		= 2;//固定资产
	public static int SAMPLE			= 3;//样品
	public static int PURCHASE_ORDER	= 4;//采购单
	public static int DELIVERY_ORDER	= 5;//交货单
	public static int TRANSPORT_ORDER	= 6;//转运单
	public static int REPAIR_ORDER		= 7;//返修单
	public static int B2B_ORDER			= 8;//B2B订单

	public FinanceApplyTypeKey() {
		dbRow = new DBRow();
		dbRow.add(String.valueOf(FinanceApplyTypeKey.NO_ASOCIATE), "无关联");
		dbRow.add(String.valueOf(FinanceApplyTypeKey.FIXED_ASSETS), "固定资产");
		dbRow.add(String.valueOf(FinanceApplyTypeKey.SAMPLE), "样品");
		dbRow.add(String.valueOf(FinanceApplyTypeKey.PURCHASE_ORDER), "采购单");
		dbRow.add(String.valueOf(FinanceApplyTypeKey.DELIVERY_ORDER), "交货单");
		dbRow.add(String.valueOf(FinanceApplyTypeKey.TRANSPORT_ORDER), "转运单");
		dbRow.add(String.valueOf(FinanceApplyTypeKey.REPAIR_ORDER), "返修单");
		dbRow.add(String.valueOf(FinanceApplyTypeKey.B2B_ORDER), "B2B订单");
	}
	
	public ArrayList getFinanceApplyTypeKeyIds()
	{
		return(dbRow.getFieldNames());
	}

	public String getFinanceApplyTypeKeyName(String id)
	{
		return(dbRow.getString(id));
	}
}
