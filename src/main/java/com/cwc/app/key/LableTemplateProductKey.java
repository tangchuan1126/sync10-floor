package com.cwc.app.key;

import com.cwc.db.DBRow;


public class LableTemplateProductKey extends LableTemplateProductCommonKey
{	
	
	public LableTemplateProductKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(FIRST), "商品标签");
		row.add(String.valueOf(SECOND), "位置标签");
		row.add(String.valueOf(THIRD), "外箱商品标签");

	}

}
