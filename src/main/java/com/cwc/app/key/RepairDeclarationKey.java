package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class RepairDeclarationKey {
	DBRow row;
	
	public static int NODELARATION = 1;	//无报关
	public static int DELARATION = 2;//有报关
	public static int DELARATING = 3;//报关中
	public static int FINISH = 4;//报关完成
	
	public RepairDeclarationKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(RepairDeclarationKey.NODELARATION), "无需报关");
		row.add(String.valueOf(RepairDeclarationKey.DELARATION), "需要报关");
		row.add(String.valueOf(RepairDeclarationKey.DELARATING), "报关中");
		row.add(String.valueOf(RepairDeclarationKey.FINISH), "报关完成");
	}
	
	public ArrayList<String> getStatuses()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
