package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ImportTransportDetailErrorKey {
	DBRow row;
	
	public static String ProductNotExist = "ProductNotExist";
	public static String CountNoOne	= "CountNoOne";
	public static String SerialNumberRepeat = "SerialNumberRepeat";
	public static String ProductNotInPlate = "ProductNotInPlate";
	

	public ImportTransportDetailErrorKey() 
	{
		row = new DBRow();
		row.add(ImportTransportDetailErrorKey.ProductNotExist,"<font color='red'>商品不存在</font>");
		row.add(ImportTransportDetailErrorKey.CountNoOne,"<font color='red'>序列号只能标识唯一商品</font>");
		row.add(ImportTransportDetailErrorKey.SerialNumberRepeat,"<font color='red'>序列号重复</font>");
		row.add(ImportTransportDetailErrorKey.ProductNotInPlate,"<font color='red'>货物没放在托盘</font>");
		
	}
	
	public ArrayList getErrorMessage()
	{
		return(row.getFieldNames());
	}

	public String getErrorMessageById(String id)
	{
		return(row.getString(id));
	}
}
