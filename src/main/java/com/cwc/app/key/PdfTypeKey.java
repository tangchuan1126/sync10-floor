package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class PdfTypeKey {
	    DBRow row;
	    public static final int Invoice= 1;//special_task invoice  pdf
	   

	    public PdfTypeKey()
	    {
	        row = new DBRow();
	        row.add(String.valueOf(Invoice), "Invoice");
	    }

	    public ArrayList<?> getStatus()
	    {
	        return row.getFieldNames();
	    }

	    public String getStatusById(int id)
	    {
	        return row.getString(String.valueOf(id));
	    }
         
	    public DBRow getRow()
	    {
	        return row;
	    }
	   


}
