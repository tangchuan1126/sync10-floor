package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WebcamPositionTypeKey {
	DBRow row;
	public static final int LOCATION = 1;
	public static final int STAGING = 2 ;
	public static final int DOCKS = 3 ;
	public static final int PARKING = 4 ;
	public static final int AREA = 5 ;
	public static final int WEBCAM = 6 ;
	public static final int PRINTER = 7 ;
	public static final int ROAD = 8 ;
	public static final int LIGHT = 9 ;
	
	public WebcamPositionTypeKey() {
		row = new DBRow();
		row.add(String.valueOf(WebcamPositionTypeKey.LOCATION), "LOCATION");
		row.add(String.valueOf(WebcamPositionTypeKey.STAGING), "STAGING");
		row.add(String.valueOf(WebcamPositionTypeKey.DOCKS), "DOCKS");
		row.add(String.valueOf(WebcamPositionTypeKey.PARKING), "PARKING");
		row.add(String.valueOf(WebcamPositionTypeKey.AREA), "AREA");
		row.add(String.valueOf(WebcamPositionTypeKey.WEBCAM), "WEBCAM");
		row.add(String.valueOf(WebcamPositionTypeKey.PRINTER), "PRINTER");
		row.add(String.valueOf(WebcamPositionTypeKey.ROAD), "ROAD");
		row.add(String.valueOf(WebcamPositionTypeKey.LIGHT), "LIGHT");
	}
	public ArrayList getWebcamPositionTypeKeyKeys()
	{
		return(row.getFieldNames());
	}

	public String getWebcamPositionTypeKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
