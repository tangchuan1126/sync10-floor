package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class PurchaseMoneyKey {
	DBRow row;
	
	public static int NOTTRANSFER = 1;
	public static int PARTTRANSFER = 2;
	public static int ALLTRANSFER = 3;
	public static int CANNOTTRANSFER = 4;
	
	public PurchaseMoneyKey() 
	{
		row = new DBRow();
	
		row.add(String.valueOf(PurchaseMoneyKey.NOTTRANSFER),"未转款");
		row.add(String.valueOf(PurchaseMoneyKey.PARTTRANSFER),"<font color=red>部分转款</font>");
		row.add(String.valueOf(PurchaseMoneyKey.ALLTRANSFER),"全部转款");
		row.add(String.valueOf(PurchaseMoneyKey.CANNOTTRANSFER),"不可转款");
	}
	
	public ArrayList getQuoteStatus()
	{
		return(row.getFieldNames());
	}

	public String getQuoteStatusById(String id)
	{
		return(row.getString(id));
	}
}
