package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class OccupyStatusTypeKey {

	DBRow row;
	public static int RESERVERED = 1;
	public static int OCUPIED = 2;
	public static int RELEASED = 3;

	
	
	public OccupyStatusTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(OccupyStatusTypeKey.RESERVERED), "Reserved");	//保留
		row.add(String.valueOf(OccupyStatusTypeKey.OCUPIED), "Occupied");		//占用
		row.add(String.valueOf(OccupyStatusTypeKey.RELEASED), "Released"); 	//释放 也就是Free	
	}

	public ArrayList getOccupyStatusKeys() {
		return (row.getFieldNames());
	}

	public String getOccupyStatusValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getOccupyStatusValue(String id) {
		return (row.getString(id));
	}

}
