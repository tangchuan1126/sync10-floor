package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class OccupyTypeKey {

	DBRow dbRow;
	public static int DOOR = 1;
	public static int SPOT = 2;
	
	public OccupyTypeKey()
	{
		dbRow = new DBRow();
		dbRow.add(String.valueOf(OccupyTypeKey.DOOR), "Door");
		dbRow.add(String.valueOf(OccupyTypeKey.SPOT), "Spot");
	}
	
	public ArrayList getOccupyTypeKeys()
	{
		return(dbRow.getFieldNames());
	}

	public String getOccupyTypeKeyName(String id)
	{
		return(dbRow.getString(id));
	}
	
	public String getOccupyTypeKeyName(int id)
	{
		return(dbRow.getString(String.valueOf(id)));
	}
}
