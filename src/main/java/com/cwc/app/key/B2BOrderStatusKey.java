package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderStatusKey {

	DBRow row;
	
	public static int Enough = 1;			//足够
	public static int ConfigNotEnough = 2;	//配置不够
	public static int ProductNotEnough = 3;	//商品不够
	
	public B2BOrderStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderStatusKey.Enough),"<font color=\"green\">足够</font>");
		row.add(String.valueOf(B2BOrderStatusKey.ConfigNotEnough),"<font color=\"blue\">配置不够</font>");
		row.add(String.valueOf(B2BOrderStatusKey.ProductNotEnough),"<font color=\"red\">商品不够</font>");
	}

	public ArrayList getB2BOrderStatusKeys()
	{
		return(row.getFieldNames());
	}

	public String getB2BOrderStatusKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
