package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class ReturnProductKey
{
	DBRow row;
	
	public static int FINISH = 1;			//登记退货处理
	public static int CANCEL = 2;			//暂无
	public static int WAITINGPRODUCT = 3;	//创建退货明细时
	public static int FINISHALL = 4;		//原暂无，现:退货完成
	public static int WAITINGPAY = 5;		//创建账单
	public static int CREATEORDER = 6;		//创建账单时，售后承担，创建订单
	public static int LITTLE_PRODUCT = 7;	 //部分退货
	public static int WAITING = 8;			//刚创建退货
	
	public ReturnProductKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnProductKey.WAITING),"等待处理");
		row.add(String.valueOf(ReturnProductKey.WAITINGPRODUCT),"<font color='#ff6600'><strong>等待退货</strong></font>");
		row.add(String.valueOf(ReturnProductKey.FINISH),"<font color=blue><strong>登记退货</strong></font>");
		row.add(String.valueOf(ReturnProductKey.CANCEL),"<font color=red><strong>取消</strong></font>");
		row.add(String.valueOf(ReturnProductKey.FINISHALL),"<font color=green><strong>退货完成</strong></font>");
		row.add(String.valueOf(ReturnProductKey.WAITINGPAY),"<font color='#000099'><strong>等待付款</strong></font>");
		row.add(String.valueOf(ReturnProductKey.CREATEORDER),"<font color='#cc3300'><strong>已有订单</strong></font>");
		row.add(String.valueOf(ReturnProductKey.LITTLE_PRODUCT), "部分退货");
	}
	
	public ArrayList getReturnProductStatus()
	{
		return(row.getFieldNames());
	}

	public String getReturnProductStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
