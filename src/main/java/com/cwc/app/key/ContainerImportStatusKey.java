package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ContainerImportStatusKey {

	DBRow row;
	
	public static int IMPORTED = 1;
	public static int SHIPPED = 2;
	public static int RECEIVED = 3;
	
	public ContainerImportStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ContainerImportStatusKey.IMPORTED), "imported");
		row.add(String.valueOf(ContainerImportStatusKey.SHIPPED), "shipped");
		row.add(String.valueOf(ContainerImportStatusKey.RECEIVED), "received");
	}

	public ArrayList getContainerImportStatusKeys()
	{
		return(row.getFieldNames());
	}

	public String getContainerImportStatusKey(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
}
