package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class PerformStatusKey
{
	DBRow row;
	public static int NotImplemented = 1;	//未执行
	public static int Executed = 2;			//已执行
 
	public PerformStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(PerformStatusKey.NotImplemented), "未执行");
		row.add(String.valueOf(PerformStatusKey.Executed), "已执行");
		
	}
	
	public ArrayList getPerformStatusKeys()
	{
		return(row.getFieldNames());
	}

	public String getPerformStatus(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
