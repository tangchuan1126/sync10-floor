package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderTagKey {

	DBRow row;
	
	public DBRow getRow() {
		return row;
	}

	public static int NOTAG = 1;			 
	public static int TAG = 2;		 
	public static int FINISH = 3;
	public static int FINISH_PURCHASE = 4;
 
	public B2BOrderTagKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderTagKey.NOTAG), "无需打签");
		row.add(String.valueOf(B2BOrderTagKey.TAG), "需要打签");
		row.add(String.valueOf(B2BOrderTagKey.FINISH), "打签完成");
		row.add(String.valueOf(B2BOrderTagKey.FINISH_PURCHASE), "采购单内部标签完成");
		 
	}

	public ArrayList<String> getB2BOrderTags()
	{
		return((ArrayList<String>)row.getFieldNames());
	}

	public String getB2BOrderTagById(String id)
	{
		return(row.getString(id));
	}
	
	public String getB2BOrderTagById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
