package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WaitingTypeKey
{
	DBRow row;
	public static int NotWaiting = 0;		//无需等待		报表计算用时用gate_check_out_time - (有window_check_in_time用window,没有用dock_check_in_time)
	public static int EarlyWaiting = 1 ; 	//来早等待  早<=2h 报表计算用时用gate_check_out_time - appointment_time 
	public static int LateWaiting = 2 ; 	//来晚等待  晚>=1h 报表计算用时用gate_check_out_time - dock_check_in_time
	public static int WarehouseWaitng = 3 ; //仓库等待	报表计算用时用gate_check_out_time - window_check_in_time
	public static int NoAppointment = 4 ; //没有预约时间	报表计算用时用gate_check_out_time - dock_check_in_time
 
	public WaitingTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WaitingTypeKey.NotWaiting), "No Waiting");
		row.add(String.valueOf(WaitingTypeKey.EarlyWaiting), "Early Waiting");
		row.add(String.valueOf(WaitingTypeKey.LateWaiting), "Late Waiting");
		row.add(String.valueOf(WaitingTypeKey.WarehouseWaitng),"Warehouse Waitng");		
		row.add(String.valueOf(WaitingTypeKey.NoAppointment),"Waiting");		
	}
	
	public ArrayList getWaitingTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getWaitingTypeKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
