package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WmsItemUnitTicketKey {
	DBRow row;
	
	public static String PIECE = "Piece";
	public static String PALLET = "Pallet";
	public static String PACKAGE = "Package";
	public static String CONTAINER = "Container";
	public static String INNER_CASE = "Inner Case"; 
	
	public WmsItemUnitTicketKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WmsItemUnitTicketKey.PIECE), "PIC");
		row.add(String.valueOf(WmsItemUnitTicketKey.PALLET), "PLT");
		row.add(String.valueOf(WmsItemUnitTicketKey.PACKAGE), "PKG");
		row.add(String.valueOf(WmsItemUnitTicketKey.CONTAINER), "CNTR");
		row.add(String.valueOf(WmsItemUnitTicketKey.INNER_CASE), "INNER");
	}
	
	public ArrayList getWmsItemUnitKeys()
	{
		return(row.getFieldNames());
	}

	public String getWmsItemUnitKeyByKey(String id)
	{
		return(row.getString(id));
	}
	
}
