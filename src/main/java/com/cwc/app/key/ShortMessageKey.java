package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ShortMessageKey {
	
	DBRow dbRow;
	public static int NOT_SEND= 0;//未发送，没有电话号码
	public static int SEND_SUCCESS = 1;//发送成功
	public static int SEND_FAIL = 2 ; //发送失败
	public static int IFACE_ERROR = 3;//短信接口问题
	public static int PROGRAM_EXCEPTION	= 500; //程序异常
	public static int NOT_CONTENT		= -9;//短信内容为空
	public static int CONTENT_LONG		= -10;//短信内容超长
	public static int BALANCE_NOT_ENOUGH= -11;//余额不足
	public static int CONTENT_ILLEGAL	= -12;//内容有非法词
	
	public ShortMessageKey() {
		dbRow = new DBRow();
		dbRow.add(String.valueOf(ShortMessageKey.NOT_SEND), "未发送");
		dbRow.add(String.valueOf(ShortMessageKey.SEND_SUCCESS), "发送成功");
		dbRow.add(String.valueOf(ShortMessageKey.SEND_FAIL), "发送失败");
		dbRow.add(String.valueOf(ShortMessageKey.IFACE_ERROR), "短信接口问题");
		dbRow.add(String.valueOf(ShortMessageKey.PROGRAM_EXCEPTION), "程序异常");
		dbRow.add(String.valueOf(ShortMessageKey.NOT_CONTENT), "短信内容为空");
		dbRow.add(String.valueOf(ShortMessageKey.CONTENT_LONG), "短信内容超长");
		dbRow.add(String.valueOf(ShortMessageKey.BALANCE_NOT_ENOUGH), "余额不足");
		dbRow.add(String.valueOf(ShortMessageKey.CONTENT_ILLEGAL), "内容有非法词");
	}
	
	public ArrayList getShortMessgeKeyIds()
	{
		return(dbRow.getFieldNames());
	}

	public String getShortMessageKeyName(String id)
	{
		return(dbRow.getString(id));
	}
}



