package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class QAKey {
	DBRow row;
	
	public static int ALL = 0;
	public static int UNANSWERED = 1;
	public static int HASANSWERED = 2;

	public QAKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(QAKey.UNANSWERED),"<font color='#4a8ebc'>未回答</font>");
		row.add(String.valueOf(QAKey.HASANSWERED),"已回答");
	}
	
	public ArrayList getQuoteStatus()
	{
		return(row.getFieldNames());
	}

	public String getQuoteStatusById(String id)
	{
		return(row.getString(id));
	}
}
