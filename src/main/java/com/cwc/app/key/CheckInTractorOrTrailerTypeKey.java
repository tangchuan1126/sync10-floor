package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class CheckInTractorOrTrailerTypeKey {

	DBRow row;
	public static int TRACTOR = 1;
	public static int TRAILER = 2;
//	public static int TRACTOR_TRAILER = 2;
//	public static int ANOTHER_TRACTOR_TRAILER = 3;
	
	public CheckInTractorOrTrailerTypeKey() 
	{
		row = new DBRow();

		row.add(String.valueOf(CheckInTractorOrTrailerTypeKey.TRACTOR), "Tractor");
		row.add(String.valueOf(CheckInTractorOrTrailerTypeKey.TRAILER), "Trailer");
//		row.add(String.valueOf(CheckInTractorOrTrailerTypeKey.TRACTOR_TRAILER), "Tractor+Trailer");
//		row.add(String.valueOf(CheckInTractorOrTrailerTypeKey.ANOTHER_TRACTOR_TRAILER), "Tractor+Another Trailer");
 
	}

	public ArrayList getContainerTypeKeys() {
		return (row.getFieldNames());
	}

	public String getContainerTypeKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getContainerTypeKeyValue(String id) {
		return (row.getString(id));
	}

}
