package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderDrawbackKey {
	DBRow row;
	
	public static int NODRAWBACK = 1;	//无退税
	public static int DRAWBACK = 2;//有退税
	public static int DRAWBACKING = 3;//退税中
	public static int FINISH = 4;//退税完成
	
	public B2BOrderDrawbackKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderDrawbackKey.NODRAWBACK), "不需要退税");
		row.add(String.valueOf(B2BOrderDrawbackKey.DRAWBACK), "需要退税");
		row.add(String.valueOf(B2BOrderDrawbackKey.DRAWBACKING), "退税中");
		row.add(String.valueOf(B2BOrderDrawbackKey.FINISH), "退税完成");
	}
	
	public ArrayList<String> getB2BOrderDrawbackKeys()
	{
		return(row.getFieldNames());
	}

	public String getB2BOrderDrawbackKeyById(String id)
	{
		return(row.getString(id));
	}
	
	public String getB2BOrderDrawbackKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
