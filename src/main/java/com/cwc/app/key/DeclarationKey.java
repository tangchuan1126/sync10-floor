package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class DeclarationKey {
	DBRow row;
	
	public static int NODELARATION = 1;	//无报关
	public static int DELARATION = 2;//有报关
	public static int DELARATING = 3;//报关中
	public static int FINISH = 4;//报关完成
	
	public DeclarationKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(DeclarationKey.NODELARATION), "无需报关");
		row.add(String.valueOf(DeclarationKey.DELARATION), "需要报关");
		row.add(String.valueOf(DeclarationKey.DELARATING), "报关中");
		row.add(String.valueOf(DeclarationKey.FINISH), "报关完成");
	}
	
	public ArrayList<String> getStatuses()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
