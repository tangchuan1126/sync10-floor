package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class ApprovePurchaseKey
{
	DBRow row;
	
	public static int WAITAPPROVE = 0;		//待审核
	public static int APPROVE = 1;	//已审核
	
	public ApprovePurchaseKey() 
	{
		row = new DBRow();
	}

	public ArrayList getOrderRateStatus()
	{
		return(row.getFieldNames());
	}

	public String getOrderRateStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
