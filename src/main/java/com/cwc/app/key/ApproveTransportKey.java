package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class ApproveTransportKey
{
	DBRow row;
	
	public static int WAITAPPROVE = 1;		//待审核
	public static int APPROVE = 2;	//已审核
	
	public ApproveTransportKey() 
	{
		row = new DBRow();
	}

	public ArrayList getApproveTransportStatus()
	{
		return(row.getFieldNames());
	}

	public String getApproveTransportStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
