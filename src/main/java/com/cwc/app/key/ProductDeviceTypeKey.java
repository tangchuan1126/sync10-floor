package com.cwc.app.key;

import java.util.ArrayList;
import java.util.List;

import com.cwc.db.DBRow;

public class ProductDeviceTypeKey {

	DBRow row;
	// 信息来源（设备类型）
	public static int Web = 1;
	public static int Android = 2;
	public static int Excel = 3;
	public static int EDI = 4;
	public static int Other = 5;

	public ProductDeviceTypeKey() {
		
		row = new DBRow();
		row.add(String.valueOf(ProductDeviceTypeKey.Web), "Web");
		row.add(String.valueOf(ProductDeviceTypeKey.Android), "Android");
		row.add(String.valueOf(ProductDeviceTypeKey.Excel), "Excel");
		row.add(String.valueOf(ProductDeviceTypeKey.EDI), "EDI");
		row.add(String.valueOf(ProductDeviceTypeKey.Other), "Other");
	}
	
	public DBRow[] getKey() {
		
		List<?> list = this.getProductTypeKeys();
		
		DBRow[] rowArray = new DBRow[list.size()];
		
		for(int i = 0;i<list.size();i++){
			
			DBRow dbrow = new DBRow();
			dbrow.put("id", Integer.valueOf(list.get(i).toString()));
			dbrow.put("name", this.getProductTypeKeyValue(Integer.valueOf(list.get(i).toString())));
			
			rowArray[i] = dbrow;
		}
		
		return rowArray;
	}

	public ArrayList<?> getProductTypeKeys() {
		
		return (row.getFieldNames());
	}

	public String getProductTypeKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}
}
