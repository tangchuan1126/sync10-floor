package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ImportContainerErrorKey {
	
	DBRow row;
	
	public static String CONTAINERID = "CONTAINERID";//容器编号不能为空
	public static String HARDWOARID = "HARDWOARID";//容器硬件号不能为空
	public static String CONTAINERTYPEERROR = "CONTAINERTYPEERROR";//容器类型不正确
	
	public ImportContainerErrorKey(){
		
		row = new DBRow();
		row.add(ImportContainerErrorKey.CONTAINERID,"<font color=red>容器编号不能为空</font>");
		row.add(ImportContainerErrorKey.HARDWOARID,"<font color=red>容器硬件号不能为空</font>");
		row.add(ImportContainerErrorKey.CONTAINERTYPEERROR,"<font color=red>容器类型不存在，请核实类别是否输入正确</font>");
		
	}
	public ArrayList getContainerErrorMessage()
	{
		return(row.getFieldNames());
	}

	public String getContainerErrorMessageById(String id)
	{
		return(row.getString(id));
	}

}
