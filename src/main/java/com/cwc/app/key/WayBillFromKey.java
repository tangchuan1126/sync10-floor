package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WayBillFromKey {
DBRow row;
	
	public static int Record = 1;		//抄单生成
	public static int Split = 2;		//拆分生成
	public static int Replace = 3;		//替换生成
	
	public WayBillFromKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WayBillFromKey.Record), "抄单生成");
		row.add(String.valueOf(WayBillFromKey.Split), "拆分生成");
		row.add(String.valueOf(WayBillFromKey.Replace), "替换生成");
	}

	public ArrayList getWayBillFrom()
	{
		return(row.getFieldNames());
	}

	public String getWayBillFromById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
