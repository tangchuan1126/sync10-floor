package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class DrawbackKey {
	DBRow row;
	
	public static int NODRAWBACK = 1;	//无退税
	public static int DRAWBACK = 2;//有退税
	public static int DRAWBACKING = 3;//退税中
	public static int FINISH = 4;//退税完成
	
	public DrawbackKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(DrawbackKey.NODRAWBACK), "不需要退税");
		row.add(String.valueOf(DrawbackKey.DRAWBACK), "需要退税");
		row.add(String.valueOf(DrawbackKey.DRAWBACKING), "退税中");
		row.add(String.valueOf(DrawbackKey.FINISH), "退税完成");
	}
	
	public ArrayList<String> getDrawbacks()
	{
		return(row.getFieldNames());
	}

	public String getDrawbackById(String id)
	{
		return(row.getString(id));
	}
	
	public String getDrawbackById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
