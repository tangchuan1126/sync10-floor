package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ReturnServicePalletHandleStatusKey {
	
	DBRow row;
	public static int NON_PROCESS = 1;	//未处理
	public static int PROCESSING = 2 ; //处理中
	public static int FINISH_PROCESS = 3 ; //处理完成
 
	public ReturnServicePalletHandleStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnServicePalletHandleStatusKey.NON_PROCESS), "未处理");
		row.add(String.valueOf(ReturnServicePalletHandleStatusKey.PROCESSING), "处理中");
		row.add(String.valueOf(ReturnServicePalletHandleStatusKey.FINISH_PROCESS), "处理完成");
		
	}
	
	public ArrayList getReturnServicePalletHandleStatusKeys()
	{
		return(row.getFieldNames());
	}

	public String getReturnServicePalletHandleStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getReturnServicePalletHandleStatusById(String id)
	{
		return(row.getString(id));
	}
}
