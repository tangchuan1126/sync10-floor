package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class BillTypeKey {

	DBRow row;
	
	public DBRow getRow() {
		return row;
	}

	public static int ebay = 1;			 
	public static int product = 2;		 
	public static int shipping = 3;
	public static int warranty = 4;
	public static int order =  5; // pay for order
 
	public BillTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(BillTypeKey.ebay), "Ebay");
		row.add(String.valueOf(BillTypeKey.product), "Products");
		row.add(String.valueOf(BillTypeKey.shipping), "Shipping");
		row.add(String.valueOf(BillTypeKey.warranty), "Warranty");
		row.add(String.valueOf(BillTypeKey.order), "For Order");
		 
	}

	public ArrayList<String> getStatus()
	{
		return((ArrayList<String>)row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
