package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class CheckInMainDocumentsRelTypeKey {

	DBRow row;
	public final static int DELIVERY = 1;				//收货
	public final static int PICK_UP = 2;				//装货
	public final static int BOTH = 3;
	public final static int NONE = 4;
	public final static int CTNR = 5;
	public final static int PATROL = 6;
	public final static int VISITOR = 7;
	public final static int SMALL_PARCEL = 8;
	
	
	public CheckInMainDocumentsRelTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(CheckInMainDocumentsRelTypeKey.DELIVERY), "Inbound");
		row.add(String.valueOf(CheckInMainDocumentsRelTypeKey.PICK_UP), "Outbound");
		row.add(String.valueOf(CheckInMainDocumentsRelTypeKey.BOTH), "In / Out");
		row.add(String.valueOf(CheckInMainDocumentsRelTypeKey.NONE), "Unknown");
		row.add(String.valueOf(CheckInMainDocumentsRelTypeKey.CTNR), "CTNR");
		row.add(String.valueOf(CheckInMainDocumentsRelTypeKey.PATROL), "Patrol Create");
		row.add(String.valueOf(CheckInMainDocumentsRelTypeKey.VISITOR), "Visitor / Parking");
		row.add(String.valueOf(CheckInMainDocumentsRelTypeKey.SMALL_PARCEL), "Small Parcel");
	}

	public ArrayList getCheckInMainDocumentsRelTypeKeys() {
		return (row.getFieldNames());
	}

	public String getCheckInMainDocumentsRelTypeKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getCheckInMainDocumentsRelTypeKeyValue(String id) {
		return (row.getString(id));
	}

}
