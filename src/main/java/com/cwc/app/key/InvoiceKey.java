package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class InvoiceKey {
	DBRow row;
	
	public static int NOINVOICE = 1;//不需要发票
	public static int INVOICE = 2;//需要发票
	public static int INVOICING = 3;//开票中
	public static int FINISH = 4;//开票完成
	
	public InvoiceKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(InvoiceKey.NOINVOICE), "不需要发票");
		row.add(String.valueOf(InvoiceKey.INVOICE), "需要发票");
		row.add(String.valueOf(InvoiceKey.INVOICING), "开票中");
		row.add(String.valueOf(InvoiceKey.FINISH), "开票完成");
	}
	
	public ArrayList getInvoices()
	{
		return(row.getFieldNames());
	}

	public String getInvoiceById(String id)
	{
		return(row.getString(id));
	}
	
	public String getInvoiceById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
