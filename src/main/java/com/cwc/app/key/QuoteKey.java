package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class QuoteKey
{
	DBRow row;
	
	public static int WAITING = 0;
	public static int FINISH = 1;
	public static int CANCEL = 2;
	
	public QuoteKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(QuoteKey.WAITING),"报价中");
		row.add(String.valueOf(QuoteKey.FINISH),"<font color=green><strong>成交</strong></font>");
		row.add(String.valueOf(QuoteKey.CANCEL),"<font color=red>取消</font>");
	}
	
	public ArrayList getQuoteStatus()
	{
		return(row.getFieldNames());
	}

	public String getQuoteStatusById(String id)
	{
		return(row.getString(id));
	}
}
