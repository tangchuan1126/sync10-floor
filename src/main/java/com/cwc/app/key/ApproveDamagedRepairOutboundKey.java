package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class ApproveDamagedRepairOutboundKey
{
	DBRow row;
	
	public static int WAITAPPROVE = 1;		//待审核
	public static int APPROVE = 2;	//已审核
	
	public ApproveDamagedRepairOutboundKey() 
	{
		row = new DBRow();
	}

	public ArrayList getApproveDamagedRepairOutboundStatus()
	{
		return(row.getFieldNames());
	}

	public String getApproveDamagedRepairOutboundStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
