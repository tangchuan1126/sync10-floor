package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;
import com.cwc.app.util.StrUtil;
// 对应 trade 表中的payment_channel 字段
public class PaymentMethodKey {
	
	DBRow row;
	
	public DBRow getRow() {
		return row;
	}
 
	public static int PP = 0;		//PayPal 
	public static int CC = 1;		//Credit Card
	public static int WIRE = 2;		//WIRE
	public static int CASH = 3;     // CASH
	public static int CHECK = 4;
	
	public static int ParentOrder = 5;//父订单
	public static int Free = 6;//	免费
	public static int AMAZON = 7;	//AMAZON
	public static int Warranty = 8;	//质保
	public PaymentMethodKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(PaymentMethodKey.PP), "PayPal");
		row.add(String.valueOf(PaymentMethodKey.CC), "Credit Card");
		row.add(String.valueOf(PaymentMethodKey.AMAZON), "AMAZON");
		row.add(String.valueOf(PaymentMethodKey.WIRE), "WIRE");
		row.add(String.valueOf(PaymentMethodKey.CASH), "CASH");
		row.add(String.valueOf(PaymentMethodKey.CHECK), "CHECK");
		row.add(String.valueOf(PaymentMethodKey.ParentOrder), "ParentOrder");
		row.add(String.valueOf(PaymentMethodKey.Free), "经理审批");
		row.add(String.valueOf(PaymentMethodKey.Warranty),"售后承担");
	}

	public ArrayList<String> getStatus()
	{
		return((ArrayList<String>)row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
