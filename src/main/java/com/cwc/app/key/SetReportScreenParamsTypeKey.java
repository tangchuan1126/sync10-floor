package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**
 * 大屏幕设置 查询的类型
 * @author win7zr
 */
public class SetReportScreenParamsTypeKey {

	DBRow row;
	public final static int HandleTime = 1;	//处理时间 这个屏幕的数据多长时间处理完成 50(min)
	public final static int Red = 2;			//2 存成 百分之 50,60
	public final static int Yellow = 3;		//3存成 百分之 50,60

	 
	
	public SetReportScreenParamsTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(SetReportScreenParamsTypeKey.HandleTime), "Handle Time");
		row.add(String.valueOf(SetReportScreenParamsTypeKey.Red), "Red");
		row.add(String.valueOf(SetReportScreenParamsTypeKey.Yellow), "Yellow");
		 
	}

	public ArrayList getAllKey() {
		return (row.getFieldNames());
	}

	public String getKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

}
