package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;



public class LableTemplateProductCommonKey 
{
	DBRow row;
	
	public static int FIRST= 1;	//ilp 商品标签
	public static int SECOND= 2;//blp 位置标签
	public static int THIRD= 3;//clp 外箱商品标签
	
	public LableTemplateProductCommonKey(){
		row = new DBRow();
		row.add(String.valueOf(FIRST),"商品标签");
		row.add(String.valueOf(SECOND),"位置标签");
		row.add(String.valueOf(THIRD),"外箱商品标签");
	}

	public ArrayList getLableTemplates()
	{
		return(row.getFieldNames());
	}

	public String getLableTemplateNameById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
