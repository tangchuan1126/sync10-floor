package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderRegistrationTypeKey {

	DBRow row;
	public static int SEND = 1;//装货司机签到
	public static int DELEIVER = 2;//缷货司机签到
	
	public B2BOrderRegistrationTypeKey()
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderRegistrationTypeKey.SEND), "装货司机签到");
		row.add(String.valueOf(B2BOrderRegistrationTypeKey.DELEIVER), "缷货司机签到");
	}
	
	public ArrayList getB2BOrderRegistrationTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getB2BOrderRegistrationTypeKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getB2BOrderRegistrationTypeKeyValue(String id)
	{
		return(row.getString(id));
	}
}
