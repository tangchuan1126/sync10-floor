package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;
/**
 *  是否需要退货的形式
 * @author Administrator
 *
 */
public class ReturnTypeKey {

	DBRow row;
	public static int NEED = 1;	//需要退货
	public static int VIRTUAL = 2; //虚拟退货
	public static int NOT_NEED = 3; //无需退货
	
 
	public ReturnTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnTypeKey.NEED), "需要退货");
		row.add(String.valueOf(ReturnTypeKey.VIRTUAL), "虚拟退货");
		row.add(String.valueOf(ReturnTypeKey.NOT_NEED), "无需退货");
	}
	
	public ArrayList getReturnTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getReturnTypeKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getReturnTypeKeyValue(String id)
	{
		return(row.getString(id));
	}
}
