package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderWayKey {
	DBRow row;
	
	public static int SEAWAY = 1;	//海运
	public static int AIRWAY = 3;//空运
	public static int EXPRESSWAY = 4;//快递
	public static int LANDWAY = 2;//陆运
	
	public B2BOrderWayKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderWayKey.SEAWAY), "海运");
		row.add(String.valueOf(B2BOrderWayKey.AIRWAY), "空运");
		row.add(String.valueOf(B2BOrderWayKey.EXPRESSWAY), "快递");
		row.add(String.valueOf(B2BOrderWayKey.LANDWAY), "陆运");
	}
	
	public ArrayList<String> getB2BOrderWayKeys()
	{
		return(row.getFieldNames());
	}

	public String getB2BOrderWayKeyById(int id)
	{
		return row.getString(String.valueOf(id));
	}
}
