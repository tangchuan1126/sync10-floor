package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class MapColorKey 
{
	DBRow row;
	
	public static int N1 = 1; //"#FEA7F8";
	public static int N2 = 2; //"#F867D0";
	public static int N3 = 3;//"#F143CE";
	public static int N4 = 4;//"#CD1D82";
	public static int N5 = 5;//"#960A6B";
	public static int N6 = 6;//"#FF4D4D";
	public static int N7 = 7;//"#FC1829";
	public static int N8 = 8;//"#C80220";
	public static int N9 = 9;//"#950218";
	public static int N10 = 10;//"#730502";
	
	public MapColorKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(MapColorKey.N1),"#fdfc58");
		row.add(String.valueOf(MapColorKey.N2),"#e3cc07");
		row.add(String.valueOf(MapColorKey.N3),"#fdcb00");
		row.add(String.valueOf(MapColorKey.N4),"#e67c00");
		row.add(String.valueOf(MapColorKey.N5),"#ff7e7e");
		row.add(String.valueOf(MapColorKey.N6),"#e60000");
		row.add(String.valueOf(MapColorKey.N7),"#a80505");
		row.add(String.valueOf(MapColorKey.N8),"#9000ff");
		row.add(String.valueOf(MapColorKey.N9),"#560495");
		row.add(String.valueOf(MapColorKey.N10),"#010897");
	}

	public ArrayList geMapColorKey()
	{
		return(row.getFieldNames());
	}

	public String getMapColorKeyById(int status)
	{
		return(row.getString(String.valueOf(status)));
	}
	
}
