package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class PurchaseKey {
	DBRow row;
	
	public static int OPEN = 1;
	public static int LOCK = 2;
	public static int CANCEL = 3;
	public static int FINISH = 4;
	public static int AFFIRMPRICE = 5;
	public static int AFFIRMTRANSFER = 6;
	public static int DELIVERYING = 7;
	public static int APPROVEING = 9;
	public static int NOFINISH = 10;

	public PurchaseKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(PurchaseKey.OPEN),"需确认价格");
		row.add(String.valueOf(PurchaseKey.LOCK),"<font color='#4a8ebc'>锁定</font>");
		row.add(String.valueOf(PurchaseKey.CANCEL),"<font color=red>取消</font>");
		row.add(String.valueOf(PurchaseKey.FINISH),"<font color=green>采购单完成</font>");
		row.add(String.valueOf(PurchaseKey.AFFIRMPRICE),"<font color='#ff6666' >价格已确认</font>");
		row.add(String.valueOf(PurchaseKey.AFFIRMTRANSFER),"<font color=blue>需跟进交货</font>");
		row.add(String.valueOf(PurchaseKey.DELIVERYING),"<font color=blue>交货中</font>");
		
		row.add(String.valueOf(PurchaseKey.APPROVEING),"<font color=blue>到货完成审核中</font>");
		row.add(String.valueOf(PurchaseKey.NOFINISH),"未完成");
		
	}
	
	public ArrayList getQuoteStatus()
	{
		return(row.getFieldNames());
	}

	public String getQuoteStatusById(String id)
	{
		return(row.getString(id));
	}
}
