package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class CrmClientKey
{
	DBRow row;
	
	public static int FREQUENTLY_CLIENTS = 1;	//
	
	public CrmClientKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(CrmClientKey.FREQUENTLY_CLIENTS), "购买频繁顾客");
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
