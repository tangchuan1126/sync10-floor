package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class RepairOrderKey
{
	DBRow row;
	
	public static int READY = 1;	//备货中
	public static int INTRANSIT = 2;//正常运送
	public static int APPROVEING = 3;//审核中
	public static int FINISH = 4;//完成
	public static int PACKING = 5;//装箱中
	public static int NOFINISH = 7;//未完成
	public static int AlREADYRECEIVE = 8;	//已收货
	public static int PART_REPAIR = 9;//部分返修
	public static int FINISH_REPAIR = 10;//全部返修
	
	
	public RepairOrderKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(RepairOrderKey.READY), "备货中");
		row.add(String.valueOf(RepairOrderKey.PACKING), "<font color='green'>装货中</font>");
		row.add(String.valueOf(RepairOrderKey.INTRANSIT), "<font color='blue'>正常运输</font>");
		row.add(String.valueOf(RepairOrderKey.APPROVEING), "<font color='red'>审核中</font>");
		row.add(String.valueOf(RepairOrderKey.FINISH), "已入库");
		row.add(String.valueOf(RepairOrderKey.NOFINISH), "未完成");
		row.add(String.valueOf(RepairOrderKey.AlREADYRECEIVE),"到货未入库");
		row.add(String.valueOf(RepairOrderKey.PART_REPAIR), "部分返修");
		row.add(String.valueOf(RepairOrderKey.FINISH_REPAIR),"全部返修");
	}

	public ArrayList getRepairOrderStatus()
	{
		return(row.getFieldNames());
	}

	public String getRepairOrderStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
