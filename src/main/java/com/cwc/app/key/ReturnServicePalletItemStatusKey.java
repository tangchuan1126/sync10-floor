package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ReturnServicePalletItemStatusKey {
	
	DBRow row;
	public static int NA = 1;	//未处理
	public static int BER_DAMAGED = 2 ;
 
	public ReturnServicePalletItemStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnServicePalletItemStatusKey.NA), "NA");
		row.add(String.valueOf(ReturnServicePalletItemStatusKey.BER_DAMAGED), "BER/Damaged");
		
	}
	
	public ArrayList getReturnServicePalletItemStatusKeys()
	{
		return(row.getFieldNames());
	}

	public String getReturnServicePalletItemStatusKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getReturnServicePalletItemStatusKeyById(String id)
	{
		return(row.getString(id));
	}
}
