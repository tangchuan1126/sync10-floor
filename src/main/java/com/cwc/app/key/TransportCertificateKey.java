package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class TransportCertificateKey {

DBRow row;
	
	public DBRow getRow() {
		return row;
	}

	public static int NOCERTIFICATE= 1;			 
	public static int CERTIFICATE = 2;		 
	public static int FINISH = 3;		 
 
 
	public TransportCertificateKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(TransportCertificateKey.NOCERTIFICATE), "无需单证");
		row.add(String.valueOf(TransportCertificateKey.CERTIFICATE), "单证采集中");
		row.add(String.valueOf(TransportCertificateKey.FINISH), "单证采集完成");
	 
		 
	}

	public ArrayList<String> getStatus()
	{
		return((ArrayList<String>)row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
