package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class DeliveryOrderKey
{
	DBRow row;
	
	public static int READY = 1;	//准备中
	public static int INTRANSIT = 2;//运送中
	public static int FINISH = 3;//完成
	public static int APPROVEING = 4;//审核中
	public static int NOFINISH = 5;//未完成
	
	public DeliveryOrderKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(DeliveryOrderKey.READY), "<font color='#ff6600'>准备中</font>");
		row.add(String.valueOf(DeliveryOrderKey.INTRANSIT), "<font color='blue'>运输中<font>");
		row.add(String.valueOf(DeliveryOrderKey.FINISH), "<font color='green'>完成</font>");
		row.add(String.valueOf(DeliveryOrderKey.APPROVEING), "<font color='red'>审核中</font>");
		row.add(String.valueOf(DeliveryOrderKey.NOFINISH), "未完成");
	}

	public ArrayList getDeliveryOrderStatus()
	{
		return(row.getFieldNames());
	}

	public String getDeliveryOrderStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
