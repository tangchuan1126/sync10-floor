package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;
/**
 * @author geqingling
 * @version 创建时间：2014-5-17 下午5:36:53
 * return 功能中Status 类型key，对应数据库中存入的数字；
 */
 
public class ReturnOrderStatusKey {

	DBRow row;
	public static int RECEIVING = 1;
	public static int RECEIVED = 2;
	
	public ReturnOrderStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnOrderStatusKey.RECEIVING), "receiving");
		row.add(String.valueOf(ReturnOrderStatusKey.RECEIVED), "received");
	}

	public ArrayList getReturnOrderStatusKeys() {
		return (row.getFieldNames());
	}

	public String getReturnOrderStatusValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getReturnOrderStatusValue(String id) {
		return (row.getString(id));
	}

}
