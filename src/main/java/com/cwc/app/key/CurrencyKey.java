package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class CurrencyKey
{
	DBRow row;
	
	public CurrencyKey() 
	{
		row = new DBRow();
		row.add("USD","USD");
		row.add("AUD","AUD");
		row.add("EUR","EUR");
		row.add("CAD","CAD");
		row.add("CHF","CHF");
		row.add("CZK","CZK");
		row.add("GBP","GBP");
	}
	
	public ArrayList getCurrency()
	{
		return(row.getFieldNames());
	}

	public String getCurrencyById(String id)
	{
		return(row.getString(id));
	}
	
	public ArrayList CurrencyKeyList()
	{
		return row.getFieldNames();
	}
	
	public String getCurrencyNameById(String name)
	{
		return row.getString(name);
	}
}
