package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class LoadUnloadOccupancyStatusKey {
	
	DBRow dbRow;
	public static int BOOKING = 1;//占用
	public static int QUIT = 2;//放弃
	public static int RELEASE = 3;//释放
	
	public LoadUnloadOccupancyStatusKey()
	{
		dbRow = new DBRow();
		dbRow.add(String.valueOf(LoadUnloadOccupancyStatusKey.BOOKING), "占用");
		dbRow.add(String.valueOf(LoadUnloadOccupancyStatusKey.QUIT), "放弃");
		dbRow.add(String.valueOf(LoadUnloadOccupancyStatusKey.RELEASE), "释放");
	}
	
	public ArrayList getLoadUnloadOccupancyStatuss()
	{
		return(dbRow.getFieldNames());
	}

	public String getLoadUnloadOccupancyStatusName(String id)
	{
		return(dbRow.getString(id));
	}
	
	public String getLoadUnloadOccupancyStatusName(int id)
	{
		return(dbRow.getString(String.valueOf(id)));
	}
}
