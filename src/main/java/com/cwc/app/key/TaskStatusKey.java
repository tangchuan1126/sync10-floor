package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class TaskStatusKey {
	    DBRow row;
	    public static int CLOSE = 3;
	    public static int PENDING = 2;
	    public static int PROCESSING = 1;
	    public static int OPEN = 4;
	    public static int ASSIGNED = 5;
	    public static int ACCEPTED = 6;

	    public TaskStatusKey()
	    {
	        row = new DBRow();
	        row.add(String.valueOf(CLOSE), "CLOSE");
	        row.add(String.valueOf(PENDING), "PENDING");
	        row.add(String.valueOf(PROCESSING), "PROCESSING");
	        row.add(String.valueOf(OPEN), "OPEN");
	        row.add(String.valueOf(ASSIGNED), "ASSIGNED");
	        row.add(String.valueOf(ACCEPTED), "ACCEPTED");
	    }

	    public ArrayList<?> getStatus()
	    {
	        return row.getFieldNames();
	    }

	    public String getStatusById(int id)
	    {
	        return row.getString(String.valueOf(id));
	    }
         
	    public DBRow getRow()
	    {
	        return row;
	    }
	   


}
