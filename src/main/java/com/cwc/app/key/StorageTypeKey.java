package com.cwc.app.key;

import java.util.ArrayList;
import java.util.List;

import com.cwc.db.DBRow;

public class StorageTypeKey {
	
	DBRow dbRow;
	DBRow dbRowEn;
	
	public static int SELF = 1;// 自有仓库
	public static int TRANSFOR = 2;// 转运仓库
	public static int SUPPLIER = 3;// 供应商仓库
	public static int THIRD = 4;// 第三方仓库
	public static int RETAILER = 5;// shipTo仓库
	public static int REPAIR = 6;// 返修仓库
	public static int CUSTOMER = 7;//customer地址
	public static int CARRIER = 8;//carrier地址

	public StorageTypeKey() {
		
		dbRow = new DBRow();
		dbRow.add(String.valueOf(StorageTypeKey.SELF), "自有仓库");
		dbRow.add(String.valueOf(StorageTypeKey.TRANSFOR), "转运仓库");
		dbRow.add(String.valueOf(StorageTypeKey.SUPPLIER), "供应商仓库");
		dbRow.add(String.valueOf(StorageTypeKey.THIRD), "第三方仓库");
		dbRow.add(String.valueOf(StorageTypeKey.RETAILER), "零售商仓库");
		dbRow.add(String.valueOf(StorageTypeKey.REPAIR), "返修仓库");
		dbRow.add(String.valueOf(StorageTypeKey.CUSTOMER), "客户仓库");
		dbRow.add(String.valueOf(StorageTypeKey.CARRIER), "Carrier");
		dbRowEn = new DBRow();
		dbRowEn.add(String.valueOf(StorageTypeKey.SELF), "Self");
		dbRowEn.add(String.valueOf(StorageTypeKey.TRANSFOR), "Transfor");
		dbRowEn.add(String.valueOf(StorageTypeKey.SUPPLIER), "Supplier");
		dbRowEn.add(String.valueOf(StorageTypeKey.THIRD), "Third");
		dbRowEn.add(String.valueOf(StorageTypeKey.RETAILER), "Retailer");
		dbRowEn.add(String.valueOf(StorageTypeKey.REPAIR), "Repair");
		dbRowEn.add(String.valueOf(StorageTypeKey.CUSTOMER), "Customer");
		dbRowEn.add(String.valueOf(StorageTypeKey.CARRIER), "Carrier");
	}

	public ArrayList getStorageTypeKeys() {
		return (dbRow.getFieldNames());
	}

	public String getStorageTypeKeyName(String id) {
		return (dbRow.getString(id));
	}

	public String getStorageTypeKeyName(int id) {
		return (dbRow.getString(String.valueOf(id)));
	}

	public String getStorageTypeKeyNameEn(String id) {
		return (dbRowEn.getString(id));
	}

	public String getStorageTypeKeyNameEn(int id) {
		return (dbRowEn.getString(String.valueOf(id)));
	}
	
	/**
	 * 获取仓库类型[暂时]
	 * @return DBRow[]
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow[] getEnStorageTypeKey(){
		
		DBRow[] result = new DBRow[7];
		
		DBRow self = new DBRow()
		,transfor = new DBRow()
		,supplier = new DBRow()
		,third = new DBRow()
		,customer = new DBRow()
		,repair = new DBRow()
		,carrier = new DBRow();
		
		self.add("id",String.valueOf(StorageTypeKey.SELF));
		self.add("name","SELF");
		
		transfor.add("id",String.valueOf(StorageTypeKey.TRANSFOR));
		transfor.add("name","TRANSFOR");
		
		supplier.add("id",String.valueOf(StorageTypeKey.SUPPLIER));
		supplier.add("name", "SUPPLIER");
		
		third.add("id",String.valueOf(StorageTypeKey.THIRD));
		third.add("name","THIRD");
		
		customer.add("id",String.valueOf(StorageTypeKey.CUSTOMER));
		customer.add("name","CUSTOMER");
		
		repair.add("id",String.valueOf(StorageTypeKey.REPAIR));
		repair.add("name","REPAIR");
		
		carrier.add("id",String.valueOf(StorageTypeKey.CARRIER));
		carrier.add("name","CARRIER");
		
		result[0] = self;
		result[1] = transfor;
		result[2] = supplier;
		result[3] = third;
		result[4] = customer;
		result[5] = repair;
		result[6] = carrier;
		
		return result;
	}
}
