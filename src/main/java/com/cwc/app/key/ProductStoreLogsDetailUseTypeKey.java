package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProductStoreLogsDetailUseTypeKey
{
	DBRow row;
	
	public static int NotUsed = 1;
	public static int PartUsed = 2;
	public static int AllUsed = 3;
	

	public ProductStoreLogsDetailUseTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ProductStoreLogsDetailUseTypeKey.NotUsed),"未使用");
		row.add(String.valueOf(ProductStoreLogsDetailUseTypeKey.PartUsed),"部分使用");
		row.add(String.valueOf(ProductStoreLogsDetailUseTypeKey.AllUsed),"已用完");
		
	}

	public ArrayList getProductStoreLogsDetailOperationType()
	{
		return(row.getFieldNames());
	}

	public String getProductStoreLogsDetailOperationTypeById(String id)
	{
		return(row.getString(id));
	}
}
