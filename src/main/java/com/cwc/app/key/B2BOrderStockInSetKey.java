package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderStockInSetKey {

	// 升级前把stock_in_set 2改成5;
	
	DBRow row;
	public static int SHIPPINGFEE_NOTSET = 1;//无需运费
	public static int SHIPPINGFEE_SET = 2;	//需要运费
	//public static int SHIPPINGFEE_SETOVER = 3; //运费已经设置
	//public static int SHIPPINGFEE_APPLY = 4;//运费已申请
	public static int SHIPPINGFEE_TRANSFER_FINISH= 5;//运费转账完
	
	public B2BOrderStockInSetKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderStockInSetKey.SHIPPINGFEE_NOTSET), "无需运费");
		row.add(String.valueOf(B2BOrderStockInSetKey.SHIPPINGFEE_SET), "需要运费");
		row.add(String.valueOf(B2BOrderStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH), "运费完成");
	 
	}

	public ArrayList getB2BStockInSetKeys()
	{
		return(row.getFieldNames());
	}

	public String getB2BStockInSetKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
