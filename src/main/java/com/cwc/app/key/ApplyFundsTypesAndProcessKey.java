package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ApplyFundsTypesAndProcessKey {
	
	DBRow row;
	
	public static long APPLY_FUNDS_WAGE = 10011;					//资金申请的人员工资
	public static long APPLY_FUNDS_SOCIAL_SECURITY = 10012; 		//资金申请的社保
	public static long APPLY_FUNDS_LIFE_INCIDENTALS = 10016;		//资金申请的生活日杂
	public static long APPLY_FUNDS_OFFICE_COSTS = 10017; 			//资金申请的办公费用
	public static long APPLY_FUNDS_RENT = 10018;					//资金申请的房租
	public static long APPLY_FUNDS_COMMUNICATION = 10021; 			//资金申请的通讯费
	public static long APPLY_FUNDS_OTHER_FREIGHT = 10023;			//资金申请的其他运费
	public static long APPLY_FUNDS_TRAVEL_COSTS = 100005; 			//资金申请的差旅
	public static long APPLY_FUNDS_VEHICLE_COSTS = 100007;			//资金申请的车辆费用
	public static long APPLY_FUNDS_DECLARATION_DOLLARS = 100008; 	//资金申请的报关美金
	
	public static long APPLY_FUNDS_DECLARATION = 10025; 			//资金申请的报关
	public static long APPLY_FUNDS_CLEARANCE = 10026;				//资金申请的清关
	public static long APPLY_FUNDS_DRAWBACK = 10027;				//资金申请的退税
	public static long APPLY_FUNDS_INVOICED_AMOUNT = 100006; 		//资金申请的开票金额
	public static int PURCHASE_EXPECT_APPLY_FUNDS = 100009;			//采购定金申请
	public static int TRANSPORT_EXPECT_APPLY_FUNDS = 100001;		//转运单资金申请
	public static int TRANSPORT_STOCK_APPLY_FUNDS = 10015;			//转运单运费资金
	
	public static long APPLY_FUNDS_SAMPLE_PROCUREMENT = 10014; 		//资金申请的样品采购
	public static long APPLY_FUNDS_FIXED_ASSETS = 100003;			//资金申请的固定资产
	public static long APPLY_FUNDS_TEST_MARKETING_PROCUREMENT = 10013;	//资金申请货物测试销售期采购
	
	public ApplyFundsTypesAndProcessKey() {
		
		row = new DBRow();
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_WAGE), "35");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_SOCIAL_SECURITY), "36");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_LIFE_INCIDENTALS), "37");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_OFFICE_COSTS), "38");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_RENT), "39");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_COMMUNICATION), "40");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_OTHER_FREIGHT), "41");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_TRAVEL_COSTS), "42");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_VEHICLE_COSTS), "43");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_DECLARATION_DOLLARS), "44");
		
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_DECLARATION), "45");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_CLEARANCE), "46");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_DRAWBACK), "47");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_INVOICED_AMOUNT), "48");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.PURCHASE_EXPECT_APPLY_FUNDS), "25");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.TRANSPORT_EXPECT_APPLY_FUNDS), "26");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.TRANSPORT_STOCK_APPLY_FUNDS), "28");
		
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_SAMPLE_PROCUREMENT), "49");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_FIXED_ASSETS), "50");
		row.add(String.valueOf(ApplyFundsTypesAndProcessKey.APPLY_FUNDS_TEST_MARKETING_PROCUREMENT), "51");
	}

	public ArrayList<String> getAllTypesAndProcessKeys()
	{
		return((ArrayList<String>)row.getFieldNames());
	}

	public int getTypesAndProcessKeyById(int id)
	{
		return (Integer.parseInt(row.getString(String.valueOf(id))));
	}
	public int getTypesAndProcessKeyById(long id)
	{
		return (Integer.parseInt(row.getString(String.valueOf(id))));
	}
	public int getTypesAndProcessKeyById(String id)
	{
		return(Integer.parseInt(row.getString(id)));
	}
	
	public String getTypesAndProcessKey(String id){
		return row.getString(id);
	}
	
}
