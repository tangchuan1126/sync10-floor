package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WayBillOrderStatusKey {
DBRow row;
	
	public static int WAITPRINT = 0;	//待打印
	public static int PERINTED = 1;		//已打印
	public static int SHIPPED = 2;		//已发货
	public static int CANCEL = 3;		//取消
	public static int SPLIT = 4;        // 拆分中
	public static int REPLACETRACKING = 5;	//需换快递单
	
	public WayBillOrderStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WayBillOrderStatusKey.WAITPRINT), "待打印");
		row.add(String.valueOf(WayBillOrderStatusKey.PERINTED), "已打印");
		row.add(String.valueOf(WayBillOrderStatusKey.SHIPPED), "已发货");
		row.add(String.valueOf(WayBillOrderStatusKey.CANCEL), "取消");
		row.add(String.valueOf(WayBillOrderStatusKey.SPLIT), "拆分中");
		row.add(String.valueOf(WayBillOrderStatusKey.REPLACETRACKING), "需换快递单");
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
