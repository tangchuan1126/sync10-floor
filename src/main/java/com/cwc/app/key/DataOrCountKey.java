package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class DataOrCountKey {
	DBRow row;
	
	public static int DATA = 1;
	public static int COUNT = 2;
	
	public DataOrCountKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(DataOrCountKey.DATA), "DATA");
		row.add(String.valueOf(DataOrCountKey.COUNT), "COUNT");
	}
	
	public ArrayList getDataOrCountKeys()
	{
		return(row.getFieldNames());
	}

	public String getDataOrCountKeyByKey(String id)
	{
		return(row.getString(id));
	}
	
	public String getDataOrCountKeyByKey(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
}
