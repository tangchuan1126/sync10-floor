package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**
 * 退货商品的处理方式
 * @author Administrator
 *
 */
public class ReturnProductItemHandleResultTypeKey {

	DBRow row;
	
	public static int Direct_StockIn = 1; //直接入库
	public static int Destroy = 2 ; //销毁
	public static int StockInAfterTest = 3 ; //测试后入库
	public static int DestroyAfterTest = 4 ; //测试后销毁;
	public static int RepairAfterTest = 5 ; //测试后返修
	public ReturnProductItemHandleResultTypeKey() 
	{
	 
		row = new DBRow();
		row.add(String.valueOf(ReturnProductItemHandleResultTypeKey.Direct_StockIn), "直接入库");
		row.add(String.valueOf(ReturnProductItemHandleResultTypeKey.Destroy), "销毁");
		row.add(String.valueOf(ReturnProductItemHandleResultTypeKey.StockInAfterTest), "测试后入库");
		row.add(String.valueOf(ReturnProductItemHandleResultTypeKey.DestroyAfterTest), "测试后销毁");
		row.add(String.valueOf(ReturnProductItemHandleResultTypeKey.RepairAfterTest), "测试后返修");
 
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
