package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ReturnProductLogTypeKey {
	
	DBRow row;
	public static int CREATE = 1;
	public static int UPDATE = 2;
	public static int PICTURE = 3;
	public static int RECOGNITION = 4;
	public static int CHG_STORAGE = 5;
	public static int CHG_COUNT = 6;
	public static int RELATION_ORDER = 7;
	public static int CHECK = 8;

	public ReturnProductLogTypeKey()
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnProductLogTypeKey.CREATE), "创建");
		row.add(String.valueOf(ReturnProductLogTypeKey.UPDATE), "更新");
		row.add(String.valueOf(ReturnProductLogTypeKey.PICTURE), "图片采集");
		row.add(String.valueOf(ReturnProductLogTypeKey.RECOGNITION), "商品识别");
		row.add(String.valueOf(ReturnProductLogTypeKey.CHG_STORAGE), "修改仓库");
		row.add(String.valueOf(ReturnProductLogTypeKey.CHG_COUNT), "修改数量");
		row.add(String.valueOf(ReturnProductLogTypeKey.RELATION_ORDER), "关联单据");
		row.add(String.valueOf(ReturnProductLogTypeKey.CHECK), "商品测试");
	}
	
	public ArrayList getReturnProductLogKeys()
	{
		return row.getFieldNames();
	}

	public String getReturnProductLogTypeName(int id)
	{
		return row.getString(String.valueOf(id));
	}
	
	public String getReturnProductLogTypeName(String id)
	{
		return row.getString(id);
	}
}
