package com.cwc.app.key;

import com.cwc.db.DBRow;


public class LableTemplateContainerKey extends LableTemplateProductCommonKey
{
	public LableTemplateContainerKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(FIRST), "ilp");
		row.add(String.valueOf(SECOND), "blp");
		row.add(String.valueOf(THIRD), "clp");
	}
	
}
