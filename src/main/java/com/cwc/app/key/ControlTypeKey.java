package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ControlTypeKey
{
	DBRow row;
	public static int Left = 1;	//供应商
	public static int Top = 2 ; //资金申请
	public static int Android = 3 ; //预申请
	public static int APPLY_TRANSFER = 4 ; //转账申请
 
	public ControlTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ControlTypeKey.Left), "左侧列表");
		row.add(String.valueOf(ControlTypeKey.Top), "顶部列表");
		row.add(String.valueOf(ControlTypeKey.Android), "安卓列表");
	}
	
	public ArrayList getControlTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getControlTypeKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
