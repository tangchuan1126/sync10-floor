package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ImportStorageAddressErrorKey {
	
	DBRow row;
	public static String STORAGE_NAME = "STORAGE_NAME";//仓库名不能为空
	public static String STORAGE_TYPE = "STORAGE_TYPE";//仓库类型不正确
	public static String NATIVE = "NATIVE";//国家不存在
	
	public ImportStorageAddressErrorKey(){
		
		row = new DBRow();
		row.add(ImportStorageAddressErrorKey.STORAGE_NAME, "<font color=red>仓库名不能为空</font>");
		row.add(ImportStorageAddressErrorKey.STORAGE_TYPE,"<font color=red>仓库类型不正确，请核实类型再导入</font>");
		row.add(ImportStorageAddressErrorKey.NATIVE,"<font color=red>该国家不存在，请核实后再导入</font>");
	}
	
	public ArrayList getStorageAddressErrorMessage()
	{
		return(row.getFieldNames());
	}

	public String getStorageAddressErrorMessageById(String id)
	{
		return(row.getString(id));
	}
}
