package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WmsDeliveryStatusIntStrKey {
	DBRow row;
	
	public static int IMPORTED = 1;
	public static int OPEN = 2;
	public static int RECEIVED = 3;
	public static int CLOSED = 4;
	
	public WmsDeliveryStatusIntStrKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WmsDeliveryStatusIntStrKey.IMPORTED), "Imported");
		row.add(String.valueOf(WmsDeliveryStatusIntStrKey.OPEN), "Open");
		row.add(String.valueOf(WmsDeliveryStatusIntStrKey.RECEIVED), "Received");
		row.add(String.valueOf(WmsDeliveryStatusIntStrKey.CLOSED), "Closed");
	}
	
	public ArrayList getWmsDeliveryStatusIntStrKeys()
	{
		return(row.getFieldNames());
	}

	public String getWmsDeliveryStatusIntStrKeyByKey(String id)
	{
		return(row.getString(id));
	}
	
	public String getWmsDeliveryStatusIntStrKeyByKey(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public int getWmsStatusKeyByValue(String value)
	{
		int a = 0;
		ArrayList<String> keys = getWmsDeliveryStatusIntStrKeys();
		for (int i = 0; i < keys.size(); i++)
		{
			int b = Integer.parseInt(keys.get(i));
			if(getWmsDeliveryStatusIntStrKeyByKey(b).equals(value))
			{
				a = b;
			}
		}
		return a;
	}
	
}
