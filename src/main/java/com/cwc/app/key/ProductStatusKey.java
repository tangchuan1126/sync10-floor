package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class ProductStatusKey
{
	DBRow row;
	
	public static int UNKNOWN = 0;
	public static int STORE_OUT = 1;
	public static int IN_STORE = 2;
	public static int NO_STORE = 3;
	
	public ProductStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ProductStatusKey.UNKNOWN),"<font color=black>未知</font>");
		row.add(String.valueOf(ProductStatusKey.STORE_OUT),"<font color=red>缺货</font>");
		row.add(String.valueOf(ProductStatusKey.IN_STORE),"<font color=green>有货</font>");
		row.add(String.valueOf(ProductStatusKey.NO_STORE),"<font color=green>无库存</font>");
	}

	public ArrayList getOrderHandle()
	{
		return(row.getFieldNames());
	}

	public String getOrderHandleById(String id)
	{
		return(row.getString(id));
	}
}
