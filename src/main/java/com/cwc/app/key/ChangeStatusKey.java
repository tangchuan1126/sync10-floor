package com.cwc.app.key;


import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ChangeStatusKey
{
	DBRow row;
	public static int Readying = 1;
	public static int Allocate = 2;
	public static int Picking = 3;
	public static int Finish = 4;
	
	public ChangeStatusKey()
	{
		row = new DBRow();
		
		row.add(String.valueOf(ChangeStatusKey.Readying),"准备中");
		row.add(String.valueOf(ChangeStatusKey.Allocate),"已保留");
		row.add(String.valueOf(ChangeStatusKey.Picking),"拣货中");
		row.add(String.valueOf(ChangeStatusKey.Finish),"已完成");
	}
	
	public ArrayList getChangeStatusKey()
	{
		return(row.getFieldNames());
	}

	public String getChangeStatusKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getChangeStatusKeyById(String id)
	{
		return(row.getString(id));
	}
}
