package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class RepairStockInSetKey {

	// 升级前把stock_in_set 2改成5;
	
	DBRow row;
	public static int SHIPPINGFEE_NOTSET = 1;//无需运费
	public static int SHIPPINGFEE_SET = 2;	//需要运费
	//public static int SHIPPINGFEE_SETOVER = 3; //运费已经设置
	//public static int SHIPPINGFEE_APPLY = 4;//运费已申请
	public static int SHIPPINGFEE_TRANSFER_FINISH= 5;//运费转账完
	
	public RepairStockInSetKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(RepairStockInSetKey.SHIPPINGFEE_NOTSET), "无需运费");
		row.add(String.valueOf(RepairStockInSetKey.SHIPPINGFEE_SET), "需要运费");
		row.add(String.valueOf(RepairStockInSetKey.SHIPPINGFEE_TRANSFER_FINISH), "运费完成");
	 
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
