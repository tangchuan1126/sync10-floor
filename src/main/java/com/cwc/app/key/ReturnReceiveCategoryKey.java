package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;
/**
 * @author geqingling
 * @version 创建时间：2014-5-16 下午4:27:35
 * return 功能中Receive Category 类型key，对应数据库中存入的数字；1：表示Retail；2：表示End User
 */
 
public class ReturnReceiveCategoryKey {

	DBRow row;
	public static int RETAIL = 1;
	public static int END_USER = 2;
	
	public ReturnReceiveCategoryKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnReceiveCategoryKey.RETAIL), "Retail");
		row.add(String.valueOf(ReturnReceiveCategoryKey.END_USER), "End User");
	}

	public ArrayList getReturnReceiveCategoryKeys() {
		return (row.getFieldNames());
	}

	public String getReturnReceiveCategoryKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getReturnReceiveCategoryKeyValue(String id) {
		return (row.getString(id));
	}

}
