package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class TransportTagKey {

	DBRow row;
	
	public DBRow getRow() {
		return row;
	}

	public static int NOTAG = 1;			 
	public static int TAG = 2;		 
	public static int FINISH = 3;
	public static int FINISH_PURCHASE = 4;
 
	public TransportTagKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(TransportTagKey.NOTAG), "无需打签");
		row.add(String.valueOf(TransportTagKey.TAG), "需要打签");
		row.add(String.valueOf(TransportTagKey.FINISH), "打签完成");
		row.add(String.valueOf(TransportTagKey.FINISH_PURCHASE), "采购单内部标签完成");
		 
	}

	public ArrayList<String> getTransportTags()
	{
		return((ArrayList<String>)row.getFieldNames());
	}

	public String getTransportTagById(String id)
	{
		return(row.getString(id));
	}
	
	public String getTransportTagById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
