package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ApplyTypeKey {

	DBRow row;
	
	public static String PURCHASE = "100001";	//采购单
	public static String FIXED_ASSETS="2";

	public ApplyTypeKey() 
	{
		row = new DBRow();
		row.add(ApplyTypeKey.PURCHASE, "采购单");
		row.add(ApplyTypeKey.FIXED_ASSETS, "固定资产");
	}

	public ArrayList getApplyTypeStatus()
	{
		return(row.getFieldNames());
	}

	public String getApplyTypeStatusById(String status)
	{
		return(row.getString(status));
	}
	
	public static void main(String[] args) 
	{
		ApplyTypeKey key=new ApplyTypeKey();
	}
}
