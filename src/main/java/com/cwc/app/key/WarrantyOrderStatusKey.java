package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class WarrantyOrderStatusKey
{
	DBRow row;
	
	public static int WAITING = 0;	//paypal
	public static int PAY = 1;//信用卡
	public static int Free = 2;//汇款
	
	
	
	public WarrantyOrderStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WarrantyOrderStatusKey.WAITING), "待处理");
		row.add(String.valueOf(WarrantyOrderStatusKey.PAY), "已支付");
		row.add(String.valueOf(WarrantyOrderStatusKey.Free), "免费");
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
