package com.cwc.app.key;

import java.util.List;

import com.cwc.db.DBRow;

public class DisplayOrNotKey {
	
	DBRow row;
	public static int YES = 1;
	public static int NO = 2;
	
	public DisplayOrNotKey()
	{
		row = new DBRow();
		row.add(String.valueOf(DisplayOrNotKey.YES), "显示");
		row.add(String.valueOf(DisplayOrNotKey.NO), "不显示");
	}
	
	public List getDisplayOrNotKeys()
	{
		return row.getFieldNames();
	}
	
	public String getDisplayOrNotKeyName(int id)
	{
		return row.getString(String.valueOf(id));
	}

	public String getDisplayOrNotKeyName(String id)
	{
		return row.getString(id);
	}
}
