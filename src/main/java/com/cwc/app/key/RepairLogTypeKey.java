package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class RepairLogTypeKey {
	
	DBRow row;
	
	public static int Create = 1;	
	public static int Financial = 2;
	public static int Update = 3;
	public static int Goods = 4;
	public static int Freight = 5;
	public static int ImportCustoms = 6;
	public static int ExportCustoms = 7;
	public static int Label = 8;
	public static int Document = 9;
	public static int ProductShot = 10;
	public static int QualityControl = 11;
	public static int ThirdLabel = 12;
	public static int STORAGEPERSON = 13;
	
	public RepairLogTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(RepairLogTypeKey.Create),"创建记录");
		row.add(String.valueOf(RepairLogTypeKey.Financial),"财务记录");
		row.add(String.valueOf(RepairLogTypeKey.Update),"修改记录");
		row.add(String.valueOf(RepairLogTypeKey.Goods),"货物状态");
		row.add(String.valueOf(RepairLogTypeKey.Freight),"运费流程");
		row.add(String.valueOf(RepairLogTypeKey.ImportCustoms),"进口清关");
		row.add(String.valueOf(RepairLogTypeKey.ExportCustoms),"出口报关");
		row.add(String.valueOf(RepairLogTypeKey.Label),"制签流程");
		row.add(String.valueOf(RepairLogTypeKey.Document),"单证流程");
		row.add(String.valueOf(RepairLogTypeKey.ProductShot),"实物图片流程");
		row.add(String.valueOf(RepairLogTypeKey.QualityControl),"质检流程");
		row.add(String.valueOf(RepairLogTypeKey.ThirdLabel),"第三方标签");
		row.add(String.valueOf(RepairLogTypeKey.STORAGEPERSON),"到货派送");
	}
	
	public ArrayList RepairLogType()
	{
		return(row.getFieldNames());
	}

	public String getRepairLogTypeById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
