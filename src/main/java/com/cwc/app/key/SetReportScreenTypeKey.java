package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**
 * 大屏幕的分类
 * @author win7zr
 *
 */
public class SetReportScreenTypeKey {

	DBRow row;
	public static int IncomingToWindow = 1;
	public static int LoadReceive = 2;
	public static int NeedAssign = 3;
	public static int RemainJobs = 4;
	public static int UnderProcessing = 5;
	public static int CLOSETODAY = 6;
	 
	
	public SetReportScreenTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(SetReportScreenTypeKey.IncomingToWindow), "Incoming To Window");
		row.add(String.valueOf(SetReportScreenTypeKey.LoadReceive), "Load Receive");
		row.add(String.valueOf(SetReportScreenTypeKey.NeedAssign), "Need Assign");
		row.add(String.valueOf(SetReportScreenTypeKey.RemainJobs), "Remain Jobs");
		row.add(String.valueOf(SetReportScreenTypeKey.UnderProcessing), "Under Processing");
		row.add(String.valueOf(SetReportScreenTypeKey.CLOSETODAY), "Close Today");

	}

	public ArrayList getAllKey() {
		return (row.getFieldNames());
	}

	public String getKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

}
