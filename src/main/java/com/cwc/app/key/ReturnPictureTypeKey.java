package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ReturnPictureTypeKey
{

	DBRow row;
	
	public static int Postal_Package = 0;	//邮递包装
	public static int Product_Picture = 1 ; //商品图片
	
	public ReturnPictureTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnPictureTypeKey.Postal_Package), "邮递包装");
		row.add(String.valueOf(ReturnPictureTypeKey.Product_Picture), "商品图片");
	}

	public ArrayList getStatus()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
