package com.cwc.app.key;

import com.cwc.db.DBRow;
 /**
  * load consolidate  LoadConsolidateReasonKey
  * @author win7zr
  *
  */
public class LoadConsolidateReasonKey {

	DBRow row;

	public static int Low = 1;
	public static int Trailer = 2;
	public static int PreLoad = 3;
	public static int Wrong = 4;
 
	 

	public LoadConsolidateReasonKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(LoadConsolidateReasonKey.Low),"Low cube trailer");
		row.add(String.valueOf(LoadConsolidateReasonKey.Trailer),"Trailer damaged");
		row.add(String.valueOf(LoadConsolidateReasonKey.PreLoad),"Pre-load freight inside");
		row.add(String.valueOf(LoadConsolidateReasonKey.Wrong),"Wrong configuration");
 	}
	
	public String getValueOfKey(int key){
		return row.getString(String.valueOf(key));
	}
 

}
