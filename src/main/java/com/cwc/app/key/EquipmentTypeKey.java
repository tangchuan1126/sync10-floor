package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class EquipmentTypeKey
{
	DBRow row;
	DBRow englishRow;
	public static int Tractor = 1;	//车头
	public static int Container = 2 ; //车尾
 
	public EquipmentTypeKey() 
	{
		row = new DBRow();
		englishRow = new DBRow();
		
		row.add(String.valueOf(EquipmentTypeKey.Tractor), "车头");
		row.add(String.valueOf(EquipmentTypeKey.Container), "车尾");
		
		englishRow.add(String.valueOf(EquipmentTypeKey.Tractor), "Tractor");
		englishRow.add(String.valueOf(EquipmentTypeKey.Container), "Trailer");
	}
	
	public ArrayList getEquipmentTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getEquipmentTypeValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public String getEnlishEquipmentTypeValue(int id)
	{
		return(englishRow.getString(String.valueOf(id)));
	}
}
