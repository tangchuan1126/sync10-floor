package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class FileWithClassKey {
DBRow row;
	
	public static int PACKING = 1;//装箱单
	public static int OUTERPACKING = 2;//包装照片
	public static int PRODUCTPHOTO = 3;//商品照片
	public static int TRANSPORTPHOT = 4;//转运照片
	public FileWithClassKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(FileWithClassKey.PACKING), "装箱单");
		row.add(String.valueOf(FileWithClassKey.OUTERPACKING), "包装照片");
		row.add(String.valueOf(FileWithClassKey.PRODUCTPHOTO), "商品照片");
		row.add(String.valueOf(FileWithClassKey.TRANSPORTPHOT), "转运照片");
	}
	
	public ArrayList getFileWithType()
	{
		return(row.getFieldNames());
	}

	public String getFileWithTypeId(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
