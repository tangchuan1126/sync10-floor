package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class MoreLessOrEqualKey {
	DBRow row;
	
	public static int MORE = 1;
	public static int LESS = 2;
	public static int EQUAL = 3;
	
	public MoreLessOrEqualKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(MoreLessOrEqualKey.MORE), "MORE");
		row.add(String.valueOf(MoreLessOrEqualKey.LESS), "LESS");
		row.add(String.valueOf(MoreLessOrEqualKey.EQUAL), "EQUAL");
	}
	
	public ArrayList getMoreLessOrEqualKeys()
	{
		return(row.getFieldNames());
	}

	public String getMoreLessOrEqualKeyByKey(String id)
	{
		return(row.getString(id));
	}
	
}
