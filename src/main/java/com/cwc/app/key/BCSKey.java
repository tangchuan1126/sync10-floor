package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.app.exception.android.UpdatePalletTypeFailedException;
import com.cwc.app.exception.checkin.CheckInAllTasksClosedException;
import com.cwc.app.exception.checkin.CheckInMustNotHaveTasksException;
import com.cwc.app.exception.checkin.CheckInTaskCanntDeleteException;
import com.cwc.app.exception.checkin.CheckInTaskHaveDeletedException;
import com.cwc.app.exception.checkin.CheckInTaskRepeatToEntryException;
import com.cwc.app.exception.checkin.CheckInTaskRepeatToOthersException;
import com.cwc.app.exception.checkin.EquipmentHasDecidedPickedException;
import com.cwc.app.exception.checkin.EquipmentHasLoadedOrdersException;
import com.cwc.app.exception.checkin.EquipmentOccupyResourcesException;
import com.cwc.db.DBRow;


public class BCSKey
{
	DBRow row;
	
	public static int FAIL = 0;		//操作失败
	public static int SUCCESS = 1;	//操作成功
	
	public static int DATASIZEINCORRECTLY = 0;//数据大小有误
	public static int DATATYPEINCRRECTLY = 1;//数据格式错误
	public static int SYSTEMERROR = 2;//系统内部错误
	public static int LOGINERROR = 3;//登录失败,账号密码不匹配
	public static int NOPURCHASE = 4;//无此采购单，采购单有误
	public static int NOPRODUCTINPURCHASE = 5;//采购单内无此商品
	public static int ORDERHANDLEERROR = 6;//订单状态并非已打印或待发货
	public static int OPERATIONNOTPERMIT = 7;//无权限操作
	public static int ORDERNOTFOUND = 8;//无法找到订单
	public static int CANNOTINBONDSCAN = 9;//采购单无法入库
	public static int NOTEXITSPRODUCT = 10;//系统内无此商品 
	public static int NETWORKEXCEPTION = 11;//网络传输错误 
	public static int NOEXITSDELIVERYORDER = 12;//交货单不存在 
	public static int DELIVERYORDERCANNOTINWARE = 13;//交货单不可入库 
	public static int HASSUBMIT = 14;//交货单已经入库
	public static int NOEXITSTRANSPORTORDER = 15;//转运单不存在
	public static int TRANSPORTORDERCANNOTINWARE = 16;//转运单不可入库 
	public static int TRANSPORTCANNOTSEND = 17;//转运单不可发货
	public static int NOEXITSDAMAGEDREPAIR = 18;//返修单不存在
	public static int DAMAGEDREPAIRCANNOTSEND = 19;//返修单不可出库
	public static int SerialNumberRepeat = 20;	//序列号重复
	public static int SerialNumberAlreadyInStore = 21;	//序列号已入库
	public static int PickUpCountMore = 22;				//拣货数量多于待拣货数
	public static int CanNotPut = 23;				//无法放货
	public static int CanNotPutOver = 24;			//放货无法完成
	//商品基础数据
	public static int ProductNameIsExist = 25; //商品名已经存在
	public static int ProductCodeIsExist = 26; //商品条码已经存在
	public static int ProductUnionCannotDoChild = 27; //已经设置了的组合套装不能做子商品
	public static int WithProductUnionNotRepeat = 28 ;//同一个组合套装下，商品不能重复
	public static int ProductUnionNoCreateProduct = 29;  //已经作为组合的商品，不能变成组合，在其下创建商品
	public static int ProductInUnionNotDel = 30; //商品有组合关系，不能删除
	public static int ProductHasRelationStorage = 31; //商品已建跟库存关系，不能删除
	public static int DELIVERYORDERSTATUSNOTREADY = 32;//交货单状态非ready
	public static int LPNotFound = 33 ;	//lp 没有找到
	public static int ProductNotFound = 34 ; // productNotFound ;
	public static int ProductPictureDeleteFailed = 35 ; 	// product pictrue delete failed ;
	public static int SerchLoadingNotFoundException = 36 ;	// LoadingNotFoundException 
	public static int TitleNotFoundException = 37 ;	// TitleNotFoundException 
	public static int TempContainerCreateFailedException = 38 ;	// TempContainerCreateFailedException 
	public static int HasWaitApproveAreaException = 39;		//该区域有待审核项目
	public static int CheckInNotFound = 40 ;				//CHECKIN主单据没有查询到
	public static int AppVersionException = 41 ;			//android 版本的更新问题
	public static int AndroidPrintServerNameExitsException = 42 ; // android 打印服务器重名
	public static int AndroidPrintServerUnEable = 43 ; // android 打印服务器不可用
	public static int NoPermiessionEntryIdException = 44 ; // 没有当前操作当前权限
	public static int PrintServerNoPrintException = 45 ; // printServer 没有设置关联的打印机
	public static int NoRecordsException = 46 ; // printServer 没有设置关联的打印机
	public static int LoadIsCloseException = 47 ; // LoadIsCloseException 没有设置关联的打印机
	public static int LoadNumberNotExistException = 48 ; // 根据一个LoadNumber去查询是否在一个Entry的子单据中
	public static int DataFormateException = 49 ; // Data Formate Exception
	public static int DockCheckInFirstException = 50 ; // dock check in first ;
	public static int SumsangPalletNotFound = 51 ; // dock check in first ;
	public static int ContainerNotFoundException = 52 ; // dock check in first ;
	public static int FileNotFoundException = 53 ;	    //FileNotFoundExcepiton
	public static int CheckInEntryIsLeftException = 54 ;	    //CheckInEntryIsLeftException 
	public static int DoorHasUsedException = 55 ;	  			//Door has 	RESERVERED,OCUPIED
	public static int SpotHasUsedException = 56 ;	  			//Spot has 	RESERVERED,OCUPIED
	public static int CheckTaskProcessingChangeTaskToDockException = 57 ;	// Move Task To Door.
	public static int EquipmentNotFindException = 58 	; 	//当前的设备没有找到
	public static int DeleteFileException = 59 	; 	//当前的设备没有找到
	public static int AddLoadBarUseFoundException =60 ;	//添加Load的失败
	public static int EquipmentHadOutException  = 61 ;	//equipment is left ;
	public static int DeletePalletNoFailedException = 62 ; 	//delete scan palletno error
	public static int EntryTaskHasFinishException = 63 ;	 // EntryTaskHasFinishException cant assign
	public static int EquipmentHasDecidedPickedException = 64;
	public static int EquipmentOccupyResourcesException = 65;
	public static int ResourceHasUsedException = 66;
	public static int DoorNotFindException = 67;
	public static int SpotNotFindException = 68;
	public static int EquipmentInYardException = 69;
	public static int CheckInTaskCanntDeleteException = 70;
	public static int CheckInTaskRepeatToEntryException = 71;
	public static int CheckInTaskRepeatToOthersException = 72;
	public static int CheckInMustHasTasksException = 73;
	public static int CheckInAllTasksClosedException = 74;
	public static int CheckInMustNotHaveTasksException = 75;
	public static int CheckInTaskHaveDeletedException = 76;
	public static int EquipmentHasLoadedOrdersException = 77;
	public static int CheckinTaskNotFoundException = 78;
	public static int EquipmentSameToEntryException = 79;
	public static int UpdatePalletTypeFailedException = 80 ;
	public static int TaskNoAssignWarehouseSupervisorException = 81 ;
	public static int UserIsOnLineCantLoginException = 82 ;
	public static int AccountNotPermitLoginException = 83 ;		//无登陆权限
	public static int OperationNotPermitException = 84 ; 		//no operation task  
	public static int AccountNullpointException = 85 ; 	//传递的account 为空	  
	public static int NoUserByaccountException = 86 ; 	//通过account没查到user信息
	public static int GetAdidException = 87 ; 		 //未获取到adid
	public static int PackagingTypeExist = 88;       //Packaging Type Exist
	public static int RFIDExist = 89;       //RFID Exist
	public static int ReceiveNormalException = 90;       //收货微服务通用异常
	public static int ReceiveNeedConfirmException = 91;       //收货微服务需要用户确认的异常
	public static int ReceiveSpecialException = 92;       //收货微服务特殊标红异常
	
	public BCSKey() 
	{
		row = new DBRow();
	}

	public ArrayList getOrderRateStatus()
	{
		return(row.getFieldNames());
	}

	public String getOrderRateStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
