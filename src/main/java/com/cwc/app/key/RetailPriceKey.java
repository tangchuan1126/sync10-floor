package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**
 * 零售报价导入错误
 * @author Administrator
 *
 */
public class RetailPriceKey
{
	DBRow row;
	
	public static int WAIT = 0;
	public static int PASS = 1;
	public static int REJECT = 2;
	
	public RetailPriceKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(RetailPriceKey.WAIT),"待审核");
		row.add(String.valueOf(RetailPriceKey.PASS),"<span style='color:#009900'>审核通过</span>");
		row.add(String.valueOf(RetailPriceKey.REJECT),"<span style='color:red'>驳回</span>");
	}

	public ArrayList getKeys()
	{
		return(row.getFieldNames());
	}

	public String getRetailPriceById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
