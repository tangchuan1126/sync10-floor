package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class CountOperationKey
{
	DBRow row;
	public static int Increase = 1;			//库存值增加
	public static int Decrease = 2;			//库存值减少
 
	public CountOperationKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(Increase), "库存值增加");
		row.add(String.valueOf(Decrease), "库存值减少");
	}
	
	public String getCountOperationKey(int id){
		return row.getString(String.valueOf(id));
	}
}
