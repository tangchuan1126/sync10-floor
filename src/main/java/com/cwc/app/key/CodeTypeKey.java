package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class CodeTypeKey
{
	DBRow row;
	public static int Main = 1;	//主条码   
	public static int UPC = 2; 
	public static int Amazon = 3; 
	public static int Old = 4; 
	public static int Retail = 5; 
	
	
	public CodeTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(CodeTypeKey.Main),"Main Code");
		row.add(String.valueOf(CodeTypeKey.UPC),"UPC Code");
		row.add(String.valueOf(CodeTypeKey.Amazon),"Amazon Code");
		row.add(String.valueOf(CodeTypeKey.Old),"Former");
		row.add(String.valueOf(CodeTypeKey.Retail),"Retailer Code");
		
	}

	public ArrayList getCodeType()
	{
		return(row.getFieldNames());
	}

	public String getCodeTypeKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
