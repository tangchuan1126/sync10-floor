package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WayBillResultKey {
DBRow row;
	
	public static int HEAVYHAIR = 1;	//重发
	public static int REFUND = 2;		//退款
	
	public WayBillResultKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WayBillResultKey.HEAVYHAIR), "重发");
		row.add(String.valueOf(WayBillResultKey.REFUND), "已打印");
	}

	public ArrayList getWayBillResultKey()
	{
		return(row.getFieldNames());
	}

	public String getWayBillResultKey(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
