package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class B2BOrderClearanceKey {
	DBRow row;
	
	public static int NOCLEARANCE = 1;	//无清关
	public static int CLEARANCE = 2;//清关
	public static int CLEARANCEING = 3;//清关中
	public static int FINISH = 4;//清关完成
	public static int CHECKCARGO=5; // 查货中;
	public B2BOrderClearanceKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderClearanceKey.NOCLEARANCE), "无需清关");
		row.add(String.valueOf(B2BOrderClearanceKey.CLEARANCE), "需要清关");
		row.add(String.valueOf(B2BOrderClearanceKey.CLEARANCEING), "清关中");
		row.add(String.valueOf(B2BOrderClearanceKey.CHECKCARGO), "查货中");
		row.add(String.valueOf(B2BOrderClearanceKey.FINISH), "清关完成");
	}
	
	public ArrayList<String> getB2BOrderClearanceKeys()
	{
		return(row.getFieldNames());
	}

	public String getB2BOrderClearanceKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
