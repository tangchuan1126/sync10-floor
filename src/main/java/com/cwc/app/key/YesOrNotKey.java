package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class YesOrNotKey {
	
	DBRow dbRow;
	public static int YES = 1;
	public static int NO = 2;
	
	public YesOrNotKey()
	{
		dbRow = new DBRow();
		dbRow.add(String.valueOf(YesOrNotKey.YES), "Yes");
		dbRow.add(String.valueOf(YesOrNotKey.NO), "No");
	}
	
	public ArrayList getYesOrNotKeys()
	{
		return(dbRow.getFieldNames());
	}

	public String getYesOrNotKeyName(String id)
	{
		return(dbRow.getString(id));
	}
	
	public String getYesOrNotKeyName(int id)
	{
		return(dbRow.getString(String.valueOf(id)));
	}
}
