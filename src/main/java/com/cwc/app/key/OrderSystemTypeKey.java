package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class OrderSystemTypeKey
{
	DBRow row;
	public static int SYNC = 1 ; 
	public static int WMS = 2; 
 
	public OrderSystemTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(OrderSystemTypeKey.SYNC), "SYNC");
		row.add(String.valueOf(OrderSystemTypeKey.WMS), "WMS");
	}
	
	public ArrayList getTypeKeys()
	{
		return(row.getFieldNames());
	}

	public String getTypeValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
}
