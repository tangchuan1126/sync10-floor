package com.cwc.app.key;

import java.util.ArrayList;
import java.util.List;

import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;

public class WeightUOMKey {

	DBRow row;

	public static final int KG = 1;
	public static final int LBS = 2;

	public WeightUOMKey() {
		row = new DBRow();
		row.add(String.valueOf(WeightUOMKey.LBS), "LBS");
		row.add(String.valueOf(WeightUOMKey.KG), "Kg");
	}
	
	public DBRow[] getKey() {
		
		List<?> list = this.getWeightUOMKeys();
		
		DBRow[] rowArray = new DBRow[list.size()];
		
		for(int i = 0;i<list.size();i++){
			
			DBRow dbrow = new DBRow();
			dbrow.put("id", Integer.valueOf(list.get(i).toString()));
			dbrow.put("name", this.getWeightUOMKey(Integer.valueOf(list.get(i).toString())));
			
			rowArray[i] = dbrow;
		}
		
		return rowArray;
	}

	public ArrayList<?> getWeightUOMKeys() {
		return (row.getFieldNames());
	}

	public String getWeightUOMKey(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public int getWeightUOMKeyByValue(String value) {
		
		int reInt = 0;
		if (!StrUtil.isBlank(value)) {
			
			ArrayList<?> names = this.getWeightUOMKeys();
			for (Object object : names) {
				int key = Integer.parseInt(String.valueOf(object));
				String values = this.getWeightUOMKey(key);
				if (value.toUpperCase().equals(values.toUpperCase())) {
					reInt = key;
					break;
				}
			}
		}
		return reInt;
	}
}
