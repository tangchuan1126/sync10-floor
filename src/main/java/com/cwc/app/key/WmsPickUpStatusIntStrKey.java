package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WmsPickUpStatusIntStrKey {
	DBRow row;
	
	public static int IMPORTED = 1;
	public static int OPEN = 2;
	public static int PICKING = 3;
	public static int PICKED = 4;
	public static int SHIPPED = 5;
	public static int CLOSED = 6;
	
	public WmsPickUpStatusIntStrKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WmsPickUpStatusIntStrKey.IMPORTED), "Imported");
		row.add(String.valueOf(WmsPickUpStatusIntStrKey.OPEN), "Open");
		row.add(String.valueOf(WmsPickUpStatusIntStrKey.PICKING), "Picking");
		row.add(String.valueOf(WmsPickUpStatusIntStrKey.PICKED), "Picked");
		row.add(String.valueOf(WmsPickUpStatusIntStrKey.SHIPPED), "Shipped");
		row.add(String.valueOf(WmsPickUpStatusIntStrKey.CLOSED), "Closed");
	}
	
	public ArrayList getWmsPickUpStatusIntStrKeys()
	{
		return(row.getFieldNames());
	}

	public String getWmsPickUpStatusIntStrKeyByKey(String id)
	{
		return(row.getString(id));
	}
	
	public String getWmsPickUpStatusIntStrKeyByKey(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
	
	public int getWmsStatusKeyByValue(String value)
	{
		int a = 0;
		ArrayList<String> keys = getWmsPickUpStatusIntStrKeys();
		for (int i = 0; i < keys.size(); i++)
		{
			int b = Integer.parseInt(keys.get(i));
			if(getWmsPickUpStatusIntStrKeyByKey(b).equals(value))
			{
				a = b;
			}
		}
		return a;
	}
	
}
