package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class OrderRateKey
{
	DBRow row;
	
	public static int BAD = 1;
	public static int NORMAL = 2;
	public static int EBAY_COMPLAIN = 3;
	public static int PAYPAL_COMPLAIN = 4;
	
	public OrderRateKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(OrderRateKey.BAD),"差评");
		row.add(String.valueOf(OrderRateKey.NORMAL),"中评");
		row.add(String.valueOf(OrderRateKey.EBAY_COMPLAIN),"EBay投诉");
		row.add(String.valueOf(OrderRateKey.PAYPAL_COMPLAIN),"PayPal投诉");
	}

	public ArrayList getOrderRateStatus()
	{
		return(row.getFieldNames());
	}

	public String getOrderRateStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
