package com.cwc.app.key;

import java.util.ArrayList;
import com.cwc.db.DBRow;

public class PlateLevelProductInventaryExceptionKey {
	
	DBRow dbRow;
	public static int SUCCESS = 1;
	public static int PLATE_EXIST = 2;
	
	public PlateLevelProductInventaryExceptionKey()
	{
		dbRow = new DBRow();
		dbRow.add(String.valueOf(PlateLevelProductInventaryExceptionKey.SUCCESS), "Success.");
		dbRow.add(String.valueOf(PlateLevelProductInventaryExceptionKey.PLATE_EXIST), "Plate is exist.");
	}
	
	public ArrayList getPlateLevelProductInventaryExceptionKeys()
	{
		return(dbRow.getFieldNames());
	}

	public String getPlateLevelProductInventaryExceptionKeyName(String id)
	{
		return(dbRow.getString(id));
	}
	
	public String getPlateLevelProductInventaryExceptionKeyName(int id)
	{
		return(dbRow.getString(String.valueOf(id)));
	}
}
