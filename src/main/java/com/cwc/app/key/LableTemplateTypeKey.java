package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class LableTemplateTypeKey
{
	DBRow row;
	
	public static int PRODUCTLABEL= 1;	//商品标签
	public static int CONTAINERTLABEL= 2;//容器标签

	
	public LableTemplateTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(LableTemplateTypeKey.PRODUCTLABEL), "商品标签");
		row.add(String.valueOf(LableTemplateTypeKey.CONTAINERTLABEL), "容器标签");
	}

	public ArrayList getLableTemplates()
	{
		return(row.getFieldNames());
	}

	public String getLableTemplateNameById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
