package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class GoodsApplyStatusKey {
	
	DBRow row;
	
	public static String norequired_apply="1";//无需申请
	public static String no_apply="2";//未申请
	public static String already_apply="3";//已申请
	
	public GoodsApplyStatusKey(){
		
		row = new DBRow();
		row.add(GoodsApplyStatusKey.norequired_apply, "无需申请");
		row.add(GoodsApplyStatusKey.no_apply, "未申请");
		row.add(GoodsApplyStatusKey.already_apply, "已申请");
	}
	
	public ArrayList getGoodsApplyStatus(){
		return (row.getFieldNames());
	}
	
	public String getGoodsApplyStatusById(String state){
		return (row.getString(state));
	}
	
	public static void main(String[] args) {
		GoodsApplyStatusKey key = new GoodsApplyStatusKey();
	}
}
