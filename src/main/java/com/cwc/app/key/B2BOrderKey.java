package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class B2BOrderKey
{
	DBRow row;
	
	public static int READY = 1;	//备货中
	public static int INTRANSIT = 2;//正常运送
	public static int APPROVEING = 3;//审核中
	public static int FINISH = 4;//完成
	public static int PACKING = 5;//装箱中
	public static int NOFINISH = 7;//未完成
	public static int AlREADYARRIAL = 8;	//已到货
	public static int RECEIVEING = 9;	//收货中
	public static int AlREADYRECEIVE = 10;	//已收货
	public static int CANINSTORE = 11;		//条码机可下载
	public static int PARTALLOCATE = 12;	//部分保留
	
	
	public B2BOrderKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(B2BOrderKey.READY), "备货中");
		row.add(String.valueOf(B2BOrderKey.PACKING), "<font color='green'>装货中</font>");
		row.add(String.valueOf(B2BOrderKey.INTRANSIT), "<font color='blue'>运输中</font>");
		row.add(String.valueOf(B2BOrderKey.APPROVEING), "<font color='red'>审核中</font>");
		row.add(String.valueOf(B2BOrderKey.FINISH), "已入库");
		row.add(String.valueOf(B2BOrderKey.AlREADYARRIAL), "已到货");
		row.add(String.valueOf(B2BOrderKey.RECEIVEING), "收货中");
		row.add(String.valueOf(B2BOrderKey.AlREADYRECEIVE), "未完成");
		row.add(String.valueOf(B2BOrderKey.AlREADYRECEIVE),"已收货");
		row.add(String.valueOf(B2BOrderKey.PARTALLOCATE),"部分保留");
	}

	public ArrayList getB2BOrderKeys()
	{
		return(row.getFieldNames());
	}

	public String getB2BOrderKeyById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
