package com.cwc.app.key;

import java.util.List;

import com.cwc.db.DBRow;

public class CheckInSpotAreaKey {

	DBRow row;
	public static int FULL = 1;
	public static int EMPTY = 2;
	public static int TRACTOR = 3;
	
	public CheckInSpotAreaKey()
	{
		row = new DBRow();
		row.add(String.valueOf(CheckInSpotAreaKey.FULL), "FULL");
		row.add(String.valueOf(CheckInSpotAreaKey.EMPTY), "EMPTY");
		row.add(String.valueOf(CheckInSpotAreaKey.TRACTOR), "TRACTOR");
	}
	
	public List getCheckInSpotAreaKey()
	{
		return row.getFieldNames();
	}
	
	public String getCheckInSpotAreaKey(int id)
	{
		return row.getString(String.valueOf(id));
	}
	
	public String getCheckInSpotAreaKey(String id)
	{
		return row.getString(id);
	}
}
