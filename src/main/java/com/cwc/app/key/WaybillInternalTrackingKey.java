package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class WaybillInternalTrackingKey
{
	DBRow row;
	
	public static int UnKnown = 0;
	public static int NormalTransport = 1;
	public static int DeliveryCompleted = 2;
	public static int AddressError = 3;
	public static int CustomsClearanceError = 4;
	public static int WaitPickUp = 5;
	public static int NotYetDeliveryComplete = 6;//(非未知，非已妥投)
	

	public WaybillInternalTrackingKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(WaybillInternalTrackingKey.UnKnown),"未知");
		row.add(String.valueOf(WaybillInternalTrackingKey.NormalTransport), "正常运输");
		row.add(String.valueOf(WaybillInternalTrackingKey.DeliveryCompleted), "<font color='blue'>已妥投</font>");
		row.add(String.valueOf(WaybillInternalTrackingKey.AddressError), "<font color='red'>派送例外</font>");
		row.add(String.valueOf(WaybillInternalTrackingKey.CustomsClearanceError),"<font color='red'>清关例外</font>");
		row.add(String.valueOf(WaybillInternalTrackingKey.WaitPickUp),"等待提货");
		row.add(String.valueOf(WaybillInternalTrackingKey.NotYetDeliveryComplete),"尚未妥投");
	}

	public ArrayList getWaybillInternalTrack()
	{
		return(row.getFieldNames());
	}

	public String getWaybillInternalTrack(String id)
	{
		return(row.getString(id));
	}
}
