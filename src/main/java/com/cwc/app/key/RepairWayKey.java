package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class RepairWayKey {
	
	DBRow row;
	
	public static int SEAWAY = 1;	//海运
	public static int AIRWAY = 3;//空运
	public static int EXPRESSWAY = 4;//快递
	public static int LANDWAY = 2;//陆运
	
	public RepairWayKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(TransportWayKey.SEAWAY), "海运");
		row.add(String.valueOf(TransportWayKey.AIRWAY), "空运");
		row.add(String.valueOf(TransportWayKey.EXPRESSWAY), "快递");
		row.add(String.valueOf(TransportWayKey.LANDWAY), "陆运");
	}
	
	public ArrayList<String> getStatuses()
	{
		return(row.getFieldNames());
	}

	public String getStatusById(int id)
	{
		return row.getString(String.valueOf(id));
	}
}
