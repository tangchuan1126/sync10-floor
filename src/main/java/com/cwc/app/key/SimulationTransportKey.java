package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class SimulationTransportKey
{
	DBRow row;
	public static int Out = 1;	//发货
	public static int In = 2 ; //收货
 
	public SimulationTransportKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(SimulationTransportKey.Out), "发货");
		row.add(String.valueOf(SimulationTransportKey.In), "收货");
	}
	
	public ArrayList getSimulationTransportKeys()
	{
		return(row.getFieldNames());
	}

	public String getSimulationTransportKeyValue(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
