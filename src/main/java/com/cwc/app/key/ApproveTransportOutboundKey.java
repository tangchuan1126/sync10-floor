package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;


public class ApproveTransportOutboundKey
{
	DBRow row;
	
	public static int WAITAPPROVE = 1;		//待审核
	public static int APPROVE = 2;	//已审核
	
	public ApproveTransportOutboundKey() 
	{
		row = new DBRow();
	}

	public ArrayList getApproveTransportOutboundStatus()
	{
		return(row.getFieldNames());
	}

	public String getApproveTransportOutboundStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
