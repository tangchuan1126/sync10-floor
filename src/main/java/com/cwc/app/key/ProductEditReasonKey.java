package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ProductEditReasonKey
{
	DBRow row;

	public static int SELF = 1;
	public static int PRODUCTUNION = 2;
	public static int PURCHASE = 3;
	public static int FIRST = 4;
	
	

	public ProductEditReasonKey() 
	{
		row = new DBRow();
		
		row.add(String.valueOf(ProductEditReasonKey.SELF),"自主");
		row.add(String.valueOf(ProductEditReasonKey.PRODUCTUNION),"套装关联");
		row.add(String.valueOf(ProductEditReasonKey.PURCHASE),"采购关联");
		row.add(String.valueOf(ProductEditReasonKey.FIRST),"初始化");
		
	}

	public ArrayList getProductEditReason()
	{
		return(row.getFieldNames());
	}

	public String getProductEditReasonId(String id)
	{
		return(row.getString(id));
	}
}
