package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ReturnProductStatusKey {
DBRow row;
	
	public static int NoReturn = 1;	//无需退货
	public static int Waiting = 2;	//等待退货
	public static int GetImg = 3;	//获得图片
	public static int GetProduct = 4;//收到退货
	
	public ReturnProductStatusKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(ReturnProductStatusKey.NoReturn),"无需退货");
		row.add(String.valueOf(ReturnProductStatusKey.Waiting),"<font color='#ff6600'><strong>等待退货</strong></font>");
		row.add(String.valueOf(ReturnProductStatusKey.GetImg),"<font color=blue><strong>获得图片</strong></font>");
		row.add(String.valueOf(ReturnProductStatusKey.GetProduct),"<font color=red><strong>收到退货</strong></font>");
	}
	
	public ArrayList getReturnProductsStatus()
	{
		return(row.getFieldNames());
	}

	public String getReturnProductsStatusById(int id)
	{
		return(row.getString(String.valueOf(id)));
	}
}
