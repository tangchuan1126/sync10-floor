package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class AfterServiceKey
{
	DBRow row;

	public static int NORMAL_REFUNDING = 6;
	public static int DISPUTE_REFUNDING = 7;
	public static int PART_REFUNDED = 8;
	public static int ALL_REFUNDED = 9;
	public static int NORMAL_WARRANTYING = 10;
	public static int DISPUTE_WARRANTYING = 11;
	public static int WARRANTIED = 12;
	public static int OTHER_DISPUTING = 13;

	public static int FREE_WARRANTIED = 14;
	public static int FAILURE_REFUND = 15;
	public static int FAILURE_WARRANTY = 16;
	public static int FAILURE_DISPUTING = 17;

	public static int BAD_REFUND = 29;//退款差评
	public static int BAD_WARRANTY = 30;//质保差评
	public static int BAD_DISPUTING = 31;//差评
	public static int FREE_SHIPPING_FEE_WARRANTY = 32;//免运费质保
	

	public AfterServiceKey() 
	{
		row = new DBRow();
		
		row.add(String.valueOf(AfterServiceKey.NORMAL_REFUNDING),"<font color=black>正常退款中</font>");
		row.add(String.valueOf(AfterServiceKey.DISPUTE_REFUNDING),"<font color=red>争议退款中</font>");
		row.add(String.valueOf(AfterServiceKey.PART_REFUNDED),"<font color=black>已部分退款</font>");
		row.add(String.valueOf(AfterServiceKey.ALL_REFUNDED),"<font color=black>已全部退款</font>");
		row.add(String.valueOf(AfterServiceKey.NORMAL_WARRANTYING),"<font color=black>正常质保中</font>");
		row.add(String.valueOf(AfterServiceKey.DISPUTE_WARRANTYING),"<font color=red>争议质保中</font>");
		row.add(String.valueOf(AfterServiceKey.WARRANTIED),"<font color=black>有偿质保</font>");
		row.add(String.valueOf(AfterServiceKey.OTHER_DISPUTING),"<font color=red>其他争议</font>");

		row.add(String.valueOf(AfterServiceKey.FREE_WARRANTIED),"<font color=black>无偿质保</font>");
		row.add(String.valueOf(AfterServiceKey.FAILURE_REFUND),"<font color=black>退款失败</font>");
		row.add(String.valueOf(AfterServiceKey.FAILURE_WARRANTY),"<font color=black>质保失败</font>");
		row.add(String.valueOf(AfterServiceKey.FAILURE_DISPUTING),"<font color=black>争议失败</font>");
		
		row.add(String.valueOf(AfterServiceKey.BAD_REFUND),"<font color=black>退款差评</font>");
		row.add(String.valueOf(AfterServiceKey.BAD_WARRANTY),"<font color=black>质保差评</font>");
		row.add(String.valueOf(AfterServiceKey.BAD_DISPUTING),"<font color=black>争议差评</font>");
		row.add(String.valueOf(AfterServiceKey.FREE_SHIPPING_FEE_WARRANTY),"<font color=black>免运费质保</font>");
	}

	public ArrayList getOrderHandle()
	{
		return(row.getFieldNames());
	}

	public String getOrderHandleById(String id)
	{
		return(row.getString(id));
	}
}
