package com.cwc.app.key;

import java.util.ArrayList;
import java.util.List;

import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;

public class LengthUOMKey {

	DBRow row;

	public static final int MM = 1;
	public static final int CM = 2;
	public static final int DM = 3;
	public static final int INCH = 4;

	public LengthUOMKey() {
		row = new DBRow();
		row.add(String.valueOf(LengthUOMKey.INCH), "inch");
		row.add(String.valueOf(LengthUOMKey.MM), "mm");
		row.add(String.valueOf(LengthUOMKey.CM), "cm");
		row.add(String.valueOf(LengthUOMKey.DM), "dm");
	}
	
	public DBRow[] getKey() {
		
		List<?> list = this.getLengthUOMKeys();
		
		DBRow[] rowArray = new DBRow[list.size()];
		
		for(int i = 0;i<list.size();i++){
			
			DBRow dbrow = new DBRow();
			dbrow.put("id", Integer.valueOf(list.get(i).toString()));
			dbrow.put("name", this.getLengthUOMKey(Integer.valueOf(list.get(i).toString())));
			
			rowArray[i] = dbrow;
		}
		
		return rowArray;
	}
	
	public ArrayList<?> getLengthUOMKeys() {
		return (row.getFieldNames());
	}

	public String getLengthUOMKey(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public int getLengthUOMKeyByValue(String value) {
		int reInt = 0;
		if (!StrUtil.isBlank(value)) {
			value = value.toLowerCase();
			ArrayList<?> names = this.getLengthUOMKeys();
			for (Object object : names) {
				int key = Integer.parseInt(String.valueOf(object));
				String values = this.getLengthUOMKey(key);
				if (value.toUpperCase().equals(values.toUpperCase())) {
					reInt = key;
					break;
				}
			}
		}
		return reInt;
	}
}
