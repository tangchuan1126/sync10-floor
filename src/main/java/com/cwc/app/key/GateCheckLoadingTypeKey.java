package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class GateCheckLoadingTypeKey {

	DBRow row;
	public static int LOAD = 1;
	public static int CTNR = 2;
	public static int BOL = 3;
	public static int OTHERS = 4;
	
	public GateCheckLoadingTypeKey() 
	{
		row = new DBRow();
		row.add(String.valueOf(GateCheckLoadingTypeKey.LOAD), "LOAD");
		row.add(String.valueOf(GateCheckLoadingTypeKey.CTNR), "CTNR");
		row.add(String.valueOf(GateCheckLoadingTypeKey.BOL), "BOL");
		row.add(String.valueOf(GateCheckLoadingTypeKey.OTHERS), "OTHERS");
 
	}

	public ArrayList getContainerTypeKeys() {
		return (row.getFieldNames());
	}

	public String getContainerTypeKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getContainerTypeKeyValue(String id) {
		return (row.getString(id));
	}

}
