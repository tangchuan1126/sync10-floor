package com.cwc.app.beans;

public class AddInProductBean
{
	private String p_code;
	private long ps_id;
	private float quantity;
	private long adid;
	private int stat_flag; //0-未计算进库库存 ，1-已计算进库库存
	private int log_type;
	private String position = null;
	private long pc_id;
	
	public String getPCode()
	{
		
		return p_code;
	}
	public void setPCode(String p_code)
	{
		this.p_code = p_code;
	}
	public long getPsId()
	{
		return ps_id;
	}
	public void setPsId(long ps_id)
	{
		this.ps_id = ps_id;
	}
	public float getQuantity()
	{
		return quantity;
	}
	public void setQuantity(float quantity)
	{
		this.quantity = quantity;
	}
	public long getAdid()
	{
		return adid;
	}
	public void setAdid(long adid)
	{
		this.adid = adid;
	}
	public int getStatFlag()
	{
		return stat_flag;
	}
	public void setStatFlag(int stat_flag)
	{
		this.stat_flag = stat_flag;
	}
	public int getLogType()
	{
		return log_type;
	}
	public void setLogType(int log_type)
	{
		this.log_type = log_type;
	}
	
	public String getPosition()
	{
		return position;
	}
	public void setPosition(String position)
	{
		this.position = position;
	}
	
	public long getPcid()
	{
		return this.pc_id;
	}
	
	public void setPcid(long pc_id)
	{
		this.pc_id = pc_id;
	}
}
