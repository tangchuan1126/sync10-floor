package com.cwc.app.beans;

import com.cwc.db.DBRow;

/**
 * 返回商品库存和重量预计算结果
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class PreCalcuOrderBean
{
	private boolean orderIsLacking;
	private DBRow result[];
	
	public boolean isOrderIsLacking()
	{
		return orderIsLacking;
	}
	
	public void setOrderIsLacking(boolean orderIsLacking)
	{
		this.orderIsLacking = orderIsLacking;
	}
	
	public DBRow[] getResult()
	{
		return result;
	}
	
	public void setResult(DBRow[] result)
	{
		this.result = result;
	}
}
