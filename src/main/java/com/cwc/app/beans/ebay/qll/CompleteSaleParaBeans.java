package com.cwc.app.beans.ebay.qll;

import java.io.Serializable;


public class CompleteSaleParaBeans  implements Serializable
{
	 
 
	private static final long serialVersionUID = 1L;
	private String ItemID="";
	private String TrackingNumber="";
	private String Carrier="";
	private String TransactionID="";
	private String sellerID="";
	
	public String getSellerID() {
		return sellerID;
	}
	/**
	 * 用于获取token的参数
	 * @param sellerID
	 */
	public void setSellerID(String sellerID) {
		this.sellerID = sellerID;
	} 	
	 
	public String getTrackingNumber() {
		return TrackingNumber;
	}
	public String getTransactionID() {
		return TransactionID;
	}
	/**
	 * 需要上传tracking Nubmer 的交易号
	 * @param transactionID
	 */
	public void setTransactionID(String transactionID) {
		TransactionID = transactionID;
	}
	/**
	 * 上传tracking number时 要发送的运单号可包含字母数字
	 * @param trackingNumber
	 */
	public void setTrackingNumber(String trackingNumber) {
		TrackingNumber = trackingNumber;
	}
	public String getCarrier() {
		return Carrier;
	}
	/**
	 * 上传tracking number时  要发送的承担运送的厂商名字 不能包含汉字
	 * @param carrier 
	 */
	public void setCarrier(String carrier) {
		Carrier = carrier;
	}
	public String getItemID() {
		return ItemID;
	}
	/**
	 * 已成交商品的ID
	 * @param itemID
	 */
	public void setItemID(String itemID) {
		ItemID = itemID;
	}

}
