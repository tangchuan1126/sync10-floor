package com.cwc.app.beans.ebay.qll;

import java.io.Serializable;

 
public class MessageToPartnerParaBeans  implements Serializable
{
	 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String ItemID="";
    public String getItemID() {
		return ItemID;
	}
    
    public String sellerID="";
	public String getSellerID() {
		return sellerID;
	}
	/**
	 * 用于获取token的参数
	 * @param sellerID
	 */
	public void setSellerID(String sellerID) {
		this.sellerID = sellerID;
	}
	
    /**
     * 商品ID（非必填项）
     * @param itemID
     */
	public void setItemID(String itemID) {
		ItemID = itemID;
	}
	 
 
	public String getMessageBody() {
		return MessageBody;
	}
	/**
	 *  消息内容
	 * @param messageBody
	 */
	public void setMessageBody(String messageBody) {
		MessageBody = messageBody;
	}
	public String getRecipientID() {
		return RecipientID;
	}
	/**
	 * 消息接收者ID
	 * @param recipientID
	 */
	public void setRecipientID(String recipientID) {
		RecipientID = recipientID;
	}
	 
	 

    public String MessageBody="";
    public String RecipientID="";
    
    
}
