package com.cwc.app.beans.ebay.qll;

import java.io.Serializable;


public class MessageToBidderParaBeans  implements Serializable
{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String ItemID="";
    public String getItemID() {
		return ItemID;
	}
    /**
     * 商品的ID
     * @param itemID
     */
	public void setItemID(String itemID) {
		ItemID = itemID;
	}
	 
	public String getMessageBody() {
		return MessageBody;
	}
	/**
	 * 消息内容
	 * @param messageBody
	 */
	public void setMessageBody(String messageBody) {
		MessageBody = messageBody;
	}
	 
	public String getRecipientID() {
		return RecipientID;
	}
	/**
	 * RecipientID;  消息 接收者ID
	 * @param recipientID
	 */
	public void setRecipientID(String recipientID) {
		RecipientID = recipientID;
	}
	  
    public String MessageBody="";
 
    public String RecipientID="";
   
	 
    public String sellerID;
	public String getSellerID() {
		return sellerID;
	}
	/**
	 * 用于获取token的参数
	 * @param sellerID
	 */
	public void setSellerID(String sellerID) {
		this.sellerID = sellerID;
	}
    
}
