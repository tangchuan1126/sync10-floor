package com.cwc.app.beans.ebay.qll;
 
public class MessageResponseToRequestParaBeans {

   public String MessageBody="";
   public String ItemID="";
   public String sellerID="";
   
	public String getSellerID() {
		return sellerID;
	}
	/**
	 * 用于获取token的参数
	 * @param sellerID
	 */
	public void setSellerID(String sellerID) {
		this.sellerID = sellerID;
	}
	
	public String getMessageBody() {
		return MessageBody;
	}
	/**
	 * MessageBody   要发送消息的内容
	 * @param messageBody
	 */
	public void setMessageBody(String messageBody) {
		MessageBody = messageBody;
	}
	public String getItemID() {
		return ItemID;
	}
	/**
	 * 商品的ID
	 * @param itemID
	 */
	public void setItemID(String itemID) {
		ItemID = itemID;
}
	
}
