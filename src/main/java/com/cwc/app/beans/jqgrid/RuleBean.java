package com.cwc.app.beans.jqgrid;

public class RuleBean {
	private String field;//搜索字段
	private String op;//关系
	private String data;//值
	
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getOp() {
		return op;
	}
	public void setOp(String op) {
		this.op = op;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
}
