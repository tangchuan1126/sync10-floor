package com.cwc.app.beans;

public class AddShipProductBean
{
	public long getOid()
	{
		return oid;
	}
	public void setOid(long oid)
	{
		this.oid = oid;
	}
	public String getDeliveryCode()
	{
		return delivery_code;
	}
	public void setDeliveryCode(String delivery_code)
	{
		this.delivery_code = delivery_code;
	}
	public float getProductWeight()
	{
		return product_weight;
	}
	public void setProductWeight(float product_weight)
	{
		this.product_weight = product_weight;
	}
	public String getDeliveryOperator()
	{
		return delivery_operator;
	}
	public void setDeliveryOperator(String delivery_operator)
	{
		this.delivery_operator = delivery_operator;
	}
	public String getDeliveryto()
	{
		return deliveryto;
	}
	public void setDeliveryto(String deliveryto)
	{
		this.deliveryto = deliveryto;
	}
	public String getDeliveryAccount()
	{
		return delivery_account;
	}
	public void setDeliveryAccount(String delivery_account)
	{
		this.delivery_account = delivery_account;
	}
	
	private long oid;					//订单号
	private String delivery_code;		//运单号
	private float product_weight;		//重量
	private String delivery_operator;	//发货人
	private String deliveryto;			//目的地
	private String delivery_account;	//当前登录帐号
	private long scid;					//快递公司ID
	public long getScid()
	{
		return scid;
	}
	public void setScid(long scid)
	{
		this.scid = scid;
	}
}
