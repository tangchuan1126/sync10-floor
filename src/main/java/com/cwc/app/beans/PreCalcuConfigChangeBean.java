package com.cwc.app.beans;

import com.cwc.db.DBRow;

/**
 * 返回商品库存和重量预计算结果
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class PreCalcuConfigChangeBean
{
	private int storeStatus;
	private DBRow result[];
	private long ps_id;
	
	public int getStoreStatus() {
		return storeStatus;
	}
	public void setStoreStatus(int storeStatus) {
		this.storeStatus = storeStatus;
	}
	public DBRow[] getResult() {
		return result;
	}
	public void setResult(DBRow[] result) {
		this.result = result;
	}
	public long getPs_id() {
		return ps_id;
	}
	public void setPs_id(long ps_id) {
		this.ps_id = ps_id;
	}
}
