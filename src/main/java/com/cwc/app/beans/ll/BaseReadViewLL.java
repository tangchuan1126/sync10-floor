package com.cwc.app.beans.ll;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

/**
 * 
 * @author liliang
 * @PKField类型 只支持long和String
 */
public class BaseReadViewLL{
	private String viewName;
	private DBUtilAutoTran dbUtilAutoTran;
	private String sql = "";
	private boolean debugInfo = true;
	
	public String getViewName() {
		return viewName;
	}


	public DBUtilAutoTran getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public BaseReadViewLL() {

	}

	public BaseReadViewLL(String viewName) {
		this.viewName = viewName;
	}
	/**
	 * @TODO 获取表的所有记录
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAll() throws Exception {
		String sql = "select * from " + viewName;
		return dbUtilAutoTran.selectMutliple(sql);
	}
}
