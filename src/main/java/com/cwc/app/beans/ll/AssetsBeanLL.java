package com.cwc.app.beans.ll;

import com.cwc.db.DBRow;

public class AssetsBeanLL extends BaseBeanLL {
	private AssetsBeanLL() {
	}
	
	public AssetsBeanLL(String tableName, String PK) {
		super(tableName,PK);
	}
	
	public boolean checkApplyMoneyAssociationById(String id,int type) throws Exception {
		DBRow para = new DBRow();
		para.add("aid", id);
		para.add("category_id", 100023);
		DBRow operator = new DBRow();
		if(type==2) {
			operator.add("category_id", "<>");
		}
		
		DBRow[] rows = this.getRowsByPara(para, operator, null, "", null);
		if(rows.length>0) 
			return true;
		else
			return false;
	}
}
