package com.cwc.app.beans.ll;

import com.cwc.app.util.StrUtil;

public class PurchaseDetailBeanLL extends BaseBeanLL {
	private PurchaseDetailBeanLL() {
	}
	
	public PurchaseDetailBeanLL(String tableName, String PK) {
		super(tableName,PK);
	}

	public boolean checkApplyMoneyAssociationById(String id,int type) throws Exception {
		return getRowById(Long.toString(StrUtil.getLong(id)))==null?false:true;
	}
}
