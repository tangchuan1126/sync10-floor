package com.cwc.app.beans.ll;

import com.cwc.db.DBRow;
import com.cwc.app.util.StrUtil;

public class DeliveryOrderBeanLL extends BaseBeanLL {
	private DeliveryOrderBeanLL() {
	}
	
	public DeliveryOrderBeanLL(String tableName, String PK) {
		super(tableName,PK);
	}
	
	public boolean checkApplyMoneyAssociationById(String id,int type) throws Exception {
		return getRowById(Long.toString(StrUtil.getLong(id)))==null?false:true;
	}
}
