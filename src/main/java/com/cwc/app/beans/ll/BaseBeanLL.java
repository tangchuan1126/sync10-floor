package com.cwc.app.beans.ll;


import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.cwc.app.util.StrUtil;
/**
 * 
 * @author liliang
 * @PKField类型 只支持long和String
 */
public class BaseBeanLL{
	private String tableName;
	private String PK;
	private DBUtilAutoTran dbUtilAutoTran;
	private String sql = "";
	private boolean debugInfo = true;
	
	public String getTableName() {
		return tableName;
	}

	public String getPK() {
		return PK;
	}

	public DBUtilAutoTran getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public BaseBeanLL() {

	}

	public BaseBeanLL(String tableName, String PK) {
		this.tableName = tableName;
		this.PK = PK;
	}
	/**
	 * @TODO 获取表的所有记录
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAll() throws Exception {
		String sql = "select * from " + tableName;
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	public DBRow[] getAll(String orderby) throws Exception {
		String sql = "select * from " + tableName;
		if(!orderby.equals("")) {
			sql += " order by " + orderby;
		}
		return dbUtilAutoTran.selectMutliple(sql);
	}

	/**
	 * @TODO 根据id获取表记录查询
	 * @param id id值
	 * @return
	 * @throws Exception
	 */
	public DBRow getRowById(String id) throws Exception {
		sql = "";
		DBRow para = new DBRow();
		para.add(PK, id);
		sql = "select * from " + tableName + " where " + PK + " = ?";
		return dbUtilAutoTran.selectPreSingle(sql, para);
	}
	
	public DBRow getRowById(long id) throws Exception {
		sql = "";
		DBRow para = new DBRow();
		para.add(PK, id);
		sql = "select * from " + tableName + " where " + PK + " = ?";
		return dbUtilAutoTran.selectPreSingle(sql, para);
	}
	/**
	 * @TODO 表查询
	 * @param para 表查询参数
	 * @param operator >,<,=,<>
	 * @param orAnd and,or
	 * @param orderBy order by PK desc ...
	 * @param pc 分页
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRowsByPara(DBRow para, DBRow operator,DBRow orAnd, String orderBy,PageCtrl pc) throws Exception {
		sql = "";
		
		operator = operator==null?new DBRow():operator;
		orAnd = orAnd==null?new DBRow():orAnd;
		
		DBRow row = new DBRow();
		sql = "select * from " + tableName;
		int t = 0;
		for(int i=0;para!=null && i<para.getFieldNames().size();i++){
			String rowName = para.getFieldNames().get(i).toString();
			String rowValue = para.getValue(rowName)==null?"":para.getValue(rowName).toString();
			
			String operatorValue = operator.getValue(rowName)==null?"=":operator.getValue(rowName).toString();
			String orAndValue = orAnd.getValue(rowName)==null?"and":orAnd.getValue(rowName).toString();
			
			if(i==0 && orAndValue.equals("or"))
				sql += " where 1=2";
			else if(i==0 && orAndValue.equals("and"))
				sql += " where 1=1";
			
			if(!rowValue.equals("")) {
				sql += " " + orAndValue + " " + rowName + " " + operatorValue + " ?";
				row.add(rowName, rowValue);
				t++;
			}
		}
		
		if(!orderBy.equals("")) {
			sql += " order by "+orderBy;
		}
		
		if(pc!=null)
			if(t==0)
				return dbUtilAutoTran.selectMutliple(sql, pc);
			else
				return dbUtilAutoTran.selectPreMutliple(sql,row,pc);
		else
			if(t==0)
				return dbUtilAutoTran.selectMutliple(sql);
			else
				return dbUtilAutoTran.selectPreMutliple(sql, row);
	}
	
	/**
	 * @TODO 表插入
	 * @param data 插入这张表的数据，PK主键值不能包括在参数内
	 * @return
	 * @throws Exception
	 */
	public DBRow insertRow(DBRow row) throws Exception {
		if(row != null && row.getFieldNames().size()>0) {
			long PKValue = dbUtilAutoTran.getSequance(tableName);
			row.add(PK, PKValue);
	
			if(dbUtilAutoTran.insert(tableName, row))
				return row;
		}
		return null;
	}
	
	public DBRow insertRowInc(DBRow row) throws Exception {
		long id = dbUtilAutoTran.insertReturnId(tableName, row);
		row.add(PK, id);
		return row;
	}
	/**
	 * @TODO 表数据更新
	 * @param data 更新这张表的数据，PK主键值必须包括在参数内
	 * @return
	 * @throws Exception
	 */
	public DBRow updateRow(DBRow row) throws Exception {
		sql = " where "+PK+"="+row.getValue(PK).toString()+"";
		if(dbUtilAutoTran.update(sql, tableName, row)==1)
			return getRowById(row.getValue(PK).toString());
		else
			return null;
	}
	
	public int updateRowByCond(String cond, DBRow row) throws Exception {
		sql = cond;
		return dbUtilAutoTran.update(sql, tableName, row);
	}
	/**
	 * @TODO 表数据删除
	 * @param ids 根据根据多个id删除，ids格式:ids=1001,1002,1003
	 * @return 返回影响行数
	 * @throws Exception
	 */
	public int delete(String[] ids) throws Exception {
		String PKsValue = "0";
		for(int i=0;i<ids.length;i++){
			PKsValue += ","+ids[i];
		}
		
		return dbUtilAutoTran.delete("where "+PK+" in("+PKsValue+")", tableName);
	}
	
	public int deleteByCond(String cond) throws Exception {
		return dbUtilAutoTran.delete(cond, tableName);
	}
	/**
	 * @TODO 判断资金表的关联Id,有子类重写方法
	 * @param id 传递关联单据id值。特殊单号：交货单P100010-4,	采购单P100010。特殊单号都在子类里实现方法
	 * @param type 区分固定资产与样品
	 * @return 返回根据id查询出的表单DBRow，表单类型由子类决定
	 * @throws Exception
	 */
	public boolean checkApplyMoneyAssociationById(String id,int type) throws Exception {
		return getRowById(Long.toString(StrUtil.getLong(id)))==null?false:true;
	}
	
	public String viewSql() {
		return debugInfo?sql:"";
	}
	/**
	 * @TODO 获取对照显示表
	 * @param FieldKey 对照键
	 * @param FieldValue 对照值
	 * @return
	 * @throws Exception
	 */
	public DBRow getDBRowMap(String FieldKey, String FieldValue) throws Exception {
		sql = "";
		sql = "select "+FieldKey+","+FieldValue+" from "+tableName;
		DBRow[] rows = dbUtilAutoTran.selectMutliple(sql);
		DBRow row = new DBRow();
		for(int i=0; i<rows.length; i++) {
			row.add(rows[i].getString(FieldKey), rows[i].getString(FieldValue));
		}
		return row;
	}
	
	public DBRow[] getDBRowsByIds(String ids,PageCtrl pc) throws Exception {
		sql = "select * from " + tableName + " where " + PK + " in (" + ids + ")";
		if(pc == null) {
			return dbUtilAutoTran.selectMutliple(sql);
		}
		else {
			return dbUtilAutoTran.selectMutliple(sql,pc);
		}
	}
	
	public DBRow[] getDBRowsByFKIds(String FK, String ids,PageCtrl pc) throws Exception {
		sql = "select * from " + tableName + " where " + FK + " in (" + ids + ")";
		if(pc == null) {
			return dbUtilAutoTran.selectMutliple(sql);
		}
		else {
			return dbUtilAutoTran.selectMutliple(sql,pc);
		}
	}
}
