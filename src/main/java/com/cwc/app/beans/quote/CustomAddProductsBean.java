package com.cwc.app.beans.quote;

/**
 * 定制增加商品
 * @author Administrator
 *
 */
public class CustomAddProductsBean 
{
	 private long pid;
	 private String name;
	 private float quantity;

	public float getQuantity() 
	{
		return quantity;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public long getPid()
	{
		return pid;
	}

	public void setPid(long pid)
	{
		this.pid = pid;
	}
}
