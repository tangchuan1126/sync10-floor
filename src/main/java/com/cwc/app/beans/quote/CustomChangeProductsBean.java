package com.cwc.app.beans.quote;

import java.util.ArrayList;

/**
 * 定制商品
 * 新增和减少商品
 * 定制套装里面原商品数量发生了变化，数量增加：算新增一个商品，数量减少：算减少一个商品
 * @author Administrator
 *
 */
public class CustomChangeProductsBean 
{
	/**
	 * 获得定制增加商品列表
	 * @return
	 */
	public ArrayList<CustomAddProductsBean> getCustomAddProductsBeanList() 
	{
		return customAddProductsBeanList;
	}
	
	/**
	 * 获得定制移除商品列表
	 * @return
	 */
	public ArrayList<CustomRemoveProductsBean> getCustomRemoveProductsBeanList()
	{
		return customRemoveProductsBeanList;
	}
	
	/**
	 * 增加定制增加商品
	 * @param customAddProductsBean
	 */
	public void addCustomAddProductsBean(CustomAddProductsBean customAddProductsBean) 
	{
		customAddProductsBeanList.add(customAddProductsBean);
	}
	
	/**
	 * 增加定制移除商品
	 * @param customRemoveProductsBean
	 */
	public void addCustomRemoveProductsBean(CustomRemoveProductsBean customRemoveProductsBean) 
	{
		customRemoveProductsBeanList.add(customRemoveProductsBean);
	}
	
	private  ArrayList<CustomAddProductsBean> customAddProductsBeanList = new ArrayList<CustomAddProductsBean>();
	private  ArrayList<CustomRemoveProductsBean> customRemoveProductsBeanList = new ArrayList<CustomRemoveProductsBean>();
}


