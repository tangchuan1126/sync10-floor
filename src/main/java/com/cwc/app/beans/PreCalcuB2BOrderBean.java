package com.cwc.app.beans;

import com.cwc.db.DBRow;

/**
 * 返回商品库存和重量预计算结果
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class PreCalcuB2BOrderBean
{
	private int orderStatus;
	private DBRow result[];
	private long ps_id;
	private int orderConfigChange;
	
	
	public DBRow[] getResult()
	{
		return result;
	}
	
	public void setResult(DBRow[] result)
	{
		this.result = result;
	}

	public int getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(int orderStatus) {
		this.orderStatus = orderStatus;
	}

	public long getPs_id() {
		return ps_id;
	}

	public void setPs_id(long ps_id) {
		this.ps_id = ps_id;
	}

	public int getOrderConfigChange() {
		return orderConfigChange;
	}

	public void setOrderConfigChange(int orderConfigChange) {
		this.orderConfigChange = orderConfigChange;
	}
}
