package com.cwc.app.beans.log;

public class ProductStorageBackUpBean {
	
	private long pc_id;								//商品ID，不可为0
	private long ps_id;								//仓库ID，不可为0
	private int available_count;					//可用数量，非空
	private int physical_count;						//物理数量，非空
	private long product_line_id;					//产品线ID，不可为0
	private long[] catalogs;						//分类路径数组数组内数据已按树形排序，不可为0
	
	public long getPc_id() {
		return pc_id;
	}
	public void setPc_id(long pc_id) {
		this.pc_id = pc_id;
	}
	public long getPs_id() {
		return ps_id;
	}
	public void setPs_id(long ps_id) {
		this.ps_id = ps_id;
	}
	public int getAvailable_count() {
		return available_count;
	}
	public void setAvailable_count(int available_count) {
		this.available_count = available_count;
	}
	public int getPhysical_count() {
		return physical_count;
	}
	public void setPhysical_count(int physical_count) {
		this.physical_count = physical_count;
	}
	public long getProduct_line_id() {
		return product_line_id;
	}
	public void setProduct_line_id(long product_line_id) {
		this.product_line_id = product_line_id;
	}
	public long[] getCatalogs() {
		return catalogs;
	}
	public void setCatalogs(long[] catalogs) {
		this.catalogs = catalogs;
	}
	
}
