package com.cwc.app.beans.log;

public class FilterProductStoreLogBean
{
	private long oid;				//业务单据ID
	private int bill_type;			//业务单据类型
	private int operation;			//操作
	private long ps_id;				//仓库
	private long pc_id;				//商品ID
	
	private long adid;				//操作账号ID
	
	private int quantity_type;		//操作库存类型
	
	private long title_id;			//操作的那个title的库存
	private String lot_number;		//操作的批次
	private long catalogs;			//商品分类ID
	private long product_line_id;	//产品线ID
	
	public long getOid() {
		return oid;
	}
	public void setOid(long oid) {
		this.oid = oid;
	}
	public int getBill_type() {
		return bill_type;
	}
	public void setBill_type(int bill_type) {
		this.bill_type = bill_type;
	}
	public int getOperation() {
		return operation;
	}
	public void setOperation(int operation) {
		this.operation = operation;
	}
	
	public long getPs_id() {
		return ps_id;
	}
	public void setPs_id(long ps_id) {
		this.ps_id = ps_id;
	}
	public long getPc_id() {
		return pc_id;
	}
	public void setPc_id(long pc_id) {
		this.pc_id = pc_id;
	}
	public long getAdid() {
		return adid;
	}
	public void setAdid(long adid) {
		this.adid = adid;
	}
	public int getQuantity_type() {
		return quantity_type;
	}
	public void setQuantity_type(int quantity_type) {
		this.quantity_type = quantity_type;
	}
	public long getTitle_id() {
		return title_id;
	}
	public void setTitle_id(long title_id) {
		this.title_id = title_id;
	}
	public String getLot_number() {
		return lot_number;
	}
	public void setLot_number(String lot_number) {
		this.lot_number = lot_number;
	}
	public long getProduct_line_id() {
		return product_line_id;
	}
	public void setProduct_line_id(long product_line_id) {
		this.product_line_id = product_line_id;
	}
	public long getCatalogs() {
		return catalogs;
	}
	public void setCatalogs(long catalogs) {
		this.catalogs = catalogs;
	}
}
