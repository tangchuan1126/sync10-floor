package com.cwc.app.beans.log;

public class OutboundLogBean {
	private long pc_id;
	private long transport_id;
	private long container_id;
	private int count;
	private String serial_number;
	private String p_name;
	private String p_code;
	private String machine_id;
	private long outbound_adid;
	
	public long getPc_id() {
		return pc_id;
	}
	public void setPc_id(long pc_id) {
		this.pc_id = pc_id;
	}
	public long getTransport_id() {
		return transport_id;
	}
	public void setTransport_id(long transport_id) {
		this.transport_id = transport_id;
	}
	public long getContainer_id() {
		return container_id;
	}
	public void setContainer_id(long container_id) {
		this.container_id = container_id;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getSerial_number() {
		return serial_number;
	}
	public void setSerial_number(String serial_number) {
		this.serial_number = serial_number;
	}
	public String getP_name() {
		return p_name;
	}
	public void setP_name(String p_name) {
		this.p_name = p_name;
	}
	public String getP_code() {
		return p_code;
	}
	public void setP_code(String p_code) {
		this.p_code = p_code;
	}
	public String getMachine_id() {
		return machine_id;
	}
	public void setMachine_id(String machine_id) {
		this.machine_id = machine_id;
	}
	public long getOutbound_adid() {
		return outbound_adid;
	}
	public void setOutbound_adid(long outbound_adid) {
		this.outbound_adid = outbound_adid;
	}
	
}
