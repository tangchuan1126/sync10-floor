package com.cwc.app.beans.log;

public class ProductStorageLocationBackUpBean {
	private long ps_id;				//仓库ID，非0
	private long pc_id;				//商品ID，非0
	private String p_code;			//商品条码，非空
	private int available_quantity;	//可用数量，非空
	private int physical_quantity;	//物理数量，非空
	private long slc_id;			//位置ID，非0
	private String position;		//位置编码，非空
	private long time_number;		//排序，非0
	private long title_id;			//所属人，非0
	private String lot_number;		//批次，非空
	private long system_bill_id;	//系统单据号，非0
	private long system_bill_type;	//系统单据类型，非0
	private long product_line_id;	//产品线，非0
	private long[] catalogs;		//商品分类线，非空
	
	public long getPs_id() {
		return ps_id;
	}
	public void setPs_id(long ps_id) {
		this.ps_id = ps_id;
	}
	public long getPc_id() {
		return pc_id;
	}
	public void setPc_id(long pc_id) {
		this.pc_id = pc_id;
	}
	public String getP_code() {
		return p_code;
	}
	public void setP_code(String p_code) {
		this.p_code = p_code;
	}
	public int getAvailable_quantity() {
		return available_quantity;
	}
	public void setAvailable_quantity(int available_quantity) {
		this.available_quantity = available_quantity;
	}
	public int getPhysical_quantity() {
		return physical_quantity;
	}
	public void setPhysical_quantity(int physical_quantity) {
		this.physical_quantity = physical_quantity;
	}
	public long getSlc_id() {
		return slc_id;
	}
	public void setSlc_id(long slc_id) {
		this.slc_id = slc_id;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public long getTime_number() {
		return time_number;
	}
	public void setTime_number(long time_number) {
		this.time_number = time_number;
	}
	public long getTitle_id() {
		return title_id;
	}
	public void setTitle_id(long title_id) {
		this.title_id = title_id;
	}
	public String getLot_number() {
		return lot_number;
	}
	public void setLot_number(String lot_number) {
		this.lot_number = lot_number;
	}
	public long getSystem_bill_id() {
		return system_bill_id;
	}
	public void setSystem_bill_id(long system_bill_id) {
		this.system_bill_id = system_bill_id;
	}
	public long getSystem_bill_type() {
		return system_bill_type;
	}
	public void setSystem_bill_type(long system_bill_type) {
		this.system_bill_type = system_bill_type;
	}
	public long getProduct_line_id() {
		return product_line_id;
	}
	public void setProduct_line_id(long product_line_id) {
		this.product_line_id = product_line_id;
	}
	public long[] getCatalogs() {
		return catalogs;
	}
	public void setCatalogs(long[] catalogs) {
		this.catalogs = catalogs;
	}
}
