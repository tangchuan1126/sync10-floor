package com.cwc.app.beans.log;

public class ProductStorePhysicalLogBean
{
	private long system_bill_id; 					//系统业务单据ID，不可为0
	private long system_bill_type; 					//系统业务单据类型，不可为0
	private long slc_id;							//操作位置ID，不可为0
	private int quantity; 							//操作数量，不可为0
	private long ps_id;								//操作仓库ID，不可为0
	private long pc_id; 							//操作商品ID，不可为0
	private long operation_adid;					//操作人员ID，不可为0
	private int operation_type;						//操作类型，不可为0
	private String serial_number = null;			//操作序列号
	private long lp_id = 0;							//操作的容器ID，不可为0
	private String machine;							//操作机器号，不可为0
	private long title_id;							//操作的title库存
	private String lot_number;						//操作的批次
	private long product_line_id;			//产品线ID，不可为0
	private long[] catalogs;				//分类路径数组数组内数据已按树形排序，不可为0
	
	public long getSystem_bill_id() {
		return system_bill_id;
	}
	public void setSystem_bill_id(long system_bill_id) {
		this.system_bill_id = system_bill_id;
	}
	public long getSystem_bill_type() {
		return system_bill_type;
	}
	public void setSystem_bill_type(long system_bill_type) {
		this.system_bill_type = system_bill_type;
	}
	public long getSlc_id() {
		return slc_id;
	}
	public void setSlc_id(long slc_id) {
		this.slc_id = slc_id;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public long getPs_id() {
		return ps_id;
	}
	public void setPs_id(long ps_id) {
		this.ps_id = ps_id;
	}
	public long getPc_id() {
		return pc_id;
	}
	public void setPc_id(long pc_id) {
		this.pc_id = pc_id;
	}
	public long getOperation_adid() {
		return operation_adid;
	}
	public void setOperation_adid(long operation_adid) {
		this.operation_adid = operation_adid;
	}
	public int getOperation_type() {
		return operation_type;
	}
	public void setOperation_type(int operation_type) {
		this.operation_type = operation_type;
	}
	public String getSerial_number() {
		return serial_number;
	}
	public void setSerial_number(String serial_number) {
		this.serial_number = serial_number;
	}
	public long getLp_id() {
		return lp_id;
	}
	public void setLp_id(long lp_id) {
		this.lp_id = lp_id;
	}
	public String getMachine() {
		return machine;
	}
	public void setMachine(String machine) {
		this.machine = machine;
	}
	public long getProduct_line_id() {
		return product_line_id;
	}
	public void setProduct_line_id(long product_line_id) {
		this.product_line_id = product_line_id;
	}
	public long[] getCatalogs() {
		return catalogs;
	}
	public void setCatalogs(long[] catalogs) {
		this.catalogs = catalogs;
	}
	public long getTitle_id() {
		return title_id;
	}
	public void setTitle_id(long title_id) {
		this.title_id = title_id;
	}
	public String getLot_number() {
		return lot_number;
	}
	public void setLot_number(String lot_number) {
		this.lot_number = lot_number;
	}
	
}
