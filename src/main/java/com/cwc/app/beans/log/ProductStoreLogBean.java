package com.cwc.app.beans.log;

public class ProductStoreLogBean
{
	private long oid;			//业务单据ID，不可为0
	private int bill_type;		//业务单据类型，不可为0
	private int operation;		//操作，不可为0
	private double quantity;	//数量
	private long ps_id;			//仓库，不可为0
	private long pc_id;			//商品ID，不可为0
	private String account;		//操作账号，不可为空
	
	private long adid;			//操作账号ID，不可为0
	
	private int quantity_type;	//操作库存类型
	
	private long title_id;		//操作的那个title的库存，不可为0
	private String lot_number;	//操作的批次，不可为空
	
	private long product_line_id;//产品线ID，不可为0
	private long[] catalogs;	//分类路径数组数组内数据已按树形排序，不可为空
	
	
	private long cancel_psl_id = 0;
	
//	private double merge_count;
//	private double lacking_quantity;
//	private double store_count;

	public long getOid()
	{
		return oid;
	}
	
	public void setOid(long oid)
	{
		this.oid = oid;
	}
	
	public int getOperation()
	{
		return operation;
	}
	
	public void setOperation(int operation)
	{
		this.operation = operation;
	}
	
	public double getQuantity()
	{
		return quantity;
	}
	
	public long getPs_id()
	{
		return ps_id;
	}
	
	public void setPs_id(long ps_id)
	{
		this.ps_id = ps_id;
	}
	
	public long getPc_id()
	{
		return pc_id;
	}
	
	public void setPc_id(long pc_id)
	{
		this.pc_id = pc_id;
	}
	
	public String getAccount()
	{
		return account;
	}
	
	public void setAccount(String account)
	{
		this.account = account;
	}

	public void setQuantity(double quantity)
	{
		this.quantity = quantity;
	}

	public long getAdid() {
		return adid;
	}

	public void setAdid(long adid) {
		this.adid = adid;
	}

	public int getBill_type() {
		return bill_type;
	}

	public void setBill_type(int bill_type) {
		this.bill_type = bill_type;
	}

	public int getQuantity_type() {
		return quantity_type;
	}

	public void setQuantity_type(int quantity_type) {
		this.quantity_type = quantity_type;
	}

	public long getCancel_psl_id() {
		return cancel_psl_id;
	}

	public void setCancel_psl_id(long cancel_psl_id) {
		this.cancel_psl_id = cancel_psl_id;
	}

	public long getTitle_id() {
		return title_id;
	}

	public void setTitle_id(long title_id) {
		this.title_id = title_id;
	}

	public String getLot_number() {
		return lot_number;
	}

	public void setLot_number(String lot_number) {
		this.lot_number = lot_number;
	}

	public long getProduct_line_id() {
		return product_line_id;
	}

	public void setProduct_line_id(long product_line_id) {
		this.product_line_id = product_line_id;
	}

	public long[] getCatalogs() {
		return catalogs;
	}

	public void setCatalogs(long[] catalogs) {
		this.catalogs = catalogs;
	}
}
