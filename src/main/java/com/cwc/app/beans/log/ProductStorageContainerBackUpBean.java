package com.cwc.app.beans.log;

public class ProductStorageContainerBackUpBean {
	private long pc_id;								//商品ID，非0
	private long slc_id;							//位置ID，非0
	private long con_id;							//容器ID，非0
	private long title_id;							//所属人ID，非0
	private int available_count;					//可用数量
	private int physical_count;						//物理数量
	private String lot_number;						//批次号，非空
	private int container_type;						//容器类型，非0
	private long container_type_id;					//容器类型ID，非0
	private long product_line_id;					//产品线ID，不可为0
	private long[] catalogs;						//分类路径数组数组内数据已按树形排序，不可为0
	
	public long getPc_id() {
		return pc_id;
	}
	public void setPc_id(long pc_id) {
		this.pc_id = pc_id;
	}
	public long getSlc_id() {
		return slc_id;
	}
	public void setSlc_id(long slc_id) {
		this.slc_id = slc_id;
	}
	public long getCon_id() {
		return con_id;
	}
	public void setCon_id(long con_id) {
		this.con_id = con_id;
	}
	public long getTitle_id() {
		return title_id;
	}
	public void setTitle_id(long title_id) {
		this.title_id = title_id;
	}
	public int getAvailable_count() {
		return available_count;
	}
	public void setAvailable_count(int available_count) {
		this.available_count = available_count;
	}
	public int getPhysical_count() {
		return physical_count;
	}
	public void setPhysical_count(int physical_count) {
		this.physical_count = physical_count;
	}
	public String getLot_number() {
		return lot_number;
	}
	public void setLot_number(String lot_number) {
		this.lot_number = lot_number;
	}
	public int getContainer_type() {
		return container_type;
	}
	public void setContainer_type(int container_type) {
		this.container_type = container_type;
	}
	public long getContainer_type_id() {
		return container_type_id;
	}
	public void setContainer_type_id(long container_type_id) {
		this.container_type_id = container_type_id;
	}
	public long getProduct_line_id() {
		return product_line_id;
	}
	public void setProduct_line_id(long product_line_id) {
		this.product_line_id = product_line_id;
	}
	public long[] getCatalogs() {
		return catalogs;
	}
	public void setCatalogs(long[] catalogs) {
		this.catalogs = catalogs;
	}

}
