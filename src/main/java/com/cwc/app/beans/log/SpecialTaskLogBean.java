package com.cwc.app.beans.log;

import us.monoid.json.JSONObject;

public class SpecialTaskLogBean {
    private String operation_content;//操作内容 可为空
    private int module;//所属模块 非空
    private String operation_note;//操作内容 非空
    private int type;//所属类型 task or invoice 非空
    private Long relation_id;//关联单据id 非空
    private String note;//备注 可为空
    private Long adid;//操作人id 可为空
    private String account;//账户名字 可为空
    private JSONObject department;//部门  可为空
    
    
	public String getOperation_content() {
		return operation_content;
	}
	public void setOperation_content(String operation_content) {
		this.operation_content = operation_content;
	}
	public int getModule() {
		return module;
	}
	public void setModule(int module) {
		this.module = module;
	}
	public String getOperation_note() {
		return operation_note;
	}
	public void setOperation_note(String operation_note) {
		this.operation_note = operation_note;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	public Long getRelation_id() {
		return relation_id;
	}
	public void setRelation_id(Long relation_id) {
		this.relation_id = relation_id;
	}
	public Long getAdid() {
		return adid;
	}
	public void setAdid(Long adid) {
		this.adid = adid;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public JSONObject getDepartment() {
		return department;
	}
	public void setDepartment(JSONObject department) {
		this.department = department;
	}
	
    
}
