package com.cwc.app.beans.log;

public class ProductStoreAllocateLogBean
{
	private long pc_id; 							//商品ID，不可为0
	private long slc_id;							//位置ID，不可为0
	private long ps_id;								//仓库ID，不可为0
	private int allocate_piece_count;			//分配的商品数量，不可为0
	private long allocate_adid;						//分配操作人，不可为0
	private long system_bill_id; 					//业务单据号，不可为0
	private long system_bill_type;					//业务单据类型，不可为0
	private long con_id;							//容器ID，不可为0
	private int container_type;						//容器类型，不可为0
	private long container_type_id;					//容器类型ID，不可为0
	private int allocate_container_count = 0;		//分配的容器数量
	private String lot_number;						//操作的批次号，非空
	private long title_id = 0;						//操作的货的所有人，不可为0
	private long product_line_id;					//产品线ID，不可为0
	private long[] catalogs;						//分类路径数组数组内数据已按树形排序，非空
	
	public long getPc_id() {
		return pc_id;
	}
	public void setPc_id(long pc_id) {
		this.pc_id = pc_id;
	}
	public long getSlc_id() {
		return slc_id;
	}
	public void setSlc_id(long slc_id) {
		this.slc_id = slc_id;
	}
	public long getPs_id() {
		return ps_id;
	}
	public void setPs_id(long ps_id) {
		this.ps_id = ps_id;
	}
	public long getCon_id() {
		return con_id;
	}
	/**
	 * 容器ID，可能为空
	 * @param con_id
	 */
	public void setCon_id(long con_id) {
		this.con_id = con_id;
	}
	public int getContainer_type() {
		return container_type;
	}
	/**
	 * 容器类型，可能为空
	 * @param container_type
	 */
	public void setContainer_type(int container_type) {
		this.container_type = container_type;
	}
	public long getContainer_type_id() {
		return container_type_id;
	}
	/**
	 * 容器类型ID，可能为空
	 * @param container_type_id
	 */
	public void setContainer_type_id(long container_type_id) {
		this.container_type_id = container_type_id;
	}
	public int getAllocate_container_count() {
		return allocate_container_count;
	}
	/**
	 * 容器数量，可能为空
	 * @param allocate_container_count
	 */
	public void setAllocate_container_count(int allocate_container_count) {
		this.allocate_container_count = allocate_container_count;
	}
	public double getAllocate_piece_count() {
		return allocate_piece_count;
	}
	public void setAllocate_piece_count(int allocate_piece_count) {
		this.allocate_piece_count = allocate_piece_count;
	}
	public long getAllocate_adid() {
		return allocate_adid;
	}
	public void setAllocate_adid(long allocate_adid) {
		this.allocate_adid = allocate_adid;
	}
	public long getSystem_bill_id() {
		return system_bill_id;
	}
	public void setSystem_bill_id(long system_bill_id) {
		this.system_bill_id = system_bill_id;
	}
	public long getSystem_bill_type() {
		return system_bill_type;
	}
	public void setSystem_bill_type(long system_bill_type) {
		this.system_bill_type = system_bill_type;
	}
	public String getLot_number() {
		return lot_number;
	}
	public void setLot_number(String lot_number) {
		this.lot_number = lot_number;
	}
	public long getTitle_id() {
		return title_id;
	}
	public void setTitle_id(long title_id) {
		this.title_id = title_id;
	}
	public long[] getCatalogs() {
		return catalogs;
	}
	public void setCatalogs(long[] catalogs) {
		this.catalogs = catalogs;
	}
	public long getProduct_line_id() {
		return product_line_id;
	}
	public void setProduct_line_id(long product_line_id) {
		this.product_line_id = product_line_id;
	}
	
}
