package com.cwc.app.beans.log;

import java.util.List;
import java.util.Map;

public class AccessLogBean {

	// 操作类型
	private int operate_type;

	// HTTP请求基本属性
	private String request_uri; // 请求的URI部分
	private String method; // 请求的方法，如GET，POST，PUT等
	private String query_string; // 请求的QueryString部分
	private String user_agent; // 请求的浏览器信息，HEADER：User-Agent
	private String content_type; // 请求的Content-Type
	private String locale; // 请求的locale信息，把HttpServletRequest.getLocale()返回对象转成字符串
	private boolean secure; // 是否通过HTTPS
	private String remote_addr; // 请求地址
	private String local_addr; // 服务器地址
	private int local_port; // 服务器端口

	// 请求的用户属性
	private String account;
	private long adid;
	private long adgid;
	private String employe_name;

	// HTTP请求主体内容
	private List<Map<String, Object>> parameters; // 请求参数，如果是保含上传文件（multipart）的请况，则不计附件部分
	private boolean multipart;


	public int getOperate_type() {
		return operate_type;
	}

	public void setOperate_type(int operate_type) {
		this.operate_type = operate_type;
	}

	public String getRequest_uri() {
		return request_uri;
	}

	public void setRequest_uri(String request_uri) {
		this.request_uri = request_uri;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getQuery_string() {
		return query_string;
	}

	public void setQuery_string(String query_string) {
		this.query_string = query_string;
	}

	public String getUser_agent() {
		return user_agent;
	}

	public void setUser_agent(String user_agent) {
		this.user_agent = user_agent;
	}

	public String getContent_type() {
		return content_type;
	}

	public void setContent_type(String content_type) {
		this.content_type = content_type;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getRemote_addr() {
		return remote_addr;
	}

	public void setRemote_addr(String remote_addr) {
		this.remote_addr = remote_addr;
	}

	public String getLocal_addr() {
		return local_addr;
	}

	public void setLocal_addr(String local_addr) {
		this.local_addr = local_addr;
	}

	public int getLocal_port() {
		return local_port;
	}

	public void setLocal_port(int local_port) {
		this.local_port = local_port;
	}

	public List<Map<String, Object>> getParameters() {
		return parameters;
	}

	public void setParameters(List<Map<String, Object>> parameters) {
		this.parameters = parameters;
	}

	public boolean isSecure() {
		return secure;
	}

	public void setSecure(boolean secure) {
		this.secure = secure;
	}

	public boolean isMultipart() {
		return multipart;
	}

	public void setMultipart(boolean multipart) {
		this.multipart = multipart;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public long getAdid() {
		return adid;
	}

	public void setAdid(long adid) {
		this.adid = adid;
	}

	public long getAdgid() {
		return adgid;
	}

	public void setAdgid(long adgid) {
		this.adgid = adgid;
	}

	public String getEmploye_name() {
		return employe_name;
	}

	public void setEmploye_name(String employe_name) {
		this.employe_name = employe_name;
	}

	public AccessLogBean() {
	}

}
