package com.cwc.app.beans.log;

public class FilterAccessLogBean {
	// 操作类型
	private int operate_type;
	private long adid;
	private long adgid;
	
	public int getOperate_type() {
		return operate_type;
	}
	public void setOperate_type(int operate_type) {
		this.operate_type = operate_type;
	}
	public long getAdid() {
		return adid;
	}
	public void setAdid(long adid) {
		this.adid = adid;
	}
	public long getAdgid() {
		return adgid;
	}
	public void setAdgid(long adgid) {
		this.adgid = adgid;
	}
}
