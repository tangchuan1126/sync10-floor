package com.cwc.app.beans;
import java.io.Serializable;
import com.cwc.db.DBRow;

/**
 * 管理员登陆状态bean 在登录过程中程序设置里面的状态值
 * 管理员是否登录成功，并不是看session是否存在，而是根据session中管理的登录标识是否成功
 * @author TurboShop
 * TurboShop.cn all rights reserved.
 */
public class AdminLoginBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	//账号
	private String account;
	//ID
	private long adid;
	//无效
	private long adgid;
	//登录日期
	private String loginDate;
	
	private long province;
	
	private long city;
	//无效
	private long ps_id;
	
	private String employe_name;
	// 判断登录标记
	private boolean isLogin = false;
	// 判断从正确后台入口登录
	private boolean isLoginRightPath = false;

	private String email;
	// 更灵活的附加数据，尽量不要存放大量数据
	private DBRow attach; 
	
	private DBRow[] titles;
	
	private String storage_name;
	//部门
	private DBRow[] department;
	//仓库
	private DBRow[] warehouse;
	
	private boolean isAdministrator;
	
	private long corporationId;
	
	private long corporationType;
	
	public long getCorporationId() {
		return corporationId;
	}

	public void setCorporationId(long corporationId) {
		this.corporationId = corporationId;
	}

	public long getCorporationType() {
		return corporationType;
	}

	public void setCorporationType(long corporationType) {
		this.corporationType = corporationType;
	}

	public boolean isAdministrator() {
		return isAdministrator;
	}

	public void setAdministrator(boolean isAdministrator) {
		this.isAdministrator = isAdministrator;
	}

	public DBRow[] getDepartment() {
		return department;
	}

	public void setDepartment(DBRow[] department) {
		this.department = department;
	}

	public DBRow[] getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(DBRow[] warehouse) {
		this.warehouse = warehouse;
	}

	public String getAccount() {
		return account;
	}
	
	public void setAccount(String account) {
		this.account = account;
	}

	public long getAdgid() {
		return adgid;
	}

	public void setAdgid(long adgid) {
		this.adgid = adgid;
	}

	public long getAdid() {
		return adid;
	}

	public void setAdid(long adid) {
		this.adid = adid;
	}

	public String getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(String loginDate) {
		this.loginDate = loginDate;
	}

	public void setProvince(long province) {
		this.province = province;
	}

	public long getProvince() {
		return province;
	}

	public void setCity(long city) {
		this.city = city;
	}

	public long getCity() {
		return city;
	}

	public boolean isLogin() {
		return (isLogin);
	}

	public void setIsLogin() {
		this.isLogin = true;
	}

	public void setNotLogin() {
		this.isLogin = false;
	}

	public boolean isLoginRightPath() {
		return (isLoginRightPath);
	}

	public void setIsLoginRightPath() {
		this.isLoginRightPath = true;
	}

	public long getPs_id() {
		return ps_id;
	}

	public void setPs_id(long ps_id) {
		this.ps_id = ps_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public DBRow getAttach() {
		return attach;
	}

	public void setAttach(DBRow attach) {
		this.attach = attach;
	}

	public String getEmploye_name() {
		return employe_name;
	}

	public void setEmploye_name(String employe_name) {
		this.employe_name = employe_name;
	}

	public DBRow[] getTitles() {
		return titles;
	}

	public void setTitles(DBRow[] titles) {
		this.titles = titles;
	}

	public void setStorage_name(String storage_name) {
		this.storage_name = storage_name;
	}

	public String getStorage_name() {
		return storage_name;
	}
	
	
}
