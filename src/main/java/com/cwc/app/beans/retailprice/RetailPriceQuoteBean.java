package com.cwc.app.beans.retailprice;

import com.cwc.db.DBRow;


public class RetailPriceQuoteBean 
{
	public String getPsName() {
		return psName;
	}
	public void setPsName(String psName) {
		this.psName = psName;
	}
	public long getPsId() {
		return psId;
	}
	public void setPsId(long psId) {
		this.psId = psId;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public double getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public long getCaId() {
		return caId;
	}
	public void setCaId(long caId) {
		this.caId = caId;
	}
	public String getCountryArea() {
		return countryArea;
	}
	public void setCountryArea(String countryArea) {
		this.countryArea = countryArea;
	}
	public DBRow[] getProducts() {
		return products;
	}
	public void setProducts(DBRow[] products) {
		this.products = products;
	}
	
	private String psName;
	private long psId;
	private String currency;
	private double exchangeRate;
	private long caId;
	private String countryArea;
	private DBRow products[];
	
}
