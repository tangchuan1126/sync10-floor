package com.cwc.app.api.mgr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.PreCalcuB2BOrderBean;
import com.cwc.app.beans.log.ProductStoreAllocateLogBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.beans.log.ProductStorePhysicalLogBean;
import com.cwc.app.exception.android.ContainerNotFoundException;
import com.cwc.app.exception.bcs.PickUpCountMoreException;
import com.cwc.app.exception.bcs.PutLocationSerialNumberRepeatException;
import com.cwc.app.exception.location.LocationCatalogNotExitsException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorLogMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorLogFilterIFace;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.zj.FloorConfigChangeMgrZJ;
import com.cwc.app.floor.api.zj.FloorLPTypeMgrZJ;
import com.cwc.app.floor.api.zj.FloorLocationMgrZJ;
import com.cwc.app.floor.api.zj.FloorOutListDetailMgrZJ;
import com.cwc.app.floor.api.zj.FloorOutboundOrderMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductCatalogMgrZJ;
import com.cwc.app.floor.api.zl.FloorContainerMgrZYZ;
import com.cwc.app.floor.api.zr.FloorTempContainerMgrZr;
import com.cwc.app.floor.api.zyj.FloorB2BOrderMgrZyj;
import com.cwc.app.key.B2BOrderConfigChangeKey;
import com.cwc.app.key.B2BOrderStatusKey;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.key.ProductStorePhysicalOperationKey;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.RelationBean;
import com.cwc.db.DBRow;

public class ProductStoreMgrAPI{

	static Logger log = Logger.getLogger("ACTION");
	@Autowired
	private FloorLogMgr floorLogMgr;
	@Autowired
	private FloorCatalogMgr floorCatalogMgr;
	@Autowired
	private FloorProductCatalogMgrZJ floorProductCatalogMgrZJ;
	@Autowired
	private FloorProductMgr floorProductMgr;
	@Autowired
	private FloorLPTypeMgrZJ floorLPTypeMgrZJ;
	@Autowired
	private FloorOutListDetailMgrZJ floorOutListDetailMgrZJ;
	@Autowired
	private FloorLocationMgrZJ floorLocationMgrZJ;
	@Autowired
	private FloorB2BOrderMgrZyj floorB2BOrderMgrZyj;
	@Autowired
	private FloorOutboundOrderMgrZJ floorOutboundOrderMgrZJ;
	@Autowired
	private SerialNumberMgrAPI serialNumberMgr;
	@Autowired
	private FloorLogFilterIFace logFilter;
	@Autowired
	private FloorContainerMgrZYZ floorContainerMgrZYZ;
	@Autowired
	private FloorConfigChangeMgrZJ floorConfigChangeMgrZJ;
	@Autowired
	private FloorProductStoreMgr floorProductStoreMgr;
	@Autowired
	private FloorTempContainerMgrZr floorTempContainerMgrZr;
	
	/**
	 * 预保留库存
	 * 基于neo4j图数据库
	 * 保存小拣货单明细
	 * @param ps_id
	 * @param pc_id
	 * @param title_id
	 * @param quantity
	 * @param system_bill_id
	 * @param system_type_id
	 * @param lot_number
	 * @param adminLoggerBean
	 * @param containerTypes
	 * @throws Exception
	 */
	public void reserveProductStore(long ps_id, long pc_id, long title_id,float quantity, long system_bill_id, int system_type_id,String lot_number,AdminLoginBean adminLoggerBean,DBRow[] containerTypes)
		throws Exception 
	{
		//allocate库存
		for (int i = 0; i < containerTypes.length; i++) 
		{
			long container_type_id = containerTypes[i].get("container_type_id",0l);
			int container_type = containerTypes[i].get("container_type",0);
			int con_quantity = containerTypes[i].get("con_quantity",0);
				
			this.reserveProductStoreSub(ps_id, pc_id, title_id, system_bill_id, system_type_id, lot_number, adminLoggerBean, container_type, container_type_id, con_quantity);
		}
	}
	
	/**
	 * 预保留库存
	 * 基于neo4j图数据库
	 * 保存小拣货单明细
	 * @param ps_id
	 * @param pc_id
	 * @param title_id
	 * @param system_bill_id
	 * @param system_type_id
	 * @param lot_number
	 * @param adminLoggerBean
	 * @param container_type
	 * @param container_type_id
	 * @param con_quantity
	 * @throws Exception
	 */
	public void reserveProductStoreSub(long ps_id, long pc_id, long title_id,long system_bill_id, int system_type_id,String lot_number,AdminLoginBean adminLoggerBean,int container_type,long container_type_id,int con_quantity)
		throws Exception
	{
		DBRow[] allocateProductDetails = floorProductStoreMgr.allocateProductStore(ps_id, pc_id, title_id, container_type, container_type_id, con_quantity, lot_number);
			
		for (int j = 0; j < allocateProductDetails.length; j++) 
		{
			long from_con_id = allocateProductDetails[j].get("from_con_id",0l);
			int from_container_type = allocateProductDetails[j].get("from_container_type",0);
			long from_container_type_id = allocateProductDetails[j].get("from_container_type_id",0l);
			long pick_con_id = allocateProductDetails[j].get("pick_con_id",0l);
			long slc_id = allocateProductDetails[j].get("slc_id",0l);
			int pick_count = allocateProductDetails[j].get("pick_count",0);
				
			//记录库存日志
			//this.saveProductStoreLog(ps_id, pc_id, system_type_id, system_bill_id, pick_count*-1, title_id, lot_number, adminLoggerBean,ProductStoreOperationKey.OUT_STORE_TRANSPORT);
			
			//记录保留日志
			//this.saveAllocateProductStoreLog(ps_id, slc_id, pc_id, system_type_id, system_bill_id,pick_count*-1, title_id, lot_number, adminLoggerBean, from_con_id,container_type,container_type_id, pick_con_id);
			
			//生成单据拣货单明细（自身扣货明细）
			this.addOutListDetail(pc_id, slc_id,pick_count, system_bill_id,system_type_id,adminLoggerBean.getAdid(),0, from_container_type, from_container_type_id, from_con_id,container_type,container_type_id, pick_con_id,title_id);
		}
	}	
	
	/**
	 * 拣货单异常，再次预保留
	 * @param ps_id
	 * @param pc_id
	 * @param title_id
	 * @param container_type
	 * @param container_type_id
	 * @param con_quantity
	 * @param system_bill_id
	 * @param system_type_id
	 * @param lot_number
	 * @param adminLoggerBean
	 * @return 返回大拣货单明细
	 * @throws Exception
	 */
	public DBRow[] reserveProductStorePickException(long ps_id,long pc_id,long title_id,int container_type,long container_type_id,int con_quantity,long system_bill_id, int system_type_id,String lot_number,AdminLoginBean adminLoggerBean)
		throws Exception 
	{
			ArrayList<DBRow> newOutListDetail = new ArrayList<DBRow>();
			//allocate库存
			DBRow[] allocateProductDetails = floorProductStoreMgr.allocateProductStore(ps_id, pc_id, title_id, container_type, container_type_id, con_quantity, lot_number);
				
			for (int j = 0; j < allocateProductDetails.length; j++) 
			{
				long from_con_id = allocateProductDetails[j].get("from_con_id",0l);
				int from_container_type = allocateProductDetails[j].get("from_container_type",0);
				long from_container_type_id = allocateProductDetails[j].get("from_container_type_id",0l);
				long pick_con_id = allocateProductDetails[j].get("pick_con_id",0l);
				long slc_id = allocateProductDetails[j].get("slc_id",0l);
				int pick_count = allocateProductDetails[j].get("pick_count",0);
					
				//记录库存日志
				//this.saveProductStoreLog(ps_id, pc_id, system_type_id, system_bill_id, pick_count*-1, title_id, lot_number, adminLoggerBean,ProductStoreOperationKey.OUT_STORE_TRANSPORT);
					
				//记录保留日志
				//this.saveAllocateProductStoreLog(ps_id, slc_id, pc_id, system_type_id, system_bill_id,pick_count*-1, title_id, lot_number, adminLoggerBean, from_con_id,container_type,container_type_id, pick_con_id);
					
				//生成大拣货单明细
				DBRow outStorebillOrderDetail = this.addOutStorebillOrderDetail(pc_id, slc_id,pick_count, system_bill_id,system_type_id,adminLoggerBean.getAdid(),0, from_container_type, from_container_type_id, from_con_id, container_type,container_type_id, pick_con_id);
				newOutListDetail.add(outStorebillOrderDetail);
			}
				
			return newOutListDetail.toArray(new DBRow[0]);
	}
	
	/**
	 * 记录库存日志,mongodb模式
	 * @param pc_id	商品ID
	 * @param system_type_id	系统单据类型
	 * @param system_bill_id	系统单据ID
	 * @param count	数量
	 * @param title_id 所属
	 * @param lot_number	批次
	 * @param adminLoggerBean	操作人bean
	 * @throws Exception
	 */
/*	private void saveProductStoreLog(long ps_id,long pc_id,int system_type_id,long system_bill_id,int count,long title_id,String lot_number,AdminLoginBean adminLoggerBean,int operation)
		throws Exception
	{
			//记录库存日志
			HashMap mangoDBProduct = this.productMongoDB(pc_id);
			long[] catalogs = (long[]) mangoDBProduct.get("catalogs");
			long product_line_id = Long.parseLong(mangoDBProduct.get("product_line_id").toString());
			
			ProductStoreLogBean productStoreLogBean = new ProductStoreLogBean();
			productStoreLogBean.setAccount(adminLoggerBean.getAccount());
			productStoreLogBean.setAdid(adminLoggerBean.getAdid());
			productStoreLogBean.setBill_type(system_type_id);
			productStoreLogBean.setCatalogs(catalogs);
			productStoreLogBean.setLot_number(lot_number);
			productStoreLogBean.setOid(system_bill_id);
			productStoreLogBean.setOperation(operation);
			productStoreLogBean.setPc_id(pc_id);
			productStoreLogBean.setProduct_line_id(product_line_id);
			productStoreLogBean.setQuantity(count);
			productStoreLogBean.setTitle_id(title_id);
			productStoreLogBean.setPs_id(ps_id);
			
			floorLogMgr.availableProductStoreLog(productStoreLogBean,true);
	}*/
	
/*	*//**
	 * 记录aloocateLog
	 * @param ps_id
	 * @param slc_id
	 * @param pc_id
	 * @param system_type_id
	 * @param system_bill_id
	 * @param count
	 * @param title_id
	 * @param lot_number
	 * @param adminLoggerBean
	 * @param from_con_id
	 * @param pick_container_type
	 * @param pick_container_type_id
	 * @param pick_con_id
	 * @throws Exception
	 *//*
	private void saveAllocateProductStoreLog(long ps_id,long slc_id,long pc_id,int system_type_id,long system_bill_id,int count,long title_id,String lot_number,AdminLoginBean adminLoggerBean,long from_con_id,int pick_container_type,long pick_container_type_id,long pick_con_id)
		throws Exception
	{
			//记录Allocate日志
			ProductStoreAllocateLogBean productStoreAllocateLogBean = new ProductStoreAllocateLogBean();
			productStoreAllocateLogBean.setAllocate_adid(adminLoggerBean.getAdid());
			productStoreAllocateLogBean.setAllocate_piece_count(count);
			
			productStoreAllocateLogBean.setAllocate_container_count(1);
			productStoreAllocateLogBean.setCon_id(from_con_id);
			productStoreAllocateLogBean.setContainer_type(pick_container_type);
			productStoreAllocateLogBean.setContainer_type_id(pick_container_type_id);
			productStoreAllocateLogBean.setCon_id(pick_con_id);
			
			productStoreAllocateLogBean.setPc_id(pc_id);
			productStoreAllocateLogBean.setPs_id(ps_id);
			productStoreAllocateLogBean.setSlc_id(slc_id);
			productStoreAllocateLogBean.setSystem_bill_id(system_bill_id);
			productStoreAllocateLogBean.setSystem_bill_type(system_type_id);
			

			productStoreAllocateLogBean.setTitle_id(title_id);
			productStoreAllocateLogBean.setLot_number(lot_number);
			floorLogMgr.allocateProductStoreLog(productStoreAllocateLogBean,true);
	}*/
	
	/**
	 * 记录物理操作日志
	 * @param ps_id
	 * @param slc_id
	 * @param pc_id
	 * @param quantity
	 * @param con_id
	 * @param title_id
	 * @param operationType
	 * @param system_bill_type
	 * @param system_bill_id
	 * @param lot_number
	 * @param machine
	 * @param serial_number
	 * @param adminLoggerBean
	 * @throws Exception
	 */
/*	private void saveProductPhysicalLog(long ps_id,long slc_id,long pc_id,int quantity,long con_id,long title_id,int operationType,int system_bill_type,long system_bill_id,String lot_number,String machine,String serial_number,AdminLoginBean adminLoggerBean)
		throws Exception
	{
	
			HashMap<String,Object> mangoDBProduct = this.productMongoDB(pc_id);
			long[] catalogs = (long[]) mangoDBProduct.get("catalogs");
			long product_line_id = Long.parseLong(mangoDBProduct.get("product_line_id").toString());
			
			ProductStorePhysicalLogBean productPhysicalLogBean = new ProductStorePhysicalLogBean();
			productPhysicalLogBean.setCatalogs(catalogs);
			productPhysicalLogBean.setLot_number(lot_number);
			productPhysicalLogBean.setLp_id(con_id);
			productPhysicalLogBean.setMachine(machine);
			productPhysicalLogBean.setOperation_adid(adminLoggerBean.getAdid());
			productPhysicalLogBean.setOperation_type(operationType);
			productPhysicalLogBean.setPc_id(pc_id);
			productPhysicalLogBean.setProduct_line_id(product_line_id);
			productPhysicalLogBean.setPs_id(ps_id);
			productPhysicalLogBean.setQuantity(quantity);
			productPhysicalLogBean.setSerial_number(serial_number);
			productPhysicalLogBean.setSlc_id(slc_id);
			productPhysicalLogBean.setSystem_bill_id(system_bill_id);
			productPhysicalLogBean.setSystem_bill_type(system_bill_type);
			productPhysicalLogBean.setTitle_id(title_id);
			
			floorLogMgr.physicalProductStoreLog(productPhysicalLogBean,true);
	}
	*/
	
	/**
	 * 
	 * @param pc_id
	 * @param slc_id
	 * @param quantity
	 * @param system_bill_id
	 * @param system_bill_type
	 * @param adid
	 * @param ps_location_id
	 * @param from_container_type
	 * @param from_container_type_id
	 * @param from_con_id
	 * @param pick_container_type
	 * @param pick_container_type_id
	 * @param pick_con_id
	 * @param title_id
	 * @throws Exception
	 */
	public void addOutListDetail(long pc_id,long slc_id,int quantity,long system_bill_id,int system_bill_type,long adid,long ps_location_id,int from_container_type,long from_container_type_id,long from_con_id,int pick_container_type,long pick_container_type_id,long pick_con_id,long title_id)
		throws Exception
	{
			DBRow location = floorLocationMgrZJ.getDetailLocationCatalogById(slc_id);
			long area_id = location.get("slc_area",0l);
			//生成拣货单明细
			DBRow outListDetail = new DBRow();
			outListDetail.add("out_list_pc_id",pc_id);
			outListDetail.add("out_list_slc_id",slc_id);
			outListDetail.add("out_list_area_id",area_id);
			outListDetail.add("pick_up_quantity",quantity);
			outListDetail.add("system_bill_id",system_bill_id);
			outListDetail.add("system_bill_type",system_bill_type);
			outListDetail.add("create_adid",adid);
			outListDetail.add("create_time",DateUtil.NowStr());
			outListDetail.add("ps_location_id",ps_location_id);
			outListDetail.add("from_container_type",from_container_type);
			outListDetail.add("from_container_type_id",from_container_type_id);
			outListDetail.add("from_con_id",from_con_id);
			outListDetail.add("pick_container_type",pick_container_type);
			outListDetail.add("pick_container_type_id",pick_container_type_id);
			outListDetail.add("pick_con_id",pick_con_id);
			outListDetail.add("title_id",title_id);
			
			floorOutListDetailMgrZJ.addOutListDetail(outListDetail);
	}
	
	public DBRow addOutStorebillOrderDetail(long pc_id,long slc_id,int quantity,long system_bill_id,int system_bill_type,long adid,long ps_location_id,int from_container_type,long from_container_type_id,long from_con_id,int pick_container_type,long pick_container_type_id,long pick_con_id)
		throws Exception
	{
			DBRow location = floorLocationMgrZJ.getDetailLocationCatalogById(slc_id);
			long area_id = location.get("slc_area",0l);
			
			DBRow row=new DBRow();
			row.add("out_list_pc_id",pc_id);
			row.add("out_list_slc_id",slc_id);
			row.add("out_list_area_id",area_id);
			row.add("pick_up_quantity",quantity);
			row.add("system_bill_id",system_bill_id);
			row.add("system_bill_type",system_bill_type);
			row.add("create_adid",adid);
			row.add("create_time",DateUtil.NowStr());
			row.add("ps_location_id",ps_location_id);
			row.add("from_container_type",from_container_type);
			row.add("from_container_type_id",from_container_type_id);
			row.add("from_con_id",from_con_id);
			row.add("pick_container_type",pick_container_type);
			row.add("pick_container_type_id",pick_container_type_id);
			row.add("pick_con_id",pick_con_id);
			row.add("out_storebill_order",system_bill_id);
			row.add("need_execut_quantity",quantity);
			
			
			long out_list_detail_id = floorOutboundOrderMgrZJ.addOutStoreBillOrderDetail(row);
			
			return floorOutboundOrderMgrZJ.getDetailOutStoreBillOrderDetailById(out_list_detail_id);
	}
	
	/**
	 * B2B订单仓库预计算
	 * @param cartItems
	 * @param ps_id
	 * @param title_id
	 * @return
	 * @throws Exception
	 */
	public PreCalcuB2BOrderBean calcuOrderB2B(DBRow[] cartItems,long ps_id,long title_id)
		throws Exception
	{
			DBRow clpCount = new DBRow();
			DBRow blpCount = new DBRow();
			DBRow ilpCount = new DBRow();
			DBRow orignailCount = new DBRow();
			DBRow productCount = new DBRow();
			
			DBRow ccNeedBLPCount = new DBRow();
			DBRow ccNeedILPCount = new DBRow();
			DBRow ccNeedOrignailCount = new DBRow();
			
			ArrayList<DBRow> list = new ArrayList<DBRow>();
			int orderStatus = 0;
			int orderNeedCC = B2BOrderConfigChangeKey.NotNeed;//1不需要2可以3不可以
			
			if (cartItems.length>0)
			{
				orderStatus = B2BOrderStatusKey.Enough;
			}
					
			for (int i = 0; i < cartItems.length; i++)
			{
				int orderItemStatus = B2BOrderStatusKey.Enough;
				int orderItemNeedCC = B2BOrderConfigChangeKey.NotNeed;
				
				long pc_id = cartItems[i].get("pc_id",0l);
				long clp_type_id = cartItems[i].get("clp_type",0l);
				int clp_count = cartItems[i].get("clp_count",0);
				
				long blp_type_id = cartItems[i].get("blp_type",0l);
				int blp_count = cartItems[i].get("blp_count",0);
				
				long ilp_type_id = cartItems[i].get("ilp_type",0l);
				int ilp_count = cartItems[i].get("ilp_count",0);
				
				int orignail_count = cartItems[i].get("orignail_count",0);
				
				int product_count = cartItems[i].get("product_count",0);
				
				String lot_number = cartItems[i].getString("lot_number");
				//正常数量
				int clp_store_count = floorProductStoreMgr.availableCount(ps_id, title_id, pc_id,ContainerTypeKey.CLP,clp_type_id, lot_number);	//CLP库存内数量
//				int blp_store_count = floorProductStoreMgr.availableCount(ps_id, title_id, pc_id,ContainerTypeKey.BLP,blp_type_id, lot_number);	//BLP库存内数量
//				int ilp_store_count = floorProductStoreMgr.availableCount(ps_id, title_id, pc_id,ContainerTypeKey.ILP,ilp_type_id, lot_number);	//ILP库存内数量//去掉ILP和BLP
				int orignail_store_count = floorProductStoreMgr.availableCount(ps_id, title_id, pc_id,ContainerTypeKey.Original,0, lot_number);	//库内散装放置的货物
				
				Set<String> groupby= new HashSet<String>();
				groupby.add("pc_id");
				
				DBRow[] productStoreCount = floorProductStoreMgr.productCount(ps_id,0,0,0,new long[]{title_id}, pc_id, lot_number,null,0,groupby); 
				
				int product_store_count;
				if (productStoreCount.length==0)
				{
					product_store_count = 0;
				}
				else
				{
					product_store_count = productStoreCount[0].get("available",0);
				}
				
				
				//ConfigChange
				int cci_type = -1;
				long cci_type_id = 0;
				int cci_count = 0;
				int cci_product_count = 0;
				
				//检查CLP是否够
				if(clp_count+this.getSumLPCount(clpCount,clp_type_id)>clp_store_count) 
				{
					if (orderStatus == B2BOrderStatusKey.Enough)
					{
						orderStatus = B2BOrderStatusKey.ConfigNotEnough;
					}
					
					orderItemStatus = B2BOrderStatusKey.ConfigNotEnough;
					
					if (orderItemStatus == B2BOrderStatusKey.ConfigNotEnough)
					{
						DBRow clp = floorLPTypeMgrZJ.getDetailCLP(clp_type_id);

						cci_type = ContainerTypeKey.CLP;
						cci_count = (clp_count+this.getSumLPCount(clpCount, clp_type_id)-clp_store_count);
						cci_product_count = clp.get("sku_lp_total_piece",0)*cci_count;
						cci_type_id = clp_type_id;
						
						long inner_type_id = clp.get("sku_lp_box_type",0);
						int inner_count = clp.get("sku_lp_total_box",0);
						int inner_need_count = inner_count*cci_count;
						
						if (inner_type_id==ContainerTypeKey.Original)
						{
							if (inner_need_count+this.getSumProductCount(ccNeedOrignailCount,pc_id)+this.getSumProductCount(productCount,pc_id)>orignail_store_count)
							{
								orderNeedCC = B2BOrderConfigChangeKey.CanNot;
								orderItemNeedCC = B2BOrderConfigChangeKey.CanNot;
							}
							else
							{
								if (orderNeedCC==B2BOrderConfigChangeKey.NotNeed)
								{
									orderNeedCC = B2BOrderConfigChangeKey.Can;
								}
								if (orderItemNeedCC==B2BOrderConfigChangeKey.NotNeed)
								{
									orderItemNeedCC = B2BOrderConfigChangeKey.Can;
								}
							}
							
							this.addSumProductCount(ccNeedOrignailCount, pc_id, inner_need_count);
						}
						else
						{
							/*
							if (inner_need_count+this.getSumLPCount(ccNeedBLPCount,inner_type_id)+this.getSumLPCount(blpCount,inner_type_id)>blp_store_count)
							{
								orderNeedCC = B2BOrderConfigChangeKey.Can;
								orderItemNeedCC = B2BOrderConfigChangeKey.Can;
							}
							else
							{
								orderNeedCC = B2BOrderConfigChangeKey.CanNot;
								orderItemNeedCC = B2BOrderConfigChangeKey.CanNot;
							}
							this.addSumLPCount(ccNeedBLPCount,cci_type_id,inner_need_count);
							*///去掉ILP和BLP
						}
					}
				}
				this.addSumLPCount(clpCount, clp_type_id, clp_count);//累加需要的CLP数量
				/*
				//检查BLP是否够
				if (blp_count+this.getSumLPCount(blpCount,blp_type_id)>blp_store_count) 
				{
					if (orderStatus == B2BOrderStatusKey.Enough)
					{
						orderStatus = B2BOrderStatusKey.ConfigNotEnough;
					}
					
					orderItemStatus = B2BOrderStatusKey.ConfigNotEnough;
					
					if (orderItemStatus == B2BOrderStatusKey.ConfigNotEnough)
					{
						DBRow blp = floorLPTypeMgrZJ.getDetailBLPType(blp_type_id);
						
						if (orderItemNeedCC==B2BOrderConfigChangeKey.NotNeed)
						{
							cci_count = blp_count+this.getSumLPCount(blpCount,blp_type_id)- blp_store_count;
							cci_type = ContainerTypeKey.BLP;
							cci_product_count = blp.get("box_total_piece",0)*cci_count;
							cci_type_id = blp_type_id;
							
							int inner_count = blp.get("box_total",0);
							long inner_type_id = blp.get("box_inner_type",0);
							int inner_need_count = inner_count*cci_count;
							
							if (inner_type_id==ContainerTypeKey.Original)
							{
								if (inner_need_count+this.getSumProductCount(ccNeedOrignailCount,pc_id)+this.getSumProductCount(productCount,pc_id)>orignail_store_count)
								{
									if (orderNeedCC==B2BOrderConfigChangeKey.NotNeed)
									{
										orderNeedCC = B2BOrderConfigChangeKey.Can;
									}
									if (orderItemNeedCC==B2BOrderConfigChangeKey.NotNeed)
									{
										orderItemNeedCC = B2BOrderConfigChangeKey.Can;
									}
								}
								else
								{
									orderNeedCC = B2BOrderConfigChangeKey.CanNot;
									orderItemNeedCC = B2BOrderConfigChangeKey.CanNot;
								}
								
								this.addSumProductCount(orignailCount, pc_id,inner_need_count);
							}
							else
							{

								if (inner_need_count+this.getSumLPCount(ccNeedILPCount,inner_type_id)+this.getSumLPCount(ilpCount,inner_type_id)>ilp_store_count)
								{
									orderNeedCC = B2BOrderConfigChangeKey.Can;
									orderItemNeedCC = B2BOrderConfigChangeKey.Can;
								}
								else
								{
									orderNeedCC = B2BOrderConfigChangeKey.CanNot;
									orderItemNeedCC = B2BOrderConfigChangeKey.CanNot;
								}
								this.addSumProductCount(ilpCount,cci_type_id, inner_need_count);
							}
						}
						else
						{
							orderItemNeedCC = B2BOrderConfigChangeKey.CanNot;
							orderNeedCC = B2BOrderConfigChangeKey.CanNot;
						}
					}
				}
				this.addSumLPCount(blpCount, blp_type_id, blp_count);//累加需要的BLP数量
				
				//检查ILP是否够
				if(ilp_count+this.getSumLPCount(ilpCount,ilp_type_id)>ilp_store_count)
				{
					if(orderStatus == B2BOrderStatusKey.Enough)
					{
						orderStatus = B2BOrderStatusKey.ConfigNotEnough;
					}
					
					orderItemStatus = B2BOrderStatusKey.ConfigNotEnough;
					
					if (orderItemNeedCC==B2BOrderConfigChangeKey.NotNeed)
					{
						DBRow ilp = floorLPTypeMgrZJ.getDetailILPType(ilp_type_id);
						
						cci_count = ilp_count+this.getSumLPCount(ilpCount,ilp_type_id) - ilp_store_count;
						cci_type = ContainerTypeKey.ILP;
						cci_product_count = ilp.get("ibt_total",0)*cci_count;
						cci_type_id = ilp_type_id;
						
						int inner_count = ilp.get("ibt_total",0);
						int inner_need_count = inner_count*cci_count;
						
						if (inner_need_count+this.getSumProductCount(ccNeedOrignailCount,pc_id)+this.getSumProductCount(productCount,pc_id)>orignail_store_count)
						{
							if (orderNeedCC==B2BOrderConfigChangeKey.NotNeed)
							{
								orderNeedCC = B2BOrderConfigChangeKey.Can;
							}
							if (orderItemNeedCC==B2BOrderConfigChangeKey.NotNeed)
							{
								orderItemNeedCC = B2BOrderConfigChangeKey.Can;
							}
						}
						else
						{
							orderNeedCC = B2BOrderConfigChangeKey.CanNot;
							orderItemNeedCC = B2BOrderConfigChangeKey.CanNot;
						}
						this.addSumProductCount(ccNeedOrignailCount, pc_id,inner_need_count);
					}
				}
				this.addSumLPCount(ilpCount, ilp_type_id, ilp_count);
				*///去掉ILP和BLP
				//检查使用Original是否够
				if(orignail_count+this.getSumLPCount(orignailCount,pc_id)>orignail_store_count)
				{
					if(orderStatus == B2BOrderStatusKey.Enough)
					{
						orderStatus = B2BOrderStatusKey.ConfigNotEnough;
						orderItemStatus = B2BOrderStatusKey.ConfigNotEnough;
					}
				}
				this.addSumLPCount(orignailCount, pc_id, orignail_count);
				
				
				//判断库存总数是否够
				if (product_count+this.getSumProductCount(productCount, pc_id)>product_store_count) 
				{
					orderStatus = B2BOrderStatusKey.ProductNotEnough;
					orderItemStatus = B2BOrderStatusKey.ProductNotEnough;
					
					orderNeedCC = B2BOrderConfigChangeKey.CanNot;
					orderItemNeedCC = B2BOrderConfigChangeKey.CanNot;
					
				}
				this.addSumProductCount(productCount,pc_id,product_count);
				
				DBRow calcuCartItem = new DBRow();
				calcuCartItem.add("pc_id",pc_id);
				calcuCartItem.add("clp_type_id",clp_type_id);
				calcuCartItem.add("clp_count",clp_count);
				calcuCartItem.add("clp_store_count",clp_store_count);
				calcuCartItem.add("blp_type_id",blp_type_id);
				calcuCartItem.add("blp_count",blp_count);
//				calcuCartItem.add("blp_store_count",blp_store_count);//去掉ILP和BLP
				calcuCartItem.add("ilp_type_id",ilp_type_id);
				calcuCartItem.add("ilp_count",ilp_count);
//				calcuCartItem.add("ilp_store_count",ilp_store_count);//去掉ILP和BLP
				calcuCartItem.add("orignail_count",orignail_count);
				calcuCartItem.add("orignail_store_count",orignail_store_count);
				calcuCartItem.add("product_count",product_count);
				calcuCartItem.add("product_store_count",product_store_count);
				calcuCartItem.add("lot_number",lot_number);
				calcuCartItem.add("order_item_status",orderItemStatus);
				
				calcuCartItem.add("order_item_configChange",orderItemNeedCC);
				calcuCartItem.add("cci_type",cci_type);
				calcuCartItem.add("cci_type_id",cci_type_id);
				calcuCartItem.add("cci_pc_id",pc_id);
				calcuCartItem.add("cci_count",cci_count);
				calcuCartItem.add("cci_product_count",cci_product_count);
				
				
				list.add(calcuCartItem);
			}
			
			PreCalcuB2BOrderBean preCalcuB2BOrderBean = new PreCalcuB2BOrderBean();
			preCalcuB2BOrderBean.setOrderStatus(orderStatus);
			preCalcuB2BOrderBean.setResult(list.toArray(new DBRow[0]));
			preCalcuB2BOrderBean.setPs_id(ps_id);
			preCalcuB2BOrderBean.setOrderConfigChange(orderNeedCC);
			
			return preCalcuB2BOrderBean;
	}
	
	/**
	 * 审核容器，单纯的copy容器树
	 * @param ps_id
	 * @param con_id
	 * @throws Exception
	 */
	public void differentContainerApproveCopyTree(long ps_id,long con_id,long slc_id,long pc_id,long title_id,String lot_number,long time_number)
		throws Exception
	{
			Map<String,Object> locProps = new HashMap<String, Object>();
			locProps.put("slc_id",slc_id);
			locProps.put("pc_id",pc_id);
			locProps.put("title_id",title_id);
			locProps.put("lot_number",lot_number);
			locProps.put("time_number",time_number);
			
			floorProductStoreMgr.copyTree(0,ps_id,con_id,locProps);
	}
	
	/**
	 * 库存审核记录日志
	 * @param ps_id
	 * @param slc_id
	 * @param pc_id
	 * @param quantity
	 * @param con_id
	 * @param title_id
	 * @param physicalOperation
	 * @param operation
	 * @param system_bill_type
	 * @param system_bill_id
	 * @param lot_number
	 * @param machine
	 * @param serial_number
	 * @param adminLoggerBean
	 * @throws Exception
	 */
	public void differentContainerApprove(long ps_id,long slc_id,long pc_id,int quantity,long con_id,long title_id,int physicalOperation,int operation,int system_bill_type,long system_bill_id,String lot_number,String machine,String serial_number,AdminLoginBean adminLoggerBean)
			throws Exception
	{
		// 记录物理操作日志
		//this.saveProductPhysicalLog(ps_id, slc_id, pc_id, quantity, con_id, title_id,physicalOperation,system_bill_type,system_bill_id, lot_number, machine, serial_number, adminLoggerBean);
		//记录库存日志
		//this.saveProductStoreLog(ps_id, pc_id, system_bill_type, system_bill_id, quantity, title_id, lot_number, adminLoggerBean, operation);
	}
	
	public PreCalcuB2BOrderBean calcuB2bOrderOptimalWareHouse(long b2b_oid,long optimal_psid,DBRow[] cartItems)
		throws Exception
	{
		DBRow b2bOrder = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
		long title_id = b2bOrder.get("title_id",0l);
		PreCalcuB2BOrderBean calcuOrderB2B = this.calcuOrderB2B(cartItems,optimal_psid,title_id);
			
		return calcuOrderB2B;
	}
	
	/**
	 * 计算B2B订单非最优仓库
	 */
	public ArrayList<PreCalcuB2BOrderBean> calcuB2BOrderNotOptimalWareHouse(long b2b_oid,long optimal_psid,DBRow[] cartItems)
		throws Exception
	{
			DBRow[] productStoreCatalog = floorCatalogMgr.getProductDevStorageCatalog(null);
			
			DBRow transport = floorB2BOrderMgrZyj.getDetailB2BOrderById(b2b_oid);
			long title_id = transport.get("title_id",0l);
						
			ArrayList<PreCalcuB2BOrderBean> enoughList = new ArrayList<PreCalcuB2BOrderBean>();
			ArrayList<PreCalcuB2BOrderBean> configNotEnoughList = new ArrayList<PreCalcuB2BOrderBean>();
			ArrayList<PreCalcuB2BOrderBean> productNotEnoughList = new ArrayList<PreCalcuB2BOrderBean>();
			ArrayList<PreCalcuB2BOrderBean> productOtherList = new ArrayList<PreCalcuB2BOrderBean>();
						
			for (int i = 0; i < productStoreCatalog.length; i++) 
			{
				long ps_id = productStoreCatalog[i].get("id",0l);
				if(optimal_psid!=ps_id)
				{
					PreCalcuB2BOrderBean calcuOrderB2B = this.calcuOrderB2B(cartItems,ps_id,title_id);			
					if (calcuOrderB2B.getOrderStatus()==B2BOrderStatusKey.Enough)
					{
						enoughList.add(calcuOrderB2B);
					}
					else if (calcuOrderB2B.getOrderStatus()==B2BOrderStatusKey.ConfigNotEnough)
					{
						configNotEnoughList.add(calcuOrderB2B);
					}
					
					else if (calcuOrderB2B.getOrderStatus()==B2BOrderStatusKey.ProductNotEnough)
					{
						productNotEnoughList.add(calcuOrderB2B);
					}
					else
					{
						productOtherList.add(calcuOrderB2B);
					}
				}
			}
			
			ArrayList<PreCalcuB2BOrderBean> returnList = new ArrayList<PreCalcuB2BOrderBean>();
			
			for (int i = 0; i < enoughList.size(); i++) 
			{
				returnList.add(enoughList.get(i));
			}
			
			for (int i = 0; i < configNotEnoughList.size(); i++) 
			{
				returnList.add(configNotEnoughList.get(i));
			}
			
			for (int i = 0; i < productNotEnoughList.size(); i++) 
			{
				returnList.add(productNotEnoughList.get(i));
			}
			
			for (int i = 0; i < productOtherList.size(); i++) 
			{
				returnList.add(productOtherList.get(i));
			}
			
			return returnList;
	}
	
	/**
	 * 添加LP计数
	 * @param countRow
	 * @param lp_type_id
	 * @param count
	 * @throws Exception
	 */
	private void addSumLPCount(DBRow countRow,long lp_type_id,int count)
		throws Exception
	{
		if(countRow.getString(String.valueOf(lp_type_id)).equals(""))
		{
			countRow.add(String.valueOf(lp_type_id),count);
		}
		else
		{
			int currentQuantity = countRow.get(String.valueOf(lp_type_id),0);
			currentQuantity += count;
			countRow.add(String.valueOf(lp_type_id),currentQuantity);
		}
	}
	
	/**
	 * 获得LP计数
	 * @param countRow
	 * @param lp_type_id
	 * @return
	 * @throws Exception
	 */
	private int getSumLPCount(DBRow countRow,long lp_type_id)
		throws Exception
	{
		if (countRow.getString( String.valueOf(lp_type_id)).equals(""))
		{
			return(0);
		}
		else
		{
			return (countRow.get(String.valueOf(lp_type_id),0));
		}
	}
	
	/**
	 * 添加商品计数
	 * @param productCount
	 * @param pc_id
	 * @param count
	 * @throws Exception
	 */
	private void addSumProductCount(DBRow productCount,long pc_id,int count)
		throws Exception
	{
		if(productCount.getString(String.valueOf(pc_id)).equals(""))
		{
			productCount.add(String.valueOf(pc_id),count);
		}
		else
		{
			int currentQuantity = productCount.get(String.valueOf(pc_id),0);
			currentQuantity += count;
			productCount.add(String.valueOf(pc_id),currentQuantity);
		}
	}
	
	/**
	 * 获得商品计数
	 * @param productCount
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	private int getSumProductCount(DBRow productCount,long pc_id)
		throws Exception
	{
		if (productCount.getString( String.valueOf(pc_id)).equals(""))
		{
			return(0);
		}
		else
		{
			return (productCount.get(String.valueOf(pc_id),0));
		}
	}
	
	/*
	 * 为MongoDB构建商品对象（商品分类路径与产品线）
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public HashMap<String,Object> productMongoDB(long pc_id)
		throws Exception
	{
		HashMap<String,Object> result = new HashMap<String,Object>();
		
		DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
		long catalog_id = product.get("catalog_id",0l);
		
		DBRow catalog = floorProductCatalogMgrZJ.getDetailProductCatalog(catalog_id);
		long product_line_id = catalog.get("product_line_id",0l);
		
		result.put("product_line_id",product_line_id);
		
		DBRow[] fatherCatalogs = floorProductCatalogMgrZJ.getFatherTree(catalog_id);
		
		ArrayList<Long> list = new ArrayList<Long>();
		for (int i = 0; i < fatherCatalogs.length; i++) 
		{
			list.add(fatherCatalogs[i].get("id",0l));
		}
		list.add(catalog_id);
		
		long[] catalogs = new long[list.size()];
		for (int i = 0; i < catalogs.length; i++) 
		{
			catalogs[i] = list.get(i).longValue();
		}
		result.put("catalogs",catalogs);
		
		return result;
	}
	
	/*
	 * 拣货
	 * @param ps_location_id
	 * @param quantity
	 * @throws Exception
	 */
	public void pickUpPhysical(long ps_id,long slc_id,long pc_id,float quantity,AdminLoginBean adminLoggerBean,long out_id,String serial_number,String machine,String foType,int from_type,long from_type_id,long from_con_id,int pick_type,long pick_type_id,long pick_con_id,long opation_con_id)
		throws Exception
	{
		try 
		{
			DBRow outListDetail = null;
				
			if(foType.toUpperCase().equals("FIFO"))
			{
				outListDetail = floorOutboundOrderMgrZJ.getOutStorebillOrderDetailByPcOutSlc(pc_id,out_id,slc_id,from_type,from_type_id,from_con_id,pick_type,pick_type_id,pick_con_id);
			}
			else if (foType.toUpperCase().equals("SNFO"))
			{
				outListDetail = floorOutboundOrderMgrZJ.getOutStorebillOrderDetailByPcOutSlcSN(pc_id,out_id,slc_id,serial_number);
			}

			long osod_id = outListDetail.get("out_list_detail_id",0l);
			long title_id = outListDetail.get("title_id",0l);
			
			float need_execut_quantity = outListDetail.get("need_execut_quantity",0f);
			float pickquantiy = quantity;
			if(quantity>need_execut_quantity)
			{
				throw new PickUpCountMoreException();
			}
			HashMap<String,Object> mangoDBProduct = this.productMongoDB(pc_id);
			long[] catalogs = (long[]) mangoDBProduct.get("catalogs");
			long product_line_id = Long.parseLong(mangoDBProduct.get("product_line_id").toString());
			
			floorProductStoreMgr.allocateProductStorePhysical(ps_id, pick_type,pick_type_id, title_id,opation_con_id, pc_id,(int)quantity);
			
			floorOutboundOrderMgrZJ.decOutboundOrderDetailNeedExecutQuantity(osod_id,pickquantiy);//拣货单待执行数量减少
			
			Map<String,Object> searchFor = new HashMap<String, Object>();
			searchFor.put("con_id",opation_con_id);
			RelationBean[] reb = floorProductStoreMgr.searchLocates(ps_id,NodeType.Container, searchFor);
			String lot_number = (String) reb[0].getRelProps().get("lot_number");
			
			
			//this.saveProductPhysicalLog(ps_id, slc_id, pc_id,(int)quantity,pick_con_id, title_id,ProductStorePhysicalOperationKey.PickUp,ProductStoreBillKey.PICK_UP_ORDER,out_id, lot_number, machine, serial_number,adminLoggerBean);
			
			if (!serial_number.equals(""))
			{
				//记录序列号出库
				serialNumberMgr.outSerialProduct(serial_number, pc_id);
			}
		} 
		catch (PickUpCountMoreException e) 
		{
			throw e;
		}
	}
	
	public DBRow[] pickUpException(long ps_id,long slc_id,long pc_id,float error_quantity,long out_id,String serial_number,String machine,String foType,int from_type,long from_type_id,long from_con_id,int pick_type,long pick_type_id,long pick_con_id,long error_con_id,AdminLoginBean adminLoggerBean)
		throws Exception
	{
		DBRow outListDetail = floorOutboundOrderMgrZJ.getOutStorebillOrderDetailByPcOutSlc(pc_id,out_id,slc_id,from_type,from_type_id,from_con_id,pick_type,pick_type_id,pick_con_id);
		long osod_id = outListDetail.get("out_list_detail_id",0l);
		long title_id = outListDetail.get("title_id",0l);
		//拣货单明细待拣数为0，标记差异数
		DBRow paraOutStoreBillOrderDetail = new DBRow();
		paraOutStoreBillOrderDetail.add("need_execut_quantity",0);
		paraOutStoreBillOrderDetail.add("exception_quantity",error_quantity);
		floorOutboundOrderMgrZJ.modOutStoreBillOrderDetail(osod_id, paraOutStoreBillOrderDetail);
			
		return this.reserveProductStorePickException(ps_id, pc_id, title_id,pick_type,pick_type_id,(int)error_quantity,out_id,ProductStoreBillKey.PICK_UP_ORDER,null, adminLoggerBean);
	}
	
	/*
	 * 放货基于图数据库
	 */
	public void putProductToLocation(long ps_id,long pc_id,float quantity,long system_bill_id,int system_bill_type,AdminLoginBean adminLoggerBean,String location,String serial_number,long lp_id,long title_id,String lot_number,String machine,ProductStoreLogBean inProductStoreLogBean)
		throws Exception
	{
			DBRow putLog = logFilter.getProductStorePhysicalLogLayUpBySN(system_bill_id, system_bill_type, serial_number);
			
			if(putLog != null)
			{
				throw new PutLocationSerialNumberRepeatException();
			}
			
			DBRow storeLocationCatalog = floorLocationMgrZJ.getLocationCatalogByPsidAndLocation(ps_id,location);
			if(storeLocationCatalog ==null)
			{
				storeLocationCatalog = new DBRow();
				throw new LocationCatalogNotExitsException();
			}
			long slc_id = storeLocationCatalog.get("slc_id",0l);
			long time_number = System.currentTimeMillis();
			
			//基于图数据库的放货（图数据库节点间构建关系）
			Map<String,Object> containerNode = new HashMap<String, Object>();
			containerNode.put("con_id",lp_id);
			
			Map<String,Object> storageLocationCatalogNode = new HashMap<String, Object>();
			storageLocationCatalogNode.put("slc_id",slc_id);
			
			Map<String,Object> locates = new HashMap<String, Object>();
			locates.put("pc_id",pc_id);
			locates.put("title_id",title_id);
			locates.put("lot_number",lot_number);
			locates.put("time_number",time_number);

			floorProductStoreMgr.addRelation(ps_id,NodeType.Container,NodeType.StorageLocationCatalog,containerNode,storageLocationCatalogNode,locates);
			
			//容器放到位置后更新容器所在位置与仓库
			DBRow containerPara = new DBRow();
			containerPara.add("at_slc_id",slc_id);
			containerPara.add("at_ps_id",ps_id);
			
			floorContainerMgrZYZ.modContainer(lp_id,containerPara);
			
			//记录物理库存日志
			//this.saveProductPhysicalLog(ps_id, slc_id, pc_id,(int)quantity,lp_id, title_id,ProductStorePhysicalOperationKey.LayUp, system_bill_type, system_bill_id, lot_number, machine, serial_number, adminLoggerBean);
			//记录库存日志
			//this.saveProductStoreLog(ps_id, pc_id, system_bill_type, system_bill_id,(int) quantity, title_id, lot_number, adminLoggerBean,inProductStoreLogBean.getOperation());
	}
	
	/*
	 * ConfigChange放货（放货后同一事务内为原单据保留）
	 * @param ps_id
	 * @param pc_id
	 * @param quantity
	 * @param cc_id
	 * @param adminLoggerBean
	 * @param location
	 * @param serial_number
	 * @param lp_id
	 * @param title_id
	 * @param lot_number
	 * @param machine
	 * @param inProductStoreLogBean
	 * @throws Exception
	 */
	public void putProductToLocationForConfigChange(long ps_id,long pc_id,float quantity,long cc_id,AdminLoginBean adminLoggerBean,String location,String serial_number,long lp_id,long title_id,String lot_number,String machine,ProductStoreLogBean inProductStoreLogBean) 
		throws Exception
	{
		DBRow configChange = floorConfigChangeMgrZJ.getDetailConfigChangeByCcid(cc_id);
		long system_bill_id = configChange.get("cc_system_id",0l);
		int system_bill_type = configChange.get("cc_system_type",0);
		
		this.putProductToLocation(ps_id, pc_id, quantity,cc_id,ProductStoreBillKey.CONFIG_CHANGE, adminLoggerBean, location, serial_number, lp_id, title_id, lot_number, machine, inProductStoreLogBean);
		
		if (system_bill_id!=0 && system_bill_type !=0)
		{
			DBRow container = floorContainerMgrZYZ.getDetailContainer(lp_id);
			
			
			this.reserveProductStoreSub(ps_id, pc_id, title_id,system_bill_id,system_bill_type,lot_number, adminLoggerBean,container.get("container_type",0),container.get("type_id",0l),1);
		}
		
	}
	
	/*
	 * 获得容器内装商品数量
	 * @param containter_type
	 * @param container_type_id
	 * @return
	 * @throws Exception
	 */
	public int getLPTypeBuiltInProductCount(int containter_type,long container_type_id)
		throws Exception
	{
			int container_product_piece_count = 0;
			if (containter_type == ContainerTypeKey.CLP) 
			{
				DBRow lpType = floorLPTypeMgrZJ.getDetailCLP(container_type_id);
				container_product_piece_count = lpType.get("sku_lp_total_piece",0);
			}
			/*
			else if (containter_type == ContainerTypeKey.BLP) 
			{
				DBRow lpType = floorLPTypeMgrZJ.getDetailBLPType(container_type_id);
				container_product_piece_count = lpType.get("box_total_piece",0);
			} 
			else if (containter_type == ContainerTypeKey.ILP) 
			{
				DBRow lpType = floorLPTypeMgrZJ.getDetailILPType(container_type_id);
				container_product_piece_count = lpType.get("ibt_total",0);
			}
			*///去掉ILP和BLP
			
			return container_product_piece_count;
	}
	
	/*
	 * 获得容器类型的可用数，类型为原样时，获得的是TLP内的散件数量
	 * @param ps_id
	 * @param pc_id
	 * @param title_id
	 * @param container_type
	 * @param container_type_id
	 * @param lot_number
	 * @return
	 * @throws Exception
	 */
	public int getContainerStoreAvailableCount(long ps_id,long pc_id,long title_id,int container_type,long container_type_id,String lot_number)
			throws Exception
	{
		return floorProductStoreMgr.availableCount(ps_id,title_id,pc_id,container_type,container_type_id,lot_number);
	}
	
	/*
	 * 现在临时库内查询，如没有容器树，再到操作人的所属仓库查找
	 */
	public String containerLoadProduct(long ps_id,long con_id)
			throws Exception
	{
		try 
		{
			String result = floorProductStoreMgr.containerTree(0, con_id, null, 0l, 0l, null, null).toString();
			if (result.equals("{}"))
			{
				result = floorProductStoreMgr.containerTree(ps_id, con_id, null, 0l, 0l, null, null).toString();
				if (result.equals("{}"))
				{
					throw new ContainerNotFoundException();
				}
			}
			
			return result;
		}
		catch (ContainerNotFoundException e) 
		{
			throw e;
		}
	}
	
	/*
	 * 获得库存，根据商品ID进行GroupBy
	 */
	public DBRow[] getProductStoreCount(String ps_id, long slc_area, long slc_id,String titleIds, long pc_id, String lot_number, String catalogs,long product_line)
		throws Exception 
	{
			long[] title_id = new long[0];
			long[] catalog_id  = new long[0];
			
			if (!titleIds.equals("")) 
			{
				String[] titles = titleIds.split(",");
				
				title_id = new long[titles.length];
				for (int i = 0; i < titles.length; i++)
				{
					title_id[i] = Long.parseLong(titles[i]);
				}
			}
			if(!catalogs.equals(""))
			{
				String[] catalog = catalogs.split(",");
				catalog_id = new long[catalog.length];
				for (int i = 0; i < catalog_id.length; i++) 
				{
					catalog_id[i] = Long.parseLong(catalog[i]);
				}
			}
			
			Set<String> groupBy = new HashSet<String>();
			groupBy.add("pc_id");
			
			DBRow[] rows = floorProductStoreMgr.productCount(Long.parseLong(ps_id), slc_area, slc_id,0,title_id, pc_id, lot_number, catalog_id, product_line,groupBy);
			return rows;
	}
	
	/*
	 * 根据商品ID，title_id进行GroupBy
	 */
	public DBRow[] getProductStoreCountGroupByTitle(String ps_id, long slc_area, long slc_id,String titleIds, long pc_id, String lot_number, String catalogs,long product_line)
		throws Exception 
	{
			long[] title_id = new long[0];
			long[] catalog_id  = new long[0];
			
			if (!titleIds.equals("")) 
			{
				String[] titles = titleIds.split(",");
				
				title_id = new long[titles.length];
				for (int i = 0; i < titles.length; i++)
				{
					title_id[i] = Long.parseLong(titles[i]);
				}
			}
			if(!catalogs.equals(""))
			{
				String[] catalog = catalogs.split(",");
				catalog_id = new long[catalog.length];
				for (int i = 0; i < catalog_id.length; i++) 
				{
					catalog_id[i] = Long.parseLong(catalog[i]);
				}
			}
				
			Set<String> groupBy = new HashSet<>();
			groupBy.add("pc_id");
			groupBy.add("title_id");
				
			DBRow[] rows = floorProductStoreMgr.productCount(Long.parseLong(ps_id), slc_area, slc_id,0,title_id, pc_id, lot_number, catalog_id, product_line,groupBy);
			return rows;
	}		
	
	/*
	 * 根据商品ID，title_id，lot_number进行GroupBy
	 */
	public DBRow[] getProductStoreCountGroupByLotNumber(long ps_id,long title_id,long pc_id, String lot_number)
		throws Exception 
	{
			long[] title_ids = new long[]{title_id};
			long[] catalog_id  = new long[0];
			
			Set<String> groupBy = new HashSet<>();
			groupBy.add("pc_id");
			groupBy.add("title_id");
			groupBy.add("lot_number");
					
			DBRow[] rows = floorProductStoreMgr.productCount(ps_id,0,0,0,title_ids,pc_id,lot_number,catalog_id,0,groupBy);
			return rows;
	}
	
	/*
	 * 根据商品ID，title_id，slc_id进行GroupBy
	 */
	public DBRow[] getProductStoreCountGroupBySlc(long ps_id, long title_id,long pc_id, String lot_number) 
		throws Exception 
	{
			long[] title_ids = new long[]{title_id};
			long[] catalog_id  = new long[0];
			
			Set<String> groupBy = new HashSet<>();
			groupBy.add("pc_id");
			groupBy.add("title_id");
			groupBy.add("slc_id");
			
			DBRow[] rows = floorProductStoreMgr.productCount(ps_id,0,0,0,title_ids,pc_id,lot_number,catalog_id,0,groupBy);
			return rows;
	}

	public DBRow[] getProductStoreCountGroupByContainer(long ps_id,long slc_id,int container_type,long title_id,long pc_id, String lot_number)
		throws Exception 
	{
			long[] title_ids = new long[]{title_id};
			long[] catalog_id  = new long[0];
			
			Set<String> groupBy = new HashSet<>();
			groupBy.add("pc_id");
			groupBy.add("title_id");
			groupBy.add("slc_id");
			groupBy.add("con_id");
			
			DBRow[] rows = floorProductStoreMgr.productCount(ps_id,0,0,container_type,title_ids,pc_id,lot_number,catalog_id,0,groupBy);
			return rows;
	}
	
	public DBRow[] getProductStoreContainerCount(long ps_id,long slc_id,long title_id,long pc_id,String lot_number)
		throws Exception
	{
			long[] title_ids = new long[]{title_id};
			long[] catalog_id  = new long[0];
			
			Set<String> groupBy = new HashSet<>();
			groupBy.add("pc_id");
			groupBy.add("con_id");
			
			int clp_count = 0;
			int blp_count = 0;
			int ilp_count = 0;
			int tlp_count = 0;
			
			DBRow[] rows = floorProductStoreMgr.productCount(ps_id,0,0,0,title_ids,pc_id,lot_number,catalog_id,0,groupBy);
			for (int i = 0; i < rows.length; i++) 
			{
				long con_id = rows[i].get("con_id",0l);
				DBRow container = floorContainerMgrZYZ.getDetailContainer(con_id);
				int container_type = container.get("container_type",0);
				
				switch (container_type)
				{
					case 1:
						clp_count++;
						break;
					case 2:
						blp_count++;
						break;
					case 3:
						tlp_count++;
						break;
					case 4:
						ilp_count++;
						break;
				}
				
			}
			ArrayList<DBRow> results = new ArrayList<DBRow>();
			if (clp_count!=0) 
			{
				DBRow clp = new DBRow();
				clp.add("container_count",clp_count);
				clp.add("container_type",ContainerTypeKey.CLP);
				results.add(clp);
			}
			/*
			if (blp_count!=0) 
			{
				DBRow blp = new DBRow();
				blp.add("container_count",blp_count);
				blp.add("container_type",ContainerTypeKey.BLP);
				results.add(blp);
			}
			if (ilp_count!=0) 
			{
				DBRow ilp = new DBRow();
				ilp.add("container_count",ilp_count);
				ilp.add("container_type",ContainerTypeKey.ILP);
				results.add(ilp);
			}
			*///去掉ILP和BLP
			if (tlp_count!=0) 
			{
				DBRow tlp = new DBRow();
				tlp.add("container_count",tlp_count);
				tlp.add("container_type",ContainerTypeKey.TLP);
				results.add(tlp);
			}
			
			return results.toArray(new DBRow[0]);
	}
	
	public boolean hasInnerContainer(long ps_id,long con_id)
		throws Exception
	{
			boolean result;
			JSONObject containerTree = floorProductStoreMgr.containerTree(ps_id, con_id, null, 0l, 0l, null, null);
			if (!containerTree.toString().equals("{}"))
			{
				result = containerTree.has("children");
			}
			else
			{
				result = false;
			}
			
			return result;
	}
	
	public HashMap<String,Object> getContainerInnerTree(long ps_id,long con_id,long pc_id)
		throws Exception
	{
		HashMap<String,Object> result = new HashMap<String,Object>();
		JSONObject containerJson = floorProductStoreMgr.containerTree(ps_id, con_id, null, 0l, 0l, null, null);
		
		//容器内直接放的商品
		DBRow innerProduct = new DBRow();
		JSONArray productJSONArray = containerJson.getJSONArray("products");
		for (int j = 0; j < productJSONArray.length(); j++) 
		{
			JSONObject innerProductJSONObject = productJSONArray.getJSONObject(j);
			long inner_pc_id = innerProductJSONObject.getLong("pc_id");
			
			if (inner_pc_id==pc_id)
			{
				
				int locked = innerProductJSONObject.getInt("locked_quantity");
				int physical = innerProductJSONObject.getInt("quantity");;
				int available = physical - locked;
				
				innerProduct.add("available",available);
				innerProduct.add("physical",physical);
				innerProduct.add("locked",locked);
			}
		}
		result.put("innerProduct",innerProduct);
		
		//容器内放的容器树
		ArrayList<DBRow> innerList = new ArrayList<DBRow>();
		JSONArray containerTree = containerJson.getJSONArray("children");
		for (int i = 0; i < containerTree.length(); i++) 
		{
			JSONObject innerContainerJson = containerTree.getJSONObject(i);
			
			DBRow innerContainer = new DBRow();
			innerContainer.add("con_id",innerContainerJson.getLong("con_id"));
			innerContainer.add("container_type",innerContainerJson.getInt("container_type"));
			innerContainer.add("pc_id",pc_id);
			
			JSONArray innerProductJSONArray = innerContainerJson.getJSONArray("products");
			
			for (int j = 0; j < innerProductJSONArray.length(); j++) 
			{
				JSONObject innerProductJSONObject = innerProductJSONArray.getJSONObject(j);
				long inner_pc_id = innerProductJSONObject.getLong("pc_id");
				
				if (inner_pc_id==pc_id)
				{
					int available = innerProductJSONObject.getInt("total_quantity");
					int locked = innerProductJSONObject.getInt("total_locked_quantity");
					int physical = available - locked;
					
					innerContainer.add("available",available);
					innerContainer.add("physical",physical);
					innerContainer.add("locked",locked);
				}
			}
			innerList.add(innerContainer);
		}
		
		result.put("innerContainerTree",innerList);
		
		return result;
	}

	
	public void copyTempContainerToContainer(long con_id) 
		throws Exception 
	{
			Set<Long> containers = floorProductStoreMgr.containers(0, con_id);
			for (Long containerId : containers) 
			{
				DBRow tempContainer = floorTempContainerMgrZr.getContainer(containerId);
				DBRow conteinr = floorContainerMgrZYZ.getDetailContainer(containerId);
				if (conteinr!=null)
				{
					floorContainerMgrZYZ.deleteContainer(containerId);
				}
				floorContainerMgrZYZ.addContainer(tempContainer);
				floorTempContainerMgrZr.delTempContainer(con_id);
			}
	}
	
	public DBRow[] productCountOfContainer(long ps_id,long con_id,long title_id,long pc_id)
		throws Exception
	{
			return floorProductStoreMgr.productCountOfContainer(ps_id,con_id, title_id, pc_id);
	}
}
