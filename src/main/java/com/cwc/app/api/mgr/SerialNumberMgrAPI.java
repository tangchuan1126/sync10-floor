package com.cwc.app.api.mgr;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.app.exception.serialnumber.NumberIndexOfException;
import com.cwc.app.exception.serialnumber.SerialNumberAlreadyInStoreException;
import com.cwc.app.exception.serialnumber.SerialNumberAlreadyOutStoreException;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.zj.FloorPurchaseMgr;
import com.cwc.app.floor.api.zj.FloorSerialNumberMgrZJ;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;

public class SerialNumberMgrAPI {

	static Logger log = Logger.getLogger("ACTION");
	@Autowired
	private FloorSerialNumberMgrZJ floorSerialNumberMgrZJ;
	@Autowired
	private FloorPurchaseMgr floorPurchaseMgr;
	@Autowired
	private FloorProductMgr floorProductMgr;
	
	/**
	 * 打印后记录序列号（存入数据库）
	 * @param serial_number
	 * @param supplier_id
	 * @param pc_id
	 * @param purchase_id
	 * @throws Exception
	 */
	public void addSerialNumber(String serial_number,long supplier_id,long pc_id,long purchase_id)
		throws Exception 
	{
		try 
		{
			String[] serial_numbers = serial_number.split(",");
			
			for (int i = 0; i < serial_numbers.length; i++)
			{
				DBRow exist = floorSerialNumberMgrZJ.getSerialNumberDetail(Long.parseLong(serial_numbers[i]));
				if(exist!=null)
				{
					throw new NumberIndexOfException();
				}
				DBRow serialNumber = new DBRow();
				serialNumber.add("serial_number", serial_numbers[i]);
				serialNumber.add("supplier_id", supplier_id);
				serialNumber.add("pc_id", pc_id);
				serialNumber.add("purchase_id", purchase_id);
				DBRow purchaseDetail = floorPurchaseMgr.getPurchaseDetailByPcid(purchase_id, pc_id);
				serialNumber.add("purchase_unit_price", purchaseDetail.get("price",0d));
				serialNumber.add("warranty_date", DateUtil.NowStr());
				serialNumber.add("number", serial_numbers[i].substring(10));
				floorSerialNumberMgrZJ.addSerialNumber(serialNumber);
			}
		}
		catch(NumberIndexOfException e)
		{
			throw e;
		}
	}
	
	/**
	 * 生产序列号(只是生成，未存入到数据库)
	 * @param supplier_id
	 * @param pc_id
	 * @param needcount
	 * @return
	 * @throws Exception
	 */
	public DBRow madeSerialNumber(long supplier_id,long pc_id,int needcount)
		throws Exception
	{
		int forcount = 1;//生成序列号数量
		if(needcount>1)
		{
			forcount = 2;
		}
		
		TDate tDate = new TDate();
		String year = String.valueOf(tDate.getIntYear()).substring(2);
		String week;
		if(tDate.getIntWeekYear()<10)
		{
			week = "0"+tDate.getIntWeekYear();
		}
		else
		{
			week = String.valueOf(tDate.getIntWeekYear());
		}
		
		String serial_number_before = supplier_id+year+week;
		
		DBRow max = floorSerialNumberMgrZJ.getNumberLikeSerialNumber(supplier_id, serial_number_before);
		String serial_number;
		StringBuffer serial_numbers = new StringBuffer("");
		int maxnumber = 1;
		for (int i = 0; i < forcount; i++) 
		{
			
			if (!max.getString("maxNumber").trim().equals("")) 
			{
				maxnumber = Integer.parseInt(max.getString("maxNumber"))+1;//存在最大值时需要加1,否则会重复
			}
			maxnumber +=i;//一次生成两个，根据需要再加当前循环次数
			if (maxnumber > 99999) 
			{
				throw new NumberIndexOfException();
			}
			StringBuffer number = new StringBuffer();
			if (String.valueOf(maxnumber).length() != 5) 
			{

				for (int q = 0; q < 5 - String.valueOf(maxnumber).length(); q++) 
				{
					number.append("0");
				}
				number.append(maxnumber);
			}
			serial_number = serial_number_before + number.toString();
			serial_numbers.append(serial_number);
			if(i<forcount-1)
			{
				serial_numbers.append(",");
			}
		}
		
		DBRow product = floorProductMgr.getDetailProductByPcid(pc_id);
		
		DBRow serialnumber = new DBRow();
		serialnumber.add("serial_number",serial_numbers.toString());
		serialnumber.add("p_code",product.getString("p_code"));
		
		return serialnumber;
	}
	
	public DBRow getSerialNumberDetail(long serial_number) 
		throws Exception 
	{
		return floorSerialNumberMgrZJ.getSerialNumberDetail(serial_number);
	}
	
	public void initSerialProduct()
		throws Exception
	{
		DBRow[] products = floorProductMgr.getAllProducts(null);
		
		for (int i = 0; i < products.length; i++) 
		{
			for (int j = 0; j < 10; j++) 
			{
				DBRow serialProduct = new DBRow();
				serialProduct.add("pc_id",products[i].get("pc_id",0l));
				serialProduct.add("serial_number",products[i].getString("p_code")+products[i].get("pc_id",0l)+j+"");
				serialProduct.add("supplier_id",100);
				
				floorSerialNumberMgrZJ.addSerialProduct(serialProduct);
			}
		}
	}
	
	/**
	 * 序列号入库
	 * @param serial_number
	 * @param pc_id
	 * @param slc_id
	 * @throws Exception
	 */
	public void putSerialProduct(String serial_number,long pc_id,long slc_id)
		throws Exception
	{
		try
		{
			DBRow serialProduct = floorSerialNumberMgrZJ.getDetailSerialProduct(serial_number);
			
			if (serialProduct == null)
			{
				serialProduct = new DBRow();
				serialProduct.add("serial_number",serial_number);
				serialProduct.add("pc_id",pc_id);
				serialProduct.add("in_time",DateUtil.NowStr());
				serialProduct.add("at_slc_id",slc_id);
				
				floorSerialNumberMgrZJ.addSerialProduct(serialProduct);
			}
			else
			{
				DBRow serialProductPara = new DBRow();
			
				if(!serialProduct.getString("in_time").equals("")&&serialProduct.getString("out_time").equals(""))
				{
					throw new SerialNumberAlreadyInStoreException();
				}
				
				serialProductPara.add("in_time",DateUtil.NowStr());
				serialProductPara.add("at_slc_id",slc_id);
				serialProductPara.add("out_time",null);
				floorSerialNumberMgrZJ.modSerialProduct(serialProduct.get("serial_product_id",0l), serialProductPara);
			}
		}
		catch (SerialNumberAlreadyInStoreException e) 
		{
			throw e;
		}
	}
	
	/**
	 * 序列号出库
	 * @param serial_number
	 * @param pc_id
	 * @throws Exception
	 */
	public void outSerialProduct(String serial_number,long pc_id)
		throws Exception
	{
		DBRow serialProduct = floorSerialNumberMgrZJ.getDetailSerialProduct(serial_number);
		
		if (serialProduct == null)
		{
			serialProduct = new DBRow();
			serialProduct.add("serial_number",serial_number);
			serialProduct.add("pc_id",pc_id);
			serialProduct.add("out_time",DateUtil.NowStr());
			
			floorSerialNumberMgrZJ.addSerialProduct(serialProduct);
		}
		else
		{
			if(!serialProduct.getString("out_time").equals(""))
			{
				throw new SerialNumberAlreadyOutStoreException();
			}
			
			DBRow serialProductPara = new DBRow();
			serialProductPara.add("out_time",DateUtil.NowStr());
			
			floorSerialNumberMgrZJ.modSerialProduct(serialProduct.get("serial_product_id",0l), serialProductPara);
		}
	}
	
	/**
	 * 
	 * @param pc_id
	 * @param serial_number
	 * @param supplier_id
	 * @throws Exception
	 */
	public void addSerialProduct(long pc_id,String serial_number,long supplier_id)
		throws Exception
	{
		DBRow serialProduct = floorSerialNumberMgrZJ.getDetailSerialProduct(serial_number);
		
		if(serialProduct==null)
		{
			DBRow addSerialProduct = new DBRow();
			addSerialProduct.add("pc_id",pc_id);
			addSerialProduct.add("serial_number",serial_number);
			addSerialProduct.add("supplier_id",supplier_id);
			
			floorSerialNumberMgrZJ.addSerialProduct(addSerialProduct);
		}
	}
	
	public DBRow getDetailSerialProduct(String serial_number)
		throws Exception
	{
		return floorSerialNumberMgrZJ.getDetailSerialProduct(serial_number);
	}
	
	/**
	 * 
	 * @param serial_number
	 * @param slc_id
	 * @throws Exception
	 */
	public void serialProductPositioning(String serial_number,long slc_id)
		throws Exception
	{
		DBRow serialProduct = floorSerialNumberMgrZJ.getDetailSerialProduct(serial_number);
		
		if(serialProduct!=null)
		{
			DBRow para = new DBRow();
			para.add("at_slc_id",slc_id);
			
			floorSerialNumberMgrZJ.modSerialProduct(serialProduct.get("serial_product_id",0l), para);
		}
	}
}
