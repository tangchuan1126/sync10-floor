package com.cwc.app.api.mgr;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.app.floor.api.cc.FloorReportDataInterfaceMgrCc;
import com.cwc.app.floor.api.zj.FloorCheckInMgrZJ;
import com.cwc.app.floor.api.zj.FloorEquipmentMgr;
import com.cwc.app.floor.api.zj.FloorSQLServerMgrZJ;
import com.cwc.app.floor.api.zj.FloorSpaceResourcesRelationMgr;
import com.cwc.app.floor.api.zj.FloorWMSAppointmentMgr;
import com.cwc.app.floor.api.zr.FloorStorageDoorZr;
import com.cwc.app.floor.api.zr.FloorStorageYardControlZr;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.EquipmentTypeKey;
import com.cwc.app.key.SpaceRelationTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class YMSMgrAPI  {
	@Autowired
	private FloorEquipmentMgr floorEquipmentMgr;
	@Autowired
	private FloorSpaceResourcesRelationMgr floorSpaceResourcesRelationMgr;
	@Autowired
	private FloorCheckInMgrZwb floorCheckInMgrZwb;
	@Autowired
	private FloorStorageDoorZr floorStorageDoorZr;
	@Autowired
	private FloorStorageYardControlZr floorStorageYardControlZr;
	@Autowired
	private FloorCheckInMgrZJ floorCheckInMgrZJ;
	@Autowired
	private FloorSQLServerMgrZJ floorSQLServerMgrZJ;
	@Autowired
	private FloorWMSAppointmentMgr floorWMSAppointmentMgr;
	@Autowired
	private FloorReportDataInterfaceMgrCc floorReportDataInterfaceMgrCc;
	
	/**
	 * 添加设备
	 * @author zhanjie
	 * @param check_in_entry_id
	 * @param equipment_type
	 * @param equipment_number
	 * @param equipment_purpose
	 * @param equipment_status
	 * @param seal_delivery
	 * @throws Exception
	 */
	public long addEquipment(long check_in_entry_id,int equipment_type,String equipment_number,int equipment_purpose,int equipment_status,String seal_delivery,int patrol_create,int patrol_lost)
		throws Exception
	{
		DBRow equipment = new DBRow();
		equipment.add("equipment_type",equipment_type);
		equipment.add("check_in_entry_id",check_in_entry_id);
		equipment.add("check_in_time",DateUtil.NowStr());
		equipment.add("equipment_number",equipment_number);
		equipment.add("equipment_purpose",equipment_purpose);
		equipment.add("equipment_status",equipment_status);
		equipment.add("patrol_create",patrol_create);
		equipment.add("patrol_lost",patrol_lost);
		if (seal_delivery!=null&&!seal_delivery.equals(""))
		{
			equipment.add("seal_delivery",seal_delivery);
		}
			
		return floorEquipmentMgr.addEquipment(equipment);
	}
	
	/**
	 * 通过设备ID获得设备详细
	 * @author zhanjie
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailEquipment(long equipment_id)
		throws Exception
	{
		return floorEquipmentMgr.getEquipmentByEquipmentId(equipment_id);
	}
	
	/**
	 * 修改设备
	 * @param equipment_id
	 * @param para
	 * @throws Exception
	 */
	public void updateEquipment(long equipment_id,DBRow para)
		throws Exception
	{
		floorEquipmentMgr.updateEquipment(equipment_id, para);
	}
	
	/**
	 * 删除设备
	 * @param equipment_id
	 * @throws Exception
	 */
	public void delEquipment(long equipment_id)
		throws Exception
	{
		floorEquipmentMgr.delEquipment(equipment_id);
	}
	
	/**
	 * 释放关联(Task,设备)占用的资源,不关心状态
	 * @author zhanjie
	 * @param relation_type
	 * @param relation_id
	 * @param operator_id
	 * @throws Exception
	 */
	public void releaseResourcesByRelation(int relation_type,long relation_id,long operator_id)
		throws Exception
	{
		//记录释放资源日志
		long dlo_id = 0;
		if (relation_type == SpaceRelationTypeKey.Equipment)
		{
			System.out.println("============relation_id:"+relation_id+"============");
			DBRow equipment = floorEquipmentMgr.getEquipmentByEquipmentId(relation_id);
			if(equipment == null){
				return;
			}
			dlo_id = equipment.get("check_in_entry_id",0l);
		}
		else if (relation_type == SpaceRelationTypeKey.Task)
		{
			DBRow task = floorCheckInMgrZwb.selectDetailByDetailId(relation_id);
			dlo_id = task.get("dlo_id",0l);
		}
		
//		this.releaseResourcesLog(operator_id, dlo_id, relation_type, relation_id);
		
		//释放资源
		floorSpaceResourcesRelationMgr.delSpaceResourcesRelation(relation_type, relation_id);
	}
	
	/**
	 * 添加空间资源关系
	 * @author zhanjie
	 * @param resources_type
	 * @param resources_id
	 * @param relation_type
	 * @param relation_id
	 * @param module_type
	 * @param module_id
	 * @param occupy_status
	 * @param adid
	 * @throws Exception
	 */
	public void operationSpaceRelation(int resources_type,long resources_id,int relation_type,long relation_id,int module_type,long module_id,int occupy_status,long adid)
		throws Exception
	{
		//清除之前的空间资源关系
		//this.releaseResourcesByRelation(relation_type, relation_id, adid);
		floorSpaceResourcesRelationMgr.delSpaceResourcesRelation(relation_type, relation_id);
		
		//添加空间资源占用关系
		DBRow spaceResourceRelation = new DBRow();
		spaceResourceRelation.add("resources_type",resources_type);
		spaceResourceRelation.add("resources_id",resources_id);
		spaceResourceRelation.add("relation_type",relation_type);
		spaceResourceRelation.add("relation_id",relation_id);
		spaceResourceRelation.add("module_type",module_type);
		spaceResourceRelation.add("module_id",module_id);
		spaceResourceRelation.add("occupy_status", occupy_status);
		floorSpaceResourcesRelationMgr.addSpaceResourcesRelation(spaceResourceRelation);
		
		//记录资源占用日志
//		this.addUseResourceLog(adid,module_id, resources_type, resources_id, relation_type, relation_id, occupy_status);
	}
	
	/**
	 * 释放资源记录日志
	 * @param operator_id
	 * @param dlo_id
	 * @param relation_type
	 * @param relation_id
	 * @return
	 * @throws Exception
	 */
//	private long releaseResourcesLog(long operator_id,long dlo_id,int relation_type,long relation_id)
//		throws Exception
//	{
//		String relation = "";
//		if (relation_type == SpaceRelationTypeKey.Equipment)
//		{
//			DBRow equipment = floorEquipmentMgr.getEquipmentByEquipmentId(relation_id);
//			relation = "Equipment"+equipment.getString("equipment_number");
//		}
//		else if (relation_type == SpaceRelationTypeKey.Task)
//		{
//			DBRow task = floorCheckInMgrZwb.selectDetailByDetailId(relation_id);
//			relation = "Task"+task.getString("ctn_number");
//		}
//		
//		String resource = "";
//		DBRow spaceResourceRelation = floorSpaceResourcesRelationMgr.getSpaceResorceRelation(relation_type, relation_id);
//		if (spaceResourceRelation !=null)
//		{
//			long resources_id = spaceResourceRelation.get("resources_id",0l);
//			int resources_type = spaceResourceRelation.get("resources_type",0);
//					
//			if (resources_type == OccupyTypeKey.DOOR) 
//			{
//				DBRow door = floorStorageDoorZr.getDoorInfoBySdId(resources_id);
//				resource = "DOOR"+door.getString("doorId");
//			}
//			else if (resources_type==OccupyTypeKey.SPOT)
//			{
//				DBRow yard = floorStorageYardControlZr.getSpotInfoBySdId(resources_id);
//				resource = "SPOT"+yard.getString("yc_no");
//			}
//		}		
//		
//		String note = relation+" Released "+resource;
//		
//    	DBRow row = new DBRow();
//    	row.add("operator_id", operator_id);
//    	row.add("note",note);
//    	row.add("dlo_id", dlo_id);
//    	row.add("log_type", CheckInLogTypeKey.USERESOURCE);
//    	row.add("operator_time",DateUtil.NowStr());
//    	return this.floorCheckInMgrZwb.addCheckInLog(row);
//	}
	
	/**
	 * 记录占用资源日志
	 * @author zhanjie
	 * @param operator_id
	 * @param dlo_id
	 * @param log_type
	 * @param resources_type
	 * @param resources_id
	 * @param relation_type
	 * @param relation_id
	 * @param occpuy_status
	 * @return
	 * @throws Exception
	 */
//	private long addUseResourceLog(long operator_id,long dlo_id,int resources_type,long resources_id,int relation_type,long relation_id,int occupy_status)
//		throws Exception 
//	{
//		String resource = "";
//		if (resources_type == OccupyTypeKey.DOOR) 
//		{
//			DBRow door = floorStorageDoorZr.getDoorInfoBySdId(resources_id);
//			resource = "DOOR"+door.getString("doorId");
//		}
//		else if (resources_type==OccupyTypeKey.SPOT)
//		{
//			DBRow yard = floorStorageYardControlZr.getSpotInfoBySdId(resources_id);
//			resource = "SPOT"+yard.getString("yc_no");
//		}
//		
//		String relation = "";
//		if (relation_type == SpaceRelationTypeKey.Equipment)
//		{
//			DBRow equipment = floorEquipmentMgr.getEquipmentByEquipmentId(relation_id);
//			relation = "Equipment"+equipment.getString("equipment_number");
//		}
//		else if (relation_type == SpaceRelationTypeKey.Task)
//		{
//			DBRow task = floorCheckInMgrZwb.selectDetailByDetailId(relation_id);
//			relation = "Task"+task.getString("ctn_number");
//		}
//		
//		String note = relation+" "+new OccupyStatusTypeKey().getOccupyStatusValue(occupy_status)+" "+resource;
//		
//    	DBRow row = new DBRow();
//    	row.add("operator_id", operator_id);
//    	row.add("note",note);
//    	row.add("dlo_id", dlo_id);
//    	row.add("log_type", CheckInLogTypeKey.USERESOURCE);
//    	row.add("operator_time",DateUtil.NowStr());
//    	return this.floorCheckInMgrZwb.addCheckInLog(row);
//    }
	
	/**
	 * 根据EntryId获得设备使用的资源
	 * @author zhanjie
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getEntryEquipmentUseResource(long entry_id ,int equipment_status)
		throws Exception
	{
		return floorSpaceResourcesRelationMgr.getEntryEquipmentUseResource(entry_id,equipment_status);
	}
	
	/**
	 * 基于关联
	 * @author zhanjie
	 * @param relation_type
	 * @param relation_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getSpaceResorceRelation(int relation_type,long relation_id)
		throws Exception
	{
		return floorSpaceResourcesRelationMgr.getSpaceResorceRelation(relation_type, relation_id);
	}
	
	/**
	 * checkOut专用释放资源，释放设备占用的资源，同时释放该设备对应的任务的资源
	 * @author zhanjie
	 * @param equipment_id
	 * @param adid
	 * @throws Exception
	 */
	public void checkOutReleaseResources(long out_entry_id,long equipment_id,long adid,String ...checkoutTimes)
		throws Exception
	{
		//checkout设备
		DBRow equipmentPara = new DBRow();
		String checkoutTime = DateUtil.NowStr();
		if (checkoutTimes != null && checkoutTimes.length > 0 && checkoutTimes[0] != null && !"".equals(checkoutTimes[0])) {
			checkoutTime = checkoutTimes[0];
		}
		equipmentPara.add("check_out_entry_id",out_entry_id);
		equipmentPara.add("check_out_time",checkoutTime);
		equipmentPara.add("equipment_status",CheckInMainDocumentsStatusTypeKey.LEFT);
		floorEquipmentMgr.updateEquipment(equipment_id, equipmentPara);
		
		//释放设备占用的资源
		this.releaseResourcesByRelation(SpaceRelationTypeKey.Equipment, equipment_id,adid);		
		//获得与设备关联的任务
		DBRow[] tasks = floorCheckInMgrZwb.getTaskByEquimentId(equipment_id,new int[0]);
		//释放任务占用的资源
		for (int i = 0; i < tasks.length; i++) 
		{
			long dlo_detail_id = tasks[i].get("dlo_detail_id",0l);
			this.releaseResourcesByRelation(SpaceRelationTypeKey.Task,dlo_detail_id,adid);
		}
	}
	
	/**
	 * 根据设备ID获得与设备关联的任务
	 * @author zhanjie
	 * @param equipment_id
	 * @param number_status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTaskByEquimentId(long equipment_id,int[] number_status)
		throws Exception
	{
		return floorCheckInMgrZwb.getTaskByEquimentId(equipment_id, number_status);
	}
	
	/**
	 * 输入设备号，返回最后一次checkin进来的设备的Entry对应的所有设备
	 * @author zhanjie
	 * @param equipment_number
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getInYardEquipmentByEquipmentNumberLast(long ps_id, String equipment_number,int equipment_status)
		throws Exception
	{
		DBRow lastEquipment = floorEquipmentMgr.getInYardEquipmentByEquipmentNumberLast(ps_id, equipment_number);
		DBRow[] result = null;
		if(lastEquipment!=null){
			long entry_id = lastEquipment.get("check_in_entry_id",0l);
			
			result = floorSpaceResourcesRelationMgr.getEntryEquipmentUseResource(entry_id,equipment_status);
		}
		return result;
	}
	
	/**
	 * CheckOutEntryId关联的设备与对应的资源
	 * @author zhanjie
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCheckOutEntryUseResource(long entry_id)
		throws Exception
	{
		return floorSpaceResourcesRelationMgr.getCheckOutEntryUseResource(entry_id);
	}
	
	/**
	 * 判断一个资源对于一个Module是否可用
	 * @param module_type
	 * @param module_id
	 * @param resource_type
	 * @param resource_id
	 * @return
	 * @throws Exception
	 */
	public boolean resourceJudgeCanUse (int module_type,long module_id,int resource_type,long resource_id)
		throws Exception
	{
		DBRow[] moduleUseResource = floorSpaceResourcesRelationMgr.getMoedUseResource(module_type, module_id, resource_type, resource_id);
		if (moduleUseResource.length>0)
		{
			return true;
		}
		else
		{
			DBRow[] resourceUseRelation = floorSpaceResourcesRelationMgr.getSpaceResourceUse(resource_type, resource_id);
			
			if (resourceUseRelation.length>0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	
	/**
	 * 获取在某个设备下面的某个资源的下的Task
	 * @param entry_id
	 * @param resources_id
	 * @param resources_type
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月29日
	 */
 	public DBRow[] getRelationByResouceAndEntryIdAndEquipmentId(long entry_id , long resources_id , int resources_type , long equipment_id,int relation_type) throws Exception{
 		return floorSpaceResourcesRelationMgr.getRelationByResouceAndEntryIdAndEquipmentId(entry_id, resources_id, resources_type, equipment_id,relation_type);
 	}
 	

 	/**
 	 * @param srr_id
 	 * @param updateRow
 	 * @throws Exception
 	 * @author zhangrui
 	 * @Date   2014年11月29日
 	 */
	public void updateSpaceResourcesRelation(long srr_id , DBRow updateRow) throws Exception{
		  floorSpaceResourcesRelationMgr.updateSpaceResourcesRelation(srr_id, updateRow);
	}
	
	/**
	 * 通过EntryId 获取车头
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月29日
	 */
	public DBRow getEntryTractor(long entry_id) throws Exception{
		return floorEquipmentMgr.getEntryTractor(entry_id);
	}
	
	/**
	 * 基于区域显示占用的车位的数量
	 * @author zhanjie
	 * @param ps_id
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] occupySpotForAreaView(long ps_id,long area_id)
		throws Exception
	{
		DBRow[] spotArea = floorSpaceResourcesRelationMgr.occupySpotForAreaView(ps_id, area_id);
		for (int i = 0; i < spotArea.length; i++) {
				spotArea[i].add("last_time",DateUtil.showLocalparseDateToNoYear24Hours(spotArea[i].get("last_time",""), ps_id) ); 

		}
		return spotArea;
	}
	
	/**
	 * 基于区域显示占用的门的数量
	 * @author zhanjie
	 * @param ps_id
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] occupyDoorForAreaView(long ps_id,long area_id)
		throws Exception
	{
		DBRow[] doorArea = floorSpaceResourcesRelationMgr.occupyDoorForAreaView(ps_id, area_id);
		for (int i = 0; i < doorArea.length; i++) {
				doorArea[i].add("last_time",DateUtil.showLocalparseDateToNoYear24Hours(doorArea[i].get("last_time",""), ps_id) ); 
		}
		return doorArea;
	}
	
	/**
	 * 门活动报表
	 * @param ps_id
	 * @param start_date
	 * @return
	 * @throws Exception
	 */
	public DBRow[] gateActivityReport(long ps_id,String start_date,String end_date)
		throws Exception
	{
		String start = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(start_date, ps_id));
		String end = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(end_date,ps_id));
		return floorCheckInMgrZJ.gateActivityReport(ps_id,start,end);
	}
	
	/**
	 * 任务活动报表
	 * @param ps_id
	 * @param start_date
	 * @return
	 * @throws Exception
	 */
	public DBRow[] taskActivityReport(long ps_id,String start_date,String end_date)
		throws Exception
	{
		String start = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(start_date, ps_id));
		String end = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(end_date,ps_id));
		DBRow[] tasks = floorCheckInMgrZJ.taskActivityReport(ps_id,start,end);
		for (int i = 0; i < tasks.length; i++) {
			if(tasks[i].get("labor", "").equals("")){
				DBRow labors = this.floorCheckInMgrZJ.findlaborsByTaskId(tasks[i].get("dlo_detail_id",0l));
				tasks[i].add("labor", labors.get("labor",""));
			}
		}
		return tasks;
	}
	
	/**
	 * 设备报表
	 * @author zhanjie
	 * @param ps_id
	 * @param start_date
	 * @return
	 * @throws Exception
	 */
	public DBRow[] equipmentReport(long ps_id,String start_date,String end_date)
		throws Exception
	{
		String start = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(start_date, ps_id));
		String end = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(end_date,ps_id));
		DBRow[] result = floorCheckInMgrZJ.equipmentReport(ps_id,start,end);
		
		EquipmentTypeKey etk = new EquipmentTypeKey();
		for (int i = 0; i < result.length; i++) 
		{
			result[i].add("equipment_type",etk.getEnlishEquipmentTypeValue(result[i].get("equipment_type",0)));
		}
		
		return result;
	}
	
	/**
	 * 获得任务开始时间在时间范围内的Task
	 * @author zhanjie
	 * @param start_date
	 * @param end_date
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTasksByStartDate(String start_date,String end_date)
		throws Exception
	{
		return floorCheckInMgrZJ.getTasksByStartDate(start_date, end_date);
	}
	
	
	/**
	 * 释放设备占用的资源，同时释放该设备对应的任务的资源
	 * @param out_entry_id
	 * @param equipment_id
	 * @param adid
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月16日 下午7:47:40
	 */
	public void checkInWindowEquipmentTaskReleaseResources(long out_entry_id,long equipment_id,long adid)
		throws Exception
	{
		//释放设备占用的资源
		this.releaseResourcesByRelation(SpaceRelationTypeKey.Equipment, equipment_id,adid);		
		//获得与设备关联的任务
		DBRow[] tasks = floorCheckInMgrZwb.getTaskByEquimentId(equipment_id,new int[0]);
		//释放任务占用的资源
		for (int i = 0; i < tasks.length; i++) 
		{
			long dlo_detail_id = tasks[i].get("dlo_detail_id",0l);
			this.releaseResourcesByRelation(SpaceRelationTypeKey.Task,dlo_detail_id,adid);
		}
	}

	
	/**
	 * 获得当前小时需要处理的仓库
	 * @author yuehaibo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStorageCatalogForQuartz(int ps_type)
		throws Exception
	{
			return floorWMSAppointmentMgr.getProductStorageCatalogForQuartz(ps_type);
	}
	
	/**
	 * 抓取出库的Appointment
	 * @author zhanjie
	 * @param ps_id
	 * @param start_date
	 * @param end_date
	 * @param companyIDs
	 * @throws Exception
	 */
	public void getOutboundAppointment(long ps_id,String start_date,String end_date)
		throws Exception
	{
		String companyIDs = this.wmsStorage(ps_id);
		if(companyIDs != null && !companyIDs.equals("")){
			
			DBRow[] appointmentOutbound = floorSQLServerMgrZJ.getOutboundAppointment(start_date, end_date, companyIDs);
			
			for (int i = 0; i < appointmentOutbound.length; i++) 
			{				
				appointmentOutbound[i].add("ps_id",ps_id);
				appointmentOutbound[i].add("in_out",1);
				appointmentOutbound[i].add("appointment_time",DateUtil.utcTime(appointmentOutbound[i].getString("appointment_time"),ps_id));
				appointmentOutbound[i].add("line_pallets",MoneyUtil.round(appointmentOutbound[i].getString("line_pallets"),0));
				appointmentOutbound[i].add("create_time",DateUtil.NowStr());
				
				floorWMSAppointmentMgr.addWMSAppointment(appointmentOutbound[i]);
			}
			
			String startUTC = DateUtil.showUTCTime(DateUtil.createDateTime(start_date+" 00:00:00"), ps_id);
			String endUTC = DateUtil.showUTCTime(DateUtil.createDateTime(end_date+" 23:59:59"), ps_id);
			
			DBRow[] noAppointmentTodayOutbounds = floorWMSAppointmentMgr.noTodayOutboundAppointment(ps_id,startUTC,endUTC);
			for (int i = 0; i < noAppointmentTodayOutbounds.length; i++) 
			{
				DBRow[] secordAppointmentOutbounds = floorSQLServerMgrZJ.getOutboundAppointment(noAppointmentTodayOutbounds[i].get("number_type",0),noAppointmentTodayOutbounds[i].getString("number"),companyIDs);
				for (int j = 0; j < secordAppointmentOutbounds.length; j++) 
				{
					secordAppointmentOutbounds[j].add("ps_id",ps_id);
					secordAppointmentOutbounds[j].add("in_out",1);
					secordAppointmentOutbounds[j].add("appointment_time",DateUtil.utcTime(secordAppointmentOutbounds[j].getString("appointment_time"),ps_id));
					secordAppointmentOutbounds[j].add("line_pallets",MoneyUtil.round(secordAppointmentOutbounds[j].getString("line_pallets"),0));
					secordAppointmentOutbounds[j].add("create_time",DateUtil.NowStr());	
					floorWMSAppointmentMgr.addWMSAppointment(secordAppointmentOutbounds[j]);
				}
			}
		}
	}
	
	/**
	 * 抓取出库的Appointment
	 * @author zhanjie
	 * @param ps_id
	 * @param start_date
	 * @param end_date
	 * @param companyIDs
	 * @throws Exception
	 */
	public void getInboundAppointment(long ps_id,String start_date,String end_date)
		throws Exception
	{
		String companyIDs = this.wmsStorage(ps_id);

		if(companyIDs != null && !companyIDs.equals("")){
			
		
			DBRow[] appointmentInbound = floorSQLServerMgrZJ.getInboundAppointment(start_date, end_date, companyIDs);
			for (int i = 0; i < appointmentInbound.length; i++) 
			{				
				appointmentInbound[i].add("ps_id",ps_id);
				appointmentInbound[i].add("in_out",2);
				appointmentInbound[i].add("appointment_time",DateUtil.utcTime(appointmentInbound[i].getString("appointment_time"),ps_id));
				appointmentInbound[i].add("create_time",DateUtil.NowStr());
				floorWMSAppointmentMgr.addWMSAppointment(appointmentInbound[i]);
			}
			
			String startUTC = DateUtil.showUTCTime(DateUtil.createDateTime(start_date+" 00:00:00"), ps_id);
			String endUTC = DateUtil.showUTCTime(DateUtil.createDateTime(end_date+" 23:59:59"), ps_id);

			
			DBRow[] noAppointmentTodayInbounds = floorWMSAppointmentMgr.noTodayInboundAppointment(ps_id,startUTC,endUTC);
			for (int i = 0; i < noAppointmentTodayInbounds.length; i++) 
			{
				DBRow[] secordAppointmentInbounds = floorSQLServerMgrZJ.getInboundAppointment(noAppointmentTodayInbounds[i].getString("receipt_no"), companyIDs);
				
				for (int j = 0; j < secordAppointmentInbounds.length; j++) 
				{
					secordAppointmentInbounds[j].add("ps_id",ps_id);
					secordAppointmentInbounds[j].add("in_out",2);
					secordAppointmentInbounds[j].add("appointment_time",DateUtil.utcTime(secordAppointmentInbounds[j].getString("appointment_time"),ps_id));
					secordAppointmentInbounds[j].add("create_time",DateUtil.NowStr());		
					floorWMSAppointmentMgr.addWMSAppointment(secordAppointmentInbounds[j]);
				}
			}
		}
	}
	
	private String wmsStorage (long ps_id)
		throws Exception
	{
		DBRow[] searchWMSStorage = floorCheckInMgrZJ.findWmsStorage(ps_id, 2);
		StringBuffer companyIDs = new StringBuffer();
		for (int i = 0; i < searchWMSStorage.length; i++) 
		{
			companyIDs.append("'");
			companyIDs.append(searchWMSStorage[i].getString("company_id"));
			companyIDs.append("'");
			if (i<searchWMSStorage.length-1)
			{
				companyIDs.append(",");
			}
		}
		
		return companyIDs.toString();
	}
	
	/**
	 * 导回WMS一段时间的预约了的MaterBOL和Order独立预约了的预约时间
	 * @author zhanjie
	 * @param ps_id
	 * @param start_date
	 * @param end_date
	 * @throws Exception
	 */
	public void getAppointment(long ps_id,String start_date,String end_date)
		throws Exception
	{
		getAppointment(ps_id, start_date, end_date, true);
	}
	
	/**
	 * 导回WMS一段时间的预约了的MaterBOL和Order独立预约了的预约时间
	 * @author yuehaibo
	 * @param ps_id
	 * @param start_date
	 * @param end_date
	 * @param del 是否先删除旧数据
	 * @throws Exception
	 */
	public void getAppointment(long ps_id,String start_date,String end_date, boolean del)
		throws Exception
	{
		if(del){
			// 删除start_date到end_date的预约时间
			floorWMSAppointmentMgr.delWMSAppointmentByPsid(ps_id, start_date, end_date);
		}
		
		// by yuehaibo 添加start_date到end_date的预约时间
		this.getOutboundAppointment(ps_id, start_date, end_date);

		// by yuehaibo 添加start_date到end_date的预约时间
		this.getInboundAppointment(ps_id, start_date, end_date);
	}
	
	/**
	 * 将一个Load的Customer合并，用逗号分隔
	 * @param loadNumber
	 * @param companyID
	 * @return
	 * @throws Exception
	 */
	public String concatMasterBOLCustomerID(String loadNumber,String companyID)
		throws Exception
	{
		DBRow[] rows = floorSQLServerMgrZJ.getMasterBOLsByLoadCompany(loadNumber, companyID);
		
		StringBuffer customers = new StringBuffer();
		for (int i = 0; i < rows.length; i++) 
		{
			customers.append(rows[i].getString("CustomerID"));
			
			if (i<rows.length-1)
			{
				customers.append(",");
			}
		}
		
		return customers.toString();
	}
	
	public DBRow[] scordOutBoundReport(long ps_id,String start_time,String end_time,PageCtrl pc)
		throws Exception
	{
		String start = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(start_time, ps_id));
		String end = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(end_time,ps_id));
		
		return floorCheckInMgrZJ.scordOutBoundReport(ps_id,start,end, pc);
	}
	
	public DBRow[] scordOutBoundReport(long ps_id, String start_time,String end_time) throws Exception {
		String start = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat, DateUtil.utcTime(start_time, ps_id));
		String end = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat, DateUtil.utcTime(end_time, ps_id));
		return floorCheckInMgrZJ.scordOutBoundReport(ps_id, start, end);
	}
	
	public DBRow[] scordInBoundReport(long ps_id, String start_time,String end_time, PageCtrl pc) throws Exception {
		String start = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(start_time, ps_id));
		String end = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(end_time, ps_id));
		return floorCheckInMgrZJ.scordInBoundReport(ps_id, start, end, pc);
	}
	
	public DBRow[] scordInBoundReport(long ps_id, String start_time,String end_time) throws Exception {
		String start = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat, DateUtil.utcTime(start_time, ps_id));
		String end = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(end_time, ps_id));
		return floorCheckInMgrZJ.scordInBoundReport(ps_id, start, end);
	}
	
	public DBRow[] scheduleInBound(long ps_id,String start_time,String end_time,PageCtrl pc)
		throws Exception
	{
		String start = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(start_time, ps_id));
		String end = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(end_time,ps_id));
		
		return floorCheckInMgrZJ.scheduleInboundTomorrow(ps_id, start,end, pc);
	}
	
	
	
	public DBRow[] scheduleOutBound(long ps_id,String start_time,String end_time,PageCtrl pc)
		throws Exception
	{
		String start = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(start_time, ps_id));
		String end = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,DateUtil.utcTime(end_time,ps_id));
		
		return floorCheckInMgrZJ.scheduleOutboundTomorrow(ps_id, start,end, pc);
	}
	public DBRow[] receiveScheduleInbound(long ps_id,String start_time,String end_time)throws Exception	{
		//查wms库
		DBRow[] result = null;
		DBRow[] rows = floorReportDataInterfaceMgrCc.getCompanyIdByPsId(ps_id);
		if(rows!=null && rows.length>0){
			String[] companyIds = new String[rows.length];
			for (int i=0; i<rows.length; i++) {
				companyIds[i] = rows[i].getString("company_id");
			}
			result = floorReportDataInterfaceMgrCc.receiveScheduleInbound(companyIds, start_time,end_time);
		}
		return result;
	}
	
	
	public void setFloorEquipmentMgr(FloorEquipmentMgr floorEquipmentMgr) {
		this.floorEquipmentMgr = floorEquipmentMgr;
	}

	public void setFloorSpaceResourcesRelationMgr(
			FloorSpaceResourcesRelationMgr floorSpaceResourcesRelationMgr) {
		this.floorSpaceResourcesRelationMgr = floorSpaceResourcesRelationMgr;
	}

	public void setFloorCheckInMgrZwb(FloorCheckInMgrZwb floorCheckInMgrZwb) {
		this.floorCheckInMgrZwb = floorCheckInMgrZwb;
	}

	public void setFloorStorageDoorZr(FloorStorageDoorZr floorStorageDoorZr) {
		this.floorStorageDoorZr = floorStorageDoorZr;
	}

	public void setFloorStorageYardControlZr(
			FloorStorageYardControlZr floorStorageYardControlZr) {
		this.floorStorageYardControlZr = floorStorageYardControlZr;
	}

	public void setFloorCheckInMgrZJ(FloorCheckInMgrZJ floorCheckInMgrZJ) {
		this.floorCheckInMgrZJ = floorCheckInMgrZJ;
	}

	public void setFloorSQLServerMgrZJ(FloorSQLServerMgrZJ floorSQLServerMgrZJ) {
		this.floorSQLServerMgrZJ = floorSQLServerMgrZJ;
	}

	public void setFloorWMSAppointmentMgr(
			FloorWMSAppointmentMgr floorWMSAppointmentMgr) {
		this.floorWMSAppointmentMgr = floorWMSAppointmentMgr;
	}

	public void setFloorReportDataInterfaceMgrCc(
			FloorReportDataInterfaceMgrCc floorReportDataInterfaceMgrCc) {
		this.floorReportDataInterfaceMgrCc = floorReportDataInterfaceMgrCc;
	}
}
