package com.cwc.app.api.mgr;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.app.exception.shipTo.CountryNotExistException;
import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.fa.FloorCustomerMgr;
import com.cwc.app.floor.api.zj.FloorCountryMgrZJ;
import com.cwc.app.floor.api.zj.FloorShipToMgrZJ;
import com.cwc.app.floor.api.zr.FloorClpTypeMgrZr;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class BasicDataMgrAPI {

	static Logger log = Logger.getLogger("ACTION");
	@Autowired
	private FloorClpTypeMgrZr floorClpTypeMgrZr;
	@Autowired
	private FloorCustomerMgr floorCustomerMgr;
	@Autowired
	private FloorCatalogMgr floorCatalogMgr;
	@Autowired
	private FloorCountryMgrZJ floorCountryMgrZJ;
	@Autowired
	private FloorShipToMgrZJ floorShipToMgrZJ;
	
	
	//根据type id 查询 license_plate_tpye and container_type
	public Map findContainerTypeAndCLpType(List<Integer> type_id) throws Exception 
	{
		try
		{
			Map<String, Object> type = new HashMap();
			for(int i= 0;i<type_id.size();i++)
			{
				DBRow ClpTypevalue = floorClpTypeMgrZr.findContainerTypeAndClpType(type_id.get(i));
				if(ClpTypevalue!=null)
				{
					String typeid = ClpTypevalue.get("lpt_id","");
					type.put(typeid,ClpTypevalue);
				}
			}				
	 		return type;
		}catch (Exception e) {
			throw new Exception(e);
 		}
	}
	
	/**
	 * customer模块 -> 搜索 列表 共用方法
	 * @param dbRow
	 * @param pc
	 * @return
	 * @throws Exception
	 * @author Yxy
	 * @date: 2015年4月10日 下午6:23:32
	 */
	public DBRow[] getAllCustomerList(DBRow dbRow, PageCtrl pc)throws Exception {
		
		try
		{
			DBRow[] allCustomerList = floorCustomerMgr.getAllCustomerList(dbRow, pc);
			
			return allCustomerList;
			
			
		}catch (Exception e) {
			throw new Exception(e);
 		}
		
	}
	
	
//	public long addStorageCatalog(
//			String storage_title,
//			String shipToAddress,
//			String shipToCity,
//			long pro_id,
//			long nation_id,
//			String deliver_zip_code,
//			long ship_to_id,
//			int storage_type
//			) throws Exception
//	{
//		
//		
//		DBRow isExistProductStorageCatalog = floorCatalogMgr.getProductStorageDetailCatalogByTitle(storage_title);
//		if (isExistProductStorageCatalog == null)
//		{
//			DBRow shipTo = floorShipToMgrZJ.getDetailShipToById(ship_to_id);
//			String ship_to_name = shipTo == null ? "" : shipTo.getString("ship_to_name");
//			
//			DBRow country = floorCountryMgrZJ.getDetailCountryByCcid(nation_id);
//			if (country == null) 
//			{
//				throw new CountryNotExistException();
//			}
//			
//			DBRow province = floorCountryMgrZJ.getDetailProByProId(pro_id);
//			String pro_name = "";
//			if (province != null)
//			{
//				pro_name = province.getString("pro_name");
//			}
//		
//			long storageId = addProductStorageCatalogShiTo(storage_title, deliver_zip_code,null,null, nation_id,pro_id, "", shipToAddress, "", pro_name,ship_to_id, storage_type);
//			
////			ShipToIndexMgr.getInstance().addIndex(ship_to_id);
////			ShipToStorageIndexMgr.getInstance().addIndex(storageId, storage_title, "", shipToAddress, "", deliver_zip_code, "", country.getString("c_code"), pro_name, pro_name, ship_to_id+"", ship_to_name, StorageTypeKey.RETAILER+"");
//		}
//		else
//		{
//			return isExistProductStorageCatalog.get("id", 0L);
//		}
//		
//		
//	}
	
	
	
	private long addProductStorageCatalogShiTo(String storage_title,String zip_code,String contact,String phone,long nation_id,long pro_id,String city,String home_number,String street,String pro_name,long storage_type_id, int storage_type)
			throws Exception
		{
			DBRow storageCatalog = new DBRow();
			storageCatalog.add("title",storage_title);
			storageCatalog.add("send_zip_code",zip_code);
			storageCatalog.add("contact",null);
			storageCatalog.add("phone",null);
			storageCatalog.add("native",nation_id);
			storageCatalog.add("deliver_zip_code",zip_code);
			storageCatalog.add("pro_id",pro_id);
			storageCatalog.add("city",city);
			storageCatalog.add("send_nation",nation_id);
			storageCatalog.add("send_pro_id",pro_id);
			storageCatalog.add("send_city",city);
			storageCatalog.add("send_house_number",home_number);
			storageCatalog.add("send_street",street);
			storageCatalog.add("send_contact",null);
			storageCatalog.add("send_phone",null);
			storageCatalog.add("send_pro_input",pro_name);
			storageCatalog.add("deliver_nation",nation_id);
			storageCatalog.add("deliver_pro_id",pro_id);
			storageCatalog.add("deliver_city",city);
			storageCatalog.add("deliver_house_number",home_number);
			storageCatalog.add("deliver_street",street);
			storageCatalog.add("deliver_contact",null);
			storageCatalog.add("deliver_phone",null);
			storageCatalog.add("deliver_pro_input",pro_name);
			storageCatalog.add("storage_type",storage_type);
		      storageCatalog.add("storage_type_id",storage_type_id);
			
			return floorCatalogMgr.addProductStorageCatalog(storageCatalog);
		}
	
	
	public FloorClpTypeMgrZr getFloorClpTypeMgrZr() {
		return floorClpTypeMgrZr;
	}

	public void setFloorClpTypeMgrZr(FloorClpTypeMgrZr floorClpTypeMgrZr) {
		this.floorClpTypeMgrZr = floorClpTypeMgrZr;
	}

	public FloorCustomerMgr getFloorCustomerMgr() {
		return floorCustomerMgr;
	}

	public void setFloorCustomerMgr(FloorCustomerMgr floorCustomerMgr) {
		this.floorCustomerMgr = floorCustomerMgr;
	}
	
}
