package com.cwc.app.floor.api.fa.basicdata;

import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.app.floor.api.fa.basicdata.BasicBean;

/**  
 * 
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.beans.RequirementItem.java] 
 * @ClassName:    [RequirementItem]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年4月2日 上午10:38:33]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年4月2日 上午10:38:33]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
public class RequirementItem implements BasicBean{
	
	public static String REQUIREMENT_ID = "ri_id";
	public static String REQUIREMENT_NAME = "requirement_name";
	public static String DISPLAY_TYPE = "display_type";
	public static String DESCRIPTION = "description";
	
	private int ri_id;
	private String requirement_name;
	private int display_type;
	private String description;
	@Override
	public DBRow toDBRow(boolean flag) {
		
		DBRow row = new DBRow();
		if (getRi_id() != 0 || flag) {
			row.add(REQUIREMENT_ID, getRi_id());
		}
		row.add(REQUIREMENT_NAME, StrUtil.dealNull(getRequirement_name()));
		row.add(DISPLAY_TYPE, getDisplay_type());
		row.add(DESCRIPTION, StrUtil.dealNull(getDescription()));
		return row;
		
	}
	public int getRi_id() {
		return ri_id;
	}
	public void setRi_id(int ri_id) {
		this.ri_id = ri_id;
	}
	public String getRequirement_name() {
		return requirement_name;
	}
	public void setRequirement_name(String requirement_name) {
		this.requirement_name = requirement_name;
	}
	public int getDisplay_type() {
		return display_type;
	}
	public void setDisplay_type(int display_type) {
		this.display_type = display_type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
