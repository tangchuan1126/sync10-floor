package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorReturnProductSubItemsMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public long addReturnProductSubItem(DBRow row) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("return_product_sub_items");
			long id = dbUtilAutoTran.getSequance(tableName);
			row.add("rpsi_id", id);
			dbUtilAutoTran.insert(tableName, row);
			return id;
		}catch (Exception e) {
			throw new Exception("FloorReturnProductSubItemsMgrZr.addReturnProductSubItem(DBRow):"+e);
		}
	}
	public DBRow[] getReturnProductSubItem(long rpi_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("return_product_sub_items");
			StringBuffer sql = new StringBuffer("select * from ").append(tableName)
			.append(" where rpi_id=").append(rpi_id);
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorReturnProductSubItemsMgrZr.getReturnProductSubItem(rpi_id):"+e);
		}
	}
	public void updateSubReturnProductItem(DBRow updateRow , long rpsi_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("return_product_sub_items");
			dbUtilAutoTran.update(" where rpsi_id="+rpsi_id, tableName, updateRow);
		}catch (Exception e) {
			throw new Exception("FloorReturnProductSubItemsMgrZr.updateSubReturnProductItem(rpsi_id):"+e);
		}
	}
}
