package com.cwc.app.floor.api.fa.basicdata;

import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

/**
 * 
 * @ProjectName: [basicdata]
 * @Package: [com.oso.basicdata.floor.TitleShipToMgr.java]
 * @ClassName: [TitleShipToMgr]
 * @Description: [Title Ship To Config ]
 * @Author: [赵永亚]
 * @CreateDate: [2015年3月24日 下午3:09:06]
 * @UpdateUser: [赵永亚]
 * @UpdateDate: [2015年3月24日 下午3:09:06]
 * @UpdateRemark: [说明本次修改内容]
 * @Version: [v1.0]
 * 
 */
public class FloorTitleShipToMgrZYY {

	private final static String TABLE = "title_ship_config";
	private final static String TABLE_VALUES = "title_ship_config_detail";
	private final static String FIELD_TITLE_ID = "title_id";
	private final static String FIELD_SHIP_TO_ID = "ship_to_id";
	private final static String FIELD_CONFIG_TYPE = "config_type";
	private final static String FIELD_CONFIG_ID = "config_id";
	private final static String CUSTOMER_KEY = "customer_key";
	public static String FIELD_TSC_ID = "tsc_id";
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 返回0：失败 1：报错成功 2：已存在
	 * 
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public int addTst(DBRow data) throws Exception {
		// TODO 样子 title_id ，ship_to_id,config_type,config_id 确保唯一
		if (findTstExists(data)) {
			// True 表示已存在 改，不能添加，抛出已存在一场
			// throw new
			// Exception("addTst(data) error: Configuration already exists");
			return 2;
		}
		try {
			if (dbUtilAutoTran.insert(TABLE, data)) {
				return 1;
			} else {
				return 0;
			}
		} catch (Exception e) {
			throw new Exception("addTst(data) error:" + e);
		}
	}
	/**
	 * 返回0：失败 1：报错成功 2：已存在
	 * 
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public int addTst(DBRow data,DBRow[] values) throws Exception {
		// TODO 样子 title_id ，ship_to_id,config_type,config_id 确保唯一
		if (findTstExists(data)) {
			// True 表示已存在 改，不能添加，抛出已存在一场
			// throw new
			// Exception("addTst(data) error: Configuration already exists");
			return 2;
		}
		try {
			long tsc_id = dbUtilAutoTran.insertReturnId(TABLE, data);
			
			insertDetails(values,tsc_id);
//			for(int i = 0,j=values.length;i<j;i++){
//				DBRow _val = values[i];
//				_val.add("tsc_id", tsc_id);
//				dbUtilAutoTran.insert(TABLE_VALUES, _val);
//			}
			
			return 1;
			
		} catch (Exception e) {
			throw new Exception("addTst(data,values) error:" + e);
		}
	}

	public int updateTst(DBRow data,DBRow[] values) throws Exception{
		//TODO 验证
		boolean flag = findTstExists(data);
		if(flag){
			return 2;
		}
		try {
			String sql = " where tsc_id="+data.get(FIELD_TSC_ID);
			//TODO 删除子项
			long tsc_id = data.get(FIELD_TSC_ID,0);
			delDetailsByTscid(tsc_id);
			insertDetails(values,tsc_id);
			return dbUtilAutoTran.update(sql, TABLE, data);
		} catch (Exception e) {
			throw new Exception("updateTst(data) error:" + e);
		}
	}
	
	public void insertDetails(DBRow[] values,long tsc_id) throws Exception{
		for(int i = 0,j=values.length;i<j;i++){
			DBRow _val = values[i];
			_val.add("tsc_id", tsc_id);
			dbUtilAutoTran.insert(TABLE_VALUES, _val);
		}
	}
	
	public int delDetailsByTscid(long tsc_id) throws Exception{
		String wherecond = " WHERE tsc_id=?";
		DBRow para = new DBRow();
		para.put("tsc_id", tsc_id);
		return dbUtilAutoTran.deletePre(wherecond, para, TABLE_VALUES);
	}
	
	public int updateTst(DBRow data) throws Exception{
		//TODO 验证
		boolean flag = findTstExists(data);
		if(flag){
			return 2;
		}
		try {
			String sql = " where tstid="+data.get(FIELD_TSC_ID);
			return dbUtilAutoTran.update(sql, TABLE, data);
		} catch (Exception e) {
			throw new Exception("updateTst(data) error:" + e);
		}
	}
	
	public boolean findTstExists(DBRow data) throws Exception {
		return findTstExists(data.get(FIELD_TITLE_ID, 0),
				data.get(FIELD_SHIP_TO_ID, 0), data.get(FIELD_CONFIG_TYPE, 0),
				data.get(FIELD_CONFIG_ID, 0),data.get(CUSTOMER_KEY, 0),data.get(FIELD_TSC_ID, 0),data.get("process_step", 0));
	}

	/**
	 * 验证 Title Ship To 是否存在
	 * 
	 * @param title_id
	 * @param ship_to_id
	 * @param config_type
	 * @param config_id
	 * @return
	 * @throws Exception
	 */
	public boolean findTstExists(int title_id, int ship_to_id, int config_type,
			int config_id,int customer_key,int tsc_id, int process_step) throws Exception {
		try {

			StringBuffer sql = new StringBuffer("select * from ");
			sql.append(TABLE).append(" where 1=1 AND title_id=?")
			   .append(" AND ship_to_id=?").append(" AND config_type=?")
			   .append(" AND config_id=?").append(" AND customer_key=?")
			   .append(" AND process_step=?")
			   .append(" AND tsc_id!=?  limit 1");
			DBRow params = new DBRow();
			params.add("title_id", title_id);
			params.add("ship_to_id", ship_to_id);
			params.add("config_type", config_type);
			params.add("config_id", config_id);
			params.add("customer_key", customer_key);
			params.add("process_step", process_step);
			params.add("tsc_id", tsc_id);
			

			DBRow[] result = dbUtilAutoTran.selectPreMutliple(sql.toString(),params);
			return result.length>0;
		} catch (Exception e) {
			throw new Exception("" + e);
		}
	}

	/**
	 * 根据 title_id 和 ship_to_id 查询Title_Ship_To 的配置
	 * 
	 * @param title_id
	 *            Title 的ID
	 * @param ship_to_id
	 *            Ship_To 的ID
	 * @return
	 */

	public DBRow[] findTstByTitleIdAndShipTo(int title_id, int ship_to_id)
			throws Exception {
		return findTstByTitleIdAndShipTo(0,title_id, ship_to_id,0,null, null,0);
	}

	public DBRow[] findTstByTitleIdAndShipTo(int customer_key,int title_id, int ship_to_id,int ri_id,String val,
			PageCtrl pc, int process_step) throws Exception {
		try {
			String sql = buildSql(customer_key,title_id, ship_to_id,ri_id,val,true, process_step);
			DBRow[] rows =null;
			if (null != pc) {
				rows= dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			} else {
				rows= dbUtilAutoTran.selectMutliple(sql.toString());
			}
			
			if(null!=rows && rows.length>0){
				//TODO 子程序名
				for(DBRow r :rows){
					DBRow[] children = dbUtilAutoTran.selectMutliple(buildDetailSql(r.get("tsc_id", 0)));
					r.add("children", children);
				}
			}
			return rows;
		} catch (Exception e) {
			throw new Exception("findTstByTitleIdAndShipTo" + e);
		}
	}
	
	/**
	 * 
	 * @param customer_key 客户ID
	 * @param title_id 
	 * @param ship_to_id
	 * @param ri_id
	 * @param val
	 * @param pc
	 * @param flag 查询条件是AND 还是OR ，如果flag = flase 是or 查询，如果是true 是and查询
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findTstByTitleIdAndShipTo(int customer_key,int title_id, int ship_to_id,int ri_id,String val,
			PageCtrl pc,boolean flag, int config_type, int config, int tsc_id, int process_step) throws Exception {
		try {
			String sql = buildSql(customer_key,title_id, ship_to_id,ri_id,val,flag, config_type, config, tsc_id, process_step);
			DBRow[] rows =null;
			if (null != pc) {
				rows= dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			} else {
				rows= dbUtilAutoTran.selectMutliple(sql.toString());
			}
			
			if(null!=rows && rows.length>0){
				//TODO 子程序名
				for(DBRow r :rows){
					DBRow[] children = dbUtilAutoTran.selectMutliple(buildDetailSql(r.get("tsc_id", 0)));
					r.add("children", children);
				}
			}
			return rows;
		} catch (Exception e) {
			throw new Exception("findTstByTitleIdAndShipTo" + e);
		}
	}
	public DBRow findTstById(int tstid) throws Exception {
		try {
			String sql = buildSql(0,tstid,0, 0,null,true, 0);
			DBRow[] rows = dbUtilAutoTran.selectMutliple(sql.toString()).clone();
			if(null!=rows){
				return rows[0];
			}
			return null;
		} catch (Exception e) {
			throw new Exception("findTstByTitleIdAndShipTo" + e);
		}
	}
	public int deleteTstById(int tstid) throws Exception {
		try {

			String sql = " where tsc_id = " + tstid;
			String sql2 = " where tsc_id = " + tstid;
			dbUtilAutoTran.delete(sql2, TABLE_VALUES);
			return dbUtilAutoTran.delete(sql, TABLE);
		} catch (Exception e) {
			throw new Exception("FloorTitleShipToMgrZYY.deleteTstById(long id) error:"
					+ e);
		}
	}

	public DBUtilAutoTran getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public String buildSql(int customer_key,int title_id, int ship_to_id,int ri_id,String val,boolean flag, int process_step){
		return this.buildSql(customer_key, title_id, ship_to_id, ri_id, val, flag, 0, 0, 0, process_step);
	}
	
	/**
	 * 
	 * @param title_id Title ID 
	 * @param ship_to_id Ship TO ID  
	 * @param ri_id  Requirement Item id 
	 * @param val Requirement_val
	 * @param flag 查询类别判断，如果是 true 是and 如果是false 是or 
	 * @CreateDate 2015年4月9日 10:39:21
	 * @return
	 */
	public String buildSql (int customer_key,int title_id, int ship_to_id,int ri_id,String val,boolean flag, int config_type, int config, int tsc_id, int process_step){
		
		// 2015年4月9日 10:40:16 现在 只处理 title_id 和ship_to_id
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT d.*,st.ship_to_name,customer.customer_id FROM ( SELECT tsc.*,t.title_name FROM(")
		.append(" SELECT *,CASE config_type 	WHEN 1 then (SELECT p_name FROM product d  where d.pc_id = tst.config_id limit 1)")
		.append(" WHEN 2 then (SELECT title from product_catalog pc  WHERE pc.id= tst.config_id  limit 1)")
		.append(" WHEN 3 then (SELECT name FROM product_line_define pld WHERE pld.id=tst.config_id  limit 1 )")
		.append(" END as config FROM title_ship_config tst ");
		//TODO 如果是 true 不管，如果是false 处理代码 2015年4月20日 16:42:47
		if(flag){
			sql.append(" where 1=1");
			if (title_id > 0) {
				sql.append(" AND title_id = " + title_id);
			}
			if (ship_to_id > 0) {
				sql.append(" AND ship_to_id = " + ship_to_id);
			}
			if(customer_key > 0){
				sql.append(" AND customer_key = " + customer_key);
			}
			
			if(ri_id>0){
				//TODO 目前处理用in 方式，有待修改完善
				sql.append(" AND tsc_id in " );
				sql.append("( SELECT tsc_id FROM title_ship_config_detail WHERE ri_id =").append(ri_id);
				if(!StrUtil.isBlank(val)){
					sql.append(" AND (requirement_value_int= ").append(val)
						.append(" OR FIND_IN_SET(" ).append(val).append(",requirement_value_str))");
				}
				sql.append(" ) ");
			}
			
			if(config_type > 0){
				if(tsc_id > 0){
					sql.append(" AND tsc_id = " + tsc_id);
				}
				
				if(config > 0){
					sql.append(" AND tst.config_id = " + config);
				}
				
				if(config_type > 0){
					sql.append(" AND tst.config_type = " + config_type);
				}
			}
			
			if(process_step > 0){
				sql.append(" AND tst.process_step = " + process_step);
			}
			
		}else{
			if(title_id+ship_to_id+customer_key+ri_id>0){
				sql.append(" where 1=0");
			}
			if (title_id > 0) {
				sql.append(" OR title_id = " + title_id);
			}
			if (ship_to_id > 0) {
				sql.append(" OR ship_to_id = " + ship_to_id);
			}
			if(customer_key > 0){
				sql.append(" OR customer_key = " + customer_key);
			}
			
			if(ri_id>0){
				//TODO 目前处理用in 方式，有待修改完善
				sql.append(" OR tsc_id in " );
				sql.append("( SELECT tsc_id FROM title_ship_config_detail WHERE ri_id =").append(ri_id);
				if(!StrUtil.isBlank(val)){
					sql.append(" AND (requirement_value_int= ").append(val)
						.append(" OR FIND_IN_SET(" ).append(val).append(",requirement_value_str))");
				}
				sql.append(" ) ");
			}
		}
		
		
		
		
		sql.append(" ORDER BY tst.tsc_id DESC) tsc LEFT JOIN title t on t.title_id = tsc.title_id )d ")
		.append(" LEFT JOIN ship_to  st ON st.ship_to_id = d.ship_to_id")
		.append(" LEFT JOIN customer_id  customer ON customer.customer_key = d.customer_key");
		sql.append(" ORDER BY tsc_id DESC");
		//TODO 处理 ri_id 和 val
		
		return sql.toString();
	}
	
	private String buildDetailSql(int tsc_id ){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * ,CASE display_type WHEN 1 THEN  	bb.requirement_value_str")
		.append(" WHEN 2 THEN   bb.requirement_value_str WHEN 6 THEN   bb.requirement_value_str ")
		.append(" WHEN 3 THEN  (SELECT item_name FROM requirement_item_details rid WHERE rid.ri_id= bb.ri_id AND item_value = bb.requirement_value_int)")
		.append(" WHEN 4 THEN  (SELECT  GROUP_CONCAT(item_name SEPARATOR ',') FROM requirement_item_details WHERE FIND_IN_SET(item_value,bb.requirement_value_str) AND ri_id=bb.ri_id)")
		.append(" WHEN 5 THEN  (SELECT item_name FROM requirement_item_details rid WHERE rid.ri_id= bb.ri_id AND item_value = bb.requirement_value_int)")
		.append(" END as display_type_val FROM(")
		.append(" SELECT t.*,ri.requirement_name, ri.display_type from (SELECT  tscd.*  FROM title_ship_config_detail tscd WHERE 1=1");
		if(tsc_id>0){
			sql.append(" and  tscd.tsc_id="+tsc_id);
		}
		sql.append(" ) t LEFT JOIN requirement_item ri ON t.ri_id = ri.ri_id  )bb");
		return sql.toString();
	}
	
	public DBRow[] getAllCustomer() throws Exception {
		try {
			String sql = (new StringBuilder()).append("SELECT *  FROM ").append(ConfigBean.getStringValue("customer_id")).toString();
			DBRow customer[] = dbUtilAutoTran.selectMutliple(sql);
			return customer;
		} catch (Exception e) {
			throw new Exception((new StringBuilder()).append("FloorTitleShipToMgrZYY.getAllCustomer() error:").append(e).toString());
		}
	}
	
	//查国家 a public 
	//查地区 b public 
	//查县 c public  
	// 这查 这表  private // 修改用户信息： ：基本信息 () ： 密码 (xiug zhi)。
	// java 一般要写  
	// 关于地区 这，地址，//
	
	
	/**
	 * 功能：根据pc_id获取产品信息
	 * @param pc_id
	 * @return
	 * @throws Exception
	 * @author lixf  2015年4月28日
	 */
	public DBRow getProductInfoByPcid(long pc_id) throws Exception{
		StringBuffer sqlBuffer = new StringBuffer();
		sqlBuffer.append("SELECT * FROM product p WHERE p.pc_id = ?");
		
		DBRow params = new DBRow();
		params.add("pc_id", pc_id);
		
		try{
			DBRow product = dbUtilAutoTran.selectPreSingle(sqlBuffer.toString(), params);
			return product;
		}catch(Exception e){
			throw new Exception("FloorTitleShipToMgrZYY.getProductInfoByPcid(long pc_id) " + e);
		}
	}
	
	/**
	 * 功能：根据catalog_id查询分类信息
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 * @author lixf  2015年4月28日
	 */
	public DBRow getCatalogInfoByCatalog(long catalog_id) throws Exception{
		StringBuffer sqlBuffer = new StringBuffer();
		sqlBuffer.append("SELECT * FROM product_catalog pc WHERE pc.id = ?");
		
		DBRow params = new DBRow();
		params.add("id", catalog_id);
		
		try{
			DBRow productCatalog = dbUtilAutoTran.selectPreSingle(sqlBuffer.toString(), params);
			return productCatalog;
		}catch(Exception e){
			throw new Exception("FloorTitleShipToMgrZYY.getProductLineByCatalog(long catalog_id) " + e);
		}
	}
}
