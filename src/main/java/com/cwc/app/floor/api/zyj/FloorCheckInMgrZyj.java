package com.cwc.app.floor.api.zyj;

import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.ContainerImportStatusKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.PlateStatusInOrOutBoundKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.SpaceRelationTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorCheckInMgrZyj {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加导入Container信息
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addImportContainer(DBRow row)throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("import_container_info"), row);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj addImportContainer"+e);
		}
	}
	
	/**
	 * 添加导入PlateNo信息
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addImportContainerPlate(DBRow row)throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("import_container_plates"), row);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj addImportContainerPlate"+e);
		}
	}
	
	/**
	 * 通过ContainerNo，查询container信息
	 * @param containerNo
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findContainerInfoByNo(String containerNo,int[] status, PageCtrl pc)throws Exception
	{
		try
		{
			String sql = "select * from import_container_info where 1=1";
			if(!StrUtil.isBlank(containerNo))
			{
				sql += " and container_no = '"+ containerNo+"'";
			}
			if(status != null && status.length > 0)
			{
				sql += " and (status = "+status[0];
				for (int i = 1; i < status.length; i++) 
				{
					sql += " or status = "+status[i];
				}
				sql += ")";
			}
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findContainerInfoByNo"+e);
		}
	}
	
	/**
	 * 通过ContainerNo查询Plates
	 * @param containerNo
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlatesByContainerNo(String containerNo, PageCtrl pc)throws Exception
	{
		try
		{
			String sql = "select * from import_container_plates where container_no = '"+ containerNo+"'";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlatesByContainerNo"+e);
		}
	}
	
	/**
	 * 通过ContainerInfoID查询Plates
	 * @param containerNo
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlatesByIcid(long ic_id, PageCtrl pc)throws Exception
	{
		try
		{
			
			String sql = "select icp.*, sll.location_name from import_container_plates icp"
					+" left join storage_load_unload_location sll on sll.id = icp.staging_area"
					+" where ic_id = "+ ic_id;
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlatesByIcid"+e);
		}
	}
	
	/**
	 * 通过ContainerNo信息的ID更新plate表
	 * @param ic_id
	 * @param row
	 * @throws Exception
	 */
	public void updateContainerPlateBatch(long ic_id, DBRow row) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where ic_id = " + ic_id, ConfigBean.getStringValue("import_container_plates"), row);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj updateContainerPlateBatch"+e);
		}
	}
	
	/**
	 * 通过ContainerNo托盘信息的ID更新plate表
	 * @param icp_id
	 * @param row
	 * @throws Exception
	 */
	public void updateContainerPlateByIcpId(long icp_id, DBRow row) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where icp_id = " + icp_id, ConfigBean.getStringValue("import_container_plates"), row);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj updateContainerPlateByIcpId"+e);
		}
	}
	
	
	/**
	 * 通过plateNo信息的ID更新plate表
	 * @param plateNo
	 * @param row
	 * @throws Exception
	 */
	public void updateContainerPlateByPlateNo(String plateNo, DBRow row) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where plate_no = '" + plateNo+"'", ConfigBean.getStringValue("import_container_plates"), row);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj updateContainerPlateByPlateNo"+e);
		}
	}
	
	/**
	 * 根据icid更新导入container信息表
	 * @param icid
	 * @param row
	 * @throws Exception
	 */
	public void updateContainerInfoByIcid(long icid, DBRow row) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where ic_id = " + icid, ConfigBean.getStringValue("import_container_info"), row);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj updateContainerInfoByIcid"+e);
		}
	}
	
	/**
	 * 通过entryID查询container信息
	 * @param entryId
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findContainerInfosByEntryId(long entryId, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select dd.*, ici.po_no po_no1, ici.status, ici.appointment_time from door_or_location_occupancy_details dd");
			sql.append(" left join import_container_info ici on dd.ic_id = ici.ic_id");
			sql.append(" where dd.dlo_id = "+entryId);
			sql.append(" and number_type = ").append(ModuleKey.CHECK_IN_CTN);
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findContainerInfosByEntryId"+e);
		}
	}
	
	/**
	 * 通过ContainerNo查询Plates
	 * @param containerNo
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlatesByPlateNo(String plateNo, PageCtrl pc)throws Exception
	{
		try
		{
			String sql = "select * from import_container_plates where 1=1";
			if(!StrUtil.isBlank(plateNo))
			{
				sql += " and plate_no = '"+ plateNo+"'";
			}
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlatesByPlateNo"+e);
		}
	}
	
	/**
	 * 通过ContainerNo查询Plates
	 * @param containerNo
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow findImportContainerByIcid(long ic_id)throws Exception
	{
		try
		{
			String sql = "select * from import_container_info where 1=1 and ic_id = " + ic_id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj dImportContainerByIcid"+e);
		}
	}
	
	
	/**
	 * 通过ContainerInfoID查询Plates
	 * @param containerNo
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findStagingAreasByIcid(long ic_id, PageCtrl pc)throws Exception
	{
		try
		{
			String sql = "select staging_area from import_container_plates where ic_id = "+ ic_id;
			sql += " group by staging_area";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findStagingAreasByIcid"+e);
		}
	}
	
	/**
	 * 通过ContainerInfoID查询Plates
	 * @param ic_id
	 * @param dod_id:checin 明细ID
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlatesByIcStagingAreaId(long ic_id,long staging, PageCtrl pc)throws Exception
	{
		try
		{
			String sql = "select icp.*, dd.dlo_id from import_container_plates icp";
			sql += " LEFT JOIN door_or_location_occupancy_details dd on dd.dlo_detail_id = icp.ship_entryd_id and dd.ic_id = icp.ic_id";
			sql += " where icp.ic_id = "+ ic_id;
			if(staging > 0)
			{
				sql += " and staging_area = " + staging;
			}
//			if(dod_id > 0)
//			{
//				sql += " and ship_entryd_id = "+dod_id;
//			}
			sql += " ORDER BY dlo_id desc, plate_no";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlatesByIcStagingAreaId"+e);
		}
	}
	
	/**
	 * 通过entryID查询pono信息
	 * @param entryId
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPonoInfosByEntryId(long entryId, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from door_or_location_occupancy_details dd");
			sql.append(" left join import_container_info ici on dd.ic_id = ici.ic_id");
			sql.append(" where dd.dlo_id = "+entryId);
			sql.append(" and dd.number_type = ").append(ModuleKey.CHECK_IN_PONO);
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPonoInfosByEntryId"+e);
		}
	}
	
	/**
	 * 查询plate在库情况
	 * @param type 通过时间查询时，查的是入库还是出库 in:1, out:2
	 * @param startTime
	 * @param endTime
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateNoInOutInfo(int type, String startTime, String endTime, long ps_id_in, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select icp.plate_no,icp.staging_area, sll.location_name, ddi.finish_time finish_time_in, ddo.finish_time finish_time_out");
//			sql.append(" , ddi.number ctnr, ddo.number pono");
			sql.append(", ddi.dlo_id dlo_id_in, ddo.dlo_id dlo_id_out");
			sql.append(", dmi.ps_id ps_id_in, psi.title ps_title_in");
			sql.append(" , ici.container_no ctnr, ici.po_no pono, icp.icp_id, ici.customer_id, icp.box_number,icp.weight");
			sql.append(" , ddi.dlo_detail_id dlo_detail_in, ddo.dlo_detail_id dlo_detail_out");
//			, pso.id ps_id_out, pso.title ps_title_out
			sql.append(" from import_container_plates icp");
			sql.append(" join import_container_info ici on icp.ic_id = ici.ic_id");
			sql.append(" left join storage_load_unload_location sll on sll.id = icp.staging_area");
			sql.append(" join door_or_location_occupancy_details ddi on ddi.ic_id = icp.ic_id and ddi.number_type = ").append(ModuleKey.CHECK_IN_CTN);
			sql.append(" left join door_or_location_occupancy_details ddo on ddo.dlo_detail_id = icp.ship_entryd_id and ddo.number_type = ").append(ModuleKey.CHECK_IN_PONO);
			sql.append(" join door_or_location_occupancy_main dmi on dmi.dlo_id = ddi.dlo_id");
			sql.append(" left join door_or_location_occupancy_main dmo on dmo.dlo_id = ddo.dlo_id");
			sql.append(" left join product_storage_catalog psi on psi.id = dmi.ps_id");
//			sql.append(" left join product_storage_catalog pso on pso.id = dmo.ps_id");
			sql.append(" where 1=1");
			if(ps_id_in > 0)
			{
				sql.append(" and dmi.ps_id = ").append(ps_id_in);
			}
			if(!StrUtil.isBlank(startTime))
			{
				if(1 == type)
				{
					sql.append(" and ddi.finish_time >= '").append(startTime).append("'");
				}
				else if(2 == type)
				{
					sql.append(" and ddo.finish_time >= '").append(startTime).append("'");
				}
			}
			if(!StrUtil.isBlank(endTime))
			{
				if(1 == type)
				{
					sql.append(" and ddi.finish_time <= '").append(endTime).append("'");
				}
				else if(2 == type)
				{
					sql.append(" and ddo.finish_time <= '").append(startTime).append("'");
				}
			}
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlateNoInOutInfo"+e);
		}
	}

	
	public DBRow getPlateInfoBy(String pallet_no) throws Exception {
		try{
			String sql = "select icp.* , sll.location_name from import_container_plates  as icp left join storage_load_unload_location sll on sll.id = icp.staging_area  where IFNULL(ship_entryd_id,0) = 0 and  plate_no = '"+pallet_no+"'";
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj getPlateInfoBy"+e);
		}
		
	}

	public DBRow getContainerInfoBy(long ic_id) throws Exception{
		try{
			String sql = "select * from import_container_info where ic_id=" +ic_id ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj getContainerInfoBy"+e);
		}
	}
	
	/**
	 * 获取这个IC_id 实际收了多少个box
	 * @param ic_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月25日
	 */
	public int getSumOfContainReceiveTotalQty(long ic_id) throws Exception{
		try{
			String sql = "select SUM(box_number) as total_box from import_container_plates where ic_id ="+ic_id ;
			DBRow retunRow = dbUtilAutoTran.selectSingle(sql);
			if(retunRow == null){
				return 0 ;
			}
			return retunRow.get("total_box", 0);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj getSumOfContainReceiveTotalQty"+e);
		}
		
	}
	
	public DBRow[] getImportRepeatByLoadNo(String LoadNo) throws Exception{
		try{
			String sql = "select * from import_container_info where po_no='"+LoadNo+"' and status="+ContainerImportStatusKey.IMPORTED ;
			
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj getSumOfContainReceiveTotalQty"+e);
		}
		
	}
	/**
	 * 搜索plate库存
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchPlatesLevelInventary(int type, String startTime, String endTime, long ps_id_in, int status,long entry_id_in, long entry_id_out, long ic_id_in, long ic_id_out, PageCtrl pc)throws Exception
	{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select icp.plate_no, sll.location_name, ddi.finish_time finish_time_in, ddo.finish_time finish_time_out");
			sql.append(" , ddi.dlo_id dlo_id_in, ddo.dlo_id dlo_id_out , dmi.ps_id ps_id_in, psi.title ps_title_in, icp.ship_entryd_id");
			sql.append(" , ici.container_no ctnr, ici.po_no pono, ici.customer_id, icp.box_number, ici.ic_id, ici.bol, ici.qty, ici.ship_out_date");
			sql.append(" , dmi.gate_liscense_plate gate_liscense_plate_in, dmi.gate_driver_name gate_driver_name_in, dmi.gate_driver_liscense gate_driver_liscense_in, dmi.company_name company_name_in");
			sql.append(" , dmo.gate_liscense_plate gate_liscense_plate_out, dmo.gate_driver_name gate_driver_name_out, dmo.gate_driver_liscense gate_driver_liscense_out, dmo.company_name company_name_out");
			sql.append(" from import_container_plates icp");
			sql.append(" join import_container_info ici on icp.ic_id = ici.ic_id");
			sql.append(" left join storage_load_unload_location sll on sll.id = icp.staging_area");
			sql.append(" join door_or_location_occupancy_details ddi on ddi.ic_id = icp.ic_id and ddi.number_type = ").append(ModuleKey.CHECK_IN_CTN);
			sql.append(" left join door_or_location_occupancy_details ddo on ddo.dlo_detail_id = icp.ship_entryd_id and ddo.number_type = ").append(ModuleKey.CHECK_IN_PONO);
			sql.append(" join door_or_location_occupancy_main dmi on dmi.dlo_id = ddi.dlo_id");
			sql.append(" left join door_or_location_occupancy_main dmo on dmo.dlo_id = ddo.dlo_id");
			sql.append(" left join product_storage_catalog psi on psi.id = dmi.ps_id");
			sql.append(" where 1=1");
			if(ps_id_in > 0)
			{
				sql.append(" and dmi.ps_id = ").append(ps_id_in);
			}
			if(!StrUtil.isBlank(startTime))
			{
				if(PlateStatusInOrOutBoundKey.ON_HAND == type)
				{
					sql.append(" and ddi.finish_time >= '").append(startTime).append("'");
				}
				else if(PlateStatusInOrOutBoundKey.SHIPPED == type)
				{
					sql.append(" and ddo.finish_time >= '").append(startTime).append("'");
				}
			}
			if(!StrUtil.isBlank(endTime))
			{
				if(PlateStatusInOrOutBoundKey.ON_HAND == type)
				{
					sql.append(" and ddi.finish_time <= '").append(endTime).append("'");
				}
				else if(PlateStatusInOrOutBoundKey.SHIPPED == type)
				{
					sql.append(" and ddo.finish_time <= '").append(endTime).append("'");
				}
			}
			if(PlateStatusInOrOutBoundKey.ON_HAND == status)
			{
				sql.append(" and ddo.dlo_id is null");
			}
			else if(PlateStatusInOrOutBoundKey.SHIPPED == status)
			{
				sql.append(" and ddo.dlo_id is not null");
			}
			if(entry_id_in > 0)
			{
				sql.append(" and dmi.dlo_id = ").append(entry_id_in);
			}
			if(entry_id_out > 0)
			{
				sql.append(" and dmo.dlo_id = ").append(entry_id_out);
			}
			if(ic_id_in > 0)
			{
				sql.append(" and ici.ic_id = ").append(ic_id_in);
			}
			if(ic_id_out > 0)
			{
				sql.append(" and ici.ic_id = ").append(ic_id_out);
				sql.append(" and dmo.dlo_id is not null ");
			}
//			sql.append(" order by ");
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj searchPlatesLevelInventary"+e);
		}
	}
	
	/**
	 * 通过entryID获取进来的plate总数
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public int findPlateCountByEntryIdIn(long entry_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select count(icp.plate_no) cn");
			sql.append(" from import_container_plates icp");
			sql.append(" join door_or_location_occupancy_details ddi on ddi.ic_id = icp.ic_id and ddi.number_type = ").append(ModuleKey.CHECK_IN_CTN);;
			sql.append(" where 1=1");
			sql.append(" and ddi.dlo_id = ").append(entry_id);
			return dbUtilAutoTran.selectSingle(sql.toString()).get("cn", 0);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findPlateCountByEntryIdIn"+e);
		}
	}
	
	/**
	 * 通过entryID获取提取的plate总数
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public int findPlateCountByEntryIdOut(long entry_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select count(icp.plate_no) cn");
			sql.append(" from import_container_plates icp");
			sql.append(" left join door_or_location_occupancy_details ddo on ddo.dlo_detail_id = icp.ship_entryd_id and ddo.number_type = ").append(ModuleKey.CHECK_IN_PONO);;
			sql.append(" where 1=1");
			sql.append(" and ddo.dlo_id = ").append(entry_id);
			return dbUtilAutoTran.selectSingle(sql.toString()).get("cn", 0);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findPlateCountByEntryIdOut"+e);
		}
	}
	
	/**
	 * 通过icId获取进来的plate总数
	 * @param ic_id
	 * @return
	 * @throws Exception
	 */
	public int findPlateCountByIcidIn(long ic_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select count(icp.plate_no) cn");
			sql.append(" from import_container_plates icp");
			sql.append(" where 1=1");
			sql.append(" and icp.ic_id = ").append(ic_id);
			return dbUtilAutoTran.selectSingle(sql.toString()).get("cn", 0);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findPlateCountByIcidIn"+e);
		}
	}
	
	/**
	 * 通过entryID获取进来的plate总数
	 * @param ic_id
	 * @return
	 * @throws Exception
	 */
	public int findPlateCountByIcidOut(long ic_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select count(icp.plate_no) cn");
			sql.append(" from import_container_plates icp");
			sql.append(" where 1=1");
			sql.append(" and icp.ic_id = ").append(ic_id);
			sql.append(" and icp.ship_entryd_id is not null");
			return dbUtilAutoTran.selectSingle(sql.toString()).get("cn", 0);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findPlateCountByIcidOut"+e);
		}
	}
	
	/**
	 * 通过entry Detail ID获取进来的plate总数
	 * @param dlod_id
	 * @return
	 * @throws Exception
	 */
	public int findPlateCountByEntryDetailIdOut(long dlod_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select count(icp.plate_no) cn");
			sql.append(" from import_container_plates icp");
			sql.append(" where 1=1");
			sql.append(" and icp.ship_entryd_id = ").append(dlod_id);
			return dbUtilAutoTran.selectSingle(sql.toString()).get("cn", 0);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findPlateCountByEntryDetailIdOut"+e);
		}
	}
	
	
	/****************************************Consolidate******************************************************/
	
	/**
	 * 添加：consolidate_master_bols
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long insertConsolidateMasterBols(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("consolidate_master_bols"), row);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj insertConsolidateMasterBols"+e);
		}
	}
	
	/**
	 * 添加：consolidate_orders
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long insertConsolidateOrders(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("consolidate_orders"), row);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj insertConsolidateOrders"+e);
		}
	}
	
	/**
	 * 添加：consolidate_orders
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long insertConsolidateOrderLines(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("consolidate_order_lines"), row);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj insertConsolidateOrderLines"+e);
		}
	}
	
	/**
	 * 添加：consolidate_order_plates
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long insertConsolidateOrderPlates(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("consolidate_order_plates"), row);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj insertConsolidateOrderPlates"+e);
		}
	}
	
	/**
	 * 通过loadno查询需要consolidate数据
	 * @param loadNo
	 * @param companyId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolsByLoadNo(String loadNo, long masterBolNo, String companyId, String customerId, String[] status, long load_id, PageCtrl pc)  throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM consolidate_master_bols WHERE 1=1");
			if(!StrUtil.isBlank(loadNo))
			{
				sql.append(" and load_no = '").append(loadNo).append("'");
			}
			if(masterBolNo > 0)
			{
				sql.append(" and master_bol_no = ").append(masterBolNo);
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and company_id = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and customer_id = '").append(customerId).append("'");
			}
			sql.append(sqlStatus(status));
			if(load_id > 0)
			{
				sql.append(" and load_id = ").append(load_id);
			}
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findMasterBolsByLoadNo"+e);
		}
	}
	
	public String sqlStatus(String[] status)
	{
		String sql = "";
		if(null != status && status.length > 0)
		{
			sql += " AND (";
			sql += " Status = '"+status[0]+"'";
			for (int i = 1; i < status.length; i++) 
			{
				sql += " OR Status = '"+status[i]+"'";
			}
			sql += " ) ";
		}
		return sql;
	}
	
	/**
	 * 通过orderno……查询order信息
	 * @param masterBolNo
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @param po_no
	 * @param status
	 * @param load_id
	 * @param order_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findConsolidateOrdersByOrderNo(long masterBolNo, long orderNo, String companyId, String customerId, String po_no, String[] status, long load_id, long order_id,PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from consolidate_orders where 1=1");
			if(masterBolNo > 0)
			{
				sql.append(" and master_bol_no = ").append(masterBolNo);
			}
			if(orderNo > 0)
			{
				sql.append(" and order_no = ").append(orderNo);
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and company_id = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and customer_id = '").append(customerId).append("'");
			}
			if(!StrUtil.isBlank(po_no))
			{
				sql.append(" and po_no = '").append(po_no).append("'");
			}
			sql.append(sqlStatus(status));
			if(load_id > 0)
			{
				sql.append(" and load_id = ").append(load_id);
			}
			if(order_id > 0)
			{
				sql.append(" and order_id = ").append(order_id);
			}
//			System.out.println(sql.toString());
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findConsolidateOrdersByOrderNo"+e);
		}
	}
	
	
	/**
	 * 通过orderno……查询order信息
	 * @param masterBolNo
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @param po_no
	 * @param status
	 * @param load_id
	 * @param order_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findConsolidateOrderLinesByOrderNo(long orderNo, String companyId, String customerId, String po_no
			, long load_id, long order_id, String item_id, String lot_number,int line_no, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from consolidate_order_lines where 1=1");
			if(orderNo > 0)
			{
				sql.append(" and order_no = ").append(orderNo);
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and company_id = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and customer_id = '").append(customerId).append("'");
			}
			if(!StrUtil.isBlank(po_no))
			{
				sql.append(" and po_no = '").append(po_no).append("'");
			}
			if(load_id > 0)
			{
				sql.append(" and load_id = ").append(load_id);
			}
			if(order_id > 0)
			{
				sql.append(" and order_id = ").append(order_id);
			}
			if(!StrUtil.isBlank(item_id))
			{
				sql.append(" and item_id = '").append(item_id).append("'");
			}
			if(!StrUtil.isBlank(lot_number))
			{
				sql.append(" and lot_number = '").append(lot_number).append("'");
			}
			if(line_no > 0)
			{
				sql.append(" and line_no = ").append(line_no);
			}
//			System.out.println(sql.toString());
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findConsolidateOrderLinesByOrderNo"+e);
		}
	}
	
	/**
	 * 通过orderno……查询order信息
	 * @param masterBolNo
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @param po_no
	 * @param status
	 * @param load_id
	 * @param order_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findConsolidateOrderPlatesByOrderNo(long orderNo, String companyId, String plateNo, int line_no
			,long orderNoFrom, String companyIdFrom, String plateNoFrom, int line_noFrom,String itemId, String lot_number, String SupplierID
			,long order_id, long plate_id,PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from consolidate_order_plates where 1=1");
			if(orderNo > 0)
			{
				sql.append(" and order_no = ").append(orderNo);
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and company_id = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(plateNo))
			{
				sql.append(" and plate_no = '").append(plateNo).append("'");
			}
			if(line_no > 0)
			{
				sql.append(" and line_no = ").append(line_no);
			}
			if(orderNoFrom > 0)
			{
				sql.append(" and order_no_from = ").append(orderNoFrom);
			}
			if(!StrUtil.isBlank(companyIdFrom))
			{
				sql.append(" and company_id_from = '").append(companyIdFrom).append("'");
			}
			if(!StrUtil.isBlank(plateNoFrom))
			{
				sql.append(" and plate_no_from = '").append(plateNoFrom).append("'");
			}
			if(line_noFrom > 0)
			{
				sql.append(" and line_no_from = ").append(line_noFrom);
			}
			if(!StrUtil.isBlank(itemId))
			{
				sql.append(" and item_id = '").append(itemId).append("'");
			}
			if(!StrUtil.isBlank(lot_number))
			{
				sql.append(" and lot_number = '").append(lot_number).append("'");
			}
			if(!StrUtil.isBlank(SupplierID))
			{
				sql.append(" and supplier_id = '").append(SupplierID).append("'");
			}
			if(order_id > 0)
			{
				sql.append(" and order_id = ").append(order_id);
			}
			if(plate_id > 0)
			{
				sql.append(" and plate_id = ").append(plate_id);
			}
			
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findConsolidateOrderPlatesByOrderNo"+e);
		}
	}
	
	/**
	 * 删除托盘
	 * @param orderNo
	 * @param companyId
	 * @param plateNo
	 * @param line_no
	 * @param orderNoFrom
	 * @param companyIdFrom
	 * @param plateNoFrom
	 * @param line_noFrom
	 * @param order_id
	 * @param plate_id
	 * @throws Exception
	 */
	public void deleteConsolidatePlateByOrderNoPlateNo(
			long orderNo, String companyId, String plateNo, int line_no
			,long orderNoFrom, String companyIdFrom, String plateNoFrom, int line_noFrom
			,long order_id, long plate_id
			
			) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" where 1=1");
			if(orderNo > 0)
			{
				sql.append(" and order_no = ").append(orderNo);
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and company_id = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(plateNo))
			{
				sql.append(" and plate_no = '").append(plateNo).append("'");
			}
			if(line_no > 0)
			{
				sql.append(" and line_no = ").append(line_no);
			}
			if(orderNoFrom > 0)
			{
				sql.append(" and order_no_from = ").append(orderNoFrom);
			}
			if(!StrUtil.isBlank(companyIdFrom))
			{
				sql.append(" and company_id_from = '").append(companyIdFrom).append("'");
			}
			if(!StrUtil.isBlank(plateNoFrom))
			{
				sql.append(" and plate_no_from = '").append(plateNoFrom).append("'");
			}
			if(line_noFrom > 0)
			{
				sql.append(" and line_no_from = ").append(line_noFrom);
			}
			if(order_id > 0)
			{
				sql.append(" and order_id = ").append(order_id);
			}
			if(plate_id > 0)
			{
				sql.append(" and plate_id = ").append(plate_id);
			}
			dbUtilAutoTran.delete(sql.toString(), ConfigBean.getStringValue("consolidate_order_plates"));
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj deleteConsolidatePlateByOrderNoPlateNo"+e);
		}
	}
	
	
	/**
	 * 更新托盘
	 * @param orderNo
	 * @param companyId
	 * @param plateNo
	 * @param line_no
	 * @param orderNoFrom
	 * @param companyIdFrom
	 * @param plateNoFrom
	 * @param line_noFrom
	 * @param order_id
	 * @param plate_id
	 * @throws Exception
	 */
	public void updateConsolidatePlateByOrderNoPlateNo(
			long orderNo, String companyId, String plateNo, int line_no
			,long orderNoFrom, String companyIdFrom, String plateNoFrom, int line_noFrom
			,long order_id, long plate_id, DBRow row
			) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" where 1=1");
			if(orderNo > 0)
			{
				sql.append(" and order_no = ").append(orderNo);
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and company_id = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(plateNo))
			{
				sql.append(" and plate_no = '").append(plateNo).append("'");
			}
			if(line_no > 0)
			{
				sql.append(" and line_no = ").append(line_no);
			}
			if(orderNoFrom > 0)
			{
				sql.append(" and order_no_from = ").append(orderNoFrom);
			}
			if(!StrUtil.isBlank(companyIdFrom))
			{
				sql.append(" and company_id_from = '").append(companyIdFrom).append("'");
			}
			if(!StrUtil.isBlank(plateNoFrom))
			{
				sql.append(" and plate_no_from = '").append(plateNoFrom).append("'");
			}
			if(line_noFrom > 0)
			{
				sql.append(" and line_no_from = ").append(line_noFrom);
			}
			if(order_id > 0)
			{
				sql.append(" and order_id = ").append(order_id);
			}
			if(plate_id > 0)
			{
				sql.append(" and plate_id = ").append(plate_id);
			}
			dbUtilAutoTran.update(sql.toString(), ConfigBean.getStringValue("consolidate_order_plates"), row);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj updateConsolidatePlateByOrderNoPlateNo"+e);
		}
	}
	
	/**
	 * 更新托盘
	 * @param plate_id
	 * @throws Exception
	 */
	public void updateConsolidatePlateByOrderNoPlateId(long plate_id, DBRow row) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" where 1=1");
			if(plate_id > 0)
			{
				sql.append(" and plate_id = ").append(plate_id);
			}
			dbUtilAutoTran.update(sql.toString(), ConfigBean.getStringValue("consolidate_order_plates"), row);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj updateConsolidatePlateByOrderNoPlateId"+e);
		}
	}
	
	/**
	 * 删除托盘
	 * @param plate_id
	 * @throws Exception
	 */
	public void deleteConsolidatePlateByOrderNoPlateId(long plate_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" where 1=1");
			if(plate_id > 0)
			{
				sql.append(" and plate_id = ").append(plate_id);
			}
			dbUtilAutoTran.delete(sql.toString(), ConfigBean.getStringValue("consolidate_order_plates"));
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj deleteConsolidatePlateByOrderNoPlateId"+e);
		}
	}
	
	/**
	 * 通过loadno查询load信息
	 * @param loadNo
	 * @param CompanyID
	 * @param CustomerID
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadInfosByLoadNo(String loadNo, String CompanyID, String CustomerID, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from consolidate_master_bols where 1=1");
			if(!StrUtil.isBlank(loadNo))
			{
				sql.append(" and load_no = '").append(loadNo).append("'");
			}
			if(!StrUtil.isBlank(CompanyID))
			{
				sql.append(" and company_id = '").append(CompanyID).append("'");
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql.append(" and customer_id = '").append(CustomerID).append("'");
			}
			sql.append(sqlStatus(status));
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findLoadInfosByLoadNo"+e);
		}
	}
	
	/**
	 * 通过masterBolNo查询load信息
	 * @param masterBolNo
	 * @param CompanyID
	 * @param CustomerID
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadInfosByMasterBolNo(long masterBolNo, String CompanyID, String CustomerID, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from consolidate_master_bols where 1=1");
			if(masterBolNo > 0)
			{
				sql.append(" and master_bol_no = ").append(masterBolNo);
			}
			if(!StrUtil.isBlank(CompanyID))
			{
				sql.append(" and company_id = '").append(CompanyID).append("'");
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql.append(" and customer_id = '").append(CustomerID).append("'");
			}
			sql.append(sqlStatus(status));
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findLoadInfosByMasterBolNo"+e);
		}
	}
	
	
	/**
	 * 通过orderNo查询Orders信息
	 * @param orderNo
	 * @param CompanyID
	 * @param CustomerID
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderInfosByOrderNo(long orderNo, String CompanyID, String CustomerID, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from consolidate_orders where 1=1");
			if(orderNo > 0)
			{
				sql.append(" and order_no = ").append(orderNo);
			}
			if(!StrUtil.isBlank(CompanyID))
			{
				sql.append(" and company_id = '").append(CompanyID).append("'");
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql.append(" and customer_id = '").append(CustomerID).append("'");
			}
			sql.append(sqlStatus(status));
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findOrderInfosByOrderNo"+e);
		}
	}
	
	
	/**
	 * 通过orderNo查询Orders信息
	 * @param orderNo
	 * @param CompanyID
	 * @param CustomerID
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderInfosByLoadNo(String loadNo, String CompanyID, String CustomerID, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from consolidate_orders where 1=1");
			if(!StrUtil.isBlank(loadNo))
			{
				sql.append(" and load_no = '").append(loadNo).append("'");
			}
			if(!StrUtil.isBlank(CompanyID))
			{
				sql.append(" and company_id = '").append(CompanyID).append("'");
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql.append(" and customer_id = '").append(CustomerID).append("'");
			}
			sql.append(sqlStatus(status));
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findOrderInfosByLoadNo"+e);
		}
	}
	
	/**
	 * 通过load_id查询Orders信息
	 * @param load_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderInfosByLoadId(long load_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from consolidate_orders where 1=1");
			if(load_id > 0)
			{
				sql.append(" and load_id = ").append(load_id);
			}
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findOrderInfosByLoadId"+e);
		}
	}
	
	/**
	 * 通过order_id查询Plates信息
	 * @param order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateInfosByOrderId(long order_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from consolidate_order_plates where 1=1");
			if(order_id > 0)
			{
				sql.append(" and order_id = ").append(order_id);
			}
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findPlateInfosByOrderId"+e);
		}
	}

	
	/***************************************************托盘库存***************************************************/
	
	/**
	 * 添加：plate_level_product_inventary
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long insertPlateLevelProductInventary(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("plate_level_product_inventary"), row);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj insertPlateLevelProductInventary"+e);
		}
	}
	
	/**
	 * 通过主键更新：plate_level_product_inventary
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public void updatePlateLevelProductInventaryById(long pi_id, DBRow row) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where plate_inventary_id = "+pi_id, ConfigBean.getStringValue("plate_level_product_inventary"), row);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj updatePlateLevelProductInventaryById"+e);
		}
	}
	
	
	/**
	 * 通过收货单据和托盘，查询托盘
	 * @param receive_type
	 * @param receive_id
	 * @param plate_no
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateByPlateNoReceiveTypeNo(int receive_order_type, long receive_order, String plate_no) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from plate_level_product_inventary");
			sql.append(" where 1=1");
			sql.append(" and receive_order_type = ").append(receive_order_type);
			sql.append(" and receive_order = ").append(receive_order);
			sql.append(" and plate_no = '").append(plate_no).append("'");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findPlateByPlateNoReceiveTypeNo"+e);
		}
	}
	
	/**
	 * 通过staging_area_id查询Staging名称
	 * @param staging_area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findStagingAreaByID(long staging_area_id) throws Exception
	{
		try
		{
			return dbUtilAutoTran.selectSingle("select * from storage_load_unload_location where id = " + staging_area_id);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findStagingAreaByID"+e);
		}
	}
	
	//searchPlatesLevelInventary
	//int type, String startTime, String endTime, long ps_id_in, int status,long entry_id_in
	//, long entry_id_out, long ic_id_in, long ic_id_out, PageCtrl pc
	/**
	 * 过滤托盘
	 * @param receiveType
	 * @param receiveOrder
	 * @param shippedType
	 * @param shippedOrder
	 * @param ps_id
	 * @param startTime
	 * @param endTime
	 * @param associateType
	 * @param associateOrder
	 * @param timeType
	 * @param plateStatus
	 * @param plateNo
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventarys(int receiveType, long receiveOrder
			, int shippedType, long shippedOrder, long ps_id, String startTime, String endTime, String plateNo, long plate_inventary_id
			, int associateType, long associateOrder,int timeType, int plateStatus,PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from plate_level_product_inventary where 1=1");
			if(!StrUtil.isBlank(plateNo))
			{
				sql.append(" and plate_no = '").append(plateNo).append("'");
			}
			if(plate_inventary_id > 0)
			{
				sql.append(" and plate_inventary_id = ").append(plate_inventary_id);
			}
			if(ps_id > 0)
			{
				sql.append(" and ps_id = ").append(ps_id);
			}
			if(!StrUtil.isBlank(startTime))
			{
				if(PlateStatusInOrOutBoundKey.ON_HAND == timeType)
				{
					sql.append(" and receive_time >= '").append(startTime).append("'");
				}
				else if(PlateStatusInOrOutBoundKey.SHIPPED == timeType)
				{
					sql.append(" and shipped_time >= '").append(startTime).append("'");
				}
			}
			if(!StrUtil.isBlank(endTime))
			{
				if(PlateStatusInOrOutBoundKey.ON_HAND == timeType)
				{
					sql.append(" and receive_time <= '").append(endTime).append("'");
				}
				else if(PlateStatusInOrOutBoundKey.SHIPPED == timeType)
				{
					sql.append(" and shipped_time <= '").append(endTime).append("'");
				}
			}
			if(PlateStatusInOrOutBoundKey.ON_HAND == plateStatus)
			{
				sql.append(" and (shipped_order is null or shipped_order = 0)");
			}
			else if(PlateStatusInOrOutBoundKey.SHIPPED == plateStatus)
			{
				sql.append(" and (shipped_order is not null and shipped_order != 0)");
			}
			if(receiveType > 0)
			{
				sql.append(" and receive_order_type = ").append(receiveType);
			}
			if(receiveOrder > 0)
			{
				sql.append(" and receive_order = ").append(receiveOrder);
			}
			if(shippedType > 0)
			{
				sql.append(" and shipped_order_type = ").append(shippedType);
			}
			if(shippedOrder > 0)
			{
				sql.append(" and shipped_order = ").append(shippedOrder);
			}
			if(associateType > 0)
			{
				sql.append(" and associate_order_type = ").append(associateType);
			}
			if(associateOrder > 0)
			{
				sql.append(" and associate_order = ").append(associateOrder);
			}
//			sql.append(" order by ");
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findPlateLevelProductInventarys"+e);
		}
	}
	
	
	public DBRow findStorageById(long ps_id) throws Exception
	{
		try
		{
			return dbUtilAutoTran.selectSingle("select * from product_storage_catalog where id = "+ps_id);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findStorageById"+e);
		}
	}
	
	/**
	 * 通过entry明细id获取entry
	 * @param dlo_detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findEntrySomeInfoByDetailId(long dlo_detail_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select gate_liscense_plate,gate_driver_name,gate_driver_liscense,company_name, dm.dlo_id , dd.number_type, number");
			sql.append(" from door_or_location_occupancy_details dd");
			sql.append(" join door_or_location_occupancy_main dm on dm.dlo_id = dd.dlo_id");
			sql.append(" where dd.dlo_detail_id = " + dlo_detail_id);
			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findEntrySomeInfoByDetailId"+e);
		}
	}
	
	/**
	 * 通过导入的container的托盘id查询container信息
	 * @param icp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findContainerInfoByIcpId(long icp_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select ici.container_no ctnr, ici.po_no pono, icp.ic_id ");
			sql.append(" from import_container_plates icp");
			sql.append(" join import_container_info ici on icp.ic_id = ici.ic_id");
			sql.append(" where icp.icp_id = " + icp_id);
			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findContainerInfoByIcpId"+e);
		}
	}
	
	
	/**
	 * 通过主键查询：plate_level_product_inventary
	 * @param pi_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findPlateLevelProductInventaryById(long pi_id) throws Exception
	{
		try
		{
			return dbUtilAutoTran.selectSingle("select * from plate_level_product_inventary where plate_inventary_id = "+pi_id);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findPlateLevelProductInventaryById"+e);
		}
	}
	
	/**
	 * 通过主键查询：plate_level_product_inventary
	 * @param sql
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventarysBySql(StringBuffer sql, PageCtrl pc) throws Exception
	{
		try
		{
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findPlateLevelProductInventarysBySql"+e);
		}
	}
	
	/**
	 * 通过主键更新：plate_level_product_inventary
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public void updatePlateLevelProductInventaryByAssociate(int associateType,long associateOrder, DBRow row) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where associate_order_type = "+associateType+" and associate_order = "+associateOrder, ConfigBean.getStringValue("plate_level_product_inventary"), row);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj updatePlateLevelProductInventaryByAssociate"+e);
		}
	}
	
	/**
	 * 通过account查询account info
	 * @param account
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAccountInfoByAccount(String account) throws Exception
	{
		try
		{
			return dbUtilAutoTran.selectMutliple("select * from account_id where account_id = '"+account+"'");
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findAccountInfoByAccount"+e);
		}
	}
	
	/**
	 * 通过Customer查询Customer
	 * @param Customer
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findCustomerInfoByCustomer(String Customer) throws Exception
	{
		try
		{
			return dbUtilAutoTran.selectMutliple("select * from customer_id where customer_id = '"+Customer+"'");
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findAccountInfoByCustomer"+e);
		}
	}
	
	
	/**
	 * 添加insertCustomer
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long insertCustomer(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("customer_id"), row);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj insertCustomer"+e);
		}
	}
	
	/**
	 * 添加account
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long insertAccount(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("account_id"), row);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj insertAccount"+e);
		}
	}
	
	/**
	 * 通过提货关联系统更新：plate_level_product_inventary
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public void updatePlateLevelProductInventaryByShipped(int shippedType,long shippedOrder, DBRow row) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where shipped_order_type = "+shippedType+" and shipped_order = "+shippedOrder, ConfigBean.getStringValue("plate_level_product_inventary"), row);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj updatePlateLevelProductInventaryByShipped"+e);
		}
	}
	
	
	/**
	 * 通过托盘相关信息，看是否存在
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventaryByPlateInfos() throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from plate_level_product_inventary where 1=1");
			
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findPlateLevelProductInventaryByPlateInfos"+e);
		}
	}
	
	
	public DBRow[] findImportPlateNotInPlateInventary() throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select ip.plate_no from import_container_plates ip LEFT JOIN plate_level_product_inventary pi on ip.plate_no = pi.plate_no where pi.associate_order is null");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e){
			 throw new Exception("FloorCheckInMgrZyj findImportPlateNotInPlateInventary"+e);
		}
	}
	
	
	/**
	 * 通过entryID查询Load信息
	 * @param entryId
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadInfosImportContainerInfoByEntryId(long entryId, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from door_or_location_occupancy_details dd");
			sql.append(" join import_container_info ici on dd.ic_id = ici.ic_id");
			sql.append(" where dd.dlo_id = "+entryId);
			sql.append(" and dd.number_type = ").append(ModuleKey.CHECK_IN_LOAD);
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findLoadInfosImportContainerInfoByEntryId"+e);
		}
	}
	
	/**
	 * 通过Entry查询托盘信息
	 * @param entryId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventaryByEntry(long entryId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pi.*, ii.*");
			sql.append(" , ddi.dlo_id entry_in, ddi.number number_in, ddi.number_type number_type_in");
			sql.append(" , ddo.dlo_id entry_out, ddo.number number_out, ddo.number_type number_type_out");
			sql.append(" FROM plate_level_product_inventary pi ");
			sql.append(" JOIN door_or_location_occupancy_details ddi ON ddi.dlo_detail_id = pi.receive_order AND pi.receive_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			sql.append(" LEFT JOIN door_or_location_occupancy_details ddo ON ddo.dlo_detail_id = pi.shipped_order AND pi.shipped_order_type =").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			sql.append(" JOIN import_container_info ii ON ii.ic_id = ddi.ic_id");
			sql.append(" WHERE 1=1");
			if(entryId > 0)
			{
				sql.append(" AND (ddi.dlo_id = ").append(entryId)
				.append(" OR ddo.dlo_id = ").append(entryId).append(")");
			}
			sql.append(" ORDER BY pi.receive_user DESC, pi.staging_area_id, pi.plate_no");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlateLevelProductInventaryByEntry"+e);
		}
	}
	
	
	/**
	 * 通过Entry查询托盘信息
	 * @param entryId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventaryByEntryIn(long entryIdIn) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pi.*");
			sql.append(" , ddi.dlo_id entry_in, ddi.number number_in, ddi.number_type number_type_in");
			sql.append(" FROM plate_level_product_inventary pi ");
			sql.append(" JOIN door_or_location_occupancy_details ddi ON ddi.dlo_detail_id = pi.receive_order AND pi.receive_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			sql.append(" WHERE 1=1");
			if(entryIdIn > 0)
			{
				sql.append(" AND ddi.dlo_id = ").append(entryIdIn);
			}
			sql.append(" ORDER BY pi.receive_user DESC, pi.staging_area_id, pi.plate_no");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlateLevelProductInventaryByEntryIn"+e);
		}
	}
	
	
	/**
	 * 通过Entry查询托盘信息
	 * @param entryId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventaryByEntryOut(long entryIdOut) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pi.*");
			sql.append(" , ddo.dlo_id entry_out, ddo.number number_out, ddo.number_type number_type_out");
			sql.append(" FROM plate_level_product_inventary pi ");
			sql.append(" LEFT JOIN door_or_location_occupancy_details ddo ON ddo.dlo_detail_id = pi.shipped_order AND pi.shipped_order_type =").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			sql.append(" WHERE 1=1");
			if(entryIdOut > 0)
			{
				sql.append(" AND ddo.dlo_id = ").append(entryIdOut);
			}
			sql.append(" AND (pi.shipped_time IS NOT NULL AND shipped_order IS NOT NULL)");
			sql.append(" ORDER BY pi.receive_user DESC, pi.staging_area_id, pi.plate_no");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlateLevelProductInventaryByEntryOut"+e);
		}
	}
	
	
	/**
	 * 通过Entry查询托盘信息
	 * @param entryId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventaryByIcId(long ic_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pi.*, ddi.ic_id");
			sql.append(" , ddi.dlo_id entry_in, ddi.number number_in, ddi.number_type number_type_in");
			sql.append(" FROM plate_level_product_inventary pi ");
			sql.append(" JOIN door_or_location_occupancy_details ddi ON ddi.dlo_detail_id = pi.receive_order AND pi.receive_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			sql.append(" JOIN import_container_info ii ON ii.ic_id = ddi.ic_id");
			sql.append(" WHERE 1=1");
			if(ic_id > 0)
			{
				sql.append(" AND ii.ic_id = ").append(ic_id);
			}
			sql.append(" ORDER BY pi.receive_user DESC, pi.staging_area_id, pi.plate_no");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlateLevelProductInventaryByIcId"+e);
		}
	}
	
	
	/**
	 * 通过Entry查询托盘信息
	 * @param entryId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventaryByIcIdIn(long ic_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pi.*, ddi.ic_id");
			sql.append(" , ddi.dlo_id entry_in, ddi.number number_in, ddi.number_type number_type_in");
			sql.append(" FROM plate_level_product_inventary pi ");
			sql.append(" JOIN door_or_location_occupancy_details ddi ON ddi.dlo_detail_id = pi.receive_order AND pi.receive_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			sql.append(" JOIN import_container_info ii ON ii.ic_id = ddi.ic_id");
			sql.append(" WHERE 1=1");
			if(ic_id > 0)
			{
				sql.append(" AND ii.ic_id = ").append(ic_id);
			}
			sql.append(" ORDER BY pi.receive_user DESC, pi.staging_area_id, pi.plate_no");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlateLevelProductInventaryByIcIdIn"+e);
		}
	}
	
	
	
	/**
	 * 通过Entry查询托盘信息
	 * @param entryId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventaryByIcIdOut(long ic_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pi.*, ddi.ic_id");
			sql.append(" , ddi.dlo_id entry_in, ddi.number number_in, ddi.number_type number_type_in");
			sql.append(" FROM plate_level_product_inventary pi ");
			sql.append(" JOIN door_or_location_occupancy_details ddi ON ddi.dlo_detail_id = pi.receive_order AND pi.receive_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			sql.append(" JOIN import_container_info ii ON ii.ic_id = ddi.ic_id");
			sql.append(" WHERE 1=1");
			if(ic_id > 0)
			{
				sql.append(" AND ii.ic_id = ").append(ic_id);
			}
			sql.append(" AND (pi.shipped_time IS NOT NULL AND shipped_order IS NOT NULL)");
			sql.append(" ORDER BY pi.receive_user DESC, pi.staging_area_id, pi.plate_no");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlateLevelProductInventaryByIcIdOut"+e);
		}
	}
	
	/**
	 * 通过EntryDetail查询托盘信息
	 * @param entryIdDetailIn
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventaryByEntryDetailIn(long entryIdDetailIn) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pi.*");
			sql.append(" FROM plate_level_product_inventary pi ");
			sql.append(" WHERE 1=1");
			if(entryIdDetailIn > 0)
			{
				sql.append(" AND pi.receive_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
				sql.append(" AND pi.receive_order = ").append(entryIdDetailIn);
			}
			sql.append(" ORDER BY pi.receive_user DESC, pi.staging_area_id, pi.plate_no");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlateLevelProductInventaryByEntryDetailIn"+e);
		}
	}
	
	
	/**
	 * 通过EntryDetail查询托盘信息
	 * @param entryIdDetailOut
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventaryByEntryDetailOut(long entryIdDetailOut) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pi.*");
			sql.append(" FROM plate_level_product_inventary pi ");
			sql.append(" WHERE 1=1");
			if(entryIdDetailOut > 0)
			{
				sql.append(" AND pi.shipped_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
				sql.append(" AND pi.shipped_order = ").append(entryIdDetailOut);
			}
			sql.append(" ORDER BY pi.receive_user DESC, pi.staging_area_id, pi.plate_no");
//			System.out.println(sql.toString());
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlateLevelProductInventaryByEntryDetailOut"+e);
		}
	}
	
	/**
	 * 通过entryId获取明细
	 * @param entryId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月4日 上午10:13:23
	 */
	public DBRow[] findEntryDetailsByEntryId(long entryId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select *");
			sql.append(" FROM door_or_location_occupancy_details");
			sql.append(" WHERE dlo_id =  ").append(entryId);
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findEntryDetailsByEntryId"+e);
		}
	}
	
	
	/**
	 * 通过entryDetailId查询托盘信息
	 * @param entryDetailId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventaryInByEntryDetail(long entryDetailId) throws Exception
	{
		try
		{

			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pi.*, ii.*");
			sql.append(" , ddi.dlo_id entry_in, ddi.number number_in, ddi.number_type number_type_in");
			sql.append(" , ddo.dlo_id entry_out, ddo.number number_out, ddo.number_type number_type_out");
			sql.append(" FROM plate_level_product_inventary pi ");
			sql.append(" JOIN door_or_location_occupancy_details ddi ON ddi.dlo_detail_id = pi.receive_order AND pi.receive_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			sql.append(" LEFT JOIN door_or_location_occupancy_details ddo ON ddo.dlo_detail_id = pi.shipped_order AND pi.shipped_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			sql.append(" JOIN import_container_info ii ON ii.ic_id = ddi.ic_id");
			sql.append(" WHERE 1=1");
			if(entryDetailId > 0)
			{
				sql.append(" AND pi.receive_order = ").append(entryDetailId);
				sql.append(" AND pi.receive_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			}
			sql.append(" ORDER BY pi.receive_user DESC, pi.staging_area_id, pi.plate_no");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlateLevelProductInventaryInByEntryDetail"+e);
		}
	}
	
	/**
	 * 通过entryDetailId查询托盘信息
	 * @param entryDetailId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPlateLevelProductInventaryOutByEntryDetail(long entryDetailId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pi.*, ii.*");
			sql.append(" , ddi.dlo_id entry_in, ddi.number number_in, ddi.number_type number_type_in");
			sql.append(" , ddo.dlo_id entry_out, ddo.number number_out, ddo.number_type number_type_out");
			sql.append(" FROM plate_level_product_inventary pi ");
			sql.append(" JOIN door_or_location_occupancy_details ddi ON ddi.dlo_detail_id = pi.receive_order AND pi.receive_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			sql.append(" JOIN door_or_location_occupancy_details ddo ON ddo.dlo_detail_id = pi.shipped_order AND pi.shipped_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			sql.append(" JOIN import_container_info ii ON ii.ic_id = ddo.ic_id");
			sql.append(" WHERE 1=1");
			if(entryDetailId > 0)
			{
				sql.append(" AND pi.shipped_order = ").append(entryDetailId);
				sql.append(" AND pi.shipped_order_type = ").append(ModuleKey.CHECK_IN_ENTRY_DETAIL);
			}
			sql.append(" ORDER BY pi.receive_user DESC, pi.staging_area_id, pi.plate_no");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findPlateLevelProductInventaryOutByEntryDetail"+e);
		}
	}
	
	/**
	 * 通过entryin prupose查询设备
	 * @param equipment_purpose
	 * @param check_in_entry
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午12:08:19
	 */
	public DBRow[] findEquipmentReOccupyByEntryInIdpurpose(int equipment_purpose, long check_in_entry, int equipment_type) throws Exception
	{
		try
		{
			String sql = "select * from entry_equipment ee"
						+ " left join space_resources_relation sr on sr.relation_id = ee.equipment_id and sr.relation_type = 1"
						+ " left join storage_door sd on sd.sd_id = sr.resources_id and sr.resources_type = 1"
						+ " left join storage_yard_control sy on sy.yc_id = sr.resources_id and sr.resources_type = 2 where 1=1";
			if(equipment_purpose > 0)
			{
				sql += " and ee.equipment_purpose = "+equipment_purpose;
			}
			sql += " and ee.check_in_entry_id = " + check_in_entry;
			if(equipment_type > 0)
			{
				sql += " and equipment_type = "+equipment_type;
			}
//			System.out.println("in:"+sql);
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findEquipmentReOccupyByEntryInIdpurpose"+e);
		}
	}
	
	
	/**
	 * 通过entryOut prupose查询设备
	 * @param equipment_purpose
	 * @param check_in_entry
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午12:08:19
	 */
	public DBRow[] findEquipmentReOccupyByEntryOutIdpurpose(int equipment_purpose, long check_in_entry, int equipment_type) throws Exception
	{
		try
		{
			String sql = "select * from entry_equipment ee"
						+ " left join space_resources_relation sr on sr.relation_id = ee.equipment_id and sr.relation_type = 1"
						+ " left join storage_door sd on sd.sd_id = sr.resources_id and sr.resources_type = 1"
						+ " left join storage_yard_control sy on sy.yc_id = sr.resources_id and sr.resources_type = 2"
						+ " where ee.check_in_entry_id != "+check_in_entry+" and ee.check_out_entry_id = " + check_in_entry;
			if(equipment_type > 0)
			{
				sql += " and equipment_type = "+equipment_type;
			}
//			System.out.println("out:"+sql);
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findEquipmentReOccupyByEntryOutIdpurpose"+e);
		}
	}
	
	/**
	 * 添加设备返回ID
	 * @param row
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午12:20:20
	 */
	public long addEquipment(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId("entry_equipment", row);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj addEquipment"+e);
		}
	}
	
	/**
	 * 通过设备ID，查询设备、资源信息
	 * @param id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午6:45:06
	 */
	public DBRow findEquipmentReOccupyById(long id) throws Exception
	{
		try
		{
			String sql = "select * from entry_equipment ee"
					+ " left join space_resources_relation sr on sr.relation_id = ee.equipment_id and sr.relation_type = 1"
					+ " left join storage_door sd on sd.sd_id = sr.resources_id and sr.resources_type = 1"
					+ " left join storage_yard_control sy on sy.yc_id = sr.resources_id and sr.resources_type = 2"
					+ " where ee.equipment_id = "+id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj addEquipment"+e);
		}
	}
	
	
	/**
	 * 更新设备
	 * @param row
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午12:20:20
	 */
	public void updateEquipment(long id , DBRow row) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where equipment_id = "+id, "entry_equipment", row);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj updateEquipment"+e);
		}
	}
	
	/**
	 * 删除设备
	 * @param id
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 下午10:00:03
	 */
	public void deleteEquipment(long id)throws Exception
	{
		try
		{
			dbUtilAutoTran.delete(" where equipment_id = "+id, "entry_equipment");
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj deleteEquipment"+e);
		}
	}
	
	/**
	 * 通过entryId查询entry的task所在的门或停车位
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月26日 下午9:09:45
	 */
	public DBRow[] findTasksOccupysEquipmentsByEntryId(long entry_id)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select IFNULL(sr.resources_type,dd.occupancy_type) occupancy_type,IFNULL(sr.resources_id,dd.rl_id) rl_id,sr.srr_id,dd.*,dm.*, IFNULL(sd.doorId, sy.yc_no) as occupy_name,IFNULL(sd.area_id, sy.area_id) as area_id ");
			sql.append(" , ee.equipment_number, ee.equipment_type, ee.seal_pick_up");
			sql.append(" from door_or_location_occupancy_main  dm ");
			sql.append(" join door_or_location_occupancy_details dd on dm.dlo_id = dd.dlo_id ");
			sql.append(" left join entry_equipment ee on ee.equipment_id = dd.equipment_id");
			sql.append(" left join space_resources_relation sr on sr.relation_id = dd.dlo_detail_id and sr.relation_type = ").append(SpaceRelationTypeKey.Task);
			sql.append(" left join storage_door sd on sd.sd_id=sr.resources_id and sr.resources_type = ").append(OccupyTypeKey.DOOR);
			sql.append(" left join storage_yard_control sy on sy.yc_id=sr.resources_id and sr.resources_type = ").append(OccupyTypeKey.SPOT);
			sql.append(" where dd.dlo_id=").append(entry_id);
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findTasksOccupysEquipmentsByEntryId"+e);
		}
	}
	
	/**
	 * 查询inYardDropOffEquipment
	 * @param equipmentNumber
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 上午8:56:27
	 */
	public DBRow[] findInYardDropOffEquipmentByNumber(String equipmentNumber, int moduleKey, long ps_id,int equipment_type) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select ee.*,sr.*,sd.doorId,sd.sd_id,sd.area_id as door_area_id,sy.yc_no,sy.yc_id,sy.area_id as spot_area_id from entry_equipment ee");
			sql.append(" left join space_resources_relation sr on sr.relation_id = ee.equipment_id and sr.relation_type = 1");
			sql.append(" and sr.module_type = ").append(moduleKey);
			sql.append(" left join storage_door sd on sd.sd_id = sr.resources_id and sr.resources_type = 1");
			sql.append(" left join storage_yard_control sy on sy.yc_id = sr.resources_id and sr.resources_type = 2");
//			sql.append(" where ee.check_out_entry_id is null");
			sql.append(" where 1=1");
			sql.append(" and ee.equipment_number = '").append(equipmentNumber).append("'");
//			sql.append(" and ee.equipment_purpose = 2");
			sql.append(" and ee.check_out_time is null");
			sql.append(" and ee.ps_id = ").append(ps_id);
			sql.append(" and ee.equipment_type = ").append(equipment_type);
			sql.append(" order by ee.equipment_id desc");
 			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findInYardDropOffEquipmentByNumber"+e);
		}
	}
	
	
	/**
	 * 查询inYardDropOffEquipment
	 * @param equipmentNumber
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 上午8:56:27
	 */
	public DBRow[] findInYardDropOffEquipmentByNumberNotThisEntry(String equipmentNumber, int moduleKey, long ps_id, long entry_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select ee.*,sr.*,sd.doorId,sd.sd_id,sd.area_id as door_area_id,sy.yc_no,sy.yc_id,sy.area_id as spot_area_id,dm.company_name as carrier from entry_equipment ee");
			sql.append(" left join space_resources_relation sr on sr.relation_id = ee.equipment_id and sr.relation_type = 1");
			sql.append(" and sr.module_type = ").append(moduleKey);
			sql.append(" left join storage_door sd on sd.sd_id = sr.resources_id and sr.resources_type = 1");
			sql.append(" left join storage_yard_control sy on sy.yc_id = sr.resources_id and sr.resources_type = 2");
			sql.append(" left join door_or_location_occupancy_main dm on dm.dlo_id = ee.check_in_entry_id");
			sql.append(" where 1=1");
			sql.append(" and ee.check_in_entry_id !=").append(entry_id);
			sql.append(" and ee.equipment_number = '").append(equipmentNumber).append("'");
//			sql.append(" and ee.equipment_purpose = 2");
			sql.append(" and ee.check_out_time is null");
			sql.append(" and ee.ps_id = ").append(ps_id);
			sql.append(" order by ee.equipment_id desc");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findInYardDropOffEquipmentByNumberNotThisEntry"+e);
		}
	}
	
	/**
	 * 通过entryId，numberType查询tasks
	 * @param entry_id
	 * @param numberTypes
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月27日 下午8:52:27
	 */
	public DBRow[] findEntryTasksByEntryIdNumberTypes(long entry_id, int[] numberTypes, long equipment_id) throws Exception
	{
		try
		{
			String sql = "select * from door_or_location_occupancy_details where dlo_id = " + entry_id;
			if(numberTypes != null && numberTypes.length > 0)
			{
				sql += " and (number_type = " + numberTypes[0];
				for (int i = 1; i < numberTypes.length; i++) 
				{
					sql += " or number_type = " + numberTypes[i];
				}
				sql += ")";
			}
			if(equipment_id > 0)
			{
				sql += " and equipment_id = " + equipment_id;
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findEntryTasksByEntryIdNumberTypes"+e);
		}
	}
	
	/**
	 * 通过设备ID查task
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月29日 下午5:07:17
	 */
	public DBRow[] findTasksByEquipmentId(long equipment_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select dd.*,sr.* from door_or_location_occupancy_details dd");
			sql.append(" left join entry_equipment ee on ee.equipment_id = dd.equipment_id");
			sql.append(" left join space_resources_relation sr on sr.relation_id = dd.dlo_detail_id and sr.relation_type = ").append(SpaceRelationTypeKey.Task);
			sql.append(" where ee.equipment_id = " + equipment_id);
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findTasksByEquipmentId"+e);
		}
	}
	
	/**
	 * 通过DetailID查task
	 * @param detail_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月29日 下午5:07:17
	 */
	public DBRow findTasksByTaskDetailId(long detail_id) throws Exception
	{
		try
		{
			String sql = "select * from door_or_location_occupancy_details where dlo_detail_id = " + detail_id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findTasksByTaskDetailId"+e);
		}
	}
	
	/**
	 * 通过明细ID，查询明细设备资源
	 * @param detail_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月2日 下午3:10:08
	 */
	public DBRow findTaskEquipmentResourceByDetailId(long detail_id) throws Exception
	{
		try
		{
			String sql = "select ee.*,sr.*,ee.*,dd.* from door_or_location_occupancy_details dd"
					+ " left join space_resources_relation sr on sr.relation_id = dd.dlo_detail_id and sr.relation_type = 2"
					+ " left join storage_door sd on sd.sd_id = sr.resources_id and sr.resources_type = 1"
					+ " left join storage_yard_control sy on sy.yc_id = sr.resources_id and sr.resources_type = 2"
					+ " left join entry_equipment ee on ee.equipment_id = dd.equipment_id"
					+ " where dlo_detail_id = " + detail_id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj findTaskEquipmentResourceByDetailId"+e);
		}
	}
	
	/**
	 * 添加order信息
	 * @param row
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月9日 上午10:00:33
	 */
	public long addWmsOrderInfo(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("wms_order_info"), row);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj addWmsOrderInfo"+e);
		}
	}
	
	/**
	 * 通过task查询orderInfos
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月9日 上午10:10:40
	 */
	public DBRow[] findWmsOrderInfosByTaskId(long taskId) throws Exception
	{
		try
		{
			String sql = "select * from wms_order_info where task_detail_id = " + taskId;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj addWmsOrderInfo"+e);
		}
	}
	
	/**
	 * 通过TaskId删除orders
	 * @param taskId
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月9日 下午2:52:08
	 */
	public void deleteWmsOrderInfosByTaskId(long taskId) throws Exception
	{
		try
		{
			dbUtilAutoTran.delete(" where task_detail_id = " + taskId, ConfigBean.getStringValue("wms_order_info"));
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrZyj addWmsOrderInfo"+e);
		}
	}
	
	
	/**
	 * 更新没有windowCheckIn时间的所有设备
	 * @param row
	 * @param entry_id
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月13日 下午12:39:21
	 */
	public void updateEntryEquipmentNoWindowCheckInTime(DBRow row, long entry_id) throws Exception
	{
		try{
			dbUtilAutoTran.update(" where check_in_entry_id = " + entry_id + " and check_in_window_time is null"
					, ConfigBean.getStringValue("entry_equipment"), row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZyj updateEntryEquipmentNoWindowCheckInTime" + e);
		}
	}
	
	/**
	 * 通过账号ID查询账号名
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月17日 下午5:36:02
	 */
	public DBRow findAdminById(long adid) throws Exception
	{
		try{
			return dbUtilAutoTran.selectSingle("select * from admin where adid = "+adid);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZyj findAdminById" + e);
		}
	}
	
	/**
	 * 通过单据ID查询task
	 * @param os_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月18日 下午3:04:21
	 */
	public DBRow[] findTaskByOsid(long os_id) throws Exception
	{
		try{
			return dbUtilAutoTran.selectMutliple("select * from door_or_location_occupancy_details where lr_id="+os_id);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZyj findAdminById" + e);
		}
	}
	
	/**
	 * 通过number，resource，requpment等查询task
	 * @param number
	 * @param number_type
	 * @param equipment_id
	 * @param resource_type
	 * @param resource_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月19日 上午11:59:20
	 */
	public DBRow[] findTasksByTaskNumberEquipmentOccupys(String number, int number_type, long equipment_id, int resource_type, long resource_id, long entry_id, String company_id, long receipt_no) throws Exception
	{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from door_or_location_occupancy_details dd");
			sql.append(" left join space_resources_relation sr on sr.relation_id = dd.dlo_detail_id and sr.relation_type = ").append(SpaceRelationTypeKey.Task);
			sql.append(" where 1=1");
			if(entry_id > 0)
			{
				sql.append(" and dd.dlo_id = ").append(entry_id);
			}
			if(number_type > 0)
			{
				sql.append(" and dd.number_type = ").append(number_type);
			}
			if(!StrUtil.isBlank(number))
			{
				sql.append(" and dd.number = '").append(number).append("'");
			}
			if(equipment_id > 0)
			{
				sql.append(" and dd.equipment_id = ").append(equipment_id);
			}
			if(resource_type > 0)
			{
				sql.append(" and sr.resources_type = ").append(resource_type);
			}
			if(resource_id > 0)
			{
				sql.append(" and sr.resources_id = ").append(resource_id);
			}
			if(!StrUtil.isBlank(company_id))
			{
				sql.append(" and dd.company_id = '").append(company_id).append("'");
			}
			if(receipt_no > 0)
			{
				sql.append(" and receipt_no=").append(receipt_no);
			}
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZyj findTasksByTaskNumberEquipmentOccupys" + e);
		}
	}
	
	/**
	 * 查某个entry下未离开的设备及task信息
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月31日 上午11:25:44
	 */
	public DBRow[] getNotLeftEquipmentByEntryAndTotalTask(long entry_id) throws Exception{
		try{
			String sql = "select eq.* , srr.*, count(details.dlo_detail_id) as total_task from entry_equipment as eq "
					+ " LEFT JOIN  door_or_location_occupancy_details as details  on details.equipment_id = eq.equipment_id"
					+"  LEFT JOIN space_resources_relation as srr on srr.relation_id =  eq.equipment_id and srr.relation_type ="+ SpaceRelationTypeKey.Equipment
					+ " where eq.check_in_entry_id  = "+entry_id
					+ " and (eq.equipment_status = "+CheckInMainDocumentsStatusTypeKey.UNPROCESS+" or eq.equipment_status = "+CheckInMainDocumentsStatusTypeKey.PROCESSING
					+ " or eq.equipment_status = "+CheckInMainDocumentsStatusTypeKey.INYARD+ " or eq.equipment_status = "+CheckInMainDocumentsStatusTypeKey.LEAVING+")"
					+" GROUP BY eq.equipment_id ORDER BY eq.equipment_type, eq.equipment_purpose";
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getEquipmentByEntryAndTotalTask(entry_id)"+e);
		}
	}
	
	/**
	 * 通过entryId获取task数量
	 * @param entryId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月5日 下午5:16:56
	 */
	public int findTaskCountByEntryId(long entryId) throws Exception
	{
		try
		{
			String sql = "select count(*)cn from door_or_location_occupancy_details where dlo_id = "+entryId;
			return dbUtilAutoTran.selectSingle(sql).get("cn", 0);
		}
		catch(Exception e)
		{
			throw new Exception("FloorCheckInMgrZr.findTaskCountByEntryId(entryId)"+e);
		}
	}
	/**
	 * 查询gate发给window的通知
	 * @param entryId
	 * @return
	 * @throws Exception
	 * @author:xujia
	 * @date: 2015年1月5日 下午5:16:56
	 */
	public DBRow[] findWindowSchedule(long entryId,long detail_id) throws Exception
	{
		try
		{
			String sql = "select * from " +  ConfigBean.getStringValue("schedule")  + " where "
					   + "((associate_process ="+ProcessKey.GateHasTaskNotifyWindow+" and associate_id = "+detail_id+") "
					   + "or (associate_process ="+ProcessKey.GateNoTaskNotifyWindow+" and associate_id = "+entryId+")) "
					   + "and associate_type="+ModuleKey.CHECK_IN + " and associate_main_id="+entryId+"  and schedule_state < 10" ;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch(Exception e)
		{
			throw new Exception("FloorCheckInMgrZyj findWindowSchedule"+e);
		}
	}
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	

}
