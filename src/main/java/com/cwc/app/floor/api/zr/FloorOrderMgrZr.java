package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorOrderMgrZr {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
	public DBRow getClientRowByEmailId(String email_id) throws Exception{
		try{
			String sql = "select * from " +  ConfigBean.getStringValue("trade_client") + " where client_id = '" + email_id +"'";
		 
			return dbUtilAutoTran.selectSingle(sql);		
		}catch(Exception e){
			throw new Exception("FloorOrderMgrZr.getClientRowByEmailId(row):"+e);
		}
	}
	public DBRow getClientRowByTxnId(String txn_id) throws Exception{
		
		try{
			String sql = "select * from " +  ConfigBean.getStringValue("trade") + " where client_id = '" + txn_id +"'";
			return dbUtilAutoTran.selectSingle(sql);		
		}catch(Exception e){
			throw new Exception("FloorOrderMgrZr.getClientRowByTxnId(row):"+e);
		}
	}
	public void updateClientByClientIdAndRow(String client_id , DBRow updateRow) throws Exception{
		try{
			dbUtilAutoTran.update(" where client_id= '" + client_id+"'" ,ConfigBean.getStringValue("trade_client"), updateRow);
		}catch(Exception e){
			throw new Exception("FloorOrderMgrZr.updateClientByClientIdAndRow(row):"+e);
		}
	}
	
	public void insertClientByRow(DBRow insertRow) throws Exception {
		try{
			dbUtilAutoTran.insert(ConfigBean.getStringValue("trade_client"), insertRow);
 		}catch(Exception e){
			throw new Exception("FloorOrderMgrZr.insertClientByRow(row):"+e);
		}
	}
	public DBRow[] getClientByEmail(String email) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("trade_address_info");
			String sql = "select * from " + tableName + " where 1=1 and client_id='" + email+"'";
			return dbUtilAutoTran.selectMutliple(sql);
 		}catch(Exception e){
			throw new Exception("FloorOrderMgrZr.getClientByEmail(row):"+e);
		}
	}
	public long insertClientAddressInfo(DBRow insertRow) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("trade_address_info");
			return dbUtilAutoTran.insertReturnId(tableName, insertRow);
 		}catch(Exception e){
			throw new Exception("FloorOrderMgrZr.insertClientAddressInfo(row):"+e);
		}
	}
	public long insertOrderDBRow(DBRow insertDBRow) throws Exception {
		try{
			long oid =  dbUtilAutoTran.getSequance(ConfigBean.getStringValue("porder"));
			insertDBRow.add("oid", oid);
			String tableName = ConfigBean.getStringValue("porder");
			dbUtilAutoTran.insert(tableName, insertDBRow);
			return oid;
 		}catch(Exception e){
			throw new Exception("FloorOrderMgrZr.insertOrderDBRow(row):"+e);
		}
	}
	public long insertTrade(DBRow insertDBRow) throws Exception {
		try{
			 
			String tableName = ConfigBean.getStringValue("trade");
			long id = dbUtilAutoTran.insertReturnId(tableName, insertDBRow);
			return id; 
 		}catch(Exception e){
			throw new Exception("FloorOrderMgrZr.insertTradeItem(row):"+e);
		}
	}
	public long insertTradeItem(DBRow insertDBRow) throws  Exception {
		try{
			 
			String tableName = ConfigBean.getStringValue("trade_item");
			long id = dbUtilAutoTran.insertReturnId(tableName, insertDBRow);
			return id; 
 		}catch(Exception e){
			throw new Exception("FloorOrderMgrZr.insertTradeItem(row):"+e);
		}
	}
	public DBRow[] getAllTrade(PageCtrl page) throws Exception {
		try{
			 
		//	String sql = "select * from trade , trade_address_info , trade_client where trade_client.client_id =  trade.client_id and trade_address_info.address_id = trade.address_id";
			String sql = "select * from trade where 1=1 order by trade_id desc";
			
			return dbUtilAutoTran.selectMutliple(sql, page);
 		}catch(Exception e){
			throw new Exception("FloorOrderMgrZr.insertTradeItem():"+e);
		}
	}
	public DBRow[] getQuartationOrderAndItemById(long id) throws Exception {
		try{ 
			String sql = "select * from quote_order_item where quote_order_item.qoid = " + id;
			return dbUtilAutoTran.selectMutliple(sql);
 		}catch(Exception e){
			throw new Exception("FloorOrderMgrZr.getQuartationOrderAndItemById(id):"+e);
		}
	}
	public DBRow[] getWarrantyOrderAndItemById(long id) throws Exception {
		try{
			 
			String sql = "select * from warranty_order_items where warranty_order_items.wo_id = " + id;
			return dbUtilAutoTran.selectMutliple(sql);
 		}catch(Exception e){
			throw new Exception("FloorOrderMgrZr.getWarrantyOrderAndItemById(id):"+e);
		}
	}
	public DBRow getProductById(long pc_id) throws Exception {
		try{
			String sql = "select * from  "+ConfigBean.getStringValue("product")+" where pc_id = " + pc_id;
			return dbUtilAutoTran.selectSingle(sql);
 		}catch(Exception e){
			throw new Exception("FloorOrderMgrZr.getProductById(id):"+e);
		}
	}
	public DBRow getProductProfit(long ccid , long pc_id) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("product_profit");
			String sql  = "select * from " + tableName + " as pp "
						 +" join country_area_mapping as cam on cam.ca_id = pp.ca_id and cam.ccid = "+ccid
						 +" where pp.pid="+pc_id;
			return dbUtilAutoTran.selectSingle(sql);
 		}catch(Exception e){
			throw new Exception("FloorOrderMgrZr.getProductProfit(id):"+e);
		}
	}
	
	
	public DBRow[] getSetProductsBy(long setId) throws Exception{
		try{
			String sql = "select * from product_union where set_pid =" + setId;
			return dbUtilAutoTran.selectMutliple(sql);	
		}catch (Exception e) {
			throw new Exception("FloorOrderMgrZr.getSetProductsBy(id):"+e);
		}
	}
	
	public DBRow[] getAddressInfoListByClientId(String client_id) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("trade_address_info");
			// String sql =  "select * from " + tableName + " where client_id ='" + client_id + "'";
			 String sql = "select distinct trade.delivery_address_state as address_state,trade.delivery_address_city as address_city, trade.delivery_address_country as address_country,trade.delivery_address_country_code as address_country_code,trade.delivery_address_name as address_name,trade.delivery_address_status as address_status,trade.delivery_address_street as address_street,trade.delivery_address_zip as address_zip from trade where  trade.client_id =  '"+client_id+"';";
	 	//System.out.println("sql : "+ sql);
			return dbUtilAutoTran.selectMutliple(sql);	
		}catch (Exception e) {
			throw new Exception("FloorOrderMgrZr.getAddressInfoListByClientId(id):"+e);
		}
	}
	 
	public DBRow getDBRowByIdAndTableName(long id , String tableName , String column) throws Exception{
		try{
			String sql = "select * from " + tableName + " where " +column + " =" + id;	
		
			return  dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorOrderMgrZr.getDBRowByIdAndTableName(id,tableName,column):"+e);
		}
	}
	
	
	public DBRow[] getBillItemByBillOrderId(long billId) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("bill_order_item"); 
			String sql = "select * from "  + tableName + " where bill_id =" + billId;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getBillItemByBillOrderId(pc):"+e);
		}
	}
	public DBRow getTradeBillByTxnId(String txnId) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("trade"); 
			String sql = "select * from "  + tableName + " where txn_id ='" + txnId + "'";
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getTradeBillByTxnId(txnId):"+e);
		}
		
	}
	public DBRow getTradeBillByTxnIdAndPaymentStatus(String txnId, String paymentStatus) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("trade"); 
			String sql = "select * from "  + tableName + " where txn_id ='" + txnId + "' and payment_status='" + paymentStatus+"'";
 			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getTradeBillByTxnIdAndPaymentStatus(txnId,paymentStatus):"+e);
		}
		
	}
	public DBRow getTradeBillByTxnIdAndCreateOrder(String txnId) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("trade"); 
			String sql = "select * from "  + tableName + " where txn_id ='" + txnId + "' and is_create_order =1";
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getTradeBillByTxnIdAndCreateOrder(txnId):"+e);
		}
		
	}
	public DBRow getTradeByOrderIdAndIsCreateOrder(long orderId) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("trade");
			String sql = "select * from "  + tableName + " where relation_id ='" + orderId + "' and is_create_order =1";
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getTradeByOrderIdAndIsCreateOrder(orderId):"+e);
		}
		
		
	}
	public void updateTradeByTradeId(DBRow updateRow , long tradeId) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("trade"); 
			dbUtilAutoTran.update(" where trade_id=" + tradeId, tableName, updateRow); 
			 
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.updateTradeByTradeId(updateRow,tradeId):"+e);
		}
	}
	public int getOrderByOid(long oid) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("porder"); 
			String sql = "select count(*) as s from " + tableName +" where oid =" + oid;
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			int count =  0 ;
			count = row.get("s", 0);
			return count ;
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getOrderByOid(oid):"+e);
		}
	}
	public DBRow getOrderByTxnId(String txnId) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("porder"); 
			String sql = "select * from " + tableName + " where txn_id ='" + txnId+"'";
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getOrderByTxnId(txnId):"+e);
		}
	}
	public DBRow getOrderRowByOrderId(long oid) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("porder"); 
			String sql = "select * from " + tableName + " where  oid=" + oid;
			return dbUtilAutoTran.selectSingle(sql); 
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getOrderRowByOrderId(oid):"+e);
		}
	}
	public DBRow[] getRelationTradeByTxnId(long oid , boolean isAll) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("trade"); 
			
			String sql = "select * from " + tableName + " where relation_type=0 and relation_id=" + oid ;
			if(!isAll){
				sql += " limit 0,5";
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getOrderByTxnId(txnId):"+e);
		}
	}
	public DBRow[] getTradeItemsByOid(long oid) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("trade_item"); 
			
			String sql = "select * from " + tableName + " where oid=" + oid ;
			 
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getTradeItemsByOid(oid):"+e);
		}
	}
	public DBRow[] getNeedFixRow() throws Exception{
		try{
			 String sql = "select trade.payment_channel ,trade.trade_id , porder.pay_type from trade,porder where porder.oid  = trade.relation_id and pay_type >= 0 ;";
			 
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getTradeItemsByOid(oid):"+e);
		}
	}
	public DBRow[] getTradeByTxnIdAndIsPending(String txnId) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("trade") + " where txn_id ='" + txnId+"' and payment_status = 'Pending' ;";
			 
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getTradeByTxnIdAndIsPending(oidtxnId):"+e);
		}
	}
	public void updatePendingRows(String txnId ,long relationId) throws Exception {
		try{
			DBRow data = new DBRow();
			data.add("relation_type", 0);
			data.add("relation_id", relationId);
			dbUtilAutoTran.update("where txn_id='"+txnId+"'", ConfigBean.getStringValue("trade"), data);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.updatePendingRows(txnId,relationId):"+e);
		}
		
	}
	public void updatePendingIsCreate(String txnId , int isCreateOrder ) throws Exception{
		try{
			DBRow data = new DBRow();
		 
			data.add("is_create_order", isCreateOrder);
			dbUtilAutoTran.update("where txn_id='"+txnId+"' and payment_status='Pending'", ConfigBean.getStringValue("trade"), data);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.updatePendingIsCreate(txnId,isCreateOrder):"+e);
		}
	}
	
	public DBRow[] filterTrade(PageCtrl pc , String st , String end , int isCreateOrder,long relationId , String paymentStatus,String caseType,String reasonCode)throws Exception{ 
		try{
			StringBuffer sql = new StringBuffer("select * from " +  ConfigBean.getStringValue("trade") + " where 1=1 ");
			if(st.length() > 0){
				sql.append(" and post_date >='").append(st.trim()+" 00:00:00'");
			}
			if(end.length() > 0){
				sql.append(" and post_date <='").append(end.trim() +" 59:59:59'" );
			}
			if(relationId != 0l){
				sql.append(" and relation_id=").append(relationId);
			}
			if(paymentStatus.length() > 0 && !paymentStatus.equals("all")){
				sql.append(" and payment_status = '").append(paymentStatus).append("'");
			}
			if(caseType.length() > 0 && !caseType.equals("all")){
				sql.append(" and case_type='").append(caseType).append("' ");
			}
			if(reasonCode.length() > 0 && !reasonCode.equals("all")){
				sql.append(" and reason_code ='").append(reasonCode).append("' ");
			}
			sql.append(" order by trade_id desc");
			
			 
			
			 
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
 		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.filterTrade(pc, st, end,isCreateOrder,relationId,paymentStatus,caseType,reasonCode):"+e);
		}
	}
	public DBRow getTradeByTradeId(long tradeId) throws Exception{
		try{
			 String sql = "select * from "  + ConfigBean.getStringValue("trade") + " where trade_id =" + tradeId;
			 return dbUtilAutoTran.selectSingle(sql);
 		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getTradeByTradeId(tradeId):"+e);
		}
	}
}