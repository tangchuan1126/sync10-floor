package com.cwc.app.floor.api.ll;

import com.cwc.app.beans.ll.AssetsBeanLL;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class FloorAssetsMgrLL {
	public AssetsBeanLL assetsBeanLL;

	public AssetsBeanLL getAssetsBeanLL() {
		return assetsBeanLL;
	}

	public void setAssetsBeanLL(AssetsBeanLL assetsBeanLL) {
		this.assetsBeanLL = assetsBeanLL;
	}
	
	public DBRow[] getRowsByPara(DBRow para, DBRow operator,DBRow orAnd, String orderBy,PageCtrl pc) throws Exception {
		return assetsBeanLL.getRowsByPara(para, operator,orAnd, orderBy, pc);
	}
}
