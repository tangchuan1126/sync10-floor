package com.cwc.app.floor.api.tjh;


import com.cwc.app.util.ConfigBean;
import com.cwc.db.*;

public class FloorAssetsMgr {
	
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 分页查询资产信息
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] AssetsList(PageCtrl pc,String type) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("assets") + " where 1=1";
			if(type.equals("1")) {
				sql += " and category_id <> 100023";
			}else if(type.equals("2")) {
				sql += " and category_id = 100023";
			}
			sql += " order by aid desc";
			if(pc != null )
			{
				return (dbUtilAutoTran.selectMutliple(sql, pc));
			}
			else
			{
				return (dbUtilAutoTran.selectMutliple(sql));
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorAssetsMgr.AssetsList(PageCtrl pc) error:"+e);
		}
		
	}
	
	/**
	 * 添加资产信息
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addAssets(DBRow row) 
		throws Exception 
	{
		try 
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("assets"));
			row.add("aid", id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("assets"), row);
			return (id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorAssetsMgr.addAssets(DBRow row) error:"+e);
		}
	}
	
	/**
	 * 获取某一具体的资产详细信息
	 * @param aid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailAssets(long aid) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("assets")+" where aid="+aid;
			return dbUtilAutoTran.selectSingle(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorAssetsMgr.getDetailAssets(long aid) error:"+e);
		}
	}

	/**
	 * 获取某一具体的资产详细信息
	 * @param aid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAccounts() 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("account");
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorAssetsMgr.getAccount() error:"+e);
		}
	}
	public DBRow[] getAdminGroup() 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("admin_group");
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorAssetsMgr.getAdminGroup() error:"+e);
		}
	}
	public DBRow[] getAdmin() 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("admin");
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorAssetsMgr.getAdmin() error:"+e);
		}
	}
	/**
	 * 修改某一条资产信息
	 * @param id
	 * @param row
	 * @throws Exception
	 */
	public void updateAssets(long id, DBRow row) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.update("where aid="+id, ConfigBean.getStringValue("assets"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("updateAssets(id,row) error:"+e);
		}
		
	}
	
	/**
	 * 删除一条资产信息
	 * @param aid
	 * @throws Exception 
	 */
	public void delAssets(long aid) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.delete("where aid="+aid, ConfigBean.getStringValue("assets"));
		} 
		catch (Exception e) 
		{
			throw new Exception("delAssets(aid) error:"+e);
		}
	}
	
	/**
	 * 根据资产类别过滤资产信息
	 * @param categoryId
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAssetsByCategoryIdAndLocationId(int state,int goodsState,long locationId,long categoryId, PageCtrl pc, long productLineId, String type) 
	throws Exception 
{
	try 
	{		
		String mysql = "select * from "+ConfigBean.getStringValue("assets")+" where 1=1";
		if(categoryId != 0)
		{
			mysql+=" and category_id in (select id from "+ConfigBean.getStringValue("assets_category")+" where parentid in (select id from "+ConfigBean.getStringValue("assets_category")+" where id="+categoryId+" or parentid = "+categoryId+") or id="+categoryId+")";
		}
		if(locationId != 0)
		{
			mysql+= " and location_id in(select id from "+ConfigBean.getStringValue("office_location")+" where parentid in(select id from "+ConfigBean.getStringValue("office_location")+" where parentid = "+locationId+" or id="+locationId+") or id="+locationId+")";
		}
		if(state != -1)
		{
			mysql+= " and state="+state;
		}
		if(goodsState != -1)
		{
			mysql+= " and goodsState="+goodsState;
		}
		if(type.equals("1")) {
			mysql+= " and category_id <> 100023";
		}else if(type.equals("2")) {
			mysql+= " and category_id = 100023";
		}
		if(productLineId != 0) {
			mysql+= " and product_line_id = " + productLineId;
		}
		mysql +=" order by aid,purchase_Date desc";
		return dbUtilAutoTran.selectMutliple(mysql, pc);
	} 
	catch (Exception e) 
	{
		throw new Exception("getAssetsByCategoryId(categoryId,pc) error:"+e);
	}		
}

	
	/**
	 * 根据查询字符查询相关的资产信息
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchAssets(String key, PageCtrl pc, String type) 
		throws Exception 
	{
		
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("assets")+" where (a_name like '"+key+"%' or aid like '"+key+"%' or requisitioned = '"+key+"')";
			if(type.equals("1")) {
				sql += " and category_id <> 100023";
			}else if(type.equals("2")) {
				sql += " and category_id = 100023";
			}
			sql += " order by aid,purchase_Date desc";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("getSearchAssets(key,pc) error:"+e);
		}
	}

	/**
	 * 批量修改资产信息
	 * @param aid
	 * @param rows
	 * @throws Exception
	 */
	public void batchModAssets(String aid, DBRow rows) 
		throws Exception 
	{
		try {
			if (aid != null) 
			{
					long assetsId = Long.parseLong(aid);
					
					dbUtilAutoTran.update("where aid ="+assetsId, ConfigBean.getStringValue("assets"), rows);
			}
		} catch (Exception e) {
			throw new Exception("batchModAssets(aids,rows) error:"+e);
		}
		
	}
	
	/**
	 * 根据类别查询所有的资产信息(导出资产查询方法)
	 * @param category_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] exportAssets(int location_id,int product_line_id,int categoryId,int state,int goodsState,String type) 
		throws Exception 
	{
		try 
		{
			String sql = "select ass.*,adm.employe_name,off.office_name,cat.chName,line.name from "+ConfigBean.getStringValue("assets")+" as ass left join "+ConfigBean.getStringValue("admin")+" as adm  on ass.creater_id=adm.adid left join "+ConfigBean.getStringValue("office_location")+" as off " +
					" on ass.location_id=off.id left join "+ConfigBean.getStringValue("product_line_define")+" as line on ass.product_line_id=line.id left join " +ConfigBean.getStringValue("assets_category")+ " as cat on ass.category_id=cat.id where 1=1";
		//	DBRow row = new DBRow();
			if(location_id!=0){
				sql+= " and ass.location_id in(select id from "+ConfigBean.getStringValue("office_location")+" where parentid in(select id from "+ConfigBean.getStringValue("office_location")+" where parentid = "+location_id+" or id="+location_id+") or id="+location_id+")";
			}
			if(product_line_id!=0){
				sql+= " and ass.product_line_id = " + product_line_id;
			}
			if(categoryId != 0)
			{
				sql+=" and category_id in (select id from "+ConfigBean.getStringValue("assets_category")+" where parentid in (select id from "+ConfigBean.getStringValue("assets_category")+" where id="+categoryId+" or parentid = "+categoryId+") or id="+categoryId+")";
			}
			if(state != -1)
			{
				sql+= " and state="+state;
			}
			if(goodsState != -1)
			{
				sql+= " and goodsState="+goodsState;
			}
			if(type.equals("1")) {
				sql+= " and ass.category_id <> 100023";
			}else if(type.equals("2")) {
				sql+= " and ass.category_id = 100023";
			}

				return dbUtilAutoTran.selectMutliple(sql);

			
		} 
		catch (Exception e) 
		{
			throw new Exception("exportAssets(category_id) error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}


}
