package com.cwc.app.floor.api.zj;

import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.key.ProductTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorProductMgrZJ 
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 获得导出商品数据
	 * @param catalog_id
	 * @param uniog_flag
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getExportProduct(long catalog_id)
	throws Exception
	{
		try 
		{
			String whereCatalog = "";
			if(catalog_id>0)
			{
				whereCatalog = " join pc_child_list as pcl on pcl.pc_id = p.catalog_id and pcl.search_rootid = "+catalog_id+" ";
			}
			
			String sql = "select * from "+ConfigBean.getStringValue("product")+" as p "
						+"join product_code as pcode on pcode.pc_id = p.pc_id "
						+whereCatalog;
			
			
			return (dbUtilAutoTran.selectMutliple(sql.toString()));
			
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ.getExportProduct error:"+e);
		}
		 
	}

	/**
	 * 导出商品检索
	 * @param catalog_id
	 * @param union_flag
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getExportProductShow(long catalog_id,PageCtrl pc)
	throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("product")+" where orignal_pc_id = 0 ");
			
			
			if(catalog_id!=0)
			{
				DBRow para = new DBRow();
				
				sql.append("and catalog_id in (select id from "+ConfigBean.getStringValue("product_catalog")+" where parentid in(select id from "+ConfigBean.getStringValue("product_catalog")+" where id=? or parentid=?) or id =?)");
				
				para.add("catalog_id",catalog_id);
				para.add("parent_id",catalog_id);
				para.add("id",catalog_id);
				
				return (dbUtilAutoTran.selectPreMutliple(sql.toString(),para,pc));
			}
			
			return (dbUtilAutoTran.selectMutliple(sql.toString(),pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ.getExportProduct error:"+e);
		}
	}
	
	/**
	 * 根据商品获得关系
	 * @param products
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getExportProdutUnion(long catalog_id,long product_line_id)
	throws Exception
	{
		try 
		{
			
			String productline="";
			if(product_line_id>0)
			{
				productline = " and pc.product_line_id="+product_line_id+" ";
			}
			
			String cid = "";
			if(catalog_id>0)
			{
				cid = "join pc_child_list as pcl on pc.id = pcl.pc_id and search_rootid="+catalog_id+" ";
			}
			
			//暂未修改
			String sql = "select p.p_name as product ,ps.p_name as accessories ,pu.quantity as quantity  from `product_union` as pu  " 
						+"join product as p on pu.set_pid = p.pc_id and p.orignal_pc_id = 0 "  
						+"join product_catalog as pc on pc.id = p.catalog_id "+productline	
						+cid
						+"join product as ps on  pu.pid = ps.pc_id and ps.orignal_pc_id=0 "
						+" order by pu.set_pid";
			
			return (dbUtilAutoTran.selectMutliple(sql));
			
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZj.getExportProdutUnion error:"+e);
		}
	}
	
	/**
	 * 根据配件ID获得使用此商品为配件的套装
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public int getSuitByPid(long pc_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select count(1) as counts from "+ConfigBean.getStringValue("product_union")+" where pid = ?";
			DBRow para = new DBRow();
			para.add("pid",pc_id);
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			return (result.get("counts",0));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ.getSuitByPid error:"+e);
		}
	}
	
	/**
	 * 根据套装ID，获得组装后的套装价格与重量
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow UpdateSuitPara(long pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select p.pc_id,sum(ps.weight*pu.quantity) as weight,sum(ps.unit_price*pu.quantity) as unit_price from product as p"
						+" inner join product_union as pu on p.pc_id = pu.set_pid"
						+" inner join product as ps on pu.pid = ps.pc_id"
						+" where p.pc_id = ?"
						+" group by p.pc_id";
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ.UpdateSuitPara error:"+e);
		}
	}
	
	/**
	 * 根据套装商品ID，解除套装关系
	 * @param set_pid
	 * @return
	 * @throws Exception
	 */
	public int delProductUnionBySetPid(long set_pid)
		throws Exception
	{
		try 
		{
			return (dbUtilAutoTran.delete("where set_pid="+set_pid,ConfigBean.getStringValue("product_union")));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ.delProductUnionBySetPid error:"+e);
		}
	}
	
	/**
	 * 根据仓库ID获得仓库位置信息
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLocationCatalogByPsid(long ps_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_location_catalog")+" where slc_psid = ?";
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorProductMgrZJ.getStorageLocationCatalogByPsid error:"+e);
		}
	}
	
	/**
	 * 根据商品名商品条码查询商品详细
	 * @param pname
	 * @param pcode
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductByPnamePcode(String pname,String pcode)
		throws Exception
	{
		String sql = "select p.* from "+ConfigBean.getStringValue("product")+" as p "
					+"join product_code as pcode on p.pc_id = pcode.pc_id and pcode.p_code =?"
					+"where p.p_name =?";
		DBRow para = new DBRow();
		para.add("pcode",pcode);
		para.add("pname",pname);
		
		
		return dbUtilAutoTran.selectPreSingle(sql, para);
	}
	
	/**
	 * 模糊查询商品明细
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDetailProductLikeSearch(String key,PageCtrl pc)
		throws Exception
	{
		try 
		{
			
			String sql = "select p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,p.pc_id,pcode_main.p_code as p_code,p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width from product as p "
						+"join "+ConfigBean.getStringValue("product_code")+" as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type="+CodeTypeKey.Main+" "
						+"where p_name like '%"+key+"%' " 
						+" union "
						+"select p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,p.pc_id,pcode_main.p_code as p_code,p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width from product as p "
						+"join product_code as pcode on p.pc_id = pcode.pc_id and pcode.p_code like '%"+key+"%' "
						+"join "+ConfigBean.getStringValue("product_code")+" as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type="+CodeTypeKey.Main+" ";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ getDetailProductLikeSearch error:"+e);
		}
	}
	
	/**
	 * 根据采购单ID查询包含在采购单内的商品
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductsByPurchaseId(long purchase_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select p.*,pd.purchase_count as count from "+ConfigBean.getStringValue("product")+" as p,"+ConfigBean.getStringValue("purchase_detail")+" as pd where p.pc_id = pd.product_id and pd.purchase_id = ?";
			
			DBRow para = new DBRow();
			para.add("purchase_id",purchase_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para,pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ.getProductsByPurchaseId error:"+e);
		}
	}
	
	/**
	 * 在采购单商品内根据关键字检索
	 * @param purchase_id
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchProductsByPurchaseIdKey(long purchase_id,String key,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "SELECT p.*, pd.purchase_count AS count FROM	product AS p "
						+"left join product_code as pcode on p.pc_id = pcode.pc_id and pcode.p_code like '%"+key+"%' "
						+"join purchase_detail AS pd on p.pc_id = pd.product_id and pd.purchase_id=? "
						+"where p.p_name like '%"+key+"%'";
			DBRow para = new DBRow();
			para.add("purchase_id",purchase_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para,pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ.searchProductsByPurchaseIdKey error:"+e);
		}
	}
	
	public void localMysql(String[] s)
		throws Exception
	{
		for(int q= 0;q<s.length;q++)
		{
			try 
			{
				DBRow c = new DBRow();
				long slc_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("storage_location_catalog"));
				c.add("slc_id",slc_id);
				c.add("slc_position_all",s[q]);
				if(s[q].substring(0,2).toUpperCase().equals("BJ"))
				{
					c.add("slc_psid",100000);
				}
				if(s[q].substring(0,2).toUpperCase().equals("LA"))
				{
					c.add("slc_psid",100043);
				}
				if(s[q].substring(0,2).toUpperCase().equals("GZ"))
				{
					c.add("slc_psid",100006);
				}
				
				c.add("slc_ps_title",s[q].substring(0,2).toUpperCase());
				c.add("slc_area",s[q].substring(2,5));
				c.add("slc_type",s[q].substring(5,6).toUpperCase());
				c.add("slc_position",s[q].substring(6));
				
				dbUtilAutoTran.insert(ConfigBean.getStringValue("storage_location_catalog"),c);
				
			} 
			catch (Exception e) 
			{
				throw new Exception("FloorAnswerMgrZJ.addAnswer error:"+e);
			}
		}
	}
	
	/**
	 * 添加商品，已包含ID
	 * @param dbrow
	 * @throws Exception
	 */
	public void addProductForDB(DBRow dbrow)
		throws Exception
	{
		dbUtilAutoTran.insert(ConfigBean.getStringValue("product"),dbrow);
	}
	
	public DBRow[] getProductStorageByPcidPsid(long ps_id,long pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_storage")+" where cid = ? and pc_id = ?";
			
			DBRow para = new DBRow();
			para.add("cid",ps_id);
			para.add("pc_id",pc_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ.getProductStorageByPsid error:"+e);
		}
	}
	
	/**
	 * 检查条码是否重复，现包含检查p_code,p_code2,upc
	 * @param upc
	 * @return
	 * @throws Exception
	 */
	public DBRow checkPcodeExist(String p_code)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("product_code")+" where p_code = ?";
		
		DBRow para = new DBRow();
		para.add("p_code",p_code);
		
		return dbUtilAutoTran.selectPreSingle(sql, para);
	}
	
//	public DBRow[] getProductInCids(long catalog_id, long product_line_id, int union_flag, int product_file_types, int product_upload_status, PageCtrl pc)
//		throws Exception
//	{
//		try
//		{
//			String unionFlagCond = "";
//			
//			DBRow para = new DBRow();
//			
//			String productline="";
//			if(product_line_id>0)
//			{
//				productline = " and pc.product_line_id=? ";
//				para.add("prodcut_line_id",product_line_id);
//			}
//			
//			String cid = "";
//			if(catalog_id>0)
//			{
//				cid = "join pc_child_list as pcl on pc.id = pcl.pc_id and search_rootid=? ";
//				para.add("search_rootid",catalog_id);
//			}
//			
//			
//			
//			para.add("orignal_pc_id",0);
//			
//			if (union_flag>0)
//			{
//				unionFlagCond = " and p.union_flag=?";
//				para.add("union_flag",union_flag);
//			}
//			
//			
//			
//			//应改进为先按分类名称排序，再按商品名
//			String sql = "select p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,p.pc_id,pcode_main.p_code as p_code,pcode_amazon.p_code as p_code2,pcode_upc.p_code as upc,p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width from product as p "
//						+"join product_code as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type="+CodeTypeKey.Main+" "
//						+"left join product_code as pcode_amazon on p.pc_id = pcode_amazon.pc_id and pcode_amazon.code_type="+CodeTypeKey.Amazon+" "
//						+"left join product_code as pcode_upc on p.pc_id = pcode_upc.pc_id and pcode_upc.code_type="+CodeTypeKey.UPC+" "
//						+"join product_catalog as pc on pc.id = p.catalog_id "+productline		
//						+cid
//						+"where p.orignal_pc_id = ?"+unionFlagCond + " order by p_name asc";
//			
//			
//			
//			if ( pc!=null )
//			{
//				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
//			}
//			else
//			{
//				return(dbUtilAutoTran.selectPreMutliple(sql,para));
//			}
//			
//		}
//		catch (Exception e)
//		{
//			throw new Exception("FloorProductMgr.getProductInCids(row) error:" + e);
//		}
//	}
	
	
	public DBRow[] getProductInCids(long catalog_id, long product_line_id, int union_flag, int product_file_types, int product_upload_status, PageCtrl pc)
	throws Exception
	{
		try
		{
			String sql = this.getProductInCidsSql(catalog_id, product_line_id, union_flag, product_file_types, product_upload_status);
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectMutliple(sql, pc));
			}
			else
			{
				return(dbUtilAutoTran.selectMutliple(sql));
			}
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgr.getProductInCids(long catalog_id, long product_line_id, int union_flag, int product_file_types, int product_upload_status,int orignal_pc_id, PageCtrl pc) error:" + e);
		}
	}
	
	/**
	 * 组织查询商品的sql
	 * @param product_file_types
	 * @param product_upload_status
	 * @return
	 * @throws Exception
	 */
	public String getProductInCidsSql(long catalog_id, long product_line_id, int union_flag, int product_file_type, int product_upload_status) throws Exception
	{
		try 
		{
			StringBuffer sb = new StringBuffer();
			//商品某一类型的文件是否上传了
			if(0 != product_file_type)
			{
				//-- 商品照片某个类型未全(全)的,AND m.count_product IS NUL不加就不管是否上传了
				sb.append(" select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc");
				sb.append(" from (select s.* from ");
				sb.append(" 		( select p.pc_id , p.alive, p.catalog_id, p.heigth, p.length,");
				sb.append(" 						 p.orignal_pc_id, p.p_name, p.union_flag, p.unit_name,");
				sb.append(" 						 p.unit_price, p.volume, p.weight, p.width");
				sb.append(" 			from product p ");
				sb.append(" 			LEFT JOIN (select pc_id , count(pc_id) as count_product from product_file ");
				sb.append(" 							where file_with_type = ").append(FileWithTypeKey.PRODUCT_SELF_FILE);
				sb.append("								and product_file_type = ").append(product_file_type);
				sb.append(" 							GROUP BY pc_id, product_file_type)m");
				sb.append(" 			ON m.pc_id = p.pc_id ");
				sb.append(" 			WHERE p.orignal_pc_id = 0");
				if(union_flag > 0)
				{
					sb.append(" 		AND p.union_flag = ").append(union_flag);
				}
				
				if(1 == product_upload_status)
				{
					sb.append("				AND m.count_product IS NULL");
				}
				else if(2 == product_upload_status)
				{
					sb.append("				AND m.count_product IS NOT NULL");
				}
				
				sb.append("				)s");
				sb.append(" 	LEFT JOIN product_catalog c ON s.catalog_id = c.id LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
				if(0 != catalog_id)
				{
					sb.append("     JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				sb.append(" 	WHERE l.id IS NOT NULL ");
				if(0 != product_line_id)
				{
					sb.append(" AND l.id = ").append(product_line_id);
				}
				if(0 != catalog_id)
				{
					sb.append(" 	AND pcl.search_rootid = ").append(catalog_id);
				}
				sb.append(" 	) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append("	LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
			}
			//商品未完成上传文件
			else if(0 == product_file_type && 1 == product_upload_status)
			{
				sb.append(" select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc from ");
				sb.append("  (select s.* from ");
				sb.append("  	(select w.pc_id , w.alive, w.catalog_id, w.heigth, w.length,");
				sb.append("  				w.orignal_pc_id, w.p_name, w.union_flag, w.unit_name,");
				sb.append("  				w.unit_price, w.volume, w.weight, w.width");
				sb.append("  	 from product w left JOIN");
				sb.append("  		(select p.pc_id , n.count_product from product p LEFT JOIN");
				sb.append("  			(select count(*) as count_product,pc_id  ");
				sb.append("  					from (select * from product_file where file_with_type = ").append(FileWithTypeKey.PRODUCT_SELF_FILE);
				sb.append(						" GROUP BY pc_id, product_file_type) m GROUP BY pc_id)n");
				sb.append("  			on n.pc_id = p.pc_id ");
				sb.append("  			where p.orignal_pc_id = 0");
				sb.append("  			and n.count_product = 5 ");
				sb.append("  			and p.orignal_pc_id = 0)q");
				sb.append("  	ON w.pc_id = q.pc_id ");
				sb.append("  	where w.orignal_pc_id = 0 ");
				if(union_flag > 0)
				{
					sb.append("		AND w.union_flag = 0");
				}
				sb.append("  	and (q.count_product is null or q.count_product < 5) )s");
				sb.append("  LEFT JOIN product_catalog c ON s.catalog_id = c.id LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
				if(0 != catalog_id)
				{
					sb.append("  JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				sb.append("  WHERE l.id IS NOT NULL ");
				if(0 != product_line_id)
				{
					sb.append("AND l.id = ").append(product_line_id);
				}
				if(0 != catalog_id)
				{
					sb.append("  AND pcl.search_rootid = ").append(catalog_id);
				}
				sb.append("	 ) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
			}
			//商品已完成上传文件
			else if(0 == product_file_type && 2 == product_upload_status)
			{
				sb.append("select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc ");
				sb.append("   from(select q.* from ");
				sb.append(" 		(select p.pc_id ,p.alive, p.catalog_id, p.heigth, p.length,");
				sb.append(" 				p.orignal_pc_id, p.p_name, p.union_flag, p.unit_name,");
				sb.append(" 				p.unit_price, p.volume, p.weight, p.width");
				sb.append(" 		   from product p LEFT JOIN");
				sb.append(" 			(select count(*) as count_product,pc_id  ");
				sb.append(" 					from (select * from product_file where file_with_type = ").append(FileWithTypeKey.PRODUCT_SELF_FILE);
				sb.append("						GROUP BY pc_id, product_file_type) m GROUP BY pc_id)n");
				sb.append(" 			on n.pc_id = p.pc_id ");
				sb.append(" 			where p.orignal_pc_id = 0");
				sb.append(" 			and n.count_product = 5 ");
				if(union_flag > 0)
				{
					sb.append(" 		AND p.union_flag = ").append(union_flag);
				}
				sb.append(" 			and p.orignal_pc_id = 0)q");
				sb.append(" 	LEFT JOIN product_catalog c ON q.catalog_id = c.id LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
				if(0 != catalog_id)
				{
					sb.append(" 	JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				sb.append(" 	WHERE l.id IS NOT NULL ");
				if(0 != product_line_id)
				{
					sb.append("		AND l.id = ").append(product_line_id);
				}
				if(0 != catalog_id)
				{
					sb.append(" 	AND pcl.search_rootid = ").append(catalog_id);
				}
				sb.append("		 ) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
			}
			//不管商品是否上传了文件
			else
			{
				//-- 不管商品是否上传了文件
				sb.append("select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc from ");
				sb.append("	(select p.pc_id , p.alive, p.catalog_id, p.heigth, p.length,");
				sb.append("						 p.orignal_pc_id, p.p_name, p.union_flag, p.unit_name,");
				sb.append("						 p.unit_price, p.volume, p.weight, p.width");
				sb.append("			from product p ");
				sb.append("	 LEFT JOIN product_catalog c ON p.catalog_id = c.id LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
				if(0 != catalog_id)
				{
					sb.append("  JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				sb.append("	 WHERE l.id IS NOT NULL ");
				sb.append(" AND p.orignal_pc_id = 0 ");
				if(0 != product_line_id)
				{
					sb.append("  AND l.id = ").append(product_line_id);
				}
				if(0 != catalog_id)
				{
					sb.append("	 AND pcl.search_rootid = ").append(catalog_id);
				}
				if(union_flag > 0)
				{
					sb.append("  AND p.union_flag = ").append(union_flag);
				}
				sb.append("	 ) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
					
			}
			return sb.toString();
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductInCidsSql(int product_file_types, int product_upload_status) error:" + e);
		}
	}
	
	/**
	 * 根据国家名称查询省份
	 * @param countyr_code
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProvinceByCountryCode(String country_code)
		throws Exception
	{
		try 
		{
			String sql = " select cp.* from "+ConfigBean.getStringValue("country_code")+" as cc"
						+" left join "+ConfigBean.getStringValue("country_province")+" as cp on cp.nation_id=cc.ccid"
						+" where cc.c_code = ?";
//			String sql = "select cp.* from country_code as cc ,country_province as cp where cp.nation_id=cc.ccid and cc.c_code = ?";
			
			DBRow para = new DBRow();
			para.add("c_code",country_code);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgr.getProvinceByCountryCode error:"+e);
		}
	}
	
	/**
	 * 根据产品线查询商品
	 * @param product_line_id
	 * @param union_flag
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductByProductLineId(long product_line_id,int union_flag,PageCtrl pc)
		throws Exception
	{
		String unionFlagCond = "";
		
		if (union_flag>0)
		{
			unionFlagCond = " and p.union_flag="+union_flag+" ";
		}
		
		String whereProductLine = "";
		if(product_line_id>0)
		{
			whereProductLine = " and pc.product_line_id = "+product_line_id+" ";
		}
		
		String sql = "select p.* from "+ConfigBean.getStringValue("product")+" as p "
					+"join "+ConfigBean.getStringValue("product_catalog")+" as pc on p.catalog_id = pc.id "+whereProductLine
					+"where p.orignal_pc_id = 0"+unionFlagCond;
		
		return dbUtilAutoTran.selectMutliple(sql,pc);
	}
	
	/**
	 * 根据商品数组获得对应的套装
	 * @param pc_ids
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductByPids(long[] pc_ids,long ps_id,long waybill_id)
		throws Exception
	{
		try
		{
			StringBuffer distinct = new StringBuffer();
			
			if(waybill_id == 0)
			{
				for (int i = 0; i < pc_ids.length; i++) 
				{
					if(i!=0)
					{
						distinct.append(",");
					}
					distinct.append(pc_ids[i]);
				}
			}
			
			if(pc_ids==null||pc_ids.length==0)
			{
				return null;
			}
			
			String joinProduct = "";
			for (int i = 0; i < pc_ids.length; i++) 
			{
				if(i==0)
				{
					joinProduct +=" join product_union as pu0 on p.pc_id = pu0.set_pid and pu0.pid="+pc_ids[i];
				}
				else
				{
					joinProduct +=" join product_union as pu"+i+" on pu0.set_pid = pu"+i+".set_pid and pu"+i+".pid="+pc_ids[i];
				}
				
			}
			
			String sql = ""; 
			
			if (waybill_id == 0 ) 
			{
				sql = "select p.*,ps.store_count from "+ConfigBean.getStringValue("product")+" as p "
				+"join product_storage as ps on p.pc_id = ps.pc_id and ps.store_count>0 and ps.cid = "+ps_id 
				+joinProduct
				+" where p.orignal_pc_id = 0 and p.pc_id not in("+distinct+")"
				+" order by ps.store_count desc";
			}
			else
			{
				sql = "select p.*,ps.store_count from "+ConfigBean.getStringValue("product")+" as p "
				+"join product_storage as ps on p.pc_id = ps.pc_id and ps.store_count>0 and ps.cid = "+ps_id 
				+joinProduct
				+" join waybill_order_item woi on woi.pc_id != p.pc_id and woi.waybill_order_id = "+waybill_id+" "
				+" where p.orignal_pc_id = 0"
				+" order by ps.store_count desc";
			}
			
			
			
			PageCtrl pc = new PageCtrl();
			pc.setPageCount(1);
			pc.setPageSize(5);
			
			return dbUtilAutoTran.selectMutliple(sql,pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ getProductByPids error:"+e);
		}
	}
	
	/**
	 * 根据商品数组获得不完整套装
	 * @param pc_ids
	 * @param ps_id
	 * @param waybill_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBackUpProductByPids(long[] pc_ids,long ps_id,long waybill_id)
		throws Exception
	{
		try
		{
			if(pc_ids==null||pc_ids.length==0)
			{
				return null;
			}
			
			String joinProduct = "";
			for (int i = 0; i < pc_ids.length; i++) 
			{
				if(i==0)
				{
					joinProduct +=" join product_union as pu0 on p.pc_id = pu0.set_pid and pu0.pid="+pc_ids[i];
				}
				else
				{
					joinProduct +=" join product_union as pu"+i+" on pu0.set_pid = pu"+i+".set_pid and pu"+i+".pid="+pc_ids[i];
				}
				
			}
			
			String sql = "select DISTINCT p.*,bus.back_up_store_count from "+ConfigBean.getStringValue("product")+" as p "
						+"join back_up_storage as bus on p.pc_id = bus.pc_id and bus.back_up_store_count>0 and bus.ps_id = "+ps_id 
						+joinProduct
						+" where p.orignal_pc_id != 0"
						+" order by bus.back_up_store_count desc";

			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ getBackUpProductByPids error:"+e);
		}
	}
	/**
	 * 获得商品对应的文件
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductFileByPcid(long pc_id, boolean isBarrierPurchase, boolean isBarrierTransport)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_file")+" where pc_id = ?";
			if(isBarrierPurchase)
			{
				sql += " and file_with_type != " + FileWithTypeKey.PURCHASE_PRODUCT_FILE + " and file_with_type != " + FileWithTypeKey.PRODUCT_TAG_FILE;
			}
			if(isBarrierTransport)
			{
				sql += " and file_with_type != " + FileWithTypeKey.product_file + " and file_with_type != " + FileWithTypeKey.TRANSPORT_PRODUCT_TAG_FILE;
			}
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ getProductFileByPcid error:"+e);
		}
	}
	
	/**
	 * 添加商品文件
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addProductFile(DBRow dbrow)
		throws Exception
	{
		try
		{
			return (dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_file"),dbrow));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ addProductFile error:"+e);
		}
	}
	
	/**
	 * 根据商品ID删除文件
	 * @param pc_id
	 * @throws Exception
	 */
	public void delProductFileByPcid(long pc_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where pc_id="+pc_id,ConfigBean.getStringValue("product_file"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ delProductFileByPcid error:"+e);
		}
	}
	
	/**
	 * 获得取货商品列表，商品，仓库分组
	 * @param ps_id
	 * @param product_line_id
	 * @param catalog_id
	 * @param status
	 * @param pc_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLackingProductByPs(long ps_id,long product_line_id,long catalog_id,int[] status,long pc_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String whereStatus = "";
			if(status.length>0)
			{
				whereStatus += " and (";
				for (int i = 0; i < status.length; i++) 
				{
					whereStatus += "wo.status="+status[i];
					
					if(i!=(status.length-1))
					{
						whereStatus+=" or ";
					}
				}
				whereStatus +=")";
			}
			
			String whereProduct = "";
			if (pc_id>0)
			{
				whereProduct = " and p.pc_id = "+pc_id+" ";
			}
			
			String whereProductLineOrCatalog = "";
			
			if(catalog_id>0)
			{
				whereProductLineOrCatalog = "join product_catalog as pc on pc.id = p.catalog_id "
										   +"join pc_child_list as pcl on pcl.pc_id = pc.id and pcl.search_rootid = "+catalog_id+" ";
			}
			else if(product_line_id>0&&catalog_id==0)
			{
				whereProductLineOrCatalog = " join product_catalog as pc on pc.id = p.catalog_id and pc.product_line_id = "+product_line_id+" "; 
			}
			
			String whereStorage = "";
			if(ps_id>0)
			{
				whereStorage = " and wo.ps_id="+ps_id+" ";
			}
			
			String sql = "select p_name,pc_id,catalog_id,sum(stockout_count) as stockout,cid as ps_id from "
				+"("
				+"	select p.p_name,ps.pc_id,p.catalog_id,(woq.woi_q-ps.store_count)as stockout_count,ps.cid from " 
				+"	(" 
				+"	 select woi.pc_id,sum(woi.quantity) as woi_q,wo.ps_id from "+ConfigBean.getStringValue("waybill_order")+" as wo "
				+"   join "+ConfigBean.getStringValue("waybill_order_item")+" as woi ON woi.waybill_order_id = wo.waybill_id and (woi.product_type ="+ProductTypeKey.NORMAL+" or woi.product_type = "+ProductTypeKey.UNION_STANDARD+") and woi.product_status = "+ProductStatusKey.STORE_OUT+" "
				+"	 where wo.product_status = "+ProductStatusKey.STORE_OUT+whereStatus+whereStorage
				+"	 group by wo.ps_id,woi.pc_id "
				+"	)as woq "
				+"  join "+ConfigBean.getStringValue("product_storage")+" ps on ps.cid = woq.ps_id and ps.pc_id = woq.pc_id " 
				+"  join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = ps.pc_id "+whereProduct
				+whereProductLineOrCatalog
				+"	where (woq.woi_q-ps.store_count)>0 "
				+"	union all "
				+"  select p.p_name,ps.pc_id,p.catalog_id,(woq.woi_q-ps.store_count)as stockout_count,ps.cid from "
				+"	( "
				+"	 select pu.pid as pc_id,sum(woi.quantity*pu.quantity)woi_q,wo.ps_id from "+ConfigBean.getStringValue("waybill_order")+" as wo "
				+"	 join "+ConfigBean.getStringValue("waybill_order_item")+" as woi on wo.waybill_id = woi.waybill_order_id and woi.product_type = "+ProductTypeKey.UNION_CUSTOM+" and woi.product_status = "+ProductStatusKey.STORE_OUT+" " 
				+"	 join "+ConfigBean.getStringValue("product_union")+" as pu ON woi.pc_id = pu.set_pid "
				+"	 where wo.product_status = "+ProductStatusKey.STORE_OUT+whereStatus+whereStorage
				+"	 group by pu.pid,wo.ps_id order by pu.pid,wo.ps_id "
				+"	) as woq "
				+"	join "+ConfigBean.getStringValue("product_storage")+" ps on ps.cid = woq.ps_id and ps.pc_id = woq.pc_id "
				+"	join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = ps.pc_id "
				+whereProductLineOrCatalog
				+"	where (woq.woi_q-ps.store_count)>0 "
				+") as stockoutCount GROUP BY pc_id,cid order by pc_id,ps_id";
			
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZJ getLackingProductByPs error:"+e);
		}
		
	}
	public void updateProduct(DBRow row , long pid ) throws Exception{
		try{
			dbUtilAutoTran.update(" where pc_id=" + pid, ConfigBean.getStringValue("product"), row);
		}catch (Exception e) {
			throw new Exception("FloorProductMgrZJ updateProduct error:"+e);		}
	}
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public DBRow[] getProductByUnitName(String unit_name)
		throws Exception
	{
		String sql = "select * from product where unit_name = ? ";
		
		DBRow para = new DBRow();
		para.add("unit_name",unit_name);
		
		return dbUtilAutoTran.selectPreMutliple(sql,para);
	}
	
	public void executeSQL(String sql )
		throws Exception
	{
		dbUtilAutoTran.executeSQL(sql);
	}
	
}
