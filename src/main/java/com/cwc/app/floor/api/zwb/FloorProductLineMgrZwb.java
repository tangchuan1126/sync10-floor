package com.cwc.app.floor.api.zwb;


import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorProductLineMgrZwb {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	
	//查询所有生产线
	public DBRow[] getAllProductLine() throws Exception {
		try {
			String sql="select * from product_line_define";
			return dbUtilAutoTran.selectMutliple(sql);	
		} catch (Exception e) {
			throw new Exception("FloorProductLineMgrZwb getAllProductLine error:"+e);
		}
	}
	//根据产品线id查询在分类表的id
	public DBRow[] getProductLineIdInCatalog(long productLineId)throws Exception{
		try{
			String sql="select * from product_catalog where parentid=0 and product_line_id="+productLineId;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb getProductLineIdInCatalog error:"+e);
		}
	}
	//根据产品能 线id查询产品分类
	public DBRow[] getProductCatalogByProductLine(long productId) throws Exception{
		try{
			String sql="select * from product_catalog where parentid="+productId;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb getProductCatalogByProductLine error:"+e);
		}
	}
	
	
	//根据生产线id查询供应商
	public DBRow[] getSupplier(long id) throws Exception {
		try {
			String sql="select * from supplier where product_line_id="+id+"";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorPhotoMgrZwb getAllPhoto error:"+e);
		}
	}
	
	//查询所有供应商
	public DBRow[] getAllSupplier() throws Exception {
		try {
			String sql="select * from supplier";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorPhotoMgrZwb getAllPhoto error:"+e);
		}
	}
	
	//根据供应商id查询采购单号
	public DBRow[] getPurchase(String supplierId) throws Exception {
		try {
			String sql="select * from purchase where supplier="+supplierId+"";;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorPhotoMgrZwb getPurchase error:"+e);
		}
	}
	
	//查询所有采购单
	public DBRow[] getAllPurchase() throws Exception {
		try {
			String sql="select * from purchase";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorPhotoMgrZwb getAllPhoto error:"+e);
		}
	}
	
	//根据商品id 采购单id查询 工厂类型
	public DBRow getFactorType(long purchaseId,long productId)throws Exception{
		try {
			String sql="select factory_type from purchase_detail where product_id="+productId+" and purchase_id="+purchaseId;
			DBRow dbrow=this.dbUtilAutoTran.selectSingle(sql);
			return dbrow;
		} catch (Exception e) {
			throw new Exception("FloorProductLineMgrZwb getFactorType error:"+e);
		}
	}
	
	
	//android按搜索的值查询
	public DBRow[] seachProductBySeachValue(String type,String seachValue)throws Exception{
		try{
			String sql="select * from product p join product_code c on p.pc_id=c.pc_id where p.orignal_pc_id=0 and (p.pc_id='"+seachValue+"' or p_name='"+seachValue+"' or p_code='"+seachValue+"')";
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb seachProductByType error:"+e);
		}
	}

	
	//android  查询产品线下所有商品
	public DBRow[] selectAllProductByProductLineId(long productLineId,String type,String seachValue)throws Exception{
		try{
			String sql="select * from (select * from product_catalog where product_line_id="+productLineId+") as c left join product p on c.id=p.catalog_id where p.orignal_pc_id=0";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectAllProductByProductLineId error:"+e);
		}
	}
	//android 查询分类下所有商品
	public DBRow[] selectAllProductByProductCatalogId(long catalogId,String type,String seachValue)throws Exception{
		try{
			//String sql="select * from (select * from pc_child_list where search_rootid="+catalogId+") as c left join product p on c.pc_id=p.catalog_id where p.orignal_pc_id=0";
			String sql="select * from (select pc_id as pcid from pc_child_list where search_rootid="+catalogId+") as c left join product p on c.pcid=p.catalog_id where p.orignal_pc_id=0";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectAllProductByProductCatalogId error:"+e);
		}
	}
	
	public DBRow selectProductByProductId(long productId)throws Exception{
		try{
			String sql="select * from product p join product_catalog c on p.catalog_id=c.id join product_code d on p.pc_id=d.pc_id where p.pc_id="+productId;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectProductByProductId error:"+e);
		}
	}
	
	//添加产品线和title 关系
	public void addProductLineTitle(DBRow row)throws Exception{
		try{
			this.dbUtilAutoTran.insert("title_product_line",row);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb addProductLineTitle error:"+e);
		}
	}
	//查询当前产品线下的title
	public DBRow[] selectTitleByProductLine(long productLineId)throws Exception{
		try{
			String sql="select * from title_product_line tp join title t on tp.tpl_title_id=t.title_id where tpl_product_line_id="+productLineId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectTitleByProductLine error:"+e);
		}
	}
	
	//添加产品分类和title 关系
	public void addProductCatalogTitle(DBRow row)throws Exception{
		try{
			this.dbUtilAutoTran.insert("title_product_catalog",row);
			
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb addProductCatalogTitle error:"+e);
		}
	}
	
	//根据产品分类id 查询title关系
	public DBRow[] selectTitleByProductCatalog(long productCatalogId)throws Exception{
		try{
			String sql="select * from title_product_catalog tp join title t on tp.tpc_title_id=t.title_id where tpc_product_catalog_id="+productCatalogId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectTitleByProductCatalog error:"+e);
		}
	}
	
	//查询产品线下所有分类
	public DBRow[] selectAllProductCatalogByLineId(long productLineId)throws Exception{
		try{
			String sql="select * from product_catalog where product_line_id="+productLineId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectAllProductCatalogByLineId error:"+e);
		}
	}
	//查询某分类下的所有子分类
	public DBRow[] selectAllProductCatalogByCatalogId(long productCatalogId)throws Exception{
		try{
			String sql="select distinct(pc_id) from pc_child_list where search_rootid="+productCatalogId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectAllProductCatalogByCatalogId error:"+e);
		}
	}	
	//根据产品分类id查询父级
	public DBRow[] selectProductCatalogParentByCatalogId(long productCatalogId)throws Exception{
		try{
			String sql="select * from pc_child_list where pc_id="+productCatalogId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectProductCatalogParentByCatalogId"+e);
		}
	}
	//根据父级查询是否包含该title
	public DBRow[] selectProductCatalogTitleByParentCatalogId(long titleId,String ids)throws Exception{
		try{
			String sql="select * from title_product_catalog where tpc_title_id="+titleId+" and tpc_product_catalog_id in ("+ids+")";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectProductCatalogTitleByParentCatalogId"+e);
		}
	}
	//添加商品title关系
	public void addProductTitle(DBRow row)throws Exception{
		try{
			this.dbUtilAutoTran.insert("title_product",row);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb addProductTitle error:"+e);
		}
	}
	//删除产品线与title关系
	public int detTitleByProductLine(long productLineId,long titleId)throws Exception{
		try{
			return this.dbUtilAutoTran.delete("where tpl_product_line_id="+productLineId+" and tpl_title_id="+titleId,"title_product_line");
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb detTitleByProductLine error:"+e);
		}
	}
	//删除产品分类与title关系
	public int detTitleByProductCatalog(long productCatalogId,long titleId)throws Exception{
		try{
			return this.dbUtilAutoTran.delete("where tpc_product_catalog_id="+productCatalogId+" and tpc_title_id="+titleId, "title_product_catalog");
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb detTitleByProductCatalog error:"+e);
		}
	}
	//判断 产品分类上的产品线是否包含该title
	public DBRow[] selectProductLineTitleByCatalogId(long catalogId)throws Exception{
		try{
			String sql="select * from product_catalog p join title_product_line t on p.product_line_id=t.tpl_product_line_id where id="+catalogId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectProductLineTitleByCatalogId error:"+e);
		}
	}
	//删除产品 与title 关系
	public int detTitleByProduct(long productId,long titleId)throws Exception{
		try{
			return this.dbUtilAutoTran.delete("where tp_pc_id="+productId+" and tp_title_id="+titleId,"title_product");
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb detTitleByProduct error:"+e);
		}
	}
	//变更产品线与title的关系
	public void updateTitleProductLine(long productLineId,long titleId,DBRow row)throws Exception{
		try{
			this.dbUtilAutoTran.update("where tpl_product_line_id="+productLineId+" and tpl_title_id="+titleId,"title_product_line",row);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb updateTitleProductLine error:"+e);
		}
	}
	//变更产品分类与title的关系
	public void updateTitleProductCatalog(long productCatalogId,long titleId,DBRow row)throws Exception{
		try{
			this.dbUtilAutoTran.update("where tpc_product_catalog_id="+productCatalogId+" and tpc_title_id="+titleId,"title_product_catalog", row);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb updateTitleProductCatalog error:"+e);
		}
	}
	//变更产品与title的关系
	public void updateTitleProduct(long productId,long titleId,DBRow row)throws Exception{
		try{
			this.dbUtilAutoTran.update("where tp_pc_id="+productId+" and tp_title_id="+titleId,"title_product",row);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb updateTitleProduct error:"+e);
		}
	}
	//根据产品id查询 商品与title关系表
	public DBRow[] selectProductTitleByProductId(long productId)throws Exception{
		try{
			String sql="select * from title_product tp join title t on tp.tp_title_id=t.title_id where tp_pc_id="+productId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectProductTitleByProductId error:"+e);
		}
	}
	
	//查询title下  可用总数量
	public DBRow selectTitleCount(long titleId,long productId)throws Exception{
		try{
			String sql="select sum(available_count) as available_count ,sum(physical_count) as physical_count,title_id " +
					   "from product_storage_title where pc_id="+productId+" and title_id="+titleId+" GROUP BY title_id";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectTitleCount error:"+e);
		}
	}
	//title下 循环批次数量
	public DBRow[] selectLotNumberCount(long titleId,long productId,long ps_id,long lotNumber)throws Exception{
		try{
			String sql="select sum(available_count) as available_count ,sum(physical_count) as physical_count,lot_number " +
					   "from product_storage_title where pc_id="+productId+" and title_id="+titleId;
			if(lotNumber!=0){
				sql+=" and lot_number="+lotNumber;
			}
			if(ps_id!=0){
				sql+=" and ps_id="+ps_id;
			}
			sql+="  GROUP BY lot_number";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectLotNumberCount error:"+e);
		}
	}
	//商品 title 下所有批次的 位置数量
	public DBRow[] selectLotNumberLocationCount(long pcId,long titleId,long lot_number,long ps_id)throws Exception{
		try{
			String sql="select *,sum(physical_quantity) as physical_quantity_count,sum(theoretical_quantity) as theoretical_quantity_count from product_store_location " +
					"where pc_id="+pcId;
			if(lot_number!=0){
				sql+=" and lot_number="+lot_number;
			}
			if(titleId!=0){
				sql+=" and title_id="+titleId;
			}
			if(ps_id!=0){
				sql+=" and ps_id="+ps_id;
			}
			sql+=" GROUP BY position";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectLotNumberLocationCount error:"+e);
		}
	}
	
	//分仓库查库存
	public DBRow[] selectWarehouse(long productId,long titleId,long lot_number)throws Exception{
		try{
			String sql="select *,sum(physical_quantity) as physical_quantity_count,sum(theoretical_quantity) " +
					   "as theoretical_quantity_count from product_store_location " +
					   "where pc_id="+productId;
			if(lot_number!=0){
				sql+=" and lot_number="+lot_number;
			}
			if(titleId!=0){
				sql+=" and title_id="+titleId;
			}
			sql+=" GROUP BY ps_id";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectWarehouse error:"+e);
		}
	}
	
	//商品title 位置下所有托盘 数量
	public DBRow[] selectLotNuberLpCount(long productId,long titleId,long locationId,long lotNumber)throws Exception{
		try{
			String sql="select *,sum(available_count) as available_count_sum,sum(physical_count) as physical_count_sum from" +
					" product_storage_container where pc_id="+productId;
			if(lotNumber!=0){
				sql+=" and title_id="+titleId;
			}
			if(titleId!=0){
				sql+=" and lot_number="+lotNumber;
			}
			if(locationId!=0){
				sql+=" and slc_id="+locationId;
			}
			sql+=" GROUP BY con_id";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectLotNuberLpCount error:"+e);
		}
	}
	//容器类型总数
	public DBRow[] selectContainerTypeNumber(long productId,long titleId,long lotNumberId)throws Exception{
		try{
			String sql="select *,sum(available_count) as available_count_sum,sum(physical_count) as physical_count_sum " +
						"from product_storage_container " +
						"where pc_id="+productId;
			if(titleId!=0){
				sql+=" and title_id="+titleId;
			}
			if(lotNumberId!=0){
				sql+=" and lot_number="+lotNumberId;
			}
			sql+=" GROUP BY container_type";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectContainerTypeNumber error "+e);
		}
	}
	//查询根据容器类型id0
	public DBRow[] selectContainerDetail(long productId,long titleId,long lotNumberId,long containerType)throws Exception{
		try{
			String sql="select * from product_storage_container " +
					"where pc_id="+productId+" and container_type="+containerType;
			if(titleId!=0){
				sql+=" and title_id="+productId;
			}
			if(lotNumberId!=0){
				sql+=" and lot_number="+lotNumberId;
			}
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception(""+e);
		}
	}
	
	//根据托盘id查询托盘
	public DBRow selectLpByLpId(long lpId)throws Exception{
		try{
			String sql="select * from container where con_id="+lpId;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectLpByLpId error:"+e);
		}
	}
	
	//查询title是否存在
	public DBRow[] selectTitleByTitleName(String titleName)throws Exception{
		try{
			String sql="select * from title where title_name='"+titleName+"'";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectTitleByTitleName:error"+e);
		}
	}
	
	//根据产品线名字查询产品线
	public DBRow[] selectProductLineByName(String productLineName)throws Exception{
		try{
			String sql="select * from product_line_define where name='"+productLineName+"'";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectProductLineByName:error"+e);
		}
	}
	
	//验证产品线与title关系表 是否重复
	public DBRow[] selectProductLineTitleByTitleIdAndLineId(long titleId,long productLineId)throws Exception{
		try{
			String sql="select * from title_product_line where tpl_title_id="+titleId+" and tpl_product_line_id="+productLineId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectProductLineTitleByTitleIdAndLineId:error"+e);
		}
	}
	
	//查询产品线和title的关系导出用
	public DBRow[] downProductLineAndTitle(long title_id)throws Exception{
		try{
			String sql="select * from title_product_line tl join title t " +
					"on tl.tpl_title_id=t.title_id join product_line_define pl " +
					"on tl.tpl_product_line_id=pl.id where title_id="+title_id;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb downProductLineAndTitle:error"+e);
		}
	}
	
	//客户    产品线title 查询    
	public DBRow[] seachProductLineTitleBySeachValue(long titleId,long adminId)throws Exception{
		try{
			String sql="select * from title_admin ta " +
					"join title t on ta.title_admin_title_id=t.title_id " +
					"JOIN title_product_line tl on t.title_id=tl.tpl_title_id " +
					"join product_line_define pd on tl.tpl_product_line_id=pd.id " +
					"where ta.title_admin_adid="+adminId+" ";
			if(titleId > 0){
				sql+=" and t.title_id="+titleId;
			}
            sql+=" ORDER BY name";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb seachProductLineTitleBySeachValue:error"+e);
		}
	}
	
	//非客户  产品线title 查询
	public DBRow[] selectProductLineTitleBySeachAdmin(long titleId)throws Exception{
		try{
			String sql="select * from product_line_define pld " +
					"left join title_product_line tpl on pld.id=tpl.tpl_product_line_id " +
					"left join title tl on tpl.tpl_title_id=tl.title_id " +
					"where 1=1 ";
			if(titleId > 0){
				sql+=" and tpl_title_id="+titleId;
			}   
				sql+=" ORDER BY title_name DESC"; 
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectProductLineTitleBySeachAdmin"+e);
		}
	}
	
	//非客户 产品分类查询
	public DBRow[] selectProductCatalogTitleBySeachAdmin(long titleId)throws Exception{
		try{
			String sql="select * from product_catalog pcg " +
					"left join title_product_catalog tpc on tpc.tpc_product_catalog_id=pcg.id " +
					"left join title t on tpc.tpc_title_id=t.title_id " +
					"where 1=1";
			if(titleId>0){
				sql+=" and tpc.tpc_title_id="+titleId;
			}
			    sql+=" ORDER BY title_name DESC";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectProductCatalogTItleBySeachAdmin"+e);
		}
	}
	
	
	//产品分类与title关系导出查询
	public DBRow[] seachProductCatalogTitleBySeachValue(long titleId,long adminId)throws Exception{
		try{
			String sql="select * from title_admin ta join title t " +
					"on ta.title_admin_title_id=t.title_id JOIN title_product_catalog tc " +
					"on t.title_id=tc.tpc_title_id join product_catalog pc " +
					"on tc.tpc_product_catalog_id=pc.id " +
					"where ta.title_admin_adid="+adminId+" ";
			if(titleId >0){
				sql+=" and t.title_id="+titleId;
			}
			sql+=" ORDER BY title";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb seachProductCatalogTitleBySeachValue:error:"+e);
		}
	}
	
	//查询产品分类是否存在
	public DBRow[] selectProductCatalogByCatalogName(String catalogName)throws Exception{
		try{
			String sql="select * from product_catalog where title='"+catalogName+"'";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectProductCatalogByCatalogName:error:"+e);
		}
	}
	
	public DBRow[] selectProductCatalogTitleByTitleIdAndCatalogId(long titleId,long productCatalogId)throws Exception{
		try{
			String sql="select * from title_product_catalog where tpc_title_id="+titleId+" and tpc_product_catalog_id="+productCatalogId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectProductCatalogTitleByTitleIdAndCatalogId:error:"+e);
		}
	}
	
	//非客户 根据title查询产品下
	public DBRow[] selectProductLinewByTitleId(long titleId)throws Exception{
		try{
			String sql="select * from title_product_line tl " +
					"join product_line_define pl on tl.tpl_product_line_id=pl.id " +
					"where tl.tpl_title_id="+titleId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectProductLinewBy"+e);
		}
	}
	//非客户 根据title查询产品分类
	public DBRow[] selectProductCatalogByTitleId(long titleId)throws Exception{
		try{
			String sql="select * from title_product_catalog tpc " +
					"join product_catalog pcg on tpc.tpc_product_catalog_id=pcg.id " +
					"where tpc.tpc_title_id="+titleId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectProductCatalogByTitleId"+e);
		}
	}
	
	//查询所有title不区分帐号
	public DBRow[] selectAllTitle()throws Exception{
		try{
			String sql="select * from title order by title_name";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectAllTitle"+e);
		}
	}
	
	//非客户登录 根据titleid 查询产品线
	public DBRow[] selectProductLineAllByTitleId(Long[] titleId)throws Exception{
		try{
			String sql="select * from product_line_define pld " +
					"join title_product_line tpl on pld.id=tpl.tpl_product_line_id " +
					"join title tl on tl.title_id=tpl.tpl_title_id " +
					"where 1=1 ";
			if(null != titleId && titleId.length > 0)
			{
				sql += " AND (tl.title_id = " + titleId[0];
				for (int i = 1; i < titleId.length; i++) 
				{
					sql += " OR tl.title_id = " + titleId[i];
				}
				sql += " )";
			}
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb selectProductLineByTitleId()"+e);
		}
	}
	
	//查询用户下是否包含该title
	public DBRow[] findAdminTitleByTitleIdAndAdid(long adId,long titleId)throws Exception{
		try{
			String sql="select * from title_admin where title_admin_title_id="+titleId+" and title_admin_adid="+adId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb findAdminTitleByTitleIdAndAdid()"+e);
		}
	}
	//验证 登录用户是否存在 该title名
	public DBRow[] findAdminTItleByTitleName(long adminId,long titleId)throws Exception{
		try{
			String sql="select * from title_admin where title_admin_adid="+adminId+" and title_admin_title_id="+titleId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb findAdminTItleByTitleName"+e);
		}
	}
	
	//查询 用户title关系表的最大排序
	public DBRow findMaxSort()throws Exception{
		try{
			String sql="select max(title_admin_sort) sort from title_admin";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb findMaxSort"+e);
		}
	}
	
	//想用户与关系表里插入数据
	public long insertAdminTitle(DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.insertReturnId("title_admin",row);
		}catch(Exception e){
			throw new Exception("FloorProductLineMgrZwb insertAdminTitle"+e);
		}
	}
	
	  /**
     * ProductCategory导出excel
     * @author Yuanxinyu
     **/
    public DBRow[] getAllProductCategoryDetail() throws Exception{

        try {

            String sql = "select a.title as level1 ,b.title as level2 ,c.title as level3 from (SELECT id,title FROM product_catalog WHERE parentid = 0) a left join ( SELECT id,parentid,title FROM product_catalog WHERE parentid <> 0) b on a.id=b.parentid left join (SELECT parentid,title FROM product_catalog WHERE parentid <> 0) c on b.id=c.parentid";

            return dbUtilAutoTran.selectMutliple(sql);

        }catch (Exception e){
            throw new Exception("FloorProductLineMgrZwb.getAllProductCategoryDetail() error:" + e);
        }
    }

	
	

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
