package com.cwc.app.floor.api.qll;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorEmailSceneMgr {
	private DBUtilAutoTran dbUtilAutoTran;

	public DBRow[] getScene() 
	throws Exception
	{
	
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("email_scene")+" where parentid = 0 or parentid is null";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getScene() error:"+e);
		}
	}
	
	 
	
	/**
	 * 添加资产类别信息
	 * @throws Exception
	 */
	public long addScene(DBRow dbrow) 
		throws Exception
	{
		 try
		 {
			 long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("email_scene"));
			 dbrow.add("id", id);
			 dbUtilAutoTran.insert(ConfigBean.getStringValue("email_scene"), dbrow);
			 return (id);
		 } 
		 catch (Exception e)
		 {
			throw new Exception("addScene(DBRow dbrow) error:" + e);
		 }
		
	}
	
	/**
	 * 根据id查询父类信息
	 * @param categoryId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getParentScene(long parentid) 
		throws Exception 
	{
		try {
			DBRow para = new DBRow();
			para.add("parentid",parentid);
			String sql = "select * from "+ConfigBean.getStringValue("email_scene")+" where parentid= ?  ";
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("getParentScene(long parentId) error:"+e);
		}
	}

	/**
	 * 删除资产类别信息
	 * @param categoryId
	 * @return
	 * @throws Exception 
	 */
	public void delScene(long id) 
		throws Exception 
	{
		
		try 
		{
			dbUtilAutoTran.delete("where id="+id,ConfigBean.getStringValue("email_scene"));
		} 
		catch (Exception e) 
		{
			throw new Exception("delScene(long id) error:"+e);
		}
		
	 }
	
	/**
	 * 根据id查询资产类别信息
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getDetailScene(long id) 
		throws Exception 
	{
		try 
		{
			DBRow para = new DBRow();
			para.add("id",id);
			String sql = "select * from "+ConfigBean.getStringValue("email_scene")+" where id = ?";
			return (dbUtilAutoTran.selectPreSingle(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailScene(int id) error:"+e);
		}
	}
	/**
	 * 根据sname查询资产类别信息
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getDetailSceneByName(String sname) 
		throws Exception 
	{
		try 
		{
			DBRow para = new DBRow();
			para.add("sname",sname);
			String sql = "select * from "+ConfigBean.getStringValue("email_scene")+" where sname = ?";
			return (dbUtilAutoTran.selectPreSingle(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailSceneByName(int id) error:"+e);
		}
	}
	
	/**
	 * 修改某一具体的资产类别信息
	 * @param id
	 * @throws Exception
	 */
	public void modScene(long id, DBRow dbrow) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.update("where id="+id, ConfigBean.getStringValue("email_scene"), dbrow);
		} 
		catch (Exception e) 
		{
			throw new Exception("modScene(int id, DBRow dbrow) error:"+e);
		}
		
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

}
