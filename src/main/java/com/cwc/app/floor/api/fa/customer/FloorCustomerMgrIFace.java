/**
 * 
 */
package com.cwc.app.floor.api.fa.customer;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

/**
 * @author Zhangchangjiang
 *
 */
public interface FloorCustomerMgrIFace {
	public boolean CheckAddressNameUnique(String addressName) throws Exception;

	public boolean CheckCustomerIdUnique(String customerId) throws Exception;

	public boolean CheckCustomerNameUnique(String customerName)
			throws Exception;

	public long insertCustomer(DBRow dbRow) throws Exception;

	public long insertProductStorageCatalog(DBRow dbRow) throws Exception;

	public DBRow getCustomerByCustomerKey(DBRow dbRow) throws Exception;

	public DBRow getProvinceByProId(long proId) throws Exception;

	public int updateCustomerID(DBRow dbRow) throws Exception;

	public int updateProductStorageCatalog(DBRow dbRow) throws Exception;

	public DBRow[] getAllCustomerList(DBRow dbRow, PageCtrl pc)
			throws Exception;

	public DBRow[] getStorageProvinceByCcid(long ccid) throws Exception;

	public DBRow[] getAllCountryCode() throws Exception;
}
