package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorProductStoreLocationMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加库位库存信息
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addProductStoreLocation(DBRow dbrow)
		throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_store_location"), dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ addProductStoreLocation error:"+e);
		}
	}
	
	/**
	 * 删除库位库存
	 * @param ps_location_id
	 * @throws Exception
	 */
	public void delProductStoreLocation(long ps_location_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where ps_location_id="+ps_location_id,ConfigBean.getStringValue("product_store_location"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ addProductStoreLocation error:"+e);
		}
	}
	
	public void modProductStoreLocation(long ps_location_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where ps_location_id="+ps_location_id,ConfigBean.getStringValue("product_store_location"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ addProductStoreLocation error:"+e);
		}
	}
	
	/**
	 * 操作物理库存
	 * @param ps_location_id
	 * @param value
	 * @throws Exception
	 */
	public void inOrOutProductStoreLocationPhysical(long ps_location_id,float value)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.increaseFieldVal(ConfigBean.getStringValue("product_store_location"),"where ps_location_id="+ps_location_id,"physical_quantity",value);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ inOrOutProductStoreLocationPhysical error:"+e);
		}
	}
	
	/**
	 * 操作理论库存
	 * @param ps_location_id
	 * @param value
	 * @throws Exception
	 */
	public void inOrOutProductStoreLocationTheoretical(long ps_location_id,float value)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.increaseFieldVal(ConfigBean.getStringValue("product_store_location"),"where ps_location_id="+ps_location_id,"theoretical_quantity",value);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ inOrOutProductStoreLocationPhysical error:"+e);
		}
	}
	
	/**
	 * 获得所在位置的商品的最老
	 * @param pc_id
	 * @param ps_id
	 * @param slc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getFirstProductStoreLocation(long pc_id,long ps_id,long slc_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_location")
						+" where ps_id = ? and pc_id = ? and slc_id = ? ";
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("pc_id",pc_id);
			para.add("slc_id",slc_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ getFirstProductStoreLocation error:"+e);
		}
	}
	
	/**
	 * 获得该仓库该货物可保留的最老的位置库存
	 * @param ps_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOldestLocationTheoretical(long ps_id,long pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_location")+" as psl "
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" slc on psl.slc_id = slc.slc_id "
						+" where psl.ps_id = ? and psl.pc_id = ? and psl.theoretical_quantity>0 order by psl.time_number,psl.ps_location_id";
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("pc_id",pc_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ getOldestLocationTheoretical error:"+e);
		}
	}
	
	public DBRow[] getOldestLocationTheoreticalNotSlc(long ps_id,long pc_id,long slc_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_location")+" as psl "
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" slc on psl.slc_id = slc.slc_id "
						+" where psl.ps_id = ? and psl.pc_id = ? and psl.slc_id <> ? and psl.theoretical_quantity>0 order by psl.time_number,psl.ps_location_id";
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("pc_id",pc_id);
			para.add("slc_id",slc_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ getOldestLocationTheoretical error:"+e);
		}
	}
	
	/**
	 * 获得该仓库位置上最老的物理值
	 * @param ps_id
	 * @param slc_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOldestLocationPhysical(long ps_id,long slc_id,long pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_location")
						+" where ps_id = ? and pc_id = ? and slc_id = ? and physical_quantity>0 order by time_number,ps_location_id";
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("pc_id",pc_id);
			para.add("slc_id",slc_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ getOldestLocationPhysical error:"+e);
		}
	}
	
	public DBRow getDetailProductStoreLocation(long ps_id,long slc_id,long pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_location")+" as psl "
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" slc on psl.slc_id = slc.slc_id "
						+"where psl.ps_id = ? and psl.pc_id = ? and psl.slc_id = ? and psl.physical_quantity>0";
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("pc_id",pc_id);
			para.add("slc_id",slc_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ getDetailProductStoreLocation error:"+e);
		}
	}
	
	public DBRow[] getAllStoreLocation()
		throws Exception
	{
		String sql = "select ps_id,quantity,pc_id,position from storage_location where pc_id is not null";
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	public DBRow[] productStoreLocationSum()
		throws Exception
	{
		String sql = "select ps_id,pc_id,SUM(physical_quantity) quantity from product_store_location GROUP BY pc_id,ps_id";
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 根据区域获得区域下所有库位库存
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoreLocationByArea(long area_id)
		throws Exception
	{
		try 
		{
			String sql = "select psl.*,slc.slc_position_all from "+ConfigBean.getStringValue("product_store_location")+" as psl "
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on slc.slc_area = ? and psl.slc_id = slc.slc_id "
						+"order by slc.slc_position_all";
			
			
			DBRow para = new DBRow();
			para.add("slc_area",area_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ getProductStoreLocation error:"+e);
		}
	}
	
	/**
	 * 根据位置获得物理库存
	 * @param slc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoreLocationBySlcId(long slc_id)
		throws Exception
	{
		try 
		{
			String sql = "select psl.*,slc.slc_position_all from "+ConfigBean.getStringValue("product_store_location")+" as psl "
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on psl.slc_id = slc.slc_id "
						+"where psl.slc_id = ? "
						+"order by slc.slc_position_all";
			
			
			DBRow para = new DBRow();
			para.add("slc_id",slc_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ getProductStoreLocationBySlcId error:"+e);
		}
	}
	
	/**
	 * 根据区域获得库位库存上的商品信息
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoreLocationProduct(long area_id)
		throws Exception
	{
		try 
		{
			String sql = "select distinct p.* from "+ConfigBean.getStringValue("product_store_location")+" as psl "
			+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on slc.slc_area = ? and psl.slc_id = slc.slc_id "
			+"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = psl.pc_id "
			+"order by p.pc_id";

			DBRow para = new DBRow();
			para.add("slc_area",area_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ getProductStoreLocationProduct error:"+e);
		}
	}
	
	/**
	 * 根据区域获得库位商品条码
	 * @param area_id
	 * @return //返回p.p_name
	 * @throws Exception
	 */
	public DBRow[] getProductStoreLocationProductCode(long area_id)
		throws Exception
	{
		try 
		{
			String sql = "select distinct pc.* ,p.p_name from "+ConfigBean.getStringValue("product_store_location")+" as psl "
			+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on slc.slc_area = ? and psl.slc_id = slc.slc_id "
			+"join "+ConfigBean.getStringValue("product_code")+" as pc on psl.pc_id = pc.pc_id "
			+"join "+ConfigBean.getStringValue("product")+ " as p on p.pc_id =  pc.pc_id "
			+"order by psl.pc_id";
			
			DBRow para = new DBRow();
			para.add("slc_area",area_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ getProductStoreLocationProductCode error:"+e);
		}
	}
	
	public DBRow[] getProductStoreLocationSerialNumber(long area_id)
		throws Exception
	{
		try
		{
			String sql = "select sp.* from "+ConfigBean.getStringValue("product_store_location")+" as psl "
			+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on slc.slc_area = ? and psl.slc_id = slc.slc_id "
			+"join "+ConfigBean.getStringValue("serial_product")+" as sp on psl.pc_id = sp.pc_id and sp.out_time is null "
			+"order by psl.pc_id";

			DBRow para = new DBRow();
			para.add("slc_area",area_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ ");
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
