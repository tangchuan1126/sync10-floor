package com.cwc.app.floor.api.zj;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorWMSAppointmentMgr {
	@Autowired
	private DBUtilAutoTran dbUtilAutoTran;

	
	/**
	 * 获得当前小时需要处理的仓库
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStorageCatalogForQuartz(int ps_type)
		throws Exception
	{
		try 
		{
			String sql = "SELECT id FROM " + ConfigBean.getStringValue("product_storage_catalog") + " WHERE storage_type = " + ps_type;
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWMSAppointmentMgr getProductStorageCatalogForQuartz error:"+e);
		}
	}
	
	/**
	 * 添加预约临时表
	 * @author zhanjie
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addWMSAppointment(DBRow dbrow)
		throws Exception
	{
		return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("appointment_temp"),dbrow);
	}
	
	/**
	 * 基于仓库删除预约临时表
	 * @author zhanjie
	 * @param ps_id
	 * @throws Exception
	 */
	public void delWMSAppointmentByPsid(long ps_id)
		throws Exception
	{
		dbUtilAutoTran.delete("where ps_id = "+ps_id,ConfigBean.getStringValue("appointment_temp"));
	}
	
	/**
	 * 基于仓库删除预约临时表中指定日期的记录
	 * 
	 * 
	 * @author yuehaibo
	 * @param ps_id
	 * @throws Exception
	 */
	public void delWMSAppointmentByPsid(long ps_id, String start_date, String end_date)
		throws Exception
	{
		Date start = DateUtil.utcTime(start_date + " 00:00:00", ps_id);
		start_date = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,start);
		
		Date end = DateUtil.utcTime(end_date + " 23:59:59", ps_id);
		end_date = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat,end);
		
		dbUtilAutoTran.delete("where ps_id = " + ps_id 
				+ " AND DATE_FORMAT(appointment_time, '%Y-%m-%d %H:%i:%S') >= '" + start_date 
				+ "' AND DATE_FORMAT(appointment_time, '%Y-%m-%d %H:%i:%S') <= '" + end_date + "'",
				ConfigBean.getStringValue("appointment_temp"));
	}
	
	/**
	 * 今天未在WMS预约，却今天进行了CheckIn的
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] noTodayOutboundAppointment(long ps_id,String start_time,String end_time)
		throws Exception
	{
		String sql = "select DISTINCT dlod.number_type,dlod.number from door_or_location_occupancy_details as dlod " 
					+"join door_or_location_occupancy_main as dlo on dlod.dlo_id = dlo.dlo_id "
					+"left join appointment_temp as orderLineView on (orderLineView.load_no = dlod.number and orderLineView.load_no is not null and dlod.number_type = 10) or (orderLineView.order_no = dlod.number and dlod.number_type = 11) or (orderLineView.pono = dlod.number and dlod.number_type = 17) "
					+"where dlo.ps_id = "+ps_id+" and dlo.gate_check_in_time >='"+start_time+"' and dlo.gate_check_in_time<='"+end_time+"'  and dlod.number_type in (10,11,17) and orderLineView.order_no is null ";
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	public DBRow[] noTodayInboundAppointment(long ps_id,String start_time,String end_time)
		throws Exception
	{
		String sql = "select DISTINCT dlod.number_type,dlod.number,dlod.receipt_no from door_or_location_occupancy_details as dlod "
					+"join door_or_location_occupancy_main as dlo on dlod.dlo_id = dlo.dlo_id " 
					+"left join appointment_temp as orderLineView on orderLineView.receipt_no = dlod.receipt_no and orderLineView.company_id = dlod.company_id "
					+"where dlo.ps_id = "+ps_id+" and orderLineView.receipt_no is null and dlo.gate_check_in_time >='"+start_time+"' and dlo.gate_check_in_time<='"+end_time+"'  and dlod.number_type in (12,13) "; 
		
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	
	

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
