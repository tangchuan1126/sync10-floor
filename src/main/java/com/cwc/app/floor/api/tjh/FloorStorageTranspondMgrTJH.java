package com.cwc.app.floor.api.tjh;


import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorStorageTranspondMgrTJH {
	
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 添加代发仓库信息
	 * @param rows
	 * @throws Exception 
	 */
	public void addStorageTranspond(DBRow rows) 
		throws Exception 
	{
		try 
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("storage_transpond"));
			rows.add("id", id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("storage_transpond"), rows);
		} 
		catch (Exception e) 
		{
			throw new Exception("addStorageTranspond(DBRow rows) error:"+e);
		}
	}
	
	/**
	 * 分页查询仓库间商品代发信息
	 * @param pc
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllStorageTranspond(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_transpond")+" order by id desc";
			if(pc != null)
			{
				return (dbUtilAutoTran.selectMutliple(sql, pc));
			}
			else
			{
				return (dbUtilAutoTran.selectMutliple(sql));
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllStorageTranspond(PageCtrl pc) error："+e);
		}
		
	}
	
	/**
	 * 理论仓库和商品类别是否存在于仓库间商品代发表中
	 * @param t_category_id
	 * @param t_storage_id
	 * @param parent_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailStorageTransond(long t_category_id,long t_storage_id, long parent_id) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_transpond")+" where y_sid="+t_storage_id+" and catalog_id = "+parent_id+"";
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailStorageTransond(long t_category_id,long t_storage_id, long parent_id) error："+e);
		}
	}

	/**
	 * 查看仓库间商品代发的信息
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getDetailStorageTrandspondById(long id) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_transpond")+" where id="+id;
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailStorageTrandspondById(long id) error："+e);		
		}
	}

	/**
	 * 修改某一具体的仓库间商品的代发
	 * @param transpond_id
	 * @param rows
	 * @throws Exception 
	 */
	public void modStorageTranspond(long transpond_id, DBRow rows) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.update("where id="+transpond_id, ConfigBean.getStringValue("storage_transpond"), rows);
		} 
		catch (Exception e) 
		{
			throw new Exception("modStorageTranspond(long transpond_id, DBRow rows) error："+e);
		}
		
	}

	/**
	 * 删除仓库间商品的代发关系
	 * @param id
	 * @throws Exception 
	 */
	public void delStorageTranspond(long id) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.delete("where id="+id, ConfigBean.getStringValue("storage_transpond"));
		} catch (Exception e) 
		{
			throw new Exception("delStorageTranspond(long id) error："+e);
		}
		
	}

	
	

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	/**
	 * 获取全部的仓库间的商品代发关系
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageTransponds() 
		throws Exception 
	{
		String sql = "select * from "+ConfigBean.getStringValue("storage_transpond");
		return dbUtilAutoTran.selectMutliple(sql);
	}

	
}
