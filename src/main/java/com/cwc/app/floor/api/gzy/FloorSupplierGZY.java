package com.cwc.app.floor.api.gzy;


import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.cwc.app.util.StrUtil;

/*
 * 对供应商维护模块的操作
 */
public class FloorSupplierGZY{
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	//根据id获取supplier的详细信息
	public DBRow getDetailSupplier(long supplierid)throws Exception{
		try{
			
			DBRow row = dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("supplier") + " where id=" + supplierid);
			return row;
		}
		catch (Exception e){
			throw new Exception("getDetailAdmin(row) error:" + e);
		}
}
	
	//获取所有供应商信息
	public DBRow[] getSupplierInfoList(PageCtrl pc)throws Exception{
		try{
			DBRow row[];
			if (pc!=null){
				row = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("supplier") + " order by id desc",pc);
			}
			else{
				row = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("supplier") + " order by id desc");
			}
			return row;
		}
		catch (Exception e){
			throw new Exception("getSupplierInfoList() error:" + e);
		}
	}
	//根据关键字查询供应商的信息
	public DBRow[] getSearchSupplier(String key, PageCtrl pc) throws Exception {
		try{
			DBRow[] row = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("supplier") + " where sup_name like '%"+key+"%' order by id desc",pc);
			return row;
		}
		catch (Exception e)
		{
			throw new Exception("getSearchSupplier() error:" + e);
		}
	}
	//增加供应商信息
	public long addSupplier(DBRow row) throws Exception {
		try{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("supplier"));
			row.add("id",id);
			row.add("password", StrUtil.getMD5(String.valueOf(id)));
			dbUtilAutoTran.insert(ConfigBean.getStringValue("supplier"),row);
			return(id);
		}
		catch (Exception e){
			throw new Exception("addSupplier(row) error:" + e);
		}
		
	}
	//增加供应商信息
	public void modSupplier(long id,DBRow row) throws Exception {
		try {
			dbUtilAutoTran.update("where id=" + id,ConfigBean.getStringValue("supplier"),row);
		} catch (Exception e) {
			throw new Exception("modSupplier() error:" + e);
		}
		
	}
	//根据条件获得供应商的信息
	public DBRow[] getSupplierInfoListByCondition(long nationId,long provinceId,long productLineId,PageCtrl pc)throws Exception{
		try{
			DBRow para = new DBRow();
			
			DBRow row[] = null;
			String sql = "select * from "+ConfigBean.getStringValue("supplier")+" where 1=1 ";
			if(nationId == 0 && provinceId == 0 && productLineId == 0){
				sql = "select * from "+ConfigBean.getStringValue("supplier")+" order by id desc";
				row = dbUtilAutoTran.selectMutliple(sql, pc);
				return row;
			}
			if(nationId != 0){
				para.add("nation_id",nationId);
				sql += "and nation_id=? ";
			}
			if(provinceId != 0){
				para.add("province_id", provinceId);
				sql += "and province_id=? ";
			}
			if(productLineId != 0){
				para.add("product_line_id", productLineId);
				sql += "and product_line_id = ? ";
			}
			sql += "order by id desc";
			if (null != pc){
				row = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			}
			else{
				row = dbUtilAutoTran.selectPreMutliple(sql,para);
			}
			return(row);
		}
		catch (Exception e){
			throw new Exception("getActionsByPage() error:" + e);
		}
		
		
	}
	
	
}
