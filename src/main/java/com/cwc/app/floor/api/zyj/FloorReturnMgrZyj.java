package com.cwc.app.floor.api.zyj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorReturnMgrZyj {
	
	private DBUtilAutoTran dbUtilAutoTran;

	
	
	public DBRow[] findReturnPalletRows(long return_receive_id, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM ").append(ConfigBean.getStringValue("return_pallet_view"))
			.append(" WHERE return_receive_id = ").append(return_receive_id);
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorReturnMgrZyj.findReturnPalletRows(PageCtrl pc):"+e);
		}
	}
	
	public DBRow[] getReturnPalletsByPara(long return_receive_id,String palletNo,String ra_rv,
			String cust_ref_type,String cust_ref_no,PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM ").append(ConfigBean.getStringValue("return_pallet_view")).append(" WHERE return_receive_id= ").append(return_receive_id);
			if(!"".equals(palletNo)){
				sql.append(" AND pallet_no='").append(palletNo).append("'");
			}

			if(!"".equals(ra_rv)){
				sql.append(" AND ra_rv='").append(ra_rv).append("'");
			}
			
			if(!"".equals(cust_ref_type)){
				sql.append(" AND cust_ref_type='").append(cust_ref_type).append("'");
			}
			
			if(!"".equals(cust_ref_no)){
				sql.append(" AND cust_ref_no='").append(cust_ref_no).append("'");
			}
//			System.out.println(sql);
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorReturnMgrZyj.findReturnPalletRows(PageCtrl pc):"+e);
		}
	}
	
	public long addReturnPalletRow(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("return_pallet_view"), row);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorReturnMgrZyj.addReturnPalletRow(DBRow row):"+e);
		}
	}
	
	
	public void updateReturnPalletRow(DBRow row, long palletId) throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where pallet_view_id = " + palletId, ConfigBean.getStringValue("return_pallet_view"), row);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorReturnMgrZyj.updateReturnPalletRow(DBRow row, long palletId):"+e);
		}
	}
	
	/**
	 * 修改pallet的status状态
	 * @param palletStatus   		status的状态
	 * @param pallet_view_id		return_pallet_view表 的pallet_view_id
	 */
	public void updateReturnPalletStatus(int palletStatus,long pallet_view_id) throws Exception
	{
		try
		{
			String sql = "UPDATE return_pallet_view SET status = "+palletStatus+" WHERE pallet_view_id = "+pallet_view_id;
//			System.out.println(sql);
			dbUtilAutoTran.executeSQL(sql);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorReturnMgrZyj.updateReturnPalletStatus(int palletStatus,long pallet_view_id):"+e);
		}
	}
	
	public void deleteReturnPalletRow(long palletId) throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where pallet_view_id = " + palletId, ConfigBean.getStringValue("return_pallet_view"));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorReturnMgrZyj.deleteReturnPalletRow(long palletId):"+e);
		}
	}
	
	
	public DBRow findReturnPalletRowById(long pallet_view_id) throws Exception
	{
		try
		{
			return dbUtilAutoTran.selectSingle("SELECT * FROM return_pallet_view where pallet_view_id = " + pallet_view_id);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorReturnMgrZyj.findReturnPalletRowById(long pallet_view_id):"+e);
		}
	}
	
	public DBRow[] findReturnPalletItemRows(long pallet_view_id, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM ").append(ConfigBean.getStringValue("return_pallet_item"))
			.append(" WHERE pallet_view_id = ").append(pallet_view_id).append(" ORDER BY item_id DESC");
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorReturnMgrZyj.findReturnPalletItemRows(long pallet_view_id, PageCtrl pc):"+e);
		}
	}
	
	public long addReturnPalletItemRow(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("return_pallet_item"), row);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorReturnMgrZyj.addReturnPalletItemRow(PageCtrl pc):"+e);
		}
	}
	
	
	public void updateReturnPalletItemRow(DBRow row, long item_id) throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where item_id = " + item_id, ConfigBean.getStringValue("return_pallet_item"), row);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorReturnMgrZyj.updateReturnPalletItemRow(DBRow row, long item_id):"+e);
		}
	}
	
	public DBRow findReturnPalletItemRowById(long item_id) throws Exception
	{
		try
		{
			return dbUtilAutoTran.selectSingle("SELECT * FROM return_pallet_item where item_id = " + item_id);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorReturnMgrZyj.findReturnPalletItemRowById(long item_id):"+e);
		}
	}
	
	
	public void deleteReturnPalletItemRow(long item_id) throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where item_id = " + item_id, ConfigBean.getStringValue("return_pallet_item"));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorReturnMgrZyj.deleteReturnPalletItemRow(long item_id):"+e);
		}
	}
	
	public int countPalletItemCountByPalletId(long pallet_id)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT count(*) pallet_item_count FROM return_pallet_item WHERE pallet_view_id = ").append(pallet_id);
			return dbUtilAutoTran.selectSingle(sql.toString()).get("pallet_item_count", 0);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorReturnMgrZyj.countPalletItemCountByPalletId(long pallet_id):"+e);
		}
	}
	
	public int countPalletCountByReceiveId(long receive_id)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT COUNT(*) pallet_num_count FROM return_pallet_view WHERE return_receive_id = ").append(receive_id);
			return dbUtilAutoTran.selectSingle(sql.toString()).get("pallet_num_count", 0);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorReturnMgrZyj.countPalletCountByReceiveId(long receive_id):"+e);
		}
	}
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
	
}
