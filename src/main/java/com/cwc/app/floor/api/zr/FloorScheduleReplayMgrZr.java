package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorScheduleReplayMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	

	
	public long addScheduleReplay(DBRow row) throws Exception{
		try{
  			row.add("sch_replay_time",DateUtil.NowStr());
			long adid =Long.parseLong(row.getString("adid"));
			row.add("adid",adid);
			long id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("schedule_replay"), row);
			return id;
		}catch(Exception e){
			throw new Exception("FloorScheduleReplayMgrZr.addScheduleReplay(row):"+e);
		}
	}
	public DBRow[] getRowsByScheduleId(long id) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("schedule_replay") + " where schedule_id="+id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorScheduleReplayMgrZr.addScheduleReplay(row):"+e);
		}
	}
	public void deleteScheduleReplayById(long id) throws Exception{
		try{
			//改成调用存储过程的删除 
			dbUtilAutoTran.delete("where schedule_id="+id, ConfigBean.getStringValue("schedule_replay"));
		}catch(Exception e){
			throw new Exception("FloorScheduleReplayMgrZr.deleteScheduleReplayById(id):"+e);
		}
	}
}
