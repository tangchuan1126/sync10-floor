package com.cwc.app.floor.api;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.beans.FieldInfo;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

/**
 * 对应表为WMS_ORDERS <b>Application describing:</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 <br>
 * <b>Company:vvme</b><br>
 * <b>Date:</b>2014-12-17下午5:47:47<br>
 * 
 * @author cuicong
 */

public class FloorWMSOrdersMgr {
	
	private DBUtilAutoTran_Optm dbUtilAutoTran;
	private String table_schema;
	
	
	public void setTable_schema(String table_schema) {
		this.table_schema = table_schema;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	/**
	 * excel数据导入orderlines临时表
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public boolean save(DBRow row) throws Exception{
	     return this.dbUtilAutoTran.insert(ConfigBean.getStringValue("wms_order_lines_tmp"), row);
	}
	
	/**
	 * 根据customer_id transport_id删除临时表记录
	 * @param cid
	 * @param tid
	 * @throws Exception
	 */
	public void delLinesTmp(String cid,AdminLoginBean user) throws Exception{
		try{
			this.dbUtilAutoTran.delete("where customer_id='"+cid+"' and importer="+user.getAdid(), ConfigBean.getStringValue("wms_order_lines_tmp"));
		}catch(Exception e){
			throw new Exception("delLinesTmp(cid,tid) error:"+e);
		}
	}
	
	/**
	 * 获取order_lines_tmp表字段与excel对应关系相关信息
	 * @return
	 * @throws Exception
	 */
	public Map<Integer,FieldInfo> getFieldInfo() throws Exception{
		Map<Integer,FieldInfo> map=new HashMap<Integer,FieldInfo>();
		
		String sql="select a.is_nullable as isnil,a.column_name as cl ,a.data_type as dt ,a.column_type as ct,a.CHARACTER_MAXIMUM_LENGTH as cml,a.NUMERIC_PRECISION as np,"+
				"a.NUMERIC_SCALE as ns,b.excel_sn as sn,b.excel_title as title,b.format as format,b.order_column_name as ocn from "+
				"information_schema.`COLUMNS` a INNER JOIN "+
				"wms_xls_map b on a.column_name=b.column_name  where "+
				 "a.TABLE_NAME='wms_order_lines_tmp' and b.table_name='wms_order_lines_tmp' and b.enable=1 and b.excel_sn is not null and a.table_schema='"+table_schema+"'";
		
		DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
		
		if(null!=rows){
			for(DBRow row:rows){
				FieldInfo fi=new FieldInfo();
				
				String cl=row.getString("cl"); //字段名字
				fi.setName(cl);
				fi.setType(row.getString("dt")); //字段类型
				fi.setIsNil(row.getString("isnil")); //是否可以为空
				
				Object cml=row.getValue("cml"); //字符类型的最大长度
				if(null!=cml){
					fi.setMaxLen(Integer.valueOf(cml.toString()));
				}
				
				Object np=row.getValue("np"); //数值最大位数
				if(null!=np){
					fi.setNumPs(Integer.valueOf(np.toString()));
				}
				
				Object ns=row.getValue("ns"); //数值小数点最大位数
				if(null!=ns){
					fi.setNumSc(Integer.valueOf(ns.toString()));
				}
				
				Object sn=row.getValue("sn"); //临时表字段对应数据库字段哪列
				int snInt=Integer.valueOf(sn.toString());
				fi.setExcelSn(snInt);
				
				fi.setXlsTitle(row.getString("title"));
				fi.setFormat(row.getString("format"));
				fi.setOrderColumnName(row.getString("ocn"));
				map.put(snInt, fi);
				
			}
			
			return map;
		}
		
		return null;
	}
	
	/**
	 * 根据dn查询order临时表的关于主order的信息
	 * @param dn
	 * @return
	 * @throws Exception
	 */
	public DBRow [] getOrderTmpByDn(String dn,String cid,long uid) throws Exception{
		try{
			String sql="select distinct ship_not_before,ship_not_later,ship_to_zip_code,delivery_plant,dn,order_number,retail_po,order_date,ship_to_party_name,ship_to_address,ship_to_city," +
					"ship_to_state,mabd,req_ship_date,bill_to,loadno,customer_id,freight_type,pickup_appointment_date,pickup_appointment_time,bol_tracking,carrier,country,ft,order_type,"
					+ "d.id as bill_to_id,d.title as bill_to_name,d.deliver_house_number as bill_to_address1,d.deliver_street as bill_to_address2,d.deliver_city as bill_to_city,"
					+ "e.p_code as bill_to_state,f.c_country as bill_to_country,d.deliver_zip_code as bill_to_zip_code,d.deliver_contact as bill_to_contact,d.deliver_phone as bill_to_phone  " +
					"from wms_order_lines_tmp left join product_storage_catalog d on d.title=bill_to "
					+ "left join country_province e on e.pro_id=d.deliver_pro_id left join country_code f on f.ccid=d.deliver_nation where dn='"+dn+"' and customer_id='"+cid+"' and importer="+uid;
			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getLinesTmpByDn(dn) error:"+e);
		}
	}
	
	/**
	 * 查询运输公司
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getCarrierInfo(String scac) throws Exception{
		try{
			String sql="select carrier from carrier_scac_mcdot where scac='"+scac+"'";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("getCarrierInfo(String id) error:"+e);
		}
	}
	
	/**
	 * 获取明细
	 * @param dn
	 * @param cid
	 * @param importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findOrderLines(String dn, String cid, long importer) throws Exception{
		try{
			StringBuilder sb=new StringBuilder();
			sb.append("select title,case when (select pc_id from product where p_name=model_number) is not null then (select pc_id from product where p_name=model_number) else 0 end as pc_id,material_number,material_number,sum(ceil(order_qty)) as delivery_count,"
					+ "case when (material_number='' or material_number is null) then model_number else concat(model_number,'/',material_number) end as b2b_p_name,model_number,sum(ceil(order_qty)) as b2b_count,sum(ceil(order_qty)) as b2b_wait_count,"
					+ "sum(weight)/sum(order_qty) as b2b_weight,sum(weight) as total_weight,sum(pallet_spaces) as pallet_spaces,sum(boxes) as boxes,note")
			.append(" from wms_order_lines_tmp where dn='").append(dn)
			.append("' and customer_id='").append(cid).append("' and importer=").append(importer)
			.append(" group by material_number,model_number,title;");
			return this.dbUtilAutoTran.selectMutliple(sb.toString());
		}catch(Exception e){
			throw new Exception("findOrderLines(String dn, String cid, long importer) error:"+e);
		}
	}
	
	/**
	 * 查询运输公司
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getCarrierId(String fr_company) throws Exception{
		try{
			String sql="select fr_id from freight_resources where fr_company='"+fr_company.trim()+"'";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("getCarrierInfo(String id) error:"+e);
		}
	}
	
	/**
	 * 仓库映射关系
	 * @return
	 * @throws Exception 
	 */
	public DBRow getConfigSearchWare(String delivery_plant) throws Exception{
		try{
			String sql="select ps_id from config_search_warehouse_location where company_id=?";
			DBRow row=new DBRow();
			row.add("delivery_plant", delivery_plant);
			return this.dbUtilAutoTran.selectPreSingle(sql, row);
		}catch(Exception e){
			throw new Exception("getConfigSearchWare(cid) error:"+e);
		}
	}
	
	/**
	 * 查映射
	 * @param delivery_plant
	 * @return
	 * @throws Exception
	 */
	public DBRow getCompanyId(String delivery_plant,String customerId) throws Exception{
		try {
			String sql="select * from wms_company_dic where delivery_plant=? and customer_id=?";
			DBRow para=new DBRow();
			para.put("delivery_plant", delivery_plant);
			para.put("customer_id", customerId);
			return this.dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 查映射
	 * @param delivery_plant
	 * @return
	 * @throws Exception
	 */
	public DBRow getDeliveryPlant(String companyId,String customerId) throws Exception{
		try {
			String sql="select * from wms_company_dic where company_id=? and customer_id=?";
			DBRow para=new DBRow();
			para.put("company_id", companyId);
			para.put("customer_id", customerId);
			return this.dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 校验临时表里数据的正确性
	 * @param cid 客户ID
	 * @param tid 运输公司ID
	 * @throws SQLException 
	 */
	public DBRow [] checkLinesTmp(String cid,long importer) throws Exception{
		try{
			StringBuffer sql=new StringBuffer("select dn,count(1) as num from (SELECT dn,ship_to_address,ship_to_city,ship_to_state,count(1) FROM `wms_order_lines_tmp` where customer_id='")
								.append(cid).append("' and importer=").append(importer)
								.append(" group by dn,ship_to_address,ship_to_city,ship_to_state,loadno) a group by dn having count(1)>1");
			
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql.toString());
			
			return rows;
		}catch(Exception e){
			throw new Exception("checkLinesTmp(cid,tid) error:"+e);
		}
	}
	
	/**
	 * 查找wms_order_lines_tmp
	 * @param para
	 * @return
	 * @throws Exception
	 * @author: ye
	 */
	public DBRow[] getB2bWmsOrderLinesTmp(DBRow para) throws Exception{
		try{
			String sql="select * from "+ConfigBean.getStringValue("wms_order_lines_tmp");
			
			@SuppressWarnings("unchecked")
			ArrayList<String> list=para.getFieldNames();
			
			if(list.size()>0){
				sql+=" where ";
				for(String fn:list){
					sql+=fn+"=? and ";
				}
			}
			
			sql=sql.substring(0,sql.length()-5);
			
			return this.dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch(Exception e){
			throw new Exception("getB2bWmsOrderLinesTmp(DBRow para) error:"+e);
		}
	}
	
	/**
	 * 查找wms_order_lines_tmp
	 * @param para
	 * @return
	 * @throws Exception
	 * @author: ye
	 */
	public DBRow [] getB2bWmsOrderLinesTmp(DBRow para,String [] fields) throws Exception{
		if(null==fields || fields.length<=0)
			return getB2bWmsOrderLinesTmp(para);
		
		try{
			StringBuilder sb=new StringBuilder("select ").append(StringUtils.join(fields, ",")).append(" from ")
					.append(ConfigBean.getStringValue("wms_order_lines_tmp"));
			
			@SuppressWarnings("unchecked")
			ArrayList<String> list=para.getFieldNames();
			
			if(list.size()>0){
				sb.append(" where ");
				for(String fn:list){
					sb.append(fn).append("=? and ");
				}
			}
			
			String sql=sb.toString().substring(0,sb.toString().length()-5);
			
			return this.dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch(Exception e){
			throw new Exception("getB2bWmsOrderLinesTmp(DBRow para,String [] fields) error:"+e);
		}
	}
	
	/**
	 * 插叙出要插入或者更新的dn
	 * @param importer
	 * @param customer_id
	 * @return
	 * @throws Exception
	 */
	public DBRow [] getCorrectData(long importer,String customer_id) throws Exception{
		try{
			String sql="select distinct dn,loadno,correct,country from "+ConfigBean.getStringValue("wms_order_lines_tmp")+" where correct in (4,13,34) and importer="+importer+" and customer_id='"+customer_id+"'";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getCorrectData(DBRow para) error:" + e);
		}
	}
	
	/**
	 * 插叙出要插入或者更新的dn在excel中的行号
	 * @param importer
	 * @param customer_id
	 * @return
	 * @throws Exception
	 */
	public DBRow [] getCorrectDataRow(long importer,String customer_id,Integer... correct) throws Exception{
		try{
			String sql="select row from "+ConfigBean.getStringValue("wms_order_lines_tmp")+" where correct in ("+StringUtils.join(correct,",")+") and importer="+importer+" and customer_id='"+customer_id+"' order by row";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getCorrectDataRow(DBRow para) error:" + e);
		}
	}
	/**
	 * 合并相同sku商品数量重量等
	 * @param cid
	 * @param dn
	 * @param model_number
	 * @param material_number
	 * @param importer
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getItemSum(String cid,String dn,String model_number,String material_number,long importer) throws Exception{
		try{
			String sql="select model_number,sum(order_qty) as order_qty,sum(weight) as weight,sum(boxes) as boxes,sum(pallet_spaces) as pallet_spaces,sum(trailer_feet) as trailer_feet from "+
					ConfigBean.getStringValue("wms_order_lines_tmp")+" where customer_id='"+cid+"' and dn='"+dn+"' and model_number='"+model_number+"' and importer="+importer+" and material_number='"+material_number+"' group by model_number,material_number";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getItemSum(String cid,String dn,String model_number,String importer) error:" + e);
		}
	}
	
	/**
	 * 查询订单shipTo信息
	 * @param dn
	 * @param cid
	 * @return
	 * @throws Exception
	 */
	public DBRow [] getOrderShipTo(String dn,String cid) throws Exception{
		try{
			String sql="select distinct ship_to_party_name,ship_to_address,ship_to_city,ship_to_state,loadno,ship_to_zip_code from "+ConfigBean.getStringValue("wms_order_lines_tmp")
					+" where dn='"+dn+"' and customer_id='"+cid+"'";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getOrderShipTo(String dn,String cid) error:" + e);
		}
	}
	
	/**
	 * 查询此次导入所有dn
	 * @param cid
	 * @param importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] getDnNum(String cid,long importer) throws Exception{
		try{
			String sql="select distinct dn from "+ConfigBean.getStringValue("wms_order_lines_tmp")+" where customer_id='"+cid+"' and importer="+importer;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getDnNum(String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 更新临时表记录校验结果
	 * @param dn
	 * @param customerId
	 * @param importer
	 * @param correct
	 * @throws Exception
	 */
	public void updateCorrect(String dn, String customerId, long importer,int correct) throws Exception{
	   try
	    {
		   String sql="update "+ConfigBean.getStringValue("wms_order_lines_tmp") + " set correct="+correct+" where dn='" + dn+"' and customer_id='"+customerId+"' and importer="+importer;
	      this.dbUtilAutoTran.executeSQL(sql);
	    }
	    catch (Exception e)
	    {
	      throw new Exception("updateCorrect(String dn, String cid, String tid, long uid,int correct) error:" + e);
	    }
	}
	
	/**
	 * 根据b2boid更新
	 * @param b2boid
	 * @param importer
	 * @param correct
	 * @throws Exception
	 */
	public void updateCorrect(String b2boid,long importer,int correct) throws Exception{
	   try
	    {
		   String sql="update "+ConfigBean.getStringValue("wms_order_lines_tmp") +" a inner join b2b_order b"+
				   " on a.customer_id=b.customer_id and a.dn=b.customer_dn set a.correct="+correct+" where  a.correct=19 and b.b2b_oid="+b2boid+" and a.importer="+importer;
	      this.dbUtilAutoTran.executeSQL(sql);
	    }
	    catch (Exception e)
	    {
	      throw new Exception("updateCorrect(String b2boid,long importer,int correct) error:" + e);
	    }
	}
	
	/**
	 * 找出需要校验错误为2的dn
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public DBRow [] findCorrect2(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select dn from "+ConfigBean.getStringValue("wms_order_lines_tmp")+" where  customer_id='"+p_customer_id+"' and importer="+p_importer+" and correct=0 group by dn having count(1)>1";
			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findCorrect2(long p_customer_id,String p_importer) error:" + e);
		}
	}
	
	/**
	 * 校验相同的loadno下的pickup appointment必须相同
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findCorrect26(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select dn,row,pickup_appointment_date,pickup_appointment_time,delivery_plant,carrier,req_ship_date,loadno from "+ConfigBean.getStringValue("wms_order_lines_tmp")+" where loadno in ("
					+"select loadno from (select distinct loadno,pickup_appointment_date,pickup_appointment_time,delivery_plant,carrier,req_ship_date from wms_order_lines_tmp "+
					" where  customer_id='"+p_customer_id+"' and importer="+p_importer+" and correct=0) a group by loadno having count(1)>1) "
							+ "and customer_id='"+p_customer_id+"' and importer="+p_importer+" and correct=0";
			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findCorrect26(long p_customer_id,String p_importer) error:" + e);
		}
	}
	
	/**
	 * 找出sku错误的数据
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect20(String p_customer_id,long p_importer) throws Exception{
		try{
//			String sql="update "+ConfigBean.getStringValue("wms_order_lines_tmp")+" LEFT JOIN product on p_name=CONCAT(model_number,'/',material_number) set correct=20 where "+
//					" customer_id="+p_customer_id+" and importer="+p_importer+" and pc_id is null and correct=0;";
			
			String sql="update "+ConfigBean.getStringValue("wms_order_lines_tmp")+" LEFT JOIN product on p_name=model_number set correct=20 where "+
					" customer_id="+p_customer_id+" and importer="+p_importer+" and pc_id is null and correct=0;";
			
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect20(long p_customer_id,String p_importer) error:" + e);
		}
	}
	
	/**
	 * 获取customer
	 * @param customer
	 * @return
	 */
	public DBRow getCustomer(String customer) throws Exception{
		try{
			String sql="select * from customer_brand where brand_name='"+customer+"'";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("getCustomer(String customer) error:" + e);
		}
	}
	
	/**
	 * 相同DN下的lines不能出现重复的SKU
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect43(String p_customer_id,long p_importer) throws Exception{
		try{
//			String sql="select dn from wms_order_lines_tmp where "+
//					" customer_id="+p_customer_id+" and importer="+p_importer+" and correct=0 group by dn,material_number,model_number,title having count(1)>1;";

			String sql="select dn from wms_order_lines_tmp where "+
					" customer_id="+p_customer_id+" and importer="+p_importer+" and correct=0 group by dn,model_number,title having count(1)>1;";
			
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			for(DBRow row:rows){
				String dn=row.getString("dn");
				updateCorrect(dn,p_customer_id,p_importer,43);
			}
		}catch(Exception e){
			throw new Exception("findCorrect43(long p_customer_id,String p_importer) error:" + e);
		}
	}
	
	/**
	 * 标记title不存在的错误
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect46(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="update "+ConfigBean.getStringValue("wms_order_lines_tmp")+" LEFT JOIN title on title_name=title set correct=46 where "+
					" customer_id="+p_customer_id+" and importer="+p_importer+" and correct=0 and title_id is null;";
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect46(long p_customer_id,String p_importer) error:" + e);
		}
	}
	
	/**
	 * 找出Carrier不存在
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect45(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp a left join carrier_scac_mcdot b on a.carrier=b.scac set correct=45 where "+
					" customer_id="+p_customer_id+" and importer="+p_importer+" and correct=0 and a.carrier is not null and b.id is null;";
			
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect45(long p_customer_id,String p_importer) error:" + e);
		}
	}
	/**
	 * 找出错误类型为9的错误并更新
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect9(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select DISTINCT a.customer_dn from wms_order_lines_tmp b INNER JOIN b2b_order a on a.customer_dn=b.dn "+
						"and a.customer_id=b.customer_id where b.customer_id='"+p_customer_id+"' and importer="+p_importer+" and b.correct=0 and a.b2b_order_status=8";
			
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			if(rows.length>0){
				String dns="'";
				for(DBRow r:rows){
					String dn=r.getString("customer_dn");
					
					dns+=dn+"','";
				}
				dns+="0'";
				
				sql="update "+ConfigBean.getStringValue("wms_order_lines_tmp")+" set correct=9 where dn in ("+dns+") and "+
						" customer_id='"+p_customer_id+"' and importer="+p_importer;
				this.dbUtilAutoTran.executeSQL(sql);
			}
			
		}catch(Exception e){
			throw new Exception("findCorrect9(long p_customer_id,String p_importer) error:" + e);
		}
	}
	
	/**
	 * 找出错误类型为38的错误并更新
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect38(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select DISTINCT a.customer_dn from wms_order_lines_tmp b INNER JOIN b2b_order a on a.customer_dn=b.dn "+
						"and a.customer_id=b.customer_id where b.customer_id='"+p_customer_id+"' and importer="+p_importer+" and b.correct=0 and (a.b2b_order_status=1 or a.b2b_order_status=10)";
			
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			if(rows.length>0){
				String dns="'";
				for(DBRow r:rows){
					String dn=r.getString("customer_dn");
					
					dns+=dn+"','";
				}
				dns+="0'";
				
				sql="update "+ConfigBean.getStringValue("wms_order_lines_tmp")+" set correct=38 where dn in ("+dns+") and "+
						" customer_id='"+p_customer_id+"' and importer="+p_importer;
				this.dbUtilAutoTran.executeSQL(sql);
			}
			
		}catch(Exception e){
			throw new Exception("findCorrect9(long p_customer_id,String p_importer) error:" + e);
		}
	}
	
	/**
	 * 找出0的没判断的数据
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findCorrect0(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select DISTINCT a.customer_dn from wms_order_lines_tmp b INNER JOIN b2b_order a on a.customer_dn=b.dn "+ 
						"and a.customer_id=b.customer_id where b.customer_id='"+p_customer_id+"' and importer="+p_importer+" and b.correct=0 ";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findCorrect0(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 找出dn的title信息
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] findTitleInfo(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select distinct dn,ship_to_party_name,ship_to_address,ship_to_city,ship_to_state from wms_order_lines_tmp where customer_id='"+p_customer_id+"' and importer="+p_importer+" and correct=0 ";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findTitleInfo(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 查找REQ.SHIP DATE时间已过的
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect35(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct=35 where concat(date_format(req_ship_date,'%Y-%m-%d '),'23:59:59')<NOW() and correct=0 and customer_id='"+p_customer_id+"' and importer="+p_importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect35(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 查找REQ.SHIP DATE时间大于mabd时间
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect44(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct=44 where req_ship_date>=mabd and correct=0 and customer_id='"+p_customer_id+"' and importer="+p_importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect44(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * ship_not_before
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect54(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct=54 where ship_not_before is not null and concat(date_format(ship_not_before,'%Y-%m-%d '),'23:59:59')<NOW() and correct=0 and customer_id='"+p_customer_id+"' and importer="+p_importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect54(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * ship_not_before 大于 req_ship_date
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect56(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct=56 where ship_not_before is not null and concat(date_format(ship_not_before,'%Y-%m-%d '),'23:59:59')>concat(date_format(req_ship_date,'%Y-%m-%d '),'23:59:59') and correct=0 and customer_id='"+p_customer_id+"' and importer="+p_importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect56(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * ship_not_before 大于 req_ship_date
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect57(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct=57 where ship_not_later is not null and concat(date_format(ship_not_later,'%Y-%m-%d '),'23:59:59')<concat(date_format(req_ship_date,'%Y-%m-%d '),'23:59:59') and correct=0 and customer_id='"+p_customer_id+"' and importer="+p_importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect57(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * ship_not_before
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect55(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct=55 where ship_not_later is not null and concat(date_format(ship_not_later,'%Y-%m-%d '),'23:59:59')<NOW() and correct=0 and customer_id='"+p_customer_id+"' and importer="+p_importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect55(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 查找pickupappointment时间已过的
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect37(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct=37 where date_format(pickup_appointment_date,concat('%Y-%m-%d ',ifnull(date_format(pickup_appointment_time,'%H:%i:%s'),'23:59:59')))<=NOW() and correct=0 and customer_id='"+p_customer_id+"' and importer="+p_importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect37(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 查找pickupappointment时间大于mabd
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect47(String p_customer_id,long p_importer) throws Exception{ 
		try{
			String sql="update wms_order_lines_tmp set correct=47 where pickup_appointment_date>=mabd and correct=0 and customer_id='"+p_customer_id+"' and importer="+p_importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect37(String p_customer_id,long p_importer) error:" + e);
		}
	}
	/**
	 * 查找MABD时间已过的
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect36(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct=36 where mabd<NOW() and correct=0 and customer_id='"+p_customer_id+"' and importer="+p_importer;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect36(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 仓库信息是否查找到
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public DBRow [] findCorrect8(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select distinct dn,delivery_plant from wms_order_lines_tmp where customer_id='"+p_customer_id+"' and importer="+p_importer;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findCorrect8(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 找出错误类型为5和10的错误并更新
	 * @param p_customer_id
	 * @param p_importer
	 * @param dn
	 * @throws Exception
	 */
	public void findCorrect5And101(String p_customer_id,long p_importer) throws Exception{
		try{
			DBRow [] rows=findCorrect0(p_customer_id,p_importer);
			
			for(DBRow r:rows){
				String dn=r.getString("customer_dn");
				
				String sql="select count(*) as cnt from (select DISTINCT item_id,material_number from b2b_order_item where b2b_oid="+
		 					"(select b2b_oid from b2b_order where customer_dn='"+dn+"' and customer_id='"+p_customer_id+"')) a left join "+
		 				"(select DISTINCT model_number,material_number from wms_order_lines_tmp where customer_id='"+p_customer_id+"' and dn="+dn+" and importer="+p_importer
		 				+" and correct=0) b on a.item_id=b.model_number and a.material_number=b.material_number where item_id is null or model_number is null;";
				
				DBRow row=this.dbUtilAutoTran.selectSingle(sql);
				int cnt=Integer.parseInt(row.getString("cnt"));
				
				if(cnt>0){//先左连接查询 
					updateCorrect(dn,p_customer_id,p_importer,5);
				}else{
//					sql="SELECT count(1) as cnt FROM b2b_order_item b, transport c, transport_detail d WHERE d.transport_id = c.transport_id "+
//					"AND b.b2b_detail_id=d.b2b_detail_id and b.b2b_oid = (select b2b_oid from b2b_order where customer_id='"+p_customer_id+"' and customer_dn='"+dn+"') order by c.transport_id;";
//					
//					row=this.dbUtilAutoTran.selectSingle(sql);
//					cnt=Integer.parseInt(row.getString("cnt"));
					
//					if(cnt>0){
//						updateCorrect(dn,p_customer_id,p_importer,10);
//					}else{
						//右连接查询
						sql="select count(*) as cnt from (select DISTINCT item_id,material_number from b2b_order_item where b2b_oid="+
			 					"(select b2b_oid from b2b_order where customer_dn='"+dn+"' and customer_id='"+p_customer_id+"')) a right join "+
			 				"(select DISTINCT model_number,material_number from wms_order_lines_tmp where customer_id='"+p_customer_id+"' and dn="+dn+" and importer="+p_importer
			 				+" and correct=0) b on a.item_id=b.model_number and a.material_number=b.material_number where item_id is null or model_number is null;";
						
						row=this.dbUtilAutoTran.selectSingle(sql);
						cnt=Integer.parseInt(row.getString("cnt"));
						
						if(cnt>0){
							updateCorrect(dn,p_customer_id,p_importer,5);
						}else{
							sql="select count(*) as cnt from (select DISTINCT item_id,material_number from b2b_order_item where b2b_oid="+
				 					"(select b2b_oid from b2b_order where customer_dn='"+dn+"' and customer_id='"+p_customer_id+"')) a inner join "+
				 				"(select DISTINCT model_number,material_number from wms_order_lines_tmp where customer_id='"+p_customer_id+"' and dn="+dn+" and importer="+p_importer
				 				+" and correct=0) b where a.item_id=b.model_number and a.material_number=b.material_number ;";
							
							row=this.dbUtilAutoTran.selectSingle(sql);
							cnt=Integer.parseInt(row.getString("cnt"));
							
							if(cnt==0){
								updateCorrect(dn,p_customer_id,p_importer,5);
							}
						}
//					}
					
				}
			}
		}catch(Exception e){
			throw new Exception("findCorrect5(String p_customer_id,long p_importer,String dn) error:" + e);
		}
	}
	
	/**
	 * 找出错误类型为5和10的错误并更新
	 * @param p_customer_id
	 * @param p_importer
	 * @param dn
	 * @throws Exception
	 */
	public void findCorrect5And10(String p_customer_id,long p_importer) throws Exception{
		try{
			DBRow [] rows=findCorrect0(p_customer_id,p_importer);
			
			for(DBRow r:rows){
				String dn=r.getString("customer_dn");
				
				String sql="select count(*) as cnt from (select DISTINCT item_id,material_number from b2b_order_item where b2b_oid="+
		 					"(select b2b_oid from b2b_order where customer_dn='"+dn+"' and customer_id='"+p_customer_id+"')) a left join "+
		 				"(select DISTINCT model_number,material_number from wms_order_lines_tmp where customer_id='"+p_customer_id+"' and dn="+dn+" and importer="+p_importer
		 				+" and correct=0) b on a.item_id=b.model_number where item_id is null;";
				
				DBRow row=this.dbUtilAutoTran.selectSingle(sql);
				int cnt=Integer.parseInt(row.getString("cnt"));
				
				if(cnt>0){//先左连接查询 
					updateCorrect(dn,p_customer_id,p_importer,5);
				}else{
						//右连接查询
						sql="select count(*) as cnt from (select DISTINCT item_id,material_number from b2b_order_item where b2b_oid="+
			 					"(select b2b_oid from b2b_order where customer_dn='"+dn+"' and customer_id='"+p_customer_id+"')) a right join "+
			 				"(select DISTINCT model_number,material_number from wms_order_lines_tmp where customer_id='"+p_customer_id+"' and dn="+dn+" and importer="+p_importer
			 				+" and correct=0) b on a.item_id=b.model_number where item_id is null ;";
						
						row=this.dbUtilAutoTran.selectSingle(sql);
						cnt=Integer.parseInt(row.getString("cnt"));
						
						if(cnt>0){
							updateCorrect(dn,p_customer_id,p_importer,5);
						}else{
							sql="select count(*) as cnt from (select DISTINCT item_id,material_number from b2b_order_item where b2b_oid="+
				 					"(select b2b_oid from b2b_order where customer_dn='"+dn+"' and customer_id='"+p_customer_id+"')) a inner join "+
				 				"(select DISTINCT model_number from wms_order_lines_tmp where customer_id='"+p_customer_id+"' and dn="+dn+" and importer="+p_importer
				 				+" and correct=0) b where a.item_id=b.model_number;";
							
							row=this.dbUtilAutoTran.selectSingle(sql);
							cnt=Integer.parseInt(row.getString("cnt"));
							
							if(cnt==0){
								updateCorrect(dn,p_customer_id,p_importer,5);
							}
						}
//					}
					
				}
			}
		}catch(Exception e){
			throw new Exception("findCorrect5And10(String p_customer_id,long p_importer,String dn) error:" + e);
		}
	}
	
	public DBRow [] findPsInfoByTitleCityZipRetailAddress(String l_ship_to_party_name,String l_ship_to_city,
			String l_ship_to_zip_code,String l_retailerid,String l_ship_to_address) throws Exception{
		try{
			String sql="select * from product_storage_catalog where title=REPLACE('"+l_ship_to_party_name+"',' ','_') "
					+ " and deliver_city like concat('%','"+l_ship_to_city+"','%') and deliver_zip_code='"+l_ship_to_zip_code
					+"' and deliver_street like REPLACE('"+l_ship_to_address+"',' ','')"+" and ship_to_id="+l_retailerid;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findPsInfoByTitleCityZipRetailAddress(String l_ship_to_party_name,String l_ship_to_city,String l_ship_to_zip_code,String l_retailerid,String l_ship_to_address) error:" + e);
		}
	}
	
	public DBRow [] findPsInfoByTitleCityZipRetail(String l_ship_to_party_name,String l_ship_to_city,
			String l_ship_to_zip_code,String l_retailerid) throws Exception{
		try{
			String sql="select * from product_storage_catalog where title=REPLACE('"+l_ship_to_party_name+"',' ','_') "
					+ " and deliver_city like concat('%','"+l_ship_to_city+"','%') and deliver_zip_code='"+l_ship_to_zip_code
					+"' and ship_to_id="+l_retailerid;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findPsInfoByTitleCityZipRetail(String l_ship_to_party_name,String l_ship_to_city,String l_ship_to_zip_code,String l_retailerid) error:" + e);
		}
	}
	
	public DBRow [] findPsInfoByTitleCityZip(String l_ship_to_party_name,String l_ship_to_city,String l_ship_to_zip_code) throws Exception{
		try{
		String sql="select * from product_storage_catalog where title=REPLACE('"+l_ship_to_party_name+"',' ','_') "
				+ " and deliver_city like concat('%','"+l_ship_to_city+"','%') and deliver_zip_code='"+l_ship_to_zip_code+"'";
		return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findPsInfoByTitleCityZip(String l_ship_to_party_name,String l_ship_to_city,String l_ship_to_zip_code) error:" + e);
		}
	}
	
	public DBRow [] findPsInfoByTitleCity(String l_ship_to_party_name,String l_ship_to_city) throws Exception{
		try{
		String sql="select * from product_storage_catalog where title=REPLACE('"+l_ship_to_party_name+"',' ','_') "
				+ " and deliver_city like concat('%','"+l_ship_to_city+"','%') ";
		return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findPsInfoByTitleCity(String l_ship_to_party_name,String l_ship_to_city) error:" + e);
		}
	}
	
	public DBRow findDNOtherErr(String dn,String p_customer_id,long p_importer) throws Exception{
		try {
			String sql="select count(1) as cnt from wms_order_lines_tmp where correct<>0 and dn='"+dn+"' and customer_id='"+p_customer_id+"' and importer="+p_importer;
			return this.dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("findDNOtherErr(String dn,String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 检查相同dn的其他明显有没有问题
	 * @param dn
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public boolean checkDnLines(String dn,String p_customer_id,long p_importer) throws Exception{
		String sql="select count(1) as cnt from wms_order_lines_tmp where correct<>0 and dn='"+dn+"' and customer_id='"+p_customer_id+"' and importer="+p_importer;
		
		DBRow db=this.dbUtilAutoTran.selectSingle(sql);
		
		//去掉 一个dn对应多条明细部分sku能对应上,部分sku对应不上的问题 这种明细不处理只标记出来
		if(Integer.parseInt(db.getString("cnt"))>0){
			return false;
		}
		return true;
	}
	
	/**
	 * 检查地址
	 * @param titles
	 * @param address
	 * @return
	 * @throws Exception
	 */
	public DBRow checkAddress(String titles,String address) throws Exception{
		try{
			String sql="select count(1) as cnt from product_storage_catalog where title='"+titles+"' and deliver_street='"+address+"'";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("checkAddress(String titles,String address) error:" + e);
		}
	}
	
	/**
	 * 查找出已经存在的dn
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect11(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp a left join b2b_order b on a.dn=b.customer_dn and a.customer_id=b.customer_id set a.correct=11 where a.importer="+p_importer+" and a.customer_id='"+p_customer_id+"' and b.b2b_oid is not null";
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect11(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 查找出已经存在的dn
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect4(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="update wms_order_lines_tmp a left join b2b_order b on a.dn=b.customer_dn and a.customer_id=b.customer_id set correct=4 where a.importer="+p_importer+" and a.customer_id='"+p_customer_id+"' and b.b2b_oid is null";
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect4(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 一个order是能FTL一个load
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect53(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select loadno from wms_order_lines_tmp where correct=0 and freight_type='ftl' and loadno is not null and importer="+p_importer+" and customer_id='"+p_customer_id+"'";
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			String pre="select distinct dn,loadno from wms_order_lines_tmp where correct=0 and importer="+p_importer+" and customer_id='"+p_customer_id+"' and loadno=?";
			DBRow para=new DBRow();
			for(DBRow row:rows){
				String loadno=row.getString("loadno");
				para.put("loadno", loadno);
				DBRow [] dbs=this.dbUtilAutoTran.selectPreMutliple(pre, para);
				if(dbs.length>1){
					for(DBRow r:dbs){
						String dn=r.getString("dn");
						updateCorrect(dn,p_customer_id,p_importer,53);
					}
				}
			}
		}catch(Exception e){
			throw new Exception("findCorrect53(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 更新load时,有loadno值carrier必填
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect40(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="update  wms_order_lines_tmp a left join carrier_scac_mcdot b on a.carrier=b.scac set correct=40 where  a.importer="+p_importer+" and a.customer_id='"+p_customer_id+"' and correct=0 and loadno is not null and b.carrier is null";
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("findCorrect40(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 找出完全一样的dn
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect19(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select DISTINCT a.dn,b.b2b_oid,a.correct from wms_order_lines_tmp a left join b2b_order b on a.dn=b.customer_dn and a.customer_id=b.customer_id "+
						"and a.ship_to_city=b.deliver_city and a.ship_to_state=b.address_state_deliver and REPLACE(a.ship_to_address,' ','')=b.deliver_house_number and "+ 
						"REPLACE(a.ship_to_party_name,' ','_')=b.ship_to_party_name and a.retail_po=b.retail_po and a.order_number=b.order_number and ifnull(a.carrier,'')=b.carriers "+
						"and ifnull(a.bol_tracking,'')=b.bol_tracking "+
						"and ifnull(a.delivery_appointment,'')=b.delivery_appointment where a.importer="+p_importer+" and a.customer_id='"+p_customer_id+"' and a.correct in (0,14,11)";
			
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			for(DBRow r:rows){
				String b2b_oid=r.getString("b2b_oid");
				String dn=r.getString("dn");
				String correct=r.getString("correct");
				
				//0的需要判断是否有相同dn的其他Line有问题
				if(correct.equals("0")){
					sql="select count(1) as cnt from wms_order_lines_tmp where correct<>0 and dn='"+dn+"' and customer_id='"+p_customer_id+"' and importer="+p_importer;
					
					DBRow db=this.dbUtilAutoTran.selectSingle(sql);
					
					//去掉 一个dn对应多条明细部分sku能对应上,部分sku对应不上的问题 这种明细不处理只标记出来
					if(Integer.parseInt(db.getString("cnt"))>0){
						continue;
					}
				}
				
				if(StringUtils.isEmpty(b2b_oid)){
					updateCorrect(dn,p_customer_id,p_importer,21);
					continue;
				}
				
				sql="select count(1) as cnt from (select  item_id,sum(b2b_delivery_count) as a1 ,sum(total_weight) as a2,sum(pallet_spaces) as a3,sum(trailer_feet) as a4,sum(config_change) as a5,"+
						"sum(new_multiplier) as a6,sum(internal_cost) as a7,sum(boxes) as a8 from b2b_order_item where b2b_oid="+b2b_oid+" group by item_id,material_number,sloc) a "+
						"left join (select model_number,sum(ceil(order_qty)) as a1,sum(weight) as a2,sum(pallet_spaces) as a3,sum(trailer_feet) as a4,sum(config_change) as a5,"+
						"sum(new_multiplier) as a6,sum(internal_cost) as a7,sum(boxes) as a8 "+
						"from wms_order_lines_tmp where dn='"+dn+"' and customer_id='"+p_customer_id+"' and importer="+p_importer+
					    " group by material_number,model_number,sloc) b on a.a1=b.a1 and a.a2=b.a2 and a.a3=b.a3 and a.a4=b.a4 "+ 
					    "and a.a5=b.a5 and a.a6=b.a6 and a.a7=b.a7 and a.a8=b.a8 where b.model_number is null";
				
				DBRow db=this.dbUtilAutoTran.selectSingle(sql);
				
				if(Integer.parseInt(db.getString("cnt"))>0){
					updateCorrect(dn,p_customer_id,p_importer,21);
					continue;
				}else{
					updateCorrect(dn,p_customer_id,p_importer,19);
//					sql="select a.load_no as a1,b.loadno as a2,a.pickup_appointment as a3,b.pickup_appointment as a4 from b2b_order a left join "
//							+ "(select DISTINCT customer_id,dn,loadno,pickup_appointment from wms_order_lines_tmp where customer_id='"+p_customer_id+"' and dn='"+dn+"' "
//									+ " and importer="+p_importer+") b on a.customer_id=b.customer_id and a.customer_dn=b.dn where b2b_oid="+b2b_oid;
//					
//					db=this.dbUtilAutoTran.selectSingle(sql);
//					
//					String b2bloadno=db.getString("a1");
//					String loadno=db.getString("a2");
//					String b2bappoint=db.getString("a3");
//					String appoint=db.getString("a4");
//					
//					if(StringUtils.isEmpty(b2bappoint) && !StringUtils.isEmpty(appoint)){
//						updateCorrect(dn,p_customer_id,p_importer,15);
//					}else{
//						if(!StringUtils.isEmpty(b2bloadno)){
//							if(!b2bloadno.equals(loadno)){
//								updateCorrect(dn,p_customer_id,p_importer,22);
//							}
//						}else{
//							updateCorrect(dn,p_customer_id,p_importer,15);
//						}
//					}
//					
//					if(StringUtils.isEmpty(b2bappoint) && !StringUtils.isEmpty(appoint)){
//						updateCorrect(dn,p_customer_id,p_importer,24);
//					}else{
//						if(!b2bappoint.equals(appoint)){
//							updateCorrect(dn,p_customer_id,p_importer,23);
//						}else{
//							updateCorrect(dn,p_customer_id,p_importer,19);
//						}
//					} 
				}
			}
		}catch(Exception e){
			throw new Exception("findCorrect19(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 查找都有的dn
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] findCommonDn(String p_customer_id,long p_importer) throws Exception{
		try{
//			String sql="select distinct dn from (select * from wms_order_lines_tmp where customer_id='"+p_customer_id+
//					"' and importer="+p_importer+" and correct in (0,14,11)) a,(select * from b2b_order where customer_id='"+p_customer_id+"' and b2b_order_status<>6) b where a.dn=b.customer_dn";
			
			String sql="select distinct dn from wms_order_lines_tmp where customer_id='"+p_customer_id+
					"' and importer="+p_importer+" and correct in (14,11)";
			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findCommonDn(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 找出load关系有变化
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect22(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select a.customer_dn,a.load_no,b.loadno from b2b_order a INNER JOIN wms_order_lines_tmp b on a.customer_id=b.customer_id and a.customer_dn=b.dn"+
					"where a.b2b_order_status<>6 and b.correct=0 and customer_id='"+p_customer_id+"' "
										+ " and importer="+p_importer;
			
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			for(DBRow row:rows){
				String dn=row.getString("customer_dn");
				String b2bloadno=row.getString("load_no");
				String loadno=row.getString("loadno");
				
				if(StringUtils.isEmpty(b2bloadno) && !StringUtils.isEmpty(loadno)){
					updateCorrect(dn,p_customer_id,p_importer,15);
					continue;
				}else{
					updateCorrect(dn,p_customer_id,p_importer,22);
					continue;
				}
			}
			
			return ;
		}catch(Exception e){
			throw new Exception("findCorrect22(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 查找错误类型31的数据
	 * @param p_customer_id
	 * @param p_importer
	 * @throws Exception
	 */
	public void findCorrect31(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select a.dn,a.loadno,b.b2b_oid from wms_order_lines_tmp a left join b2b_order b on a.dn=b.customer_dn "
					+ "and a.customer_id=b.customer_id where a.correct=19  and a.customer_id='"+p_customer_id+"' "
										+ " and a.importer="+p_importer;
					
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			String loadnoSql="select order_id from wms_load_order_master where load_no=?";
			
			Map<String,Set<String>> map=new HashMap<String,Set<String>>();
			Map<String,String> dnOrder=new HashMap<String,String>();
			for(DBRow row:rows){
				String dn=row.getString("dn");
				String loadno=row.getString("loadno");
				String b2boid=row.getString("b2b_oid");
				
				dnOrder.put(b2boid, dn);
				
				if(!map.containsKey(loadno)){
					Set<String> set=new HashSet<String>();
					set.add(b2boid);
					
					map.put(loadno, set);
				}else{
					map.get(loadno).add(b2boid);
				}
			}
			
			Iterator<Entry<String, Set<String>>> it=map.entrySet().iterator();
			DBRow para=new DBRow();
			
			while(it.hasNext()){
				Entry<String,Set<String>> entry=(Entry<String,Set<String>>)it.next();
				
				String loadno=entry.getKey();
				Set<String> oids=entry.getValue();
				
				para.put("load_no", loadno);
				
				DBRow [] datas=this.dbUtilAutoTran.selectPreMutliple(loadnoSql, para);
				
				if(datas.length==0 || null==datas)
					continue;
				
				Set<String> orderIds=new HashSet<String>();
				
				for(DBRow r:datas){
					String order_id=r.getString("order_id");
					orderIds.add(order_id);
				}
				
				if(oids.size()!=orderIds.size()){
					for(String oid:oids)
						updateCorrect(oid,p_importer,31);
				}else{
					oids.removeAll(orderIds);
					
					if(!oids.isEmpty()){
						for(String oid:oids)
							updateCorrect(oid,p_importer,31);
					}
				}
				
			}
			
		}catch(Exception e){
			throw new Exception("findCorrect31(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	public void findCorrect32(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select distinct loadno from wms_order_lines_tmp where correct=19 and customer_id='"+p_customer_id+"' "
										+ " and importer="+p_importer;
			
			DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
			
			String osql="select distinct correct from wms_order_lines_tmp where loadno=? and correct<>19 and customer_id='"+p_customer_id+"' "
										+ " and importer="+p_importer;
			
			DBRow para=new DBRow();
			for(DBRow r:rows){
				String loadno=r.getString("loadno");
				
				if(!StringUtils.isEmpty(loadno)){
					para.put("loadno", loadno);
					DBRow [] datas=this.dbUtilAutoTran.selectPreMutliple(osql, para);
					
					if(datas.length>0){
						updateCorrectByLoadno(loadno,p_customer_id,p_importer,32);
					}
				}
						
			}
		}catch(Exception e){
			throw new Exception("findCorrect32(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	public void updateCorrectByLoadno(String loadno,String p_customer_id, long p_importer,int correct) throws Exception{
		try{
			String sql="update wms_order_lines_tmp set correct="+correct+" where customer_id='"+p_customer_id+"' "
											+ " and importer="+p_importer+" and loadno='"+loadno+"' and correct=19";
			
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("updateCorrectByLoadno(String loadno,String p_customer_id, long p_importer,int correct) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws SQLException
	 */
	public DBRow [] getLoadNo(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select distinct dn,loadno from wms_order_lines_tmp  where correct in (15,24) and customer_id='"+p_customer_id+"' "
										+ " and importer="+p_importer;
			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getLoadNo(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception 
	 */
	public DBRow [] getPkAppoint(String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="select distinct dn,loadno from wms_order_lines_tmp  where correct=14 and customer_id='"+p_customer_id+"' "
										+ " and importer="+p_importer;
			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getPkAppoint(String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 更新load
	 * @param dn
	 * @param customer_id
	 * @param loadno
	 * @throws Exception
	 */
	public void updateLoadno(String dn,String customer_id,String loadno) throws Exception{
		String sql="update b2b_order set load_no='"+loadno+"' where customer_dn='"+dn+"' and customer_id='"+customer_id+"'";
		this.dbUtilAutoTran.executeSQL(sql);
	}
	
	/**
	 * 更新pickupappointment
	 * @param dn
	 * @param customer_id
	 * @param appointment
	 * @throws Exception
	 */
	public void updatePickupAppointment(String dn,String customer_id,String appointment) throws Exception{
		String sql="update b2b_order set pickup_appointment='"+appointment+"' where customer_dn='"+dn+"' and customer_id='"+customer_id+"'";
		this.dbUtilAutoTran.executeSQL(sql);
	}
	
	/**
	 * 导入dn时查询校验dn下某些列必须相同
	 * @param dn
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findByDn(String dn,String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="SELECT delivery_plant,dn,retail_po,order_number,order_date,ship_to_party_name,"+
						"ship_to_address,ship_to_city,ship_to_state,"+
						"ship_to_zip_code,country,mabd,req_ship_date,loadno,pickup_appointment_date,pickup_appointment_time,"+
						"ship_date,bol_tracking,carrier,bill_to,ft,row FROM wms_order_lines_tmp where dn='"+dn+"' and customer_id='"+p_customer_id+"' and importer="+p_importer;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findByDn(String dn,String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 查询导入load时需要校验dn下某些列必须相同
	 * @param dn
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findLoadNoByDn(String dn,String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="SELECT dn,loadno,pickup_appointment_date,pickup_appointment_time,"+
						"row FROM wms_order_lines_tmp where dn='"+dn+"' and customer_id='"+p_customer_id+"' and importer="+p_importer;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findLoadNoByDn(String dn,String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 查询导入load时需要校验dn下某些列必须相同
	 * @param dn
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] findFtBillByDn(String dn,String p_customer_id,long p_importer) throws Exception{
		try{
			String sql="SELECT dn,bill_to,ft,"+
						"row FROM wms_order_lines_tmp where dn='"+dn+"' and customer_id='"+p_customer_id+"' and importer="+p_importer;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findFtBillByDn(String dn,String p_customer_id,long p_importer) error:" + e);
		}
	}
	
	/**
	 * 查找完全一样的dn后在进行load比较
	 * @param p_customer_id
	 * @param p_importer
	 * @return
	 * @throws Exception
	 */
	public DBRow [] getCorrect(String p_customer_id,long p_importer,Set<Integer> correct) throws Exception{
		try{
			String sql="select distinct dn,loadno,pickup_appointment_date,pickup_appointment_time,correct,carrier,ft,bill_to from "+ConfigBean.getStringValue("wms_order_lines_tmp")+" where correct in ("+StringUtils.join(correct, ",")+") and customer_id='"+p_customer_id+"' and importer="+p_importer;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getCorrect(String p_customer_id,long p_importer,Set<Integer> correct) error:" + e);
		}
	}
	
	/**
	 * 导出模板
	 * @return
	 * @throws Exception
	 */
	public DBRow [] getExcelExport() throws Exception{
		try{
			String sql="select * from wms_xls_export";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getExcelExport() error:" + e);
		}
	}
	
	/**
	 * 获取订单的总托盘数
	 * @param oids
	 * @return
	 * @throws Exception
	 */
	public DBRow getTotalPallets(String oids) throws Exception{
		try{
			String sql="select sum(pallet_spaces) as total_pallets from b2b_order_item where b2b_oid in ("+oids+")";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("getTotalPallets(String oids) error:" + e);
		}
	}
	
	/**
	 * 导入load 时 更新carrier和freight type
	 * @param cid
	 * @param importer
	 * @throws Exception
	 */
	public void updateCarrierAndFreightType(String cid,long importer) throws Exception{
		try{
			String sql="update b2b_order a inner join (select distinct dn,customer_id,b.carrier,upper(freight_type) as freight_type,c.carrier as carrname from wms_order_lines_tmp b left join carrier_scac_mcdot c on b.carrier=c.scac where correct in (15,24,19,41,42) and  customer_id='"+cid+"' and importer="+importer+") b on a.customer_dn=b.dn and a.customer_id=b.customer_id"+ 
					" set carriers=b.carrier,freight_carrier=b.freight_type,b2b_order_waybill_name=b.carrname ";
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("updateCarrierAndFreightType(String cid,long importer) error:" + e);
		}
	}
	
	public DBRow [] findSO(String cid,long importer,int correct) throws Exception{
		try{
			String sql="select b.b2b_oid,b.order_number as so,a.order_number,a.dn,a.row from wms_order_lines_tmp a left join b2b_order b on b.customer_dn=a.dn and b.customer_id=a.customer_id and a.correct="+correct+" and a.customer_id='"+cid+"' and a.importer="+importer +" where b.b2b_oid is not null";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findSO(String cid,long importer) error:" + e);
		}
	}

/***************************************************************************/
/********** 其他字典表所需要DAO **************/
/***************************************************************************/
	
	/**
	 * 
	 * @param dn
	 * @param cid
	 * @param importer
	 * @return
	 * @throws Exception
	 */
	public DBRow getDeliverStreetByDn(String dn,String cid,long importer) throws Exception{
		try{
			String sql="select deliver_street from product_storage_catalog where title=(select REPLACE(ship_to_party_name,' ','_') from wms_order_lines_tmp where dn='"+dn+"' and customer_id='"+cid+"' and importer="+importer+" limit 1);";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("getDeliverStreetByDn(String dn,String cid,long importer) error:" + e);
		}
	}
	
	/**
	 * 获取时区
	 * @return
	 * @throws Exception
	 *
	*/
	public DBRow [] getJetLag() throws Exception{
		try{
			String sql = "SELECT ps.id, cp.timezone_id FROM "+ConfigBean.getStringValue("product_storage_catalog")+" ps "
					+ " join country_province cp on cp.pro_id = ps.pro_id"
					+ " where cp.timezone_id is not null";
			DBRow[] jetLags = this.dbUtilAutoTran.selectMutliple(sql);
			return jetLags;
		}catch(Exception e){
			throw new Exception("getJetLag() error:" + e);
		}
	}
	
	/**
	 * 根据国家ID查找国家名称
	 * @param ccid
	 * @return
	 * @throws Exception
	 */
	public DBRow findCountry(int ccid) throws Exception{
		try{
			String sql="select c_country from "+ConfigBean.getStringValue("country_code")+" where ccid="+ccid;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("findCcid(String country) error:" + e);
		}
	}
	
	/**
	 * 根据国家名称查询国家ID
	 * @param country
	 * @return
	 * @throws Exception
	 */
	public DBRow findCcid(String country) throws Exception{
		try{
			String sql="select ccid from "+ConfigBean.getStringValue("country_code")+" where c_country='"+country+"'";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("findCcid(String country) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param shipToId
	 * @return
	 * @throws Exception
	 */
	public DBRow getShipToName1(String shipToId) throws Exception{
		try{
			String sql="select * from ship_to where ship_to_id=?";
			DBRow para=new DBRow();
			para.put("ship_to_id", shipToId);
			return this.dbUtilAutoTran.selectPreSingle(sql, para);
		}catch(Exception e){
			throw new Exception("getShipToName(String shipToId) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param shipTo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getShipToId(String shipTo) throws Exception{
		try{
			String sql="select * from ship_to where '"+shipTo+"' like CONCAT('%',ship_to_name,'%')";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getShipToId(String shipTo) error:" + e);
		}
	}
	
	/**
	 * 仓库映射关系
	 * @return
	 * @throws Exception 
	 */
	public DBRow getPsidByTitle(String delivery_plant) throws Exception{
		try{
			String sql="select id from product_storage_catalog where title=?";
			DBRow row=new DBRow();
			row.add("title", delivery_plant);
			return this.dbUtilAutoTran.selectPreSingle(sql, row);
		}catch(Exception e){
			throw new Exception("getConfigSearchWare(cid) error:"+e);
		}
	}
	
/***************************************************************************/
/********** 以下 Load APPT 模块所需要DAO **************/
/***************************************************************************/
	
	/**
	 * 
	 * @param oids
	 * @return
	 * @throws Exception
	 */
	public DBRow [] checkOrder(String oids) throws Exception{
		try{
			String sql="select distinct freight_carrier,customer_id from b2b_order where b2b_oid in ("+oids+")";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("checkOrder(String oids) error:" + e);
		}
	}

	
/***************************************************************************/
/********** 过程调用 DAO **************/
/***************************************************************************/

	/**
	 * 调用过程做数据比对
	 * @param importer
	 * @param cid
	 * @param nation_id
	 * @throws Exception
	 */
	public void callProcedureForDiff(long importer,String cid,long nation_id) throws Exception{
		this.dbUtilAutoTran.CallStringSingleResultProcedure("call sp_diff_excel_data('"+cid+"',"+importer+","+nation_id+")");
	}
	
	/**
	 * 调用过程做数据比对
	 * @param importer
	 * @param cid
	 * @param nation_id
	 * @throws Exception
	 */
	public void callProcedureForIn(long importer,String cid,long nation_id,String employName) throws Exception{
		this.dbUtilAutoTran.CallStringSingleResultProcedure("call sp_excel_import_data('"+cid+"',"+importer+","+nation_id+",'"+employName+"')");
	}
	
	/**
	 * b2b_order 写入 WMS订单编号
	 * 
	 * @param oid
	 * @param wid
	 * @throws Exception
	 */
	public void updateB2bOrder(long oid, long wid) throws Exception {
		if (oid==0 || wid==0) throw new Exception("No orderid");
		DBRow data = new DBRow ();
		data.add("b2b_order_number", String.valueOf(wid));
		data.add("oid", oid);
		int r = dbUtilAutoTran.updatePreNoTx("update b2b_order set b2b_order_number =? where b2b_oid =?", data, "b2b_order");
		if (r!=1) throw new Exception("No update");
	}
}
