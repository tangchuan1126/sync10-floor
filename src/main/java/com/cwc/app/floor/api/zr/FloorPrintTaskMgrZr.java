package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorPrintTaskMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public int countPrintTask(String who, int status) throws Exception{
		int countSum = 0 ;
		try{
			String tableName =  ConfigBean.getStringValue("print_task");
			String sql = "select count(*) as count_sum from " + tableName + " where who= ? and state = ? ";
			DBRow param = new DBRow();
			param.add("who", who);
			param.add("state",status);
			DBRow countSumRow = dbUtilAutoTran.selectPreSingle(sql,param);
			if(countSumRow != null){
				countSum = countSumRow.get("count_sum", 0);
			}
			return countSum ;
		}catch(Exception e){
			throw new Exception("FloorAndroidMgrZr.countPrintTask(who,status):"+e);
		}
	}
	
	public DBRow[] queryPrintTask(String who, int status) throws Exception{
 		try{
			String tableName =  ConfigBean.getStringValue("print_task");
			String sql = "select *  from " + tableName + " where who= ? and state = ? ";
			DBRow param = new DBRow();
			param.add("who", who);
			param.add("state",status);
			return dbUtilAutoTran.selectPreMutliple(sql,param);
		}catch(Exception e){
			throw new Exception("FloorAndroidMgrZr.queryPrintTask(who,status):"+e);
		}
	}
	public DBRow[] queryPrintTaskByPrintId(long print_id , int status ) throws Exception{
 		try{
			String tableName =  ConfigBean.getStringValue("print_task");
			String sql = "select *  from " + tableName + " where print_server_id="+print_id+" and state ="+status ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorAndroidMgrZr.queryPrintTask(who,status):"+e);
		}
	}
	public DBRow[] queryPrintTaskByPrintIdOrderbyTime(long print_id , int status ) throws Exception{
 		try{
			String tableName =  ConfigBean.getStringValue("print_task");
			String sql = "select *  from " + tableName + " where print_server_id="+print_id+" and state ="+status + " order by date desc " ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorAndroidMgrZr.queryPrintTask(who,status):"+e);
		}
	}
	public void updatePrintTaskState(long task_id , int state) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("print_task");
			DBRow data = new DBRow();
			data.add("state", state);
			dbUtilAutoTran.update(" where print_task_id = "+ task_id, tableName, data);
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.updatePrintTaskState(task_id,state):"+e);
		}
	}
	public void updatePrintTask(long task_id , DBRow updateRow) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("print_task");
			dbUtilAutoTran.update(" where print_task_id = "+ task_id, tableName, updateRow);
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.updatePrintTask(task_id,updateRow):"+e);
		}
	}
	public long addPrintTask(DBRow data) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("print_task");
			return dbUtilAutoTran.insertReturnId(tableName, data);
		}catch(Exception e){
			throw new Exception("FloorAndroidMgrZr.addPrintTask(row):"+e);
		}
	}
	public DBRow[] getAllAndroidPrintServer() throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("android_printer_server");
			String sql = "select * from " + tableName ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorAndroidMgrZr.getAllAndroidPrintServer():"+e);
		}
	}
	
	public DBRow[] getAllAndroidPrintServerByPsId(long ps_id) throws Exception{
		try{
			String sql = " SELECT c.*, GROUP_CONCAT(DISTINCT e.area_name ORDER BY e.area_name ) area_name  FROM "+
						" android_printer_server c "+
						" LEFT JOIN  storage_zone_printerservice d "+
						" ON d.service_id = c.printer_server_id "+
						" LEFT JOIN  storage_location_area  e "+
						" ON d.area_id = e.area_id "+
						" where c.ps_id="+ps_id+
						" GROUP BY c.printer_server_id ";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorAndroidMgrZr.getAllAndroidPrintServerByPsId(ps_id):"+e);
		}
	}
	
	
	public long addAnroidPrintServer(DBRow data) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("android_printer_server");
			return dbUtilAutoTran.insertReturnId(tableName, data);
		}catch(Exception e){
			throw new Exception("FloorAndroidMgrZr.addAnroidPrintServer(row):"+e);
		}
	}
	public int countAndroidPrintServerNameBy(String serverName) throws Exception{
		int count = 0 ;
		try{
			String tableName =  ConfigBean.getStringValue("android_printer_server");
			String sql = "select count(*) as count_sum from " + tableName + " where printer_server_name= ? " ;
			DBRow param = new DBRow();
			param.add("printer_server_name", serverName);
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, param);
			if(result != null){
				return result.get("count_sum", 0);
			}
			return count ;
 		}catch(Exception e){
			throw new Exception("FloorAndroidMgrZr.countAndroidPrintServerNameBy(row):"+e);
		}
	}
	public void deleteAndroidPrintServer(long printer_server_id) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("android_printer_server");
			dbUtilAutoTran.delete(" where printer_server_id ="+ printer_server_id, tableName);
 		}catch(Exception e){
			throw new Exception("FloorAndroidMgrZr.deleteAndroidPrintServer(printer_server_id):"+e);
		}
	}
	public DBRow getPrintServerByAdid(long adid) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("android_printer_server");
			String sql = "select * from "  +  tableName + " where adid = ? " ;
			DBRow para = new DBRow();
			para.add("adid", adid);
			DBRow[] result =  dbUtilAutoTran.selectPreMutliple(sql, para);
			if(result != null && result.length == 1 ){
				return  result[0];
			}
			return null ;
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getPrintServerByAdid(adid):"+e);
		}
	}
	
	public void updatePrintServer(long print_server_id , DBRow updateRow )throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("android_printer_server");
			dbUtilAutoTran.update(" where printer_server_id ="+print_server_id, tableName, updateRow);
 		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.updatePrintServer(who,updateRow):"+e);
		}
	}
	public DBRow getPrintServerByName(String printServerName) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("android_printer_server");
			String sql = "select * from " + tableName + " where printer_server_name=?";
			DBRow para = new DBRow();
			para.add("printer_server_name", printServerName);
			return dbUtilAutoTran.selectPreSingle(sql, para );
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getPrintServerByName(printServerName):"+e);
 		}
	}
	public DBRow getPrintServerById(long printServerId) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("android_printer_server");
			String sql = "select * from " + tableName + " where printer_server_id=?";
			DBRow para = new DBRow();
			para.add("printer_server_id", printServerId);
			return dbUtilAutoTran.selectPreSingle(sql, para );
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getPrintServerByName(printServerName):"+e);
 		}
	}
	public DBRow [] queryPrintTask(long print_server_id , int state) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("print_task");
			String sql = "select *  from " + tableName + " where print_server_id= ? and state = ? ";
			DBRow param = new DBRow();
			param.add("print_server_id", print_server_id);
			param.add("state",state);
			return dbUtilAutoTran.selectPreMutliple(sql, param);
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.queryPrintTask(print_server_id,state):"+e);
 		}
	}
	
	public DBRow[] queryAllPrintServerPrinterAndZoneBy(long ps_id ,  int print_type) throws Exception{
		try{
			String sql = "select aps.* , p.physical_area as area_id , sla.area_name as zone from android_printer_server as aps "  
						+" LEFT JOIN printer as p on aps.printer_server_id = p.servers "
						+" LEFT JOIN storage_location_area as sla on sla.area_id =  p.physical_area "
						+" where aps.ps_id = "+ps_id;
			if(print_type != -1){//两种类型的都查询出来就是-1 ，那么就不用添加查询条件
				sql += " and p.type ="+print_type ;
			}
			sql += " GROUP BY printer_server_id ";
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.queryAllPrintServerPrinterAndZoneBy(ps_id,print_type):"+e);
		}
	}
	public DBRow[] getAllPrinterBy(long ps_id , PageCtrl pc) throws Exception{
		try{
			
			String sql = "  SELECT printer.`name` , sla.area_name , printer.type , ps.printer_server_name  , ps.last_refresh_time "
					    + " FROM printer LEFT JOIN storage_location_area AS sla ON printer.physical_area = sla.area_id LEFT JOIN android_printer_server as ps on ps.printer_server_id = printer.servers "
					   	+ " WHERE "
					   	+ " printer.p_type = 1 and printer.ps_id = " + ps_id ;
			if(pc != null){
				return dbUtilAutoTran.selectMutliple(sql, pc);
			}else{
				return dbUtilAutoTran.selectMutliple(sql);
			}
		}catch(Exception e){
			throw new Exception("FloorAndroidMgrZr.getAllPrinterBy(ps_id,pc):"+e);
		}
	 
	}
}
