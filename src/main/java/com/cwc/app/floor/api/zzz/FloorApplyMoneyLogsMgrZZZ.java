package com.cwc.app.floor.api.zzz;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorApplyMoneyLogsMgrZZZ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	/**
	 * 资金申请日志
	 * @param row
	 * @throws Exception
	 */
	public void addApplyMoneyLogs(DBRow row)
	throws Exception
	{
		try
		{
				long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("apply_money_logs"));
				row.add("aid",id);
				dbUtilAutoTran.insert(ConfigBean.getStringValue("apply_money_logs"), row);
		}
		catch(Exception e)
		{
			throw new Exception("FloorApplyMoneyLogsMgrZZZ.addApplyMoneyLogs(row):"+e);
		}
	}
	/**
	 * 根据资金申请ID获取该条申请的相关日志
	 * @param id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApplyMoneyLogsByApplyId(long id,PageCtrl pc)
	throws Exception
	{
		try
		{
			String sql="select * from "+ConfigBean.getStringValue("apply_money_logs")+" where applyid="+id;
			if(pc!=null)
				return dbUtilAutoTran.selectMutliple(sql,pc);
			else
				return dbUtilAutoTran.selectMutliple(sql);
		}
		catch(Exception e)
		{
			throw new Exception("FloorApplyMoneyLogsMgrZZZ.getApplyMoneyLogsByApplyId(id,pc)"+e);
		}
	}
}
