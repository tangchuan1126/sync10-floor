package com.cwc.app.floor.api.zwb;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorTransportMgrZwb {

	private DBUtilAutoTran dbUtilAutoTran;

	
	
	//根据转运单id查 关联的采购单id
	public DBRow getTransport(long transportId) throws Exception {
		try {
			String sql="select * from transport where transport_id="+transportId;
			return this.dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorTransportMgrZwb getTransport error:"+e);
		}
	}
	
	/**
	 * 根据 商品名字检索
	 * @param transportId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportProductByName(long transport_id,String name)throws Exception{
		try{
			String sql="select * from transport_detail where transport_id="+transport_id+" and transport_p_name  like '%"+name+"%' ";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZwb getTransportProductByName error:"+ e);
		}
	}
	
	//根据转运单号 查出该转运单下的商品
	public DBRow[] getTransportProduct(long transportId) throws Exception {
		try {
			String sql="select * from transport_detail where transport_id="+transportId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorTransportMgrZwb getTransport error:"+e);
		}
	}
	
	//添加转运单跟进
	public long addTransferLog(DBRow row)throws Exception{
        try {
			return this.dbUtilAutoTran.insertReturnId("apply_transfer_logs",row);
		} catch (Exception e) {
			throw new Exception("FloorTransportMgrZwb getTransportProduct error:"+e);
		}
	}
	
	//根据转账id查询 跟进日志信息
	public DBRow[] getTransferlogs(long transferId)throws Exception{
		 try {
			   String sql="select * from apply_transfer_logs where transfer_id="+transferId;
				return this.dbUtilAutoTran.selectMutliple(sql);
			} catch (Exception e) {
				throw new Exception("FloorTransportMgrZwb getTransferlogs error:"+e);
			}
	}
	
	//查询根据出库仓库 到货仓库转运单
	public DBRow[] findTransport(long send_psid,long receive_psid)throws Exception{
		try{
			String sql="select * from transport where transport_status=5 and out_id is null";
			if(send_psid!=0){
				sql+=" and send_psid="+send_psid;
			}
			if(receive_psid!=0){
				sql+=" and receive_psid="+receive_psid;
			}
		    return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZwb findTransport"+e);
		}
	}
	//查询转运单总数量 根据出库仓库和到货仓库
	public DBRow findTransportCount(long send_psid,long receive_psid)throws Exception{
		try{
			String sql="select count(*) as count from transport where transport_status=5 and out_id is null";
			if(send_psid!=0){
				sql+=" and send_psid="+send_psid;
			}
			if(receive_psid!=0){
				sql+=" and receive_psid="+receive_psid;
			}
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZwb findTransportCount"+e);
		}
	}
	
	//更新转运单
	 public void updateTransportByOutId(String transportIds,DBRow row)throws Exception{
		 try{
			 this.dbUtilAutoTran.update("where transport_id in("+transportIds+")", "transport", row);
		 }catch(Exception e){
			 throw new Exception("FloorWaybillMgrZwb updateTransportByOutId"+e);
		 }
	 }
	 
	 //根据拣货单id 查询转运单
	 public DBRow[] selectTransportByOutId(long out_id)throws Exception{
		 try{
			 String sql="select * from transport where out_id="+out_id;
			 return this.dbUtilAutoTran.selectMutliple(sql);
		 }catch(Exception e){
			 throw new Exception("FloorWaybillMgrZwb selectTransportByOutId"+e);
		 }
	 }
	 
	 //根据出库单id 查询转运单
	 public DBRow[] findTransportPcByOutId(long out_id)throws Exception{
		 try{
			 String sql="select transport_pc_id from transport tr join transport_detail td"+
				        " on tr.transport_id=td.transport_id where out_id="+out_id;
			 return this.dbUtilAutoTran.selectMutliple(sql);
		 }catch(Exception e){
			 throw new Exception("FloorWaybillMgrZwb findTransportByOutId"+e);
		 }
	 }
	//根据 货物id查询 物品详细
	 public DBRow[] findProductByProductId(String products)throws Exception{
		 try{
			 String sql="select * from product where pc_id in("+products+")";
			 return this.dbUtilAutoTran.selectMutliple(sql);
		 }catch(Exception e){
			 throw new Exception("FloorWaybillMgrZwb findProductByProductId"+e);
		 }
	 }
	 //根据转运单id查询
	 public DBRow[] findTransportCreateOut(String transportIds,int type)throws Exception{
		 try{
			 String sql="select *,count(*) count,sum(pick_up_quantity) sum_quantity from out_list_detail " +
			 		"GROUP BY out_list_area_id,out_list_slc_id,from_container_type,from_container_type_id,from_con_id," +
			 		"pick_container_type,pick_container_type_id,pick_con_id,system_bill_type,title_id " +
			 		"HAVING system_bill_id in ("+transportIds+") and system_bill_type="+type;
			 return this.dbUtilAutoTran.selectMutliple(sql);
		 }catch(Exception e){
			 throw new Exception("FloorWaybillMgrZwb findTransportCreateOut"+e);
		 }
	 }
	 
	 //插入数据 到捡货详细
	 public long addOutOrderDetail(DBRow row)throws Exception{
		 try{
			 return this.dbUtilAutoTran.insertReturnId("out_storebill_order_detail",row);
		 }catch(Exception e){
			 throw new Exception("FloorWaybillMgrZwb addOutOrderDetail"+e);
		 }
	 }
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
