package com.cwc.app.floor.api.zyj.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.floor.api.zr.FloorBoxTypeZr;
import com.cwc.app.floor.api.zyj.model.Customer;
import com.cwc.app.floor.api.zyj.service.CustomerService;
import com.cwc.app.floor.api.zyj.service.ProductCustomerSerivce;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class CustomerServiceImpl implements CustomerService {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 获取所有的品牌商
	 * 
	 * @return
	 */
	public List<Customer> getAll() {
		List<Customer> list = new ArrayList<Customer>();
		try {
			DBRow[] rows = this.dbUtilAutoTran.selectMutliple("SELECT a.customer_key customer_id,a.customer_id customer_name  from customer_id a");
			if(rows != null && rows.length > 0){
				for(DBRow row : rows){
					list.add(this.convertToCustomer(row));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 获取指定ID的品牌商
	 * 
	 * @return
	 */
	public Customer getCustomer(int id) {
		Customer customer = null;
		try {
			DBRow param_row = new DBRow();
			param_row.add("customer_key", id);
			DBRow[] rows = this.dbUtilAutoTran
					.selectPreMutliple(
							"select a.customer_key customer_id,a.customer_id customer_name from customer_id a where a.customer_key=?",
							param_row);

			if (rows != null && rows.length > 0) {
				customer = new Customer();
				for (int i = 0; i < rows.length; i++) {
					customer = this.convertToCustomer(rows[i]);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return customer;
	}

	/**
	 * 获取与指定产品和CLP Type 的所有Title
	 * @param product
	 * @param clpTypeId
	 * @return
	 */
	public List<Customer> getCustomersByProdAndCLP(int productId,int clpTypeId){
		List<Customer> list = new ArrayList<Customer>();
		FloorBoxTypeZr  t;
		try {
			DBRow param_row = new DBRow();
			//从lp_title_ship_to 表中查询  与 指定的product,clpType相关的且Customer,Title 为 所有的记录条数
			StringBuilder customer_count_sql = new StringBuilder("select count(1) totalCount from  lp_title_ship_to a    where 	a.lp_type_id =" + clpTypeId + " and a.lp_pc_id =" + productId + " and a.customer_id=0 and a.title_id=0");  
			
			
			DBRow row = this.dbUtilAutoTran.selectSingle(customer_count_sql.toString());
			if(row != null && row.get("totalCount", 0) > 0){
				list = this.getCustomers(productId);
			}else{
				StringBuilder sql = new StringBuilder();
				sql.append("SELECT DISTINCT	a.customer_id,c.customer_id customer_name FROM lp_title_ship_to a INNER JOIN customer_id c ON a.customer_id = c.customer_key");
				sql.append(" WHERE a.title_id>0 and	a.lp_type_id = " + clpTypeId + " AND a.lp_pc_id =" + productId);
				sql.append(" UNION");
				
				sql.append(" SELECT DISTINCT	a.customer_id,c.customer_id customer_name FROM lp_title_ship_to a INNER JOIN customer_id c ON a.customer_id = c.customer_key");
				sql.append(" WHERE a.title_id=0 and	a.lp_type_id = " + clpTypeId + " AND a.lp_pc_id =" + productId);
				sql.append(" UNION");			
				
				sql.append(" SELECT distinct c.customer_key customer_id,c.customer_id customer_name from title_product  t ");
				sql.append(" INNER join lp_title_ship_to a  on t.tp_pc_id=a.lp_pc_id and t.tp_title_id=a.title_id ");
				sql.append(" INNER join customer_id c on t.tp_customer_id=c.customer_key ");
				sql.append(" WHERE a.lp_pc_id=" + productId  +" and a.lp_type_id =" + clpTypeId + " and  a.customer_id=0 and a.title_id>0");
				
						
				DBRow[] rows = this.dbUtilAutoTran.selectMutliple(sql.toString());
				if (rows != null && rows.length > 0) {
					for (int i = 0; i < rows.length; i++) {
						list.add(this.convertToCustomer(rows[i]));
					}
				}				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	/**
	 * 获取与指定产品和CLP Type 的所有Title
	 * @param product
	 * @param clpTypeId
	 * @param titleId
	 * @return
	 */
  public List<Customer> getCustomersByProdAndCLP(int productId, int clpTypeId, int titleId)
  {
	    List list = new ArrayList();
	    try
	    {
	      DBRow param_row = new DBRow();

	      StringBuilder customer_count_sql = new StringBuilder(new StringBuilder().append("select count(1) totalCount from  lp_title_ship_to a    where \ta.lp_type_id =").append(clpTypeId).append(" and a.lp_pc_id =").append(productId).append(" and a.customer_id=0 and a.title_id=0").toString());

	      DBRow row = this.dbUtilAutoTran.selectSingle(customer_count_sql.toString());
	      if ((row != null) && (row.get("totalCount", 0) > 0)) {
	        list = getCustomers(productId,titleId);
	      } else {
	        StringBuilder sql = new StringBuilder();
	        sql.append("SELECT DISTINCT\ta.customer_id,c.customer_id customer_name FROM lp_title_ship_to a INNER JOIN customer_id c ON a.customer_id = c.customer_key");
	        sql.append(new StringBuilder().append(" WHERE a.title_id=" + titleId + " and\ta.lp_type_id = ").append(clpTypeId).append(" AND a.lp_pc_id =").append(productId).toString());
	        sql.append(" UNION");

	        sql.append(" SELECT DISTINCT\ta.customer_id,c.customer_id customer_name FROM lp_title_ship_to a ");
	        sql.append(" INNER JOIN title_product t on a.lp_pc_id=t.tp_pc_id and a.customer_id=t.tp_customer_id");
	        sql.append(" INNER JOIN customer_id c ON a.customer_id = c.customer_key");
	        sql.append(new StringBuilder().append(" WHERE a.title_id=0 and\ta.lp_type_id = ").append(clpTypeId).append(" AND a.lp_pc_id =").append(productId).append(" and t.tp_title_id=").append(titleId).toString());
	        sql.append(" UNION");

	        sql.append(" SELECT distinct c.customer_key customer_id,c.customer_id customer_name from title_product  t ");
	        sql.append(" INNER join lp_title_ship_to a  on t.tp_pc_id=a.lp_pc_id and t.tp_title_id=a.title_id ");
	        sql.append(" INNER join customer_id c on t.tp_customer_id=c.customer_key ");
	        sql.append(new StringBuilder().append(" WHERE a.lp_pc_id=").append(productId).append(" and a.lp_type_id =").append(clpTypeId).append(" and  a.customer_id=0 and a.title_id=").append(titleId).toString());

	        DBRow[] rows = this.dbUtilAutoTran.selectMutliple(sql.toString());
	        if ((rows != null) && (rows.length > 0))
	          for (int i = 0; i < rows.length; ++i)
	            list.add(convertToCustomer(rows[i]));
	      }
	    }
	    catch (SQLException e)
	    {
	      e.printStackTrace();
	    }

	    return list;
  }
	
	/**
	 * 获取与特定产品相关的所有Customer
	 * @param productId 产品ID
	 * @return
	 */
	public List<Customer>  getCustomers(int productId){
		List<Customer> list = new ArrayList<Customer>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple("SELECT distinct a.tp_customer_id customer_id,c.customer_id customer_name FROM title_product a LEFT JOIN customer_id c ON a.tp_customer_id= c.customer_key WHERE a.tp_pc_id =?",param_row);
			
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToCustomer(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;			
		
		
	}
	
		/**
	 * 查询与指定 product,title 相关的Customer
	 * @param productId  
	 * @param titleId
	 * @return
	 */
	public List<Customer>  getCustomers(int productId,int titleId){
		List<Customer> list = new ArrayList<Customer>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			param_row.add("tp_title_id", titleId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple("SELECT distinct a.tp_customer_id customer_id,c.customer_id customer_name FROM title_product a INNER JOIN customer_id c ON a.tp_customer_id= c.customer_key WHERE a.tp_pc_id =? and a.tp_title_id=? order by a.tp_id",param_row);
			
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToCustomer(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;		
		
	}
	
	/**
	 * 将DBRow 对象转变为对应的 CustomerId对象
	 * 
	 * @param row
	 * @return
	 */
	private Customer convertToCustomer(DBRow row) {
		Assert.notNull(row);
		Customer customer = new Customer();
		customer.setId(row.get("customer_id", 1));
		customer.setName(row.get("customer_name", ""));
		return customer;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

}
