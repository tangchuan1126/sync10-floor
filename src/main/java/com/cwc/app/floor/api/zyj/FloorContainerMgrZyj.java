package com.cwc.app.floor.api.zyj;

import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.cwc.app.util.StrUtil;

public class FloorContainerMgrZyj {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 通过容器编号得到容器信息
	 * @param number
	 * @return
	 * @throws Exception
	 * 添加titleName(张睿)
	 */
	public DBRow findContainerByNumber(String number) throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("container") + " as c " 
						+"left join title on title.title_id = c.title_id where container = '" + number + "'";
			
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.findContainerByNumber(String number):"+e);
		}
	}
	
	/**
	 * 通过盒子类型ID获得盒子类型信息及商品名称
	 * @param type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findBoxTypeRelateInfoByTypeId(long type_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT bt.* , p.p_name FROM box_type bt LEFT JOIN product p ON bt.box_pc_id = p.pc_id" );
			sql.append(" WHERE 1=1");
			if(type_id > 0)
			{
				sql.append(" AND bt.box_type_id = " + type_id);
			}
			sql.append(" ORDER BY bt.box_type_id desc");
			return dbUtilAutoTran.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.findBoxTypeRelateInfoByTypeId(long type_id):"+e);
		}
	}
	
	/**
	 * 通过clp类型ID，获取clp类型信息及商品名称等
	 * @param type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findClpTypeRelateInfoByTypeId(long type_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer("SELECT ct.* , t.title_name, p.p_name FROM clp_type as ct ");
//			sql.append(" LEFT JOIN product_storage_catalog  as psc ON  ct.sku_lp_to_ps_id  = psc.id ");
			sql.append(" LEFT JOIN product as p ON ct.sku_lp_pc_id = p.pc_id");
			sql.append(" LEFT JOIN title t ON t.title_id = ct.sku_lp_title_id");
			sql.append(" WHERE 1=1");
			if(type_id > 0)
			{
				sql.append(" AND ct.sku_lp_type_id = " + type_id);
			}
			sql.append(" ORDER BY ct.sku_lp_type_id DESC ");
			return dbUtilAutoTran.selectSingle(sql.toString());
		}
		catch (Exception e) {
			throw new Exception("FloorContainerMgrZyj.findClpTypeRelateInfoByTypeId(long type_id):"+e);
		}
	}
	
	/**
	 * 通过内箱类型ID获得内箱类型信息及商品名称
	 * @param type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow  findInnerBoxTypeRelateInfoByTypeId(long type_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT ibt.* , p.p_name FROM inner_box_type ibt LEFT JOIN product p ON ibt.ibt_pc_id = p.pc_id" );
			sql.append(" WHERE 1=1");
			if(type_id > 0)
			{
				sql.append(" AND ibt.ibt_id = " + type_id);
			}
			sql.append(" ORDER BY ibt.ibt_id desc");
			return dbUtilAutoTran.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.findInnerBoxTypeRelateInfoByTypeId(long type_id):"+e);
		}
	}
	
	/**
	 * 通过内箱类型ID获得内箱类型信息及商品名称
	 * @param type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findTlpTypeRelateInfoByTypeId(long type_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM container_type " );
			sql.append(" WHERE 1=1");
			if(type_id > 0)
			{
				sql.append(" AND type_id = " + type_id);
			}
			sql.append(" ORDER BY type_id desc");
			return dbUtilAutoTran.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.findTlpTypeRelateInfoByTypeId(long type_id):"+e);
		}
	}
	
	
	/**
	 * 通过容器ID获得其子容器信息及商品总数
	 * @param containerId
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findContainerInnerInfoByContainerId(long containerId, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT cp.cp_quantity productCount, c.container, c.container_type, c.con_id");
			sql.append(" FROM container_loading cl");
			sql.append(" JOIN container_product cp ON cl.con_id = cp.cp_lp_id");
			sql.append(" LEFT JOIN container c ON cl.con_id = c.con_id");
			sql.append(" WHERE 1=1");
			sql.append(" AND cl.parent_con_id = " + containerId);
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.findContainerInnerInfoByContainerId(long containerId, PageCtrl pc):"+e);
		}
	}
	/**
	 * 通过容器ID获得其子容器总数做减法用算散件
	 * @param containerId
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow findContainerSumByContainerId(long containerId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT SUM(cp.cp_quantity) productCount, c.container, c.container_type, c.con_id");
			sql.append(" FROM container_loading cl");
			sql.append(" JOIN container_product cp ON cl.con_id = cp.cp_lp_id");
			sql.append(" LEFT JOIN container c ON cl.con_id = c.con_id");
			sql.append(" WHERE 1=1");
			sql.append(" AND cl.parent_con_id = " + containerId);
			return this.dbUtilAutoTran.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.findContainerInnerInfoByContainerId(long containerId, PageCtrl pc):"+e);
		}
	}
	
	
	public long addInnerBoxType(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId("inner_box_type", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.addInnerBoxType()():"+e);
		}
	}
	
	public long addBoxType(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId("box_type", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.addBoxType()():"+e);
		}
	}
	
	public long addBoxPs(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId("box_ps", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.addBoxPs()():"+e);
		}
	}
	
	public long addClpType(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId("clp_type", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.addClpType()():"+e);
		}
	}
	
	public long addContainerType(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId("container_type", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.addContainerType()():"+e);
		}
	}
	
	public long addContainer(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId("container", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.addContainerType()():"+e);
		}
	}
	
	public void updateContainer(DBRow row, long id) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where con_id = " + id, "container", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.updateContainer()():"+e);
		}
	}
	
	public DBRow[] findAllProductStoageCatalog() throws Exception
	{
		try
		{
			String sql = "select id from product_storage_catalog";
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.findAllProductStoageCatalog()():"+e);
		}
	}
	
	public int getMaxIndexBy(long pc_id , long ps_id) throws Exception{
		try{
			int index =  0 ;
			String tableName = ConfigBean.getStringValue("box_ps");
			String sql = "select max(box_sort) as max_sort from "+tableName+" where box_pc_id =? and to_ps_id =? ";
			DBRow para = new DBRow();
			para.add("box_pc_id", pc_id);
			para.add("to_ps_id", ps_id);
			DBRow row = dbUtilAutoTran.selectPreSingle(sql, para);
			if(row != null){
				index = row.get("max_sort", 0);
			}
			return index ;
		}catch (Exception e) {
			throw new Exception("FloorContainerMgrZyj.getMaxIndexBy(pc_id,ps_id):"+e);
 		}
	}
	
	public long addContainerLoading(DBRow row)throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId("container_loading", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.addContainerLoading(DBRow row):"+e);
		}
	}
	
	public long addContainerLoadingChildList(DBRow row) throws Exception
	{
		
		try
		{
			return dbUtilAutoTran.insertReturnId("container_loading_child_list", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.addContainerLoadingChildList(DBRow row):"+e);
		}
	}
	
	public void callContainerLoadingChildList(long con_id) throws Exception
	{
		try 
		{
			String sql = "call container_loading_child_list("+con_id+")";
			dbUtilAutoTran.CallStringSingleResultProcedure(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj callContainerLoadingChildList error:"+e);
		}
	}
	
	
	public DBRow[] findAllProducts() throws Exception
	{
		try
		{
			return dbUtilAutoTran.selectMutliple("select pc_id from product");
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.findAllProducts:"+e);
		}
	}
	
	public void deleteAndContainerAndExample()throws Exception
	{
		try
		{
			dbUtilAutoTran.CallStringSingleResultProcedure(" call delete_container_and_example");
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.deleteAndContainerAndExample:"+e);
		}
	}

	public DBRow[] findContainersByType(int type) throws Exception
	{
		try
		{
			return dbUtilAutoTran.selectMutliple("select * from container_type where container_type = " + type);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.deleteAndContainerAndExample:"+e);
		}
	}
	
	
	public DBRow[] findAllContainer(PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			/*
			sb.append(" SELECT c.*, ct.type_name FROM container c JOIN inner_box_type ibt ON c.type_id = ibt.ibt_id AND c.container_type = " + ContainerTypeKey.ILP);
			sb.append(" LEFT JOIN container_type ct ON ct.type_id = ibt.ibt_lp_type");
			sb.append(" UNION");
			sb.append(" SELECT c.*, ct.type_name FROM container c JOIN box_type bt ON c.type_id = bt.box_type_id AND c.container_type = " + ContainerTypeKey.BLP);
			sb.append(" LEFT JOIN container_type ct ON ct.type_id = bt.container_type_id");
			sb.append(" UNION");
			*///去掉ILP和BLP
			sb.append(" SELECT c.*, ct.type_name FROM container c JOIN clp_type ctt ON c.type_id = ctt.sku_lp_type_id AND c.container_type = " + ContainerTypeKey.CLP);
			sb.append(" LEFT JOIN container_type ct ON ct.type_id = ctt.lp_type_id");
			sb.append(" UNION");
			sb.append(" SELECT c.*, c.container type_name FROM container c JOIN container_type ct ON c.type_id = ct.type_id AND c.container_type = " + ContainerTypeKey.TLP);
			
			if(null != pc)
			{
				return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sb.toString());
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorContainerMgrZyj.findAllContainer:"+e);
		}
	}
	
	public DBRow[] findContainerByTypeContainerTypeName(String key,long type ,int container_type, PageCtrl pc) throws Exception {
		
		try {
			
			StringBuffer sb = new StringBuffer();
			
			if( ContainerTypeKey.CLP == container_type ) {
				
				sb.append(" SELECT c.*, ct.type_name,ctt.pc_id FROM container c left JOIN license_plate_type ctt ON c.type_id = ctt.lpt_id ");
				sb.append(" LEFT JOIN container_type ct ON ct.type_id = ctt.basic_type_id WHERE 1=1");
				if(container_type > 0)
				{
					sb.append(" AND c.container_type = " + container_type);
				}
				if(!StrUtil.isBlank(key))
				{
					sb.append(" AND c.container LIKE '%" + key + "%'");
				}
				if(type > 0)
				{
					sb.append(" AND ct.type_id = " + type);
				}
				
				sb.append(" ORDER BY c.con_id DESC ");
				
			} else if(ContainerTypeKey.TLP == container_type) {
				
				sb.append(" SELECT c.*, ct.type_name FROM container c left JOIN container_type ct ON c.type_id = ct.type_id ");
				sb.append(" WHERE 1=1");
				
				if(container_type > 0) {
					sb.append(" AND c.container_type = " + container_type);
				}
				
				if(!StrUtil.isBlank(key)) {
					sb.append(" AND c.container LIKE '%" + key + "%'");
				}
				
				if(type > 0) {
					sb.append(" AND ct.type_id = " + type);
				}
				
				sb.append(" ORDER BY c.con_id DESC ");
				
			} else {
				
				sb.append(" select * from ( ");
				sb.append(" SELECT c.*, ct.type_name FROM container c left JOIN license_plate_type ctt ON c.type_id = ctt.lpt_id ");
				//sb.append(" SELECT c.*, ct.type_name FROM container c left JOIN license_plate_type ctt ON c.type_id = ctt.lpt_id AND c.container_type = " + ContainerTypeKey.CLP);
				sb.append(" LEFT JOIN container_type ct ON ct.type_id = ctt.basic_type_id ");
				//sb.append(" WHERE 1=1");
				sb.append(" where c.container_type = ").append(ContainerTypeKey.CLP);
				
				if(type > 0) {
					sb.append(" AND ct.type_id = " + type);
				}
				
				if(!StrUtil.isBlank(key)) {
					sb.append(" AND c.container LIKE '%" + key + "%'");
				}
				
				sb.append(" UNION");
				
				sb.append(" SELECT c.*, ct.type_name FROM container c left JOIN container_type ct ON c.type_id = ct.type_id ");
				//sb.append(" SELECT c.*, ct.type_name FROM container c left JOIN container_type ct ON c.type_id = ct.type_id AND c.container_type = " + ContainerTypeKey.TLP);
				//sb.append(" WHERE 1=1 ");
				sb.append(" where c.container_type = ").append(ContainerTypeKey.TLP);
				if(type > 0) {
					sb.append(" AND ct.type_id = " + type);
				}
				if(!StrUtil.isBlank(key)) {
					sb.append(" AND c.container LIKE '%" + key + "%'");
				}
				
				sb.append(" ) as container ORDER BY con_id DESC ");
			}
			
			if(null != pc) {
				return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
				
			} else {
				
				return dbUtilAutoTran.selectMutliple(sb.toString());
			}
		} catch (Exception e) {
			throw new Exception("FloorContainerMgrZyj.findContainerByTypeContainerTypeName:"+e);
		}
	}
	
	public DBRow findILPContainerWithPName(long container_id) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT p.p_name, ibt.stack_length_qty, ibt.stack_width_qty, ibt.stack_height_qty, ct.type_name");
			sb.append(" FROM container c JOIN license_plate_type ibt ON c.type_id = ibt.lpt_id");
			sb.append(" LEFT JOIN container_type ct ON ct.type_id = ibt.basic_type_id ");
			sb.append(" LEFT JOIN product p ON ibt.pc_id = p.pc_id");
//			sb.append(" WHERE c.container_type = " + ContainerTypeKey.ILP);//remove ILP和BLP
			sb.append(" AND c.con_id = " + container_id);
			return dbUtilAutoTran.selectSingle(sb.toString());
		}
		catch (Exception e)
		{
			throw new Exception("FloorContainerMgrZyj.findILPContainerWithPName:"+e);
		}
	}
	
	public DBRow[] findProductsByIdLtSome(long pc_id) throws Exception
	{
		try
		{
			return dbUtilAutoTran.selectMutliple("select pc_id from product where pc_id < " + pc_id);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.findProductsByIdLtSome:"+e);
		}
	}
	
	public DBRow[] findContainerLoadingByConIdAndParentId(long con_id, long parent_con_id) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select * from container_loading where 1=1");
			if(con_id > 0)
			{
				sb.append(" and con_id = " + con_id);
			}
			if(parent_con_id > 0)
			{
				sb.append(" and parent_con_id = " + parent_con_id);
			}
			return dbUtilAutoTran.selectMutliple(sb.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.findProductsByIdLtSome:"+e);
		}
	}
	
	/**
	 * 获得SKU到卖场的可使用CLP
	 * @param pc_id
	 * @param title_id
	 * @param to_ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCLPTypeForSkuToShip(long pc_id, long title_id, long ps_id) throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM ").append(ConfigBean.getStringValue("clp_type")).append(" WHERE 1=1");
			if(pc_id > 0)
			{
				sql.append(" AND sku_lp_pc_id = ").append(pc_id);
			}
			if(title_id > 0)
			{
				sql.append(" AND sku_lp_title_id = ").append(title_id);
			}
			if(ps_id > 0)
			{
				sql.append(" AND sku_lp_to_ps_id = ").append(ps_id);
			}
			sql.append(" ORDER BY sku_lp_sort ASC");
			
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.getCLPTypeForSkuToShip error:"+e);
		}
	}
	
	/**
	 * 通过容器ID获取容器上商品总数
	 * @param container_id
	 * @return
	 * @throws Exception
	 */
	public int findContainerProductCount(long container_id) throws Exception{
		try{
			String sql = "select count(cp_quantity) cpSumQty from " + ConfigBean.getStringValue("container_product")  + " where cp_lp_id = "+container_id;
		
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			if(null == result)
			{
				return 0;
			}
			return result.get("cpSumQty", 0);
			
		}catch (Exception e) {
			 throw new Exception("FloorContainerMgrZyj.findContainerProductCount(container_id):"+e);
		}
	}
	
	/**
	 * 根据商品查询ilp类型
	 * @param pcid
	 * @param begin
	 * @param length
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findIlpTypesByPcid(long pcid, int begin, int length)throws Exception{
		try
		{
			String sql="select *,CONCAT(CONVERT(ibt_total_length,CHAR),'*',CONVERT(ibt_total_width,CHAR),'*',CONVERT(ibt_total_height,CHAR)) box_type_name";
			sql += "  from inner_box_type where ibt_pc_id="+pcid;
			if(begin >= 0 && length > 0)
			{
				sql += " LIMIT "+begin+","+length;
			}
			else
			{
				if(length > 0)
				{
					sql += " LIMIT "+length;
				}
			}
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorContainerMgrZyj.findIlpTypesByPcid()"+e);
		}
	}
	
	/**
	 * 根据商品查询Blp类型
	 * @param pcid
	 * @param begin
	 * @param length
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findBoxTypesByPcId(long pcid, int begin, int length) throws Exception  {
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select bt.* , ct.type_name,");
			sql.append(" CONCAT(CONVERT(box_total_length,CHAR),'*',CONVERT(box_total_width,CHAR),'*',CONVERT(box_total_height,CHAR)) box_type_name,");
			sql.append(" case when box_inner_type = 0 then '商品' else 'ILP' end inner_type");
			sql.append(" FROM box_type bt left join container_type as ct on ct.type_id =bt.container_type_id ");
			sql.append(" where bt.box_pc_id = ").append(pcid);
			sql.append(" order by bt.box_type_sort asc ");
			if(begin >= 0 && length > 0)
			{
				sql.append(" LIMIT ").append(begin).append(",").append(length);
			}
			else
			{
				if(length > 0)
				{
					sql.append(" LIMIT ").append(length);
				}
			}
 			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorContainerMgrZyj.findBoxTypesByPcId():"+e);
		}
	}
	
	
	//查询ilp根据商品
	public DBRow[] selectIlp(long productId, int length)throws Exception{
		try{
			String sql="select ibt.*,ct.type_name from license_plate_type ibt join container_type ct on ibt.basic_type_id=ct.type_id where ibt.pc_id="+productId;
			sql += " AND  ct.container_type=4" ;
			sql += " order by ct.type_id, ibt.stack_length_qty, ibt.stack_width_qty, ibt.stack_height_qty";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr selectIlp()"+e);
		}
	}
	
	public DBRow[] getBoxSkuByPcId(long pc_id, long ship_to) throws Exception 
	{
		try
		{
			StringBuffer sql = new StringBuffer("select * from license_plate_type bt");
			//	sql.append(" left join lp_title_ship_to bs on bt.lpt_id = bs.lp_type_id");
			sql.append(" where bt.active=1 and bt.pc_id = ").append(pc_id);
			//l.append(" and bt.container_type =2 ");
		//	sql.append(" and (bs.ship_to_id is null ");
		//	if(ship_to > 0)
		//	{
		//		sql.append(" or bs.ship_to_id = ").append(ship_to);
		//	}
		//	sql.append(")");
			return dbUtilAutoTran.selectMutliple(sql.toString());

		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.getBoxSkuByPcId(pc_id):"+e);

 		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

}
