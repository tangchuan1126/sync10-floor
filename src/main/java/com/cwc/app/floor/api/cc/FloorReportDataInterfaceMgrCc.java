package com.cwc.app.floor.api.cc;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.DBUtilAutoTranSQLServer;

public class FloorReportDataInterfaceMgrCc {
	private DBUtilAutoTran dbUtilAutoTran;
	private DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer;
	
	/**
	 * 查询CustomerID对应的所有Item.
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getItemsByCustomerId(String customerId) throws Exception {
		String sql = "SELECT CompanyID, ItemID, Description, PoundPerPackage"
				+ " FROM CustomerItems"
				+ " WHERE CustomerID = '"+customerId+"'";
		return dbUtilAutoTranSQLServer.selectMutliple(sql);
	}
	/**
	 * 查询一段时间内customer对应的所有receiving
	 * @param customerId
	 * @param from
	 * @param to
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReceiveByCustomerIdAndDate(String customerId, String from, String to) throws Exception {
		String sql = "SELECT rl.receipt_no, rl.item_id, rl.goods_total, rl.damage_total, ee.equipment_number, rl.unloading_finish_time time"
				+ " from receipts r "
				+ " join receipt_lines rl on rl.receipt_id=r.receipt_id"
				+ " LEFT JOIN door_or_location_occupancy_details dd on dd.dlo_detail_id=r.detail_id"
				+ " LEFT JOIN entry_equipment ee on ee.equipment_id=dd.equipment_id"
				+ " where r.customer_id='"+customerId+"'"
				+ " and rl.unloading_finish_time>='"+from+"' "
				+ " and rl.unloading_finish_time<'"+to+"'";
		return dbUtilAutoTran.selectMutliple(sql);
	}
	/**
	 * 根据companyId查询ps_id
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow getPsIdByCompanyId(String companyId) throws Exception{
		String sql = "SELECT cs.ps_id from config_search_warehouse_location cs "
				+ " where cs.company_id='w12'"
				+ " GROUP BY ps_id";
		return dbUtilAutoTran.selectSingle(sql);
	}
	/**
	 * 根据psId查询companyId
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCompanyIdByPsId(long psId) throws Exception{
		String sql = "SELECT * from config_search_warehouse_location cs  where cs.ps_id="+psId;
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	public DBRow[] receiveScheduleInbound(String[] companyIds, String startTime, String endTime) throws Exception{
		String company = "";
		if(companyIds!=null && companyIds.length>0){
			company = " and r.CompanyID in (";
			for (String s : companyIds) {
				company += "'"+s+"',";
			}
			company = company.substring(0, company.length()-1);
			company += ") ";
		}
		
		String sql = "SELECT r.CompanyID, r.ReceiptNo, r.ScheduledDate, r.AppointmentDate, r.InYardDate, r.DevannedDate, r.LastFreeDate, "
				+ "	r.CarrierID, r.CustomerID, r.Status, r.SupplierID, r.ReferenceNo, r.ContainerNo, "
				+ "	rl.ExpectedQty, rl.Pallets, rl.ItemID, ci.Grade, ci.Width, ci.Length, ci.Height, ci.PoundPerPackage"
				+ " from Receipts r"
				+ " JOIN ReceiptLines rl on rl.CompanyID=r.CompanyID AND rl.ReceiptNo=r.ReceiptNo"
				+ " LEFT JOIN CustomerItems ci on ci.CompanyID=r.CompanyID and ci.CustomerID=r.CustomerID and ci.ItemID=rl.ItemID"
				+ " where r.AppointmentDate >= '"+startTime+"'"
				+ " and r.AppointmentDate < '"+endTime+"'"
				+ company
				+ " and r.Status <> 'closed'";
		
		return dbUtilAutoTranSQLServer.selectMutliple(sql);
	}
	
	public DBRow[] appointmentCountInbound(String... companyId) throws Exception{
		if(companyId.length == 0){
			return null;
		}
		String com = "";
		for (String s : companyId) {
			com += "'"+s+"',";
		}
		if(com.length()>0){
			com = com.substring(0, com.length()-1);
		}
		String sql = "select appointmentdate, COUNT(1) num "
				+ " from Receipts "
				+ " where companyid in ("+com+") and status<>'Closed' and AppointmentDate<>'' "
				+ " GROUP BY AppointmentDate "
				+ " order by AppointmentDate";
		
		return dbUtilAutoTranSQLServer.selectMutliple(sql); 
	}
	public DBRow[] appointmentCountOutbound(String... companyId) throws Exception{
		if(companyId.length == 0){
			return null;
		}
		String com = "";
		for (String s : companyId) {
			com += "'"+s+"',";
		}
		if(com.length()>0){
			com = com.substring(0, com.length()-1);
		}
		String sql = "select appointmentdate, COUNT(1) num  "
				+ " from MasterBOLs "
				+ " where companyid in ("+com+") and status<>'Closed' and AppointmentDate<>''"
				+ " GROUP BY AppointmentDate"
				+ " order by AppointmentDate";
		
		return dbUtilAutoTranSQLServer.selectMutliple(sql); 
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	public void setDbUtilAutoTranSQLServer(
			DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer) {
		this.dbUtilAutoTranSQLServer = dbUtilAutoTranSQLServer;
	}
}
