package com.cwc.app.floor.api.zr;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorWayBillOrderMgrZR {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	public DBRow[] getWalbillByRow(DBRow row , PageCtrl pc) throws Exception{
		try{
			 long psId = Long.parseLong(row.getString("ps_id"));
			 String outId =  row.getString("out_id");
			 StringBuffer sql  = new StringBuffer("select * from waybill_order where   parent_waybill_id <> -1  and waybill_order.ps_id  = " + psId);
			 if(null != outId  && outId.length() > 0 ){
				 sql.append(" and  waybill_order.out_id="+outId);
			 }
			 return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getWalbillByRow():"+e);
		}
	}
	public long addNewStoreBill(DBRow row) throws Exception{
		try{
			String dateExp = "yyyy-MM-dd HH:mm:ss";
			SimpleDateFormat format = new SimpleDateFormat(dateExp);
			String date = format.format(new Date());
			row.add("create_time", date);
			row.add("state", "1");
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("out_storebill_order"), row);
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.addNewStoreBill():"+e);
		}
	}
	public DBRow[] getStoresByPsIdAndState(long psId,String state) throws Exception{
		try{
			String sql = "select * from out_storebill_order where ps_id = "+psId+" and state = '"+state+"'";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getStoresByPsIdAndState():"+e);
		}
	}
	
	public void updateWayBillStore(long id , String ids) throws Exception{
		try{
			DBRow updateRow = new DBRow();
			updateRow.add("out_id", id);
			dbUtilAutoTran.update("where waybill_id in ("+ids+")", ConfigBean.getStringValue("waybill_order"), updateRow);
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.updateWayBillStore():"+e);
		}
	}
	public DBRow[] getAllStoreIsOpenAndPsId(long psid)throws Exception{
		try{
			 String sql = "select * from "+ ConfigBean.getStringValue("out_storebill_order")+" where out_storebill_order.state = '1' and out_storebill_order.ps_id = "+psid;
			 
			 return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getAllStoreIsOpenAndPsId():"+e);
		}
	}
	public DBRow[] getAllItemInWayBillById(long wayBillId) throws Exception{
		try{
			 String sql = " select waybill_order_item.*,product.p_name , waybill_order_item.pc_id  from waybill_order_item LEFT JOIN  product on product.pc_id = waybill_order_item.pc_id   where  waybill_order_id = " + wayBillId;
			  
			 return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getAllItemInWayBillById():"+e);
		}
	}
	public DBRow getWayBillById(long id) throws Exception{
		try{
			 String sql = "select * from " +  ConfigBean.getStringValue("waybill_order") +" where waybill_id = "+id;
			 return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getWallBillById():"+e);
		}
	}
	public long addWayBillByDBRow(DBRow row) throws Exception{
		try{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("waybill_order"), row);
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.addWayBillByDBRow():"+e);
		}
	}

	/**
	 *  1.更新WayItem表中的数据和插入新的数据 By Id 
	 *  2.如果是减掉过后的数量为零了那么就要删除这条记录Item
	 *  3.如果是Item已经删除完了那么就把煮运单和主运单对应Item都删除 //已经
	 */
	public void updateWayItemByWayId(String id  , String number ,long oldWayId ,long newWayId) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("waybill_order_item");
			// 更新原始Item的中的记录
			String sqlSelect = "select * from "+ tableName+" where waybill_order_item_id = " + id + " and waybill_order_id=" +oldWayId ; 
		 
			DBRow row = dbUtilAutoTran.selectSingle(sqlSelect);
			if(row != null ){
			 
				int count =  0;
				String quantity = row.getString("quantity");
				int length = quantity.indexOf(".");
				if(length != -1){
					count = Integer.parseInt(quantity.substring(0, length));
				}else{
					count = Integer.parseInt(quantity);
				}
				int value = Integer.parseInt(number) - count;
				if( value > 0){
					 throw new RuntimeException("拆分数量大于了购买数量");
				}else{
					//这个时候要插入新的值。如果是等于0 的要删除原来的值
					row.remove("waybill_order_item_id");
					row.add("quantity",number);
					row.add("waybill_order_id", newWayId);
				 	dbUtilAutoTran.insertReturnId(tableName, row);
				 
					if(value == 0){
						//这个时候是要删除
						dbUtilAutoTran.delete(" where  waybill_order_item_id = " + id, tableName);
					}else {
						// 这个时候是要更新
						DBRow updateRow = new DBRow();
						updateRow.add("quantity", -value);
						dbUtilAutoTran.update(" where waybill_order_item_id = " + id, tableName, updateRow);
					}
				}
			}
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.updateWayItemByWayId():"+e);
		}
	}
	public DBRow getWayItemsByWayId(long wayId) throws Exception {
		try{
			DBRow result = new DBRow();
			 String sql = "select * from waybill_order_item where  waybill_order_item.waybill_order_id = " + wayId;
			 DBRow[] rows =  dbUtilAutoTran.selectMutliple(sql);
			 result.add("split", rows);
			 String waySelect = "select * from  waybill_order where waybill_id = " + wayId;
			 result.add("waybill", dbUtilAutoTran.selectSingle(waySelect));
			 return result;
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getWayItemsByWayId():"+e);
		}
	}
	public DBRow[] getSplitInfoById(long wayBillId) throws Exception {
		try{
			 StringBuffer sql = new StringBuffer("select	waybill_order_item.*, t.waybill_id,product.p_name,t.status ");
			 sql.append(" from waybill_order_item,(select waybill_order.waybill_id , waybill_order.`status`	 from waybill_order where parent_waybill_id = "+wayBillId+" ) t , product ");
			 sql.append(" where product.pc_id = waybill_order_item.pc_id and t.waybill_id = waybill_order_item.waybill_order_id order by t.waybill_id asc ");
			 // System.out.println("sql : " + sql.toString());
			 return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getSplitInfoById():"+e);
		}
	}
	/**
	 * @param row 6 2 1/3 
	 * 2 (2+ ?)
	 * @param parentWayId
	 * @throws Exception 要重新计算原始运单中的quantity,shipping_cost,prodcut_cost,weight,saleroom
	 * 
	 */
	public void cencelWayBillItems(DBRow row , long parentWayId) throws Exception{
		String tableName = ConfigBean.getStringValue("waybill_order_item");
		try{
			//row 是要删除的Row 里面包含有
			// 要查询出waybill_order中的 items是不是还包含有这个商品。如果有的话就把添加的记录加上去。没有的话就要insert一条新的记录
			
			DBRow updateRow = null ;
			String sqlSelect =  "select	* ,t.parent_waybill_id from waybill_order_item , (select waybill_order.parent_waybill_id from waybill_order where waybill_order.waybill_id = "+row.getString("waybill_order_id")+") t  where waybill_order_item.pc_id = "+row.getString("pc_id")+" and waybill_order_item.waybill_order_id = t.parent_waybill_id;";
			updateRow =  dbUtilAutoTran.selectSingle(sqlSelect);
			if(null != updateRow){
				String addCount = row.getString("quantity");
				String oldCount = updateRow.getString("quantity");
				int length = addCount.indexOf(".");
				int addCountInt = 0;
				int oldCountInt = 0 ;
				if(length != -1){
					addCountInt = Integer.parseInt((addCount.substring(0,length)));
					oldCountInt = Integer.parseInt((oldCount.substring(0,oldCount.indexOf("."))));
				}else{
					addCountInt = Integer.parseInt((addCount ));
					oldCountInt = Integer.parseInt((oldCount ));
				}
				int total = addCountInt + oldCountInt;
				updateRow.add("quantity", total);
				updateRow.add("waybill_order_id", parentWayId);
				if(updateRow.get("oid", 0l) == 0l){
					updateRow.add("oid", row.get("oid", 0l));
				}
				updateRow.remove("parent_waybill_id");
				// 要重新的计算quantity,shipping_cost,prodcut_cost,weight,saleroom
				// 读取row中的数据 然后根据 
				double basic =  total / Double.parseDouble("" + addCount );
				updateRow.add("prodcut_cost", row.get("prodcut_cost", 0.0d) * basic);
				updateRow.add("weight", row.get("weight", 0.0d) * basic);
				updateRow.add("saleroom", row.get("saleroom", 0.0d) * basic);
				
			 	dbUtilAutoTran.update(" where waybill_order_item_id = " + updateRow.getString("waybill_order_item_id") , tableName, updateRow);
				// 删除原来的记录
			 	dbUtilAutoTran.delete("where waybill_order_item_id = " + row.getString("waybill_order_item_id"), tableName);
				
			}else{
				//用Row 来update
				DBRow parentRow  = this.getWayBillById(parentWayId);
				row.add("tracking_number", parentRow.getString("tracking_number"));
				row.remove("p_name");
				row.add("waybill_order_id", parentWayId);
				row.remove("pc_id");
				dbUtilAutoTran.update(" where waybill_order_item_id = " + row.getString("waybill_order_item_id"), tableName, row);
			}
			
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.cencelWayBillItems():"+e);
		}
	}
	public int updateWayBillStatus(long id) throws Exception{
		int status = 0;
		try { 
			String tableName = ConfigBean.getStringValue("waybill_order");
			String countSumSql =  "select sum(waybill_order_item.quantity) as sum_quantity from waybill_order_item where  waybill_order_item.waybill_order_id  = " + id;
			DBRow countSumRow = dbUtilAutoTran.selectSingle(countSumSql);
			if(countSumRow.get("sum_quantity", 0.0) <= 0){
				// 表示是 取消的状态
				status = 3;
			}
			else
			{
				String selectSubWayBillSql = "select count(*) as sum_sub_way from waybill_order where parent_waybill_id = " + id;
				DBRow countSubWayBill = dbUtilAutoTran.selectSingle(selectSubWayBillSql);
				if( countSubWayBill.get("sum_sub_way", 0) <= 0 )
				{
					//待打印
					status = 0;
				}
				else
				{
					//拆分中
					status = 4;
				}
			}
			DBRow updateRow = new DBRow();
			updateRow.add("status", status);
			dbUtilAutoTran.update(" where waybill_id = " + id , tableName, updateRow);
			return status;
		}
		catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR updateWayBillStatus error:"+e);
		}
	}
	public DBRow getWayBillOrderItemInfo(String id) throws Exception {
		try{
			long fixId = Long.parseLong(id);
			String sql = "select order_item_id , pc_id as  cart_pid , product_type as cart_product_type from waybill_order_item where waybill_order_item_id ="	+ fixId;
			return dbUtilAutoTran.selectSingle(sql);
 		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getWayBillOrderItemInfo():"+e);
		}
	}
	public void deleteWayBillById(long id) throws Exception {
		try{
			 dbUtilAutoTran.delete(" where waybill_id = " + id, ConfigBean.getStringValue("waybill_order"));
 		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.deleteWayBillById():"+e);
		}
	}
	public DBRow getWayBillSaleRoomById(long wayBillId) throws Exception {
		try{
			String sql = "select  sum(waybill_order_item.saleroom) sum_sale from waybill_order_item where waybill_order_id = " + wayBillId;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getWayBillSaleRoomById():"+e);
		}
	}
	public DBRow getUserNameByAdid(long adid) throws Exception{
		try{
			String sql = "select admin.employe_name,admin.account from admin  where adid =   "  + adid;   
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getUserNameByAdid():"+e);
		}
	}
	public void updateWayBillStatus(long wayBillId , DBRow updateRow) throws Exception{
		try{
			 dbUtilAutoTran.update(" where waybill_id=" + wayBillId,ConfigBean.getStringValue("waybill_order"),updateRow);
			 
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.updateWayBillStatus():"+e);
		}
	}
	public DBRow getPostDateByOid(long oid) throws Exception {
		try{
			 String sql = "select  post_date from "+ConfigBean.getStringValue("porder")+" where oid = " + oid;
			 return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getPostDateByOid():"+e);
		}
	}
	
	public DBRow[] getOutBoundListByPsId(long psId , PageCtrl pc) throws Exception{
		try{
			String sql = "select out_storebill_order.* , product_storage_catalog.title from out_storebill_order , product_storage_catalog where product_storage_catalog.id = out_storebill_order.ps_id   and out_storebill_order.ps_id = " + psId + " order by out_storebill_order.out_id desc";
			return dbUtilAutoTran.selectMutliple(sql, pc); 
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getOutBoundListByPsId():"+e);
		}
	}
	public DBRow[] getWayBillByOutId(long outId) throws Exception {
		try{
			String sql = "select `status` ,create_account,print_account,delivery_account,cancel_account,split_account,waybill_id,create_date,print_date,delivery_date,cancel_date,split_date from waybill_order where out_id  = " + outId + " limit 0,5";
 			return dbUtilAutoTran.selectMutliple(sql); 
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getWayBillByOutId():"+e);
		}
	}
	public DBRow[] getOutBoundByNumber(long outId,PageCtrl pc) throws Exception {
		try{
			String sql = "select out_storebill_order.* , product_storage_catalog.title from out_storebill_order , product_storage_catalog where product_storage_catalog.id = out_storebill_order.ps_id   and  out_storebill_order.out_id like '%"+outId+"%'";
		 
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getOutBoundByNumber():"+e);
		}
	}
	public DBRow[] getOutBoundByFilter(DBRow queryRow,PageCtrl pc) throws Exception {
		try{
			int state = queryRow.get("state", -1);
			long psId = queryRow.get("ps_id", 0l);
			String sql = "select out_storebill_order.*,product_storage_catalog.title from out_storebill_order , product_storage_catalog where product_storage_catalog.id = out_storebill_order.ps_id and  product_storage_catalog.id = "+psId;
			if(state != -1){
				sql += " and  out_storebill_order.state =" + state;
			} 
			sql +=  " order by out_storebill_order.out_id desc";
 			return dbUtilAutoTran.selectMutliple(sql, pc);
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getOutBoundByNumber():"+e);
		}
	}
	public void updateBoundByRow(DBRow updateRow) throws Exception {
		try{
			long outId = updateRow.get("out_id", 0l);
			String tableName = ConfigBean.getStringValue("out_storebill_order");
			updateRow.remove("out_id");
			dbUtilAutoTran.update(" where out_id =" + outId, tableName, updateRow);
			
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.updateBoundByRow():"+e);
		}
	}
	public DBRow[] getWaybillByOutIdPc(long outId , PageCtrl pc) throws Exception {
		try{
			 String sql = "select * from waybill_order where out_id = " + outId;
			 return dbUtilAutoTran.selectMutliple(sql, pc);		
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getWaybillByOutIdPc():"+e);
		}
	}
	public DBRow[] getWayBillBySearchKey(long[] ids) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("waybill_order");
 			if(ids == null  || (ids != null && ids.length < 1)){
				return (new DBRow[0]);
			}else{
				StringBuffer whereContion = new StringBuffer("");
					for(int index = 0 , count = ids.length ; index < count ; index++ ){
						if(index == 0){
							whereContion.append(" where waybill_id=").append(ids[0]);
						}else{
							whereContion.append(" or waybill_id=").append(ids[index]);
			}
				}
 			 return dbUtilAutoTran.selectMutliple("select * from " +tableName + whereContion.toString());
			}
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZR.getTradeByTradeId4Search():"+e);
		}
	}
	
	public DBRow[] getWaybillNoteByWayBillId(long wayBillId , int number) 
		throws Exception 
	{
		try
		{
		 
			StringBuffer sql = null ;
			if(number  != 0)
			{
				 sql = new StringBuffer("select waybill_note.*,admin.employe_name from waybill_note left join admin on waybill_note.adid = admin.adid where waybill_note.waybill_id =").append(wayBillId).append(" order by wn_id desc limit 0,").append(number).append("; ");
			}
			else
			{
				 sql = new StringBuffer("select waybill_note.*,admin.employe_name from waybill_note left join admin on waybill_note.adid = admin.adid where waybill_note.waybill_id =").append(wayBillId).append(" order by wn_id desc");

			}
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZR.getWaybillNoteByWayBillId():"+e);
		}		
	}
	
	public DBRow[] getItemOidsByWayBillId(long wayBillId)  throws Exception {
		try
		{
			String tableName =  ConfigBean.getStringValue("waybill_order_item");
			String sql =  "select distinct(waybill_order_item.oid) from "+tableName+" where waybill_order_id ="+wayBillId;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZR.getItemOidsByWayBillId():"+e);
		}
	}
}
