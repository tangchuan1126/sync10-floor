package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorProductCatalogMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 获得全部分类平铺
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllProductCatalogChild()
		throws Exception
	{
		try 
		{
			String sql = "select pcl.*,pc.hs_code,pc.title,pc.tax from pc_child_list as pcl "
						+"inner join product_catalog as pc on pc.id = pcl.pc_id and pcl.search_rootid=0";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCatalogMgrZJ getAllProductCatalogChild error:"+e);
		}
	}
	
	/**
	 * 获得所有子分类
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllChildCatalog(long catalog_id)
		throws Exception
	{
		try 
		{
			String sql = "select pcl.*,pc.hs_code,pc.title,pc.tax from pc_child_list as pcl "
						+"join product_catalog as pc on pc.id = pcl.pc_id and pcl.pc_id<>? and pcl.search_rootid=?";
			
			DBRow para = new DBRow();
			para.add("pc_id",catalog_id);
			para.add("search_rootid",catalog_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCatalogMgtZJ getAllChildCatalog error:"+e);
		}
	}
	
	/**
	 * 传回的是一级分类的ID
	 * @param catalog_id
	 */
	public void callProductCatalogChildList(long catalog_id)
		throws Exception
	{
		try 
		{
			String sql = "call pc_child_list("+catalog_id+")";
			dbUtilAutoTran.CallStringSingleResultProcedure(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCatalogMgrZJ callProductCatalogChildList error:"+e);
		}
	}
	
	/**
	 * 获得顶级分类
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getFirstCatalog(long catalog_id)
		throws Exception
	{
		String sql = "select * from pc_child_list where search_rootid IN (select search_rootid from pc_child_list where pc_id=?) and parentid=0 AND search_rootid !=0";
		
		
		DBRow para = new DBRow();
		para.add("pc_id",catalog_id);
		
		return dbUtilAutoTran.selectPreSingle(sql,para);
	}
	
	public DBRow isExistParent(long parentid,long catalog_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_catalog")+" where id= ? and parentid= ? ";
			
			DBRow para = new DBRow();
			para.add("id",catalog_id);
			para.add("parentid",parentid);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCatalogMgrZJ isExistParent error:"+e);
		}
	}
	
	public DBRow checkCatalogParent(long move_to_catalog,long catalog_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from pc_child_list where pc_id = ? and search_rootid=?";
			
			DBRow para = new DBRow();
			para.add("pc_id",move_to_catalog);
			para.add("search_rootid",catalog_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductCatalogMgrZJ checkCatalogParent error:"+e);
		}
	}
	
	/**
	 * 根据商品分类获得所有父分类
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFatherTree(long catalog_id)
		throws Exception
	{
		try 
		{
			String sql = "select pc.* from pc_child_list as pcl "
						+"join product_catalog as pc on pcl.search_rootid = pc.id and pcl.pc_id=? and pcl.search_rootid !=0 " 
						+"order by levv desc";
			
			DBRow para = new DBRow();
			para.add("catalog_id",catalog_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCatalogMgrZJ getFatherTree error:"+e);
		}
	}
	
	public DBRow getDetailProductCatalog(long catalog_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_catalog")+" where id = ? ";
			
			DBRow para = new DBRow();
			para.add("catalog_id",catalog_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCatalogMgrZJ getDetailProductCatalog error:"+e);
		}
	}
	
	/**
	 * 查询产品分类与产品线关系
	 * @param catalog_id
	 * @param product_line
	 * @return
	 * @throws Exception
	 */
	public DBRow isExistCategoryProductLine(long catalog_id, long product_line)throws Exception
		{
			try 
			{
				String sql = "select * from "+ConfigBean.getStringValue("product_catalog")+" where id= ? and product_line_id= ? ";
				
				DBRow para = new DBRow();
				para.add("id",catalog_id);
				para.add("product_line_id",product_line);
				
				return dbUtilAutoTran.selectPreSingle(sql, para);
			}
			catch (Exception e) 
			{
				throw new Exception("FloorProductCatalogMgrZJ isExistCategoryProductLine error:"+e);
			}
		}
	
	

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
