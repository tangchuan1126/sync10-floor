package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
 

public class FloorPurchaseMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public String getLastDetailEta(long purchase_id) throws Exception {
		try{
			 String sql = "select max(eta) as eta_time from purchase_detail where purchase_id =" + purchase_id;
 
			 DBRow row =  dbUtilAutoTran.selectSingle(sql);
			 if(row != null){
				 return row.getString("eta_time") ;
			 }
			  return "";
		}catch(Exception e){
			throw new Exception("FloorPurchaseMgrZr.getLastDetailEta(purchase_id):"+e);
		}
	}
	public void updatePurchaseDetailEta(String eta , long purchase_id)throws Exception {
		try{
				dbUtilAutoTran.update("where purchase_id =" + purchase_id, "eta ='"+eta+"'", "purchase_detail") ; 
		}catch(Exception e){
			throw new Exception("FloorPurchaseMgrZr.updatePurchaseDetailEta(eta,purchase_id):"+e);
		}
	}

	public DBRow[] getPurchasesBySearchKey(long[] billIds) throws Exception{
		try{
			
			String tableName =  ConfigBean.getStringValue("purchase");
			if(billIds == null  || (billIds != null && billIds.length < 1)){
				return (new DBRow[0]);
			}else{
				StringBuffer whereContion = new StringBuffer("");
				
					for(int index = 0 , count = billIds.length ; index < count ; index++ ){
						if(index == 0){
							whereContion.append(" where purchase_id=").append(billIds[0]);
						}else{
							whereContion.append(" or purchase_id=").append(billIds[index]);
					}
				}
				return dbUtilAutoTran.selectMutliple("select * from " +tableName + whereContion.toString());
			}
		}catch(Exception e){
			throw new Exception("FloorPurchaseMgrZr.getPurchasesBySearchKey(billIds):"+e);
		}	
		 
	}
	/**
	 * 通过采购单ID,日志类型，查询采购单日志
	 * @param purchase
	 * @param types
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseLogsByPurchaseIdAndType(long purchase, int[] types) throws Exception{
		try{
			String sql = "select * from purchase_followup_logs";
			if(null != types && types.length > 0){
				if(1 == types.length){
					sql += " where followup_type = " + types[0];
				}else{
					String sqlWhere = " where ( followup_type = " + types[0];
					for(int i = 1; i < types.length; i ++){
						sqlWhere += " or followup_type = " + types[i];
					}
					sqlWhere += ")";
					sql += sqlWhere;
				}
				sql += " and purchase_id = " + purchase;
			}else{
				sql += " where purchase_id = " + purchase;
			}
			sql += " order by followup_type, followup_date desc";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorPurchaseMgrZr.getPurchaseLogsByPurchaseIdAndType(long purchase, int[] types):"+e);
		}	
	}
}
