package com.cwc.app.floor.api.ll;

import com.cwc.app.beans.ll.AdminBeanLL;
import com.cwc.db.DBRow;

public class FloorAdminMgrLL {
	private AdminBeanLL adminBeanLL;

	public AdminBeanLL getAdminBeanLL() {
		return adminBeanLL;
	}

	public void setAdminBeanLL(AdminBeanLL adminBeanLL) {
		this.adminBeanLL = adminBeanLL;
	}
	
	public DBRow getAdminById(String id) throws Exception{
		return adminBeanLL.getRowById(id);
	}
}
