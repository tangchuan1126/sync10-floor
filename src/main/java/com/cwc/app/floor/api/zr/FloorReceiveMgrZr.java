package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorReceiveMgrZr
{

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public DBRow[] getCommonReceivePallet(long lr_id) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("wms_receive_pallet_type_count"); 
			String sql = "select * from "+ tableName + " where LR_ID="+lr_id + " and IFNULL(pallet_type_count,0) > 0 order by receive_pallet_id desc ";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionMgrZr.getCommonReceivePallet(lr_id):"+e);
		}
	}
	public long addCommonReceivePallet(DBRow insertRow) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("wms_receive_pallet_type_count");
			return dbUtilAutoTran.insertReturnId(tableName, insertRow);
		}catch(Exception e){
			throw new Exception("FloorQuestionMgrZr.addCommonReceivePallet(insertRow):"+e);
		}
	}
	public int deleteCommonReceivePallet(long receive_pallet_id) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("wms_receive_pallet_type_count");
			return dbUtilAutoTran.delete(" where receive_pallet_id="+receive_pallet_id, tableName);
		}catch(Exception e){
			throw new Exception("FloorQuestionMgrZr.deleteCommonReceivePallet(receive_pallet_id):"+e);
		}
	}
	public int updateCommonReceivePalletType(long receive_pallet_id , DBRow updateRow) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("wms_receive_pallet_type_count");
			return dbUtilAutoTran.update(" where receive_pallet_id="+receive_pallet_id, tableName, updateRow);
		}catch(Exception e){
			throw new Exception("FloorQuestionMgrZr.updateCommonReceivePalletType(receive_pallet_id):"+e);
		}
	}
	
}
