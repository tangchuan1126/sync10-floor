package com.cwc.app.floor.api.zzq;

import java.util.List;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.DBUtilAutoTranSQLServer;

/**
 * @author Zhengziqi 2015年3月4日
 *
 */
/**
 * @author 	Zhengziqi
 * 2015年7月1日
 *
 */
public class FloorReceiptsMgrZzq {


	private DBUtilAutoTran dbUtilAutoTran;
	
	private DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer;
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public void setDbUtilAutoTranSQLServer(
			DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer) {
		this.dbUtilAutoTranSQLServer = dbUtilAutoTranSQLServer;
	}

	/**
	 *
	 * query Receipt Info By company_id and receipt_no 2015年3月4日
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow queryReceiptInfoByRN(DBRow param) throws Exception {

		try {

			String sql = "select * from receipts r where r.company_id = ? and r.receipt_no = ? ";

			return dbUtilAutoTran.selectPreSingle(sql, param);

		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryReceiptInfoByRN(DBRow param) error:"
							+ e);
		}
	}
	
	/**
	 *
	 * query Ship_To by pc_id
	 * 2015年5月21日
	 * @param pc_id
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] queryShipToByProductId(Long pc_id, int title_id, int customer_key, long ps_id) throws Exception {
		try {
			StringBuffer sb = new StringBuffer(" select st.ship_to_name, st.ship_to_id, GROUP_CONCAT(ltso.lp_title_ship_id) as ltso_ids, GROUP_CONCAT(ltso.lp_type_id) as lpt_ids from ship_to st ");
			sb.append(" join lp_title_ship_to ltso on st.ship_to_id = ltso.ship_to_id ")
			.append(" join license_plate_type lpt on ltso.lp_type_id = lpt.lpt_id ")
			.append(" where lpt.pc_id = ")
			.append(pc_id);
			if(title_id>0){
				sb.append(" and ( ltso.title_id is null or ltso.title_id = ")
				.append(title_id).append(")");
			}
			
			sb.append(" and ( ltso.customer_id is null or ltso.customer_id = ")
			.append(customer_key)
			.append(") and ( ltso.ps_id is null or ltso.ps_id = ")
			.append(ps_id)
			.append(") group by st.ship_to_name ");
			return dbUtilAutoTran.selectMutliple(sb.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryShipToByProductId(Long pc_id) error:"
							+ e);
		}
	}
	
	/**
	 *
	 * TODO
	 * 2015年6月6日
	 * @param pc_id
	 * @param title_id
	 * @param customer_key
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] queryCLPByProductId(Long pc_id, int title_id, int customer_key, long ps_id) throws Exception {
		try {
			StringBuffer sb = new StringBuffer(" select lpt.*, ct.type_name, GROUP_CONCAT(ltst.ship_to_id) as ship_to_ids, GROUP_CONCAT(st.ship_to_name) as ship_to_names, lpt.lp_title_id ");
			sb.append(" from license_plate_type lpt ")
			.append(" join container_type ct on lpt.basic_type_id=ct.type_id ")
			.append(" left join lp_title_ship_to ltst on ltst.lp_type_id = lpt.lpt_id and ltst.lp_pc_id = lpt.pc_id ")
			.append(" and ltst.ps_id = ")
			.append(ps_id);
			if(title_id>0){
				sb.append(" and ltst.title_id = ")
				.append(title_id);
			}
			
			sb.append(" and ltst.customer_id = ")
			.append(customer_key)
			.append(" left join ship_to st on st.ship_to_id = ltst.ship_to_id ")
			.append(" where lpt.pc_id = ")
			.append(pc_id)
			.append(" and (FIND_IN_SET(")
			.append(title_id)
			.append(",lpt.lp_title_id) or lpt.lp_title_id is null)");
			// 添加 or lpt.lp_title_id is null ，为了方便 查询出数据
			return dbUtilAutoTran.selectMutliple(sb.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryCLPByProductId(Long pc_id) error:"
							+ e);
		}
	}
	
	/**
	 *
	 * query All Ship_To
	 * 2015年5月30日
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] queryAllShipTo() throws Exception {
		try {
			String sql = " select st.ship_to_name, st.ship_to_id from ship_to st ";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryAllShipTo(Long pc_id) error:"
							+ e);
		}
	}
	
	/**
	 *
	 * query Receipt Info By RN And Detail_Id
	 * 2015年4月14日
	 * @param param
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow queryReceiptInfoByRNAndDetailId(DBRow param) throws Exception {

		try {

			String sql = "select r.*, dolod.note as detail_note from receipts r left join door_or_location_occupancy_details dolod on dolod.dlo_detail_id = ? where r.company_id = ? and r.receipt_no = ? ";

			return dbUtilAutoTran.selectPreSingle(sql, param);

		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryReceiptInfoByRNAndDetailId(DBRow param) error:"
							+ e);
		}
	}

	public DBRow[] queryReceiptLinesInfoByRNId(DBRow paramRNRow)
			throws Exception {

		try {

			StringBuffer sql = new StringBuffer();
			sql.append(" select rl.*, count(c.con_id) as pallets_no,count(cp.cp_lp_id) as receive_pallet,  sum(cp.cp_quantity) as qty_no");
			sql.append(" ,rl.counted_user,rl.unloading_finish_time,cu.employe_name as counted_user_name,scan_end_user.employe_name as scan_end_user");
			sql.append(" ,rl.scan_start_adid,scan_start_user.employe_name as scan_start_user, rl.scan_start_time");
			sql.append(" from receipt_lines rl ");
			sql.append(" left join receipt_rel_container rrc on  rl.receipt_line_id = rrc.receipt_line_id ");
			sql.append(" LEFT JOIN container_config cc ON cc.container_config_id = rrc.container_config_id AND cc.is_delete = 0");
			sql.append(" left join container c on rrc.plate_no = c.con_id and c.is_delete = 0 ");
			sql.append(" left join container_product cp on cp.cp_lp_id = c.con_id  ");
			sql.append(" left join admin cu on cu.adid = rl.counted_user  ");
			sql.append(" left join admin scan_end_user on scan_end_user.adid = rl.scan_end_adid  ");
			sql.append(" left join admin scan_start_user on scan_start_user.adid = rl.scan_start_adid  ");
			sql.append(" where receipt_id =  ? ");
			sql.append(" and expected_qty > 0 ");
			sql.append(" group by rl.receipt_line_id ");
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), paramRNRow);

		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryReceiptLinesInfoByRNId(DBRow param) error:"
							+ e);
		}
	}

	/**
	 *
	 * 查询lines 2015年3月21日
	 * 
	 * @param paramRNRow
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] queryReceiptLinesInfoByRN(DBRow paramRNRow) throws Exception {

		try {

			StringBuffer sql = new StringBuffer();
			sql.append(" select rl.*, count(c.con_id) as pallets_no,  sum(cp.cp_quantity) as qty_no from receipt_lines rl ");
			sql.append(" left join receipt_rel_container rrc on  rl.receipt_line_id = rrc.receipt_line_id ");
			sql.append(" left join container c on rrc.plate_no = c.con_id ");
			sql.append(" left join container_product cp on cp.cp_lp_id = c.con_id  ");
			sql.append(" where company_id =  ? ");
			sql.append(" and receipt_no = ?  ");
			sql.append(" and expected_qty > 0 ");
			sql.append(" group by rl.receipt_line_id ");
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), paramRNRow);

		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryReceiptLinesInfoByRN(DBRow param) error:"
							+ e);
		}
	}

	/**
	 *
	 * 批量更新wms的rn lines信息（item_id,lot_no,qty）至mysql数据库 2015年3月5日
	 * 
	 * @param rows
	 * @throws Exception
	 *
	 */
	public void updateRNLineByBatch(DBRow[] rows) throws Exception {
		try {
			if (rows != null && rows.length > 0) {
				int len = rows.length;
				StringBuffer sql = null;
				String[] sqls = new String[len];
				for (int i = 0; i < len; i++) {
					sql = new StringBuffer(
							" update receipt_lines set wms_item = '")
							.append(rows[i].get("item_id"))
							.append("', wms_lot_no = '")
							.append(rows[i].get("lot_no"))
							.append("', wms_qty = '")
							.append(rows[i].get("expected_qty"))
							.append("' where expected_qty > 0 and company_id = '")
							.append(rows[i].get("company_id"))
							.append("' and receipt_no = '")
							.append(rows[i].get("receipt_no"))
							.append("' and line_no = ")
							.append(rows[i].get("line_no"));
					sqls[i] = sql.toString();
				}
				this.dbUtilAutoTran.batchUpdate(sqls);
			}
		} catch (Exception e) {
			throw new Exception("FloorReceiptMgrZzq.updateRNLineByBatch" + e);
		}
	}

	/**
	 *
	 * 批量更新打印记录 2015年3月24日
	 * 
	 * @param pallet_ids
	 * @param adid
	 * @param dateStr
	 * @param print_number
	 * @throws Exception
	 *
	 */
	public void updatePrintInfo(String pallet_ids, String adid, String dateStr,
			int print_number) throws Exception {
		try {
			String sqls = " update receipt_rel_container set print_date = '"
					+ dateStr + "' , print_user =" + adid + ", print_number = "
					+ print_number + " where plate_no in  (" + pallet_ids
					+ ") ";
			this.dbUtilAutoTran.executeSQL(sqls);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZzq.updatePrintInfo" + e);
		}
	}

	/**
	 *
	 * RN打印后更新打印时间、人、数量 2015年4月6日
	 * 
	 * @param rnId
	 * @param upRow
	 * @throws Exception
	 *
	 */
	public void updateRNPrintInfo(long receipt_no, String company_id,
			DBRow upRow) throws Exception {
		try {
			dbUtilAutoTran.update("where receipt_no = " + receipt_no
					+ " and company_id = '" + company_id + "' ", "receipts",
					upRow);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZzq.updateRNPrintInfo" + e);
		}
	}

	/**
	 *
	 * 获取指定RN的所有line no 2015年3月5日
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 *
	 */
	public String queryReceiptLineNos(DBRow param) throws Exception {

		try {
			String lines = "";
			StringBuffer sql = new StringBuffer();
			sql.append(
					" select GROUP_CONCAT(CAST(line_no AS char)) as 'lines' from receipt_lines  ")
					.append(" where expected_qty > 0 and company_id = '")
					.append(param.get("company_id"))
					.append("' and receipt_no = '")
					.append(param.get("receipt_no"))
					.append("' group by company_id,receipt_no ");
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			if (row != null && row.size() > 0) {
				lines = row.getString("lines");
			}
			return lines;
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryReceiptLineNos() error:"
							+ e);
		}
	}

	/**
	 *
	 * 根据cc_id获取pallet_ids 2015年3月5日
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 *
	 */
	public String queryPalletIdsByContainerConfigId(String container_config_id)
			throws Exception {

		try {
			String pallet_ids = "";
			StringBuffer sql = new StringBuffer();
			sql.append(
					" select GROUP_CONCAT( CAST(rrc.plate_no AS char)) as pallet_ids from receipt_rel_container rrc  ")
					.append(" join container_config cc on rrc.container_config_id = cc.container_config_id ")
					.append(" join container c on c.con_id = rrc.plate_no and c.is_delete = 0 ")
					.append(" where cc.container_config_id in ( ")
					.append(container_config_id)
					.append(") group by cc.container_config_id ");
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			if (row != null && row.size() > 0) {
				pallet_ids = row.getString("pallet_ids");
			}
			return pallet_ids;
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryPalletIdsByContainerConfigId() error:"
							+ e);
		}
	}

	/**
	 *
	 * 根据line id查找entrey id 2015年3月24日
	 * 
	 * @param line_id
	 * @return
	 * @throws Exception
	 *
	 */
	public String queryEntryIdByLineId(String line_id) throws Exception {

		try {
			String equipment_id = "";
			StringBuffer sql = new StringBuffer();
			sql.append(" select dood.equipment_id from receipt_lines rl  ")
					.append(" left join door_or_location_occupancy_details dood on rl.detail_id = dood.dlo_detail_id ")
					.append(" where rl.receipt_line_id = ").append(line_id);
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			if (row != null && row.size() > 0) {
				equipment_id = row.getString("equipment_id");
			}
			return equipment_id;
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryEntryIdByLineId() error:"
							+ e);
		}
	}

	/**
	 *
	 * 根据line id查找entrey id 2015年3月24日
	 * 
	 * @param line_id
	 * @return
	 * @throws Exception
	 *
	 */
	public String queryAreaIdByAdid(String adid) throws Exception {

		try {
			String area = "";
			StringBuffer sql = new StringBuffer();
			sql.append(" select * from admin where adid = ").append(adid);
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			if (row != null && row.size() > 0) {
				area = row.getString("AreaId");
			}
			return area;
		} catch (Exception e) {
			throw new Exception("FloorReceiptMgrZzq.queryAreaIdByAdid() error:"
					+ e);
		}
	}

	/**
	 *
	 * 根据rn id查找entrey id 2015年3月24日
	 * 
	 * @param line_id
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow queryEntryIdByRNId(String receipt_id) throws Exception {

		try {
			StringBuffer sql = new StringBuffer();
			sql.append(
					" select r.company_id, r.receipt_no, dood.equipment_id,dood.dlo_id from receipts r  ")
					.append("  left join door_or_location_occupancy_details dood on r.detail_id = dood.dlo_detail_id ")
					.append(" where r.receipt_id = ").append(receipt_id);
			return dbUtilAutoTran.selectSingle(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryEntryIdByRNId() error:"
							+ e);
		}
	}

	/**
	 *
	 * TODO 2015年3月17日
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] queryPalletsByLineAndId(DBRow param) throws Exception {

		try {
			StringBuffer sql = new StringBuffer();
			sql.append(
					" select c.con_id, rl.receipt_line_id, rl.receipt_no,  rl.supplier, r.customer_id, rl.item_id, rl.lot_no, cc.length, cc.width, cc.height, CAST(cp.cp_quantity AS char) as count_qty, cp.cp_pc_id as count_pcid, count(CAST(sp.serial_number AS char)) as sn_qty, sp.pc_id as sn_pcid   from receipt_lines rl ")
					.append(" left join receipt_rel_container rrc on rl.receipt_line_id = rrc.receipt_line_id ")
					.append(" left join receipts r on r.company_id = rl.company_id and r.receipt_no = rl.receipt_no ")
					.append("left join container c on rrc.plate_no = c.con_id ")
					.append(" left join container_config cc on cc.container_config_id = rrc.container_config_id ")
					.append(" left join container_product cp on  cp.cp_lp_id = c.con_id ")
					.append(" left join serial_product sp on  sp.at_lp_id = c.con_id ")
					.append(" where rl.expected_qty > 0 and rl.receipt_line_id = ")
					.append(param.get("receipt_line_id"))
					.append(" and c.con_id in ( ")
					.append(param.get("pallet_ids"))
					.append(") group by c.con_id ");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryPalletsByLineAndId() error:"
							+ e);
		}
	}
	
	/**
	 *
	 * TODO
	 * 2015年5月14日
	 * @param receipt_line_id
	 * @param pallet_type
	 * @param license_plate_type_id
	 * @return
	 * @throws Exception
	 * add ship_to_name by zhaoyy 2015年6月2日 15:03:01
	 */
	public DBRow[] queryContainerConfigByOwnParams(long receipt_line_id, String pallet_type, long license_plate_type_id, long ship_to) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" select cc.*,ifnull(st.ship_to_name,'') as ship_to_name , ifnull(lpt.lp_name,'') as lp_name , rrc.print_user as print_user_name,rrc.print_date , count(rrc.receipt_rel_container_id) as plate_number,")
				.append(" case when cc.goods_type = 0 then SUM(rrc.normal_qty)  ")
				.append(" else SUM(rrc.damage_qty) end as goods_qty ")
				.append(" from container_config cc ")
				.append(" join receipt_rel_container rrc on cc.container_config_id = rrc.container_config_id and rrc.receipt_line_id = cc.receipt_line_id ")
				.append(" join container c on c.con_id = rrc.plate_no and c.is_delete = 0 ")
				.append(" LEFT JOIN ship_to st ON st.ship_to_id = cc.ship_to_id")
				.append(" LEFT JOIN license_plate_type lpt on lpt.lpt_id = cc.license_plate_type_id")
				.append(" where cc.receipt_line_id = ")
				.append(receipt_line_id);
			if(pallet_type != null && !"".equals(pallet_type)){
				sql.append(" and cc.pallet_type = '")
				.append(pallet_type)
				.append("' ");
			}
			if(license_plate_type_id > 0){
				sql.append(" and cc.license_plate_type_id = ")
				.append(license_plate_type_id);
			}	
			if(ship_to > 0){
				sql.append(" and cc.ship_to_id = ")
				.append(ship_to);
			}	
			sql.append(" group by cc.container_config_id ");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryContainerConfigByOwnParams() error:"
							+ e);
		}
	}
	
	/**
	 *
	 * query Container_Config By Own Params For TLP
	 * 2015年6月5日
	 * @param receipt_line_id
	 * @param pallet_type
	 * @param length
	 * @param width
	 * @param height
	 * @param is_partial
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] queryContainerConfigByOwnParamsForTLP(long receipt_line_id, String pallet_type, int length, int width, int height, int is_partial) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" select cc.*,  rrc.print_user as print_user_name,rrc.print_date , count(rrc.receipt_rel_container_id) as plate_number,")
				.append(" case when cc.goods_type = 0 then SUM(rrc.normal_qty)  ")
				.append(" else SUM(rrc.damage_qty) end as goods_qty ")
				.append(" from container_config cc ")
				.append(" join receipt_rel_container rrc on cc.container_config_id = rrc.container_config_id ")
				.append(" join container c on c.con_id = rrc.plate_no and c.is_delete = 0 ")
				.append(" where cc.is_delete = 0 and cc.receipt_line_id = ")
				.append(receipt_line_id)
				.append(" and cc.length = ")
				.append(length)
				.append(" and cc.width = ")
				.append(width)
				.append(" and cc.height = ")
				.append(height);
			if(pallet_type != null && !"".equals(pallet_type)){
				sql.append(" and cc.pallet_type = '")
				.append(pallet_type)
				.append("' ");
			}
			if(is_partial > 0){
				sql.append(" and cc.is_partial = ")
				.append(is_partial);
			}	
			sql.append(" group by cc.container_config_id ");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryContainerConfigByOwnParamsForTLP() error:"
							+ e);
		}
	}
	
	/**
	 *
	 * query Pallet ids By Receipt_No And Company_Id
	 * 2015年4月7日
	 * @param receipt_id
	 * @return
	 * @throws Exception
	 *
	 */
	public String queryPalletsByReceiptNoAndCompanyId(long receipt_id) throws Exception {
		try {
			String con_ids = null;
			StringBuffer sql = new StringBuffer();
			sql.append( " select GROUP_CONCAT(CAST(c.con_id AS char)) as con_ids from container c ")
			.append(" join receipt_rel_container rrc on rrc.plate_no = c.con_id ")
			.append(" join receipt_lines rl on rl.receipt_line_id = rrc.receipt_line_id ")
			.append(" where c.is_delete = 0 and rl.receipt_id = ")
			.append(receipt_id)
			.append(" group by rl.company_id, rl.receipt_no ");
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			if (row != null && row.size() > 0) {
				con_ids = row.getString("con_ids");
			}
			return con_ids;
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryPalletsByReceiptNoAndCompanyId() error:"
							+ e);
		}
	}

	/**
	 *
	 * query Pallets By Receipt_Line_Id
	 * 2015年4月24日
	 * @param receipt_line_id
	 * @return
	 * @throws Exception
	 *
	 */
	public String queryPalletsByReceiptLineId(long receipt_line_id) throws Exception {
		try {
			String con_ids = null;
			StringBuffer sql = new StringBuffer();
			sql.append( " select GROUP_CONCAT(CAST(c.con_id AS char)) as con_ids from container c ")
			.append(" join receipt_rel_container rrc on rrc.plate_no = c.con_id ")
			.append(" join receipt_lines rl on rl.receipt_line_id = rrc.receipt_line_id ")
			.append(" where c.is_delete = 0 and rl.receipt_line_id = ")
			.append(receipt_line_id)
			.append(" group by rl.receipt_line_id ");
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			if (row != null && row.size() > 0) {
				con_ids = row.getString("con_ids");
			}
			return con_ids;
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryPalletsByReceiptLineId() error:"
							+ e);
		}
	}
	
	/**
	 *
	 * query Pallet_Type By License_Plate_Type_Id
	 * 2015年5月19日
	 * @param receipt_line_id
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow queryPalletTypeByLicensePlateTypeId(long license_plate_type_id) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append( " select ct.* from container_type ct ")
			.append(" join license_plate_type lpt on lpt.basic_type_id =  ct.type_id ")
			.append(" where lpt.lpt_id = ")
			.append(license_plate_type_id);
			return dbUtilAutoTran.selectSingle(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryPalletTypeByLicensePlateTypeId() error:"
							+ e);
		}
	}
	
	/**
	 *
	 * 批量删除指定line 2015年3月5日
	 * 
	 * @param list
	 * @return
	 * @throws Exception
	 *
	 */
	public int deleteReceiptLinesByList(DBRow param) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" where company_id = '").append(param.get("company_id"))
					.append("' and receipt_no = '")
					.append(param.get("receipt_no"))
					.append("' and line_no in ( ")
					.append(param.get("lineNosToDel")).append(" )");
			return dbUtilAutoTran.delete(sql.toString(), "receipt_lines");
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.deleteReceiptLinesByList() error:"
							+ e);
		}
	}

	/**
	 *
	 * 批量增加lines 2015年3月5日
	 * 
	 * @param rows
	 * @param RNId
	 * @throws Exception
	 *
	 */
	public void insertReceiptLinesByList(List<DBRow> rows, long RNId)
			throws Exception {
		try {
			if (rows != null && rows.size() > 0) {
				int len = rows.size();
				StringBuffer sql = null;
				String[] sqls = new String[len];
				for (int i = 0; i < len; i++) {
					sql = new StringBuffer(
							"INSERT INTO receipt_lines( receipt_id, company_id, receipt_no, line_no, item_id, warehouse_id, zone_id, location_id, pallet_size, po_no, po_line_no, lot_no, expected_qty, pallets, note, qty_per_pallet, is_check_sn, sn_size, is_check_lot_no) VALUES(")
							.append(RNId).append(",'")
							.append(rows.get(i).get("company_id")).append("',")
							.append(rows.get(i).get("receipt_no")).append(",")
							.append(rows.get(i).get("line_no")).append(",'")
							.append(rows.get(i).get("item_id")).append("','")
							.append(rows.get(i).get("warehouse_id"))
							.append("','").append(rows.get(i).get("zone_id"))
							.append("','")
							.append(rows.get(i).get("location_id"))
							.append("','")
							.append(rows.get(i).get("pallet_size"))
							.append("','").append(rows.get(i).get("po_no"))
							.append("',").append(rows.get(i).get("po_line_no"))
							.append(",'").append(rows.get(i).get("lot_no"))
							.append("',")
							.append(rows.get(i).get("expected_qty"))
							.append(",'").append(rows.get(i).get("pallets"))
							.append("','").append(rows.get(i).get("note"))
							.append("',")
							.append(rows.get(i).get("qty_per_pallet"))
							.append(",").append(rows.get(i).get("is_check_sn"))
							.append(",").append(rows.get(i).get("sn_size"))
							.append(",")
							.append(rows.get(i).get("is_check_lot_no"))
							.append(")");
					sqls[i] = sql.toString();
				}
				this.dbUtilAutoTran.batchUpdate(sqls);
			}
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.insertReceiptLinesByList() error:"
							+ e);
		}
	}

	/**
	 * 筛选出需要更新的line 2015年3月5日
	 * 
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] queryLinesDifferentWithWMS(DBRow param) throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					" select * from receipt_lines where expected_qty > 0 and (wms_item != item_id or wms_lot_no != lot_no or wms_qty != expected_qty) and company_id = ? and receipt_no = ?");
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryLinesDifferentWithWMS(DBRow param) error:"
							+ e);
		}
	}

	/**
	 * 
	 * 删除Line前验证需要 统计给定line的关联托盘id,并计算这些托盘与货物的关联记录数 2015年3月6日
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] countSNRelWithPalletInLineList(DBRow param) throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					"  select rl.receipt_line_id , GROUP_CONCAT(CAST(cp.cp_id AS char)) as cp_ids, count(cp.cp_quantity) as counts  from receipt_lines rl  ");
			sql.append(
					" left join receipt_rel_container rrc on  rl.receipt_line_id = rrc.receipt_line_id ")
					.append("  left join container c on rrc.plate_no = c.con_id  ")
					.append("  left join container_product cp on cp.cp_lp_id = c.con_id   ")
					.append(" where expected_qty > 0 and rl.company_id = '")
					.append(param.get("company_id"))
					.append("' and rl.receipt_no = '")
					.append(param.get("receipt_no"))
					.append("' and rl.line_no in (")
					.append(param.get("lineNosToDel")).append(") ")
					.append(" group by rl.receipt_line_id  ");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.countSNRelWithPalletInLineList(DBRow param) error:"
							+ e);
		}
	}

	/**
	 * 更新Line前验证需要 统计给定RN下需要更新line的关联托盘id,并计算这些托盘与货物的关联记录数 2015年3月6日
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] countSNRelWithPalletInLineUpdate(DBRow param)
			throws Exception {
		try {
			StringBuffer sql = new StringBuffer(" select * from ( ");
			sql.append(
					" select rl.receipt_line_id , GROUP_CONCAT(CAST(cp.cp_id AS char)) as cp_ids, count(cp.cp_quantity) as counts  from receipt_lines rl  ")
					.append(" left join receipt_rel_container rrc on  rl.receipt_line_id = rrc.receipt_line_id ")
					.append("  left join container c on rrc.plate_no = c.con_id  ")
					.append("  left join container_product cp on cp.cp_lp_id = c.con_id   ")
					.append(" where rl.expected_qty > 0 and (rl.wms_item != rl.item_id or rl.wms_lot_no != rl.lot_no ) and rl.company_id = ? and rl.receipt_no = ?  ")
					.append(" group by rl.receipt_line_id  ")
					.append(" ) as snCheck  ").append(" where counts > 0  ");
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.countSNRelWithPalletInLineUpdate(DBRow param) error:"
							+ e);
		}
	}

	/**
	 * 关闭SN前验证sn 统计RN下所有需要SN验证的line的expected_qty和实际qty 2015年3月6日
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] countSNNumWithPalletInLineByRN(DBRow param) throws Exception {
		try {
			StringBuffer sql = new StringBuffer(" select * from ( ");
			sql.append(
					"select  rl.receipt_line_id , rl.expected_qty, GROUP_CONCAT(CAST(sp.serial_product_id  AS char)) as cp_ids, count(sp.serial_product_id) as counts  from receipts r   ")
					.append("  join receipt_lines rl on rl.receipt_id = r.receipt_id ")
					.append("  join receipt_rel_container rrc on  rl.receipt_line_id = rrc.receipt_line_id ")
					.append("   join container c on rrc.plate_no = c.con_id  ")
					.append("   join serial_product sp on sp.at_lp_id = c.con_id      ")
					.append(" where expected_qty > 0  and r.receipt_id = ? and rl.is_check_sn = 1")
					.append(" group by rl.receipt_line_id  ")
					.append(" ) as snCheck  ")
					.append(" where counts is NULL  or counts != expected_qty  ");
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.countSNNumWithPalletInLineByRN(DBRow param) error:"
							+ e);
		}
	}

	/**
	 *
	 * 关闭SN前验证qty 统计RN下所有line的expected_qty和实际qty 2015年3月6日
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] countQTYNumWithPalletInLineByRN(DBRow param)
			throws Exception {
		try {
			StringBuffer sql = new StringBuffer(" select * from ( ");
			sql.append(
					"select  rl.receipt_line_id , rl.expected_qty, GROUP_CONCAT(CAST(cp.cp_id AS char)) as cp_ids, sum(cp.cp_quantity)  as counts  from receipts r   ")
					.append(" left join receipt_lines rl on rl.receipt_id = r.receipt_id ")
					.append(" left join receipt_rel_container rrc on  rl.receipt_line_id = rrc.receipt_line_id ")
					.append("  left join container c on rrc.plate_no = c.con_id  ")
					.append("  left join container_product cp on cp.cp_lp_id = c.con_id   ")
					.append(" where expected_qty > 0  and r.receipt_id = ? ")
					.append(" group by rl.receipt_line_id  ")
					.append(" ) as qtyCheck  ")
					.append(" where counts != expected_qty or counts is NULL  ");
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.countQTYNumWithPalletInLineByRN(DBRow param) error:"
							+ e);
		}
	}

	/**
	 *
	 * count Damage Qty In Normal Pallet 2015年4月6日
	 * 
	 * @param con_id
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow countDamageQtyInNormalPallet(long con_id) throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					" select SUM(rrc.normal_qty) as goods, SUM(rrc.damage_qty) as damages, GROUP_CONCAT(plate_no) as pallets from receipt_rel_container rrc  ");
			sql.append(
					" join container_config cc on cc.container_config_id = rrc.container_config_id ")
					.append(" where cc.goods_type = 0 and rrc.damage_qty != 0 and rrc.receipt_line_id in (select receipt_line_id from receipt_rel_container where plate_no = ")
					.append(con_id).append(" ) group by rrc.receipt_line_id ");
			return dbUtilAutoTran.selectSingle(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.countDamageQtyInNormalPallet(DBRow param) error:"
							+ e);
		}
	}

	/**
	 *
	 * update Receipt_Rel_Container Damage Qty 2015年4月6日
	 * 
	 * @param pallets
	 * @throws Exception
	 *
	 */
	public void updateReceiptRelContainerDamageQty(String pallets)
			throws Exception {
		try {
			String sql = " update receipt_rel_container rrc set rrc.damage_qty = 0, rrc.normal_qty = 0 where rrc.plate_no in ("
					+ pallets + ")";
			dbUtilAutoTran.executeSQL(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.updateReceiptRelContainerDamageQty(DBRow param) error:"
							+ e);
		}
	}

	/**
	 *
	 * update Receipt_Line Goods_Qty 2015年4月6日
	 * 
	 * @param qty
	 * @param receipt_line_id
	 * @throws Exception
	 *
	 */
	public void updateReceiptLineGoodsQty(int qty, long receipt_line_id)
			throws Exception {
		try {
			String sql = " update receipt_lines set goods_total =  " + qty
					+ " where receipt_line_id = " + receipt_line_id;
			dbUtilAutoTran.executeSQL(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.updateReceiptLineGoodsQty(DBRow param) error:"
							+ e);
		}
	}

	/**
	 *
	 * 并不删除，而是将lines的expected_qty至为0 2015年3月5日
	 * 
	 * @param rows
	 * @throws Exception
	 *
	 */
	public void delReceiptLinesByList(String company_id, String receipt_no,
			String lineNosToDel) throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					" update receipt_lines set expected_qty = 0 where  expected_qty > 0 and company_id = '");
			sql.append(company_id).append("' and receipt_no = '")
					.append(receipt_no).append("' and line_no in ( ")
					.append(lineNosToDel).append(" )");
			dbUtilAutoTran.executeSQL(sql.toString());
		} catch (Exception e) {
			throw new Exception("FloorReceiptMgrZzq.delReceiptLinesByList" + e);
		}
	}

	/**
	 * 批量更新rn lines表的(wms_item,wms_lot_no,wms_qty)字段至(item_id,lot_no,qty)
	 * 备注:至少item_id或 lot_no发生变化 TODO 2015年3月5日
	 * 
	 * @param company_id
	 * @param receipt_no
	 * @throws Exception
	 *
	 */
	public void updateReceiptLinesByWMSItemLotInfo(String company_id,
			String receipt_no) throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					" update receipt_lines set item_id = wms_item, lot_no = wms_lot_no, expected_qty = wms_qty where expected_qty > 0 and (wms_item != item_id or wms_lot_no != lot_no ) and company_id = '");
			sql.append(company_id).append("' and receipt_no = '")
					.append(receipt_no).append("' ");
			dbUtilAutoTran.executeSQL(sql.toString());
		} catch (Exception e) {
			throw new Exception("FloorReceiptMgrZzq.updateReceiptLinesByWMSItemLotInfo" + e);
		}
	}

	/**
	 * 批量更新rn lines表的(wms_item,wms_lot_no,wms_qty)字段至(item_id,lot_no,qty)
	 * 备注:item_id或 lot_no未发生变化,只有qty变化 TODO 2015年3月5日
	 * 
	 * @param company_id
	 * @param receipt_no
	 * @throws Exception
	 *
	 */
	public void updateReceiptLinesByWMSInfo(String company_id, String receipt_no)
			throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					" update receipt_lines set item_id = wms_item, lot_no = wms_lot_no, expected_qty = wms_qty where expected_qty > 0 and (wms_item = item_id or wms_lot_no = lot_no ) and wms_qty != expected_qty and company_id = '");
			sql.append(company_id).append("' and receipt_no = '")
					.append(receipt_no).append("' ");
			dbUtilAutoTran.executeSQL(sql.toString());
		} catch (Exception e) {
			throw new Exception("FloorReceiptMgrZzq.updateReceiptLinesByWMSInfo" + e);
		}
	}

	/**
	 * 修改rn状态 2015年3月5日
	 * 
	 * @param company_id
	 * @param receipt_no
	 * @throws Exception
	 *
	 */
	public void updateReceiptStatus(String receipt_id, DBRow upRow)
			throws Exception {
		try {
			dbUtilAutoTran.update("where receipt_id = " + receipt_id,
					"receipts", upRow);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZzq.updateReceiptStatus" + e);
		}
	}
	
	/**
	 *
	 * update Receipt_Lines
	 * 2015年4月9日
	 * @param receipt_line_id
	 * @param upRow
	 * @throws Exception
	 *
	 */
	public void updateReceiptLine(String receipt_line_id, DBRow upRow)
			throws Exception {
		try {
			dbUtilAutoTran.update("where receipt_line_id = " + receipt_line_id,
					"receipt_lines", upRow);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZzq.updateReceiptLine" + e);
		}
	}

	/**
	 * 关闭RN前验证SN 获取RN下所有相同item_id,lot_no的重复sn的信息 2015年3月6日
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] countSNRepeat(DBRow param) throws Exception {
		try {
			StringBuffer sql = new StringBuffer("select * from ( ");
			sql.append(
					"  select sp.serial_product_id, sp.serial_number, c.item_id, c.lot_number, count(sp.serial_number) as counts from receipts r     ")
					.append(" join receipt_lines rl on rl.receipt_id = r.receipt_id ")
					.append(" join receipt_rel_container rrc on  rl.receipt_line_id = rrc.receipt_line_id ")
					.append("  join container c on rrc.plate_no = c.con_id  ")
					.append("  join serial_product sp on sp.at_lp_id = c.con_id      ")
					.append(" where expected_qty > 0  and r.receipt_id = ? ")
					.append(" group by sp.serial_number, c.item_id, c.lot_number  ")
					.append(" ) as snCheck where counts > 1 ");
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.countSNRepeat(DBRow param) error:"
							+ e);
		}
	}

	/**
	 * 清除rn lines表中wms_item = item_id， wms_lot_no = lot_no， wms_qty =
	 * expected_qty记录 的(wms_item,wms_lot_no,wms_qty)字段 2015年3月5日
	 * 
	 * @param company_id
	 * @param receipt_no
	 * @throws Exception
	 *
	 */
	public void updateReceiptLinesWMSInfoByRN(String company_id,
			String receipt_no) throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					" update receipt_lines set wms_item = NULL, wms_lot_no = NULL, wms_qty = NULL  where expected_qty > 0 and wms_item = item_id and wms_lot_no = lot_no and wms_qty = expected_qty and company_id = '");
			sql.append(company_id).append("' and receipt_no = '")
					.append(receipt_no).append("' ");
			dbUtilAutoTran.executeSQL(sql.toString());
		} catch (Exception e) {
			throw new Exception("FloorReceiptMgrZzq.updateReceiptLinesWMSInfoByRN" + e);
		}
	}
	
	//putaway
	public long  addMovement(DBRow row)throws Exception{
		try {
			String table_name ="movements";
			return 	dbUtilAutoTran.insertReturnId(table_name, row);
		} catch (Exception e) {
			throw new Exception("FloorReceiptMgrZzq.addMovement" + e);
		}
	}
	
	public DBRow queryOpenMovement(long pallet_id)throws Exception{
		try {
			StringBuffer sb = new StringBuffer(" select m.*, rl.item_id from movements m  ");
			sb.append(" join receipt_rel_container rrc on rrc.plate_no = m.plate_no ")
			.append(" join receipt_lines rl on rl.receipt_line_id = rrc.receipt_line_id ")
			.append(" where m.status =0 and m.plate_no = ")
			.append(pallet_id);
			return 	dbUtilAutoTran.selectSingle(sb.toString());
		} catch (Exception e) {
			throw new Exception("FloorReceiptMgrZzq.queryOpenMovement" + e);
		}
	}
//	public DBRow[] queryMovement(long pallet_id)throws Exception{
//		try {
//			String sql ="select * from movements where pallet_no = " + pallet_id;
//			return 	dbUtilAutoTran.selectMutliple(sql);
//		} catch (Exception e) {
//			throw new Exception("FloorReceiptMgrZzq.queryMovement" + e);
//		}
//	}
	public void updateMovementStatus(long plate_no, long adid, String date)throws Exception{
		try {
			String sql ="update movements m set m.status=1, date_updated='" + date + "', user_updated=" + adid + " where m.plate_no="+plate_no;
			dbUtilAutoTran.executeSQL(sql);
		} catch (Exception e) {
			throw new Exception("FloorReceiptMgrZzq.updateMovementStatus" + e);
		}
	}
	
	/**
	 *
	 * update Multiple Movement Status
	 * 2015年5月5日
	 * @param plate_nos
	 * @param adid
	 * @param date
	 * @throws Exception
	 *
	 */
	public void updateMultipleMovementStatus(String plate_nos, long adid, String date)throws Exception{
		try {
			String sql ="update movements m set m.status=1, date_updated='" + date + "', user_updated=" + adid + " where m.plate_no in (" + plate_nos + ")";
			dbUtilAutoTran.executeSQL(sql);
		} catch (Exception e) {
			throw new Exception("FloorReceiptMgrZzq.updateMovementStatus" + e);
		}
	}
	
	public DBRow qureyWmsMovementInfo(long pallet_no ) throws Exception{
		try {
			StringBuffer sb = new StringBuffer(" select m.CompanyID as company_id, m.PlateNo as plate_no, m.LocationID as slc_name, m.MovementPrinted as movement_printed, m.PlatePrinted as plate_printed, m.DateCreated as date_created, m.UserCreated as user_created , rl.CompanyID, rl.ReceiptNo, rl.[LineNo], rl.ItemID as item_id ");
			sb.append(" from  Movements m ")
			.append(" join ReceiptLinePlates rlp on rlp.PlateNo = m.PlateNo ")
			.append(" join ReceiptLines rl on rl.CompanyID = rlp.CompanyID and rl.ReceiptNo = rlp.ReceiptNo and rl.[LineNo] = rlp.[LineNo] and rl.CompanyID = m.CompanyID  ")
			.append(" where m.Status='Open' and m.PlateNo = ")
			.append(pallet_no);
			DBRow row =  dbUtilAutoTranSQLServer.selectSingle(sb.toString());
			return row;
		} catch (Exception e) {
			throw new Exception("FloorReceiptMgrZzq.qureyWmsMovementInfo" + e);
		}
	}
	
//	public DBRow[] updateWmsMovement(long pallet_no ) throws Exception{
//		try {
//			String sql ="Update  Movements m set m.Status='Closed' where m.PlateNo="+pallet_no;
//			return dbUtilAutoTranSQLServer.select(sql);
//		} catch (Exception e) {
//			throw new Exception("FloorReceiptMgrZzq.updateWmsMovement" + e);
//		}
//	}
	
	public DBRow queryConIdByPlateNo(long plate_no) throws Exception{
		try {
			String sql ="select con_id from container where container = " + plate_no;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorReceiptMgrZzq.queryConIdByPlateNo" + e);
		}
	}
	
	public DBRow[] queryConIdByPlateNos(String plate_nos) throws Exception{
		try {
			StringBuffer sql = new StringBuffer(" select c.con_id, c.container, rl.company_id, c.status from container c ");
			sql.append(" join receipt_rel_container rrc on rrc.plate_no = c.con_id ")
			.append(" join receipt_lines rl on rl.receipt_line_id = rrc.receipt_line_id ")
			.append(" where c.con_id in (")
			.append(plate_nos)
			.append(")");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception("FloorReceiptMgrZzq.queryConIdByPlateNo" + e);
		}
	}
	
	public DBRow[] queryLocationIdByLocationNameInStaging(long psId, String positionName)
			throws Exception {
		try {
			String sql = "select '2' obj_type, s.id obj_id ,s.location_name obj_name,s.psc_id ps_id ,s.sd_id sd_id,s.x ,s.y, s.height,s.width,s.angle,s.latlng,d.doorId doorId from  "
					+ " storage_load_unload_location s left join "
					+ " storage_door d  on  s.sd_id = d.sd_id "
					+ " where  s.location_name = '"
					+ positionName
					+ "'"
					+ " and s.psc_id = " + psId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq queryLocationIdByLocationNameInStaging error:" + e);
		}
	}
	
	public DBRow[] queryLocationIdByLocationNameInLocation(long psId, String slc_position)
			throws Exception {
		try {
			String sql = "select '1' obj_type,s.slc_id obj_id,s.slc_position obj_name, s.slc_psid ps_id,s.x x,s.y y,s.height height ,s.width width, s.angle angle,s.latlng latlng ,s.is_three_dimensional,d.area_id area_id ,d.area_name area_name from  "
					+ " storage_location_catalog s left join "
					+ " storage_location_area d on  s.slc_area =d.area_id where   s.slc_position = '"
					+ slc_position + "'" + " and s.slc_psid = " + psId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq queryLocationIdByLocationNameInLocation error:" + e);
		}
	}

	/**
	 *
	 * query Container Info By Id
	 * 2015年4月14日
	 * @param con_id
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow queryContainerInfoById(long con_id) throws Exception {
		try {
			StringBuffer sb = new StringBuffer(" select c.location_type, c.location_id, t.title_id, c.con_id, c.container, c.container_type, c.is_full, c.is_has_sn, c.type_id, cc.goods_type as is_damage, ");
			sb.append(" cp.cp_quantity as quantity, rl.lot_no, rl.pc_id, r.receipt_no ")
			.append("from container c ")
			.append(" join receipt_rel_container rrc on rrc.plate_no = c.con_id ")
			.append(" join receipt_lines rl on rl.receipt_line_id = rrc.receipt_line_id ")
			.append(" join receipts r on r.receipt_id = rl.receipt_id ")
			.append(" join container_product cp on cp.cp_lp_id = c.con_id ")
			.append(" join container_config cc on cc.container_config_id = rrc.container_config_id ")
			.append(" join title t on t.title_name = r.supplier_id ")
			.append(" where con_id =  ")
			.append(con_id);
			return this.dbUtilAutoTran.selectSingle(sb.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq queryContainerInfoById error:" + e);
		}
	}
	
	public DBRow queryContainerInfoForInventory(long con_id) throws Exception {
		try {
			StringBuffer sb = new StringBuffer(" select c.title_id, c.con_id, c.container, c.container_type, c.is_full, c.is_has_sn, c.type_id,  ");
			sb.append(" cp.cp_quantity as quantity ")
			.append("from container c ")
			.append(" join container_product cp on cp.cp_lp_id = c.con_id ")
			.append(" where con_id =  ")
			.append(con_id);
			return this.dbUtilAutoTran.selectSingle(sb.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq queryContainerInfoById error:" + e);
		}
	}
	
	/**
	 *
	 * query Product Info By Conatiner Id
	 * 2015年4月14日
	 * @param con_id
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow queryProductInfoByConatinerId(long con_id) throws Exception {
		try {
			StringBuffer sb = new StringBuffer(" select p.pc_id, t.title_id , pc.product_line_id as product_line, p.catalog_id as catalogs, p.p_name, rl.lot_no, rl.item_id as model_number from product p ");
			sb.append(" LEFT join product_catalog pc on pc.id = p.catalog_id ")
			.append(" join receipt_lines rl on rl.pc_id = p.pc_id ")
			.append(" join receipt_rel_container rrc on rrc.receipt_line_id = rl.receipt_line_id ")
			.append(" join container c on c.con_id = rrc.plate_no ")
			.append(" join receipts r on r.receipt_id = rl.receipt_id ")
			.append(" join title t on t.title_name = r.supplier_id ")
			.append(" where c.con_id = ")
			.append(con_id);
			return this.dbUtilAutoTran.selectSingle(sb.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq queryProductInfoByConatinerId error:" + e);
		}
	}
	
	/**
	 *
	 * query Product Info By Pc_Id
	 * 2015年7月13日
	 * @param con_id
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow queryProductInfoByPcId(long pc_id) throws Exception {
		try {
			StringBuffer sb = new StringBuffer(" select p.pc_id, pc.product_line_id as product_line, p.catalog_id as catalogs, p.p_name from product p ");
			sb.append(" LEFT join product_catalog pc on pc.id = p.catalog_id ")
			.append(" where p.pc_id = ")
			.append(pc_id);
			return this.dbUtilAutoTran.selectSingle(sb.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq queryProductInfoByConatinerId error:" + e);
		}
	}
	
	/**
	 *
	 * update Location Info By PlateNos
	 * 2015年4月15日
	 * @param pallets
	 * @param location
	 * @throws Exception
	 *
	 */
	public void updateLocationInfoByPlateNos(String pallets, String location)
			throws Exception {
		try {
			DBRow row = new DBRow();
			row.add("location", location);
			String sql = " where  con_id in ("
					+ pallets + ")";
			dbUtilAutoTran.update(sql, ConfigBean.getStringValue("container"), row);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.updateLocationInfoByPlateNos error:" + e);
		}
	}
	
	/**
	 *
	 * find Containers By Line_Id For Inventory
	 * 2015年4月25日
	 * @param line_id
	 * @param goods_type
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] findContainerByLineIdForInventory(long line_id) throws Exception{
		try{
			StringBuffer sb = new StringBuffer(" SELECT con.con_id, con.location, ");
			sb.append(" case when rrc.normal_qty > 0 then rrc.normal_qty ")
			.append(" ELSE rrc.damage_qty END as qty ")
			.append(" FROM receipt_rel_container rrc ")
			.append(" JOIN container con ON con.con_id = rrc.plate_no ")
			.append(" JOIN container_config cc ON cc.container_config_id=rrc.container_config_id ")
			.append(" WHERE rrc.receipt_line_id = ")
			.append(line_id);
			return dbUtilAutoTran.selectMutliple(sb.toString());
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findContainerByLineIdForInventory error:" + e);
		}
	}
	
	/**
	 *
	 * query Breaked Pallets Info By Line
	 * 2015年5月28日
	 * @param receipt_line_id
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] queryBreakedPalletInfoByLine(Long receipt_line_id) throws Exception {
		try {
			StringBuffer sb = new StringBuffer(" select ccf.cc_from_id, ccf.quantity as break_qty,  c.container as wms_container_id, rrc.normal_qty, c.con_id ");
			sb.append(" from receipt_rel_container rrc  ")
			.append(" JOIN container c on c.con_id = rrc.plate_no ")
			.append(" JOIN cc_from ccf on ccf.container_id = c.con_id and ccf.receipt_line_id = rrc.receipt_line_id ")
			.append(" where rrc.receipt_line_id = ")
			.append(receipt_line_id);
			return this.dbUtilAutoTran.selectMutliple(sb.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryBreakedPalletInfoByLine(Long receipt_line_id) error:"
							+ e);
		}
	}
	
	/**
	 *
	 * query Product By MainCode
	 * 2015年6月2日
	 * @param mainCode
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] queryProductByMainCode(String mainCode) throws Exception {
		try {
			String sql = " select p.* from product p, product_code pc where p.pc_id = pc.pc_id and p_code = '" + mainCode
					+ "'";
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryProductByMainCode(String mainCode) error:"
							+ e);
		}
	}
	
	/**
	 *
	 * query Entry By Con_Ids
	 * 2015年6月10日
	 * @param con_ids
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow queryEntryByConIds(String con_ids) throws Exception {
		try {
			StringBuffer sb = new StringBuffer(" select DISTINCT ee.* from entry_equipment ee ");
			sb.append(" join door_or_location_occupancy_details dolod on dolod.equipment_id = ee.equipment_id ")
			.append(" left join receipt_rel_container rrc on rrc.detail_id = dolod.dlo_detail_id ")
			.append(" join (select DISTINCT receipt_line_id from receipt_rel_container  where plate_no in (")
			.append(con_ids)
			.append(")) as r on r.receipt_line_id = rrc.receipt_line_id ");
			return this.dbUtilAutoTran.selectSingle(sb.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryEntryByConIds(String con_ids) error:"
							+ e);
		}
	}
	
	/**
	 *
	 * query Entry By Line_Id
	 * 2015年6月10日
	 * @param line_id
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow queryEntryByLineId(long line_id) throws Exception {
		try {
			StringBuffer sb = new StringBuffer(" select DISTINCT ee.* from entry_equipment ee ");
			sb.append(" join door_or_location_occupancy_details dolod on dolod.equipment_id = ee.equipment_id ")
			.append(" left join receipt_rel_container rrc on rrc.detail_id = dolod.dlo_detail_id ")
			.append(" where rrc.receipt_line_id = ")
			.append(line_id)
			.append(" group by ee.equipment_id ");
			return this.dbUtilAutoTran.selectSingle(sb.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryEntryByLineId(long line_id) error:"
							+ e);
		}
	}
	
	/**
	 *
	 * check If All Pallets Closed
	 * 2015年6月6日
	 * @param receipt_line_id
	 * @return
	 * @throws Exception
	 *
	 */
	public Boolean checkIfAllPalletsClosed(long receipt_line_id) throws Exception {
		try {
			Boolean result = false;
			StringBuffer sql = new StringBuffer();
			sql.append( " select c.* from container c ")
			.append(" join receipt_rel_container rrc on rrc.plate_no = c.con_id ")
			.append(" where c.status = 1 and c.is_delete = 0 and rrc.receipt_line_id = ")
			.append(receipt_line_id);
			
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			if (row != null && row.size() > 0) {
				result = true;
			}
			return result;
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.checkIfAllPalletsClosed() error:"
							+ e);
		}
	}
	
	
	/**
	 *
	 * query Account By Adid
	 * 2015年6月10日
	 * @param adid
	 * @return
	 * @throws Exception
	 *
	 */
	public String queryAccountByAdid(long adid) throws Exception {
		try {
			String account = "";
			String sql = " select * from admin where adid = " + adid;
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			if (row != null && row.size() > 0) {
				account = row.getString("account");
			}
			return account;
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptMgrZzq.queryAccountByAdid() error:"
							+ e);
		}
	}
	
	/**
     * 查询某个clp type的shipTo group by
     * @param id
     * @throws Exception
     * @author:zyj
     * @date: 2015年6月16日 下午3:02:06
   */
	public DBRow queryLpTitleAndShipToGroupShipTo(long id) throws Exception {
		try{
			
			StringBuffer sql = new StringBuffer();
			
			sql.append("select GROUP_CONCAT(CAST(lts.ship_to_id AS CHAR)) ship_to_ids from lp_title_ship_to lts where lts.lp_type_id =").append(id)
			.append(" and lts.ship_to_id != 0 GROUP BY lts.lp_type_id");
			
			return dbUtilAutoTran.selectSingle(sql.toString());
 		}catch (Exception e) {
 			throw new Exception(
					"FloorReceiptMgrZzq.queryLpTitleAndShipToGroupShipTo() error:"
							+ e);
		}
	}
	
	/**
	 *
	 * find Same SN
	 * 2015年7月6日
	 * @param sn
	 * @param pc_id
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] findSameSN(String sn, long pc_id) throws Exception{
		try {
			String sql = "SELECT sp.serial_product_id FROM serial_product sp "
						+ " WHERE sp.pc_id = " + pc_id + " AND sp.pc_id = " + pc_id ;
			DBRow[] rows = dbUtilAutoTran.selectMutliple(sql);
			return rows;
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrZzq.findSameSN error:" + e);
		}
		
	}
	
	/**
	 *
	 * update Serial Product 
	 * 2015年7月6日
	 * @param sp_ids
	 * @param para
	 * @return
	 * @throws Exception
	 *
	 */
	public long updateSerialProduct(String sp_ids, DBRow para) throws Exception{
		try {
			return dbUtilAutoTran.update("where serial_product_id in (" + sp_ids + ")", "serial_product", para);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrZzq.updateSerialProduct error:" + e);
		}
	}
}