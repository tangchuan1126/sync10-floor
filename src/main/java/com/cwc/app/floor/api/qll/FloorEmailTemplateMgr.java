package com.cwc.app.floor.api.qll;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorEmailTemplateMgr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	/**
	 * 获取具体一条数据
	 * @param tid
	 * @return
	 * @throws Exception
	 */
	public  DBRow  getDetailTemplate(long tid) 
		throws Exception
	{
		try
		{      
			String sql = "select * from "+ ConfigBean.getStringValue("emailtemplate") +" where template_id=?";
			DBRow para = new DBRow();
			para.add("template_id", tid);
			return dbUtilAutoTran.selectPreSingle(sql, para);
			 
		}
		catch (Exception e) 
		{
			throw new Exception ("FloorEmailTemplateMgr.getDetailTemplate(PageCtrl pc) error:"+e);
		}
		
		
	}
	
	public DBRow getDetailTemplateByName(String name) 
		throws Exception
	{
		try
		{      
			String sql = "select * from "+ ConfigBean.getStringValue("emailtemplate") +" where template_name=?";
			
			DBRow para = new DBRow();
			para.add("template_name", name);
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception ("FloorEmailTemplateMgr.getDetailTemplateByName(PageCtrl pc) error:"+e);
		}
	}
	
	public DBRow getDetailTemplateByNameBusines(String name,String busines) 
		throws Exception
	{
		try
		{      
			String sql = "select * from "+ ConfigBean.getStringValue("emailtemplate") +" where template_name=? and receiver=?";
			
			DBRow para = new DBRow();
			para.add("template_name", name);
			para.add("busines", busines);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception ("FloorEmailTemplateMgr.getDetailTemplateByNameBusines(PageCtrl pc) error:"+e);
		}
	}

	/**
	 * 分页显示模板  
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllTemplate(PageCtrl pc) 
		throws Exception
	{
		  try 
		  { 
 			     String sql="select * from "+ConfigBean.getStringValue("emailtemplate")+" order by template_id desc";
			     if(pc != null)
				 {
					 return (dbUtilAutoTran.selectMutliple(sql, pc));
				 }
				 else
				 {
					return (dbUtilAutoTran.selectMutliple(sql));
			     }
		
		   } 
		   catch (Exception e) 
		   {
				throw new Exception("FloorEmailTemplateMgr.getAllTemplate(PageCtrl pc) error:"+e);
		   }
		
	}
	
	/**
	 * 添加数据
	 * @param row
	 * @throws Exception
	 */
	public void addTemplate(DBRow row)
		throws Exception
	{
		long id;
		try 
		{
			id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("emailtemplate"));
			row.add("template_id", id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("emailtemplate"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorEmailTemplateMgr.addTemplate(DBRow row) error:"+e);
		}
		
	}
	
	/**
	 * 删除邮件模板
	 * @param id
	 * @throws Exception
	 */
	public void  delTemplate(long id) 
		throws Exception
	{
		 
		try 
		{
			dbUtilAutoTran.delete("where template_id="+id, ConfigBean.getStringValue("emailtemplate"));
		} 
		catch (Exception e) 
		{
			throw new Exception("delTemplate(aid) error:"+e);
		}
	}
	
	/**
	 * 修改邮件模板
	 * @param id
	 * @param row
	 * @throws Exception
	 */
	public void modTemplate(long id,DBRow row)
		throws Exception
	{
		  
		try
		{
			dbUtilAutoTran.update("where template_id="+id, ConfigBean.getStringValue("emailtemplate"), row);
		} 
		catch (Exception e) 
		{
		    throw new Exception("modTemplate(long id,DBRow row) error:"+e);
		}
	 
	}
	 
	/**
	 * 通过ID  获得邮件模板
	 * @param id
	 * @param receiver
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTemplateById(long id,String receiver,PageCtrl pc) 
		throws Exception
	{
		try 
		{			
			String sql = " select * from "+ConfigBean.getStringValue("emailtemplate")+" , "+ConfigBean.getStringValue("email_scene")
						+" where   email_scene.id=emailtemplate.scene_id  ";
			
			if( receiver !=null && ! receiver.equals(""))
			{
				sql += " and   emailtemplate.receiver= '"+receiver+"'";
			}
			if(id!= 0 )
			{
				sql += " and ( email_scene.id = "+id+"  or email_scene.parentid = '"+id+"')  ";
			}
			
			sql+=" order by template_id desc";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} 
		
		catch (Exception e) 
		{
			throw new Exception("getTemplateById() error:"+e);
		}
		
	}
	
		/**
		 * 过滤查询
		 * @param key
		 * @param pc
		 * @return
		 * @throws Exception
		 */
		public DBRow[] getSearchTemplate(String key, PageCtrl pc) 
			throws Exception 
	{	
		
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("emailtemplate")+" where template_name like '%"+key+"%'  order by template_id ";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("getSearchTemplate(key,pc) error:"+e);
		}
	}
 	
		/**
		 * 获取全部邮件场景
		 * @param key
		 * @param pc
		 * @return
		 * @throws Exception
		 */
		public DBRow[] getEmailScene() 
			throws Exception 
	{
		
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("email_scene");
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("getSearchTemplate(key,pc) error:"+e);
		}
	}


}
