package com.cwc.app.floor.api.zwb;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorAdminMgrZwb {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBRow[] getAdmin(String name,String pwd)throws Exception{
		try{
           String sql="select * from admin where account='"+name+"' and llock=0 and pwd='"+pwd+"'";
           return this.dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("FloorAdminMgrZwb.getAdmin() error:" + e);
		}
	}
	
	public DBRow[] getAdminDetil(long psId)throws Exception{
		try{
           String sql="select * from product_storage_catalog where id="+psId;
           //System.out.println(sql);
           return this.dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("FloorAdminMgrZwb.getAdmin() error:" + e);
		}
	}
	
	//根据用户id查询 该用户下的所有title
	public DBRow[] getProprietaryByadminId(long adminId)throws Exception{
		try{
			String sql="select * from title_admin ta join title t on ta.title_admin_title_id=t.title_id where ta.title_admin_adid="+adminId+" order by t.title_name";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorAdminMgrZwb.getProprietaryByadminId() error:"+e);
		}
	}
	
	//查询登录用户所在分组 是否为客户的分组  
	public DBRow findAdminGroupByAdminId(long adminId)throws Exception{
		try{
			String sql="select * from admin where adid="+adminId+" and adgid=100026";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorAdminMgrZwb findAdminGroupByAdminId()"+e);
		}
	}
	/**
	 * 通过用户ID，部门为客户的用户个数，即查询此用户是否是客户
	 * @param adminId
	 * @return
	 * @throws Exception
	 * zyj
	 */
	public int findAdminByGroupClientAdid(long adminId) throws Exception{
		try{
			String sql="select count(*) adminCount from admin where adid="+adminId+" and adgid=100026";
			return this.dbUtilAutoTran.selectSingle(sql).get("adminCount", 0);
		}catch(Exception e){
			throw new Exception("FloorAdminMgrZwb findAdminByGroupClientAdid(long adminId)"+e);
		}
	}
    
	public DBRow findAdminPs(long adminId)throws Exception{
		try{
			String sql="select * from admin a join product_storage_catalog p on a.ps_id=p.id where a.adid="+adminId;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorAdminMgrZwb findAdminPs"+e);
		}
	}
  
	

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}	
}
