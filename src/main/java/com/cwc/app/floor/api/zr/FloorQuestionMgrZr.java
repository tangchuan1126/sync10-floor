package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorQuestionMgrZr
{

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	 
 
	public DBRow getQuestionById( long question_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("question_q") ;
			String sql = "select * from " + tableName  + " where question_id="+question_id ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorQuestionMgrZr.getQuestionById(knowledage_id):"+e);
		}
	}
	public void deleteQuestionById(long question_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("question_q") ;
			dbUtilAutoTran.delete(" where question_id="+question_id, tableName);
		}catch (Exception e) {
			throw new Exception("FloorQuestionMgrZr.deleteQuestionById(question_id):"+e);
		}
	}
	public void updateQuestionById(long question_id , DBRow updateRow) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("question_q") ;
			dbUtilAutoTran.update(" where question_id="+question_id, tableName, updateRow);
		}catch (Exception e) {
			throw new Exception("FloorQuestionMgrZr.updateKnowledgeById(updateQuestionById,updateRow):"+e);
		}
	}
	public DBRow[] getAllProductCatelogByProductLineId(long product_line_id) throws Exception {
		try{
			String sql = "select * from "+ConfigBean.getStringValue("product_catalog") + " where product_line_id="+product_line_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorQuestionMgrZr.getAllProductCatelogByProductLineId(product_line_id):"+e);
		}
	}
	public int countAllQuestion() throws Exception {
		try{
			String sql = "select count(*) as sum_acount from " + ConfigBean.getStringValue("question_q");
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			return row.get("sum_acount", 0);
		}catch (Exception e) {
			throw new Exception("FloorQuestionMgrZr.countAllQuestion():"+e);
		}
	}
	public DBRow[] getQueryRows(int start , int end ) throws Exception {
		try{
			String sql = "select * from " +  ConfigBean.getStringValue("question_q") + " order by question_id limit " + start + ","+end;
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorQuestionMgrZr.getQueryRows():"+e);
		}
		
		
		
	}
	 
}
