package com.cwc.app.floor.api.gql;


import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.app.util.ConfigBean;

public class FlooCustomSeachConditionsGql {
	private DBUtilAutoTran dbUtilAutoTran;
	


	//添加数据到商品详细模版表
	public long addProductDetailLable(DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("lable_template_product_detail"), row);
		}catch(Exception e){
			throw new Exception("FlooCustomSeachConditionsGql addProductDetailLable"+e);
		}
	}
	
	//查询商品模版详细表
	public DBRow[] getLableDetail(long pc_id,long lable_template_type,long title_id)throws Exception{
		try{
			String sql="select * from "+ConfigBean.getStringValue("lable_template_relation")+" lr " +
					"left join "+ConfigBean.getStringValue("lable_template_product_detail")+" ld " +
					"on lr.lable_template_detail_id=ld.detail_lable_id " +
					"where 1=1";
			if(pc_id>0){
				sql+=" and lr.pc_id="+pc_id;
			}
			if(lable_template_type >0){
				sql+=" and ld.lable_template_type="+lable_template_type;
			}
			if(title_id>0){
				sql+=" and ld.title_id="+title_id;
			}
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FlooCustomSeachConditionsGql getlableDetail"+e);
		}
	}
	
	//添加商品模版关系
	public long addLableTemplateRelation(DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("lable_template_relation"), row);
		}catch(Exception e){
			throw new Exception("FlooCustomSeachConditionsGql addLableTemplateRelation"+e);
		}
	}
	
	//根据 详细模版id 查询是否 有多商品 使用该标签
	public DBRow getCountDetailLableTemplate(long lable_template_detail_id)throws Exception{
		try{
			String sql="select count(*) as count from "+ConfigBean.getStringValue("lable_template_relation")+" where lable_template_detail_id="+lable_template_detail_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FlooCustomSeachConditionsGql getCountDetailLableTemplate"+e);
		}
	}
	
	//删除 商品与模版关系根据关系id
	public long detLableTemplateRelation(long relation_id)throws Exception{
		try{
			return this.dbUtilAutoTran.delete(" where relation_id="+relation_id, ConfigBean.getStringValue("lable_template_relation"));
		}catch(Exception e){
			throw new Exception("FlooCustomSeachConditionsGql detLableTemplateRelation"+e);
		}
	}
	
	//删除商品与模版关系 根据 商品id 和模版id
	public long detLableTemplateRelationByPcId(long pc_id,long lable_template_detail_id)throws Exception{
		try{
			return this.dbUtilAutoTran.delete("where pc_id="+pc_id+" and lable_template_detail_id="+lable_template_detail_id, ConfigBean.getStringValue("lable_template_relation"));
		}catch(Exception e){
			throw new Exception("FlooCustomSeachConditionsGql detLableTemplateRelationByPcId"+e);
		}
	}
	
	//删除详细模版
	public long detLableTemplateDetail(long detail_lable_id)throws Exception{
		try{
			return this.dbUtilAutoTran.delete(" where detail_lable_id="+detail_lable_id, ConfigBean.getStringValue("lable_template_product_detail"));
		}catch(Exception e){
			throw new Exception("FlooCustomSeachConditionsGql detLableTemplateDetail"+e);
		}
	}
	
	/**
	 * 按titleid和label_name批量删除模板关系和详细模板,(puduct_list 页面按title批量删除用)
	 * @author gql 2015-05-21
	 * @param title_id
	 * @param label_name
	 * @return
	 * @throws Exception
	 */
	public void batchDelLable(long title_id, String label_name)throws Exception{
		try{
			StringBuffer sql = new StringBuffer("DELETE lt,ltr FROM lable_template_product_detail lt ")
								.append("LEFT JOIN lable_template_relation ltr ")
								.append("ON lt.detail_lable_id = ltr.lable_template_detail_id ")
								.append("WHERE lt.title_id =").append(title_id)
								.append(" AND lt.lable_name = '").append(label_name).append("'");
			
			this.dbUtilAutoTran.executeSQL(sql.toString());
		}catch(Exception e){
			throw new Exception("FlooCustomSeachConditionsGql batchDelLableTemplateRelation"+e);
		}
	}
	
	
	/**
	 * 根据模版名字查询模版 删除用(puduct_list 页面按title批量删除,查询列表数据)
	 * @author gql 2015-05-21
	 * @param name
	 * @param title
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLableByName(String name,long title)throws Exception{
		try{
			
			String sql="select DISTINCT l.lable_name,l.lable_height,l.lable_template_type,l.lable_type,l.lable_width,l.print_name,l.title_id,t.title_name"
					+ " from "+ConfigBean.getStringValue("lable_template_product_detail")+" l"
					+ " LEFT JOIN title t ON t.title_id = l.title_id"
					+ " where l.lable_name  like '%"+name+"%'"
					+ " AND (l.title_id = "+title+" OR l.title_id = 0)";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FlooCustomSeachConditionsGql getLableByName"+e);
		}
	}
//	public DBRow[] getLableByName(String name)throws Exception{
//		try{
//			String sql="select * from "+ConfigBean.getStringValue("lable_template_product_detail")+" where lable_name  like '%"+name+"%'";
//			return this.dbUtilAutoTran.selectMutliple(sql);
//		}catch(Exception e){
//			throw new Exception("FlooCustomSeachConditionsGql getLableByName"+e);
//		}
//	}
    
	//修改模版详细
	public long updateDetailLableTemplate(long detail_lable_id,DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.update("where detail_lable_id="+detail_lable_id, ConfigBean.getStringValue("lable_template_product_detail"), row);
		}catch(Exception e){
			throw new Exception("FlooCustomSeachConditionsGql updateDetailLableTemplate"+e);
		}
	}
	
	//获得商品的title列表
	public DBRow[] getProductTitleList(long pc_id)throws Exception{
		try{
			String sql="select * from title_product tp left join title t on tp.tp_title_id=t.title_id where tp.tp_pc_id="+pc_id;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FlooCustomSeachConditionsGql getProductTitleList"+e);
		}
		
	}
	
	//根据详细模版id查询 模版信息
	public DBRow getLableTemplateById(long detail_lable_id)throws Exception{
		try{
			String sql="select * from "+ConfigBean.getStringValue("lable_template_product_detail")+" where detail_lable_id="+detail_lable_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FlooCustomSeachConditionsGql getLableTemplateById"+e);
		}
	}
	
	
	//商品基础信息 查根据商品id 登录用户 查询商品的模版
	public DBRow[] findLableByPcIdAndLoginId(long pc_id,long login_id,long lable_template_type,long detail_type)throws Exception{
		try{
			String sql="select p.*, t.*, ltr.*, ltpd.*,pcode_main.p_code AS main_code, "
					+ "pcode_amazon.p_code AS amazon_code,  pcode_upc.p_code AS upc_code "
					+ "from lable_template_relation ltr join lable_template_product_detail "
					+ "ltpd on ltr.lable_template_detail_id = ltpd.detail_lable_id "
					+ "join product p on ltr.pc_id = p.pc_id "
					+ "JOIN product_code AS pcode_main ON p.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1 "
					+ "LEFT JOIN product_code AS pcode_amazon ON p.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3 "
					+ "LEFT JOIN product_code AS pcode_upc ON p.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2 "
					+ "LEFT JOIN title t on ltpd.title_id = t.title_id "
					+ "LEFT JOIN title_product tp on t.title_id = tp.tp_title_id and tp.tp_pc_id = p.pc_id ";
					if(login_id>0){
						sql+=" join title_admin ta on ta.title_admin_title_id = tp.tp_title_id";
					}
					sql+=" where p.pc_id = "+pc_id;
					if(login_id>0){
						sql+=" and ta.title_admin_adid ="+login_id;
					}
					if(lable_template_type>0){
						sql+=" and ltpd.lable_template_type="+lable_template_type;
					}
					if(detail_type>0){
						sql+=" and ltpd.type="+detail_type;
					}
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FlooCustomSeachConditionsGql findLableByPcIdAndLoginId");
		}
	}
	
	
	
	
    
    public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
