package com.cwc.app.floor.api.tjh;



import java.sql.SQLException;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorOfficeLocationMgrTJH {

	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加办公地点
	 * @param row
	 * @return 
	 * @throws Exception 
	 */
	public DBRow addOfficeLocation(DBRow row) 
		throws Exception 
	{
		try 
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("office_location"));
			row.add("id", id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("office_location"), row);
			//row.add(creation_date,DateUtil.NowStr());
		} 
		catch (Exception e) 
		{
			throw new Exception("addOfficeLocation(DBRow row) error: "+e);
		}
		return row;
		
	}
	
	/**
	 * 获取某一办公地点的详细信息
	 * @param parentid
	 * @return
	 * @throws Exception 
	 */
	public DBRow getDetailOfficeLocation(long parentid) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("office_location") +" where id="+parentid;
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailParentOfficeLocation(parentid) error:"+e);
		}
	}

	/**
	 * 修改某一具体的办公地点信息
	 * @param id
	 * @param row
	 * @throws Exception 
	 */
	public void modOfficeLocation(long id, DBRow row) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.update("where id="+id, ConfigBean.getStringValue("office_location"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("modOfficeLocation(id,row) error:"+e);
		}
		
	}
	
	/**
	 * 根据id获取父类下的所有子类信息
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getDetailParentOfficeLocation(long id) 
		throws Exception 
	{
		try 
		{
			String sql = "select id as id,parentid as pId,office_name as name from "+ConfigBean.getStringValue("office_location")+" where parentid="+id+" order by id,sort";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailParentOfficeLocation(id)" + e);
		}
	}
	/*
	 * 办公地点名称重名验证
	 */
	public DBRow[] getOfficeLocationNmae(String office_name,int parentid) 
			throws Exception 
		{
			try 
			{
				String sql = "select * from "+ConfigBean.getStringValue("office_location")+" where parentid="+parentid+" and office_name= '"+office_name+"' ";
				return (dbUtilAutoTran.selectMutliple(sql));
			} 
			catch (Exception e) 
			{
				throw new Exception("getParentOfficeLocationNmae(String office_name) error:"+e);
			}
		}
	
	/**
	 * 
	 * 获取办公地点是否绑定帐号
	 * 
	 * */
	public DBRow[] getAccount(long id) 
			throws Exception 
		{
			try 
			{
				String sql = "select * from "+ConfigBean.getStringValue("admin")+" where office_location="+id+" ";
				return (dbUtilAutoTran.selectMutliple(sql));
			} 
			catch (Exception e) 
			{
				throw new Exception("getParentOfficeLocation() error:"+e);
			}
		}
	

	/**
	 * 删除办公地点信息
	 * @param id
	 * @throws Exception 
	 */
	public void delOfficeLocation(long id) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.delete("where id="+id, ConfigBean.getStringValue("office_location"));
		} 
		catch (Exception e) 
		{
			throw new Exception("delOfficeLocation(id) error:"+e);
		}
		
	}
	/**
	 * 删除办公地点信息
	 * @param id
	 * @throws Exception 
	 */
	public void delOfficeLocationNode(long id) 
			throws Exception 
		{
			try 
			{
				dbUtilAutoTran.delete("where parentid="+id, ConfigBean.getStringValue("office_location"));
			} 
			catch (Exception e) 
			{
				throw new Exception("delOfficeLocationNode(id) error:"+e);
			}
			
		}
	/**
	 * 查询所有的父类办公地点信息
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getParentOfficeLocation() 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("office_location")+" where parentid=0 or parentid=null";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getParentOfficeLocation() error:"+e);
		}
	}

	/**
	 * 获取所有的办公地点信息
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllOfficeLocation() 
		throws Exception 
	{
		try 
		{
			String sql = "select id as id,parentid as pId,office_name as name from "+ConfigBean.getStringValue("office_location");
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllOfficeLocation() error:"+e);
		}
	}
	/**
	 * 根据办公地点名获取办公地点信息
	 * @return
	 * @throws Exception 
	 */
	public DBRow getLocationIdByName(String office_name) throws Exception{
		
		try {
			String sql = "select * from "+ConfigBean.getStringValue("office_location")+" where office_name='"+office_name+"'";
			return (dbUtilAutoTran.selectSingle(sql));
			
		} catch (Exception e) {
			throw new Exception("getLocationIdByName(office_name) error:"+e);
		}
	}
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	

}
