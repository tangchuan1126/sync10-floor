 package com.cwc.app.floor.api.xj;

 

import com.cwc.app.util.Constant;
import com.cwc.app.util.ConstantC;
import com.cwc.app.util.PointD;
import com.cwc.db.DBUtilAutoTran;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;


public class FloorCarsCommandsMgrXJ {
	private DBUtilAutoTran dbUtilAutoTran;
	public Mongo  mongo;
	public DB   db;
	private DBCollection history;
	private DBCollection lastHistory;
	
	 /*
    public FloorCarsCommandsMgrXJ(){  
    	   	mongo=MongodbConn.getMongo();
			db=mongo.getDB(ConstantC.mongodbData);
			db=MongodbConn.auth(db);
	    	history=db.getCollection(Constant.historyName);
	    	lastHistory=db.getCollection(Constant.lasHistoryName); 
		   
	    	DBObject index = new BasicDBObject();  
			// combine name&age as hybrid index  
			 
			index.put("aid", 1);
			index.put("t", 1);
			history.createIndex(index); 
			
	    	  index = new BasicDBObject();  
			// combine name&age as hybrid index  
			//index.put("id", 1);  
			index.put("aid", 1);
			index.put("t", 1);
			lastHistory.createIndex(index); 
    }
    
    
    public String queryHistory(long asset_id, long starttime,long endtime){ 
           DBObject queryCondition = new BasicDBObject();   
           queryCondition.put("aid",  asset_id+""); 
           queryCondition.put("t", new BasicDBObject("$gt", starttime).append("$lte", endtime));  
           DBCursor dbCursor = history.find(queryCondition).sort((DBObject) new BasicDBObject().put("gt", "1")); 
           List<DBObject>  list=new ArrayList<DBObject>();
        while(dbCursor.hasNext()){
        	DBObject db=dbCursor.next();
        	String time=ConstantC.longToTime(Long.parseLong(db.get("gt").toString()));
        	db.put("gt", time);
        	String t=ConstantC.longToTime(Long.parseLong(db.get("t").toString()));
        	db.put("t", t); 
        	db.put("sta",ConstantC.getStatus( db.get("sta").toString()));
        	
        	double lon=Double.parseDouble(db.get("lon").toString());
        	double lat=Double.parseDouble(db.get("lat").toString());
        	PointD p=ConstantC.changeMap(lon, lat);
        	db.put("lon", p.getX());
        	db.put("lat", p.getY());
        	
        	db.removeField("_id");
        	list.add(db);
        }
        String result=null;
		try {
			result = JSONUtil.serialize(list);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
    	return result;
    }
    
    public String getLastPos(String[] ids) {
		DBObject queryCondition = new BasicDBObject();        
        BasicDBList values = new BasicDBList();  
        for(int i=0;i<ids.length;i++){
        	values.add(ids[i]);
        }  
        queryCondition.put("aid", new BasicDBObject("$in", values));  
        
        DBCursor dbCursor = lastHistory.find(queryCondition); 
        List<DBObject>  list=new ArrayList<DBObject>();
        while(dbCursor.hasNext()){
        	DBObject db=dbCursor.next();
        	String time=ConstantC.longToTime(Long.parseLong(db.get("gt").toString()));
        	db.put("gt", time);
        	String t=ConstantC.longToTime(Long.parseLong(db.get("t").toString()));
        	db.put("t", t); 
        	db.put("sta",ConstantC.getStatus( db.get("sta").toString()));
        	
        	double lon=Double.parseDouble(db.get("lon").toString());
        	double lat=Double.parseDouble(db.get("lat").toString());
        	PointD p=ConstantC.changeMap(lon, lat);
        	db.put("lon", p.getX());
        	db.put("lat", p.getY());
        	db.removeField("_id");
        	list.add(db);
        }
        String result=null;
		try {
			result = JSONUtil.serialize(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return result;
	}


	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

*/
	
}
