package com.cwc.app.floor.api;

import us.monoid.json.JSONObject;

import com.cwc.app.beans.log.FilterAccessLogBean;
import com.cwc.app.beans.log.FilterProductStoreAllocateLogBean;
import com.cwc.app.beans.log.FilterProductStoreLogBean;
import com.cwc.app.beans.log.FilterProductStorePhysicalLogBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public interface FloorLogFilterIFace {
	public DBRow[] getSearchProductStoreSysLogs(String start_date,String end_date,FilterProductStoreLogBean filterProductStoreLogBean,int onGroup,int in_or_out,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getSearchProductStoreAllocateLogs(String start_date,String end_date,FilterProductStoreAllocateLogBean filterProductStoreAllocateLogBean,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getSearchProductStorePhysicalLogs(String start_date,String end_date,FilterProductStorePhysicalLogBean filterProductStorePhysicalLogBean,PageCtrl pc)
	throws Exception;
	
	public DBRow[] getSearchAccessLog(String st,String en,FilterAccessLogBean filterAccessLogBean,PageCtrl pc)
	throws Exception;
	
	public DBRow getSearchDetailAccessLog(String uuid)
	throws Exception;
	
	public DBRow getProductStorePhysicalLogLayUpBySN(long system_bill_id,int system_bill_type,String serial_number)
	throws Exception;
	/**
	 * 查询日志
	 * @param modelName		日志记录名称。应取LogServModelKey值。
	 * @param queryJson		筛选条件。
	 * @param pageCtrl		分页
	 * @param start_date	开始日期（包含），UTC时间。格式：yyyy-MM-dd hh:mm:ss 或 yyyy/MM/dd hh:mm:ss。
	 * @param end_date		结束日期（不包含），UTC时间。格式：yyyy-MM-dd hh:mm:ss 或 yyyy/MM/dd hh:mm:ss。
	 * @return
	 * @throws Exception
	 * @author CHENCHEN
	 */
	public DBRow[] queryLogs(String modelName, JSONObject queryJson, PageCtrl pageCtrl, String start_date, String end_date) throws Exception;
	/**
	 * 查询日志
	 * @param modelName		日志记录名称。应取LogServModelKey值。
	 * @param queryJson		筛选条件。
	 * @param pageCtrl		分页
	 * @param start_date	开始日期（包含），UTC时间。格式：yyyy-MM-dd hh:mm:ss 或 yyyy/MM/dd hh:mm:ss。
	 * @param end_date		结束日期（不包含），UTC时间。格式：yyyy-MM-dd hh:mm:ss 或 yyyy/MM/dd hh:mm:ss。
	 * @param onGroup		聚合条件。onGroup==1时，sum(quantity)聚合（pc_id,title_id,lot_number)；否则不聚合。
	 * @param in_or_out		子筛选条件。in_or_out==1时，筛选quantity>0的结果；in_or_out!=1时，筛选quantity<0的结果；in_or_out==null时，不筛选。
	 * @return
	 * @throws Exception
	 * @author CHENCHEN
	 */
	public DBRow[] queryLogs(String modelName, JSONObject queryJson, PageCtrl pageCtrl, String start_date, String end_date, Integer onGroup, Integer in_or_out) throws Exception;
}
