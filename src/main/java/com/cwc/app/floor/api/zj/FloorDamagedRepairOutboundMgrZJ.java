package com.cwc.app.floor.api.zj;

import com.cwc.app.key.DamagedRepairKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorDamagedRepairOutboundMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 增加返修发货
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addDamagedRepairOutbound(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long dro_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("damaged_repair_outbound"));
			
			dbrow.add("dro_id",dro_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("damaged_repair_outbound"),dbrow);
			
			return (dro_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairOutboundMgrZJ addDamagedRepairOutbound error:"+e);
		}
	}
	
	/**
	 * 清空返修发货明细
	 * @param repair_id
	 * @throws Exception
	 */
	public void delDamagedRepairOutbound(long repair_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete(" where dro_repair_id="+repair_id,ConfigBean.getStringValue("damaged_repair_outbound"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairOutboundMgrZJ delDamagedRepairOutbound error:"+e);
		}
	}
	
	/**
	 * 获得返修单比较
	 * @param repair_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] compareDamagedRepairPacking(long repair_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("damaged_repair_outbound_compare_view")+" where real_send_count != repair_count and repair_id=?";
			
			DBRow para = new DBRow();
			para.add("repair_id",repair_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairOutboundMgrZJ compareDamagedRepairPacking error:"+e);
		}
	}
	
	/**
	 * 获得返修发货明细
	 * @param repair_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDamagedRepairOutboundByRepairId(long repair_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("damaged_repair_outbound_compare_view")+" where repair_id=?";
			
			DBRow para = new DBRow();
			para.add("repair_id",repair_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairOutboundMgrZJ getDamagedRepairOutboundByRepairId error:"+e);
		}
	}
	
	

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
