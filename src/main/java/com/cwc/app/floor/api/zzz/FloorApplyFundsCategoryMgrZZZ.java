package com.cwc.app.floor.api.zzz;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorApplyFundsCategoryMgrZZZ 
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	/**
	 * 增加资金申请类别
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addApplyMoneyCategory(DBRow row)
	throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("apply_money_category"));
			row.add("category_id",id);
			
		    dbUtilAutoTran.insert(ConfigBean.getStringValue("apply_money_category"),row);
		    return (id);
		}
		catch (Exception e)
		{
			throw new Exception("FloorApplyFoundsCategoryMgrZZZ.addApplyMoneyCategory(row) error:" + e);
		}
	}
	/**
	 * 获取全部的资金申请父级类别数据
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllApplyMoneyCategoryPrent()
	throws Exception
	{
		try
		{	
			String sql="select * from "+ConfigBean.getStringValue("apply_money_category")+" where prent_id = 0 or prent_id is null";
			return (dbUtilAutoTran.selectMutliple(sql));
		}
		catch(Exception e)
		{
			throw new Exception("FloorApplyFoundsCategoryMgrZZZ.getAllApplyMoneyCategoryPrent()error:"+e);
		}
	}
	/**
	 * 获取所有的子节点的资金申请类型
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllApplyMoneyCategoryChildren(DBRow row)
	throws Exception
	{
		try
		{
			String sql="select * from "+ConfigBean.getStringValue("apply_money_category")+" where prent_id = ? order by category_id";
			return dbUtilAutoTran.selectPreMutliple(sql, row);
		}
		catch(Exception e)
		{
			throw new Exception("FloorApplyFoundsCategoryMgrZZZ.getAllApplyMoneyCategoryChildren()error:"+e);
		}
	}
	/**
	 * 根据id查询资金申请类型
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getApplyMoneyCategoryByCategoryId(DBRow row)
	throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("apply_money_category")+" where category_id=?";	
			return dbUtilAutoTran.selectPreSingle(sql,row);
		}
		catch(Exception e)
		{
			throw new Exception("FloorApplyFoundsCategoryMgrZZZ.getApplyMoneyCategoryByCategoryId(id)error"+e);
		}
		
	}
	public DBRow getApplyMoneyProductLineById(DBRow row)
	throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_line_define")+" where id=?";	
			return dbUtilAutoTran.selectPreSingle(sql,row);
		}
		catch(Exception e)
		{
			throw new Exception("FloorApplyFoundsCategoryMgrZZZ.getApplyMoneyProductLineById(id)error"+e);
		}
	}
	public DBRow getApplyMoneyAccountById(DBRow row)
	throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("account_view")+" where acid=?";	
			return dbUtilAutoTran.selectPreSingle(sql,row);
		}
		catch(Exception e)
		{
			throw new Exception("FloorApplyFoundsCategoryMgrZZZ.getApplyMoneyAccountById(id)error"+e);
		}
	}
	/**
	 * 根据categoryId修改资金申请类型
	 * @param id
	 * @param row
	 * @throws Exception
	 */
	public void updateApplyFoundsCategoryByCategoryId(long id,DBRow row)
	throws Exception
	{
		try
		{			
			dbUtilAutoTran.update("where category_id="+id, ConfigBean.getStringValue("apply_money_category"), row);
			
		}
		catch(Exception e)
		{
			throw new Exception("FloorApplyFoundsCategoryMgrZZZ.updateApplyFoundsCategoryByCategoryId(id)error:"+e);
		}
	}
	/**
	 * 根据categoryId删除资金申请类型
	 * @param id
	 * @throws Exception
	 */
	public void delApplyFoundsCategoryByCategoryId(DBRow row)
	throws Exception
	{
		try
		{
			dbUtilAutoTran.deletePre("where category_id=?",row, ConfigBean.getStringValue("apply_money_category"));
		}
		catch(Exception e)
		{
			throw new Exception("FloorApplyFoundsCategoryMgrZZZ.delApplyFoundsCategoryByCategoryId(long id)error:"+e);
		}
	
	}
	/**
	 * 获取所有的资金申请类型
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllApplyMoneyCategory()
	throws Exception
	{
		try
		{	
			String sql="select * from "+ConfigBean.getStringValue("apply_money_category");
			return (dbUtilAutoTran.selectMutliple(sql));
		}
		catch(Exception e)
		{
			throw new Exception("FloorApplyFoundsCategoryMgrZZZ.getAllApplyMoneyCategory()error:"+e);
		}
	}
	
}
