package com.cwc.app.floor.api;

import java.sql.SQLException;
import java.util.Date;

import com.cwc.app.key.WmsOrderUpdateTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTranSQLServer;

public class FloorWMSOrderSyncMgr {

	private DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer;
	public final static String OSO = "OSO_";
	
	public void setDbUtilAutoTranSQLServer(DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer) {
		this.dbUtilAutoTranSQLServer = dbUtilAutoTranSQLServer;
	}
	
	public int saveOrder(DBRow order) throws Exception {
		//save
		
		String sql = "insert orders ([CompanyID],[OrderNo],[Status],[CustomerID],[AccountID],[OrderedDate],[RequestedDate],[ReferenceNo],[PONo],[StoreID],[StoreName],[StoreAddress1],[StoreAddress2],[StoreCity],[StoreState],[StoreZipCode],[StoreCountry],[StoreContact],[StorePhone],[StoreExtension],[StoreFax],[StoreEmail],[StoreStoreNo],[StoreBatchCode],[StoreHome],[ShipToID],[ShipToName],[ShipToAddress1],[ShipToAddress2],[ShipToCity],[ShipToState],[ShipToZipCode],[ShipToCountry],[ShipToContact],[ShipToPhone],[ShipToExtension],[ShipToFax],[ShipToEmail],[ShipToStoreNo],[ShipToBatchCode],[ShipToHome],[BillToID],[BillToName],[BillToAddress1],[BillToAddress2],[BillToCity],[BillToState],[BillToZipCode],[BillToCountry],[BillToContact],[BillToPhone],[BillToExtension],[BillToFax],[BillToEmail],[BillToStoreNo],[BillToBatchCode],[BillToHome],[FreightTerm],[CarrierID],[WarehouseID],[PickingType],[ScheduledDate],[AppointmentDate],[StagingAreaID],[ProNo],[LoadNo],[ShippedDate],[ETADate],[OrderType],[RetailerOrderType],[DeptNo],[CanceledDate],[VehicleNo],[Seals],[MasterReferenceNo],[DivisionDescription],[ShippingAccountNo],[ShippingZipCode],[DockID],[PalletTypeID],[LinkSequenceNo],[RFControlNo],[SWControlNo],[SHControlNo],[INControlNo],[Note],[BOLNote],[LabelNote],[CartonLabelPrinted],[PalletLabelPrinted],[PickTicketPrinted],[UCCLabelPrinted],[ShippingLabelPrinted],[ReturnLabelPrinted],[TallySheetPrinted],[PackingListPrinted],[PackingSerialNosPrinted],[BillOfLadingPrinted],[EDIOrder],[Send753],[Send945],[Send856],[Send810],[Send214],[Send857],[CustomerInvoiceNo],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated]) values (?,(select ISNULL(Max(orderNo), 0) + 1 from orders WHERE CompanyID =?),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		int n = dbUtilAutoTranSQLServer.insertPre(sql, order, "orders");
		if (n<1) throw new Exception("insert order error");
		return n;
	}
	
	/**
	 * 获取地址信息
	 * 
	 * @param CompanyID
	 * @param CustomerID
	 * @param AccountID
	 * @param Name
	 * @param Address1
	 * @param Address2
	 * @param Contact
	 * @param Phone
	 * @return
	 * @throws SQLException
	 */
	public DBRow getShipToAddressID(String CompanyID, String CustomerID, String AccountID, String Name, String Address1, String Address2, String Contact, String Phone) throws SQLException {
		DBRow para = new DBRow();
		para.add("CompanyID", CompanyID);
		para.add("CustomerID", CustomerID);
		para.add("AccountID", AccountID);
		para.add("Name", Name);
		para.add("Address1", Address1);
		para.add("Address2", Address2);
		para.add("Contact", Contact);
		para.add("Phone", Phone);
		return dbUtilAutoTranSQLServer.selectPreSingle("select top 1 * from CustomerAccountAddresss t where t.CompanyID=? and t.CustomerID=? and t.AccountID=? and t.Name=? and t.Address1=? and t.Address2=? and t.Contact=? and t.Phone=? order by DateCreated desc", para);
	}
	/**
	 * 获取CustomerName
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws SQLException 
	 */
	public String getCustomerName(String CompanyID, String CustomerID) throws SQLException {
		DBRow para = new DBRow();
		para.add("CompanyID", CompanyID);
		para.add("CustomerID", CustomerID);
		DBRow row = dbUtilAutoTranSQLServer.selectPreSingle("select top 1 t.CustomerName [Name] from Customers t where t.CompanyID=? and t.CustomerID=?", para);
		if (row!=null) {
			//return row.get("Name", "");
		}
		return "";
	}
	
	public String getAccountName(String CompanyID, String CustomerID, String AccountID) throws SQLException {
		DBRow para = new DBRow();
		para.add("CompanyID", CompanyID);
		para.add("CustomerID", CustomerID);
		para.add("AccountID", AccountID);
		DBRow row = dbUtilAutoTranSQLServer.selectPreSingle("select top 1 Name from CustomerAccounts t where t.CompanyID=? and t.CustomerID=? and AccountID=?", para);
		if (row!=null) {
			//return row.get("Name", "");
		}
		return "";
	}
	
	/**
	 * 获取最近BILL信息
	 * 
	 * @param CompanyID
	 * @param CustomerID
	 * @param AccountID
	 * @param BillToID
	 * @return
	 * @throws SQLException
	 */
	public DBRow getBillInfo(String CompanyID, String CustomerID, String AccountID, String BillToID) throws SQLException {
		DBRow para = new DBRow();
		para.add("CompanyID", CompanyID);
		para.add("CustomerID", CustomerID);
		para.add("AccountID", AccountID);
		para.add("BillToID", BillToID);
		return dbUtilAutoTranSQLServer.selectPreSingle("select top 1 [BillToID],[BillToName],[BillToAddress1],[BillToAddress2],[BillToCity],[BillToState],[BillToZipCode],[BillToCountry],[BillToContact],[BillToPhone],[BillToExtension],[BillToFax],[BillToEmail],[BillToStoreNo],[BillToBatchCode],[BillToHome] from Orders t where t.CompanyID=? and t.CustomerID=? and t.AccountID=? and t.BillToID=? order by t.orderNo desc", para);
	}
	
	/**
	 * 获取CarrierID
	 * 
	 * @param CompanyID
	 * @param scac
	 * @return
	 * @throws SQLException
	 */
	public String getCarrierId(String CompanyID, String scac) throws SQLException {
		//无参数时 返回空串
		if (CompanyID==null || CompanyID== null || "".equals(CompanyID) || "".equals(scac)) return "";
		DBRow para = new DBRow();
		para.add("CompanyID", CompanyID);
		para.add("SCAC", scac);
		DBRow row = dbUtilAutoTranSQLServer.selectPreSingle("select top 1 CarrierID from Carriers where CompanyId=? and SCACCode=? order by datecreated desc", para);
		if (row!=null) {
			return row.get("CarrierID", "");
		}
		return "";
	}
	
	/**
	 * 修改WMS记录
	 * 
	 * @param order
	 * @return
	 * @throws Exception
	 */
	public int updateOrder(DBRow order) throws Exception {
		//移除无关项
		String CompanyID = order.get("CompanyID","");
		long OrderNo = order.get("OrderNo",0L);
		
		order.remove("CompanyID");
		order.remove("OrderNo");
		
		order.remove("StagingAreaID");
		order.remove("ShippedDate");
		order.remove("ETADate");
		order.remove("RetailerOrderType");
		order.remove("DeptNo");
		order.remove("VehicleNo");
		order.remove("Seals");
		order.remove("MasterReferenceNo");
		order.remove("DivisionDescription");
		order.remove("ShippingAccountNo");
		order.remove("ShippingZipCode");
		order.remove("DockID");
		order.remove("PalletTypeID");
		order.remove("LinkSequenceNo");
		order.remove("RFControlNo");
		order.remove("SWControlNo");
		order.remove("SHControlNo");
		order.remove("INControlNo");
		order.remove("BOLNote");
		order.remove("PickingType");
		order.remove("OrderType");
		order.remove("CanceledDate");
		
		
		order.remove("Status");
		order.remove("CartonLabelPrinted");
		order.remove("PalletLabelPrinted");
		order.remove("PickTicketPrinted");
		order.remove("UCCLabelPrinted");
		order.remove("ShippingLabelPrinted");
		order.remove("ReturnLabelPrinted");
		order.remove("TallySheetPrinted");
		order.remove("PackingListPrinted");
		order.remove("PackingSerialNosPrinted");
		order.remove("BillOfLadingPrinted");
		order.remove("EDIOrder");
		order.remove("Send753");
		order.remove("Send945");
		order.remove("Send856");
		order.remove("Send810");
		order.remove("Send214");
		order.remove("Send857");
		order.remove("CustomerInvoiceNo");
		order.remove("DateCreated");
		order.remove("UserCreated");

		if (order.size()>0) {
			//System.out.println(DBRowUtils.dbRowAsString(order));
			return dbUtilAutoTranSQLServer.updatePre(" where CompanyID='"+CompanyID+"' and OrderNo="+OrderNo, null, "Orders", order);
		}
		return 0;
	}
	
	/**
	 * 保存WMS Order及detail
	 * 
	 * @param order
	 * @param lines
	 * @return
	 * @throws Exception 
	 */
	public long saveOrder(DBRow order, DBRow[] lines) throws Exception {
		long OrderNo = 0;
		
		String CompanyID = "";
		long oid = order.get("LinkSequenceNo", 0L);
		if (oid==0) throw new Exception("no order id");
		/*
		DBRow[] rows =  this.getWMSOrder( order.get("CompanyID", ""), order.get("CustomerID", ""), order.get("AccountID", ""), order.get("ReferenceNo", ""), order.get("PONo", ""), order.get("ShipToID", ""), order.get("ShipToAddress1", ""), "");
		DBRow[] rows1 = this.getWMSOrder( order.get("CompanyID", ""), order.get("CustomerID", ""), order.get("AccountID", ""), order.get("ReferenceNo", ""), order.get("PONo", ""), order.get("ShipToID", ""), order.get("ShipToAddress1", ""), OSO+String.valueOf(oid));
		if (rows1!=null && rows.length>0) {
			DBRow od = getWMSOrder(OSO+String.valueOf(oid));
			CompanyID = od.get("CompanyID", "");
			OrderNo = od.get("OrderNo",0);
			saveOrderLine(oid,""+OrderNo,lines);
		} else {
			if (rows==null || rows.length==0) {
				//保存订单
				order.add("LinkSequenceNo", OSO+ oid);
				saveOrder(order);
				
				//获取OrderNo
				DBRow od = getWMSOrder(OSO+String.valueOf(oid));
				CompanyID = od.get("CompanyID", "");
				OrderNo = od.get("OrderNo",0);
				//清除标记
				cleanWMSOrder(CompanyID, OrderNo);
				//保存明细
				saveOrderLine(oid,""+OrderNo,lines);
			} else {
				//已经存在订单。
				
				if (rows.length==1) {
					OrderNo = rows[0].get("OrderNo",0);
				} else {
					throw new Exception("have more orders");
				}
			}
		}*/
		//保存订单
		order.add("LinkSequenceNo", OSO + oid);
		saveOrder(order);
		//获取OrderNo
		DBRow od = getWMSOrder(OSO+String.valueOf(oid));
		CompanyID = od.get("CompanyID", "");
		OrderNo = od.get("OrderNo",0);
		//清除标记
		cleanWMSOrder(CompanyID, OrderNo);
		
		
		//保存明细
		saveOrderLine(oid,""+OrderNo,lines);
		//服务项
		saveOutServices(CompanyID, od.get("CustomerID", ""), od.get("AccountID", ""), "Outbound", OrderNo);
		
		return Long.parseLong(""+OrderNo);
		
	}
	
	/**
	 * 出库服务项
	 * 
	 * @param CompanyID
	 * @param CustomerID
	 * @param AccountID
	 * @param Type Outbound,Inbound,Storage,Freight
	 * @param OrderNo
	 */
	public void saveOutServices(String CompanyID, String CustomerID, String AccountID, String Type, long OrderNo) {
		if (CompanyID==null|| CustomerID==null || AccountID== null || Type== null || OrderNo==0) return;
		if ("".equals(CompanyID)|| "".equals(CustomerID) || "".equals(AccountID) || "".equals(Type)) return;
		String sql = "INSERT INTO OrderServices ([CompanyID], [OrderNo], [ServiceID], [LineNo], [Unit], [Rate], [Cost], [ItemGrade], [Note], [BillToCustomerID], [InvoiceNo]) select CompanyID,"+OrderNo+" as OID,t.ServiceID,ROW_NUMBER() OVER (ORDER BY ServiceID ) as [LineNo],unit,Rate,cost,ItemGrade,'' note, BillToCustomerID, 0 as InvoiceNo from CustomerAccountServices t where CompanyID ='"+CompanyID+"' and CustomerID='"+CustomerID+"' and Mode ='"+Type+"' and accountid ='"+AccountID+"' and autoInvoice =1 order by ServiceID";
		try {
			dbUtilAutoTranSQLServer.executeSQL(sql);
		} catch (SQLException e) {
		}
	}
	
	/**
	 * 入库服务项
	 * 
	 * @param CompanyID
	 * @param CustomerID
	 * @param OrderNo
	 */
	public void saveInServices(String CompanyID, String CustomerID, long OrderNo) {
		if (CompanyID==null|| CustomerID==null || OrderNo==0 ) return;
		if ("".equals(CompanyID)|| "".equals(CustomerID) ) return;
		String sql = "INSERT INTO ReceiptServices ([CompanyID], [ReceiptNo], [ServiceID], [LineNo], [Period], [GraceDays], [Unit], [Rate], [Cost], [ItemGrade], [Note], [BillToCustomerID], [InvoiceNo]) select [CompanyID], "+OrderNo+" as [ReceiptNo], [ServiceID], ROW_NUMBER() OVER (ORDER BY ServiceID ) as [LineNo], [Period], [GraceDays], [Unit], [Rate], [Cost], [ItemGrade], '' as [Note], [BillToCustomerID], 0 as [InvoiceNo] from CustomerServices t where t.CompanyID ='"+CompanyID+"' and t.CustomerID ='"+CustomerID+"' and autoInvoice = 1 order by ServiceID";
		try {
			dbUtilAutoTranSQLServer.executeSQL(sql);
		} catch (SQLException e) {
		}
	}
	
	public void delOrderLine(String CompanyID, String OrderNo) throws Exception {
		DBRow para = new DBRow();
		para.add("CompanyID", CompanyID);
		para.add("OrderNo", OrderNo);
		dbUtilAutoTranSQLServer.deletePre(" where CompanyID = ? and OrderNo = ? ", para, "OrderLines");
	}
	
	/**
	 * 取消Order，需要将orderLines数量 0
	 * 
	 * @param CompanyID
	 * @param OrderNo
	 * @return
	 * @throws Exception
	 */
	public boolean cancelOrder(String CompanyID, String OrderNo)  throws Exception {
		DBRow para = new DBRow();
		para.add("CompanyID", CompanyID);
		para.add("OrderNo", OrderNo);
		DBRow data = new DBRow();
		data.add("OrderedQty", 0);
		int cnt = dbUtilAutoTranSQLServer.updatePre(" where CompanyID='"+CompanyID+"' and OrderNo="+OrderNo, para, "OrderLines", data);
		if (cnt>0) {
			return true;
		}
		return false;
	}
	
	/**
	 * 删除Order
	 * @param CompanyID
	 * @param OrderNo
	 * @return
	 * @throws Exception
	 */
	public boolean deleteOrder(String CompanyID, String OrderNo)  throws Exception {
		DBRow para = new DBRow();
		para.add("CompanyID", CompanyID);
		para.add("OrderNo", OrderNo);
		int cnt = dbUtilAutoTranSQLServer.deletePre(" where CompanyID=? and OrderNo=?", para, "OrderLines");
		if (cnt>0) {
			dbUtilAutoTranSQLServer.deletePre(" where CompanyID=? and OrderNo=?", para, "OrderServices");
			int cnt2 = dbUtilAutoTranSQLServer.deletePre(" where CompanyID=? and OrderNo=?", para, "Orders");
			if (cnt2>0) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 保存WMS OrderLines
	 *	
	 * @param OrderNo
	 * @param lines
	 * @throws Exception 
	 */
	public void saveOrderLine(long oid, String OrderNo, DBRow[] lines) throws Exception {
		for (int i=0;i<lines.length;i++) {
			//line 数据整理
			lines[i].add("orderNo", OrderNo);
			lines[i].add("LineNo", i+1);
			lines[i].add("ReferenceLineNo", String.format("%06d", i+1));
			int n = dbUtilAutoTranSQLServer.insertPre("insert OrderLines (CompanyID,OrderNo,[LineNo],[ItemID],WarehouseID,ZoneID,BuyerItemID,SupplierID,LotNo,PoundPerPackage,PONo,ReferenceNo,ReferenceLineNo,OrderedQty,Pallets,UnitPrice,PrePackQty,PrePackDescription,CustomerPallets,CommodityDescription,NMFC,FreightClass,PalletWeight,Length,Width,Height,Note) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", lines[i], "OrderLines");
			if (n<1) throw new Exception("insert order line error");
		}
	}
	/**
	 * 获取WMSOrder记录
	 * 
	 * @param oid
	 * @return
	 * @throws SQLException
	 */
	public DBRow getWMSOrder(String oid) throws SQLException {
		DBRow para = new DBRow();
		para.add("LinkSequenceNo", oid);
		DBRow data = dbUtilAutoTranSQLServer.selectPreSingle("select * from Orders where LinkSequenceNo = ?", para);
		if (data!=null && !"".equals(data.get("OrderNo", 0))) {
			return data;
		}
		return new DBRow();
	}
	
	/**
	 * 是否已经存在订单
	 * 
	 * @param CompanyID
	 * @param CustomerID
	 * @param AccountID
	 * @param ReferenceNo
	 * @param PONo
	 * @param ShipToID
	 * @param LinkSequenceNo
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getWMSOrder(String CompanyID, String CustomerID, String AccountID, String ReferenceNo, String PONo, String ShipToID, String ShipToAddress, String LinkSequenceNo) throws SQLException {
		DBRow para = new DBRow();
		para.add("CompanyID", CompanyID);
		para.add("CustomerID", CustomerID);
		para.add("AccountID", AccountID);
		
		para.add("ReferenceNo", ReferenceNo);
		para.add("PONo", PONo);
		
		para.add("ShipToID", ShipToID);
		
		para.add("ShipToAddress1", ShipToAddress);
		
		para.add("LinkSequenceNo", LinkSequenceNo);
		
		return dbUtilAutoTranSQLServer.selectPreMutliple("select * from Orders where CompanyID =? and CustomerID =? and AccountID = ? and ReferenceNo= ? and PONo=? and ShipToID=? and ShipToAddress1=? and LinkSequenceNo=?", para);
	}
	/**
	 * 清除oid标记
	 * 
	 * @param CompanyID
	 * @param OrderNo
	 * @throws Exception
	 */
	public void cleanWMSOrder(String CompanyID, long OrderNo) throws Exception  {
		if ("".equals(CompanyID)|| OrderNo==0) {
			throw new Exception("No CompanyID or OrderNo");
		}
		DBRow para = new DBRow();
		para.add("CompanyID", CompanyID);
		para.add("OrderNo", OrderNo);
		
		DBRow data = new DBRow();
		data.add("LinkSequenceNo", "");
		//data.add("CompanyID", CompanyID);
		//data.add("OrderNo", OrderNo);
		dbUtilAutoTranSQLServer.update(" where CompanyID='"+CompanyID+"' and OrderNo="+OrderNo, "Orders", data);
	}
	
	/**
	 * 回写WMS:更新状态，更新Load,Appointment信息
	 * 
	 * @author Liang Jie
	 * @param row
	 * @param updateType
	 * @throws Exception
	 */
	public void updateOrder(DBRow row, int updateType) throws Exception {
		try
		{
			String companyId = row.getString("CompanyID");
			String orderNo = row.getString("OrderNo");
			long ps_id = row.get("send_psid", 0L);
			DBRow data = new DBRow();
			
			if(updateType == WmsOrderUpdateTypeKey.COMMIT || updateType == WmsOrderUpdateTypeKey.CANCEL){
				data.add("Status", row.get("Status",""));
				//add by wangcr 04/17/2015
				if (updateType == WmsOrderUpdateTypeKey.CANCEL) {
					data.add("CanceledDate", DateUtil.showLocationTime(new Date(), ps_id));
				}
			}else if(updateType == WmsOrderUpdateTypeKey.LOADAPPT){
				data.add("LoadNo", row.getString("LoadNo"));
				//add by wangcr 04/17/2015  加入 carrier
				if (!"".equals(row.getString("CarrierID"))) {
					data.add("CarrierID", this.getCarrierId(companyId, row.getString("CarrierID")));
				} else {
					data.add("CarrierID", "");
				}
				//modify by wangcr 04/17/2015 no appt
				if (!"".equals(row.getString("AppointmentDate"))) {
					data.add("AppointmentDate", row.getString("AppointmentDate"));
				} else {
					data.add("AppointmentDate", null);
				}
			} else if(updateType == WmsOrderUpdateTypeKey.SO) {
				if ("".equals(row.get("ReferenceNo",""))) return;
				//更新同方ReferenceNo
				data.add("ReferenceNo", row.get("ReferenceNo",""));
				dbUtilAutoTranSQLServer.update(" where CompanyID='"+companyId+"' and OrderNo="+orderNo, "Orderlines", data);
			}
			
			dbUtilAutoTranSQLServer.update(" where CompanyID='" + companyId+"' and OrderNo=" + orderNo,"Orders",data);
		}
		catch (Exception e)
		{
			throw new Exception("Modify WMS Order Error:" + e);
		}
	}
	
	public DBRow[] getCustomerItem(String CompanyID, String CustomerID, String ItemID) throws Exception {
		DBRow para = new DBRow();
		para.add("CompanyID", CompanyID);
		para.add("CustomerID", CustomerID);
		para.add("ItemID", ItemID);
		return dbUtilAutoTranSQLServer.selectPreMutliple("select * from CustomerItems where CompanyID =? and CustomerID =? and ItemID =?", para);
	}
	
	/**
	 * 保存RN信息, id:receipt_id
	 * 
	 * @param rn
	 * @param lines
	 * @throws Exception
	 */
	public long saveRN( DBRow rn, DBRow[] lines) throws Exception {
		long oid = rn.get("id", 0L);
		if (oid==0) throw new Exception("no Receipts id");
		saveRNMain(rn, oid);
		
		DBRow od = getWMSReceipts(OSO+String.valueOf(oid));
		//获取生成的 ReceiptNo
		long RNNo = od.get("ReceiptNo", 0L);
		if (RNNo==0)  throw new Exception("no ReceiptNO");
		
		cleanWMSReceipts(od.get("CompanyID", ""), od.get("ReceiptNo", 0L), od.get("ReferenceNo", ""));
		
		saveRNLines(oid, String.valueOf(RNNo), lines, rn.get("customerId", ""));
		saveInServices(rn.get("companyId", ""), rn.get("customerId", ""), RNNo);
		return Long.parseLong(""+RNNo);
	}
	
	public long updateRN( DBRow rn, DBRow[] lines) throws Exception {
		String CompanyID = rn.get("CompanyID", "");
		long ReceiptNo = rn.get("ReceiptNo", 0L);
		
		if ("".equals(CompanyID)|| ReceiptNo==0) {
			throw new Exception("No CompanyID or ReceiptNo");
		}
		
		DBRow data = new DBRow();
		data.add("Status", rn.get("status", ""));
		data.add("CustomerID", rn.get("customerId", ""));
		data.add("SupplierID", rn.get("supplierId", ""));
		
		data.add("EnteredDate", rn.getValue("enteredDate"));
		
		data.add("WarehouseID", rn.get("warehouseId", ""));
		data.add("ReferenceNo", rn.get("referenceNo", ""));
		data.add("PONo", rn.get("poNo", ""));
		data.add("BOLNo", rn.get("bolNo", ""));
		data.add("ContainerNo", rn.get("containerNo", ""));
		data.add("Seals", rn.get("seals", ""));
		data.add("CarrierID", rn.get("carrierId", ""));
		
		data.add("ScheduledDate", rn.getValue("scheduleDate"));
		
		if ("".equals(rn.get("appointmentDate",""))) {
			data.addNull("AppointmentDate");
		} else {
			data.add("AppointmentDate", rn.getValue("appointmentDate"));
		}
		if ("".equals(rn.get("InYardDate",""))) {
			data.addNull("InYardDate");
		} else {
			data.add("InYardDate", rn.getValue("inYardDate"));
		}
		if ("".equals(rn.get("DevannedDate",""))) {
			data.addNull("DevannedDate");
		} else {
			data.add("DevannedDate", rn.getValue("devannerDate"));
		}
		if ("".equals(rn.get("LastFreeDate",""))) {
			data.addNull("LastFreeDate");
		} else {
			data.add("LastFreeDate", rn.getValue("lastFreeDate"));
		}
		data.add("ReceiptType", rn.get("receiptType",""));
		data.add("Note", rn.get("note",""));
		
		data.add("LinkSequenceNo", rn.getString("LinkSequenceNo", ""));
		data.add("REControlNo", rn.get("reControlNo",""));
		data.add("EDIOrder", rn.get("ediOrder",0));
		data.add("Send944", rn.get("send944",0));
		data.add("Send947", rn.get("send947",0));
		
		data.add("DateCreated", rn.getValue("dateCreated"));
		data.add("UserCreated", rn.get("userCreated",""));
		data.add("DateUpdated", rn.getValue("dateUpdated"));
		data.add("UserUpdated", rn.get("userUpdated",""));
		int r = dbUtilAutoTranSQLServer.update(" where CompanyID='"+CompanyID+"' and ReceiptNo="+ReceiptNo, "Receipts", data);
		if (r==1) {
			DBRow para = new DBRow();
			para.add("CompanyID", CompanyID);
			para.add("ReceiptNo", ReceiptNo);
			int cnt = dbUtilAutoTranSQLServer.deletePre(" where CompanyID=? and ReceiptNo=?", para, "ReceiptLines");
			saveRNLines(0L,String.valueOf(ReceiptNo),lines, rn.get("customerId", ""));
			return ReceiptNo;
		} else {
			throw new Exception("update error");
		}
	}
	
	/**
	 * 保存RN主信息
	 * 
	 * @param rnMain
	 * @return 保存是否成功 1
	 * @throws Exception
	 */
	private int saveRNMain(DBRow rnMain, long id) throws Exception {
		String sql = "INSERT INTO Receipts ([CompanyID], [ReceiptNo], [Status], [CustomerID], [SupplierID], [EnteredDate], [WarehouseID], [ReferenceNo], [PONo], [BOLNo], [ContainerNo], [Seals], [CarrierID], "
				+ "[ScheduledDate], [AppointmentDate], [InYardDate], [DevannedDate], [LastFreeDate], [ReceiptType], [Note], [CartonlabelPrinted], [PlatePrinted], [ReceiptTicketPrinted], [TallySheetPrinted], [ReceiptPrinted], [LinkSequenceNo], [REControlNo], [EDIOrder], [Send944], [Send947], [DateCreated], [UserCreated], [DateUpdated], [UserUpdated]) VALUES (?,(select ISNULL(Max(ReceiptNo), 0) + 1 from Receipts WHERE CompanyID =?),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,0,0,0,0,0,?,?,?,?,?,?,?,?,?)";
		
		DBRow data = new DBRow();
		data.add("CompanyID", rnMain.get("companyId", ""));
		data.add("ReceiptNo", rnMain.get("companyId", ""));
		data.add("Status", rnMain.get("status", ""));
		data.add("CustomerID", rnMain.get("customerId", ""));
		data.add("SupplierID", rnMain.get("supplierId", ""));
		
		data.add("EnteredDate", rnMain.get("enteredDate",""));
		
		data.add("WarehouseID", rnMain.get("warehouseId", ""));
		data.add("ReferenceNo", rnMain.get("referenceNo", ""));
		data.add("PONo", rnMain.get("poNo", ""));
		data.add("BOLNo", rnMain.get("bolNo", ""));
		data.add("ContainerNo", rnMain.get("containerNo", ""));
		data.add("Seals", rnMain.get("seals", ""));
		data.add("CarrierID", rnMain.get("carrierId", ""));
		
		data.add("ScheduledDate", rnMain.get("scheduleDate"));
		
		if ("".equals(rnMain.get("appointmentDate",""))) {
			data.addNull("AppointmentDate");
		} else {
			data.add("AppointmentDate", rnMain.getValue("appointmentDate"));
		}
		if ("".equals(rnMain.get("InYardDate",""))) {
			data.addNull("InYardDate");
		} else {
			data.add("InYardDate", rnMain.getValue("inYardDate"));
		}
		if ("".equals(rnMain.get("DevannedDate",""))) {
			data.addNull("DevannedDate");
		} else {
			data.add("DevannedDate", rnMain.getValue("devannerDate"));
		}
		if ("".equals(rnMain.get("LastFreeDate",""))) {
			data.addNull("LastFreeDate");
		} else {
			data.add("LastFreeDate", rnMain.getValue("lastFreeDate"));
		}
		data.add("ReceiptType", rnMain.get("receiptType",""));
		data.add("Note", rnMain.get("note",""));
		
		data.add("LinkSequenceNo", OSO+ id);
		data.add("REControlNo", rnMain.get("reControlNo",""));
		data.add("EDIOrder", rnMain.get("ediOrder",0));
		data.add("Send944", rnMain.get("send944",0));
		data.add("Send947", rnMain.get("send947",0));
		
		data.add("DateCreated", rnMain.getValue("dateCreated"));
		data.add("UserCreated", rnMain.get("userCreated",""));
		data.add("DateUpdated", rnMain.getValue("dateUpdated"));
		data.add("UserUpdated", rnMain.get("userUpdated",""));
		
		int n = dbUtilAutoTranSQLServer.insertPre(sql, data, "Receipts");
		
		if (n<1) throw new Exception("insert Receipts error");
		
		return n;
	}
	
	/**
	 * 按oid 找 RN
	 * 
	 * @param oid
	 * @return
	 * @throws SQLException
	 */
	private DBRow getWMSReceipts(String oid) throws SQLException {
		DBRow para = new DBRow();
		para.add("LinkSequenceNo", oid);
		DBRow data = dbUtilAutoTranSQLServer.selectPreSingle("select * from Receipts where LinkSequenceNo = ?", para);
		if (data!=null && !"".equals(data.get("ReceiptNo", 0))) {
			return data;
		}
		return new DBRow();
	}
	
	/**
	 * 保存RN明细信息
	 * 
	 * @param oid
	 * @param RNNo
	 * @param lines
	 * @throws Exception
	 */
	private void saveRNLines(long oid, String RNNo, DBRow[] lines, String CustomerID) throws Exception {
		if (lines!=null) {
			for (int i=0;i<lines.length;i++) {
				lines[i].add("ReceiptNo", Long.parseLong(RNNo));
				lines[i].add("LineNo", i+1);
				saveRNLine(lines[i], CustomerID);
			}
		}
	}
	
	/**
	 * 保存RN Lines
	 * 
	 * @param line
	 * @return
	 * @throws Exception
	 */
	private int saveRNLine(DBRow line, String CustomerID) throws Exception {
		DBRow RNLine = new DBRow();
		RNLine.add("CompanyID", line.get("companyId",""));
		RNLine.add("ReceiptNo", line.get("receiptNo",0L));
		RNLine.add("LineNo", line.get("lineNo",0));
		RNLine.add("ItemID", line.get("itemId",""));
		RNLine.add("WarehouseID", line.get("warehouseId",""));
		RNLine.add("ZoneID", line.get("zoneId",""));
		RNLine.add("LocationID", line.get("locationId",line.get("warehouseId","")));
		DBRow[] rows = getCustomerItem(line.get("companyId",""), CustomerID, line.get("itemId",""));
		if (rows.length==1) {
			RNLine.add("PalletSizeID", rows[0].get("PalletSizeID",""));
		} else {
			RNLine.add("PalletSizeID", line.get("palletSize",""));
		}
		
		RNLine.add("PONo", line.get("poNo",""));
		RNLine.add("POLineNo", line.get("poLineNo",0));
		if ("".equals(line.get("lotNo",""))) {
			RNLine.add("LotNo", "None");
		} else { 
			RNLine.add("LotNo", line.get("lotNo","None"));
		}
		RNLine.add("ExpectedQty", line.get("expectedQty",0));
		RNLine.add("Pallets", line.get("pallets",0.0f));
		RNLine.add("Note", line.get("note",""));
		int n = dbUtilAutoTranSQLServer.insertPre("INSERT INTO [ReceiptLines] ([CompanyID], [ReceiptNo], [LineNo], [ItemID], [WarehouseID], [ZoneID], [LocationID], [PalletSizeID], [PONo], [POLineNo], [LotNo], [ExpectedQty], [Pallets], [Note]) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", RNLine, "ReceiptLines");
		if (n<1) throw new Exception("insert ReceiptLines error");
		return n;
	}
	
	/**
	 * 清除标记
	 * 
	 * @param CompanyID
	 * @param ReceiptNo
	 * @param LinkSequenceNo
	 * @throws Exception
	 */
	private void cleanWMSReceipts(String CompanyID, long ReceiptNo, String LinkSequenceNo) throws Exception  {
		if ("".equals(CompanyID)|| ReceiptNo==0) {
			throw new Exception("No CompanyID or ReceiptNo");
		}
		DBRow para = new DBRow();
		para.add("CompanyID", CompanyID);
		para.add("ReceiptNo", ReceiptNo);
		
		DBRow data = new DBRow();
		data.add("LinkSequenceNo", LinkSequenceNo);

		dbUtilAutoTranSQLServer.update(" where CompanyID='"+CompanyID+"' and ReceiptNo="+ReceiptNo, "Receipts", data);
	}
	
	/**
	 * 删除RN
	 * 
	 * @param CompanyID
	 * @param RNNo
	 * @return
	 * @throws Exception
	 */
	public boolean deleteRN(String CompanyID, long RNNo)  throws Exception {
		if (CompanyID==null || "".equals(CompanyID)|| RNNo==0) return false;
		DBRow para = new DBRow();
		para.add("CompanyID", CompanyID);
		para.add("ReceiptNo", RNNo);
		
		int cnt = dbUtilAutoTranSQLServer.deletePre(" where CompanyID=? and ReceiptNo=?", para, "ReceiptLines");
		if (cnt>=0) {
			int cnt2 = dbUtilAutoTranSQLServer.deletePre(" where CompanyID=? and ReceiptNo=? and Status in ('Imported','Open')", para, "Receipts");
			if (cnt2>0) {
				dbUtilAutoTranSQLServer.deletePre("where CompanyID=? and ReceiptNo=?", para, "ReceiptServices");
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 取消RN
	 * 
	 * @param CompanyID
	 * @param RNNo
	 * @return
	 * @throws Exception
	 */
	public boolean cancelRN(String CompanyID, long RNNo)  throws Exception {
		if (CompanyID==null || "".equals(CompanyID)|| RNNo==0) return false;
		DBRow para = new DBRow();
		para.add("CompanyID", CompanyID);
		para.add("ReceiptNo", RNNo);
		DBRow data1 = new DBRow();
		data1.add("Status", "Closed");
		//data1.add("CarrierID", "Cancel");
		data1.add("BOLNo", "Cancel");
		
		int m = dbUtilAutoTranSQLServer.update(" where CompanyID='"+CompanyID+"' and ReceiptNo="+RNNo, "Receipts", data1);
		if (m>0) {
			DBRow data = new DBRow();
			data.add("ExpectedQty", 0);
			int cnt = dbUtilAutoTranSQLServer.updatePre(" where CompanyID='"+CompanyID+"' and ReceiptNo="+RNNo, para, "ReceiptLines", data);
			if (cnt>0) return true;
		}
		return false;
	}
}