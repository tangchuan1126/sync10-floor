package com.cwc.app.floor.api.zr;


import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorEbayMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
	public DBRow getEbayTokenBySellerId(String sellerId) throws Exception{
		try{
			String sql = "select * from "+ConfigBean.getStringValue("ebay_token")+" where user_id='"+ sellerId.trim() + "'";
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorBillMgrZr.getEbayTokenBySellerId(sellerId):"+e); 
		}
	}
	public void updateTradeItem(long itemId , DBRow row) throws Exception{
		try{
			 dbUtilAutoTran.update(" where trade_item_id=" +itemId , ConfigBean.getStringValue("trade_item"), row);
		}catch (Exception e) {
			throw new Exception("FloorBillMgrZr.updateTradeItem(itemId, row):"+e); 
		}
	}
	public DBRow[] getAllSellerOnPayPal20() throws Exception{
		try{
			return dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("ebay_token"));
		}catch (Exception e) {
			throw new Exception("FloorBillMgrZr.getAllSellerOnPayPal20():"+e); 
		}
	}
	public long addEbayAccount(DBRow row) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("ebay_token");
			return dbUtilAutoTran.insertReturnId(tableName, row);
		}catch (Exception e) {
			throw new Exception("FloorBillMgrZr.addEbayAccount():"+e); 
		}
	}
	public DBRow[] getAllEbayAccount() throws Exception {
		try{
			 String tableName = ConfigBean.getStringValue("ebay_token");
			 return dbUtilAutoTran.selectMutliple("select * from " + tableName );
		}catch (Exception e) {
			throw new Exception("FloorBillMgrZr.getAllEbayAccount():"+e); 
		}
	}
	public void updateEbayAccount(long ebay_id , DBRow row) throws Exception {
		try{
			 String tableName = ConfigBean.getStringValue("ebay_token");
			 dbUtilAutoTran.update(" where id="+ebay_id, tableName, row);
		}catch (Exception e) {
			throw new Exception("FloorBillMgrZr.updateEbayAccount():"+e); 
		}
	}
	public DBRow getEbayAccountById(long ebay_account_id) throws Exception {
		try{
			 String tableName = ConfigBean.getStringValue("ebay_token");
			return dbUtilAutoTran.selectSingle("select * from " +tableName + " where id =" + ebay_account_id);
		}catch (Exception e) {
			throw new Exception("FloorBillMgrZr.getEbayAccountById():"+e); 
		}
	}
	 
}
