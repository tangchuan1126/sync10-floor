package com.cwc.app.floor.api.zyj.service;

/**
 * 该接口定义了查询登录用户信息的方法
 * @author lujintao
 *
 */
public interface AdminService {
	/**
	 * 查询出登录用户所对应的customerId
	 * @param userId
	 * @return
	 */
	public int getCustomerId(int userId);
	
	
}
