package com.cwc.app.floor.api.fa.country;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;
import com.cwc.db.PageCtrl;
/**
 * 省份信息
 * table country_province
 * @author liyi
 *
 */
public class FloorProvinceMgrLiy {
	private DBUtilAutoTran_Optm dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	/**
	 * 根据国家，获取所有省份
	 * @param ccid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllProvinceByProvinceId(long ccid,PageCtrl pc) throws Exception {
		try {
			String sql = "select * from "
					+ ConfigBean.getStringValue("country_province")
					+ " where nation_id=? order by pro_name asc";
			DBRow para = new DBRow();
			para.add("nation_id", ccid);
			return (dbUtilAutoTran.selectPreMutliple(sql, para,pc));
		} catch (Exception e) {
			throw new Exception(
					"FloorProvinceMgr.getAllProvinceByProvinceId(long ccid,PageCtrl pc) error:"
							+ e);
		}
	}
	/**
	 * 新增省份
	 * @param row
	 * @throws Exception
	 */
	public long insertProvince(DBRow row) throws Exception {
		try{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("country_province"), row);
		} catch (Exception e) {
			throw new Exception(
					"FloorProvinceMgr.insertProvince(DBRow row) error:"
							+ e);
		}
	}
	/**
	 * 获得省份
	 * @param proId
	 * @return
	 * @throws Exception
	 */
	public DBRow getProvinceById(long proId) throws Exception {
		try{
			String sql = "select * from "+ ConfigBean.getStringValue("country_province")+" where pro_id="+proId;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorProvinceMgr.getProvinceById(long proid) error:"
							+ e);
		}
	}
	
	/**
	 * 更新省份信息
	 * @param proId
	 * @param row
	 * @throws Exception
	 */
	public int updateProvince(long proId,DBRow row) throws Exception {
		try{
			return dbUtilAutoTran.update("where pro_id="+proId,ConfigBean.getStringValue("country_province"), row);
		} catch (Exception e) {
			throw new Exception(
					"FloorProvinceMgr.updateProvince(long proId,DBRow row) error:"
							+ e);
		}
	}
	
	
	/**
	 * 删除省份
	 * @param proId
	 * @throws Exception
	 */
	public int deltetProvinceById(long proId) throws Exception {
		try{
			return dbUtilAutoTran.delete("where pro_id="+proId, ConfigBean.getStringValue("country_province"));
		} catch (Exception e) {
			throw new Exception(
					"FloorProvinceMgr.deltetProvinceById(long proId) error:"
							+ e);
		}
	}
	/**
	 * 根据某个字段单独检查
	 * @param cond
	 * @return
	 * @throws Exception
	 */
	public boolean checkProvinceField(String condition,long ccid) throws Exception {
		try{
			StringBuilder sql = new StringBuilder("select count(pro_id) cnt from "+ConfigBean.getStringValue("country_province") + " where nation_id="+ccid);
			sql.append(condition);
			if(dbUtilAutoTran.selectMutliple(sql.toString())[0].get("cnt",0l)>0) {
				return true;
			} 
			return false;
		}catch(Exception e) {
			throw new Exception("FloorProvinceMgr.checkProvinceField(String condition,long ccid) "+e.getMessage());
		}
	}
	
	
	/**
	 * 检查省份是否重复
	 * @param proId
	 * @param para
	 * @return
	 * @throws Exception
	 */
	public boolean[] checkProvince(long proId,DBRow para,long ccid) throws Exception {
		try{
			boolean[] isExist = new boolean[2];
			boolean pro_Name_isExist=false,p_code_isExist=false;
			DBRow province = new DBRow();
			if(proId>0) { //修改的时候 判断某个字段没有进行修改，就不需要验证
				province = getProvinceById(proId);
			}
			if (para.get("pro_name")!=null && !"".equals(para.getString("pro_name"))) {
				if(!province.getString("pro_name").equals(para.getString("pro_name"))) {
					pro_Name_isExist = checkProvinceField(" and pro_name='"+para.getString("pro_name")+"' ",ccid);	
				}
			}
			if (para.get("p_code")!=null && !"".equals(para.getString("p_code"))) {
				if(!province.getString("p_code").equals(para.getString("p_code"))) {
					p_code_isExist = checkProvinceField(" and p_code='"+para.getString("p_code")+"' ",ccid);	
				}
			}
			isExist[0] = p_code_isExist;
			isExist[1] = pro_Name_isExist;
					
			return isExist;
			
		}catch(Exception e) {
			throw new Exception("FloorProvinceMgr.checkProvince(long proId,DBRow para,long ccid) "+e.getMessage());
		}
	}
	

}
