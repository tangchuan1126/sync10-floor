package com.cwc.app.floor.api.qll;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorPaypalPaymentMgr {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 获取数据库中的paypal 账号信息用于初始化paypal API
	 * @return
	 * @throws Exception
	 */
	public DBRow getAccountInfo()
		throws Exception 
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("paypal_account")+" where id= 1  ";
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAccountInfo() error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public void addPaymentByCreadCardAndCustomerExecute(DBRow row) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("vertualterminal_hist") ;
			long id = dbUtilAutoTran.getSequance(tableName);
			row.add("id", id);
			dbUtilAutoTran.insert(tableName, row);
		}catch (Exception e) {
			throw new Exception("addPaymentByCreadCardAndCustomerExecute(row) error:"+e);
		}
	}
}
