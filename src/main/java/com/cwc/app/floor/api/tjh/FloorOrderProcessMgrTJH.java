package com.cwc.app.floor.api.tjh;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorOrderProcessMgrTJH {
	
	private DBUtilAutoTran dbUtilAutoTran;

	
	/**
	 * 查询所有的订单批处理信息
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllOrderProcess(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("actual_storage_sales")+" order by delivery_date desc";
			if(pc != null)
			{
				return (dbUtilAutoTran.selectMutliple(sql, pc));
			}
			else
			{
				return (dbUtilAutoTran.selectMutliple(sql));
			}
			
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllOrderProcess() error:"+e);
		}
	}
	
	/**
	 * 根据id获取商品的名称
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProductNameById(long id) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product")+" where pc_id="+id;
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProductNameById(long id) error:"+e);
		}
	}

	/**
	 * 查询实际仓库产品需求分布情况
	 * @param end_date 
	 * @param cid 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOrdersProduct(String start_date, String end_date, long cid) 
		throws Exception 
	{
			try 
			{
				StringBuffer sql = new StringBuffer("select p.ccid,ov.ps_id,ov.p_pid,ov.unit_price,ov.catalog_id,p.pro_id,sum(ov.p_quantity) as quantity,p.delivery_date from "+ConfigBean.getStringValue("porder")+" p "
			     		+"inner join "+ConfigBean.getStringValue("order_all_products_4sell_view")+" ov on p.delivery_date between '"+start_date+" 00:00:00' and '"+end_date+" 23:59:59' "
			     		+"and ov.oid=p.oid");
				if(cid != 0)
				{
				     sql.append("and p.ps_id = "+cid);
				}
				
				sql.append(" group by p.delivery_date,ov.p_pid,p.ccid,p.pro_id,ov.ps_id");
				
				return (dbUtilAutoTran.selectMutliple(sql.toString()));
			} 
			catch (Exception e) 
			{
				throw new Exception("getOrdersProduct(String startDate, String endDate, long cid) error:"+e);
			}
		
	}


	/**
	 * 把数据添加到实际货物需求表中
	 * @param row
	 * @throws Exception
	 */
	public void statcProcuctNum(DBRow row) 
		throws Exception 
	{
		try 
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("actual_storage_sales"));
			row.add("op_id", id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("actual_storage_sales"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("statcProcuctNum(DBRow row) error:"+e);
		}
		
	}

	/**
	 * 统计实际产品需求分布前先删除前一天的数据
	 * @param end_date 
	 * @param id 
	 * @throws Exception 
	 */
	public void deleteProductSpread(String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.delete(" where delivery_date between '"+start_date+" 00:00:00' and '"+end_date+" 23:59:59'", ConfigBean.getStringValue("actual_storage_sales"));
		} 
		catch (Exception e) 
		{
			throw new Exception("deleteProductSpread() error:"+e);
		}
		
	}

	

	/**
	 * 根据id查询商品类别最顶层名称
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProductCategoryById(long id) 
		throws Exception 
	{
		try 
		{
			//String sql = "select title from "+ConfigBean.getStringValue("product_catalog")+" where id in(select parentid from "+ConfigBean.getStringValue("product_catalog")+" where id in(select parentid from "+ConfigBean.getStringValue("product_catalog")+" where id="+id+" or parentid=0) or parentid=0)";
			String sql = "select p.title,p.id from "+ConfigBean.getStringValue("product_catalog")+" p,"+ConfigBean.getStringValue("product_catalog")+" c1,product_catalog c2 where p.id = c1.parentid and c1.id = c2.parentid and c2.id="+id;
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProductCategoryById(long id) error:"+e);
		}
	}
	
	/**
	 * 根据商品类别id获取父类的信息
	 * @param category_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getProductCategory(long category_id) 
		throws Exception 
	{
		try 
		{
			String sql = "select p.title,p.id from "+ConfigBean.getStringValue("product_catalog")+" p,"+ConfigBean.getStringValue("product_catalog")+" c1 where p.id = c1.parentid and c1.id ="+category_id;
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProductCategory(long category_id) error:"+e);
		}
	}

	/**
	 * 删除统计理论仓库需要货物前一天的数据
	 * @param id
	 * @throws Exception
	 */
	public void delSpreadTheoryProduct(String start_date,String end_date) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.delete("where delivery_date between '"+start_date+" 00:00:00' and '"+end_date+" 23:59:59'", ConfigBean.getStringValue("theory_storage_sales"));
		} 
		catch (Exception e) 
		{
			throw new Exception("delSpreadTheoryProduct(String date) error:"+e);
		}
		
	}

	/**
	 * 分页查询所有的理论仓库需要的货物需求信息
	 * @param pc 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllTheoryStorageForProduct(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("theory_storage_sales")+" order by delivery_date desc";
			if(pc != null)
			{
				return (dbUtilAutoTran.selectMutliple(sql, pc));
			}
			else
			{
				return (dbUtilAutoTran.selectMutliple(sql));
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllTheoryStorageForProduct() error" +e);
		}
	}
	
	/**
	 * 根据优先发货仓库和商品计算出理论仓库需要的货物需求
	 * @param end_date 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] statsTheoryStorageForProduct(String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = "select sum(total_quantity) as quantity,product_id,prior_storage_id,unit_price,catalog_id,delivery_date from "+ConfigBean.getStringValue("actual_storage_sales")+" " +
					"where delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by product_id,prior_storage_id,delivery_date";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("statsTheoryStorageForProduct() error:" +e);
		}
	}

	

	/**
	 * 将统计一段时间的理论仓库需要的货物需求添加到数据库
	 * @param row
	 * @throws Exception 
	 */
	public void addTheoryStorageQuantityByProduct(DBRow row) 
		throws Exception 
	{
		try 
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("theory_storage_sales"));
			row.add("theory_id", id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("theory_storage_sales"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("addTheoryStorageQuantityByProduct(DBRow row) error:"+e);
		}
	}

	
	/**
	 * 分页查询计划仓库的货物需信息
	 * @param pc
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllPlanStorageSales(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("plan_storage_sales")+" order by delivery_date desc,plan_id desc";
			if(pc != null)
			{
				return (dbUtilAutoTran.selectMutliple(sql, pc));
			}
			else
			{
				return (dbUtilAutoTran.selectMutliple(sql));
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllPlanStorageSales(PageCtrl pc) error："+e);
		}
	}
	
	/**
	 * 删除计划仓库中货物需求的数据
	 * @param end_date 
	 * @param string
	 * @throws Exception 
	 */
	public void deleteOriginalPlanStorageSales(String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.delete("where delivery_date between '"+start_date+" 00:00:00' and '"+end_date+" 23:59:59'", ConfigBean.getStringValue("original_storage_sales"));
		} 
		catch (Exception e) 
		{
			throw new Exception("deletePlanStorageSales(String date) error："+e);
		}
		
	}
	

	/**
	 * 统计代发仓库后的本来仓库的货物需求（查询产品是否需要代发）
	 * @param storage 
	 * @param product_id 
	 * @return
	 * @throws Exception 
	 */
	public DBRow statsOriginalStorageSales(long storage, long product_id,String deliveryDate) 
		throws Exception 
	{
		try 
		{
			String sql = "select t.product_id,t.unit_price,t.quantity,s.d_sid,t.catalog_id,s.modality_id,s.y_sid,t.delivery_date from "+ConfigBean.getStringValue("theory_storage_sales")+"" +
					" t,"+ConfigBean.getStringValue("storage_transpond")+" s where " +"t.catalog_id in (select id from "+ConfigBean.getStringValue("product_catalog")
					+" where find_in_set(id,product_catalog_child_list((select id from "+ConfigBean.getStringValue("product_catalog")+" where id=s.catalog_id)))) " +
											"and t.storage_id = s.y_sid and t.storage_id="+storage+" and t.product_id = "+product_id+" and t.delivery_date='"+deliveryDate+"'";
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e)
		{
			throw new Exception("statsOriginalStorageSales() error："+e);
		}
	}

	/**
	 * 将统计后的代发的计划仓库所需的货物需求保存到数据库
	 * @param row
	 * @throws Exception 
	 */
	public void addOriginalStorageSales(DBRow row) 
		throws Exception 
	{
		try 
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("original_storage_sales"));
			row.add("original_id", id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("original_storage_sales"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("addPlanStorageSales(DBRow row) error："+e);
		}
		
	}
	
	/**
	 * 查询实际仓库的货物需求
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getActualStorageSales() 
		throws Exception 
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("actual_storage_sales");
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getActualStorageSales() error："+e);
		}
	}

	/**
	 * 获取所有的理论仓库的货物需求
	 * @param end_date 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllTheoryStorageSales(String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("theory_storage_sales")+" where delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59'";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllTheoryStorageSales() error："+e);
		}
	}

	
	/**
	 * 获取仓库代发后计划仓库中的货物需求
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPlanStorageSalesByProduct() 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("original_storage_sales");
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getPlanStorageSalesByProduct() error："+e);
		}
	}

	/**
	 * 获取计划仓库的货物需求
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPlanStorageSales() 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("plan_storage_sales");
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getPlanStorageSales() error："+e);
		}
		
	}

	/**
	 * 删除计划仓库的货物需求
	 * @param end_date 
	 * @param p_id
	 * @throws Exception
	 */
	public void deletePlanStorageSales(String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.delete("where delivery_date between '"+start_date+" 00:00:00' and '"+end_date+" 23:59:59'", ConfigBean.getStringValue("plan_storage_sales"));
		} 
		catch (Exception e) 
		{
			throw new Exception("deletePlanStorageSales(long p_id) error："+e);
		}
		
	}

	/**
	 * 统计计划仓库的货物需求
	 * @param end_date 
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] statsPlanStorageProductSales(String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = "select sum(quantity) as quantity,catalog_id,product_id,d_storage_id,unit_price,delivery_date from "+ConfigBean.getStringValue("original_storage_sales") +" where delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by product_id,d_storage_id,left(delivery_date,10)";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("statsPlanStorageProductSales() error:"+e);
		}
	}

	/**
	 * 保存计划仓库所需货物数据到数据库
	 * @param row
	 * @throws Exception
	 */
	public void addPlanStorageSales(DBRow rows) 
		throws Exception 
	{
		try 
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("plan_storage_sales"));
			rows.add("plan_id", id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("plan_storage_sales"), rows);
			
		} 
		catch (Exception e) 
		{
			throw new Exception("addPlanStorageSales(DBRow row) error："+e);
		}
		
	}

	/**
	 * 分页查询产品代发后本来仓库所需的产品需求
	 * @param pc
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllOriginalStorageSales(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("original_storage_sales") +" order by delivery_date desc";
			if(pc != null)
			{
				return (dbUtilAutoTran.selectMutliple(sql, pc));
			}
			else
			{
				return (dbUtilAutoTran.selectMutliple(sql));
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllOriginalStorageSales(PageCtrl pc) error："+e);
		}
		
	}

	/**
	 * 查询全球各国的产品需求总金额
	 * @param country_id 
	 * @param catalog_id 
	 * @param end_date 
	 * @param st_date 
	 * @param p_line_id 
	 * @return
	 * @throws Exception
	 */
	public DBRow getAllProductNumByCatalogId(long country_id, String catalog_id, String st_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = "select sum(total_quantity) as total_quantity,nation_id from "+ConfigBean.getStringValue("actual_storage_sales") +" where catalog_id in("+catalog_id+") and nation_id = "+country_id+" and delivery_date between '"+st_date+" 0:00:00' and '"+end_date+" 23:59:59' group by nation_id";
			//sql = "select sum(unit_price) as price,nation_id from "+ConfigBean.getStringValue("actual_storage_sales") +" where nation_id = "+country_id+" and delivery_date between '"+st_date+" 0:00:00' and '"+end_date+" 23:59:59' group by nation_id";
				
			return (dbUtilAutoTran.selectSingle(sql));			
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllProductNumByCatalogId(long country_id, String catalog_id, String st_date, String end_date) error：" + e);
		}
	}
	

	/**
	 * 根据fusionMaps的ID查询出对应的国家的信息
	 * @param fusion_id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getCountryByfusion_id(String fusion_id) 
		throws Exception 
	{
		try 
		{
			long fid = Long.parseLong(fusion_id);
			if(fid < 10)
			{
				fusion_id = "0"+fid;
			}
			
			String sql = "select * from "+ConfigBean.getStringValue("country_code")+" where fusion_id='"+fusion_id+"'";
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getCountryByfusion_id(String fusion_id) error："+e);
		}
	}


	/**
	 * 查询某一段时间内各国家以及该国家的区域查询产品需求总额
	 * @param pro_id
	 * @param catalog 
	 * @param end_date 
	 * @param start_date 
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProvinceProductNeedSpreadByCatalogId(long pro_id, String catalog, String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
				String sql = "select sum(total_quantity) as total_quantity,province_id from "+ConfigBean.getStringValue("actual_storage_sales")+" where catalog_id in("+catalog+") and province_id = "+pro_id+" and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
				return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProvinceProductNeedSpreadByCatalogId(long pro_id, String catalog, String start_date, String end_date) error："+e);
		}
	}
	
	
	/**
	 * 查询全世界各国以及该国家的省份以及州的产品需求分布情况
	 * @param fusion_id
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getFusionMapsBySalesSpreadJson(String fusion_id) 
		throws Exception 
	{
		try 
		{
			if(fusion_id.equals("0"))
			{
				String sql = "select sum(ass.unit_price) as price,cc.fusion_id from "+ConfigBean.getStringValue("actual_storage_sales") +" ass,"+ConfigBean.getStringValue("country_code")+" cc where ass.nation_id=cc.ccid  group by ass.nation_id";
				return (dbUtilAutoTran.selectMutliple(sql));
			}
			else
			{
				Integer id = Integer.parseInt(fusion_id);
				if(id < 10)
				{
					fusion_id = "0"+id;
				}
				String sql = "select sum(ass.unit_price) as price,cp.fusion_id from "+ConfigBean.getStringValue("actual_storage_sales")+" ass,"+ConfigBean.getStringValue("country_province")+" cp where ass.nation_id=(select ccid from "+ConfigBean.getStringValue("country_code")+" where fusion_id='"+fusion_id+"')  and ass.province_id=cp.pro_id group by ass.nation_id,ass.province_id;";
				return (dbUtilAutoTran.selectMutliple(sql));
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("getFusionMapsBySalesSpreadJson(String fusion_id) error："+e);
		}
	}
	
	/**
	 * 根据产品名称获取产品的信息
	 * @param p_name
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProductIdByName(String p_name) 
		throws Exception 
	{
		try 
		{
			DBRow param = new DBRow();
			String sql = "select pc_id from "+ConfigBean.getStringValue("product")+" where p_name = ? and orignal_pc_id = 0";
			param.add("p_name", p_name);
			return (dbUtilAutoTran.selectPreSingle(sql, param));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProductIdByName(String p_name) error："+e);
		}
	}
	
	/**
	 * 根据商品id查看实际的产品需求的总金额的分布情况
	 * @param nation_id
	 * @param pid
	 * @param end_date 
	 * @param st_date 
	 * @return
	 * @throws Exception 
	 */
	public DBRow statsProductSpreadMapsByProductId(long nation_id, long pid, String st_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = "select sum(total_quantity) as total_quantity,nation_id from "+ConfigBean.getStringValue("actual_storage_sales")+" where product_id = ? and nation_id = ? and delivery_date between '"+st_date+" 0:00:00' and '"+end_date+" 23:59:59' group by nation_id";
			
			DBRow param = new DBRow();
			param.add("product_id", pid);
			param.add("nation_id", nation_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, param));
		} 
		catch (Exception e) 
		{
			throw new Exception("statsProductSpreadMapsByProductId(long nation_id, long pid) error："+e);
		}
	}

	/**
	 * 根据商品查看该产品在各个国家的各省份或各州的实际需求的金额的分布情况
	 * @param province_id
	 * @param product_id
	 * @param end_date 
	 * @param start_date 
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProductSpreadByProductId(long province_id, long product_id, String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = "select sum(total_quantity) as total_quantity,province_id from "+ConfigBean.getStringValue("actual_storage_sales")+" where product_id = ? and province_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
			
			DBRow param = new DBRow();
			param.add("product_id", product_id);
			param.add("province_id", province_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, param));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProductSpreadByProductId(long province_id, long product_id) error："+e);
		}
	}
	


	/**
	 * 获取某一时间段内的所有的计划仓库的数据
	 * @param start_date
	 * @param end_date
	 * @param cid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPlanStorageSales(String start_date, String end_date,long cid) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("plan_storage_sales")+" where delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' ";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getPlanStorageSales(String start_date, String end_date,long cid) error："+e);
		}
	}

	/**
	 * 查询某一时间段内实际仓库和计划仓库的货物需求量的情况
	 * @param pc
	 * @param start_date
	 * @param end_date
	 * @param catalog_id
	 * @param cmd 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStoragesProductQuantityByDate(PageCtrl pc, String start_date, String end_date, String cmd) 
		throws Exception 
	{
		try 
		{
			String sql = null;
			if(cmd.equals("actual"))
			{
				sql = "select * from "+ConfigBean.getStringValue("actual_storage_sales")+" where delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' order by delivery_date desc";
			}
			else if(cmd.equals("plan"))
			{
				sql = "select * from "+ConfigBean.getStringValue("plan_storage_sales")+" where delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' order by delivery_date desc";
			}
			
			return (dbUtilAutoTran.selectMutliple(sql, pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("getStoragesProductQuantityByDate(PageCtrl pc, String start_date, String end_date) error:"+e);
		}
	}
	
	/**
	 * 产品类别过滤查询某一时间段内仓库的货物需求量的情况
	 * @param pc
	 * @param start_date
	 * @param end_date
	 * @param catalog_id
	 * @param cmd 
	 * @return
	 * @throws Exception
	 */
	
	public DBRow[] getStoragesProductQuantityByCatalogIds(long catalog_id,long product_line_id,String start_date, String end_date,PageCtrl pc,String cmd,long ccid,long pro_id) 
		throws Exception 
	{
	try 
	{	
		String table = "";
		if(cmd.equals("actual"))
		{
			table = ConfigBean.getStringValue("actual_storage_sales");
			
		}
		else if(cmd.equals("plan"))
		{
			table = ConfigBean.getStringValue("plan_storage_sales");
		}
		
		DBRow para = new DBRow();
		
		String cid = "";
		if(catalog_id>0)
		{
			cid = "and search_rootid=? ";
			para.add("search_rootid",catalog_id);
		}
		
		String productline="";
		if(product_line_id>0)
		{
			productline = " and pc.product_line_id=? ";
			para.add("prodcut_line_id",product_line_id);
		}
		
		para.add("1",1);
		
		String nation = "";
		if(ccid>0)
		{
			nation = " and nation_id=? ";
			para.add("nation_id",ccid);
		}
		
		String province = "";
		if(pro_id>0)
		{
			province = " and province_id=?  ";
			para.add("province_id",pro_id);
		}
		
		String sql = "select * from "+table+" as t ";
		
		if(!cid.equals(""))
		{
			sql += "join pc_child_list as pcl on t.catalog_id = pcl.pc_id "+cid;
					
		}
		
		if(!productline.equals(""))
		{
			sql += "join product_catalog as pc on pcl.pc_id = pc.id "+productline;
		}
					
			sql +="where 1=? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' "+nation+province
					+"order by delivery_date desc";
		
		return (dbUtilAutoTran.selectPreMutliple(sql,para,pc));
	} 
	catch (Exception e) 
	{
		throw new Exception("getStoragesProductQuantityByCatalogIds(PageCtrl pc, String start_date, String end_date, String catalog_ids) error:"+e);
	}
}
	
	
	/**
	 * 根据商品名称过滤查询某一时间段内实际仓库的货物需求量的情况
	 * @param pc
	 * @param start_date
	 * @param end_date
	 * @param product_id
	 * @param cmd 
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getActualOrPlanStorageProductQuantityByProductId(PageCtrl pc,String start_date, String end_date, long product_id, String cmd,long ccid,long pro_id) 
		throws Exception 
	{
		try 
		{
			StringBuffer sql = new StringBuffer("");
			if(cmd.equals("actual"))
			{
				sql.append("select * from "+ConfigBean.getStringValue("actual_storage_sales")+" where delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' and product_id = ?");
				
				if(ccid>0)
				{
					sql.append(" and nation_id="+ccid);
				}
				
				if(pro_id>0)
				{
					sql.append(" and province_id="+pro_id);
				}
			}
			else if(cmd.equals("plan"))
			{
				sql.append("select * from "+ConfigBean.getStringValue("plan_storage_sales")+" where delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' and product_id = ?");
			}
			
			sql.append(" order by delivery_date desc");
			
			DBRow param = new DBRow();
			param.add("product_id", product_id);
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(), param, pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("getActualStorageProductQuantityByProductId(PageCtrl pc,String start_date, String end_date, long product_id) error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	/**
	 * 
	 * @param pc
	 * @param start_date
	 * @param end_date
	 * @param c_id
	 * @param cmd
	 * @param product_lines
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getStorageProductQuantityByLineIds(long product_line_id,String start_date, String end_date,String cmd,PageCtrl pc,long ccid,long pro_id) 
		throws Exception 
	{
		try 
		{
			String table = "";
			
			if(cmd.equals("actual"))
			{
				table = ConfigBean.getStringValue("actual_storage_sales");
				
			}
			else if(cmd.equals("plan"))
			{
				table = ConfigBean.getStringValue("plan_storage_sales");
			}
			
			String nation = "";
			if(ccid>0)
			{
				nation = " and nation_id="+ccid+" ";
			}
			
			String province = "";
			if(pro_id>0)
			{
				province = " and province_id="+pro_id+" ";
			}
			
			String sql = "select * from "+table+" as t "
						+"join "+ConfigBean.getStringValue("product_catalog")+" as pc on t.catalog_id = pc.id and pc.product_line_id = ? "
						+"where delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' "+nation+province
						+"order by delivery_date desc";
			
			DBRow para = new DBRow();
			para.add("product_line_id",product_line_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("getStorageProductQuantityByLineIds(PageCtrl pc,String start_date, String end_date, String cmd,String product_line_ids) error:"+e);
		}
		
		
	}

	/**
	 * 根据产品线查询某一时间段内某一个国家下的某一区域的实际的产品需求总额
	 * @param province_id
	 * @param pro_line_id
	 * @param start_date
	 * @param end_date
	 * @return
	 * @throws Exception
	 */
	public DBRow getAllProvinceSpreadProductNeedByLineId(long province_id,String pro_line_id, String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = "select sum(total_quantity) as total_quantity,province_id from "+ConfigBean.getStringValue("actual_storage_sales")+" " +
					"where catalog_id in("+pro_line_id+") " +
							"and province_id = "+province_id+" and delivery_date between '"+start_date+" 0:00:00' " +
									"and '"+end_date+" 23:59:59' group by province_id";
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllProvinceSpreadProductNeedByLineId(long province_id,String pro_line_id, String start_date, String end_date) error:"+e);
		}
	}

	/**
	 * 根据产品线查询某一时间段内某一个国家的实际的产品需求总额
	 * @param country_id
	 * @param pro_line_id
	 * @param st_date
	 * @param end_date
	 * @return
	 * @throws Exception 
	 */
	public DBRow getAllCountryProductNeedSpreadByLineIds(long country_id,String pro_line_ids, String st_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql= "select sum(total_quantity) as total_quantity,nation_id from "+ConfigBean.getStringValue("actual_storage_sales") +" " +
					"where catalog_id in("+pro_line_ids+") and nation_id = "+country_id+" and delivery_date between '"+st_date+" 0:00:00' and '"+end_date+" 23:59:59' group by nation_id";
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllCountryProductNeedSpreadByLineIds(long country_id,String pro_line_ids, String st_date, String end_date) error:"+e);
		}
	}

	/**
	 * 获取产品类别以及该类下所有的子集的结果集
	 * @param catalog_id
	 * @return
	 */
	public DBRow getCatalogResultSet(long catalog_id) 
		throws Exception
	{
		try 
		{
			String sql = "select product_catalog_child_list('"+catalog_id+"') as catalog_ids";
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getCatalogResultSet(long catalog_id) error:"+e);
		}
	}

	/**
	 * 根据产品线id获取产品线以及该类下所有的子集的结果集
	 * @param p_line_id
	 * @return
	 */
	public DBRow getProductLineIds(long p_line_id) 
		throws Exception
	{
		try 
		{
			String sql = "select product_line_child_list('"+p_line_id+"') as line_ids";
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getCatalogResultSet(long catalog_id) error:"+e);
		}
	}

	/**
	 * 根据时间段查询实际的发货仓库中所需要的产品在各国家的数量分布
	 * @param country_id
	 * @param st_date
	 * @param end_date
	 * @return
	 * @throws Exception 
	 */
	public DBRow getAllCountryProductNeedNumByDate(long country_id, String st_date,String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = "select sum(total_quantity) as total_quantity,nation_id from "+ConfigBean.getStringValue("actual_storage_sales") +" where nation_id = ? and delivery_date between '"+st_date+" 0:00:00' and '"+end_date+" 23:59:59' group by nation_id";
			DBRow parRow = new DBRow();
			parRow.add("nation_id", country_id);
			return (dbUtilAutoTran.selectPreSingle(sql, parRow));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllCountryProductNeedNumByDate(long country_id, String st_date,String end_date) error:"+e);
		}
	}

	/**
	 * 根据时间段查询各个省份中实际仓库所需要的产品数量
	 * @param province_id
	 * @param start_date
	 * @param end_date
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProvinceProductNeedSpreadByDate(long province_id, String start_date,String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = "select sum(total_quantity) as total_quantity,province_id from "+ConfigBean.getStringValue("actual_storage_sales")+" where  province_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
			DBRow param = new DBRow();
			param.add("province_id", province_id);
			return (dbUtilAutoTran.selectPreSingle(sql, param));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProvinceProductNeedSpreadByDate(long province_id, String start_date,String end_date) error:"+e);
		}
	}
	
	public DBRow[]  getDate(String startDate,String endDate)
		throws Exception 
	{
	
		//缺少后面天大于 等于前面天的判断
		 long  DAY = 24L * 60L * 60L * 1000L;   
		 Date dateTemp = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
	     Date dateTemp2 = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
		 long n =  (dateTemp2.getTime() - dateTemp.getTime() ) / DAY   ;
		 int num = Long.valueOf(n).intValue();
		 DBRow[] row = new DBRow[num+1];
		 Calendar calendarTemp = Calendar.getInstance();
		 calendarTemp.setTime(dateTemp);
	     int i= 0;
	     while (calendarTemp.getTime().getTime()!= dateTemp2.getTime())
	     {
	            String temp = new SimpleDateFormat("yyyy-MM-dd").format(calendarTemp.getTime()) ;
	            row[i] = new DBRow();
	            row[i].add("date",temp );
	            calendarTemp.add(Calendar.DAY_OF_YEAR, 1);
	            i++;
	     }
	     row[i] = new DBRow();
	     row[i].add("date", new SimpleDateFormat("yyyy-MM-dd").format(dateTemp2.getTime()) );
		 return row;
	}
	
	/**
	 * 产品需求
	 * @param start_date
	 * @param end_date
	 * @param catalog_id
	 * @param pro_line_id
	 * @param pc_id
	 * @param ca_id
	 * @param ccid
	 * @param pro_id
	 * @param type
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] productDemandAnalysisNew(String start_date,String end_date,long catalog_id,long pro_line_id,long pc_id,long ca_id,long ccid,long pro_id,String order_source,int type,PageCtrl pc)
		throws Exception
	{
		String onGroup = "";
		
//		String whereProductLine = "";
//		if(pro_line_id>0&&!(catalog_id>0))
//		{
//			whereProductLine = " join "+ConfigBean.getStringValue("product_catalog")+" as pc on pc.id = p.catalog_id"
//							  +" and pc.product_line_id = "+pro_line_id+" ";
//		}
//		else if(pro_line_id>0&&catalog_id>0)
//		{
//			whereProductLine = " and pc.product_line_id = "+pro_line_id+" ";
//		}
//		
//		String whereProductCatalog = "";
//		if(catalog_id>0)//需要根据分类过滤
//		{
//			whereProductCatalog = " join "+ConfigBean.getStringValue("product_catalog")+" as pc on pc.id = p.catalog_id "+whereProductLine//尝试新方式产品线与商品分类在需要时再join
//								 +" join pc_child_list as pcl on pcl.pc_id = pc.id and pcl.search_rootid = "+catalog_id+" ";
//		}
//		else//不需要根据分类过滤，但有可能需要根据产品线过滤
//		{
//			whereProductCatalog += whereProductLine;
//		}
		
		String whereProductLine = "";
		if(pro_line_id>0)
		{
			whereProductLine = " and pc.product_line_id = "+pro_line_id+" ";
		}
		
		String whereCatalog = "";
		if(catalog_id>0)
		{
			whereCatalog = " join pc_child_list as pcl on pcl.pc_id = p.catalog_id and pcl.search_rootid = "+catalog_id+" ";
			
		}
		
		String whereProdcut = "";
		if(pc_id>0)
		{
			whereProdcut = " and p.pc_id = "+pc_id+" ";
		}
		
		String whereOrderSource = "";
		if(order_source!=null&&!order_source.trim().equals(""))
		{
			whereOrderSource = " and o.order_source='"+order_source+"' ";
		}
		
		String whereArea = "";
		if(ca_id>0)
		{
			whereArea = " and ca.ca_id = "+ca_id+" ";	
		}
		else
		{
			if (type==2)
			{
				onGroup = ",ca_id";
			}
		}
		
		String whereCountry = "";
		if(ccid>0)
		{
			whereCountry = " and po.ccid = "+ccid+" ";
		}
		else
		{
			if(type==2&&ca_id>0)
			{
				onGroup = ",ccid";
			}
		}
		
		String whereProvince = "";
		if(pro_id>0)
		{
			whereProvince = " and po.pro_id = "+pro_id+" ";
		}
		else
		{
			if(type==2&&ccid>0)
			{
				onGroup = ",pro_id";
			}
		}
		DBRow[] dateStr =getDate(start_date,end_date);
		
		String sql ="select pc_id,p_name,p_code,pl_name,title,sum(quantity) as sum_quantity,ca_id,ccid,pro_id,area_name,c_country,pro_name,prefer_ps_id,plan_ps_id,modality, ";
		
		for(int i=0; i<dateStr.length; i++)
		{
			sql +="  SUM(CASE post_date  WHEN '"+dateStr[i].getString("date")+"' THEN quantity ELSE 0  END)  date_"+i;
			if(i!=(dateStr.length-1))
			{
				sql +=" , ";
			}
		}
		
			   sql+=" from ("
				   	+"select p.pc_id,p.p_name,pcode.p_code,pld.`name` as pl_name,pc.title,sum(pi.quantity) as quantity,ca.ca_id,po.ccid,po.pro_id,ca.`name` as area_name,cc.c_country,cp.pro_name,pi.prefer_ps_id,pi.plan_ps_id,pi.modality,left(post_date,10)as post_date from porder_item as pi "
					+"join porder as po on pi.oid = po.oid and (pi.product_type=1 or pi.product_type=2) "
					+"join product as p on pi.pid = p.pc_id "+whereProdcut
					+"join product_code as pcode on p.pc_id = pcode.pc_id and code_type="+CodeTypeKey.Main+" "
					+"join product_catalog as pc on p.catalog_id = pc.id "+whereProductLine
					+whereCatalog
					+"left join product_line_define as pld on pld.id = pc.product_line_id "
					+"join country_area_mapping as cam on cam.ccid = po.ccid "
					+"join country_area as ca on ca.ca_id = cam.ca_id "+whereArea
					+"join country_code as cc on cc.ccid = cam.ccid "+whereCountry
					+"left join country_province as cp on cp.pro_id = po.pro_id " 
					+"where po.post_date>'"+start_date+" 0:00:00' and po.post_date<'"+end_date+" 23:59:59' "+whereOrderSource+whereProvince
					+"group by p.pc_id,post_date "
					+"union all "
					+"select p.pc_id,p.p_name,pcode.p_code,pld.`name` as pl_name,pc.title,sum(pi.quantity*pu.quantity) as quantity,ca.ca_id,po.ccid,po.pro_id,ca.`name` as area_name,cc.c_country,cp.pro_name,pi.prefer_ps_id,pi.plan_ps_id,pi.modality,left(post_date,10)as post_date from porder_item as pi "
					+"join porder as po on pi.oid = po.oid "
					+"join product_union as pu on pu.set_pid = pi.pid and pi.product_type = 4 "
					+"join product as p on p.pc_id = pu.pid "+whereProdcut
					+"join product_code as pcode on p.pc_id = pcode.pc_id and code_type="+CodeTypeKey.Main+" "
					+"join product_catalog as pc on p.catalog_id = pc.id "+whereProductLine
					+whereCatalog
					+"left join product_line_define as pld on pld.id = pc.product_line_id "
					+"join country_area_mapping as cam on cam.ccid = po.ccid " 
					+"join country_area as ca on ca.ca_id = cam.ca_id " 
					+"join country_code as cc on cc.ccid = cam.ccid " 
					+"left join country_province as cp on cp.pro_id = po.pro_id " 
					+"where po.post_date>'"+start_date+" 0:00:00' and po.post_date<'"+end_date+" 23:59:59' "+whereOrderSource+whereProvince
					+"group by p.pc_id,post_date ";
			   sql+=") as customer_needs group by pc_id"+onGroup;
			   
		return dbUtilAutoTran.selectMutliple(sql,pc);
	}
	
	public DBRow[] productDemandAnalysis(String start_date,String end_date,long catalog_id,long pro_line_id,long pc_id,long ca_id,long ccid,long pro_id,int type,PageCtrl pc)
		throws Exception
	{
		//type 1代表求和计算，group by以选中的地域层级  2代表分组求和group by 选中级别的下一级别
		String innerGroupBy = "";//构成基础数据group by条件
		String formGroupBy = "";//分日期统计group by条件
		
		
		String whereProductLine = "";
		if(pro_line_id>0)
		{
			whereProductLine = " and pc.product_line_id = "+pro_line_id+" ";
		}
		
		String whereCatalog = "";
		if(catalog_id>0)
		{
			whereCatalog = " join pc_child_list as pcl on pcl.pc_id = ass.catalog_id and pcl.search_rootid = "+catalog_id+" ";
			
		}
		
		String whereProduct = "";
		if(pc_id>0)
		{
			whereProduct = " and ass.product_id = "+pc_id+" ";
		}
		
		String whereArea = "";
		if(ca_id>0)
		{
			whereArea = " and ca.ca_id = "+ca_id+" ";	
		}
		else
		{
			if(type == 2)//求和时销售区域已经自动求和
			{
				innerGroupBy = ",ca.ca_id ";
				formGroupBy = ",ca_id ";
			}
		}
		
		String whereCountry = "";
		if(ccid>0)
		{
			whereCountry = " and ass.nation_id = "+ccid+" ";
		}
		else
		{
			if(type==2&&ca_id>0)
			{
				innerGroupBy = ",ass.nation_id ";
				formGroupBy = ",nation_id ";
			}
		}
		
		String whereProvince = "";
		if(pro_id>0)
		{
			whereProvince = " and ass.province_id = "+pro_id+" ";
		}
		else
		{
			if(type==2&&ccid>0)
			{
				innerGroupBy = ",ass.province_id ";
				formGroupBy = ",province_id ";
			}
		}
		
		DBRow[] dateStr =getDate(start_date,end_date);
		String sql = " SELECT product_id,p_name,pl_name,title,area_name,c_country,pro_name,sum(total_quantity) as sum_total_quantity,";
		
		for(int i=0; i<dateStr.length; i++)
		{
			sql +="  SUM(CASE delivery_date  WHEN '"+dateStr[i].getString("date")+"' THEN total_quantity ELSE 0  END)  date_"+i;
			if(i!=(dateStr.length-1))
			{
				sql +=" , ";
			}
		}
		
		sql +=" FROM (";
		
		sql +="select p.pc_id as product_id,p.p_name,pld.`name` as pl_name,pc.title,ca.ca_id,ca.`name` as area_name,ass.nation_id,cc.c_country,ass.province_id,cp.pro_name,sum(ass.total_quantity) as total_quantity,left(delivery_date,10) as delivery_date from actual_storage_sales as ass "
			 +"join product_catalog as pc on ass.catalog_id = pc.id "+whereProductLine
			 +whereCatalog
			 +"join product as p on p.pc_id = ass.product_id "+whereProduct
			 +"join country_area_mapping as cam on cam.ccid = ass.nation_id "+whereCountry
			 +"join country_area as ca on ca.ca_id = cam.ca_id "+whereArea
			 +"join country_code as cc on cc.ccid = cam.ccid "
			 +"left join country_province as cp on cp.pro_id = ass.province_id "+whereProvince//使用左联，有省份为-1的情况，-1代表Other
			 +"left join product_line_define as pld on pld.id = pc.product_line_id "//使用做左联，商品分类有可能未划分产品线
			 +"where ass.delivery_date BETWEEN '"+start_date+" 0:00:00' AND '"+end_date+" 23:59:59' "
			 +"GROUP BY ass.product_id,delivery_date "+innerGroupBy
			 +"ORDER BY p_name";
		sql +=" ) as basedata GROUP BY product_id "+formGroupBy;
		sql +=" ORDER BY pl_name,title,p_name ";

		return dbUtilAutoTran.selectMutliple(sql,pc);
	}
}
