package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorInvoiceMgrZJ 
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加模板商品描述
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addRoundInvoice(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("round_invoice"),dbrow);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorInvoiceMgrZJ addRoundInvoice error:"+e);
		}
	}
	
	/**
	 * 根据发票模板ID获得发票明细
	 * @param invoice_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailInvoiceTemplate(long invoice_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("invoice_template")+" where invoice_id = ?";
			
			DBRow para = new DBRow();
			para.add("invoice_id",invoice_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorInvoiceMgrZJ getDetailInvoiceTemplate error:"+e);
		}
	}
	
	/**
	 * 根据仓库ID获得仓库下所有发件人信息
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllDelivererInfoByPsid(long ps_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("ps_id", ps_id);
			
			String sql = "SELECT *  FROM " + ConfigBean.getStringValue("deliverer_info") + " where ps_id=?";				
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorInvoiceMgrZJ getAllDelivererInfoByPsid error:" + e);
		}
	}
	
	/**
	 * 修改发票模板
	 * @param invoice_id
	 * @param para
	 * @throws Exception
	 */
	public void modInvoice(long invoice_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update(" where invoice_id="+invoice_id,ConfigBean.getStringValue("invoice_template"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorInvoiceMgrZJ modInvoice error:" + e);
		}
	}
	
	/**
	 * 根据di_id获得发件人
	 * @param di_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailDelivererInfo(long di_id)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("deliverer_info") + " where di_id=?";
			
			DBRow para = new DBRow();
			para.add("di_id",di_id);
			
			return(dbUtilAutoTran.selectPreSingle(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorInvoiceMgrZJ getDetailInvoiceTemplate error:" + e);
		}
	}
	
	/**
	 * 根据模板ID获得模板下所有轮循发票信息
	 * @param invoice_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllRoundInvoiceByInvoiceId(long invoice_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("round_invoice")+" where invoice_template_id = ?";
			
			DBRow para = new DBRow();
			para.add("invoice_template_id",invoice_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorInvoiceMgrZJ getAllRoundInvoiceByInvoiceId error:"+e);
		}
	}
	
	/**
	 * 根据模板样式ID删除模板样式
	 * @param round_invoice_id
	 * @throws Exception
	 */
	public void delRoundInvoiceById(long round_invoice_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete(" where round_invoice_id="+round_invoice_id,ConfigBean.getStringValue("round_invoice"));
		} catch (Exception e) 
		{
			throw new Exception("FloorInvoiceMgrZJ delRoundInvoiceById error:"+e);
		}
	}
	
	/**
	 * 根据Id获得发票模板样式
	 * @param round_invoice_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailRoundInvoiceById(long round_invoice_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("round_invoice")+" where round_invoice_id=?";
			
			DBRow para = new DBRow();
			para.add("round_invoice_id",round_invoice_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorInvoiceMgrZJ getDetailRoundInvoiceById error:"+e);
		}
	}
	
	/**
	 * 修改发票样式
	 * @param round_invoice_id
	 * @param para
	 * @throws Exception
	 */
	public void modRoundInvoiceById(long round_invoice_id,DBRow para)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where round_invoice_id="+round_invoice_id,ConfigBean.getStringValue("round_invoice"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorInvoiceMgrZJ modRoundInvoiceById error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
