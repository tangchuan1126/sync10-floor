package com.cwc.app.floor.api;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.*;

public class FloorExpressMgr
{
	private DBUtilAutoTran dbUtilAutoTran;

	public DBRow[] getAllExpressCompany(PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			String sql = "select * from " + ConfigBean.getStringValue("shipping_company") + " order by sc_id desc";
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectMutliple(sql,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectMutliple(sql);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getAllExpressCompany() error:" + e);
		}
	}

	public DBRow[] getAllInternationalExpressCompany(PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			String sql = "select * from " + ConfigBean.getStringValue("shipping_company") + " where domestic=0 order by sc_id desc";

			if (pc!=null)
			{ 
				row = dbUtilAutoTran.selectMutliple(sql,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectMutliple(sql);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getAllInternationalExpressCompany() error:" + e);
		}
	}
	
	public DBRow[] getAllDomesticExpressCompanyByScId(long sc_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select sc.* from " + ConfigBean.getStringValue("shipping_company") + " sc," + ConfigBean.getStringValue("product_storage_catalog") + " psc where sc.ps_id=psc.id and psc.native=(select psc.native from " + ConfigBean.getStringValue("shipping_company") + " sc," + ConfigBean.getStringValue("product_storage_catalog") + " psc where sc.sc_id=? and sc.ps_id=psc.id) and domestic=1";
			DBRow para = new DBRow();
			para.add("sc_id", sc_id);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getAllExpressCompany() error:" + e);
		}
	}

	public DBRow[] getZoneByScId(long sc_id)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("shipping_zone") + " where sc_id=? order by sz_id asc";
			
			DBRow para = new DBRow();
			para.add("sc_id",sc_id);
			
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql,para);

			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getZoneByScId() error:" + e);
		}
	}

	public DBRow[] getFeeBySzId(long sz_id)
		throws Exception
	{
		try
		{
			String sql = "SELECT sf.sf_id,st_weight,en_weight,f_weight_fee,k_weight_fee FROM " + ConfigBean.getStringValue("shipping_weight") + " sw," + ConfigBean.getStringValue("shipping_weight_fee_mapping") + " swfm," + ConfigBean.getStringValue("shipping_fee") + " sf where sf.sz_id=? and sf.sf_id=swfm.sf_id and swfm.sw_id=sw.sw_id Order By sw.st_weight asc";
			
			DBRow para = new DBRow();
			para.add("sz_id",sz_id);
			
			
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql,para);
	
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getFeeBySzId() error:" + e);
		}
	}

	public DBRow[] getFeeWeightSection(long sc_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("sc_id",sc_id);
			
			String sql = "select * from " + ConfigBean.getStringValue("shipping_weight") + " where sc_id=? order by st_weight asc";
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql,para);
	
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getFeeWeightSection() error:" + e);
		}
	}
	
	public DBRow[] getFeeWeightSectionOrderByEnWeightDesc(long sc_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("sc_id",sc_id);
			
			String sql = "select * from " + ConfigBean.getStringValue("shipping_weight") + " where sc_id=? order by en_weight desc";
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql,para);
	
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getFeeWeightSection() error:" + e);
		}
	}

	public long addZone(DBRow row)
		throws Exception
	{
		try
		{
			long sz_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("shipping_zone"));
			row.add("sz_id",sz_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("shipping_zone"),row);
			return(sz_id);
		}
		catch (Exception e)
		{
			throw new Exception("addZone(row) error:" + e);
		}
	}
	
	public long addFee(DBRow row)
		throws Exception
	{
		try
		{
			long sf_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("shipping_fee"));
			row.add("sf_id",sf_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("shipping_fee"),row);
			return(sf_id);
		}
		catch (Exception e)
		{
			throw new Exception("addFee(row) error:" + e);
		}
	}
	
	public void addZoneCountryMapping(DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.insert(ConfigBean.getStringValue("shipping_zone_country_mapping"),row);
		}
		catch (Exception e)
		{
			throw new Exception("addZoneCountryMapping(row) error:" + e);
		}
	}
	
	public void addZoneProvinceMapping(DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.insert(ConfigBean.getStringValue("shipping_zone_province_mapping"),row);
		}
		catch (Exception e)
		{
			throw new Exception("addZoneProvinceMapping(row) error:" + e);
		}
	}
	
	public long addCompany(DBRow row)
		throws Exception
	{
		try
		{
			
			long sc_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("shipping_company"));
			row.add("sc_id",sc_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("shipping_company"),row);
			return(sc_id);
		}
		catch (Exception e)
		{
			throw new Exception("addCompany(row) error:" + e);
		}
	}

	public long addWeight(DBRow row)
		throws Exception
	{
		try
		{
			long sw_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("shipping_weight"));
			row.add("sw_id",sw_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("shipping_weight"),row);
			return(sw_id);
		}
		catch (Exception e)
		{
			throw new Exception("addWeight(row) error:" + e);
		}
	}
		
	public DBRow getDetailWeightByScidWeightRange(long sc_id,float weight)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("sc_id",sc_id);
			para.add("st_weight",weight);
			para.add("en_weight",weight);
			
			DBRow row = dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("shipping_weight") + " where sc_id=? and ?>st_weight and ?<=en_weight",para);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailWeightByScidWeightRange(row) error:" + e);
		}
	}
	
	public void delFee(long sf_id)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.delete("where sf_id=" + sf_id,ConfigBean.getStringValue("shipping_fee"));
		}
		catch (Exception e)
		{
			throw new Exception("delFee(row) error:" + e);
		}
	}
	
	public void delFeeBySzId(long sz_id)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.delete("where sz_id=" + sz_id,ConfigBean.getStringValue("shipping_fee"));
		}
		catch (Exception e)
		{
			throw new Exception("delFeeBySzId(row) error:" + e);
		}
	}
	
	public void delWeightFeeMappingBySwId(long sw_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where sw_id="+sw_id,ConfigBean.getStringValue("shipping_weight_fee_mapping"));
		}
		catch (Exception e)
		{
			throw new Exception("delWeightFeeMappingBySwId(row) error:" + e);
		}
	}
	
	public void delWeightFeeMappingBySfId(long sf_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where sf_id="+sf_id,ConfigBean.getStringValue("shipping_weight_fee_mapping"));
		}
		catch (Exception e)
		{
			throw new Exception("delWeightFeeMappingBySfId(row) error:" + e);
		}
	}
	
	public void delWeight(long sw_id)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.delete("where sw_id=" + sw_id,ConfigBean.getStringValue("shipping_weight"));
		}
		catch (Exception e)
		{
			throw new Exception("delWeight(row) error:" + e);
		}
	}

	public void delWeightByScId(long sc_id)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.delete("where sc_id=" + sc_id,ConfigBean.getStringValue("shipping_weight"));
		}
		catch (Exception e)
		{
			throw new Exception("delWeightByScId(row) error:" + e);
		}
	}
	
	public DBRow[] getWeightFeeMappingBySwid(long sw_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("sw_id",sw_id);
			
			String sql = "select * from " + ConfigBean.getStringValue("shipping_weight_fee_mapping") + " where sw_id=? ";
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql,para);
	
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getWeightFeeMappingBySwid() error:" + e);
		}
	}
	
	public DBRow getDetailCompany(long sc_id)
		throws Exception
	{
		try
		{
			DBRow row = dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("shipping_company") + " where sc_id=" + sc_id);
//			DBRow row = dbUtilAutoTran.selectSingleCache("select * from " + ConfigBean.getStringValue("shipping_company") + " where sc_id=" + sc_id,new String[]{ConfigBean.getStringValue("shipping_company")});
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailCompany(row) error:" + e);
		}
	}

	public DBRow getDetailCompanyByName(String name)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("shipping_company") + " where name=?";
			
			DBRow para = new DBRow();
			para.add("name", name);
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql,para);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailCompany(row) error:" + e);
		}
	}
	
	public DBRow getDetailCompanyByParentid(long parentid)
		throws Exception
	{
		try
		{
			DBRow row = dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("shipping_company") + " where parentid=" + parentid);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailCompanyByParentid(row) error:" + e);
		}
	}

	public DBRow getDetailWeight(long adid)
		throws Exception
	{
		try
		{
			DBRow row = dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("shipping_weight") + " where sw_id=" + adid);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailWeight(row) error:" + e);
		}
	}
		
	public DBRow getDetailFeeByCompanyWeightZone(long sc_id,long sw_id,long sz_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("sc_id",sc_id);
			para.add("sw_id",sw_id);
			para.add("sz_id",sz_id);
			
			String sql = "select sf.sf_id,sf.f_weight_fee,sf.k_weight_fee from " + ConfigBean.getStringValue("shipping_weight") + " sw," + ConfigBean.getStringValue("shipping_weight_fee_mapping") + " swfm,"+ConfigBean.getStringValue("shipping_zone")+" sz,"+ConfigBean.getStringValue("shipping_fee")+"  sf," + ConfigBean.getStringValue("shipping_company") + " sc where sc.sc_id=? and sw.sw_id=? and sz.sz_id=? and sc.sc_id=sw.sc_id and sw.sw_id=swfm.sw_id and swfm.sf_id=sf.sf_id and sf.sz_id=sz.sz_id";
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql,para);
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailFeeByCompanyWeightZone(row) error:" + e);
		}
	}
	
	public void modFee(long sf_id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where sf_id=" + sf_id,ConfigBean.getStringValue("shipping_fee"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modFee(row) error:" + e);
		}
	}
	
	public void addWeightFeeMapping(DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.insert(ConfigBean.getStringValue("shipping_weight_fee_mapping"),row);
		}
		catch (Exception e)
		{
			throw new Exception("addWeightFeeMapping(row) error:" + e);
		}
	}
		
	public void delZoneByScId(long sc_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where sc_id=" + sc_id,ConfigBean.getStringValue("shipping_zone"));
		}
		catch (Exception e)
		{
			throw new Exception("delZoneByScId(row) error:" + e);
		}
	}
	
	public void delZoneBySzId(long sz_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where sz_id=" + sz_id,ConfigBean.getStringValue("shipping_zone"));
		}
		catch (Exception e)
		{
			throw new Exception("delZoneBySzId(row) error:" + e);
		}
	}
	
	public void delCompany(long sc_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where sc_id=" + sc_id,ConfigBean.getStringValue("shipping_company"));
		}
		catch (Exception e)
		{
			throw new Exception("delCompany(row) error:" + e);
		}
	}
	
	public void modCompany(long sc_id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where sc_id=" + sc_id,ConfigBean.getStringValue("shipping_company"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modCompany(row) error:" + e);
		}
	}

	public void modZone(long sz_id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where sz_id=" + sz_id,ConfigBean.getStringValue("shipping_zone"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modZone(row) error:" + e);
		}
	}
		
	public DBRow getDetailZone(long sz_id)
		throws Exception
	{
		try
		{
			DBRow row = dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("shipping_zone") + " where sz_id=" + sz_id);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailZone(row) error:" + e);
		}
	}
	
	public DBRow getDetailZoneByScidName(long sc_id,String name)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("shipping_zone") + " where sc_id=? and name=?";
			DBRow para = new DBRow();
			para.add("sc_id", sc_id);
			para.add("name", name);
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql,para);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailZoneByScidName(row) error:" + e);
		}
	}
	
	public void modWeight(long sw_id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where sw_id=" + sw_id,ConfigBean.getStringValue("shipping_weight"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modWeight(row) error:" + e);
		}
	}
	
	public DBRow getDetailWeightByWeightScid(long sc_id,float weight)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("sc_id", sc_id);
			para.add("1", weight);
			para.add("2", weight);
			
			DBRow row = dbUtilAutoTran.selectPreSingle("select * from  "+ConfigBean.getStringValue("shipping_weight")+" where sc_id=? and st_weight<=? and ?<=en_weight",para);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailWeightByWeightScid(row) error:" + e);
		}
	}
	
	public DBRow getDetailZoneByCcidScid(long sc_id,long ccid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("sc_id", sc_id);
			para.add("ccid", ccid);
			
			String sql = "select * from "+ConfigBean.getStringValue("shipping_zone_country_mapping")+" szcm ,"+ConfigBean.getStringValue("shipping_zone")+" sz where szcm.sz_id=sz.sz_id and sz.sc_id=? and szcm.ccid=?";
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql,para);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailZoneByCcidScid(row) error:" + e);
		}
	}
	
	public DBRow getDetailZoneByProIdScid(long sc_id,long pro_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("sc_id", sc_id);
			para.add("pro_id", pro_id);
			
			String sql = "select * from "+ConfigBean.getStringValue("shipping_zone_province_mapping")+" szcm ,"+ConfigBean.getStringValue("shipping_zone")+" sz where szcm.sz_id=sz.sz_id and sz.sc_id=? and szcm.pro_id=?";
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql,para);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailZoneByProIdScid(row) error:" + e);
		}
	}
	
	public DBRow[] getZoneCountry(long sc_id,long sz_id)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			para.add("sc_id",sc_id);
			para.add("sz_id",sz_id);
			
			String sql = "select * from "+ConfigBean.getStringValue("country_code")+"  cc Left JOIN "+ConfigBean.getStringValue("shipping_zone_country_mapping")+" szcm on szcm.ccid=cc.ccid and szcm.sc_id=?  where szcm.sz_id is null or sz_id=? order by c_country asc";
			
			row = dbUtilAutoTran.selectPreMutliple(sql,para);
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getZoneCountry() error:" + e);
		}
	}
	
	public DBRow[] getZoneProvince(long sc_id,long sz_id)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			para.add("sc_id1",sc_id);
			para.add("sc_id2",sc_id);
			para.add("sz_id",sz_id);
			
			String sql = "select * from (select cp.pro_id,cp.pro_name,nation_id,p_code from "+ConfigBean.getStringValue("shipping_company")+" sc,"+ConfigBean.getStringValue("product_storage_catalog")+" psc,"+ConfigBean.getStringValue("country_province")+" cp where sc.sc_id=? and sc.ps_id=psc.id and psc.native=cp.nation_id)  cc Left JOIN shipping_zone_province_mapping  szpm on szpm.pro_id=cc.pro_id and szpm.sc_id=?  where szpm.sz_id is null or sz_id=?";
			 
			row = dbUtilAutoTran.selectPreMutliple(sql,para);
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getZoneProvince() error:" + e);
		}
	}

	public void delZoneCountryMappingBySzId(long sz_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where sz_id=" + sz_id,ConfigBean.getStringValue("shipping_zone_country_mapping"));
		}
		catch (Exception e)
		{
			throw new Exception("delZoneCountryMappingBySzId(row) error:" + e);
		}
	}
	
	public void delZoneProvinceMappingBySzId(long sz_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where sz_id=" + sz_id,ConfigBean.getStringValue("shipping_zone_province_mapping"));
		}
		catch (Exception e)
		{
			throw new Exception("delZoneProvinceMappingBySzId(row) error:" + e);
		}
	}
		
	public void delZoneCountryMappingByScId(long sc_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where sc_id=" + sc_id,ConfigBean.getStringValue("shipping_zone_country_mapping"));
		}
		catch (Exception e)
		{
			throw new Exception("delZoneCountryMappingByScId(row) error:" + e);
		}
	}
	
	public void delZoneProvinceMappingByScId(long sc_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where sc_id=" + sc_id,ConfigBean.getStringValue("shipping_zone_province_mapping"));
		}
		catch (Exception e)
		{
			throw new Exception("delZoneProvinceMappingByScId(row) error:" + e);
		}
	}
		
	public DBRow[] getZoneCountryMappingBySzId(long sz_id)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			para.add("sz_id",sz_id);
			
			String sql = "select * from "+ConfigBean.getStringValue("country_code")+"  cc,"+ConfigBean.getStringValue("shipping_zone_country_mapping")+" szcm where szcm.ccid=cc.ccid and sz_id=? order by c_country asc";
			
			row = dbUtilAutoTran.selectPreMutliple(sql,para);
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getZoneCountryMappingBySzId() error:" + e);
		}
	}

	public DBRow[] getZoneProvinceMappingBySzId(long sz_id)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			para.add("sz_id",sz_id);
			
			String sql = "select * from "+ConfigBean.getStringValue("country_province")+"  cc,"+ConfigBean.getStringValue("shipping_zone_province_mapping")+" szcm where szcm.pro_id=cc.pro_id and sz_id=? order by pro_name asc";
			
			row = dbUtilAutoTran.selectPreMutliple(sql,para);
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getZoneProvinceMappingBySzId() error:" + e);
		}
	}
	
	public DBRow[] getZoneProvinceMappingBySzId(long sc_id,long sz_id)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			para.add("sc_id",sc_id);
			para.add("sz_id",sz_id);
			
			String sql = "select * from (select cp.pro_id,cp.pro_name,nation_id,p_code from "+ConfigBean.getStringValue("shipping_company")+" sc,"+ConfigBean.getStringValue("product_storage_catalog")+" psc,"+ConfigBean.getStringValue("country_province")+" cp where sc.sc_id=? and sc.ps_id=psc.id and psc.native=cp.nation_id) cc,"+ConfigBean.getStringValue("shipping_zone_province_mapping")+" szcm where szcm.pro_id=cc.pro_id and sz_id=? order by pro_name asc";
			
			row = dbUtilAutoTran.selectPreMutliple(sql,para);
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getZoneProvinceMappingBySzId() error:" + e);
		}
	}
		
	public DBRow[] getAllValidateExpressCompany(PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			String sql = "select * from " + ConfigBean.getStringValue("shipping_company") + " where locked=0 order by sc_id desc";
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectMutliple(sql,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectMutliple(sql);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getAllValidateExpressCompany() error:" + e);
		}
	}
			
	public DBRow[] getExpressCompanysByPsId(long ps_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			
			para.add("ps_id", ps_id);
			
			String sql = "select * from " + ConfigBean.getStringValue("shipping_company") + " where ps_id=? and locked=0 and parentid=0 order by sc_id desc";
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getExpressCompanysByPsId() error:" + e);
		}
	}

	public DBRow[] getAllExpressCompanysByPsId(long ps_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			String sql = "select * from " + ConfigBean.getStringValue("shipping_company") + " where ps_id=? and parentid=0 order by sc_id desc";
			DBRow para = new DBRow();
			para.add("ps_id", ps_id);
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para);
			}

			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getAllExpressCompanysByPsId() error:" + e);
		}
	}
	
	public DBRow[] getAllExpressCompanysByPsIdDomestic(long ps_id,int domestic,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			String sql = "select * from " + ConfigBean.getStringValue("shipping_company") + " where ps_id=? and domestic=? and parentid=0 order by sc_id desc";
			DBRow para = new DBRow();
			para.add("ps_id", ps_id);
			para.add("domestic", domestic);
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para);
			}
	
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getAllExpressCompanysByPsIdDomestic() error:" + e);
		}
	}
		
	public DBRow getDetailWeightByStEnScId(long sc_id,float st,float en)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("sc_id", sc_id);
			para.add("st", st);
			para.add("en", en);
			
			DBRow row = dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("shipping_weight") + " where sc_id=? and st_weight=? and en_weight=?",para);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailWeightByStEnScId(row) error:" + e);
		}
	}
				
	public DBRow[] getDomesticExpressCompanyByPsIdProId(long ps_id,long pro_id,float weight,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select sc.* from " + ConfigBean.getStringValue("shipping_company") + " sc," + ConfigBean.getStringValue("shipping_zone_province_mapping") + " szpm   where sc.ps_id=? and sc.domestic=1 and sc.sc_id=szpm.sc_id and szpm.pro_id=? and sc.st_weight<=? and ?<en_weight";
			DBRow para = new DBRow();
			para.add("ps_id", ps_id);
			para.add("pro_id", pro_id);
			para.add("weight1", weight);
			para.add("weight2", weight);
			
			//System.out.println(ps_id+" - "+pro_id+" - "+weight);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getDomesticExpressCompanyByPsIdProId() error:" + e);
		}
	}
	
	public DBRow[] getDomesticExpressCompanyByPsIdProId(long ps_id,long pro_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select sc.* from " + ConfigBean.getStringValue("shipping_company") + " sc," + ConfigBean.getStringValue("shipping_zone_province_mapping") + " szpm   where sc.ps_id=? and sc.domestic=1 and sc.sc_id=szpm.sc_id and szpm.pro_id=? ";
			DBRow para = new DBRow();
			para.add("ps_id", ps_id);
			para.add("pro_id", pro_id);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getDomesticExpressCompanyByPsIdProId() error:" + e);
		}
	}
	
	public DBRow[] getInternationalExpressCompanyByPsIdProId(long ps_id,long ccid,float weight,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select sc.* from " + ConfigBean.getStringValue("shipping_company") + " sc," + ConfigBean.getStringValue("shipping_zone_country_mapping") + " szcm   where sc.ps_id=? and sc.domestic=0 and sc.sc_id=szcm.sc_id and szcm.ccid=? and sc.st_weight<=? and ?<en_weight";
			
			DBRow para = new DBRow();
			para.add("ps_id", ps_id);
			para.add("ccid", ccid);
			para.add("weight1", weight);
			para.add("weight2", weight);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getInternationalExpressCompanyByPsIdProId() error:" + e);
		}
	}
	
	public DBRow[] getInternationalExpressCompanyByPsIdProId(long ps_id,long ccid,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select sc.* from " + ConfigBean.getStringValue("shipping_company") + " sc," + ConfigBean.getStringValue("shipping_zone_country_mapping") + " szcm   where sc.ps_id=? and sc.domestic=0 and sc.sc_id=szcm.sc_id and szcm.ccid=? ";
			
			DBRow para = new DBRow();
			para.add("ps_id", ps_id);
			para.add("ccid", ccid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getInternationalExpressCompanyByPsIdProId() error:" + e);
		}
	}
	
	public DBRow[] getExpressCompanysByScIds(long sc_ids[],PageCtrl pc)
		throws Exception
	{
		try
		{
			String sc_ids_str = "";
			
			if (sc_ids.length>0)
			{
				int i=0;
				for (; i<sc_ids.length-1; i++)
				{
					sc_ids_str += sc_ids[i]+",";
				}
				sc_ids_str += sc_ids[i];
			}
			else
			{
				sc_ids_str = "-1";
			}
			
			String sql = "select * from " + ConfigBean.getStringValue("shipping_company") + " where sc_id in ("+sc_ids_str+")";
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getExpressCompanysByScIds() error:" + e);
		}
	}
			

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}








