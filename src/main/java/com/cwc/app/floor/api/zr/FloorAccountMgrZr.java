package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorAccountMgrZr
{

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	public long addAccount(DBRow row) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("account");
			return dbUtilAutoTran.insertReturnId(tableName, row);
		}catch (Exception e) {
			throw new Exception("FloorAccountMgrZr.addAccount(DBRow):"+e);
		}
	}
	public  DBRow[] getAccountByWithIdAndWithType(long account_with_id , int account_with_type) throws Exception {
		try{
			String sql = "select * from "  + ConfigBean.getStringValue("account") + " where account_with_id=" + account_with_id + " and account_with_type=" + account_with_type ;
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorAccountMgrZr.getAccountByWithIdAndWithType(account_with_id,account_with_type):"+e);
		}
	}
	public void updateAccount(long account_id , DBRow updateRow) throws Exception {
		try{
			dbUtilAutoTran.update(" where account_id=" + account_id, ConfigBean.getStringValue("account"), updateRow);
		}catch (Exception e) {
			throw new Exception("FloorAccountMgrZr.updateAccount(account_id,updateRow):"+e);

		}
	}
	public void deleteAccountById(long account_id) throws Exception{
		try{
			dbUtilAutoTran.delete(" where account_id=" + account_id,  ConfigBean.getStringValue("account"));
			
		}catch (Exception e) {
			throw new Exception("FloorAccountMgrZr.deleteAccountById(account_id):"+e);

		}
	}
	public DBRow getAccountById(long account_id) throws Exception{
		try{
			String sql = "select *  from " +  ConfigBean.getStringValue("account") + " where account_id=" + account_id;
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e) {
			throw new Exception("FloorAccountMgrZr.getAccountById(account_id):"+e);

		}
	}
	
}
