package com.cwc.app.floor.api.zr;

import org.apache.commons.lang.StringUtils;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
 

public class FloorFileMgrZr
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public long addFile(DBRow row) throws Exception {
		try{
			return  dbUtilAutoTran.insertReturnId("file", row);	
		}catch (Exception e) {
			throw new Exception("FloorFileMgrZr.addFile(row):"+e);
		}
	}
	
	public int getIndexOfFileByFileName(String fileName) throws Exception 
	{
		try
		{

			String tableName =  ConfigBean.getStringValue("file");
			StringBuffer sql = new StringBuffer("select * from ").append(tableName).append(" where file_name like '")
			.append(fileName).append("%' order by file_id desc limit 0,1");
 			DBRow fileRow = dbUtilAutoTran.selectSingle(sql.toString());
			if(fileRow == null)
			{	
				return 0;
			}else
			{
				String name = fileRow.getString("file_name");
				String[] tempArray = name.split("_");
				int index = 1 ;
				String stringIndex = tempArray[tempArray.length -1];
				if(stringIndex.indexOf(".") != -1){
					stringIndex = stringIndex.substring(0, stringIndex.indexOf(".") );
				}
				try
				{
					//第一次的重复的时候会出错
					index = Integer.parseInt(stringIndex) + 1;
				}catch (Exception e) {
					 
				}finally{}
				return index ;
			}	
		}catch (Exception e) 
		{
			throw new Exception("FloorFileMgrZr.getIndexOfFileByFileName(fileName):"+e);
		}
	}
	public void updateFileById(long file_id , DBRow row) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("file");
			dbUtilAutoTran.update("where file_id=" + file_id, tableName, row);	
		}catch (Exception e) {
			throw new Exception("FloorFileMgrZr.updateFileById(file_id,row):"+e);
		}
	}
	public DBRow getFileById(long file_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("file");
			return dbUtilAutoTran.selectSingle("select * from "+tableName + " where file_id="+file_id);
		}catch (Exception e) {
			throw new Exception("FloorFileMgrZr.updateFileById(file_id,row):"+e);
		}
	}
	public DBRow[] getAllFileByFileWithIdAndFileWithType(long fileWithId , int fileWithType) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("file");
			String sql = "select * from " +tableName + " where file_with_type="+fileWithType + " and file_with_id="+fileWithId;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorFileMgrZr.updateFileById(pc,fileWithType):"+e);
		}
	}
	public int deleteFile(long fileId ) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("file");
			return dbUtilAutoTran.delete("where file_id=" + fileId, tableName);
		}catch (Exception e) {
			throw new Exception("FloorFileMgrZr.deleteFile(fileId):"+e);
		}
	}
	public DBRow[] getAllFileByFileWithIdAndFileWithTypeAndFileWithClass(long fileWithId , int fileWithType , int fileWithClass) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("file");
			String sql = "select * from " +tableName + " where file_with_type="+fileWithType + " and file_with_id="+fileWithId +" and file_with_class="+fileWithClass;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorFileMgrZr.getAllFileByFileWithIdAndFileWithTypeAndFileWithClass(fileWithId,fileWithType,fileWithClass):"+e);
		}
	}
	//查询商品文件
	public DBRow[] getProductFileByWithIdAndWithTypeFileTypeAndPcId(long file_with_id,
			int file_with_type, int product_file_type, String pc_id) throws Exception {
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select product_file.* ,product.p_name from product_file left join product on product_file.pc_id = product.pc_id where file_with_type=")
			.append(file_with_type).append(" and file_with_id=").append(file_with_id)
			.append(" and product_file_type=").append(product_file_type);
			if(pc_id!=""){
				sql=sql.append(" and product_file.pc_id in (").append(pc_id).append(")");
			}
			return dbUtilAutoTran.selectMutliple(sql.toString());
			
		}catch (Exception e) {
			throw new Exception("FloorTransportMgrZr.getProductFileByWithIdAndWithTypeFileTypeAndPcId(file_with_id ,file_with_type ,product_file_type,pc ):"+e);
		}
	}
	
	
	public int getIndexOfFilebyFileName(String fileName) throws Exception
	{
		try
		{

			String tableName = "file";
			StringBuffer sql = new StringBuffer("select * from ").append(tableName).append(" where file_name like '")
			.append(fileName).append("%' order by file_id desc limit 0,1");
			DBRow fileRow = dbUtilAutoTran.selectSingle(sql.toString());
			if(fileRow == null)
			{	
				return 0;
			}else
			{
				String name = fileRow.getString("file_name");
				String[] tempArray = name.split("_");
				int index = 1 ;
				String stringIndex = tempArray[tempArray.length -1];
				if(stringIndex.indexOf(".") != -1){
					stringIndex = stringIndex.substring(0, stringIndex.indexOf(".") );
				}
				try
				{
					//第一次的重复的时候会出错
					index = Integer.parseInt(stringIndex) + 1;
				}catch (Exception e) {
					 
				}finally{}
				return index ;
			}	
		}catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZr.getIndexOfFilebyFileName(fileName):"+e);
		}
	}
	public void deleteFileByFileWithTypeAndFileWithId(long file_with_id , int file_with_type) throws Exception{
		try{
			dbUtilAutoTran.delete(" where file_with_type="+file_with_type + " and file_with_id="+ file_with_id, ConfigBean.getStringValue("file"));
		}catch (Exception e) {
			throw new Exception("FloorTransportMgrZr.deleteFileByFileWithTypeAndFileWithId(file_with_id,file_with_type):"+e);
		}
		
	}
	//文件分类
	public DBRow[] getPicturesByClasKey(int dlo_id,int classKey) throws Exception{
		try{
			String sql = "select * from file where file_with_class = "+classKey+" and file_with_id="+dlo_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorFileMgrZr.getPicturesByClasKey(dlo_id,classKey):"+e);
		}
	}
	public DBRow[] getFileBy(long fileWithId, int fileWithType, int fileWithClass , long file_with_detail_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("file");
			
			String sql = "select * from " +tableName + " where file_with_type="+fileWithType + " and file_with_id="+fileWithId +" and file_with_class="+fileWithClass ; 
			if(file_with_detail_id > 0l){
				sql  += " and file_with_detail_id="+file_with_detail_id;
			} 
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorFileMgrZr.getFileBy():"+e);
		}
	}
	public DBRow[] getFileBy(long fileWithId, int fileWithType, int fileWithClass , long file_with_detail_id , String option_file_param) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("file");
			String sql = "select * from " +tableName + " where file_with_type="+fileWithType + " and file_with_id="+fileWithId +" and file_with_class="+fileWithClass ; 
		 
			 if(file_with_detail_id > 0l){
				sql  += " and file_with_detail_id="+file_with_detail_id;
			} 
			 if(StringUtils.isNotBlank(option_file_param)){
					sql  += " and option_file_param='"+option_file_param+"'";
			} 
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorFileMgrZr.getFileBy():"+e);
		}
	}
	public DBRow[] getFileBy(long fileWithId, int fileWithType, int fileWithClass) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("file");
			String sql = "select * from " +tableName + " where 1=1 "; 
			
			if(fileWithId > 0l){
				sql  += " and file_with_id="+fileWithId;
			} 
			if(fileWithType>0l){
				sql  += " and file_with_type="+fileWithType;
			}
			if(fileWithClass>0l){
				sql  += " and file_with_class="+fileWithClass;
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorFileMgrZr.getFileBy():"+e);
		}
	}

}
