package com.cwc.app.floor.api.ll;


import com.cwc.app.beans.ll.CountryCodeBeanLL;
import com.cwc.app.beans.ll.TransportApproveBeanLL;
import com.cwc.app.beans.ll.TransportApproveDetailBeanLL;
import com.cwc.app.beans.ll.TransportBeanLL;
import com.cwc.app.beans.ll.TransportDetailBeanLL;
import com.cwc.app.beans.ll.TransportDrawbackBeanLL;
import com.cwc.app.beans.ll.TransportLogsBeanLL;
import com.cwc.app.beans.ll.TransportOutboundBeanLL;
import com.cwc.app.beans.ll.TransportSendApproveBeanLL;
import com.cwc.app.beans.ll.TransportSendApproveDetailBeanLL;
import com.cwc.app.beans.ll.TransportWarehouseBeanLL;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class FloorTransportMgrLL {
	private TransportBeanLL transportBeanLL;
	private CountryCodeBeanLL countryCodeBeanLL;
	private TransportLogsBeanLL transportLogsBeanLL;
	private TransportApproveBeanLL transportApproveBeanLL;
	private TransportApproveDetailBeanLL transportApproveDetailBeanLL;
	private TransportDetailBeanLL transportDetailBeanLL;
	private TransportWarehouseBeanLL transportWarehouseBeanLL;
	private TransportOutboundBeanLL transportOutboundBeanLL;
	private TransportSendApproveBeanLL transportSendApproveBeanLL;
	private TransportSendApproveDetailBeanLL transportSendApproveDetailBeanLL;
	private TransportDrawbackBeanLL transportDrawbackBeanLL;
	
	public TransportSendApproveBeanLL getTransportSendApproveBeanLL() {
		return transportSendApproveBeanLL;
	}

	public void setTransportSendApproveBeanLL(
			TransportSendApproveBeanLL transportSendApproveBeanLL) {
		this.transportSendApproveBeanLL = transportSendApproveBeanLL;
	}

	public TransportSendApproveDetailBeanLL getTransportSendApproveDetailBeanLL() {
		return transportSendApproveDetailBeanLL;
	}

	public void setTransportSendApproveDetailBeanLL(
			TransportSendApproveDetailBeanLL transportSendApproveDetailBeanLL) {
		this.transportSendApproveDetailBeanLL = transportSendApproveDetailBeanLL;
	}

	public TransportOutboundBeanLL getTransportOutboundBeanLL() {
		return transportOutboundBeanLL;
	}

	public void setTransportOutboundBeanLL(
			TransportOutboundBeanLL transportOutboundBeanLL) {
		this.transportOutboundBeanLL = transportOutboundBeanLL;
	}

	public TransportWarehouseBeanLL getTransportWarehouseBeanLL() {
		return transportWarehouseBeanLL;
	}

	public void setTransportWarehouseBeanLL(
			TransportWarehouseBeanLL transportWarehouseBeanLL) {
		this.transportWarehouseBeanLL = transportWarehouseBeanLL;
	}

	public TransportDetailBeanLL getTransportDetailBeanLL() {
		return transportDetailBeanLL;
	}

	public void setTransportDetailBeanLL(TransportDetailBeanLL transportDetailBeanLL) {
		this.transportDetailBeanLL = transportDetailBeanLL;
	}

	public TransportApproveBeanLL getTransportApproveBeanLL() {
		return transportApproveBeanLL;
	}

	public void setTransportApproveBeanLL(
			TransportApproveBeanLL transportApproveBeanLL) {
		this.transportApproveBeanLL = transportApproveBeanLL;
	}

	public TransportApproveDetailBeanLL getTransportApproveDetailBeanLL() {
		return transportApproveDetailBeanLL;
	}

	public void setTransportApproveDetailBeanLL(
			TransportApproveDetailBeanLL transportApproveDetailBeanLL) {
		this.transportApproveDetailBeanLL = transportApproveDetailBeanLL;
	}

	public TransportLogsBeanLL getTransportLogsBeanLL() {
		return transportLogsBeanLL;
	}

	public void setTransportLogsBeanLL(TransportLogsBeanLL transportLogsBeanLL) {
		this.transportLogsBeanLL = transportLogsBeanLL;
	}

	public CountryCodeBeanLL getCountryCodeBeanLL() {
		return countryCodeBeanLL;
	}

	public void setCountryCodeBeanLL(CountryCodeBeanLL countryCodeBeanLL) {
		this.countryCodeBeanLL = countryCodeBeanLL;
	}

	public TransportBeanLL getTransportBeanLL() {
		return transportBeanLL;
	}

	public void setTransportBeanLL(TransportBeanLL transportBeanLL) {
		this.transportBeanLL = transportBeanLL;
	}
	
	public DBRow getTransportById(String id) throws Exception {
		return transportBeanLL.getRowById(id);
	}
	
	public DBRow updateTransport(DBRow row) throws Exception {
		return transportBeanLL.updateRow(row);
	}
	
	public DBRow[] getCounty() throws Exception {
		return countryCodeBeanLL.getAll();
	}
	
	public DBRow getCountyById(String id) throws Exception {
		return countryCodeBeanLL.getRowById(id);
	}
	
	public DBRow[] getTransportLogsByType(long transport_id, int transport_type) throws Exception {
		DBRow para = new DBRow();
		para.add("transport_id", transport_id);
		if(transport_type != 0)
			para.add("transport_type", transport_type);
		return transportLogsBeanLL.getRowsByPara(para, null, null, "logs_id desc", null);

	}
	
	public DBRow[] getTransportLogsByTransportIdAndType(long transport, int[] types) throws Exception{
		try{
			String sql = "select * from transport_logs";
			if(null != types && types.length > 0){
				if(1 == types.length){
					sql += " where transport_type = " + types[0];
				}else{
					String sqlWhere = " where ( transport_type = " + types[0];
					for(int i = 1; i < types.length; i ++){
						sqlWhere += " or transport_type = " + types[i];
					}
					sqlWhere += ")";
					sql += sqlWhere;
				}
				sql += " and transport_id = " + transport;
			}else{
				sql += " where transport_id = " + transport;
			}
			sql += " order by transport_type";
			return transportLogsBeanLL.getDbUtilAutoTran().selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorTransportMgrLL.getTransportLogsByTransportIdAndType(long transport, int[] types):"+e);
		}	
	}
	private DBRow insertTransportLogs(DBRow row) throws Exception {
		return transportLogsBeanLL.insertRow(row);
	}
	
	public void insertLogs(String id, String content, long oprator_id, String oprator, int type) 
		throws Exception
	{
		TDate tDate = new TDate();
		DBRow logRow = new DBRow();
		logRow.add("transport_id", id);
		logRow.add("transport_content", content);
		logRow.add("transporter_id", oprator_id);
		logRow.add("transporter", oprator);
		logRow.add("transport_type", type);
		logRow.add("transport_date", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
		insertTransportLogs(logRow);
		
		DBRow row = new DBRow();
		row.add("transport_id", id);
		row.add("updatedate", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
		row.add("updateby", oprator_id);
		row.add("updatename", oprator);
		updateTransport(row);
	}
	
	public void insertLogs(String id, String content, long oprator_id, String oprator, int type,int activity_id) throws Exception{
		TDate tDate = new TDate();
		DBRow logRow = new DBRow();
		logRow.add("transport_id", id);
		logRow.add("transport_content", content);
		logRow.add("transporter_id", oprator_id);
		logRow.add("transporter", oprator);
		logRow.add("transport_type", type);
		logRow.add("transport_date", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
		logRow.add("activity_id",activity_id);
		insertTransportLogs(logRow);
		
		DBRow row = new DBRow();
		row.add("transport_id", id);
		row.add("updatedate", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
		row.add("updateby", oprator_id);
		row.add("updatename", oprator);
		updateTransport(row);
	}
	
	public DBRow[] getAnalysis(String st, String en, int analysisType, int analysisStatus, int day, PageCtrl pc) throws Exception {
		StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("transport")+" where 1=1");
		
		if(!st.equals("")) {
			sql.append(" and transport_date >'"+st+"'");
		}
		
		if(!en.equals("")) {
			sql.append(" and transport_date <='"+en+"'");
		}
		
		if(analysisType == 1) {//报关 
			if(analysisStatus==1) {
				sql.append(" and declaration_over is null");
				sql.append(" and now() > ADDDATE(transport_date,INTERVAL "+day+" DAY)");
				sql.append(" and declaration in (2,3)");
			}
			else {
				sql.append(" and declaration_over is not null");
				sql.append(" and declaration_over>"+day);
			}
		}
		else if(analysisType == 2) {
			if(analysisStatus==1) {
				sql.append(" and clearance_over is null");
				sql.append(" and now() > ADDDATE(transport_date,INTERVAL "+day+" DAY)");
				sql.append(" and clearance in (2,3)");
			}
			else {
				sql.append(" and clearance_over is not null");
				sql.append(" and clearance_over>"+day);
			}
		}
		else if(analysisType == 3) {
			if(analysisStatus==1) {
				sql.append(" and drawback_over is null");
				sql.append(" and now() > ADDDATE(transport_date,INTERVAL "+day+" DAY)");
				sql.append(" and drawback in (2,3)");
			}
			else {
				sql.append(" and drawback_over is not null");
				sql.append(" and drawback_over>"+day);
			}
		}
		else if(analysisType == 4) {
			if(analysisStatus==1) {
				sql.append(" and invoice_over is null");
				sql.append(" and now() > ADDDATE(transport_date,INTERVAL "+day+" DAY)");
				sql.append(" and invoice in (2,3)");
			}
			else {
				sql.append(" and invoice_over is not null");
				sql.append(" and invoice_over>"+day);
			}
		}
		else if(analysisType == 5) {
			if(analysisStatus==1) {
				sql.append(" and all_over is null");
				sql.append(" and now() > ADDDATE(transport_date,INTERVAL "+day+" DAY)");
			}	
			else {
				sql.append(" and all_over is not null");
				sql.append(" and all_over>"+day);
			}
		}
		
		sql.append(" order by transport_id desc");
		
		return transportLogsBeanLL.getDbUtilAutoTran().selectMutliple(sql.toString(), pc);
	}
	
	public void deleteTransportApproveById(String id) throws Exception{
		String[] ids = new String[1];
		ids[0] = id;
		transportApproveBeanLL.delete(ids);
	}
	
	public void deleteTransportApproveDetailByCond(String cond) throws Exception {
		transportApproveDetailBeanLL.deleteByCond(cond);
	}
	
	public DBRow getTransportApproveById(String id) throws Exception {
		return transportApproveBeanLL.getRowById(id);
	}
	
	public void updateTransportDetailByCond(String cond,DBRow row) throws Exception {
		transportDetailBeanLL.updateRowByCond(cond, row);
	}
	
	public DBRow[] getTransportDetailByTransportId(String transport_id) throws Exception {
		DBRow row = new DBRow();
		row.add("transport_id", transport_id);
		return transportDetailBeanLL.getRowsByPara(row, null, null, "", null);
	}
	
	public void deleteTransportDetailById(String id) throws Exception {
		String[] ids = new String[1];
		ids[0] = id;
		transportDetailBeanLL.delete(ids);
	}
	
	public void deleteTransportWarehouseByTransportId(String transport_id) throws Exception {
		String cond = "where tw_transport_id="+transport_id;
		transportWarehouseBeanLL.deleteByCond(cond);
	}
	
	public DBRow[] getTransportWarehousesByTransportId(String transport_id) throws Exception {
		DBRow para = new DBRow();
		para.add("tw_transport_id", transport_id);
		return transportWarehouseBeanLL.getRowsByPara(para, null, null, "", null);
	}
	
	public DBRow getTransportOutboundApproveById(String id) throws Exception {
		return transportSendApproveBeanLL.getRowById(id);
	}
	
	public void deleteTransportOutboundApproveDetailByCond(String cond) throws Exception {
		transportSendApproveDetailBeanLL.deleteByCond(cond);
	}
	
	public void deleteTransportOutboundApproveById(String id) throws Exception{
		String[] ids = new String[1];
		ids[0] = id;
		transportSendApproveBeanLL.delete(ids);
	}
	
	public void updateTransportOutboundDetailByCond(String cond,DBRow row) throws Exception {
		transportDetailBeanLL.updateRowByCond(cond, row);
	}
	
	public void transportSetDrawback(DBRow row) 
		throws Exception 
	{
		
		if(row.get("id", 0)==0) {
			row.remove("id");
			transportDrawbackBeanLL.insertRowInc(row);
		}else {
			transportDrawbackBeanLL.updateRow(row);
		}
	}
	
	public DBRow getDrawback(String transport_id) throws Exception {
		DBRow para = new DBRow();
		para.add("transport_id", transport_id);
		DBRow[] rows = transportDrawbackBeanLL.getRowsByPara(para, null, null, "", null);
		return rows==null||rows.length==0?(new DBRow()):rows[0];
	}

	/**
	 * 通过转运单的ID查询到所有的日志
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportLogsByTransportId(long transport_id) throws Exception{
		try {
			return transportDrawbackBeanLL.getDbUtilAutoTran().selectMutliple("SELECT * FROM transport_logs WHERE transport_id="+transport_id);
		} catch (Exception e) {
			throw new Exception("FloorTransportMgrLL.getTransportLogsByTransportId(long transport_id)"+e);
		}
	}
	public TransportDrawbackBeanLL getTransportDrawbackBeanLL() {
		return transportDrawbackBeanLL;
	}

	public void setTransportDrawbackBeanLL(
			TransportDrawbackBeanLL transportDrawbackBeanLL) {
		this.transportDrawbackBeanLL = transportDrawbackBeanLL;
	}
}
