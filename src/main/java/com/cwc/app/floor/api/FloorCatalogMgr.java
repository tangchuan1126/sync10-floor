package com.cwc.app.floor.api;

import java.util.ArrayList;
import java.util.List;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.*;
import com.cwc.init.StorageJetLagInit;

public class FloorCatalogMgr
{
	private DBUtilAutoTran dbUtilAutoTran;
	private StorageJetLagInit storageJetLagInit;
	
	public void setStorageJetLagInit(StorageJetLagInit storageJetLagInit) {
		
		this.storageJetLagInit = storageJetLagInit;
	}

	public long addProductStorageCatalog(DBRow row)
		throws Exception
	{
		try
		{
			long ps_id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_storage_catalog"),row);
			storageJetLagInit.init(dbUtilAutoTran);
			return ps_id;
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.addProductStorageCatalog(row) error:" + e);
		}
	}
	
	public void modProductStorageCatalog(long id,DBRow row)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.update("where id=" + id,ConfigBean.getStringValue("product_storage_catalog"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.modProductStorageCatalog(row) error:" + e);
		}
	}
	
	public void delProductStorageCatalog(long id)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.delete("where id=" + id,ConfigBean.getStringValue("product_storage_catalog"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.delProductStorageCatalog(row) error:" + e);
		}
	}
	
	public DBRow[] getProductStorageCatalogByParentId(long parentid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("parentid",parentid);
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where parentid=? order by native asc,id desc";

			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
//				return(dbUtilAutoTran.selectPreMutlipleCache(sql,para,pc,new String[]{ConfigBean.getStringValue("product_storage_catalog")}));				
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
//				return(dbUtilAutoTran.selectPreMutlipleCache(sql,para,new String[]{ConfigBean.getStringValue("product_storage_catalog")}));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.getProductStorageCatalogByParentId(row) error:" + e);
		}
	}
	
	public DBRow[] getProductDevStorageCatalogByParentId(long parentid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("parentid",parentid);
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where parentid=? and dev_flag=1 order by sort asc";
	
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
//			return(dbUtilAutoTran.selectPreMutlipleCache(sql,para,pc,new String[]{ConfigBean.getStringValue("product_storage_catalog")}));		
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.getProductDevStorageCatalogByParentId(row) error:" + e);
		}
	}
	
	/**
	 * 获得可发货仓库
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductDevStorageCatalog(PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where dev_flag=1 order by sort asc";
		
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.getProductDevStorageCatalogByParentId(row) error:" + e);
		}
	}
	
	/**
	 * 收货国家当地仓库
	 * @param parentid
	 * @param nativ
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNativeProductDevStorageCatalogByParentId(long parentid,long nativ,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("parentid",parentid);
			para.add("native",nativ);
			
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where parentid=? and native=? and dev_flag=1 order by sort asc";
	
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
//			return(dbUtilAutoTran.selectPreMutlipleCache(sql,para,pc,new String[]{ConfigBean.getStringValue("product_storage_catalog")}));		
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.getProductDevStorageCatalogByParentId(row) error:" + e);
		}
	}
	
	/**
	 * 收货国家海外仓库
	 * @param parentid
	 * @param nativ
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNonNativeProductDevStorageCatalogByParentId(long parentid,long nativ,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("parentid",parentid);
			para.add("native",nativ);
			
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where parentid=? and native!=? and dev_flag=1 order by sort asc";
	
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
//			return(dbUtilAutoTran.selectPreMutlipleCache(sql,para,pc,new String[]{ConfigBean.getStringValue("product_storage_catalog")}));		
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.getNonNativeProductDevStorageCatalogByParentId(row) error:" + e);
		}
	}
		
	public DBRow getDetailProductStorageCatalogById(long id)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("id",id);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("product_storage_catalog") + " psc," + ConfigBean.getStringValue("country_code") + " cc where id=? and psc.native=cc.ccid",para));
//			return(dbUtilAutoTran.selectPreSingleCache("select * from " + ConfigBean.getStringValue("product_storage_catalog") + " psc," + ConfigBean.getStringValue("country_code") + " cc where id=? and psc.native=cc.ccid",para,new String[]{ConfigBean.getStringValue("product_storage_catalog"),ConfigBean.getStringValue("country_code")}));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.getDetailProductStorageCatalogById(row) error:" + e);
		}
	}
	
	public DBRow getProductStorageDetailCatalogByTitle(String title)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("title",title);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where title=?",para));
//			return(dbUtilAutoTran.selectPreSingleCache("select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where title=?",para,new String[]{ConfigBean.getStringValue("product_storage_catalog")}));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.getProductStorageDetailCatalogByTitle(row) error:" + e);
		}
	}
	
//	public DBRow[] getProductStorageCatalogByParentIdByCount(long parentid,int count)
//		throws Exception
//	{
//		try
//		{
//			
//			DBRow para = new DBRow();
//			para.add("parentid",parentid);
//			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where parentid=? order by sort asc";
//			
//			return(dbUtilAutoTran.selectPreMutlipleCacheByCount(sql,para,count,new String[]{ConfigBean.getStringValue("product_storage_catalog")}));
//		}
//		catch (Exception e)
//		{
//			throw new Exception("FloorCatalogMgr.getProductStorageCatalogByParentIdByCount(row) error:" + e);
//		}
//	}
	
	//--------
	
	public long addProductCatalog(DBRow row) throws Exception {
		
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_catalog"),row);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.addProductCatalog(row) error:" + e);
		}
	}
	
	public void updateProductCategoryByCategory(long parentid,long categoryId) throws Exception {
		
		try {
			String sql = "update product set catalog_id = " +categoryId+ " where catalog_id = "+parentid;
			
			dbUtilAutoTran.executeSQL(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.updateProductCategoryByCategory(long parentid,long categoryId) error:" + e);
		}
	}
	
	public void modProductCatalog(long id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where id=" + id,ConfigBean.getStringValue("product_catalog"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.modProductCatalog(row) error:" + e);
		}
	}
	
	public void delProductCatalog(long id)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.delete("where id=" + id,ConfigBean.getStringValue("product_catalog"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.delProductCatalog(row) error:" + e);
		}
	}

	public DBRow getDetailProductCatalogById(long id)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("id",id);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("product_catalog") + " where id=?",para));
//			return(dbUtilAutoTran.selectPreSingleCache("select * from " + ConfigBean.getStringValue("product_catalog") + " where id=?",para,new String[]{ConfigBean.getStringValue("product_catalog")}));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.getDetailProductCatalogById(row) error:" + e);
		}
	}
	
	public DBRow getDetailProductCatalogByName(String name)
	throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("title",name);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("product_catalog") + " where title=?",para));
		// return(dbUtilAutoTran.selectPreSingleCache("select * from " + ConfigBean.getStringValue("product_catalog") + " where id=?",para,new String[]{ConfigBean.getStringValue("product_catalog")}));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.getDetailProductCatalogById(row) error:" + e);
		}
	}
	
	public DBRow[] getProductCatalogByParentId(long parentid,PageCtrl pc)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("parentid",parentid);
			String sql = "select * from " + ConfigBean.getStringValue("product_catalog") + " where parentid=? order by sort asc";
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectPreMutliple(sql, para, pc));				
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql, para));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.getProductCatalogByParentId(row) error:" + e);
		}
	}
	
	public DBRow[] getAllCatalog()
		throws Exception
	{
		
		return dbUtilAutoTran.selectMutliple("select * from product_catalog order by id");
	}
	
	/**
	 * 获得所有仓库
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllProductStoreCatalog()
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("product_storage_catalog");
		
		return (dbUtilAutoTran.selectMutliple(sql));
	}
	
	/**
	 * 获得所有发货仓库
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllSendProductStoreCatalog()
	throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("product_storage_catalog")+" where dev_flag = 1";
		
		return (dbUtilAutoTran.selectMutliple(sql));
	}
	
	/**
	 * 通过商品分类名称获取商品分类
	 * @param title
	 * @return
	 * @throws Exception
	 * zyj
	 */
	public DBRow[] getProductCatalogsByName(String title) throws Exception
	{
		try
		{
			
			return(dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("product_catalog") + " where title='"+title+"'"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.getProductCatalogsByName(String title) error:" + e);
		}
	}
	
	public DBRow getProductCatalogByName(String title) throws Exception {
		
		try {
			
			String sql = "select * from "+ConfigBean.getStringValue("product_catalog")+" where title = ? ";
			
			DBRow para = new DBRow();
			para.add("title",title);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.getProductCatalogByName(String title) error:"+e);
		}
	}
	
	public DBRow getProductCatalogById(long id) throws Exception{
		
		try {
			
			String sql = "select * from "+ConfigBean.getStringValue("product_catalog")+" where id = ? ";
			
			DBRow para = new DBRow();
			para.add("id",id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.getProductCatalogById(long id) error:"+e);
		}
	}
	
	
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public void updateCategoryProductLine(long id,DBRow row)
			throws Exception
		{
			try
			{
				
				dbUtilAutoTran.update("where id=" + id,ConfigBean.getStringValue("product_catalog"),row);
			}
			catch (Exception e)
			{
				throw new Exception("FloorCatalogMgr.updateCategoryProductLine(row) error:" + e);
			}
		}
	
	/**
	 * 获取psid
	 * @param shipToAddress
	 * @param shipToCity
	 * @param pro_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getPsidByShipToAddress(String shipToAddress,String shipToCity,long pro_id) throws Exception{
		try{
			String sql="select * from "+ConfigBean.getStringValue("product_storage_catalog")+" where deliver_city='"+shipToCity+"' and deliver_address='"+shipToAddress+"' and deliver_pro_id="+pro_id;
			return this.dbUtilAutoTran.selectPreSingle(sql, null);
		}catch(Exception e){
			throw new Exception("getPsidByShipToAddress(shipToAddress,shipToCity,pro_id) error:"+e);
		}
	}
	
	/**
	 * 获取psid
	 * @param storage_type_id
	 * @param shipToCity
	 * @param pro_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getPsidByShipToId(long storage_type_id,String shipToCity,long pro_id) throws Exception{
		try{
			String sql="select * from "+ConfigBean.getStringValue("product_storage_catalog")+" where deliver_city='"+shipToCity+"' and storage_type_id='"+storage_type_id+"' and deliver_pro_id="+pro_id;
			return this.dbUtilAutoTran.selectPreSingle(sql, null);
		}catch(Exception e){
			throw new Exception("getPsidByShipToId(storage_type_id,shipToCity,pro_id) error:"+e);
		}
	}
	
	/**
	 * 根据shipTo直接查找对应的ps
	 * @param shipToName title
	 * @return ps相关信息
	 * @throws Exception
	 */
	public DBRow getPsidByShipToName(String shipToName) throws Exception{
		try{
			String sql="select * from "+ConfigBean.getStringValue("product_storage_catalog")+" where title=?";
			DBRow row=new DBRow();
			row.add("title", shipToName);
			return this.dbUtilAutoTran.selectPreSingle(sql, row);
		}catch(Exception e){
			throw new Exception("getPsidByShipToName(shipToName) error:"+e);
		}
	}
	
	/**
	 * 获取psid
	 * @param shipToName
	 * @return
	 * @throws Exception
	 */
	public DBRow getPsidByShipToName(String shipToName,String shipToAddress,String shipToCity,long pro_id) throws Exception{
		try{
			String sql="select * from "+ConfigBean.getStringValue("product_storage_catalog")+" where title=? and deliver_street=? and deliver_city=? and deliver_pro_id=? ";
			DBRow row=new DBRow();
			
			row.add("title", shipToName);
			row.add("deliver_street", shipToAddress);
			row.add("deliver_city", shipToCity);
			row.add("deliver_pro_id", pro_id);
			
			return this.dbUtilAutoTran.selectPreSingle(sql, row);
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("getPsidByShipToName(shipToName) error:"+e);
		}
	}
	
	public DBRow getPsidByLike(String shipToName,String shipToAddress,String shipToCity,long pro_id) throws Exception{
		try{
			String sql="select * from "+ConfigBean.getStringValue("product_storage_catalog")+" where '"+shipToName.replaceAll(" ", "_")+"' like concat(title,'%') and deliver_street=? and deliver_city=? and deliver_pro_id=? ";
			DBRow row=new DBRow();
			
			row.add("deliver_street", shipToAddress);
			row.add("deliver_city", shipToCity);
			row.add("deliver_pro_id", pro_id);
			
			return this.dbUtilAutoTran.selectPreSingle(sql, row);
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("getPsidByShipToName(shipToName) error:"+e);
		}
	}
	
	/***************************************************************************************************************************************/
	
	public DBRow[] getSubProductCatalogByParentId(long id) throws Exception{
		
		try {
			
			String sql = "select * from product_catalog where parentid = "+id;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getSubProductCatalogByParentId(long id) error:" + e);
		}
	}
	
	public DBRow[] getTitleForCategory(long id) throws Exception{
		
		try {
			
			String sql = "select tpc_title_id title from title_product_catalog where tpc_product_catalog_id = "+id;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getTitleForCategory(long id) error:" + e);
		}
	}
	
	public DBRow[] getTitleForSubCatagory(long id) throws Exception{
		
		try {
			
			String sql = "select tpc.tpc_title_id title from product_catalog pc INNER JOIN title_product_catalog tpc on pc.id = tpc.tpc_product_catalog_id where pc.parentid = "+id+" GROUP BY tpc.tpc_title_id";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getTitleForSubCatagory(long id) error:" + e);
		}
	}
	
	public DBRow[] getTitleForCategoryProduct(long id) throws Exception{
		
		try {
			
			String sql = "select tp.tp_title_id title from product p INNER JOIN title_product tp on p.pc_id = tp.tp_pc_id where p.catalog_id = "+id+" group by tp.tp_title_id";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getTitleForCategoryProduct(long id) error:" + e);
		}
	}
	
	public DBRow[] getTitleForCategoryProductAndSubCatagory(long id) throws Exception{
		
		try {
			
			String sql = "select tp.tp_title_id title from product p INNER JOIN title_product tp on p.pc_id = tp.tp_pc_id where p.catalog_id = "+id+" group by tp.tp_title_id UNION select tpc.tpc_title_id title from product_catalog pc INNER JOIN title_product_catalog tpc on pc.id = tpc.tpc_product_catalog_id where pc.parentid = "+id+" GROUP BY tpc.tpc_title_id";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getTitleForCategoryProductAndSubCatagory(long id) error:" + e);
		}
	}
	
	public void deleteTitleProductCatalog(long id,long title) throws Exception{
		
		try {
			
			String sql = " where tpc_title_id = "+title+" and tpc_product_catalog_id = "+ id;
			
			dbUtilAutoTran.delete(sql, "title_product_catalog");
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.deleteTitleProductCatalog(long id,long title) error:" + e);
		}
	}
	
	public DBRow getParentCategory(long id) throws Exception{
		
		try {
			
			String sql = "select * from  product_catalog where id = (select parentid from product_catalog where id = "+id+")";
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getParentCategory(long id) error:" + e);
		}
	}
	
	public void insertTitleProductCatalog(DBRow data) throws Exception{
		
		try {
			
			dbUtilAutoTran.insert("title_product_catalog", data);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.insertTitleProductCatalog(long id) error:" + e);
		}
	}
	
	public DBRow[] getTitleForLineByLineId(long id) throws Exception{
		
		try {
			
			String sql = "select tpl_title_id title from title_product_line where tpl_product_line_id = " + id;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getTitleForLineByLineId(long id) error:" + e);
		}
	}
	
	public DBRow[] getTitleForLineCategoryRelation(long id) throws Exception{
		
		try {
			
			String sql = "select tpc.tpc_title_id title from product_catalog pc INNER JOIN title_product_catalog tpc on pc.id = tpc.tpc_product_catalog_id where pc.product_line_id = "+id+" and pc.parentid = 0 GROUP BY tpc.tpc_title_id";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getTitleForLineCategoryRelation(long id) error:" + e);
		}
	}
	
	public void deleteTitleProductLine(long id,long title) throws Exception{
		
		try {
			
			String sql = " where tpl_title_id = "+title+" and tpl_product_line_id = "+id;
			
			dbUtilAutoTran.delete(sql, "title_product_line");
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.deleteTitleProductLine(long id,long title) error:" + e);
		}
	}
	
	public void deleteTitleProductLineByline(long id) throws Exception{
		
		try {
			
			String sql = " where tpl_product_line_id = "+id;
			
			dbUtilAutoTran.delete(sql, "title_product_line");
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.deleteTitleProductLineByline(long id) error:" + e);
		}
	}
	
	public DBRow[] getTitleForLine(long id) throws Exception{
		
		try {
			
			String sql = "select tpl_title_id title from title_product_line where tpl_product_line_id = (select product_line_id from product_catalog where id = "+id+") group by tpl_title_id";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getTitleForLine(long id) error:" + e);
		}
	}
	
	public void insertTitleProductLine(DBRow data) throws Exception{
		
		try {
			
			dbUtilAutoTran.insert("title_product_line", data);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.insertTitleProductLine(long id) error:" + e);
		}
	}
	
	public DBRow getProductCategory(long id) throws Exception{
		
		try {
			
			String sql = "select * from product_catalog where id = "+id;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getProductCategory(long id) error:" + e);
		}
	}
	
	public DBRow[] getProductCategoryTree(long id) throws Exception{
		
		try {
			
			DBRow param = new DBRow();
			param.put("id", id);
			
			String sql = "select id,title name,parentid parent,sort from product_catalog where parentid = ? order by sort";
			
			DBRow[] category = dbUtilAutoTran.selectPreMutliple(sql, param);
			
			for(DBRow one : category){
				
				DBRow[] sub = this.getProductCategoryTree(one.get("id",0));
				
				if(sub != null && sub.length > 0){
					
					one.put("children", sub);
				}
			}
			
			return category;
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getProductCategory(long id) error:" + e);
		}
	}
	
	public DBRow getProductCatalogSort(long parentid) throws Exception {
		
		try {
				
			String sql = "select Max(sort)+1 as sort from product_catalog where parentid = "+parentid+" ORDER BY sort";
			
			return dbUtilAutoTran.selectSingle(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.getProductCatalogSort(long parentid) error:" + e);
		}
	}
	
	public DBRow[] getProductCatalogByLineId(long id) throws Exception {
		
		try {
			
			String sql = "select * from product_catalog pc where pc.product_line_id="+id;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.getProductCatalogByLineId(long id) error:" + e);
		}
	}
	
	public DBRow[] getMaxProductCatalogByLineId(long id) throws Exception {
		
		try {
			
			String sql = "select * from product_catalog pc where parentid = 0 and pc.product_line_id="+id;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.getProductCatalogByLineId(long id) error:" + e);
		}
	}
	
	public DBRow getTitleProductCatalog(long id,long titleId) throws Exception {
		
		try {
				
			String sql = "select * from title_product_catalog where tpc_product_catalog_id = "+id+" and tpc_title_id = "+titleId;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.getTitleProductCatalog(long id,long titleId) error:" + e);
		}
	}
	
	public DBRow[] getTitleProductCatalog(long id) throws Exception {
		
		try {
				
			String sql = "select * from title_product_catalog where tpc_product_catalog_id = "+id;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.getTitleProductCatalog(long id) error:" + e);
		}
	}
	
	public DBRow getTitleProductLine(long id,long titleId) throws Exception {
		
		try {
				
			String sql = "select * from title_product_line where tpl_product_line_id ="+id+" and tpl_title_id = "+titleId;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.getTitleProductLine(long id,long titleId) error:" + e);
		}
	}
	
	/***************************************************************************************************************************************/
	
	public DBRow[] getCustomerTitleByCategory(long id) throws Exception{
		
		try {
			
			String sql = "select tpc_title_id title,tpc_customer_id customer from title_product_catalog where tpc_product_catalog_id = "+id;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getCustomerTitleByCategory(long id) error:" + e);
		}
	}
	
	public DBRow[] getCustomerTitleLowerByCategory(long id) throws Exception{
		
		try {
			
			String sql = "select tp.tp_title_id title,tp.tp_customer_id customer from product p INNER JOIN title_product tp on p.pc_id = tp.tp_pc_id where p.catalog_id = "+id+" group by tp.tp_title_id,tp.tp_customer_id UNION select tpc.tpc_title_id title,tpc.tpc_customer_id customer from product_catalog pc INNER JOIN title_product_catalog tpc on pc.id = tpc.tpc_product_catalog_id where pc.parentid = "+id+" GROUP BY tpc.tpc_title_id,tpc.tpc_customer_id";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getCustomerTitleLowerByCategory(long id) error:" + e);
		}
	}
	
	public void deleteTitleProductCatalog(long id, long title, long customer) throws Exception{
		
		try {
			
			String sql = " where tpc_title_id = "+title+" and tpc_product_catalog_id = "+ id + " and tpc_customer_id = "+customer;
			
			dbUtilAutoTran.delete(sql, "title_product_catalog");
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.deleteTitleProductCatalog(long id, long title, long customer) error:" + e);
		}
	}
	
	public DBRow[] getCustomerTitleLowerByLine(long id) throws Exception{
		
		try {
			
			String sql = "select tpc.tpc_title_id title ,tpc.tpc_customer_id customer from product_catalog pc INNER JOIN title_product_catalog tpc on pc.id = tpc.tpc_product_catalog_id where pc.product_line_id = "+id+" and pc.parentid = 0 GROUP BY tpc.tpc_title_id,tpc.tpc_customer_id";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getCustomerTitleLowerByLine(long id) error:" + e);
		}
	}
	
	public DBRow[] getCustomerTitleByLine(long id) throws Exception{
		
		try {
			
			String sql = "select tpl_title_id title ,tpl_customer_id customer from title_product_line where tpl_product_line_id = " + id;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getCustomerTitleByLine(long id) error:" + e);
		}
	}
	
	public void deleteTitleProductLine(long id,long title,long customer) throws Exception{
		
		try {
			
			String sql = " where tpl_title_id = "+title+" and tpl_product_line_id = "+ id +" and tpl_customer_id = "+customer;
			
			dbUtilAutoTran.delete(sql, "title_product_line");
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.deleteTitleProductLine(long id,long title,long customer) error:" + e);
		}
	}
	
	public DBRow getTitleProductCatalog(long id, long titleId, long customer) throws Exception {
		
		try {
				
			String sql = "select * from title_product_catalog where tpc_product_catalog_id = "+id+" and tpc_title_id = "+titleId+" and tpc_customer_id = "+customer;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.getTitleProductCatalog(long id, long titleId, long customer) error:" + e);
		}
	}
	
	public DBRow[] getProductByCategory(long id) throws Exception {
		
		try {
			
			String sql = " select * from product where catalog_id = "+id;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.getProductByCategory(long id) error:" + e);
		}
	}
	
	public void updateProduct(long id ,DBRow param) throws Exception {
		
		try {
			
			String sql = " where pc_id = "+id;
			
			dbUtilAutoTran.update(sql, "product", param);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.updateProduct(long id ,DBRow param) error:" + e);
		}
	}
	
	public DBRow getTitle(long id) throws Exception {
		
		try {
			
			String sql = " select * from title where title_id = "+id;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.getTitle(long id) error:" + e);
		}
	}
	
	public DBRow[] getTitle() throws Exception {
		
		try {
			
			String sql = " select title_id id,title_name name from title order by name";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.getTitle() error:" + e);
		}
	}

	public DBRow getCustomer(long id) throws Exception {
		
		try {
			
			String sql = " select * from customer_id cus where EXISTS (select * from product_storage_catalog psc where psc.storage_type_id = cus.customer_key and psc.storage_type = '7') and cus.customer_key = "+id;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.getCustomer(long id) error:" + e);
		}
	}
	
	public DBRow[] getCustomer() throws Exception {
		
		try {
			
			String sql = " select customer_key id,customer_id name from customer_id cus where EXISTS (select * from product_storage_catalog psc where psc.storage_type_id = cus.customer_key and psc.storage_type = '7') order by name ";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorCatalogMgr.getCustomer() error:" + e);
		}
	}
	
	public DBRow getProduct(long id) throws Exception{

        try {

            String sql = "select * from product where pc_id = "+id;

            return dbUtilAutoTran.selectSingle(sql);

        }catch (Exception e){
            throw new Exception("FloorCatalogMgr.getProduct(long id) error:" + e);
        }
    }
	
	/**
	 * 修改商品分类后
	 * 
	 * 1.原分类以及父分类与customer/title的关系[是否应该删除]
	 * 2.现分类以及父分类与customer/title的关系[是否应该添加]
	 * 3.原产品线与customer/title的关系[是否需要删除]
	 * 4.现产品线与customer/title的关系[是否需要添加]
	*/
	public void updateCustomerTitleAndCategoryLineRelation(long pcId,long categoryId) throws Exception{
		
		DBRow product = this.getProduct(pcId);
		
		//1.原分类以及父分类与title的关系[是否应该删除]
		if(categoryId != 0){
			this.customerTitleAndCategory(categoryId);
		}
		
		//2.现分类以及父分类与title的关系[是否应该添加]
		this.customerTitleAndCategory(product.get("catalog_id", 0L));
		
		//3.原产品线与title的关系[是否需要删除]
		if(categoryId != 0){
			
			DBRow category = this.getProductCatalogById(categoryId);
			
			this.customerTitleAndLine(category.get("product_line_id",0L));
		}
		
		//4.现产品线与title的关系[是否需要添加]
		DBRow catalog = this.getProductCatalogById(product.get("catalog_id", 0L));
		
		if(catalog != null ){
			
			this.customerTitleAndLine(catalog.get("product_line_id",0L));
		}
	}
	
	//1.2.品牌商/title 与分类父分类的关系[是否需要删除/是否需要添加]
	public void customerTitleAndCategory(long categoryId) throws Exception{
		
		//如果存在分类
		if(categoryId != 0){
			
			//查询分类与customer/title的关系
			DBRow[] title1 = this.getCustomerTitleByCategory(categoryId);
			
			//查询分类下分类和商品与customer/title的关系
			DBRow[] title2 = this.getCustomerTitleLowerByCategory(categoryId);
			
			//比较分类与分类下商品和分类的customer/title
			DBRow[] comRes = this.compareCusomerTitle(title1, title2);
			
			for(DBRow one : comRes){
				
				if(one.get("flag", "").equals("insert")){
					
					DBRow param = new DBRow();
					param.put("tpc_title_id", one.get("title",0));
					param.put("tpc_product_catalog_id", categoryId);
					param.put("tpc_customer_id", one.get("customer",0));
					
					this.insertTitleProductCatalog(param);
					
				} else {
					
					this.deleteTitleProductCatalog(categoryId,one.get("title",0),one.get("customer",0));
				}
			}
			
			//判断是否原父分类依然有父分类
			DBRow parentCategory = this.getParentCategory(categoryId);
			
			if(parentCategory != null){
				
				this.customerTitleAndCategory(parentCategory.get("id",0L));
			}
		}
	}
	
	//3.4.品牌商/title 与产品线的关系[是否需要删除][是否需要添加]
	public void customerTitleAndLine(long lineId) throws Exception{
		
		//如果存在产品线
		if(lineId != 0){
			
			//查询产品线与customer/title的关系
			DBRow[] title1 = this.getCustomerTitleByLine(lineId);
			//查询产品线下分类与customer/title的关系
			DBRow[] title2 = this.getCustomerTitleLowerByLine(lineId);
			
			//比较分类与分类下商品和分类的customer/title
			DBRow[] comRes = this.compareCusomerTitle(title1, title2);
			
			for(DBRow one : comRes){
				
				if(one.get("flag", "").equals("insert")){
					
					DBRow param = new DBRow();
					param.put("tpl_title_id", one.get("title",0));
					param.put("tpl_product_line_id", lineId);
					param.put("tpl_customer_id", one.get("customer",0));
					
					this.insertTitleProductLine(param);
					
				}else{
					
					this.deleteTitleProductLine(lineId,one.get("title",0L));
				}
			}
		}
	}
	
	public DBRow[] compareCusomerTitle(DBRow[] row1, DBRow[] row2) throws Exception{
		
		List<DBRow> result = new ArrayList<DBRow>();
		
		for(DBRow one : row1){
			
			boolean find = false;
			
			for(DBRow two : row2){
				
				if(one.get("customer",0) == two.get("customer",-1) && one.get("title",0) == two.get("title",-1)){
					
					find = true;
				}
			}
			
			if(!find){
				
				DBRow delRow = new DBRow();
				delRow.put("customer", one.get("customer",0));
				delRow.put("title", one.get("title",0));
				delRow.put("flag", "delete");
				
				result.add(delRow);
			}
		}
		
		for(DBRow one : row2){
			
			boolean find = false;
			
			for(DBRow two : row1){
				
				if(one.get("customer",0) == two.get("customer",-1) && one.get("title",0) == two.get("title",-1)){
					
					find = true;
				}
			}
			
			if(!find){
				
				DBRow delRow = new DBRow();
				delRow.put("customer", one.get("customer",0));
				delRow.put("title", one.get("title",0));
				delRow.put("flag", "insert");
				
				result.add(delRow);
			}
		}
		
		return result.toArray(new DBRow[result.toArray().length]);
	}
}