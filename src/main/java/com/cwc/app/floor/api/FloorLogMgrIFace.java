package com.cwc.app.floor.api;

import us.monoid.json.JSONObject;

public interface FloorLogMgrIFace {
	/**
	 * 
	 * @async  参数含义:是否异步执行；如果为true，则将请求放入一个异步对列中，然后由监听者负责实际提交日志的动作，这种情况下返回null
	 * 如果为false，表示立即执行提交日志的动作，这种情况下返回提交后获得的日志ID；以下所有方法的async参数均同一含义！ 
	 */
	/**
	 * 记录日志文件到mongo
	 * @param modelName	日志记录名称,应取LogServModelKey值
	 * @param logJson	日志内容, 该方法默认添加创建时间post_date字段,其值为当前系统时间(UTC)
	 * @param async	是否异步
	 * @return
	 * @throws Exception
	 * @author CHENCHEN
	 */
	public String addLog(String modelName, JSONObject logJson, boolean async) throws Exception;

	public String addLog(Object logBean, boolean async) throws Exception;
}
