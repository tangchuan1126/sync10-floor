package com.cwc.app.floor.api.sbb;

import java.util.Map;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

/**
 * 角色管理ROLE
 * @since Sync10-ui 1.0
 * @author subin
 * */
public class FloorRoleMgrSbb{
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	/**
	 * 角色管理 >> 查询角色信息
	 * @param 分页
	 * @return 角色列表
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow[] getSearchRoleList(Map<String,Object> parameter) throws Exception{
		
		try {
			String searchConditions = (String) parameter.get("searchConditions");
			
			String sql = "select * from admin_group where 1=1 ";
			
			if(!searchConditions.equals("")){
				
				sql += " and (name like '%"+searchConditions+"%')";
			}
			
			sql += " order by adgid desc ";
			
			return dbUtilAutoTran.selectMutliple(sql, (PageCtrl)parameter.get("PageCtrl"));
			
		}catch (Exception e){
			throw new Exception("FloorRoleMgrSbb.getAllRoleList(Map<String,Object> parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 插入角色
	 * @param 角色信息
	 * @return 角色ID
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public long insertRole(DBRow parameter) throws Exception{
		
		try {
			
			return dbUtilAutoTran.insertReturnId("admin_group", parameter);
		
		}catch (Exception e){
			throw new Exception("FloorRoleMgrSbb.insertRole(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 修改角色
	 * @param 角色信息
	 * @return 是否成功
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public int updateRole(DBRow parameter) throws Exception{
		
		try {
			
			String sql = " where adgid = "+parameter.get("adgid", -1L);
			
			return dbUtilAutoTran.update(sql, "admin_group", parameter);
			
		}catch (Exception e){
			throw new Exception("FloorRoleMgrSbb.updateRole(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 删除角色
	 * @param 角色ID
	 * @return 是否成功
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public int deleteRole(long id) throws Exception{
		
		try {
			
			String sql = " where adgid = "+id;
			
			return dbUtilAutoTran.delete(sql, "admin_group");
			
		}catch (Exception e){
			throw new Exception("FloorRoleMgrSbb.deleteRole(long id) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 查询角色信息
	 * @param 角色名
	 * @return 角色列表
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow getRoleNameByRoleName(String parameter) throws Exception{
		
		try {
			
			String sql = "select * from admin_group where name = '"+parameter+"'";
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorRoleMgrSbb.roleValidator(String parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 查询角色信息除了自身
	 * @param 角色名,角色ID
	 * @return 角色
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getRoleByRoleNameBesideSelf(long adgid,String parameter) throws Exception{
		
		try {
			
			String sql = "select * from admin_group where name= '"+parameter+"' and adgid != "+adgid;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorRoleMgrSbb.getRoleByRoleNameBesideSelf(long adgid,String parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理>> 获取角色菜单单一权限
	 * @param DBRow
	 * @return DBRow[]
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getRoleMenuPermission(DBRow parameter) throws Exception{
		
		try {
			
			String sql = "select * from turboshop_control_tree_map where adgid = "+parameter.get("adgid", -1)+" and ctid = "+parameter.get("ctid", -1);
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorRoleMgrSbb.getRoleMenuPermission(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 获取角色页面单一权限
	 * @param DBRow
	 * @return DBRow[]
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getRolePagePermission(DBRow parameter) throws Exception{
		
		try {
			
			String sql = "select * from admin_group_auth where adgid = "+parameter.get("adgid", -1)+" and ataid = "+parameter.get("ataid", -1);
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorRoleMgrSbb.getRolePagePermission(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 通过ID获得角色信息
	 * @param 账号ID
	 * @return 单个账号信息
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow getRoleById(long id) throws Exception{
		
		try {
			
			String sql = "select * from admin_group where adgid = "+id;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorRoleMgrSbb.getRoleById(long id) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 删除权限
	 * @param 角色ID
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void deleteRolePermission(long id) throws Exception{
		
		try {
			
			String sql = " where adgid = "+id;
			
			dbUtilAutoTran.delete(sql, "turboshop_control_tree_map");
			dbUtilAutoTran.delete(sql, "admin_group_auth");
			
		}catch (Exception e){
			throw new Exception("FloorRoleMgrSbb.deleteRolePermission(long id) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 添加菜单权限
	 * @param DBRow
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public void insertRoleMenuPermission(DBRow parameter) throws Exception{
		
		try {
			
			dbUtilAutoTran.insert("turboshop_control_tree_map", parameter);
			
		}catch (Exception e){
			throw new Exception("FloorRoleMgrSbb.insertRoleMenuPermission(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 添加页面权限
	 * @param DBRow
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void insertRolePagePermission(DBRow parameter) throws Exception{
		
		try {
			
			dbUtilAutoTran.insert("admin_group_auth", parameter);
			
		}catch (Exception e){
			throw new Exception("FloorRoleMgrSbb.insertRolePagePermission(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 角色管理 >> 验证部门下是否存在账号
	 * @param 部门ID
	 * @return 
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public boolean existDeptForAccount(long id) throws Exception {
		
		try {
			
			boolean result = false;
			
			String sql = "select * from admin_department where department_id = "+id;
			
			DBRow[] row = dbUtilAutoTran.selectMutliple(sql);
			
			if(row != null && row.length > 0){
				
				result = true;
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("FloorRoleMgrSbb.existDeptForAccount(long id) error:" + e);
		}
	}
	
	
	
	 /**
     * 角色管理 >> 添加部门与职务关系
     * @param DBRow
     * @return null
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public void insertAdminGroupPost(DBRow parameter) throws Exception{

        try {

            dbUtilAutoTran.insert("admin_group_post", parameter);

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.insertAdminGroupPost(DBRow parameter) error:" + e);
        }
    }

    /**
     * 角色管理 >> 删除本部门职务
     * @param 角色ID
     * @return null
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public void deleteAdminGroupPostByAdpId(long adp_id ,long adg_id) throws Exception{

        try {

            String sql = " where adp_id = " + adp_id +" and adg_id = " + adg_id;

            dbUtilAutoTran.delete(sql, "admin_group_post");

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.deleteAdminGroupPostByAdpId(long adp_id) error:" + e);
        }
    }

    /**
     * 角色管理 >> 查询本部门关联职务
     * @param 部门ID
     * @return 职务[]
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public DBRow[] queryAdminGroupPostByAdgId(long adg_id) throws Exception{

        try {

            String sql = "select * from admin_group_post where adg_id = "+adg_id+" order by id desc";

            return dbUtilAutoTran.selectMutliple(sql);

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.queryAdminGroupPostByAdgId(long adg_id) error:" + e);
        }
    }

    /**
     * 角色管理 >> 查询所有职务
     * @param 部门ID
     * @return 职务[]
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public DBRow[] queryAdminPostAll() throws Exception{

        try {

            String sql = "select * from admin_post";

            return dbUtilAutoTran.selectMutliple(sql);

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.queryAdminPostAll() error:" + e);
        }
    }

    /**
     * 角色管理 >> 查询所有职务排除部门关联的职务
     * @param 部门ID
     * @return 职务[]
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public DBRow[] queryAdminPostByParams(String params) throws Exception{

        try {

            String sql = "select * from admin_post where id not in ( " + params +")";

            return dbUtilAutoTran.selectMutliple(sql);

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.queryAdminPostByParams(String parms) error:" + e);
        }
    }

    /**
     * 角色管理 >> 根据职务ID查询职务信息
     * @param 职务ID
     * @return 职务[]
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public DBRow[] queryIdExistAdminPostByParams(String params) throws Exception{

        try {

            String sql = "select * from admin_post where id in ( " + params +")";

            return dbUtilAutoTran.selectMutliple(sql);

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.deleteAdminGroupPostByAdpId(String parms) error:" + e);
        }
    }

    /**
     * 角色管理 >> 根据职务ID查询职务信息
     * @param 职务ID
     * @return 职务[]
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public DBRow[] queryAdminPostById(long id) throws Exception{

        try {

            String sql = "select * from admin_post where id = " + id;

            return dbUtilAutoTran.selectMutliple(sql);

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.queryAdminPostById(long id) error:" + e);
        }
    }

    /**
     * 角色管理 >> 根据职务ID查询职务信息
     * @param 职务ID
     * @return 职务[]
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public DBRow getAdminPostById(long id) throws Exception{

        try {

            String sql = "select * from admin_post where id = " + id;

            return dbUtilAutoTran.selectSingle(sql);

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.getAdminPostById(long id) error:" + e);
        }
    }

    /**
     * 角色管理 >> 根据adid查询创建人信息
     * @param adid
     * @return 账户[]
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public DBRow getAdminByAdid(long adid) throws Exception{

        try {

            String sql = "select * from admin where adid = " + adid;

            return dbUtilAutoTran.selectSingle(sql);

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.getAdminByAdid(long adid) error:" + e);
        }
    }

    /**
     * 角色管理 >> 查询职务信息
     * @param 分页
     * @return 职务列表
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public DBRow[] querySearchPostList(Map<String,Object> parameter) throws Exception{

        try {
            String searchConditions = (String) parameter.get("searchConditions");

            String sql = "select * from admin_post where 1=1 ";

            if(!searchConditions.equals("")){

                sql += " and (name like '%"+searchConditions+"%')";
            }

            sql += " order by id desc ";

            return dbUtilAutoTran.selectMutliple(sql, (PageCtrl)parameter.get("PageCtrl"));

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.getSearchPostList(Map<String,Object> parameter) error:" + e);
        }
    }

    /**
     * 角色管理 >> 查询职务信息除了自身(modify时验证)
     * @param 职务名,职务ID
     * @return 职务
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public DBRow[] queryPostByNameNotId(long id,String name) throws Exception{

        try {

            String sql = "select * from admin_post where name= '"+name+"' and id != "+id;

            return dbUtilAutoTran.selectMutliple(sql);

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.queryPostByNameNotId(long id,String name) error:" + e);
        }
    }

    /**
     * 角色管理 >> 修改职务
     * @param 职务信息
     * @return 是否成功
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public int updatePost(DBRow parameter) throws Exception{

        try {

            String sql = " where id = "+parameter.get("id", -1L);

            return dbUtilAutoTran.update(sql, "admin_post", parameter);

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.updatePost(DBRow parameter) error:" + e);
        }
    }

    /**
     * 角色管理 >> 插入职务
     * @param 职务信息
     * @return 职务ID
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public long insertPost(DBRow parameter) throws Exception{

        try {

            return dbUtilAutoTran.insertReturnId("admin_post", parameter);

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.insertPost(DBRow parameter) error:" + e);
        }
    }

    /**
     * 角色管理 >> 查询职务信息(add时验证)
     * @param 职务名
     * @return 职务
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public DBRow getPostByName(String name) throws Exception{

        try {

            String sql = "select * from admin_post where name = '"+name+"'";

            return dbUtilAutoTran.selectSingle(sql);

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.roleValidator(String parameter) error:" + e);
        }
    }

    /**
     * 角色管理 >> 查询职务表中最大ID值
     * ｛因 admin_post表中id非自增，有特殊处理
     *      所以查询出最大ID+1作为插入表中的ID
     * ｝
     * @param 职务名
     * @return 职务
     * @since Sync10-ui 1.0
     * @author Yuanxinyu
     **/
    public DBRow getAdminPostMaxId() throws Exception{

        try {

            String sql = "select max(id) as maxId from admin_post";

            return dbUtilAutoTran.selectSingle(sql);

        }catch (Exception e){
            throw new Exception("FloorRoleMgrSbb.getAdminPostMaxId() error:" + e);
        }
    }

	
	
}
