package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

/**
 * Task processing 过程中LoadBar使用情况
 * @author win7zr
 *
 */
public class FloorLoadBarUseMgrZr {
	
	private DBUtilAutoTran dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
	public long addLoadBarUse(DBRow row) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("load_bar_use");
			return dbUtilAutoTran.insertReturnId(tableName, row);
		}catch (Exception e) {
			throw new Exception("FloorLoadBarUseMgrZr.addLoadBarUse(DBRow):"+e);
		}
	}
	
	public int updateLoadBarUse(long load_bar_use_id , DBRow updateRow) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("load_bar_use");
			return dbUtilAutoTran.update("where load_bar_use_id="+load_bar_use_id, tableName, updateRow);
		}catch(Exception  e){
			throw new Exception("FloorLoadBarUseMgrZr.updateLoadBarUse(DBRow):"+e);
		}
	}
	
	public DBRow getLoadBarUse(long equipment_id , long load_bar_id ) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("load_bar_use");
			String sql = "select * from " + tableName  + " where equipment_id="+equipment_id + " and load_bar_id="+load_bar_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorLoadBarUseMgrZr.getLoadBarUse(DBRow):"+e);
		}
	}
	/**
	 * 得到一种LoadBar在某个设备下一共使用了多少个 
	 * @param equipment_id
	 * @param load_bar_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月11日
	 */
	public int getLoadBarUseCount(long equipment_id , long load_bar_id ) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("load_bar_use");
			String sql = " select sum(count) as total  from " + tableName + " where  equipment_id= ? and load_bar_id=? ";
			DBRow para = new DBRow();
			para.add("equipment_id",equipment_id);
			para.add("load_bar_id",load_bar_id);
			DBRow data = dbUtilAutoTran.selectPreSingle(sql, para);
			return data != null ? data.get("total", 0) : 0 ;
		}catch(Exception e){
			throw new Exception("FloorLoadBarUseMgrZr.getLoadBarUse(equipment_id,load_bar_id):"+e);
		}
	}
	public DBRow[] getLoadBarGroupInfo(long equipment_id , long entry_id) throws Exception{
		try{
			String sql = "select * , sum(count) as total from load_bar_use as uses "
					 + "  LEFT JOIN load_bar lb on  lb.load_bar_id  = uses.load_bar_id "
					 + "  where equipment_id = "+equipment_id+" and dlo_id = "+entry_id+" GROUP BY lb.load_bar_id ";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorLoadBarUseMgrZr.getLoadBarGroupInfo(equipment_id,entry_id):"+e);
		}
	}
	
	public void deleteLoadBarUse(long equipment_id , long load_bar_id) throws Exception{
		try{
			dbUtilAutoTran.delete(" where equipment_id="+equipment_id + " and load_bar_id="+load_bar_id, "load_bar_use");
		}catch(Exception e){
			throw new Exception("FloorLoadBarUseMgrZr.deleteLoadBarUse(equipment_id,load_bar_id):"+e);
		}
	}
}
