package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorScheduleSetMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public DBRow getOnlyOneRow() throws Exception{
		try{
			return dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("schedule_set"));
		}catch(Exception e){
			throw new Exception("FloorScheduleSetMgrZr.getOnlyOneRow():"+e);
		}
	}
	
	
	public void updateSchedule(long id,DBRow row) throws Exception {
		try{
			dbUtilAutoTran.update("where schedule_set_id="+id, ConfigBean.getStringValue("schedule_set"), row);		
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.updateSchedule(row):"+e);
		}
	}
	
}
