package com.cwc.app.floor.api;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorJbpmMgr
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void fixSubTaskId(String executionId)
		throws Exception
	{
		try
		{
			DBRow detail = this.getDetailExecutionById(executionId);
			
			DBRow newID = new DBRow();
			newID.add("HISACTINST_", detail.get("HISACTINST_", 0l)-1);
			
			dbUtilAutoTran.update("where ID_='"+executionId+"'", "jbpm4_execution", newID);
		}
		catch (Exception e)
		{
			throw new Exception("fixSubTaskId(row) error:" + e);
		}
	}
	
	public DBRow getDetailExecutionById(String executionId)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("ID_",executionId);
			return(dbUtilAutoTran.selectPreSingle("select * from jbpm4_execution where ID_=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.getDetailExecutionById(row) error:" + e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
