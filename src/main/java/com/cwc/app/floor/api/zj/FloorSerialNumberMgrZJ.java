package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorSerialNumberMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加序列号
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public void addSerialNumber(DBRow dbrow)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.insert(ConfigBean.getStringValue("serial_number"),dbrow);
		}
		catch (Exception e)
		{
			throw new Exception("FloorSerialNumberMgrZJ addSerialNumber error:"+e);
		}
	}

	/**
	 * 获得序号
	 * @param supplier_id
	 * @param serial_number
	 * @return
	 * @throws Exception
	 */
	public DBRow getNumberLikeSerialNumber(long supplier_id,String serial_number)
		throws Exception
	{
		try 
		{
			String sql = "select max(number) as maxNumber from "+ConfigBean.getStringValue("serial_number")+" where supplier_id =? and serial_number like ?";
			
			DBRow para = new DBRow();
			para.add("supplier_id",supplier_id);
			para.add("serial_number",serial_number+"%");

			return (dbUtilAutoTran.selectPreSingle(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorSerialNumberMgrZJ getNumberLikeSerialNumber error:"+e);
		}
	}
	
	/**
	 * 通过序列号获得序列号的详细信息
	 * @param serial_number
	 * @return
	 * @throws Exception
	 */
	public DBRow getSerialNumberDetail(long serial_number)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("serial_number")+" where serial_number = ?";
			
			DBRow para = new DBRow();
			para.add("serial_number",serial_number);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorSerialNumberMgrZJ getSerialNumberDetail error:"+e);
		}
	}
	
	public long addSerialProduct(DBRow row)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("serial_product"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("floorSerialNumberMgrZJ addSerialProduct error:"+e);
		}
	}
	
	/**
	 * 修改序列号
	 * @param serial_product_id
	 * @param para
	 * @throws Exception
	 */
	public void modSerialProduct(long serial_product_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where serial_product_id="+serial_product_id,ConfigBean.getStringValue("serial_product"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSerialNumberZJ modSerialProduct error:"+e);
		}
	}
	
	public DBRow getDetailSerialProduct(String sn)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("serial_product")+ " where serial_number = ?";
			
			DBRow para = new DBRow();
			para.add("serial_number",sn);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSerialNumberMgrZJ getDetailSerialProduct error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
