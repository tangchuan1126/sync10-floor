package com.cwc.app.floor.api.zr;


import com.cwc.app.key.MoreLessOrEqualKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorBoxSkuPsMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	public DBRow[] getAllBoxProductByPage(PageCtrl pageCtrl) throws Exception{
		try{
 			 
			 StringBuffer sql = new StringBuffer();
			 
			 sql.append(	"select  ps.* , psc.title,bt.* from box_ps as ps " )
			 .append("      left join product_storage_catalog  as psc on ps.to_ps_id = psc.id ")
			 .append("  left join box_type as bt on ps.box_type_id = bt.box_type_id order by ps.to_ps_id desc , box_sort ");
 
			 
			 return dbUtilAutoTran.selectMutliple(sql.toString());
 		}catch(Exception e){
			throw new Exception("FloorBoxProductMgrZr.getAllBoxProductByPage(pageCtrl):"+e);
		}
	}
	public long addBoxSkuPs(DBRow row) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("lp_title_ship_to");	
			return dbUtilAutoTran.insertReturnId(tableName, row);
		}catch (Exception e) {
			throw new Exception("FloorBoxProductMgrZr.addBoxSkuPs(row):"+e);
 		}
	}
	public void deleteBoxSkuPs(long box_type_ps_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("box_ps");	
			dbUtilAutoTran.delete(" where box_type_ps_id =" +box_type_ps_id, tableName);
		}catch (Exception e) {
			throw new Exception("FloorBoxProductMgrZr.deleteBoxSkuPs(box_type_ps_id):"+e);
		}
	}
	public DBRow getBoxSkuPsByPsIdAndPcid(long pc_id , long ps_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("box_ps");
			StringBuffer sql = new StringBuffer("select * from ").append(tableName);
			sql.append(" where box_pc_id = ? ").append(" and to_ps_id =? and box_sort = ?");
			DBRow para = new DBRow();
			para.add("box_pc_id", pc_id);
			para.add("to_ps_id", ps_id);
			para.add("box_sort", 1);
			return dbUtilAutoTran.selectPreSingle(sql.toString(), para);
		}catch (Exception e) {
			throw new Exception("FloorBoxProductMgrZr.getBoxSkuPsByPsIdAndPcid(pc_id,ps_id):"+e);
		}
	}
	public void updateBoxSkuPs(long box_type_ps_id , DBRow row) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("lp_title_ship_to");
 		 
			dbUtilAutoTran.update(" where lp_title_ship_id ="+box_type_ps_id, tableName, row);
		}catch (Exception e) {
			throw new Exception("FloorBoxProductMgrZr.updateBoxSkuPsBy(box_type_ps_id,row):"+e);
 		}
	}
	public DBRow[] getAllBoxSkuByPcId(long pc_id) throws Exception {
		try{
			StringBuffer sql = new StringBuffer("");
			sql.append("	select  ps.* , psc.title,bt.* from box_ps as ps ");
			sql.append(" left join product_storage_catalog  as psc on ps.to_ps_id = psc.id ");
			sql.append(" left join box_type as bt on ps.box_type_id = bt.box_type_id ");
			sql.append(" where ps.box_pc_id =? ");
			sql.append(" order by ps.to_ps_id desc , box_sort ");
			DBRow  para = new DBRow();
			para.add("box_pc_id", pc_id);
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
		}catch (Exception e) {
			throw new Exception("FloorBoxProductMgrZr.getAllBoxSkuByPcId(pc_id,row):"+e);
		}
	}
	public int getMaxIndexBy(long pc_id , long ps_id) throws Exception{
		try{
			int index =  0 ;
			String tableName = ConfigBean.getStringValue("lp_title_ship_to");
			String sql = "select max(ship_to_sort) as max_sort from "+tableName+" where lp_pc_id =? and ship_to_id =? ";
			DBRow para = new DBRow();
			para.add("lp_pc_id", pc_id);
			para.add("ship_to_id", ps_id);
			DBRow row = dbUtilAutoTran.selectPreSingle(sql, para);
			if(row != null){
				index = row.get("max_sort", 0);
			}
			return index ;
		}catch (Exception e) {
			throw new Exception("FloorBoxProductMgrZr.getMaxIndexBy(pc_id,ps_id):"+e);
 		}
	}
	
	public int countNumBy(long pc_id , long ps_id , long box_type_id) throws Exception{
		try{
			int count =  0 ;
			String tableName = ConfigBean.getStringValue("box_ps");
			StringBuffer sql = new StringBuffer();
			sql.append("select count(*) as count_sum from ").append(tableName)
			.append(" where box_pc_id = ? ").append(" and to_ps_id = ? ")
			.append(" and box_type_id=? ");
			DBRow para = new DBRow();
			para.add("box_pc_id", pc_id);
			para.add("to_ps_id", ps_id);
			para.add("box_type_id", box_type_id);
			DBRow result = dbUtilAutoTran.selectPreSingle(sql.toString(), para);
			if(result != null){
				count = result.get("count_sum", 0);
			}
			return count ;
		}catch (Exception e) {
			throw new Exception("FloorBoxProductMgrZr.countNumBy(pc_id,ps_id,box_type_id):"+e);
		}
	}
 	public DBRow getBoxSkuPsBy(long box_type_ps_id ) throws Exception {
 		try{
			String tableName = ConfigBean.getStringValue("lp_title_ship_to");

 			return dbUtilAutoTran.selectSingle("select * from " + tableName + " where lp_title_ship_id="+box_type_ps_id );
 		}catch (Exception e) {
			throw new Exception("FloorBoxProductMgrZr.getBoxSkuPsBy(box_type_ps_id):"+e);
		}
 	}
 	public DBRow[] getAllBoxSkuByPcIdPsId(long pc_id ,long to_ps_id,long basic_container_type) throws Exception {
		try{
			StringBuffer sql = new StringBuffer("");
			sql.append("	select  ps.* , psc.title, st.*,ct.*,lp.* from lp_title_ship_to as ps ");
			sql.append(" left join product_storage_catalog  as psc on ps.ship_to_id = psc.id ");
			sql.append(" left join license_plate_type as lp on ps.lp_type_id = lp.lpt_id ");
			sql.append(" left join ship_to as st on ps.ship_to_id = st.ship_to_id ");
			sql.append(" join container_type ct on ct.type_id = lp.basic_type_id");
			sql.append(" WHERE 1=1 AND lp.container_type=2");
			if(pc_id > 0)
			{
				sql.append(" AND ps.lp_pc_id = " + pc_id);
			}
			if(to_ps_id > 0)
			{
				sql.append(" AND ps.ship_to_id = "  + to_ps_id);
			}
			if(basic_container_type > 0)
			{
				sql.append(" AND lp.basic_type_id = "  + basic_container_type);
			}
			sql.append(" order by ps.ship_to_id desc , ship_to_sort ");
//			System.out.println("sss:"+pc_id+","+to_ps_id+","+basic_container_type);
//			System.out.println("sss:"+sql.toString());
			
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorBoxProductMgrZr.getAllBoxSkuByPcId(pc_id,row):"+e);
		}
	}
 	
 	/**
 	 * 将box type到ship to 分组
 	 * @param box_pc_id
 	 * @return
 	 * @throws Exception
 	 */
 	public DBRow[] findShipToNamesByBoxShipTo(long box_pc_id)throws Exception
 	{
 		try{
 			String sql = "select bp.blp_ship_to_id, st.ship_to_name from (select blp_ship_to_id from box_ps" +
 					" where box_pc_id = " + box_pc_id+
 					" group by blp_ship_to_id) bp";
 			sql += " join ship_to st on bp.blp_ship_to_id = st.ship_to_id";
 			return dbUtilAutoTran.selectMutliple(sql);
 		}catch (Exception e) {
			throw new Exception("FloorBoxProductMgrZr.findShipToNamesByBoxShipTo(pc_id,row):"+e);
		}
 	}
 	
 	
 	public DBRow[] findShipToBoxCountByPcid(long pc_id, int limit, int type) throws Exception
 	{
 		try{
 			String sql = "SELECT a.*, st.ship_to_name FROM ("+
 					" SELECT ps.ship_to_id, count(*) cn FROM lp_title_ship_to ps " +
 					" WHERE  1=1 " +
 					" AND ps.lp_pc_id = " + pc_id+
 					" GROUP BY ps.ship_to_id) a "+
 					" JOIN ship_to st ON a.ship_to_id = st.ship_to_id";
 			if(limit > 0 && type > 0)
 			{
 				if(MoreLessOrEqualKey.MORE == type)
 				{
 					sql += " AND a.cn > " + limit;
 				}
 				else if(MoreLessOrEqualKey.LESS == type)
 				{
 					sql += " AND a.cn < " + limit;
 				}
 				else if(MoreLessOrEqualKey.EQUAL == type)
 				{
 					sql += " AND a.cn = " + limit;
 				}
 			}
 			sql += " ORDER BY a.cn DESC";
 			return dbUtilAutoTran.selectMutliple(sql);
 		}catch (Exception e) {
			throw new Exception("FloorBoxProductMgrZr.findShipToBoxCountByPcid(pc_id,row):"+e);
		}
 	}
}
