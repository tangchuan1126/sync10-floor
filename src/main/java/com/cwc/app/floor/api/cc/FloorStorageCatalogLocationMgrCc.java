package com.cwc.app.floor.api.cc;

import org.apache.log4j.Logger;

import com.cwc.app.key.ProductStatusKey;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorStorageCatalogLocationMgrCc {
	
	static Logger log = Logger.getLogger("ACTION");
	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBRow[] findStorageCatalogLocation(Long[] pc_line_id, Long[] catalog_id,Long[] ps_id,String code,int union_flag,long adid, Long[] title_id, int type, String[] lot_number_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select loc.*,sla.area_name,sum(physical_quantity) as physical_quantity_count,sum(theoretical_quantity) as theoretical_quantity_count ");
			sql.append(" from product_store_location loc");
			sql.append(" LEFT JOIN storage_location_catalog slc on slc.slc_position_all = loc.position ");
			sql.append(" LEFT JOIN storage_location_area sla on slc.slc_area = sla.area_id ");
			sql.append(" JOIN (");
			sql.append(" SELECT q.*, pst.lot_number, pst.ps_id, pst.title_id FROM");
			sql.append(" (SELECT p.* FROM product p");
			sql.append("	JOIN product_catalog c ON p.catalog_id = c.id");
			sql.append("	JOIN pc_child_list pcl ON pcl.pc_id = p.catalog_id");
			sql.append("	JOIN product_line_define pld ON pld.id = c.product_line_id ");
			sql.append("	LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
//			sql.append("	JOIN title_product_line tpl ON tpl.tpl_title_id = tp.tp_title_id");
//			sql.append("	JOIN title_product_catalog tpc ON tpc.tpc_title_id= tp.tp_title_id");
			sql.append("	LEFT JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id");
			sql.append("	LEFT JOIN product_code pc ON p.pc_id = pc.pc_id");
			sql.append("	WHERE 1=1");
			if(adid > 0){
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			if(null != code && !"".equals(code.trim())){
				sql.append(" AND (p.p_name LIKE '%"+code+"%' OR pc.p_code LIKE '%"+code+"%')");
			}
			if(null != title_id && title_id.length > 0){
				sql.append(" AND (tp.tp_title_id = " + title_id[0]);
				for (int i = 1; i < title_id.length; i++){
					sql.append(" OR tp.tp_title_id = " + title_id[i]);
				}
				sql.append(" )");
			}
			if(union_flag > -1){
				sql.append(" AND p.union_flag = " + union_flag);
			}
			if(null != pc_line_id && pc_line_id.length > 0){
				sql.append(" AND (pld.id = " + pc_line_id[0]);
				for (int i = 1; i < pc_line_id.length; i++){
					sql.append(" OR pld.id = " + pc_line_id[i]);
				}
				sql.append(" )");
			}
			if(null != catalog_id && catalog_id.length > 0){
				sql.append(" AND (pcl.search_rootid = " + catalog_id[0]);
				for (int i = 1; i < catalog_id.length; i++) {
					sql.append(" OR pcl.search_rootid = " + catalog_id[i]);
				}
				sql.append(" )");
			}
			sql.append("	GROUP BY p.pc_id) q");
			sql.append(" LEFT JOIN product_storage ps ON q.pc_id = ps.pc_id ");
			sql.append(" LEFT JOIN product_storage_title pst ON pst.pc_id = ps.pc_id AND pst.ps_id = ps.cid ");
			sql.append(" WHERE 1=1");
			if(null != ps_id && ps_id.length > 0){
				sql.append(" AND (ps.cid = " + ps_id[0]);
				for (int i = 1; i < ps_id.length; i++){
					sql.append(" OR ps.cid = " + ps_id[i]);
				}
				sql.append(" )");
			}
			if(ProductStatusKey.IN_STORE == type){//有货
				sql.append(" AND IFNULL(ps.store_count,0) > 0 ");
			}
			else if(ProductStatusKey.NO_STORE == type){//无库存，即库存为0
				sql.append(" AND IFNULL(ps.store_count,0) = 0");
			}
			if(null != lot_number_id && lot_number_id.length > 0){
				sql.append(" AND (pst.lot_number = '"+lot_number_id[0]+"'");
				for (int i = 1; i < lot_number_id.length; i++){
					sql.append(" OR pst.lot_number = '"+lot_number_id[i]+"'");
				}
				sql.append(" )");
			}
			sql.append(" ) pro ON pro.pc_id = loc.pc_id ");
			sql.append(" and pro.lot_number = loc.lot_number");
			sql.append("  and pro.title_id = loc.title_id");
			sql.append(" and pro.ps_id = loc.ps_id ");
			sql.append(" GROUP BY position");
			return(dbUtilAutoTran.selectMutliple(sql.toString()));
		}
		catch (Exception e){
			throw new Exception("FloorStorageCatalogLocationMgrCc.findProductStorageByName(long pc_line_id, long catalog_id,long ps_id,String code,int union_flag,long adid, long title_id, int type, String lot_number_id) error:" + e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
}
