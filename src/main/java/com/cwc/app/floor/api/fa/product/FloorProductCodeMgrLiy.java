package com.cwc.app.floor.api.fa.product;

import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

public class FloorProductCodeMgrLiy {
	private DBUtilAutoTran_Optm dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	/**
	 * 根据产品获得所有的code
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllProductCode(long pc_id) throws Exception {
		try{
			DBRow[] rows = null;
			StringBuilder sql = new StringBuilder("select pcode_id,p_code,retailer_id,pc_id,code_type,ship_to_name from product_code ");
			sql.append(" LEFT JOIN ship_to on ship_to.ship_to_id=product_code.retailer_id ");
			sql.append(" where product_code.pc_id="+pc_id +"  ORDER BY product_code.code_type desc ");
			rows = dbUtilAutoTran.selectMutliple(sql.toString());
			return rows;
		}catch(Exception e) {
			throw new Exception("FloorProductCodeMgr.getAllProductCode(long pc_id) error"+e.getMessage());
		}
	}
	
	/**
	 * 添加code
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long insertProductCode(DBRow row) throws Exception {
		try{
			int codeType = row.get("code_type",0);
			long pc_id = row.get("pc_id",0l);
			if(CodeTypeKey.UPC==codeType) {//update upcCode
				DBRow oldRow = dbUtilAutoTran.selectSingle("select * from "+ConfigBean.getStringValue("product_code") + " where code_type="+CodeTypeKey.UPC+" and pc_id="+pc_id);
				if(oldRow!=null) {
					long pcode_id = oldRow.get("pcode_id",0l);
					oldRow = new DBRow();
					oldRow.add("code_type",CodeTypeKey.Old);
					dbUtilAutoTran.update("where pcode_id="+pcode_id,ConfigBean.getStringValue("product_code"), oldRow);
				}
			}
			
			DBRow param_row = new DBRow();
			param_row.put("pc_id", row.get("pc_id"));
			param_row.put("p_code", row.get("p_code"));
			String sql = "select pcode_id from  product_code a  where  a.pc_id=? and a.p_code=? and code_type=" + CodeTypeKey.Old;
			if(row.get("retailer_id", 0) > 0){
				sql += " and a.retailer_id=?";
				param_row.put("retailer_id", row.get("retailer_id"));
			}else{
				sql += " and a.retailer_id=0";
			}
			DBRow pcode_id_row = this.dbUtilAutoTran.selectPreSingle(sql,param_row);
			if(pcode_id_row != null && pcode_id_row.get("pcode_id", 0) > 0){
				DBRow codeTyeRow = new DBRow();
				codeTyeRow.add("code_type",row.get("code_type"));
				dbUtilAutoTran.update("where pcode_id=" + pcode_id_row.get("pcode_id", 0L),ConfigBean.getStringValue("product_code"), codeTyeRow);
				return pcode_id_row.get("pcode_id", 0L);
			}else{
				return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_code"), row);
			}
			
			
		}catch(Exception e) {
			throw new Exception("FloorProductCodeMgr.insertProductCode(DBRow row) error"+e.getMessage());
		}
	}
	/**
	 * getCode
	 * @param pcode_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getProductCode(long pcode_id) throws Exception {
		try{
			String sql = "select * from "+ ConfigBean.getStringValue("product_code")+" where pcode_id="+pcode_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e) {
			throw new Exception("FloorProductCodeMgr.getProductCode(long pcode_id) error"+e.getMessage());
		}
	}
	
	/**
	 * 删除code
	 * @param pcode_id
	 * @return
	 * @throws Exception
	 */
	public long deleteProductCode(long pcode_id) throws Exception {
		try{
			return dbUtilAutoTran.delete("where pcode_id="+pcode_id, ConfigBean.getStringValue("product_code"));
		}catch(Exception e) {
			throw new Exception("FloorProductCodeMgr.deleteProductCode(long pcode_id) error"+e.getMessage());
		}
	}
	/**
	 * 更新code
	 * @param pcode_id
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long updateProductCode(long pcode_id,DBRow row) throws Exception {
		try{
			//return dbUtilAutoTran.update("where pcode_id="+pcode_id,ConfigBean.getStringValue("product_code"), row);
			if(pcode_id > 0) {//update upcCode
				DBRow oldRow = dbUtilAutoTran.selectSingle("select * from "+ConfigBean.getStringValue("product_code") + " where pcode_id=" + pcode_id);
				if(oldRow!=null) {
					oldRow = new DBRow();
					oldRow.add("code_type",CodeTypeKey.Old);
					dbUtilAutoTran.update("where pcode_id="+pcode_id,ConfigBean.getStringValue("product_code"), oldRow);
				}
			}
			row.remove("pcode_id");
			DBRow param_row = new DBRow();
			param_row.put("pc_id", row.get("pc_id"));
			param_row.put("p_code", row.get("p_code"));
			String sql = "select pcode_id from  product_code a  where  a.pc_id=? and a.p_code=? and code_type=" + CodeTypeKey.Old;
			if(row.get("retailer_id", 0) > 0){
				sql += " and a.retailer_id=?";
				param_row.put("retailer_id", row.get("retailer_id"));
			}else{
				sql += " and a.retailer_id=0";
			}
			DBRow pcode_id_row = this.dbUtilAutoTran.selectPreSingle(sql,param_row);
			if(pcode_id_row != null && pcode_id_row.get("pcode_id", 0) > 0){
				DBRow codeTyeRow = new DBRow();
				codeTyeRow.add("code_type",row.get("code_type"));
				dbUtilAutoTran.update("where pcode_id=" + pcode_id_row.get("pcode_id", 0L),ConfigBean.getStringValue("product_code"), codeTyeRow);
				return pcode_id_row.get("pcode_id", 0L);
			}else{
				return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_code"), row);
			}
		}catch(Exception e) {
			throw new Exception("FloorProductCodeMgr.updateProductCode(long pcode_id,DBRow row) error"+e.getMessage());
		}
	}
	/**
	 * 
	 * @param condition
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public boolean checkCodeField(String condition,long pc_id) throws Exception {
		try{
			StringBuilder sql = new StringBuilder("select count(pcode_id) cnt from "+ConfigBean.getStringValue("product_code") + " where pc_id="+pc_id);
			sql.append(condition);
			if(dbUtilAutoTran.selectMutliple(sql.toString())[0].get("cnt",0l)>0) {
				return true;
			} 
			return false;
		}catch(Exception e) {
			throw new Exception("FloorProductCodeMgr.checkCodeField(String condition,long pc_id) error "+e.getMessage());
		}
	}
	
	
	/**
	 * 判断当前产品下不能用重复的code
	 * @param pcode_id
	 * @param pc_id
	 * @param parm 
	 * @return
	 * @throws Exception
	 */
	public boolean checkProductCode(long pcode_id,long pc_id,DBRow parm) throws Exception {
		try{
			DBRow pcCode = new DBRow();
			boolean flag1=false,flag2 = false;
			if(pcode_id>0) {
				pcCode = getProductCode(pcode_id);
			}
			StringBuilder sql = new StringBuilder();
			if (parm.get("p_code")!=null && !"".equals(parm.getString("p_code"))) {
				if(pcCode.getString("p_code").equals(parm.getString("p_code"))) {
					flag1= true;
				}
				sql.append(" and code_type<>" + CodeTypeKey.Old + " and p_code='"+parm.getString("p_code")+"' ");
			}
			if (parm.get("retailer_id")!=null && !"".equals(parm.getString("retailer_id"))) {
				if(pcCode.getString("retailer_id").equals(parm.getString("retailer_id"))) {
					flag2 = true;
				}
				sql.append(" and retailer_id="+parm.getString("retailer_id"));
			}
			if(flag1&&flag2) 
				return false;
			return checkCodeField(sql.toString(), pc_id);
		}catch(Exception e) {
			throw new Exception("FloorProductCodeMgr.checkProductCode(long pcode_id,long pc_id,DBRow parm) error"+e.getMessage());
		}
	}
	/**
	 * 获得所有的retailer
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllRetailer() throws Exception{ 
		try{
			DBRow[] rows = null;
			StringBuilder sql = new StringBuilder("select * from "+ConfigBean.getStringValue("ship_to")  +" order by ship_to_name desc ");
			rows = dbUtilAutoTran.selectMutliple(sql.toString());
			return rows;
		}catch(Exception e) {
			throw new Exception("FloorProductCodeMgr.getAllRetailer() error"+e.getMessage());
		}
	}
	
}
