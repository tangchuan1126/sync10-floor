package com.cwc.app.floor.api.tjh;



import java.util.ArrayList;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorProductSalesMgrTJH {
	private DBUtilAutoTran dbUtilAutoTran;
	/**
	 * 查询某一段时间内已发货的产品销售情况
	 * @param pc
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllProductSales(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_sales");
			if(pc != null)
			{
				return (dbUtilAutoTran.selectMutliple(sql, pc));
			}
			else
			{
				return (dbUtilAutoTran.selectMutliple(sql));
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllProductSales(PageCtrl pc) error："+e);
		}
	}
	
	/**
	 * 删除上一次统计的某一时间的已发货的产品信息
	 * @param end_date 
	 * @param id
	 * @throws Exception 
	 */
	public void deleteLastProductSales(String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.delete("where delivery_date between '"+start_date+" 00:00:00' and '"+end_date+" 23:59:59'", ConfigBean.getStringValue("product_sales"));
		} 
		catch (Exception e) 
		{
			throw new Exception("deleteLastProductSales(long id) error："+e);
		}
		
	}
	
	/**
	 * 统计某一段时间内普通订单中已发货的产品销售情况
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] statsProdyctSalesByDelivey(String deliveryDate,String end) 
		throws Exception 
	{
		try 
		{
			String sql = "select p.oid,orpv.p_pid,orpv.p_quantity,p.delivery_date,p.ccid,p.pro_id,orpv.unit_price,orpv.weight,p.mc_gross_rmb,p.total_weight,p.post_date,p.product_cost,p.parent_oid,p.order_source,p.shipping_cost,orpv.catalog_id from "+ConfigBean.getStringValue("porder")+" p "
						+"inner join "+ConfigBean.getStringValue("order_all_products_view")+" orpv on p.oid = orpv.oid and p.delivery_date between '"+deliveryDate+" 0:00:00' and '"+end+" 23:59:59'";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("statsProdyctSalesByDelivey(String startDate, String endDate) error："+e);
		}
	}

	/**
	 * 将统计的某一段时间内已发货的产品销售信息保存到数据库中
	 * @param row
	 * @throws Exception 
	 */
	public void addProductSalesByDeliveryDate(DBRow row) 
		throws Exception 
	{
		try 
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("product_sales"));
			row.add("ps_id", id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("product_sales"), row);
			
		} 
		catch (Exception e) 
		{
			throw new Exception("addProductSalesByDeliveryDate(DBRow row) error："+e);
		}
		
	}

	/**
	 * 获取所有的已发货的产品销售信息
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllProductSales() 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_sales");
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllProductSales() error：" + e);
		}
	}
	
	/**
	 * 查询所有的订单信息
	 * @param endDate 
	 * @param startDate 
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllOrders(String startDate, String endDate) 
		throws Exception 
	{
		try 
		{
			String sql = "select p.oid,orpv.p_pid,orpv.p_quantity,p.delivery_date,p.ccid,p.pro_id,orpv.unit_price,orpv.weight,p.mc_gross_rmb,p.total_weight,p.post_date,p.total_cost,p.parent_oid from "+ConfigBean.getStringValue("porder")+" p inner join "+ConfigBean.getStringValue("order_all_products_view")+" orpv on p.oid = orpv.oid and p.delivery_date between '"+startDate+" 0:00:00' and '"+endDate+" 23:59:59'";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllOrders(String startDate, String endDate) error："+e);
		}
	}

	/**
	 * 统计有子订单的所有的某一时间段内已发货的产品的销售情况
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] statsParentAndChildOrdersProductSales(String startDate,String endDate) 
		throws Exception 
	{
		try 
		{
			String sql = "select p.oid,orpv.p_pid,orpv.p_quantity,p.delivery_date,p.ccid,p.pro_id,orpv.unit_price,orpv.weight,p.mc_gross_rmb,p.total_weight,p.post_date,p.total_cost,p.parent_oid from "+ConfigBean.getStringValue("porder")+" p inner join "+ConfigBean.getStringValue("order_all_products_view")+" orpv on p.oid = orpv.oid and p.parent_oid = 0 and p.delivery_date between '"+startDate+" 0:00:00' and '"+endDate+" 23:59:59' and p.parent_oid > 0";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("statsParentAndChildOrdersProductSales(String startDate,String endDate) error："+e);
		}
	}


	/**
	 * 根据子订单的id查询主订单的信息
	 * @param parentId
	 * @return
	 * @throws Exception 
	 */
	public DBRow getParentOrderById(long parentId) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("porder")+" where oid="+parentId;
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getParentOrderById(long parentId) error："+e);
		}
	}

	/**
	 * 查看所有国家的产品销售额
	 * @param l
	 * @param catalog_id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getAllCountryProductSales(long countryId, long catalog_id) 
		throws Exception 
	{
		try 
		{
			if(catalog_id != 0)
			{
				String sql = "select sum(saleroom) as price,country_id from "+ConfigBean.getStringValue("product_sales") +" where catalog_id in((select id from "+ConfigBean.getStringValue("product_catalog")+" where find_in_set(id,getCatalogParentLst((select id from "+ConfigBean.getStringValue("product_catalog")+" where id = "+catalog_id+"))))) and country_id = "+countryId+" group by country_id";
				return (dbUtilAutoTran.selectSingle(sql));
			}
			else
			{
				String sql = "select sum(saleroom) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" where country_id = "+countryId+" group by country_id";
				return (dbUtilAutoTran.selectSingle(sql));
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllCountryProductSales(long countryId, long catalog_id) error："+e);
		}
	}


	/**
	 * 根据国家ID获取该国家的产品销售额分布情况
	 * @param 
	 * @param catalog_id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProvinceProductSalesRoomSpreadById(long provinceId, long catalog_id) 
		throws Exception 
	{
		try 
		{
			if(catalog_id != 0)
			{
				String sql = "select sum(saleroom) as price,province_id from "+ConfigBean.getStringValue("product_sales") +" where catalog_id in((select id from "+ConfigBean.getStringValue("product_catalog")+" where find_in_set(id,getCatalogParentLst((select id from "+ConfigBean.getStringValue("product_catalog")+" where id = "+catalog_id+"))))) and province_id = "+provinceId+" group by country_id,province_id";
				return (dbUtilAutoTran.selectSingle(sql));
			}
			else
			{
				String sql = "select sum(saleroom) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" where province_id = "+provinceId+" group by country_id,province_id";
				return (dbUtilAutoTran.selectSingle(sql));
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("getProvinceProductSalesRoomSpreadById(long provinceId, long catalog_id) error："+e);
		}
	}


	/**
	 * 查询所有国家的产品销售成本的分布情况
	 * @param country_id
	 * @param catalog_id
	 * @return
	 */
	public DBRow getAllCountryProductCost(long country_id, long catalog_id) 
		throws Exception
	{
		try 
		{
			if(catalog_id != 0)
			{
				String sql = "select sum(product_cost) as price from "+ConfigBean.getStringValue("product_sales") +" where catalog_id in((select id from "+ConfigBean.getStringValue("product_catalog")+" where find_in_set(id,getCatalogParentLst((select id from "+ConfigBean.getStringValue("product_catalog")+" where id = "+catalog_id+"))))) and country_id = "+country_id+" group by country_id";
				return (dbUtilAutoTran.selectSingle(sql));
			}
			else
			{
				String sql = "select sum(product_cost) as price from "+ConfigBean.getStringValue("product_sales")+" where country_id = "+country_id+" group by country_id";
				return (dbUtilAutoTran.selectSingle(sql));
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllCountryProductCost(long provinceId, long catalog_id) error："+e);
		}
	}

	/**
	 * 查询所有国家的产品销售利润的分布情况
	 * @param country_id
	 * @param catalog_id
	 * @return
	 */
	public DBRow getAllCountryProductProfit(long country_id, long catalog_id) 
		throws Exception
	{
		
		try 
		{
			if(catalog_id != 0)
			{
				String sql = "select sum(profit) as price from "+ConfigBean.getStringValue("product_sales") +" where catalog_id in((select id from "+ConfigBean.getStringValue("product_catalog")+" where find_in_set(id,getCatalogParentLst((select id from "+ConfigBean.getStringValue("product_catalog")+" where id = "+catalog_id+"))))) and country_id = "+country_id+" group by country_id";
				return (dbUtilAutoTran.selectSingle(sql));
			}
			else
			{
				String sql = "select sum(profit) as price from "+ConfigBean.getStringValue("product_sales")+" where country_id = "+country_id+" group by country_id";
				return (dbUtilAutoTran.selectSingle(sql));
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllCountryProductCost(long provinceId, long catalog_id) error："+e);
		}
	}

	/**
	 * 查看产品成本的分布情况
	 * @param l
	 * @param catalog_id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProvinceProductCostByID(long provinceId, long catalog_id) 
		throws Exception 
	{
		try 
		{
			if(catalog_id != 0)
			{
				String sql = "select sum(product_cost) as price,country_id from "+ConfigBean.getStringValue("product_sales") +" where catalog_id in((select id from "+ConfigBean.getStringValue("product_catalog")+" where find_in_set(id,getCatalogParentLst((select id from "+ConfigBean.getStringValue("product_catalog")+" where id = "+catalog_id+"))))) and province_id = "+provinceId+" group by country_id,province_id";
				return (dbUtilAutoTran.selectSingle(sql));
			}
			else
			{
				String sql = "select sum(product_cost) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" where province_id = "+provinceId+" group by country_id,province_id";
				return (dbUtilAutoTran.selectSingle(sql));
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("getProvinceProductCostByID(long provinceId, long catalog_id) error："+e);
		}
	}

	/**
	 * 查看各国下各省份或州的产品销售的利润分布情况
	 * @param provinceId
	 * @param catalog_id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProvinceProductProfitById(long provinceId, long catalog_id) 
		throws Exception 
	{
		try 
		{
			if(catalog_id != 0)
			{
				String sql = "select sum(profit) as price,country_id from "+ConfigBean.getStringValue("product_sales") +" where catalog_id in((select id from "+ConfigBean.getStringValue("product_catalog")+" where find_in_set(id,getCatalogParentLst((select id from "+ConfigBean.getStringValue("product_catalog")+" where id = "+catalog_id+"))))) and province_id = "+provinceId+" group by country_id,province_id";
				return (dbUtilAutoTran.selectSingle(sql));
			}
			else
			{
				String sql = "select sum(profit) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" where province_id = "+provinceId+" group by country_id,province_id";
				return (dbUtilAutoTran.selectSingle(sql));
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("getProvinceProductProfitById(long provinceId, long catalog_id) error："+e);
		}
	}

	/**
	 * 根据产品id查询该产品某一短时间内的销售额和利润以及成本在各国的分布情况
	 * @param country_id
	 * @param p_id
	 * @param cmd
	 * @param end_date 
	 * @param start_date 
	 * @return
	 * @throws Exception 
	 */
	public DBRow getAllProductSalesOrProfitOrCostByProductId(long country_id, long p_id,String cmd, String start_date, String end_date) 
		throws Exception 
	{

		try 
		{
			String sql = null;
			if(cmd.equals("salesRoom"))
			{
				sql = "select sum(saleroom) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" where product_id = ? and country_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
			}
			else if(cmd.equals("cost"))
			{
				sql = "select sum(product_cost) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" where product_id = ? and country_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
			}
			else if(cmd.equals("profit"))
			{
				sql = "select sum(profit) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" where product_id = ? and country_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
			}
			
			DBRow param = new DBRow();
			param.add("product_id", p_id);
			param.add("country_id", country_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, param));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllProductSalesOrProfitOrCostByProductId(long country_id, long p_id,String cmd) error："+e);
		}
	}

	/**
	 * 根据产品id查询该产品的销售额和利润以及成本在各国的各区域的分布情况
	 * @param province_id
	 * @param cmd
	 * @param product_id
	 * @param end_date 
	 * @param start_date 
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProvinceSalesOrProfitOrCostByPid(long province_id, String cmd,long product_id, String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = null;
			if(cmd.equals("salesRoom"))
			{
				sql = "select sum(saleroom) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" where product_id = ? and province_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
			}
			else if(cmd.equals("cost"))
			{
				sql = "select sum(product_cost) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" where product_id = ? and province_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
			}
			else if(cmd.equals("profit"))
			{
				sql = "select sum(profit) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" where product_id = ? and province_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
			}
			
			DBRow param = new DBRow();
			param.add("product_id", product_id);
			param.add("province_id", province_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, param));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProvinceSalesOrProfitOrCostByPid(long country_id, String cmd,long product_id) error："+e);
		}
	}

	/**
	 * 根据商品来源查询某一时间段内产品销额、利润和成本的分布情况
	 * @param country_id
	 * @param order_source
	 * @param cmd
	 * @param end_date 
	 * @param start_date 
	 * @return
	 * @throws Exception 
	 */
	public DBRow getAllCountrySalesOrProfitOrCostByOrderSource(long country_id,String order_source, String cmd, String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = null;
			if(cmd.equals("salesRoom"))
			{
				sql = "select sum(saleroom) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" where order_source = ? and country_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
			}
			else if(cmd.equals("cost"))
			{
				sql = "select sum(product_cost) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" where order_source = ? and country_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
			}
			else if(cmd.equals("profit"))
			{
				sql = "select sum(profit) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" where order_source = ? and country_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
			}
			
			DBRow param = new DBRow();
			param.add("order_source", order_source);
			param.add("country_id", country_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, param));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllCountrySalesOrProfitOrCostByOrderSource(long country_id,String order_source, String cmd) error："+e);
		}
	}

	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	/**
	 * 根据产品来源查询产品销售额、利润和成本在各国的各区域的分布情况
	 * @param province_id
	 * @param cmd
	 * @param order_source
	 * @param end_date 
	 * @param start_date 
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProvinceSalesOrProfitOrCostByOrderSource(long province_id,String cmd, String order_source, String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = null;
			if(cmd.equals("salesRoom"))
			{
				sql = "select sum(saleroom) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" where order_source = ? and province_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
			}
			else if(cmd.equals("cost"))
			{
				sql = "select sum(product_cost) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" where order_source = ? and province_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
			}
			else if(cmd.equals("profit"))
			{
				sql = "select sum(profit) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" where order_source = ? and province_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
			}
			
			DBRow param = new DBRow();
			param.add("order_source", order_source);
			param.add("province_id", province_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, param));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProvinceSalesOrProfitOrCostByOrderSource(long province_id,String cmd, String order_source) error："+e);
		}
	}

	/**
	 * 根据产品类别和产品线查询某一时间段内各个国家的销售额、利润、成本的分布情况
	 * @param country_id
	 * @param p_id
	 * @param cmd
	 * @param start_date
	 * @param end_date
	 * @return
	 * @throws Exception 
	 */
	public DBRow getAllCountryProductSalesOrProfitOrCostByDate(long country_id,String cmd, String start_date, String end_date) 
		throws Exception 
	{

		    try 
		    {
				String sql = null;
				if(cmd.equals("salesRoom"))
				{
					sql = "select sum(saleroom) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" where country_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
				}
				else if(cmd.equals("cost"))
				{
					sql = "select sum(product_cost) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" where  country_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
				}
				else if(cmd.equals("profit"))
				{
					sql = "select sum(profit) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" where country_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
				}
				DBRow param = new DBRow();
				param.add("country_id", country_id);
				return dbUtilAutoTran.selectPreSingle(sql, param);
				
			} 
		    catch (Exception e) 
			{
				throw new Exception("getAllCountryProductSalesOrProfitOrCostByDate(long country_id, String cmd, String start_date, String end_date error：)"+e);
			}
	}

	/**
	 * 根据产品类别和产品线查询某一时间段内各个国家的销售额、利润、成本的分布情况
	 * @param country_id
	 * @param p_id
	 * @param cmd
	 * @param start_date
	 * @param end_date
	 * @return
	 * @throws Exception
	 */
	public DBRow getAllCountryProductSalesOrProfitOrCostByCatalogId(long country_id, String p_id,String cmd, String start_date, String end_date) 
	throws Exception 
{

	    try 
	    {
			String sql = null;
				if(cmd.equals("salesRoom"))
				{
					sql = "select sum(saleroom) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" " +
							"where catalog_id in("+p_id+") and country_id = "+country_id+" and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
				}
				else if(cmd.equals("cost"))
				{
					sql = "select sum(product_cost) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" " +
							"where catalog_id in("+p_id+") and country_id = "+country_id+" and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
				}
				else if(cmd.equals("profit"))
				{
					sql = "select sum(profit) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" " +
							"where catalog_id in("+p_id+") and country_id = "+country_id+" and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
				}
			return dbUtilAutoTran.selectSingle(sql);
			
		} 
	    catch (Exception e) 
		{
			throw new Exception("getAllCountryProductSalesOrProfitOrCostByCatalogId(long country_id, String p_id,String cmd, String start_date, String end_date error：)"+e);
		}
}
	
	/**
	 * 根据产品类别和产品线查询某一时间段内产品销售额、利润以及成本的分布情况
	 * @param province_id
	 * @param catalog_id
	 * @param start_date
	 * @param end_date
	 * @param cmd 
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProvinceProductSalesOrProfitOrCostById(long province_id,String catalog_id, String cmd, String start_date, String end_date) 
		throws Exception 
	{
		 try 
		    {
				String sql = null;
					if(cmd.equals("salesRoom"))
					{
						sql = "select sum(saleroom) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" " +
								"where catalog_id in("+catalog_id+") and province_id = "+province_id+" and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
					}
					else if(cmd.equals("cost"))
					{
						sql = "select sum(product_cost) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" " +
								"where catalog_id in("+catalog_id+") and province_id = "+province_id+" and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
					}
					else if(cmd.equals("profit"))
					{
						sql = "select sum(profit) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" " +
								"where catalog_id in("+catalog_id+") and province_id = "+province_id+" and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
					}
					
				return dbUtilAutoTran.selectSingle(sql);
				
			} 
		    catch (Exception e) 
			{
				throw new Exception("DBRow getProvinceProductSalesOrProfitOrCostById(long province_id,long catalog_id, String cmd, String start_date, String end_date) error："+e);
			}
	}

	/**
	 * 获取某一时间段内所有的产品销售的信息
	 * @param start_date
	 * @param end_date
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllProductSalesByDate(String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_sales")+" where delivery_date between '"+start_date+" 00:00:00' and '"+end_date+" 23:59:59'";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllProductSalesByDate(String start_date, String end_date) error："+e);
		}
	}

	/**
	 * 根据产品线查询全球所有的产品销售总额的分布情况
	 * @param l
	 * @param pro_line_id
	 * @param cmd
	 * @param start_date
	 * @param end_date
	 * @return
	 * @throws Exception 
	 */
	public DBRow getAllCountryProductSalesOrProfitOrCostByLineId(long country_id,String pro_line_id, String cmd, String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = null;
			if(cmd.equals("salesRoom"))
			{
				sql = "select sum(saleroom) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" " +
						"where catalog_id in("+pro_line_id+") and country_id = "+country_id+" and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
			}
			else if(cmd.equals("cost"))
			{
				sql = "select sum(product_cost) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" " +
						"where catalog_id in("+pro_line_id+") and country_id = "+country_id+" and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
			}
			else if(cmd.equals("profit"))
			{
				sql = "select sum(profit) as price,country_id from "+ConfigBean.getStringValue("product_sales")+" " +
				"where catalog_id in("+pro_line_id+") and country_id = "+country_id+" and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
			}
			return dbUtilAutoTran.selectSingle(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllCountryProductSalesOrProfitOrCostByLineId(long country_id,String pro_line_id, String cmd, String start_date, String end_date) error:"+e);
		}
	}

	/**
	 * 
	 * @param province_id
	 * @param pro_line_id
	 * @param cmd
	 * @param start_date
	 * @param end_date
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProvinceProductSalesOrProfitOrCostByLineId(long province_id,String pro_line_id, String cmd, String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = null;
			if(cmd.equals("salesRoom"))
			{
				sql = "select sum(saleroom) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" " +
						"where catalog_id in("+pro_line_id+") and province_id = "+province_id+" and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
			}
			else if(cmd.equals("cost"))
			{
				sql = "select sum(product_cost) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" " +
				"where catalog_id in("+pro_line_id+") and province_id = "+province_id+" and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
			}
			else if(cmd.equals("profit"))
			{
				sql = "select sum(profit) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" " +
				"where catalog_id in("+pro_line_id+") and province_id = "+province_id+" and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by province_id";
			}
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProvinceProductSalesOrProfitOrCostByLineId(long province_id,String pro_line_id, String cmd, String start_date, String end_date) error:"+e);
		}
	}
	
	/**
	 * 分类ID
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	private String allCatalogs(String catalog_id)
		throws Exception
	{
		try{
			StringBuffer sb = new StringBuffer(catalog_id);
			int stop;
			ArrayList<DBRow> a = new ArrayList<DBRow>();
			do {
				String sqls = "select id from "+ConfigBean.getStringValue("product_catalog")+" where parentid in ("
						+ catalog_id + ")";
				DBRow[] s = dbUtilAutoTran.selectMutliple(sqls);
				
				StringBuffer csb = new StringBuffer("");
				for ( stop = 0; stop < s.length; stop++) 
				{
					a.add(s[stop]);
					csb.append(s[stop].getString("id"));
					if (stop < s.length - 1) {
						csb.append(",");
					}
				}
				catalog_id = csb.toString();
				
			} while (!catalog_id.equals(""));
			
			DBRow[] ids = a.toArray(new DBRow[0]);
			for(int i = 0;i<ids.length;i++)
			{
				sb.append(","+ids[i].getString("id"));
			}
			return sb.toString();
		} 
		catch (Exception e) 
		{
			throw new Exception(e);
		}
	}

	/**
	 * 查询某一时间段内产品销售的情况
	 * @param input_st_date
	 * @param input_en_date
	 * @param catalog_id
	 * @param pc
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllProductSalesByDeliveryDate(String input_st_date,String input_en_date, PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_sales")+" where delivery_date between '"+input_st_date+" 0:00:00' and '"+input_en_date+" 23:59:59' order by delivery_date desc,oid desc";
			return (dbUtilAutoTran.selectMutliple(sql, pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllProductSalesByDeliveryDate(String input_st_date,String input_en_date, long catalog_id, PageCtrl pc) error:"+e);
		}
	}
	
	/**
	 * 根据产品线过滤查询某一时间内的产品销售的情况
	 * @param input_st_date
	 * @param input_en_date
	 * @param p_line_id
	 * @param pc
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllProductSalesProductByLineId(String input_st_date,String input_en_date, String p_line_id, PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_sales")+" where catalog_id in("+p_line_id+") and delivery_date between '"+input_st_date+" 00:00:00' and '"+input_en_date+" 23:59:59' order by delivery_date desc,oid desc";
			return (dbUtilAutoTran.selectMutliple(sql, pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllProductSalesProductByLineId(String input_st_date,String input_en_date, String p_line_id, PageCtrl pc) error:"+e);
		}
	}
	
	public DBRow[] getAllProductSalesByCatalogIds(String input_st_date,String input_en_date, String catalog_ids, PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			String sql;
			if(!catalog_ids.equals("") && !catalog_ids.equals("0"))
			{
				sql = "select * from "+ConfigBean.getStringValue("product_sales")+" where catalog_id in("+catalog_ids+") and delivery_date between '"+input_st_date+" 0:00:00' and '"+input_en_date+" 23:59:59' order by delivery_date desc,oid desc";
			}
			else
			{
				sql = "select * from "+ConfigBean.getStringValue("product_sales")+" where delivery_date between '"+input_st_date+" 0:00:00' and '"+input_en_date+" 23:59:59' order by delivery_date desc,oid desc";
			}
			return (dbUtilAutoTran.selectMutliple(sql, pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllProductSalesByCatalogId(String input_st_date,String input_en_date, long catalog_id, PageCtrl pc) error:"+e);
		}
	}

	/**
	 * 根据商品条码或名称过滤某一时间段内的产品销售的情况
	 * @param input_st_date
	 * @param input_en_date
	 * @param product_id
	 * @param pc
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllProductSalesByProductId(String input_st_date,String input_en_date, long product_id, PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			DBRow param = new DBRow();
			String sql = "select * from "+ConfigBean.getStringValue("product_sales")+" where " +
					"product_id = ? and delivery_date between '"+input_st_date+" 00:00:00' " +
							"and '"+input_en_date+" 23:59:59' order by delivery_date desc,oid desc";
			param.add("product_id", product_id);
			return (dbUtilAutoTran.selectPreMutliple(sql, param, pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllProductSalesByProductId(String input_st_date,String input_en_date, long product_id, PageCtrl pc) error:"+e);
		}
	}

	

	/**
	 * 根据产品来源过滤查询某一时间段的产品销售情况
	 * @param input_st_date
	 * @param input_en_date
	 * @param order_source
	 * @param pc
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllProductSalesByOrderSource(String input_st_date,String input_en_date, String order_source, PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_sales")+" where order_source =? and delivery_date between '"+input_st_date+" 00:00:00' and '"+input_en_date+" 23:59:59' order by delivery_date desc,oid desc";
			DBRow param = new DBRow();
			param.add("order_source", order_source);
			return (dbUtilAutoTran.selectPreMutliple(sql, param, pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllProductSalesByOrderSource(String input_st_date,String input_en_date, String order_source, PageCtrl pc) error:"+e);
		}
	}

	/**
	 * 获取产品类别以及该类下所有的子集的结果集
	 * @param catalog_id
	 * @return 
	 * @return
	 * @throws Exception 
	 */

	public DBRow getAllSonCatalogIds(long catalog_id) 
		throws Exception 
	{
		try 
		{
			String sql = "select product_catalog_child_list('"+catalog_id+"') as ids";
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllSonCatalogIds(long catalog_id) error:"+e);
		}
	}

	/**
	 * 根据时间段查询在某一国家各省份、州的产品销售量的分布情况
	 * @param province_id
	 * @param cmd
	 * @param start_date
	 * @param end_date
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProvinceProductSalesOrProfitOrCostByDate(long province_id,String cmd, String start_date, String end_date) 
		throws Exception 
	{
		try 
		{
			String sql = null;
			if(cmd.equals("salesRoom"))
				{
					sql = "select sum(saleroom) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" " +
							"where province_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
				}
				else if(cmd.equals("cost"))
				{
					sql = "select sum(product_cost) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" " +
							"where  province_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
				}
				else if(cmd.equals("profit"))
				{
					sql = "select sum(profit) as price,province_id from "+ConfigBean.getStringValue("product_sales")+" " +
							"where province_id = ? and delivery_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' group by country_id";
				}
			DBRow param = new DBRow();
			param.add("province_id", province_id);
			return (dbUtilAutoTran.selectPreSingle(sql, param));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProvinceProductSalesOrProfitOrCostByDate(long province_id,String cmd, String start_date, String end_date) error:"+e);
		}
	}
}
