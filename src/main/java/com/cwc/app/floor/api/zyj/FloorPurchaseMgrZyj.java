package com.cwc.app.floor.api.zyj;

import com.cwc.app.key.PurchaseKey;
import com.cwc.app.key.PurchaseLogTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorPurchaseMgrZyj {
	
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 通过采购单的ID，文件的类型数组，查询文件列表信息
	 * @param purhcaseId
	 * @param types
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurhcaseFileInfosByPurchaseIdTypes(long purhcaseId, int[] types) throws Exception{
		
		try {
			String sql = "SELECT * FROM file WHERE file_with_id = " + purhcaseId;
			if(null != types && types.length > 0){
				if(1 == types.length){
					sql += " AND file_with_type = " + types[0];
				}else{
					sql += " AND (file_with_type = " + types[0];
					for (int i = 0; i < types.length; i++) {
						sql += " OR file_with_type = " + types[i];
					}
					sql += " )";
				}
			}
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorPurchaseMgrZyj.getPurhcaseFileInfosByPurchaseIdTypes(long purhcaseId, int[] types):"+e);
		}
	}
	
	/**
	 * 通过主单据ID，文件类型查询商品文件
	 * @param file_with_type
	 * @param file_with_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllProductTagFilesByPcId(long purhcaseId, int[] types) throws Exception {
		try{
			String sql = "SELECT * FROM product_file WHERE file_with_id = " + purhcaseId;
			if(null != types && types.length > 0){
				if(1 == types.length){
					sql += " AND file_with_type = " + types[0];
				}else{
					sql += " AND (file_with_type = " + types[0];
					for (int i = 0; i < types.length; i++) {
						sql += " OR file_with_type = " + types[i];
					}
					sql += " )";
				}
			}
			return dbUtilAutoTran.selectMutliple(sql);
			 
		}catch(Exception e){
			throw new Exception("FloorPurchaseMgrZyj.getAllProductTagFilesByPcId(long purhcaseId, int[] types):"+e);
		}
	}
	
	/**
	 * 通过主单据ID，文件类型查询商品文件
	 * @param file_with_type
	 * @param file_with_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllProductFilesByProductModelAndTypes(long purhcaseId, int[] types, int[] file_types) throws Exception {
		try{
			String sql = "SELECT * FROM product_file WHERE file_with_id = " + purhcaseId;
			if(null != types && types.length > 0){
				if(1 == types.length){
					sql += " AND file_with_type = " + types[0];
				}else{
					sql += " AND (file_with_type = " + types[0];
					for (int i = 1; i < types.length; i++) {
						sql += " OR file_with_type = " + types[i];
					}
					sql += " )";
				}
			}
			if(null != file_types && file_types.length > 0){
				if(1 == file_types.length){
					sql += " AND product_file_type = " + file_types[0];
				}else{
					sql += " AND (product_file_type = " + file_types[0];
					for (int i = 1; i < file_types.length; i++) {
						sql += " OR product_file_type = " + file_types[i];
					}
					sql += " )";
				}
			}
			
			return dbUtilAutoTran.selectMutliple(sql);
			 
		}catch(Exception e){
			throw new Exception("FloorPurchaseMgrZyj.getAllProductFilesByProductModelAndTypes(long purhcaseId, int[] types, int[] file_types):"+e);
		}
	}
	
	/**
	 * 通过采购单ID,日志类型，阶段类型，得到列表中的第一条日志
	 * @return
	 * @throws Exception
	 */
	public DBRow getFollowUpLogsByPurchaseIdAndTypeActivity(long purchaseId, int type, int typeSub) throws Exception{
		try 
		{
			String sql = "SELECT * FROM purchase_followup_logs WHERE purchase_id = "+purchaseId+" AND followup_type = "+type+" AND followup_type_sub = "+typeSub+" ORDER BY logs_id ASC LIMIT 1";
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZyj.getFollowUpLogsByPurchaseIdAndTypeActivity(long purhcaseId, int[] types):"+e);
		}
	}
	
	/**
	 * 查询某个子流程需要根据时，采购单的数量
	 * 如果产品线不为0，那么加上产品线的查询条件
	 * @return
	 * @throws Exception
	 */
	public int getPurchaseNeedFollowUpCountByProcessKey(int day, int processKey, int[] activitys, long product_line_id) throws Exception
	{
		try 
		{
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT count(purchase_id) AS need_followup_purchase_count FROM");
			sb.append(	" (SELECT * FROM");
			sb.append(			" (SELECT p.purchase_id, f.followup_date AS last_followup_date");
			if(PurchaseKey.AFFIRMTRANSFER == processKey)
			{
				sb.append(						" FROM (SELECT ph.*,t.transport_id FROM purchase ph LEFT JOIN ");
				sb.append(							" (SELECT transport_id,purchase_id FROM transport where transport_status != 4)t");
				sb.append(						" ON ph.purchase_id = t.purchase_id");
				sb.append(						" WHERE t.transport_id IS NULL) p");
			}
			else
			{
				sb.append(						" FROM purchase p");
			}
			sb.append(							" JOIN purchase_followup_logs f ON p.purchase_id = f.purchase_id");
			sb.append(							" AND p.purchase_status != ").append(PurchaseKey.CANCEL);
			sb.append(							" AND f.followup_type = ").append(processKey);
			String purchaseProcessWhere = "";
			if(null != activitys && activitys.length > 0)
			{
				if(1 == activitys.length)
				{
					sb.append(		" AND f.followup_type_sub = " + activitys[0]);	
					purchaseProcessWhere += handleSqlWhereAnd(activitys, processKey);
					if(!"".equals(purchaseProcessWhere))
					{
						purchaseProcessWhere += ")";
					}
				}
				else
				{
					sb.append(		" AND (f.followup_type_sub = " + activitys[0]);
					purchaseProcessWhere += handleSqlWhereAnd(activitys, processKey);
					for (int i = 1; i < activitys.length; i++) {
						sb.append(		" 	OR f.followup_type_sub = " + activitys[i]);
						purchaseProcessWhere += handleSqlWhereOr(activitys, processKey);
					}
					sb.append(			")");
				}
			}
			sb.append(purchaseProcessWhere);
			if(-1 != product_line_id)
			{
				sb.append(						" AND p.product_line_id = ").append(product_line_id);
			}
			sb.append(						" ORDER BY f.followup_date DESC)pur");
			sb.append(					" GROUP BY purchase_id");
			sb.append(				" )need_pur");
			sb.append(" WHERE");
			sb.append(	" date_add(");
			sb.append(		" last_followup_date,");
			sb.append(		" INTERVAL ").append(day).append(" DAY");
			sb.append(		" )<= CURRENT_DATE");
			
			DBRow result = dbUtilAutoTran.selectSingle(sb.toString());
			return result.get("need_followup_purchase_count", 0);
			
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZyj.getPurchaseNeedFollowUpCount(int day, int processKey, int product_line_id):"+e);
		}
	}
	
	private String handleSqlWhereAnd(int[] activitys, int processKey) throws Exception{
		try 
		{
			String purchaseProcessWhere = "";
			if(PurchaseLogTypeKey.INVOICE == processKey)
			{
				purchaseProcessWhere += " AND (p.invoice = " + activitys[0];
			}
			else if(PurchaseLogTypeKey.DRAWBACK == processKey)
			{
				purchaseProcessWhere += " AND (p.drawback = " + activitys[0];
			}
			else if(PurchaseLogTypeKey.NEED_TAG == processKey)
			{
				purchaseProcessWhere += " AND (p.need_tag = " + activitys[0];
			}
			else if(PurchaseLogTypeKey.THIRD_TAG == processKey)
			{
				purchaseProcessWhere += " AND (p.need_third_tag = " + activitys[0];
			}
			else if(PurchaseLogTypeKey.QUALITY == processKey)
			{
				purchaseProcessWhere += " AND (p.quality_inspection = " + activitys[0];
			}
			else if(PurchaseLogTypeKey.PRODUCT_MODEL == processKey)
			{
				purchaseProcessWhere += " AND (p.product_model = " + activitys[0];
			}
			else if(processKey == PurchaseLogTypeKey.PRICE)
			{
				purchaseProcessWhere += " AND (p.purchase_status = " + activitys[0];
			}
			return purchaseProcessWhere;
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZyj.handleSqlWhereAnd(int processKey, int product_line_id):"+e);
		}
	}
	
	private String handleSqlWhereOr(int[] activitys, int processKey) throws Exception
	{
		try 
		{
			String purchaseProcessWhere = "";
			for (int i = 1; i < activitys.length; i++) {
				
				if(PurchaseLogTypeKey.INVOICE == processKey)
				{
					purchaseProcessWhere += " OR p.invoice = " + activitys[i];
				}
				else if(PurchaseLogTypeKey.DRAWBACK == processKey)
				{
					purchaseProcessWhere += " OR p.drawback = " + activitys[i];
				}
				else if(PurchaseLogTypeKey.NEED_TAG == processKey)
				{
					purchaseProcessWhere += " OR p.need_tag = " + activitys[i];
				}
				else if(PurchaseLogTypeKey.THIRD_TAG == processKey)
				{
					purchaseProcessWhere += " OR p.need_third_tag = " + activitys[i];
				}
				else if(PurchaseLogTypeKey.QUALITY == processKey)
				{
					purchaseProcessWhere += " OR p.quality_inspection = " + activitys[i];
				}
				else if(PurchaseLogTypeKey.PRODUCT_MODEL == processKey)
				{
					purchaseProcessWhere += " OR p.product_model = " + activitys[i];
				}
				else if(processKey == PurchaseLogTypeKey.PRICE)
				{
					purchaseProcessWhere += " OR p.purchase_status = " + activitys[i];
				}
			}
			purchaseProcessWhere += ")";
			return purchaseProcessWhere;
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZyj.handleSqlWhereOr(int processKey, int product_line_id):"+e);
		}
	}
	
	/**
	 * 产品线分组，得到所有产品线下需要跟进的采购单数
	 * @param day
	 * @param processKey
	 * @param activitys
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseNeedFollowUpCountGroupProductLineByProcessKey(int day, int processKey, int[] activitys) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append(" SELECT count(purchase_id) as purchase_need_follow_count, product_line_id, product_line_name");
			sb.append(	" FROM (SELECT * FROM");
			sb.append(			" (SELECT");
			sb.append(					" p.purchase_id,");
			sb.append(					" f.followup_date AS last_followup_date,");
			sb.append(					" p.product_line_id,");
			sb.append(					" l.NAME AS product_line_name");
			
			if(PurchaseKey.AFFIRMTRANSFER == processKey)
			{
				sb.append(						" FROM (SELECT ph.*,t.transport_id FROM purchase ph LEFT JOIN ");
				sb.append(							" (SELECT transport_id,purchase_id FROM transport where transport_status != 4)t");
				sb.append(						" ON ph.purchase_id = t.purchase_id");
				sb.append(						" WHERE t.transport_id IS NULL) p");
			}
			else
			{
				sb.append(						" FROM purchase p");
			}
			sb.append(				" JOIN purchase_followup_logs f ON p.purchase_id = f.purchase_id");
			sb.append(				" AND p.purchase_status != ").append(PurchaseKey.CANCEL);
			sb.append(				" AND f.followup_type = ").append(processKey);
			String purchaseProcessWhere = "";
			if(null != activitys && activitys.length > 0)
			{
				if(1 == activitys.length)
				{
					sb.append(		" AND f.followup_type_sub = " + activitys[0]);	
					purchaseProcessWhere += handleSqlWhereAnd(activitys, processKey);
					if(!"".equals(purchaseProcessWhere))
					{
						purchaseProcessWhere += ")";
					}
				}
				else
				{
					sb.append(		" AND (f.followup_type_sub = " + activitys[0]);
					purchaseProcessWhere += handleSqlWhereAnd(activitys, processKey);
					for (int i = 1; i < activitys.length; i++) {
						sb.append(		" 	OR f.followup_type_sub = " + activitys[i]);
						purchaseProcessWhere += handleSqlWhereOr(activitys, processKey);
					}
					sb.append(			")");
				}
			}
			sb.append(purchaseProcessWhere);
			sb.append(				" LEFT JOIN product_line_define l ON p.product_line_id = l.id");
			sb.append(				" ORDER BY f.followup_date DESC)pur");
			sb.append(		" GROUP BY purchase_id)need_pur");
			sb.append(" WHERE");
			sb.append(	" date_add(");
			sb.append(		" last_followup_date,");
			sb.append(		" INTERVAL ").append(day).append(" DAY");
			sb.append(	" )<= CURRENT_DATE");
			sb.append(" GROUP BY product_line_id");
			
			return dbUtilAutoTran.selectMutliple(sb.toString());
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZyj.getPurchaseNeedFollowUpCountGroupProductLineByProcessKey(int day, int processKey):"+e);
		}
	}
	
	/**
	 * 通过产品线和阶段查询采购单列表
	 * @param productLineId
	 * @param processKey
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseRowsByProductLineIdAndProcessKey(long productLineId, int processKey, int activity, int day, PageCtrl pc) throws Exception
	{
			try
			{
				String sql = "";
				StringBuffer sb = new StringBuffer();
				sb.append("SELECT * FROM");
				sb.append(	" (SELECT * FROM");
				sb.append(			" ( SELECT p.*, f.followup_date AS last_followup_date");
				if(PurchaseKey.AFFIRMTRANSFER == processKey)
				{
					sb.append(						" FROM (SELECT ph.*,t.transport_id FROM purchase ph LEFT JOIN ");
					sb.append(							" (SELECT transport_id,purchase_id FROM transport where transport_status != 4)t");
					sb.append(						" ON ph.purchase_id = t.purchase_id");
					sb.append(						" WHERE t.transport_id IS NULL) p");
				}
				else
				{
					sb.append(						" FROM purchase p");
				}
				sb.append(				" JOIN purchase_followup_logs f ON p.purchase_id = f.purchase_id");
				sb.append(				" AND p.purchase_status != ").append(PurchaseKey.CANCEL);
				sb.append(				" AND f.followup_type = ").append(processKey);
				int[] array = {activity}; 
				String where = handleSqlWhereAnd(array, processKey);
				if(!"".equals(where))
				{
					where += ")";
				}
				sb.append(where);
				sb.append(				" AND p.product_line_id = ").append(productLineId);
				sb.append(				" ORDER BY f.followup_date DESC");
				sb.append(			" )pur GROUP BY purchase_id");
				sb.append(	" )need_pur");
				sb.append(" WHERE");
				sb.append(	" date_add(");
				sb.append(		" last_followup_date,");
				sb.append(		" INTERVAL ").append(day).append(" DAY");
				sb.append(	" )<= CURRENT_DATE");
				sb.append(" ORDER BY purchase_id desc");
				
				sql = sb.toString();
				return dbUtilAutoTran.selectMutliple(sql, pc);
			} 
			catch (Exception e) 
			{
				throw new Exception("FloorTransportMgrZyj.getPurchaseRowsByProductLineIdAndProcessKey(long productLineId, int processKey, pc):"+e);
			}
	}
	
	/**
	 * 由于采购单的创建者ID未存，因此需要将创建者Id更新进来
	 * 获取所有采购单的创建者名字和id,关联人员表
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllPurchaseAdmin() throws Exception
	{
		try 
		{
			String sql = "select m.adid,p.purchase_id from purchase p " +
					"left join admin m on p.proposer = m.employe_name GROUP BY p.purchase_id;";
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZyj.getAllPurchaseAdmin():"+e);
		}
	}
	
	/**
	 * 根据当前登录者的Id，获取其上次创建的采购单的ID
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow getPurchaseLastTimeCreateByAdid(long adid) throws Exception
	{
		try 
		{
			String sql = "select purchase_id from purchase where proposer_id = "+adid+" ORDER BY purchase_date desc LIMIT 0,1";
			return dbUtilAutoTran.selectSingle(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZyj.getPurchaseLastTimeCreateByAdid():"+e);
		}
	}
	
	//得到所有采购单，处于数据用
	public DBRow[] getAllPurchase()throws Exception
	{
		try 
		{
			String sql = "select * from purchase";
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorTransportMgrZyj.getAllPurchase():"+e);
		}
	}
	
	//通过采购单的Id及跟进，查询日志
	public DBRow getPurchaseLogsByPurchaseIdAndProcessKey(long purchase_id, int processKey, int activity)throws Exception
	{
		try 
		{
			String sql = "select count(*) as purchaseCountByProcessActivity from purchase_followup_logs where purchase_id = "+ purchase_id +" and followup_type = "+ processKey +" and followup_type_sub = " + activity;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorTransportMgrZyj.getPurchaseLogsByPurchaseIdAndProcessKey(long purchase_id, int processKey, int activity):"+e);
		}
	}
	/**
	 * 通过采购单ID，日志内容查询日志
	 * @param purchase_id
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseLogsByPurchaseIdAndCountent(long purchase_id, String context) throws Exception
	{
		try 
		{
			String sql = "select * from purchase_followup_logs f where f.purchase_id = "+purchase_id+" and f.followup_content like '%"+context+"%'";
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorTransportMgrZyj.getPurchaseLogsByPurchaseIdAndCountent(long purchase_id, String context):"+e);
		}
	}
	
	/**
	 *  通过商品，关联ID，类型查询文件
	 * @param pc_id
	 * @param file_with_type
	 * @param file_with_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllProductFileByPcId(int[] file_with_type, int[] file_with_class, long file_with_id) throws Exception 
	{
		try{
 
			StringBuffer sql = new StringBuffer();
			sql.append("select * from product_file ").append(" where file_with_id=").append(file_with_id);
			if(null != file_with_type && file_with_type.length > 0){
				if(1 == file_with_type.length){
					sql.append(" AND file_with_type = " + file_with_type[0]);
				}else{
					sql.append(" AND (file_with_type = " + file_with_type[0]);
					for (int i = 0; i < file_with_type.length; i++) {
						sql.append(" OR file_with_type = " + file_with_type[i]);
					}
					sql.append(" )");
				}
			}
			if(null != file_with_class && file_with_class.length > 0){
				if(1 == file_with_class.length){
					sql.append(" AND product_file_type = " + file_with_class[0]);
				}else{
					sql.append(" AND (product_file_type = " + file_with_class[0]);
					for (int i = 1; i < file_with_class.length; i++) {
						sql.append(" OR product_file_type = " + file_with_class[i]);
					}
					sql.append(" )");
				}
			}
			return dbUtilAutoTran.selectMutliple(sql.toString());
			 
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZyj.getAllProductFileByPcId(DBRow):"+e);
		}
	}
	
	
	public DBRow[] findPurchase(PageCtrl pc) throws Exception 
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from purchase");
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZyj.findPurchase:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

}
