package com.cwc.app.floor.api.zj;

import java.util.ArrayList;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorReturnStepMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBRow[] filterReturnStep(long ps_id,String catalog_ids,PageCtrl pc)
		throws Exception
	{
		try 
		{
			//获得跟仓库分类下所有子分类
//			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
//			ArrayList al = tree.getAllSonNode(ps_id);
//			al.add(ps_id);

			String cid = ps_id+"";
			int i=0;

//			for (; i<al.size()-1; i++)
//			{
//				cid += al.get(i)+",";
//			}
//			cid += al.get(i);
			
			StringBuffer sql = new StringBuffer("select * from " + ConfigBean.getStringValue("product_storage") + " p," + ConfigBean.getStringValue("product_storage_catalog") + " c,"+ConfigBean.getStringValue("product")+" pc where pc.pc_id=p.pc_id and p.cid=c.id and cid in ("+cid+")  and (damaged_count>0 or damaged_package_count>0)");
			
			if(!catalog_ids.equals(""))
			{
				sql.append(" and pc.catalog_id in ("+catalog_ids+")");
			}
			
			return dbUtilAutoTran.selectMutliple(sql.toString(),pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorReturnStepMgrZJ filterReturnStep error:"+e);
		}
	}
	
	public DBRow[] getReturnStepByName(String catalogid,long ps_id,String code,PageCtrl pc)
		throws Exception
	{
		try
		{
			
			//获得跟仓库分类下所有子分类
//			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
//			ArrayList al = tree.getAllSonNode(ps_id);
//			al.add(ps_id);
	
			String cid = ps_id+"";
//			int i=0;
//			for (; i<al.size()-1; i++)
//			{
//				cid += al.get(i)+",";
//			}
//			cid += al.get(i);
			
			String catalogWhere = "";
			if(!catalogid.equals(""))
			{
				catalogWhere = " and p.catalog_id in ("+catalogid+")";
			}
			
			StringBuffer sql = new StringBuffer("SELECT * FROM product_storage ps "
											   +"join product_storage_catalog c "
											   +"join product p on p.pc_id = ps.pc_id and p.p_name LIKE '%"+code+"%'"
											   +"join product_code pcode on ps.pc_id = pcode.pc_id "
											   +"WHERE p.cid IN("+cid+") AND(damaged_count > 0	OR damaged_package_count > 0) "
											   +catalogWhere
											   +"union " 
											   +"SELECT * FROM product_storage ps "
											   +"join product_storage_catalog c "
											   +"join product p on p.pc_id = ps.pc_id " 
											   +"join product_code pcode on ps.pc_id = pcode.pc_id and pcode.p_code LIKE '%"+code+"%' "
											   +"WHERE p.cid IN("+cid+") AND(damaged_count > 0	OR damaged_package_count > 0)"
											   +catalogWhere);
			
			
			
			sql.append(" order by pid desc");
	
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectMutliple(sql.toString(),pc));
			}
			else
			{
				return(dbUtilAutoTran.selectMutliple(sql.toString()));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnStepMgrZJ getProductStorageByName error:" + e);
		}
	}
	
	/**
	 * 残损件存放位置
	 * @param ps_id
	 * @param pc_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnStepLocationByPcid(long ps_id,long pc_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("return_step_location")+" where ps_id=? and pc_id=?";
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("pc_id",pc_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para, pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnStepMgrZJ getReturnStepLocationByPcid error:" + e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
