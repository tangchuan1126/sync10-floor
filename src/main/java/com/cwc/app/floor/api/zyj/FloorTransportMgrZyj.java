package com.cwc.app.floor.api.zyj;

import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorTransportMgrZyj {
	
	private DBUtilAutoTran dbUtilAutoTran;

	
	public void updateTransport(long transportId, DBRow transport) throws Exception{
		try {
			dbUtilAutoTran.update(" where transport_id = "+transportId, ConfigBean.getStringValue("transport"), transport);
		} catch (Exception e) {
			throw new Exception("FloorTransportMgrZyj.updateTrackStatus(track)"+e);
		}
		
	}
	
	/**
	 * 通过采购单的ID，获得交货记录
	 * @param purhcaseId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportRowsByPurchaseId(long purchaseId) throws Exception{
		try {
			return dbUtilAutoTran.selectMutliple("select * from transport where purchase_id = " + purchaseId);
		} catch (Exception e) {
			throw new Exception("FloorTransportMgrZyj.getTransportRowsByPurchaseId(long purhcaseId)"+e);
		}
	}
	
	/**
	 * 通过转运单ID，删除转运单的日志
	 * @param transportId
	 * @throws Exception
	 */
	public void deleteTransportLogsByTransportId(long transportId) throws Exception
	{
		try 
		{
			String sql = " where transport_id = " + transportId;
			dbUtilAutoTran.delete(sql, ConfigBean.getStringValue("transport_logs"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZyj.deleteTransportLogsByTransportId(long transportId)"+e);
		}
	}
	/**
	 * 删除转运单的运费项目
	 * @param transportId
	 * @throws Exception
	 */
	public void deleteTransportFreightByTransportId(long transportId) throws Exception
	{
		try 
		{
			String sql = " where transport_id = " + transportId;
			dbUtilAutoTran.delete(sql, ConfigBean.getStringValue("transport_freight_cost"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZyj.deleteTransportFreightByTransportId(long transportId)"+e);
		}
	}
	
	/**
	 * 根据转运单ID，文件的类型数组，删除文件
	 * @param transportId
	 * @param types
	 * @throws Exception
	 */
	public void deleteTransportFileByTransportIdAndTypes(long transportId, int[] types) throws Exception
	{
		try 
		{
			String sql = " where file_with_id = " + transportId;
			if(null != types && types.length > 0)
			{
				String sqlWhere = "";
				if(1 == types.length)
				{
					sqlWhere = " and file_with_type = " + types[0];
				}
				else
				{
					sqlWhere = " and (file_with_type = " + types[0];
					for (int i = 1; i < types.length; i++) {
						sqlWhere += " or file_with_type = " + types[i];
					}
					sqlWhere += ")";
				}
				sql += sqlWhere;
			}
			dbUtilAutoTran.delete(sql, ConfigBean.getStringValue("file"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorTransportMgrZyj.deleteTransportLogsByTransportId(long transportId, int[] types)"+e);
		}
	}
	/**
	 * 根据转运单ID，类型删除转运单下商品的相关文件
	 * @param transportId
	 * @param types
	 * @throws Exception
	 */
	public void deleteTransportProductFileByTransportIdAndTypes(long transportId, int[] types) throws Exception
	{
		try
		{
			String sql = " where file_with_id = " + transportId;
			if(null != types && types.length > 0)
			{
				String sqlWhere = "";
				if(1 == types.length)
				{
					sqlWhere = " and file_with_type = " + types[0];
				}
				else
				{
					sqlWhere = " and (file_with_type = " + types[0];
					for (int i = 1; i < types.length; i++) {
						sqlWhere += " or file_with_type = " + types[i];
					}
					sqlWhere += ")";
				}
				sql += sqlWhere;
			}
			dbUtilAutoTran.delete(sql, ConfigBean.getStringValue("product_file"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZyj.deleteTransportProductFileByTransportIdAndTypes(long transportId, int[] types)"+e);
		}
	}
	
	
	/**
	 * 根据当前登录者的Id，获取其上次创建的交货单的ID
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow getTransportLastTimeCreateByAdid(long adid) throws Exception
	{
		try 
		{
			String sql = "select transport_id from transport where create_account_id = "+adid+" and purchase_id = 0 ORDER BY transport_date desc LIMIT 0,1";
			return dbUtilAutoTran.selectSingle(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZyj.getTransportLastTimeCreateByAdid():"+e);
		}
	}
	
	/**
	 * 根据当前登录者的Id，获取其上次创建的交货单的ID
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow getTransportPurchaseLastTimeCreateByAdid(long adid) throws Exception
	{
		try 
		{
			String sql = "select transport_id from transport where create_account_id = "+adid+" and purchase_id != 0 ORDER BY transport_date desc LIMIT 0,1";
			return dbUtilAutoTran.selectSingle(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZyj.getTransportPurchaseLastTimeCreateByAdid():"+e);
		}
	}
	
	
	/**
	 * 根据 商品名字检索
	 * @param repairId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportDetailsByTransportProductName(long repair_order_id, String name)throws Exception{
		try{
			String sql="select * from transport_detail where transport_id="+repair_order_id+" and transport_p_name  = '"+name+"'";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZyj.getTransportDetailsByTransportProductName error:"+ e);
		}
	}
	
	/**
	 * 通过转运单号及sn查询sn是否存在
	 * @author Administrator
	 * @param transport_id
	 * @param sn
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportItemsByTransportIdAndSn(long transport_id, String sn) throws Exception
	{
		try
		{
			String sql = "select * from transport_detail where transport_id = " + transport_id + " and transport_product_serial_number = '"+sn+"'";
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorTransportMgrZyj.getTransportItemsByTransportIdAndSn error:" + e);
		}
	}
	
	
	/**
	 * 根据状态和仓库获取转运单
	 * @param machine
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportByStatusStroageCount(String transport_status, long send_storage_id, long receive_storage_id, long purchase_id, int count) throws Exception
	{
		try
		{
			int[] transportStatus = null;
			if(!StrUtil.isBlank(transport_status))
			{
				String[] ts = transport_status.split(",");
				if(ts.length > 0)
				{
					transportStatus = new int[ts.length];
					for (int i = 0; i < ts.length; i++) 
					{
						transportStatus[i] = Integer.parseInt(ts[i]);
					}
				}
			}
			
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT t.transport_id, sendarea.sup_name AS fromwarehouse, receivearea.title AS towarehouse")
			.append(",t.title_id, tit.title_name")
			.append(" from "+ConfigBean.getStringValue("transport")).append(" as t  left join ")
			.append(ConfigBean.getStringValue("supplier"))
			.append(" as sendarea on t.send_psid = sendarea.id") 
			.append(" left join ").append(ConfigBean.getStringValue("product_storage_catalog"))
			.append(" as receivearea on receivearea.id = t.receive_psid")
			.append(" left join title tit on tit.title_id = t.title_id")
			.append(" where 1=1 ");
			if(transportStatus.length > 0)
			{
				sql.append(" and (t.transport_status = "+transportStatus[0]);
				for (int i = 1; i < transportStatus.length; i++)
				{
					sql.append(" or t.transport_status = "+transportStatus[i]);
				}
				sql.append(" )");
			}
			if(send_storage_id > 0)
			{
				sql.append(" and t.send_psid = "+send_storage_id );
			}
			if(receive_storage_id > 0)
			{
				sql.append(" and t.receive_psid = "+receive_storage_id);
			}
			if(purchase_id > 0)
			{
				sql.append(" and t.purchase_id = "+purchase_id);
			}
			sql.append(" and t.purchase_id != 0");
			if(count > 0)
			{
				sql.append(" order by t.transport_id desc ");
				sql.append(" LIMIT 0, " + count);
			}
			else
			{
				sql.append(" order by t.transport_id desc");
			}
			
			//System.out.println("getTransport:"+sql.toString());

			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch (Exception e)
		{
			throw new Exception("FloorTransportMgrZyj.getTransportByStatusAndStroage(String transport_status, long send_storage_id, long receive_storage_id):"+e);
		}
	}
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
