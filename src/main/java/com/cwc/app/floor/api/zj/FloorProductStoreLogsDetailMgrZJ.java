package com.cwc.app.floor.api.zj;

import com.cwc.app.key.ProductStoreCountTypeKey;
import com.cwc.app.key.ProductStoreLogsDetailOperationTypeKey;
import com.cwc.app.key.ProductStoreLogsDetailUseTypeKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorProductStoreLogsDetailMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加库存日志明细
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addProductStoreLogsDetail(DBRow row)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_store_logs_detail"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLogsDetailMgrZJ addProductStoreLogsDetail error:"+e);
		}
	}
	
	/**
	 * 添加拼装关系日志
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addProductStoreUnionLog(DBRow row)
		throws Exception
	{
		return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_store_union_log"),row);
	}
	
	/**
	 * 修改库存日志明细
	 * @param psl_id
	 * @param row
	 * @throws Exception
	 */
	public void modProductStoreLogsDetail(long psld_id,DBRow row)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where psld_id = "+psld_id,ConfigBean.getStringValue("product_store_logs_detail"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLogsDetailMgrZJ modProductStoreLogsDetail error:"+e);
		}
	}
	
	/**
	 * 根据日志ID更新批次
	 * @param psl_id
	 * @param row
	 * @throws Exception
	 */
	public void modProductStoreLogDetails(long psl_id,DBRow row)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where psl_id = "+psl_id,ConfigBean.getStringValue("product_store_logs_detail"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLogsDetailMgrZJ modProductStoreLogDetails error:"+e);
		}
	}
	
	/**
	 * 查询可出库的日志明细
	 * @param ps_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoreLogsDetailsCanOut(long ps_id,long pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_logs_detail")+" where ps_id = ? and pc_id = ? and use_status !="+ProductStoreLogsDetailUseTypeKey.AllUsed;
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("pc_id",pc_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLogsDetailMgrZJ getProductStoreLogsDetailsCanOut error:"+e);
		}
	}
	
	/**
	 * 根据日志获得所有日志详细
	 * @param psl_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoreDetailLogsByPslid(long psl_id)
		throws Exception
	{
		try 
		{
			String sql = "select psld.*,psc.title,p.p_name from "+ConfigBean.getStringValue("product_store_logs_detail")+" as psld "
						+"join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on psld.ps_id = psc.id "
						+"join "+ConfigBean.getStringValue("product")+" as p on psld.pc_id = p.pc_id "
						+" where psld.psl_id = ?";
			
			DBRow para = new DBRow();
			para.add("psl_id",psl_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql,para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLogsDetailMgrZJ getProductStoreDetailLogsByPslid error:"+e);
		}
		
	}
	
	/**
	 * 根据库存日志明细ID获得库存日志明细
	 * @param psld_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getProductStoreDetailLogByPsld(long psld_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_logs_detail")+" as psld "
						+"join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on psld.ps_id = psc.id "
						+"join "+ConfigBean.getStringValue("product")+" as p on psld.pc_id = p.pc_id "
						+" where psld_id = ? ";
			
			DBRow para = new DBRow();
			para.add("psld_id",psld_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLogsDetailMgrZJ getProductStoreDetailLogByPsld error:"+e);
		}
	}
	
	/**
	 * 获得根据单据号，商品ID获得对应的未取消的出库批次
	 * @param pc_id
	 * @param ps_id
	 * @param bill_type
	 * @param bill_id
	 * @param quantity_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoreLogsDetailsOutStore(long pc_id,long ps_id,int bill_type,long bill_id,int quantity_type)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_logs_detail")+" as psld "
						+"join "+ConfigBean.getStringValue("product_store_logs")+" as psl on psl.is_cancel = 1 and psld.psl_id = psl.psl_id and psl.pc_id = ? and psl.ps_id = ? and psl.bill_type = ? and psl.oid = ? "
						+"where psld.operation_type = "+ProductStoreLogsDetailOperationTypeKey.OutStore;
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("ps_id",ps_id);
			para.add("bill_type",bill_type);
			para.add("bill_id",bill_id);
			
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLogsDetailMgrZJ getProductStoreLogsDetailsOutStore error:"+e);
		}
	}
	
	/**
	 * 获得根据单据号，商品ID获得对应的未取消的批次
	 * @param pc_id
	 * @param ps_id
	 * @param bill_type
	 * @param bill_id
	 * @param quantity_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoreLogsDetails(long pc_id,long ps_id,int bill_type,long bill_id,int quantity_type)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_logs_detail")+" as psld "
						+"join "+ConfigBean.getStringValue("product_store_logs")+" as psl on psl.is_cancel = 1 and psld.psl_id = psl.psl_id and psl.pc_id = ? and psl.ps_id = ? and psl.bill_type = ? and psl.oid = ? ";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("ps_id",ps_id);
			para.add("bill_type",bill_type);
			para.add("bill_id",bill_id);
			
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLogsDetailMgrZJ getProductStoreLogsDetails error:"+e);
		}
	}
	
	/**
	 * 根据商品,仓库，单据类型，单据号，获得入库批次
	 * @param pc_id
	 * @param ps_id
	 * @param bill_type
	 * @param bill_id
	 * @param quantity_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoreLogsDetailsInStore(long pc_id,long ps_id,int bill_type,long bill_id,int quantity_type)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_logs_detail")+" as psld "
						+"join "+ConfigBean.getStringValue("product_store_logs")+" as psl on psl.is_cancel = 1 and psld.psl_id = psl.psl_id and psl.pc_id = ? and psl.ps_id = ? and psl.bill_type = ? and psl.oid = ? "
						+"where psld.operation_type = "+ProductStoreLogsDetailOperationTypeKey.InStore;
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("ps_id",ps_id);
			para.add("bill_type",bill_type);
			para.add("bill_id",bill_id);
			
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLogsDetailMgrZJ getProductStoreLogsDetailsInStoreCombinationSystem error:"+e);
		}
	}
	
	/**
	 * 根据目标批次Id，获得批次组合关系
	 * @param to_psld_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoreUnionLogByToPsldId(long to_psld_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_union_log")+" where to_psld_id = ? ";
			
			DBRow para = new DBRow();
			para.add("to_psld_id",to_psld_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductStoreLogsDetailMgrZJ getProductStoreUnionLogByToPsldId error:"+e);
		}
	}
	
	/**
	 * 当前批次的使用批次
	 * @param from_psld_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoreLogDetailsByFromPsld(long from_psld_id)
		throws Exception
	{
		try 
		{
			String sql = "select psld.*,psc.title,p.p_name from "+ConfigBean.getStringValue("product_store_logs_detail")+" as psld "
						+"join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on psld.ps_id = psc.id "
						+"join "+ConfigBean.getStringValue("product")+" as p on psld.pc_id = p.pc_id "
						+"where psld.from_psld_id = ? ";
			
			DBRow para = new DBRow();
			para.add("from_psld_id",from_psld_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLogsDetailMgrZJ getProductStoreLogDetailsByFromPsld error:"+e);
		}
	}
	
	/**
	 * 依靠目标批次ID从组合日志中查询来源批次
	 * @param to_psld_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStorelogDetailsFromUnionLogByToPsld(long to_psld_id)
		throws Exception
	{
		try 
		{
			String sql = "select psld.*,psc.title,p.p_name from "+ConfigBean.getStringValue("product_store_logs_detail")+" as psld "
						+"join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on psld.ps_id = psc.id "
						+"join "+ConfigBean.getStringValue("product")+" as p on psld.pc_id = p.pc_id "
						+"join "+ConfigBean.getStringValue("product_store_union_log")+" as psul on psul.to_psld_id = ? and psul.from_psld_id = psld.psld_id ";
			
			DBRow para = new DBRow();
			para.add("to_psld_id",to_psld_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLogsDetailMgrZJ getProductStorelogDetailsByToPsld error:"+e);
		}
	}
	
	/**
	 * 依靠来源批次从组合日志中查询目的批次
	 * @param to_psld_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStorelogDetailsFromUnionLogByFromPsld(long from_psld_id)
		throws Exception
	{
		try 
		{
			String sql = "select distinct psld.*,psc.title,p.p_name from "+ConfigBean.getStringValue("product_store_logs_detail")+" as psld "
						+"join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on psld.ps_id = psc.id "
						+"join "+ConfigBean.getStringValue("product")+" as p on psld.pc_id = p.pc_id "
						+"join "+ConfigBean.getStringValue("product_store_union_log")+" as psul on psul.from_psld_id = ? and psul.to_psld_id = psld.psld_id ";
			
			DBRow para = new DBRow();
			para.add("from_psld_id",from_psld_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLogsDetailMgrZJ getProductStorelogDetailsByToPsld error:"+e);
		}
	}
	
	public DBRow[] getInProductStoreLogByPcid(long pc_id,long ps_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("product_store_logs")+" where is_cancel = 1 "
					+"and (operation ="+ProductStoreOperationKey.IN_STORE_DELIVERY+" or operation = "+ProductStoreOperationKey.IN_STORE_TRANSPORT+" or operation ="+ProductStoreOperationKey.IN_STORE_TRANSPORT_AUDIT+") " 
					+"and pc_id = ? and ps_id = ? and quantity>0 and quantity_type = "+ProductStoreCountTypeKey.STORECOUNT+" order by post_date desc ";
		
		DBRow para = new DBRow();
		para.add("pc_id",pc_id);
		para.add("ps_id",ps_id);
		
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	/**
	 * 积压商品
	 * @param product_line_id
	 * @param catalog_id
	 * @param ps_id
	 * @param pc_id
	 * @param backlog_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] backlogOfGoods(long product_line_id,long catalog_id,long ps_id,long pc_id,int backlog_day,PageCtrl pc)
		throws Exception
	{
		try {
			String whereProductLineOrCatalog = "";
			if(catalog_id>0)
			{
				whereProductLineOrCatalog = "join product_catalog as pc on pc.id = p.catalog_id "
										   +"join pc_child_list as pcl on pcl.pc_id = pc.id and pcl.search_rootid = "+catalog_id+" ";
			}
			else if(product_line_id>0&&catalog_id==0)
			{
				whereProductLineOrCatalog = " join product_catalog as pc on pc.id = p.catalog_id and pc.product_line_id = "+product_line_id+" "; 
			}
			
			String whereStorage = "";
			if(ps_id>0)
			{
				whereStorage = " and psld.ps_id="+ps_id+" ";
			}
			
			String sql = "";
			if(pc_id >0)
			{
				sql = "select p.p_name,p.pc_id,psld.ps_id,p.catalog_id,sum(quantity-used_quantity) as backlog_quantity from "+ConfigBean.getStringValue("product_store_logs_detail")+" as psld "
					 +"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = psld.pc_id and psld.pc_id = "+pc_id+" "
					 +whereStorage
					 +"where date_add(psld.create_time,interval "+backlog_day+" day)<=current_date and psld.use_status !=3 and psld.operation_type !=3 " 
					 +"group by p.pc_id,psld.ps_id";
			}
			else
			{
				sql = "select p.p_name,p.pc_id,psld.ps_id,p.catalog_id,sum(quantity-used_quantity) as backlog_quantity from "+ConfigBean.getStringValue("product_store_logs_detail")+" as psld "
				 +"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = psld.pc_id "
				 +whereProductLineOrCatalog+whereStorage
				 +"where date_add(psld.create_time,interval "+backlog_day+" day)<=current_date and psld.use_status !=3 and psld.operation_type !=3 "
				 +"group by p.pc_id,psld.ps_id";
			}
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLogsDetailMgrZJ backlogOfGoods error:"+e);
		}
	}
	
	/**
	 * 获得压货批次
	 * @param pc_id
	 * @param ps_id
	 * @param backlog_day
	 * @return
	 * @throws Exception
	 */
	public DBRow[] backlogGoodsStoreLogDetals(long pc_id,long ps_id,int backlog_day)
		throws Exception
	{
		
			String sql = "select psld.*,pc.title,p.p_name from "+ConfigBean.getStringValue("product_store_logs_detail")+" as psld "
					 	+"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = psld.pc_id and psld.pc_id = "+pc_id+" "
					 	+"join "+ConfigBean.getStringValue("product_catalog")+" as pc on p.catalog_id = pc.id "
					 	+"where psld.ps_id = "+ps_id+" and date_add(psld.create_time,interval "+backlog_day+" day)<=current_date and psld.use_status !=3 and psld.operation_type !=3 "
					 	+"order by psld.create_time asc";
			
			return dbUtilAutoTran.selectMutliple(sql);
		
	}
	
	/**
	 * 通过批次，计算仓库商品一段时间内出库率
	 * @param st
	 * @param en
	 * @param product_line_id
	 * @param catalog_id
	 * @param pc_id
	 * @param ps_id
	 * @param sales_rate
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] salesRate(String st,String en,long product_line_id,long catalog_id,long pc_id,long ps_id,int sales_rate,int greater_less,PageCtrl pc)
		throws Exception
	{
		String whereProductLineOrCatalog = "";
		if(catalog_id>0)
		{
			whereProductLineOrCatalog = "join product_catalog as pc on pc.id = p.catalog_id "
									   +"join pc_child_list as pcl on pcl.pc_id = pc.id and pcl.search_rootid = "+catalog_id+" ";
		}
		else if(product_line_id>0&&catalog_id==0)
		{
			whereProductLineOrCatalog = " join product_catalog as pc on pc.id = p.catalog_id and pc.product_line_id = "+product_line_id+" "; 
		}
		
		String greaterOrLess = "";
		if(greater_less==1)
		{
			greaterOrLess = ">";
		}
		else
		{
			greaterOrLess = "<";
		}
		
		String whereStorage = "";
		if(ps_id>0)
		{
			whereStorage = " and psld.ps_id="+ps_id+" ";
		}
		
		String sql = "";
		if(pc_id >0)
		{
			sql = "select p.p_name,p.pc_id,psld.ps_id,p.catalog_id,sum(used_quantity) as sum_used_quantity,sum(quantity) as sum_quantity from "+ConfigBean.getStringValue("product_store_logs_detail")+" as psld "
				 +"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = psld.pc_id and psld.pc_id = "+pc_id+" "
				 +whereStorage
				 +"where psld.create_time >'"+st+" 0:00:00' and psld.create_time <'"+en+" 23:59:59' and psld.use_status !=3 and psld.operation_type !=3 " 
				 +"group by p.pc_id";
		}
		else
		{
			sql = "select p.p_name,p.pc_id,psld.ps_id,p.catalog_id,sum(used_quantity) as sum_used_quantity,sum(quantity) as sum_quantity from "+ConfigBean.getStringValue("product_store_logs_detail")+" as psld "
			 +"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = psld.pc_id "
			 +whereProductLineOrCatalog+whereStorage
			 +"where psld.create_time >'"+st+" 0:00:00' and psld.create_time <'"+en+" 23:59:59' and psld.use_status !=3 and psld.operation_type !=3 "
			 +"group by p.pc_id,psld.ps_id";
		}
		
		sql = "select * from "
			+"(" 
			+"	select p_name,pc_id,catalog_id,sum_used_quantity,sum_quantity,(sum_used_quantity/sum_quantity*100) as sales_rate from " 
			+"	("
			+		sql	
			+"	) quantityView "
			+") as salesRateView "
			+"where sales_rate"+greaterOrLess+sales_rate;
		
		return dbUtilAutoTran.selectMutliple(sql, pc);
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
