package com.cwc.app.floor.api.zj;

import java.util.Date;

import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorRefundLogMgrZJ 
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 插入运单退款记录
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addRefundLog(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("refund_log"));
			dbrow.add("refund_id",id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("refund_log"),dbrow);
			
			return (id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRefundLogMgrZJ.addRefundLog error:"+e);
		}
	}
	/**
	 * 修改退款记录
	 * @param refund_id
	 * @param dbrow
	 * @throws Exception
	 */
	public void modRefundLog(long refund_id,DBRow dbrow)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where refund_id="+refund_id,ConfigBean.getStringValue("refund_log"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRefundLogMgrZJ.modRefundLog error:"+e);
		}
	}
	
	/**
	 * 获得退款记录
	 * @param begintime
	 * @param endtime
	 * @param refund_status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRefundLogs(Date begintime,Date endtime,int refund_status,PageCtrl pc)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("refund_log")+" where 1=1 ");
			
			DBRow para = new DBRow();
			
			if(!begintime.equals(""))
			{
				sql.append(" and refund_time>=?");
				para.add("begintime",DateUtil.FormatDatetime("yyyy-MM-dd 00:00:00", begintime));
			}
			
			if(!endtime.equals(""))
			{
				sql.append(" and refund_time<?");
				para.add("endtime",DateUtil.FormatDatetime("yyyy-MM-dd 23:59:59", endtime));
			}
			
			if(refund_status!=0)
			{
				sql.append(" and refund_status =?");
				para.add("refund_status",refund_status);
			}
			
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(),para,pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRefundLogMgrZJ.getRefundLogs error:"+e);
		}
	}
	
	/**
	 * 根据运单号模糊查询退款记录
	 * @param refund_delivery_code
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchRefundLogByDeliveryCode(String refund_delivery_code,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql ="select * from "+ConfigBean.getStringValue("refund_log")+" where refund_delivery_code like '%"+refund_delivery_code+"%'";
			
			return (dbUtilAutoTran.selectMutliple(sql,pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRefundLogMgrZJ.searchRefundLogByDeliveryCode error:"+e);
		}
	}
	
	/**
	 * 根据运单号查询退款记录
	 * @param refund_delivery_code
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow searchDetailRefundLogByDeliveryCode(String refund_delivery_code)
		throws Exception
	{
		try 
		{
			String sql ="select * from "+ConfigBean.getStringValue("refund_log")+" where refund_delivery_code = '"+refund_delivery_code+"'";
			
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRefundLogMgrZJ.searchRefundLogByDeliveryCode error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
