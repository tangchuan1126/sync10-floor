package com.cwc.app.floor.api.wp;

import org.apache.log4j.Logger;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorFolderInfoMgrWp {
	static Logger log = Logger.getLogger("ACTION");
	
	private DBUtilAutoTran dbUtilAutoTran;
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	public int deleteKmlTabel(String field, String ps_id,String tablename)throws Exception{
		String sql = "where " + field + " = '" + ps_id + "'";
	
			return dbUtilAutoTran.delete(sql, tablename);
	
	}

	public void deleteWebcam(String ps_id) throws Exception {
		String sql = "delete  from " + ConfigBean.getStringValue("webcam")
				+ " where ps_id ="+ ps_id;
		  dbUtilAutoTran.executeSQL(sql);
	}

	public void deleteCheckInZone(String ps_id) throws Exception {
		String sql = "delete  from "
				+ ConfigBean.getStringValue("check_in_zone_with_title")
				+ "  where area_id in ( select distinct area_id  from "
				+ ConfigBean.getStringValue("storage_location_area")
				+ " where   area_psid='" + ps_id + "')";
		
		dbUtilAutoTran.executeSQL(sql);

	}
	 public int updateFolderkml(DBRow data,DBRow condition) throws Exception{
		 
		 String ps_id= condition.getString("ps_id");
		 String area_name =condition.getString("area_name","");
		 String placemark_name =condition.getString("placemark_name","");
		 String folder_name =condition.getString("floder_name","");
		 String wherecond ="where ps_id= '"+ps_id+"'" ;
		 if(area_name.equals("")){
			 wherecond+=" and area_name = '"+area_name+"'";
		 }
		 if(placemark_name.equals("")){
			 wherecond+=" and placemark_name ='"+placemark_name+"'";
		 }
		 wherecond+=" and folder_name ='"+folder_name+"'";
		 String tablename =ConfigBean.getStringValue("folder_from_kml");
    	return 	 dbUtilAutoTran.update(wherecond, tablename, data);
	 }
	 public DBRow getStorageById(String psId) throws Exception{
		 String sql = "select * from "+ConfigBean.getStringValue("product_storage_catalog")+" p where p.id="+psId;
		 return dbUtilAutoTran.selectSingle(sql);
	 }
	public DBRow[] getExcelAreaData(String ps_id) throws Exception {
		try {
			String sql = "SELECT a.*, h.doorId, b.title_name FROM " +
					ConfigBean.getStringValue("storage_location_area") + "  a" +
					" LEFT JOIN (select c.area_id area_id , GROUP_CONCAT(distinct e.doorId ORDER BY e.doorId) doorId from "+ConfigBean.getStringValue("storage_area_door")+" c, "+ConfigBean.getStringValue("storage_door")+" e where c.sd_id =e.sd_id GROUP BY area_id) h ON a.area_id = h.area_id" +
					" LEFT JOIN (select m.area_id area_id , GROUP_CONCAT(distinct n.title_name ) title_name from "+ConfigBean.getStringValue("check_in_zone_with_title")+" m, "+ConfigBean.getStringValue("title")+" n where m.title_id =n.title_id GROUP BY area_id) b ON a.area_id = b.area_id" +
					" where  a.area_psid="+ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorFolderInfoMgrWp.getExcelAreaData():" + e);
		}

	}

	public DBRow[] getExcelDocksData(String ps_id) throws Exception {
		try {
			String sql = "select  c.* ,d.area_name " + "from "
					+ ConfigBean.getStringValue("storage_door") + " c "
					+" LEFT JOIN "+ConfigBean.getStringValue("storage_location_area")+" d "
					+" on c.area_id =d.area_id "
					+ " WHERE c.ps_id =" + ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorFolderInfoMgrWp.getExcelDocksData():" + e);
		}
	}

	public DBRow[] getExcelParkingData(String ps_id) throws Exception {
		try {
			String sql = "select  c.* ,d.area_name " + "from "
					+ ConfigBean.getStringValue("storage_yard_control") + " c "
					+" LEFT JOIN "+ ConfigBean.getStringValue("storage_location_area") +" d "
					 +" on c.area_id = d.area_id "
					+ " WHERE c.ps_id =" + ps_id +" order by yc_no ";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorFolderInfoMgrWp.getExcelParkingData():" + e);
		}
	}

	public DBRow[] getExcelStagingData(String ps_id) throws Exception {
		try {
			String sql = "select  c.* ,d.doorId " + "from "
					+ ConfigBean.getStringValue("storage_load_unload_location") + " c "
					+" LEFT JOIN " +ConfigBean.getStringValue("storage_door")+" d "
					+ " on c.sd_id =d. sd_id "
					+ " WHERE c.psc_id =" + ps_id ;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorFolderInfoMgrWp.getExcelStagingData():" + e);
		}

	}

	public DBRow[] getExcelLocationData(String ps_id) throws Exception {
		try {
			String sql = "select  c.* , s.area_name " + "from "
					+ ConfigBean.getStringValue("storage_location_catalog") + " c "
					+ "JOIN "+ConfigBean.getStringValue("storage_location_area")+" s on s.area_id = c.slc_area "
					+ " WHERE c.slc_psid =" + ps_id + "";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorFolderInfoMgrWp.getExcelLocationData():" + e);
		}
	}

	public DBRow[] getExcelBaseData(String ps_id) throws Exception {
		try {
			String sql = "select c.* from "
					+ ConfigBean.getStringValue("storage_warehouse_base")
					+ " c  where c.ps_id = "+ ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorFolderInfoMgrWp.getExcelBaseData():" + e);
		}
	}
	public DBRow[] getExcelWebcamData(String ps_id) throws Exception {
		try {
			String sql = "select c.* from "
					+ ConfigBean.getStringValue("webcam")
					+ " c  where c.ps_id = "+ ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorFolderInfoMgrWp.getExcelWebcamData():" + e);
		}
	}
	public DBRow[] getExcelPrinterData(String ps_id) throws Exception {
		try {
			String sql = "select c.* from "
					+ ConfigBean.getStringValue("printer")
					+ " c  where c.ps_id = "+ ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorFolderInfoMgrWp.getExcelPrinterData():" + e);
		}
	}
	/**
	 * 通过ps_id查询area
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaByPsId(String ps_id) throws Exception{
		try {
			String sql = "select  c.* from "+ 
			ConfigBean.getStringValue("storage_location_area") +
			" c where c.area_psid ="+ ps_id +" ORDER BY c.area_type DESC, c.area_name";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorFolderInfoMgrWp.getAreaByPsId():" + e);
		}
	}
	/**
	 * 通过ps_id,type 查询area
	 * @param ps_id
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaByPsidAndType(String ps_id,String type) throws Exception{
		try {
			String sql = "select  c.* from "+ 
					ConfigBean.getStringValue("storage_location_area") +
					" c where c.area_psid ="+ ps_id + " and c.area_type="+type;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorFolderInfoMgrWp.getAreaInfo():" + e);
		}
	} 
	/**
	 * 通过ps_id查询door
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDoorByPsId(String ps_id) throws Exception{
		try {
			String sql = "select  c.* from "+ 
					ConfigBean.getStringValue("storage_door") +
					" c where c.ps_id ="+ ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorFolderInfoMgrWp.getDoorByPsId():" + e);
		}
	}
	public DBRow[] getAllTitle() throws Exception{
		try {
			return dbUtilAutoTran.select(ConfigBean.getStringValue("title"));
		} catch (Exception e) {
			throw new Exception("FloorFolderInfoMgrWp.getAllTitle():" + e);
		}
	}
	
	public DBRow[] getLocationbyArea(String slc_area,long psId)throws Exception{
		String sql = "select  c.* , s.area_name " + "from "
				+ ConfigBean.getStringValue("storage_location_catalog") + " c "
				+ "JOIN "+ConfigBean.getStringValue("storage_location_area")+" s on s.area_id = c.slc_area "
				+ " WHERE c.slc_psid =" + psId +" and s.area_name ='"+slc_area+"'";
		try {
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorFolderInfoMgrWp.getLocationbyArea():" + e);
		}
		
	}
	
		public DBRow[] getWebcambyPsid(long psId)throws Exception{
			String sql ="select c.* from "	+ConfigBean.getStringValue("webcam") +"  c where  ps_id ="+psId ;
			try {
				return dbUtilAutoTran.selectMutliple(sql);
			} catch (Exception e) {
				throw new Exception("FloorFolderInfoMgrWp.getWebcambyPsid():" + e);
			}
	   }
		public DBRow[] getPrinterbyPsid(long psId)throws Exception{
			String sql ="select c.*,d.area_name area_name,d.area_id area_id from "+
		            ConfigBean.getStringValue("printer")+" c " +
					"left join "+ConfigBean.getStringValue("storage_location_area") +" d on  c.physical_area=d.area_id  where  c.ps_id="+psId;
			try {
				return dbUtilAutoTran.selectMutliple(sql);
			} catch (Exception e) {
				throw new Exception("FloorFolderInfoMgrWp.getPrinterbyPsid():" + e);
			}
		}
	
}