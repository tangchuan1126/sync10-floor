package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorAnswerMgrZJ 
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加答案
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addAnswer(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long answer_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("answer"));
			dbrow.add("answer_id",answer_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("answer"),dbrow);
			
			return (answer_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorAnswerMgrZJ.addAnswer error:"+e);
		}
	}
	
	/**
	 * 根据答案ID获得答案
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailAnswerById(long id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("answer")+" where answer_id="+id;
			
			return dbUtilAutoTran.selectSingle(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorAnswerMgrZJ.getDetailAnswerById error:"+e);
		}
	}
	
	/**
	 * 根据问题ID检索该问题的答案
	 * @param question_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAnswersByQuestionId(long question_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("answer")+" where question_id = ?";
			
			DBRow para = new DBRow();
			para.add("question_id",question_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql,para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorAnswerMgrZJ.getAnswersByQuestionId error:"+e);
		}
	}
	
	/**
	 * 修改答案
	 * @param answer_id
	 * @param para
	 * @throws Exception
	 */
	public void modAnswer(long answer_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where answer_id="+answer_id,ConfigBean.getStringValue("answer"),para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorAnswerMgrZJ.modAnswer error:"+e);
		}
	}
	
	/**
	 * 删除答案
	 * @param id
	 * @throws Exception
	 */
	public void delAnswer(long id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where answer_id="+id,ConfigBean.getStringValue("answer"));
		}
		catch (RuntimeException e)
		{
			throw new Exception("FloorAnswerMgrZJ.delAnswer error:"+e);
		}
	}
	
	/**
	 * 根据问题ID删除答案
	 * @param question_id
	 * @throws Exception
	 */
	public void delAnswerByQuestionId(long question_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where question_id="+question_id,ConfigBean.getStringValue("answer"));
		}
		catch (RuntimeException e)
		{
			throw new Exception("FloorAnswerMgrZJ.delAnswer error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
