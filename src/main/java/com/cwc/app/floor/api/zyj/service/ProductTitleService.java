package com.cwc.app.floor.api.zyj.service;

import java.util.List;
import com.cwc.app.floor.api.zyj.model.Customer;
import com.cwc.app.floor.api.zyj.model.Title;


/**
 * 定义了根据产品查询相关生产商，品牌商的方法
 * @author Administrator
 *
 */
public interface ProductTitleService {
	/**
	 * 获取与特定产品相关的所有Customer
	 * @param productId 产品ID
	 * @return
	 */
	public List<Customer>  getCustomers(int productId);
	
	/**
	 * 获取与特定产品,特定生产商相关的所有Customer
	 * @param productId 产品ID
	 * @param titleId  生产商ID
	 * @return
	 */
	public List<Customer>  getCustomers(int productId,int titleId);
	
	/**
	 * 获取与特定产品相关的所有Title
	 * @param productId 产品ID
	 * @return
	 */
	public List<Title>  getTitles(int productId);
	

	
	/**
	 * 获取与特定产品,特定品牌商相关的所有Title
	 * @param productId 产品ID
	 * @param titleId  生产商ID
	 * @return
	 */
	public List<Title>  getTitles(int productId,int customerId);
	
	/**
	 * 获取与特定品牌商相关的所有Title
	 * @param customerId 品牌商ID
	 * @return
	 */
	public List<Title>  getTitlesByCustomerId(int customerId);
}
