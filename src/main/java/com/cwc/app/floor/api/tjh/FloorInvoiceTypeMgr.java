package com.cwc.app.floor.api.tjh;



import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorInvoiceTypeMgr {
	
	public DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 添加发票模板信息
	 * 
	 * @param dbRow
	 * @throws Exception
	 */
	public void addInvoiceInfo(DBRow dbRow) 
		throws Exception 
	{
		try 
		{
			
			long invoice_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("invoice_template"));
			dbRow.add("invoice_id", invoice_id);
			
			dbUtilAutoTran.insert(
					ConfigBean.getStringValue("invoice_template"), dbRow);
			
		} 
		catch (Exception e) 
		{
			throw new Exception("addInvoiceInfo error:" + e);
		}
	}
/**
 * 根据仓库ID 查找该仓库下面的数据
 * @param ps_id 仓库ID
 * @return
 * @throws Exception
 */
	public DBRow[] getDetailInvoiceInfo(long ps_id) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "
					+ ConfigBean.getStringValue("invoice_template")
					+ " where ps_id=" + ps_id;
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailInvoiceInfo error:" + e);
		}
	}
	
	/**
	 * 查询出口发件人信息
	 * @param di_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailInvoiceCompanyNameByDiId(long di_id) 
		throws Exception
	{
		try
		{
			String sql = "select CompanyName from "
				+ ConfigBean.getStringValue("deliverer_info")
				+ " where di_id=" + di_id;
		return (dbUtilAutoTran.selectSingle(sql));
		}
		catch (Exception e) 
		{
			throw new Exception("getDetailInvoiceCompanyNameByDiId error:" + e);	
		}
	}



	/**
	 * 获取所有的发件人信息
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllInvoice() 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("deliverer_info");
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllInvoice() error:"+e);
		}
	}

	/**
	 * 根据id查看某一发票的详细信息
	 * @param invoiceId
	 * @return
	 * @throws Exception 
	 */
	public DBRow getDetailInvoiceTemplateById(long invoiceId) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("invoice_template") + " where invoice_id="+invoiceId;
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailInvoiceTemplateById(invoiceId) error:"+e);
		}
	}

	/**
	 * 修改某一具体的发票模板信息
	 * @param invoice_id
	 * @param row
	 * @throws Exception
	 */
	public void modInvoiceTemplate(long invoice_id, DBRow row) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.update("where invoice_id = "+invoice_id, ConfigBean.getStringValue("invoice_template"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("modInvoiceTemplate(invoice_id,row) error:"+e);
		}
		
	}

	/**
	 * 获取仓库信息
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getStorageCategory(long id) 
		throws Exception 
	{
		
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_storage_catalog")+" where id="+id;
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getStorageCategory(id) error:"+e);
		}
		
	}
	
	


	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
