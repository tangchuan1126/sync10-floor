package com.cwc.app.floor.api.zyj.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.app.floor.api.zyj.model.Customer;
import com.cwc.app.floor.api.zyj.model.ProductCustomer;
import com.cwc.app.floor.api.zyj.model.Title;
import com.cwc.app.floor.api.zyj.service.ProductCustomerSerivce;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;


public class ProductCustomerSerivceImpl implements  ProductCustomerSerivce{
	private static final String TABLE = "title_product";
	private DBUtilAutoTran dbUtilAutoTran;
	
	private FloorCatalogMgr floorCatalogMgr;
	
	private FloorProprietaryMgrZyj floorProprietaryMgrZyj;
	/**
	 * 新增;返回新增记录的ID
	 * @param productCustomer
	 * @return  如果新增 成功，返回新增记录的ID;否则，返回0
	 */
	public boolean add(ProductCustomer productCustomer){
		boolean flag = false;
		if(productCustomer != null){
			try {
				if(productCustomer.getCustomers() != null &&  productCustomer.getCustomers().size() >0){
					for(Customer customer : productCustomer.getCustomers()){
						DBRow row = new DBRow();
						row.add("tp_pc_id", productCustomer.getProductId());
						row.add("tp_customer_id",customer.getId());
						row.add("tp_title_id", productCustomer.getTitle().getId());
						this.dbUtilAutoTran.insertReturnId(TABLE, row);
					}
				}
				
				//维护关系
				updateCustomerTitleAndCategoryLineRelation(productCustomer.getProductId());
				
				flag = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}
	
	/**
	 * 更新
	 * @param productCustomer
	 * @return
	 */
	public boolean update(ProductCustomer productCustomer){
		boolean flag = false;
		if(productCustomer != null && productCustomer.getCustomers() != null && productCustomer.getCustomers().size() > 0){
			List<Customer> customers = this.getCustomers(productCustomer.getProductId(), productCustomer.getTitle().getId());
			
			List<Customer> addList = new ArrayList<Customer>();
			
			List<Customer> removeList = new ArrayList<Customer>();
			//如果品牌商 在之前的产品品牌关系中存在，但在新的产品品牌关系中不存在
			for(Customer item : customers){
				if(!this.contain(productCustomer.getCustomers(), item)){
					removeList.add(item);
				}
			}
			//如果品牌商 在新的产品品牌关系中存在，但在老的产品品牌关系中不存在
			for(Customer item : productCustomer.getCustomers()){
				if(!this.contain(customers, item)){
					addList.add(item);
				}
			}	
			if(removeList.size() > 0){
				for(Customer item : removeList){
					this.deleteByTitle(productCustomer.getProductId(), item.getId(), productCustomer.getTitle().getId());
				}
			}
			if(addList.size() > 0){
				try {
					for(Customer item : addList){
						DBRow row = new DBRow();
						row.add("tp_pc_id", productCustomer.getProductId());
						row.add("tp_customer_id",item.getId());
						row.add("tp_title_id", productCustomer.getTitle().getId());
						this.dbUtilAutoTran.insertReturnId(TABLE, row);
					}
					
					//维护关系
					updateCustomerTitleAndCategoryLineRelation(productCustomer.getProductId());
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException("Update ProductCustomer failed");
				}	
			}
			
			flag = true;
		}
		return flag;
	}
	
	/**
	 * 判断list中是否包含指定的Customer
	 * @param list
	 * @param customer
	 * @return
	 */
	private boolean contain(List<Customer> list,Customer customer){
		boolean flag = false;
		for(Customer item : list){
			if(item.getId().equals(customer.getId())){
				flag = true;
				break;
			}
		}
		return flag;
	}
	
	/**
	 * 删除指定产品与特定品牌商的关系
	 * @param productId    产品ID
	 * @param customerId   品牌商ID
	 * @return
	 */
	public boolean deleteByTitle(int productId,int titleId){
		boolean flag = false;
		
		String sqlWhere = "where tp_pc_id=" + productId + " and tp_title_id=" + titleId;
		try {
			this.dbUtilAutoTran.delete(sqlWhere, TABLE);
			flag = true;
			
			//维护关系
			updateCustomerTitleAndCategoryLineRelation(productId);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("delete a ProductCustomer failed:productId " + productId + " titleId " + titleId);
		}
		
		return flag;
	}
	
	/**
	 * 为产品的特定品牌商移除特定的生产商
	 * @param productId    产品ID
	 * @param customerId   品牌商ID 
	 * @param titleId	         生产商ID
	 * @return
	 */
	public boolean deleteByTitle(int productId,int customerId,int titleId){
		boolean flag = false;
		
		String sqlWhere = "where tp_pc_id=" + productId + " and tp_customer_id=" + customerId + " and tp_title_id=" + titleId;
		try {
			this.dbUtilAutoTran.delete(sqlWhere, TABLE);
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;		
	}
	
	/**
	 * 判断指定的产品和指定的品牌商是否已建立关系
	 * @param productId   产品ID
	 * @param titleId  生产商ID
	 * @return
	 */
	public boolean  exist(int productId,int titleId){
		boolean flag = false;
		
		try {
			String sql = "select count(1) count from " + TABLE + " a where a.tp_pc_id=? and a.tp_title_id=?";
			DBRow para = new DBRow();
			para.add("tp_pc_id", productId);
			para.add("tp_title_id", titleId);
			DBRow row = this.dbUtilAutoTran.selectPreSingle(sql, para);
			flag = ((int) row.get("count", 0) > 0) ? true : false;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return flag;
	}
	
	/**
	 * 判断指定的产品和指定的生产商、品牌商是否已建立关系
	 * @param productId   产品ID
	 * @param titleId     生产商ID
	 * @param customerId  品牌商ID
	 * @return
	 */
	public boolean  exist(int productId,int titleId,int customerId){
		boolean flag = false;
		
		try {
			String sql = "select count(1) count from " + TABLE + " a where a.tp_pc_id=? and a.tp_title_id=? and a.tp_customer_id=?";
			DBRow para = new DBRow();
			para.add("tp_pc_id", productId);
			para.add("tp_title_id", titleId);
			para.add("tp_customer_id", customerId);
			DBRow row = this.dbUtilAutoTran.selectPreSingle(sql, para);
			flag = ((int) row.get("count", 0) > 0) ? true : false;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return flag;		
	}
	
	/**
	 * 分页查询与特定产品相关的ProductCustomer
	 * @param productId
	 * @return
	 */
	public List<ProductCustomer>  getByProduct(int productId){
		//SELECT a.tp_title_id,a.tp_customer_id,b.customer_name,c.title_name  from title_product a inner JOIN customer_id b on  a.tp_customer_id=b.customer_key inner join title c on a.tp_title_id=c.title_id where a.tp_pc_id=1001747;
		List<ProductCustomer> list = new ArrayList<ProductCustomer>();
		
		Map<Integer,ProductCustomer> map = new HashMap<Integer,ProductCustomer>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple(" SELECT a.tp_customer_id customer_id,b.customer_id customer_name,a.tp_title_id title_id,c.title_name  from title_product a inner JOIN customer_id b on  a.tp_customer_id=b.customer_key inner join title c on a.tp_title_id=c.title_id where a.tp_pc_id=? order by a.tp_title_id desc",param_row);
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					Integer titleId = rows[i].get("title_id", 0);
					ProductCustomer productCustomer = map.get(titleId);
					
					if( productCustomer == null){
						productCustomer = new ProductCustomer();
						productCustomer.setProductId(productId);
						productCustomer.getTitle().setId(rows[i].get("title_id", 0));
						productCustomer.getTitle().setName(rows[i].get("title_name", ""));
						list.add(productCustomer);
						map.put(titleId, productCustomer);
					}
					this.addCustomer(rows[i], productCustomer);
					
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	/**
	 * 获取与特定产品相关的所有ProductCustomer
	 * @param productId
	 * @return
	 */
	public List<Customer>  getCustomers(int productId,int titleId){
		List<Customer> list = new ArrayList<Customer>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			param_row.add("tp_title_id", titleId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple("SELECT distinct a.tp_customer_id customer_id,c.customer_id customer_name FROM title_product a INNER JOIN customer_id c ON a.tp_customer_id= c.customer_key WHERE a.tp_pc_id =? and a.tp_title_id=?",param_row);
			
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToCustomer(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;		
		
	}
	
	/**
	 * 获取与特定产品相关的所有Customer
	 * @param productId 产品ID
	 * @return
	 */
	public List<Customer>  getCustomers(int productId){
		List<Customer> list = new ArrayList<Customer>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple("SELECT distinct a.tp_customer_id customer_id,c.customer_id customer_name FROM title_product a LEFT JOIN customer_id c ON a.tp_customer_id= c.customer_key WHERE a.tp_pc_id =?",param_row);
			
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToCustomer(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;			
		
		
	}
	
	/**
	 * 获取与特定产品相关的所有Title
	 * @param productId 产品ID
	 * @return
	 */
	public List<Title>  getTitles(int productId){
		List<Title> list = new ArrayList<Title>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple(" SELECT distinct title_id,title_name FROM title_product p LEFT JOIN  title t on  p.tp_title_id=t.title_id where p.tp_pc_id=?",param_row);
			
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToTitle(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;			
		
		
	}
	
	/**
	 * 获取与特定产品,特定品牌商相关的所有Title
	 * @param productId 产品ID
	 * @param titleId  生产商ID
	 * @return
	 */
	public List<Title>  getTitles(int productId,int customerId){
		List<Title> list = new ArrayList<Title>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			param_row.add("tp_customer_id", customerId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple("SELECT distinct a.tp_title_id title_id,t.title_name FROM title_product a LEFT JOIN title t ON a.tp_title_id= t.title_id WHERE a.tp_pc_id =? and a.tp_customer_id=?",param_row);
			
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToTitle(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;			
	}
	
	/**
	 * 获取与特定品牌商相关的所有Title
	 * @param customerId 品牌商ID
	 * @return
	 */
	public List<Title>  getTitlesByCustomerId(int customerId){
		List<Title> list = new ArrayList<Title>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_customer_id", customerId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple("select distinct a.tp_title_id title_id,t.title_name from title_product a left join title t on a.tp_title_id=t.title_id  where a.tp_customer_id=?",param_row);
			
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToTitle(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;			
	}
	
	/**
	 * 获取与特定产品,品牌相关的所有ProductCustomer
	 * @param productId
	 * @param customerId  品牌商ID
	 * @return
	 */
	public List<ProductCustomer>  getByProductAndCustomer(int productId,int customerId){
		List<ProductCustomer> list = new ArrayList<ProductCustomer>();
		try {
			DBRow customer_row = this.dbUtilAutoTran.selectSingle("select a.customer_key id,a.customer_id name from customer_id a where a.customer_key=" + customerId);
			
			if(customer_row != null){
				String customerName = customer_row.get("name", "");
				Customer customer = new Customer(customerId,customerName);
				DBRow param_row = new DBRow();
				param_row.add("tp_pc_id", productId);
				param_row.add("tp_customer_id", customerId);
				DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple(" SELECT a.tp_title_id title_id,c.title_name  from title_product a inner join title c on a.tp_title_id=c.title_id where a.tp_pc_id=? and a.tp_customer_id=? order by a.tp_title_id desc",param_row);
				if(rows != null && rows.length > 0){
					for(int i = 0; i < rows.length; i++){
						Integer titleId = rows[i].get("title_id", 0);
						ProductCustomer productCustomer = new ProductCustomer();
						
							productCustomer = new ProductCustomer();
							productCustomer.setProductId(productId);
							productCustomer.getTitle().setId(rows[i].get("title_id", 0));
							productCustomer.getTitle().setName(rows[i].get("title_name", ""));
							
							productCustomer.getCustomers().add(customer);
							list.add(productCustomer);
					}
				}				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;	 
	 }
	
	/**
	 * 获取与特定产品,生产商相关的所有ProductCustomer
	 * @param productId
	 * @param titleId  生产商ID
	 * @return
	 */
	public List<ProductCustomer>  getByProductAndTitle(int productId,int titleId){
		List<ProductCustomer> list = new ArrayList<ProductCustomer>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			param_row.add("tp_title_id", titleId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple(" SELECT a.tp_customer_id customer_id,b.customer_id customer_name,a.tp_title_id title_id,c.title_name  from title_product a inner JOIN customer_id b on  a.tp_customer_id=b.customer_key inner join title c on a.tp_title_id=c.title_id where a.tp_pc_id=?  and a.tp_title_id=?",param_row);
			if(rows != null && rows.length > 0){
				ProductCustomer productCustomer = new ProductCustomer();
				for(int i = 0; i < rows.length; i++){
					if(i == 0){
						productCustomer.setProductId(productId);
						productCustomer.getTitle().setId(rows[i].get("title_id", 0));
						productCustomer.getTitle().setName(rows[i].get("title_name", ""));
					}
					this.addCustomer(rows[i], productCustomer);
				}
				list.add(productCustomer);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	
	/**
	 * 将DBRow 对象转变为对应的 CustomerId对象
	 * @param row
	 * @return
	 */
	private Customer convertToCustomer(DBRow row){
		Assert.notNull(row);
		Customer customer = new Customer();
		customer.setId(row.get("customer_id", 1));
		customer.setName( row.get("customer_name", "") );
		return customer;
	}
	
	/**
	 * 将DBRow 对象转变为对应的 Title对象
	 * @param row
	 * @return
	 */
	private Title convertToTitle(DBRow row){
		Assert.notNull(row);
		Title title = new Title();
		title.setId(row.get("title_id", 1));
		title.setName( row.get("title_name", "") );
		return title;
	}
	
	/**
	 * 往指定的ProductCustomer 中 添加生产商
	 * @param row
	 * @param productCustomer
	 */
	private void addCustomer(DBRow row,ProductCustomer productCustomer){
		Assert.notNull(row);
		Assert.notNull(productCustomer);
		productCustomer.getCustomers().add(this.convertToCustomer(row));
	}
	
	public void updateCustomerTitleAndCategoryLineRelation(long pcId) throws Exception{
		
		DBRow product = floorProprietaryMgrZyj.getProductDetailByPcId(pcId);
		
		//1.2.品牌商/title 与分类父分类的关系[是否需要删除/是否需要添加]
		this.customerTitleAndCategory(product.get("catalog_id", 0L));
		
		DBRow category = floorCatalogMgr.getProductCatalogById(product.get("catalog_id", 0L));
		
		if(category != null){
			
			//3.4.原title与产品线的关系[是否需要删除]
			this.customerTitleAndLine(category.get("product_line_id",0L));
		}
	}
	
	//1.2.品牌商/title 与分类父分类的关系[是否需要删除/是否需要添加]
	public void customerTitleAndCategory(long categoryId) throws Exception{
		
		//如果存在分类
		if(categoryId != 0){
			
			//查询分类与customer/title的关系
			DBRow[] title1 = floorCatalogMgr.getCustomerTitleByCategory(categoryId);
			
			//查询分类下分类和商品与customer/title的关系
			DBRow[] title2 = floorCatalogMgr.getCustomerTitleLowerByCategory(categoryId);
			
			//比较分类与分类下商品和分类的customer/title
			DBRow[] comRes = this.compareCusomerTitle(title1, title2);
			
			for(DBRow one : comRes){
				
				if(one.get("flag", "").equals("insert")){
					
					DBRow param = new DBRow();
					param.put("tpc_title_id", one.get("title",0));
					param.put("tpc_product_catalog_id", categoryId);
					param.put("tpc_customer_id", one.get("customer",0));
					
					floorCatalogMgr.insertTitleProductCatalog(param);
					
				} else {
					
					floorCatalogMgr.deleteTitleProductCatalog(categoryId,one.get("title",0),one.get("customer",0));
				}
			}
			
			//判断是否原父分类依然有父分类
			DBRow parentCategory = floorCatalogMgr.getParentCategory(categoryId);
			
			if(parentCategory != null){
				
				this.customerTitleAndCategory(parentCategory.get("id",0L));
			}
		}
	}
	
	//3.4.品牌商/title 与产品线的关系[是否需要删除][是否需要添加]
	public void customerTitleAndLine(long lineId) throws Exception{
		
		//如果存在产品线
		if(lineId != 0){
			
			//查询产品线与customer/title的关系
			DBRow[] title1 = floorCatalogMgr.getCustomerTitleByLine(lineId);
			//查询产品线下分类与customer/title的关系
			DBRow[] title2 = floorCatalogMgr.getCustomerTitleLowerByLine(lineId);
			
			//比较分类与分类下商品和分类的customer/title
			DBRow[] comRes = this.compareCusomerTitle(title1, title2);
			
			for(DBRow one : comRes){
				
				if(one.get("flag", "").equals("insert")){
					
					DBRow param = new DBRow();
					param.put("tpl_title_id", one.get("title",0));
					param.put("tpl_product_line_id", lineId);
					param.put("tpl_customer_id", one.get("customer",0));
					
					floorCatalogMgr.insertTitleProductLine(param);
					
				}else{
					
					floorCatalogMgr.deleteTitleProductLine(lineId,one.get("title",0L));
				}
			}
		}
	}
	
	public DBRow[] compareCusomerTitle(DBRow[] row1, DBRow[] row2) throws Exception{
		
		List<DBRow> result = new ArrayList<DBRow>();
		
		for(DBRow one : row1){
			
			boolean find = false;
			
			for(DBRow two : row2){
				
				if(one.get("customer",0) == two.get("customer",-1) && one.get("title",0) == two.get("title",-1)){
					
					find = true;
				}
			}
			
			if(!find){
				
				DBRow delRow = new DBRow();
				delRow.put("customer", one.get("customer",0));
				delRow.put("title", one.get("title",0));
				delRow.put("flag", "delete");
				
				result.add(delRow);
			}
		}
		
		for(DBRow one : row2){
			
			boolean find = false;
			
			for(DBRow two : row1){
				
				if(one.get("customer",0) == two.get("customer",-1) && one.get("title",0) == two.get("title",-1)){
					
					find = true;
				}
			}
			
			if(!find){
				
				DBRow delRow = new DBRow();
				delRow.put("customer", one.get("customer",0));
				delRow.put("title", one.get("title",0));
				delRow.put("flag", "insert");
				
				result.add(delRow);
			}
		}
		
		return result.toArray(new DBRow[result.toArray().length]);
		
		//String[] y = x.toArray(new String[0]);
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
