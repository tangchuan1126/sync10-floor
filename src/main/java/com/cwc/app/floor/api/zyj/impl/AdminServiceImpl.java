package com.cwc.app.floor.api.zyj.impl;

import java.sql.SQLException;

import com.cwc.app.floor.api.zyj.service.AdminService;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

/**
 * 该接口定义了查询登录用户信息的方法
 * @author lujintao
 *
 */
public class AdminServiceImpl implements AdminService{
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 查询出登录用户所对应的customerId
	 * @param userId
	 * @return
	 */
	public int getCustomerId(int userId){
		int customerId = 0;
		
		try {
			DBRow row = this.dbUtilAutoTran.selectSingle("select customer_id from admin where adid=" + userId);
			if(row != null){
				customerId = row.get("customer_id", 0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return customerId;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
