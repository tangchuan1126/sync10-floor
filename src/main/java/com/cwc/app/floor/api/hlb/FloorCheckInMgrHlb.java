package com.cwc.app.floor.api.hlb;


import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.DBUtilAutoTranSQLServer;

public class FloorCheckInMgrHlb {
	private DBUtilAutoTran dbUtilAutoTran;
	
	private DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public void setDbUtilAutoTranSQLServer(
			DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer) {
		this.dbUtilAutoTranSQLServer = dbUtilAutoTranSQLServer;
	}
	
	/**
	 * @Title: findDoorInfoById
	 * @Description: 通过 loadNo 查询门信息
	 * @param loadNo
	 * @param entry_Id
	 * @return
	 * @throws Exception
	 */
	public DBRow findDoorInfoById(String loadNo, long entry_Id) throws Exception {
		try {
			String sql = "select  * from storage_door sd join door_or_location_occupancy_details dd  on dd.rl_id = sd.sd_id where dd.load_number='" + loadNo + "'and dd.dlo_id=" + entry_Id ;
			return this.dbUtilAutoTran.selectSingle(sql);
		} catch(Exception e) {
			 throw new Exception("FloorCheckInMgrHlb.findDoorInfoById" + e);
		}
	}
	
	public DBRow[] findMasterBolLinesByLoadNo(String loadNo)throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT boll.OrderNo, boll.CompanyID FROM MasterBOLLines boll");
			sql.append(" JOIN MasterBOLs bol ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ");
			sql.append(" WHERE bol.LoadNo = '").append(loadNo).append("'");
			sql.append(" ORDER BY OrderNo DESC");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception("FloorCheckInMgrHlb.findMasterBolLinesByLoadNo error:"+e);
		}
	}
	
	public DBRow[] findOrdersNoAndCompanyByLoadNo(String loadNo)throws Exception {
		try
		{
			String sql = "SELECT OrderNo, CompanyID FROM Orders WHERE LoadNo = '"+loadNo+"'";
			sql+=" ORDER BY OrderNo DESC";
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorCheckInMgrHlb.findOrdersNoAndCompanyByLoadNo error:"+e);
		}
	}
	
	public DBRow[] findMcDotByCompanyName(String companyName)throws Exception {
		String sql = "select * from carrier_scac_mcdot where carrier = '" + companyName +"' group by carrier";
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * @Title: findCompanyNameByMcDot
	 * @Description: 根据 mc_dot 查询运输公司名字
	 * @param mc_dot
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findCompanyNameByMcDot(String mc_dot) throws Exception{
		String sql = "select carrier from carrier_scac_mcdot where mc_dot = '" + mc_dot +"' and mc_dot <> ' ' group by mc_dot";
		return dbUtilAutoTran.selectMutliple(sql);
	}	
	/**
	 * @Title: findNameByLicense
	 * @Description: 根据车牌号查询驾驶员名字
	 * @param licenseNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findNameByLicense(String licenseNo) throws Exception {
		String sql = "select gate_driver_name from door_or_location_occupancy_main where gate_driver_liscense='" + licenseNo +"' and gate_driver_name <> '' group by gate_driver_liscense";
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * @Title: ModMcDot
	 * @Description: 根据运输公司名字更新 mc_dot
	 * @param companyName
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long ModMcdotByCompanyName(String companyName, DBRow row) throws Exception{
		try{
			 return dbUtilAutoTran.update("where carrier = '" + companyName + "'", "carrier_scac_mcdot", row);
		}catch(Exception e){
			 throw new Exception("ModMcdotByCompanyName: " + e);
		}
	}
	
	/**
	 * @Title: ModCompanyNameByMcdot
	 * @Description: 根据 mc_dot更新运输公司名字
	 * @param mc_dot
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long ModCompanyNameByMcdot(String mc_dot, DBRow row) throws Exception{
		try{
			 return dbUtilAutoTran.update("where mc_dot = '" + mc_dot + "'", "carrier_scac_mcdot", row);
		}catch(Exception e){
			 throw new Exception("ModCompanyNameByMcdot: " + e);
		}
	}
	
	/**
	 * @Title: findCarrierInfoByCompanyName
	 * @Description: 根据运输公司名字查询  carrier_scac_mcdot 表信息
	 * @param companyName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findCarrierInfoByCompanyName(String companyName) throws Exception {
		String sql = "select * from carrier_scac_mcdot where carrier = '" + companyName + "' group by carrier";
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * @Title: addAsset
	 * @Description: 添加 carrier 信息
	 * @param carrierInfo
	 * @return
	 * @throws Exception
	 */
	public long addCarrier(DBRow carrierInfo) throws Exception {
		try {
			
			return  dbUtilAutoTran.insertReturnId("carrier_scac_mcdot", carrierInfo);

		} catch (Exception e) {
			throw new Exception("FloorCheckInMgrHlb addCarrier error:"+e);
		}
	}
	
	/**
	 * @Title: findCarrierInfoByMcdot
	 * @Description: 根据mc_dot查询  carrier_scac_mcdot 表信息
	 * @param mc_dot
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findCarrierInfoByMcdot(String mc_dot) throws Exception {
		String sql = "select * from carrier_scac_mcdot where mc_dot = '" + mc_dot + "' and mc_dot <> ' ' group by carrier";
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * @Title: getSearchCheckInDBCarrierJSON
	 * @Description: carrier 自动提示
	 * @param carrier
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchCheckInDBCarrierJSON(String carrier) throws Exception {
		try{
			String sql = "";
			if (carrier.length() > 2) {
				sql = "select carrier from carrier_scac_mcdot where carrier like '" + carrier + "%'";
			}
			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrHlb getSearchCheckInDBCarrierJSON" + e);
		}
	}
	
	
	/**
	 * @Title: getSearchCheckInDBMcdotJSON
	 * @Description: mc_dot 自动提示
	 * @param mc_dot
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchCheckInDBMcdotJSON(String mc_dot) throws Exception {
		try{
			String sql = "";
			if (mc_dot.length() > 2) {
				sql = "select mc_dot from carrier_scac_mcdot where mc_dot like '" + mc_dot + "%'";
			}
			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrHlb getSearchCheckInDBMcdotJSON" + e);
		}
	}
	
	public DBRow matchNameAndMcdot(String carrierName, String mc_dot) throws Exception {
		
		try{
			DBRow perData = new DBRow();
			perData.add("carrier", carrierName);
			perData.add("mc_dot", mc_dot);
			String sql = "SELECT * FROM carrier_scac_mcdot WHERE carrier=? AND mc_dot=?";

			return this.dbUtilAutoTran.selectPreSingle(sql, perData);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrHlb matchNameAndMcdot" + e);
		}
	}	
	public long updateDrivenInfo(long main_id, DBRow row) throws Exception {
		
		try{
			return dbUtilAutoTran.update("where dlo_id="+main_id, ConfigBean.getStringValue("door_or_location_occupancy_main"), row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrHlb updateDrivenInfo" + e);
		}
	}
	
	public DBRow matchNameAndMcdotInMain(String carrierName, String mc_dot) throws Exception {
		
		try{
			String sql = "SELECT * FROM door_or_location_occupancy_main WHERE company_name='" + carrierName +"' AND mc_dot='" + mc_dot +"'";

			
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrHlb matchNameAndMcdot" + e);
		}
	}		
	/**
	 * @Title: findCompanyNameByMcDot
	 * @Description: 根据 mc_dot 查询运输公司名字
	 * @param mc_dot
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findCompanyNameByMcDotInMain(String mc_dot) throws Exception{
		String sql = "select company_name from door_or_location_occupancy_main where mc_dot = '" + mc_dot +"' and mc_dot <> ' ' group by mc_dot";
		return dbUtilAutoTran.selectMutliple(sql);
	}		
	
	public DBRow[] findMcDotByCompanyNameInMain(String companyName)throws Exception {
		String sql = "select * from door_or_location_occupancy_main where company_name= '" + companyName +"' group by company_name";
		return dbUtilAutoTran.selectMutliple(sql);
	}	
	
	/**
	 * 更新没有windowCheckIn时间的所有设备
	 * @param row
	 * @param entry_id
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月13日 下午12:39:21
	 */
	public void updateEntryEquipmentNoWindowCheckInTime(DBRow row, long entry_id) throws Exception
	{
		try{
			dbUtilAutoTran.update(" where check_in_entry_id = " + entry_id + " and check_in_window_time is null"
					, ConfigBean.getStringValue("entry_equipment"), row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrHlb updateEntryEquipmentNoWindowCheckInTime" + e);
		}
	}
	
	public DBRow[] findPalletInfosByDetail_id(long detail_id, long delivery_or_pick_up)
		    throws Exception {
			  String sql = "";
			if(delivery_or_pick_up==1) {
				sql = "SELECT pallet_number, pallet_type, pallet_type_count FROM wms_receive_pallet_type_count WHERE dlo_detail_id ="+ detail_id;
			} else if(delivery_or_pick_up==2) {
			    sql = "SELECT wms_pallet_number pallet_number, wms_pallet_type pallet_type, wms_pallet_type_count pallet_type_count FROM wms_load_order_pallet_type_count WHERE dlo_detail_id ="+ detail_id;
			}
		    
		    return this.dbUtilAutoTran.selectMutliple(sql);
	}
}
