package com.cwc.app.floor.api.tjh;




import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorProductLineMgrTJH {
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 获取所有的产品线信息
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllProductLine() 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_line_define")+" order by name";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllProductLine() error："+e);
		}
	}

	
	/**
	 * 根据产品线id获取该产品线下的所有的产品类别
	 * @param productLineId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductCatalogByProductLineId(long productLineId) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from product_catalog where id IN " +
						 "(select DISTINCT pcl.search_rootid from product_catalog as pc JOIN pc_child_list as pcl ON pc.id = pcl.pc_id where product_line_id=?) " +
						 "order by title";
			
			DBRow para = new DBRow();
			para.add("product_line_id",productLineId);
			
			return (dbUtilAutoTran.selectPreMutliple(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProductCatalogByProductLineId(long productLineId) error："+e);
		}
	}
	
	public DBRow[] getProductCatalogByProductLineId(long productLineId,long parent_catalog_id) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from product_catalog where id IN " +
						 "(select DISTINCT pcl.search_rootid from product_catalog as pc JOIN pc_child_list as pcl ON pc.id = pcl.pc_id where product_line_id=?) and parentid=?" +
						 "order by title";
			
			DBRow para = new DBRow();
			para.add("product_line_id",productLineId);
			para.add("parent_catalog_id",parent_catalog_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProductCatalogByProductLineId(long productLineId) error："+e);
		}
	}
	
	/**
	 * 根据产品线，商品分类ID，获得该分类下所有与父类产品线ID同样的分类
	 * @param product_line_id
	 * @param product_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSonProductCatalogByProductLiineId(long product_line_id,long catalog_id)
	throws Exception
	{
		try 
		{
			String sql = "SELECT DISTINCT pc.* FROM product_catalog AS pc JOIN pc_child_list AS pcl ON pc.id = pcl.pc_id WHERE product_line_id = ? and search_rootid=?";
			
			DBRow para = new DBRow();
			para.add("product_line_id",product_line_id);
			para.add("catalog_id",catalog_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductLineMgrTJH getSonProductCatalogByProductLiineId error:"+e);
		}
	}

	/**
	 * 添加产品线信息
	 * @param params
	 * @throws Exception 
	 */
	public long addProductLine(DBRow params) 
		throws Exception 
	{
		try 
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("product_line_define"));
			params.add("id", id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("product_line_define"), params);
			return id;
		} 
		catch (Exception e) 
		{
			throw new Exception("addProductLine(DBRow params) error:"+e);
		}
	}


	/**
	 * 修改某一产品线下的产品类别
	 * @param productLineId
	 * @throws Exception
	 */
	public void modProductLine(long productLineId) 
		throws Exception 
	{
		try 
		{
			DBRow row = new DBRow();
			dbUtilAutoTran.update("where product_line_id="+productLineId, ConfigBean.getStringValue("product_catalog"),row);
		} 
		catch (Exception e) 
		{
			throw new Exception("modProductLine(long productLineId) error："+e);
		}
	}


	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}


	/**
	 * 给产品线添加产品类别
	 * @param catalog_id
	 * @param param
	 * @throws Exception 
	 */
	public void updateProductCatalogByPId(long catalog_id, DBRow param) 
		throws Exception 
	{
		dbUtilAutoTran.update("where id in (select pc_id from pc_child_list where search_rootid = "+catalog_id+")", ConfigBean.getStringValue("product_catalog"), param);
	}
	
	/**
	 * 清除产品线下的产品分类（父类内子类不属于该产品线的不会被清除）
	 * @param product_line_id
	 * @param catalog_id
	 * @param para
	 * @throws Exception
	 */
	public void cleanProductCatalogForProductLine(long product_line_id,long catalog_id,DBRow para)
		throws Exception
	{
		dbUtilAutoTran.update("where id in(SELECT DISTINCT pcl.pc_id FROM product_catalog AS pc JOIN pc_child_list AS pcl ON pc.id = pcl.pc_id WHERE product_line_id = "+product_line_id+" and search_rootid="+catalog_id+")",ConfigBean.getStringValue("product_catalog"),para);
	}

	/**
	 * 获取某一详细的产品线信息
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getProductLineById(long id) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_line_define")+" where id="+id;
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProductLineById(long id) error："+e);
		}
	}


	/**
	 * 删除产品线信息
	 * @param id
	 * @throws Exception 
	 */
	public void deleteProductLine(long id) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.delete("where id="+id, ConfigBean.getStringValue("product_line_define"));
		} 
		catch (Exception e) 
		{
			throw new Exception("deleteProductLine(long id) error："+e);
		}
		
	}


	/**
	 * 修改某一具体的产品线信息
	 * @param id
	 * @param row
	 * @throws Exception
	 */
	public void updateProductLine(long id, DBRow row) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.update("where id="+id, ConfigBean.getStringValue("product_line_define"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("updateProductLine(long id, DBRow row) error："+e);
		}
		
	}


	/**
	 * 将产品类别中的产品线信息清空
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	public void deleteProductCatalogByLineId(long id,DBRow param) 
		throws Exception 
	{
		try 
		{

			dbUtilAutoTran.update("where id="+id, ConfigBean.getStringValue("product_catalog"), param);
		} 
		catch (Exception e) 
		{
			throw new Exception("deleteProductCatalogByLineId(long id) error："+e);
		}
	}
	
	//根据产品线名称获取产品线信息
	public DBRow getProductLineByName(String name) throws Exception{
		try {
			String sql = "select * from "+ConfigBean.getStringValue("product_line_define")+" where name='"+name+"'";
			return (dbUtilAutoTran.selectSingle(sql));
		} catch (Exception e) {
			throw new Exception("getProductLineByName(name) error:"+e);
		}
	}	
	
	/**
	 * 清空一个关联分类的产品线
	 * @param product_line_id
	 * @author subin
	 */
	public void emptyProductLineForProductCatalog(long product_line_id) throws Exception {
		
		DBRow row = new DBRow();
		row.add("product_line_id", null);
		
		String where = "where product_line_id = "+product_line_id;
		
		dbUtilAutoTran.update(where,"product_catalog",row);
	}
}
