package com.cwc.app.floor.api.zyj;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorProductMgrZyj{

	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 通过商品Id，文件类型，文件关联ID查询商品文件
	 * @param pc_id
	 * @param file_with_type
	 * @param file_with_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllProductFileByPcId(String pc_id, long file_with_type, String file_with_id, int is_unable_provide) throws Exception {
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select product_file.* ,product.p_name from product_file left join product on product_file.pc_id = product.pc_id where 1 = 1");
			if(!"".equals(pc_id))
			{
				String[] pc_ids = pc_id.split(",");
				if(pc_ids.length > 0)
				{
					sql.append(" and (product_file.pc_id = ").append(pc_ids[0]);
					for (int i = 1; i < pc_ids.length; i++) {
						sql.append(" or product_file.pc_id = ").append(pc_ids[i]);
					}
					sql.append(")");
				}
			}
			if(0 != file_with_type && -1 != file_with_type)
			{
				sql.append(" and file_with_type=").append(file_with_type);
			}
			if(!"".equals(file_with_id))
			{
				String[] file_with_ids = file_with_id.split(",");
				if(file_with_ids.length > 0)
				{
					for (int i = 0; i < file_with_ids.length; i++)
					{
						String regEx="[^0-9]";   
						Pattern p = Pattern.compile(regEx);   
						Matcher m = p.matcher(file_with_ids[i]);   
						if(0 == i)
						{
							sql.append(" and (file_with_id = ").append(m.replaceAll("").trim());
						}
						else
						{
							sql.append(" or file_with_id = ").append(m.replaceAll("").trim());
						}
					}
					sql.append(")");
				}
				
			}
			if(1 == is_unable_provide || 2 == is_unable_provide)
			{
				sql.append(" AND is_unable_to_provide = " + is_unable_provide);
			}
			return dbUtilAutoTran.selectMutliple(sql.toString());
			 
		}
		catch(Exception e)
		{
			throw new Exception("FloorProductMgrZyj.getAllProductFileByPcId(DBRow):"+e);
		}
	}
	
	/**
	 * 通过商品文件的ID，查询商品文件记录
	 * @param product_file_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getProductFileByProductFileId(long product_file_id) throws Exception
	{
		try 
		{
			String sql = "select * from product_file where pf_id = " + product_file_id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgrZyj.getProductFileByProductFileId(DBRow):"+e);
		}
	}
	
	/**
	 * 根据商品Id，文件类型，商品文件类型查询文件
	 * @param pc_id
	 * @param file_with_types
	 * @param file_types
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductFileByPcIdFileTypes(long pc_id, int[] file_with_types, int[] file_types) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT * FROM product_file WHERE pc_id = ").append(pc_id);
			for (int i = 0; i < file_with_types.length; i++) 
			{
				if(0 == i)
				{
					sb.append(" AND (file_with_type = ").append(file_with_types[i]);
				}
				else
				{
					sb.append(" OR file_with_type = ").append(file_with_types[i]);
				}
			}
			sb.append(")");
			for (int i = 0; i < file_types.length; i++) 
			{
				if(0 == i)
				{
					sb.append(" AND (product_file_type = ").append(file_types[i]);
				}
				else
				{
					sb.append(" OR product_file_type = ").append(file_types[i]);
				}
			}
			sb.append(")");
			return dbUtilAutoTran.selectMutliple(sb.toString());
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZyj.getProductFileByPcIdFileTypes(long pc_id, int[] file_with_types, int[] file_types):"+e);
		}
	}
	
	/**
	 * 将商品、商品线、条码关联，根据产品线查询
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductInfosProductLineProductCodeByLineId(long pc_line_id, PageCtrl pc) throws Exception
	{
		try 
		{
			StringBuffer sb = new StringBuffer();
			
			sb.append("select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc from ");
			sb.append("(select s.* from ");
			sb.append("	(select w.pc_id , w.alive, w.catalog_id, w.heigth, w.length,");
			sb.append("				w.orignal_pc_id, w.p_name, w.union_flag, w.unit_name,");
			sb.append("				w.unit_price, w.volume, w.weight, w.width");
			sb.append("	 from product w left JOIN");
			sb.append("		(select p.pc_id , n.count_product from product p LEFT JOIN");
			sb.append("			(select count(*) as count_product,pc_id  ");
			sb.append("					from (select * from product_file where file_with_type = 31 GROUP BY pc_id, product_file_type) m GROUP BY pc_id)n");
			sb.append("			on n.pc_id = p.pc_id ");
			sb.append("			where p.orignal_pc_id = 0");
			sb.append("			and n.count_product = 5 ");
			sb.append("			and p.orignal_pc_id = 0)q");
			sb.append("	ON w.pc_id = q.pc_id where w.orignal_pc_id = 0 and (q.count_product is null or q.count_product < 5) )s");
			sb.append(" LEFT JOIN product_catalog c ON s.catalog_id = c.id LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
			sb.append(" WHERE l.id IS NOT NULL AND l.id = ").append(pc_line_id).append( ") z");
			sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
			sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
			sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
			return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZyj.getProductInfosProductLineProductCode(long pc_line_id):"+e);
		}
	}
	
	/**
	 * 得到需要上传文件的商品总数
	 * @return
	 * @throws Exception
	 */
	public DBRow getProductFileNeedUploadTotal() throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			
			sb.append("select count(*) as need_upload_product_total from product w left JOIN");
			sb.append(" (select p.pc_id , n.count_product from product p LEFT JOIN");
			sb.append(" (select count(*) as count_product,pc_id from ");
			sb.append(" (select * from product_file where file_with_type = 31 GROUP BY pc_id, product_file_type) m GROUP BY pc_id)n on n.pc_id = p.pc_id ");
			sb.append(" where p.orignal_pc_id = 0");
			sb.append(" and n.count_product = 5 ");
			sb.append(" and p.orignal_pc_id = 0)q");
			sb.append(" ON w.pc_id = q.pc_id where w.orignal_pc_id = 0 and (q.count_product is null or q.count_product < 5)");
			
			return dbUtilAutoTran.selectSingle(sb.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZyj.getProductFileNeedUploadTotal():"+e);
		}
	}
	
	/**
	 * 需要上传文件的商品，按产品线分
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductFileNeedUploadByProductLineGroupBy() throws Exception
	{
		try 
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select count(*) as need_upload_pc_count,l.id as product_line_id,l.`name` as product_line_name from ");
			sb.append("	(select w.pc_id,w.catalog_id from product w left JOIN");
			sb.append("		(select p.pc_id , n.count_product from product p LEFT JOIN");
			sb.append("			(select count(*) as count_product,pc_id  ");
			sb.append("				from (select * from product_file where file_with_type = 31 GROUP BY pc_id, product_file_type) m GROUP BY pc_id)n");
			sb.append("			on n.pc_id = p.pc_id ");
			sb.append("			where p.orignal_pc_id = 0");
			sb.append("			and n.count_product = 5 ");
			sb.append("			and p.orignal_pc_id = 0)q");
			sb.append("		ON w.pc_id = q.pc_id where w.orignal_pc_id = 0 and (q.count_product is null or q.count_product < 5) )s");
			sb.append(" LEFT JOIN product_catalog c ON s.catalog_id = c.id LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
			sb.append(" WHERE l.id IS NOT NULL GROUP BY l.id");
			return dbUtilAutoTran.selectMutliple(sb.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZyj.getProductFileNeedUploadByProductLineGroupBy():"+e);
		}
	}
	
	/**
	 * 根据商品Id，文件类型，商品文件类型查询文件
	 * @param pc_id
	 * @param file_with_types
	 * @param file_types
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductFileByPcIdFileTypesAndIsUnableProvide(long pc_id, long file_with_id, int[] file_with_types, int[] file_types, int is_unable_to_provide) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT * FROM product_file WHERE pc_id = ").append(pc_id);
			sb.append(" AND file_with_id = ").append(file_with_id);
			for (int i = 0; i < file_with_types.length; i++) 
			{
				if(0 == i)
				{
					sb.append(" AND (file_with_type = ").append(file_with_types[i]);
				}
				else
				{
					sb.append(" OR file_with_type = ").append(file_with_types[i]);
				}
			}
			sb.append(")");
			for (int i = 0; i < file_types.length; i++) 
			{
				if(0 == i)
				{
					sb.append(" AND (product_file_type = ").append(file_types[i]);
				}
				else
				{
					sb.append(" OR product_file_type = ").append(file_types[i]);
				}
			}
			sb.append(")");
			if(1 == is_unable_to_provide)
			{
				sb.append("AND is_unable_to_provide = " + is_unable_to_provide);
			}
			return dbUtilAutoTran.selectMutliple(sb.toString());
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZyj.getProductFileByPcIdFileTypes(long pc_id, int[] file_with_types, int[] file_types):"+e);
		}
	}
	
	
	/**
	 * 通过商品ID，查询商品相关信息
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findProductInfosByPcid(long pc_id) throws Exception
	{
		try 
		{
			StringBuffer sb = new StringBuffer();
			
			sb.append("select p.*,c.*, l.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc ");
			sb.append(" from product p");
			sb.append(" JOIN product_code AS pcode_main ON p.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
			sb.append("	LEFT JOIN product_catalog c ON p.catalog_id = c.id");
			sb.append(" LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
			sb.append(" LEFT JOIN product_code AS pcode_amazon ON p.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
			sb.append(" LEFT JOIN product_code AS pcode_upc ON p.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
			sb.append(" where p.pc_id = ").append(pc_id);
//			System.out.println("oneProduct:"+sb.toString());
			return dbUtilAutoTran.selectSingle(sb.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZyj.findProductInfosByPcid:"+e);
		}
	}
	
	public void deleteAllBasicDatas() throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("", ConfigBean.getStringValue("product"));
			dbUtilAutoTran.delete("", ConfigBean.getStringValue("product_code"));
			dbUtilAutoTran.delete("", ConfigBean.getStringValue("product_line_define"));
			dbUtilAutoTran.delete("", ConfigBean.getStringValue("product_catalog"));
			dbUtilAutoTran.delete("", ConfigBean.getStringValue("product_edit_logs"));
			dbUtilAutoTran.delete("", ConfigBean.getStringValue("title_product"));
			dbUtilAutoTran.delete("", ConfigBean.getStringValue("title_product_catalog"));
			dbUtilAutoTran.delete("", ConfigBean.getStringValue("title_product_line"));
			dbUtilAutoTran.delete("", ConfigBean.getStringValue("container_type"));
			dbUtilAutoTran.delete("", ConfigBean.getStringValue("license_plate_type"));
			dbUtilAutoTran.delete("", ConfigBean.getStringValue("lp_title_ship_to"));
			dbUtilAutoTran.delete(" where ship_to_id<>0", ConfigBean.getStringValue("ship_to"));
			dbUtilAutoTran.delete(" where storage_type = " + StorageTypeKey.RETAILER, ConfigBean.getStringValue("product_storage_catalog"));
			dbUtilAutoTran.delete("", ConfigBean.getStringValue("pc_child_list"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrZyj.deleteAllBasicDatas:"+e);
		}
	}
	
	/**
	 * 通过分类ID查商品
	 * @param catalog_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public int getProductsByCategoryid(long catalog_id,PageCtrl pc)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT COUNT(DISTINCT(p.pc_id)) cn FROM product p");
			sql.append(" JOIN pc_child_list AS pcl ON p.catalog_id = pcl.pc_id");
			sql.append(" WHERE pcl.search_rootid = ").append(catalog_id);
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			if(null != row)
			{
//				System.out.println("c:"+catalog_id+","+row.get("cn", 0));
				return row.get("cn", 0);
			}
			return 0;
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductsByCategoryid(row) error:" + e);
		}
	}
	
	/**
	 * 功能；根据catalog_id和title_id统计商品数
	 * @param catalog_id
	 * @param title_id
	 * @return
	 * @throws Exception
	 */
	public int getProductsByCategoryidAndTitleid(long catalog_id, long title_id) throws Exception{
		StringBuffer sqlBuffer =  new StringBuffer();
		
		sqlBuffer.append("SELECT COUNT(DISTINCT p.pc_id) cn ")
				 .append("  FROM product p ")
				 .append("  JOIN pc_child_list pcl ON pcl.pc_id = p.catalog_id ")
				 .append("  JOIN title_product tp ON tp.tp_pc_id = p.pc_id ")
				 .append(" WHERE 1 = 1 ")
				 .append("   AND pcl.search_rootid = " + catalog_id)
				 .append("   AND tp.tp_title_id = " + title_id);
		
		try {
			DBRow count = dbUtilAutoTran.selectSingle(sqlBuffer.toString());
			
			if(count!=null && count.size()>0)
			{
				return count.get("cn", 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorProductMgr.getProductsByCategoryidAndTitleid(long catalog_id, long title_id) error:" + e);
		}
		
		return 0;
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
