package com.cwc.app.floor.api.cc;

import java.io.FileOutputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.WebcamPositionTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorGoogleMapsMgrCc{
	static Logger log = Logger.getLogger("ACTION");
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 获取GoogleMap边界数据（国家）
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getGoogleMapsBoundaryByCountry(String ids) throws Exception{
		try {
			String sql = "SELECT cc.ccid region_id, cc.c_country region_name, cc.c_code region_code, cb.cb_id boundary_id, cb.boundary boundary" +
					" FROM "+ConfigBean.getStringValue("country_code")+" cc " +
					" LEFT JOIN "+ConfigBean.getStringValue("country_boundary")+" cb on cc.ccid = cb.cc_id" +
					" WHERE cc.ccid IN ("+ids+")";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getGoogleMapsBoundaryByCountry():"+e);
		}
	}
	/**
	 * 获取GoogleMap边界数据（省/州）
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getGoogleMapsBoundaryByProvince(String ids) throws Exception{
		try {
			String sql = "SELECT cp.pro_id region_id, cp.nation_id parent_region_id, cp.pro_name region_name, cp.p_code region_code, cpb.pb_id boundary_id, cpb.boundary boundary" +
					" FROM "+ConfigBean.getStringValue("country_province")+" cp" +
					" LEFT JOIN "+ConfigBean.getStringValue("country_province_boundary")+" cpb on cp.pro_id = cpb.pro_id" +
					" WHERE cp.pro_id IN ("+ids+")";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getGoogleMapsBoundaryByCountry():"+e);
		}
	}
	/**
	 * 查询所有gps设备
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllAsset() throws Exception{
		try {
			String t_asset = ConfigBean.getStringValue("t_asset");
			String t_group = ConfigBean.getStringValue("t_group");
			String t_group_asset = ConfigBean.getStringValue("t_group_asset");
			String t_icon = ConfigBean.getStringValue("t_icon");
			String t_asset_type = ConfigBean.getStringValue("t_asset_type");
			String sql = "select IFNULL(g.id,0) group_id,IFNULL(g.`name`,'No group') group_name,IFNULL(g.def,'No group') group_def, " +
					"	a.id asset_id,a.`name` asset_name,a.callnum asset_callnum, " +
					"	a.def asset_def,a.demo asset_demo,a.imei asset_imei,aty.`name` asset_type,i.url icon_url " +
					"from " + t_asset + " a " +
					"LEFT JOIN " + t_group_asset  + " ga ON a.id = ga.assetid " +
					"LEFT JOIN " + t_group + " g ON g.id = ga.groupid " +
					"LEFT JOIN " + t_icon + " i ON a.icon = i.id " +
					"LEFT JOIN " + t_asset_type + " aty ON aty.id = a.assettype " +
					"ORDER BY g.id,a.assettype ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getAllAssets():"+e);
		}
	}
	/**
	 * 查询所有gps groupby groupid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllAssetGroupByGroupId() throws Exception{
		try {
			String t_asset = ConfigBean.getStringValue("t_asset");
			String t_group = ConfigBean.getStringValue("t_group");
			String t_group_asset = ConfigBean.getStringValue("t_group_asset");
			String t_icon = ConfigBean.getStringValue("t_icon");
			String t_asset_type = ConfigBean.getStringValue("t_asset_type");
			String sql = "select IFNULL(g.id,0) group_id,IFNULL(g.`name`,'No group') group_name " +
					"from " + t_asset + " a " +
					"LEFT JOIN " + t_group_asset  + " ga ON a.id = ga.assetid " +
					"LEFT JOIN " + t_group + " g ON g.id = ga.groupid " +
					"LEFT JOIN " + t_icon + " i ON a.icon = i.id " +
					"LEFT JOIN " + t_asset_type + " aty ON aty.id = a.assettype " +
					"GROUP BY g.id "+
					"ORDER BY g.id";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getAllAssets():"+e);
		}
	}
	/**
	 * 查询所有gps设备by groupId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllAssetByGroupId(long groupId) throws Exception{
		try {
			
			String wherecond ="";
			if(groupId==0l){
				wherecond=" where g.id is null";
			}else{
				wherecond=" where g.id ="+groupId;
			}
			String t_asset = ConfigBean.getStringValue("t_asset");
			String t_group = ConfigBean.getStringValue("t_group");
			String t_group_asset = ConfigBean.getStringValue("t_group_asset");
			String t_icon = ConfigBean.getStringValue("t_icon");
			String t_asset_type = ConfigBean.getStringValue("t_asset_type");
			String sql = "select IFNULL(g.id,0) group_id,IFNULL(g.`name`,'No group') group_name,IFNULL(g.def,'No group') group_def, " +
					"	a.id asset_id,a.`name` asset_name,a.callnum asset_callnum, " +
					"	a.def asset_def,a.demo asset_demo,a.imei asset_imei,aty.`name` asset_type,i.url icon_url " +
					"from " + t_asset + " a " +
					"LEFT JOIN " + t_group_asset  + " ga ON a.id = ga.assetid " +
					"LEFT JOIN " + t_group + " g ON g.id = ga.groupid " +
					"LEFT JOIN " + t_icon + " i ON a.icon = i.id " +
					"LEFT JOIN " + t_asset_type + " aty ON aty.id = a.assettype " +
					wherecond+
					" ORDER BY g.id,a.assettype ";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getAllAssets():"+e);
		}
	}
	/**
	 * 查询所有仓库KML
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllStorageKml() throws Exception{

		try {
			String storage_kml = ConfigBean.getStringValue("storage_kml");
			String storage_warehouse_base = ConfigBean.getStringValue("storage_warehouse_base");
			String product_storage_catalog = ConfigBean.getStringValue("product_storage_catalog");
			String sql ="select  DISTINCT s.ps_id ,p.title,p.deliver_city deliver_city ,sk.kml ,sk.id sk_id "
					+ " from " +storage_warehouse_base 
					+" s join "
					+  product_storage_catalog 
					+" p on s.ps_id=p.id "
					+" LEFT JOIN "
					+ storage_kml
					+" sk on s.ps_id=sk.ps_id  ORDER BY p.sort,s.ps_id ;";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getAllAssets():"+e);
		}
	}
	/**
	 * 查询单个仓库KML
	 * @return
	 * @throws Exception
	 */
	public DBRow getStorageKmlByPsId(int psId) throws Exception{
		try {
			String sql ="select DISTINCT s.ps_id ,p.title,p.deliver_city deliver_city ,sk.kml ,sk.id sk_id "
					+" from storage_warehouse_base" 
					+" s join product_storage_catalog"
					+" p on s.ps_id=p.id "
					+" LEFT JOIN storage_kml "
					+" sk on s.ps_id=sk.ps_id "
					+" where s.ps_id="+psId
					+" ORDER BY p.sort,s.ps_id ";
			return dbUtilAutoTran.selectSingle(sql);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getAllAssets():"+e);
		}
	}
	public long addStorageKml(DBRow row) throws Exception{
		try {
			String tablename="storage_kml";
			return dbUtilAutoTran.insertReturnId(tablename, row);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.addStorageKml():"+e);
		}
	}
	/**
	 * 查询设备右键菜单命令
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getContextCommand() throws Exception {
		try {
			String sql = "select * from t_command c order by c.group";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getContextCommand():" + e);
		}

	}
public DBRow[] getParkingDocksOccupancy(long ps_id) throws Exception {
	try {
		String sql="SELECT DISTINCT "
				+ "'docks' obj_type, sd.sd_id obj_id, "
				+ "sd.ps_id, srr.occupy_status, ee.rel_type, "
				+ "dd.number_status number_status, "
				+ "dd.number_type number_type  "
				+ "FROM storage_door sd "
				+ "JOIN space_resources_relation srr ON sd.sd_id = srr.resources_id "
				+ "JOIN entry_equipment ee ON ee.equipment_id = srr.relation_id "
				+ "LEFT JOIN  door_or_location_occupancy_details dd "
				+ "ON ee.equipment_id=dd.equipment_id "
				+ "and ee.rel_type=3 and number_status=2 "
				+ "WHERE "
				+ "srr.resources_type = 1 "
				+ "AND srr.relation_type = 1 "
				+ "AND sd.ps_id =? "
				+ "UNION ALL "
				+ "SELECT DISTINCT "
				+ "'parking' obj_type, yc.yc_id obj_id,"
				+ "yc.ps_id, srr.occupy_status, ee.rel_type,"
				+ "dd.number_status number_status,"
				+ "dd.number_type number_type "
				+ "FROM storage_yard_control yc "
				+ "JOIN space_resources_relation srr ON yc.yc_id = srr.resources_id "
				+ "JOIN entry_equipment ee ON ee.equipment_id = srr.relation_id "
				+ "LEFT JOIN  door_or_location_occupancy_details  dd   ON  ee.equipment_id=dd.equipment_id  and ee.rel_type=3 and number_status=2 "
				+ "WHERE "
				+ "srr.resources_type = 2 "
				+ "AND srr.relation_type = 1 "
				+ "AND yc.ps_id = ? "
				+ "ORDER BY obj_type, obj_id ";
				DBRow r = new DBRow();
				r.add("psid", ps_id);
				r.add("psid1", ps_id);
		return dbUtilAutoTran.selectPreMutliple(sql, r);
	} catch (Exception e) {
		e.printStackTrace();
		throw new Exception("FloorGoogleMapsMgrCc.getParkingDocksOccupancy():" + e);
	}
}
public DBRow[] getParkingDocksOccupancyDetail(long id ,int type) throws Exception {
	try {
		String sql="SELECT DISTINCT srr.relation_id," //-- 设备的id 
				+ " srr.resources_type, "//-- 资源的类型
				+ " srr.resources_id,"//-- 资源的id
				+ " srr.occupy_status,"// -- 占用或者保留 
				+ " srr.module_id,"// -- entryid 
				+ " ee.equipment_status equipment_status, "// -- 设备状态
				+ " ee.equipment_number equipment_number, "//-- 设备号
				+ " ee.equipment_type equipment_type,"//  -- 车头或者车尾 
				+ " ee.rel_type rel_type, "// -- delivery 、 pick up 、both、none 
				+ " ee.check_in_warehouse_time check_in_time ,"
				+ " dd.number_status number_status,"//-- 任务状态  
				+ " dd.number number, "// -- 任务编号
				+ " dd.number_type number_type "//-- 任务类型
				+ " FROM space_resources_relation srr "
				+ " JOIN entry_equipment ee ON srr.relation_type = 1 "
				+ " and srr.relation_id=ee.equipment_id "
				+ " LEFT JOIN door_or_location_occupancy_details dd  "
				+ " on ee.equipment_id=dd.equipment_id "
				+ " WHERE srr.resources_id ="+id
				+ " AND srr.resources_type ="+type
				+ "  ORDER BY relation_id ";
		return dbUtilAutoTran.selectMutliple(sql);
	} catch (Exception e) {
		e.printStackTrace();
		throw new Exception("FloorGoogleMapsMgrCc.getParkingDocksOccupancy():" + e);
	}
}
/*public DBRow[] getParkingDocksOccupancyDetail(long id ,int type) throws Exception {
	try {
		String sql=" SELECT DISTINCT srr.relation_type, srr.relation_id, srr.resources_type,srr.resources_id, srr.occupy_status, srr.module_id, ee.equipment_status item_one, ee.equipment_number item_two, ee.equipment_type item_three, ee.rel_type item_four, ee.check_in_time item_five "
				+ " FROM space_resources_relation srr "
				+ " JOIN entry_equipment ee ON srr.relation_type = 1"
				+ " AND srr.relation_id = ee.equipment_id"
				+ " WHERE srr.resources_id ="+id+" and srr.resources_type ="+type
				+ " union all "
				+ " SELECT DISTINCT srr.relation_type, srr.relation_id, srr.resources_type, srr.resources_id, srr.occupy_status, srr.module_id,dd.number_status item_one, dd.number item_two, ee.equipment_number item_three, dd.number_type item_four, '' item_five"
				+ " FROM space_resources_relation srr"
				+ " JOIN door_or_location_occupancy_details dd ON srr.relation_type = 2"
				+ " JOIN entry_equipment ee on dd.equipment_id=ee.equipment_id "
				+ " AND srr.relation_id = dd.dlo_detail_id"
				+ " WHERE srr.resources_id ="+id+" and srr.resources_type ="+type;
		return dbUtilAutoTran.selectMutliple(sql);
	} catch (Exception e) {
		e.printStackTrace();
		throw new Exception("FloorGoogleMapsMgrCc.getParkingDocksOccupancy():" + e);
	}
}
*/
	/**
	 * 查询围栏
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getGeoFencing(PageCtrl pc) throws Exception{
		String sql = "select l.*,t.`name` type,LOWER(t.nameen) typeen from t_alarm_label l,t_map_type t where l.geotype in (3,4,5) and l.geotype = t.id";
		try{
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getGeoFencing:" + e);
		}
	}
	/**
	 * 查询线路
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getGeoLine(PageCtrl pc) throws Exception{
		String sql = "select l.*,t.`name` type,LOWER(t.nameen) typeen from t_alarm_label l,t_map_type t where l.geotype = 2 and l.geotype = t.id";
		try{
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getGeoLine:" + e);
		}
	}
	/**
	 * 查询关键点
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getGeoPoint(PageCtrl pc) throws Exception{
		String sql = "select l.*,t.`name` type,LOWER(t.nameen) typeen from t_alarm_label l,t_map_type t where l.geotype = 1 and l.geotype = t.id";
		try{
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getGeoLine:" + e);
		}
	}
	/**
	 * 查询AlarmLabel
	 * @param row
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getGeoAlarmLabel(DBRow row, PageCtrl pc) throws Exception{
		String geotype = row.get("geotype", "");
		try{
			String sql = "select l.*,t.`name` type,LOWER(t.nameen) typeen from t_alarm_label l,t_map_type t where l.geotype in("+geotype+") and l.geotype = t.id";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getGeoAlarmLabel:" + e);
		}
	}
	/**
	 * AlarmLabel数量统计
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getGeoAlarmLabelCount() throws Exception{
		String sql = "select l.geotype,count(1) num from t_alarm_label l GROUP BY l.geotype";
		try{
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getGeoAlarmLabelCount:" + e);
		}
	}
	/**
	 * 保存围栏
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long saveGeoALarmLabel(DBRow row) throws Exception{
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("t_alarm_label"), row);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.saveGeoALarmLabel:" + e);
		}
	}
	/**
	 * 删除围栏
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	public long removeAlarmLabel(String ids) throws Exception{
		try {
			String wherecond = " where id in ("+ids+") ";
			return dbUtilAutoTran.delete(wherecond, ConfigBean.getStringValue("t_alarm_label"));
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.removeAlarmLabel:" + e);
		}
	}
	/**
	 * 查询asset关联的alarm
	 * @param row
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAlarmByAsset(DBRow row, PageCtrl pc) throws Exception{
		String id = row.get("assetid", "");
		String type = row.get("geotype", "");
		try{
			String sql = "select aa.id asset_alarm_id,aa.alarmtype alarm_type, t.`name` alarm_name, a.* from t_alarm_label a ,t_asset_alarm_label aa, t_alarm_type t" +
						" where a.id = aa.alarmid" +
						" and aa.alarmtype = t.id" +
						" and aa.assetid = "+id+
						"  and a.geotype in ("+type+")";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getAlarmByAsset:" + e);
		}
	}
	/**
	 * 查询asset未关联的alarm
	 * @param row
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAlarmNoAsset(DBRow row, PageCtrl pc) throws Exception{
		String id = row.get("assetid", "");
		String type = row.get("geotype", "");
		try{
			String sql = "select * from t_alarm_label a " +
			"  where a.id not in (select aa.alarmid from t_asset_alarm_label aa where aa.assetid = "+id+")" +
			"  and a.geotype in ("+type+")";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getAlarmNoAsset:" + e);
		}
	}
	/**
	 * 取消asset关联的alarm
	 * @param id
	 * @throws Exception
	 */
	public void removeAssetAlarm(long id) throws Exception{
		String tablename = ConfigBean.getStringValue("t_asset_alarm_label");
		String wherecond = " where id = "+id;
		try {
			dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.removeAssetAlarm:" + e);
		}
	}
	/**
	 * 添加asset关联的alarm
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addAssetAlarm(DBRow row) throws Exception{
		String tablename = ConfigBean.getStringValue("t_asset_alarm_label");
		try {
			return dbUtilAutoTran.insertReturnId(tablename, row);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.addAssetAlarm:" + e);
		}
	}
	/**
	 * 查询所有设备分组
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllGroup() throws Exception {
		try{
			String sql = "select * from t_group";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.findAllGroup:" + e);
		}
	}
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	/**
	 * 查询webcam
	 * @param ip
	 * @param port
	 * @return
	 * @throws Exception 
	 */
	public DBRow getWebcamByIp(String ip, String port) throws Exception{
		try{
			String sql = "select * from webcam w where w.ip = '"+ip+"' and w.port = '"+port+"'";
			return dbUtilAutoTran.selectSingle(sql);
		}catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc getWebcamByIp error:" +e);
		}
	}
	
	/**
	 * 添加webcam
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addWebcam(DBRow row) throws Exception{
		try{
			return dbUtilAutoTran.insertReturnId("webcam", row);
		}catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc addWebcam error:" +e);
		}
	}

	/**
	 * 更新webcam
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public int updateWebcamInfo(DBRow row)throws Exception{
		String wherecond ="where id="+row.getString("id");
		return dbUtilAutoTran.update(wherecond, ConfigBean.getStringValue("webcam"), row);
	};
	/**
	 * 更新webcambyid
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public int updateWebcamByid(DBRow row)throws Exception{
		String wherecond ="where id="+row.getString("id");
		return dbUtilAutoTran.update(wherecond, ConfigBean.getStringValue("webcam"), row);
	};
	
	/**
	 * 
	 * @param ps_id
	 * @return DBRow
	 * @throws Exception
	 */
	public DBRow queryCamInfo(long ps_id)throws Exception{
		String sql ="select c.ip,c.user,c.password from webcam c where c.ps_id="+ps_id;
		DBRow ret =new DBRow();
		DBRow[] rows= dbUtilAutoTran.selectMutliple(sql);
		if(rows.length>0){
			ret=rows[0];
		}
		return ret;
	};
	/**
	 * 查找title
	 * @param titleName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTitle(String titleName)throws Exception{
		String sql = "select * from "+ConfigBean.getStringValue("title")+" c where c.title_name ='"+ titleName +"'";
		return dbUtilAutoTran.selectMutliple(sql);
	}
	/**
	 * 查找ZONEAA和title关系
	 * @return
	 */
	public DBRow[] getZoneTitle(long areaId,long titleId)throws Exception{
		
		String sql = "select * from "+ConfigBean.getStringValue("check_in_zone_with_title")+" c where c.title_id ="+ titleId +" and c.area_id ="+ areaId;
		return dbUtilAutoTran.selectMutliple(sql);
	}
	/**
	 * 添加ZONEAA和title关系
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addZoneTitle(DBRow row)throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("check_in_zone_with_title"), row);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc addZoneTitle error:" +e);
		}
	}
	/**
	 * 添加title
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addTitle(DBRow row)throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("title"), row);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc addTitle error:" +e);
		}
	}

	/**
	 * 根据名称查询location关联area表，查询lcation对应的area
	 * @param psId
	 * @param slc_position
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLoctionInfoByName(long psId, String slc_position)
			throws Exception {
		try {
			String sql = "select '1' obj_type,s.slc_id obj_id,s.slc_position obj_name,"
					+ " s.slc_psid ps_id,s.x x,s.y y,s.height height ,"
					+ "s.width width, s.angle angle,s.latlng latlng ,s.is_three_dimensional,d.area_id area_id ,d.area_name area_name from  "
					+ ConfigBean.getStringValue("storage_location_catalog")
					+ " s left join " + ConfigBean.getStringValue("storage_location_area")+
					" d on  s.slc_area =d.area_id where   s.slc_position = '" + slc_position + "'"
					+ " and s.slc_psid = " + psId;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorGoogleMapsMgrCc getLoctionInfoByName error:" + e);
		}
	}
	/**
	 * 根据名称和仓库Id查询staging
	 * @param psId
	 * @param positionName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStagingInfoByName(long psId, String positionName)
			throws Exception {
		try {
			String sql = "select '2' obj_type,"
					+ " s.id obj_id ,s.location_name obj_name,"
					+ "s.psc_id ps_id ,s.sd_id sd_id,s.x ,s.y,"
					+ " s.height,s.width,s.angle,s.latlng,d.doorId doorId from  "
					+ ConfigBean.getStringValue("storage_load_unload_location")
					+ " s left join " +ConfigBean.getStringValue("storage_door")+
					" d  on  s.sd_id = d.sd_id "
					+ " where  s.location_name = '" + positionName + "'"
					+ " and s.psc_id = " + psId;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorGoogleMapsMgrCc getStagingInfoByName error:" + e);
		}
	}
	public DBRow[] getStagingInfoByPsId(long psId)
			throws Exception {
		try {
			String sql = " select '2' obj_type,"
					+ " s.id obj_id ,"
					+ "s.location_name obj_name,"
					+ "s.psc_id ps_id ,"
					+ "s.sd_id sd_id,"
					+ "s.x ,"
					+ "s.y,"
					+ " s.height,"
					+ "s.width,"
					+ "s.angle,"
					+ "s.latlng,"
					+ "d.doorId doorId from  "
					+ ConfigBean.getStringValue("storage_load_unload_location")
					+ " s left join " +ConfigBean.getStringValue("storage_door")+
					" d  on  s.sd_id = d.sd_id "
					+ " where   s.psc_id = " + psId;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorGoogleMapsMgrCc getStagingInfoByPsId error:" + e);
		}
	}
	/**
	 * 根据名称和仓库Id查询docks
	 * @param psId
	 * @param positionName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDocksInfoByName(long psId, String positionName)
			throws Exception {
		try {
			String sql = "select '3'obj_type,"
					+ "s.sd_id obj_id,"
					+ "s.doorId obj_name, s.ps_id ,"
					+ "s.area_id,s.x,s.y,s.height,"
					+ "s.width,s.angle, s.latlng, "
					+ "d.area_name area_name from  "
					+ ConfigBean.getStringValue("storage_door")
					+ " s left join " +ConfigBean.getStringValue("storage_location_area")+
					" d on  s.area_id =d.area_id where  s.doorId = '" + positionName + "'"
					+ " and s.ps_id = " + psId;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorGoogleMapsMgrCc getDocksInfoByName error:" + e);
		}
	}
	/**
	 * 根据名称和仓库Id查询parking
	 * @param psId
	 * @param positionName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getParkingInfoByName(long psId, String positionName)
			throws Exception {
		try {
			String sql = "select '4' obj_type,s.yc_id obj_id ,s.yc_no obj_name,s.ps_id ,s.area_id,s.x,s.y,s.height,s.width,s.angle,s.latlng ,d.area_name area_name from  "
					+ ConfigBean.getStringValue("storage_yard_control")
					+ " s left join " +ConfigBean.getStringValue("storage_location_area")+
					" d on  s.area_id =d.area_id where  s.yc_no = '" + positionName + "'"
					+ " and s.ps_id = " + psId;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorGoogleMapsMgrCc getParkingInfoByName error:" + e);
		}
	}
	/**
	 * 根据名称查询area坐标
	 * @param psId
	 * @param areaName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaPositionByName(long psId, String areaName) throws Exception {
		try {
			String sql = "select '5' obj_type,s.area_id obj_id,s.area_name obj_name,s.area_psid ps_id ,s.x,s.y,s.height,s.width,s.angle,s.latlng ,s.area_type type,s.area_subtype from  "
		            +ConfigBean.getStringValue("storage_location_area")+
					" s where  s.area_name = '"+areaName+"'" +
					" and s.area_psid = "+psId;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc getAreaPositionByName error:" +e);
		}
	}
	/**
	 * 更新storage_location_area表
	 * @param areaId
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public int updateStorageArea(long areaId, DBRow row) throws Exception{
		try {
			return dbUtilAutoTran.update(" where area_id="+areaId, ConfigBean.getStringValue("storage_location_area"), row);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc updateStorageArea error:" +e);
		}
	}
	/**
	 * 
	 * @param areaId
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public void updateStorageAreaType(long areaId,int type) throws Exception{
		try {
			String sql = "update "+ConfigBean.getStringValue("storage_location_area")+" set area_type="+type+" where area_id="+areaId;
			dbUtilAutoTran.executeSQL(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc updateStorageAreaType error:" +e);
		}
	}
	
	/**
	 * 更新storage_location_catalog表
	 * 
	 * @param locationId
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public int updateStorageLocation(long locationId, DBRow row)
			throws Exception {
		try {
			return dbUtilAutoTran.update(" where slc_id=" + locationId,
					ConfigBean.getStringValue("storage_location_catalog"), row);
		} catch (Exception e) {
			throw new Exception(
					"FloorGoogleMapsMgrCc updateStorageLocation error:" + e);
		}
	}
	/**
	 * 更新storage_load_unload_location表
	 * 
	 * @param stagingId
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public int updateStorageStaging(long stagingId, DBRow row) throws Exception {
		try {
			return dbUtilAutoTran.update(" where id=" + stagingId,
					ConfigBean.getStringValue("storage_load_unload_location"),
					row);
		} catch (Exception e) {
			throw new Exception(
					"FloorGoogleMapsMgrCc updateStorageStaging error:" + e);
		}
	}
	/**
	 * 更新storage_door表
	 * 
	 * @param dockId
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public int updateStorageDocks(long dockId, DBRow row) throws Exception {
		try {
			return dbUtilAutoTran.update(" where sd_id=" + dockId,
					ConfigBean.getStringValue("storage_door"), row);
		} catch (Exception e) {
			throw new Exception(
					"FloorGoogleMapsMgrCc updateStorageDocks error:" + e);
		}
	}
	/**
	 * update table storage_yard_control
	 * 
	 * @param parkingId
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public int updateStorageParking(long parkingId, DBRow row) throws Exception {
		try {
			return dbUtilAutoTran.update(" where yc_id=" + parkingId,
					ConfigBean.getStringValue("storage_yard_control"), row);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc updateStorageArea error:"
					+ e);
		}
	}


	/**
	 * 查询仓库内area和title的关系
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaTitleByPsId(long ps_id) throws Exception{
		try {
			String sql = " SELECT s.area_id,s.area_name,s.area_type,t.title_id obj_id,t.title_name obj_name from storage_location_area s " +
					" LEFT JOIN check_in_zone_with_title c on s.area_id = c.area_id" +
					" LEFT JOIN title t on t.title_id = c.title_id" +
					" WHERE s.area_psid = "+ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getAreaTitleByPsId error:" +e);
		}
	}
	/**
	 * 查询仓库内area和title的关系
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTitleByPsIdAndAreaname(long  psId ,String area_name) throws Exception{
		try {
			String sql = " SELECT s.area_id,s.area_name,s.area_type,t.title_id,t.title_name from storage_location_area s " +
					" LEFT JOIN check_in_zone_with_title c on s.area_id = c.area_id" +
					" LEFT JOIN title t on t.title_id = c.title_id" +
					" WHERE s.area_psid = "+psId+ " and s.area_name ='"+area_name+"'";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getTitleByPsIdAndAreaId error:" +e);
		}
	}
	/**
	 * 查询仓库内area和door的关系
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaDoorByPsId(String psId) throws Exception{
		try {
			String sql = "SELECT sd.*, a.area_name, a.area_type  from storage_door sd " +
					"left	join storage_area_door sad ON sad.sd_id = sd.sd_id" +
					"left	join storage_location_area a on a.area_id = sad.area_id" +
					"	where a.area_psid ="+psId;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getAreaDoorByPsId error:" +e);
		}
	}
	public DBRow[] getAreaDoorCountsByPsId(long psId) throws Exception{
		
		try {
			String sql ="select a.*,count(b.sd_id) counts  from " +
					"storage_location_area  a " +
					"LEFT JOIN storage_area_door b on a.area_id=b.area_id " +
					"LEFT JOIN storage_door c ON c.sd_id =b.sd_id " +
					"where  a.area_psid="+psId+" group by a.area_id ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc getAreaDoorCountsByPsId error:" +e);
		}
	}
	public DBRow[] getAreaDoorByPsIdandArea(long psId,long area_id) throws Exception{
		
		try {
			String sql ="select a.* from storage_door a," +
					" storage_area_door b," +
					" storage_location_area c" +
					"  where a.sd_id =b.sd_id " +
					" and c.area_id=b.area_id " +
					" and c.area_id="+area_id  +
					"  and c.area_psid="+psId +
					" order by a.doorId ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc getAreaDoorByPsIdandArea error:" +e);
		}
	}
	public DBRow[] getAreaPersonByPsIdandArea(long psId,long area_id) throws Exception{
		
		try {
			String sql ="SELECT a.* FROM admin a, admin_warehouse c WHERE"
					+ "  a.adid = c.adid AND c.area_id ="+area_id
					+ "  AND c.warehouse_id ="+psId
					+ " ORDER BY a.adid ";

			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc getAreaDoorByPsIdandArea error:" +e);
		}
	}
	public DBRow[] getPersonforAreaByPsId(long psId) throws Exception{
		
		try {
			String sql ="select a.*, count(b.adid) counts " +
					" from storage_location_area a " +
					" LEFT JOIN admin_warehouse b " +
					" ON a.area_id=b.area_id " +
					" and a.area_psid=b.warehouse_id " +
					" where a.area_psid="+psId+
					" GROUP BY a.area_id";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc getPersonforAreaByPsId error:" +e);
		}
	}
	public DBRow[] getPersonByPsId(long psId) throws Exception{
		try { 
			String sql ="select distinct  a.* " +
					"from admin a join admin_warehouse b "
					+ " on a.adid=b.adid  " +
					"where b.warehouse_id="+psId ;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc getPersonByPsId error:" +e);
		}
	}
	/**
	 * 
	 * @param ps_id
	 * @param id
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public int deletePersonAreaByAdid(long ps_id ,long area_id,long adid) throws Exception{
		try {
			String wherecond="where warehouse_id="+ps_id+" and area_id="+area_id+" and adid="+adid;
			String tablename="admin_warehouse";
			return dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc deletePersonAreaByAdid error:" +e);
		}
	}
	/**
	 * 
	 * @param ps_id
	 * @param area_id
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public long insertPersonAreaByAdid(long ps_id,long area_id,long adid) throws Exception{
		try {
			DBRow data=new DBRow();
			data.add("warehouse_id",ps_id);
			data.add("area_id",area_id);
			data.add("adid",adid);
			String tablename="admin_warehouse";
			return dbUtilAutoTran.insertReturnId(tablename, data);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc insertPersonAreaByAdid error:" +e);
		}
	}
	/**
	 * 通过id查询摄像头信息
	 * @param positionId
	 * @return
	 * @throws Exception 
	 */
	public DBRow getWebcamPositionByIp(String positionId,int type) throws Exception {
		try {
			String sql = "select c.* from "
					+ ConfigBean.getStringValue("webcam_position")
					+ "  c where position_id =" + positionId +" and position_type="+type;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.getWebcamPositionByIp error:" + e);
		}
	}
	
	/**
	 * 通过area_id删除某一块area  
	 * @param area_id
	 * @throws Exception
	 */
	public int deleteAreaById(long ps_id ,long area_id) throws Exception{
		
		try {
			String wherecond ="where area_id="+area_id+" and area_psid="+ps_id;
			String tablename=ConfigBean.getStringValue("storage_location_area");
			return dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteAreaById():" + e);
		}
	}
	public int deleteAreaById(long ps_id ,long[] area_ids) throws Exception{
		
		try {
			String wherecond ="where area_id in "+LongArraytoString(area_ids)+" and area_psid="+ps_id;
			String tablename=ConfigBean.getStringValue("storage_location_area");
			return dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteAreaById():" + e);
		}
	}
	
	/**
	 * 通过slc_id删除某一块location
	 * @param area_id
	 * @throws Exception
	 */
	public int deleteLocationById(long ps_id ,long slc_id) throws Exception{
		
		try {
			String wherecond ="where slc_id ="+slc_id+" and slc_psid="+ps_id;
			String tablename=ConfigBean.getStringValue("storage_location_catalog");
			return dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteLocationById():" + e);
		}
	}
	
	public int deleteLocationById(long ps_id ,long[] slc_id) throws Exception{
		try {
			String wherecond ="where slc_id in "+LongArraytoString(slc_id)+" and slc_psid ="+ps_id;
			String tablename=ConfigBean.getStringValue("storage_location_catalog");
			return dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteLocationById():" + e);
		}
	}	

	/**
	 * 通过id删除某一块staging
	 * @param id
	 * @throws Exception
	 */
	public int deleteStagingById(long ps_id,long id) throws Exception{
		
		try {
			//String sql="delete from "+ConfigBean.getStringValue("storage_load_unload_location")+" where id ="+id;
			String wherecond ="where id ="+id+" and psc_id="+ps_id;
			String tablename=ConfigBean.getStringValue("storage_load_unload_location");
			return dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteStagingById():" + e);
		}
	}
	public int deleteStagingById(long ps_id,long[] ids) throws Exception{
		
		try {
			//String sql="delete from "+ConfigBean.getStringValue("storage_load_unload_location")+" where id ="+id;
			String wherecond ="where id in "+LongArraytoString(ids)+" and psc_id="+ps_id;
			String tablename=ConfigBean.getStringValue("storage_load_unload_location");
			return dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteStagingById():" + e);
		}
	}
	/**
	 * 通过yc_id 删除某一个parking
	 * @param yc_id
	 * @throws Exception
	 */
	public int deletePrakingById(long ps_id,long yc_id) throws Exception{
		try {
			String wherecond ="where yc_id ="+yc_id+" and ps_id="+ps_id;
			String tablename=ConfigBean.getStringValue("storage_yard_control");
			return dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deletePrakingById():" + e);
		}
	}
	public int deletePrakingById(long ps_id,long[] yc_ids) throws Exception{
		try {
			String wherecond ="where yc_id in "+LongArraytoString(yc_ids)+" and ps_id="+ps_id;
			String tablename=ConfigBean.getStringValue("storage_yard_control");
			return dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deletePrakingById():" + e);
		}
	}
	/**
	 * 通过sd_id 删除某一个docks
	 * @param yc_id
	 * @throws Exception
	 */
	public int deleteDocksById(long ps_id,long sd_id) throws Exception{
		
		try {
			String wherecond ="where sd_id ="+sd_id+" and ps_id="+ps_id;
			String tablename=ConfigBean.getStringValue("storage_door");
			return dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deletePrakingById():" + e);
		}
	}
	public int deleteDocksById(long ps_id,long[] sd_ids) throws Exception{
		
		try {
			String wherecond ="where sd_id in "+LongArraytoString(sd_ids)+" and ps_id="+ps_id;
			String tablename=ConfigBean.getStringValue("storage_door");
			return dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deletePrakingById():" + e);
		}
	}
	/**
	 * 通过id 删除某一个webcam
	 * @param id
	 * @throws Exception
	 */
	public int deleteWebcamById(long ps_id,long id) throws Exception{
		
		try {
			String wherecond ="where id ="+id+" and ps_id="+ps_id;
			String tablename=ConfigBean.getStringValue("webcam");
		return 	dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteWebcamById():" + e);
		}
	}
	public int deleteWebcamById(long ps_id,long[] ids) throws Exception{
		
		try {
			String wherecond ="where id in "+LongArraytoString(ids)+" and ps_id="+ps_id;
			String tablename=ConfigBean.getStringValue("webcam");
			return 	dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteWebcamById():" + e);
		}
	}
	/**
	 * 通过id 删除某一个webcam
	 * @param id
	 * @throws Exception
	 */
	public int deletePrinterById(long ps_id,long id) throws Exception{
		
		try {
			String wherecond ="where p_id ="+id+" and ps_id="+ps_id;
			String tablename=ConfigBean.getStringValue("printer");
			return 	dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deletePrinterById():" + e);
		}
	}
	/**
	 * 根据 area_id 删除title关系表
	 * @param area_id
	 * @throws Exception
	 */
	public void deleteCheckTitleByAreaId(long area_id)throws Exception{
		
		try {
			String sql ="delete from "+ConfigBean.getStringValue("check_in_zone_with_title") 
					+" where  area_id ="+ area_id;
			dbUtilAutoTran.executeSQL(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteCheckTitleByAreaId():" + e);
		}
		
	}
	public void deleteCheckTitleByAreaId(long[] area_ids)throws Exception{
		
		try {
			String sql ="delete from "+ConfigBean.getStringValue("check_in_zone_with_title") 
					+" where  area_id in "+ LongArraytoString(area_ids);
			dbUtilAutoTran.executeSQL(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteCheckTitleByAreaId():" + e);
		}
		
	}
	/**
	 * 根据 area_id 删除door关系表
	 * @param area_id
	 * @throws Exception
	 */
	public void deleteAreaDoorByAreaId(long area_id)throws Exception{
		
		try {
			String sql ="delete from "+ConfigBean.getStringValue("storage_area_door") 
					+" where  area_id ="+ area_id;
			dbUtilAutoTran.executeSQL(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteCheckTitleByAreaId():" + e);
		}
	}
	public void deleteAreaDoorByAreaId(long[] area_ids)throws Exception{
		
		try {
			String sql ="delete from "+ConfigBean.getStringValue("storage_area_door") 
					+" where  area_id in"+LongArraytoString(area_ids) ;
			dbUtilAutoTran.executeSQL(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteCheckTitleByAreaId():" + e);
		}
	}
	/**
	 * 更新door状态
	 * @param sd_id
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public int setStorageDoorStatus(long sd_id, int status) throws Exception {
		try{
			String wherecond = " where sd_id="+sd_id;
			DBRow data = new DBRow();
			data.add("sd_status", status);
			return dbUtilAutoTran.update(wherecond, ConfigBean.getStringValue("storage_door"), data);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.setStorageDoorStatus():" + e);
		}
	}
	/**
	 * 添加打印机数据
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addPrinter(DBRow row )throws Exception{
		
		try {
			String tablename=ConfigBean.getStringValue("printer");
			return dbUtilAutoTran.insertReturnId(tablename, row);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.addPrinter():" + e);
		}
		
	}
	public long addPrinterArea(DBRow row )throws Exception{
		
		try {
			String tablename=ConfigBean.getStringValue("storage_zone_printerservice");
			return dbUtilAutoTran.insertReturnId(tablename, row);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.addPrinterArea():" + e);
		}
		
	}
	public long deletePrinterArea(long service_id )throws Exception{
		String wherecond=" where service_id="+service_id;
		try {
			String tablename=ConfigBean.getStringValue("storage_zone_printerservice");
			return dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.addPrinterArea():" + e);
		}
		
	}
	/**
	 * 修改打印机数据
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public int updatePrinter(DBRow row )throws Exception{
		
		  long id =Long.parseLong(row.getString("p_id"));
		  long psId =Long.parseLong(row.getString("ps_id"));
		  String wherecond =" where p_id="+id+" and ps_id ="+psId;
		  String tablename=ConfigBean.getStringValue("printer");
		try {
			return   dbUtilAutoTran.update(wherecond, tablename, row);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.updatePrinter():" + e);
		}
	}
	/**
	 * 删除仓库WarehouseBase
	 * @param psId
	 * @throws Exception
	 */
	public void deleteStorageWarehouseBaseByPsid(long psId)throws Exception{
		try {
			String sql = "delete from " + ConfigBean.getStringValue("storage_warehouse_base") + " where ps_id=" + psId;
			dbUtilAutoTran.executeSQL(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteStorageWarehouseBaseByPsid():" + e);
		}
	}
	/**
	 * 添加仓库WarehouseBase
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addStorageWarehouseBase(DBRow row)throws Exception{
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("storage_warehouse_base"), row);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.addStorageWarehouseBase():" + e);
		}
	}
	/**
	 * 查询storageRoad
	 * @param rid
	 * @return
	 * @throws Exception
	 */
	public DBRow getStorageRoadByRid(long rid) throws Exception{
		String sql = "select s.r_id,s.name, s.source, s.target,s.length, asText(s.geom) geom from storage_road s where s.r_id="+rid;
		try {
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getStorageRoadByRid:" + e);
		}
	}
	/**
	 * 查询storageRoad
	 * @param rid
	 * @return 
	 * @return
	 * @throws Exception
	 */
	public int deleteRoadByRid(long ps_id,long rid) throws Exception{
		try {
			String wherecond =" where ps_id ="+ps_id +" and  r_id="+rid;
			String tablename="storage_road";
			return dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getStorageRoadByRid:" + e);
		}
	}
	/**
	 * 查询StorageRoad
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageRoadByPsid(long psId)throws Exception{
		try {
			String sql = "SELECT s.r_id,s.name, s.source, s.target,s.length, asText(s.geom) geom from storage_road s where s.ps_id="+psId;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getStorageRoadByPsid:" + e);
		}
	}
	/**
	 * 查询location entery road
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocationEnteryRoadByPsid(long psId)throws Exception{
		try {
			String sql = "SELECT s.slc_id, s.slc_position, s.entery_road, s.entery_point, s.road_point from storage_location_catalog s where s.slc_psid="+psId;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getLocationEnteryRoadByPsid:" + e);
		}
	}
	/**
	 * 根据printServer 去查询Print
	 * @param printServerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPrintByPrintServer(long printServerId)throws Exception{
		try {
			String sql = "select * from  " + ConfigBean.getStringValue("printer") + " where servers=" +printServerId; 
 			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getPrintByPrintServer:" + e);
		}
	}
	/**
	 * 计算最短路径
	 * @param psId
	 * @param from 起点单个点 pid
	 * @param to  终点,单个或多个点  pid1,pid2,pid3...
	 * @return  pid1,pid2,pid3..[ pid4,pid5..][ pid6,pid7..]多结果用" "隔开
	 */
	public DBRow getDijkstraByPointId(long psId, String from, String to) throws Exception{
		String sql = "select r_dijkstra("+psId+","+from+",'"+to+"') path";
		try {
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getDijkstraByPointId:" + e);
		}
	}
	/**
	 * 查询路线对应的点
	 * @param psId
	 * @param pointIds
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRoutePathPoints(long psId, String pointIds) throws Exception{
		String sql = "select point_id, CONCAT(x(geom),' ',y(geom)) latlng from storage_road_point where point_id in ("+pointIds+") and ps_id="+psId;
		try {
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getRoutePathPoints:" + e);
		}
	}
	/**
	 * 查询距离最近的路
	 * @param psId
	 * @param latlngStr
	 * @return
	 * @throws Exception
	 */
	public DBRow getNearestRoad(long psId, String latlngStr) throws Exception{
		String sql = "SELECT r_nearest_road("+psId+",'"+latlngStr+"',0,null) road";
		try {
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getNearestRoad:" + e);
		}
	}
	public DBRow[] getPrinterServerByAreaIdandPtype(long area_id ) throws Exception{
		String sql="select a.* from "+ConfigBean.getStringValue("android_printer_server")+" a  , "
				+ConfigBean.getStringValue("storage_zone_printerservice")
				+ " b where a.printer_server_id=b.service_id and b.area_id="+area_id;
		try {
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.getPrinterByAreaIdandPtype:" + e);
		}
	}
	public DBRow[] getStorageCoordinateSys(long psId)throws Exception{
		String sql="select *,r_latlng_to_coord(s.ps_id, s.lng, s.lat) coor from storage_warehouse_base s where s.type='warehouse' and s.ps_id="+psId+" ORDER BY s.param";
		try {
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.getStorageCoordinateSys:" + e);
		}
	}
	
	public DBRow[] getLayerInfoBypsIdAndName(long psId ,String name,int type )throws Exception{
		DBRow[] row=null;
		String sql ="";
		switch (type) {
		case WebcamPositionTypeKey.AREA:
			sql ="select c.* from "+ConfigBean.getStringValue("storage_location_area") 
			+" c where c.area_psid="+psId +" and c.area_name='"+name+"'";
			row= dbUtilAutoTran.selectMutliple(sql);
			break;
		case WebcamPositionTypeKey.LOCATION:
			sql ="select c.* from "+ConfigBean.getStringValue("storage_location_catalog") 
			+" c where c.slc_psid="+psId +" and c.slc_position='"+name+"'";
			row= dbUtilAutoTran.selectMutliple(sql);
			break;
		case WebcamPositionTypeKey.STAGING:
			sql ="select c.* from "+ConfigBean.getStringValue("storage_load_unload_location") 
			+" c where c.psc_id="+psId +" and c.location_name='"+name+"'";
			row= dbUtilAutoTran.selectMutliple(sql);
			break;
		case WebcamPositionTypeKey.PARKING:
			sql ="select c.* from "+ConfigBean.getStringValue("storage_yard_control") 
			+" c where c.ps_id="+psId +" and c.yc_no='"+name+"'";
			row= dbUtilAutoTran.selectMutliple(sql);
			break;
		case WebcamPositionTypeKey.DOCKS:
			sql ="select c.* from "+ConfigBean.getStringValue("storage_door") 
			+" c where c.ps_id="+psId +" and c.doorId='"+name+"'";
			row= dbUtilAutoTran.selectMutliple(sql);
			break;
		case WebcamPositionTypeKey.PRINTER:
			sql ="select c.* from "+ConfigBean.getStringValue("printer") 
			+" c where c.ps_id="+psId +" and c.name='"+name+"'";
			row= dbUtilAutoTran.selectMutliple(sql);
			break;
		case WebcamPositionTypeKey.LIGHT:
			sql = "SELECT s.* from "+ConfigBean.getStringValue("storage_light")+" s where s.ps_id="+psId+" and s.name='"+name+"'";
			row =dbUtilAutoTran.selectMutliple(sql);
			break;
	}
		return row;
		
	}
	/**
	 * @throws Exception 
	 * 查询打印机名字是否存在
	 * @param row
	 * @return count
	 * @throws
	 */
	public DBRow checkPrinterNameIsExist(String name,long psId) throws Exception{
		//String tablename=ConfigBean.getStringValue("printer");
		String sql ="select count(*) from printer p where p.name='"+name+"' and p.ps_id="+psId;
		try {
			return dbUtilAutoTran.selectSingle(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.getStorageCoordinateSys:" + e);
		}
		
	}
	
	/**
	 * 查询StorageRoadPoint
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageRoadPointByPsid(long psId)throws Exception{
		try {
			String sql = "SELECT s.rp_id,s.point_id,astext(s.geom) geom from "+ConfigBean.getStringValue("storage_road_point")+" s where s.ps_id="+psId;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getStorageRoadPointByPsid:" + e);
		}
	}
	
	/**
	 * 查询StorageLight
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLightByPsid(long psId)throws Exception{
		try {
			String sql = "SELECT s.* from "+ConfigBean.getStringValue("storage_light")+" s where s.ps_id="+psId;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getStorageLightByPsid:" + e);
		}
	}
	/**
	 * 查询StorageLight
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow queryLightSingleByIdAndName(long psId,String name)throws Exception{
		try {
			String sql = "SELECT s.* from "+ConfigBean.getStringValue("storage_light")+" s where s.ps_id="+psId+" and s.name='"+name+"'";
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getStorageLightByPsid:" + e);
		}
	}
	/**
	 * 添加light数据
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addLight(DBRow row )throws Exception{
		
		try {
			String tablename=ConfigBean.getStringValue("storage_light");
			return dbUtilAutoTran.insertReturnId(tablename, row);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.addLight():" + e);
		}
		
	}
	/**
	 * 修改light数据
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public int updateLight(DBRow row )throws Exception{
		  long id =Long.parseLong(row.getString("id"));
		  long psId =Long.parseLong(row.getString("ps_id"));
		  String wherecond =" where id="+id+" and ps_id="+psId;
		  String tablename=ConfigBean.getStringValue("storage_light");
		try {
			return   dbUtilAutoTran.update(wherecond, tablename, row);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.updateLight():" + e);
		}
	}
	/**
	 * 修改light数据
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public int deleteLightById(long psId,long id )throws Exception{
		String wherecond =" where id="+id+" and ps_id="+psId;
		String tablename=ConfigBean.getStringValue("storage_light");
		try {
			return   dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteLightById():" + e);
		}
	}
	/**
	 * 查询container 信息
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow[]  selectContainer(int status)throws Exception{
	String sql ="select c.* from import_container_info c   where c.status= "+status;
		try {
			return   dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.selectContainer():" + e);
		}
	}
	/**
	 * 查询containerPlate 信息
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow[]  selectContainerPlates(long ic_id,long stagingId)throws Exception{
		String sql="";
		if(ic_id!=-1){
			
		sql ="SELECT "
				+ " c.*, d.psc_id ps_id, "
				+ " d.location_name NAME, e.container_no CTNR "
				//+ " g.gate_driver_name secondname, g.gate_check_in_time "
				//+ " check_int_time, g.gate_liscense_plate liscense_plate "
				+ " FROM storage_load_unload_location d, "
				+ " import_container_info e,"
				+ " import_container_plates c "
				//+ " LEFT JOIN door_or_location_occupancy_details f ON c.ic_id=f.ic_id "
				//+ " LEFT JOIN door_or_location_occupancy_main g ON f.dlo_id=g.dlo_id "
				+ " WHERE "
				+ " c.ic_id = "+ic_id+""
				+ " AND c.staging_area = d.id "
				+ " AND c.ic_id = e.ic_id "
				+ " AND c.staging_area = "+stagingId;
		}else{
			sql ="SELECT "
					+ " c.*, d.psc_id ps_id, "
					+ " d.location_name NAME, e.container_no CTNR "
				//	+ " g.gate_driver_name secondname, g.gate_check_in_time "
				//	+ " check_int_time, g.gate_liscense_plate liscense_plate "
					+ " FROM storage_load_unload_location d, "
					+ " import_container_info e,"
					+ " import_container_plates c "
					//+ " LEFT JOIN door_or_location_occupancy_details f ON c.ic_id=f.ic_id "
					//+ " LEFT JOIN door_or_location_occupancy_main g ON f.dlo_id=g.dlo_id "
					+ " WHERE "
					+ " c.staging_area = d.id "
					+ " AND c.ic_id = e.ic_id "
					+ " AND c.staging_area = "+stagingId;		}
		try {
			return   dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.selectContainerPlates():" + e);
		}
	}
/**
 * 查询CTNR
 * @param ic_id
 * @param stagingId
 * @return
 * @throws Exception
 */
	public DBRow[]  distinctContainerPlates(long ic_id,long stagingId)throws Exception{
		String sql="";
		if(ic_id!=-1){
			sql ="select DISTINCT c.ic_id ic_id,e.container_no CTNR "
					+ " from import_container_plates c ,import_container_info e "
					+ " where c.ic_id= "+ic_id+" and  c.ic_id=e.ic_id  and c.staging_area="+stagingId;
		}else{
			sql =" SELECT DISTINCT c.ic_id ic_id,e.container_no CTNR "
					+ " FROM import_container_plates c, import_container_info e "
					+ " WHERE c.ic_id=e.ic_id AND c.staging_area ="+stagingId;
		}
		try {
			return   dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.distinctContainerPlates():" + e);
		}
	}
/**
 * 查询containerPlate 信息
 * @param row
 * @return
 * @throws Exception
 */
public DBRow[]  queryContainerCounts(long ic_id,long ps_id)throws Exception{
		String sql="";
		if(ic_id!=-1){
			sql ="select DISTINCT c.ic_id ic_id ,d.psc_id ps_id, d.id staging_id,d.location_name name from import_container_plates c ,storage_load_unload_location d  where c.ic_id= "+ic_id+" and c.staging_area =d.id and d.psc_id ="+ps_id;
		}else{
			sql ="select DISTINCT c.ic_id ic_id ,d.psc_id ps_id, d.id staging_id,d.location_name name from import_container_plates c ,storage_load_unload_location d  where c.staging_area =d.id and d.psc_id ="+ps_id;
		}
		try {
			return   dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.queryContainerCounts():" + e);
		}
	}
/**
 * 通过ps_id查询door
 * @param ps_id
 * @return
 * @throws Exception
 */
public DBRow[] getDoorByPsId(long ps_id) throws Exception{
	try {
		String sql = "select  c.* from "+ 
				ConfigBean.getStringValue("storage_door") +
				" c where c.ps_id ="+ ps_id;
		return dbUtilAutoTran.selectMutliple(sql);
	} catch (Exception e) {
		throw new Exception("FloorGoogleMapsMgrCc.getDoorByPsId():" + e);
	}
}
/**
 * 差选location的信息包括对应的area_name
 * @param ps_id
 * @return
 * @throws Exception
 */
public DBRow[] getLocationByPsId(long ps_id) throws Exception{
		try {
			String sql = "select  c.slc_id obj_id ,"
					+ "c.slc_psid ps_id , "
					+ "c.slc_ps_title title, "
					+ "c.slc_area area_id, "
					+ "c.slc_type type, "
					+ "c.slc_position obj_name,"
					+ "c.slc_position_all name_full, "
					+ "c.slc_x slc_x, "
					+ "c.slc_y slc_x, "
					+ "c.is_three_dimensional is_three_dimensional , "
					+ "c.x, "
					+ "c.y, "
					+ "c.width, "
					+ "c.height, "
					+ "c.latlng, "
					+ "d.area_name area_name from "+ 
					ConfigBean.getStringValue("storage_location_catalog") +
					" c , "+
					ConfigBean.getStringValue("storage_location_area")+
					" d "
					+ " where c.slc_area=d.area_id  and c.slc_psid ="+ ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getLocationByPsId():" + e);
		}
}
public DBRow[] getAreaByPsId(long ps_id) throws Exception{
	try {
		String sql = "select c.area_id obj_id ,"
				+ "c.area_name obj_name,"
				+ "c.area_psid ps_id ,"
				+ "c.area_img,"
				+ "c.area_type type,"
				+ "c.x,"
				+ "c.y,"
				+ "c.width,"
				+ "c.height,"
				+ "c.latlng,"
				+ "c.area_subtype from "+
				ConfigBean.getStringValue("storage_location_area")
				+" c "
				+ " where c.area_psid ="+ ps_id;
		return dbUtilAutoTran.selectMutliple(sql);
	} catch (Exception e) {
		throw new Exception("FloorGoogleMapsMgrCc.getAreaByPsId():" + e);
	}
}
public DBRow[] getStorageBaseData(long ps_id)throws Exception{
	try {
		String sql ="select c.swb_id id ,"
				+ "c.ps_id ps_id ,"
				+ "c.type type,"
				+ "c.param param,"
				+ "c.lng lng,"
				+ "c.lat lat from "
				+ConfigBean.getStringValue("storage_warehouse_base") +
				" c where  c.ps_id="+ps_id +" order by c.type ,c.param";
		return dbUtilAutoTran.selectMutliple(sql);
	} catch (Exception e) {
		throw new Exception("FloorGoogleMapsMgrCc.getStorageBaseDataByPsId():" + e);	
	}
}


public DBRow[] getAllTitle()throws Exception{
	
		try {
			String sql=" select c.title_id obj_id ,c.title_name obj_name  from title c ";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.getAllTitle():" + e);	
		}
		
	}
//根据用户id查询 该用户下的所有title
	public DBRow[] getProprietaryByadminId(long adminId)throws Exception{
		try{
			String sql="select * from title_admin ta join title t on ta.title_admin_title_id=t.title_id where ta.title_admin_adid="+adminId+" order by t.title_name";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorGoogleMapsMgrCc.getProprietaryByadminId() error:"+e);
		}
	}
	
	public DBRow[] getStorageTimeOutBypsId(long ps_id)throws Exception{
		
		try {
			String sql =" select s.* from set_storage_timeout s  where s.ps_id ="+ps_id; 
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.getStorageTimeOutBypsId() error:"+e);
		}
		
	}
	public long  addStorageTimeOut(DBRow row)throws Exception{
		try {
			String tablename="set_storage_timeout";
			return this.dbUtilAutoTran.insertReturnId(tablename, row);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.addStorageTimeOut() error:"+e);
		}
		
	}
	public long  updateStorageTimeOutByPsId(DBRow row)throws Exception{
		try {
			String wherecond =" where ps_id="+row.get("ps_id", 0l);
			String tablename="set_storage_timeout";
			return this.dbUtilAutoTran.update(wherecond, tablename, row);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.updateStorageTimeOutByPsId() error:"+e);
		}
		
	}
	
	public DBRow[] getStorageWithoutOnMap()throws Exception{
		
		try {
			String sql ="select p.* from product_storage_catalog p where storage_type =1 and id not in (select distinct ps_id from storage_warehouse_base ) ";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.getStorageWithoutOnMap() error:"+e);
		}
	}
	public long  addWerahouseBaseData(DBRow data)throws Exception{
		try {
			String tablename="storage_warehouse_base";
			return dbUtilAutoTran.insertReturnId(tablename, data);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.addWerahouseBaseData() error:"+e);
		}
		
	}
	
	public DBRow[] getDocksData(long ps_id)throws Exception{
		
		try {
			String sql ="select 'docks'obj_type, "
					+ " s.sd_id obj_id, "
					+ " s.doorId obj_name, s.ps_id , "
					+ " s.area_id,s.x,s.y,s.height, "
					+ " s.width,s.angle, s.latlng, "
					+ " d.area_name area_name , "
					+ " s.sd_status available_status from  "
					+ ConfigBean.getStringValue("storage_door")
					+ " s left join " +ConfigBean.getStringValue("storage_location_area")+
					" d on  s.area_id =d.area_id where "
					+ "  s.ps_id = " + ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.getDocksData() error:"+e);
		}
	}
	public DBRow[] getParkingData(long ps_id)throws Exception{
		
		try {
			String sql ="select 'parking' obj_type, yc.yc_id obj_id,"
					+ " yc.yc_no obj_name,"
					+ " yc.area_id , "
					+ " yc.ps_id, "
					+ " yc.x, "
					+ " yc.y, "
					+ " yc.width, "
					+ " yc.height, "
					+ " yc.angle, "
					+ " yc.latlng,"
					+ "	yc.yc_status available_status from  "
					+ ConfigBean.getStringValue("storage_yard_control")
					+ " yc left join " +ConfigBean.getStringValue("storage_location_area")+
					" d on  yc.area_id =d.area_id where "
					+ "  yc.ps_id = " + ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorGoogleMapsMgrCc.getParkingData() error:"+e);
		}
	}
	
	/**
	 * 添加Storage point
	 * @param psId
	 * @param latlngStr
	 * @return
	 * @throws Exception
	 */
	public long addStorageRoadPoint(DBRow point)throws Exception{
		try {
			String geomStr = point.getString("geom");
			point.remove("geom");
			long id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("storage_road_point"), point);
			String sql = "update " + ConfigBean.getStringValue("storage_road_point") + " p set p.geom=GeomFromText('POINT("+geomStr+")') where p.rp_id="+id;
			dbUtilAutoTran.executeSQL(sql);
			return id;
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.addStorageRoadPoint():" + e);
		}
	}
	/**
	 * 添加Road
	 * @param psId
	 * @param latlngStr
	 * @param source
	 * @param target
	 * @return
	 * @throws Exception
	 */
	public long addStorageRoad(DBRow road) throws Exception{
		try {
			String geomStr = road.getString("geom");
			road.remove("geom");
			long id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("storage_road"), road);
			String sql = "update " + ConfigBean.getStringValue("storage_road") + " p set p.geom=GeomFromText('MULTILINESTRING(("+geomStr+"))') where p.r_id="+id;
			dbUtilAutoTran.executeSQL(sql);
			return id;
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.addStorageRoad:" + e);
		}
	}
	/**
	 * 删除Road_point
	 * @param id
	 * @throws Exception 
	 */
	public void deleteStorageRoadPointById(long id) throws Exception{
		String sql = "delete from "+ConfigBean.getStringValue("storage_road_point")+ " where rp_id="+id;
		try {
			dbUtilAutoTran.executeSQL(sql);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteStorageRoadPointById:" + e);
		}
	}
	/**
	 * 删除road
	 * @param id
	 * @throws Exception 
	 */
	public void deleteStorageRoadById(long id) throws Exception{
		String sql = "delete from  "+ConfigBean.getStringValue("storage_road")+" where r_id="+id;
		try {
			dbUtilAutoTran.executeSQL(sql);
		} catch (SQLException e) {
			throw new Exception("FloorGoogleMapsMgrCc.deleteStorageRoadById:" + e);
		}
	}
    /**
     * 查询printerServer
     * @param ps_id
     * @return
     * @throws Exception
     */
	public DBRow[] getPrintServerByPsId(long ps_id,PageCtrl pc) throws Exception {
		try {
			DBRow para =new DBRow();
			para.add("ps_id",ps_id);
			String sql = " SELECT c.*, GROUP_CONCAT(DISTINCT e.area_name ORDER BY e.area_name ) area_name  FROM  android_printer_server c  LEFT JOIN  storage_zone_printerservice d  ON d.service_id = c.printer_server_id  LEFT JOIN  storage_location_area  e  ON d.area_id = e.area_id  where c.ps_id=?"
					+ " GROUP BY c.printer_server_id ";
			return this.dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		} catch (Exception e) {
			throw new Exception(
					"FloorGoogleMapsMgrCc.getAllAndroidPrintServerByPsId(ps_id):"
							+ e);
		}
	}
	public String convertCoordinateToLatlng(String psId, String x, String y,
			String xPosition, String yPosition, String angle) throws Exception {
		long _psId = Long.parseLong(psId);
		double _x = Double.parseDouble(x);
		double _y = Double.parseDouble(y);
		double _xp = Double.parseDouble(xPosition);
		double _yp = Double.parseDouble(yPosition);
		double _angle = Double.parseDouble(angle);
		return convertCoordinateToLatlng(_psId,_x,_y,_xp,_yp,_angle);
	}
public String convertCoordinateToLatlng(long psId, double x, double y, double xPosition, double yPosition, double angle) throws Exception {
		
	DBRow[] baseData =getExcelBaseData(psId);
		double maxX = 0;
		double maxY = 0;
		int pCount = 0;
		char[] whChar = new char[26];
		DBRow whRow = new DBRow();
		for(int i=0; i<baseData.length; i++){
			DBRow r = baseData[i];
			String type = r.getString("type").toLowerCase();
			String val = r.getString("param").toLowerCase();
			if("warehouse".equals(type)){
				char p = val.toCharArray()[0];
				whChar[pCount] = p;
				whRow.add(p+"",r.getString("lng")+","+r.getString("lat"));
				pCount++;
			}else if("maxx".equals(type)){
				maxX = Double.parseDouble(val);
			}else if("maxy".equals(type)){
				maxY = Double.parseDouble(val);
			}
		}
		//基础数据有误，无法计算
		if(pCount<3 || maxX<=0 || maxY<=0){
			return "";
		}
		//按字母编号排序
		whChar = Arrays.copyOfRange(whChar, 0, pCount);
		Arrays.sort(whChar);
		String pointA = whRow.getString(whChar[0]+"");
		String pointB = whRow.getString(whChar[1]+"");
		String pointD = whRow.getString(whChar[whChar.length-1]+"");
		
		//计算缩放比例和旋转角度
		double aLng = Double.parseDouble(pointA.split(",")[0]);
		double aLat = Double.parseDouble(pointA.split(",")[1]);
		double bLng = Double.parseDouble(pointB.split(",")[0]);
		double bLat = Double.parseDouble(pointB.split(",")[1]);
		double dLng = Double.parseDouble(pointD.split(",")[0]);
		double dLat = Double.parseDouble(pointD.split(",")[1]);
		
		
		double ratio = Math.hypot(aLat-bLat,aLng-bLng)/maxX;
		double radX = Math.atan((aLat-bLat)/(aLng-bLng));
		double radY = Math.atan((aLat-dLat)/(aLng-dLng));
		if(radY <= 0){
			radY += Math.PI;
		}
		radY -= Math.PI/2;
		double radN = radX - radY;
		double maxY_map = Math.hypot(aLat-dLat,aLng-dLng)/ratio; 
		
		double X = x;
		double Y = y;
		double L = xPosition;
		double W = yPosition;
		
		double[] XS = new double[]{X,X+L,X+L,X};
		double[] YS = new double[]{Y,Y,Y+W,Y+W};
		//位置相对仓库旋转处理
		if(angle != 0){
			for(int j=1; j<4; j++){
				XS[j] -= XS[0];
				YS[j] -= YS[0];
				
				double r = Math.toRadians(angle) + Math.atan(YS[j]/XS[j]);
				double l = Math.hypot(XS[j],YS[j]); 
				XS[j] = l*Math.cos(r) + XS[0];
				YS[j] = l*Math.sin(r) + YS[0];
				if(Double.isNaN(XS[j])){
					XS[j] = XS[0];
				}
				if(Double.isNaN(YS[j])){
					YS[j] = YS[0];
				}
			}
		}
		String cds = "";
		//仓库相对地图旋转、缩放、平移等处理
		for(int j=0; j<4; j++){
			YS[j] /= maxY/maxY_map;  //Y轴拉伸
			
			XS[j] = XS[j] + YS[j]*Math.sin(radN)*Math.cos(radN);
			YS[j] = YS[j]*Math.cos(radN);
			
			int sign = XS[j]>0 ? 1 : -1 ;//由于atan(x)结果在第1,4象限，计算rad时2,3象限的点被旋转180°，即中心对称，因此需作此处理
			double rad = radX + Math.atan(YS[j]/XS[j]);
			double len = Math.hypot(XS[j],YS[j]); 
			double lat = sign*len*ratio*Math.sin(rad) + aLat;
			double lng = sign*len*ratio*Math.cos(rad) + aLng;
			if(Double.isNaN(lat)){
				lat = aLat;
			}
			if(Double.isNaN(lng)){
				lng = aLng;
			}
			cds += lng +","+ lat + ",0.0 ";
		}
		cds += cds.split(" ")[0];
		return cds;
	}
	public String convertCoordinateToLatlng(long psId, double x, double y)throws Exception{
		DBRow[] baseData = getExcelBaseData(psId);
		double maxX = 0;
		double maxY = 0;
		
		int pCount = 0;
		char[] whChar = new char[26];
		DBRow whRow = new DBRow();
		for(int i=0; i<baseData.length; i++){
			DBRow r = baseData[i];
			String type = r.getString("type").toLowerCase();
			String val = r.getString("param").toLowerCase();
			if("warehouse".equals(type)){
				char p = val.toCharArray()[0];
				whChar[pCount] = p;
				whRow.add(p+"",r.getString("lng")+","+r.getString("lat"));
				pCount++;
			}else if("maxx".equals(type)){
				maxX = Double.parseDouble(val);
			}else if("maxy".equals(type)){
				maxY = Double.parseDouble(val);
			}
		}
		//基础数据有误，无法计算
		if(pCount<3 || maxX<=0 || maxY<=0){
			return "";
		}
		//按字母编号排序
		whChar = Arrays.copyOfRange(whChar, 0, pCount);
		Arrays.sort(whChar);
		String pointA = whRow.getString(whChar[0]+"");
		String pointB = whRow.getString(whChar[1]+"");
		String pointD = whRow.getString(whChar[whChar.length-1]+"");
		
		//计算缩放比例和旋转角度
		double aLng = Double.parseDouble(pointA.split(",")[0]);
		double aLat = Double.parseDouble(pointA.split(",")[1]);
		double bLng = Double.parseDouble(pointB.split(",")[0]);
		double bLat = Double.parseDouble(pointB.split(",")[1]);
		double dLng = Double.parseDouble(pointD.split(",")[0]);
		double dLat = Double.parseDouble(pointD.split(",")[1]);
		double ratio = Math.hypot(aLat-bLat,aLng-bLng)/maxX;
		double radX = Math.atan((aLat-bLat)/(aLng-bLng));
		double radY = Math.atan((aLat-dLat)/(aLng-dLng));
		if(radY <= 0){
			radY += Math.PI;
		}
		radY -= Math.PI/2;
		double radN = radX - radY;
		double maxY_map = Math.hypot(aLat-dLat,aLng-dLng)/ratio; 
		
		double X = x;
		double Y = y;
		
		//仓库相对地图旋转、缩放、平移等处理
		Y /= maxY/maxY_map;  //Y轴拉伸
		
		X = X + Y*Math.sin(radN)*Math.cos(radN);
		Y = Y*Math.cos(radN);
		
		int sign = X>0 ? 1 : -1 ;//由于atan(x)结果在第1,4象限，计算rad时2,3象限的点被旋转180°，即中心对称，因此需作此处理
		double rad = radX + Math.atan(Y/X);
		double len = Math.hypot(X,Y); 
		double lat = sign*len*ratio*Math.sin(rad) + aLat;
		double lng = sign*len*ratio*Math.cos(rad) + aLng;
		if(Double.isNaN(lat)){
			lat = aLat;
		}
		if(Double.isNaN(lng)){
			lng = aLng;
		}
		return lng +","+ lat;
	}
	public String convertLatlngToCoordinate(long psId,double lat, double lng)throws Exception {
		DBRow[] baseData = getExcelBaseData(psId);
		double maxX = 0;
		double maxY = 0;
		
		int pCount = 0;
		char[] whChar = new char[26];
		DBRow whRow = new DBRow();
		for(int i=0; i<baseData.length; i++){
			DBRow r = baseData[i];
			String type = r.getString("type").toLowerCase();
			String val = r.getString("param").toLowerCase();
			if("warehouse".equals(type)){
				char p = val.toCharArray()[0];
				whChar[pCount] = p;
				whRow.add(p+"",r.getString("lng")+","+r.getString("lat"));
				pCount++;
			}else if("maxx".equals(type)){
				maxX = Double.parseDouble(val);
			}else if("maxy".equals(type)){
				maxY = Double.parseDouble(val);
			}
		}
		//基础数据有误，无法计算
		if(pCount<3 || maxX<=0 || maxY<=0){
			return "";
		}
		//按字母编号排序
		whChar = Arrays.copyOfRange(whChar, 0, pCount);
		Arrays.sort(whChar);
		String pointA = whRow.getString(whChar[0]+"");
		String pointB = whRow.getString(whChar[1]+"");
		String pointD = whRow.getString(whChar[whChar.length-1]+"");
		
		//计算缩放比例和旋转角度
		double aLng = Double.parseDouble(pointA.split(",")[0]);
		double aLat = Double.parseDouble(pointA.split(",")[1]);
		double bLng = Double.parseDouble(pointB.split(",")[0]);
		double bLat = Double.parseDouble(pointB.split(",")[1]);
		double dLng = Double.parseDouble(pointD.split(",")[0]);
		double dLat = Double.parseDouble(pointD.split(",")[1]);
		double ratio = Math.hypot(aLat-bLat,aLng-bLng)/maxX;
		double radX = Math.atan((aLat-bLat)/(aLng-bLng));
		double radY = Math.atan((aLat-dLat)/(aLng-dLng));
		if(radY <= 0){
			radY += Math.PI;
		}
		radY -= Math.PI/2;
		double radN = radX - radY;
		double maxY_map = Math.hypot(aLat-dLat,aLng-dLng)/ratio; 
		
		lng -= aLng;
		lat -= aLat;
		
		int sign = lng>0 ? 1 : -1 ;//由于atan(x)结果在第1,4象限，计算rad时2,3象限的点被旋转180°，即中心对称，因此需作此处理
		double rad = radX - Math.atan(lat/lng);
		double len = Math.hypot(lat,lng);
		
		double Y = sign*len*Math.sin(rad)/ratio*maxY/maxY_map;
		double X = sign*len*Math.cos(rad)/ratio;
		
		Y /= -Math.cos(radN);
		X -= Y*Math.sin(radN)/(maxY/maxY_map);
		
		if(Double.isNaN(X)){
			X = 0;
		}
		if(Double.isNaN(Y)){
			Y = 0;
		}
		return X+","+Y;
		
	}
	public int deleteLayerByPsId(String field, long ps_id,String tablename)throws Exception{
		String wherecond = "where " + field + " = '" + ps_id + "'";
		return dbUtilAutoTran.delete(wherecond, tablename);
	}
	public void deleteWebcam(String ps_id) throws Exception {
		String sql = "delete  from " + ConfigBean.getStringValue("webcam")
				+ " where ps_id ="+ ps_id;
		  dbUtilAutoTran.executeSQL(sql);
	}

	public void deleteCheckInZone(String ps_id) throws Exception {
		String sql = "delete  from "
				+ ConfigBean.getStringValue("check_in_zone_with_title")
				+ "  where area_id in ( select distinct area_id  from "
				+ ConfigBean.getStringValue("storage_location_area")
				+ " where   area_psid='" + ps_id + "')";
		
		dbUtilAutoTran.executeSQL(sql);

	}
	 
		/**
		 * @param data
		 * @param condition
		 * @return
		 * @throws Exception
		 */
		public int updateFolderkml(DBRow data,DBRow condition) throws Exception{
			 String ps_id= condition.getString("ps_id");
			 String area_name =condition.getString("area_name","");
			 String placemark_name =condition.getString("placemark_name","");
			 String folder_name =condition.getString("folder_name","");
			 String wherecond ="where ps_id= '"+ps_id+"' and folder_name= '"+folder_name+"'" ;
			 if(!area_name.equals("")&&area_name!=null){
				 wherecond+=" and area_name = '"+area_name+"'";
			 }
			 if(!placemark_name.equals("")&&placemark_name!=null){
				 wherecond+=" and placemark_name ='"+placemark_name+"'";
			 }
			 String tablename =ConfigBean.getStringValue("folder_from_kml");
	    	return 	 dbUtilAutoTran.update(wherecond, tablename, data);
		 }
	 public DBRow getStorageById(String psId) throws Exception{
		 String sql = "select * from "+ConfigBean.getStringValue("product_storage_catalog")+" p where p.id="+psId;
		 return dbUtilAutoTran.selectSingle(sql);
	 }
	public DBRow[] getExcelAreaData(long ps_id) throws Exception {
		try {
			String sql = "SELECT a.*, h.doorId, b.title_name FROM " +
					ConfigBean.getStringValue("storage_location_area") + "  a" +
					" LEFT JOIN (select c.area_id area_id , GROUP_CONCAT(distinct e.doorId ORDER BY e.doorId) doorId from "+ConfigBean.getStringValue("storage_area_door")+" c, "+ConfigBean.getStringValue("storage_door")+" e where c.sd_id =e.sd_id GROUP BY area_id) h ON a.area_id = h.area_id" +
					" LEFT JOIN (select m.area_id area_id , GROUP_CONCAT(distinct n.title_name ) title_name from "+ConfigBean.getStringValue("check_in_zone_with_title")+" m, "+ConfigBean.getStringValue("title")+" n where m.title_id =n.title_id GROUP BY area_id) b ON a.area_id = b.area_id" +
					" where  a.area_psid="+ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getExcelAreaData():" + e);
		}

	}

	public DBRow[] getExcelDocksData(long ps_id) throws Exception {
		try {
			String sql = "select  c.* ,d.area_name " + "from "
					+ ConfigBean.getStringValue("storage_door") + " c "
					+" LEFT JOIN "+ConfigBean.getStringValue("storage_location_area")+" d "
					+" on c.area_id =d.area_id "
					+ " WHERE c.ps_id =" + ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getExcelDocksData():" + e);
		}
	}

	public DBRow[] getExcelParkingData(long ps_id) throws Exception {
		try {
			String sql = "select  c.* ,d.area_name " + "from "
					+ ConfigBean.getStringValue("storage_yard_control") + " c "
					+" LEFT JOIN "+ ConfigBean.getStringValue("storage_location_area") +" d "
					 +" on c.area_id = d.area_id "
					+ " WHERE c.ps_id =" + ps_id +" order by yc_no ";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getExcelParkingData():" + e);
		}
	}

	public DBRow[] getExcelStagingData(long ps_id) throws Exception {
		try {
			String sql = "select  c.* ,d.doorId " + "from "
					+ ConfigBean.getStringValue("storage_load_unload_location") + " c "
					+" LEFT JOIN " +ConfigBean.getStringValue("storage_door")+" d "
					+ " on c.sd_id =d. sd_id "
					+ " WHERE c.psc_id =" + ps_id ;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getExcelStagingData():" + e);
		}

	}

	public DBRow[] getExcelLocationData(long ps_id) throws Exception {
		try {
			String sql = "select  c.* , s.area_name " + "from "
					+ ConfigBean.getStringValue("storage_location_catalog") + " c "
					+ "JOIN "+ConfigBean.getStringValue("storage_location_area")+" s on s.area_id = c.slc_area "
					+ " WHERE c.slc_psid =" + ps_id + "";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getExcelLocationData():" + e);
		}
	}

	public DBRow[] getExcelBaseData(long ps_id) throws Exception {
		try {
			String sql = "select c.* from "
					+ ConfigBean.getStringValue("storage_warehouse_base")
					+ " c  where c.ps_id = "+ ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getExcelBaseData():" + e);
		}
	}
	public DBRow[] getExcelWebcamData(long ps_id) throws Exception {
		try {
			String sql = "select c.* from "
					+ ConfigBean.getStringValue("webcam")
					+ " c  where c.ps_id = "+ ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getExcelWebcamData():" + e);
		}
	}
	public DBRow[] getExcelPrinterData(long ps_id) throws Exception {
		try {
			String sql = "select c.* from "
					+ ConfigBean.getStringValue("printer")
					+ " c  where c.ps_id = "+ ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getExcelPrinterData():" + e);
		}
	}
	/**
	 * 通过ps_id查询area
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaByPsId(String ps_id) throws Exception{
		try {
			String sql = "select  c.* from "+ 
			ConfigBean.getStringValue("storage_location_area") +
			" c where c.area_psid ="+ ps_id +" ORDER BY c.area_type DESC, c.area_name";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getAreaByPsId():" + e);
		}
	}
	/**
	 * 通过ps_id,type 查询area
	 * @param ps_id
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaByPsidAndType(String ps_id,String type) throws Exception{
		try {
			String sql = "select  c.* from "+ 
					ConfigBean.getStringValue("storage_location_area") +
					" c where c.area_psid ="+ ps_id + " and c.area_type="+type;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getAreaInfo():" + e);
		}
	} 
	/**
	 * 通过ps_id查询door
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDoorByPsId(String ps_id) throws Exception{
		try {
			String sql = "select  c.* from "+ 
					ConfigBean.getStringValue("storage_door") +
					" c where c.ps_id ="+ ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getDoorByPsId():" + e);
		}
	}
	public DBRow[] getLocationbyArea(String slc_area,long psId)throws Exception{
		String sql = "select  c.* , s.area_name " + "from "
				+ ConfigBean.getStringValue("storage_location_catalog") + " c "
				+ "JOIN "+ConfigBean.getStringValue("storage_location_area")+" s on s.area_id = c.slc_area "
				+ " WHERE c.slc_psid =" + psId +" and s.area_name ='"+slc_area+"'";
		try {
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorGoogleMapsMgrCc.getLocationbyArea():" + e);
		}
		
	}
	
		public DBRow[] getWebcambyPsid(long psId)throws Exception{
			String sql ="select c.* from "	+ConfigBean.getStringValue("webcam") +"  c where  ps_id ="+psId ;
			try {
				return dbUtilAutoTran.selectMutliple(sql);
			} catch (Exception e) {
				throw new Exception("FloorGoogleMapsMgrCc.getWebcambyPsid():" + e);
			}
	   }
		public DBRow[] getPrinterbyPsid(long psId)throws Exception{
			String sql ="select c.*,d.area_name area_name,d.area_id area_id from "+
		            ConfigBean.getStringValue("printer")+" c " +
					"left join "+ConfigBean.getStringValue("storage_location_area") +" d on  c.physical_area=d.area_id  where  c.ps_id="+psId;
			try {
				return dbUtilAutoTran.selectMutliple(sql);
			} catch (Exception e) {
				throw new Exception("FloorGoogleMapsMgrCc.getPrinterbyPsid():" + e);
			}
		}
		public DBRow[] queryStorageKml()throws  Exception{
			
			try {
				String tableName=ConfigBean.getStringValue("storage_kml");
				return 	dbUtilAutoTran.select(tableName);
			} catch (Exception e) {
				throw new Exception("FloorGoogleMapsMgrCc.queryStorageKml():"+e);
			}
			
		}
		/**
		 * 添加storage_kml数据
		 * @return
		 * @throws Exception
		 */
		public long addOrUpdateStorageKml(DBRow storageKml) throws Exception{
			try {
				String psId = storageKml.getString("ps_id");
				String storage_kml = ConfigBean.getStringValue("storage_kml");
				String sql = "SELECT * FROM "+storage_kml+" s WHERE s.ps_id = ? ";
				DBRow para = new DBRow();
				para.add("ps_id", psId);
				DBRow row = dbUtilAutoTran.selectPreSingle(sql, para);
				if(row == null){
					return dbUtilAutoTran.insertReturnId(storage_kml, storageKml);
				}else{
					return dbUtilAutoTran.update(" where ps_id = " + psId, storage_kml, storageKml);
				}
			} catch (Exception e) {
				throw new Exception("FloorGoogleMapsMgrCc.addOrUpdateStorageKml():"+e);
			}
		}

		public long addLocationCatalog(DBRow dbrow) throws Exception {
			try {
				long slc_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("storage_location_catalog"));
				dbrow.add("slc_id", slc_id);
				dbUtilAutoTran.insert(ConfigBean.getStringValue("storage_location_catalog"),dbrow);
				return slc_id;
			} catch (Exception e) {
				throw new Exception("FloorGoogleMapsMgrCc addLocationCatalog error:"
						+ e);
			}
		}
       
		public long addLocationArea(DBRow dbrow) throws Exception {
			try {
				long area_id = this.dbUtilAutoTran.getSequance(ConfigBean
						.getStringValue("storage_location_area"));
				dbrow.add("area_id", area_id);
				this.dbUtilAutoTran.insert(
						ConfigBean.getStringValue("storage_location_area"), dbrow);

				return area_id;
			} catch (Exception e) {
				throw new Exception("FloorGoogleMapsMgrCc addLocationArea error:" + e);
			}
		}
		 public long addStorageAreaDoor(long areaId, long sdId)
				    throws Exception
				  {
				    try
				    {
				      long id = 0L;
				      String table = ConfigBean.getStringValue("storage_area_door");
				      String sql = "select 1 from " + table + " s where s.area_id=" + areaId + " and s.sd_id=" + sdId;
				      DBRow[] rows = this.dbUtilAutoTran.selectMutliple(sql);
				      if (rows.length == 0) {
				        DBRow r = new DBRow();
				        r.add("area_id", areaId);
				        r.add("sd_id", sdId);
				        id = this.dbUtilAutoTran.insertReturnId(table, r);
				      }
				      return id;
				    } catch (Exception e) {
				      throw new Exception("FloorGoogleMapsMgrCc addStorageAreaDoor error:" + e);
				    }
				  }
		 

			public long addLoadUnloadLocation(DBRow rows) throws Exception {
				try {
					return this.dbUtilAutoTran.insertReturnId(
							ConfigBean.getStringValue("storage_load_unload_location"),
							rows);
				} catch (Exception e) {
					throw new Exception(
							"FloorGoogleMapsMgrCc addLoadUnloadLocation error:" + e);
				}
			}
			 public long addStorageYardControl(DBRow row)
					    throws Exception
					  {
					    try
					    {
					      return this.dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("storage_yard_control"), row);
					    } catch (SQLException e) {
					      throw new Exception("FloorGoogleMapsMgrCc addStorageYardControl error:" + e);
					    }
					  }

				public long addStorageDoor(DBRow drow) throws Exception {
					try {
						return this.dbUtilAutoTran.insertReturnId(
								ConfigBean.getStringValue("storage_door"), drow);
					} catch (Exception e) {
						throw new Exception("FloorGoogleMapsMgrCc addStorageDoor error:"
								+ e);
					}
				}
				
				/**
				 * 根据位置查询LOCATION信息
				 * @param position
				 * @return
				 * @throws Exception
				 */
				public DBRow getLocationByPosition(long psId, String name) throws Exception{
					try {
						String sql="select c.*  from "
								+ConfigBean.getStringValue("storage_location_catalog")+" c" +
								" where (slc_position_all = '"+name+"' or slc_position = '"+name+"') and slc_psid="+psId;
						return dbUtilAutoTran.selectSingle(sql);
					} catch (Exception e) {
						throw new Exception("FloorGoogleMapsMgrCc.getLocationByPosition():"+e);
					}
				}
				
				/**
				 * 查询未被占用的门
				 * @param position
				 * @return
				 * @throws Exception
				 */
				public DBRow[] queryEmptyDocks(long psId) throws Exception{
					try {
						String sql=" select sd.sd_id obj_id ,sd.doorId obj_name,sd.ps_id,sd.x,sd.y,sd.sd_status,sd.width,sd.height,sd.angle,sd.latlng "
								+ " from storage_door sd where sd.ps_id="+psId
								+" and  sd.sd_id NOT IN( select srr.resources_id from "
								+ " space_resources_relation srr where srr.resources_type=1 )";
						return dbUtilAutoTran.selectMutliple(sql);
					} catch (Exception e) {
						throw new Exception("FloorGoogleMapsMgrCc.queryEmptyDocks():"+e);
					}
				}
				/**
				 * 通过id来查询dock
				 * @param position
				 * @return
				 * @throws Exception
				 */
				public DBRow queryDocksById(long psId,long id) throws Exception{
					try {
						String sql=" select sd.sd_id obj_id ,sd.doorId obj_name,sd.ps_id,sd.x,sd.y,sd.sd_status,sd.width,sd.height,sd.angle,sd.latlng "
								+ " from storage_door sd where sd.ps_id="+psId
								+" and  sd.sd_id ="+id;
						return dbUtilAutoTran.selectSingle(sql);
					} catch (Exception e) {
						throw new Exception("FloorGoogleMapsMgrCc.queryDocksById():"+e);
					}
				}
				public DBRow[] getStorageOnMap()throws Exception{
					
					try {
						String sql ="select p.* from product_storage_catalog p where storage_type =1 and id in (select distinct ps_id from storage_warehouse_base ) ";
						return dbUtilAutoTran.selectMutliple(sql);
					} catch (Exception e) {
						e.printStackTrace();
						throw new Exception("FloorGoogleMapsMgrCc.getStorageWithoutOnMap() error:"+e);
					}
				}
				public int deleteStorageKml(long ps_id)throws Exception{
					
					try {
						String wherecond=" where ps_id ="+ps_id; 
						String tablename=ConfigBean.getStringValue("storage_kml"); 
						return dbUtilAutoTran.delete(wherecond, tablename);
					} catch (Exception e) {
						e.printStackTrace();
						return 0;
					}
				}
				public int deleteStorageWerehouseBase(long ps_id) throws Exception {
						try {
							String wherecond=" where ps_id ="+ps_id; 
							String tablename=ConfigBean.getStringValue("storage_warehouse_base"); 
							return dbUtilAutoTran.delete(wherecond, tablename);
						} catch (Exception e) {
							e.printStackTrace();
							return 0;
						}
				}

				public DBRow[] getAllStroageBase()throws Exception{
					try {
						String sql =" select c.*,d.title from storage_warehouse_base c join product_storage_catalog d  on c.ps_id=d.id  where c.type ='warehouse' order by c.ps_id,c.param  ";
						return  dbUtilAutoTran.selectMutliple(sql);
					} catch (Exception e) {
						e.printStackTrace();
						throw new Exception("FloorGoogleMapsMgrCc.getAllStroageBase() error:"+e);
					}
				}
				public DBRow getGisVersion()throws Exception{
					try {
						String sql =" select c.confvalue from config c  where c.confname ='gis_version'";
						return  dbUtilAutoTran.selectSingle(sql);
					} catch (Exception e) {
						e.printStackTrace();
						throw new Exception("FloorGoogleMapsMgrCc.getAllStroageBase() error:"+e);
					}
				}
				public void batchSql(String[] sql)throws Exception{
					try {
						dbUtilAutoTran.batchUpdate(sql);
					} catch (Exception e) {
						e.printStackTrace();
						throw new Exception("FloorGoogleMapsMgrCc.batchSql() error:"+e);
					}
				}
				public DBRow[] selectInCounts(long ps_id,String[] fieldValues,String fieldName,String tableName)throws Exception{
					try {
						String fieldStr="";
						for(int i=0;i<fieldValues.length;i++){
							fieldStr+="'"+fieldValues[i]+"',";
						}
						String psIdCon = "";
						switch(tableName.trim()){
							case "storage_location_catalog": 
								psIdCon = " and t.slc_psid="+ ps_id+ " ";
								break;
							default: 
								psIdCon = " and t.ps_id="+ ps_id+ " ";
						}
						fieldStr=fieldStr.substring(0, fieldStr.length()-1);
						String sql ="select count(1) count from " +tableName +" t where t."+fieldName+" in ("+fieldStr+") "+psIdCon;
						return 	dbUtilAutoTran.selectMutliple(sql);
					} catch (Exception e) {
						e.printStackTrace();
						throw new Exception("FloorGoogleMapsMgrCc.selectInCounts() error:"+e);
					}
				}
				public void updateGisVersion()throws Exception{
					try {
						
						String sql ="update config c set c.confvalue=c.confvalue+1 where c.confname='gis_version'";
						dbUtilAutoTran.executeSQL(sql);
					} catch (Exception e) {
						e.printStackTrace();
						throw new Exception("FloorGoogleMapsMgrCc.updateGisVersion() error:"+e);
					}
				}
				/**
				 * 查询 查询商品的类型，返回其子类型到父类型的集合
				 * @param pc_id
				 * @return
				 * @throws Exception
				 */
				public DBRow queryCatalogs(long pc_id)throws Exception{
					try {
						
						String sql ="SELECT p.pc_id, p.p_name,p.catalog_id,"
								+ " IFNULL(CONCAT(CASE c1.parentid WHEN 0 THEN '' ELSE CONCAT(CASE c2.parentid WHEN 0 THEN '' ELSE CONCAT(c2.parentid, ',') END, c2.id, ',') END,  p.catalog_id), '') catalogs"
								+ " from product p "
								+ " left JOIN product_catalog c1 on c1.id = p.catalog_id"
								+ " LEFT JOIN product_catalog c2 on c2.id = c1.parentid"
								+ " WHERE p.pc_id ="+pc_id;
					return 	dbUtilAutoTran.selectSingle(sql);
					} catch (Exception e) {
						e.printStackTrace();
						throw new Exception("FloorGoogleMapsMgrCc.queryCatalogs() error:"+e);
					}
				}
			    /**
			     * 将long数组返回mysql in () 的string类型
			     * @param params
			     * @return
			     * @throws Exception
			     */
				public String LongArraytoString(long[] params)throws Exception{
					if(params.length<=0){
						return "()" ;
					}else{
						String ret="(";
						for(int i=0;i<params.length;i++){
							ret+=Long.toString(params[i]);
							ret+=",";
						}
						ret =ret.substring(0, ret.lastIndexOf(","));
					    ret+=")";	
					    return ret ;
					}
				}
}
