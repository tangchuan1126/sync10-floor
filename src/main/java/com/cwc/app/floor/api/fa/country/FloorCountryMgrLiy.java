package com.cwc.app.floor.api.fa.country;

import java.util.HashMap;
import java.util.Map;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;
import com.cwc.db.PageCtrl;
/**
 * 国家信息
 * table country_code
 * @author liyi
 *
 */
public class FloorCountryMgrLiy {
	private DBUtilAutoTran_Optm dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	/**
	 * 获取所有的国家列表信息
	 * @param par
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllCountry(DBRow par,PageCtrl pc) throws Exception {
		try{
			DBRow[] rows = null;
			StringBuilder sql = new StringBuilder("select * from "+ConfigBean.getStringValue("country_code") );
			
			if (par.get("searchConditions")!=null && !"".equals(par.getString("searchConditions"))) {
				sql.append(" where c_country like '%"+par.getString("searchConditions")+"%' ");
				sql.append(" or c_code like '%"+par.getString("searchConditions")+"%' ");
				sql.append(" or c_code2 like '%"+par.getString("searchConditions")+"%'");	
			}
			
			if(pc != null) {
				rows = dbUtilAutoTran.selectMutliple(sql.toString(),pc);
			} else {
				rows = dbUtilAutoTran.selectMutliple(sql.toString());
			}
			return rows;
		}catch(Exception e) {
			throw new Exception("FloorCountryMgr.getAllCountry(DBRow par,PageCtrl pc) error "+e.getMessage());
		}
	}
	/**
	 * 根据国家，获取所有省份
	 * @param ccid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllProvinceByCountryId(long ccid,PageCtrl pc) throws Exception {
		try {
			String sql = "select * from "
					+ ConfigBean.getStringValue("country_province")
					+ " where nation_id=? order by pro_name asc";
			DBRow para = new DBRow();
			para.add("ccid", ccid);
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} catch (Exception e) {
			throw new Exception("FloorCountryMgr.getAllProvinceByCountryId(long ccid,PageCtrl pc) error:"+ e);
		}
	}
	/**
	 * 新增国家
	 * @param row
	 * @throws Exception
	 */
	public long insertCountry(DBRow row) throws Exception {
		try{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("country_code"), row);
		} catch (Exception e) {
			throw new Exception(
					"FloorCountryMgr.insertCountry(DBRow row) error:"
							+ e);
		}
	}
	/**
	 * 获取国家信息
	 * @param ccid
	 * @return
	 * @throws Exception
	 */
	public DBRow getCountryById(long ccid) throws Exception {
		try{
			String sql = "select * from "+ ConfigBean.getStringValue("country_code")+" where ccid="+ccid;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorCountryMgr.getCountryById(long ccid) error:"
							+ e);
		}
	}
	
	/**
	 * 更新国家信息
	 * @param ccid
	 * @param row
	 * @throws Exception
	 */
	public int updateCountry(long ccid,DBRow row) throws Exception {
		try{
			return dbUtilAutoTran.update("where ccid="+ccid,ConfigBean.getStringValue("country_code"), row);
		} catch (Exception e) {
			throw new Exception(
					"FloorCountryMgr.updateCountry(long ccid,DBRow row) error:"
							+ e);
		}
	}
	
	/**
	 * 删除国家
	 * @param ccid
	 * @return
	 * @throws Exception
	 */
	public int deleteCountryById(long ccid) throws Exception {
		try{
			deltetProvinceByCcid(ccid); //先把省份删除
			return dbUtilAutoTran.delete("where ccid="+ccid, ConfigBean.getStringValue("country_code"));
		} catch (Exception e) {
			throw new Exception(
					"FloorCountryMgr.deleteCountryById(long ccid) error:"
							+ e);
		}
		
	}
	/**
	 * 根据国家，删除所有的省份
	 * @param ccid
	 * @throws Exception
	 */
	public void deltetProvinceByCcid(long ccid) throws Exception {
		try{
			dbUtilAutoTran.delete("where nation_id="+ccid, ConfigBean.getStringValue("country_province"));
		} catch (Exception e) {
			throw new Exception(
					"FloorCountryMgr.deltetProvinceByCcid(long ccid) error:"
							+ e);
		}
	}
	
	/**
	 * 根据字段名称进行检查
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public boolean checkCountryField(String condition) throws Exception {
		try{
			StringBuilder sql = new StringBuilder("select count(ccid) cnt from "+ConfigBean.getStringValue("country_code")+" where 1=1 ");
			sql.append(condition);
			if(dbUtilAutoTran.selectMutliple(sql.toString())[0].get("cnt",0l)>0) {
				return true;
			} 
			return false;
		}catch(Exception e) {
			throw new Exception("FloorCountryMgr.checkCountryField(String condition) error "+e.getMessage());
		}
	}
	/**
	 * 
	 * @param dbRow原数据库的数据
	 * @param pageRow 页面传来的数据
	 * @param fieldName 要验证的字段
	 * @return
	 * @throws Exception
	 */
	public boolean getCheckedResult(DBRow dbRow,DBRow pageRow,String fieldName) throws Exception{
		boolean isExist = false;
		if(dbRow.getString(fieldName).equals(pageRow.getString(fieldName))) {
			isExist = false;
		}else {
			isExist =checkCountryField(" and "+fieldName+"='"+pageRow.getString(fieldName)+"' ");
		}
		return isExist;
	}
	
	
	/**
	 * 检查国家 name,code,code2 不能重复
	 * @param ccid
	 * @param para
	 * @return
	 * @throws Exception
	 */
	public Map<String,Boolean> checkCountry(long ccid,DBRow para) throws Exception {
		try{
			Map<String,Boolean> isExists = new HashMap<String,Boolean>();
			boolean c_code_isExist = false;
			boolean c_code2_isExist = false;
			boolean c_country_isExist = false;
			DBRow country = new DBRow();
			if(ccid>0) { //修改的时候 判断某个字段没有进行修改，就不需要验证
				country = getCountryById(ccid);
			}
			if(para.get("c_code")!=null && !"".equals(para.getString("c_code"))) {
				c_code_isExist = getCheckedResult(country,para,"c_code");
				isExists.put("c_code_isExist", c_code_isExist);
			}
			if(para.get("c_code2")!=null && !"".equals(para.getString("c_code2"))) {
				c_code2_isExist = getCheckedResult(country,para,"c_code2");
				isExists.put("c_code2_isExist", c_code2_isExist);
			}
			if (para.get("c_country")!=null && !"".equals(para.getString("c_country"))) {
				c_country_isExist = getCheckedResult(country,para,"c_country");
				isExists.put("c_country_isExist", c_country_isExist);
			}
			
			return isExists;
		}catch(Exception e) {
			throw new Exception("FloorCountryMgr.checkCountry(long ccid,DBRow para) error"+e.getMessage());
		}
	}
	

}
