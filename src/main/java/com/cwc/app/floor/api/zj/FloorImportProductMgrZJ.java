package com.cwc.app.floor.api.zj;

import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorImportProductMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 商品信息临时导入表
	 * @param dbrow
	 * @throws Exception
	 */
	public long addImportProduct(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long pc_id;
			try 
			{
				pc_id = Long.parseLong(dbrow.getString("pc_id"));
			}
			catch(NumberFormatException e) 
			{
				pc_id = 0;
			}
			
			if(pc_id==0)
			{
				pc_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("product"));
				dbrow.add("pc_id",pc_id);
			}
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("import_product"),dbrow);
			
			return pc_id;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ addImportProduct error:"+e);
		}
	}
	
	/**
	 * 通过条码获得临时商品信息
	 * @param pcode
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailTemporaryProductByPname(String p_name,String filename)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("import_product")+" where filename= ? and p_name = ?";
			
			if(p_name.equals(""))
			{
				return null;
			}
			
			DBRow para = new DBRow();
			para.add("p_name",p_name);
			para.add("filename",filename);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception();
		}
	}
	
	/**
	 * 删除商品信息临时导入表
	 * @throws Exception
	 */
	public void delImportProduct(String filename)
		throws Exception
	{
		DBRow para = new DBRow();
		para.add("filename",filename);
		dbUtilAutoTran.deletePre("where filename=?",para,ConfigBean.getStringValue("import_product"));
	}
	
	/**
	 * 套装关系存入临时表
	 * @param dbrow
	 * @throws Exception
	 */
	public void addImportUnion(DBRow dbrow)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.insert(ConfigBean.getStringValue("import_product_union"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ addImportUnion error:"+e);
		}
	}
	
	/**
	 * 删除套装关系
	 * @throws Exception
	 */
	public void delImportUnion(String filename)
		throws Exception
	{
		try 
		{
			DBRow para = new DBRow();
			para.add("filename",filename);
			
			dbUtilAutoTran.deletePre("where filename=?",para,ConfigBean.getStringValue("import_product_union"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ delImportUnion error:"+e);
		}
	}
	
	
	/**
	 * 检查导入商品信息，返回错误的商品信息
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkImportProduct(String filename)
		throws Exception
	{
		try 
		{
			String sql = "select number ,pc_id,p_name,catalog_id,unit_name,unit_price ,length,width,heigth,weight,union_flag,alive,volume,filename,coalesce(errortype,'CATALOGTYPEERROR') as errortype from import_product where (catalog_id regexp '[^0-9]$')and (filename='"+filename+"')"
						+" union "
						+"select number,pc_id,p_name,catalog_id,unit_name ,unit_price,length,width,heigth,weight,union_flag,alive,volume,filename,coalesce(errortype,'CATALOGEERROR') as errortype from import_product ip where (not(catalog_id in (select product_catalog.id from product_catalog)))and (filename='"+filename+"')"
						+" union "
						+"select number,pc_id,p_name,catalog_id,unit_name,unit_price,length,width,heigth,weight,union_flag,alive,volume,filename,coalesce(errortype,'PCODEEMPTY') as errortype from import_product ip WHERE not EXISTS(select ipc.pc_id from import_product_code ipc where filename='"+filename+"' and ipc.pc_id = ip.pc_id and ipc.code_type = "+CodeTypeKey.Main+")AND(filename='"+filename+"')"
						+" union "
						+"SELECT ip.number,ip.pc_id,ip.p_name,ip.catalog_id,ip.unit_name,ip.unit_price,ip.length,ip.width,ip.heigth,ip.weight,ip.union_flag,ip.alive,ip.volume,ip.filename,COALESCE(ip.errortype, 'PCODEHAS')AS errortype FROM import_product ip join import_product_code as ipc on ipc.pc_id = ip.pc_id and ipc.filename='"+filename+"' JOIN product_code as pcode on ipc.p_code = pcode.p_code and ipc.pc_id<>pcode.pc_id WHERE ip.filename='"+filename+"'" 
						+" union "
						+"select number,pc_id,p_name,catalog_id,unit_name,unit_price,length,width,heigth,weight,union_flag,alive,volume,filename,coalesce(errortype,'PNAMEEMPTY') as errortype from import_product where ((isnull(p_name) or (p_name = '')))and (filename='"+filename+"')"
						+" union "
						+"select ip.number,ip.pc_id,ip.p_name,ip.catalog_id,ip.unit_name,ip.unit_price,ip.length,ip.width,ip.heigth,ip.weight,ip.union_flag,ip.alive,ip.volume,ip.filename,coalesce(errortype,'PNAMEHAS') as errortype from (import_product ip join product p) where ((ip.p_name = p.p_name) and (ip.pc_id <> p.pc_id))and (ip.filename='"+filename+"')and(p.orignal_pc_id =0)"
						+" union "
						+"SELECT ip.number,ip.pc_id,ip.p_name,ip.catalog_id,ip.unit_name,ip.unit_price,ip.length,ip.width,ip.heigth,ip.weight,ip.union_flag,ip.alive,ip.volume,ip.filename,COALESCE(ip.errortype,'SELFPCODEHAS')AS errortype FROM	import_product ip JOIN import_product_code ipc ON ipc.pc_id = ip.pc_id and ipc.filename = '"+filename+"' JOIN import_product_code ipc2 ON ipc.p_code = ipc2.p_code and ipc2.filename = '"+filename+"' AND ipc.pc_id <> ipc2.pc_id WHERE ip.filename = '"+filename+"'"
						+" union "
						+"select ip.number,ip.pc_id,ip.p_name,ip.catalog_id,ip.unit_name,ip.unit_price,ip.length,ip.width,ip.heigth,ip.weight,ip.union_flag,ip.alive,ip.volume,ip.filename,coalesce(ip.errortype,'SELFPNAMEHAS') as errortype from (import_product ip join import_product ip2) where ((ip.p_name = ip2.p_name) and (ip.pc_id <> ip2.pc_id))and (ip.filename='"+filename+"')and(ip2.filename='"+filename+"')"
						+" union "
						+"select number,pc_id,p_name,catalog_id,unit_name,unit_price,length,width,heigth,weight,union_flag,alive,volume,filename,coalesce(errortype,'UNITPRICEERROR') as errortype from import_product where ((not((unit_price regexp '^(([0-9]+.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*.[0-9]+)|([0-9]*[1-9][0-9]*))$'))) and (not(pc_id in (select import_product_union.set_pid from import_product_union group by import_product_union.set_pid))))and (filename='"+filename+"')"
						+" union "
						+"select number,pc_id,p_name,catalog_id,unit_name,unit_price,length,width,heigth,weight,union_flag,alive,volume,filename,coalesce(errortype,'UNITNAMEEMPTY') as errortype from import_product where (isnull(unit_name) or (unit_name = ''))and (filename='"+filename+"')"
//						+" union "
//						+"select number,pc_id,p_name,catalog_id,unit_name,unit_price,length,width,heigth,weight,union_flag,alive,volume,filename,coalesce(errortype,'VOLUMEERROR') as errortype  from import_product where (not((volume regexp '^(([0-9]+.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*.[0-9]+)|([0-9]*[1-9][0-9]*))$')))and (filename='"+filename+"')"
						+" union "
						+"select number,pc_id,p_name,catalog_id,unit_name,unit_price,length,width,heigth,weight,union_flag,alive,volume,filename,coalesce(errortype,'WEIGHTERROR') as errortype from import_product where ((not((weight regexp '^(([0-9]+.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*.[0-9]+)|([0-9]*[1-9][0-9]*))$'))) and (not(pc_id in (select import_product_union.set_pid from import_product_union group by import_product_union.set_pid))))and (filename='"+filename+"')"
						+" union "
						+"select number,pc_id,p_name,catalog_id,unit_name,unit_price,length,width,heigth,weight,union_flag,alive,volume,filename,coalesce(errortype,'ALIVEERROR') as errortype from import_product where (alive !=0) and (alive!=1) and (filename='"+filename+"')"
						+" union "
						+"select number,pc_id,p_name,catalog_id,unit_name,unit_price,length,width,heigth,weight,union_flag,alive,volume,filename,coalesce(errortype,'LENGTHERROR') as errortype  from import_product where (not((length regexp '^(0|([0-9]*[1-9][0-9]*))$')))and (filename='"+filename+"')"
						+" union "
						+"select number,pc_id,p_name,catalog_id,unit_name,unit_price,length,width,heigth,weight,union_flag,alive,volume,filename,coalesce(errortype,'WIDTHERROR') as errortype  from import_product where (not((width regexp '^(0|([0-9]*[1-9][0-9]*))$')))and (filename='"+filename+"')"
						+" union "
						+"select number,pc_id,p_name,catalog_id,unit_name,unit_price,length,width,heigth,weight,union_flag,alive,volume,filename,coalesce(errortype,'HEIGTHERROR') as errortype  from import_product where (not((heigth regexp '^(0|([0-9]*[1-9][0-9]*))$')))and (filename='"+filename+"')";
						
				String realSql = "";
				String[] sqls = sql.split(" union ");
				
				DBRow[] error = null;
				for (int i = 0; i < sqls.length; i++) 
				{
					realSql = sqls[i]+" order by number";
					error = dbUtilAutoTran.selectMutliple(realSql);
					
					if(error !=null&&error.length>0)
					{
						break;
					}
				}
			
			return error;
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ checkImportProduct error:"+e);
		}
	}
	
	/**
	 * 检查导入套装关系，返回错误的套装关系（不包括套装条码本身重复）
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkImportProductUnion(String filename)
		throws Exception
	{
		try 
		{
			String sql ="select number,set_pid,pid,quantity,product,accessories,filename,coalesce(errortype,'ACCESSORISEMPTY') as errortype from import_product_union where ((isnull(accessories) or (accessories = '')))and (filename='"+filename+"')"
						+" union  "
						+"select number,set_pid,pid,quantity,product,accessories,filename,coalesce(errortype,'ACCESSORISNOEXIST') as errortype from import_product_union where (pid = 0)and(filename='"+filename+"')"
						+" union  "
						+"select ipu.number,ipu.set_pid,ipu.pid,ipu.quantity,ipu.product,ipu.accessories,ipu.filename,coalesce(ipu.errortype,'ACCESSORISREPEAT') as errortype from (SELECT	ipu.*,count(pid) as counts from	import_product_union AS ipu WHERE filename = '"+filename+"' GROUP BY set_pid,pid order by number asc) as ipu where counts>1"
						+" union  "
						+"select ipu.number,ipu.set_pid,ipu.pid,ipu.quantity,ipu.product,ipu.accessories,ipu.filename,coalesce(ipu.errortype,'CANNOTACCESSORISR') as errortype from import_product_union AS ipu WHERE filename='"+filename+"' and pid in (SELECT set_pid FROM import_product_union where filename='"+filename+"' union SELECT set_pid FROM product_union)"
						+" union  "
						+"select ipu.number,ipu.set_pid,ipu.pid,ipu.quantity,ipu.product,ipu.accessories,ipu.filename,coalesce(ipu.errortype,'CANNOTPRODUCT') as errortype from import_product_union AS ipu WHERE filename='"+filename+"' and set_pid in (SELECT pid FROM import_product_union where filename='"+filename+"' union SELECT pid FROM product_union)"
						+" union  "
						+"select number,set_pid,pid,quantity,product,accessories,filename,coalesce(errortype,'PRODUCTEMPTY') as errortype from import_product_union where ((isnull(product) or (product = '')))and(filename='"+filename+"')"
						+" union  "
						+"select number,set_pid,pid,quantity,product,accessories,filename,coalesce(errortype,'PRODUCTNOEXIST') as errortype from import_product_union where (set_pid = 0)and(filename='"+filename+"')"
						+" union  "
						+"select number,set_pid,pid,quantity,product,accessories,filename,coalesce(errortype,'QUANTITYERROR') as errortype from import_product_union where (not((quantity regexp '^(([0-9]+.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*.[0-9]+)|([0-9]*[1-9][0-9]*))$')))and(filename='"+filename+"')"
						+" union  "
						+" select number,set_pid,pid,quantity,product,accessories,filename,errortype from (select number,set_pid,pid,quantity,product,accessories,filename,coalesce(errortype,'SELFPRODUCTREPEAT') as errortype,(((max(number) - min(number)) + 1) - count(set_pid)) AS contrast from import_product_union where filename='"+filename+"' group by set_pid)as counts where contrast>0";

				
			String realSql = "";
			String[] sqls = sql.split(" union  ");
			
			DBRow[] error = null;
			for (int i = 0; i < sqls.length; i++) 
			{
				realSql = sqls[i]+" order by number";

				error = dbUtilAutoTran.selectMutliple(realSql);
				
				if(error !=null&&error.length>0)
				{
					break;
				}
			}
		
			return error;
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ checkImportProductUnion error:"+e);
		}
	}
	
	/**
	 * 检查导入商品套装关系，是否套装条码重复
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkImportProductUnionSelfProduct(String filename)
		throws Exception
	{
		try 
		{
			String sql = "select * from (select set_pid,product,accessories,max(number) AS maxnumber,min(number) AS minnumber,count(set_pid) AS counts,(((max(number) - min(number)) + 1) - count(set_pid)) AS contrast,filename,coalesce(errortype,'SELFPRODUCTREPEAT') as errortype from "+ConfigBean.getStringValue("import_product_union")+" where filename='"+filename+"' group by set_pid)as counts where contrast>0";
			
			DBRow para = new DBRow();
			para.add("filename",filename);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ checkImportProductUnionSelfProduct error:"+e);
		}
	}
	
	/**
	 * 根据文件名获得导入文件的商品信息
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getImportProductByFilename(String filename)
		throws Exception
	{
		try 
		{
			String sql = "select ip.*,pcode_main.p_code as p_code,pcode_amazon.p_code as p_code2,pcode_upc.p_code as upc from "+ConfigBean.getStringValue("import_product")+" as ip "
						+"join import_product_code as pcode_main on ip.pc_id = pcode_main.pc_id and ip.filename = pcode_main.filename and pcode_main.code_type = "+CodeTypeKey.Main+" "
						+"left join import_product_code as pcode_amazon on ip.pc_id = pcode_amazon.pc_id and ip.filename = pcode_amazon.filename and pcode_main.code_type = "+CodeTypeKey.Amazon+" "
						+"left join import_product_code as pcode_upc on ip.pc_id = pcode_upc.pc_id and ip.filename = pcode_upc.filename and pcode_main.code_type = "+CodeTypeKey.UPC+" "
						+"where ip.filename = ? ";
			DBRow para = new DBRow();
			para.add("filename",filename);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ getImportProductByFilename error:"+e);
		}
	}
	
	/**
	 * 根据ipa_id获得商品导入数据
	 * @param ipa_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getImportProductByIpaId(long ipa_id)
		throws Exception
	{
		try 
		{
			String sql = "select ip.*,pcode_main.p_code as p_code,pcode_amazon.p_code as p_code2,pcode_upc.p_code as upc from "+ConfigBean.getStringValue("import_product")+" as ip "
						+"join import_product_code as pcode_main on ip.pc_id = pcode_main.pc_id and ip.filename = pcode_main.filename and pcode_main.code_type = "+CodeTypeKey.Main+" "
						+"left join import_product_code as pcode_amazon on ip.pc_id = pcode_amazon.pc_id and ip.filename = pcode_amazon.filename and pcode_main.code_type = "+CodeTypeKey.Amazon+" "
						+"left join import_product_code as pcode_upc on ip.pc_id = pcode_upc.pc_id and ip.filename = pcode_upc.filename and pcode_main.code_type = "+CodeTypeKey.UPC+" "
						+"where ip.ipa_id = ? ";
			DBRow para = new DBRow();
			para.add("ipa_id",ipa_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ getImportProductByFilename error:"+e);
		}
	}
	/**
	 * 根据文件名获得导入文件商品组合信息
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getImportProductUnionByFilename(String filename)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("import_product_union")+" where filename = ?";
			
			DBRow para = new DBRow();
			para.add("filename",filename);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ getImportProductUnionByFilename error:"+e);
		}
	}
	
	/**
	 * 为了删除需更新的关系
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getImportProductUnionSetPidByFilename(String filename)
		throws Exception
	{
		try 
		{
			String sql = "select set_pid from "+ConfigBean.getStringValue("import_product_union")+" where filename = ? group by set_pid";
			
			DBRow para = new DBRow();
			para.add("filename",filename);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ getImportProductUnionSetPidByFilename error:"+e);
		}
	}
	
	/**
	 * 根据IpaId获得导入套装关系
	 * @param ipa_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getImportProductUnionSetPidByIpaId(long ipa_id)
		throws Exception
	{
		try 
		{
			String sql = "select set_pid,product from "+ConfigBean.getStringValue("import_product_union")+" where ipa_id = ? group by set_pid";
			
			DBRow para = new DBRow();
			para.add("ipa_id",ipa_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ getImportProductUnionSetPidByIpaId error:"+e);
		}
}
	
	/**
	 * 获得一个套装商品的关系
	 * @param filename
	 * @param set_pid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getImportProductUnionByFilenameWithSetpid(String filename,long set_pid)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("import_product_union")+" where filename = ? and set_pid = ?";
			
			DBRow para = new DBRow();
			para.add("filename",filename);
			para.add("set_pid",set_pid);
			
			return  dbUtilAutoTran.selectPreMutliple(sql,para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ getImportProductUnionByFilenameWithSetpid error:"+e);
		}
	}
	
	/**
	 * 获得一个导入的套装商品的关系
	 * @param ipa_id
	 * @param set_pid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getImportProductUnionByIpaIdWithSetpid(long ipa_id,long set_pid)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("import_product_union")+" where ipa_id = ? and set_pid = ?";
			
			DBRow para = new DBRow();
			para.add("ipa_id",ipa_id);
			para.add("set_pid",set_pid);
			
			return  dbUtilAutoTran.selectPreMutliple(sql,para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ getImportProductUnionByFilenameWithSetpid error:"+e);
		}
	}
	
	/**
	 * 添加临时条码
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addImportProductCode(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return (dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("import_product_code"),dbrow));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ addImportProductCode error:"+e);
		}
	}
	
	public DBRow[] getImportProductCodeByPcid(long pc_id,String filename)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("import_product_code")+" where pc_id = ? and filename = ? order by code_type asc";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("filename",filename);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ getImportProductCodeByPcid error:"+e);
		}
	}
	
	/**
	 * 删除全部导入信息
	 * @throws Exception
	 */
	public void delAllImportDB()
		throws Exception
	{
		dbUtilAutoTran.delete("where ipa_id=0",ConfigBean.getStringValue("import_product_union"));
		dbUtilAutoTran.delete("where ipa_id=0", ConfigBean.getStringValue("import_product"));
		dbUtilAutoTran.delete("where ipa_id=0", ConfigBean.getStringValue("import_product_code"));
	}
	
	/**
	 * 申请审核后将导入临时表内的信息标记已申请审核，定时任务就不删对应的数据了
	 * @param filename
	 * @param ipa_id
	 * @throws Exception
	 */
	public void hasProductApprove(String filename,long ipa_id)
		throws Exception
	{
		DBRow para = new DBRow();
		para.add("ipa_id",ipa_id);
		
		dbUtilAutoTran.update("where filename='"+filename+"'",ConfigBean.getStringValue("import_product"),para);
		dbUtilAutoTran.update("where filename='"+filename+"'",ConfigBean.getStringValue("import_product_union"),para);
		dbUtilAutoTran.update("where filename='"+filename+"'",ConfigBean.getStringValue("import_product_code"),para);
	}
	
	/**
	 * 导入文件申请审核
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addImportProductApprove(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return (dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("import_product_approve"),dbrow));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ addImportProductApprove error:"+e);
		}
	}
	
	/**
	 * 修改文件导入审核
	 * @param ipa_id
	 * @param para
	 * @throws Exception
	 */
	public void modImportProductApprove(long ipa_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where ipa_id = "+ipa_id,ConfigBean.getStringValue("import_product_approve"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ modImportProductApprove error:"+e);
		}
	}
	
	/**
	 * 过滤文件上传修改商品审核
	 * @param import_adid
	 * @param approve_adid
	 * @param sort_coulme
	 * @param sort_type
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] filterImportProductApprove(long import_adid,long approve_adid,String sort_coulme,String sort_type,PageCtrl pc)
		throws Exception
	{
		try {
			String importWhere = "";
			if(import_adid>0)
			{
				importWhere = " and import_adid = "+import_adid+" ";
			}
			
			String approveWhere = "";
			if(approve_adid>0)
			{
				approveWhere = " and approve_adid = "+approve_adid+" ";
			}

			String orderby = "";
			if(!sort_coulme.equals(""))
			{
				orderby = " order by "+sort_coulme+" "+sort_type;
			}
			else
			{
				orderby = " order by ipa_id desc";
			}
			
			String sql = "select * from "+ConfigBean.getStringValue("import_product_approve")+" where 1=1"
						+importWhere+approveWhere+orderby;
			
			return (dbUtilAutoTran.selectMutliple(sql, pc));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorImportProductMgrZJ filterImportProductApprove error:"+e);
		}
	}
	
	/**
	 * 获得导入审核
	 * @param ipa_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailImportProductApprove(long ipa_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("import_product_approve")+" where ipa_id = ?";
			
			DBRow para = new DBRow();
			para.add("ipa_id",ipa_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception();
		}
	}
	
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
