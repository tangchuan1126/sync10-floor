package com.cwc.app.floor.api.zr;

import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorTempContainerMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	private static String tableFix = "temp_" ;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	/**
	 * ͨ��������ŵõ�������Ϣ
	 * @param number
	 * @return
	 * @throws Exception
	 * ���titleName(���)
	 */
	public DBRow findContainerByNumber(String number) throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue(tableFix+"container") + " as c " 
						+"left join title on title.title_id = c.title_id where container = '" + number + "'";
			
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZyj.findContainerByNumber(String number):"+e);
		}
	}
	/**
	 * ����ϵͳ�����Container (CBIT)�� ����android���TLP
	 * @param insertRow
	 * @return
	 * @throws Exception
	 */
	public long addTempContainer(DBRow insertRow) throws Exception {
		try{
			ContainerTypeKey containerTypeKey = new ContainerTypeKey();
		      String tableName = ConfigBean.getStringValue("temp_container");
		      //long containerId = this.dbUtilAutoTran.getSequance(ConfigBean.getStringValue("container"));
		      //insertRow.add("con_id", containerId);
		      
		      String containerName = insertRow.getString("container");
		      String fixPre = containerTypeKey.getContainerTypeKeyValue(insertRow.get("container_type", 0));
		      if (!StrUtil.isBlank(containerName)) {
		        containerName = "TLP" + containerName;
		        insertRow.add("container", containerName);
		      }
		      
		      long containerId = this.dbUtilAutoTran.insertReturnId(tableName, insertRow);
		      
		      if (StrUtil.isBlank(containerName)) {
		          containerName = fixPre + containerId;
		          
		          DBRow updateData = new DBRow();
		          updateData.add("container", containerName);
		          
		          updateContainer(containerId, updateData);
		      }  	  
			return containerId ;
 			
		}catch(Exception e){
			throw new Exception("FloorTempContainerMgrZr.addTempContainer(insertRow):"+e);
		}
	}
	public void copyToTempContainer(DBRow copyRow) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue(tableFix+"container");
	 
			dbUtilAutoTran.insert(tableName, copyRow);
  			
		}catch(Exception e){
			throw new Exception("FloorTempContainerMgrZr.copyToTempContainer(insertRow):"+e);
		}
	}
	
	public void delTempContainer(long temp_con_id)
			throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where con_id = "+temp_con_id,ConfigBean.getStringValue("temp_container"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTempContainerMgrZr delTempContainer error:"+e);
		}
	}
	
	public void deleteContainerProduct(long cp_lp_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue(tableFix+"container_product");
			dbUtilAutoTran.delete(" where cp_lp_id="+cp_lp_id, tableName);
		}catch (Exception e) {
			 throw new Exception("FloorTempContainerMgrZr.deleteContainerProduct(cp_lp_id):"+e);
		}
	}
	public void addContainerLoading(DBRow data) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue(tableFix+"container_loading");
			dbUtilAutoTran.insertReturnId(tableName, data);
 		}catch (Exception e) {
			 throw new Exception("FloorTempContainerMgrZr.addContainerLoading(data):"+e);
		}
	}
	public void deleteContainerLoading(long con_id , long parent_con_id ) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue(tableFix+"container_loading");
			dbUtilAutoTran.delete(" where  con_id = "+con_id+" and parent_con_id="+parent_con_id, tableName);
  		}catch (Exception e) {
			 throw new Exception("FloorTempContainerMgrZr.addContainerLoading(con_id , parent_con_id):"+e);
		}
	}
	
	public void updateContainer(long container_id , DBRow updateRow) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue(tableFix+"container");
			dbUtilAutoTran.update(" where con_id="+container_id, tableName, updateRow);
		}catch (Exception e) {
			 throw new Exception("FloorTempContainerMgrZr.updateContainer(container_id , updateRow):"+e);
		}
	}
	public void addChildListItem(DBRow row)throws Exception{
		try{
			 dbUtilAutoTran.insert(ConfigBean.getStringValue(tableFix+"container_loading_child_list"), row);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.addChildListItem(row):"+e);
		}
	}	
	public DBRow[] getChildListBySearchRootId(long search_root_id) throws Exception{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append(" select  * from ")
			.append( ConfigBean.getStringValue(tableFix+"container_loading_child_list")).append(" where search_root_con_id =").append(search_root_id);
			return dbUtilAutoTran.selectMutliple(sql.toString());
 		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getChildListBySearchRootId(search_root_id):"+e);
		}
	}
	public void deleteChildListBySearchRootId(long search_root_id) throws Exception{
		try{
			dbUtilAutoTran.delete(" where search_root_con_id="+search_root_id, ConfigBean.getStringValue(tableFix+"container_loading_child_list"));
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.deleteChildListBySearchRootId(search_root_id):"+e);
		}
	}
	public void addContainerProduct(DBRow row) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue(tableFix+"container_product");
			dbUtilAutoTran.insert(tableName, row);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.addContainerProduct(row):"+e);
		}
	}
	public DBRow getContainer(long con_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue(tableFix+"container");
			return dbUtilAutoTran.selectSingle("select * from " + tableName+  " where con_id = " + con_id )  ; 
 		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getContainer(con_id):"+e);
		}
	}}
