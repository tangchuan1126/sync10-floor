package com.cwc.app.floor.api;

import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorAdminMgr {
	private DBUtilAutoTran dbUtilAutoTran;
	
	public long addAdmin(DBRow row) throws Exception {
		try {
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("admin"));
			row.add("adid", id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("admin"), row);
			return (id);
		} catch (Exception e) {
			throw new Exception("addAdmin(row) error:" + e);
		}
	}
	
	public void delAdmin(long adid) throws Exception {
		try {
			
			dbUtilAutoTran.delete("where adid=" + adid, ConfigBean.getStringValue("admin"));
		} catch (Exception e) {
			throw new Exception("delAdmin(row) error:" + e);
		}
	}
	
	public void modifyAdmin(long adid, DBRow row) throws Exception {
		try {
			
			dbUtilAutoTran.update("where adid=" + adid, ConfigBean.getStringValue("admin"), row);
		} catch (Exception e) {
			throw new Exception("modifyAdmin(row) error:" + e);
		}
	}
	
	public DBRow getDetailAdmin(long adid) throws Exception {
		try {
			
			DBRow row = dbUtilAutoTran.selectSingle("select aw.warehouse_id as ps_id , admin.*   from admin LEFT JOIN admin_warehouse as aw on admin.adid = aw.adid where admin.adid ="+adid+"   GROUP BY admin.adid ");
			/*System.out.println("select * from " + ConfigBean.getStringValue("admin")
					+ " where adid=" + adid);*/
			return (row);
		} catch (Exception e) {
			throw new Exception("getDetailAdmin(row) error:" + e);
		}
	}
	
	public DBRow[] getAllAdmin(PageCtrl pc) throws Exception {
		try {
			
			DBRow row[];
			if (pc != null) {
				row = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("admin")
						+ " order by account asc", pc);
			} else {
				row = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("admin")
						+ " order by account asc");
			}
			
			return (row);
		} catch (Exception e) {
			throw new Exception("getAllAdmin() error:" + e);
		}
	}
	
	public DBRow[] getAllAdminGroup(PageCtrl pc) throws Exception {
		try {
			
			DBRow row[];
			if (pc != null) {
				row = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("admin_group")
						+ " order by adgid desc", pc);
			} else {
				row = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("admin_group")
						+ " order by adgid desc");
			}
			
			return (row);
		} catch (Exception e) {
			throw new Exception("getAllAdminGroup() error:" + e);
		}
	}
	
	public DBRow getDetailAdminGroup(long adgid) throws Exception {
		try {
			
			DBRow row = dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("admin_group")
					+ " where adgid=" + adgid);
			return (row);
		} catch (Exception e) {
			throw new Exception("getDetailAdminGroup() error:" + e);
		}
	}
	
	public DBRow getDetailAdminByAccount(String account) throws Exception {
		try {
			String sql = "select ps.title,ad.* from " + ConfigBean.getStringValue("admin") + " as ad " + " left join "
					+ ConfigBean.getStringValue("product_storage_catalog") + " as ps on ad.ps_id = ps.id "
					+ " where account=?";
			
			DBRow para = new DBRow();
			para.add("account", account);
			DBRow row = dbUtilAutoTran.selectPreSingle(sql, para);
			return (row);
		} catch (Exception e) {
			throw new Exception("getDetailAdminByAccount() error:" + e);
		}
	}
	
	public DBRow[] getDetailAdminByAccountOrEmployeName(String account) throws Exception {
		try {
			
			DBRow para = new DBRow();
			para.add("account", account);
			para.add("employe_name", account);
			DBRow[] row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("admin")
					+ " where account=? or employe_name =?", para);
			return (row);
		} catch (Exception e) {
			throw new Exception("getDetailAdminByAccount() error:" + e);
		}
	}
	
	public DBRow[] getPermitActionByAdgid(long adgid) throws Exception {
		try {
			
			DBRow para = new DBRow();
			para.add("adgid", adgid);
			DBRow row[] = dbUtilAutoTran
					.selectPreMutliple(
							"select * from "
									+ ConfigBean.getStringValue("admin_group_auth")
									+ " admin_group_auth,"
									+ ConfigBean.getStringValue("authentication_action")
									+ " authentication_action where admin_group_auth.adgid=? and admin_group_auth.ataid=authentication_action.ataid",
							para);
			return (row);
		} catch (Exception e) {
			throw new Exception("getPermitActionByAdgid() error:" + e);
		}
	}
	
	public DBRow[] getControlTreeByParentid(long parentid) throws Exception {
		try {
			DBRow para = new DBRow();
			para.add("parentid", parentid);
			
			String sql = "select * from " + ConfigBean.getStringValue("control_tree")
					+ " where parentid=? order by id asc";
			
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql, para);
			
			return (row);
		} catch (Exception e) {
			throw new Exception("getControlTreeByParentid() error:" + e);
		}
	}
	
	public DBRow[] getControlTreeByParentidOrderBySort(long parentid, int control_type) throws Exception {
		try {
			DBRow para = new DBRow();
			para.add("parentid", parentid);
			para.add("control_type", control_type);
			
			String sql = "select * from " + ConfigBean.getStringValue("control_tree")
					+ " where parentid=? and control_type = ? order by sort asc";
			
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql, para);
			
			return (row);
		} catch (Exception e) {
			throw new Exception("getControlTreeByParentidOrderBySort() error:" + e);
		}
	}
	
	public DBRow[] getControlTreeAllByControlTypeSort(int control_type) throws Exception {
		String sql = "select * from " + ConfigBean.getStringValue("control_tree") + " where 1=? ";
		
		DBRow para = new DBRow();
		para.add("1", 1);
		
		if (control_type != 0) {
			sql += " and control_type = ? ";
			para.add("control_type", control_type);
		}
		
		sql += " order by parentid asc,sort asc";
		
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	public DBRow getDetailAuthenticationActionByAction(String action) throws Exception {
		try {
			
			DBRow para = new DBRow();
			para.add("action", action);
			
			DBRow row = dbUtilAutoTran.selectPreSingle(
					"select * from " + ConfigBean.getStringValue("authentication_action") + " where action=?", para);
			return (row);
		} catch (Exception e) {
			throw new Exception("getDetailAuthenticationActionByAction(row) error:" + e);
		}
	}
	
	public DBRow[] getControlTreeByAdgid(long adgid) throws Exception {
		try {
			
			DBRow para = new DBRow();
			para.add("adgid", adgid);
			
			String sql = "select * from " + ConfigBean.getStringValue("control_tree") + ","
					+ ConfigBean.getStringValue("control_tree_map") + " where adgid=? and ctid=id order by id asc";
			
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql, para);
			
			return (row);
		} catch (Exception e) {
			throw new Exception("getControlTreeByAdgid() error:" + e);
		}
	}
	
	public DBRow getDetailControlTreeByLink(String link) throws Exception {
		try {
			
			DBRow para = new DBRow();
			para.add("link", link);
			
			DBRow row = dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("control_tree")
					+ " where link=?", para);
			return (row);
		} catch (Exception e) {
			throw new Exception("getDetailControlTreeByLink(row) error:" + e);
		}
	}
	
	public DBRow getDetailControlTreeById(long id) throws Exception {
		try {
			DBRow para = new DBRow();
			para.add("id", id);
			
			DBRow row = dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("control_tree")
					+ " where id=?", para);
			return (row);
		} catch (Exception e) {
			throw new Exception("getDetailControlTreeById(row) error:" + e);
		}
	}
	
	public DBRow[] getPermitPageAdgid(String page) throws Exception {
		try {
			
			DBRow para = new DBRow();
			para.add("page", page);
			
			String sql = "select ctm.adgid adgid from " + ConfigBean.getStringValue("control_tree") + " ct,"
					+ ConfigBean.getStringValue("control_tree_map") + " ctm where ct.link=? and ct.id=ctm.ctid";
			
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql, para);
			
			return (row);
		} catch (Exception e) {
			throw new Exception("getPermitPageAdgid() error:" + e);
		}
	}
	
	public DBRow[] getPermitPageAdid(String page) throws Exception {
		try {
			
			DBRow para = new DBRow();
			para.add("page", page);
			
			String sql = "select ctme.adid adid from " + ConfigBean.getStringValue("control_tree") + " ct,"
					+ ConfigBean.getStringValue("control_tree_map_extend")
					+ " ctme where ct.link=? and ct.id=ctme.ctid";
			
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql, para);
			
			return (row);
		} catch (Exception e) {
			throw new Exception("getPermitPageAdid() error:" + e);
		}
	}
	
	public DBRow[] getPermitActionAdgid(String action) throws Exception {
		try {
			
			DBRow para = new DBRow();
			para.add("action", action);
			
			String sql = "select aga.adgid adgid from " + ConfigBean.getStringValue("authentication_action") + " aua,"
					+ ConfigBean.getStringValue("admin_group_auth") + " aga where aua.action=? and aua.ataid=aga.ataid";
			
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql, para);
			
			return (row);
		} catch (Exception e) {
			throw new Exception("getPermitActionAdgid() error:" + e);
		}
	}
	
	public DBRow[] getPermitActionAdid(String action) throws Exception {
		try {
			
			DBRow para = new DBRow();
			para.add("action", action);
			
			String sql = "select agae.adid adid from " + ConfigBean.getStringValue("authentication_action") + " aua,"
					+ ConfigBean.getStringValue("admin_group_auth_extend")
					+ " agae where aua.action=? and aua.ataid=agae.ataid";
			
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql, para);
			
			return (row);
		} catch (Exception e) {
			throw new Exception("getPermitActionAdid() error:" + e);
		}
	}
	
	public long addRole(DBRow row) throws Exception {
		try {
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("admin_group"));
			
			row.add("adgid", id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("admin_group"), row);
			return (id);
		} catch (Exception e) {
			throw new Exception("addRole(row) error:" + e);
		}
	}
	
	public void modifyRole(long adgid, DBRow row) throws Exception {
		try {
			dbUtilAutoTran.update("where adgid=" + adgid, ConfigBean.getStringValue("admin_group"), row);
		} catch (Exception e) {
			throw new Exception("modifyRole(row) error:" + e);
		}
	}
	
	public long addRoleControlPageMap(DBRow row) throws Exception {
		try {
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("control_tree_map"));
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("control_tree_map"), row);
			return (id);
		} catch (Exception e) {
			throw new Exception("addRoleControlPageMap(row) error:" + e);
		}
	}
	
	public long addAdminControlPageMap(DBRow row) throws Exception {
		try {
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("control_tree_map_extend"));
			dbUtilAutoTran.insert(ConfigBean.getStringValue("control_tree_map_extend"), row);
			return (id);
		} catch (Exception e) {
			throw new Exception("addAdminControlPageMap(row) error:" + e);
		}
	}
	
	public long addRoleActionMap(DBRow row) throws Exception {
		try {
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("admin_group_auth"));
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("admin_group_auth"), row);
			return (id);
		} catch (Exception e) {
			throw new Exception("addRoleActionMap(row) error:" + e);
		}
	}
	
	public long addAdminActionMap(DBRow row) throws Exception {
		try {
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("admin_group_auth_extend"));
			dbUtilAutoTran.insert(ConfigBean.getStringValue("admin_group_auth_extend"), row);
			return (id);
		} catch (Exception e) {
			throw new Exception("addAdminActionMap(row) error:" + e);
		}
	}
	
	public void delRole(long adgid) throws Exception {
		try {
			dbUtilAutoTran.delete("where adgid=" + adgid, ConfigBean.getStringValue("admin_group"));
		} catch (Exception e) {
			throw new Exception("delRole(row) error:" + e);
		}
	}
	
	public void delRoleControlPageMapByAdgid(long adgid) throws Exception {
		try {
			dbUtilAutoTran.delete("where adgid=" + adgid, ConfigBean.getStringValue("control_tree_map"));
		} catch (Exception e) {
			throw new Exception("delRoleControlPageMapByAdgid(row) error:" + e);
		}
	}
	
	public void delAdminControlPageMapByAdid(long adid) throws Exception {
		try {
			dbUtilAutoTran.delete("where adid=" + adid, ConfigBean.getStringValue("control_tree_map_extend"));
		} catch (Exception e) {
			throw new Exception("delAdminControlPageMapByAdid(row) error:" + e);
		}
	}
	
	public void delRoleActionMapByAdgid(long adgid) throws Exception {
		try {
			dbUtilAutoTran.delete("where adgid=" + adgid, ConfigBean.getStringValue("admin_group_auth"));
		} catch (Exception e) {
			throw new Exception("delRoleActionMapByAdgid(row) error:" + e);
		}
	}
	
	public void delAdminActionMapByAdid(long adid) throws Exception {
		try {
			dbUtilAutoTran.delete("where adid=" + adid, ConfigBean.getStringValue("admin_group_auth_extend"));
		} catch (Exception e) {
			throw new Exception("delAdminActionMapByAdid(row) error:" + e);
		}
	}
	
	public DBRow getDetailRoleByName(String name) throws Exception {
		try {
			
			DBRow para = new DBRow();
			para.add("name", name);
			DBRow row = dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("admin_group")
					+ " where name=?", para);
			return (row);
		} catch (Exception e) {
			throw new Exception("getDetailRoleByName() error:" + e);
		}
	}
	
	public DBRow getDetailRoleByAdgid(long adgid) throws Exception {
		try {
			
			DBRow row = dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("admin_group")
					+ " where adgid=" + adgid);
			return (row);
		} catch (Exception e) {
			throw new Exception("getDetailRoleByAdgid() error:" + e);
		}
	}
	
	public DBRow[] getActionsByPage(PageCtrl pc, long id) throws Exception {
		try {
			DBRow para = new DBRow();
			para.add("page", id);
			
			DBRow row[];
			
			if (pc != null) {
				row = dbUtilAutoTran.selectPreMutliple(
						"select * from " + ConfigBean.getStringValue("authentication_action") + " where page=?", para,
						pc);
			} else {
				row = dbUtilAutoTran.selectPreMutliple(
						"select * from " + ConfigBean.getStringValue("authentication_action") + " where page=?", para);
			}
			
			return (row);
		} catch (Exception e) {
			throw new Exception("getActionsByPage() error:" + e);
		}
	}
	
	public DBRow[] getAdminByAdgid(long adgid, PageCtrl pc) throws Exception {
		try {
			DBRow para = new DBRow();
			para.add("adgid", adgid);
			
			DBRow row[];
			if (pc != null) {
				row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("admin")
						+ " where adgid=? order by adid desc", para, pc);
			} else {
				row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("admin")
						+ " where adgid=? order by adid desc", para);
			}
			
			return (row);
		} catch (Exception e) {
			throw new Exception("getAdminByAdgid() error:" + e);
		}
	}
	
	/**
	 * 根据部门ID获取账号
	 * @param 部门ID
	 * @return 账号信息
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAccountByDeptId(long adgid) throws Exception {
		
		try {
			
			DBRow para = new DBRow();
			para.add("adgid", adgid);
			
			return dbUtilAutoTran.selectPreMutliple("select adid, "+adgid+" as adgid ,employe_name from admin where exists (select * from admin_department where admin.adid = admin_department.adid and department_id = ?) order by adid desc", para);
		
		} catch (Exception e) {
			
			throw new Exception("getAccountByDeptId() error:" + e);
		}
	}
	
	/**
	 * zyj 通过ataid或者ataScription查询用户权限
	 * 
	 * @param adgid
	 * @param adid
	 * @param ataid
	 * @param ataScription
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAdminAthAction(DBRow[] adgid, long adid, long ataid, String action) throws Exception {
		try {
			String sql1 = "select aa.* from admin_group_auth aga "
					+ " join authentication_action aa on aga.ataid = aa.ataid where 1=1 ";
			if (ataid > 0) {
				sql1 += " and aa.ataid = " + ataid;
			}
			if (!StrUtil.isBlank(action)) {
				sql1 += " and aa.action = '" + action + "'";
			}
			if (adgid.length > 0) {
				sql1 += " and (aga.adgid = " + adgid[0].get("adgid", 0L);
				for (int i = 1; i < adgid.length; i++) {
					sql1 += " or aga.adgid = " + adgid[i].get("adgid", 0L);
				}
				sql1 += ")";
			}
			String sql2 = "select * from turboshop_admin_group_auth_extend taa "
					+ " join authentication_action aa on taa.ataid = aa.ataid where 1=1 ";
			if (ataid > 0) {
				sql2 += " and aa.ataid = " + ataid;
			}
			if (ataid > 0) {
				sql2 += " and taa.adid = " + adid;
			}
			if (!StrUtil.isBlank(action)) {
				sql2 += " and aa.action = '" + action + "'";
			}
			// System.out.println("sql1:"+sql1);
			// System.out.println("sql2:"+sql2);
			DBRow[] result = dbUtilAutoTran.selectMutliple(sql1);
			if (result.length == 0) {
				result = dbUtilAutoTran.selectMutliple(sql2);
			}
			return result;
		} catch (Exception e) {
			throw new Exception("getAdminByAdgid() error:" + e);
		}
	}
	
	public DBRow[] findAdminRoleByAdid(long adid) throws Exception {
		String sql = "select * from admin_group_relation where adid = " + adid;
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * @author zhanjie
	 * @param adid 登录人ID
	 * @param control_type 需要的权限类型
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAdminControlTreeByAdid(long adid, int control_type) throws Exception {
		try {
			String sql = "select tct.id,tct.ico,tct.title,ad.adid from turboshop_control_tree as tct "
					+ "join turboshop_control_tree_map as tctm on tct.id = tctm.ctid "
					+ "join admin_group as ag on tctm.adgid = ag.adgid " + "join admin as ad on ad.adgid = ag.adgid "
					+ " where ad.adid = ? and tct.control_type = ? " + "union all "
					+ "select tct.id,tct.ico,tct.title,ad.adid from turboshop_control_tree as tct "
					+ "join turboshop_control_tree_map_extend as tctme on tct.id = tctme.ctid "
					+ "join admin as ad on tctme.adid = ad.adid " + "where ad.adid = ? and tct.control_type = ? ";
			
			DBRow para = new DBRow();
			para.add("adid", adid);
			para.add("type", control_type);
			para.add("exted_adid", adid);
			para.add("exted_type", control_type);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("FloorAdminMgr getAdminControlTreeByAdid error:" + e);
		}
	}
	
	/**
	 * 根据仓库id和分组id 查询人员列表 zwb
	 * 
	 * @param ps_id
	 * @param group_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAdminsByPsIdAndGId(long ps_id,long group_id)throws Exception{
		try{
			/*String sql="select * from admin where adgid="+group_id+" and ps_id="+ps_id+" and (proJsId=10 or proJsId=5)";*/
			String sql = " select * from admin as admin "
					+" where exists (select * from admin_department as adept where adept.adid = admin.adid and adept.department_id = "+group_id+" and adept.post_id in (5,10)) "
					+" and exists (select * from admin_warehouse as aware where admin.adid = aware.adid and aware.warehouse_id = "+ps_id+" ) ";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorAdminMgr getAdminsByPsIdAndGId error:"+e);
		}
	}
	
	/**
	 * 
	 * 根据员工名字模糊查询
	 * 
	 * @param ps_id
	 * @param group_id
	 * @return
	 * @throws Exception <b>Date:</b>2015年1月28日下午4:11:13<br>
	 * @author: cuicong
	 */
	public DBRow[] getAdminsEmployeName(String employe_name) throws Exception {
		try {
			String sql = "select * from admin where employe_name like '%" + employe_name + "%'";
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorAdminMgr getAdminsByPsIdAndGId error:" + e);
		}
	}
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
