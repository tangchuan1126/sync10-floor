package com.cwc.app.floor.api;

import static us.monoid.web.Resty.content;
import static us.monoid.web.Resty.put;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;
import us.monoid.web.JSONResource;
import us.monoid.web.Resty;
import us.monoid.web.Resty.Option;

import com.cwc.app.beans.log.FilterAccessLogBean;
import com.cwc.app.beans.log.FilterProductStoreAllocateLogBean;
import com.cwc.app.beans.log.FilterProductStoreLogBean;
import com.cwc.app.beans.log.FilterProductStorePhysicalLogBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class FloorLogFilterMgr implements FloorLogFilterIFace {
	private String restBaseUrl;

	public String getRestBaseUrl() {
		return restBaseUrl;
	}

	public void setRestBaseUrl(String restBaseUrl) {
		this.restBaseUrl = restBaseUrl;
	}

	private DBRow[] convertRows(JSONArray rows) throws Exception {
		DBRow[] dbRows = new DBRow[rows.length()];
		for (int i = 0; i < rows.length(); i++) {
			JSONObject row = rows.getJSONObject(i);
			dbRows[i] = convertRow(row);
		}
		// inspect(dbRows);

		return dbRows;
	}

	private DBRow convertRow(JSONObject row) throws Exception {
		DBRow dbRow = new DBRow();
		for (Iterator<String> itr = row.keys(); itr.hasNext();) {
			String k = itr.next();

			if (k.equals("post_date")) {
				// 时间格式转换
				String v = row.getString(k);
				v = v.replace('T', ' ').substring(0, 19);
				dbRow.add(k, v);
			} else if (row.get(k) instanceof JSONArray) {
				JSONArray a = (JSONArray) row.get(k);
				Object[] oa = new Object[a.length()];
				for (int j = 0; j < a.length(); j++) {
					oa[j] = a.get(j);
				}
				dbRow.addObject(k, oa);
			} else if(k.equals("_id")) {
				dbRow.add("id", row.getString(k));
			} else if (!k.startsWith("_")) {
				dbRow.addObject(k, row.get(k));
			}
		}// for
		return dbRow;
	}

	private DBRow[] invokeRest(Object bean, PageCtrl pageCtrl,
			String start_date, String end_date, Integer onGroup,
			Integer in_or_out) throws Exception {
		JSONObject fieldsJSON = new JSONObject(bean);
		JSONObject pcJSON = new JSONObject();
		pcJSON.put("pageNo", pageCtrl.getPageNo());
		pcJSON.put("pageSize", pageCtrl.getPageSize());
		JSONObject reqJSON = new JSONObject();
		reqJSON.put("fields", fieldsJSON);
		reqJSON.put("page_ctrl", pcJSON);
		if (onGroup != null)
			reqJSON.put("on_group", onGroup);
		if (in_or_out != null)
			reqJSON.put("in_or_out", in_or_out);
		reqJSON.put("start_date", start_date);
		reqJSON.put("end_date", end_date);

		String restUrl = restUrlFor(bean);
		JSONResource result = new Resty(Option.timeout(1000000)).json(restUrl,
				put(content(reqJSON)));
		// System.out.println(result.toObject().toString(4));

		pcJSON = (JSONObject) result.get("page_ctrl");
		pageCtrl.setAllCount(pcJSON.getInt("allCount"));
		pageCtrl.setRowCount(pcJSON.getInt("rowCount"));
		pageCtrl.setPageCount(pcJSON.getInt("pageCount"));

		JSONArray rows = (JSONArray) result.get("rows");

		return convertRows(rows);
	}

	private DBRow invokeRest(String modelName, String id) throws Exception {

		String restUrl = restBaseUrl + "/log_mgr/" + modelName + "/" + id;
		JSONResource result = new Resty(Option.timeout(1000000)).json(restUrl);

		return convertRow(result.object());
	}
	
	//ADD BY CHENCHEN
	private DBRow[] invokeRest(String modelName, JSONObject queryJson,	PageCtrl pageCtrl, 
			String start_date, String end_date, Integer onGroup, Integer in_or_out) throws Exception {
		JSONObject pcJSON = new JSONObject();
		pcJSON.put("pageNo", pageCtrl.getPageNo());
		pcJSON.put("pageSize", pageCtrl.getPageSize());
		JSONObject reqJSON = new JSONObject();
		reqJSON.put("fields", queryJson);
		reqJSON.put("page_ctrl", pcJSON);
		if (onGroup != null)
			reqJSON.put("on_group", onGroup);
		if (in_or_out != null)
			reqJSON.put("in_or_out", in_or_out);
		reqJSON.put("start_date", start_date);
		reqJSON.put("end_date", end_date);

		String restUrl = restBaseUrl + "/log_filter/" + modelName;
		JSONResource result = new Resty(Option.timeout(1000000)).json(restUrl,
				put(content(reqJSON)));
		// System.out.println(result.toObject().toString(4));

		pcJSON = (JSONObject) result.get("page_ctrl");
		pageCtrl.setAllCount(pcJSON.getInt("allCount"));
		pageCtrl.setRowCount(pcJSON.getInt("rowCount"));
		pageCtrl.setPageCount(pcJSON.getInt("pageCount"));

		JSONArray rows = (JSONArray) result.get("rows");

		return convertRows(rows);
	}

	

	@Override
	public DBRow[] getSearchProductStoreSysLogs(String start_date,
			String end_date,
			FilterProductStoreLogBean filterProductStoreLogBean, int onGroup,
			int in_or_out, PageCtrl pageCtrl) throws Exception {
		return invokeRest(filterProductStoreLogBean, pageCtrl, start_date,
				end_date, onGroup, in_or_out);
	}

	private String restUrlFor(Object queryBean) {
		int i1 = queryBean.getClass().getName().lastIndexOf("Filter");
		int i2 = queryBean.getClass().getName().lastIndexOf("Bean");
		String modelName = queryBean.getClass().getName().substring(i1 + 6, i2);
		return restBaseUrl + "/log_filter/" + modelName;
	}

	private void inspect(DBRow[] rows) throws Exception {
		JSONArray a = new JSONArray();
		for (int i = 0; i < rows.length; i++) {
			List<String> keys = rows[i].getFieldNames();
			JSONObject row = new JSONObject();
			for (String k : keys) {
				row.put(k, rows[i].getValue(k));
			}
			a.put(row);
		}
	}

	@Override
	public DBRow[] getSearchProductStoreAllocateLogs(
			String start_date,
			String end_date,
			FilterProductStoreAllocateLogBean filterProductStoreAllocateLogBean,
			PageCtrl pageCtrl) throws Exception {
		return invokeRest(filterProductStoreAllocateLogBean, pageCtrl,
				start_date, end_date, null, null);
	}

	@Override
	public DBRow[] getSearchProductStorePhysicalLogs(
			String start_date,
			String end_date,
			FilterProductStorePhysicalLogBean filterProductStorePhysicalLogBean,
			PageCtrl pageCtrl) throws Exception {
		return invokeRest(filterProductStorePhysicalLogBean, pageCtrl,
				start_date, end_date, null, null);
	}

	@Override
	public DBRow[] getSearchAccessLog(String start_date, String end_date,
			FilterAccessLogBean filterAccessLogBean, PageCtrl pageCtrl)
			throws Exception {
		return invokeRest(filterAccessLogBean, pageCtrl, start_date, end_date,
				null, null);
	}

	@Override
	public DBRow getSearchDetailAccessLog(String id) throws Exception {
		return invokeRest("AccessLog", id);
	}

	@Override
	public DBRow getProductStorePhysicalLogLayUpBySN(long system_bill_id,
			int system_bill_type, String serial_number) throws Exception {
		FilterProductStorePhysicalLogBean logBean = new FilterProductStorePhysicalLogBean();
		logBean.setSystem_bill_id(system_bill_id);
		logBean.setSystem_bill_type(system_bill_type);
		logBean.setSerial_number(serial_number);
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(1);
		pc.setPageSize(2);
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String start_date="2014-01-01";
		String end_date=df.format(new Date());
		
		DBRow[] result = this.getSearchProductStorePhysicalLogs(start_date, end_date, logBean, pc);
		if(result == null || result.length == 0) return null;
		if(result.length > 1) throw new Exception(String.format(
				"getProductStorePhysicalLogLayUpBySN: Multiple rows found for[system_bill_id=%d, system_bill_type=%d, serial_number=%s]",
							system_bill_id,system_bill_type,serial_number));
		return result[0];
	}

	@Override
	public DBRow[] queryLogs(String modelName, JSONObject queryJson, PageCtrl pageCtrl, String start_date, String end_date)	throws Exception {
		return queryLogs(modelName, queryJson, pageCtrl, start_date, end_date, null, null);
	}

	@Override
	public DBRow[] queryLogs(String modelName, JSONObject queryJson,PageCtrl pageCtrl, String start_date, String end_date,Integer onGroup, Integer in_or_out) throws Exception {
		return invokeRest(modelName, queryJson, pageCtrl, start_date, end_date, onGroup, in_or_out);
	}

}
