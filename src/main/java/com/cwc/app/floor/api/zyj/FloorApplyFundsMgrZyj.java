package com.cwc.app.floor.api.zyj;

import com.cwc.app.key.ApplyStatusKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorApplyFundsMgrZyj {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBRow[] getApplyMoneyByAssociateTypeAndTypes()throws Exception
	{
		try {
			String sql = "select * from apply_money where association_type_id = 6 and types = 10015";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorApplyFundsMgrZyj.getApplyMoneyByAssociateTypeAndTypes():"+e);
		}
	}
	
	/**
	 * 通过资金的ID，获取此资金下申请转账的总金额
	 * @param apply_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getTotalApplyTransferByApplyId(long apply_id) throws Exception
	{
		try {
			String sql = "SELECT SUM(standard_money) as totalApplyTransfer FROM apply_transfer WHERE apply_money_id = " + apply_id;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorApplyFundsMgrZyj.getTotalApplyTransferByApplyId():"+e);
		}
	}
	
	/**
	 * 通过关联ID,关联类型ID,查询所有资金申请
	 * @param association_id
	 * @param association_type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApplyMoneyByAssociationIdAndAssociationTypeIdNoCancel(long association_id,int association_type_id) throws Exception
	{
	 	try
	 	{
			String sql = "select * from "+ConfigBean.getStringValue("apply_money")+" where `status` !="+ApplyStatusKey.CANCEL+" and association_id = ? and association_type_id =?";
			
			DBRow para = new DBRow();
			para.add("association_id",association_id);
			para.add("association_type_id",association_type_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
	 	catch (Exception e) 
	 	{
			throw new Exception("FloorApplyFundsMgrZyj.getApplyMoneyByAssociationIdAndAssociationTypeIdNoCancel error:"+e);
		}
	}
	
	public long insertApplyMoney(DBRow row) throws Exception
	{
		try 
		{
			long pkVal = dbUtilAutoTran.getSequance("apply_money");
			row.add("apply_id", pkVal);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("apply_money"), row);
			return pkVal;
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorApplyFundsMgrZyj.insertApplyMoney(DBRow row) error:"+e);
		}
	}
	/**
	 * 添加申请资金的文件
	 * @param row
	 * @throws Exception
	 */
	public void insertApplyMoneyFile(DBRow row) throws Exception
	{
		try 
		{
			long pkVal = dbUtilAutoTran.getSequance("apply_images");
			row.add("id", pkVal);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("apply_images"), row);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorApplyFundsMgrZyj.insertApplyMoneyFile(DBRow row) error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
