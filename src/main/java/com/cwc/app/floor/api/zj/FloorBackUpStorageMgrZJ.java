package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorBackUpStorageMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 添加套装拆散库存
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addBackUpStorage(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long back_up_storage_id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("back_up_storage"),dbrow);
			
			return back_up_storage_id;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorBackUpStorageMgrZJ addBackUpStorage error:"+e);
		}
	}
	
	public void delBackUpStorage(long back_up_storage_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where back_up_storage_id = "+back_up_storage_id,ConfigBean.getStringValue("back_up_storage"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorBackUpStorageMgrZJ delBackUpStorage error:"+e);
		}
	}
	
	public DBRow getDetailBackUpStore(long pc_id,long ps_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("back_up_storage")+" where pc_id = ? and ps_id = ?";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("ps_id",ps_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorBackUpStorageMgrZJ getDetailBackUpStore error:"+e);
		}
	}
	
	/**
	 * 添加拆分需求
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addSplitRequireDetail(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long srd_id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("split_require_detail"),dbrow);
			return srd_id;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorBackUpStorageMgrZJ addSplitRequireDetail error:"+e);
		}
	}
	
	public void modSplitRequireDetail(long srd_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where srd_id="+srd_id,ConfigBean.getStringValue("split_require_detail"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorBackUpStorageMgrZJ modSplitRequireDetail error:"+e);
		}
	}
	
	/**
	 * 根据出库单获得拆分要求
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSplitRequireDetailSplitAndToPcidByOut(long out_id)
		throws Exception
	{
		try 
		{
			String sql = "select distinct split_pc_id,to_pc_id from "+ConfigBean.getStringValue("split_require_detail")+" as srd "
						+"join waybill_order wo on srd.waybill_id = wo.waybill_id and wo.out_id = "+out_id+" "
						+"order by split_pc_id";
			//where srd.is_perform = 1 
			
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorBackUpStorageMgrZJ getSplitRequireDetailToPcid error:"+e);
		}
	}
	
	/**
	 * 根据出库单获得拆货需求
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSplitRequireDetailsByOut(long out_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("split_require_detail")+" as srd "
			+"join waybill_order wo on srd.waybill_id = wo.waybill_id and wo.out_id = "+out_id+" "
			+"order by split_pc_id";

			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorBackUpStorageMgrZJ getSplitRequireDetailsByOut error:"+e);
		}
	}
	
	/**
	 * 根据出库ID获得拆货要求的拆货商品与目标商品ID
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSplitRequireDetailToPcidByOut(long out_id)
		throws Exception
	{
		try 
		{
			String sql = "select distinct split_pc_id,to_pc_id from "+ConfigBean.getStringValue("split_require_detail")+" as srd "
						+"join waybill_order wo on srd.waybill_id = wo.waybill_id and wo.out_id = "+out_id+" "
						+"where srd.is_perform = 1 order by split_pc_id";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorBackUpStorageMgrZJ getSplitRequireDetailToPcid error:"+e);
		}
	}
	
	/**
	 * 查询拆散目标商品
	 * @param split_pc_id
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public long getToPcIdBySplitPcidAndOut(long split_pc_id,long out_id)
		throws Exception
	{
		String sql = "select distinct split_pc_id,to_pc_id from "+ConfigBean.getStringValue("split_require_detail")+" as srd "
		+"join waybill_order wo on srd.waybill_id = wo.waybill_id and wo.out_id = "+out_id+" "
		+"where srd.is_perform = 1 and srd.split_pc_id = "+split_pc_id;

		DBRow result = dbUtilAutoTran.selectSingle(sql); 
		
		if(result!=null)
		{
			return result.get("to_pc_id",0l);
		}
		else
		{
			return -1;
		}
		
		
	}
	
	
	/**
	 * 根据拆散商品与目标商品获得拆货要求
	 * @param split_pc_id
	 * @param to_pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSplitRequireDetailBySplitPcid(long split_pc_id,long to_pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("split_require_detail")+" where split_pc_id = ? and to_pc_id = ?";
			
			DBRow para = new DBRow();
			para.add("split_pc_id",split_pc_id);
			para.add("to_pc_id",to_pc_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorBackUpStorageMgrZJ getSplitRequireDetailBySplitPcid error:"+e);
		}
	}
	
	/**
	 * 根据运单号获得拆货要求
	 * @param waybill_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSplitRequireDetailByWaybillId(long waybill_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("split_require_detail")+" where waybill_id = ?";
			
			DBRow para = new DBRow();
			para.add("waybill_id",waybill_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorBackUpStorageMgrZJ getSplitRequireDetailByWaybillId error:"+e);
		}
	}
	
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
