package com.cwc.app.floor.api.tjh;

import java.sql.SQLException;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

/**
 * 
 * @author Administrator
 * 资产类别业务处理实现类
 */
public class FloorAssetsCategoryMgr{
	
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 获取所有的资产类别信息
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAssetsCategory() 
		throws Exception
	{
		
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("assets_category")+" where parentid = 0 or parentid is null";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAssetsCategory() error:"+e);
		}
	}
	
	/**
	 * 获取所有的产品线信息
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAssetsProductLine(long productLineId) 
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_line_define")+" where id ="+productLineId;
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAssetsCategory() error:"+e);
		}
	}
	/**
	 * 添加资产类别信息
	 * @throws Exception
	 */
	public long addAssetsCategory(DBRow dbrow) 
		throws Exception
	{
		 try
		 {
			 long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("assets_category"));
			 dbrow.add("id", id);
			 
			 dbUtilAutoTran.insert(ConfigBean.getStringValue("assets_category"), dbrow);
			 return (id);
		 } 
		 catch (Exception e)
		 {
			throw new Exception("addAssetsCategory(DBRow dbrow) error:" + e);
		 }
		
	}
	
	/**
	 * 根据id查询父类信息
	 * @param categoryId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getParentAssetsCategoryJSON(long categoryId) 
		throws Exception 
	{
		try {
			DBRow para = new DBRow();
			para.add("parentid",categoryId);
			String sql = "select * from "+ConfigBean.getStringValue("assets_category")+" where parentid= ? order by sort ";
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("getParentAssetsCategoryJSON(int parentId) error:"+e);
		}
	}
	public DBRow[] getParentAssetsCategoryJSON(long categoryId,String type) 
		throws Exception 
	{
		try {
			DBRow para = new DBRow();
			para.add("parentid",categoryId);
			String sql = "";
			if(type.equals("1"))
				sql = "select id,chName,parentid from "+ConfigBean.getStringValue("assets_category")+" where parentid= ? and id <> 100023 and parentid <> 100023  order by sort ";
			else if(type.equals("3")) {
				sql = "select id,name as chName,100023 as parentid from product_line_define where 100023=?";
			}
			else 
				sql = "select id,chName,parentid from "+ConfigBean.getStringValue("assets_category")+" where parentid= ? and  id = 100023 ";
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("getParentAssetsCategoryJSON(int parentId) error:"+e);
		}
	}

	/**
	 * 删除资产类别信息
	 * @param categoryId
	 * @return
	 * @throws Exception 
	 */
	public void delAssetsCategory(long categoryId) 
		throws Exception 
	{
		
		try 
		{
			 dbUtilAutoTran.delete("where id="+categoryId,ConfigBean.getStringValue("assets_category"));
		} 
		catch (Exception e) 
		{
			throw new Exception("delAssetsCategory(long categoryId) error:"+e);
		}
	}
	
	/**
	 * 根据id查询资产类别信息
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getDetailAssetsCategory(long id) 
		throws Exception 
	{
		try 
		{
			DBRow para = new DBRow();
			para.add("id",id);
			
			String sql = "select * from "+ConfigBean.getStringValue("assets_category")+" where id = ?";
			
			return (dbUtilAutoTran.selectPreSingle(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailAssetsCategory(int id) error:"+e);
		}
	}
	
	/**
	 * 修改某一具体的资产类别信息
	 * @param id
	 * @throws Exception
	 */
	public void modAssetsCategory(int id, DBRow dbrow) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.update("where id="+id, ConfigBean.getStringValue("assets_category"), dbrow);
		} 
		catch (Exception e) 
		{
			throw new Exception("modAssetsCategory(int id, DBRow dbrow) error:"+e);
		}
		
	}
	
	/**
	 * 获取parentid=0或为null的商品分类信息
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getProductParentCatalog() 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_catalog")+" where parentid = 0 or parentid is null";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProductParentCatalog() error:"+e);
		}
	}
	
	/**
	 * 根据父类id获取该类下所有商品分类子类信息
	 * @param parentid
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getProductCatalogByParentId(long parentid) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_catalog")+" where parentid="+parentid;
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProductCatalogByParentId(long parentid) error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	

}
