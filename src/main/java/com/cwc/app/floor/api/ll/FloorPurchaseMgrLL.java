package com.cwc.app.floor.api.ll;

import com.cwc.app.beans.ll.PurchaseBeanLL;
import com.cwc.app.beans.ll.PurchaseDetailBeanLL;
import com.cwc.app.beans.ll.PurchaseFollowupLogsBeanLL;
import com.cwc.app.key.ApplyStatusKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class FloorPurchaseMgrLL {
	private PurchaseBeanLL purchaseBeanLL;
	private PurchaseFollowupLogsBeanLL purchaseFollowupLogsBeanLL;
	private PurchaseDetailBeanLL purchaseDetailBeanLL;
	
	public PurchaseDetailBeanLL getPurchaseDetailBeanLL() {
		return purchaseDetailBeanLL;
	}

	public void setPurchaseDetailBeanLL(PurchaseDetailBeanLL purchaseDetailBeanLL) {
		this.purchaseDetailBeanLL = purchaseDetailBeanLL;
	}

	public PurchaseBeanLL getPurchaseBeanLL() {
		return purchaseBeanLL;
	}

	public void setPurchaseBeanLL(PurchaseBeanLL purchaseBeanLL) {
		this.purchaseBeanLL = purchaseBeanLL;
	}

	public PurchaseFollowupLogsBeanLL getPurchaseFollowupLogsBeanLL() {
		return purchaseFollowupLogsBeanLL;
	}

	public void setPurchaseFollowupLogsBeanLL(
			PurchaseFollowupLogsBeanLL purchaseFollowupLogsBeanLL) {
		this.purchaseFollowupLogsBeanLL = purchaseFollowupLogsBeanLL;
	}
	
	public DBRow updatePurchase(DBRow row) throws Exception {
		return purchaseBeanLL.updateRow(row);
	}
	
	private DBRow insertPurchaseFolloupLogs(DBRow row) throws Exception {
		return purchaseFollowupLogsBeanLL.insertRow(row);
	}
	
	public void insertLogs(String id, String content, long oprator_id, String oprator, int type) throws Exception {
		TDate tDate = new TDate();		
		DBRow logRow = new DBRow();
		logRow.add("purchase_id", id);
		logRow.add("followup_content", content);
		logRow.add("follower_id", oprator_id);
		logRow.add("follower", oprator==null?"":oprator);
		logRow.add("followup_type", type);
		logRow.add("followup_date", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));

		insertPurchaseFolloupLogs(logRow);

		DBRow row = new DBRow();
		row.add("purchase_id", id);
		row.add("updatetime", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
		
		this.updatePurchase(row);
		
	}
	
	public void insertLogs(String id, String content, long oprator_id, String oprator, int type, int typeSub) throws Exception {
		TDate tDate = new TDate();		
		DBRow logRow = new DBRow();
		logRow.add("purchase_id", id);
		logRow.add("followup_content", content);
		logRow.add("follower_id", oprator_id);
		logRow.add("follower", oprator==null?"":oprator);
		logRow.add("followup_type", type);
		logRow.add("followup_date", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
		logRow.add("followup_type_sub", typeSub);
		insertPurchaseFolloupLogs(logRow);
		DBRow row = new DBRow();
		row.add("purchase_id", id);
		row.add("updatetime", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
		this.updatePurchase(row);
	}
	public DBRow getPurchaseById(String id) throws Exception {
		return purchaseBeanLL.getRowById(id);
	}
	
	public DBRow[] getAnalysis(String st, String en, int analysisType, int analysisStatus, int day, PageCtrl pc) throws Exception {
		StringBuffer sql = new StringBuffer("select p.*,coalesce(am.`status`,"+ApplyStatusKey.CANNOT_TRANSFERS+") as moneyStatus, coalesce(am.amount,0) as amount,coalesce(am.amount_transfer,0) as amount_transfer from "+ConfigBean.getStringValue("purchase")+" as p"
				   +" left join "+ConfigBean.getStringValue("apply_money")+" as am  on association_id=purchase_id and types=100001"
				   +" where 1=1");
		
		if(!st.equals("")) {
			sql.append(" and purchase_date >'"+st+"'");
		}
		
		if(!en.equals("")) {
			sql.append(" and purchase_date <='"+en+"'");
		}
		
		if(analysisType == 1) {//需确认价格
			if(analysisStatus==1) {
				sql.append(" and price_affirm_over is null");
				sql.append(" and now() > ADDDATE(purchase_date,INTERVAL "+day+" DAY)");
				sql.append(" and purchase_status = 1");
			}
			else {
				sql.append(" and price_affirm_over is not null");
				sql.append(" and price_affirm_over>"+day);
			}
		}
		else if(analysisType == 2) {//价格已确认
			if(analysisStatus==1) {
				sql.append(" and apply_money_over is null");
				sql.append(" and now() > ADDDATE(purchase_date,INTERVAL "+day+" DAY)");
				sql.append(" and purchase_status = 5");
			}
			else {
				sql.append(" and apply_money_over is not null");
				sql.append(" and apply_money_over>"+day);
			}
		}
		else if(analysisType == 3) {
			if(analysisStatus==1) {
				sql.append(" and purchase_over is null");
				sql.append(" and now() > ADDDATE(purchase_date,INTERVAL "+day+" DAY)");
				sql.append(" and purchase_status <> 4");
			}
			else {
				sql.append(" and purchase_over is not null");
				sql.append(" and purchase_over>"+day);
			}
		}
		
		sql.append(" order by purchase_id desc");
		
		return purchaseBeanLL.getDbUtilAutoTran().selectMutliple(sql.toString(), pc);
	}
	
	public void updatePurchaseDetailByPurchaseId(String purchase_id, DBRow row) throws Exception {
		String cond = "where purchase_id = " + purchase_id;
		purchaseDetailBeanLL.updateRowByCond(cond, row);
	}
	/**
	 * 更新采购单的条款
	 * @param purchase_id
	 * @param row
	 * @throws Exception
	 */
	public void updatePurchaseTerms(long purchase_id, DBRow row) throws Exception{
		try {
			purchaseBeanLL.getDbUtilAutoTran().update(" where purchase_id = " + purchase_id,ConfigBean.getStringValue("purchase") , row);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}
