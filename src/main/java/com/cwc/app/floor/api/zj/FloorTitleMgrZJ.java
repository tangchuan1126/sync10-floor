package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorTitleMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBRow[] getTitlesByAdid(long adid)
		throws Exception
	{
		try 
		{
			String sql = "select distinct t.* from "+ConfigBean.getStringValue("title")+" as t "
						+"join "+ConfigBean.getStringValue("title_admin")+" as ta on ta.title_admin_title_id = t.title_id "
						+"where ta.title_admin_adid = ? order by title_admin_sort asc ";
			
			DBRow para = new DBRow();
			para.add("title_admin_adid",adid);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTitleMgrZJ getTitlesByAdid error:"+e);
		}
	}
	
	public DBRow [] findAllTitle() throws Exception{
		try 
		{
			return this.dbUtilAutoTran.select(ConfigBean.getStringValue("title"));
		}
		catch (Exception e) 
		{
			throw new Exception("findAllTitle() error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
