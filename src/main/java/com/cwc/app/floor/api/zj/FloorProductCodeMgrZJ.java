package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorProductCodeMgrZJ
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加商品条码
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addProductCode(DBRow dbrow)
		throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_code"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCodeMgr addProductCode error:"+e);
		}
	}
	
	/**
	 * 删除商品条码
	 * @param pcode_id
	 * @throws Exception
	 */
	public void delProductCode(long pcode_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where pcode_id="+pcode_id,ConfigBean.getStringValue("product_code"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCodeMgr delProductCode error:"+e);
		}
	}
	
	/**
	 * 根据商品ID删除商品所有条码
	 * @param pc_id
	 * @throws Exception
	 */
	public void delProductCodeByPcid(long pc_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where pc_id="+pc_id,ConfigBean.getStringValue("product_code"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCodeMgr delProductCodeByPcid error:"+e);
		}
	}
	
	/**
	 * 修改条码
	 * @param pcode_id
	 * @param para
	 * @throws Exception
	 */
	public void modProductCode(long pcode_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where pcode_id="+pcode_id,ConfigBean.getStringValue("product_code"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCodeMgr modProductCode error:"+e);
		}
	}
	
	/**
	 * 根据条码搜索
	 * @param p_code
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductCode(String p_code)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_code")+" where p_code = ?";//(ZJ)
			
			DBRow para = new DBRow();
			para.add("p_code",p_code);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCodeMgr getDetailProductCode error:"+e);
		}
	}
	
	public DBRow[] getProductCodesByPcid(long pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_code")+" where pc_id=? order by code_type asc";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCodeMgr getProductCodesByPcid error:"+e);
		}
	}
	
	/**
	 * 根据商品商品ID和条码类型获得条码（不查询旧条码）
	 * @param pc_id
	 * @param code_type
	 * @return
	 * @throws Exception
	 */
	public DBRow getProductCodeByPcidAndCodeType(long pc_id,int code_type)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_code")+" where pc_id =? and code_type <>4 and code_type = ?";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("code_type",code_type);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCodeMgrZJ getProductCodeByPcidAndCodeType error:"+e);
		}
	}
	
	
	/**
	 * 获得使用中的条码
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getUseProductCodes(long pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_code")+" where pc_id=? and code_type !=4 order by code_type asc";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCodeMgrZJ getUseProductCodes error:"+e);
		}
	}
	
	public DBRow[] getAllProductCode()
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_code")+" order by pc_id";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCodeMgrZJ getAllProductCode error:"+e);
		}
	}

	
	/**
	 * 通过pcid、codeType、code查询code
	 * @param codeType
	 * @param pcCode
	 * @param pc_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月5日 上午9:59:08
	 */
	public DBRow[] getProductCodeByTypeAndCodePcid(int codeType, String pcCode, long pc_id) throws Exception
	{
		try
		{
			String sql = "select * from " +ConfigBean.getStringValue("product_code")+" pcode where 1=1 ";
			if(codeType > 0)
			{
				sql += " and pcode.code_type = "+codeType;
			}
			if(null != pcCode && !"".equals(pcCode))
			{
				sql += " and pcode.p_code = '" + pcCode+"'";
			}
			if(pc_id > 0)
			{
				sql += " and pcode.pc_id = " + pc_id;
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductCodeMgrZJ.getProductCodeByTypeAndCode(int codeType, String code) error:"+e);
		}
		
	}
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
