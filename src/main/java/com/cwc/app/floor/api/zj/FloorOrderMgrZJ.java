package com.cwc.app.floor.api.zj;

import com.cwc.app.key.HandStatusleKey;
import com.cwc.app.key.HandleKey;
import com.cwc.app.key.TracingOrderKey;
import com.cwc.app.key.WayBillOrderStatusKey;
import com.cwc.app.key.WaybillInternalTrackingKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorOrderMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 根据订单返回订单产生的订单任务
	 * @param order_id
	 * @return
	 * @throws Exception
	 */
	public int getOrderTaskSum(long order_id)
	throws Exception
	{
		try 
		{
			String sql = "select count(1) as ordertaskcount from "+ConfigBean.getStringValue("order_task")+" where oid="+order_id;
			DBRow dbrow = dbUtilAutoTran.selectSingle(sql);
			int count = dbrow.get("ordertaskcount",0);
			return (count);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorOrderMgr.getOrderTaskSum error:"+e);
		}
	}
	
	/**
	 * 根据client_id查询坏评次数
	 * @param client_id
	 * @return
	 * @throws Exception
	 */
	public int getOrderBadReview(String client_id,int status)
		throws Exception
	{
		try 
		{
			String sql = "select count(1) as badReview from "+ConfigBean.getStringValue("porder")+" where bad_feedback_flag=? and client_id = ?";
			
			DBRow para = new DBRow();
			para.add("status",status);
			para.add("client_id",client_id);
			
			DBRow db = dbUtilAutoTran.selectPreSingle(sql,para);
			int count = db.get("badReview",0);
			
			return (count);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorOrderMgr.getOrderBadReview error:"+e);
		}
	}
	
	/**
	 * 根据收件人姓名，国家ID，省份ID，收货城市，邮编来查询相似订单
	 * @param address_name
	 * @param ccid
	 * @param pro_id
	 * @param address_city
	 * @param zip
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getResembleOrder(String address_name,long ccid,long pro_id,String address_city,String zip)
		throws Exception
	{
		try 
		{
//			String sql = "select * from "+ConfigBean.getStringValue("porder")+" where address_name='"+address_name+"' and ccid="+ccid+" and pro_id="+pro_id+" and address_city = '"+address_city+"' and address_zip = '"+zip+"' and (handle=1 or handle =2)";
			String sql = "select * from "+ConfigBean.getStringValue("porder")+" where address_name=? and ccid=? and pro_id=? and address_city = ? and address_zip = ? and (handle="+HandleKey.WAIT4_RECORD+" or handle ="+HandleKey.WAIT4_OUTBOUND+" or handle="+HandleKey.OUTBOUNDING+" or handle="+HandleKey.ALLOUTBOUND+")";
			
			DBRow para = new DBRow();
			para.add("address_name",address_name);
			para.add("ccid",ccid);
			para.add("pro_id",pro_id);
			para.add("address_city",address_city);
			para.add("zip",zip);
			
//			return (dbUtilAutoTran.selectMutliple(sql));
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderMgrZJ getResembleOrder error:"+e);
		}
	}
	
	/**
	 * 运单已发货后追踪
	 * @param st
	 * @param en
	 * @param product_line_id
	 * @param catalog_id
	 * @param pc_id
	 * @param ca_id
	 * @param ccid
	 * @param pro_id
	 * @param internal_tracking_status
	 * @param daytype
	 * @param lastday
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] trackingOrder(String st,String en,long product_line_id,long catalog_id,long pc_id,long ca_id,long ccid,long pro_id,int internal_tracking_status,int daytype,int lastday,int extended_day,PageCtrl pc)
		throws Exception
	{
		String whereProductLineOrCatalog = "";
		
		if(catalog_id>0)
		{
			whereProductLineOrCatalog = "join product_catalog as pc on pc.id = p.catalog_id "
									   +"join pc_child_list as pcl on pcl.pc_id = pc.id and pcl.search_rootid = "+catalog_id+" ";
		}
		else if(product_line_id>0&&catalog_id==0)
		{
			whereProductLineOrCatalog = " join product_catalog as pc on pc.id = p.catalog_id and pc.product_line_id = "+product_line_id+" "; 
		}
		
		String whereArea = "";
		if(ca_id>0)
		{
			whereArea = " join country_area_mapping as cam on cam.ccid = o.ccid "
					   +" join country_area as ca on ca.ca_id = cam.ca_id and ca.ca_id = "+ca_id+" "; 	
		}
		
		String whereCountry = "";
		if(ccid>0)
		{
			whereCountry = " and o.ccid = "+ccid+" ";
		}
		
		String whereProvince = "";
		if(pro_id>0)
		{
			whereProvince = " and o.pro_id = "+pro_id+" ";
		}
		
		String whereInternalTracking = "";
		if(internal_tracking_status!=-1)
		{
			if(internal_tracking_status !=WaybillInternalTrackingKey.NotYetDeliveryComplete)
			{
				whereInternalTracking = " and o.internal_tracking_status ="+internal_tracking_status;
			}
			else
			{
				whereInternalTracking = " and (o.internal_tracking_status !="+WaybillInternalTrackingKey.DeliveryCompleted+" and o.internal_tracking_status !="+WaybillInternalTrackingKey.UnKnown+")";
			}
			
		}
		
		
		String whereNoDeliveryComplete = "";
		String dayType = "";
		switch (daytype)
		{
			case 1:
				dayType = "post_date";
			break;
			
			case 2:
				dayType = "delivery_date";
			break;
		}
		
		if(lastday>0)
		{
			whereNoDeliveryComplete = " and (DATEDIFF(NOW(),"+dayType+")>"+lastday+" and o.internal_tracking_status !="+WaybillInternalTrackingKey.DeliveryCompleted+" and o.internal_tracking_status !="+WaybillInternalTrackingKey.UnKnown+")";
			
			whereInternalTracking = "";
		}
		
		String whereExtended = "";
		if(extended_day>0)
		{
			String internalTracingStatus = "";
			if(internal_tracking_status!=-1)
			{
				if (internal_tracking_status==WaybillInternalTrackingKey.NotYetDeliveryComplete) 
				{
					internalTracingStatus = " and pn.trace_type = "+TracingOrderKey.WAYBILL_TRACKING+" ";
				}
				else
				{
					internalTracingStatus = " and pn.trace_child_type = "+internal_tracking_status+" ";
				}
			}
			whereExtended = " join "+ConfigBean.getStringValue("porder_note")+" as pn where o.oid = pn.oid "+internalTracingStatus+" and DATEDIFF(NOW(),pn.post_date)>"+extended_day;
			
		}
		
		
		String sql = "";
		
		if (pc_id>0) 
		{
			sql = "select DISTINCT o.* from "+ConfigBean.getStringValue("porder")+" as o "
			+"join "+ConfigBean.getStringValue("porder_item")+" as oi on oi.oid = o.oid and (oi.product_type = 1 or oi.product_type=2)"
			+"join "+ConfigBean.getStringValue("product")+" as p on oi.pid = p.pc_id and p.pc_id = "+pc_id+" "
			+whereArea
//			+whereExtended
			+"where o.internal_tracking_status !="+WaybillInternalTrackingKey.UnKnown+" and post_date>'"+st+" 0:00:00' and post_date<'"+en+" 23:59:59' "+whereCountry+whereProvince+whereInternalTracking+whereNoDeliveryComplete
			+" union "
			+" select DISTINCT o.* from "+ConfigBean.getStringValue("porder")+" as o "
			+"join "+ConfigBean.getStringValue("porder_item")+" as oi on o.oid = oi.oid and (oi.product_type = 2 or oi.product_type =3 or oi.product_type=4)"
			+"join "+ConfigBean.getStringValue("product_union")+" as pu on oi.pid = pu.set_pid "
			+"join "+ConfigBean.getStringValue("product")+" as p on pu.pid = p.pc_id and p.pc_id = "+pc_id+" "
			+whereArea
//			+whereExtended
			+"where o.internal_tracking_status !="+WaybillInternalTrackingKey.UnKnown+" and post_date>'"+st+" 0:00:00' and post_date<'"+en+" 23:59:59' "+whereCountry+whereProvince+whereInternalTracking+whereNoDeliveryComplete+" ";
		}
		else
		{
			sql = "select DISTINCT o.* from "+ConfigBean.getStringValue("porder")+" as o "
				 	+"join "+ConfigBean.getStringValue("porder_item")+" as oi on oi.oid = o.oid "
				 	+"join "+ConfigBean.getStringValue("product")+" as p on oi.pid = p.pc_id "
				 	+whereProductLineOrCatalog
				 	+whereArea
//				 	+whereExtended
				 	+"where o.internal_tracking_status !="+WaybillInternalTrackingKey.UnKnown+" and post_date>'"+st+" 0:00:00' and post_date<'"+en+" 23:59:59' "+whereCountry+whereProvince+whereInternalTracking+whereNoDeliveryComplete;
		}
		
		return dbUtilAutoTran.selectMutliple(sql, pc);
	}
	
	

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
