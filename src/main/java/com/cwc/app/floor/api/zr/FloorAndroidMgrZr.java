package com.cwc.app.floor.api.zr;


import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.LoadUnloadOccupancyStatusKey;
import com.cwc.app.key.LoadUnloadOccupancyTypeKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.key.TransportRegistrationTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorAndroidMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	public DBRow[] getTransportOutBound(long transport_id) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("transport_outbound");
			return dbUtilAutoTran.selectMutliple("select * from " + tableName + " where to_transport_id=" + transport_id);
		}catch(Exception e){
			throw new Exception("FloorAndroidMgrZr.getTransportOutBound(transport_id):"+e);
		}
	}
	
	public DBRow[] getProductForTransport(long transport_id) throws Exception {
		try{
			String sql = "select distinct p.* from "+ConfigBean.getStringValue("product")+" as p "
			+"join "+ConfigBean.getStringValue("transport_outbound")+" as tob on p.pc_id = tob.to_pc_id and tob.to_transport_id = ?"
			+" order by p.pc_id";
			DBRow para = new DBRow();
			para.add("to_transport_id",transport_id);
		//	System.out.println("getProductForTransport : " + sql.toString() );
		return dbUtilAutoTran.selectPreMutliple(sql, para);
			
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getProductForTransport(transport_id):"+e);
		}
	}
	public DBRow[] getPositionProduct(long transport_id) throws Exception {
		try{
			String sql = "select distinct  p.* from "+ConfigBean.getStringValue("product")+" as p "
			+"join "+ConfigBean.getStringValue("transport_warehouse")+" as twh on p.pc_id = twh.tw_product_id and twh.tw_transport_id = ?"
			+" order by p.pc_id";
			DBRow para = new DBRow();
			para.add("to_transport_id",transport_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
			
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getProductForTransport(transport_id):"+e);
		}
	}
	
	public DBRow[] getProductCodeForTransport(long transport_id) throws Exception {
		try{
			String sql = "select distinct pcode.*,p.p_name from "+ConfigBean.getStringValue("product_code")+" as pcode "
			+"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = pcode.pc_id "
			+"join "+ConfigBean.getStringValue("transport_outbound")+" as tob on p.pc_id = tob.to_pc_id and tob.to_transport_id =?"
			+" order by pcode.pc_id";
	
			DBRow para = new DBRow();
			para.add("to_transport_id",transport_id);
  			return dbUtilAutoTran.selectPreMutliple(sql, para);
			
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getProductCodeForTransport(transport_id):"+e);
		}
	}
	public DBRow[] getSerialProductForTransport(long transport_id) throws Exception {
		try{
			
			String sql = "select distinct sp.* from "+ConfigBean.getStringValue("serial_product")+" as sp "
			+"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = sp.pc_id "
			+"join "+ConfigBean.getStringValue("transport_outbound")+" as tob on p.pc_id = tob.to_pc_id and tob.to_transport_id = ?"
			+" order by sp.pc_id";

			DBRow para = new DBRow();
			para.add("to_transport_id",transport_id);
 			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getSerialProductForTransport(transport_id):"+e);
		}
	}
	public DBRow  getContainerProductTotalInfo(long cp_lp_id) throws Exception {
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select sum(p.weight * cp.cp_quantity) as total_weight , sum(p.heigth * p.width * p.length * cp.cp_quantity ) as total_v from ").append(ConfigBean.getStringValue("container_product"))
			.append(" as cp left join ").append(ConfigBean.getStringValue("product")).append(" as p on p.pc_id = cp.cp_pc_id where cp.cp_lp_id =").append(cp_lp_id);
			 
			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getContainerProduct(cp_lp_id):"+e);
		}
	}
 
	/**
	 * 查询对应LP的type信息.
	 * (处理Lp 不存在的情况)
	 * @param cp_lp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getContainerTypeInfo(String lp_name)throws Exception {
		try{
			StringBuffer sql = new StringBuffer("select * from ").append(ConfigBean.getStringValue("container")).append(" as c left join ")
			.append(ConfigBean.getStringValue("container_type")).append(" as ct  on ct.type_id = c.type_id where c.container ='");	
			sql.append(lp_name).append("'");
		 
			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getContainerTypeInfo(lp_name):"+e);
		}
	}
	/**
	 * 查询对应LP的type信息.
	 * (处理Lp 不存在的情况)
	 * @param cp_lp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getContainerTypeInfo(long cp_lp_id)throws Exception {
		try{
			StringBuffer sql = new StringBuffer("select * from ").append(ConfigBean.getStringValue("container")).append(" as c left join ")
			.append(ConfigBean.getStringValue("container_type")).append(" as ct  on ct.type_id = c.type_id where c.con_id =");	
			sql.append(cp_lp_id);
		 
			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getContainerTypeInfo(cp_lp_id):"+e);
		}
	}
	
	/**
	 * 查询checkIn 的transport
	 * @param transport_state
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportByTranposrtOrderKey(long transport_state) throws Exception {
		try{
			StringBuffer sql = new StringBuffer("select t.transport_id , sendarea.title as fromwarehouse, receivearea.title  as towarehouse, sla.area_name  from ").
			append(ConfigBean.getStringValue("transport")).append(" as t  left join ")
			.append(ConfigBean.getStringValue("product_storage_catalog"))
			.append(" as sendarea on t.send_psid = sendarea.id") 
			.append(" left join ").append(ConfigBean.getStringValue("product_storage_catalog"))
			.append(" as receivearea on receivearea.id = t.receive_psid  left join storage_location_area as sla on sla.area_id = t.transport_area_id  where t.transport_status =")
			.append(transport_state);
 			return dbUtilAutoTran.selectMutliple(sql.toString());
			 
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getContainerTypeInfo(transport_state):"+e);
		}
	}
	/**
	 * 下载放货的transport 
	 * 1.主要是收货中的,和收货完成的
	 * 2.返回title_id 和title
	 * @return
	 * @throws Exception    
	 */
	 
	public DBRow[] getPositionedTransport(long psid) throws Exception{
		try{
			StringBuffer sql = new StringBuffer("select t.transport_id ,t.purchase_id, sendarea.title as fromwarehouse, receivearea.title  as towarehouse, sla.area_name , t.title_id ,title.title_name from ").
			append(ConfigBean.getStringValue("transport")).append(" as t  left join ")
			.append("title on title.title_id = t.title_id")
			.append(" left join ")
			.append(ConfigBean.getStringValue("product_storage_catalog"))
			.append(" as sendarea on t.send_psid = sendarea.id") 
			.append(" left join ").append(ConfigBean.getStringValue("product_storage_catalog"))
			.append(" as receivearea on receivearea.id = t.receive_psid  left join storage_location_area as sla on sla.area_id = t.transport_area_id  where t.receive_psid = ").append(psid)
			.append(" and ( t.transport_status =")
			.append(TransportOrderKey.RECEIVEING).append(" or t.transport_status =").append(TransportOrderKey.AlREADYRECEIVE).append(" or t.transport_status = "+TransportOrderKey.APPROVEING+")");
 			return dbUtilAutoTran.selectMutliple(sql.toString());
			
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getPositionedTransport():"+e);
		}
	}
	
	//查询门和位置--------------------
//	public DBRow[] getPositionedTransportAddDoors(long id) throws Exception{
//		try{
//			String sql="select * from door_or_location_occupancy_info where rel_id="+id+"rel_type="+ProductStoreBillKey.DELIVERY_ORDER+"rel_occupancy_use="+TransportRegistrationTypeKey.DELEIVER;
//			return this.dbUtilAutoTran.selectMutliple(sql);
//			
//		}catch (Exception e) {
//			throw new Exception("FloorAndroidMgrZr.getPositionedTransport():"+e);
//		}
//	}
	  
	public DBRow[] getCheckInTransport(long psid) throws Exception {
		try{
			StringBuffer sql = new StringBuffer("select t.transport_id , t.title_id, t.purchase_id ,title.title_name, sendarea.title as fromwarehouse, receivearea.title  as towarehouse   from ").
			append(ConfigBean.getStringValue("transport")).append(" as t  left join ")
			.append("title on title.title_id = t.title_id")
			.append(" left join ")
			.append(ConfigBean.getStringValue("product_storage_catalog"))
			.append(" as sendarea on t.send_psid = sendarea.id") 
			.append(" left join ").append(ConfigBean.getStringValue("product_storage_catalog"))
			.append(" as receivearea on receivearea.id = t.receive_psid   where t.receive_psid = ").append(psid)
			.append(" and ( t.transport_status =")
			.append(TransportOrderKey.AlREADYARRIAL).append(" or t.transport_status =").append(TransportOrderKey.RECEIVEING).append(" ) ");
//  			System.out.println(sql.toString() );
			return dbUtilAutoTran.selectMutliple(sql.toString());
			 
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getCheckInTransport():"+e);
		}
	}
	public DBRow getSupplier(long purchase_id)throws Exception{
		try{
			String sql="select * from purchase p join supplier s on p.supplier=s.id where p.purchase_id="+purchase_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorAndroidMgrZr.getSupplier():"+e);
		}
	}
	public DBRow[] getTransportNotPurcharsId(long psid) throws Exception {
		try{
			StringBuffer sql = new StringBuffer("select t.transport_id,t.reserve_type,sendarea.title as fromwarehouse, receivearea.title  as towarehouse, sla.area_name  from ").
			append(ConfigBean.getStringValue("transport")).append(" as t  left join ")
			.append(ConfigBean.getStringValue("product_storage_catalog"))
			.append(" as sendarea on t.send_psid = sendarea.id") 
			.append(" left join ").append(ConfigBean.getStringValue("product_storage_catalog"))
			.append(" as receivearea on receivearea.id = t.receive_psid  left join storage_location_area as sla on sla.area_id = t.transport_area_id  where t.transport_status =")
			.append(TransportOrderKey.PACKING).append(" and t.purchase_id < 1 ")
			.append("and t.send_psid = ").append(psid);
 			return dbUtilAutoTran.selectMutliple(sql.toString());
			
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getTransportNotPurcharsId():"+e);
		}
	}
	public DBRow[] getTransportDetailBy(long transport_id) throws Exception{
		try{
			String sql  = "select * from " + ConfigBean.getStringValue("transport_detail") + " where transport_id=" + transport_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getTransportDetailBy():"+e);
		}
	}
	public DBRow[] getPositionProductCode(long transport_id) throws Exception {
		try{
			String sql = "select distinct pcode.* , p.p_name from "+ConfigBean.getStringValue("product_code")+" as pcode "
			+"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = pcode.pc_id "
			+"join "+ConfigBean.getStringValue("transport_warehouse")+" as twh on p.pc_id = twh.tw_product_id and twh.tw_transport_id =?"
			+" order by pcode.pc_id";
	
			DBRow para = new DBRow();
			para.add("to_transport_id",transport_id);
 			return dbUtilAutoTran.selectPreMutliple(sql, para);
			
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getPositionProductCode(transport_id):"+e);
		}
	}
	 
	public DBRow[] getPositionSerialProduct(long transport_id) throws Exception {
		try{
			
			String sql = "select distinct sp.* from "+ConfigBean.getStringValue("serial_product")+" as sp "
			+"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = sp.pc_id "
			+"join "+ConfigBean.getStringValue("transport_warehouse")+" as twh on p.pc_id = twh.tw_product_id and twh.tw_transport_id = ?"
			+" order by sp.pc_id";

			DBRow para = new DBRow();
			para.add("to_transport_id",transport_id);
 			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getSerialProductForTransport(transport_id):"+e);
		}
	}
	public DBRow[] getTransportWarehouse(long transport_id) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("transport_warehouse");
			return dbUtilAutoTran.selectMutliple("select * from " + tableName + " where tw_transport_id=" + transport_id);
		}catch(Exception e){
			throw new Exception("FloorAndroidMgrZr.getTransportOutBound(transport_id):"+e);
		}
	}
	public void updateTransportState(int transport_state , long transport_id) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("transport");
			DBRow data = new DBRow();
			data.add("transport_status", transport_state);
			
			dbUtilAutoTran.update("where transport_id=" +transport_id , tableName, data );
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.updateTransportState(transport_state,transport_id):"+e);
		}
	}
	//outbound baseinfo
	public DBRow[] getOutboundProductInfo(long transport_id) throws Exception {
		try{
			String sql = "select distinct p.* from "+ConfigBean.getStringValue("product")+" as p "
			+"join "+ConfigBean.getStringValue("transport_detail")+" as tob on p.pc_id = tob.transport_pc_id and tob.transport_id = ?"
			+" order by p.pc_id";
			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
 		return dbUtilAutoTran.selectPreMutliple(sql, para);
			
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getOutboundProductInfo(transport_id):"+e);
		}
	}
	public DBRow[] getProductCodeForOutboundTransport(long transport_id) throws Exception {
		try{
			String sql = "select distinct pcode.*, p.p_name from "+ConfigBean.getStringValue("product_code")+" as pcode "
			+"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = pcode.pc_id "
			+"join "+ConfigBean.getStringValue("transport_detail")+" as tob on p.pc_id = tob.transport_pc_id and tob.transport_id =?"
			+" order by pcode.pc_id";
	
			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
  			return dbUtilAutoTran.selectPreMutliple(sql, para);
			
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getProductCodeForOutboundTransport(transport_id):"+e);
		}
	}
	public DBRow[] getSerialProductForOutboundTransport(long transport_id) throws Exception {
		try{
			
			String sql = "select distinct sp.* from "+ConfigBean.getStringValue("serial_product")+" as sp "
			+"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = sp.pc_id "
			+"join "+ConfigBean.getStringValue("transport_detail")+" as tob on p.pc_id = tob.transport_pc_id and tob.transport_id = ?"
			+" order by sp.pc_id";

			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
 			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getSerialProductForTransport(transport_id):"+e);
		}
	}
	public DBRow[] getDoorLocationAnd(long transport_id) throws Exception {
		try{
			StringBuffer sql = new StringBuffer( "select * from ") 
			.append(ConfigBean.getStringValue("door_or_location_occupancy_info"))
			.append(" where rel_id= ").append( transport_id).append(" and  door_or_location_occupancy_info.rel_type = ").append(ProductStoreBillKey.TRANSPORT_ORDER) ;
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getDoorAndLocationNameByTransport(transport_id):"+e);
		}
	}
	public DBRow[]  getDoorName(long transport_id,  int type ) throws Exception{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from ").append(ConfigBean.getStringValue("door_or_location_occupancy_info")).append(" as dol ") 
			.append("left join " + ConfigBean.getStringValue("storage_door"))
			.append(" as sd on sd.sd_id =dol.rl_id  where dol.rel_id = ? ") 
			.append("  and dol.occupancy_type = 1 ")
			.append(" and dol.rel_occupancy_use = ? ");
			DBRow para = new DBRow();
			para.add("rel_id", transport_id);
			para.add("rel_occupancy_use", type);
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getDoorName(id):"+e);
		}
	}
	public DBRow[]  getLocationName(long transport_id , int type) throws Exception{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from ").append(ConfigBean.getStringValue("door_or_location_occupancy_info")).append(" as dol ") 
			.append(" left join " + ConfigBean.getStringValue("storage_load_unload_location"))
			.append(" as slul on slul.id =dol.rl_id  where dol.rel_id = ? ")
			.append("  and dol.occupancy_type = 2 ")	
			.append("  and dol.rel_occupancy_use = ? ");	

			DBRow para = new DBRow();
			para.add("rel_id", transport_id);
			para.add("rel_occupancy_use", type);
			return dbUtilAutoTran.selectPreMutliple(sql.toString(),para);
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getDoorName(id):"+e);
		}
	}
	/**
	 * 运输中的
	 * @param machine
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportByMachine(long ps_id) throws Exception{
		try{

			StringBuffer sql = new StringBuffer("select t.transport_id , sendarea.title as fromwarehouse, receivearea.title  as towarehouse, sla.area_name  from ").
			append(ConfigBean.getStringValue("transport")).append(" as t  left join ")
			.append(ConfigBean.getStringValue("product_storage_catalog"))
			.append(" as sendarea on t.send_psid = sendarea.id") 
			.append(" left join ").append(ConfigBean.getStringValue("product_storage_catalog"))
			.append(" as receivearea on receivearea.id = t.receive_psid  left join storage_location_area as sla on sla.area_id = t.transport_area_id  where (t.transport_status = ")
			.append(TransportOrderKey.READY).append(" or t.transport_status = "+TransportOrderKey.INTRANSIT +" )")
			.append(" and t.send_psid =").append(ps_id)
			.append(" order by t.transport_id desc ");
 		 
			 
			 
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getTransportByMachine(machine):"+e);
		}
	}
	public DBRow[] getProductFileBy(long maxPcid , int limit) throws Exception{
		try{
			
			StringBuffer sql = new StringBuffer("select * from ")
			.append(ConfigBean.getStringValue("product"))
			.append(" where   product.pc_id >").append(maxPcid)
		//	.append(" and ps_id =").append(ps_id).append("") //先不加这个条件
			.append(" order  by  pc_id  desc ")
			.append(" limit 0,").append(limit);
 			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getProductFileBy( machine,maxPcid,limit):"+e);
		}
	}
	public DBRow getProductPictureBy(long pc_id , int file_with_type , int... product_file_type) throws Exception{
		 try{
			 	StringBuffer sql = new StringBuffer();
			 	sql.append("select * from ").append(ConfigBean.getStringValue("product_file"))
			 	.append(" where pc_id=").append(pc_id).append(" and file_with_type ="+file_with_type);
			 	if(product_file_type != null){
			 		sql.append(" and product_file_type=").append(product_file_type[0]);
			 	}
			 	sql.append(" limit 0,1 ");
			  
			 
			 
				
				return dbUtilAutoTran.selectSingle(sql.toString());
			 
		 }catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getProductPictureBy( file_with_id,file_with_type,product_file_type):"+e);
		}
	}
	public DBRow[] getAllFile(long file_with_id , int file_with_type) throws Exception {
		 try{
			 StringBuffer sql = new StringBuffer();
			 	sql.append("select * from ").append(ConfigBean.getStringValue("product_file"))
			 	.append(" where pc_id=").append(file_with_id).append(" and file_with_type ="+file_with_type);
			 	return dbUtilAutoTran.selectMutliple(sql.toString());
		 }catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getAllFile( file_with_id,file_with_type):"+e);
		}
	}
	public void clearDoor(long transport_id ,int type , String dataTime )throws Exception{
		 try{
			 DBRow data = new DBRow() ;
			 data.add("occupancy_status", LoadUnloadOccupancyStatusKey.RELEASE);
			 data.add("book_end_time", dataTime);
			 dbUtilAutoTran.update(" where rel_type = " +ProductStoreBillKey.TRANSPORT_ORDER + " and occupancy_type =  " 
					 +LoadUnloadOccupancyTypeKey.DOOR + " and rel_id = " + transport_id
					 + " and rel_occupancy_use = " + type 
					 , ConfigBean.getStringValue("door_or_location_occupancy_info"), data) ;
		 }catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.clearDoor( transport_id,type,dataTime):"+e);
		}
	}
	public void clearLocation(long transport_id , int type , String dataTime) throws Exception {
		 try{
			 DBRow data = new DBRow() ;
			 data.add("occupancy_status", LoadUnloadOccupancyStatusKey.RELEASE);
			 data.add("book_end_time", dataTime);
			 dbUtilAutoTran.update(" where rel_type = " +ProductStoreBillKey.TRANSPORT_ORDER + " and occupancy_type =  " 
					 +LoadUnloadOccupancyTypeKey.LOCATION + " and rel_id = " + transport_id
					 + " and rel_occupancy_use =" + type 
					 , ConfigBean.getStringValue("door_or_location_occupancy_info"), data) ;
		 }catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.clearLocation( transport_id,type,dataTime):"+e);
		}
	}
	public long addNewLp(String lpname, long bill_id , int billType) throws Exception{
		try{
			
			DBRow data = new DBRow();
			data.add("container", lpname);
		 
			
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("container"), data);
			
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.addNewLp(lpName):"+e);
		}
	}
	public int countContainerTypeAndBillId(String lpName , long bill_id , int billType)  throws Exception{
		try{
			int count = 0 ;
			String sql = " select count(*) as count_sum from " +
			ConfigBean.getStringValue("container") + " where container = ?   ";
			DBRow param = new DBRow();
			param.add("container", lpName.trim());
			 
			
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, param );
			if(result != null){
				count = result.get("count_sum", 0);
			}
			return count ;
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.countContainerTypeAndBillId(lpName,bill_id,billType):"+e);
		}
	}
	
	public void addContainerProduct(DBRow row) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("container_product");
			dbUtilAutoTran.insert(tableName, row);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.addContainerProduct(row):"+e);
		}
	}
	
	public void deleteContainerProduct(long cp_lp_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("container_product");
			dbUtilAutoTran.delete(" where cp_lp_id="+cp_lp_id, tableName);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.deleteContainerProduct(cp_lp_id):"+e);
		}
	}
	public void addContainerLoading(DBRow data) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("container_loading");
			dbUtilAutoTran.insertReturnId(tableName, data);
 		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.addContainerLoading(data):"+e);
		}
	}
	public void deleteContainerLoading(long con_id , long parent_con_id ) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("container_loading");
			dbUtilAutoTran.delete(" where  con_id = "+con_id+" and parent_con_id="+parent_con_id, tableName);
  		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.addContainerLoading(con_id , parent_con_id):"+e);
		}
	}
	
	public void updateContainer(long container_id , DBRow updateRow) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("container");
			dbUtilAutoTran.update(" where con_id="+container_id, tableName, updateRow);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.updateContainer(container_id , updateRow):"+e);
		}
	}
	public DBRow getContainerLocation(long container_id) throws Exception {
		  try{
		   String sql = "select slc.slc_id , slc.slc_position_all from product_storage_container as psc left join storage_location_catalog as slc on slc.slc_id = psc.slc_id where psc.con_id =?";
		   DBRow para = new DBRow();
		   para.add("con_id", container_id);
		   DBRow[] rows = dbUtilAutoTran.selectPreMutliple(sql, para);
		   if(rows != null && rows.length > 0){
		    for(DBRow row : rows){
		     if(row.get("slc_id", 0l) != 0l || row.getString("slc_position_all").trim().length() > 0 ){
		    	 return row;
		     }
		    }
		   }
		   return null ;
		  }catch (Exception e) {
		    throw new Exception("FloorAndroidMgrZr.getContainerLocation(container_id):"+e);
		  }
		 }
	public DBRow[] getChildListBySearchRootId(long search_root_id) throws Exception{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append(" select  * from ")
			.append(ConfigBean.getStringValue("container_loading_child_list")).append(" where search_root_con_id =").append(search_root_id);
			return dbUtilAutoTran.selectMutliple(sql.toString());
 		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getChildListBySearchRootId(search_root_id):"+e);
		}
	}
	public void deleteChildListBySearchRootId(long search_root_id) throws Exception{
		try{
			dbUtilAutoTran.delete(" where search_root_con_id="+search_root_id, ConfigBean.getStringValue("container_loading_child_list"));
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.deleteChildListBySearchRootId(search_root_id):"+e);
		}
	}
	public void addChildListItem(DBRow row)throws Exception{
		try{
			 dbUtilAutoTran.insert(ConfigBean.getStringValue("container_loading_child_list"), row);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.addChildListItem(row):"+e);
		}
	}
	public DBRow getParentContainer(long container_id) throws Exception{
		try{
			return  dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("container_loading") + " where con_id = " + container_id );
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getParentContainer(container_id):"+e);
 		}
	}
	public DBRow getContainerById(long  container_id) throws Exception {
		try{
			return  dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("container") + " where con_id = " + container_id );
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getContainerById(containerId):"+e);
		}
	}
	public DBRow[] getContainerProducts(long container_id) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("container_product")  + " where cp_lp_id = ? ";
		
			DBRow para = new DBRow();
			para.add("cp_lp_id", container_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para );
			
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getContainerProducts(container_id):"+e);
		}
	}
	public DBRow[] getContainerChildList(long container_id ) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("container_loading_child_list") + " where search_root_con_id= ? ";
			DBRow para = new DBRow();
			para.add("search_root_con_id", container_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getContainerChildList(container_id):"+e);
		}
	}
	public DBRow getProductInfo(long p_id) throws Exception{
		try{
			DBRow para = new DBRow();
			para.add("pc_id", p_id);
			String sql = "select p.*,pc.title from product as p left join product_catalog  as pc on  p.catalog_id = pc.id where p.pc_id = ? ";
			return dbUtilAutoTran.selectPreSingle(sql, para);	
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getProductInfo(p_id):"+e);
		}
	}
	public DBRow[] getProductCode(long pc_id) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("product_code") + " where pc_id = ? ";
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para); 
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getProductCode(p_id):"+e);
		}
	}
	public DBRow[] getProductPictrueInfo(long pc_id) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("product_file") + " where pc_id = ? ";
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getProductPictrueInfo(pc_id):"+e);
		}
	}
	public DBRow[] getLoginTitles(long adid) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("title_admin") + " where title_admin_adid = ? ";
			DBRow para = new DBRow();
			para.add("title_admin_adid",adid);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getProductPictrueInfo(pc_id):"+e);
		}
	}
	public DBRow[] getProductTitles(long pc_id) throws Exception{
		try{
			String sql = "SELECT tp.*,t.title_name from title_product as tp left join title as t on t.title_id = tp.tp_title_id where tp.tp_pc_id = ? ";
			DBRow para = new DBRow();
			para.add("tp_pc_id",pc_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getProductTitles(pc_id):"+e);
		}
	}
	public boolean deleteProductFile(long  pf_id) throws Exception{
		try{
			int count =	dbUtilAutoTran.delete(" where pf_id = "+ pf_id, ConfigBean.getStringValue("product_file"));
			if(count == 1){
				return true ;
			}
			return false ;
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.deleteProductFile(pf_id):"+e);
		}
	}
	public int getProductMainCodeByPcid(long pcid , int code_type ,String p_code )throws Exception{
		try{
			String sql = "select count(*) as sum_count from " +  ConfigBean.getStringValue("product_code") + " where p_code= ? and pc_id = ? and code_type = ? " ;
			DBRow para = new DBRow();
			para.add("p_code", p_code);
			para.add("pc_id", pcid);
			para.add("code_type", code_type);
			DBRow row = dbUtilAutoTran.selectPreSingle(sql.toString(), para );
			return row.get("sum_count", 0);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getProductMainCodeByPcid(pcid , code_type ,p_code):"+e);

 		}
	}
	public DBRow getProductCode(long pcid , int code_type )throws Exception{
		try{
			String sql = "select * from  " +  ConfigBean.getStringValue("product_code") + " where    pc_id = ? and code_type = ? " ;
			DBRow para = new DBRow();
 			para.add("pc_id", pcid);
			para.add("code_type", code_type);
			DBRow row = dbUtilAutoTran.selectPreSingle(sql.toString(), para );
			return row;
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getProductCode(pcid , code_type):"+e);
 		}
	}
	public long addProductCode(DBRow insertRow) throws Exception{
		try{
			return dbUtilAutoTran.insertReturnId( ConfigBean.getStringValue("product_code"), insertRow);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.addProductCode(insertRow):"+e);
		}
	}
	public void updateProductCode(long id , DBRow updateRow) throws Exception{
		try{
			dbUtilAutoTran.update(" where pcode_id="+id,  ConfigBean.getStringValue("product_code"), updateRow);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.updateProductCode(id,updateRow):"+e);
		}
	}
	public int countPName(String p_name) throws Exception{
		try{
			String sql = "select count(*) as count_sum from " + ConfigBean.getStringValue("product") + " where p_name = ? ";
			DBRow para =  new  DBRow();
			para.add("p_name", p_name);
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para );
			if(result != null){
				return result.get("count_sum", 0);
			}
			return  0;
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.countPName(p_name):"+e);
		}
	}
	public DBRow[] getProductCodeByPcid(long pc_id) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("product_code")  + " where pc_id = ?  order by code_type " ;
			DBRow para = new DBRow();
			para.add("pc_id", pc_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getProductCodeByPcid(pc_id):"+e);
 		}
	}
	public void deleteFile(long file_id) throws Exception{
		try{
			dbUtilAutoTran.delete(" where file_id = " + file_id,  ConfigBean.getStringValue("file"));
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.deleteFile(file_id):"+e);
		}
	}
	public DBRow[] getProductBy(PageCtrl page) throws Exception{
		try{
			String sql = "select p.* , pc.p_code from product as p   left  join product_code as pc on p.pc_id =  pc.pc_id and pc.code_type ="+CodeTypeKey.Main + " order by p.pc_id desc  ";
			return dbUtilAutoTran.selectMutliple(sql, page);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getProductBy(page):"+e);
		}
	}
	public DBRow[] getProductBy(long maxPcId , int length  , String flag ) throws Exception{
		try{
			String sql = "";
			if(flag.toUpperCase().equals("UP")){
				 sql = "select p.* , pc.p_code from product as p   left  join product_code as pc on p.pc_id =  pc.pc_id and pc.code_type ="+CodeTypeKey.Main + " where  p.pc_id > "+maxPcId+" order by p.pc_id desc  LIMIT 0,"+length;
			}else{
				 sql = "select p.* , pc.p_code from product as p   left  join product_code as pc on p.pc_id =  pc.pc_id and pc.code_type ="+CodeTypeKey.Main + " where  p.pc_id < "+maxPcId+" order by p.pc_id desc  LIMIT 0,"+length;

			} 
   			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			 throw new Exception("FloorAndroidMgrZr.getProductBy(page):"+e);
		}
	}
	public DBRow[] getAllFileByFileWithIdAndFileWithType(long fileWithId , int fileWithType) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("file");
			String sql = "select * from " +tableName + " where file_with_type="+fileWithType + " and file_with_id="+fileWithId + " order by file_id desc ";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getAllFileByFileWithIdAndFileWithType(pc,fileWithType):"+e);
		}
	}
	
	public DBRow getBlpType(long cotainerTypeId ) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("box_type");
			return dbUtilAutoTran.selectSingle("select * from " + tableName+  " where box_type_id = " + cotainerTypeId )  ; 
 		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getBlpType(cotainerTypeId):"+e);
		}
	}
	public DBRow getILpType(long cotainerTypeId ) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("inner_box_type");
			return dbUtilAutoTran.selectSingle("select * from " + tableName+  " where ibt_id = " + cotainerTypeId )  ; 
 		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getBlpType(cotainerTypeId):"+e);
		}
	}
	public DBRow getClpType(long cotainerTypeId ) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("clp_type");
			return dbUtilAutoTran.selectSingle("select * from " + tableName+  " where sku_lp_type_id = " + cotainerTypeId )  ; 
 		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getBlpType(cotainerTypeId):"+e);
		}
	}
	public DBRow getContainer(long con_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("container");
			return dbUtilAutoTran.selectSingle("select * from " + tableName+  " where con_id = " + con_id )  ; 
 		}catch (Exception e) {
			throw new Exception("FloorAndroidMgrZr.getContainer(con_id):"+e);
		}
	}
	/**
	 * 通过searhcId和levv删除容器平铺表
	 * @param search_root_id
	 * @param levv
	 * @param isLevvEqual true：levv=isLevvEqual；false:levv>=isLevvEqual
	 * zyj
	 * @throws Exception
	 */
	public void deleteChildListBySearchRootIdLevv(long search_root_id, int levv, boolean isLevvEqual) throws Exception
	{
		try
		{
			String where = " where search_root_con_id="+search_root_id;
			if(levv > 0)
			{
				if(isLevvEqual)
				{
					where += " and levv = " + levv;
				}
				else
				{
					where += " and levv >= " + levv;
				}
			}
			if(levv < 0)
			{
				where += " and levv != " + Math.abs(levv);
			}
			//System.out.println("where:"+where);
			dbUtilAutoTran.delete(where, ConfigBean.getStringValue("container_loading_child_list"));
		}
		catch (Exception e) 
		{
			 throw new Exception("FloorAndroidMgrZr.deleteChildListBySearchRootIdLevv(search_root_id, levv):"+e);
		}
	}
	
	
	
	public void deleteChildListBySearchRootAndContId(long search_root_id, long cont_id) throws Exception
	{
		try
		{
			dbUtilAutoTran.delete(" where search_root_con_id="+search_root_id+" and cont_id="+cont_id, ConfigBean.getStringValue("container_loading_child_list"));
		}
		catch (Exception e) 
		{
			 throw new Exception("FloorAndroidMgrZr.deleteChildListBySearchRootId(search_root_id):"+e);
		}
	}
	
	
}
