package com.cwc.app.floor.api.slj;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

/**
 * 账号管理DAO
 * @since Sync10-ui 1.0
 * @author subin
 * */
public class FloorOfficeAddressMgrSlj{
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	

	

	/**
	 * 账号管理 >> 获取全部办公地点
	 * @param 账号ID,办公地点ID
	 * @return 全部办公地点(显示账号关联的办公地点,用于树列表)
	
	 * @author shilijun
	**/
	public DBRow[] getOfficeLocationList(long id,long locationId) throws Exception{
		
		try {
			
			String sql = "select id,office_name as name ,parentid as pid,if(offadm.office_location is null,'false','true') as checked,if(offadm.office_location is null,'false','true') as open from office_location LEFT OUTER JOIN (select office_location from admin where adid = "+id+") as offadm on office_location.id = offadm.office_location where parentid= "+ locationId +" order by id,sort ";
			
			DBRow[] result = dbUtilAutoTran.selectMutliple(sql);
			
			//递归算法
			for(DBRow oneResult:result){
				
				long parentid = oneResult.get("id", -1);
				
				DBRow[] tmpResult = getOfficeLocationList(id,parentid);
				
				if(tmpResult.length!=0){
					oneResult.add("children", tmpResult);
				}else{
					continue;
				}
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("FloorOfficeAddressMgrSlj.getOfficeLocationList() error:" + e);
		}
	}
	

}