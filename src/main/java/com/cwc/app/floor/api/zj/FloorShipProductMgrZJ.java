package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorShipProductMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 今天产生的所有外部运单号
	 * @return
	 */
	public DBRow[] getTodayShipProduct()
		throws Exception
	{
		try 
		{
			String sql = "select ship.delivery_code from "+ConfigBean.getStringValue("porder")+" p left join "+ConfigBean.getStringValue("ship_product")+" ship  on p.oid= ship.name where to_days(ship.post_date) =to_days(now())";
			return (dbUtilAutoTran.selectMutliple(sql));
		}
		catch (Exception e)
		{
			throw new Exception("FloorShipProductMgrZJ.getTodayShipProduct error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
