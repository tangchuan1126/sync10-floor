package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorContainerLoadingMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加容器与容器装载关系
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addContainerLoading(DBRow row)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("container_loading"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerLoadingMgrZJ addContainerLoading error:"+e);
		}
	}
	
	/**
	 * 删除容器装载关系
	 * @param con_id
	 * @param parent_con_id
	 * @throws Exception
	 */
	public void delContainerLoading(long con_id,long parent_con_id)
		throws Exception
	{
		try 
		{
			DBRow para = new DBRow();
			para.add("con_id",con_id);
			para.add("parent_id",parent_con_id);
			
			dbUtilAutoTran.deletePre("where con_id = ? and parent_con_id = ?",para,ConfigBean.getStringValue("container_loading"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerLoadingMgrZJ delContainerLoading error:"+e);
		}
	}
	
	/**
	 * 获得一个容器装载关系
	 * @param con_id
	 * @param parent_con_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailContainerLoadingByCon(long con_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("container_loading")+" where con_id = ?";
			
			DBRow para = new DBRow();
			para.add("con_id",con_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerLoadingMgrZJ getDetailContainerLoading error:"+e);
		}
	}
	
	/**
	 * 查询容器内装载的关系
	 * @param parent_con_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDetailContainerLoadingByParent(long parent_con_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("container_loading")+" where parent_con_id = ?";
			
			DBRow para = new DBRow();
			para.add("parent_con_id",parent_con_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerLoadingMgrZJ getDetailContainerLoadingByParent error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
