package com.cwc.app.floor.api;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorQuoteMgr
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBRow[] getAllQuotes(PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			
			String sql = "select * from " + ConfigBean.getStringValue("quote_order") + " order by qoid desc";
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectMutliple(sql,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectMutliple(sql);
			}

			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getAllQuotes() error:" + e);
		}
	}

	public DBRow[] getQuoteItemsByQoid(long qoid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			para.add("qoid",qoid);
			
			String sql = "select * from " + ConfigBean.getStringValue("quote_order_item") + " where qoid=? order by qoiid asc";
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getQuoteItemsByQoid() error:" + e);
		}
	}
		
	public DBRow getDetailQuoteByQoid(long qoid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("qoid",qoid);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("quote_order") + " where qoid=?",para));
//			return(dbUtilAutoTran.selectPreSingleCache("select * from " + ConfigBean.getStringValue("quote_order") + " where qoid=?",para,new String[]{ConfigBean.getStringValue("quote_order")}));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailQuoteByQoid(row) error:" + e);
		}
	}
	
	public DBRow getDetailQuoteItemByQoidPid(long qoid,long pid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("qoid",qoid);
			para.add("pid",pid);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("quote_order_item") + " where qoid=? and pid=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailQuoteItemByPid(row) error:" + e);
		}
	}
			
	public long addQuote(DBRow row)
		throws Exception
	{
		try
		{
			long qoid = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("quote_order"));
			row.add("qoid",qoid);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("quote_order"),row);
			return(qoid);
		}
		catch (Exception e)
		{
			throw new Exception("addQuote(row) error:" + e);
		}
	}
		
	public long addQuoteOrderItem(DBRow row)
		throws Exception
	{
		try
		{
			long qoiid = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("quote_order_item"));
			row.add("qoiid",qoiid);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("quote_order_item"),row);
			return(qoiid);
		}
		catch (Exception e)
		{
			throw new Exception("addQuoteOrderItem(row) error:" + e);
		}
	}
		
	public void modQuoteOrder(long qoid,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where qoid=" + qoid,ConfigBean.getStringValue("quote_order"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modQuoteOrder(row) error:" + e);
		}
	}
		
	public void delQuoteItem(long qoid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where qoid=" + qoid,ConfigBean.getStringValue("quote_order_item"));
		}
		catch (Exception e)
		{
			throw new Exception("delQuoteItem(row) error:" + e);
		}
	}
	
	public long addWholeSellDiscount(DBRow row)
		throws Exception
	{
		try
		{
			long wsd_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("wholesell_discount"));
			row.add("wsd_id",wsd_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("wholesell_discount"),row);
			return(wsd_id);
		}
		catch (Exception e)
		{
			throw new Exception("addWholeSellDiscount(row) error:" + e);
		}
	}
	
	public void delWholeSellDiscount(long wsd_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where wsd_id=" + wsd_id,ConfigBean.getStringValue("wholesell_discount"));
		}
		catch (Exception e)
		{
			throw new Exception("delWholeSellDiscount(row) error:" + e);
		}
	}
	
	public void modWholeSellDiscount(long wsd_id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where wsd_id=" + wsd_id,ConfigBean.getStringValue("wholesell_discount"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modWholeSellDiscount(row) error:" + e);
		}
	}
	
	public DBRow getDetailWholeSellDiscountByWsdId(long wsd_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("wsd_id",wsd_id);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("wholesell_discount") + " where wsd_id=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailWholeSellDiscountByWsdId(row) error:" + e);
		}
	}
		
	public DBRow[] getWholeSellDiscounts(PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql = "select * from " + ConfigBean.getStringValue("wholesell_discount") + "  order by wsd_id asc";
			DBRow row[] = dbUtilAutoTran.selectMutliple(sql,pc);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getWholeSellDiscounts() error:" + e);
		}
	}
		
	public DBRow getDetailWholeSellDiscountByName(String name)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("name",name);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("wholesell_discount") + " where name=?",para));
//			return(dbUtilAutoTran.selectPreSingleCache("select * from " + ConfigBean.getStringValue("wholesell_discount") + " where name=?",para,new String[]{ConfigBean.getStringValue("wholesell_discount")}));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailWholeSellDiscountByName(row) error:" + e);
		}
	}
	
	public DBRow getDetailWholeSellDiscountByImpClass(String imp_class)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("imp_class",imp_class);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("wholesell_discount") + " where imp_class=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailWholeSellDiscountByImpClass(row) error:" + e);
		}
	}

	public DBRow getDetailQuoteByOid(long oid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("oid",oid);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("quote_order") + " where oid=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailQuoteByQoid(row) error:" + e);
		}
	}
	
	
	public DBRow[] getSearchResult(String key,String st,String en,String create_account,int quote_status,int field,PageCtrl pc)
		throws Exception
	{
		try
		{
			String cond = "";
			
			DBRow para = new DBRow();
			para.add("st", st);
			para.add("en", en);
			
			if (create_account.equals("0")==false)
			{
				cond += " and create_account=?";
				para.add("create_account", create_account);
			}
			
			if (quote_status>-1)
			{
				cond += " and quote_status=?";
				para.add("quote_status", quote_status);
			}
			
			if (field == 1)
			{
				para.add("key", key);
				cond += " and qoid=?";
			}
			else if (field == 2)
			{
				para.add("key", key);
				cond += " and client_id=?";
			}
			else if (field == 3)
			{
				try
				{
					para.add("key", Long.parseLong(key));
					cond += " and oid=?";
				}
				catch(NumberFormatException e)
				{
					para.add("key", -1);
					cond += " and oid=?";
				}
			}
			
			String sql = "select * from " + ConfigBean.getStringValue("quote_order") + " where post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day)  "+cond+" order by qoid desc";
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getSearchResult() error:" + e);
		}
	}
	
	public DBRow[] getProductCountryProfitByPid(long pid,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product") + " p," + ConfigBean.getStringValue("product_profit") + " pp,"+ConfigBean.getStringValue("country_area_mapping")+" as cam," + ConfigBean.getStringValue("country_code") + " cc where pc_id=? and p.pc_id=pp.pid and cam.ca_id = pp.ca_id and cc.ccid=cam.ccid";            
			DBRow para = new DBRow();
			para.add("pid", pid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getProductCountryProfitByPid() error:" + e);
		}
	}

	public DBRow getDetailProductCountryProfitByPidCcId(long pid,long ccid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("ccid",ccid);
			para.add("pid",pid);
			
			
			String sql  = "select * from " + ConfigBean.getStringValue("product_profit") + " as pp "
			 +" join country_area_mapping as cam on cam.ca_id = pp.ca_id and cam.ccid = ?"
			 +" where pp.pid=?";
			
			return(dbUtilAutoTran.selectPreSingle(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailProductCountryProfitByPidCcId(row) error:" + e);
		}
	}
	
	public long addRetailPrice(DBRow row)
		throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("retail_price"));
			row.add("rp_id",id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("retail_price"),row);
			return(id);
		}
		catch (Exception e)
		{
			throw new Exception("addRetailPrice(row) error:" + e);
		}
	}
	
	public long addRetailPriceItems(DBRow row)
		throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("retail_price_items"));
			row.add("rpi_id",id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("retail_price_items"),row);
			return(id);
		}
		catch (Exception e)
		{
			throw new Exception("addRetailPriceItems(row) error:" + e);
		}
	}

	public DBRow[] getAllRetailPrice(PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("retail_price") + " order by rp_id desc";
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getAllRetailPrice() error:" + e);
		}
	}
		
	public DBRow[] getRetailPriceItemsByRpId(long rp_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("rp_id", rp_id);
			
			String sql = "select * from " + ConfigBean.getStringValue("retail_price_items") + " where rp_id=? order by rpi_id desc";
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getRetailPriceItemsByRpId() error:" + e);
		}
	}
		
	public DBRow getDetailRetailPriceByRpId(long rp_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("rp_id",rp_id);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("retail_price") + " where rp_id=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailQuoteByQoid(row) error:" + e);
		}
	}
	
	public DBRow getDetailRetailPriceItemByRpiId(long rpi_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("rpi_id",rpi_id);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("retail_price_items") + " where rpi_id=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailRetailPriceItemByRpiId(row) error:" + e);
		}
	}
		
	public long addProductProfit(DBRow row)
		throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("product_profit"));
			row.add("pp_id",id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("product_profit"),row);
			return(id);
		}
		catch (Exception e)
		{
			throw new Exception("addQuote(row) error:" + e);
		}
	}
		
	public void modRetailPriceByRpId(long rp_id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where rp_id=" + rp_id,ConfigBean.getStringValue("retail_price"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modRetailPriceByRpId(row) error:" + e);
		}
	}
		
	public void delProductProfitBypid(long pid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where pid=" + pid,ConfigBean.getStringValue("product_profit"));
		}
		catch (Exception e)
		{
			throw new Exception("delProductProfitBypid(row) error:" + e);
		}
	}
		
	public DBRow getDetailProductProfitByPidCcid(long pid,long ccid)
		throws Exception
	{
		try
		{
			
			String sql  = "select * from " + ConfigBean.getStringValue("product_profit") + " as pp "
			 +" join country_area_mapping as cam on cam.ca_id = pp.ca_id and cam.ccid = "+ccid
			 +" where pp.pid="+pid;
			
			return(dbUtilAutoTran.selectSingle(sql));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailProductProfitByPidCcid(row) error:" + e);
		}
	}
		
	public void modProductProfitByPidCaid(long pid,long ca_id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where pid=" + pid + " and ca_id="+ca_id,ConfigBean.getStringValue("product_profit"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modProductProfitByPidCcid(row) error:" + e);
		}
	}
		
	public int getQuoteItemCountByQoid(long qoid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("qoid",qoid);
			
			int c = dbUtilAutoTran.selectPreSingle("select count(qoiid) c from " + ConfigBean.getStringValue("quote_order_item") + " where qoid=?",para).get("c", 0);
			
			return(c);
		}
		catch (Exception e)
		{
			throw new Exception("getQuoteItemCountByQoid(row) error:" + e);
		}
	}

	public DBRow[] getNotProductProfitInSetBySetIdCcid(long set_id,long ccid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("set_id",set_id);
			para.add("ccid",ccid);
			
			String sql = "select * from  (select pc_id,p_name from "+ConfigBean.getStringValue("product_union")+"  pu,"+ConfigBean.getStringValue("product")+" p where p.pc_id=pu.pid and pu.set_pid=? order by p.pc_id desc) tmp  left join "+ConfigBean.getStringValue("product_profit")+" pp  on tmp.pc_id=pp.pid and pp.ccid=? where profit is null";
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getNotProductProfitInSetBySetIdCcid(row) error:" + e);
		}
	}
		
	public DBRow[] getServiceQuoteDiscountRange(long adgid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("adgid",adgid);
			
			String sql = "select a.account,a.adid,discount_range from "+ConfigBean.getStringValue("admin")+" a  left join "+ConfigBean.getStringValue("service_quote_discount")+" sqd on a.adid=sqd.adid where  a.adgid=? order by a.account asc";
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getServiceQuoteDiscountRange(row) error:" + e);
		}
	}
	
	public long addServiceQuoteDiscountRange(DBRow row)
		throws Exception
	{
		try
		{
			long qoid = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("service_quote_discount"));
			dbUtilAutoTran.insert(ConfigBean.getStringValue("service_quote_discount"),row);
			return(qoid);
		}
		catch (Exception e)
		{
			throw new Exception("addServiceQuoteDiscountRange(row) error:" + e);
		}
	}
		
	public void cleanServiceQuoteDiscountRange()
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete(null,ConfigBean.getStringValue("service_quote_discount"));
		}
		catch (Exception e)
		{
			throw new Exception("cleanServiceQuoteDiscountRange(row) error:" + e);
		}
	}
		
	public DBRow getDetailServiceQuoteDiscountRangeByAdid(long adid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("adid",adid);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("service_quote_discount") + " where adid=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailServiceQuoteDiscountRangeByAdid(row) error:" + e);
		}
	}
	
	public DBRow[] getCountryAreaByPid(long pid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("pid",pid);
			
			return(dbUtilAutoTran.selectPreMutliple("select ca.ca_id,name,pp.* from " + ConfigBean.getStringValue("product_profit") + " pp," + ConfigBean.getStringValue("country_area") + " ca where pp.pid=? and pp.ca_id=ca.ca_id group by ca.ca_id",para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getCountryAreaByPid(row) error:" + e);
		}
	}
	
	public DBRow[] getCountryByPidCaid(long pid,long ca_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("pid",pid);
			para.add("ca_id",ca_id);
			
			String sql = "select rpi.new_retail_price,c.c_country,profit,c_code,rpi.shipping_fee from " + ConfigBean.getStringValue("country_area") + " ca," + ConfigBean.getStringValue("country_area_mapping") + " cam," + ConfigBean.getStringValue("country_code") + " c," + ConfigBean.getStringValue("product_profit") + " pp left join " + ConfigBean.getStringValue("retail_price_items") + " rpi on pp.rpi_id=rpi.rpi_id where pp.pid=? and pp.ccid=cam.ccid and cam.ca_id=ca.ca_id and ca.ca_id=? and pp.ccid=c.ccid";
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getCountryByPidCaid(row) error:" + e);
		}
	}
			
	public void updateRetailPrice(DBRow updateRetailPriceRow ,long rp_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("retail_price") ;
			dbUtilAutoTran.update(" where rp_id="+rp_id, tableName, updateRetailPriceRow);
		}catch (Exception e) {
			throw new Exception("updateRetailPrice(updateRetailPriceRow , rp_id) error:" + e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
