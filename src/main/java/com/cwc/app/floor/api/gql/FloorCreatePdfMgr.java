package com.cwc.app.floor.api.gql;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;


public class FloorCreatePdfMgr {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 增加
	 * @param dbUtilAutoTran
	 * @throws Exception 
	 */
	public Long Add(DBRow data) throws Exception{
		try{
    		String tablename=ConfigBean.getStringValue("pdf_file_relationship");
			return dbUtilAutoTran.insertReturnId(tablename, data); 
    	}catch(Exception e){
    		throw new Exception("FloorCreatePdfMgr Add"+e);
    	}
	}
	/**
	 * 修改
	 * @param dbUtilAutoTran
	 */
	public int Update(Long id,DBRow data) throws Exception{
		try{
    		String tablename=ConfigBean.getStringValue("pdf_file_relationship");
			return dbUtilAutoTran.update(" where id="+id, tablename, data); 
    	}catch(Exception e){
    		throw new Exception("FloorCreatePdfMgr Update"+e);
    	}
	}
	/**
	 * 删除
	 * @param dbUtilAutoTran
	 */
	public int Delete(Long id) throws Exception{
		try{
    		String tablename=ConfigBean.getStringValue("pdf_file_relationship");
			return dbUtilAutoTran.delete(" where id="+id, tablename); 
    	}catch(Exception e){
    		throw new Exception("FloorCreatePdfMgr Delete"+e);
    	}
	}
	/**
	 * 查询 
	 * @param dbUtilAutoTran
	 */
	public DBRow Search(Long id)throws Exception{
		try{
    		String tablename=ConfigBean.getStringValue("pdf_file_relationship");
    		String sql=" select * from "+tablename+" where id="+id;
			return dbUtilAutoTran.selectSingle(sql); 
    	}catch(Exception e){
    		throw new Exception("FloorCreatePdfMgr Search(id)"+e);
    	}
	}
	public DBRow[] Search(String adid,int module_type,int pdf_type)throws Exception{
		try{
    		String tablename=ConfigBean.getStringValue("pdf_file_relationship");
    		String sql=" select * from "+tablename+" where adid="+adid+" and module_type="+module_type+" and associate_type="+pdf_type;
			return dbUtilAutoTran.selectMutliple(sql); 
    	}catch(Exception e){
    		throw new Exception("FloorCreatePdfMgr Search(adid,module_type,pdf_type)"+e);
    	}
	}
	public DBRow[] Search(String adid,int module_type,int pdf_type,Long associate_id)throws Exception{
		try{
    		String tablename=ConfigBean.getStringValue("pdf_file_relationship");
    		String sql=" select * from "+tablename+" where adid="+adid+" and module_type="+module_type+" and associate_type="+pdf_type+" and associate_id="+associate_id;
    		return dbUtilAutoTran.selectMutliple(sql); 
    	}catch(Exception e){
    		throw new Exception("FloorCreatePdfMgr Search(adid,module_type,pdf_type,associate_id)"+e);
    	}
	}
	
	public DBRow[] SearchBy(Long associate_id)throws Exception{
		try{
    		String tablename=ConfigBean.getStringValue("pdf_file_relationship");
    		String sql=" select * from "+tablename+" where associate_id="+associate_id;
    		return dbUtilAutoTran.selectMutliple(sql); 
    	}catch(Exception e){
    		throw new Exception("FloorCreatePdfMgr Search(adid,module_type,pdf_type,associate_id)"+e);
    	}
	}
    public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) 
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
