package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorProductStoreEditLogMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;

	public long addProductStoreEditLog(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return (dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_store_edit_log"),dbrow));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreEditLogMgrZJ addProductStoreEditLog error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
