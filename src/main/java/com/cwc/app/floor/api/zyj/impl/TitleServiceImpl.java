package com.cwc.app.floor.api.zyj.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.cwc.app.floor.api.zyj.model.Title;
import com.cwc.app.floor.api.zyj.service.TitleService;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;


public class TitleServiceImpl implements TitleService{
	@Autowired
	private DBUtilAutoTran dbUtilAutoTran;
	/**
	 * 获得所有的生产商
	 * @return 如果有，返回所有生产商;如果没有，返回空集合
	 */
	public List<Title> getAll(){
		List<Title> list = new ArrayList<Title>();
		try {
			DBRow[] rows = this.dbUtilAutoTran.selectMutliple("select * from title");
			if(rows != null && rows.length > 0){
				for(DBRow row : rows){
					list.add(this.convertToTitle(row));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * 获取与特定品牌商相关的所有Title
	 * @param customerId 品牌商ID
	 * @return
	 */
	public List<Title>  getTitlesByCustomerId(int customerId){
		List<Title> list = new ArrayList<Title>();
		if(customerId == 0){
			list = this.getAll();
		}else{
			try {
				DBRow param_row = new DBRow();
				param_row.add("tp_customer_id", customerId);
				DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple("select distinct a.tp_title_id title_id,t.title_name from title_product a left join title t on a.tp_title_id=t.title_id  where a.tp_customer_id=?",param_row);
				
				if(rows != null && rows.length > 0){
					for(int i = 0; i < rows.length; i++){
						list.add(this.convertToTitle(rows[i]));
					}
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}

		return list;			
	}
	
	/**
	 * 获取与指定产品和CLP Type 的所有Title
	 * @param product
	 * @param clpTypeId
	 * @return
	 */
	public List<Title> getTitlesByProdAndCLP(int productId,int clpTypeId){
		List<Title> list = new ArrayList<Title>();
		try {
			DBRow param_row = new DBRow();
			//从lp_title_ship_to 表中查询  与 指定的product,clpType相关的且Title,Customer 为 所有的记录条数
			StringBuilder customer_count_sql = new StringBuilder("select count(1) totalCount from  lp_title_ship_to a    where 	a.lp_type_id =" + clpTypeId + " and a.lp_pc_id =" + productId + " and a.customer_id=0 and a.title_id=0");  
			DBRow row = this.dbUtilAutoTran.selectSingle(customer_count_sql.toString());
			if(row != null && row.get("totalCount", 1) > 0){
				list = this.getTitles(productId);
			}else{
				StringBuilder sql = new StringBuilder();
				sql.append("SELECT DISTINCT	c.title_id,c.title_name FROM lp_title_ship_to a INNER JOIN title c ON a.title_id = c.title_id");
				sql.append(" WHERE a.customer_id>0 and	a.lp_type_id = " + clpTypeId + " AND a.lp_pc_id =" + productId);
				sql.append(" UNION");
				
				sql.append(" SELECT DISTINCT	c.title_id,c.title_name FROM lp_title_ship_to a INNER JOIN title c ON a.title_id = c.title_id");
				sql.append(" WHERE a.customer_id=0 and	a.lp_type_id = " + clpTypeId + " AND a.lp_pc_id =" + productId);
				sql.append(" UNION");			
				
				sql.append(" SELECT distinct c.title_id,c.title_name from title_product  t ");
				sql.append(" INNER join lp_title_ship_to a  on t.tp_pc_id=a.lp_pc_id and t.tp_customer_id = a.customer_id ");
				sql.append(" INNER join title c ON t.tp_title_id = c.title_id ");
				sql.append(" WHERE a.lp_pc_id=" + productId  +" and a.lp_type_id =" + clpTypeId + " and  a.customer_id>0 and a.title_id=0");
				
				
				DBRow[] rows = this.dbUtilAutoTran.selectMutliple(sql.toString());
				if(rows != null && rows.length > 0){
					for(int i = 0; i < rows.length; i++){
						list.add(this.convertToTitle(rows[i]));
					}
				}				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;		
	}
	
	/**
	 * 获取与指定产品和CLP Type以及 customer 相关 的所有Title
	 * @param productId
	 * @param clpTypeId
	 * @param customerId  
	 * @return
	 */
	public List<Title> getTitlesByProdAndCLP(int productId,int clpTypeId,int customerId){
		List<Title> list = new ArrayList<Title>();
		if(customerId == 0){
			list = this.getTitlesByProdAndCLP(productId, clpTypeId);
		}else{
			try {
				DBRow param_row = new DBRow();
				//从lp_title_ship_to 表中查询  与 指定的product,clpType相关的且Title,Customer 为 所有的记录条数
				StringBuilder customer_count_sql = new StringBuilder("select count(1) totalCount from  lp_title_ship_to a    where 	a.lp_type_id =" + clpTypeId + " and a.lp_pc_id =" + productId + " and a.customer_id=0 and a.title_id=0");  
				DBRow row = this.dbUtilAutoTran.selectSingle(customer_count_sql.toString());
				if(row != null && row.get("totalCount", 1) > 0){
					list = this.getTitles(productId,customerId);
				}else{
					StringBuilder sql = new StringBuilder();
					sql.append(" SELECT DISTINCT	c.title_id,c.title_name FROM lp_title_ship_to a INNER JOIN title c ON a.title_id = c.title_id");
					sql.append(" WHERE a.customer_id=" + customerId + " and	a.lp_type_id = " + clpTypeId + " AND a.lp_pc_id =" + productId);
					sql.append(" UNION");
					sql.append(" SELECT DISTINCT c.title_id,c.title_name FROM lp_title_ship_to a ");
					sql.append(" INNER JOIN title_product t on a.lp_pc_id=t.tp_pc_id and a.title_id=t.tp_title_id");
					sql.append(" INNER JOIN title c ON t.tp_title_id = c.title_id");
					sql.append(" WHERE	a.customer_id = 0	AND a.lp_type_id =" + clpTypeId + "	AND a.lp_pc_id =" + productId + " AND t.tp_customer_id=" + customerId); 
					sql.append(" UNION");			
					sql.append(" SELECT distinct c.title_id,c.title_name from title_product  t ");
					sql.append(" INNER join lp_title_ship_to a  on t.tp_pc_id=a.lp_pc_id and t.tp_customer_id = a.customer_id ");
					sql.append(" INNER join title c ON t.tp_title_id = c.title_id ");
					sql.append(" WHERE a.lp_pc_id=" + productId  +" and a.lp_type_id =" + clpTypeId + " and  a.customer_id=" + customerId + " and a.title_id=0");
					DBRow[] rows = this.dbUtilAutoTran.selectMutliple(sql.toString());
					if(rows != null && rows.length > 0){
						for(int i = 0; i < rows.length; i++){
							list.add(this.convertToTitle(rows[i]));
						}
					}				
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}		
		}
		return list;					
	}
	
	/**
	 * 获取与特定产品相关的所有Title
	 * @param productId 产品ID
	 * @return
	 */
	public List<Title>  getTitles(int productId){
		List<Title> list = new ArrayList<Title>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple(" SELECT distinct title_id,title_name FROM title_product p LEFT JOIN  title t on  p.tp_title_id=t.title_id where p.tp_pc_id=?",param_row);
			
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToTitle(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;			
		
		
	}
	
	/**
	 * 获取与特定产品相关的所有Title
	 * @param productId 产品ID
	 * @return
	 */
	public List<Title>  getTitles(int productId,int customerId){
		List<Title> list = new ArrayList<Title>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			param_row.add("tp_customer_id", customerId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple(" SELECT distinct title_id,title_name FROM title_product p LEFT JOIN  title t on  p.tp_title_id=t.title_id where p.tp_pc_id=? and  p.tp_customer_id=?",param_row);
			
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToTitle(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;			
	}
	
	/**
	 * 将DBRow 对象转变为对应的 CustomerId对象
	 * @param row
	 * @return
	 */
	private Title convertToTitle(DBRow row){
		Assert.notNull(row);
		Title title = new Title();
		title.setId(row.get("title_id", 1));
		title.setName( row.get("title_name", "") );
		return title;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
}
