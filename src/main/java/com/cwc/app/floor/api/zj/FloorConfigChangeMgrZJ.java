package com.cwc.app.floor.api.zj;

import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.JqGridFilterKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorConfigChangeMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 添加ConfigChange
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addConfigChange(DBRow row)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("config_change"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorConfigChangeMgrZJ addConfigChange error:"+e);
		}
	}
	
	/**
	 * 修改ConfigChange
	 * @param cc_id
	 * @param para
	 * @throws Exception
	 */
	public void modConfigChange(long cc_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where cc_id="+cc_id,ConfigBean.getStringValue("config_change"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorConfigChangeMgrZJ modConfigChange error:"+e);
		}
	}
	
	/**
	 * 删除ConfigChange
	 * @param cc_id
	 * @throws Exception
	 */
	public void delConfigChange(long cc_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where cc_id = "+cc_id, ConfigBean.getStringValue("config_change"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorConfigChangeMgrZJ delConfigChange error:"+e);
		}
	}
	
	
	/**
	 * 添加CC明细
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addConfigChangeItem(DBRow row)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("config_change_item"),row);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorConfigChangeMgrZJ addConfigChangeItem error:"+e);
		}
	}
	
	/**
	 * 修改CC明细
	 * @param cci_id
	 * @param para
	 * @throws Exception
	 */
	public void modConfigChangeItem(long cci_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where cci_id = "+cci_id,ConfigBean.getStringValue("config_change_item"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorConfigChangeMgrZJ modConfigChangeItem error:"+e);
		}
	}
	
	/**
	 * 删除CC明细
	 * @param cci_id
	 * @throws Exception
	 */
	public void delConfigChangeItem(long cci_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where cci_id = "+cci_id,ConfigBean.getStringValue("config_change_item"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorConfigChangeMgrZJ delCOnfigChangeItem error:"+e);
		}
	}
	
	/**
	 * 根据CCID删除cc明细
	 * @param cc_id
	 * @throws Exception
	 */
	public void delConfigChangeItemByCcid(long cc_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where cci_cc_id = "+cc_id,ConfigBean.getStringValue("config_change_item"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorConfigChangeMgrZJ eclConfigChangeItemByCcid error:"+e);
		}
	}
	
	/**
	 * 过滤ConfigChange
	 * @param ps_id
	 * @param title_id
	 * @param cc_status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchConfigChange(long ps_id,long title_id,int cc_status,PageCtrl pc)
		throws Exception
	{
		try 
		{
			DBRow para = new DBRow();
			para.add("1",1);
			
			String wherePs = "";
			if (ps_id != 0)
			{
				wherePs = " and cc_psid = ? ";
				para.add("ps_id",ps_id);
			}
			
			String whereStatus = "";
			if (cc_status != 0)
			{
				whereStatus = " and cc_status = ? ";
				para.add("cc_status",cc_status);
			}
			
			String whereTitle = "";
			if (title_id !=0 )
			{
				whereTitle = " and cc_title = ? ";
				para.add("cc_title",title_id);
			}
			
			String sql = "select * from "+ConfigBean.getStringValue("config_change")+" where 1=? "+wherePs+whereStatus+whereTitle;
			
			return dbUtilAutoTran.selectPreMutliple(sql, para,pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorConfigChangeMgrZJ getSearchConfigChange error:"+e);
		}
	}
	
	public DBRow getDetailConfigChangeByCcid(long cc_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("config_change")+" where cc_id = ? ";
			
			DBRow para = new DBRow();
			para.add("cc_id",cc_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorConfigChangeMgrZJ getDetailConfigChangeByCcid error:"+e);
		}
	}
	
	/**
	 * 根据cc_id获得CC明细
	 * @param cc_id
	 * @param pc
	 * @param sidx
	 * @param sord
	 * @param fillterBean
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getConfigChangeItemByCcid(long cc_id,PageCtrl pc,String sidx,String sord,FilterBean fillterBean)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from  "+ConfigBean.getStringValue("config_change_item")+" as cci " 
					   +"left join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = cci.cci_pc_id " 
					   +"left join "+ConfigBean.getStringValue("product_code")+" as pcode on pcode.pc_id = p.pc_id and pcode.code_type="+CodeTypeKey.Main+" "
					   );
			DBRow para = new DBRow();
			if(cc_id > 0)
			{
				sql.append("where cci.cci_cc_id = ?");
				para.add("cc_id",cc_id);
			}
			if(fillterBean !=null)
			{
				sql.append(new JqGridFilterKey().filterSQL(fillterBean));
			}
			
			if(sidx==null||sord==null)
			{
				sql.append(" order by cci_cc_id desc");
			}
			else
			{
				sql.append(" order by "+sidx+" "+sord);
			}
			
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(), para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorConfigChangeMgrZJ getConfigChangeItemByCcid error:"+e);
		}
	}
	
	/**
	 * 获得ConfigChangeItem
	 * @param cci_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailConfigChangeItem(long cci_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("config_change_item")+" where cci_id = ? ";
			
			DBRow para = new DBRow();
			para.add("cci_id",cci_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorConfigChangeMgrZJ getDetailConfigChangeItem error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
