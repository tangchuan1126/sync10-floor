package com.cwc.app.floor.api.zr;

import com.cwc.app.key.OccupyStatusTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

/**
 * 这个类查询的门肯定是好的门
 * @author win7zr
 *FloorStorageDoorZr
 */
public class FloorStorageDoorZr {

	private static final String appendDoorOk = " and sd_status = 0 ";
	private DBUtilAutoTran dbUtilAutoTran;
	
 	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
	/**
	 * 根据psId 和 门的状态去查询门
	 * @param ps_id
	 * @param occupied_status
	 * @param zone_id(可以为0)
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public DBRow[] getDoorByPsIdAndOccupiedStatusZoneId(long ps_id , int occupied_status ,long zone_id) throws Exception{
		try{
			String sql = "select * from storage_door where ps_id="+ ps_id + appendDoorOk + " and occupied_status="+ occupied_status ;
			if(zone_id != 0l){
				sql += " and area_id ="+zone_id ;
			}
			return dbUtilAutoTran.selectMutliple(sql);
 		}catch (Exception e) {
			throw new Exception("FloorStorageDoorZr.getFreeDoor(ps_id):"+e);
		}
	}
	
	/**
	 * 更新门
	 * @param sd_id
	 * @param row
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public void updateDoor(long sd_id , DBRow row) throws Exception{
		try{
			dbUtilAutoTran.update(" where sd_id="+sd_id, "storage_door", row);
		}catch(Exception e){
			throw new Exception("FloorStorageDoorZr.updateDoor(sd_id,row):"+e);
		}
	}
	
	public DBRow[] getDoorsBySelf(long associate_id ,  int associate_type , long zone_id) throws Exception{
		try{
			String sql  = "select * from storage_door where associate_type=" +associate_type + " and associate_id="+associate_id ;
			if( zone_id != 0l){
				sql += " and area_id="+zone_id ;
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorStorageDoorZr.getDoorsBy(associate_id,associate_type):"+e);
		}
	}
	public DBRow[] getUsedDoors(long ps_id , long zone_id) throws Exception{
		try{
			String sql = "select * from storage_door where (occupied_status="+ OccupyStatusTypeKey.OCUPIED + " or occupied_status="+OccupyStatusTypeKey.RESERVERED+")" ;
			sql += (" and ps_id="+ps_id );
			if(zone_id != 0l){
				sql += " and area_id="+zone_id ;
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorStorageDoorZr.getUsedDoors(ps_id,zone_id):"+e);
		}
	}
	public DBRow getCheckInModuleDoorInfo(long sd_id ) throws Exception{
		try{
			String sql = "SELECT main.* FROM	storage_door AS sd LEFT JOIN door_or_location_occupancy_main AS main ON sd.associate_id = main.dlo_id "  ;
			      sql += " WHERE sd.associate_type = "+ModuleKey.CHECK_IN+" and sd.occupied_status <> "+OccupyStatusTypeKey.RELEASED+" and  sd.sd_id ="+sd_id ;
		   return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorStorageDoorZr.getCheckInModuleDoorInfo(sd_id):"+e);
		}
	}
	public DBRow getDoorInfoBySdId(long sd_id) throws Exception{
		try{
			return dbUtilAutoTran.selectSingle("select * from storage_door where sd_id=" +sd_id );
		}catch(Exception e){
			throw new Exception("FloorStorageDoorZr.getDoorInfoBySdId(sd_id):"+e);
		}
	}
	/**
	 * 当前associate_id +　associate_type　占用和保留的门
	 * @param associate_id
	 * @param associate_type
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月17日
	 */
	public DBRow[] getUseDoorByAssociateIdAndType(long associate_id ,  int associate_type ) throws Exception{
		try{
			String sql = "select * from storage_door where associate_type = " +ModuleKey.CHECK_IN + ""
					+ " and ( occupied_status = "+OccupyStatusTypeKey.OCUPIED +""
					+ " or occupied_status = "+OccupyStatusTypeKey.RESERVERED+") and associate_id="+associate_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorStorageDoorZr.getUseDoorBy(associate_id + associate_type):"+e);
		}
	}
	public void freeDoorsByAssociateId(long associate_id , int associate_type , DBRow updateRow) throws Exception{
		try{
			dbUtilAutoTran.update("where associate_type ="+ associate_type + " and associate_id="+associate_id, "storage_door", updateRow);
		}catch(Exception e){
			throw new Exception("FloorStorageDoorZr.freeDoorsByAssociateId(associate_id + associate_type):"+e);
		}
	}
	
	/**
	 * 通过门ID，关联信息查询门
	 * @param doorId
	 * @param associate_id
	 * @param associate_type
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月20日 上午9:07:33
	 */
	public DBRow findDoorByDoorIdAssociation(long doorId,  int associate_type, long associate_id) throws Exception
	{
		try{
			return dbUtilAutoTran.selectSingle("select * from storage_door where associate_type ="+ associate_type + " and associate_id="+associate_id+" and sd_id = "+ doorId);
		}catch(Exception e){
			throw new Exception("FloorStorageDoorZr.findDoorByDoorIdAssociation(associate_id + associate_type):"+e);
		}
	}
	
	public long addCheckInLog(DBRow row) throws Exception {
		try {
 			return  dbUtilAutoTran.insertReturnId("check_in_log", row);
		} catch (Exception e) {
			throw new Exception("FloorStorageDoorZr addCheckInLog error:"+e);
		}
	}
	
	public DBRow[] getDoorsCheckInModuleByAssociateId(long associate_id,
			int occupancy_status ) throws Exception{
		try{
			String sql = "select * from storage_door where  associate_type="+ModuleKey.CHECK_IN+" and occupied_status="+occupancy_status+" and associate_id="+associate_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorStorageDoorZr getDoorsCheckInModuleByAssociateId error:"+e);
		}
	}
	
	
}
