package com.cwc.app.floor.api.zj;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.key.ApplyStatusKey;
import com.cwc.app.key.ApplyTypeKey;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.JqGridFilterKey;
import com.cwc.app.key.PurchaseArriveKey;
import com.cwc.app.key.PurchaseKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorPurchaseMgr {
	
	private DBUtilAutoTran dbUtilAutoTran;
	//采购单
	/**
	 * 生成采购单
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addPurchase(DBRow dbrow) 
		throws Exception
	{
		try 
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("purchase"));
			dbrow.add("purchase_id",id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("purchase"),dbrow);
			
			return (id);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.addPurchase error:"+e);
		}
	}
	
	public void updatePurchaseBasic(DBRow dbrow, long purchase_id)throws Exception{
		try 
		{
			dbUtilAutoTran.update(" where purchase_id="+purchase_id, ConfigBean.getStringValue("purchase"), dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.updatePurchaseBasic error:"+e);
		}
	}
	
	/**
	 * 根据条件过滤采购单
	 * @param pc
	 * @param para
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseByPara(PageCtrl pc,String cmd,int type,long supplier_id)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("purchase")+" where 1=1 ");
			
			DBRow param = new DBRow();
			param.add("status",type);
			
			DBRow[] purchases = null;
			
			
			
			if(cmd!=null&&!cmd.equals(""))
			{
				StringBuffer ss= new StringBuffer("select * from purchase where purchase_id in (select purchase_id from purchase_detail where purchase_id in ( select purchase_id from purchase  where "+cmd+" = ? and purchase_status !=4 and purchase_status !=3) group by purchase_id)");
				
				if(supplier_id!=0)
				{
					ss.append(" and supplier = "+supplier_id);
				}
				ss.append(" order by purchase_id desc");
				
				purchases = dbUtilAutoTran.selectPreMutliple(ss.toString(), param, pc);
			}
			else
			{
				if(supplier_id!=0)
				{
					sql.append(" and supplier = "+supplier_id);
				}
				
				sql.append(" order by purchase_id desc");
				purchases = dbUtilAutoTran.selectMutliple(sql.toString(),pc);
			}
			return purchases;
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getPurchaseByPara error:"+e);
		}
	}
	
	/**
	 * 根据关键字搜索采购单（商品名或采购单ID号）
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchPurchaseBySupplierOrId(String key,PageCtrl pc)
		throws Exception
	{
		try 
		{
			DBRow para = new DBRow();
			para.add("sup_name","'%"+key+"%'");
			String purchase_id = "";
			
			if (key.substring(0,1).toUpperCase().equals("P"))
			{
				String regEx = "[^0-9]";
				Pattern p = Pattern.compile(regEx);
				Matcher m = p.matcher(key);
				if (!m.replaceAll("").equals("")) 
				{
					purchase_id = m.replaceAll("");
				}
				
				if(purchase_id.equals("")|| (key.length() - purchase_id.length()) != 1) 
				{
					purchase_id = key;
				}
			}
			else
			{
				purchase_id = key;
			}
			String sql = "select p.*,coalesce(am.`status`,"+ApplyStatusKey.CANNOT_TRANSFERS+") as moneyStatus, coalesce(am.amount,0) as amount,coalesce(am.amount_transfer,0) as amount_transfer from "+ConfigBean.getStringValue("purchase")+" as p ,"+ConfigBean.getStringValue("supplier")+" as s,"+ConfigBean.getStringValue("apply_money")+" as am where am.association_id=p.purchase_id and types="+ApplyTypeKey.PURCHASE+" and p.supplier = s.id and s.sup_name like '%"+key+"%'"
			+" union select p.*,coalesce(am.`status`,"+ApplyStatusKey.CANNOT_TRANSFERS+") as moneyStatus, coalesce(am.amount,0) as amount,coalesce(am.amount_transfer,0) as amount_transfer from "+ConfigBean.getStringValue("purchase")+" as p left join "+ConfigBean.getStringValue("apply_money")+" as am  on association_id=purchase_id and types="+ApplyTypeKey.PURCHASE+" where purchase_id like '%"+purchase_id+"%'"
			+" order by purchase_id desc";//order by 在union最后一个子查询语句就可
						
			return (dbUtilAutoTran.selectMutliple(sql,pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.searchPurchaseBySupplierOrId error:"+e);
		}
	}
	
	
	/**
	 * 供应商采购单检索（只在属于此供应商的采购单内根据输入内容查询）
	 * @param key
	 * @param pc
	 * @param supplier_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] supplierSearchPurchaseBySupplierOrId(String key,PageCtrl pc,long supplier_id)
		throws Exception
	{
		try 
		{
			DBRow para = new DBRow();
			para.add("sup_name","'%"+key+"%'");
			String purchase_id = "";
			
			if (key.substring(0,1).toUpperCase().equals("P"))
			{
				String regEx = "[^0-9]";
				Pattern p = Pattern.compile(regEx);
				Matcher m = p.matcher(key);
				if (!m.replaceAll("").equals("")) 
				{
					purchase_id = m.replaceAll("");
				}
				
				if(purchase_id.equals("")|| (key.length() - purchase_id.length()) != 1) 
				{
					purchase_id = key;
				}
			}
			else
			{
				purchase_id = key;
			}
			String sql = "select p.* from "+ConfigBean.getStringValue("purchase")+" as p ,"+ConfigBean.getStringValue("supplier")+" as s where p.supplier = s.id and s.sup_name like '%"+key+"%' and p.purchase_status !="+PurchaseKey.CANCEL
			+" union select * from "+ConfigBean.getStringValue("purchase")+" where purchase_id like '%"+purchase_id+"%' and supplier = "+supplier_id+" and purchase_status !="+PurchaseKey.CANCEL
			+" order by purchase_id desc";//order by 在union最后一个子查询语句就可
						
			return (dbUtilAutoTran.selectMutliple(sql,pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.searchPurchaseBySupplierOrId error:"+e);
		}
	}
	/**
	 * 需跟进采购单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseByfollowup(PageCtrl pc,long ps_id,int day)
		throws Exception
	{
		try 
		{
			TDate tDate = new TDate();
			tDate.addDay(-day);
			
			StringBuffer sql = new StringBuffer("select p.*,coalesce(am.`status`,"+ApplyStatusKey.CANNOT_TRANSFERS+") as moneyStatus from "+ConfigBean.getStringValue("purchase")+" as p left join "+ConfigBean.getStringValue("apply_money")+" as am  on association_id=purchase_id and types="+ApplyTypeKey.PURCHASE+" where updatetime<= '"+tDate.formatDate("yyyy-MM-dd")+"' and purchase_status !="+PurchaseKey.FINISH+" and purchase_status !="+PurchaseKey.CANCEL);
			
			if(ps_id!=0)
			{
				sql.append(" and ps_id = "+ps_id);
			}
			sql.append(" order by purchase_id desc");
			
			return (dbUtilAutoTran.selectMutliple(sql.toString(),pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getPurchaseByfollowup error"+e);
		}
	}
	
	/**
	 * 采购单过滤
	 * @param supplier_id
	 * @param purchase_status
	 * @param money_status
	 * @param beginTime
	 * @param endTime
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseFillter(long supplier_id,long ps_id,int purchase_status,int money_status,int arrival_time,String beginTime,String endTime,PageCtrl pc,int day, long productline_id,int invoice,int drawback,int need_tag,int quality_inspection)
		throws Exception
	{
		try {
			StringBuffer sql = new StringBuffer("select p.*,coalesce(am.`status`,"+ApplyStatusKey.CANNOT_TRANSFERS+") as moneyStatus, coalesce(am.amount,0) as amount,coalesce(am.amount_transfer,0) as amount_transfer from "+ConfigBean.getStringValue("purchase")+" as p"
											   +" left join "+ConfigBean.getStringValue("apply_money")+" as am  on association_id=purchase_id and types=100001"
											   +" where 1=1");
			TDate tDate = new TDate();
			tDate.addDay(-day);
			if(day!=0)
			{
				sql.append(" and updatetime<= '"+tDate.formatDate("yyyy-MM-dd")+"' ");
			}
			
			if(productline_id != 0 && supplier_id==0) {
				sql.append(" and supplier in (select id from supplier where product_line_id = " + productline_id + ")");
			}
			
			if(supplier_id!=0)
			{
				sql.append(" and supplier = "+supplier_id);
			}
			
			if(ps_id !=0)
			{
				sql.append(" and ps_id = "+ps_id);
			}
			
			if(purchase_status != 0)
			{
				if(purchase_status == 10)
					sql.append(" and purchase_status in("+PurchaseKey.OPEN + "," + PurchaseKey.AFFIRMPRICE + "," + PurchaseKey.AFFIRMTRANSFER + "," + PurchaseKey.APPROVEING + ")");
				else
					sql.append(" and purchase_status = "+purchase_status);
			}
			
			if(money_status !=-1)
			{
				sql.append(" and am.`status` = "+money_status);
			}
			if(arrival_time !=0)
			{
				sql.append(" and arrival_time = "+arrival_time);
			}
			if(beginTime !=null && !beginTime.equals(""))
			{
				sql.append(" and purchase_date >= '"+beginTime+" 00:00:00'");
			}
			if(endTime != null && !endTime.equals(""))
			{
				sql.append(" and purchase_date <= '"+endTime+" 23:59:59'");
			}
			
			if(invoice !=0)
			{
				sql.append(" and invoice = "+invoice);
			}
			
			if(drawback !=0)
			{
				sql.append(" and drawback = "+drawback);
			}
			
			if(need_tag !=0)
			{
				sql.append(" and need_tag = "+need_tag);
			}
			
			if(quality_inspection != 0 )
			{
				sql.append(" and quality_inspection ="+quality_inspection);
			}
			
			sql.append(" order by purchase_id desc");
			
			return (dbUtilAutoTran.selectMutliple(sql.toString(),pc));
		} 
		catch(Exception e)
		{
			throw new Exception("FloorPurchaseMgr.getPurchaseFillter error:"+e);
		}
	}
	
	public DBRow[] getPurchaseSupplierFillter(long supplier_id,long ps_id,int purchase_status,int money_status,int arrival_time,String beginTime,String endTime,PageCtrl pc)
		throws Exception
	{
		try {
			StringBuffer sql = new StringBuffer("select p.*,coalesce(am.`status`,"+ApplyStatusKey.CANNOT_TRANSFERS+") as moneyStatus from "+ConfigBean.getStringValue("purchase")+" as p"
											   +" left join "+ConfigBean.getStringValue("apply_money")+" as am  on association_id=purchase_id and types="+ApplyTypeKey.PURCHASE
											   +" where 1=1 and p.purchase_status!="+PurchaseKey.CANCEL);
			
			if(supplier_id!=0)
			{
				sql.append(" and supplier = "+supplier_id);
			}
			
			if(ps_id !=0)
			{
				sql.append(" and ps_id = "+ps_id);
			}
			
			if(purchase_status != 0)
			{
				sql.append(" and purchase_status = "+purchase_status);
			}
			
			if(money_status !=-1)
			{
				sql.append(" and am.`status` = "+money_status);
			}
			if(arrival_time !=0)
			{
				sql.append(" and arrival_time = "+arrival_time);
			}
			if(beginTime !=null)
			{
				sql.append(" and purchase_date >= '"+beginTime+" 00:00:00'");
			}
			if(endTime !=null)
			{
				sql.append(" and purchase_date <= '"+endTime+" 23:59:59'");
			}
			
			sql.append(" order by purchase_id desc");
			return (dbUtilAutoTran.selectMutliple(sql.toString(),pc));
		} 
		catch(Exception e)
		{
			throw new Exception("FloorPurchaseMgr.getPurchaseSupplierFillter error:"+e);
		}
	}
	
	/**
	 * 已完成采购单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseByfinish(PageCtrl pc,long supplier_id)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("purchase")+" where purchase_status = 4");
			
			if(supplier_id!=0)
			{
				sql.append(" and supplier = "+supplier_id);
			}
			sql.append(" order by purchase_id desc");
			
			return (dbUtilAutoTran.selectMutliple(sql.toString(),pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getPurchaseByfinish error:"+e);
		}
	}
	/**
	 * 已取消采购单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseBycancel(PageCtrl pc,long supplier_id)
	throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("purchase")+" where purchase_status =  3");
			
			if(supplier_id != 0)
			{
				sql.append(" and supplier = "+supplier_id);
			}
			sql.append(" order by purchase_id desc");
			
			return (dbUtilAutoTran.selectMutliple(sql.toString(),pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getPurchaseByfinish error:"+e);
		}
	}
	
	/**
	 * 未到货采购单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseNoArrive(PageCtrl pc,long supplier_id)
		throws Exception
	{
		StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("purchase")+" where arrival_time ="+PurchaseArriveKey.NOARRIVE);
		
		if(supplier_id!=0)
		{
			sql.append(" and supplier = "+supplier_id);
		}
		sql.append(" order by purchase_id desc");
		return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
	}
	
	/**
	 * 部分到货采购单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchasePartArrive(PageCtrl pc,long supplier_id)
		throws Exception
	{
		StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("purchase")+" where arrival_time ="+PurchaseArriveKey.PARTARRIVE);
		
		if(supplier_id!=0)
		{
			sql.append(" and supplier = "+supplier_id);
		}
		sql.append(" order by purchase_id desc");
		
		return (dbUtilAutoTran.selectMutliple(sql.toString(),pc));
	}
	/**
	 * 到货完成采购单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseAllArrive(PageCtrl pc,long supplier_id)
		throws Exception
	{
		StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("purchase")+" where arrival_time = "+PurchaseArriveKey.ALLARRIVE);
		
		if(supplier_id!=0)
		{
			sql.append(" and supplier = "+supplier_id);
		}
		sql.append(" order by purchase_id desc");
		
		return (dbUtilAutoTran.selectMutliple(sql.toString(),pc));
	}
	
	/**
	 * 到货多
	 * @param pc
	 * @param supplier_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseMore(PageCtrl pc,long supplier_id)
		throws Exception
	{
		StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("purchase")+" where arrival_time = "+PurchaseArriveKey.ARRIVEAPPROVE);
		
		if(supplier_id!=0)
		{
			sql.append(" and supplier = "+supplier_id);
		}
		sql.append(" order by purchase_id desc");
		
		return (dbUtilAutoTran.selectMutliple(sql.toString(),pc));
	}
	
	/**
	 * 根据供应商查询采购单
	 * @param supplier_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchasesBySupplier(long supplier_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("purchase")+" where supplier = "+supplier_id+" order by purchase_id desc";
			
			return (dbUtilAutoTran.selectMutliple(sql, pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getPurchasesBySupplier error:"+e);
		}
	}
	
	/**
	 * 根据ID查询采购单
	 * @param purchaseid
	 * @return
	 * @throws Exception
	 */
	public DBRow getPurchaseByPurchaseid(long purchaseid) 
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("purchase")+" where purchase_id = ?";
			DBRow para = new DBRow();
			para.add("purchase_id",purchaseid);
			
			return (dbUtilAutoTran.selectPreSingle(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getPurchaseByPurchaseid error:"+e);
		}
	}
	
	/**
	 * 修改采购单
	 * @param purchaseid
	 * @param para
	 * @throws Exception
	 */
	public void updatePurchase(long purchaseid,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where purchase_id = "+purchaseid,ConfigBean.getStringValue("purchase"),para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.updatePurchase error:"+e);
		}
	}
	
	/**
	 * 判断商品是否可以转账
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public boolean checkPurchaseTransfer(long purchase_id) 
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("purchase")+" where purchase_id = ? and (purchase_status != "+PurchaseKey.OPEN+" or purchase_status != "+PurchaseKey.AFFIRMPRICE+" or purchase_status != "+PurchaseKey.CANCEL+")";
		DBRow param = new DBRow();
		param.add("purchase_id",purchase_id);
		DBRow[] result = dbUtilAutoTran.selectPreMutliple(sql,param);
		if(result.length==1)
		{
			return false;
		}
		else
		{
			return true;
		}
		
	}
	
	/**
	 * 判断采购单是否为空
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public boolean notNullPurchase(long purchase_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("purchase_detail")+" where purchase_id = ? ";
		DBRow param = new DBRow();
		param.add("purchase_id",purchase_id);
		DBRow[] result = dbUtilAutoTran.selectPreMutliple(sql,param);
		if(result.length>0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	/**
	 * 判断采购单是否可以确定价格
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public boolean checkPurchasePrice(long purchase_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("purchase_detail")+" where purchase_id = ? and (purchase_count = 0 or price is null or price = 0)";
		DBRow param = new DBRow();
		param.add("purchase_id",purchase_id);
		DBRow[] result = dbUtilAutoTran.selectPreMutliple(sql,param);
		if(result.length>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * 检查采购单是否流程完成
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public DBRow checkPurchaseFinish(long purchase_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("purchase")+" where purchase_id = "+purchase_id+" and arrival_time = "+PurchaseArriveKey.ALLARRIVE;
			
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.checkPurchaseFinish error:"+e);
		}
	}
	
	/**
	 * 根据供应商ID，采购单ID获得采购单
	 * @param purchase_id
	 * @param supplier
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailPurchaseByIdAndSupplierId(long purchase_id,long supplier)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("purchase")+" where purchase_id = ? and supplier = ?";
			
			DBRow para = new DBRow();
			para.add("purchase_id",purchase_id);
			para.add("supplier",supplier);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.checkPurchaseFinish error:"+e);
		}
	}
	
	
	
	//采购单详细
	/**
	 * 根据采购单ID获得采购单详细
	 */
	public DBRow[] getPurchaseDetail(long purchase_id,PageCtrl pc,String type)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("purchase_detail")+" where purchase_id = ? ");
			if (type!=null) 
			{
				if (type.equals("reapall")) {
					sql.append(" and reap_count = purchase_count");
				}
				if (type.equals("reappart")) {
					sql.append(" and reap_count < purchase_count and reap_count>0");
				}
				if (type.equals("reapzero")) {
					sql.append(" and reap_count = 0");
				}
				if (type.equals("goup")) {
					sql.append(" and price> product_history_price");
				}
				if (type.equals("godown")) {
					sql.append(" and price< product_history_price");
				}
				if(type.equals("differencesreap"))
				{
					sql.append(" and reap_count != purchase_count");
				}
			}
			sql.append(" order by purchase_detail_id");
			
			DBRow para = new DBRow();
			para.add("purchase_id", purchase_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(),para, pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getPurchaseDetail error:"+e);
		}
	}
	
	/**
	 * 查找采购单内是否有某商品
	 * @param purchase_id
	 * @param purchase_name
	 * @return
	 * @throws Exception
	 */
	public DBRow getPurchaseDetailByPurchasename(long purchase_id,String purchase_name)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("purchase_detail")+" where purchase_id = ? and purchase_name = ?";
			DBRow para = new DBRow();
			para.add("purchase_id", purchase_id);
			para.add("purchase_name", purchase_name);
			
			return (dbUtilAutoTran.selectPreSingle(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getPurchaseDetailByPurchasename error:"+e);
		}
	}
	
	/**
	 * 采购单内根据商品名模糊查询
	 * @param purchase_id
	 * @param purchase_name
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseDetailsByPurchasename(long purchase_id,String purchase_name)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("purchase_detail")+" where purchase_id = ? and purchase_name like ?";
			DBRow para = new DBRow();
			para.add("purchase_id", purchase_id);
			para.add("purchase_name", "%"+purchase_name+"%");
			
			return (dbUtilAutoTran.selectPreMutliple(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getPurchaseDetailByPurchasename error:"+e);
		}
	}
	public DBRow getPurchaseDetailOneByPurchaseId(long purchase_id) throws Exception{
		try {
			String sql = "SELECT * FROM purchase_detail p WHERE p.purchase_id = "+purchase_id +" LIMIT 1";
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorPurchaseMgr.getPurchaseDetailOneByPurchaseId(long purchase_id) error:"+e);
		}
	}
	/**
	 * 添加采购单详细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addPurchaseDetail(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long purchase_detail_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("purchase_detail"));
			
			dbrow.add("purchase_detail_id",purchase_detail_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("purchase_detail"),dbrow);
			
			return (purchase_detail_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.addPurchaseDetail error:"+e);
		}
	}
	
	/**
	 * 修改采购单详细
	 * @param purchase_detail_id
	 * @param param
	 */
	public void updatePurchaseDetail(long purchase_detail_id,DBRow param)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where purchase_detail_id = "+purchase_detail_id,ConfigBean.getStringValue("purchase_detail"),param);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.updatePurchaseDetail error:"+e);
		}
	}
	
	/**
	 * 根据采购单号删除采购单详细
	 * @param purchase_id
	 * @throws Exception
	 */
	public void delPurchaseDetailByPurchaseid(long purchase_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where purchase_id = "+purchase_id,ConfigBean.getStringValue("purchase_detail"));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.delPurchaseDetailByPurchaseid error:"+e);
		}
	}
	
	/**
	 * 计算采购单总价
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public double sumPurchaseDetailByPurchaseid(long purchase_id)
		throws Exception
	{
		try 
		{
			String sql = "select sum(price * order_count) as purchasesum from "+ConfigBean.getStringValue("purchase_detail")+" where purchase_id = "+purchase_id; 
			DBRow db = dbUtilAutoTran.selectSingle(sql);
			return (db.get("purchasesum",0.00));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.sumPurchaseDetailByPurchaseid error:"+e);
		}
	}
	
	/**
	 * 根据商品条码在采购单内查询商品
	 * @param purchase_id
	 * @param proudct_barcod
	 * @return
	 * @throws Exception
	 */
	public DBRow getPurchaseDetailByProductbarcod(long purchase_id,String proudct_barcod)
		throws Exception
	{
		try 
		{
			String sql = "SELECT pd.* FROM purchase_detail AS pd "
						+"join product_code as pcode on pcode.pc_id = pd.product_id and pcode.p_code = ? "
						+"WHERE pd.purchase_id = ?";
			DBRow para = new DBRow();
			para.add("proudct_barcod",proudct_barcod);
			para.add("purchase_id", purchase_id);
			
			
			return (dbUtilAutoTran.selectPreSingle(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getPurchaseDetailByProductbarcod error:"+e);
		}
	}
	
	/**
	 * 检查采购单是否完成
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkPurchaseStats(long purchase_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("purchase_detail")+" where reap_count < purchase_count and purchase_id ="+purchase_id;
		
		return (dbUtilAutoTran.selectMutliple(sql));
	}
	
	/**
	 * 检查采购单是否需要提交审核
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public int checkPurchaseApprove(long purchase_id)
		throws Exception
	{
		String sql = "select count(1) as differentcount from "+ConfigBean.getStringValue("purchase_detail")+" where reap_count != purchase_count and purchase_id ="+purchase_id;
		int result = dbUtilAutoTran.selectSingle(sql).get("differentcount",0); 
		return (result);
	}
	
	/**
	 * 获得采购单内价格与历史价格不同的商品
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getChangePriceProduct(long purchase_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("purchase_detail")+" where purchase_id=? and price!=product_history_price";
		DBRow para = new DBRow();
		para.add("purchase_id",purchase_id);
		
		return (dbUtilAutoTran.selectPreMutliple(sql, para));
	}
	
	//日志
	/**
	 * 所有操作添加日志
	 */
	public long addPurchasefollowuplogs(DBRow dbrow)
	 throws Exception
	{
		try 
		{
			long logs_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("purchase_followup_logs"));
			dbrow.add("logs_id",logs_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("purchase_followup_logs"),dbrow);
			
			return (logs_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.addPurchasefollowuplogs error:"+e);
		}
	}
	
	/**
	 * 根据不同类型显示日志
	 * @param purchase_id
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getfollowuplogs(long purchase_id,int followup_type)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("purchase_followup_logs")+" where purchase_id = ? ");
			
			DBRow param=new DBRow();
			param.add("purchase_id",purchase_id);
			if(followup_type!=0)
			{
				sql.append(" and followup_type = ?");
				param.add("followup_type",followup_type);
			}
			
			sql.append("order by followup_date desc");
			
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(),param));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getfollowuplogs error:"+e);
		}
	}
	
	
	//提供接口
	/**
	 * 根据商品名查询最后一次采购信息
	 * @param pname
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailPurchaseDetailByPnameNew(String pname,long ps_id) throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("purchase_detail")+" where purchase_name = ? and purchase_id in (select purchase_id from "+ConfigBean.getStringValue("purchase")+" where purchase_status not in(3,4) and ps_id =? ) order by purchase_detail_id desc limit 1";
			DBRow para = new DBRow();
			para.add("purchase_name",pname);
			para.add("ps_id",ps_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getDetailPurchaseDetailByPnameNew error:"+e);
		}
	}
	
	/**
	 * 根据商品条码查询最后一次采购信息
	 * @param purchase_id
	 * @param proudct_barcod
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailPurchaseDetailByProductbarcodNew(String proudct_barcod,long ps_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("purchase_detail")+" where proudct_barcod = ? and purchase_id in (select purchase_id from "+ConfigBean.getStringValue("purchase")+" where purchase_status not in(3,4) and ps_id =? ) order by purchase_detail_id desc limit 1";
			DBRow para = new DBRow();
			para.add("proudct_barcod",proudct_barcod);
			para.add("ps_id",ps_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getDetailPurchaseDetailByProductbarcodNew error:"+e);
		}
	}
	
	/**
	 * 根据商品ID查询最后一次采购信息
	 * @param product_id
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailPurchaseDetailByProductIdNew(long product_id,long ps_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("purchase_detail")+" where product_id = ? and purchase_id in (select purchase_id from "+ConfigBean.getStringValue("purchase")+" where purchase_status not in(3,4) and ps_id =? ) order by purchase_detail_id desc limit 1";
			DBRow para = new DBRow();
			para.add("proudct_id",product_id);
			para.add("ps_id",ps_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getDetailPurchaseDetailByProductbarcodNew error:"+e);
		}
	}
	
	/**
	 * 查询所有预计到货时间小于今天且未到货采购单详细
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseDetailBydelay()
		throws Exception
	{
		try 
		{
			String sql = "select * from purchase_detail where reap_count = 0 and eta<Now() and purchase_id in (select purchase_id from purchase where purchase_status not in(3,4)) order by purchase_id";
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getPurchaseDetailBydelay error:"+e);
		}
	}
	
	/**
	 * 获得未完成可创建交货单的采购单
	 * @param supplier_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNoFinishPurchase(long supplier_id)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("purchase")+" where (purchase_status !="+PurchaseKey.CANCEL+" and purchase_status !="+PurchaseKey.FINISH+")");
			
			if(supplier_id !=0)
			{
				sql.append(" and supplier = "+supplier_id);
			}
			
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getNoFinishPurchase error:"+e);
		}
	}
	
	/**
	 * 根据商品ID获得采购单详细
	 * @param purchase_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getPurchaseDetailByPcid(long purchase_id,long pc_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("purchase_detail")+" where purchase_id = ? and product_id = ?";
		
		DBRow para = new DBRow();
		para.add("purchase_id",purchase_id);
		para.add("product_id",pc_id);
		
		return dbUtilAutoTran.selectPreSingle(sql, para);
	}
	
	public DBRow[] getLastNumberLogs(long purchase_id , int followup_type , int number )throws Exception{
		try 
		{
 			if(purchase_id != 0l){
 				StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("purchase_followup_logs")+" where purchase_id =").append(purchase_id);
 				DBRow param=new DBRow();
 				param.add("purchase_id",purchase_id);
 				if(followup_type!=0){
 					sql.append(" and followup_type =").append(followup_type);
 				}
 				sql.append(" order by followup_date desc").append(" limit 0,").append(number);
 				return dbUtilAutoTran.selectMutliple(sql.toString());
 			}
 			return null ;
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr.getLastNumberLogs error:"+e);
		}
	}
	
	public DBRow[] getPurchaseDetailByPurchaseIdForJqgrid(long purchase_id,PageCtrl pc,String sidx,String sord,FilterBean fillterBean)
		throws Exception
	{
			StringBuffer sql = new StringBuffer("select * from  "+ConfigBean.getStringValue("purchase_detail")+" as pd " 
				   +"left join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = pd.product_id " 
				   +"where pd.purchase_id = ?");

			DBRow para = new DBRow();
			para.add("purchase_id",purchase_id);
			
			if(fillterBean !=null)
			{
			sql.append(new JqGridFilterKey().filterSQL(fillterBean));
			}
			
			if(sidx==null||sord==null)
			{
			sql.append(" order by purchase_id desc");
			}
			else
			{
			sql.append(" order by "+sidx+" "+sord);
			}
			
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(), para,pc));
	}
	
	/**
	 * 删除采购单明细
	 * @param purchase_detail_id
	 * @throws Exception
	 */
	public void delPurchaseDetail(long purchase_detail_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where purchase_detail_id="+purchase_detail_id,ConfigBean.getStringValue("purchase_detail"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr delPurchaseDetail error:"+e);
		}
	}
	
	/**
	 * 根据ID获得采购单详细
	 * @param purchase_detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailPurchaseDetailById(long purchase_detail_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("purchase_detail")+" where purchase_detail_id = ?";
			
			DBRow para = new DBRow();
			para.add("purchase_detail_id",purchase_detail_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr getDetailPurchaseDetailById error:"+e);
		}
	}
	
	/**
	 * 获得采购单全部货款（单体价格X采购数量）
	 * @param purchase_id
	 * @return 已四舍五入
	 * @throws Exception
	 */
	public double getPurchasePrice(long purchase_id)
		throws Exception
	{
		DBRow[] purchaseDetails = this.getPurchaseDetail(purchase_id, null, null);
		
		double allPrice = 0;
		
		for (int i = 0; i < purchaseDetails.length; i++)
		{
			allPrice += purchaseDetails[i].get("price",0d)*purchaseDetails[i].get("order_count",0f);
		}
		
		return (MoneyUtil.round(allPrice,2));
	}
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllPurchaseDetail()
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.select(ConfigBean.getStringValue("purchase_detail"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseMgr getAllPurchaseDetail error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
