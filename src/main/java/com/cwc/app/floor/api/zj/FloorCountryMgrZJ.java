package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorCountryMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBRow getDetailCountryByCountryCode(String c_code)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("country_code")+" where c_code = ? ";
			
			DBRow para = new DBRow();
			para.add("c_code",c_code);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorCountryMgrZJ getDetailCountryByCountryCode error:"+e);
		}
	}
	
	/**
	 * 根据国家名查询信息
	 * @param c_country
	 * @return
	 * @throws Exception
	 * @author:Yuanxinyu
	 * @date: 2015年3月20日 下午6:18:19
	 */
	public DBRow getDetailCountryByCountry(String c_country)
			throws Exception
		{
			try 
			{
				String sql = "select * from "+ConfigBean.getStringValue("country_code")+" where c_country = ? ";
				
				DBRow para = new DBRow();
				para.add("c_country",c_country);
				
				return dbUtilAutoTran.selectPreSingle(sql, para);
			}
			catch (Exception e) 
			{
				throw new Exception("FloorCountryMgrZJ getDetailCountryByCountry error:"+e);
			}
		}
		
    public DBRow getDetailCountryByCcid(long ccid)
            throws Exception
        {
            try 
            {
                String sql = "select * from "+ConfigBean.getStringValue("country_code")+" where ccid = ? ";
                
                DBRow para = new DBRow();
                para.add("ccid",ccid);
                
                return dbUtilAutoTran.selectPreSingle(sql, para);
            }
            catch (Exception e) 
            {
                throw new Exception("FloorCountryMgrZJ getDetailCountryByCcid error:"+e);
            }
        }
    
    public DBRow getDetailProByProId(long pro_id)
            throws Exception
        {
            try 
            {
                String sql = "select * from "+ConfigBean.getStringValue("country_province")+" where pro_id = ? ";
                
                DBRow para = new DBRow();
                para.add("pro_id",pro_id);
                
                return dbUtilAutoTran.selectPreSingle(sql, para);
            }
            catch (Exception e) 
            {
                throw new Exception("FloorCountryMgrZJ getDetailProByProId error:"+e);
            }
        }

	
	public DBRow getDetailCountryProvinceByProvinceCode(String p_code,long nation_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("country_province")+" where p_code = ? and nation_id = ? ";
			
			DBRow para = new DBRow();
			para.add("p_code",p_code);
			para.add("nation_id",nation_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorCountryMgrZJ getDetailCountryProvinceByProvinceCode error:"+e);
		}
	}
	
	
	public DBRow[] getAllCountryCode() throws Exception
		{
			try 
			{
				String sql = "select * from "+ConfigBean.getStringValue("country_code");
				
				return dbUtilAutoTran.selectMutliple(sql);
			}
			catch (Exception e) 
			{
				throw new Exception("FloorCountryMgrZJ getAllCountryCode error:"+e);
			}
		}
	
	
    public DBRow getDetailCountryByCountryName(String c_country)
            throws Exception
    {
        try
        {
            String sql = (new StringBuilder()).append("select * from ").append(ConfigBean.getStringValue("country_code")).append(" where c_country = ? ").toString();
            DBRow para = new DBRow();
            para.add("c_country", c_country);
            return dbUtilAutoTran.selectPreSingle(sql, para);
        }
        catch(Exception e)
        {
            throw new Exception((new StringBuilder()).append("FloorCountryMgrZJ getDetailCountryByCountryName error:").append(e).toString());
        }
    }

    /**
     * 查询所有国家和省份
     * @return
     * @throws Exception
     * @author:zyj
     * @date: 2015年3月23日 下午6:22:56
     */
    public DBRow[] findCountryAndProvinces() throws Exception
    {
    	 try
         {
    		 String sql = "select * from country_code cc left join country_province cp on cp.nation_id = cc.ccid ";
             return dbUtilAutoTran.selectMutliple(sql);
         }
         catch(Exception e)
         {
             throw new Exception((new StringBuilder()).append("FloorCountryMgrZJ findCountryAndProvinces error:").append(e).toString());
         }
    }

	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
