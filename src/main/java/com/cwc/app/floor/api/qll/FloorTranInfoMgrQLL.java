package com.cwc.app.floor.api.qll;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorTranInfoMgrQLL {

	private DBUtilAutoTran dbUtilAutoTran ;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	/**
	 * 根据oid查询tranID
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getTranID(long  oid) 
		throws Exception 
	{
		try 
		{
			DBRow para = new DBRow();
			para.add("oid",oid);
			String sql = "select * from "+ConfigBean.getStringValue("porder")+" where oid = ?";
			return (dbUtilAutoTran.selectPreSingle(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception(" getDetailScene(long oid)  error:"+e);
		}
	}
	/**
	 * 获取具体一条信息
	 * @param tid
	 * @return
	 * @throws Exception
	 */
	public  DBRow  getTranInfo(String tranID) 
		throws Exception
	{
		try
		{      
			String sql = "select * from "+ ConfigBean.getStringValue("tran_info") +" where tran_id=?";
			DBRow para = new DBRow();
			para.add("tran_id", tranID);
			return dbUtilAutoTran.selectPreSingle(sql, para);
			 
		}
		catch (Exception e) 
		{
			throw new Exception ("getTranInfo(String tranID)  error:"+e);
		}
		
		
	}
	/**
	 * 增加tranInfo
	 * @param row
	 * @throws Exception
	 */
	public void addTranInfo(DBRow row)
		throws Exception
	{
		long id;
		try 
		{
			id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("tran_info"));
			row.add("id", id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("tran_info"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTranInfoMgrQLL.addTranInfo(DBRow row) error:"+e);
		}
	
	}
}
