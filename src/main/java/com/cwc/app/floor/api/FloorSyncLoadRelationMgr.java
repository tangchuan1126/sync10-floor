package com.cwc.app.floor.api;

/*****************************************************
 * @author yuehaibo
 * 
 * Load, MasterOrder, Order相关数据库操作
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

@SuppressWarnings("rawtypes")
public class FloorSyncLoadRelationMgr {
	private DBUtilAutoTran dbUtilAutoTran;

	@Autowired
	private FloorAppointmentMgr floorAppointmentMgr;
	
	/**
	 * 根据Loadno获取loadid
	 * @param loadNo
	 * @throws Exception 
	 */
	public DBRow getLoadIdByLoadNo(String loadNo) throws Exception{
		try{
			String sql="select load_id,carrier_id from "+ ConfigBean.getStringValue("wms_load") +" where load_no='"+loadNo+"' and status='open'";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("getLoadIdByLoadNo(String loadNo) error:" + e);
		}
	}
	
	/**
	 * loadNo 是否重复。
	 * 
	 * @param loadNo
	 * @param loadId
	 * @return
	 * @throws Exception
	 */
	public DBRow existsLoadNo(String loadNo, long loadId)  throws Exception{
		try{
			String sql="select COUNT(1) as cnt from "+ ConfigBean.getStringValue("wms_load") +" where load_no=? and load_id !=?";
			DBRow para = new DBRow();
			para.put("load_no", loadNo);
			para.put("load_id", loadId);
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}catch(Exception e){
			throw new Exception("existsLoadNo(String loadNo, long loadId) error:" + e);
		}
	}
	
	public DBRow [] findOrderList(long load_id) throws Exception{
		try{
			String sql="select * from wms_load_order_master where load_id="+load_id;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("findOrderList(long load_id) error:" + e);
		}
	}
	
	/**
	 * 比较loadno关系是否有变化
	 * @param customerId
	 * @param loadNo
	 * @param orderIdList
	 * @param userName
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List diffLoad(String customerId, String loadNo,
			String oid,String carriers) throws Exception{
		try {
			if (loadNo == null) {
				loadNo = "";
			}
			List statusList = new ArrayList();
			String loadNoSql="select a.load_no from wms_load_order_master a,wms_load b where a.order_id="+oid+" and a.load_no=b.load_no and b.status='open'"; //and carrier_id='"+carriers+"'";
			
			DBRow [] loads=this.dbUtilAutoTran.selectMutliple(loadNoSql);
			
			if(loads.length>1)
				throw new Exception("一个order上了多个load");
			
			if(loads.length==1){ //order已经有load关系
				String oldloadno=loads[0].getString("load_no");
				
				if(!oldloadno.equals(loadNo)){ //load有变化
					statusList.add(3);
					statusList.add(oldloadno);
				}else{ //说明更系统中相同
					if(!StringUtils.isEmpty(loadNo)){
						statusList.add(4);
						statusList.add("");
					}else{ //都是空就不用处理appointment
						statusList.add(5);
						statusList.add("");
					}
				}
			}else{ //order没有load关系
				if(!StringUtils.isEmpty(loadNo)){
					statusList.add(1);
					statusList.add("");
				}else{
					statusList.add(5);
					statusList.add("");
				}
			}
				
			return statusList;
		} catch (Exception e) {
			throw new Exception("diffLoad(String customerId, String loadNo,List<String> orderIdList, String userName) error:" + e);
		}
	}
	
	public long updLoad(String customerId, String loadNo,Set<String> orderIdList, String userName,String carrierId) throws Exception {
		try {
			if (loadNo == null) {
				loadNo = "";
			}
			// 1.判断是否已经存在改load，如果没有，创建LOAD
			long newLoadId = getOrInsterLoadNo(customerId, loadNo, userName , carrierId);
			
			for(String orderId:orderIdList){
				addLoadOrderMaster(newLoadId, loadNo, orderId, "0", userName);
				// 写新增order日志
				addLoadLog(String.valueOf(newLoadId), loadNo,
						"add Order No. ", orderId, userName);
			}
			
			if(!orderIdList.isEmpty()){
				DBRow row=getB2bOrder(orderIdList);
				this.dbUtilAutoTran.update("where load_id="+newLoadId, ConfigBean.getStringValue("wms_load"), row);
			}
			
			// 根据load_no更新
			updateMasterOrderMap(newLoadId, loadNo, userName);
			return newLoadId;
		} catch (Exception e) {
			throw new Exception("updLoad(String customerId, String loadNo,Set<String> orderIdList, String userName) error:" + e);
		}
	}
	
	public DBRow getB2bOrder(Collection<String> orderIdList) throws Exception{
		try{
			String sql="select min(mabd) as mabd,requested_date as req_ship_date,freight_carrier as freight_type,freight_term as freight_term from b2b_order where b2b_oid in ("+StringUtils.join(orderIdList, ",")+")";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("getB2bOrder(Set<String> orderIdList) error:" + e);
		}
	}
	
	/**
	 * 把传入的load-order的关系，更新到数据库
	 * 
	 * @param load_no
	 * @param orderIdList
	 */
	@SuppressWarnings("unchecked")
	public Map syncLoad(String customerId, String loadNo, List<String> orderIdList,
			String userName,String carrierId) throws Exception {
		try {
			Map result = new HashMap();
			// 1.判断是否已经存在改load，如果没有，创建LOAD
			long newLoadId = getOrInsterLoadNo(customerId, loadNo, userName,carrierId);

			// 2. 查询新旧load-order关系对比集合新关系-全部order-老关系
			String orderStr = listToString(orderIdList);
			if(orderStr==null || orderStr.equals(""))
				orderStr = "-1";

			String sql = "SELECT n.load_no AS new, n.b2b_oid AS old_id, o.order_id, t.load_no AS old, t.load_id AS oldLoadId FROM"
					+ " (SELECT b2b_oid AS order_id FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " o WHERE o.b2b_oid in ("
					+ orderStr
					+ ") UNION SELECT order_id from "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_no = '"
					+ loadNo
					+ "') AS o"
					+ " LEFT JOIN (SELECT b2b_oid, '"+loadNo+"' AS load_no FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE b2b_oid in ("
					+ orderStr
					+ ")) AS n ON n.b2b_oid = o.order_id"
					+ " LEFT JOIN "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " t ON t.order_id = o.order_id";

			DBRow[] relationList = dbUtilAutoTran.selectMutliple(sql);
			if (relationList != null && relationList.length > 0) {
				// 删除相关masterOrder

				for (DBRow re : relationList) {
					String newLoadNo = re.getString("new");
					String oldLoadNo = re.getString("old");
					String orderId = re.getString("order_id");
					String oldLoadId = re.getString("oldLoadId");

					// 查询当前的aid
					String aidFrom = floorAppointmentMgr.getCurrentAid("order", orderId);
					
					// 还需要判断是否需要写约车信息改变日志
					if (newLoadNo == null || "".equals(newLoadNo)) {
						// 新关系空，需要删掉老关系
//						delLoadOrderRelationByOrderId(orderId);
						// 写删除order日志
//						addLoadLog(oldLoadId, oldLoadNo, "移除 Order", getOrderNoById(orderId), userName);

						result.put(orderId, "del");
					} else if (oldLoadNo == null || "".equals(oldLoadNo)) {
						// 老关系是空，需要添加新关系
						addLoadOrderMaster(newLoadId, newLoadNo, orderId, orderId,// 先空着masterId
								userName);
						// 写新增order日志
						addLoadLog(String.valueOf(newLoadId), newLoadNo, "添加 Order", getOrderNoById(orderId), userName);

						result.put(orderId, "add");
					} else if (!newLoadNo.equals(oldLoadNo)) {
						// 新-老关系不同，更新关系表中响应orderId的loadNo和loadId
//						updateLoadOrderMasterLoadId(newLoadId, newLoadNo,
//								orderId, userName);

						// 写删除order日志
//						addLoadLog(oldLoadId, oldLoadNo, "移除 Order", getOrderNoById(orderId), userName);
						// 写添加order日志
//						addLoadLog(String.valueOf(newLoadId), newLoadNo, "添加 Order", getOrderNoById(orderId), userName);

						result.put(orderId, "change");
					} else {
						result.put(orderId, "0");
					}

					// 查询添加后的aid
					String aidTo = floorAppointmentMgr.getCurrentAid("order", orderId);
					
					if(aidTo == null || !aidTo.equals(aidFrom)){
						// 写变化日志
						floorAppointmentMgr.addAppLog("order", orderId, aidFrom, aidTo, userName,"order:"+orderId+" 约车成功");
					}
				}

				// 根据load_no更新
				updateMasterOrderMap(newLoadId, loadNo, userName);
			}

			return result;
		} catch (Exception e) {
			throw new Exception("syncLoad(String customerId, String loadNo, List<String> orderIdList,String userName,String carrierId) error:" + e);
		}
	}

	/**
	 * 把传入的load-order的关系，更新到数据库
	 * 
	 * @param customerId
	 * @param loadNo
	 * @param orderIdList
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public Map updateLoad(String customerId, String loadNo, List<String> orderIdList,
			String userName,String carrierId) throws Exception {
		try {
			Map result = new HashMap();
			// 1.判断是否已经存在改load，如果没有，创建LOAD
			long newLoadId = getOrInsterLoadNo(customerId, loadNo, userName,carrierId);
		
			// 2. 查询新旧load-order关系对比集合新关系-全部order-老关系
			String orderStr = listToString(orderIdList);
			if(orderStr==null || orderStr.equals(""))
				orderStr = "-1";
			
			String sql = "SELECT n.load_no AS new, n.b2b_oid AS old_id, o.order_id, t.load_no AS old, t.load_id AS oldLoadId FROM"
					+ " (SELECT b2b_oid AS order_id FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " o WHERE o.b2b_oid in ("
					+ orderStr
					+ ") UNION SELECT order_id from "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_no = '"
					+ loadNo
					+ "') AS o"
					+ " LEFT JOIN (SELECT b2b_oid, '"+loadNo+"' AS load_no FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE b2b_oid in ("
					+ orderStr
					+ ")) AS n ON n.b2b_oid = o.order_id"
					+ " LEFT JOIN "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " t ON t.order_id = o.order_id";

//			System.out.println(sql);
			DBRow[] relationList = dbUtilAutoTran.selectMutliple(sql);
			if (relationList != null && relationList.length > 0) {
				// 删除相关masterOrder

				for (DBRow re : relationList) {
					String newLoadNo = re.getString("new");
					String oldLoadNo = re.getString("old");
					String orderId = re.getString("order_id");
					String oldLoadId = re.getString("oldLoadId");

					// 查询当前的aid
					String aidFrom = floorAppointmentMgr.getCurrentAid("order", orderId);
					
					// 还需要判断是否需要写约车信息改变日志
					if (newLoadNo == null || "".equals(newLoadNo)) {
						// 新关系空，需要删掉老关系
						delLoadOrderRelationByOrderId(orderId, Long.valueOf(oldLoadId));
//						System.out.println("新关系空，需要删掉老关系:"+newLoadNo+"--"+orderId+"--"+oldLoadNo);
						// 写删除order日志
						addLoadLog(oldLoadId, oldLoadNo, "移除 Order", getOrderNoById(orderId), userName);

						result.put(orderId, "del");
					} else if (oldLoadNo == null || "".equals(oldLoadNo)) {
						// 老关系是空，需要添加新关系
						addLoadOrderMaster(newLoadId, newLoadNo, orderId, orderId,// 先空着masterId
								userName);
//						System.out.println("老关系是空，需要添加新关系:"+newLoadNo+"--"+orderId+"--"+oldLoadNo);
						// 写新增order日志
						addLoadLog(String.valueOf(newLoadId), newLoadNo, "添加 Order", getOrderNoById(orderId), userName);

						result.put(orderId, "add");
					} else if (!newLoadNo.equals(oldLoadNo)) {
						// 新-老关系不同，更新关系表中响应orderId的loadNo和loadId
						updateLoadOrderMasterLoadId(newLoadId, newLoadNo,
								orderId, userName);
//						System.out.println("新-老关系不同，更新关系表中响应orderId的loadNo和loadId:"+newLoadNo+"--"+orderId+"--"+oldLoadNo);

						// 写删除order日志
						addLoadLog(oldLoadId, oldLoadNo, "移除 Order", getOrderNoById(orderId), userName);
						// 写添加order日志
						addLoadLog(String.valueOf(newLoadId), newLoadNo, "添加 Order", getOrderNoById(orderId), userName);

						result.put(orderId, "change");
					} else {
						result.put(orderId, "0");
					}

					// 查询添加后的aid
					String aidTo = floorAppointmentMgr.getCurrentAid("order", orderId);
					
					if(aidTo == null || !aidTo.equals(aidFrom)){
						if(aidTo == null && aidFrom == null){
							// 不写日志
						} else {
							// 写变化日志
							floorAppointmentMgr.addAppLog("order", orderId, aidFrom, aidTo, userName,"order:"+orderId+"约车成功");
						}
					}
				}

				// 根据load_no更新
				updateMasterOrderMap(newLoadId, loadNo, userName);
			}

			return result;
		} catch (Exception e) {
			throw new Exception("updateLoad(String customerId, String loadNo, List<String> orderIdList,String userName,String carrierId) error:" + e);
		}
	}
	
	/**
	 * 更新loadId下的order
	 * @param customerId
	 * @param loadNo
	 * @param orderIdList
	 * @param userName
	 * @param carrierId
	 * @return
	 * @throws Exception
	 */
	public void updateLoad(long loadId, List<String> orderIdList,
			String userName) throws Exception {
		try {
			String sql="select load_no,carrier_id from wms_load where load_id="+loadId;
			DBRow row=this.dbUtilAutoTran.selectSingle(sql);
			
			String loadNo=row.getString("load_no");
			String carrier_id = row.get("carrier_id", "");
			for(String orderId:orderIdList){
				addLoadOrderMaster(loadId, loadNo, orderId, "0", userName);
				// 写新增order日志
				addLoadLog(String.valueOf(loadId), loadNo,
						"add Order No. ", orderId, userName);
			}
			if(!orderIdList.isEmpty()){
				row=getB2bOrder(orderIdList);
				this.dbUtilAutoTran.update("where load_id="+loadId, ConfigBean.getStringValue("wms_load"), row);
			}
			// 根据load_no更新
			updateMasterOrderMap(loadId, loadNo, userName);
		} catch (Exception e) {
			throw new Exception("updateLoad(long loadId, List<String> orderIdList,String userName) error:" + e);
		}
	}
	
	public void modWmsLoad(long loadId,DBRow row) throws Exception{
		this.dbUtilAutoTran.update("where load_id="+loadId, ConfigBean.getStringValue("wms_load"), row);
	}
	
	public String [] getLoadIdAndAid(long id) throws Exception {
		try{

			String sql = "SELECT (SELECT load_id FROM wms_load_order_master WHERE order_id = b.b2b_oid) AS load_id,"
			+"(SELECT aid FROM wms_appointment_invoice WHERE invoice_type='load' AND invoice_id = (SELECT load_id FROM wms_load_order_master WHERE order_id = b.b2b_oid)) AS aid,"
			+" (SELECT aid FROM wms_appointment_invoice WHERE invoice_type='order' AND invoice_id = b.b2b_oid) AS order_aid"
			+" from b2b_order b where b.b2b_oid = ?";
			
			DBRow para = new DBRow();
			para.put("b.b2b_oid", id);
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			if(result != null){
				String loadid = result.getString("load_id");
				String aid = result.getString("aid");
				String orderAid = result.getString("order_aid");
				return new String[]{loadid,aid==null || aid.trim().length()==0?orderAid:aid};
			}
			return null;
		}catch(Exception e){
			throw new Exception("getLoadIdAndAid(String id) error:" + e);
		}
	}
	
	/**
	 * 查询单据现在的有效aid
	 * load，PO 查自己的
	 * order，先查所在load的，没有load再查自己的
	 * 
	 * @param type
	 * @param id
	 * @return
	 */
	public String getCurrentAid(String type, String id) throws Exception {
		try {
		String sql = "";
		DBRow para = new DBRow();
		if("order".equals(type)){
			sql = "SELECT (SELECT aid FROM wms_appointment_invoice WHERE invoice_type='load' AND invoice_id = (SELECT load_id FROM wms_load_order_master WHERE order_id = b.b2b_oid)) AS load_aid," 
			+" (SELECT aid FROM wms_appointment_invoice WHERE invoice_type='order' AND invoice_id = b.b2b_oid) AS order_aid"
			+" from b2b_order b where b.b2b_oid = ?";

			para.put("b.b2b_oid", id);
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			if(result != null){
				String loadAid = result.getString("load_aid");
				String orderAid = result.getString("order_aid");
				
				if(loadAid != null && !"".equals(loadAid)){ // 如果所在load有aid，返回load的aid
					return loadAid;
				} else if(orderAid != null && !"".equals(orderAid)){
					return orderAid;
				}
			}
		} else {
			sql = "SELECT aid FROM " + ConfigBean.getStringValue("wms_appointment_invoice") + " WHERE invoice_type = ? AND invoice_id = ?";

			para.put("invoice_type", type);
			para.put("invoice_id", id);
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			if(result!=null){
				String aid = result.getString("aid");
				if(aid != null && !"".equals(aid)){
					return aid;
				}
			}
		}
		
		// 查不到返回NULL
		return null;
		} catch (Exception e) {
			throw new Exception("getCurrentAid() error:" + e);
		}
	}

	// 更新masterOrder关系
	private void updateMasterOrderMap(long loadId, String loadNo,
			String userName) throws Exception {
		try {
			// 查询所有load_no关联的order
			String sql = "SELECT CONCAT(t.customer_id,'&',t.deliver_house_number) AS connect, t.customer_id, t.deliver_house_number, t.b2b_oid as order_id, t.load_no, "
					+ "(SELECT SUM(pallet_spaces) FROM "+ConfigBean.getStringValue("b2b_order_item")+" WHERE b2b_oid = t.b2b_oid) as total_pallets,t.company_id,"
							+ "t.deliver_zip_code,t.deliver_city,t.address_state_deliver FROM  "
					+ ConfigBean.getStringValue("b2b_order")
					+ " t, "+ConfigBean.getStringValue("wms_load_order_master")+" o  WHERE t.b2b_oid = o.order_id AND o.load_no = '" + loadNo + "' and o.master_id='0' ORDER BY connect";
//			System.out.println("查询所有load_no关联的order");
			DBRow[] orderList = dbUtilAutoTran.selectMutliple(sql);
			
			if(orderList != null){
				// 根据order的connect字段添加master order，然后添加关联关系
				Map masterMap = new HashMap();
				for (DBRow orderDetail : orderList) {
					long masterId = 0L;
					String orderId = orderDetail.getString("order_id");
					String connect = orderDetail.getString("connect");

					DBRow row = new DBRow();
					row.put("order_id", orderId);
					row.put("load_id", loadId);
					row.put("load_no", loadNo);
					row.put("user_created", userName);
					row.put("date_created", new Date());

					String total_pallets = orderDetail.getString("total_pallets");

					// 已经存在masterOrder
					if (masterMap.containsKey(connect)) {
						masterId = (long) masterMap.get(connect);
					} else {
						// 还没有connect对应的masterOrder
						String company_id = safeString(orderDetail
								.getString("company_id"));
						String customer_id = safeString(orderDetail
								.getString("customer_id"));
						String ship_to_address = safeString(orderDetail
								.getString("deliver_house_number"));
						String ship_to_zipcode = safeString(orderDetail
								.getString("deliver_zip_code"));
						String ship_to_city = safeString(orderDetail
								.getString("deliver_city"));
						String ship_to_state = safeString(orderDetail
								.getString("address_state_deliver"));

						// 新建master
						DBRow masterRow = new DBRow();
						masterRow.put("load_id", loadId);
						masterRow.put("company_id", company_id);
						masterRow.put("customer_id", customer_id);
						masterRow.put("ship_to_address", ship_to_address);
//						masterRow.put("ship_to_zipcode", ship_to_zipcode);
						masterRow.put("total_pallets", (total_pallets==null||"".equals(total_pallets))?0.00f:Float.parseFloat(total_pallets));
						masterRow.put("date_created", new Date());
						masterRow.put("ship_to_zipcode", ship_to_zipcode);
						masterRow.put("ship_to_city", ship_to_city);
						masterRow.put("ship_to_state", ship_to_state);
						
						masterId = addMasterOrder(masterRow);
						masterMap.put(connect, masterId);
					}

					row.put("master_id", masterId);
					
					DBRow data = new DBRow();
					data.put("master_id", masterId);
					dbUtilAutoTran.update(" where order_id = " + orderId, ConfigBean.getStringValue("wms_load_order_master"), data);

					// 更新总托盘数
					updateMasterOrderPallets(masterId,
							(total_pallets==null||"".equals(total_pallets))?0.00f:Float.parseFloat(total_pallets));
				}
			}
		} catch (Exception e) {
			throw new Exception("updateMasterOrderMap() error:" + e);
		}
	}
	
	/**
	 * 更新load信息
	 * @param loadId
	 * @param loadNo
	 * @param carrId
	 * @throws Exception
	 */
	public void updateLoad(long loadId,String loadNo,String carrId,String userName) throws Exception{
		try{
			String sql="update wms_load set load_no='"+loadNo+"' , carrier_id='"+carrId+"' where load_id="+loadId;
			this.dbUtilAutoTran.executeSQL(sql);
			
			sql="update wms_load_order_master set load_no='"+loadNo+"' where load_id="+loadId;
			this.dbUtilAutoTran.executeSQL(sql);
			
			addLoadLog(String.valueOf(loadId), loadNo,
					" change LoadNo#:", loadNo, userName);
		}catch(Exception e){
			throw new Exception("updateLoad(long loadId,String loadNo,String carrId) error:" + e);
		}
	}
	/**
	 * 更新masterId
	 * @param oids
	 * @param mid
	 * @param lid
	 * @throws Exception
	 */
	public void modMasterId(String oids,long mid,int lid) throws Exception{
		try{
			String sql="update wms_load_order_master set master_id="+mid+" where order_id in ("+oids+") and load_id="+lid;
			this.dbUtilAutoTran.executeSQL(sql);
		}catch(Exception e){
			throw new Exception("modMasterId(String oids,int mid,int lid) error:" + e);
		}
	}
	
	private String safeString(Object v) {
		return (v == null) ? "" : (String) v;
	}
	
	private Long safeLong(Object v) {
		return (v == null||"".equals(v)) ? 0L : Long.valueOf(v.toString());
	}

	private void delLoadOrderRelationByOrderId(String orderId, long loadId)	throws Exception {
		updateB2bLoadNo("", "", loadId, orderId);
		dbUtilAutoTran.delete(" WHERE order_id = " + orderId, ConfigBean.getStringValue("wms_load_order_master"));
	}
	
	//TODO 
	public void updateB2bLoadNo(String loadNo, String carrrier, Long loadId, String orderId) throws Exception{
		try{
			String sql="update b2b_order set load_no='"+loadNo+"', carriers='"+carrrier+"' where b2b_oid="+orderId;
			this.dbUtilAutoTran.executeSQL(sql);
			
			if ("".equals(loadNo)) {
				SyncAppt(loadId, orderId, true);
			} else {
				SyncAppt(loadId, orderId, false);
			}
		}catch(Exception e){
			throw new Exception("updateB2bLoadNo(String loadNo,long orderId) error:" + e);
		}
		
	}
	
	/**
	 * 同步已经有appt 的load 
	 * 
	 * @param load
	 * @throws Exception
	 */
	public void SyncAppt(Long load, String orderId, boolean clean) {
		if (load==null || orderId==null  || "".equals(orderId) ) return;
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			String apptid = floorAppointmentMgr.getCurrentAid("load", String.valueOf(load));
			if (apptid!=null && !"".equals(apptid)) {
				if (clean) {
					DBRow para = new DBRow();
					para.add("carriers", null);
					para.add("pickup_appointment", null);
					dbUtilAutoTran.updatePre("where b2b_oid="+ orderId, null, "b2b_order", para);
				} else {
					DBRow row = floorAppointmentMgr.getAppByAID(apptid);
					DBRow loadRow=this.dbUtilAutoTran.selectSingle("select load_no,carrier_id from wms_load where load_id="+load);
					if (row==null) row = new DBRow();
					String appttime = row.get("appointment_time", "");
					String scac = row.get("carrier_id", loadRow.get("carrier_id", ""));
					DBRow para = new DBRow();
					if (scac!=null &&  !"".equals(scac)) {
						para.add("carriers", scac);
					} else {
						para.add("carriers", null);
					}
					if(appttime==null || "".equals(appttime)) {
						para.add("pickup_appointment", null);
					} else {
						para.add("pickup_appointment", format1.parse(appttime));
					}
					dbUtilAutoTran.updatePre("where b2b_oid="+ orderId, null, "b2b_order", para);
				}
			}
		} catch (Exception e) {}
		
	}

	private void delLoadOrderRelationByLoadId(String loadId)
			throws Exception {
		dbUtilAutoTran.delete(" WHERE load_id = "
				+ loadId, ConfigBean.getStringValue("wms_load_order_master"));
	}

	// 分页查询Load
	public DBRow[] getWmsLoadList(String load_no, String status,
			String begin_time, String end_time, String company,  String customer, PageCtrl pc) throws Exception {
		try {
			boolean q=false;
			String sqlOrder="select b2b_oid from b2b_order where 1=1 ";
			
			if(checkValue(begin_time)){
				sqlOrder+="and mabd>=str_to_date('"+begin_time+" 00:00:00', '%m/%d/%Y %H:%i:%s')";
				q=true;
			}
			
			if (checkValue(end_time)) {
				sqlOrder+="and mabd<=str_to_date('"+end_time+" 23:59:59', '%m/%d/%Y %H:%i:%s')";
				q=true;
			}
			
			Set<Integer> b2boids=new HashSet<Integer>();
			Set<Integer> loadIds=new HashSet<Integer>();
			if(q){
				DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sqlOrder);
				
				for(DBRow row:rows){
					b2boids.add((Integer)row.getValue("b2b_oid"));
				}
				
				
				if(!b2boids.isEmpty()){
					String loadnoSql="select load_id from wms_load_order_master where order_id in ("+StringUtils.join(b2boids, ",")+")";
					rows=this.dbUtilAutoTran.selectMutliple(loadnoSql);
					
					for(DBRow row:rows){
						loadIds.add((Integer)row.getValue("load_id"));
					}
				}
				
			}
			
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT l.*, DATE_FORMAT(l.date_updated,'%m/%d/%Y %H:%i') AS updated, DATE_FORMAT(l.date_created,'%m/%d/%Y %H:%i') AS created,"
					+ " a.aid, (select appointment_time from wms_appointment where id = a.aid limit 1) AS appointment_time,b.storage_id FROM "
					+ ConfigBean.getStringValue("wms_load")
					+ " as l left join "
					+ ConfigBean.getStringValue("wms_appointment_invoice")
					+ " a on l.load_id = a.invoice_id AND a.invoice_type = 'load' left join wms_appointment b on a.aid=b.id where 1=1");
			
			DBRow para = new DBRow();
			if (checkValue(load_no)) {
				sql.append(" AND load_no like '%"+load_no+"%'");
			}
			if(q && !loadIds.isEmpty()){
				sql.append(" AND load_id in ("+StringUtils.join(loadIds,",")+")");
			}else if(q && loadIds.isEmpty()){
				sql.append(" AND 1=2 ");
			}
//			if (checkValue(begin_time)) {
//				sql.append(" AND date_created >= str_to_date('"+begin_time+"', '%m/%d/%Y')");
//			}
//			if (checkValue(end_time)) {
//				sql.append(" AND date_created <= str_to_date('"+end_time+"', '%m/%d/%Y')");
//			}
			if (checkValue(company)) {
				sql.append(" AND l.load_id IN (SELECT load_id FROM wms_load_order_master WHERE order_id IN (SELECT b2b_oid FROM b2b_order WHERE send_psid = '"+company+"'))");
			}
			if (checkValue(customer)) {
				sql.append(" AND l.load_id IN (SELECT load_id FROM wms_load_order_master WHERE order_id IN (SELECT b2b_oid FROM b2b_order WHERE customer_id = '"+customer+"'))");
			}
			
			sql.append(" ORDER BY date_created desc,load_id desc");
			return ((para.size() == 0) ? dbUtilAutoTran.selectMutliple(
					sql.toString(), pc) : dbUtilAutoTran.selectPreMutliple(
					sql.toString(), para, pc));
		} catch (Exception e) {
			throw new Exception("getWmsLoadList() error:" + e);
		}
	}

	// 查询单个load
	public DBRow getWmsLoad(String load_id) throws Exception {
		try {
			String sql = "SELECT DISTINCT l.*, a.aid as appointment_id, (select appointment_time from wms_appointment where id = a.aid) as appointment_time,"
					+ " (select sum(pallet_spaces) from b2b_order_item where b2b_oid IN (SELECT order_id FROM "
					+ConfigBean.getStringValue("wms_load_order_master")+" WHERE load_id = "+load_id+")) AS total_pallets "
							+ "FROM "
					+ ConfigBean.getStringValue("wms_load")
					+ " as l left join "
					+ ConfigBean.getStringValue("wms_appointment_invoice")
					+ " a on l.load_id = a.invoice_id and a.invoice_type='load' where load_id = "+load_id;
			return (dbUtilAutoTran.selectSingle(sql));
		} catch (Exception e) {
			throw new Exception("getWmsLoad() error:" + e);
		}
	}
	
	/**
	 * 更新 scac,appttime
	 * 
	 * @param load_id
	 * @param scac
	 * @return
	 * @throws Exception 
	 */
	public boolean updCarrier(long load_id, String scac) throws Exception {
		DBRow data = new DBRow();
		data.add("carrier_id", scac);
		dbUtilAutoTran.update(" where load_id="+ load_id, "wms_load", data);
		return false;
	}

	// 分页查询Order
	public DBRow[] getWmsOrderByComCusPoReq(String customer_id,
			String company_id, String order_id, String req_start, String req_end,
			String filter, PageCtrl pc) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT t.b2b_oid order_id, (select sum(pallet_spaces) from b2b_order_item where b2b_oid = t.b2b_oid) as total_pallets,t.freight_carrier, "
					+ "CONCAT(m.brand_name,'<br/>',t.b2b_order_address) as connect, t.b2b_order_address as ship_to_address, t.order_number as order_no, t.company_id,(select title from product_storage_catalog where id=t.send_psid) as company_name, "
					+ "t.customer_id, t.load_no, t.retail_po as po, t.mabd,t.send_psid FROM (SELECT * FROM "
					+ ConfigBean.getStringValue("b2b_order") + " WHERE b2b_order_status not in (1,8,9,10) and b2b_oid not in (SELECT order_id FROM "+ConfigBean.getStringValue("wms_load_order_master")+" )) AS t,customer_brand as m where t.customer_id=m.cb_id ");

			DBRow row = new DBRow();
			if (checkValue(customer_id)) {
				row.put("t.customer_id", customer_id);
				sql.append(" AND t.customer_id = ?");
			}
			if (!"0".equals(company_id) && checkValue(company_id)) {
				row.put("t.company_id", company_id);
				sql.append(" AND t.send_psid = ?");
			}
			if (checkValue(order_id)) {
				row.put("t.b2b_oid", order_id);
				sql.append(" AND t.b2b_oid = ?");
			}
			if (checkValue(req_start)) {
//				row.put("t.mabd", req_start);
				sql.append(" AND t.mabd >= str_to_date('"+req_start+" 00:00:00', '%m/%d/%Y %H:%i:%s') ");
			}
			if (checkValue(req_end)) {
//				row.put("t.mabd", req_end);
				sql.append(" AND t.mabd <= str_to_date('").append(req_end).append(" 23:59:59', '%m/%d/%Y %H:%i:%s') ");
			}
			if (checkValue(filter)) {
				sql.append(" AND t.b2b_oid NOT IN (" + filter + ")");
			}

			sql.append(" ORDER BY t.etd, connect");
			return ((row.size() == 0) ? dbUtilAutoTran.selectMutliple(
					sql.toString(), pc) : dbUtilAutoTran.selectPreMutliple(
					sql.toString(), row, pc));
		} catch (Exception e) {
			throw new Exception(
					"getWmsOrderByComCusPoReq(customer_id,	company_id, po_no, req_start, req_end, pc) error:"
							+ e);
		}
	}

	// 根据LoadId查询MasterOrder信息
	public DBRow[] getMasterOrderByLoadId(String load_id) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT m.master_order_id, m.company_id, m.customer_id,n.brand_name as customer_name, m.ship_to_address FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " l, "
					+ ConfigBean.getStringValue("wms_master_order")
					+ " m,"+ConfigBean.getStringValue("customer_brand")+" n WHERE m.customer_id=n.cb_id and m.master_order_id = l.master_id and l.load_id = ? GROUP BY m.master_order_id");
			DBRow para = new DBRow();
			para.put("m.load_id", load_id);
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(), para));
		} catch (Exception e) {
			throw new Exception("getMasterOrderByLoadId(load_id) error:" + e);
		}
	}

	// 根据masterOrderId查询order信息
	public DBRow[] getWmsOrderByMasterOrderId(String master_id) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();

			sql.append("SELECT  t.b2b_oid as order_id, t.b2b_oid as order_no, t.company_id,(select title from product_storage_catalog where id=t.send_psid) as company_name, t.send_psid, t.customer_id, t.load_no, t.retail_po as po, t.etd as etd, t.mabd,"
					+ " t.b2b_order_address, (select sum(pallet_spaces) from b2b_order_item where b2b_oid = t.b2b_oid) as pallets FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " t  WHERE t.b2b_oid IN (SELECT order_id  FROM wms_load_order_master where master_id = ?)");

			DBRow para = new DBRow();
			para.put("m.master_id", master_id);
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(), para));
		} catch (Exception e) {
			throw new Exception("getWmsOrderByLoadId(master_id) error:" + e);
		}
	}

	// 根据load_id查询order列表
	public DBRow[] getWmsOrdersByLoadId(String loadId) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();

			sql.append("SELECT t.b2b_oid order_id,freight_carrier,(select sum(pallet_spaces) from b2b_order_item where b2b_oid = t.b2b_oid) as total_pallets, "
					+ "CONCAT(m.brand_name,'<br/>',t.b2b_order_address) as connect, t.b2b_order_address as ship_to_address, t.order_number as order_no, (select title from product_storage_catalog where id=t.send_psid) as company_id, "
					+ "t.customer_id, t.load_no, t.retail_po as po, DATE_FORMAT(t.mabd,'%m/%d/%Y') as mabd FROM "
					+ ConfigBean.getStringValue("b2b_order") 
					+ " t,customer_brand m  WHERE t.customer_id=m.cb_id and t.b2b_oid IN (SELECT order_id  FROM wms_load_order_master where load_id = ?)");
			
			DBRow para = new DBRow();
			para.put("load_id", loadId);
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(), para));
		} catch (Exception e) {
			throw new Exception("getWmsOrdersByLoadId(loadId) error:" + e);
		}
	}
	
	// 根据load_id查询order列表
	public DBRow[] getMasterBolByLoadId(String loadId) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			
			sql.append("select *,p.ship_to_address as ship_to_address,CONCAT(m.brand_name,'<br/>',p.ship_to_address) as connect,k.master_id from ( ")
				.append("SELECT t.b2b_oid order_id,freight_carrier,")
				.append("(select sum(pallet_spaces) from b2b_order_item where b2b_oid = t.b2b_oid) as total_pallets,") 
				.append("t.order_number as order_no, "
						+ "(select title from product_storage_catalog where id=t.send_psid) as company_id,(select title from product_storage_catalog where id=t.send_psid) as company_name,") 
				.append("t.customer_id, t.load_no, t.retail_po as po, t.mabd,t.send_psid FROM b2b_order t ")
				.append("WHERE t.b2b_oid IN (SELECT order_id  FROM wms_load_order_master where load_id = ?)) n left join wms_load_order_master k ")
				.append("on n.order_id=k.order_id left join wms_master_order p on p.master_order_id=k.master_id left join customer_brand m on n.customer_id=m.cb_id");
			
			DBRow para = new DBRow();
			para.put("load_id", loadId);
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(), para));
		} catch (Exception e) {
			throw new Exception("getWmsOrdersByLoadId(loadId) error:" + e);
		}
	}
	
	// 根据b2b_oid查询order
	public DBRow getWmsOrdersById(String orderId) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();

			sql.append("SELECT t.b2b_oid order_id, (select sum(pallet_spaces) from b2b_order_item where b2b_oid = t.b2b_oid) as total_pallets, "
					+ "CONCAT(t.customer_id,'<br/>',t.b2b_order_address) as connect, t.b2b_order_address as ship_to_address, t.order_number as order_no, t.customer_dn, t.company_id, "
					+ "t.customer_id, t.load_no, t.retail_po as po, DATE_FORMAT(t.etd,'%m-%d-%Y') as mabd, t.b2b_order_status as status FROM "
					+ ConfigBean.getStringValue("b2b_order") 
					+ " t  WHERE b2b_oid = ?");
			
			DBRow para = new DBRow();
			para.put("b2b_oid", orderId);
			return (dbUtilAutoTran.selectPreSingle(sql.toString(), para));
		} catch (Exception e) {
			throw new Exception("getWmsOrdersById(orderId) error:" + e);
		}
	}

	// 根据po_no查询po详情
	public DBRow getWmsPoByNo(String poNo) throws Exception {
		try {
			String sql = "SELECT t.retail_po AS po, (SELECT SUM(pallet_spaces) FROM "+ConfigBean.getStringValue("b2b_order_item")+" WHERE b2b_oid = t.b2b_oid) as total_pallets FROM "	+ ConfigBean.getStringValue("b2b_order") + " t  WHERE t.retail_po = ? GROUP BY t.retail_po";
			DBRow para = new DBRow();
			para.put("retail_po", poNo);
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} catch (Exception e) {
			throw new Exception("getWmsOrdersById(orderId) error:" + e);
		}
	}

	// 删除Load
	public void delLoad(String load_id) throws Exception {
		try {

			dbUtilAutoTran.delete("where load_id='" + load_id + "'",
					ConfigBean.getStringValue("wms_load"));
		} catch (Exception e) {
			throw new Exception("delLoad(load_id) error:" + e);
		}
	}

	// 添加Load
	public long addLoad(DBRow row) throws Exception {
		try {

			return dbUtilAutoTran.insertReturnId(
					ConfigBean.getStringValue("wms_load"), row);
		} catch (Exception e) {
			throw new Exception("addLoad(row) error:" + e);
		}
	}

	// 判断是否Load是否可用
	public DBRow checkLoadNo(String loadNo,String carrId) throws Exception {
		try {
			// mdf by wangcr 2015/04/08 loadno 不能重复。
			String sql = "SELECT COUNT(1) AS count FROM " + ConfigBean.getStringValue("wms_load") + " WHERE load_no = ? ";//+"and carrier_id=?";
			DBRow para = new DBRow();
			para.put("load_no", loadNo);
			//para.put("carrier_id", carrId);
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (Exception e) {
			throw new Exception("checkLoadNo(row) error:" + e);
		}
	}

	// 根据load_no插入新记录或取已经存在的load
	public Long getOrInsterLoadNo(String customerId, String loadNo, String userName,String carrierId)
			throws Exception {
		try {
			String sql = "SELECT * FROM "
					+ ConfigBean.getStringValue("wms_load")
					+ " WHERE load_no = ? ";
			DBRow para = new DBRow();
			para.put("load_no", loadNo);
			DBRow data = dbUtilAutoTran.selectPreSingle(sql, para);
			if (data != null && data.size() > 0) {
				return Long.parseLong(data.getString("load_id"));
			} else {
				DBRow row = new DBRow();
				// load基础信息
				row.add("load_no", loadNo);
				row.add("customer_id", customerId);
				row.add("user_created", userName);
				row.add("date_created", new Date());
				row.add("status", "Open");
				row.add("carrier_id", carrierId);

				return addLoad(row);
			}
		} catch (Exception e) {
			throw new Exception("getOrInsterLoadNo(row) error:" + e);
		}
	}
	
	String getOrderNoById(String id) throws Exception {
		return dbUtilAutoTran.selectSingle("SELECT order_number FROM "+ConfigBean.getStringValue("b2b_order")+" t WHERE t.b2b_oid = " + id).getString("order_number");
	}

	// 添加Load 日志
	public void addLoadLog(String loadId, String loadNo, String lable, Object text, String userName)
			throws Exception {
		try {
			DBRow row = new DBRow();
			row.put("load_id", loadId);
			row.put("load_no", loadNo);
			row.put("lable", lable);
			row.put("text", text);
			row.put("user_created", userName);
			row.put("date_created", new Date());
			dbUtilAutoTran.insert(ConfigBean.getStringValue("wms_load_log"),
					row);
		} catch (Exception e) {
			throw new Exception("addLoadLog(load_id, option, value) error:" + e);
		}
	}

	// 根据load_id查询load的操作日志
	public DBRow[] getLoadLogsById(String loadId)
			throws Exception {
		try {
			String sql = "SELECT *,DATE_FORMAT(date_created,'%m/%d/%Y %H:%i') as date_created FROM "+ConfigBean.getStringValue("wms_load_log")+" WHERE load_id = "+loadId + " ORDER BY date_created DESC";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("getLoadLogsById(loadId) error:" + e);
		}
	}
	
	// 先根据load_id判断有没有appointment关联了此load
	public boolean checkLoadCanDel(String loadId)
			throws Exception {
		try {
			String sql = "SELECT count(1) AS count FROM "+ConfigBean.getStringValue("wms_appointment_invoice")+" WHERE invoice_type = 'load' AND invoice_id = "+loadId;
//			System.out.println(sql);
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			if(result != null && result.size()>0){
				return Integer.parseInt(result.getString("count")) > 0;
			} else {
				return false;
			}
		} catch (Exception e) {
			throw new Exception("checkLoadCanDel(loadId) error:" + e);
		}
	}

	/**
	 * 新建masterOrder
	 */
	public long addMasterOrder(DBRow row) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(
					ConfigBean.getStringValue("wms_master_order"), row);
		} catch (Exception e) {
			throw new Exception("addMasterOrder(row) error:" + e);
		}
	}
	
	public long updateMasterOrder(DBRow row,long masterOrderID) throws Exception {
		try {
			return this.dbUtilAutoTran.update("where master_order_id="+masterOrderID, ConfigBean.getStringValue("wms_master_order"), row);
		} catch (Exception e) {
			throw new Exception("updateMasterOrder(DBRow row,long masterOrderID) error:" + e);
		}
	}

	/**
	 * 新建masterOrder
	 */
	public long delMasterOrderByLoadId(String loadId) throws Exception {
		try {
			String sql = " WHERE master_order_id in (SELECT master_id FROM "
					+ConfigBean.getStringValue("wms_load_order_master")+" WHERE load_id = '"+loadId+"')";
			return dbUtilAutoTran.delete(sql, ConfigBean.getStringValue("wms_master_order"));
		} catch (Exception e) {
			throw new Exception("delMasterOrderByLoadId(loadId) error:" + e);
		}
	}
	
	/**
	 * 删除orderid的所有masterbol关系
	 */
	public long delMasterOrderByOids(String loadId) throws Exception {
		try {
			String sql = "where master_order_id not in (select master_id from wms_load_order_master where load_id="+loadId+") and load_id="+loadId;
			return dbUtilAutoTran.delete(sql, ConfigBean.getStringValue("wms_master_order"));
		} catch (Exception e) {
			throw new Exception("delMasterOrderByLoadId(loadId) error:" + e);
		}
	}

	/**
	 * 新建关联关系记录
	 */
	public long addLoadOrderMaster(long loadId, String loadNo, String orderId,
			String masterId, String userName) throws Exception {
		try {
			DBRow load = new DBRow();
			// 收货信息
			load.add("load_id", loadId);
			load.add("load_no", loadNo);
			load.add("order_id", orderId);
			load.add("master_id", masterId);
			load.add("user_created", userName);
			load.add("date_created", new Date());
			String loadnoSql="select load_id from wms_load_order_master where order_id='"+orderId+"'";
			DBRow[] exist=this.dbUtilAutoTran.selectMutliple(loadnoSql);
			if(exist!=null && exist.length>0){
				throw new Exception("b2b_oid:"+orderId+" added wms_load_order_master by other operator");
			}
			DBRow row = this.getLoadIdByLoadNo(loadNo);
			updateB2bLoadNo(loadNo, row.get("carrier_id", ""), loadId,orderId);
			return dbUtilAutoTran.insertReturnId(
					ConfigBean.getStringValue("wms_load_order_master"), load);
		} catch (Exception e) {
			throw new Exception("addLoadOrderMaster(row) error:" + e);
		}
	}

	/**
	 * 根据orderId更新关联关系记录中的loadId和loadNo
	 */
	public void updateLoadOrderMasterLoadId(long loadId, String loadNo,
			String orderId, String userName) throws Exception {
		try {
			DBRow load = new DBRow();
			// 收货信息
			load.add("load_id", loadId);
			load.add("load_no", loadNo);
			load.add("order_id", orderId);
			load.add("user_updated", userName);
			load.add("date_updated", new Date());
			
			DBRow row = this.getLoadIdByLoadNo(loadNo);
			updateB2bLoadNo(loadNo, row.get("carrier_id", ""), loadId, orderId);
			
			dbUtilAutoTran.update(" WHERE order_id = " + orderId, ConfigBean.getStringValue("wms_load_order_master"), load);
		} catch (Exception e) {
			throw new Exception(
					"updateLoadOrderMasterLoadId(loadId,orderId,userName) error:"
							+ e);
		}
	}

	/**
	 * 更新master order记录的托盘数
	 */
	public void updateMasterOrderPallets(long master_order_id,
			float total_pallets) throws Exception {
		try {
			String whereSql = " WHERE master_order_id = " + master_order_id;
			String mysql = "total_pallets=(total_pallets+" + (int)total_pallets + ")";
			dbUtilAutoTran.update(whereSql, mysql, ConfigBean.getStringValue("wms_master_order"));
		} catch (Exception e) {
			throw new Exception(
					"updateMasterOrderPallets(master_order_id, total_pallets) error:"
							+ e);
		}
	}

	/**
	 * 根据orderId删除关联关系记录
	 */
	public void delLoadOrderMaster(int order_id, Long load) throws Exception {
		try {
			DBRow para = new DBRow();
			para.put("order_id", order_id);
			dbUtilAutoTran.deletePre("where order_id = ?", para, ConfigBean.getStringValue("wms_load_order_master"));
			updateB2bLoadNo("", "", load, String.valueOf(order_id));
		} catch (Exception e) {
			throw new Exception("delLoadOrderMaster(order_id) error:" + e);
		}
	}

	/**
	 * 根据orderId删除关联关系记录
	 */
	public void delLoadOrderMasterByLoadId(String loadId) throws Exception {
		try {
			DBRow para = new DBRow();
			para.put("load_id", loadId);
			
			String sql="update b2b_order set load_no='' where b2b_oid in (select order_id from wms_load_order_master where load_id='"+loadId+"')";
			this.dbUtilAutoTran.executeSQL(sql);
			
			dbUtilAutoTran.deletePre("where load_id = ?", para,
					ConfigBean.getStringValue("wms_load_order_master"));
		} catch (Exception e) {
			throw new Exception("delLoadOrderMasterByLoadId(loadId) error:" + e);
		}
	}

	// Load综合查询List
	public DBRow[] getMasterAndOrderByLoadNo(String load_no) throws Exception {
		try {
			// 根据load_no查询所有与其关联的所有masterorderid和order
			// 先连接wms_order和wms_load_order_master, masterde load_no = 输入的load_no
			String sql = "SELECT o.*,m.master_id FROM "
					+ ConfigBean.getStringValue("wms_order") + " AS o, "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " AS m "
					+ " WHERE o.order_id = m.order_id AND m.load_no = ?";

			DBRow para = new DBRow();
			para.put("load_no", load_no);
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} catch (Exception e) {
			throw new Exception("getMasterAndOrderByLoadNo(load_no) error:" + e);
		}
	}
	
	public DBRow findMasterBol(String cid,String address,String loadId) throws Exception{
		try{
			String sql="select * from wms_master_order where  load_id="+loadId+" and customer_id="+cid+" and ship_to_address='"+address+"'";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("findMasterBol(String cid,String address,String loadId) error:" + e);
		}
	}
	
	/**
	 * company列表
	 * 
	 * @param load_no
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCompanyList() throws Exception {
		try {
			String sql = "SELECT DISTINCT company_id FROM "
					+ ConfigBean.getStringValue("b2b_order");

			return (dbUtilAutoTran.selectMutliple(sql));
		} catch (Exception e) {
			throw new Exception("getCompanyList() error:" + e);
		}
	}

	/**
	 * customer列表
	 * 
	 * @param load_no
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCustomerList() throws Exception {
		try {
			String sql = "SELECT DISTINCT customer_id FROM "
					+ ConfigBean.getStringValue("b2b_order");

			return (dbUtilAutoTran.selectMutliple(sql));
		} catch (Exception e) {
			throw new Exception("getCustomerList(load_no) error:" + e);
		}
	}

	/**************************** get set ****************************/
	public DBUtilAutoTran getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	/**
	 * 校验order 是否已经预约。
	 * @param oids
	 * @param userName
	 */
	public void checkOrders(List<String> oids, String userName) {
		for (String id : oids) {
			try {
				floorAppointmentMgr.delAppInvoiceAndApp(id, "order", userName);
			} catch (Exception e) {}
		}
	}

	private boolean checkValue(String key) {
		if (key == null) {
			return false;
		} else if ("".equals(key)) {
			return false;
		} else
			return true;
	}

	public String listToString(List<String> stringList) {
		if (stringList == null) {
			return null;
		}
		StringBuilder result = new StringBuilder();
		boolean flag = false;
		for (String string : stringList) {
			if (flag) {
				result.append(",");
			} else {
				flag = true;
			}
			result.append(string);
		}
		return result.toString();
	}

	
	public FloorAppointmentMgr getFloorAppointmentMgr() {
		return floorAppointmentMgr;
	}

	public void setFloorAppointmentMgr(FloorAppointmentMgr floorAppointmentMgr) {
		this.floorAppointmentMgr = floorAppointmentMgr;
	}
}
