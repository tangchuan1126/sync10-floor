package com.cwc.app.floor.api.gzy;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorPurchaseGZY {
	
	private DBUtilAutoTran dbUtilAutoTran;
	//根据productlineid获取供应商信息
	public DBRow[] getSupplierByProductline(long productLineId)throws Exception {
		try {
			String sql = "select * from "+ConfigBean.getStringValue("supplier")+" where product_line_id="+productLineId;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("getSupplierByProductline error:"+e);
		}
	}
	//生成采购单
	public long addPurchase(DBRow dbrow)throws Exception{
	try 
	{
		long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("purchase"));
		dbrow.add("purchase_id",id);
		dbUtilAutoTran.insert(ConfigBean.getStringValue("purchase"),dbrow);
		return (id);
	}catch (Exception e){
		throw new Exception("FloorPurchaseMgr.addPurchase error:"+e);
	}
}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
