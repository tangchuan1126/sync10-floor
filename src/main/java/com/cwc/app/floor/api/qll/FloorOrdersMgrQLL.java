package com.cwc.app.floor.api.qll;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorOrdersMgrQLL {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void modAddressValidateStatus(long oid,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where oid=" + oid,ConfigBean.getStringValue("porder"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modAddressValidateStatus(long oid,DBRow row) error:" + e);
		}
	}
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
