package com.cwc.app.floor.api.zr;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorAndroidPushMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
     
    public void updateRegisterReflection(long adid, String regId) throws Exception{
         String sql = "update admin set register_id= " + "'" + regId + "'" + " where adid=" + adid;
         try {
            dbUtilAutoTran.executeSQL(sql);
        } catch (Exception e) {
            throw new Exception("AndroidPushMessageIZr updateRegister error:"+e);
        }
    }
    
     public DBRow[] findRegisterIds(String targetIds) throws Exception{
        String[] adidsStr = StringUtils.split(targetIds, ";");
        List<String> adids = Arrays.asList(adidsStr);
        String conditions = "( ";
        for(String adid : adids){
            conditions += adid + ",";
        }
        conditions = conditions.substring(0, conditions.lastIndexOf(",")) + ")";
        try{
            String sql = " select * from admin where adid in " + conditions ;
             return this.dbUtilAutoTran.selectMutliple(sql);
         }catch(Exception e){
             throw new Exception("FloorAndroidPushMgrZr findRegisterIds"+e);
         }
    }
    
     
    public DBRow[] findAllRegisterIds() throws Exception{
        try{
            String sql = " select * from admin ";
             return this.dbUtilAutoTran.selectMutliple(sql);
         }catch(Exception e){
             throw new Exception("FloorAndroidPushMgrZr findAllRegisterIds"+e);
         }
    }
    
     
    public Boolean checkRegisterIdIsExist(String regId) throws Exception{
        try{
            String sql = " select * from admin where register_id = " + "'" + regId + "'";
             DBRow[] rows = this.dbUtilAutoTran.selectMutliple(sql);
            Boolean result = (rows.length > 0)?true:false;
            return result;
         }catch(Exception e){
             throw new Exception("FloorAndroidPushMgrZr checkRegisterIdIsExist"+e);
         }
    }
    public int countMessage(long adid , int status) throws Exception{
    	try{
    		String sql = "select count(*) as count_total from short_message_receiver where receiver_id="+adid + " and short_message_status="+status;
    		DBRow result = dbUtilAutoTran.selectSingle(sql);
    		return result != null ? result.get("count_total", 0) : 0 ;
    	 }catch(Exception e){
            throw new Exception("FloorAndroidPushMgrZr countMessage"+e);
    	}
    }
    public DBRow[] queryMessage(long short_message_id ,  int pageCount  , long adid  , int short_message_status) throws Exception {
    	try{
    		String sql = " select  message.* from short_message_receiver  as receiver LEFT JOIN  short_message_info as message ON  receiver.short_message_id = message.id where    receiver.short_message_status ="+short_message_status+" and receiver_id = "+adid+" and message.id > "+short_message_id+" LIMIT 0,"+pageCount ;
    		return dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
            throw new Exception("FloorAndroidPushMgrZr queryMessage(short_message_id , pageCount)"+e);
    	}
    }
    public void updateAdmin(long adid , DBRow updateRow) throws Exception{
    	try{
    		String tableName =  ConfigBean.getStringValue("admin") ;
    		dbUtilAutoTran.update(" where adid = " + adid, tableName, updateRow);
    	}catch(Exception e){
            throw new Exception("FloorAndroidPushMgrZr updateAdmin(adid , updateRow)"+e);
    	}
    }
    
}
