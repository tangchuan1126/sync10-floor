package com.cwc.app.floor.api.zj;

import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorOutListDetailMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加拣货单详细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addOutListDetail(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("out_list_detail"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutListDetailMgrZJ addOutListDetail error:"+e);
		}
	}
	
	/**
	 * 修改出货单明细
	 * @param out_list_detail_id
	 * @param para
	 * @throws Exception
	 */
	public void modOutListDetail(long out_list_detail_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where out_list_detail_id="+out_list_detail_id,ConfigBean.getStringValue("out_list_detail"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutListDetailMgrZJ modOutListDetail error:"+e);
		}
	}
	
	/**
	 * 单据取消，删除拣货明细
	 * @param system_bill_id
	 * @param system_bill_type
	 * @throws Exception
	 */
	public void delOutListDetail(long system_bill_id,int system_bill_type)
		throws Exception
	{
		DBRow para = new DBRow();
		para.add("system_bill_id",system_bill_id);
		para.add("system_bill_type",system_bill_type);
		
		dbUtilAutoTran.deletePre("where system_bill_id = ? and system_bill_type = ?",para,ConfigBean.getStringValue("out_list_detail"));
	}
	
	/**
	 * 根据业务单据获得拣货明细
	 * @param system_bill_id
	 * @param system_bill_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutListDetailByBill(long system_bill_id,int system_bill_type)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("out_list_detail")+" where system_bill_id = ? and system_bill_type = ?";
			
			DBRow para = new DBRow();
			para.add("system_bill_id",system_bill_id);
			para.add("system_bill_type",system_bill_type);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutListDetailMgrZJ getOutListDetailByBill error:"+e);
		}
	}
	
	public long getBillArea(long system_bill_id,int system_bill_type)
		throws Exception
	{
		try 
		{
			String sql = "select * from "
						+"("
						+"select out_list_area_id,count(out_list_detail_id)as stop_count from out_list_detail where system_bill_id = ? and system_bill_type = ? GROUP BY out_list_area_id " 
						+") as area_stop ORDER BY stop_count DESC limit 1 ";
			
			DBRow para = new DBRow();
			para.add("system_bill_id",system_bill_id);
			para.add("system_bill_type",system_bill_type);
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql, para);
			
			return row.get("out_list_area_id",0l);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutListDetailMgrZJ getBillArea error:"+e);
		}
	}
	
	/**
	 * 获得拣货单所属区域
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public long getOutArea(long out_id)
		throws Exception
	{
		String sql = "select * from ("
					+"select area_id,count(DISTINCT slc_id) as stop_count from "+ConfigBean.getStringValue("out_storebill_order_detail")+" where out_id = ? group by area_id "
					+") as area_stop ORDER BY stop_count DESC limit 1 ";
		
		DBRow para = new DBRow();
		para.add("out_id",out_id);
		
		DBRow row = dbUtilAutoTran.selectPreSingle(sql, para);
		
		if(row == null)
		{
			row = new DBRow();
		}
		
		return row.get("area_id",0l);
	}
	
	/**
	 * 获得系统单据的拣货明细
	 * @author zhanjie
	 * @param pc_id
	 * @param system_bill_type
	 * @param system_bill_id
	 * @param clp_type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutListDetailByContainerType(long pc_id,int system_bill_type,long system_bill_id,int container_type,long type_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("out_list_detail")+" where  system_bill_type = ? and system_bill_id = ? ";
			
			DBRow para = new DBRow();
			para.add("system_bill_type",system_bill_type);
			para.add("system_bill_id",system_bill_id);
			
			if (container_type !=0)
			{
				sql += " and pick_container_type = ? ";
				para.add("pick_container_type",container_type);
			}
			else
			{
//				sql += " and (pick_container_type = "+ContainerTypeKey.CLP+" or pick_container_type = "+ContainerTypeKey.BLP+" or pick_container_type = "+ContainerTypeKey.ILP+")";
				sql += " and (pick_container_type = "+ContainerTypeKey.CLP+")";//去掉ILP和BLP
			}
			
			if(pc_id != 0)
			{
				sql +=" and out_list_pc_id = ? ";
				para.add("out_list_pc_id",pc_id);
			}
			
			if (type_id !=0)
			{
				sql += " and pick_container_type_id = ? ";
				para.add("pick_container_type_id",type_id);
			}
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutListDetailMgrZJ getOutListDetailCLP error:"+e);
		}
	}
	
	public int getOutListDetailOriginalCount(long pc_id,int system_bill_type,long system_bill_id)
		throws Exception
	{
		try
		{
			String sql = "select sum(pick_up_quantity) as original_count from "+ConfigBean.getStringValue("out_list_detail")+" where  system_bill_type = ? and system_bill_id = ?  and pick_container_type = ?  and out_list_pc_id = ? ";
			
			DBRow para = new DBRow();
			para.add("system_bill_type",system_bill_type);
			para.add("system_bill_id",system_bill_id);
			para.add("pick_container_type",ContainerTypeKey.Original);
			para.add("out_list_pc_id",pc_id);
			
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			
			int orignail_count = (int)result.get("original_count",0f);
			
			return orignail_count;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutListDetailMgrZJ getOutListDetailOriginalCount error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
