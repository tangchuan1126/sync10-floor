package com.cwc.app.floor.api.zj;

import java.util.ArrayList;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorQuestionMgrZJ 
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加问题
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addQuestion(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long question_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("question"));
			dbrow.add("question_id",question_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("question"),dbrow);
			
			return (question_id);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorQuestionMgrZJ.addQuestion error:"+e);
		}
	}
	
	/**
	 * 获得所有问题，按降序排列
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllQuestion(PageCtrl pc,int question_status)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("question")+" where 1=1 ");
			if(question_status!=0)
			{
				sql.append(" and question_status ="+question_status);
			}
			sql.append(" order by question_id desc");
			
			return dbUtilAutoTran.selectMutliple(sql.toString(),pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorQuestionMgrZJ.getAllQuestion error:"+e);
		}
	}
	
	/**
	 * 根据问题ID获得问题详细
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailQuestionById(long id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("question")+" where question_id ="+id;
			
			return (dbUtilAutoTran.selectSingle(sql));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorQuestionMgrZJ.getDetailQuestionById error:"+e);
		}
	}
	
	/**
	 * 修改问题
	 * @param id
	 * @param para
	 * @throws Exception
	 */
	public void modQuestionById(long id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where question_id="+id,ConfigBean.getStringValue("question"),para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorQuestionMgrZJ.modQuestionById error:"+e);
		}
	}
	
	/**
	 * 删除问题
	 * @param id
	 * @throws Exception
	 */
	public void delQuestionById(long id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where question_id="+id,ConfigBean.getStringValue("question"));
		}
		catch (RuntimeException e)
		{
			throw new Exception("FloorQuestionMgrZJ.delQuestionById error:"+e);
		}
	}
	
	
	/**
	 * 根据商品类型ID搜索问题
	 * @param productsId
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchQuestionByCatalogId(long catalog_id,String pname,PageCtrl pc,int question_status)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("question")+" as q inner join "+ConfigBean.getStringValue("product")+" as p on q.product_id=p.pc_id inner join "+ConfigBean.getStringValue("product_catalog")+" pc on p.catalog_id = pc.id where 1=1");
			DBRow para = new DBRow();
			if(catalog_id != 0)
			{
				String pp = allCatalogs(String.valueOf(catalog_id));
				sql.append(" and (pc.id in ("+pp+") or pc.parentid in ("+pp+"))");
			}
			
			if(!pname.trim().equals(""))
			{
				sql.append(" and p.p_name like '%"+pname+"%'");
			}
			
			if(question_status!=0)
			{
				sql.append(" and question_status = ?");
				para.add("question_status",question_status);
			}
			
			sql.append(" order by question_id desc");
			
			if(para.getFieldNames().size()==0)
			{
				return (dbUtilAutoTran.selectMutliple(sql.toString(),pc));
			}
			else
			{
				return (dbUtilAutoTran.selectPreMutliple(sql.toString(),para,pc));
			}
		}
		catch (Exception e) 
		{
			throw new Exception("FloorQuestionMgrZJ searchQuestion error:"+e);
		}
	}
	
	/**
	 * 索引搜索结果过滤
	 * @param questions_id
	 * @param catalog_id
	 * @param pname
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow searchQuestionByIndex(long questions_id,long catalog_id,int question_status)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("question")+" as q inner join "+ConfigBean.getStringValue("product")+" as p on q.product_id=p.pc_id inner join "+ConfigBean.getStringValue("product_catalog")+" pc on p.catalog_id = pc.id where 1=1 ");
			DBRow para = new DBRow();
			//StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("question")+" as q inner join "+ConfigBean.getStringValue("product")+" as p on q.product_id=p.pc_id where 1=1 ");
			
			if(questions_id!=0)
			{
				sql.append("and q.question_id = ?");
				para.add("question_id",questions_id);
			}
			
			if(catalog_id != 0)
			{
				String ids = allCatalogs(String.valueOf(catalog_id));
				sql.append(" and (pc.id in ("+ids+") or pc.parentid in ("+ids+"))");
			}
			
//			if(!pname.trim().equals(""))
//			{
//				sql.append(" and p.p_name like '%"+pname+"%'");
//			}
			
			if(question_status !=0)
			{
				sql.append(" and q.question_status = ?");
				para.add("question_status",question_status);
			}
			
			if(para.getFieldNames().size()==0)
			{
				return (dbUtilAutoTran.selectSingle(sql.toString()));
			}
			else
			{
				return (dbUtilAutoTran.selectPreSingle(sql.toString(),para));
			}
		}
		catch (Exception e) 
		{
			throw new Exception("FloorQuestionMgrZJ searchQuestionByIndex error:"+e);
		}
	}
	
	
	/**
	 * 根据商品Id查询问题
	 * @param product_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchQuestionByProductId(long product_id,PageCtrl pc,int question_status)
	throws Exception
	{
		StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("question")+" where product_id = ?");
		
		DBRow para = new DBRow();
		para.add("product_id",product_id);
		if(question_status!=0)
		{
			sql.append(" and question_status = ?");
			para.add("question",question_status);
		}
		
		return dbUtilAutoTran.selectPreMutliple(sql.toString(),para,pc);
	}
	
	/**
	 * 分类ID
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	private String allCatalogs(String catalog_id)
		throws Exception
	{
		try{
			StringBuffer sb = new StringBuffer(catalog_id);
			int stop;
			ArrayList<DBRow> a = new ArrayList<DBRow>();
			do {
				String sqls = "select id from "+ConfigBean.getStringValue("product_catalog")+" where parentid in ("
						+ catalog_id + ")";
				DBRow[] s = dbUtilAutoTran.selectMutliple(sqls);
				
				StringBuffer csb = new StringBuffer("");
				for ( stop = 0; stop < s.length; stop++) 
				{
					a.add(s[stop]);
					csb.append(s[stop].getString("id"));
					if (stop < s.length - 1) {
						csb.append(",");
					}
				}
				catalog_id = csb.toString();
				
			} while (!catalog_id.equals(""));
			
			DBRow[] ids = a.toArray(new DBRow[0]);
			for(int i = 0;i<ids.length;i++)
			{
				sb.append(","+ids[i].getString("id"));
			}
			return sb.toString();
		} 
		catch (Exception e) 
		{
			throw new Exception(e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
