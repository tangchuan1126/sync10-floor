/**
 * 基于图数据库的库存模型操作接口
 */
package com.cwc.app.floor.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.Stack;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;
import us.monoid.web.Content;
import us.monoid.web.JSONResource;
import us.monoid.web.Resty;
import us.monoid.web.Resty.Option;
import us.monoid.web.TextResource;

import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.Neo4jContainerTreeBuilder;
import com.cwc.app.util.Neo4jLocationTreeBuilder;
import com.cwc.app.util.Neo4jPathAnalyser;
import com.cwc.app.util.Neo4jPathAnalyser2;
import com.cwc.app.util.Neo4jTransaction;
import com.cwc.app.util.Neo4jTreeBuilder;
import com.cwc.app.util.RelationBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorProductStoreMgr  {
	static Logger log = Logger.getLogger("JAVAPS");
	
	/**
	 * 
	 * 节点类型的枚举值，作为有关方法的参数！
	 * 
	 */
	public enum NodeType {
		StorageLocationCatalog, Container, Product
	};
	/**
	 * 
	 * 关系类型的枚举值，对调用方暂时不会用到
	 * 
	 */
	public enum RelationType {
		LOCATES, CONTAINS, AVAILABLE_LOCK
	};
	
	public interface ProductStoreMgrCallbackIFace {
		/**
		 * 
		 * @param props
		 * 			当前容器属性
		 * @param children
		 * 			当前节点的下级子容器数组
		 * @param products
		 * 			当前节点直接放置的产品数组
		 * @return
		 * 			如果需要在遍历过程中构造一颗树，则应返回生成出的当前容器树节点对象，遍历机制在进入下级时，会自动把这个生成的节点压栈
		 * 			如果不转换节点，则返回null，则遍历机制会将参数中的props压栈
		 * @throws Exception
		 */
		public Object containerNode(DBRow props, DBRow[] children, DBRow[] products) throws Exception;
		
		public Stack<Object> getTraverseStack();
		
		public void setTraverseStack(Stack<Object> stack);
		
		public void containerNodePoped(Object node) throws Exception;
		
	}

	private DBUtilAutoTran dbUtilAutoTran;

	private String restBaseUrl;

	private Resty client;

	private static long CACHE_EXPIRES = 3600 * 1000;

	private ThreadLocal<Map<String, Integer>> containerCapacityCache = new ThreadLocal<Map<String, Integer>>();
	private ThreadLocal<Map<String, Long>> containerCapacityCacheTm = new ThreadLocal<Map<String, Long>>();

	private Map<NodeType, Set<String>> nodePropNames = new HashMap<NodeType, Set<String>>();
	private Map<RelationType, Set<String>> relPropNames = new HashMap<RelationType, Set<String>>();
	private Map<String, RelationType> relNames = new HashMap<String, RelationType>();
	
	public void setProductPropNames(Set<String> propNames){
		nodePropNames.put(NodeType.Product,propNames);
	}
	
	public void setContainerPropNames(Set<String> propNames){
		nodePropNames.put(NodeType.Container,propNames);
	}
	
	public void setStorageLocationCatalogPropNames(Set<String> propNames){
		nodePropNames.put(NodeType.StorageLocationCatalog,propNames);
	}

	public FloorProductStoreMgr() {
		client = new Resty(Option.timeout(1000000));
		
		/*改为从Spring配置文件来设置
		nodePropNames.put(
				NodeType.Product,
				new HashSet<String>(Arrays.asList(new String[] { "pc_id",
						"title_id", "product_line", "catalogs", "p_name",
						"union_flag" })));
		nodePropNames.put(
				NodeType.Container,
				new HashSet<String>(Arrays.asList(new String[] { "con_id",
						"container", "container_type", "is_full", "is_has_sn",
						"type_id" })));
		nodePropNames.put(
				NodeType.StorageLocationCatalog,
				new HashSet<String>(Arrays.asList(new String[] {
						"is_three_dimensional", "slc_area", "slc_id",
						"slc_position", "slc_position_all", "slc_type",
						"slc_x", "slc_y" })));
		*/

		relPropNames.put(
				RelationType.LOCATES,
				new HashSet<String>(Arrays.asList(new String[] { "pc_id",
						"title_id", "lot_number", "time_number" })));
		relPropNames.put(
				RelationType.CONTAINS,
				new HashSet<String>(Arrays.asList(new String[] { "quantity",
						"locked_quantity" })));
		relPropNames
				.put(RelationType.AVAILABLE_LOCK,
						new HashSet<String>(Arrays.asList(new String[] {
								"container_type", "type_id", "title_id",
								"quantity" })));

		relNames.put(
				NodeType.Container + "-" + NodeType.StorageLocationCatalog,
				RelationType.LOCATES);
		relNames.put(NodeType.Container + "-" + NodeType.Container,
				RelationType.CONTAINS);
		relNames.put(NodeType.Container + "-" + NodeType.Product,
				RelationType.CONTAINS);
		relNames.put(NodeType.StorageLocationCatalog + "-" + NodeType.Product,
				RelationType.AVAILABLE_LOCK);

	}

	public DBUtilAutoTran getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public String getRestBaseUrl() {
		return restBaseUrl;
	}

	public void setRestBaseUrl(String restBaseUrl) {
		this.restBaseUrl = restBaseUrl;
	}

	public void addNodes(DBRow[] nodes, String nodeLabel, JSONArray statements)
			throws Exception {
		for (DBRow node : nodes) {

			JSONObject parameters = new JSONObject();
			List<String> props = new ArrayList<String>();

			for (Object fn : node.getFieldNames()) {
				String k = ((String) fn).toLowerCase();
				parameters.put(k, node.getValue((String) fn));
				props.add(k + ":{" + k + "}");
			}

			JSONObject statement = new JSONObject();
			statement.put("statement", "MERGE (n:" + nodeLabel + " { "
					+ StringUtils.join(props, ",") + " }) return n");
			statement.put("parameters", parameters);
			statements.put(statement);
		}
	}

	/**
	 * 返回减货算法的列表，这个方法目前应该已经不需要直接调用了，只是为了调试或分析数据用。
	 * @param ps_id
	 * @param title_id
	 * @param pc_id
	 * @param container_type
	 * @param type_id
	 * @param lot_number
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public DBRow[] allocateList(long ps_id, long title_id, long pc_id,
			int container_type, long type_id, String lot_number, int limit)
			throws Exception {

		return DBRowUtils.jsonArrayAsDBRowArray(
				availableList(ps_id, title_id, pc_id, container_type, type_id,
						lot_number, limit), null, "pick_con_id", "time_number",
				"slc_id", "from_con_id", "from_container_type",
				"from_container_type_id", "type", "available_quantity");
	}

	/**
	 * 就某个 title_id,pc_id,lot_number, container_type,type_id
	 * 而言，按取货次序返回包含这个货的所有容器，排序规则如下：
	 * 1、如果容器所在位置是2D，则排序字段取StorageLocationCatalog.time_number
	 * 2、如果容器所在位置是3D，则排序字段取LOCATES.time_number
	 * @return
	 * @throws Exception
	 */

	public JSONArray availableList(long ps_id, long title_id, long pc_id,
			int container_type, long type_id, String lot_number, int limit)
			throws Exception {
		String query = null;
		String lot_number_cond = lot_number != null && lot_number.length() > 0 ? ",lot_number:{lot_number}"
				: "";
		if (container_type != 0) {
			query = StringUtils
					.join(new String[] {
							"match  ",
							"(slc:StorageLocationCatalog)<-[loc:LOCATES {title_id:{title_id},pc_id:{pc_id}"
									+ lot_number_cond
									+ "}]-(c:Container),pth=(c)-[:CONTAINS*1..]->(p:Product {pc_id:{pc_id},title_id:{title_id}})  ",
							"optional match ",
							"(slc:StorageLocationCatalog)-[alock:AVAILABLE_LOCK {title_id:{title_id},container_type:{container_type},type_id:{type_id}} ]->(p:Product {pc_id:{pc_id},title_id:{title_id}}) ",
							"with slc, min(loc.time_number) as time_number, sum((relationships(pth)[-1]).quantity) as quantity, alock.quantity as locked_quantity ",
							"match ",
							"(slc:StorageLocationCatalog)<-[loc:LOCATES {title_id:{title_id},pc_id:{pc_id}"
									+ lot_number_cond
									+ "}]-(c:Container)-[r:CONTAINS*1..]->(p:Product {pc_id:{pc_id},title_id:{title_id}})  ",
							"where  ",
							"( ",
							"    (c.container_type={container_type} and c.type_id={type_id})  ",// 接地容器直接是所需要的容器类型，OK
							"    or  ",
							"    (c.container_type=3 and ( ",// 如果接地容器是TLP类型，则:1,要么直接下级是所需要的容器类型；2,要么是拿散件的情况(container_type=0)
							"         (startnode(r[0]).container_type={container_type} and startnode(r[0]).type_id={type_id})   ) )",
							")  ",
							"and ( ",
							"    (slc.is_three_dimensional = 1 and (r[-1]).quantity > (r[-1]).locked_quantity ) ",
							"    or ",
							"    (slc.is_three_dimensional = 0 and quantity > (case (locked_quantity is NULL) when TRUE then 0 else locked_quantity end)) ",
							") ",
							"with   ",
							"startnode(r[-1]).con_id as pick_con_id, ",
							"(case slc.is_three_dimensional when 0 then time_number else loc.time_number end) as time_number, ",
							"slc.slc_id as slc_id, ",
							"c.con_id as from_con_id, c.container_type as from_container_type, c.type_id as from_container_type_id, ",
							"(case slc.is_three_dimensional when 0 then 'Location' else 'Container' end) as type, ",
							"(case slc.is_three_dimensional when 0 then (quantity - (case (locked_quantity is NULL) when TRUE then 0 else locked_quantity end)) else ((r[-1]).quantity - (r[-1]).locked_quantity) end) as available_quantity ",
							"return ",
							"0 as distinctKey, pick_con_id, time_number, slc_id, from_con_id, from_container_type, from_container_type_id, type, available_quantity ",
							"order by time_number,from_con_id ",
							"limit {limit} " }, " \n");
		} else { // 2014年3月17日修改了算法，拿散件时，只能从接地的TLP中拿，不许拆箱
			query = StringUtils
					.join(new String[] {
							"match  ",
							"(slc:StorageLocationCatalog)<-[loc:LOCATES {title_id:{title_id},pc_id:{pc_id}"
									+ lot_number_cond
									+ "}]-(c:Container {container_type:3})-[r:CONTAINS]->(p:Product {pc_id:{pc_id},title_id:{title_id}})  ",
							"optional match ",
							"(slc:StorageLocationCatalog)-[alock:AVAILABLE_LOCK {title_id:{title_id},container_type:{container_type},type_id:{type_id}}]->(p:Product {pc_id:{pc_id},title_id:{title_id}}) ",
							"with slc, min(loc.time_number) as time_number, sum(r.quantity) as quantity, alock.quantity as locked_quantity ",
							"match ",
							"(slc:StorageLocationCatalog)<-[loc:LOCATES {title_id:{title_id},pc_id:{pc_id}"
									+ lot_number_cond
									+ "}]-(c:Container {container_type:3})-[r:CONTAINS]->(p:Product {pc_id:{pc_id},title_id:{title_id}})  ",
							"where  ",
							// 如果是2D位置，需要这个位置上有可用数即可
							"(slc.is_three_dimensional = 0 and quantity > (case (locked_quantity is NULL) when TRUE then 0 else locked_quantity end) ) ",
							"or ",
							// 如果是3D位置，需要这个接地容器上有可用数
							"(slc.is_three_dimensional = 1 and r.quantity > r.locked_quantity) ",
							"with   ",
							"c.con_id as from_con_id, c.container_type as from_container_type, c.type_id as from_container_type_id, ",
							"(case slc.is_three_dimensional when 0 then 'Location'+slc.slc_id else 'Container'+c.con_id end) as distinctKey, ",
							"(case slc.is_three_dimensional when 0 then time_number else loc.time_number end) as time_number, ",
							"slc.slc_id as slc_id, ",
							"(case slc.is_three_dimensional when 0 then 'Location' else 'Container' end) as type, ",
							"(case slc.is_three_dimensional when 0 then (quantity - (case (locked_quantity is NULL) when TRUE then 0 else locked_quantity end)) else (r.quantity - r.locked_quantity) end) as available_quantity ",
							"return ",
							"distinct(distinctKey) as distinctKey, ", // 拿散件的情况下需要做distinct，因为在一个位置（2D）或TLP容器（3D）上会尽量多拿
							"0 as pick_con_id, ",
							"time_number, ",
							"slc_id, ",
							"from_con_id, from_container_type, from_container_type_id, ",
							"type, available_quantity ",
							"order by time_number,from_con_id ",
							"limit {limit} " }, " \n");
		}

		JSONObject body = new JSONObject();
		body.put("query", query);
		JSONObject params = new JSONObject();
		params.put("title_id", title_id);
		params.put("pc_id", pc_id);
		params.put("container_type", container_type);
		params.put("type_id", type_id);
		params.put("limit", limit);
		if (lot_number_cond.length() > 0)
			params.put("lot_number", lot_number);
		body.put("params", params);

		JSONResource response = client.json(restBaseUrl + "/" + ps_id
				+ "/db/data/cypher", new Content("application/json", body
				.toString().getBytes("UTF-8")));
		if (response.object().has("exception"))
			throw new Exception(response.object().toString(4));

		JSONArray data = response.object().getJSONArray("data");

		return data;

	}

	public int containerCapacity(int container_type, long type_id)
			throws Exception {

		Map<String, Integer> cache = containerCapacityCache.get();
		Map<String, Long> tm = containerCapacityCacheTm.get();
		String k = container_type + "-" + type_id;

		if (cache == null) {
			cache = new HashMap<String, Integer>();
			tm = new HashMap<String, Long>();
			containerCapacityCache.set(cache);
			containerCapacityCacheTm.set(tm);
		}

		if (cache.containsKey(k)) {
			long timestamp = tm.get(k).longValue();
			if (System.currentTimeMillis() - timestamp < CACHE_EXPIRES)
				return cache.get(k).intValue();
		}

		String sql = null;
		switch (container_type) {
		case 1: // CLP
			sql = "select sku_lp_total_piece as capacity from clp_type where sku_lp_type_id = {type_id} "
					.replace("{type_id}", String.valueOf(type_id));
			break;
		case 2: // BLP
			sql = "select box_total_piece as capacity from box_type where box_type_id = {type_id} "
					.replace("{type_id}", String.valueOf(type_id));
			break;
		case 3: // TLP
			return 0; // TLP不限量，可以放任意多
		case 4: // ILP
			sql = "select ibt_total as capacity from inner_box_type where ibt_id = {type_id} "
					.replace("{type_id}", String.valueOf(type_id));
			break;
		default: // 0, 拿散件，即认为容量为1
			return 1;
		}

		DBRow r = dbUtilAutoTran.selectSingle(sql);
		cache.put(k, r.get("CAPACITY", 0));
		tm.put(k, System.currentTimeMillis());
		return cache.get(k);
	}
	 /**
     * 将long数组返回mysql in () 的string类型
     * @param params
     * @param bracket () {} [] 
     * @return
     * @throws Exception
     */
	public String LongArraytoString(long[] params,String 
			sbracket,String 
			ebracket)throws Exception{
		if(params.length<=0){
			return sbracket+ebracket ;
		}else{
			String ret=sbracket;
			for(int i=0;i<params.length;i++){
				ret+=Long.toString(params[i]);
				ret+=",";
			}
			ret =ret.substring(0, ret.lastIndexOf(","));
		    ret+=ebracket;	
		    return ret ;
		}
	}
	/**
	 * 分配可用库存，lot_number暂时未做处理
	 * @param ps_id
	 * @param pc_id
	 * @param title_id
	 * @param container_type
	 * @param type_id
	 * @param num
	 * @param lot_number
	 * @return
	 * @throws Exception
	 */
	public DBRow[] allocateProductStore(long ps_id, long pc_id, long title_id,
			int container_type, long type_id, int num, String lot_number)
			throws Exception {

		JSONArray availableList = availableList(ps_id, title_id, pc_id,
				container_type, type_id, lot_number, num);
		int allocated = 0;
		JSONArray statements = new JSONArray();
		List<DBRow> result = new ArrayList<DBRow>();
		int capacityOfTheType = containerCapacity(container_type, type_id);
		for (int i = 0; num > allocated && i < availableList.length(); i++) {
			// 返回列表的第一个字段是distinctKey, 其他字段从1开始
			long pick_con_id = availableList.getJSONArray(i).getLong(1);
			// long time_number = availableList.getJSONArray(i).getLong(2);
			long slc_id = availableList.getJSONArray(i).getLong(3);
			long from_con_id = availableList.getJSONArray(i).getLong(4);
			int from_container_type = availableList.getJSONArray(i).getInt(5);
			long from_container_type_id = availableList.getJSONArray(i)
					.getLong(6);
			String type = availableList.getJSONArray(i).getString(7);
			int available_quantity = availableList.getJSONArray(i).getInt(8);

			int capacity = capacityOfTheType;
			if (container_type == 0) { // 拿散件的情况，尽量多拿
				capacity = (num - allocated) >= available_quantity ? available_quantity
						: num - allocated;
			}

			JSONObject statement = new JSONObject();
			JSONObject params = new JSONObject();
			params.put("pc_id", pc_id);
			params.put("capacity", capacity);
			params.put("title_id", title_id);
			if (type.equals("Container")) {
				// 根据type分别设置locked_quantity数值
				// 因同一个容器，不允许放置不同货主的同一种货品，即Container-->Product关系上可以不带title_id
				// type=Container
				// match (c:Container { con_id:{id}})-[r:CONTAINS]->(p:Product {
				// pc_id:{pc_id}})
				// set r.locked_quantity = r.locked_quantity + {capacity)
				statement
						.put("statement",
								"match (c:Container { con_id:{id}}),(p:Product { pc_id:{pc_id},title_id:{title_id}}) "
										+ "merge (c)-[r:CONTAINS]->(p) "
										+ "on create set r.locked_quantity = {capacity}, r.quantity=0 " // 从TLP中拿散件的情况
										+ "on match  set r.locked_quantity = r.locked_quantity + {capacity} "
										+ "return 'Container' as type,c.con_id as id,r.locked_quantity as locked_quantity");
				params.put("id", pick_con_id);
			} else {
				// type=Location
				// match (slc:StorageLocationCatalog { slc_id:{id}}),
				// (p:Product)
				// create (slc)-[r:AVAILABLE_LOCK
				// {title_id:{title+id},quantity:{capacity}}]->(p)
				// 如果这种关系之前存在，那么就和之前的合并，并将锁定的quantity数量累加上去
				// 注意：2D位置上的锁，需要和container_type,type_id相关
				statement
						.put("statement",
								"match (slc:StorageLocationCatalog { slc_id:{id}}), (p:Product {pc_id:{pc_id},title_id:{title_id}}) "
										+ "merge (slc)-[r:AVAILABLE_LOCK {title_id:{title_id},container_type:{container_type},type_id:{type_id}}]->(p) "
										+ "on create set r.quantity =  {capacity} "
										+ "on match  set r.quantity = r.quantity + {capacity} "
										+ "return 'Location' as type,slc.slc_id as id,r.quantity as locked_quantity");
				params.put("id", slc_id);

				params.put("container_type", container_type);
				params.put("type_id", type_id);
			}
			statement.put("parameters", params);
			statements.put(statement);

			DBRow r = new DBRow();
			r.add("from_container_type", from_container_type);
			r.add("from_container_type_id", from_container_type_id);
			r.add("from_con_id", from_con_id);
			r.add("pick_con_id", pick_con_id);
			r.add("slc_id", slc_id);
			r.add("pick_count", capacity);
			result.add(r);

			allocated += container_type == 0 ? capacity : 1;
		}
		if (num > allocated)
			throw new Exception("没有分配完成！");
		JSONObject request = new JSONObject();
		request.put("statements", statements);
		JSONResource response = client.json(restBaseUrl + "/" + ps_id
				+ "/db/data/transaction/commit", new Content(
				"application/json", request.toString().getBytes("UTF-8")));
		if (response.object().getJSONArray("errors").length() > 0)
			throw new Exception(response.object().getJSONArray("errors")
					.toString(4));

		return result.toArray(new DBRow[0]);
	}

	/**
	 * 统计指定位置上物理库存
	 * @param ps_id
	 * @param slc_area
	 * @param slc_id
	 * @return
	 * @throws Exception
	 */
	public Map<String, DBRow> physicalCount(long ps_id, long slc_area,
			long slc_id) throws Exception {
		JSONObject query = new JSONObject();
		query.put("query",
				"MATCH (slc:StorageLocationCatalog)<-[loc:LOCATES]-(c:Container), "
						+ "(c)-[r:CONTAINS*1..]->(p:Product) "
						+ "where true "
						+ (slc_area == 0 ? ""
								: "and slc.slc_area = {slc_area} ")
						+ (slc_id == 0 ? "" : "and slc.slc_id = {slc_id}")
						+ "return " + "c.con_id as con_id, "
						+ "p.pc_id as pc_id, "
						+ "sum((r[-1]).quantity) as quantity, "
						+ "slc.slc_id as slc_id, "
						+ "c.container_type as container_type, "
						+ "c.type_id as type_id," + "loc.title_id as title_id");
		JSONObject params = new JSONObject();
		if (slc_area != 0)
			params.put("slc_area", slc_area);
		if (slc_id != 0)
			params.put("slc_id", slc_id);

		query.put("params", params);

		JSONResource response = client.json(restBaseUrl + "/" + ps_id
				+ "/db/data/cypher", new Content("application/json", query
				.toString().getBytes("UTF-8")));

		if (response.object().has("exception"))
			throw new Exception(response.object().toString(4));
		JSONArray data = response.object().getJSONArray("data");
		Map<String, DBRow> result = new HashMap<String, DBRow>();
		for (int i = 0; i < data.length(); i++) {
			JSONArray row = data.getJSONArray(i);
			DBRow dbRow = new DBRow();
			dbRow.add("con_id", row.getLong(0));
			dbRow.add("pc_id", row.getLong(1));
			dbRow.add("quantity", row.getInt(2));
			dbRow.add("slc_id", row.getLong(3));
			dbRow.add("container_type", row.getInt(4));
			dbRow.add("type_id", row.getLong(5));
			dbRow.add("title_id", row.getLong(6));
			result.put(row.getLong(0) + "-" + row.getLong(1), dbRow);
		}
		return result;
	}

	/**
	 * 统计指定容器类型的产品库存
	 * @param ps_id
	 * @param title_id
	 * @param pc_id
	 * @param container_type
	 * @param type_id
	 * @param lot_number
	 * @return
	 * @throws Exception
	 */
	public int availableCount(long ps_id, long title_id, long pc_id,
			int container_type, long type_id, String lot_number)
			throws Exception {
		String query = null;
		String lot_number_cond = lot_number != null && lot_number.length() > 0 ? ",lot_number:{lot_number}"
				: "";
		if (container_type != 0) {
			query = StringUtils
					.join(new String[] {
							"match  ",
							"(slc:StorageLocationCatalog)<-[loc:LOCATES {title_id:{title_id},pc_id:{pc_id}"
									+ lot_number_cond
									+ "}]-(c:Container),pth=(c)-[:CONTAINS*1..]->(p:Product {pc_id:{pc_id},title_id:{title_id}})  ",
							"where  ",
							" ( "
							+" (c.container_type={container_type} and c.type_id={type_id})  ",// 接地容器直接是所需要的容器类型，OK
							"    or  ",
							"    (c.container_type=3 and ( ",// 如果接地容器是TLP类型，则:1,要么直接下级是所需要的容器类型；2,要么是拿散件的情况(container_type=0)
							"         ((nodes(pth)[1]).container_type={container_type} and (nodes(pth)[1]).type_id={type_id} )   ))"
							+" )",
							"optional match ",
							"(slc:StorageLocationCatalog)-[alock:AVAILABLE_LOCK {title_id:{title_id},container_type:{container_type},type_id:{type_id}} ]->(p:Product {pc_id:{pc_id},title_id:{title_id}}) ",
							"with slc,  sum((relationships(pth)[-1]).quantity) as quantity, alock.quantity as locked_quantity ",
							"match ",
							"(slc:StorageLocationCatalog)<-[loc:LOCATES {title_id:{title_id},pc_id:{pc_id}"
									+ lot_number_cond
									+ "}]-(c:Container),pth=(c)-[:CONTAINS*1..]->(p:Product {pc_id:{pc_id},title_id:{title_id}})  ",
							"where  ",
							"( ",
							"    (c.container_type={container_type} and c.type_id={type_id})  ",// 接地容器直接是所需要的容器类型，OK
							"    or  ",
							"    (c.container_type=3 and ( ",// 如果接地容器是TLP类型，则:1,要么直接下级是所需要的容器类型；2,要么是拿散件的情况(container_type=0)
							"         ((nodes(pth)[1]).container_type={container_type} and (nodes(pth)[1]).type_id={type_id})   ))",
							")  ",
							"and ( ",
							"    (slc.is_three_dimensional = 1 and ((relationships(pth)[-1]).quantity > (relationships(pth)[-1]).locked_quantity )) ",
							"    or ",
							"    (slc.is_three_dimensional = 0 and quantity > (case (locked_quantity is NULL) when TRUE then 0 else locked_quantity end)) ",
							") ", "return count(distinct c) as count" }, " \n");
		} else { // 2014年3月17日修改了算法，拿散件时，只能从接地的TLP中拿，不许拆箱
			query = StringUtils
					.join(new String[] {
							"match  ",
							"(slc:StorageLocationCatalog)<-[loc:LOCATES {title_id:{title_id},pc_id:{pc_id}"
									+ lot_number_cond
									+ "}]-(c:Container {container_type:3})-[r:CONTAINS]->(p:Product {pc_id:{pc_id},title_id:{title_id}})  ",
							"optional match ",
							"(slc:StorageLocationCatalog)-[alock:AVAILABLE_LOCK {title_id:{title_id},container_type:{container_type},type_id:{type_id}}]->(p:Product {pc_id:{pc_id},title_id:{title_id}}) ",
							"with slc, min(loc.time_number) as time_number, sum(r.quantity) as quantity, alock.quantity as locked_quantity ",
							"match ",
							"(slc:StorageLocationCatalog)<-[loc:LOCATES {title_id:{title_id},pc_id:{pc_id}"
									+ lot_number_cond
									+ "}]-(c:Container {container_type:3})-[r:CONTAINS]->(p:Product {pc_id:{pc_id},title_id:{title_id}})  ",
							"where  ",
							// 如果是2D位置，需要这个位置上的可用数大于位置上锁定的数！
							"(slc.is_three_dimensional = 0 and quantity > (case (locked_quantity is NULL) when TRUE then 0 else locked_quantity end) ) ",
							"or ",
							// 如果是3D位置，需要这个接地容器上有可用数
							"(slc.is_three_dimensional = 1 and r.quantity > r.locked_quantity) ",
							"with   ",
							"c.con_id as from_con_id, c.container_type as from_container_type, c.type_id as from_container_type_id, ",
							"(case slc.is_three_dimensional when 0 then 'Location'+slc.slc_id else 'Container'+c.con_id end) as distinctKey, ",
							"(case slc.is_three_dimensional when 0 then time_number else loc.time_number end) as time_number, ",
							"slc.slc_id as slc_id, ",
							"(case slc.is_three_dimensional when 0 then 'Location' else 'Container' end) as type, ",
							"(case slc.is_three_dimensional when 0 then (quantity - (case (locked_quantity is NULL) when TRUE then 0 else locked_quantity end)) else (r.quantity - r.locked_quantity) end) as available_quantity ",
							"with ",
							"distinct(distinctKey) as distinctKey, available_quantity ",// 拿散件的情况下需要做distinct，因为在一个位置（2D）或TLP容器（3D）上会尽量多拿
							"return sum(available_quantity) as count" }, " \n");
		}

		JSONObject body = new JSONObject();
		body.put("query", query);
		JSONObject params = new JSONObject();
		params.put("title_id", title_id);
		params.put("pc_id", pc_id);
		params.put("container_type", container_type);
		params.put("type_id", type_id);
		if (lot_number_cond.length() > 0)
			params.put("lot_number", lot_number);
		body.put("params", params);

		JSONResource response = client.json(restBaseUrl + "/" + ps_id
				+ "/db/data/cypher", new Content("application/json", body
				.toString().getBytes("UTF-8")));
		if (response.object().has("exception"))
			throw new Exception(response.object().toString(4));

		JSONArray data = response.object().getJSONArray("data");

		return data.getJSONArray(0).getInt(0);

	}

	/**
	 * 清空指定仓库内所有东西！注意！！！这个方法一般不要使用！！！通常只是用于单元测试
	 * 
	 * @param ps_id
	 * @param reIndex
	 * 		是否清除索引定义并重建
	 * @throws Exception
	 */
	public void clearGraphDB(long ps_id, boolean reIndex) throws Exception {
		JSONArray statements = new JSONArray();
		List<String> cmds = new ArrayList<String>();
		cmds.add("match ()-[r]->() delete r");
		cmds.add("match (n) delete n");

		for (String cmd : cmds) {
			JSONObject statement = new JSONObject();
			statement.put("statement", cmd);
			statements.put(statement);
		}

		JSONObject request = new JSONObject();
		request.put("statements", statements);
		JSONResource response = client.json(restBaseUrl + "/" + ps_id
				+ "/db/data/transaction/commit", new Content(
				"application/json", request.toString().getBytes("UTF-8")));
		if (response.object().getJSONArray("errors").length() > 0)
			throw new Exception(response.object().getJSONArray("errors")
					.toString(4));

		if (reIndex) {
			cmds.clear();
			cmds.add("DROP CONSTRAINT ON (c:Container) ASSERT c.con_id IS UNIQUE");
			cmds.add("CREATE CONSTRAINT ON (c:Container) ASSERT c.con_id IS UNIQUE");
			cmds.add("drop index on :Container(container_type)");
			cmds.add("create index on :Container(container_type)");

			cmds.add("drop index on :StorageLocationCatalog(slc_id)");
			cmds.add("create index on :StorageLocationCatalog(slc_id)");

			cmds.add("drop index on :StorageLocationCatalog(ps_id)");
			cmds.add("create index on :StorageLocationCatalog(ps_id)");
			cmds.add("drop index on :StorageLocationCatalog(slc_area)");
			cmds.add("create index on :StorageLocationCatalog(slc_area)");

			cmds.add("drop index on :Product(pc_id)");
			cmds.add("create index on :Product(pc_id)");
			cmds.add("drop index on :Product(title_id)");
			cmds.add("create index on :Product(title_id)");
			cmds.add("drop index on :Product(product_line)");
			cmds.add("create index on :Product(product_line)");
			cmds.add("drop index on :Product(catalogs)");
			cmds.add("create index on :Product(catalogs)");

			for (String cmd : cmds) {
				try {
					client.json(
							restBaseUrl + "/" + ps_id + "/db/data/cypher",
							new Content("application/json", new JSONObject()
									.put("query", cmd).toString()
									.getBytes("UTF-8")));
				} catch (Exception e) {
					log.error(e.getMessage());
					System.err.println(e.getMessage());
				}
			}
		}

	}

	/**
	 * 分配物理库存
	 * @param ps_id
	 * @param container_type
	 * @param type_id
	 * @param title_id
	 * @param con_id
	 * @param pc_id
	 * @param quantity
	 * @throws Exception
	 */
	public void allocateProductStorePhysical(long ps_id, int container_type,
			long type_id, long title_id, long con_id, long pc_id, int quantity)
			throws Exception {
		JSONArray statements = new JSONArray();
		JSONObject params = new JSONObject();
		params.put("title_id", title_id);
		params.put("pc_id", pc_id);
		params.put("con_id", con_id);
		params.put("container_type", container_type);
		params.put("type_id", type_id);
		params.put("quantity", quantity);

		// 3D位置上可能的锁
		statements
				.put(new JSONObject()
						.put("statement",
								"match (slc:StorageLocationCatalog { is_three_dimensional:1 })<-[:LOCATES {title_id:{title_id}}]-(c:Container), "
										+ "(c)-[r:CONTAINS*1..]->(p:Product {pc_id:{pc_id},title_id:{title_id}}) "
										+ "where startnode(r[-1]).con_id = {con_id} and  "
										+ "startnode(r[-1]).container_type = {container_type} and startnode(r[-1]).type_id = {type_id} and "
										+ "(r[-1]).locked_quantity >= {quantity} and (r[-1]).quantity >= {quantity} "
										+ "set (r[-1]).locked_quantity = (r[-1]).locked_quantity - {quantity}, "
										+ "(r[-1]).quantity = (r[-1]).quantity - {quantity} ")
						.put("parameters", params));

		// 2D位置上可能的锁
		statements
				.put(new JSONObject()
						.put("statement",
								"match (slc:StorageLocationCatalog { is_three_dimensional:0 })<-[:LOCATES {title_id:{title_id}}]-(c:Container),"
										+ "(slc)-[alock:AVAILABLE_LOCK]->(p:Product {pc_id:{pc_id},title_id:{title_id}}), "
										+ "(c)-[r:CONTAINS*1..]->(p:Product {pc_id:{pc_id},title_id:{title_id}}) "
										+ "where startnode(r[-1]).con_id = {con_id} and "
										+ "startnode(r[-1]).container_type = {container_type} and startnode(r[-1]).type_id = {type_id} and "
										+ "(r[-1]).quantity >= {quantity} and "
										+ "alock.container_type = {container_type} and alock.type_id = {type_id} and alock.title_id = {title_id} and alock.quantity >= {quantity} "
										+ "set alock.quantity = alock.quantity - {quantity}, "
										+ "(r[-1]).quantity = (r[-1]).quantity - {quantity}")
						.put("parameters", params));

		JSONResource response = client.json(
				restBaseUrl + "/" + ps_id + "/db/data/transaction/commit",
				new Content("application/json", new JSONObject()
						.put("statements", statements).toString()
						.getBytes("UTF-8")));
		if (response.object().getJSONArray("errors").length() > 0)
			throw new Exception(response.object().getJSONArray("errors")
					.toString(4));
	}

	/**
	 * 优化图数据库，清除两类空的关系：
	 * 			Container-[r:CONTAINS]->Product,当r.quantity和r.locked_quantity均为0
	 * 			StorageLocationCatalog-[r:AVAILABLE_LOCK]->Product，当r.quantity为0
	 * @param ps_id
	 * @throws Exception
	 */
	public void optGraphDB(long ps_id) throws Exception {
		JSONArray statements = new JSONArray();
		// 清理空的容器与产品关系（CONTAINS）
		statements
				.put(new JSONObject()
						.put("statement",
								"match (c:Container)-[r:CONTAINS { quantity:0, locked_quantity:0 }]->(p:Product) delete r"));

		// 清理空的位置与产品关系（AVAILABLE_LOCK）
		statements
				.put(new JSONObject()
						.put("statement",
								"match (slc:StorageLocationCatalog)-[alock:AVAILABLE_LOCK { quantity:0 }]->(p:Product) delete alock"));

		JSONResource response = client.json(
				restBaseUrl + "/" + ps_id + "/db/data/transaction/commit",
				new Content("application/json", new JSONObject()
						.put("statements", statements).toString()
						.getBytes("UTF-8")));
		if (response.object().getJSONArray("errors").length() > 0)
			throw new Exception(response.object().getJSONArray("errors")
					.toString(4));
	}

	/****************************************************************************
	 * 以下为ProductStoreMgrIFace接口的实现方法所依赖的工具函数 * *
	 ****************************************************************************/

	private JSONObject tx(long ps_id, JSONObject... statements)
			throws Exception {
		return transaction(ps_id, new JSONArray(statements));
	}

	private JSONObject transaction(long ps_id, JSONArray statements)
			throws Exception {
		return new Neo4jTransaction(this.restBaseUrl + "/" + ps_id, this.client)
				.commit(statements);
	}

	private JSONArray query(long ps_id, JSONObject params, String... queryLines)
			throws Exception {
		JSONResource response = client.json(
				restBaseUrl + "/" + ps_id + "/db/data/cypher",
				new Content("application/json", new JSONObject()
						.put("query", StringUtils.join(queryLines, " \n"))
						.put("params", params).toString().getBytes("UTF-8")));
		if (response.object().has("exception"))
			throw new Exception(response.object().toString(4));
		

		return response.object().getJSONArray("data");

	}

	private Pattern parameterPattern = Pattern.compile("\\{\\s*(\\w+)\\s*\\}");

	private JSONObject statement(Map<String, Object> params, String... lines)
			throws Exception {
		return new JSONObject()
				.put("statement", StringUtils.join(lines, " \n")).put(
						"parameters", params);
	}

	private JSONObject statement(JSONObject params, String... lines)
			throws Exception {
		return new JSONObject()
				.put("statement", StringUtils.join(lines, " \n")).put(
						"parameters", params);
	}

	private JSONObject statement(Object[] params, String... lines)
			throws Exception {
		String s = StringUtils.join(lines, " \n");
		JSONObject paras = new JSONObject();
		// regular expression for query parameters, like {xxx} or { xxx }:
		// \{\s*(\w+)\*\}

		Matcher m = parameterPattern.matcher(s);
		List<String> paraKeys = new ArrayList<String>();
		while (m.find()) {
			String paraKey = m.group(1);
			if (!paraKeys.contains(paraKey))
				paraKeys.add(paraKey);
		}
		for (int i = 0; i < paraKeys.size(); i++) {
			if (i >= params.length)
				throw new Exception("提供的查询形式参数数目多于实际参数");
			paras.put(paraKeys.get(i), params[i]);
		}
		return new JSONObject().put("statement", s).put("parameters", paras);
	}

	private void validateProps(NodeType nt, Map<String, Object> props)
			throws Exception {
		//props中必须包含必须的属性
		if(props.keySet().containsAll(nodePropNames.get(nt))) return;
		
		throw new Exception("缺少必要的节点属性！");
	}

	private void validateProps(RelationType rt, Map<String, Object> props)
			throws Exception {
		if (relPropNames.get(rt).equals(props.keySet())) return;
		//throw new Exception("缺少必要的关系属性！");
	}

	private String paramPlaceholders(Map<String, Object> props) {
		if(props.isEmpty())return "";
		return paramPlaceholders(props, "{", "", ":", "", ",", "}");
	}
	//用于set
	private String paramPlaceequal (Map<String, Object> props,String fieldPrefix) {
		return paramPlaceholders(props, "", fieldPrefix, "=", "", ",", "");
	}

	private String paramPlaceholders(Map<String, Object> props,
			String bracketBegin, String fieldPrefix, String assign,
			String paramPrefix, String joinBy, String bracketEnd) {
		List<String> propKeys = new ArrayList<String>();
		for (String k : props.keySet()) {
			propKeys.add(fieldPrefix + k + assign + "{" + paramPrefix + k + "}");
		}
		return bracketBegin + StringUtils.join(propKeys, joinBy) + bracketEnd;
	}

	private void mergePath(JSONArray path, JSONArray statements)
			throws Exception {
		for (int i = 0; i < path.length(); i++) {
			JSONObject props = path.getJSONObject(i);
			if (props.has("con_id")) { // 容器节点
				continue;
			} else if (props.has("pc_id")) { // 产品节点
				continue;
			} else if (props.has("quantity")) { // (:Container)-[:CONTAINS]->(:Product)
				Map<String, Object> fromNode = DBRowUtils.jsonObjectAsMap(path
						.getJSONObject(i - 1));
				Map<String, Object> toNode = DBRowUtils.jsonObjectAsMap(path
						.getJSONObject(i + 1));
				Map<String, Object> params=	new HashMap<String, Object>();
				params.put("con_id", fromNode.get("con_id"));
				statements.put(statement(fromNode, "MERGE (c:Container "
						+ paramPlaceholders(params) + " )",
						"on match set " +paramPlaceequal(fromNode,"c."),
						"on create set "+paramPlaceequal(fromNode,"c."),
						"return c "
						));
				params.clear();
				params.put("pc_id", toNode.get("pc_id"));
				params.put("title_id", toNode.get("title_id"));
				statements.put(statement(toNode, "MERGE (p:Product  "
						+ paramPlaceholders(params)+" )",
						"on match set " +paramPlaceequal(toNode,"p."),
						"on create set "+paramPlaceequal(toNode,"p."),
						" return p"));
				statements
						.put(statement(
								new Object[] { fromNode.get("con_id"),
										toNode.get("pc_id"),
										toNode.get("title_id"),
										props.get("quantity"),
										props.get("locked_quantity") },

								"match (n_from:Container {con_id:{from_id}}),(n_to:Product {pc_id:{to_id},title_id:{title_id}})",
								"merge (n_from)-[r:CONTAINS]->(n_to)",
								"on create set r.quantity={quantity},r.locked_quantity = {locked_quantity}",// 如果是新建关系，则设置新值
								"on match  set r.quantity={quantity}", // 如果原关系存在，则只设置数量，不动locked_quantity
								"return r"));
			} else { // (:Container)-[:CONTAINS]->(:Container)
				Map<String, Object> fromNode = DBRowUtils.jsonObjectAsMap(path
						.getJSONObject(i - 1));
				Map<String, Object> toNode = DBRowUtils.jsonObjectAsMap(path
						.getJSONObject(i + 1));

				statements.put(statement(fromNode, "MERGE (c:Container "
						+ paramPlaceholders(fromNode) + " ) return c"));
				statements.put(statement(toNode, "MERGE (c:Container "
						+ paramPlaceholders(toNode) + " ) return c"));

				statements
						.put(statement(
								new Object[] { fromNode.get("con_id"),
										toNode.get("con_id")},
								"MATCH (n_from:Container {con_id:{from_id}}),(n_to:Container {con_id:{to_id}})",
								"MERGE (n_from)-[r:CONTAINS]->(n_to)",
								"RETURN r"));
			}
		}
	}

	public String paramSetClause(String nodeVar, Map<String, Object> props) {
		List<String> settings = new ArrayList<String>();
		for (String k : props.keySet()) {
			settings.add(nodeVar + "." + k + " = " + "{" + k + "}");
		}
		return StringUtils.join(settings, ",");
	}

	/**
	 * 向某仓库中创建单个节点的方法
	 * 
	 * @param ps_id
	 *            向哪个仓库添加节点，0意味着临时仓库（未归属具体仓库的东西），一下所有其他方法同此含义！
	 * @param nodeType
	 *            节点类型
	 * @param nodeProps
	 *            节点属性，创建不同类型节点时，给的属性不同，如： 
	 *            Product: pc_id,title_id,product_line,catalogs,p_name,union_flag
	 *            Container: con_id,container_type,type_id,container,is_full,is_has_sn 
	 *            StorageLocationCatalog:is_three_dimensional,slc_area,slc_id,slc_position,slc_position_all,slc_type,slc_x,slc_y
	 * @throws Exception
	 */

	public void addNode(long ps_id, NodeType nodeType,
			Map<String, Object> nodeProps) throws Exception {
		// 校验属性是否齐全
		validateProps(nodeType, nodeProps);
		tx(ps_id,
				statement(nodeProps, "MERGE (n:" + nodeType
						+ paramPlaceholders(nodeProps) + " ) return n"));

	}

	/**
	 * 向某仓库中创建单条关系的方法
	 * 
	 * @param ps_id
	 * @param fromNodeType
	 *            关系线起点的节点类型
	 * @param toNodeType
	 *            关系线终点的节点类型
	 * @param fromNode
	 *            关系线起点的匹配特征，例如一个容器与产品的关系，起点节点是一个容器节点，其特征由con_id表示，
	 *            这个Map中就仅此一个key
	 * @param toNode
	 *            关系线终点的匹配特征，例如一个容器与产品的关系，终点节点是一个产品节点，产品的特征是pc_id,title_id两个key
	 * @param relProps
	 *            关系上的属性，不同关系上属性不同，例如：
	 *            Container与Product的关系，上面的属性是：quantity,locked_quantity
	 *            Container与Container上关系，属性无，给空Map，不能给null！
	 *            LOCATES关系上：pc_id,title_id,lot_number,time_number
	 *            AVAILABLE_LOCK关系上：container_type,type_id,title_id,quantity
	 * @throws Exception
	 */
	public void addRelation(long ps_id, NodeType fromNodeType,
			NodeType toNodeType, Map<String, Object> fromNode,
			Map<String, Object> toNode, Map<String, Object> relProps)
			throws Exception {
		
		// 校验关系属性是否齐全
		RelationType relType = relNames.get(fromNodeType + "-" + toNodeType);
		if (!(fromNodeType == NodeType.Container && toNodeType == NodeType.Container)
				&& relType == null)
			throw new Exception("不存在此种关系：" + fromNodeType + "-" + toNodeType);

		if (!(fromNodeType == NodeType.Container && toNodeType == NodeType.Container))
			validateProps(relType, relProps);

		String stat = "";
		

		if (fromNodeType.equals(NodeType.Container)
				&& toNodeType.equals(NodeType.Product)) {
			StringBuilder createSetters = new StringBuilder();
			StringBuilder matchSetters  = new StringBuilder();
			int i = 0;
			for (String k: relProps.keySet()) {
				createSetters.append("r." + k + " = {" + k +"}");
				
				if(k.equals("quantity") || k.equals("locked_quantity")) 
					matchSetters.append( "r." + k + " = r." + k + " + {" + k +"}");
				else
					matchSetters.append("r." + k + " = {" + k +"}");
				
				i++;
				if(i<relProps.size()) {
					createSetters.append(", ");
					matchSetters.append(", ");
				}
			}
			// 如果是CP属性，MERGE时需要合并数量
			stat = String
					.format("MATCH (n_from:Container %s),(n_to:Product %s) "
							+ "MERGE (n_from)-[r:CONTAINS]->(n_to) "
							+ " ON CREATE SET " + createSetters
							+ " ON MATCH  SET " + matchSetters
							+ " RETURN r",
							paramPlaceholders(fromNode, "{", "", ":",
									"n_from_", ",", "}"),
							paramPlaceholders(toNode, "{", "", ":", "n_to_",
									",", "}"));
		} else {
			stat = String
					.format("MATCH (n_from:%s %s),(n_to:%s %s) MERGE (n_from)-[r:%s %s]->(n_to) RETURN r",
							fromNodeType,
							paramPlaceholders(fromNode, "{", "", ":",
									"n_from_", ",", "}"),
							toNodeType,
							paramPlaceholders(toNode, "{", "", ":", "n_to_",
									",", "}"), relType,
							paramPlaceholders(relProps));
		}
		Map<String, Object> params = new HashMap<String, Object>();
		for (Map.Entry<String, Object> e : fromNode.entrySet()) {
			params.put("n_from_" + e.getKey(), e.getValue());
		}
		for (Map.Entry<String, Object> e : toNode.entrySet()) {
			params.put("n_to_" + e.getKey(), e.getValue());
		}
		for (Map.Entry<String, Object> e : relProps.entrySet()) {
			params.put(e.getKey(), e.getValue());
		}

		JSONObject result = tx(ps_id, statement(params, stat));

		JSONArray data = result.getJSONArray("results").getJSONObject(0)
				.getJSONArray("data");

		if (data.length() == 0)
			throw new Exception(
					"Cannot Create Relation [try to make relation between nonexistent nodes?]");

	}

	public void removeTree(long ps_id, long con_id, JSONArray statements)
			throws Exception {
		Object[] params = new Object[] { con_id };
		// 删除可能存在的与位置的关系
		statements
				.put(statement(
						params,
						"match (slc:StorageLocationCatalog)<-[r:LOCATES]-(c:Container {con_id:{con_id}})",
						"delete r"));
		// 删除con_id及其所有下级容器及关系
		statements
				.put(statement(
						params, // 1. 先删除所有下级子容器节点与产品的包含关系
						"match",
						"(c:Container {con_id:{con_id}})-[cc:CONTAINS*]->(child:Container)-[cp:CONTAINS]->(p:Product)",
						"delete cp"))
				.put(statement(
						params, // 2. 删除所有对子节点的包含关系，并删除所有子节点
						"match",
						"path=(c:Container {con_id:{con_id}})-[cc:CONTAINS*]->(child:Container)",
						"with child,last(relationships(path)) as r",
						"delete r,child"))
				.put(statement(
						params, // 3. 删除直接对商品的CONTAINS关系
						"match",
						"(c:Container {con_id:{con_id}})-[cp:CONTAINS]->(p:Product)",
						"delete cp")).put(statement(params, // 4. 删除这个根容器
						"match (c:Container {con_id:{con_id}}) delete c"));

	}

	/**
	 * 删除一颗容器树，连同根容器及其所有下级容器全部删除
	 * @param ps_id
	 * @param con_id
	 * @throws Exception
	 */
	public void removeTree(long ps_id, long con_id) throws Exception {
		JSONArray statements = new JSONArray();
		removeTree(ps_id, con_id, statements);
		transaction(ps_id, statements);
	}

	/**
	 * 向从一个源仓库复制/更新一颗容器树（包含这个容器及其的所包含的所有下级容器）；以及放置到指定位置（可选）；对于已存在的Container->Product关系，不会修改locked_quantity值
	 * 
	 * @param src_ps_id
	 * @param dest_ps_id
	 * @param con_id
	 * @param slcProps
	 *            如果要放到指定位置，则应包含StorageLocationCatalog的全部属性:
	 *            is_three_dimensional
	 *            ,slc_area,slc_id,slc_position,slc_position_all
	 *            ,slc_type,slc_x,slc_y 如果不动位置，则为null
	 * @param locProps
	 *            如果要放到指定位置，则应包含LOCATES关系上的属性：pc_id,title_id,lot_number,
	 *            time_number ，加上slc_id
	 * 
	 * @throws Exception
	 */
	public void copyTree(long src_ps_id, long dest_ps_id, long con_id,
			Map<String, Object> locProps) throws Exception {
		Neo4jPathAnalyser analyser = analyzeTree(src_ps_id,con_id);
		copyTree(analyser, dest_ps_id, con_id, locProps);
	}
	public void copyTree(JSONObject containerTree, long dest_ps_id, Map<String, Object> locProps) throws Exception {
		Neo4jPathAnalyser analyser = new Neo4jPathAnalyser2(containerTree);
		long con_id = containerTree.getLong("con_id");
		copyTree(analyser, dest_ps_id, con_id, locProps);
	}
	private void copyTree(Neo4jPathAnalyser analyser, long dest_ps_id, long con_id,
			Map<String, Object> locProps) throws Exception {
		
		JSONArray data = analyser.getPaths();

		JSONArray statements = new JSONArray();
		
		Neo4jTransaction tx = new Neo4jTransaction(this.restBaseUrl + "/"
				+ dest_ps_id, this.client);
		
		Object[] params = new Object[] { con_id };
		long slc_id = locProps == null ? 0 : Long.valueOf(locProps.remove(
				"slc_id").toString());
		long slc_type = locProps == null ? 0 : Long.valueOf(locProps.remove(
				"slc_type").toString());
		if (slc_id != 0) { // 放置新位置，这意味着原来与原位置的关系要先解除
			validateProps(RelationType.LOCATES, locProps); // 先校验LOCATES关系属性
			statements
					.put(statement(
							params,
							"match (slc:StorageLocationCatalog)<-[r:LOCATES]-(c:Container {con_id:{con_id}})",
							"where  slc.slc_id <>" + slc_id +" or slc.slc_type <> "+slc_type ,"delete r"));

		}
		// 删除con_id及其所有下级容器及关系
		statements.put(statement(
						params, // 1. 先删除所有下级节点到它上一级的节点的关系
						"match",
						"(c:Container {con_id:{con_id}})<-[cp:CONTAINS]-(parent:Container)",
						"delete cp"))
				.put(statement(
						params, // 2. 先删除所有下级子容器节点与产品的包含关系，排除掉无须删除的，因为马上要重建
						"match",
						"(c:Container {con_id:{con_id}})-[cc:CONTAINS*]->(child:Container)-[cp:CONTAINS]->(p:Product)",
						"where not ( (child.con_id+'-'+p.pc_id+':'+p.title_id) in "
								+ analyser.getCpkeys() + ")", "delete cp"))
				.put(statement(
						params, // 3. 删除所有对子节点的包含关系，并删除所有子节点，排除掉无须删除的，因为马上要重建
						"match",
						"path=(c:Container {con_id:{con_id}})-[cc:CONTAINS*]->(child:Container)",
						"with child,last(relationships(path)) as r",
						"where not ( (startnode(r).con_id+'-'+endnode(r).con_id) in "
								+ analyser.getCckeys() + ")", "delete r,child"))
				.put(statement(
						params, // 4. 删除直接对商品的CONTAINS关系，排除掉无须删除的，因为马上要重建
						"match",
						"(c:Container {con_id:{con_id}})-[cp:CONTAINS]->(p:Product)",
						"where not ( (c.con_id+'-'+p.pc_id+':'+p.title_id) in "
								+ analyser.getCpkeys() + ")", "delete cp"));

		
		/* 如果怕一次提交语句太多，就先提交一部分（但还是在一个事务之中）
		tx.begin(statements); // 添加第一部分事务语句，删除动作

		statements = new JSONArray(); //重置事务语句数组
		*/
		
		for (int i = 0; i < data.length(); i++) {
			JSONArray path = data.getJSONArray(i);
			mergePath(path, statements);
		}

		if (slc_id != 0) { // 放置新位置
			statements
					.put(statement(
							new JSONObject(locProps).put("slc_id", slc_id).put(
									"con_id", con_id).put("slc_type",slc_type),
							"match (slc:StorageLocationCatalog {slc_id:{slc_id},slc_type:{slc_type}}),(c:Container { con_id:{con_id}})",
							"merge (c)-[loc:LOCATES {put_time:timestamp(),"
									+ paramPlaceholders(locProps, "", "", ":", "", ",", "")+ " }]->(slc)",
							"return loc"));

		}

		tx.commit(statements); //添加第二部分事务语句，并提交整个事务

	}
	
	
	public Neo4jPathAnalyser analyzeTree(long ps_id, long con_id) throws Exception {
		JSONArray data = query(ps_id,
				new JSONObject().put("con_id", con_id),
				"match path=(c:Container)-[r:CONTAINS*]->(p:Product)",
				"where c.con_id={con_id}", "return ",
				"startnode(r[-1]).con_id+'-'+p.pc_id+':'+p.title_id as cpkey,",
				"nodes(path),relationships(path)");
		

		return new Neo4jPathAnalyser(data); 
	}

	/**
	 * 遍历一个容器树，在每一个容器节点处回调一次传入的接口
	 * 
	 * 
	 */
	public JSONObject containerTree(long ps_id, long root_con_id,Long[] title_ids, long product_line,long catalog,String[] p_names,String[] model_numbers) throws Exception {
		StringBuilder conds = new StringBuilder();
		JSONObject params = new JSONObject().put("con_id", root_con_id);
		
		if(title_ids != null && title_ids.length > 0){
			conds.append(" and p.title_id in {title_ids}");
			params.put("title_ids", title_ids);
		}
		if(product_line != 0){
			conds.append(" and p.product_line = {product_line}");
			params.put("product_line", product_line);
		}
		if(catalog != 0){
			conds.append(" and {catalog} in p.catalogs");
			params.put("catalog", catalog);
		}
		if(p_names != null && p_names.length > 0){
			conds.append(" and p.p_name in {p_names}");
			params.put("p_names", p_names);
		}
		if(model_numbers != null && model_numbers.length > 0){
			conds.append(" and p.model_number in {model_numbers}");
			params.put("model_numbers", model_numbers);
		}
		
		JSONArray data = query(ps_id, params,
			"match (c:Container {con_id:{con_id}})-[rel:CONTAINS*]->(p:Product)",
			"where p.pc_id > 0",
			conds.toString(),
			"return [r IN rel | [startnode(r),r,endnode(r)]]"
		);
		
		Neo4jContainerTreeBuilder builder = new Neo4jContainerTreeBuilder(data);
		builder.build();
		return builder.getTree();
	}
	
	public void traverseTree(long ps_id, long root_con_id, int level,
			ProductStoreMgrCallbackIFace callback) throws Exception {
		if (level == 0)
			return;

		JSONArray data = query(
				ps_id,
				new JSONObject().put("con_id", root_con_id),
				"match (c:Container { con_id:{con_id}})",
				"return {data:{}} as r,c as child",
				"union",
				"match (c:Container { con_id:{con_id}})-[r1:CONTAINS]->(child:Container)",
				"return r1 as r,child as child",
				"union",
				"match (c:Container { con_id:{con_id}})-[r2:CONTAINS]->(p:Product)",
				"return r2 as r,p as child");

		DBRow nodeProps = new DBRow();
		List<DBRow> children = new ArrayList<DBRow>();
		List<DBRow> products = new ArrayList<DBRow>();
		for (int i = 0; i < data.length(); i++) {
			JSONArray r = data.getJSONArray(i);
			JSONObject rel = r.getJSONObject(0).getJSONObject("data");
			JSONObject child = r.getJSONObject(1).getJSONObject("data");
			if (child.has("con_id")) {
				if (child.getLong("con_id") == root_con_id)
					nodeProps = DBRowUtils.convertToDBRow(child);
				else{
					DBRow dbRow = DBRowUtils.convertToDBRow(rel);
					for (Iterator<String> itr = child.keys(); itr.hasNext();) {
						String k = itr.next();
						dbRow.add(k, child.get(k));
					}
					children.add(dbRow);
				}
			} else { // 需要把产品节点和CONTAINS关系上的属性合并在一起
				DBRow dbRow = DBRowUtils.convertToDBRow(rel);
				for (Iterator<String> itr = child.keys(); itr.hasNext();) {
					String k = itr.next();
					dbRow.add(k, child.get(k));
				}
				products.add(dbRow);
			}
		}

		Stack<Object> stack = callback.getTraverseStack();
		if (stack == null) {
			stack = new Stack<Object>();
			callback.setTraverseStack(stack);
		}
		Object currentNode = callback.containerNode(nodeProps,
				children.toArray(new DBRow[0]), products.toArray(new DBRow[0]));

		// 递归子节点
		stack.push(currentNode == null ? nodeProps : currentNode);
		for (DBRow c : children) {
			traverseTree(ps_id, c.get("CON_ID", 0L), level - 1, callback);
		}
		
		callback.containerNodePoped(stack.pop());

	}

	/**
	 * 比较两个仓库中，一个指定位置，返回所有有差异的“接地”容器的ID
	 * 
	 * @param src_ps_id
	 * @param dest_ps_id
	 * @param slc_id
	 * @return 返回不一致的容器ID
	 * @throws Exception
	 */
	public Set<Long> compareTree(long src_ps_id, long dest_ps_id, long slc_id)
			throws Exception {
		Set<Long> diff = new HashSet<Long>();
		Set<Long> both = new HashSet<Long>();

		// 第一步，先查出两边在slc_id指定位置上的所有接地容器集合，得出这些接地容器的差集
		JSONArray src_data = query(
				src_ps_id,
				new JSONObject().put("slc_id", slc_id),
				"match (slc:StorageLocationCatalog)<-[loc:LOCATES]-(c:Container)",
				"where slc.slc_id={slc_id}", "return c.con_id as con_id");

		for (int i = 0; i < src_data.length(); i++) {
			diff.add(src_data.getJSONArray(i).getLong(0));
		}

		JSONArray dest_data = query(
				dest_ps_id,
				new JSONObject().put("slc_id", slc_id),
				"match (slc:StorageLocationCatalog)<-[loc:LOCATES]-(c:Container)",
				"where slc.slc_id={slc_id}", "return c.con_id as con_id");

		for (int i = 0; i < dest_data.length(); i++) {
			long con_id = dest_data.getJSONArray(i).getLong(0);
			if (diff.contains(con_id)) {
				diff.remove(con_id); // 两边都有的接地容器，可以移除
				both.add(con_id); // 两边都有的接地容器，加入both集合
			} else
				diff.add(con_id); // 否则说明是一边有，另一边没有的，添加
		}

		if (both.isEmpty())
			return diff;

		String bothSet = "[" + StringUtils.join(both, ",") + "]";

		// 第二步，比较两边相同的接地容器上的结构
		src_data = query(src_ps_id, new JSONObject(),
				"match path=(c:Container)-[r:CONTAINS*]->(p:Product)",
				"where  c.con_id in " + bothSet + "", "return ",
				"startnode(r[-1]).con_id+'-'+p.pc_id+':'+p.title_id as cpkey,",
				"nodes(path),relationships(path)");
		Neo4jPathAnalyser src_analyser = new Neo4jPathAnalyser(src_data);
		

		dest_data = query(dest_ps_id, new JSONObject(),
				"match path=(c:Container)-[r:CONTAINS*]->(p:Product)",
				"where  c.con_id in " + bothSet + "", "return ",
				"startnode(r[-1]).con_id+'-'+p.pc_id+':'+p.title_id as cpkey,",
				"nodes(path),relationships(path)");
		Neo4jPathAnalyser dest_analyser = new Neo4jPathAnalyser(dest_data);

		for (long con_id : both) {
			if (!src_analyser.getPathkeys(con_id).equals(
					dest_analyser.getPathkeys(con_id))) {
				diff.add(con_id);
			}

		}

		return diff;

	}

	/**
	 * 删除指定仓库中某一个区域的所有位置（以及放置在该位置上的所有东西）
	 * 
	 * @param ps_id
	 * @param slc_area
	 * @throws Exception
	 */
	public void deleteOnArea(long ps_id, long slc_area) throws Exception {
		JSONObject params = new JSONObject().put("slc_area", slc_area);
		JSONArray statements = new JSONArray();

		// 删除slc接地容器上的所有下级容器及关系
		statements
				.put(statement(
						params, // 1. 先删除所有下级子容器节点与产品的包含关系
						"match",
						"(slc:StorageLocationCatalog {slc_area:{slc_area}})<-[r:LOCATES]-(c:Container)-[cc:CONTAINS*]->(child:Container)-[cp:CONTAINS]->(p:Product)",
						"delete cp"))
				.put(statement(
						params, // 2. 删除所有对子节点的包含关系，并删除所有子节点
						"match",
						"path=(slc:StorageLocationCatalog {slc_area:{slc_area}})<-[r:LOCATES]-(c:Container)-[cc:CONTAINS*]->(child:Container)",
						"with child,last(relationships(path)) as r",
						"delete r,child"))
				.put(statement(
						params, // 3. 删除接地容器直接对商品的CONTAINS关系
						"match",
						"(slc:StorageLocationCatalog {slc_area:{slc_area}})<-[r:LOCATES]-(c:Container)-[cp:CONTAINS]->(p:Product)",
						"delete cp"))
				.put(statement(
						params,// 4. 删除所有接地容器及关系
						"match (slc:StorageLocationCatalog {slc_area:{slc_area}})<-[r:LOCATES]-(c:Container)",
						"delete r,c"))
				.put(statement(
						params, // 5. 删除SLC节点到Product节点的AVAILABLE_LOCK关系
						"match (slc:StorageLocationCatalog {slc_area:{slc_area}})-[alock:AVAILABLE_LOCK]->(p:Product)",
						"delete alock"))
				.put(statement(params, // 6. 删除slc本身
						"match (slc:StorageLocationCatalog {slc_area:{slc_area}}) delete slc"));

		transaction(ps_id, statements);

	}

	/**
	 * 只要指定位置上接地容器所包含的产品的物理值
	 * @param src_ps_id
	 * @param dest_ps_id
	 * @param slc_id
	 * @return
	 * 			DBRow内字段：con_id,pc_id,title_id,src_quantity,dest_quantity
	 * @throws Exception
	 */
	public DBRow[] compareLocatesQuantity(long src_ps_id, long dest_ps_id,
			long slc_id) throws Exception {
		JSONArray src_data = query(
				src_ps_id,
				new JSONObject().put("slc_id", slc_id),
				"match (slc:StorageLocationCatalog {slc_id:{slc_id}})<-[loc:LOCATES]-(c:Container)-[r:CONTAINS*]->(p:Product)",
				"return c.con_id + '-' + p.pc_id + ':' + p.title_id as cpkey, ",
				"c.con_id as con_id, p.pc_id as pc_id, p.title_id as title_id, sum((r[-1]).quantity) as quantity");
		Map<String, DBRow> diff = new HashMap<String, DBRow>();
		for (int i = 0; i < src_data.length(); i++) {
			JSONArray r = src_data.getJSONArray(i);
			DBRow diffRow = DBRowUtils.convertToDBRow(r, null, "con_id",
					"pc_id", "title_id", "src_quantity");
			diffRow.add("dest_quantity", 0);
			diff.put(r.getString(0), diffRow);
		}

		JSONArray dest_data = query(
				dest_ps_id,
				new JSONObject().put("slc_id", slc_id),
				"match (slc:StorageLocationCatalog {slc_id:{slc_id}})<-[loc:LOCATES]-(c:Container)-[r:CONTAINS*]->(p:Product)",
				"return c.con_id + '-' + p.pc_id + ':' + p.title_id as cpkey, ",
				"c.con_id as con_id, p.pc_id as pc_id, p.title_id as title_id, sum((r[-1]).quantity) as quantity");
		for (int i = 0; i < dest_data.length(); i++) {
			JSONArray r = dest_data.getJSONArray(i);
			DBRow diffRow = diff.get(r.getString(0));
			if (diffRow == null) {
				// not in src, but in dest
				diffRow = DBRowUtils.convertToDBRow(r, null, "con_id", "pc_id",
						"title_id", "dest_quantity");
				diffRow.add("src_quantity", 0);
				diff.put(r.getString(0), diffRow);
			} else if (diffRow.get("src_quantity".toUpperCase(), 0) == r
					.getInt(4)) { // both in src and dest, and equals in
									// quantity
				diff.remove(r.getString(0));
			} else { // both in src and dest, but not equals in quantity
				diffRow.remove("dest_quantity".toUpperCase());
				diffRow.add("dest_quantity", r.getInt(4));
			}
		}
		return diff.values().toArray(new DBRow[0]);
	}

	/**
	 * 更新节点的方法，满足searchFor条件的所有节点会中nodeProps指出的那些属性会被替换新值
	 * 
	 * @param ps_id
	 * @param nodeType
	 * @param searchFor 
	 * 				作为查询条件，查询需要更新的节点的属性，注意：此中的keys必须是节点属性的子集，参见addNode方法注释（会校验的）
	 * 				注意：产品节点的catalogs属性是个多值类型（类似数组），但在此查询条件中不能是数组，而应该是一个Long（多值中某个值）！
	 * @param nodeProps
	 * 				更新的属性值，此中的keys必须是节点属性的子集，参见addNode方法注释（会校验的）
	 * @throws Exception
	 */
	public void updateNode(long ps_id, NodeType nodeType,
			Map<String, Object> searchFor, Map<String, Object> nodeProps)
			throws Exception {
		
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.putAll(nodeProps);
		for (Map.Entry<String, Object> e : searchFor.entrySet()) {
			params.put("search_" + e.getKey(), e.getValue());
		}
		String catalogsCondition = "";
		if (searchFor.remove("catalogs") != null) {
			catalogsCondition = "where {search_catalogs} in n.catalogs";
		}
		tx(ps_id,
				statement(
						params,
						"match (n:"
								+ nodeType
								+ paramPlaceholders(searchFor, "{", "", ":",
										"search_", ",", "}") + " ) ",
						catalogsCondition,
						"set " + paramSetClause("n", nodeProps), "return n"));

	}

	/**
	 * 搜索满足条件的某一类节点
	 * @param ps_id
	 * @param nodeType
	 * @param searchFor  参见全文
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchNodes(long ps_id, NodeType nodeType,
			Map<String, Object> searchFor) throws Exception {
		
		JSONObject params = new JSONObject(searchFor);

		String catalogsCondition = "";
		if (searchFor.remove("catalogs") != null) {
			catalogsCondition = "where {catalogs} in n.catalogs";
		}
		JSONArray data = query(ps_id, params, "match (n:" + nodeType
				+ paramPlaceholders(searchFor) + " ) ", catalogsCondition,
				"return n");

		DBRow[] result = new DBRow[data.length()];
		for (int i = 0; i < data.length(); i++) {
			JSONObject r = data.getJSONArray(i).getJSONObject(0)
					.getJSONObject("data");
			result[i] = DBRowUtils.convertToDBRow(r);
		}

		return result;
	}

	/**
	 * 批量添加同一类型多个节点
	 * @param ps_id
	 * @param nodeType
	 * @param nodeProps
	 * @throws Exception
	 */
	public void addNodes(long ps_id, NodeType nodeType, DBRow[] nodeProps)
			throws Exception {
		// 校验属性是否齐全
		JSONArray statements = new JSONArray();
		for (DBRow props : nodeProps) {
			Map<String, Object> params = DBRowUtils.dbRowAsMap(props);
			validateProps(nodeType, params);
			statements.put(statement(params, "MERGE (n:" + nodeType
					+ paramPlaceholders(params) + " ) return n"));
		}

		transaction(ps_id, statements);

	}

	/**
	 * 搜索符合条件的关系
	 * @param ps_id
	 * @param fromNodeType
	 * @param toNodeType
	 * @param fromNodeProps
	 * 			起始节点查询条件
	 * @param toNodeProps
	 * 			终止节点查询条件
	 * @param relProps
	 * 			关系上的条件，没有时给空Map，不能给null！
	 * @return
	 * @throws Exception
	 */
	public RelationBean[] searchRelations(long ps_id, NodeType fromNodeType,
			NodeType toNodeType, Map<String, Object> fromNodeProps,
			Map<String, Object> toNodeProps, Map<String, Object> relProps)
			throws Exception {
		
		// 校验关系属性是否齐全
		RelationType relType = relNames.get(fromNodeType + "-" + toNodeType);
		if (!(fromNodeType == NodeType.Container && toNodeType == NodeType.Container)
				&& relType == null)
			throw new Exception("不存在此种关系：" + fromNodeType + "-" + toNodeType);

		if (!(fromNodeType == NodeType.Container && toNodeType == NodeType.Container)
				&& !relPropNames.get(relType).containsAll(relProps.keySet()))
			throw new Exception("关系特征属性必须是关系所有属性的子集");

		Map<String, Object> params = new HashMap<String, Object>();
		for (Map.Entry<String, Object> e : fromNodeProps.entrySet()) {
			params.put("n_from_" + e.getKey(), e.getValue());
		}
		for (Map.Entry<String, Object> e : toNodeProps.entrySet()) {
			params.put("n_to_" + e.getKey(), e.getValue());
		}
		for (Map.Entry<String, Object> e : relProps.entrySet()) {
			params.put(e.getKey(), e.getValue());
		}
		String catalogsCondition = "";
		if (toNodeProps.remove("catalogs") != null) {
			catalogsCondition = "where {n_to_catalogs} in n_to.catalogs";
		}
		String stat = String
				.format("MATCH (n_from:%s %s)-[r:%s %s]->(n_to:%s %s)  %s RETURN n_from,n_to,r",
						fromNodeType,
						paramPlaceholders(fromNodeProps, "{", "", ":",
								"n_from_", ",", "}"),
						relType,
						paramPlaceholders(relProps),
						toNodeType,
						paramPlaceholders(toNodeProps, "{", "", ":", "n_to_",
								",", "}"), catalogsCondition);

		JSONArray data = query(ps_id, new JSONObject(params), stat);

		RelationBean[] result = new RelationBean[data.length()];

		for (int i = 0; i < data.length(); i++) {
			JSONArray row = data.getJSONArray(i);
			result[i] = new RelationBean(row.getJSONObject(0).getJSONObject(
					"data"), row.getJSONObject(1).getJSONObject("data"), row
					.getJSONObject(2).getJSONObject("data"));
		}

		return result;

	}
	/**
	 * 返回一棵容器树的完整结构和数据，用于界面显示（比如展示成树形）
	 * @param ps_id
	 * @param con_id
	 * @return
	 * 		返回的对象的toString()方法可以直接转化为JSON字符串，适合于返回给客户端
	 * 		如果要便于调试，使得JSON字符串以格式化形式查看，可以使用toString(4)，及以4个空格缩进层级
	 * @throws Exception
	 */
/*	public JSONObject containerTree(long ps_id, long con_id, Long... titleId) throws Exception {
		final JSONObject root = new JSONObject();
	//	final JSONObject sn = new JSONObject();
		final long  _con_id =con_id;
		final Set<Long> titleIds = new HashSet<Long>(Arrays.asList(titleId == null ? new Long[0] : titleId));

		traverseTree(ps_id, con_id, 10, new ProductStoreMgrCallbackIFace() {
			private Stack<Object> traverseStack;

			private void updateTotalQuantity(JSONObject parent, JSONObject p)
					throws Exception {
				JSONArray products = null;
				if(parent.has("products")) {
					//deep clone
					products = new JSONArray(parent.getJSONArray("products").toString());
				}
				else {
					products = new JSONArray();
				}
				
				for (int i = 0; i < products.length(); i++) {
					JSONObject o = products.getJSONObject(i);
					if (p.get("title_id").equals(o.get("title_id"))
							&& p.get("pc_id").equals(o.get("pc_id")) 
						) {
						// found
						int total_quantity = o.getInt("total_quantity")
								+ p.getInt("quantity");
						int total_locked_quantity = o
								.getInt("total_locked_quantity")
								+ p.getInt("locked_quantity");
						o.put("total_quantity", total_quantity);
						o.put("total_locked_quantity", total_locked_quantity);
						products.put(i, o);
						parent.put("products", products);
						return;
					}
				}// for

				// Not found, 说明父级没有直接包含产品
				p.put("quantity", 0);
				p.put("locked_quantity", 0);
				products.put(p);
				parent.put("products", products);
			}

			@Override
			public Object containerNode(DBRow props, DBRow[] children,
					DBRow[] products) throws Exception {
				JSONObject current = null;
				JSONObject parent = null;
				if (traverseStack.isEmpty()) { // 说明当前是根节点
					current = root;
				} else { // 说明当前节点有上级，堆栈顶端的应该就是上级节点
					parent = (JSONObject) traverseStack.peek();
					current = new JSONObject();
					parent.accumulate("children", current); // 填充父子关系
				}

				for (Object fn : props.getFieldNames()) {
					current.put(fn.toString().toLowerCase(),
							props.getValue(fn.toString()));
				}

				if (children.length > 0) {
					current.put("children", new JSONArray()); // 设置父子关系列表，等待后续回调来填充
				}
				
				JSONArray prods = new JSONArray();

				for (DBRow p : products) {
					if(!titleIds.isEmpty() && !titleIds.contains(p.get("title_id",0L))) continue;
					p.add("total_quantity", p.get("QUANTITY", 0));
					p.add("total_locked_quantity", p.get("LOCKED_QUANTITY", 0));
					DBRow[] snRows = dbUtilAutoTran.selectMutliple("select serial_number as sn from serial_product where at_lp_id="+_con_id+" and  pc_id ="+p.get("pc_id",0l));
					String[] sn=new String[snRows.length];
					for (int i=0;i<snRows.length;i++) {
						DBRow row =snRows[i];
						String s = row.getString("sn");
						sn[i]=s;
					}
					p.add("sn",sn);
					JSONObject prodInfo = new JSONObject(DBRowUtils.dbRowAsMap(p));
					for(int i = traverseStack.size()-1; i>=0; i-- ) {
						JSONObject n = (JSONObject)traverseStack.get(i);
						updateTotalQuantity(n,prodInfo);
					}
					prods.put(DBRowUtils.dbRowAsMap(p));
				}
				if (prods.length() > 0)
					current.put("products",prods);
				
				return current;

			}

			@Override
			public Stack<Object> getTraverseStack() {
				return this.traverseStack;
			}

			@Override
			public void setTraverseStack(Stack<Object> stack) {
				this.traverseStack = stack;
			}

			@Override
			public void containerNodePoped(Object node) throws Exception {
				//节点出栈后处理，检查该节点是否满足匹配条件（如果有条件的话）
				JSONObject n = (JSONObject)node;
				if(titleIds.isEmpty()) return;  //没有限制titleId条件
				JSONArray products = n.optJSONArray("products");
				if(products == null || products.length()==0){
					//这个节点应该从父级节点摘下
					if(this.traverseStack.isEmpty()) return;
					JSONObject parent = (JSONObject)this.traverseStack.peek();
					JSONArray children = parent.getJSONArray("children");
					for(int i=0; i<children.length(); i++){
						if(children.get(i).equals(n)){
							children.remove(i);
							break;
						}
					}
				}
			}

		});

		if(titleIds.isEmpty()) return root;  //没有限制titleId条件
		JSONArray products = root.optJSONArray("products");
		if(products == null || products.length()==0){
			return null;
		}
		return root;
	}*/

	/**
	 * 返回一棵容器树的完整结构和数据，用于界面显示（比如展示成树形）
	 * @param ps_id
	 * @param con_id
	 * @return
	 * 		返回的对象的toString()方法可以直接转化为JSON字符串，适合于返回给客户端
	 * 		如果要便于调试，使得JSON字符串以格式化形式查看，可以使用toString(4)，及以4个空格缩进层级
	 * @throws Exception
	 */
	public JSONObject containerTreeOld(long ps_id, long con_id, Long[] titleId, long product_line,long catalog,String[] p_names,String[] model_number) throws Exception {
		final JSONObject root = new JSONObject();
		final long  _con_id =con_id;
		final long _product_line = product_line;
		final long _catalog = catalog;
		final Set<String> pNames = new HashSet<String>(Arrays.asList(p_names == null ? new String[0] : p_names));
		final Set<String> modelNumber = new HashSet<String>(Arrays.asList(model_number == null ? new String[0] : model_number));
		final Set<Long> titleIds = new HashSet<Long>(Arrays.asList(titleId == null ? new Long[0] : titleId));

		traverseTree(ps_id, con_id, 10, new ProductStoreMgrCallbackIFace() {
			private Stack<Object> traverseStack;

			private void updateTotalQuantity(JSONObject parent, JSONObject p)
					throws Exception {
				JSONArray products = null;
				if(parent.has("products")) {
					//deep clone
					products = new JSONArray(parent.getJSONArray("products").toString());
				}
				else {
					products = new JSONArray();
				}
				
				for (int i = 0; i < products.length(); i++) {
					JSONObject o = products.getJSONObject(i);
					if (p.get("title_id").equals(o.get("title_id"))
							&& p.get("pc_id").equals(o.get("pc_id")) 
						) {
						// found
						int total_quantity = o.getInt("total_quantity")
								+ p.getInt("quantity");
						int total_locked_quantity = o
								.getInt("total_locked_quantity")
								+ p.getInt("locked_quantity");
						o.put("total_quantity", total_quantity);
						o.put("total_locked_quantity", total_locked_quantity);
						products.put(i, o);
						parent.put("products", products);
						return;
					}
				}// for

				// Not found, 说明父级没有直接包含产品
				p.put("quantity", 0);
				p.put("locked_quantity", 0);
				products.put(p);
				parent.put("products", products);
			}

			@Override
			public Object containerNode(DBRow props, DBRow[] children,
					DBRow[] products) throws Exception {
				JSONObject current = null;
				JSONObject parent = null;
				if (traverseStack.isEmpty()) { // 说明当前是根节点
					current = root;
				} else { // 说明当前节点有上级，堆栈顶端的应该就是上级节点
					parent = (JSONObject) traverseStack.peek();
					current = new JSONObject();
					//parent.accumulate("children", current); // 填充父子关系
				}

					if(parent!=null){ 
						long con_id=	props.get("con_id",0l);
						JSONArray childArray=parent.getJSONArray("children");
						int k=0;
						for(int i=0;i<childArray.length();i++){
							//获取当前children数组的各个元素
							JSONObject child =childArray.getJSONObject(i);
							//判断child存在，且有key为con_id的key，且key值等于con_id的值
							if(child.length()>0&&child.has("con_id")&&con_id==child.getLong("con_id")){
								k=i;
								//将个节点属性都加到对应的节点上去
								for (Object fn : props.getFieldNames()) {
									child.put(fn.toString().toLowerCase(),props.getValue(fn.toString()));
								}
							}
						}
						//获取父节点的children 的key值的第k个元素 key 为满足con_id相等是的key值
						current=parent.getJSONArray("children").getJSONObject(k);
					}else {//没有父节点，表示当前节点为root
						for (Object fn : props.getFieldNames()) {
							current.put(fn.toString().toLowerCase(),
							props.getValue(fn.toString()));
						}
					}
					
					/*for (Object fn : props.getFieldNames()) {
						current.put(fn.toString().toLowerCase(),
						props.getValue(fn.toString()));
					}*/

				if (children.length > 0) {
					JSONArray childArray = new JSONArray();
					for(DBRow row :children){
						JSONObject child = new JSONObject();
						for(Object o :row.getFieldNames()){
							child.put(o.toString().toLowerCase(), row.get(o));
						}
						childArray.put(child);
					}
					current.put("children", childArray); // 设置父子关系列表，等待后续回调来填充
				}
				
				JSONArray prods = new JSONArray();

				for (DBRow p : products) {
					if(!titleIds.isEmpty() && !titleIds.contains(p.get("title_id",0L))) continue;
					if(!pNames.isEmpty() && !pNames.contains(p.getString("p_name"))) continue;
					//if(!modelNumber.isEmpty() && !modelNumber.contains(p.getString("model_number"))) continue;
					if(_product_line>0l && _product_line!=p.get("product_line", 0l)) continue;
					if(_catalog>0l){
						boolean in_cata = false;
						JSONArray catalogs = (JSONArray)p.get("catalogs");
						if(catalogs != null){
							for(int i = 0; i < catalogs.length(); i++){
								if(catalogs.getLong(i) == _catalog){
									in_cata  = true;
									continue;
								}
							}
						}
						if(!in_cata) continue;
					}
					p.add("total_quantity", p.get("QUANTITY", 0));
					p.add("total_locked_quantity", p.get("LOCKED_QUANTITY", 0));
					DBRow[] snRows = dbUtilAutoTran.selectMutliple("select serial_number as sn from serial_product where at_lp_id="+_con_id+" and  pc_id ="+p.get("pc_id",0l));
					String[] sn=new String[snRows.length];
					for (int i=0;i<snRows.length;i++) {
						DBRow row =snRows[i];
						String s = row.getString("sn");
						sn[i]=s;
					}
					p.add("sn",sn);
					JSONObject prodInfo = new JSONObject(DBRowUtils.dbRowAsMap(p));
					for(int i = traverseStack.size()-1; i>=0; i-- ) {
						JSONObject n = (JSONObject)traverseStack.get(i);
						updateTotalQuantity(n,prodInfo);
					}
					prods.put(DBRowUtils.dbRowAsMap(p));
				}
				if (prods.length() > 0){
					if(prods.getJSONObject(0).has("title_id"))current.put("title_id",prods.getJSONObject(0).get("title_id"));
					current.put("products",prods);
				}
				return current;

			}

			@Override
			public Stack<Object> getTraverseStack() {
				return this.traverseStack;
			}

			@Override
			public void setTraverseStack(Stack<Object> stack) {
				this.traverseStack = stack;
			}

			@Override
			public void containerNodePoped(Object node) throws Exception {
				//节点出栈后处理，检查该节点是否满足匹配条件（如果有条件的话）
				JSONObject n = (JSONObject)node;
				//if(titleIds.isEmpty()) return;  //没有限制titleId条件
				JSONArray products = n.optJSONArray("products");
				if(products == null || products.length()==0){
					//这个节点应该从父级节点摘下
					if(this.traverseStack.isEmpty()) return;
					JSONObject parent = (JSONObject)this.traverseStack.peek();
					JSONArray children = parent.getJSONArray("children");
					for(int i=0; i<children.length(); i++){
						if(children.get(i).equals(n)){
							children.remove(i);
							break;
						}
					}
				}
			}

		});
		//if(titleIds.isEmpty()) return;  //没有限制titleId条件
		JSONArray products = root.optJSONArray("products");
		if(products == null || products.length()==0){
			return null;
		}
		
		return root;
	}
	
	
	public JSONArray searchRootContainer(
			long ps_id,
			Map<String,Object> searchForSlc,
			long type_id,
			String[] model_numbers
			)throws Exception{
		
		StringBuilder conds = new StringBuilder();
		JSONObject params = new JSONObject(searchForSlc);
		if(model_numbers != null) {
			params.put("model_number", model_numbers);
			conds.append(" and p.model_number in {model_number}");
		}
		if(type_id != 0) {
			params.put("type_id", type_id);
			conds.append(" and c.type_id = {type_id}");
		}
		JSONArray data = query(ps_id, params,
				"match (slc:StorageLocationCatalog"+paramPlaceholders(searchForSlc, "{", "", ":", "", ",", "}")+")<-[:LOCATES]-(c:Container)-[:CONTAINS*]->(p:Product)",
				"where true",
				conds.toString(),
				"return c "
				);	
		if (data.length()==0) return new JSONArray();
		JSONArray result=new JSONArray();
		for(int i=0;i<data.length();i++){
			result.put(i,data.getJSONArray(i).getJSONObject(0).getJSONObject("data"));
		}
		return result;
		
	}
	
	
	
	public List<Map<String,Object>> locationTreeOld(
			long ps_id,
			Map<String,Object> searchForSlc,
			Long[] title_ids,
			long product_line,
			long catalog,
			String[] p_names,
			String[] lot_numbers,
			int container_type,
			long type_id,
			String[] model_numbers
			)throws Exception{
		
			JSONArray cons= this.searchRootContainer(ps_id, searchForSlc, type_id, model_numbers);
			Long[]  filterConIdsArr =null ;
			if (cons.length()>0){
				filterConIdsArr	=new Long[cons.length()];
				for(int i=0; i<cons.length();i++){
					filterConIdsArr[i]=cons.getJSONObject(i).getLong("con_id");
					
				}
			}
		
		return this.locationTreeOld(ps_id, searchForSlc, title_ids, product_line, catalog, p_names, lot_numbers, container_type, type_id, model_numbers,filterConIdsArr);
		
		
		
	}
	/**
	 * 获取指定位置上的层次树（位置--容器--子容器--产品等）
	 * @param ps_id
	 * @param searchForSlc  StorageLocationCatalog的搜索条件（例如，slc_id，slc_area等）
	 * @return 
	 * 符合条件的slc对象集合，每个slc对象上有一个containers对象（其中每个是该位置上的一个容器树数组，
	 * 结构与containerTree方法返回相同）
	 */
	public JSONArray locationTree(
			long ps_id,
			Map<String,Object> searchForSlc,
			Long[] title_ids,
			long product_line,
			long catalog,
			String[] p_names,
			String[] lot_numbers,
			int container_type,
			long type_id,
			String[] model_numbers,
			Long[] con_ids
	) throws Exception {
		Set<Long> conIds = con_ids == null ? new HashSet<Long>() : new HashSet<Long>(Arrays.asList(con_ids)); 
		Set<String> lotNumbers = lot_numbers == null ? new HashSet<String>() : new HashSet<String>(Arrays.asList(lot_numbers));
		
		StringBuilder conds = new StringBuilder();
		JSONObject params = new JSONObject(searchForSlc);
		
		
		//拼装Product节点上的条件
		if(title_ids != null && title_ids.length > 0){
			conds.append(" and p.title_id in {title_ids}");
			params.put("title_ids", title_ids);
		}
		if(product_line != 0){
			conds.append(" and p.product_line = {product_line}");
			params.put("product_line", product_line);
		}
		if(catalog != 0){
			conds.append(" and {catalog} in p.catalogs");
			params.put("catalog", catalog);
		}
		if(p_names != null && p_names.length > 0){
			conds.append(" and p.p_name in {p_names}");
			params.put("p_names", p_names);
		}
		if(model_numbers != null && model_numbers.length > 0){
			conds.append(" and p.model_number in {model_numbers}");
			params.put("model_numbers", model_numbers);
		}
		//拼装LOCATES关系上的条件
		if(! lotNumbers.isEmpty()){
			conds.append(" and loc.lot_number in {lot_numbers}");
			params.put("lot_numbers", lotNumbers);
		}
		//拼装Container上的条件
		if(! conIds.isEmpty()){
			conds.append(" and c.con_id in {con_ids}");
			params.put("con_ids", conIds);
		}
		if(container_type != 0){
			conds.append(" and c.container_type = {container_type}");
			params.put("container_type", container_type);
		}
		String typeIdFilter = "";
		if(type_id != 0){
			conds.append(" and (c.container_type <> 1 or c.type_id = {type_id})");
			params.put("type_id", type_id);
			typeIdFilter = " where (endnode(r)).container_type is NULL or (endnode(r)).container_type <> 1 or (endnode(r)).type_id = {type_id} ";
		}
		
		
		JSONArray data = query(ps_id, params,
			"match (slc:StorageLocationCatalog "+ paramPlaceholders(searchForSlc) +")<-[loc:LOCATES]-(c:Container)-[rel:CONTAINS*]->(p:Product)",
			"where true ",
			conds.toString(),
			"return DISTINCT slc, collect(c)"
		);
		log.info(data.toString());
		if(data == null || data.length() == 0) return new JSONArray();
		
		//建立locationTree顶层结构
		JSONArray slcs = new JSONArray();
		for(int r=0; r<data.length(); r++){
			JSONArray row = data.getJSONArray(r);
			JSONObject slc = row.getJSONObject(0).getJSONObject("data");
			JSONArray containers = new JSONArray();
			slc.put("containers",containers);
			slcs.put(slc);
			
			JSONArray cons = row.getJSONArray(1);
			conIds = new HashSet<Long>();
			for(int c=0; c<cons.length(); c++){
				JSONObject con = cons.getJSONObject(c).getJSONObject("data");
				Long root_con_id = con.getLong("con_id");
				if(conIds.contains(root_con_id)) continue;
				conIds.add(root_con_id);
				//如果container_type=3，则忽略产品上的条件，返回整个容器树
				if(con.getInt("container_type") == 3){
					containers.put(containerTree(ps_id,root_con_id,
							null,0,0,null,null));
				}
				else {
					containers.put(containerTree(ps_id,root_con_id,
						title_ids,product_line,catalog,p_names,model_numbers));
				}
			}
			
		}
		return slcs;
		
	}
	public List<Map<String,Object>> locationTreeOld(
			long ps_id,
			Map<String,Object> searchForSlc,
			Long[] title_ids,
			long product_line,
			long catalog,
			String[] p_names,
			String[] lot_numbers,
			int container_type,
			long type_id,
			String[] model_number,
			Long... filterConIdsArr
			) throws Exception {
		Set<Long> filterConIds = filterConIdsArr == null ? new HashSet<Long>() : new HashSet<Long>(Arrays.asList(filterConIdsArr)); 
		RelationBean[] result = this.searchLocates(ps_id,NodeType.StorageLocationCatalog, searchForSlc);
		SortedMap<Long,Map<String,Object>> locTree = new TreeMap<Long,Map<String,Object>>();
		List<String> lotNumbers = Arrays.asList(lot_numbers == null ? new String[0] : lot_numbers);
		for(RelationBean rel:result){
			Map<String,Object> c1 = rel.getFromNodeProps();
			Map<String,Object> slc = rel.getToNodeProps();
			Map<String,Object> rel_pro = rel.getRelProps();
			long slc_id = Long.valueOf(slc.get("slc_id").toString());
			long con_id = Long.valueOf(c1.get("con_id").toString());
			if(filterConIds.size() >0 && !filterConIds.contains(con_id)) continue;
			
			String lot_num = rel_pro.get("lot_number").toString();
			if(container_type != 0 && container_type != Integer.valueOf(c1.get("container_type").toString())) continue; //skip
			if(container_type != 1 && type_id != 0 && type_id != Long.valueOf(c1.get("type_id").toString())) continue; //如果是CLP验证type_id如果是TLP则过。
			if(!lotNumbers.isEmpty() && !lotNumbers.contains(lot_num))continue;
			JSONObject conTree = this.containerTree(ps_id, con_id, title_ids, product_line, catalog, p_names,model_number);
			if(conTree == null) continue;
			
			List<JSONObject> containers = null;
			if(locTree.containsKey(slc_id)){
				containers = (List<JSONObject>)locTree.get(slc_id).get("containers");
			}else {
				containers = new ArrayList<JSONObject>();
				slc.put("containers", containers);
				locTree.put(slc_id, slc);
			}
		/*	if(!conTree.has("children")){
				conTree.put("lot_number", lot_num);
			}*/
			Map<String,Object> props =new HashMap<String, Object>();
			props.put("lot_number", lot_num);
			addParamConTree(conTree,props);
			containers.add(conTree);
		}
		return new ArrayList<Map<String,Object>>(locTree.values());
	}
	/**@author Administrator
	 * 往containerTree JSONObject 里面加参数
	 * @param conTree
	 * @param props
	 * @throws Exception
	 */
	public void  addParamConTree(JSONObject conTree ,Map<String,Object> props)throws Exception{
		for(String key :props.keySet()){
			conTree.put(key, props.get(key));
		}
		if(conTree.has("children")){
			JSONArray  childrens=  conTree.getJSONArray("children");
			if(childrens.length()>0){
				for(int i =0 ;i<childrens.length();i++){
				addParamConTree(childrens.getJSONObject(i),props);
				}
			}
		}
		
	}
	/**
	 * 返回某个容器的父级容器层次（容器--父容器--父容器--...--位置）
	 * @param ps_id
	 * @param con_id
	 * @return 当位置存在时，返回位置上的容器树（无分支）;当位置不存在时，返回null
	 * @throws Exception
	 */
	public Map<String, Object> parentContainerTree(long ps_id, long con_id) throws Exception {
		JSONObject params = new JSONObject();
		params.put("con_id", con_id);
		JSONArray data = query(ps_id, params, 
				"match p=(:Container{con_id:{con_id}})<-[:CONTAINS*0..]-(:Container)-[loc:LOCATES]->(slc:StorageLocationCatalog)",
		
				"return nodes(p) as path, loc, slc");
		
		Map<String, Object> result = null;
		if(data.length()!=0){
			data = data.getJSONArray(0);
			JSONArray path = data.getJSONArray(0);
			JSONObject container = null;
			for (int i = 0; i < path.length(); i++) {
				JSONObject node = path.getJSONObject(i).getJSONObject("data");
				if(node.has("slc_id")){
					result = DBRowUtils.jsonObjectAsMap(node);
				}else if(node.has("con_id")){
					if(container != null){
						JSONArray ctnrs = new JSONArray().put(container);
						node.put("children", ctnrs);
					}
					container=node;
				}
			}
			if(result != null){
				JSONArray ctnrs = new JSONArray().put(container);
				result.put("containers", ctnrs);
			}
		}
		return result;
	}
	
	/**
	 * 获取整个tree
	 * @param ps_id
	 * @param con_id
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public JSONObject wholeContainerTree(long ps_id, long con_id) throws Exception{
		
		JSONObject	parentTree =new JSONObject(parentContainerTree(ps_id,con_id));
		if(parentTree==null){
			return new JSONObject();
		}
		JSONObject childTree =containerTree(ps_id, con_id, null, 0l, 0l, null, null);
		if(childTree==null){
			 return new JSONObject();
		}
		if(parentTree.has("containers")){
			JSONObject ctnr = parentTree.getJSONArray("containers").getJSONObject(0);
			while(ctnr.has("children")){
				//lastChild = ctnr.getJSONArray("children").getJSONObject(0);
				ctnr = ctnr.getJSONArray("children").getJSONObject(0);
			}
			//ctnr = childTree;
			if(childTree.has("children")){
				ctnr.put("children", childTree.getJSONArray("children"));
			}
			if(childTree.has("products")){
				ctnr.put("products", childTree.getJSONArray("products"));
			}
		}
		return parentTree;
	}
/*	
	public JSONObject recursiveParentTree(JSONObject param)throws Exception{
		if(param.has("container")){
			if(param.getJSONObject("container").has("children")){
				return recursiveParentTree(param.getJSONObject("children"));
			}else{
				return param.getJSONObject("container");
			}
		}else {
			if(param.has("children")){
				return recursiveParentTree(param.getJSONObject("children"));
			}else{
				return param;
			}
		}
	}*/
	/**
	 * 不考虑容器结构和拿货规则的产品数量
	 * @param ps_id
	 * @param slc_area
	 * @param slc_type
	 * @param slc_id
	 * @param title_id
	 * @param pc_id
	 * @param lot_number
	 * @param catalogs
	 * @param product_line
	 * @param groupBy   可指定pc_id,title_id,lot_number,con_id（接地容器）,slc_id,slc_type
	 * @return 
	 * 		返回字段和groupBy指定的字段有关,例如：
	 * 		当groupBy指定title_id,pc_id,slc_id时，返回：
	 * 		title_id, pc_id, p_name, slc_id, available, physical, locked
	 * 		当groupBy指定title_id,pc_id,lot_number时，返回：
	 * 		title_id, pc_id, p_name, lot_number, available, physical, locked
	 * 		当groupBy指定title_id,pc_id时，返回：
	 * 		title_id, pc_id, p_name, available, physical,locked
	 * 		当groupBy指定title_id时，返回：
	 * 		title_id, available, physical, locked
	 * 		当groupBy指定pc_id时，返回：
	 * 		pc_id, p_name, available, physical,locked
	 * 		当groupBy指定lot_number时，返回：
	 * 		lot_number, available, physical,locked
	 *		当groupBy指定pc_id,lot_number时，返回：
	 * 		pc_id, p_name, lot_number, available, physical,locked
	 * 		当groupBy为空时，返回：
	 * 		available, physical,locked
	 * @throws Exception
	 */
	
	public DBRow[] productCount(long ps_id, long slc_area,long slc_type, long slc_id,
			int container_type, long[] title_id, long pc_id, String lot_number,
			long[] catalogs, long product_line, Set<String> groupBy)
			throws Exception {

		StringBuilder slc_cond = new StringBuilder();
		StringBuilder p_cond = new StringBuilder();
		StringBuilder alock_cond = new StringBuilder();
		JSONObject params = new JSONObject();

		if (slc_area != 0) {
			slc_cond.append(" and slc.slc_area = {slc_area}");
			params.put("slc_area", slc_area);

		}
		if (slc_id != 0) {
			slc_cond.append(" and slc.slc_id = {slc_id}");
			params.put("slc_id", slc_id);
		}
		if(slc_type!=0){
			slc_cond.append(" and slc.slc_type = {slc_type}");
			params.put("slc_type", slc_type);
		}
		if (container_type != 0) {
			slc_cond.append(" and root.container_type = {container_type}");
			params.put("container_type", container_type);
		}
		if (title_id != null && title_id.length > 0) {
			p_cond.append(" and p.title_id in {title_id}");
			alock_cond.append(" and alock.title_id in {title_id}");
			params.put("title_id", title_id);
		}
		if (pc_id != 0) {
			p_cond.append(" and p.pc_id = {pc_id}");
			params.put("pc_id", pc_id);
		}
		if (product_line != 0) {
			p_cond.append(" and p.product_line = {product_line}");
			params.put("product_line", product_line);
		}
		if (catalogs != null && catalogs.length > 0) {
			p_cond.append(" and any(x IN p.catalogs WHERE x IN {catalogs})");
			params.put("catalogs", catalogs);
		}
		if (lot_number != null && lot_number.length() > 0) {
			slc_cond.append(" and loc.lot_number = {lot_number}");
			params.put("lot_number", lot_number);
		}

		StringBuilder groupByFieldsWith = new StringBuilder();
		StringBuilder groupByFields = new StringBuilder();
		List<String> groupByFieldsList = new ArrayList<String>();
		if (groupBy.contains("title_id")) {
			groupByFieldsWith.append("p.title_id as title_id, ");
			groupByFields.append("title_id, ");
			groupByFieldsList.add("title_id");
		}
		if (groupBy.contains("pc_id")) {
			groupByFieldsWith.append("p.pc_id as pc_id, p.p_name as p_name, ");
			groupByFields.append("pc_id, p_name, ");
			groupByFieldsList.add("pc_id");
			groupByFieldsList.add("p_name");
		}
		if (groupBy.contains("lot_number")) {
			groupByFieldsWith.append("loc.lot_number as lot_number, ");
			groupByFields.append("lot_number, ");
			groupByFieldsList.add("lot_number");
		}
		if (groupBy.contains("slc_id")) {
			groupByFieldsWith.append("slc.slc_id as slc_id, ");
			groupByFields.append("slc_id, ");
			groupByFieldsList.add("slc_id");
		}
		if (groupBy.contains("slc_type")) {
			groupByFieldsWith.append("slc.slc_type as slc_type, ");
			groupByFields.append("slc_type, ");
			groupByFieldsList.add("slc_type");
		}
		if (groupBy.contains("con_id")) {
			groupByFieldsWith.append("root.con_id as con_id, ");
			groupByFields.append("con_id, ");
			groupByFieldsList.add("con_id");
		}

		JSONArray data = query(
				ps_id,
				params,
				"match (c:Container)-[cp:CONTAINS]->(p:Product)",
				"where true",
				p_cond.toString(),
				"with c,cp,p",
				"match (slc:StorageLocationCatalog)<-[loc:LOCATES]-(root:Container)",
				"where ( root.con_id = c.con_id or ( (root)-[:CONTAINS*]->(c) ) )",
				slc_cond.toString(),
				"optional match (slc)-[alock:AVAILABLE_LOCK]->(p)",
				"where true",
				alock_cond.toString(),
				"with",
				groupByFieldsWith.toString(),
				"sum(CASE alock WHEN NULL THEN 0 ELSE alock.quantity END) as alock_quantity,",
				"sum(cp.quantity) as physical,",
				"sum(cp.locked_quantity) as locked_quantity",
				"return",
				groupByFields.toString(), "physical,",
				"(physical - alock_quantity - locked_quantity) as available,",
				"(alock_quantity + locked_quantity) as locked");

		groupByFieldsList.add("physical");
		groupByFieldsList.add("available");
		groupByFieldsList.add("locked");
		return DBRowUtils.jsonArrayAsDBRowArray(data,
				groupByFieldsList.toArray(new String[0]));

	}
	
	/**
	 * 已弃用
	 * @param ps_id
	 * @param slc_area
	 * @param slc_id
	 * @param container_type
	 * @param title_id
	 * @param pc_id
	 * @param lot_number
	 * @param catalogs
	 * @param product_line
	 * @param groupBy
	 * @return
	 * @throws Exception
	 */
	public DBRow[] productCount(long ps_id, long slc_area, long slc_id,
			int container_type, long[] title_id, long pc_id, String lot_number,
			long[] catalogs, long product_line, Set<String> groupBy)
					throws Exception {
		
		StringBuilder slc_cond = new StringBuilder();
		StringBuilder p_cond = new StringBuilder();
		StringBuilder alock_cond = new StringBuilder();
		JSONObject params = new JSONObject();
		
		if (slc_area != 0) {
			slc_cond.append(" and slc.slc_area = {slc_area}");
			params.put("slc_area", slc_area);
			
		}
		if (slc_id != 0) {
			slc_cond.append(" and slc.slc_id = {slc_id}");
			params.put("slc_id", slc_id);
		}
		if (container_type != 0) {
			slc_cond.append(" and root.container_type = {container_type}");
			params.put("container_type", container_type);
		}
		if (title_id != null && title_id.length > 0) {
			p_cond.append(" and p.title_id in {title_id}");
			alock_cond.append(" and alock.title_id in {title_id}");
			params.put("title_id", title_id);
		}
		if (pc_id != 0) {
			p_cond.append(" and p.pc_id = {pc_id}");
			params.put("pc_id", pc_id);
		}
		if (product_line != 0) {
			p_cond.append(" and p.product_line = {product_line}");
			params.put("product_line", product_line);
		}
		if (catalogs != null && catalogs.length > 0) {
			p_cond.append(" and any(x IN p.catalogs WHERE x IN {catalogs})");
			params.put("catalogs", catalogs);
		}
		if (lot_number != null && lot_number.length() > 0) {
			slc_cond.append(" and loc.lot_number = {lot_number}");
			params.put("lot_number", lot_number);
		}
		
		StringBuilder groupByFieldsWith = new StringBuilder();
		StringBuilder groupByFields = new StringBuilder();
		List<String> groupByFieldsList = new ArrayList<String>();
		if (groupBy.contains("title_id")) {
			groupByFieldsWith.append("p.title_id as title_id, ");
			groupByFields.append("title_id, ");
			groupByFieldsList.add("title_id");
		}
		if (groupBy.contains("pc_id")) {
			groupByFieldsWith.append("p.pc_id as pc_id, p.p_name as p_name, ");
			groupByFields.append("pc_id, p_name, ");
			groupByFieldsList.add("pc_id");
			groupByFieldsList.add("p_name");
		}
		if (groupBy.contains("lot_number")) {
			groupByFieldsWith.append("loc.lot_number as lot_number, ");
			groupByFields.append("lot_number, ");
			groupByFieldsList.add("lot_number");
		}
		if (groupBy.contains("slc_id")) {
			groupByFieldsWith.append("slc.slc_id as slc_id, ");
			groupByFields.append("slc_id, ");
			groupByFieldsList.add("slc_id");
		}
		if (groupBy.contains("con_id")) {
			groupByFieldsWith.append("root.con_id as con_id, ");
			groupByFields.append("con_id, ");
			groupByFieldsList.add("con_id");
		}
		
		JSONArray data = query(
				ps_id,
				params,
				"match (c:Container)-[cp:CONTAINS]->(p:Product)",
				"where true",
				p_cond.toString(),
				"with c,cp,p",
				"match (slc:StorageLocationCatalog)<-[loc:LOCATES]-(root:Container)",
				"where ( root.con_id = c.con_id or ( (root)-[:CONTAINS*]->(c) ) )",
				slc_cond.toString(),
				"optional match (slc)-[alock:AVAILABLE_LOCK]->(p)",
				"where true",
				alock_cond.toString(),
				"with",
				groupByFieldsWith.toString(),
				"sum(CASE alock WHEN NULL THEN 0 ELSE alock.quantity END) as alock_quantity,",
				"sum(cp.quantity) as physical,",
				"sum(cp.locked_quantity) as locked_quantity", "return",
				groupByFields.toString(), "physical,",
				"(physical - alock_quantity - locked_quantity) as available,",
				"(alock_quantity + locked_quantity) as locked");
		
		groupByFieldsList.add("physical");
		groupByFieldsList.add("available");
		groupByFieldsList.add("locked");
		return DBRowUtils.jsonArrayAsDBRowArray(data,
				groupByFieldsList.toArray(new String[0]));
		
	}
	/**
	 * 
	 * 要查某个con_id（不管是否接地）所在的位置，就给参数：
	 * NodeType.Container,  searchFor是 con_id : xxxxx
	 * 那么返回的RelationBean[]  通常只有一个元素，除非你这个容器出现在多个位置（那样可能是关系错了）
	 * RelationBean里面包含了这个容器所在的根容器（接地容器）、所在SLC节点，以及关系上的全部属性
	 * 如果你要查某个产品 所在的位置，就给参数：
	 * NodeType.Product,  searchFor是任意产品属性（比如pc_id, title_id, catalogs等）
	 * 返回的RelationBean[]是这个产品所在的所有位置的LOCATES关系（含SLC和接地容器）的全部信息
	 * 如果你要查  符合某种条件的 SLC上全部的接地容器，就给参数：
	 * NodeType.StorageLocationCatalog  ,  searchFor是任意SLC的属性（比如slc_area,slc_id等等）
	 * 返回的RelationBean[]是满足条件上的SLC的全部LOCATES关系（含接地容器节点和SLC节点的全部信息）
	 * 
	 * @param ps_id
	 * @param nodeType
	 * @param searchFor
	 * @return
	 * @throws Exception
	 */
	public RelationBean[] searchLocates(long ps_id, NodeType nodeType,
			Map<String, Object> searchFor) throws Exception {
		

		JSONArray data = null;
		JSONObject params = new JSONObject(searchFor);
		switch (nodeType) {
		case Product:
			String catalogsCond = "";

			if (searchFor.remove("catalogs") != null) {
				catalogsCond = "where {catalogs} in p.catalogs";
			}
			data = query(
					ps_id,
					params,
					"match (slc:StorageLocationCatalog)<-[loc:LOCATES]-(c:Container)-[r:CONTAINS*]->(p:Product "
							+ paramPlaceholders(searchFor) + ")", catalogsCond,
					"return distinct(c),slc,loc");
			break;
		case Container:
			data = query(
					ps_id,
					params,
					"match (slc:StorageLocationCatalog)<-[loc:LOCATES]-(root:Container "
							+ paramPlaceholders(searchFor) + ")",
					"return root,slc,loc",
					"union",
					"match (slc:StorageLocationCatalog)<-[loc:LOCATES]-(root:Container)-[r:CONTAINS*]->(c:Container "
							+ paramPlaceholders(searchFor) + ")",
					"return root,slc,loc");
			break;
		case StorageLocationCatalog:
			data = query(ps_id, params, "match (slc:StorageLocationCatalog "
					+ paramPlaceholders(searchFor)
					+ ")<-[loc:LOCATES]-(c:Container) return c,slc,loc");
			break;
		}

		RelationBean[] result = new RelationBean[data.length()];

		for (int i = 0; i < data.length(); i++) {
			JSONArray row = data.getJSONArray(i);
			result[i] = new RelationBean(row.getJSONObject(0).getJSONObject(
					"data"), row.getJSONObject(1).getJSONObject("data"), row
					.getJSONObject(2).getJSONObject("data"));
		}

		return result;

	}

	/**
	 * 删除符合条件的关系
	 * @param ps_id
	 * @param fromNodeType  指定关系起始节点类型
	 * @param toNodeType	指定关系终止节点类型
	 * @param fromNodeProps 指定关系起始节点的查询条件（如不指定，则为空Map）
	 * @param toNodeProps	指定关系终止节点的查询条件（如不指定，则为空Map）
	 * @param relProps		指定关系上的查询条件（如不指定，则为空Map）
	 * @throws Exception
	 */
	public void removeRelations(long ps_id, NodeType fromNodeType,
			NodeType toNodeType, Map<String, Object> fromNodeProps,
			Map<String, Object> toNodeProps, Map<String, Object> relProps)
			throws Exception {
		
		// 校验关系属性是否齐全
		RelationType relType = relNames.get(fromNodeType + "-" + toNodeType);
		if (!(fromNodeType == NodeType.Container && toNodeType == NodeType.Container)
				&& relType == null)
			throw new Exception("不存在此种关系：" + fromNodeType + "-" + toNodeType);

		if (!(fromNodeType == NodeType.Container && toNodeType == NodeType.Container)
				&& !relPropNames.get(relType).containsAll(relProps.keySet()))
			throw new Exception("关系特征属性必须是关系所有属性的子集");

		Map<String, Object> params = new HashMap<String, Object>();
		for (Map.Entry<String, Object> e : fromNodeProps.entrySet()) {
			params.put("n_from_" + e.getKey(), e.getValue());
		}
		for (Map.Entry<String, Object> e : toNodeProps.entrySet()) {
			params.put("n_to_" + e.getKey(), e.getValue());
		}
		for (Map.Entry<String, Object> e : relProps.entrySet()) {
			params.put(e.getKey(), e.getValue());
		}
		String catalogsCondition = "";
		if (toNodeProps.remove("catalogs") != null) {
			catalogsCondition = "where {n_to_catalogs} in n_to.catalogs";
		}
		String stat = String
				.format("MATCH (n_from:%s %s)-[r:%s %s]->(n_to:%s %s)  %s delete r",
						fromNodeType,
						paramPlaceholders(fromNodeProps, "{", "", ":",
								"n_from_", ",", "}"),
						relType,
						paramPlaceholders(relProps),
						toNodeType,
						paramPlaceholders(toNodeProps, "{", "", ":", "n_to_",
								",", "}"), catalogsCondition);

		tx(ps_id, statement(params, stat));

	}

	/**
	 * 返回一个容器树内所有容器节点的con_id值的集合
	 * @param ps_id
	 * @param root_con_id
	 * @return
	 * 		如果根节点存在的话，返回的集合中也包含root_con_id
	 * 		如果只有根节点，没有下级容器，则集合中只有root_con_id
	 * 		如果根节点不存在，则返回空集合
	 * @throws Exception
	 */
	public Set<Long> containers(long ps_id, long root_con_id) throws Exception {
		JSONArray con_ids = query(ps_id,
				new JSONObject().put("con_id", root_con_id),
				"match (root:Container {con_id:{con_id}})",
				"optional match (root)-[:CONTAINS*]->(c:Container)",
				"return [root.con_id]+collect(distinct c.con_id) as con_ids");

		Set<Long> result = new HashSet<Long>();
		if (con_ids.length() > 0) {
			con_ids = con_ids.getJSONArray(0).getJSONArray(0);
			for (int i = 0; i < con_ids.length(); i++) {
				result.add(con_ids.getLong(i));
			}
		}
		return result;
	}

	/**
	 * 
	 * @param ps_id
	 * @param con_id
	 * @param title_id
	 * @param pc_id
	 * @return  字段包括： con_id,pc_id,available,physical,locked
	 * @throws Exception
	 */
	public DBRow[] productCountOfContainer(long ps_id, long con_id,
			long title_id, long pc_id) throws Exception {
		JSONObject params = new JSONObject();
		StringBuilder p_cond = new StringBuilder();
		params.put("con_id", con_id);
		if (title_id != 0) {
			params.put("title_id", title_id);
			p_cond.append("and p.title_id = {title_id}");
		}
		if (pc_id != 0) {
			params.put("pc_id", pc_id);
			p_cond.append("and p.pc_id = {pc_id}");
		}
		JSONArray data = query(
				ps_id,
				params,
				"match (root:Container {con_id:{con_id}})-[:CONTAINS]->(c:Container)-[r:CONTAINS*]->(p:Product)",
				"where true", p_cond.toString(), "with c.con_id as con_id,",
				"sum((r[-1]).quantity) as physical,",
				"sum((r[-1]).locked_quantity) as locked",
				"return con_id, physical, locked, ",
				"(physical - locked) as available");

		return DBRowUtils.jsonArrayAsDBRowArray(data, "con_id", "physical",
				"locked", "available");
	}

	/**
	 * 删除所有符合条件的节点
	 * 
	 * @param ps_id
	 * @param nodeType
	 * @param searchFor 节点搜索条件
	 * @param force 如果是true，则即使节点有关系线存在，也要删（自动先删关系线）；如果是false，则只删除符合条件节点中那些没有关系线的节点
	 * @return 删除的数量
	 * @throws Exception
	 */
	public int removeNodes(long ps_id, NodeType nodeType,
			Map<String, Object> searchFor, boolean force) throws Exception {
		
		JSONObject params = new JSONObject(searchFor);

		String catalogsCondition = "";
		if (searchFor.remove("catalogs") != null) {
			catalogsCondition = "and ({catalogs} in n.catalogs)";
		}
		JSONArray statements = new JSONArray();
		if (force) {
			// 删除符合条件并带有关系线的节点（关系线同时删除）
			statements.put(statement(params, "match (n:" + nodeType
					+ paramPlaceholders(searchFor)
					+ " )-[r]-(other) where true ", catalogsCondition,
					"delete r"));
		}
		// 删除孤立节点
		statements.put(statement(params, "match (n:" + nodeType
						+ paramPlaceholders(searchFor) + " ) where true ",
						catalogsCondition, "and not ( (n)-[]-() )", "delete n return count(n) as deletedCount"));
		
		JSONArray results = transaction(ps_id, statements).getJSONArray("results");
		
		int deletedCount = results.getJSONObject(results.length()-1)
							.getJSONArray("data")
							.getJSONObject(0)
							.getJSONArray("row")
							.getInt(0);
		
		
		return deletedCount;

	}
	/**
	 * 删除位置节点
	 * @param ps_id
	 * @param slc_ids
	 * @param slc_type
	 * @param force
	 * @return
	 * @throws Exception
	 */
	public int	 removeSlcNodes(long ps_id ,long[] slc_ids,long slc_type,boolean force) throws Exception{
		JSONObject params = new JSONObject();
		params.put("slc_type", slc_type);
		
		String slc_idstr=LongArraytoString(slc_ids,"[","]");
		JSONArray statements = new JSONArray();
		String catalogsCondition = " and  n.slc_id in "+slc_idstr+" and n.slc_type ={slc_type}";
		
		if(force){
			statements.put(statement(params, "match (n:" + NodeType.StorageLocationCatalog
					+ " )-[r]-(other) where true ",
					catalogsCondition,
					"delete r"));
		}
		statements.put(statement(params, "match (n:" + NodeType.StorageLocationCatalog
				 +" ) where true ",
				catalogsCondition, 
				"and not ( (n)-[]-() )", "delete n return count(n) as deletedCount"));
		JSONArray results = transaction(ps_id, statements).getJSONArray("results");
		int deletedCount = results.getJSONObject(results.length()-1)
				.getJSONArray("data")
				.getJSONObject(0)
				.getJSONArray("row")
				.getInt(0);


		return deletedCount;
		
	}
	/*
	 * 统计节点的某些字段的和
	 * 
	 * @param ps_id
	 * @param nodeType
	 * @param searchFor
	 * @param sumFields  变长参数，可以给多个字符串或给一个字符串数组
	 * @return 与sumFields参数对应名称的统计值（可能是大于0或等于0的整数）；如果返回null，说明图数据库中的数据可能不正常
	 * @throws Exception
	 */
	public Map<String,Integer> sumNodes(long ps_id, NodeType nodeType,Map<String, Object> searchFor,String... sumFields) throws Exception {
		JSONObject params = new JSONObject();
		for (Map.Entry<String, Object> e : searchFor.entrySet()) {
			params.put("search_" + e.getKey(), e.getValue());
		}
		String catalogsCondition = "";
		if (searchFor.remove("catalogs") != null) {
			catalogsCondition = "where {search_catalogs} in n.catalogs";
		}
		StringBuilder sumReturns = new StringBuilder();
		for(int i=0; i<sumFields.length; i++){
			if(i>0) sumReturns.append(", ");
			String sf = sumFields[i];
			sumReturns.append(String.format("sum(n.%s) as %s", sf, sf));
		}
		JSONArray data = query(ps_id,
						params,
						"match (n:"
								+ nodeType
								+ paramPlaceholders(searchFor, "{", "", ":",
										"search_", ",", "}") + " ) ",
						catalogsCondition,
						" return "+sumReturns
		);
		
		
		if(data.length() > 0 && data.getJSONArray(0).length() > 0) {
			data = data.getJSONArray(0);
			Map<String,Integer> result = new HashMap<String,Integer>();
			for(int i=0; i<sumFields.length; i++){
				result.put(sumFields[i], data.getInt(i));
			}
			return result;
		}
		else return null;
		
	}
	
	/**
	 * 统计指定位置上的容器的某些字段统计和
	 * 
	 * @param ps_id 
	 * @param searchForSlc 位置搜索条件
	 * @param searchForCon 位置上容器搜索条件
	 * @param sumFields 计和字段
	 * @return
	 * @throws Exception
	 */
	public Map<String,Integer>  sumContainers(long ps_id, Map<String, Object> searchForSlc, Map<String, Object> searchForCon, String... sumFields) throws Exception {
		//the first, put all criteria params into a map, but assign different prefixes accordingly
		JSONObject params = new JSONObject();
		for (Map.Entry<String, Object> e : searchForSlc.entrySet()) {
			params.put("slc_" + e.getKey(), e.getValue());
		}
		for (Map.Entry<String, Object> e : searchForCon.entrySet()) {
			params.put("con_" + e.getKey(), e.getValue());
		}
		//the second, make two-phased query's return patterns
		StringBuilder sumReturns = new StringBuilder();
		for(int i=0; i<sumFields.length; i++){
			if(i>0) {
				sumReturns.append(", ");
			}
			String sf = sumFields[i];
			sumReturns.append(String.format("sum(c.%s) as %s", sf, sf));
		}
		//the third, do the two-phased query, which is summing all grounding containers and summing all no-grounding containers
		JSONArray data = query(ps_id,params,
				
			"match (slc:StorageLocationCatalog"+paramPlaceholders(searchForSlc, "{", "", ":","slc_", ",", "}") + 
			")<-[:LOCATES]-(c:Container"+
			paramPlaceholders(searchForCon, "{", "", ":","con_", ",", "}") + ")", 
			"return "+sumReturns,
			"union",
			"match (slc:StorageLocationCatalog"+paramPlaceholders(searchForSlc, "{", "", ":","slc_", ",", "}") + 
			")<-[:LOCATES]-(c1:Container)-[:CONTAINS*]->(c:Container"+
			paramPlaceholders(searchForCon, "{", "", ":","con_", ",", "}") + ")",
			"return "+sumReturns
			
		);
		
		
		if(data.length() > 0) {
			
			Map<String,Integer> result = new HashMap<String,Integer>();
			for(int i=0; i<sumFields.length; i++){
				result.put(sumFields[i], 0);
			}
			
			for(int r=0; r<data.length(); r++){
				JSONArray row = data.getJSONArray(r);
				for(int i=0; i<sumFields.length; i++){
					result.put(sumFields[i], result.get(sumFields[i]) + row.getInt(i));
				}
			}
			return result;
		}
		else return null;
	}
	
	/**
	 * 指定仓库中所有物理库存大于0（实际有货）的货主ID
	 * @param ps_id
	 * @return 没有匹配时返回空List
	 * @throws Exception
	 */
	public List<Long> titlesHavePhysical(long ps_id) throws Exception {
		JSONArray data = query(ps_id,new JSONObject(),
				"match (c:Container)-[r:CONTAINS]->(p:Product)",
				"where r.quantity > 0",
				"return distinct p.title_id",
				"order by p.title_id");
		
		List<Long> result = new ArrayList<Long>();
		
		if(data.length() == 0) return result;
		
		for(int i=0; i< data.length(); i++){
			JSONArray titles = data.getJSONArray(i);
		
			for(int t=0; t<titles.length(); t++)
				result.add(titles.getLong(t));
		}
		return result;
	}

	
	/**
	 * 所有包含指定货主（title_id）的货物（物理值大于0）货位
	 * 
	 * @param ps_id
	 * @param title_id 货主ID，如果为0，表示不限制（返回所有有货的货位）
	 * @return 没有匹配时返回空数组，否则DBRow数组中每一行是一个货位记录（内含slc_area,slc_id等字段）
	 * @throws Exception
	 */
	public DBRow[] locationsContainsTitle(long ps_id, long title_id) throws Exception {
		JSONObject params = new JSONObject();
		String titleIdCondition = "";
		if(title_id != 0){
			params.put("title_id", title_id);
			titleIdCondition = "and n.title_id = {title_id}";
		}
		JSONArray data = query(ps_id, params,
				"match (slc:StorageLocationCatalog)<-[loc:LOCATES]-(c:Container)-[r:CONTAINS*]->(n)",
				"where (r[-1]).quantity > 0",
				titleIdCondition,
				"return distinct slc",
				"order by slc.slc_id");
		
		
		if(data.length() == 0) return new DBRow[0];
		
		DBRow[] result = new DBRow[data.length()];
		
		for(int i=0; i<result.length; i++){
			JSONObject row = data.getJSONArray(i).getJSONObject(0);
			result[i] = DBRowUtils.convertToDBRow(row.getJSONObject("data"));
		}
		
		return result;
		
	}
	
	/**
	 * 查询一个容器类型树
	 */
	public JSONObject searchContainerTree(long ps_id, Map<String,Object> searchForRoot) throws Exception {
		JSONArray data = query(ps_id, new JSONObject(searchForRoot),
								"match path=(c:Container ",
								paramPlaceholders(searchForRoot, "{", "", ":","", ",", "}"),
								")-[r:CONTAINS*1..]->(p:Product)",
								"with path",
								"return nodes(path),relationships(path)");
		if(data.length() == 0) return new JSONObject();
		
		return new Neo4jTreeBuilder(data).build();
		
	}
    
	public JSONArray searchContainer(long ps_id, Map<String,Object> search,String[] lot_numbers,long[] title_ids ) throws Exception {
		
		JSONObject param=new JSONObject(search);
		StringBuffer wherecond=new StringBuffer();
		Map<String,Object>	productProp =new HashMap<String, Object>();
		if(lot_numbers!=null){
			param.put("lot_number", lot_numbers);
			wherecond.append(" and  l.lot_number in {lot_number}");
		}
		if(title_ids!=null){
			param.put("title_id", title_ids);
			wherecond.append(" and  p.title_id in {title_id}");
		}
		JSONArray data = query(ps_id, param,
								"match  (slc: "+NodeType.StorageLocationCatalog+") <-[l:"+RelationType.LOCATES+"]-(root:"+NodeType.Container+")-[:CONTAINS*0..]->(c: "+NodeType.Container+" ",
								paramPlaceholders(search, "{", "", ":","", ",", "}"),
								")-[r:CONTAINS*0..]->(child:Container)-[:CONTAINS]->(p:"+ NodeType.Product+"",
								paramPlaceholders(productProp, "{", "", ":","", ",", "}"),
								")",
								" where true",
								wherecond.toString(),
								"return  distinct l ,c ,p ");
		
		JSONArray ret=new JSONArray();
		if(data.length() == 0) return ret ;
		for(int i=0;i<data.length();i++){
		 JSONArray path	=data.getJSONArray(i);
		 JSONObject l= path.getJSONObject(0).getJSONObject("data");
		 JSONObject c= path.getJSONObject(1).getJSONObject("data");
		 JSONObject p= path.getJSONObject(2).getJSONObject("data");
		 c.put("title_id", p.get("title_id"));
		 c.put("lot_number", l.get("lot_number"));
		 ret.put(c);
		}
			return  ret;
		
	}
	
	
	/**
	 *  刷新路由表
	 */
	public String refreshRouter() throws Exception {
		TextResource resp = client.text(restBaseUrl + "/ps_id2rest_port/refresh");
		return resp.toString();
	}
	
	public Set<Long> productLines(long ps_id, long[] title_ids) throws Exception {
		StringBuffer conds = new StringBuffer();
		JSONObject params = new JSONObject();
		if(title_ids != null) {
			params.put("title_id", title_ids);
			conds.append(" and p.title_id in {title_id}");
		}
		JSONArray data = query(ps_id, params,
				"match (c:Container)-[r:CONTAINS]->(p:Product)",
				"where r.quantity>0",
				conds.toString(),
				"with distinct(p) as p",
				"where p.product_line is not NULL",
				"return distinct(p.product_line)"
				);
		
		
		Set<Long>  result = new HashSet<Long>();
		for(int i=0; i<data.length(); i++){
			result.add(data.getJSONArray(i).getLong(0));
		}
		return result;
	}
	public Set<long[]> productCatalogs(long ps_id, long[] title_ids, long product_line) throws Exception {
		StringBuffer conds =new StringBuffer();
		JSONObject params = new JSONObject();
		if(title_ids != null) {
			params.put("title_id", title_ids);
			conds.append(" and p.title_id in {title_id}");
		}
		if(product_line != 0) {
			params.put("product_line", product_line);
			conds.append(" and p.product_line = {product_line}");
		}
		JSONArray data = query(ps_id, params,
				"match (c:Container)-[r:CONTAINS]->(p:Product)",
				"where r.quantity>0",
				conds.toString(),
				"with distinct(p) as p",
				"where p.catalogs is not NULL",
				"return distinct(p.catalogs)"
				);
		
		
		Set<long[]>  result = new HashSet<long[]>();
		for(int i=0; i<data.length(); i++){
			result.add(jsonArray2LongArray(data.getJSONArray(i).getJSONArray(0)));
		}
		return result;
	}
	private long[] jsonArray2LongArray(JSONArray ja) throws Exception {
		long[] la = new long[ja.length()];
		for(int i=0; i<ja.length(); i++){
			la[i] = ja.getLong(i);
		}
		return la;
	}
	
	public Set<String> modelNumbers(long ps_id, long[] title_ids, long product_line, long catalog) throws Exception {
		StringBuilder conds = new StringBuilder();
		JSONObject params = new JSONObject();
		if(title_ids != null) {
			params.put("title_id", title_ids);
			conds.append(" and p.title_id in {title_id}");
		}
		if(product_line != 0) {
			params.put("product_line", product_line);
			conds.append(" and p.product_line = {product_line}");
		}
		if(catalog != 0){
			params.put("catalog", catalog);
			conds.append(" and {catalog} in p.catalogs");
		}
		JSONArray data = query(ps_id, params,
				"match (c:Container)-[r:CONTAINS]->(p:Product)",
				"where r.quantity>0",
				conds.toString(),
				"with distinct(p) as p",
				"return distinct(p.model_number)"
				);
		
		
		Set<String>  result = new HashSet<String>();
		for(int i=0; i<data.length(); i++){
			result.add(data.getJSONArray(i).getString(0));
		}
		return result;
	}
	
	public Set<String> lotNumbers(long ps_id, long[] title_ids, long product_line, long catalog,String[] p_names, String[] model_number) throws Exception {
		StringBuilder conds = new StringBuilder();
		JSONObject params = new JSONObject();
		if(title_ids != null) {
			params.put("title_id", title_ids);
			conds.append(" and p.title_id in {title_id}");
		}
		if(product_line != 0) {
			params.put("product_line", product_line);
			conds.append(" and p.product_line = {product_line}");
		}
		if(catalog != 0){
			params.put("catalog", catalog);
			conds.append(" and {catalog} in p.catalogs");
		}
		if(p_names != null){
			params.put("p_names", p_names);
			conds.append(" and p.p_name in {p_names}");
		}
		if(model_number != null){
			params.put("model_number", model_number);
			conds.append(" and p.model_number in {model_number}");
		}
		JSONArray data = query(ps_id, params,
				"match (slc:StorageLocationCatalog)<-[l:LOCATES]-(c:Container)-[r:CONTAINS*]->(p:Product)",
				"where (r[-1]).quantity>0",
				conds.toString(),
				"with distinct(l) as l ",
				"return l.lot_number"
				);
		
		
		Set<String>  result = new HashSet<String>();
		for(int i=0; i<data.length(); i++){
			result.add(data.getJSONArray(i).getString(0));
		}
		return result;
	}
	
	public Set<Long> containerTypeIds(long ps_id, long[] title_ids, long product_line, long catalog, String[] p_names,String[] model_number, String[] lot_numbers) throws Exception {
		StringBuilder conds = new StringBuilder();
		JSONObject params = new JSONObject();
		if(title_ids != null) {
			params.put("title_id", title_ids);
			conds.append(" and p.title_id in {title_id}");
		}
		if(product_line != 0) {
			params.put("product_line", product_line);
			conds.append(" and p.product_line = {product_line}");
		}
		if(catalog != 0){
			params.put("catalog", catalog);
			conds.append(" and {catalog} in p.catalogs");
		}
		if(model_number != null) {
			params.put("model_number", model_number);
			conds.append(" and p.model_number in {model_number}");
		}
		if(p_names != null) {
			params.put("p_names", p_names);
			conds.append(" and p.p_name in {p_names}");
		}
		if(lot_numbers != null) {
			params.put("lot_number", lot_numbers);
			conds.append(" and l.lot_number in {lot_number}");
		}
		
		JSONArray data = query(ps_id, params,
				"match (slc:StorageLocationCatalog)<-[l:LOCATES]-(c:Container)-[r:CONTAINS*]->(p:Product)",
				"where (r[-1]).quantity>0",
				conds.toString(),
				"with distinct(c) as c",
				"return distinct(c.type_id)"
				);
		
		
		Set<Long>  result = new HashSet<Long>();
		for(int i=0; i<data.length(); i++){
			result.add(data.getJSONArray(i).getLong(0));
		}
		
		data = query(ps_id, params,
				"match (slc:StorageLocationCatalog)<-[l:LOCATES]-(t:Container)-[:CONTAINS*]->(c:Container)-[r:CONTAINS*]->(p:Product)",
				"where (r[-1]).quantity>0",
				conds.toString(),
				"with distinct(c) as c",
				"return distinct(c.type_id)"
				);
		for(int i=0; i<data.length(); i++){
			result.add(data.getJSONArray(i).getLong(0));
		}
		return result;
	}
	/**
	 * 查询所有有货的位置
	 * @param ps_id
	 * @param title_ids
	 * @param product_line
	 * @param catalog
	 * @param p_names
	 * @param model_number
	 * @param lot_numbers
	 * @param type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchLocations(long ps_id, long[] title_ids, long product_line, long catalog,String[] p_names, String[] model_number, String[] lot_numbers, long type_id) throws Exception {
		StringBuilder conds = new StringBuilder();
		JSONObject params = new JSONObject();
		if(title_ids != null) {
			params.put("title_id", title_ids);
			conds.append(" and p.title_id in {title_id}");
		}
		if(product_line != 0) {
			params.put("product_line", product_line);
			conds.append(" and p.product_line = {product_line}");
		}
		if(catalog != 0){
			params.put("catalog", catalog);
			conds.append(" and {catalog} in p.catalogs");
		}
		if(p_names != null) {
			params.put("p_names", p_names);
			conds.append(" and p.p_name in {p_names}");
		}
		if(model_number != null) {
			params.put("model_number", model_number);
			conds.append(" and p.model_number in {model_number}");
		}
		if(lot_numbers != null) {
			params.put("lot_number", lot_numbers);
			conds.append(" and l.lot_number in {lot_number}");
		}
		if(type_id != 0){
			params.put("type_id", type_id);
			conds.append(" and {type_id} in [x in r | startnode(x).type_id]");
		}
		JSONArray data = query(ps_id, params,
				"match (slc:StorageLocationCatalog)<-[l:LOCATES]-(c:Container)-[r:CONTAINS*]->(p:Product)",
				"where (r[-1]).quantity>0",
				conds.toString(),
				"return distinct(slc) as slc"
				);
		
		
		DBRow[]  result = new DBRow[data.length()];
		for(int i=0; i<data.length(); i++){
			result[i] = DBRowUtils.convertToDBRow(data.getJSONArray(i).getJSONObject(0).getJSONObject("data"));
		}
		
		
		return result;
	}
	/**
	 * 查询满足条件的所有type——id
	 * @param ps_id
	 * @param slc_id
	 * @param container_type
	 * @param title_ids
	 * @param product_line
	 * @param catalog
	 * @param p_names
	 * @param lot_numbers
	 * @return
	 * @throws Exception
	 */
	public Set<Long> conTypeIds(
			long ps_id,
			long slc_id, 
			int container_type,
			long[] title_ids,
			long product_line,
			long catalog,
			String[] p_names,
			String[] lot_numbers) throws Exception {
		StringBuilder conds = new StringBuilder();
		JSONObject params = new JSONObject();
		params.put("slc_id", slc_id);
		if(container_type != 0){
			params.put("container_type", container_type);
			conds.append(" and c.container_type = {container_type}");
		}
		if(title_ids != null){
			params.put("title_id", title_ids);
			conds.append(" and p.title_id in {title_id}");
		}
		if(product_line != 0){
			params.put("product_line", product_line);
			conds.append(" and p.product_line = {product_line}");
		}
		if(catalog != 0){
			params.put("catalog", catalog);
			conds.append(" and {catalog} in p.catalogs");
		}
		if(p_names != null) {
			params.put("p_name", p_names);
			conds.append(" and p.p_name in {p_name}");
		}
		if(lot_numbers != null) {
			params.put("lot_number", lot_numbers);
			conds.append(" and l.lot_number in {lot_number}");
		}
		JSONArray data = query(ps_id, params,
				"match (slc:StorageLocationCatalog)<-[l:LOCATES]-(c:Container)-[:CONTAINS*]->(p:Product)",
				"where slc.slc_id = {slc_id}",
				conds.toString(),
				"return distinct(c.type_id)");
		
		Set<Long>  result = new HashSet<Long>();
		for(int i=0; i<data.length(); i++){
			result.add(data.getJSONArray(i).getLong(0));
		}
		
		return result;
		
	}
	
	public void putAway(long ps_id,Map<String,Object> slc_props,Long... con_id)throws Exception{
		JSONObject params = new JSONObject();
		long slc_id =Long.parseLong(slc_props.get("slc_id").toString());
		long slc_type =Long.parseLong(slc_props.get("slc_type").toString());
		params.put("slc_id", slc_id);
		params.put("slc_type", slc_type);
		params.put("con_id", con_id);
		JSONObject query =new JSONObject();
		query.put("query",
				" match (s:StorageLocationCatalog{slc_id:{slc_id},slc_type:{slc_type}}) , "
				+" (slc:StorageLocationCatalog)<-[l:LOCATES]-(c:Container) "
				+" where c.con_id in {con_id} "
				+" with l ,c ,s "
				+" create Unique (s)<-[:LOCATES{lot_number:l.lot_number,time_number:l.time_number,pc_id:l.pc_id,title_id:l.title_id,put_time:timestamp()}]-(c) "
				+" delete l "
				+" return s,c ");
		query.put("params", params);
		JSONResource response = client.json(restBaseUrl + "/" + ps_id
				+ "/db/data/cypher", new Content("application/json", query
				.toString().getBytes("UTF-8")));

		if (response.object().has("exception"))
			throw new Exception(response.object().toString(4));
	/*	JSONArray data = response.object().getJSONArray("data");
		return DBRowUtils.jsonArrayAsDBRowArray(data.getJSONArray(0));*/
	};
	/**
	 * 
	 * @param ps_id 仓库
	 * @param title_ids title
	 * @param product_line
	 * @param catalog
	 * @param p_names
	 * @param lot_numbers
	 * @param container_type pallet类型
	 * @param type_id 容器包装形式
	 * @param union_flag 套装或者散件 -1表示全部
	 * @return
	 * @throws Exception
	 */
	
	public DBRow[]  queryHasInventLocations(
			long ps_id,
			long[] title_ids,
			long product_line,
			long catalog,
			String[] p_names,
			String[] lot_numbers,
			int container_type,
			long type_id,
			int union_flag
			)throws Exception{
		JSONObject params = new JSONObject();
		Map<String,Object> product_prop=new HashMap<String, Object>();
		Map<String,Object> con_prop=new HashMap<String, Object>();
		List<String> lotNumbers = Arrays.asList(lot_numbers == null ? new String[0] : lot_numbers);
		List<String> pNames = Arrays.asList(p_names == null ? new String[0] : p_names);
		
		String wherecond="";
		if(product_line>0l){
			product_prop.put("product_line", product_line);
			params.put("product_line", product_line);
		}
		if(union_flag>-1){
			product_prop.put("union_flag", union_flag);
			params.put("union_flag", union_flag);
		}
	
		if(container_type!=0){
			con_prop.put("container_type", container_type);
			params.put("container_type", container_type);
		}
		if(type_id>0l){
			con_prop.put("type_id", type_id);
			params.put("type_id", type_id);
		}
		if(catalog>0l){
			params.put("catalog", catalog);
			wherecond +=(" and {catalog} in p.catalogs");
		}
		if(!lotNumbers.isEmpty()){
			params.put("lot_number", lotNumbers);
			wherecond+=" and l.lot_number in {lot_number} ";
		}
		if(!pNames.isEmpty()){
			params.put("p_name", pNames);
			wherecond+=" and p.p_name in {p_name}";
		}
		if(title_ids!=null) {
			params.put("title_id", title_ids);
			wherecond +=" and p.title_id in {title_id}";
		}
		JSONArray data =query(ps_id,params,
				"match",
				"(slc:StorageLocationCatalog)<-[l:LOCATES]-",
				"(c:Container"+paramPlaceholders(con_prop)+")-[:CONTAINS*]->",
				"(p:Product"+paramPlaceholders(product_prop)+ ")",
				"where true",
				wherecond,
				"return  distinct slc"		
				);
		//data.getJSONArray(0).getJSONObject(0).get("data")
		JSONArray result = new JSONArray();
		for(int i=0;i<data.length();i++){
			JSONArray	child =data.getJSONArray(i);
			result.put(child.getJSONObject(0).get("data"));
		}
		return DBRowUtils.jsonArrayAsDBRowArray(result);
	}
	/**
	 * 
	 * @param ps_id
	 * @param title_ids
	 * @param catalog
	 * @param p_names
	 * @param model_number
	 * @return
	 * @throws Exception
	 */
	public Set<Long> queryProductLine(long ps_id,long[] title_ids,long catalog, String[] p_names,String[] model_number)throws Exception{
		JSONObject params = new JSONObject();
		StringBuffer conds = new StringBuffer();
		if(title_ids != null) {
			params.put("title_id", title_ids);
			conds.append(" and p.title_id in {title_id}");
		}
		if(catalog != 0){
			params.put("catalog", catalog);
			conds.append(" and {catalog} in p.catalogs");
		}
		if(p_names != null){
			params.put("p_names", model_number);
			conds.append(" and p.p_name in {p_names}");
		}
		if(model_number != null){
			params.put("model_number", model_number);
			conds.append(" and p.model_number in {model_number}");
		}
		JSONArray data =query(ps_id,params,
				"match (p:Product) ",
				"where true",
				conds.toString(),
				" return distinct  p.product_line"
				);
		Set<Long>  result = new HashSet<Long>();
		for(int i=0;i<data.length();i++){
			JSONArray	child =data.getJSONArray(i);
		if(!child.get(0).toString().equalsIgnoreCase("null")&&child.get(0)!=null)	result.add(Long.valueOf(child.get(0).toString()));
		}
		return result;
	}
	
	
}
