package com.cwc.app.floor.api.sbb;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorReceiptMgrSbb{
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	/**
	 * 添加预计收货主单据
	 * @param 
	 * @return 
	 * @author subin
	 * */
	public long insertExpectedReceipt(DBRow param) throws Exception{
		
		try {
			
			return dbUtilAutoTran.insertReturnId("receipts", param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptMgrSbb.insertExpectedReceipt(DBRow param) error:" + e);
		}
	}
	
	/**
	 * 添加预计收货明细
	 * @param 
	 * @return 
	 * @author subin
	 * */
	public void insertExpectedReceiptLines(DBRow param) throws Exception{
		
		try {
			
			dbUtilAutoTran.insert("receipt_lines", param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptMgrSbb.insertExpectedReceiptLines(DBRow param) error:" + e);
		}
	}
	
	/**
	 * 查询预计收货主单据
	 * @param 
	 * @return 
	 * @author subin
	 * */
	public DBRow getExpectedReceipt(DBRow param) throws Exception{
		
		try {
			
			String sql = "select * from receipts where company_id = ? and receipt_no = ?";
			
			return dbUtilAutoTran.selectPreSingle(sql, param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptMgrSbb.getExpectedReceipt(DBRow param) error:" + e);
		}
	}
	
	/**
	 * 查询预计收货明细
	 * @param 
	 * @return 
	 * @author subin
	 * */
	public DBRow[] getExpectedReceiptLines(DBRow param) throws Exception{
		
		try {
			
			String sql = "select * from receiptlines where company_id = ? and receipt_no = ?";
			
			return dbUtilAutoTran.selectPreMutliple(sql, param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptMgrSbb.getExpectedReceiptLines(DBRow param) error:" + e);
		}
	}
	
	/**
	 * 查询托盘相关信息
	 * @param [companyId,receiptNo]
	 * @return 
	 * @author subin
	 * */
	public DBRow[] getReceiptLinePlates(DBRow param) throws Exception{
		
		try {
			
			StringBuffer sql = new StringBuffer();
			sql.append(" select con.con_id,rl.company_id,rl.receipt_no,rl.line_no,rl.lot_no,con.container as plate_no,quant.cp_quantity,rept.supplier_id,con.location,rept.customer_id,rl.item_id,con.create_date,ad.employe_name account,rl.is_check_sn,admi.employe_name assign_user_id,rl.unloading_finish_time qty_date,admin.employe_name qty_user");
			sql.append(" from ( select cp_lp_id,sum(cp_quantity) as cp_quantity,max(update_date) qty_date ,max(update_user) qty_user from container_product group by cp_lp_id ) as quant ");
			sql.append(" LEFT JOIN container con on quant.cp_lp_id = con.con_id AND con.is_delete = 0 ");
			sql.append(" LEFT JOIN receipt_rel_container as rrc on con.con_id = rrc.plate_no ");
			sql.append(" LEFT JOIN receipt_lines as rl on rrc.receipt_line_id = rl.receipt_line_id  ");
			sql.append(" LEFT JOIN receipts as rept on rl.receipt_id = rept.receipt_id ");
			sql.append(" left join admin as admi on rl.counted_user = admi.adid ");
			sql.append(" left join admin as ad on con.create_user = ad.adid ");
			sql.append(" left join admin as adm on quant.qty_user = adm.adid ");
			sql.append(" left join admin as admin on rl.counted_user = admin.adid ");
			sql.append(" where quant.cp_quantity > 0 and rl.company_id = ? and rl.receipt_no = ? " );
			
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptMgrSbb.getReceiptLinePlates(DBRow param) error:" + e);
		}
	}
	
	public DBRow[] getReceiptLinePlatesCount(DBRow param) throws Exception{
		
		try {
			
			StringBuffer sql = new StringBuffer();
			sql.append(" select rl.company_id,rl.receipt_no,rl.line_no ,count(*) pallets ");
			sql.append(" from ( select cp_lp_id,sum(cp_quantity) as cp_quantity,max(update_date) qty_date ,max(update_user) qty_user from container_product group by cp_lp_id ) as quant ");
			sql.append(" LEFT JOIN container con on quant.cp_lp_id = con.con_id AND con.is_delete = 0 ");
			sql.append(" LEFT JOIN receipt_rel_container as rrc on con.con_id = rrc.plate_no ");
			sql.append(" LEFT JOIN receipt_lines as rl on rrc.receipt_line_id = rl.receipt_line_id  ");
			sql.append(" LEFT JOIN receipts as rept on rl.receipt_id = rept.receipt_id ");
			sql.append(" left join admin as admi on rl.counted_user = admi.adid ");
			sql.append(" left join admin as ad on con.create_user = ad.adid ");
			sql.append(" left join admin as adm on quant.qty_user = adm.adid ");
			sql.append(" left join admin as admin on rl.counted_user = admin.adid ");
			sql.append(" where quant.cp_quantity > 0 and rl.company_id = ? and rl.receipt_no = ? " );
			sql.append(" GROUP BY rl.company_id,rl.receipt_no,rl.line_no " );
			
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptMgrSbb.getReceiptLinePlates(DBRow param) error:" + e);
		}
	}
	
	/**
	 * 查询托盘与SN相关信息
	 * @param [companyId,receiptNo]
	 * @return 
	 * @author subin
	 * */
	public DBRow[] getReceiptLinePlatesSn(DBRow param) throws Exception{
		
		try {
			
			StringBuffer sql = new StringBuffer();
			sql.append(" select sp.serial_number,ctr.container plate_no,rl.company_id,sp.update_date,ad.employe_name account,rl.is_check_sn from serial_product as sp ");
			sql.append(" left join container ctr on sp.at_lp_id = ctr.con_id AND ctr.is_delete = 0 ");
			sql.append(" left join receipt_rel_container rrc on ctr.con_id = rrc.plate_no ");
			sql.append(" left join receipt_lines rl on rrc.receipt_line_id = rl.receipt_line_id ");
			sql.append(" left join admin ad on sp.update_user = ad.adid  ");
			sql.append(" where rl.company_id = ? and rl.receipt_no = ? ");
			
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptMgrSbb.getReceiptLinePlatesSn(DBRow param) error:" + e);
		}
	}
	
	public DBRow[] getProductByPname(String pName) throws Exception {
		
		try {
			
			String sql = " select * from product where p_name = '"+pName+"'";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptMgrSbb.getProductByPname(String pName) error:" + e);
		}
	}
	
	public DBRow[] getSerialProductByPlateNo(String plantNo) throws Exception {
		
		try {
			
			String sql = " select * from serial_product where at_lp_id =  '"+plantNo+"'";
			
			return dbUtilAutoTran.selectMutliple(sql); 
			
		}catch (Exception e){
			throw new Exception("FloorReceiptMgrSbb.getSerialProductByPlateNo(String plantNo) error:" + e);
		}
	}
	
	/**
	 * UNTEST 
	 * 记录日志
	 * @param DBRow[]
	 * @return 日志id
	 * @author subin
	 * */
	public long insertRecordLog(DBRow param) throws Exception{
		
		try {
			
			return dbUtilAutoTran.insertReturnId("check_in_log",param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptMgrSbb.insertRecordLog(DBRow param) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param param 要插入的shuju
	 * @param table 表名
	 * @return
	 * @throws Exception
	 * @author zhaoyy
	 */
	public long insertRecordLog(DBRow param,String table) throws Exception{
		
		try {
			
			return dbUtilAutoTran.insertReturnId(table,param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptMgrSbb.insertRecordLog(DBRow param) error:" + e);
		}
	}
	/**
	 * UNTEST 
	 * 根据WMS托盘号更新Container
	 * @param DBRow[container]更新项
	 * @return null
	 * @author subin
	 * */
	public void updateContainer(DBRow param) throws Exception{
		
		try {
			
			String sql = "where container = '"+ param.get("container", 0)+"'";
			param.remove("container");
			
			dbUtilAutoTran.update(sql, "container", param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptMgrSbb.updateContainer(DBRow param) error:" + e);
		}
	}
	
	/**
	 * UNTEST 
	 * 查询主单据的一些上下文信息
	 * @param DBRow
	 * @return 主单据的一些上下文信息
	 * @author subin
	 * */
	public DBRow getReceiptContext(DBRow param) throws Exception {
		
		try {
			
			StringBuffer sql = new StringBuffer();
			sql.append(" select max(rrc.detail_id) as detail_id,dm.company_name,ee.equipment_number,ee.seal_delivery from container con ");
			sql.append(" LEFT JOIN receipt_rel_container as rrc on con.con_id = rrc.plate_no ");
			sql.append(" LEFT JOIN door_or_location_occupancy_details as dd on rrc.detail_id = dd.dlo_detail_id ");
			sql.append(" LEFT JOIN entry_equipment as ee on dd.equipment_id = ee.equipment_id ");
			sql.append(" LEFT JOIN door_or_location_occupancy_main as dm on dd.dlo_id = dm.dlo_id ");
			sql.append(" LEFT JOIN receipt_lines as rl on rrc.receipt_line_id = rl.receipt_line_id  ");
			sql.append(" LEFT JOIN receipts as rept on rl.receipt_id = rept.receipt_id ");
			sql.append(" where rl.company_id = ? and rl.receipt_no = ? ");
			
			return dbUtilAutoTran.selectPreSingle(sql.toString(), param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptMgrSbb.getReceiptContext(DBRow param) error:" + e);
		}
	}
}