package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorProductStorageHistoryMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran; 
	
	/**
	 * 添加库存历史
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addProductStorageHistory(DBRow dbrow)
		throws Exception
	{
		return (dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_storage_history"),dbrow));
	}
	/**
	 * 查询库存历史
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	
	public DBRow[] getAllProductStorageHistory(PageCtrl pc)
	throws Exception
	{
		
		try 
		{
			String sql="select psh.*,p.p_name,psc.title from "+ConfigBean.getStringValue("product_storage_history")+" as psh"+
			        " join "+ConfigBean.getStringValue("product")+" as p on psh.pc_id=p.pc_id " +
					" join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on psh.ps_id=psc.id";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductStorageHistoryMgrZJ getAllProductStorageHistory error:"+e);
		}	
	}

	public DBRow[] getProductStorageHistoryByPsid(String st,String en,long pc_id,long psId, PageCtrl pc)
	throws Exception
	{
		try {
			String cond = "";
			if(psId>0){
				cond += " and psh.ps_id ="+psId+" ";
			}
			
			if(pc_id>0)
			{
				cond+= " and p.pc_id = "+pc_id+" ";
			}
			
			String sql="select psh.*,p.p_name,psc.title from "+ConfigBean.getStringValue("product_storage_history")+" as psh"+
			" join "+ConfigBean.getStringValue("product")+" as p on psh.pc_id=p.pc_id " +
			" join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on psh.ps_id=psc.id " +
			"where psh.post_date >= str_to_date('"+st+"','%Y-%m-%d') and psh.post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') "
			+cond+" order by psh.psh_id desc";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (Exception e) {
			throw new Exception("FloorProductStorageHistoryMgrZJ getProductStorageHistoryByPsid error:"+e);
		}
	}
	
	public DBRow[] exportProductStorageHistory(long productLine,long psId,String store_date)
	throws Exception
	{
		try {
			String whereProductLine="";
			if(productLine > 0){
				whereProductLine=" and pc.product_line_id="+productLine;
			}
			String sql="select psh.*,p.p_name,pld.name from "+ConfigBean.getStringValue("product_storage_history")+" as psh"+
			" join "+ConfigBean.getStringValue("product")+" as p on psh.pc_id=p.pc_id " +
			" join "+ConfigBean.getStringValue("product_catalog")+" as pc on p.catalog_id=pc.id"+whereProductLine+
			" join "+ConfigBean.getStringValue("product_line_define")+" as pld on pld.id=pc.product_line_id"+
			" where psh.ps_id="+psId+" and psh.post_date  between   str_to_date('"+store_date+"','%Y-%m-%d') and str_to_date('"+store_date+" 23:59:59','%Y-%m-%d %H:%i:%s')";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorProductStorageHistoryMgrZJ exportProductStorageHistory error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
