package com.cwc.app.floor.api.zr;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.quartz.CronExpression;

import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.ScheduleFinishKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.ScheduleTime;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
 

public class FloorScheduleMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public DBRow[] getScheduleSub(long schedule_id) throws Exception {
		try{
 			return dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("schedule_sub") + " where schedule_id="+schedule_id);		
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.getScheduleSub(schedule_id):"+e);
		}
	}
	public void updateScheduleSubByScheduleId(long schedule_id , DBRow row)throws Exception {
		
		try{
			dbUtilAutoTran.update(" where schedule_id="+schedule_id, ConfigBean.getStringValue("schedule_sub"), row);
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.updateScheduleSubByScheduleId(row):"+e);
		}
	}
	// 根据id 修改在添加回复的时候子任务的进度情况
	public void updateScheduleSub(long id , DBRow row) throws Exception{
		try{
			 
			dbUtilAutoTran.update("where schedule_sub_id="+id, ConfigBean.getStringValue("schedule_sub"), row);		
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.updateSchedule(row):"+e);
		}
	}
	//现在是要把任务的执行人保存在另外的一张表中的。
	//把任务参与人保存在另外的一张表。
	//应该是先插入Schedule 然后在插入其他的子任务或者是参与人信息。
	//如果是repeat任务还需要做另外的操作在另外的一张表schedule_repeat中插入一条信息
	
	
	public long addSchedule(DBRow row) throws Exception{
		try{
			row.add("schedule_state",0);
			String[] execute_user_ids = row.getString("execute_user_id").split(",");
			row.remove("execute_user_id");
			String schedule_join_execute_id = row.getString("schedule_join_execute_id");
			row.remove("schedule_join_execute_id");
			
			String corn_express = row.getString("corn_express");
			if(null != corn_express && corn_express.length() > 0){
				// 在schedule_repeat表中插入一条记录。在整个插入操作执行完后就在页面用异步的方式去生成repeat任务
				DBRow repeatRow = new DBRow();
 				repeatRow.add("repeat_express",corn_express);
				repeatRow.add("repeat_start_time", row.getString("repeat_start_time"));
				String repeat_end_time = row.getString("repeat_end_time");
				if(null != repeat_end_time && repeat_end_time.length() > 0){
					repeatRow.add("repeat_end_time",repeat_end_time);
					row.remove("repeat_end_time");
				}else{
					String repeat_times = row.getString("repeat_times");
					if( null != repeat_times && repeat_times.length() > 0){
						repeatRow.add("repeat_times",repeat_times);
						row.remove("repeat_times");
					}else{
						repeatRow.add("repeat_never_flag", "1");
					}
				} 
				// 移除row 中关于repeat的信息
				row.remove("corn_express");
				row.remove("repeat_start_time");
				long repeate_schedule_id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("schedule_repeat"), repeatRow);
				row.add("is_schedule", "3");
				row.add("repeat_id", repeate_schedule_id);
			}
			
			
			long id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("schedule"), row);
			
			if(null != execute_user_ids && execute_user_ids.length > 0){
				for(int index = 0 , count = execute_user_ids.length; index < count ; index++){
 					DBRow scheduleSubRow = new DBRow();
					scheduleSubRow.add("schedule_id", id);
					scheduleSubRow.add("schedule_execute_id", execute_user_ids[index]);
 					scheduleSubRow.add("schedule_state", "0");
					//dbUtilAutoTran.insert(ConfigBean.getStringValue("schedule_sub"), scheduleSubRow);
					dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("schedule_sub"), scheduleSubRow);
				}
			}
			
			if(null != schedule_join_execute_id && schedule_join_execute_id.length() > 0){
				String[] scheduleJoinUserIdArray = schedule_join_execute_id.split(",");
				
				for(int index = 0 , count = scheduleJoinUserIdArray.length ; index < count  ; index++ ){
 					DBRow scheduleJoin = new DBRow();
 					scheduleJoin.add("schedule_id", id);
					scheduleJoin.add("schedule_join_execute_id", scheduleJoinUserIdArray[index]);
					dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("schedule_join"), scheduleJoin);
				}
			} 
			return id;
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.addSchedule(row):"+e);
		}
	}
 
	public DBRow[] getScheduleByMonthAndExecuteUserId(DBRow row, boolean isAjax) throws Exception{
		// 根据传入的时间去进行查询这个人的当前的任务 时间是根据传入的时间。
		try{	
			
		 	String start = row.getString("start");
			String end =  row.getString("end");
			String executeUserId = row.getString("execute_user_id");
			String assign_user_id = row.getString("assign_user_id");
			String executedeptno = row.getString("executedeptno");
			String assigndeptno = row.getString("assigndeptno");
			String state = row.getString("state");
			//select  schedule.* ,  assAdmin.employe_name as assEmployName,  exeAdmin.employe_name as exeEmployName   from schedule , admin as assAdmin , admin  as exeAdmin where schedule.assign_user_id = assAdmin.adid and schedule.execute_user_id = exeAdmin.adid and schedule.execute_user_id = '100002';
			StringBuffer sql = new StringBuffer();
			sql.append("select * from `schedule_sub` left join `schedule` on `schedule`.schedule_id = schedule_sub.schedule_id where `schedule`.is_schedule = '1' ");	 
			if(null != start && start.length() > 0 && null != end && end.length() > 0){
				if(!isAjax){
					 sql.append(" and (((`schedule`.start_time >= '"+start+"' and  `schedule`.start_time < '"+end+"') or (`schedule`.end_time >='"+start+"' and `schedule`.end_time < '"+end+"')) or `schedule`.start_time <= '"+start+"' and `schedule`.end_time >= '"+end+"' )");
				}else{
					if(row.getString("direction").equals("next")){
						sql.append(" and (`schedule`.start_time >'"+start+"' and `schedule`.start_time  <'"+end+"' )");
					}else{
						sql.append(" and (`schedule`.end_time >'"+start+"' and `schedule`.end_time  <'"+end+"' )");
					}
					
				}
			}
			 
			
			if(null != assign_user_id && assign_user_id.length() > 0 ){
				sql.append(" and schedule.assign_user_id =").append(Long.parseLong(assign_user_id));
			}else{
				//assigndeptno
				if(null != assigndeptno && assigndeptno.length() > 0){
					sql.append(" and schedule.assign_user_id in ").append(" (select admin.adid from admin_group , admin where admin_group.adgid = admin.adgid and admin_group.adgid = "+assigndeptno+")");
				}
			}
			sql.append(" and schedule_sub.schedule_execute_id ");
			if(executeUserId != null && executeUserId.length() > 0){
				sql.append("="+Long.parseLong(executeUserId));
			}else{
				if(null != executedeptno && executedeptno.length() > 0 ){
					sql.append(" in "+ " (select admin.adid from admin_group , admin where admin_group.adgid = admin.adgid and admin_group.adgid = "+executedeptno+")" );
				}
			}
			if(null != state){
				if("1".equals(state)){
					//完成
					sql.append("  and schedule_sub.schedule_state = "+ScheduleFinishKey.ScheduleFinish);
				}
				if("-1".equals(state)){
					//未完成
					sql.append("  and schedule_sub.schedule_state < "+ScheduleFinishKey.ScheduleFinish);
				}
			}
			sql.append("  group by `schedule`.schedule_id");
		  // System.out.println("sql : " + sql.toString());
			return dbUtilAutoTran.selectMutliple(sql.toString());
		 }catch(Exception e){
			 throw new Exception("FloorScheduleMgrZr.getScheduleByMonthAndExecuteUserId(row):"+e);
		 }
	}
	//like detail 语句
	public DBRow[] getScheduleByExecuteUserIdAndDetail(DBRow row) throws Exception{
		 try{	
			 String executeUserId = row.getString("executeUserId");
			 String schedule_detail = row.getString("schedule_detail").trim();
			 
			 StringBuffer sql = new StringBuffer();
				sql.append("SELECT t.* , a_admin.employe_name  as 'assEmployName',e_admin.employe_name AS 'exeEmployName' from ")
						.append("(select schedule_sub.schedule_sub_id ,schedule_sub.schedule_execute_id , schedule_sub.schedule_state,")
						.append(" schedule.assign_user_id , schedule.schedule_id, schedule.end_time , schedule.start_time ,schedule.schedule_is_note , schedule.is_all_day , ")
						.append(" schedule.is_need_replay , schedule.is_update , schedule.is_schedule ,schedule.schedule_overview ")
						.append(" from schedule_sub RIGHT  JOIN schedule on schedule.schedule_id = schedule_sub.schedule_id and schedule.is_schedule = '1'")
						.append(" and  schedule.schedule_overview like  '%").append(schedule_detail).append("%'");
				sql.append(" and schedule_sub.schedule_execute_id =").append(Long.parseLong(executeUserId)).append(") t, admin AS e_admin , admin AS a_admin where e_admin.adid = t.schedule_execute_id AND a_admin.adid = t.assign_user_id ");
			 
			 return dbUtilAutoTran.selectMutliple(sql.toString());
		 }catch(Exception e){
			 throw new Exception("FloorScheduleMgrZr.getScheduleByExecuteUserIdAndDetail(row):"+e);
		 }
	}
	//带分页的task查询
	public DBRow[] getScheduleByExecuteUserIdAndDetailAndPc(DBRow row,PageCtrl pc) throws Exception{
		try{	
			/*String executeUserId = row.getString("executeUserId");
			String schedule_detail = row.getString("schedule_detail").trim();
			
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT t.* , a_admin.employe_name  as 'assEmployName',e_admin.employe_name AS 'exeEmployName' from ")
			.append("(select schedule_sub.schedule_sub_id, schedule_sub.schedule_execute_id , schedule_sub.schedule_state,")
			.append(" schedule.assign_user_id, schedule.create_time, schedule.schedule_detail, schedule.schedule_id, schedule.end_time , schedule.start_time ,schedule.schedule_is_note , schedule.is_all_day , ")
			.append(" schedule.is_need_replay , schedule.is_update , schedule.is_schedule ,schedule.schedule_overview ")
			.append(" from schedule_sub RIGHT  JOIN schedule on schedule.schedule_id = schedule_sub.schedule_id and schedule.is_schedule = '1'")
			.append(" and  schedule.schedule_overview like  '%").append(schedule_detail).append("%'");
			sql.append(" and schedule_sub.schedule_execute_id =").append(Long.parseLong(executeUserId)).append(") t, admin AS e_admin , admin AS a_admin where e_admin.adid = t.schedule_execute_id AND a_admin.adid = t.assign_user_id order by t.create_time desc");
			
			return dbUtilAutoTran.selectMutliple(sql.toString(),pc);*/
			String adid=row.getString("executeUserId");
		    DBRow[] data=null;
		    if(!StrUtil.isBlank(adid)){
		    	StringBuffer sql=new StringBuffer("SELECT task.* FROM getAllTaskByAdid task WHERE task.schedule_id IN");
		    	sql.append(" (SELECT distinct `schedule`.schedule_id FROM `schedule` LEFT JOIN schedule_sub ON `schedule`.schedule_id = schedule_sub.schedule_id")
		    	   .append(" LEFT JOIN schedule_join ON `schedule`.schedule_id = schedule_join.schedule_id ")
		    	   .append(" WHERE `schedule`.assign_user_id = '").append(adid).append("' or schedule_sub.schedule_execute_id='").append(adid)
		    	   .append("' or `schedule_join`.schedule_join_execute_id='").append(adid).append("') AND task.is_main_schedule=1 order by task.create_time desc");
//		    	System.out.println(sql.toString());
		    	data=dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		    }
			return data;

		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.getScheduleByExecuteUserIdAndDetailAndPc(row):"+e);
		}
	}
	/**
	 * @param row
	 * @throws Exception(根据row传入的信息修改Schedule信息,ScheduleSub信息,ScheduleJoin信息)
	 */
	public DBRow updateSchedule(long id,DBRow row) throws Exception {
		DBRow resultRow = new DBRow();
		try{
			 // 查询如果是 deleteusers 不为空，addusers
			String deleteusers = row.getString("deleteusers");
			String addusers = row.getString("addusers");
			row.remove("deleteusers");
			row.remove("addusers");
			row.remove("notchange");
			resultRow.add("deleteusers", deleteusers);
			resultRow.add("addusers", addusers);
			boolean isChangeflag = false;
			
			if( null != deleteusers && deleteusers.length() > 0){
				// 删除子任务
				isChangeflag = true;
				String[] deleteArray =  deleteusers.split(",");
				for(String deleteId : deleteArray){
					dbUtilAutoTran.delete("where schedule_execute_id = " + deleteId + " and schedule_id="+id,  ConfigBean.getStringValue("schedule_sub"));
				}
		 
			}
			if(null != addusers && addusers.length() > 0){
				// 添加子任务
				isChangeflag = true;
				String[] addUserArray =  addusers.split(",");
				for(String addUserId : addUserArray ){
					DBRow scheduleSubRow  = new DBRow();
 					scheduleSubRow.add("schedule_id", id);
					scheduleSubRow.add("schedule_execute_id", addUserId);
 					scheduleSubRow.add("schedule_state", "0");
					dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("schedule_sub"), scheduleSubRow);
				}
				
			}
			if(isChangeflag){
				//任务进度的重新计算,把结果保存
				String sql = "select * from  schedule_sub where schedule_sub.schedule_id = " + id;
				DBRow[] rows = dbUtilAutoTran.selectMutliple(sql);
				int sum = 0;
				for(DBRow r : rows){
					String value = r.getString("schedule_state");
					sum += Integer.parseInt((value != null && value.length() > 0) ? value :"0");
				}
				double d     =  sum /(double) rows.length ;
				BigDecimal b  =  new BigDecimal(d);    
				double  result =  b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue(); 
			    row.add("schedule_state", result);
			}
			String add_join_user_id = row.getString("add_join_user_id");
			String del_join_user_id = row.getString("del_join_user_id");
			row.remove("add_join_user_id");
			row.remove("del_join_user_id");
			//首先是删除,然后在添加新的任务参与人
			
			if(del_join_user_id != null && del_join_user_id.length() > 0 ){
 				dbUtilAutoTran.delete("where schedule_join.schedule_join_execute_id in ("+del_join_user_id+") and schedule_join.schedule_id = " + id, ConfigBean.getStringValue("schedule_join"));
			}
			if(add_join_user_id != null && add_join_user_id.length() > 0){
				String[] scheduleAddUser = add_join_user_id.split(",");
				if( scheduleAddUser.length > 0 ){
					for(int index = 0 , count = scheduleAddUser.length ; index < count ; index++ ){
						DBRow insertRow = new DBRow();
					 
						insertRow.add("schedule_id",id);
						insertRow.add("schedule_join_execute_id", scheduleAddUser[index]);
						dbUtilAutoTran.insertReturnId( ConfigBean.getStringValue("schedule_join"), insertRow);
					}
				}
				
			}
			
			resultRow.add("del_join_user_id",del_join_user_id);
			resultRow.add("add_join_user_id",add_join_user_id);
			dbUtilAutoTran.update("where schedule_id="+id, ConfigBean.getStringValue("schedule"), row);	
			return resultRow ;
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.updateSchedule(row):"+e);
		}
	}
	//值考虑是修改当前任务以后的所有的任务(值包含哪些没有跟进信息的)
	public void updateRepeatSchedule(long id , DBRow row) throws Exception{

	}
	
	
	
	/**
	 * @param row
	 * @return
	 * @throws Exception 查询当前登录人还没有安排的任务（自己给自己安排的任务 或者是别人给自己安排的。）
	 */
	public DBRow[] getUnAssignSchedule(DBRow row) throws Exception{
		try{
			String endTime = row.getString("endtime");
			String startTime  = row.getString("starttime");
			String execute_user_id = row.getString("execute_user_id");
			if(null != execute_user_id ){
				StringBuffer sql = new StringBuffer();
				sql.append("select * from `schedule_sub` , `schedule` where `schedule`.is_schedule = '0' and `schedule`.schedule_id = `schedule_sub`.schedule_id and `schedule_sub`.schedule_execute_id = "+ execute_user_id);	 
				//System.out.println(sql.toString());
				return dbUtilAutoTran.selectMutliple(sql.toString());	
			}else {
				return  null;
			} 
				
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.getUnAssignSchedule(row):"+e);
		}
	}
	
	public DBRow getScheduleById(long id) throws Exception {
		try {
			if(0l != id){
				String sql =  "select * from " + ConfigBean.getStringValue("schedule") + " where schedule_id="+id;
				return dbUtilAutoTran.selectSingle(sql);
			}else{
				return null;
			}
		} catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getScheduleById(id):"+e);
		}
	}
	
	public DBRow[] getAllUserInfoAndDept() throws Exception {
		try{
			return dbUtilAutoTran.selectMutliple(" select  admin.adid,admin.employe_name , admin_group.adgid , admin_group.name from admin , admin_group where admin.adgid = admin_group.adgid ");
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getAllUserInfoAndDept(id):"+e);
		}
	}
	public DBRow[] getScheduleAndUserInfoById(long id) throws Exception {
		try {
			if(0l != id){
				//String sql = "select  schedule.* , assign.employe_name  as assign_name,   execute.employe_name  as execute_name from  schedule , admin   assign, admin  execute where  schedule.assign_user_id = assign.adid and schedule.execute_user_id = execute.adid  and schedule.schedule_id = "+id;
				//String sql =  "select * from " + ConfigBean.getStringValue("schedule") + " where schedule_id="+id;
				StringBuffer sql = new StringBuffer( "SELECT  t.* ,assAdmin.employe_name as assign_name, exeAdmin.employe_name AS execute_name from ");
				sql.append(" (select schedule.* , schedule_sub.schedule_sub_id, schedule_sub.schedule_execute_id ,schedule_sub.schedule_state AS sub_state from schedule_sub LEFT JOIN `schedule` ON   schedule_sub.schedule_id = `schedule`.schedule_id where schedule_sub.schedule_id = "+id+" ) t ,");
				sql.append(" admin AS assAdmin , admin as exeAdmin");
				sql.append(" where assAdmin.adid = t.assign_user_id and exeAdmin.adid = t.schedule_execute_id");
				//System.out.println(".............. : " + sql.toString());
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}else{
				return null;
			}
		} catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getScheduleById(id):"+e);
		}
	}
	//删除任务的时候把子任务也删除
	public void deleteScheduleById(long id) throws Exception{
		try{
			// 改成是调用一个存储过程来删除
			String v = "call delete_schedule("+id+")";
 
			dbUtilAutoTran.CallStringSingleResultProcedure(v);	
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.deleteScheduleById(id):"+e);
		}
	}
	 
	//根据
	public DBRow[] getMangerByAdgid(long adgid) throws Exception{
		try {
			if(0l != adgid){
				 String sql  = "select admin.adid from admin_group , admin where 1=1 and admin.adgid = admin_group.adgid and IFNULL(admin.proJsId,0) >= 5 and admin_group.adgid = " +adgid;
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}else{
				return null;
			}
		} catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getMangerByAdgid(adgid):"+e);
		}
	}
	// 得到总经理下的主管或者是副主管
	public DBRow[] getManagerInfo() throws Exception{
		 try{
			 String sql = "SELECT admin.adid ,admin.adgid , admin.account , admin.employe_name from admin where   admin.proJsId >= 5 and adgid = 10000 ";
			 return dbUtilAutoTran.selectMutliple(sql);
		 }catch (Exception e) {
			 throw new Exception("FloorScheduleMgrZr.getManagerInfo():"+e);
		}
	}
	// 得到任务的参与人Id
	public DBRow[] getScheduleJoinInfo(long schedule_id) throws Exception{
		 try{
			 String sql = "SELECT schedule_join.schedule_join_execute_id FROM `schedule` RIGHT JOIN schedule_join ON schedule_join.schedule_id = `schedule`.schedule_id WHERE	schedule_join.schedule_id = " + schedule_id ;
			 return dbUtilAutoTran.selectMutliple(sql);
		 }catch (Exception e) {
			 throw new Exception("FloorScheduleMgrZr.getScheduleJoinInfo():"+e);
		}
	}
	// 如果是有部门Id 没有执行人 那么就是表示的是用部门才。如果是有执行人那么就是用执行人的Id去进行查询
	public DBRow[] getScheduleByScheduleJoin(DBRow row,boolean isAjax) throws Exception{
		try{
 
			String execute_id = row.getString("execute_user_id");
			String startTime = row.getString("start");
			String endTime = row.getString("end");
			String schedule_state = row.getString("state");
			String assign_user_id = row.getString("assign_user_id");
			String assigndeptno = row.getString("assigndeptno");
			String executedeptno = row.getString("executedeptno");
			StringBuffer sql = new StringBuffer("select schedule.* from schedule_join left join `schedule` on `schedule`.schedule_id = schedule_join.schedule_id where ");
			sql.append(" ifnull(schedule_join.schedule_id,1) <> 0 ");
			if(null != execute_id && execute_id.length() > 0){
				sql.append(" and schedule_join.schedule_join_execute_id = ").append(execute_id);
			}else{
				if(null != executedeptno && executedeptno.length() > 0){
					sql.append(" and schedule_join.schedule_join_execute_id  in ").append("(select admin.adid from admin where admin.adgid = "+executedeptno+")");
				}
			}
			
			if(null != assign_user_id && assign_user_id.length() > 0){
				sql.append(" and schedule.assign_user_id =").append(assign_user_id);
			}else{
				if(null != assigndeptno && assigndeptno.length() > 0){
					sql.append(" and `schedule`.assign_user_id in ").append("(select admin.adid from admin where admin.adgid = "+assigndeptno+")");
				}
			}
			//and (((`schedule`.start_time >= '2012-03-04 00:00:00' and  `schedule`.start_time <= '2012-04-30 23:59:59') or (`schedule`.end_time >='2012-03-04 00:00:00' and `schedule`.end_time <= '2012-04-30 23:59:59')) or `schedule`.start_time <= '2012-03-04 00:00:00' and `schedule`.end_time >= '2012-04-30 23:59:59' )
			sql.append(" and is_schedule = 1 ");
			if(null != startTime && startTime.length() > 0 && null != endTime && endTime.length() > 0 ){
				if(!isAjax){
					sql.append(" and (((`schedule`.start_time >= '");
					sql.append(startTime);
					sql.append("' and  `schedule`.start_time < '");
					sql.append(endTime);
					sql.append("') or (`schedule`.end_time >='");sql.append(startTime);sql.append("' and `schedule`.end_time < '");
					sql.append(endTime);sql.append("')) or `schedule`.start_time <= '");
					sql.append(startTime);
					sql.append("' and `schedule`.end_time >= '"+endTime+"' ) ");
				}else{
					if(row.getString("direction").equals("next")){
						sql.append(" and (`schedule`.start_time >'"+startTime+"' and `schedule`.start_time  <'"+endTime+"' )");
					}else{
						sql.append(" and (`schedule`.end_time >'"+startTime+"' and `schedule`.end_time  <'"+endTime+"' )");
					}
				}
					
			}
			 
			if(null != schedule_state && schedule_state.length() > 0){
				if(schedule_state.equals("1")){
					sql.append("and (schedule_state + 0) = 10 ");
				}
				if(schedule_state.equals("-1")){
					 sql.append("and (schedule_state + 0) < 10 ");
				}
					
			}
			sql.append("group by schedule.schedule_id ");
			 //System.out.println("getScheduleByScheduleJoin sql : "  + sql.toString());
 			return dbUtilAutoTran.selectMutliple(sql.toString());
		 }catch (Exception e) {
			 throw new Exception("FloorScheduleMgrZr.getScheduleByScheduleJoin():"+e);
		}
	}
	public DBRow getRepeatScheduleById(long id) throws Exception{
		 try{
			 String sql = "select * from " + ConfigBean.getStringValue("schedule_repeat") + " where schedule_id="+id;
			 return dbUtilAutoTran.selectSingle(sql);
		 }catch (Exception e) {
			 throw new Exception("FloorScheduleMgrZr.getRepeatScheduleById():"+e);
		}
	}
	
	// 根据时间去计算那些是需要添加到普通任务中的也就是说要执行那个存储过程的
	// 也就是说只要是时间在 row中时间段中的都是可以添加的。如果不是的话就要break掉
	public void addRepeatSchedule(DBRow row , DBRow schedule , List<String> timeList , long repeatId)  throws Exception{
		try{
			if(null != timeList){
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				for(int index = 0  , count = timeList.size() ; index < count ;index++ ){
					String value = timeList.get(index);
					String[] array = value.split(",");
					if(array.length == 2){
						//第一个是开始的时间 第二个是结束的时间分别提取开始时间和结束的时间然后做比较计算出那些是要进行提添加的在那个时间段中
							Date start = format.parse(array[0]);
							Date end = format.parse(array[1]);
							String v = "call add_repeat_schedule('"+format.format(start)+"','"+format.format(end)+"',"+Integer.parseInt(schedule.getString("schedule_id"))+","+repeatId+")";
							dbUtilAutoTran.CallStringSingleResultProcedure(v);	 
					}
				}
				
			}
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.addRepeatSchedule():"+e);
		}
		
	}
	// 根据request的开始时间和结束的时间 和repeatId去查询刚刚新生成的任务
	public DBRow[] getNewCreateRpeatScheduleByRow(DBRow row , long repeatId)throws Exception {
		try{
			String startTime = row.getString("start");
			String endTime = row.getString("end");
			StringBuffer sql = new StringBuffer("select * from `schedule`    LEFT JOIN schedule_sub on schedule_sub.schedule_id = `schedule`.schedule_id ");
			sql.append(" where 	`schedule`.is_schedule = 1 and  repeat_id = ").append(repeatId);
			sql.append("  and (((`schedule`.start_time >= '"+startTime+"' and  `schedule`.start_time < '"+endTime+"') or (`schedule`.end_time >='"+startTime+"' and `schedule`.end_time < '"+endTime+"')) or `schedule`.start_time <= '"+startTime+"' and `schedule`.end_time >= '"+endTime+"' )");
			sql.append(" GROUP BY  `schedule`.schedule_id ");
 
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getNewCreateRpeatScheduleByRow():"+e);
		}
	}
	//根据row查询出有从来不停止的任务flag。然后根据时间去判断是不是要条件一些任务
	public void addRepeatScheduleByIsNerverStop(DBRow row) throws Exception{
		try{
			String start = row.getString("start");
			String end =  row.getString("end");
			String executeUserId = row.getString("execute_user_id");
			String assign_user_id = row.getString("assign_user_id");
			String executedeptno = row.getString("executedeptno");
			String assigndeptno = row.getString("assigndeptno");
			StringBuffer sql = new StringBuffer();
			sql.append(" select `schedule`.schedule_id ,schedule_repeat.repeat_express,`schedule`.end_time , `schedule`.start_time , schedule_repeat.repeat_schedule_id ,MAX(`schedule`.end_time) as max_time from `schedule_sub` ,`schedule` ,schedule_repeat  where `schedule`.is_schedule = '1'  and `schedule`.schedule_id = schedule_sub.schedule_id ");	 
	 
			if(null != assign_user_id && assign_user_id.length() > 0 ){
				sql.append(" and schedule.assign_user_id =").append(Long.parseLong(assign_user_id));
			}else{

				if(null != assigndeptno && assigndeptno.length() > 0){
					sql.append(" and schedule.assign_user_id in ").append(" (select admin.adid from admin_group , admin where admin_group.adgid = admin.adgid and admin_group.adgid = "+assigndeptno+")");
				}
			}
			sql.append(" and schedule_sub.schedule_execute_id ");
			if(executeUserId != null && executeUserId.length() > 0){
				sql.append("="+Long.parseLong(executeUserId));
			}else{
				if(null != executedeptno && executedeptno.length() > 0 ){
					sql.append(" in "+ " (select admin.adid from admin_group , admin where admin_group.adgid = admin.adgid and admin_group.adgid = "+executedeptno+")" );
				}
			}
			sql.append(" and schedule_repeat.repeat_never_flag = '1'  and `schedule`.repeat_id = schedule_repeat.repeat_schedule_id group by schedule_repeat.repeat_schedule_id");
			 //System.out.println(" value  :"+ sql);
			DBRow[] rows =  dbUtilAutoTran.selectMutliple(sql.toString());
			if(null != rows && rows.length > 0){
				String dateExp = "yyyy-MM-dd HH:mm:ss";   
				SimpleDateFormat format = new SimpleDateFormat(dateExp);  
				Date endFormat = format.parse(row.getString("end"));
				for(int index = 0 , count = rows.length ; index < count ;index++ ){
					Date endFromDataBase = format.parse(rows[index].getString("max_time"));
					if(endFromDataBase.before(endFormat)){
						//进行添加新的任务。通过cronExpress
						long repeatId = Long.parseLong(rows[index].getString("repeat_schedule_id"));
						long schedule_id = Long.parseLong(rows[index].getString("schedule_id"));
						String fixedStartTime =  rows[index].getString("start_time").split(" ")[1];
						String fixedEndTime = rows[index].getString("end_time").split(" ")[1];
						CronExpression cronExp = new CronExpression(rows[index].getString("repeat_express"));
						int length = DateUtil.getSubDay(rows[index].getString("end_time"),rows[index].getString("start_time"));
						
						long sum = length * 24 * 60 * 60 *1000;
						List<String> values = new ScheduleTime().hasNerverStopFlag(endFormat, cronExp, endFromDataBase, fixedStartTime, fixedEndTime,sum);
						String getOldSql = "select `schedule`.schedule_id from `schedule` where repeat_id = "+repeatId+" and is_schedule = 3; ";
						DBRow repeatScheduleRow  = dbUtilAutoTran.selectSingle(getOldSql);
						if(repeatScheduleRow != null &&  Long.parseLong(repeatScheduleRow.getString("schedule_id")) != 0l){
							long repeatScheduleId = Long.parseLong(repeatScheduleRow.getString("schedule_id"));
							//	System.out.println("add length : " + values.size());
							for(String va : values){
								String[] arrayTime = va.split(","); 
								String v = "call add_repeat_schedule('"+arrayTime[0].substring(0, 19)+"','"+arrayTime[1].substring(0, 19)+"',"+repeatScheduleId+",'"+repeatId+"')";
								dbUtilAutoTran.CallStringSingleResultProcedure(v);	 
							}
						}
					}
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorScheduleMgrZr.addRepeatScheduleByIsNerverStop():"+e);
		}
	}
	public DBRow getRepeatInfoById(long id) throws Exception{
		try{
			String sql = "select * from  " + ConfigBean.getStringValue("schedule_repeat") + " where  repeat_schedule_id="+id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getRepeatInfoById():"+e);
		}
	}
	// 删除schedule表和repeat 表 , replay sub join 中的数据中的数据 以后改成一个存储过程
	public DBRow[] deleteMutiRepeatSchedule(long id , String flag) throws Exception{
		try{
			StringBuffer sql = new StringBuffer();
			DBRow[] result = null;
			if("all".equals(flag)){
				result = dbUtilAutoTran.selectMutliple(" select `schedule`.schedule_id from `schedule` where `schedule`.repeat_id = " + id);
				String v = "call delete_muti_schedule("+id+")";
				dbUtilAutoTran.CallStringSingleResultProcedure(v);	
			}else{
				 
				result = dbUtilAutoTran.selectMutliple(" select `schedule`.schedule_id from `schedule` where `schedule`.repeat_id = " + id + " and  `schedule`.start_time >= '"+flag+"'");
				String v = "call delete_muti_schedule_after_time("+id+" , '"+flag+"')";
				dbUtilAutoTran.CallStringSingleResultProcedure(v);
			}
			
			
			return result;
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getRepeatInfoById():"+e);
		}
	}
	public int deleteScheduleByRepeatIdAndStartTime(long repeatId , String startTime) throws Exception{
		try{
			int count = dbUtilAutoTran.delete(" where `schedule`.is_schedule = 1 and`schedule`.repeat_id = "+repeatId+" and `schedule`.start_time >= '"+startTime+"'",  ConfigBean.getStringValue("schedule"));
			return count;
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.deleteScheduleByRepeatIdAndStartTime():"+e);
		}
	}
	public DBRow[] getNewAddScheduleByUpdate(String starTime , long repeatId) throws Exception{
		try{
			 String sql = "select * from `schedule`  left join schedule_sub on  `schedule`.schedule_id = schedule_sub.schedule_id where repeat_id = "+repeatId+" and start_time >= '"+starTime+"' and is_schedule ='1';";
			 return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getNewAddScheduleByUpdate():"+e);
		}
	}
	// 一下是 bill中的  
	public DBRow[] getWalbillByRow(DBRow row , PageCtrl pc) throws Exception{
		try{
			 long psId = Long.parseLong(row.getString("ps_id"));
			 String outId =  row.getString("out_id");
			 StringBuffer sql  = new StringBuffer("select * from waybill_order where   parent_waybill_id <> -1  and waybill_order.ps_id  = " + psId);
			 if(null != outId  && outId.length() > 0 ){
				 sql.append(" and  waybill_order.out_id="+outId);
			 }
			 return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getWalbillByRow():"+e);
		}
	}
	public long addNewStoreBill(DBRow row) throws Exception{
		try{
			String dateExp = "yyyy-MM-dd HH:mm:ss";
			SimpleDateFormat format = new SimpleDateFormat(dateExp);
			String date = format.format(new Date());
			row.add("create_time", date);
			row.add("state", "1");
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("out_storebill_order"), row);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.addNewStoreBill():"+e);
		}
	}
	public DBRow[] getStoresByPsIdAndState(long psId,String state) throws Exception{
		try{
			String sql = "select * from out_storebill_order where ps_id = 100000 and state = '"+state+"'";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getStoresByPsIdAndState():"+e);
		}
	}
	
	public void updateWayBillStore(long id , String ids) throws Exception{
		try{
			DBRow updateRow = new DBRow();
			updateRow.add("out_id", id);
			dbUtilAutoTran.update("where waybill_id in ("+ids+")", ConfigBean.getStringValue("waybill_order"), updateRow);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.updateWayBillStore():"+e);
		}
	}
	public DBRow[] getAllStoreIsOpenAndPsId(long psid)throws Exception{
		try{
			 String sql = "select * from "+ ConfigBean.getStringValue("out_storebill_order")+" where out_storebill_order.state = '1' and out_storebill_order.ps_id = "+psid;
			 return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getAllStoreIsOpenAndPsId():"+e);
		}
	}
	public DBRow[] getAllItemInWayBillById(long wayBillId) throws Exception{
		try{
			 String sql = " select waybill_order_item.*,product.p_name , waybill_order_item.pc_id  from waybill_order_item LEFT JOIN  product on product.pc_id = waybill_order_item.pc_id   where  waybill_order_id = " + wayBillId;
		//	System.out.println(" sql : " + sql);
			 return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getAllItemInWayBillById():"+e);
		}
	}
	public DBRow getWayBillById(long id) throws Exception{
		try{
			 String sql = "select * from " +  ConfigBean.getStringValue("waybill_order") +" where waybill_id = "+id;
			 return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getWallBillById():"+e);
		}
	}
	public long addWayBillByDBRow(DBRow row) throws Exception{
		try{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("waybill_order"), row);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.addWayBillByDBRow():"+e);
		}
	}

	/**
	 *  1.更新WayItem表中的数据和插入新的数据 By Id 
	 *  2.如果是减掉过后的数量为零了那么就要删除这条记录Item
	 */
	public void updateWayItemByWayId(String id  , String number ,long oldWayId ,long newWayId) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("waybill_order_item");
			// 更新原始Item的中的记录
			String sqlSelect = "select * from "+ tableName+" where waybill_order_item_id = " + id + " and waybill_order_id=" +oldWayId ; 
		 
			DBRow row = dbUtilAutoTran.selectSingle(sqlSelect);
			if(row != null ){
			 
				int count =  0;
				String quantity = row.getString("quantity");
				int length = quantity.indexOf(".");
				if(length != -1){
					count = Integer.parseInt(quantity.substring(0, length));
				}else{
					count = Integer.parseInt(quantity);
				}
				int value = Integer.parseInt(number) - count;
				if( value > 0){
					 throw new RuntimeException("拆分数量大于了购买数量");
				}else{
					//这个时候要插入新的值。如果是等于0 的要删除原来的值
					row.remove("waybill_order_item_id");
					row.add("quantity",number);
					row.add("waybill_order_id", newWayId);
				 	dbUtilAutoTran.insertReturnId(tableName, row);
				 
					if(value == 0){
						//这个时候是要删除
						dbUtilAutoTran.delete(" where  waybill_order_item_id = " + id, tableName);
					}else {
						// 这个时候是要更新
						DBRow updateRow = new DBRow();
						updateRow.add("quantity", -value);
						dbUtilAutoTran.update(" where waybill_order_item_id = " + id, tableName, updateRow);
					}
				}
			}
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.updateWayItemByWayId():"+e);
		}
	}
	public DBRow getWayItemsByWayId(long wayId) throws Exception {
		try{
			DBRow result = new DBRow();
			 String sql = "select * from waybill_order_item where  waybill_order_item.waybill_order_id = " + wayId;
			 DBRow[] rows =  dbUtilAutoTran.selectMutliple(sql);
			 result.add("split", rows);
			 String waySelect = "select * from  waybill_order where waybill_id = " + wayId;
			 result.add("waybill", dbUtilAutoTran.selectSingle(waySelect));
			 return result;
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getWayItemsByWayId():"+e);
		}
	}
	public DBRow[] getSplitInfoById(long wayBillId) throws Exception {
		try{
			 StringBuffer sql = new StringBuffer("select	waybill_order_item.*, t.waybill_id,product.p_name ");
			 sql.append(" from waybill_order_item,(select waybill_order.waybill_id from waybill_order where parent_waybill_id = "+wayBillId+" ) t , product ");
			 sql.append(" where product.pc_id = waybill_order_item.pc_id and t.waybill_id = waybill_order_item.waybill_order_id order by t.waybill_id asc ");
			// System.out.println("sql : " + sql.toString());
			 return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getSplitInfoById():"+e);
		}
	}
	// 这个里面要包含有什么运费的 成本的重新的计算
	public void cencelWayBillItems(DBRow row , long parentWayId) throws Exception{
		String tableName = ConfigBean.getStringValue("waybill_order_item");
		try{
			//row 是要删除的Row 里面包含有
			// 要查询出waybill_order中的 items是不是还包含有这个商品。如果有的话就把添加的记录加上去。没有的话就要insert一条新的记录
			
			DBRow updateRow = null ;
			String sqlSelect =  "select	* ,t.parent_waybill_id from waybill_order_item , (select waybill_order.parent_waybill_id from waybill_order where waybill_order.waybill_id = "+row.getString("waybill_order_id")+") t  where waybill_order_item.pc_id = "+row.getString("pc_id")+" and waybill_order_item.waybill_order_id = t.parent_waybill_id;";
			updateRow =  dbUtilAutoTran.selectSingle(sqlSelect);
			if(null != updateRow){
				String addCount = row.getString("quantity");
				String oldCount = updateRow.getString("quantity");
				int length = addCount.indexOf(".");
				int addCountInt = 0;
				int oldCountInt = 0 ;
				if(length != -1){
					addCountInt = Integer.parseInt((addCount.substring(0,length)));
					oldCountInt = Integer.parseInt((oldCount.substring(0,oldCount.indexOf("."))));
				}else{
					addCountInt = Integer.parseInt((addCount ));
					oldCountInt = Integer.parseInt((oldCount ));
				}
				int total = addCountInt + oldCountInt;
				updateRow.add("quantity", total);
				updateRow.add("waybill_order_id", parentWayId);
				updateRow.remove("parent_waybill_id");
			 
				dbUtilAutoTran.update(" where waybill_order_item_id = " + updateRow.getString("waybill_order_item_id") , tableName, updateRow);
				// 删除原来的记录
				dbUtilAutoTran.delete("where waybill_order_item_id = " + row.getString("waybill_order_item_id"), tableName);
			}else{
				//用Row 来update
				row.remove("p_name");
				row.add("waybill_order_id", parentWayId);
				row.remove("pc_id");
				dbUtilAutoTran.update(" where waybill_order_item_id = " + row.getString("waybill_order_item_id"), tableName, row);
			}
			
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.cencelWayBillItems():"+e);
		}
	}
	public void updateWayBillSetParentWayBillId(long wayBillId) throws Exception{
		try{
			DBRow updateRow = new DBRow();
			updateRow.add("parent_waybill_id", -1);
			
			dbUtilAutoTran.update(" where waybill_id = " + wayBillId, ConfigBean.getStringValue("waybill_order"), updateRow);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.updateWayBillSetParentWayBillId():"+e);
		}
	}
	public DBRow[] getAdminUserByAdid(String ids) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("admin");
			DBRow[] result = null ;
			StringBuffer sql = new StringBuffer("select * from " ).append(tableName).append(" where adid in(");
			String[] idArray = ids.split(",");
			StringBuffer id = new StringBuffer();
			if(idArray != null && idArray.length > 0){
				for(int index = 0 , count = idArray.length ; index < count ; index++ ){
					id.append(",").append(idArray[index]);
				}
				if(id.length() > 1){
					sql.append(id.toString().substring(1)).append(")");
					result = dbUtilAutoTran.selectMutliple(sql.toString()) ;
				} 
			}
			return result;
			
 		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getAdminUserByAdid():"+e);
		}
		
	}
	public DBRow getScheduleByAssociate(long associate_id, int associate_type,int associate_process)  throws Exception {
		try{
			StringBuffer sql = new StringBuffer("select * from `schedule` where associate_type=");
			sql.append(associate_type).append(" and associate_process=").append(associate_process).append(" and associate_id=").append(associate_id);
			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getScheduleByAssociate():"+e);
		}
	}
	
	/**
	 * 通过关联ID,关联类型，获取任务
	 * @param associate_id
	 * @param associate_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getScheduleByAssociateIdAndType(long associate_id, int[] associate_types)  throws Exception {
		try{
			StringBuffer sql = new StringBuffer("select * from `schedule` where associate_id = ").append(associate_id);
			
			if(null != associate_types && associate_types.length > 0)
			{
				String sqlWhere = "";
				if(1 == associate_types.length)
				{
					sqlWhere = " and associate_type = " + associate_types[0];
				}
				else
				{
					sqlWhere = " and (associate_type = " + associate_types[0];
					for (int i = 1; i < associate_types.length; i++) {
						sqlWhere += " or associate_type = " + associate_types[i];
					}
					sqlWhere += ")";
				}
				sql.append(sqlWhere);
			}
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getScheduleByAssociateIdAndType():"+e);
		}
	}
	/**
	 * 根据关联的信息得到 任务执行人的集合
	 * @param associate_id
	 * @param associate_type
	 * @param associate_process
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getScheduleExecuteUsersInfoByAssociate(long associate_id, int associate_type,int associate_process) throws Exception {
		try{
			String sql = "select sub.* , admin.employe_name from `schedule` as sch  "
					+ "LEFT JOIN schedule_sub as sub on sch.schedule_id = sub.schedule_id "
					+ "LEFT JOIN admin on admin.adid = sub.schedule_execute_id "
					+ "where sch.associate_id = "+associate_id+" and sch.associate_process = "+associate_process+" and sch.associate_type ="+ associate_type;
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getScheduleExecuteUsersInfoByAssociate():"+e);
		}
	}
	 
	public void simpleUpdateSchedule(long schedule_id , DBRow row) throws Exception {
		try{
			dbUtilAutoTran.update("where schedule_id="+schedule_id, ConfigBean.getStringValue("schedule"), row);	
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.simpleUpdateSchedule():"+e);
		}
	}
	public String getScheduleSubIds(long schedule_id) throws Exception{
		try{
			
			StringBuffer sql = new StringBuffer();
			DBRow[] array  =  null ;
			sql.append("select * from ").append(ConfigBean.getStringValue("schedule_sub")).append(" where schedule_id=").append(schedule_id);
			array = dbUtilAutoTran.selectMutliple(sql.toString());
			if(array != null && array.length > 0){
				StringBuffer ids = new StringBuffer();
				for(DBRow row : array){
					ids.append(",").append(row.getString("schedule_execute_id"));
				}
				if(ids.length() > 1){
					return ids.substring(1);
				}
			}
			return "";
			
			 			
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getScheduleSubIds():"+e);
		}
		
	}
	public void updateAllScheduleOfDetail(long associate_id , int associate_type , String detail) throws Exception {
		try{
			 DBRow temp = new DBRow();
			 temp.add("schedule_detail", detail);
			dbUtilAutoTran.update(" where associate_id ="+associate_id +" and associate_type="+associate_type, ConfigBean.getStringValue("schedule"), temp);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.updateAllScheduleOfDetail():"+e);
		}
	}
	public DBRow[] getScheduleNumberNotComplete(long  adid , int number ) throws Exception 
	{ 
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select `schedule`.schedule_id ,`schedule`.is_schedule ,`schedule`.assign_user_id ,`schedule`.start_time, `schedule_sub`.schedule_state ,`schedule`.schedule_overview from `schedule`  , schedule_sub where `schedule`.schedule_id  = schedule_sub.schedule_id   and cast(`schedule_sub`.schedule_state as signed) < 10 ");
			sql.append(" and schedule_sub.schedule_execute_id =").append(adid).append(" order by `schedule`.schedule_id desc limit 0,").append(number);
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getScheduleNumberNotComplete():"+e);
		}
	}
	public DBRow[] getKeFuProductMangerUserIds() throws Exception {
		try{
			String sql = "select adid from " +  ConfigBean.getStringValue("admin") + " where adgid=100006 or adgid=100016";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getKeFuProductMangerUserIds():"+e);
		}
	}
	public DBRow[] getWorkFlowSchedule(long adid, int finish_type) throws Exception {
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from `schedule` , schedule_sub  where is_task = 1 and schedule_sub.schedule_id = `schedule`.schedule_id and schedule_sub.schedule_execute_id =").append(adid)
			.append(" and schedule_sub.is_task_finish =").append(finish_type).append(" order by `schedule`.schedule_id desc");
 			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getWorkFlow():"+e);
		}
	}
	public DBRow[] getSubScheduleByScheduleId(long schedule_id) throws Exception {
		try{
			String sql = "select * from  schedule_sub where schedule_sub.schedule_id = " + schedule_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getSubScheduleByScheduleId():"+e);
		}
	}
	public void updateSubScheduleFinish(long schedule_id) throws Exception {
		try{
			String sql = "update schedule_sub set is_task_finish=1 , schedule_state = 10 where schedule_id=" + schedule_id;
			 
			dbUtilAutoTran.executeSQL(sql); 
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.updateSubScheduleFinish(schedule_id)"+e);
		}
	}
	public DBRow countWorkFlowSchedule(long adid, int finish_type) throws Exception {
		try{
			StringBuffer sql = new StringBuffer("select count(*) as sum_count from `schedule` , schedule_sub  where is_task = 1 and schedule_sub.schedule_id = `schedule`.schedule_id and schedule_sub.schedule_execute_id =").append(adid).append(" and schedule_sub.is_task_finish=" )
			.append(finish_type);
			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.countWorkFlowSchedule( adid,  finish_type)"+e);
		}
	}
	public DBRow[] getMangerUserIds() throws Exception {
		try{
			String sql = "select adid from " +  ConfigBean.getStringValue("admin") + " where adgid=10000";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getMangerUserIds()"+e);
		}
	}
	public DBRow[] getNotFinishSchedule(long adid , int assign , long schedule_id ,  int pageSize) throws Exception {
		try{
			
			String sql = " SELECT sch.schedule_id,  sch.associate_id , sch.associate_process, sch.associate_type, sub.schedule_execute_id,sch.schedule_state,sch.schedule_detail " 
						+" FROM schedule_sub AS sub LEFT JOIN `schedule` AS sch ON sub.schedule_id = sch.schedule_id WHERE 	sch.is_schedule = '"+assign+"' and sub.schedule_execute_id="+adid ;
			if(schedule_id > 0l){
				sql += " and sch.schedule_id >" + schedule_id ;
			}
			sql += (" LIMIT 0,"+pageSize);
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getAllScheduleByAdid()"+e);
		}
	}
	/**
	 * 获取某个人没有完成的任务
	 * @param adid
	 * @param assign
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月5日
	 */
	public int countNotFinishScheduleByAdid(long adid ) throws Exception {
		try{
			
			String sql = "select count(*) as total from schedule_sub as sub LEFT JOIN `schedule` as sch ";
				   sql += "on sch.schedule_id =  sub.schedule_id where schedule_execute_id = "+adid+" and sub.schedule_state < "+ScheduleFinishKey.ScheduleFinish+"  and sch.is_schedule = 1" ;
			DBRow countRow = dbUtilAutoTran.selectSingle(sql);
 			if(countRow != null){
				return countRow.get("total", 0);
			}
			return 0 ;
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.countAllScheduleByAdid()"+e);
		}
	}
	
	/**
	 * 查询某个人没有 完成的任务 && 这个任务 没有和任何一个业务单据关联
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月5日
	 */
	public DBRow[] queryScheduleUnFinishAndNoAssociate(long adid) throws Exception{
		try{
			String sql = "" ;
			sql = " select * from  schedule_sub as sub  LEFT JOIN `schedule` as sch  on sch.schedule_id = sub.schedule_id " ;
			sql += " LEFT JOIN admin on sch.assign_user_id = admin.adid " ;
			sql += " where sub.schedule_execute_id = "+adid+" and sub.is_task_finish = 0 and IFNULL(sch.associate_id,0) = 0 " ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.querySchedule( adid )"+e);
		}
	}
	
	//is_schedule 表示已经安排下去了
	public DBRow[] getNotFinishScheduleTypeBy(long adid) throws Exception{
		try{
			
			String sql = "	select  sch.associate_process , count(*) as total from schedule_sub as ssub LEFT JOIN `schedule` as sch  on sch.schedule_id = ssub.schedule_id "+
						 "	where ssub.schedule_execute_id = "+adid+" and sch.schedule_state < "+ScheduleFinishKey.ScheduleFinish+" and sch.is_schedule = 1 GROUP BY sch.associate_process " ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.getNotFinishScheduleTypeBy( adid )"+e);
		}
	}
	/**
	 * 通过业务单据获取scheudle
	 * @param associate_id
	 * @param associate_process
	 * @param associate_type
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月6日
	 */
	public DBRow[] getScheduleBy(long associate_id, int associate_process, int associate_type) throws Exception{
		try{
			
			String sql = "select * from " +  ConfigBean.getStringValue("schedule")  + " where associate_process="+associate_process + " and associate_type="+associate_type + " and associate_id="+associate_id ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.getScheduleBy( adid )"+e);
		}
	}
	
	public DBRow[] getExcuteUserInfos(long associate_id, int associate_process, int associate_type) throws Exception{
		try{
			String sql = "select admin.* from `schedule` as sch "
					+ "LEFT JOIN schedule_sub as sub on sch.schedule_id = sub.schedule_id "
					+ "LEFT JOIN admin on admin.adid = sub.schedule_execute_id "
					+ "where sch.associate_id ="+associate_id+" and sch.associate_process ="+associate_process+" and sch.associate_type ="+ associate_type;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.getExcuteUserInfos()"+e);
		}
	}
	/**
	 * 获取当前这个人最新的Schedule
	 * @param schedule_id
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月9日
	 */
	public DBRow getLastSchedule(long adid) throws Exception{
		try{
			String sql = "select   * from schedule_sub as sub LEFT JOIN `schedule` as sch "
					+ "	on sch.schedule_id = sub.schedule_id  where  sub.schedule_execute_id  ="+adid + " and sub.schedule_state <"+ ScheduleFinishKey.ScheduleFinish
					+ " ORDER BY sub.schedule_id desc LIMIT 0,1 ";
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.getLastSchedule()"+e);
		}
	}
	public int simpleUpdateScheduleBy(long associate_id, int associate_process, int associate_type ,DBRow updateRow ) throws Exception{
		try{
			return dbUtilAutoTran.update(" where associate_id="+associate_id + " and associate_process="+associate_process + " and associate_type="+associate_type, 
					ConfigBean.getStringValue("schedule"), updateRow);
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.simpleUpdateScheduleBy()"+e);
		}
	}
	
	public DBRow[] getCheckInModelDetailNotFinishSchedule(long detail_id , long entry_id) throws Exception{
		try{
			String sql = "select * from  `schedule` as sch where sch.associate_id = "+detail_id+" and sch.schedule_state <"+ScheduleFinishKey.ScheduleFinish+" and associate_type="+ModuleKey.CHECK_IN + " and associate_main_id="+entry_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.getCheckInModelDetailNotFinishSchedule()"+e);
		}
	}
	
	/**
	 * 通过module和processkeys查询schedule数量
	 * @param module
	 * @param module_id
	 * @param processKeys
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月5日 下午6:30:49
	 */
	public int findScheduleAssociateCountByAssociateMainIdType(int module, long module_id, int[] processKeys) throws Exception
	{
		try
		{
			String sql = "select count(*) cn from  `schedule` where associate_type="+module
					+ " and associate_main_id="+module_id+" ";
			if(processKeys != null && processKeys.length > 0)
			{
				sql += " and (associate_process="+processKeys[0];
				for (int i = 1; i < processKeys.length; i++) {
					sql += " or associate_process="+processKeys[i];
				}
				sql +=")";
			}
			return dbUtilAutoTran.selectSingle(sql).get("cn", 0);
		}catch(Exception e){
			throw new Exception("FloorScheduleMgrZr.findScheduleAssociateCountByAssociateMainIdType()"+e);
		}
	}
 
}
 