package com.cwc.app.floor.api.xj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;


public class FloorProductChangeMgrXJ 
{
	private DBUtilAutoTran dbUtilAutoTran;

	
	/**
	 * 想主表和详细表添加记录
	 */
	public long addProductChange(DBRow row) throws Exception {
		try {
			
			return  dbUtilAutoTran.insertReturnId("product_change", row);
			

		} catch (Exception e) {
			throw new Exception("FloorProductMgrXJ.addProductChange(DBRow row) error:"+e);
		}
		
	}
	/**
	 * 向详细表添加记录
	 */
	public long addProductChangeItem(DBRow rowItem) throws Exception {
		try {
			
			return  dbUtilAutoTran.insertReturnId("product_change_item", rowItem);

		} catch (Exception e) {
			throw new Exception("FloorProductMgrXJ.addProductChangeItem(DBRow rowItem) error:"+e);
		}
		
	}
	/**
	 * 查询主表
	 */
	public DBRow[] findProductChange(PageCtrl pc,String ps_id,long is_open,String startTime,String endTime) throws Exception {
		try {
			String sql = "select * from product_change pc left join product_storage_catalog psc on psc.id=pc.ps_id left join admin a on a.adid=pc.adid where 1=1 ";
			if(!ps_id.equals("")){
				sql +=" and  pc.ps_id in ("+ps_id +")";
			}
			if(is_open>0){
				sql +=" and is_open="+is_open;
			}
			if(!startTime.equals("")){
				sql +=" and create_time >= "+"\'"+startTime+"\'";
			}
			if(!endTime.equals("")){
				sql +=" and create_time <= "+"\'"+endTime+"\'";
			}
			sql +=" order by pc.is_open ,pc.create_time desc";
			return  dbUtilAutoTran.selectMutliple(sql,pc);

		} catch (Exception e) {
			throw new Exception("FloorProductMgrXJ findProductChange error:"+e);
		}
		
	}
	/**
	 * 根据主表id查询详细表记录
	 */
	public DBRow[] findProductChangeItems(long cid,PageCtrl pc) throws Exception {
		try {
			String sql ="select * from product_change_item pci left join product p on p.pc_id = pci.pc_id " +
						"where pci.cid ="+cid;
			return  dbUtilAutoTran.selectMutliple(sql, pc);

		} catch (Exception e) {
			throw new Exception("FloorProductMgrXJ.findProductChangeItems(PageCtrl pc) error:"+e);
		}
	}
	/**
	 * 根据仓库id和打开状态查询主表数据
	 */
	public DBRow findProductChangeByPsId(long ps_id) throws Exception {
		try {
			String sql = "select * from product_change  where ps_id ="+ps_id +" and is_open="+1;
			return  dbUtilAutoTran.selectSingle(sql);

		} catch (Exception e) {
			throw new Exception("FloorProductMgrXJ findProductChangeByPsId error:"+e);
		}
	}
	/**
	 * 修改详细表记录
	 */
	public void modProductChangeDetail(long itemid, DBRow para) throws Exception{
		try 
		{
			dbUtilAutoTran.update("where itemid="+itemid,"product_change_item",para);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgrXJ modProductChange error:"+e);
		}
		
	}
	/**
	 * 删除详细表记录
	 */
	public void delProductChangeDetail(long itemid) throws Exception {
		try 
		{
			dbUtilAutoTran.delete("where itemid="+itemid,"product_change_item");
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgrXJ delProductChange error:"+e);
		}
		
	}
	/**
	 * 根据商品id、类型、主表id查询详细表记录
	 */
	public DBRow findProductChangeItem(long pc_id, long type,long cid) throws Exception {
		try {
			String sql = "select * from product_change_item  where pc_id ="+pc_id +" and type="+type+" and cid="+cid;
			return  dbUtilAutoTran.selectSingle(sql);

		} catch (Exception e) {
			throw new Exception("FloorProductMgrXJ findProductChangeItem error:"+e);
		}
	}
	/**
	 * 根据详细表id查询详细表记录
	 */
	public DBRow findProductChangeItemByItemId(long itemid) throws Exception {
		try {
			String sql = "select * from product_change_item  where itemid ="+itemid ;
			return  dbUtilAutoTran.selectSingle(sql);

		} catch (Exception e) {
			throw new Exception("FloorProductMgrXJ findProductChangeItemByItemId error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
	
	
	
	
	
	
	
	
}
