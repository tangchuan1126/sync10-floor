package com.cwc.app.floor.api.zj;

import java.util.regex.Matcher;
import java.util.regex.Pattern;






import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.key.DeliveryOrderKey;
import com.cwc.app.key.JqGridFilterKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorDeliveryMgrZJ {
		
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 生成交货单
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addDelveryOrder(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long delivery_order_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("delivery_order"));
			
			dbrow.add("delivery_order_id",delivery_order_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("delivery_order"),dbrow);
			
			return (delivery_order_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ addDelveryOrder error:"+e);
		}
	}
	
	/**
	 * 修改交货单
	 * @param delvery_order_id
	 * @param para
	 * @throws Exception
	 */
	public void modDelveryOrder(long delivery_order_id,DBRow para)
		throws Exception
	{
		try {
			dbUtilAutoTran.update("where delivery_order_id="+delivery_order_id,ConfigBean.getStringValue("delivery_order"),para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ modDelveryOrder error:"+e);
		}
	}
	
	/**
	 * 根据采购单号查询对应的所有运单
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDelveryOrdersByPurchaseId(long purchase_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("delivery_order")+" where delivery_purchase_id =?";
			
			DBRow para = new DBRow();
			para.add("delvery_purchase_id",purchase_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorDelveryMgrZJ getDelveryOrdersByPurchaseId error:"+e);
		}
	}
	
	/**
	 * 根据ID获得交货单详细
	 * @param delvery_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailDelveryOrderById(long delvery_order_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("delivery_order")+" where delivery_order_id=?";
			
			DBRow para = new DBRow();
			para.add("delvery_order_id",delvery_order_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorDelveryMgrZJ getDetailDelveryOrderById error:"+e);
		}
	}
	
	/**
	 * 添加交货详细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addDeliveryOrderDetail(DBRow dbrow)
		throws Exception
	{
		try {
			long delivery_order_detail_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("delivery_order_detail"));
			
			dbrow.add("delivery_order_detail_id",delivery_order_detail_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("delivery_order_detail"),dbrow);
			
			return (delivery_order_detail_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ addDeliveryOrderDetail error:"+e);
		}
	}
	
	/**
	 * 清空
	 * @param delivery_order_detail_id
	 * @throws Exception
	 */
	public void clearDeliveryOrderDetail(long delivery_order_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where delivery_order_id="+delivery_order_id,ConfigBean.getStringValue("delivery_order_detail"));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ clearDeliveryOrderDetail error:"+e);
		}
	}
	
	/**
	 * 根据交货单明细ID删除交货单明细
	 * @param delivery_order_detail_id
	 * @throws Exception
	 */
	public void delDelvieryOrderDetail(long delivery_order_detail_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where delivery_order_detail_id = "+delivery_order_detail_id,ConfigBean.getStringValue("delivery_order_detail"));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ delDelvieryOrderDetail error:"+e);
		}
	}
	
	/**
	 * 根据交货单ID获得交货单细节
	 * @param delivery_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryOrderDetails(long delivery_order_id,long purchase_id,PageCtrl pc,String sidx,String sord,FilterBean fillterBean)
		throws Exception
	{
		try 
		{
			
			DBRow para = new DBRow();
			
			StringBuffer sql = new StringBuffer("select dod.*,p.unit_name as unitName, pd.purchase_count as purchasecount,pd.reap_count as reapcount,weight from "+ConfigBean.getStringValue("delivery_order_detail")+" as dod "
						+" left join "+ConfigBean.getStringValue("purchase_detail")+" as pd on dod.product_id = pd.product_id and pd.purchase_id = ? "
						+" left join "+ConfigBean.getStringValue("product")+" as p on dod.product_id = p.pc_id"
						+" where dod.delivery_order_id = ? ");

			para.add("purchase_id",purchase_id);
			para.add("delivery_order_id",delivery_order_id);
			
			if(fillterBean !=null)
			{
				sql.append(new JqGridFilterKey().filterSQL(fillterBean));
			}
			
			if(sidx==null||sord==null)
			{
				sql.append(" order by delivery_order_detail_id desc");
			}
			else
			{
				sql.append(" order by "+sidx+" "+sord);
			}
			

			return (dbUtilAutoTran.selectPreMutliple(sql.toString(),para,pc));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorDelveryMgrZJ getDeliveryOrderDetails error:"+e);
		}
	}
	
	/**
	 * 根据交货单明细ID获得交货单明细（jqgrid修改交货单商品名用）
	 * @param delivery_order_detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDeliveryOrderDetail(long delivery_order_detail_id)
		throws Exception
	{
		String sql = "select dod.*,p.unit_name as unitName,pd.purchase_count as purchasecount,pd.reap_count as reapcount from "+ConfigBean.getStringValue("delivery_order_detail")+" as dod "
				+" left join "+ConfigBean.getStringValue("delivery_order")+" as do on do.delivery_order_id = dod.delivery_order_id "
				+" left join "+ConfigBean.getStringValue("purchase_detail")+" as pd on dod.product_id = pd.product_id and pd.purchase_id = do.delivery_purchase_id "
				+" left join "+ConfigBean.getStringValue("product")+" as p on dod.product_id = p.pc_id"
				+" where dod.delivery_order_detail_id = ? ";
		
		DBRow para = new DBRow();
		para.add("delivery_order_detail_id",delivery_order_detail_id);
		
		return (dbUtilAutoTran.selectPreSingle(sql, para));
	}
	/**
	 * 获得交货单列表（可根据供应商，状态，仓库检索）
	 * @param supplier
	 * @param status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryOrder(long supplier,int status,long ps_id,PageCtrl pc,int declaration, int clearance,int invoice, int drawback,int day, long productline_id,int stock_in_set, long delivery_create_account_id)
		throws Exception
	{
		try {
			StringBuffer sql = new StringBuffer("select do.*,psc.title as title from "+ConfigBean.getStringValue("delivery_order")+" as do,"+ConfigBean.getStringValue("purchase")+" as p,"+ConfigBean.getStringValue("product_storage_catalog")+" as psc where p.purchase_id = do.delivery_purchase_id and psc.id = do.delivery_psid");
			TDate tDate = new TDate();
			tDate.addDay(-day);
			if(day != 0) {
				sql.append(" and updatedate<= '"+tDate.formatDate("yyyy-MM-dd")+"' ");
			}
			if(productline_id != 0 && supplier ==0) {
				sql.append(" and delivery_supplier_id in (select id from supplier where product_line_id = " + productline_id + ")");
			}
			if(supplier !=0)
			{
				sql.append(" and delivery_supplier_id = "+supplier);
			}
			
			if(status !=0)
			{
				if(status==5)
					sql.append(" and delivery_order_status in("+DeliveryOrderKey.READY + "," + DeliveryOrderKey.INTRANSIT + "," + DeliveryOrderKey.APPROVEING + ")");
				else
				sql.append(" and delivery_order_status = "+status);
			}
			
			if(ps_id!=0)
			{
				sql.append(" and do.delivery_psid = "+ps_id+" and do.delivery_purchase_id = p.purchase_id");
			}
			if(declaration!=0) {
				sql.append(" and IFNULL(declaration,1)="+declaration);
			}
			if(clearance!=0) {
				sql.append(" and IFNULL(clearance,1)="+clearance);
			}
			if(invoice!=0) {
				sql.append(" and IFNULL(invoice,1)="+invoice);
			}
			if(drawback!=0) {
				sql.append(" and IFNULL(drawback,1)="+drawback);
			}
			if(stock_in_set!=0) {
				sql.append(" and ifnull(stock_in_set,1)="+stock_in_set);
			}
			if(delivery_create_account_id != 0) {
				sql.append(" and delivery_create_account_id = " + delivery_create_account_id);
			}
			sql.append(" order by delivery_order_id desc");
			
			return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
		} 
		
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ getDeliveryOrder error:"+e);
		}
	}
	
	/**
	 * 获得所有交货单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllDeliveryOrder(PageCtrl pc)
		throws Exception
	{
		String sql = "select do.*,psc.title as title from "+ConfigBean.getStringValue("delivery_order")+" as do,"+ConfigBean.getStringValue("purchase")+" as p,"+ConfigBean.getStringValue("product_storage_catalog")+" as psc where p.purchase_id = do.delivery_purchase_id and psc.id = do.delivery_psid order by delivery_order_id desc";
		
		return (dbUtilAutoTran.selectMutliple(sql,pc));
	}
	
	/**
	 * 搜索交货单
	 * @param nummber
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchDeliveryOrder(String number,PageCtrl pc,long supplier)
		throws Exception
	{
		try 
		{
			String regEx="[^0-9]";  
			Pattern p = Pattern.compile(regEx);  
			Matcher m = p.matcher(number);   
			long purchase_id = 0;
			if(!m.replaceAll("").equals(""))
			{
				purchase_id = Long.parseLong(m.replaceAll(""));
			}
			
			StringBuffer sql = new StringBuffer("select do.*,psc.title as title from "+ConfigBean.getStringValue("delivery_order")+" as do,"+ConfigBean.getStringValue("purchase")+" as p,"+ConfigBean.getStringValue("product_storage_catalog")+" as psc where p.purchase_id = do.delivery_purchase_id and psc.id = do.delivery_psid and (delivery_order_number = ? or delivery_purchase_id = ?)"); 
			
			if(supplier !=0)
			{
				sql.append(" and p.supplier="+supplier);
			}
			
			sql.append(" order by delivery_order_id desc");
			
			DBRow para = new DBRow();
			para.add("delivery_order_number",number);
			para.add("delivery_purchase_id",purchase_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(), para,pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ searchDeliveryOrder error:"+e);
		}
	}
	
	/**
	 * 根据交货单ID，商品ID获得交货单详细
	 * @param delivery_order_id
	 * @param product_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDeliveryOrderDetailsByPDId(long delivery_order_id,long product_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("delivery_order_detail")+" where delivery_order_id = ? and product_id = ?";
			
			DBRow para = new DBRow();
			para.add("delivery_order_id",delivery_order_id);
			para.add("product_id",product_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorDelveryMgrZJ getDeliveryOrderDetailsByPDId error:"+e);
		}
	}
	
	/**
	 * 修改交货单详细
	 * @param delivery_order_detail_id
	 * @param para
	 * @throws Exception
	 */
	public void modDeliveryOrderDetailById(long delivery_order_detail_id,DBRow para)
		throws Exception
	{
		try {
			dbUtilAutoTran.update("where delivery_order_detail_id="+delivery_order_detail_id,ConfigBean.getStringValue("delivery_order_detail"),para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ modDeliveryOrderDetailById error:"+e);
		}
	}
	
	
	/**
	 * 检查交货单是否完成（到达量小于单应到量）
	 * @param delivery_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkDeliveryOrderFinish(long delivery_order_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("delivery_order_detail")+" where delivery_order_id = ? and delivery_reap_count<delivery_count";
			DBRow para = new DBRow();
			para.add("delivery_order_id",delivery_order_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ checkDeliveryOrderFinish error:"+e);
		}
	}
	
	/**
	 * 检查交货单是否要审核（到达量不等于应到量）
	 * @param delivery_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkDeliveryOrderApprove(long delivery_order_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("delivery_order_detail")+" where delivery_order_id = ? and delivery_reap_count != delivery_count";
			DBRow para = new DBRow();
			para.add("delivery_order_id",delivery_order_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ checkDeliveryOrderApprove error:"+e);
		}
	}
	
	/**
	 * 根据仓库ID获得所有以该仓库为目的仓库的已起运交货单的明细
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryOrderDetailsByPsidStatus(long ps_id,int status)
		throws Exception
	{
		try 
		{
			String sql = "select dod.*,d.delivery_order_number as number from delivery_order_detail as dod,delivery_order as d,purchase as p "
						+" where dod.delivery_order_id = d.delivery_order_id and d.delivery_purchase_id = p.purchase_id "
						+" and d.delivery_order_status = ? and d.delivery_psid = ?";
			
			DBRow para = new DBRow();
			para.add("delivery_order_status",status);
			para.add("ps_id",ps_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorDelveryMgrZJ getDeliveryOrderDetailsByPsidStatus error:"+e);
		}
	}
	
	/**
	 * 根据交货单号查询交货单
	 * @param delivery_order_number
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailDeliveryOrderByDeliveryNumber(String delivery_order_number)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("delivery_order")+" where delivery_order_number = ? ";
			DBRow para = new DBRow();
			para.add("delivery_order_number",delivery_order_number);
			
			return (dbUtilAutoTran.selectPreSingle(sql,para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ getDetailDeliveryOrderByDeliveryNumber error:"+e);
		}
	}
	
	/**
	 * 检查采购单是否有未保存的交货单
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNoSaveDeliveryOrderByPurchaseID(long purchase_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("delivery_order")+" where save_status = 1 and delivery_purchase_id =? ";
			
			DBRow para = new DBRow();
			para.add("deilvery_purchase_id",purchase_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ getNoSaveDeliveryOrderByPurchaseID error:"+e);
		}
	}
	
	/**
	 * 根据交货单ID删除交货单
	 * @param delivery_order_id
	 * @throws Exception
	 */
	public void delDeliveryOrder(long delivery_order_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where delivery_order_id="+delivery_order_id,ConfigBean.getStringValue("delivery_order"));
			this.delDeliveryOrderDetailByDeliveryOrderId(delivery_order_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ delDeliveryOrder error:"+e);
		}
	}
	
	/**
	 * 根据交货单ID删除交货单明细
	 * @param delivery_order_id
	 * @throws Exception
	 */
	public void delDeliveryOrderDetailByDeliveryOrderId(long delivery_order_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where delivery_order_id = "+delivery_order_id,ConfigBean.getStringValue("delivery_order_detail"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ delDeliveryOrderDetailByDeliveryOrderId error:"+e);
		}
	}
	
	/**
	 * 根据商品ID查询该交货单内是否已有此商品
	 * @param delivery_order_id
	 * @param product_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDeliveryOrderDetailByPcId(long delivery_order_id,long product_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("delivery_order_detail")+" where delivery_order_id = ? and product_id = ?";
			
			DBRow para = new DBRow();
			para.add("delivery_order_id",delivery_order_id);
			para.add("product_id",product_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorDelveryMgrZJ getDeliveryOrderDetailByPcname error:"+e);
		}
	}
	
	/**
	 * 搜索供应商未完成的交货单
	 * @param supplier_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryOrderNoFinishBySupplier(long supplier_id)
		throws Exception
	{
		try {
			String sql = "select do.* from "+ConfigBean.getStringValue("delivery_order")+" as do,purchase as p where p.supplier = ? and do.delivery_purchase_id=p.purchase_id and delivery_order_status="+DeliveryOrderKey.READY;
			
			DBRow para = new DBRow();
			para.add("supplier_id",supplier_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e)
		{
			throw new Exception("FloorDelveryMgrZJ getDeliveryOrderNoFinishBySupplier error:"+e);
		}
	}
	
	/**
	 * 获得采购单未完成的交货单（非已完成或非审核状态的交货单）
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryOrderNoFinishByPurchaseId(long purchase_id)
		throws Exception
	{
		try {
			String sql = "select * from "+ConfigBean.getStringValue("delivery_order")+" where delivery_purchase_id=? and delivery_order_status !="+DeliveryOrderKey.FINISH+" and delivery_order_status !="+DeliveryOrderKey.APPROVEING;
			
			DBRow para = new DBRow();
			para.add("delivery_purchase_id",purchase_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ getDeliveryOrderNoFinishByPurchaseId error:"+e);
		}
	}
	
	public DBRow[] getDeliverOrderDetailsByDeliveryId(long delivery_order_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("delivery_order_detail")+" where delivery_order_id = ?";
			
			DBRow para = new DBRow();
			para.add("delivery_order_id",delivery_order_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDeliveryMgrZJ getDeliverOrderDetailsByDeliveryId error:"+e);
		}
	}

	public DBRow[] getBillByBillId4Search(long[] billIds) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("delivery_order");
			if(billIds == null  || (billIds != null && billIds.length < 1)){
				return (new DBRow[0]);
			}else{
				StringBuffer whereContion = new StringBuffer("");
					for(int index = 0 , count = billIds.length ; index < count ; index++ ){
						if(index == 0){
							whereContion.append(" where delivery_order_id=").append(billIds[0]);
						}else{
							whereContion.append(" or delivery_order_id=").append(billIds[index]);
					}
				}
				return dbUtilAutoTran.selectMutliple("select * from " +tableName + whereContion.toString());
			}
		}catch (Exception e) {
			throw new Exception("FloorDeliveryMgrZJ getBillByBillId4Search error:"+e);
		}
	}
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
