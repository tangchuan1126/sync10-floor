package com.cwc.app.floor.api;

import java.util.HashMap;
import java.util.Map;

import com.cwc.app.beans.FieldInfo;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;

/**
 * 
 * @ClassName: FloorWmsXlsMap
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author yetl
 * @date 2015年4月23日
 *
 */
public class FloorWmsXlsMap {
	private DBUtilAutoTran_Optm dbUtilAutoTran;
	private String table_schema;
	
	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public void setTable_schema(String table_schema) {
		this.table_schema = table_schema;
	}
	
	/**
	 * 获取order_lines_tmp表字段与excel对应关系相关信息
	 * @return
	 * @throws Exception
	 */
	public Map<Integer,FieldInfo> getFieldInfo() throws Exception{
		Map<Integer,FieldInfo> map=new HashMap<Integer,FieldInfo>();
		
		String sql="select a.is_nullable as isnil,a.column_name as cl ,a.data_type as dt ,a.column_type as ct,a.CHARACTER_MAXIMUM_LENGTH as cml,a.NUMERIC_PRECISION as np,"+
				"a.NUMERIC_SCALE as ns,b.excel_sn as sn,b.excel_title as title,b.format as format,b.order_column_name as ocn from "+
				"information_schema.`COLUMNS` a INNER JOIN "+
				"wms_xls_map b on a.column_name=b.column_name  where "+
				 "a.TABLE_NAME='wms_order_lines_tmp' and b.table_name='wms_order_lines_tmp' and b.enable=1 and b.excel_sn is not null and a.table_schema='"+table_schema+"'";
		
		DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
		
		if(null!=rows){
			for(DBRow row:rows){
				FieldInfo fi=new FieldInfo();
				
				String cl=row.getString("cl"); //字段名字
				fi.setName(cl);
				fi.setType(row.getString("dt")); //字段类型
				fi.setIsNil(row.getString("isnil")); //是否可以为空
				
				Object cml=row.getValue("cml"); //字符类型的最大长度
				if(null!=cml){
					fi.setMaxLen(Integer.valueOf(cml.toString()));
				}
				
				Object np=row.getValue("np"); //数值最大位数
				if(null!=np){
					fi.setNumPs(Integer.valueOf(np.toString()));
				}
				
				Object ns=row.getValue("ns"); //数值小数点最大位数
				if(null!=ns){
					fi.setNumSc(Integer.valueOf(ns.toString()));
				}
				
				Object sn=row.getValue("sn"); //临时表字段对应数据库字段哪列
				int snInt=Integer.valueOf(sn.toString());
				fi.setExcelSn(snInt);
				
				fi.setXlsTitle(row.getString("title"));
				fi.setFormat(row.getString("format"));
				fi.setOrderColumnName(row.getString("ocn"));
				map.put(snInt, fi);
				
			}
			return map;
		}
		
		return null;
	}
	
	
	
}
