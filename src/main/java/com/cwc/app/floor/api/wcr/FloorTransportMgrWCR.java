package com.cwc.app.floor.api.wcr;

import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.key.TransportRoleKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorTransportMgrWCR {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	/**
	 * 过滤本库工作的运单
	 * 
	 * @param psid 仓库编号
	 * @param pc
	 * @param status
	 * @param declaration
	 * @param clearance
	 * @param invoice
	 * @param drawback
	 * @param day
	 * @param stock_in_set
	 * @param create_account_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterTransport(long psid, PageCtrl pc, int status, int declaration, int clearance, int invoice, int drawback, int day, int stock_in_set, long create_account_id) throws Exception {
		
		StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("transport")+" where 1=1");
		TDate tDate = new TDate();
		tDate.addDay(-day);
		//修改时间
		if(day!=0)
		{
			sql.append(" and updatedate<= '"+tDate.formatDate("yyyy-MM-dd")+"' ");
		}
		//我
		if(psid !=0)//转运仓库
		{
			sql.append(" and ( send_psid=").append(psid).append(" or receive_psid=").append(psid).append(")");
		}
		//状态
		if(status != 0 )
		{
			if(status==7)
			{
				sql.append(" and transport_status in("+TransportOrderKey.READY + "," + TransportOrderKey.INTRANSIT + "," + TransportOrderKey.APPROVEING + "," + TransportOrderKey.PACKING + ")");
			}
			else
			{
				sql.append(" and transport_status="+status);
			}	
		}
		//报关
		if(declaration!=0) {
			sql.append(" and IFNULL(declaration,1)="+declaration);
		}
		//清关
		if(clearance!=0) {
			sql.append(" and IFNULL(clearance,1)="+clearance);
		}
		//发票
		if(invoice!=0) {
			sql.append(" and IFNULL(invoice,1)="+invoice);
		}
		//退税
		if(drawback!=0) {
			sql.append(" and IFNULL(drawback,1)="+drawback);
		}
		//运费
		if(stock_in_set != 0) {
			sql.append(" and IFNULL(stock_in_set,1) ="+stock_in_set);
		}
		//创建人
		if(create_account_id != 0) {
			sql.append(" and create_account_id = " + create_account_id);
		}
		sql.append(" order by transport_date desc");
		
		return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
	}
	
	/**
	 * 过滤本库质量管理工作的运单
	 * 
	 * @param psid 仓库编号
	 * @param pc
	 * @param status
	 * @param declaration
	 * @param clearance
	 * @param invoice
	 * @param drawback
	 * @param day
	 * @param stock_in_set
	 * @param create_account_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterTransport(long psid, PageCtrl pc, int status, int declaration, int clearance, int invoice, int drawback, int day, int stock_in_set, long create_account_id, int qualityInspection, int productFile, int tag, int tagThird) throws Exception {
		
		StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("transport")+" where 1=1");
		TDate tDate = new TDate();
		tDate.addDay(-day);
		//修改时间
		if(day!=0)
		{
			sql.append(" and updatedate<= '"+tDate.formatDate("yyyy-MM-dd")+"' ");
		}
		//我
		if(psid !=0)//转运仓库
		{	
			//我负责的 需要处理或处理过的
			sql.append(" and ((send_psid=").append(psid).append(" and (");
				if(qualityInspection != 0) {
					sql.append("(IFNULL(quality_inspection_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_SEND).append(" and IFNULL(quality_inspection,0) in (1,2,3)) or ");
				} else {
					sql.append("(IFNULL(quality_inspection_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_SEND).append(" and IFNULL(quality_inspection,0) in (2)) or ");
					
				}
				if(productFile!=0) {
					sql.append("(IFNULL(product_file_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_SEND).append(" and IFNULL(product_file,0) in (1,2,3)) or ");
				} else {
					sql.append("(IFNULL(product_file_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_SEND).append(" and IFNULL(product_file,0) in (2)) or ");
				}
				if(tag!=0) {
					sql.append("(IFNULL(tag_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_SEND).append(" and IFNULL(tag,0) in (1,2,3,4)) or ");
				} else {
					sql.append("(IFNULL(tag_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_SEND).append(" and IFNULL(tag,0) in (2)) or ");
				}
				if(tagThird!=0) {
					sql.append("(IFNULL(tag_third_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_SEND).append(" and IFNULL(tag_third,0) in (1,2,3,4))");
				} else {
					sql.append("(IFNULL(tag_third_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_SEND).append(" and IFNULL(tag_third,0) in (2))");
				}
				sql.append(")) or (receive_psid=").append(psid).append(" and (");
				
				if(qualityInspection != 0) {
					sql.append("(IFNULL(quality_inspection_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_RECEIVE).append(" and IFNULL(quality_inspection,0) in (1,2,3)) or ");
				} else {
					sql.append("(IFNULL(quality_inspection_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_RECEIVE).append(" and IFNULL(quality_inspection,0) in (2)) or ");
					
				}
				if(productFile!=0) {
					sql.append("(IFNULL(product_file_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_RECEIVE).append(" and IFNULL(product_file,0) in (1,2,3)) or ");
				} else {
					sql.append("(IFNULL(product_file_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_RECEIVE).append(" and IFNULL(product_file,0) in (2)) or ");
				}
				if(tag!=0) {
					sql.append("(IFNULL(tag_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_RECEIVE).append(" and IFNULL(tag,0) in (1,2,3,4)) or ");
				} else {
					sql.append("(IFNULL(tag_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_RECEIVE).append(" and IFNULL(tag,0) in (2)) or ");
				}
				if(tagThird!=0) {
					sql.append("(IFNULL(tag_third_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_RECEIVE).append(" and IFNULL(tag_third,0) in (1,2,3,4))");
				} else {
					sql.append("(IFNULL(tag_third_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_RECEIVE).append(" and IFNULL(tag_third,0) in (2))");
				}
			sql.append(")))");
		}
		//状态
		if(status != 0 )
		{
			if(status==7)
			{
				sql.append(" and transport_status in("+TransportOrderKey.READY + "," + TransportOrderKey.INTRANSIT + "," + TransportOrderKey.APPROVEING + "," + TransportOrderKey.PACKING + ")");
			}
			else
			{
				sql.append(" and transport_status="+status);
			}	
		}
		//报关
		if(declaration!=0) {
			sql.append(" and IFNULL(declaration,1)="+declaration);
		}
		//清关
		if(clearance!=0) {
			sql.append(" and IFNULL(clearance,1)="+clearance);
		}
		//发票
		if(invoice!=0) {
			sql.append(" and IFNULL(invoice,1)="+invoice);
		}
		//退税
		if(drawback!=0) {
			sql.append(" and IFNULL(drawback,1)="+drawback);
		}
		//运费
		if(stock_in_set != 0) {
			sql.append(" and IFNULL(stock_in_set,1) ="+stock_in_set);
		}
		//创建人
		if(create_account_id != 0) {
			sql.append(" and create_account_id = " + create_account_id);
		}
		//质检
		if(qualityInspection != 0) {
			sql.append(" and IFNULL(quality_inspection,1) = " + qualityInspection);
		}
		//实物图片
		if(productFile!=0) {
			sql.append(" and IFNULL(product_file,1)="+productFile);
		}
		//内部标签
		if(tag!=0) {
			sql.append(" and IFNULL(tag,1)="+tag);
		}
		//第三方标签
		if(tagThird!=0) {
			sql.append(" and IFNULL(tag_third,1)="+tagThird);
		}
		
		sql.append(" order by transport_date desc");
		
		return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
	}
	
	
	/**
	 * 需要做的本库质量管理工作的运单
	 * 
	 * @param psid
	 * @param pc
	 * @param status
	 * @param declaration
	 * @param clearance
	 * @param invoice
	 * @param drawback
	 * @param day
	 * @param stock_in_set
	 * @param create_account_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDefaultTransport(long psid,PageCtrl pc ,int status, int declaration, int clearance,int invoice, int drawback, int day,int stock_in_set, long create_account_id, int qualityInspection, int productFile, int tag, int tagThird) throws Exception {
		StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("transport")+" where 1=1");
		TDate tDate = new TDate();
		tDate.addDay(-day);
		//修改时间
		if(day!=0)
		{
			sql.append(" and updatedate<= '"+tDate.formatDate("yyyy-MM-dd")+"' ");
		}
		//我
		if(psid !=0)//转运仓库
		{	
			//我负责的 需要处理或处理过的
			sql.append(" and ((send_psid=").append(psid).append(" and (")
					.append("(IFNULL(quality_inspection_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_SEND).append(" and IFNULL(quality_inspection,0)=2) or ")
					.append("(IFNULL(product_file_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_SEND).append(" and IFNULL(product_file,0)=2) or ")
					.append("(IFNULL(tag_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_SEND).append(" and IFNULL(tag,0)=2) or ")
					.append("(IFNULL(tag_third_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_SEND).append(" and IFNULL(tag_third,0)=2)")
				.append(")) or (receive_psid=").append(psid).append(" and (")
					.append("(IFNULL(quality_inspection_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_RECEIVE).append(" and IFNULL(quality_inspection,0)=2) or ")
					.append("(IFNULL(product_file_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_RECEIVE).append(" and IFNULL(product_file,0)=2) or ")
					.append("(IFNULL(tag_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_RECEIVE).append(" and IFNULL(tag,0)=2) or ")
					.append("(IFNULL(tag_third_responsible,0)=").append(TransportRoleKey.TRANSPORT_ROLE_RECEIVE).append(" and IFNULL(tag_third,0)=2)")
			.append(")))");
		}
		//状态
		if(status != 0 )
		{
			if(status==7)
			{
				sql.append(" and transport_status in("+TransportOrderKey.READY + "," + TransportOrderKey.INTRANSIT + "," + TransportOrderKey.APPROVEING + "," + TransportOrderKey.PACKING + ")");
			}
			else
			{
				sql.append(" and transport_status="+status);
			}	
		}
		//报关
		if(declaration!=0) {
			sql.append(" and IFNULL(declaration,1)="+declaration);
		}
		//清关
		if(clearance!=0) {
			sql.append(" and IFNULL(clearance,1)="+clearance);
		}
		//发票
		if(invoice!=0) {
			sql.append(" and IFNULL(invoice,1)="+invoice);
		}
		//退税
		if(drawback!=0) {
			sql.append(" and IFNULL(drawback,1)="+drawback);
		}
		//运费
		if(stock_in_set != 0) {
			sql.append(" and IFNULL(stock_in_set,1) ="+stock_in_set);
		}
		//创建人
		if(create_account_id != 0) {
			sql.append(" and create_account_id = " + create_account_id);
		}
		//质检
		if(qualityInspection != 0) {
			sql.append(" and IFNULL(quality_inspection,1) = " + qualityInspection);
		}
		//实物图片
		if(productFile!=0) {
			sql.append(" and IFNULL(product_file,1)="+productFile);
		}
		//内部标签
		if(tag!=0) {
			sql.append(" and IFNULL(tag,1)="+tag);
		}
		//第三方标签
		if(tagThird!=0) {
			sql.append(" and IFNULL(tag_third,1)="+tagThird);
		}
		sql.append(" order by transport_date desc");
		
		return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
	}

	public DBRow getTransportForDefault(long psid, long title_id, long order_id) throws Exception {
		DBRow rt = null;
		StringBuffer sql = new StringBuffer("select * from (");
		//获取订单相似的运单
		//TODO 
		sql.append("SELECT c.* FROM b2b_order_item b, transport c, transport_detail d WHERE 1=1 AND d.transport_id = c.transport_id AND b.b2b_detail_id=d.b2b_detail_id and b.b2b_oid =").append(order_id);
		sql.append(") t ORDER BY t.transport_id desc LIMIT 0,1");
		rt = dbUtilAutoTran.selectSingle(sql.toString());
		
		if (rt==null || 0L== rt.get("transport_id", 0L)) {
			//获取发给客户的相似运单
			sql = new StringBuffer("select * from "+ConfigBean.getStringValue("transport")+" where 1=1");
			sql.append(" and receive_psid =").append(psid);
			sql.append(" and title_id =").append(title_id);
			sql.append(" and IFNULL(is_relate_purchase,2) =2");
			sql.append(" ORDER BY transport_id desc LIMIT 0,1");
			rt = dbUtilAutoTran.selectSingle(sql.toString());
		}
		return rt;
	}
	
	/**
	 * 查询b2b订单相关运单
	 * 
	 * @param order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportFormOrder(long order_id) throws Exception {
		StringBuffer sql = new StringBuffer("SELECT c.* FROM b2b_order_item b, transport c, transport_detail d WHERE d.transport_id = c.transport_id AND b.b2b_detail_id=d.b2b_detail_id and b.b2b_oid = ? order by c.transport_id");
		DBRow para = new DBRow();
		para.put("b2b_oid", order_id);
		return dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
	}

	/**
	 * 发货时过滤转运单。
	 * 状态指定失效,仅显示(备货，运输，装箱)
	 * 
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param status
	 * @param declaration
	 * @param clearance
	 * @param invoice
	 * @param drawback
	 * @param day
	 * @param stock_in_set
	 * @param create_account_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterTransportDefault(long send_psid, long receive_psid, PageCtrl pc, int status, int declaration, int clearance, int invoice, int drawback, int day, int stock_in_set, long create_account_id) throws Exception {
		StringBuffer sql = new StringBuffer("select * from " + ConfigBean.getStringValue("transport") + " where 1=1");
		TDate tDate = new TDate();
		tDate.addDay(-day);
		if (day != 0) {
			sql.append(" and updatedate<= '" + tDate.formatDate("yyyy-MM-dd") + "' ");
		}
		if (send_psid != 0)// 转运仓库
		{
			sql.append(" and send_psid=" + send_psid);
		}
		if (receive_psid != 0) {
			sql.append(" and receive_psid=" + receive_psid);
		}

		sql.append(" and transport_status in(" + TransportOrderKey.READY + "," + TransportOrderKey.INTRANSIT + "," + TransportOrderKey.PACKING + ")");

		if (declaration != 0) {
			sql.append(" and IFNULL(declaration,1)=" + declaration);
		}
		if (clearance != 0) {
			sql.append(" and IFNULL(clearance,1)=" + clearance);
		}
		if (invoice != 0) {
			sql.append(" and IFNULL(invoice,1)=" + invoice);
		}
		if (drawback != 0) {
			sql.append(" and IFNULL(drawback,1)=" + drawback);
		}
		if (stock_in_set != 0) {
			sql.append(" and IFNULL(stock_in_set,1) =" + stock_in_set);
		}
		if (create_account_id != 0) {
			sql.append(" and create_account_id = " + create_account_id);
		}
		sql.append(" order by transport_date desc");

		return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
	}
}