package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

/**
 * android 控制权限
 * @author zhangrui
 */
public class FloorAndroidControlMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 添加角色的权限
	 * @param inserRow
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月28日
	 */
	public long addRolePermission(DBRow inserRow) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("android_control_adgid");
			return dbUtilAutoTran.insertReturnId(tableName, inserRow);
		}catch(Exception e){
			throw new Exception("FloorAndroidControlMgrZr.addRolePermission(inserRow):"+e);
		}
	}
	/**
	 * 添加某个人的特殊权限
	 * @param inserRow
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月28日
	 */
	public long addAdidPermission(DBRow inserRow) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("android_control_adid");
			return dbUtilAutoTran.insertReturnId(tableName, inserRow);
		}catch(Exception e){
			throw new Exception("FloorAndroidControlMgrZr.addAdidPermission(inserRow):"+e);
		}
	}
	/**
	 * 得到某个adgid所有的权限
	 * @param adgid
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月28日
	 */
	public DBRow[] getAdgidPermission(long adgid) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("android_control_adgid") + " where adgid="+adgid ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorAndroidControlMgrZr.getAdgidPermission(adid):"+e);
		}
	}
	/**
	 * 	得到当前人personal 权限
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年9月28日
	 */
	public DBRow[] getAdidPermission(long adid) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("android_control_adid") + " where adid="+adid;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorAndroidControlMgrZr.getAdidPermission(adid):"+e);
		}
	}
	
	
	public DBRow[] getAllPermission() throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("android_control_tree") + " order by parent_id " ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorAndroidControlMgrZr.getAllPermission():"+e);
		}
	}
	
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
