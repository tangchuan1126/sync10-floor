package com.cwc.app.floor.api.ly;

import org.apache.log4j.Logger;

import com.cwc.app.key.ClearanceKey;
import com.cwc.app.key.DeclarationKey;
import com.cwc.app.key.TransportCertificateKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.key.TransportRoleKey;
import com.cwc.app.key.TransportStockInSetKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorTransportMgrLY 
{
	static Logger log = Logger.getLogger("ACTION");
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 收货管理 默认查询( 查询到达本库，并且运输状态为 运输中 或已到货的)
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param status
	 * @param declaration
	 * @param clearance
	 * @param invoice
	 * @param drawback
	 * @param day
	 * @param stock_in_set
	 * @param create_account_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterTransport(long send_psid, long receive_psid,
			PageCtrl pc, int status, int declaration, int clearance,
			int invoice, int drawback, int day, int stock_in_set,
			long create_account_id) throws Exception
	{
		StringBuffer sql = new StringBuffer("select * from "
				+ ConfigBean.getStringValue("transport") + " where 1=1");
		TDate tDate = new TDate();
		tDate.addDay(-day);
		if (day != 0)
		{
			sql.append(" and updatedate<= '" + tDate.formatDate("yyyy-MM-dd")
					+ "' ");
		}

		if (send_psid != 0L)
		{
			sql.append(" and send_psid=" + send_psid);
		}

		if (receive_psid != 0L)
		{
			sql.append(" and receive_psid=" + receive_psid);
		}

		if (status != 0)
		{
				sql.append(" and transport_status=" + status);
		}
		else 
		{
			//默认查询 这几个状态的数据
			sql.append(" and (transport_status in("
					+ TransportOrderKey.AlREADYARRIAL + ","
					+ TransportOrderKey.INTRANSIT + ","
					+ TransportOrderKey.RECEIVEING + ") or (transport_status="+TransportOrderKey.READY+" and IFNULL(purchase_id,0)!=0) )  ");
		}
		
		if (declaration != 0)
		{
			sql.append(" and IFNULL(declaration,1)=" + declaration);
		}
		if (clearance != 0)
		{
			sql.append(" and IFNULL(clearance,1)=" + clearance);
		}
		if (invoice != 0)
		{
			sql.append(" and IFNULL(invoice,1)=" + invoice);
		}
		if (drawback != 0)
		{
			sql.append(" and IFNULL(drawback,1)=" + drawback);
		}
		if (stock_in_set != 0)
		{
			sql.append(" and IFNULL(stock_in_set,1) =" + stock_in_set);
		}
		if (create_account_id != 0L)
		{
			sql.append(" and create_account_id = " + create_account_id);
		}
		sql.append(" order by transport_date desc");

		return this.dbUtilAutoTran.selectMutliple(sql.toString(), pc);
	}
	
	
	/**
	 * 过滤转运单[本库工作-运输管理]
	 * @param self_psid 	登录员工所在仓库id
	 * @param pc			分页对象
	 * @param status		运输状态
	 * @param declaration	报关状态
	 * @param clearance		清关状态
	 * @param invoice		发票
	 * @param drawback		退税
	 * @param day			更新时间
	 * @param stock_in_set	运费流程状态
	 * @param create_account_id	创建人id
	 * @return				转运单
	 * @throws Exception
	 */
	public DBRow[] fillterTransportSelf(long self_psid,PageCtrl pc,int status, int declaration, int clearance,int invoice, int drawback, int day,int stock_in_set, long create_account_id)
		throws Exception
	{
		StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("transport")+" where 1=1");
		TDate tDate = new TDate();
		tDate.addDay(-day);
		if(day != 0)
		{
			sql.append(" and updatedate<= '"+tDate.formatDate("yyyy-MM-dd")+"' ");
		}
		
		sql.append(" AND (");
		appendDefaultSqlString(sql,self_psid,TransportRoleKey.TRANSPORT_ROLE_SEND);
		sql.append(" OR ");
	    appendDefaultSqlString(sql, self_psid, TransportRoleKey.TRANSPORT_ROLE_RECEIVE);
	    sql.append("   )");
		
		
		//运输状态 -1 是查询正在运输中的，备货中，装箱中的数据
		//1：备货中  2：运输中  3：审核中   4：已入库   5：装货中  8：已到货 
		if(status != 0 )
		{
			if(status==7)
			{
				sql.append(" and transport_status in("+TransportOrderKey.READY + "," + TransportOrderKey.INTRANSIT + "," + TransportOrderKey.APPROVEING + "," + TransportOrderKey.PACKING + ")");
			}
			else
			{
				sql.append(" and transport_status="+status);
			}	
		}
		
		//报关
		if(declaration != 0 )
		{
			sql.append(" and IFNULL(declaration,1)="+declaration);
		}
		
		//清关
		if(clearance != 0 )
		{
			sql.append(" and IFNULL(clearance,1)="+clearance);
		}
		
		if(invoice!=0)
		{
			sql.append(" and IFNULL(invoice,1)="+invoice);
		}
		
		if(drawback!=0) 
		{
				sql.append(" and IFNULL(drawback,1)="+drawback);
		}
		
		//运费
		if(stock_in_set != 0 )
		{
			sql.append(" and IFNULL(stock_in_set,1) ="+stock_in_set);
		}
		
		//创建人
		if(create_account_id != 0) 
		{
			sql.append(" and create_account_id = " + create_account_id);
		}
		
		sql.append(" order by transport_date desc");
		
		return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
	}

	
	/**
	 * 查询运输管理默认需要处理的单据
	 * @param self_psid 	登录员工所在仓库id
	 * @param pc			分页对象
	 * @param status 		暂时无用
	 * @param declaration	暂时无用
	 * @param clearance 	暂时无用
	 * @param invoice 		暂时无用
	 * @param drawback		暂时无用
	 * @param day 			最大更新时间
	 * @param stock_in_set  暂时无用
	 * @param create_account_id 
	 * @return 				暂时无用
	 * @throws Exception 	暂时无用
	 */
	public DBRow[] getSelfDefaultTransport(long self_psid,PageCtrl pc ,int status, int declaration, int clearance,int invoice, int drawback, int day,int stock_in_set, long create_account_id)
			throws Exception
	{
		StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("transport")+" where 1=1 ");
		TDate tDate = new TDate();
		tDate.addDay(-day);
		if(day != 0)
		{
			sql.append(" and updatedate<= '"+tDate.formatDate("yyyy-MM-dd")+"' ");
		}
		
//		//发自本库或者到货至本库的运单
//		if(self_psid != 0)
//		{
//			sql.append(" and ( send_psid=").append(self_psid)
//			.append(" or receive_psid =").append(self_psid)
//			.append(") ");
//		}
		
		//运输状态 -1 是查询正在运输中的，备货中，装箱中的数据
		//1：备货中  2：运输中  3：审核中   4：已入库   5：装货中  8：已到货 
		
		//查询  备货中，正常运算，装货中的数据 1 2 5
		//sql.append(" and transport_status in("+TransportOrderKey.READY + "," + TransportOrderKey.INTRANSIT + "," + TransportOrderKey.APPROVEING + "," + TransportOrderKey.PACKING + ")");
		
		sql.append(" AND (");
		appendDefaultSqlString(sql,self_psid,TransportRoleKey.TRANSPORT_ROLE_SEND);
		sql.append(" OR ");
	    appendDefaultSqlString(sql, self_psid, TransportRoleKey.TRANSPORT_ROLE_RECEIVE);
	    sql.append("   )");
	    sql.append(" order by transport_date desc");
		
		return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
	}
	
	
	private StringBuffer appendDefaultSqlString(StringBuffer sb , long self_psid , int responsible)
	{
		if(responsible == 1)
		{
			sb.append(" ( send_psid=").append(self_psid);
		}
		else
		{	
			sb.append(" ( receive_psid=").append(self_psid);
		}
		
		sb.append(" and (")
		  .append("          (declaration_responsible=").append(responsible).append(" and ");
		   appendDefaultDeclarationSql(sb).append(")");
	    sb.append("       or (clearance_responsible=").append(responsible).append(" and ");
	       appendDefaultClearanceSql(sb).append(")");
		sb.append("       or (certificate_responsible=").append(responsible).append(" and ");
		  appendDefaultCertificateSql(sb).append(")");
	    sb.append("       or (stock_in_set_responsible=").append(responsible).append(" and ");
	       appendDefaultStockInSetSql(sb).append(")");
	    sb.append("   )");
	    sb.append(" )");
		return sb;
	}
	
	/**
	 * 报关 拼接默认SQL
	 * @param sb
	 * @return
	 */
	private StringBuffer appendDefaultDeclarationSql(StringBuffer sb)
	{
		//需要报关(declaration :2 和3)
		return	sb.append(" declaration in (")
			.append(DeclarationKey.DELARATION)
			.append(",").append(DeclarationKey.DELARATING)
			.append(")");
	}
	
	/**
	 *  清关 拼接默认SQL
	 * @param sb
	 * @return
	 */
	private StringBuffer appendDefaultClearanceSql(StringBuffer sb)
	{
		//需要清关 2：需要清关  3：清关中  5：查货中
		return	sb.append("  clearance in (")
		  .append(ClearanceKey.CLEARANCE)
		  .append(",").append(ClearanceKey.CLEARANCEING)
		  .append(",").append(ClearanceKey.CHECKCARGO)
		  .append(")");
	}
	
	/**
	 * 单证 拼接默认SQL
	 * @param sb
	 * @return
	 */
	private StringBuffer appendDefaultCertificateSql(StringBuffer sb)
	{
		//需要单证： 2
		return	sb.append("  IFNULL(certificate,1) =")
		  .append(TransportCertificateKey.CERTIFICATE);
	}
	
	/**
	 * 运费 拼接默认SQL
	 * @param sb
	 * @return
	 */
	private StringBuffer appendDefaultStockInSetSql(StringBuffer sb)
	{
		//需要运费：2
		return	sb.append("  IFNULL(stock_in_set,1) =")
			.append(TransportStockInSetKey.SHIPPINGFEE_SET);
	}
	
	
	
	
	
	
	public DBUtilAutoTran getDbUtilAutoTran() 
	{
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) 
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	
}
