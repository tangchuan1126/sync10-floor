package com.cwc.app.floor.api.zj;


import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorPurchaseApproveMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 添加采购单审核
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addPuchaseApprove(DBRow dbrow)
		throws Exception
	{
		try
		{
			long pa_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("purchase_approve"));
			
			dbrow.add("pa_id",pa_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("purchase_approve"),dbrow);
			
			return (pa_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseApproveMgrZJ addPuchaseApprove error:"+e);
		}
	}
	
	/**
	 * 添加采购单审核具体差异
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addPurchaseApproveDifference(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long pdd_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("purchase_detail_difference"));
			
			dbrow.add("pdd_id",pdd_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("purchase_detail_difference"),dbrow);
			
			return (pdd_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseApproveMgrZJ addPurchaseApproveDifference error:"+e);
		}
	}
	
	/**
	 * 修改采购单审核
	 * @param pa_id
	 * @param dbrow
	 * @throws Exception
	 */
	public void modPuchaseApprove(long pa_id,DBRow dbrow)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where pa_id ="+pa_id,ConfigBean.getStringValue("purchase_approve"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseApproveMgrZJ modPuchaseApprove error:"+e);
		}
	}
	
	/**
	 * 修改采购单审核具体差异
	 * @param pdd_id
	 * @param dbrow
	 * @throws Exception
	 */
	public void modPurchaseApproveDifference(long pdd_id,DBRow dbrow)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where pdd_id ="+pdd_id,ConfigBean.getStringValue("purchase_detail_difference"),dbrow);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseApproveMgrZJ modPurchaseApproveDifference error:"+e);
		}
	}
	
	/**
	 * 获得全部采购单审核，支持按仓库查找
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] allPurchaseApprove(long ps_id,PageCtrl pc,int approve_status,String sorttype)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select pa.*,ps.title as psc from "+ConfigBean.getStringValue("purchase_approve")+" as pa,"+ConfigBean.getStringValue("purchase")+" as pu,"+ConfigBean.getStringValue("product_storage_catalog")+" as ps where pa.purchase_id = pu.purchase_id and ps.id = pu.ps_id");
			if(ps_id>0)
			{
				sql.append(" and pu.ps_id = "+ps_id);
			}
			
			if(!(approve_status<0))
			{
				sql.append(" and pa.approve_status = "+approve_status);
			}
			
			if(sorttype.equals("subdate"))
			{
				sql.append(" order by pa.commit_date desc");
			}
			else if(sorttype.equals("approvedate"))
			{
				sql.append(" order by pa.approve_date desc");
			}
			else
			{
				sql.append(" order by pa.pa_id desc");
			}
			
			return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseApproveMgrZJ allPurchaseApprove error:"+e);
		}
	}
	
	/**
	 * 根据采购单审核单ID获得采购单需审核差异详细
	 * @param pa_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseApproveDifferenceByPaid(long pa_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("purchase_detail_difference")+" where pa_id = ?";
			DBRow para = new DBRow();
			para.add("pa_id",pa_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para, pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseApproveMgrZJ getPurchaseApproveDifferenceByPaid error:"+e);
		}
	}
	
	/**
	 * 根据审核状态查询采购单差异
	 * @param pa_id
	 * @param approve_status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseApproveDifferentsWithStatus(long pa_id,int approve_status)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("purchase_detail_difference")+" where pa_id = ? and approve_status = ?";
		DBRow para = new DBRow();
		para.add("pa_id",pa_id);
		para.add("approve_status",approve_status);
		
		return (dbUtilAutoTran.selectPreMutliple(sql, para));
	}
	
	/**
	 * 根据审核单ID获得审核单详细
	 * @param pa_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailPurchaseApproveByPaid(long pa_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("purchase_approve")+" where pa_id = ?";
		DBRow para = new DBRow();
		para.add("pa_id",pa_id);
		
		return (dbUtilAutoTran.selectPreSingle(sql, para));
	}
	
	/**
	 * 根据采购单ID获得审核单详细
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailPurchaseApproveByPurchaseid(long purchase_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("purchase_approve")+" where purchase_id = ?";
		DBRow para = new DBRow();
		para.add("purchase_id",purchase_id);
		
		return (dbUtilAutoTran.selectPreSingle(sql, para));
	}
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
