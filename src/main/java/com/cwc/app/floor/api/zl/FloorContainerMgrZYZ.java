package com.cwc.app.floor.api.zl;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorContainerMgrZYZ {
	
	private DBUtilAutoTran dbUtilAutoTran;

	
	/**
	 * 查询所有容器托盘
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllContainer(PageCtrl pc) throws Exception{
		
		try {
			String sql = "select con.*,ct.type_name , ct.container_type from "+ConfigBean.getStringValue("container")+" as con"+
			" left join "+ConfigBean.getStringValue("container_type")+" as ct on con.type_id = ct.type_id order by con.con_id desc";
			if(pc != null)
				return dbUtilAutoTran.selectMutliple(sql,pc);
			else
				return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorContainerMgrZYZ getAllContainer error:" +e);
		}
	}
	/**
	 * 查询一个容器的详细信息
	 * @param containerId
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailContainer(long containerId) throws Exception{
		
			try {
				String sql = "select * from "+ConfigBean.getStringValue("container")+" where con_id ="+containerId;
				return dbUtilAutoTran.selectSingle(sql);
			} catch (Exception e) {
				
				throw new Exception("floorContainerMgrZYZ getDetailContainer error:" +e);
			}
	}
	/**
	 * 高级查询
	 * @param key,type,pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchContainer(String key , long type ,int container_type, PageCtrl pc)throws Exception{
		
			try {
				String sql = "select con.*,ct.type_name ,ct.container_type from "+ConfigBean.getStringValue("container")+" as con"+
				" join "+ConfigBean.getStringValue("container_type")+" as ct on con.type_id = ct.type_id  where (con.container like '"+key+"%' or con.hardwareId like '"+key+"%')";
				
				if(type!=0){
					sql +=" and con.type_id = "+type;
				}
				if(container_type != 0){
					sql += " and ct.container_type = "+container_type;
				}
				sql += " order by con_id desc";
				return dbUtilAutoTran.selectMutliple(sql, pc);
				
			} catch (Exception e) {
				throw new Exception("floorContainerMgrZYZ getSearchContainer error:" +e);
			}
			
	}
	/**
	 * 添加容器信息
	 * @param drow
	 * @return
	 * @throws Exception
	 */
	public long addContainer(DBRow drow)throws Exception{
		
		try {
			try {
//				long conId = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("container"));
//				 drow.add("con_id", conId);
				return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("container"), drow);
			} catch (Exception e) {
				throw new Exception("addAdminGRL(drow) error:" + e);
			}
			
		} catch (Exception e) {
			throw new Exception("floorContainerMgrZYZ getDetailContainer error:" +e);
		}
	}
	/**
	 * 修改容器信息
	 */
	public void modContainer(long containerId,DBRow containerRow)throws Exception{
		
		try {
			dbUtilAutoTran.update("where con_id="+containerId, ConfigBean.getStringValue("container"), containerRow);
			
		} catch (Exception e) {
			throw new Exception("floorContainerMgrZYZ getDetailContainer error:" +e);
		}
	}
	/**
	 * 删除容器托盘信息
	 * @param containerId
	 * @return
	 * @throws Exception
	 */
	public void deleteContainer(long containerId)throws Exception{
		
		try {
			dbUtilAutoTran.delete("where con_id="+containerId, ConfigBean.getStringValue("container"));
			
		} catch (Exception e) {
			throw new Exception("floorContainerMgrZYZ deleteContainer error:" +e);
		}
	}
	
	/**
	 * 查询所有容器托盘类型
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllContainerType(PageCtrl pc, String searchKey) throws Exception {
		
		try {
			
			String sql = "select * from container_type where 1=1 ";
			
			if(searchKey != null && !searchKey.equals("")){
				
				sql += " and type_name like '%"+searchKey.trim()+"%' ";
			}
			
			sql += " order by type_id desc ";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
			
		} catch (Exception e) {
			
			throw new Exception("FloorContainerMgrZYZ getAllContainerType error:" +e);
		}
	}
	
	public DBRow[] getClpContainerType() throws Exception {
		
		try {
			
			String sql = "SELECT * from container_type ct where EXISTS (select * from container con join license_plate_type lpt on con.type_id = lpt.lpt_id where con.container_type = '1' and lpt.basic_type_id = ct.type_id ) ORDER BY ct.type_id desc";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			
			throw new Exception("FloorContainerMgrZYZ.getClpContainerType error:" +e);
		}
	}
	
	public DBRow[] getTlpContainerType() throws Exception {
		
		try {
			
			String sql = "select * from container_type ct where EXISTS ( select * from container con where con.container_type = '3' and con.type_id = ct.type_id ) ORDER BY ct.type_id desc";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			
			throw new Exception("FloorContainerMgrZYZ.getTlpContainerType error:" +e);
		}
	}
	
	/**
	 * 查询单个容器托盘类型
	 * @param typeId
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailContainerType(long typeId) throws Exception{
		
		try {
			String sql = "select * from "+ConfigBean.getStringValue("container_type")+" where type_id ="+typeId;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			
			throw new Exception("floorContainerMgrZYZ getDetailContainerType error:" +e);
		}
	}
	
	/**
	 * 修改容器托盘类型
	 */
	public void modContainerType(long typeId, DBRow rows) throws Exception{
		
			try {
				dbUtilAutoTran.update("where type_id="+typeId, ConfigBean.getStringValue("container_type"), rows);
			} catch (Exception e) {
				
				throw new Exception("floorContainerMgrZYZ modContainerType error:" +e);
			}
	}
	
	/**
	 * 增加容器托盘类型
	 */
	public long addContainerType(DBRow addRow)throws Exception{
			
			try {
//				long typeId = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("container_type"));
//				addRow.add("type_id", typeId);
				long typeId = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("container_type"), addRow);
				return (typeId);
				
			} catch (Exception e) {
				throw new Exception("floorContainerMgrZYZ addContainerType error:" +e);
			}
	}
	
	/**
	 * 删除容器托盘类型
	 */
	public long deleteContainerType(long typeId, int container_type)throws Exception{
			
			try {
			     return dbUtilAutoTran.delete("where type_id ="+typeId, ConfigBean.getStringValue("container_type"));
			} catch (Exception e) {
				throw new Exception("floorContainerMgrZYZ deleteContainerType error:" +e);
			}
	}
	/**
	 * 根据类型查询容器信息
	 */
	public DBRow[] getTlpContainerByType(long typeId, int container_type)throws Exception{
			
			try {
				String sql="select * from "+ConfigBean.getStringValue("container")+" where type_id="+typeId;
				if(container_type > 0)
				{
					sql += " and container_type="+container_type;
				}
//				System.out.println(sql);
				return dbUtilAutoTran.selectMutliple(sql);
				
			} catch (Exception e) {
				throw new Exception("floorContainerMgrZYZ getContainerByType error:" +e);
			}
	}
	
	/**
	 * 根据类型名查类型信息
	 */
	public DBRow getContainerTypeIdByName(String typeName)throws Exception{
		
			try 
			{
				String sql = "select * from "+ConfigBean.getStringValue("container_type")+" where type_name='"+typeName+"'";
				return dbUtilAutoTran.selectSingle(sql);
				
			} catch (Exception e) {
				throw new Exception("floorContainerMgrZYZ getContainerTypeIdByName error:" +e);
			}
	}
	
	/**
	 * 导出容器
	 */
	public DBRow[] exportContainer(String search_key,long type_id)throws Exception{
			
		try {
			String sql = "select con.*,ct.type_name from "+ConfigBean.getStringValue("container")+" as con"+
						 " join "+ConfigBean.getStringValue("container_type")+" as ct on con.type_id = ct.type_id where 0=0";
			if(search_key != null){
				
				sql += " and (con.container like '"+search_key+"%' or con.hardwareId like '"+search_key+"%')";
			}
			if(type_id !=0){
				
				sql +=" and con.type_id ="+type_id;
			}
			return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			throw new Exception("floorContainerMgrZYZ exportContainer error:" +e);
		}
	}
	
	/**
	 * 通过容器分类查询容器基础容器类型
	 * @param containerType
	 * @return
	 * @throws Exception
	 * zyj
	 */
	public DBRow[] findContainerTypeByContainerType(int containerType) throws Exception
	{
		try
		{
			String sql = "select * from container_type where container_type = " + containerType;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) {
			throw new Exception("floorContainerMgrZYZ findContainerTypeByContainerType error:" +e);
		}
	}
	
	/**
	 * 根据类型查询容器信息
	 */
	public DBRow[] getClpContainerByType(long typeId, int container_type)throws Exception{
			
			try {
				String sql="select * from "+ConfigBean.getStringValue("container")+" where type_id="+typeId;
				if(container_type > 0)
				{
					sql += " and container_type="+container_type;
				}
				return dbUtilAutoTran.selectMutliple(sql);
				
			} catch (Exception e) {
				throw new Exception("floorContainerMgrZYZ getContainerByType error:" +e);
			}
	}
	
	/**zyj
	 * 通过typeid查询是否有此基础容器类型的ILP
	 * @param typeId
	 * @param container_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getIlpContainerByType(long typeId)throws Exception{
			try {
				String sql="select * from inner_box_type where ibt_lp_type = " + typeId;
				return dbUtilAutoTran.selectMutliple(sql);
				
			} catch (Exception e) {
				throw new Exception("floorContainerMgrZYZ getIlpContainerByType error:" +e);
			}
	}
	
	/**zyj
	 * 通过typeid查询是否有此基础容器类型的BLP
	 * @param typeId
	 * @param container_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBlpContainerByType(long typeId)throws Exception{
			try {
				String sql="select * from box_type where container_type_id = " + typeId;
				return dbUtilAutoTran.selectMutliple(sql);
				
			} catch (Exception e) {
				throw new Exception("floorContainerMgrZYZ getBlpContainerByType error:" +e);
			}
	}
	
	
	/**zyj
	 * 通过typeid查询是否有此基础容器类型的BLP
	 * @param typeId
	 * @param container_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getClpContainerByType(long typeId)throws Exception{
			try {
				String sql="select * from license_plate_type where basic_type_id = " + typeId;
				return dbUtilAutoTran.selectMutliple(sql);
				
			} catch (Exception e) {
				throw new Exception("floorContainerMgrZYZ getClpContainerByType error:" +e);
			}
	}
	
	
	
	 /*
     * container_type导出excel
     * Yuanxinyu
     * */
    public DBRow[] exportContainerByType(String search_key)
            throws Exception
    {
        try
        {
            String sql = (new StringBuilder()).append("select * from ").append(ConfigBean.getStringValue("container_type")).append(" where 1=1").toString();
            if(!"".equals(search_key) && !"-1".equals(search_key)  && !"0".equals(search_key))
                sql = (new StringBuilder()).append(sql).append(" and container_type = ").append(search_key).toString();
            return dbUtilAutoTran.selectMutliple(sql);
        }
        catch(Exception e)
        {
            throw new Exception((new StringBuilder()).append("floorContainerMgrZYZ exportContainerByType error:").append(e).toString());
        }
    }
	
    /**
     * 查询packaging type
     * 
     *@author subin 
    */
    public DBRow[] getContainerTypeForExcel() throws Exception {
		
		try {
			
			String sql = "select type_name name from container_type order by type_name ";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			
			throw new Exception("FloorContainerMgrZYZ.getContainerTypeForExcel() error:" +e);
		}
	}
    
    /**
     * 查询customer
     * 
     * @author subin
    */
    public DBRow getCustomerById(long id) throws Exception {
		
		try {
			
			String sql = "select * from customer_id where customer_key = "+id;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		} catch (Exception e) {
			
			throw new Exception("FloorContainerMgrZYZ.getCustomerById(long id) error:" +e);
		}
	}
    
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}

