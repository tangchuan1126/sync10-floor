package com.cwc.app.floor.api.zwb;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorQuestionCatalogMgrZwb {

	private DBUtilAutoTran dbUtilAutoTran;
	
	//查询当前用户下是否为该问题添加了问题提醒
	public DBRow getQuestionCallByOne(long user_id,long question_id)throws Exception{
		try{ 
			String sql="select * from "+ConfigBean.getStringValue("question_call")+" where user_id="+user_id+" and question_id="+question_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionCallByOne" +e);
		}
	}
	
	//删除问题时 取消所有用户对该问题的提醒
	public long detQuestionCallAllByQuestionId(long questionId)throws Exception{
		try{
			return this.dbUtilAutoTran.delete("where question_id="+questionId,ConfigBean.getStringValue("question_call"));
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb detQuestionCallAllByQuestionId"+e);
		}
	}
	
	//标记为已经阅读
	public long detQuestionCallById(long userId,long questionId)throws Exception{
		try{
			return this.dbUtilAutoTran.delete("where user_id="+userId+" and question_id="+questionId,ConfigBean.getStringValue("question_call"));
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb detQuestionCallById"+e);
		}
	}
	
	//查询某用户下的问题提醒详细
	public DBRow[] getAllQuestionCallById(long userId)throws Exception{
		try{
			String sql="select * from "+ConfigBean.getStringValue("question_call")+" where user_id="+userId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getAllQuestionCallById"+e);
		}
	}
	
	//查询是否有问题需要提示
	public DBRow[] getQuestionCallByUserId(long userId)throws Exception{
		try{
			String sql="select *,count(product_line_id) count from "+ConfigBean.getStringValue("question_call")+" where user_id="+userId+" GROUP BY product_line_id";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionCallByUserId"+e);
		}
	}
	
	
	//向问题提醒表添加数据
	public long addQuestionCall(DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("question_call"), row);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb addQuestionCall"+e);
		}
	}
	
	//查询需要提醒用户组
	public DBRow[] getQuestionCallUserList()throws Exception{
		try{
			String sql="select * from admin where "+ConfigBean.getStringValue("question_condition");
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionCallUserList"+e);
		}
	}
	
	
	
	//查询某条产品线下为回答问题详细
	public DBRow[] getQuestionproductLineStatus(String ids,PageCtrl pc)throws Exception{
		try{
			String sql="select *  from question_q where question_catalog_id in("+ids+") and question_status=1 ORDER BY question_create_time DESC";
			return this.dbUtilAutoTran.selectMutliple(sql, pc);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionBproductLineStatus"+e);
		}
	}
	
	
	//查询所有为回答的问题总数 
	public DBRow getQuestionCountByStatus()throws Exception{
		try{
			String sql="select count(*) as count from question_q where question_status=1";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionCountByStatus:"+e);
		}
	}
	//查询某条产品线下为回答问题的总数
	public DBRow getQuestionCountByProductLine(String ids)throws Exception{
		try{
			String sql="select count(*) as count from question_q where question_catalog_id in("+ids+") and question_status=1";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionCountByProductLine"+e);
		}
	}
	
	
	//根据等级查询问题分类
	public DBRow[] getQuestionCatalog(long grade) throws Exception {
		try {
			String sql="select * from question_catalog where grade="+grade;
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionCatalog error:"+e);
		}
	}
	
	
	//根据等级和商品线id
	public DBRow[] getQuestionCatalogByPid(long productlineId) throws Exception {
		try {
			String sql="select * from question_catalog where product_line_id ="+productlineId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionCatalog error:"+e);
		}	
	}
	
	//查询所有问题分类数据
	public DBRow[] getAllQuestionCatalog() throws Exception {
		try {
			String sql="select * from question_catalog";
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorQuestionCatalogMgrZwb getAllQuestionCatalog error:"+e);
		}
		
	}
	//添加问题分类
	public long addQuestionCatalog(DBRow row) throws Exception{
		try{
			return this.dbUtilAutoTran.insertReturnId("question_catalog", row);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb addQuestionCatalog error:"+ e);
		}
	}
	
	//添加问题
	public long addQuestionQ(DBRow row) throws Exception{
		try{
			return this.dbUtilAutoTran.insertReturnId("question_q", row);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb addQuestionQ error:"+ e);
		}
	}
	
	//根据id查询分类
	public DBRow getQuestionCatalogById(long catalogId) throws Exception{
		try{
			String sql="select * from question_catalog where question_catalog_id="+catalogId;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionCatalogById error:"+ e);
		}
	}
	//修改问题分类
	public void updateQuestionCatalogSmall(long catalogId,DBRow row) throws Exception{
		try {
			dbUtilAutoTran.update("where question_catalog_id="+catalogId,"question_catalog", row);
		} catch (Exception e) {
			throw new Exception("FloorQuestionCatalogMgrZwb updateQuestionCatalogSmall error:"+ e);
		}
	}
	
	//查询问题根据产品线
	public DBRow[] getAllQuestionByLineId(long productLineId) throws Exception{
		try{
			String sql="select * from question_q where product_line_id="+productLineId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getAllQuestionByLineId error:"+ e);
		}
	}
	//根据问题id查询问题
	public DBRow getQuestionQById(long question_id) throws Exception{
		try{
			String sql="select * from question_q where question_id="+question_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionQBId"+e);
		}
	}
	//根据问题id查询问题getQuestionSingle
	public DBRow[] getQuestionSingle(long question_id)throws Exception{
		try{
			String sql="select * from question_q where question_id="+question_id;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionSingle"+e);
		}
	}
	
	//根据产品线id 查询产品线名字
	public String getProductLineNameById(long productLineId) throws Exception{
		try{
			String sql="select * from product_line_define where id="+productLineId;
		    DBRow row=this.dbUtilAutoTran.selectSingle(sql);
		    return row.getString("name");
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getQuestionQBId"+e);
		}
	}

	// 根据parentId 和grade查询
	public DBRow[] getAllSubclassByCatalogId(long catalogId) throws Exception {
		try {
			String sql = "select * from question_catalog where parent_id="+catalogId ;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getAllSubclassByCatalogId error:"+ e);
		}
	}

	//根据所有 parentId 查询所有 id
	public DBRow[] getAllCatalogIdByAllParentId(String str)throws Exception{
		try{
			String sql="select * from question_catalog where  parent_id in("+str+")";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatalogMgrZwb getAllCatalogIdByAllParentId error:"+ e);
		}
	}
		  //根据商品id查询下面的所有子级
	public DBRow[] getAllProductCatalog(long productCatalogId)throws Exception{
		try{
			String sql="select * from pc_child_list where search_rootid="+productCatalogId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatlogMgrZwb getAllProductCatalog error:"+e);
		}
	}
	
	//移动问题 更新问题的parentid
	public long moveUpdateQuestion(long questionId,DBRow row)throws Exception{
		try{
			long id=this.dbUtilAutoTran.update("where question_id="+questionId, "question_q",row);
			return id;
		}catch(Exception e){
			throw new Exception("FloorQuestionCatlogMgrZwb moveUpdateQuestion error:"+e);
		}
	}
	
	//根据问题id获取问题的详细信息
	public DBRow getQuestionByCatlogId(long QuestionId)throws Exception{
		try{
			String sql="select * from question_q where question_id="+QuestionId;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatlogMgrZwb getQuestionByCatlogId error:"+e);
		}
	}
	//根据ids 查询所有问题数据库
	public DBRow[] getAllQuestionByFlool(String ids,String op,PageCtrl pc)throws Exception{
		try{
			String sql="select * from question_q where question_catalog_id in("+ids+") ORDER BY "+op+" DESC" ;
			return this.dbUtilAutoTran.selectMutliple(sql,pc);
		}catch(Exception e){
			throw new Exception("FloorQuestionCatlogMgrZwb getAllQuestionByFlool error:"+e);
		}
	}

	//回答问题
	public long addQuestionAnswer(DBRow row) throws Exception {
		
		try {
			return this.dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("question_a"),row);
			
		} catch (Exception e) {
			throw new Exception("FloorQuestionCatalogMgrZwb addQuestionAnswer error:"+ e);
		}
	}
	//修改问题回答状态
	public void modQuertionAnswerState(long question_id,DBRow row) throws Exception{
		try {
			dbUtilAutoTran.update("where question_id="+question_id, ConfigBean.getStringValue("question_q"), row);
		} catch (Exception e) {
			throw new Exception("FloorQuestionCatalogMgrZwb modQuertionCatalogStatus error:"+ e);
		}
	}
	//根据问题ID查询回答内容
	public DBRow getAnswerByQuestionId(long question_id) throws Exception{
		try {
			String sql = "select * from "+ConfigBean.getStringValue("question_a")+" where question_id = "+question_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorQuestionCatalogMgrZwb getAnswerByQuestionId error:"+ e);
		}
	}
	//根据答案ID查询答案详情
	public DBRow getAnswerById(long answer_id) throws Exception{
		try {
			String sql = "select * from "+ConfigBean.getStringValue("question_a")+" where answer_id = "+answer_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorQuestionCatalogMgrZwb getAnswerById error:"+ e);
		}
	}
	//删除答案
	public void deleteQuestionAnswer(long answer_id) throws Exception{
		try {
			dbUtilAutoTran.delete("where answer_id ="+answer_id, ConfigBean.getStringValue("question_a"));
		} catch (Exception e) {

			throw new Exception("FloorQuestionCatalogMgrZwb deleteQuestionAnswer error:"+ e);
		}
	}
	//修改答案
	public void modAnswerById(long answer_id,DBRow modKnowledge) throws Exception {
		
		try {
			 this.dbUtilAutoTran.update("where answer_id="+answer_id, ConfigBean.getStringValue("question_a"), modKnowledge);
			
		} catch (Exception e) {
			throw new Exception("FloorQuestionCatalogMgrZwb modAnswerById error:"+ e);
		}
	}
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
		
	
}
