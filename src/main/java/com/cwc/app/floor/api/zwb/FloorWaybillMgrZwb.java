package com.cwc.app.floor.api.zwb;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorWaybillMgrZwb {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	
	//查询商品在该产品线下的运单数量
	public DBRow getWaybillByCountLineId(long lineId) throws Exception {
		try {
			String sql="select count(*) as count from waybill_order o join waybill_order_item i"+
						" on o.waybill_id=i.waybill_order_id where i.pc_id"+
						" in(select p.pc_id from product p join product_catalog c"+ 
						" on p.catalog_id=c.id"+ 
						" join product_line_define l"+
						" on c.product_line_id=l.id where l.id="+lineId+") and o.product_status=2 and o.status=0 and o.out_id is null";
			return this.dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorWaybillMgrZwb getWaybillByCountLineId error:"+e);
		}
	}
	
	/**
	 * @param lineId 	 产品线id
	 * @param sc_id  	 快递id
	 * @param ps_id   	 仓库id
	 * @param all_weight 重量
	 * @param pkcount    包含数量
	 * @return			 返回条件下 该产品线的运单数
	 * @throws Exception
	 */
	public DBRow getSeachWaybillByLineId(long lineId,long sc_id,long ps_id,long all_weight,long pkcount) throws Exception{		
		try{
			String sql="select count(distinct(waybill_id)) as count from waybill_order o join waybill_order_item i"+
					    " on o.waybill_id=i.waybill_order_id where i.pc_id"+
					    " in(select p.pc_id from product p join product_catalog c"+ 
						" on p.catalog_id=c.id"+ 
						" join product_line_define l"+
						" on c.product_line_id=l.id where l.id="+lineId+") and o.product_status=2 and o.status=0 and o.out_id is null";
			if(all_weight!=0){
				sql+=" and o.all_weight<"+all_weight;
			}
			if(sc_id!=0){
				sql+=" and o.sc_id="+sc_id;
			}
			if(ps_id!=0){
				sql+=" and o.ps_id="+ps_id;
			}
			if(pkcount!=0){
				sql+=" and o.pkcount="+pkcount;
			}
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb getWaybillByLineId error:"+e);
		}
	}
	
	//条件查询包含数量 123
	public DBRow getCountPkcount(long sc_id,long ps_id,long all_weight,long pkcount) throws Exception{		
		try{
			String sql="select count(*) count from waybill_order where status=0 and product_status=2 and out_id is null";
			if(all_weight!=0){
				sql+=" and all_weight<"+all_weight;
			}
			if(sc_id!=0){
				sql+=" and sc_id="+sc_id;
			}
			if(ps_id!=0){
				sql+=" and ps_id="+ps_id;
			}
			if(pkcount!=0){
				sql+=" and pkcount="+pkcount;
			}
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb getWaybillByLineId error:"+e);
		}
	}
	
	//条件查询包含数量 大于4   没有产品线
	public DBRow getCountPkcountMuchNoLine(long sc_id,long ps_id,long all_weight,long pkcount)throws Exception{
		try{
			String sql="select count(*) count from waybill_order where status=0 and product_status=2 and out_id is null";
			if(all_weight!=0){
				sql+=" and all_weight<"+all_weight;
			}
			if(sc_id!=0){
				sql+=" and sc_id="+sc_id;
			}
			if(ps_id!=0){
				sql+=" and ps_id="+ps_id;
			}
			if(pkcount!=0){
				sql+=" and pkcount >="+pkcount;
			}
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb getCountPkcount"+e);
		}
	}
	
	//条件查询包含数量 大于4
	public DBRow getCountPkcountMuch(long lineId,long sc_id,long ps_id,long all_weight,long pkcount)throws Exception{
		try{
			String sql="select count(distinct(waybill_id)) as count from waybill_order o join waybill_order_item i"+
			    " on o.waybill_id=i.waybill_order_id where i.pc_id"+
			    " in(select p.pc_id from product p join product_catalog c"+ 
				" on p.catalog_id=c.id"+ 
				" join product_line_define l"+
				" on c.product_line_id=l.id where l.id="+lineId+") and o.product_status=2 and o.status=0 and o.out_id is null";
			if(all_weight!=0){
				sql+=" and o.all_weight<"+all_weight;
			}
			if(sc_id!=0){
				sql+=" and o.sc_id="+sc_id;
			}
			if(ps_id!=0){
				sql+=" and o.ps_id="+ps_id;
			}
			if(pkcount!=0){
				sql+=" and o.pkcount >="+pkcount;
			}
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb getCountPkcount"+e);
		}
	}
	
	//包含件数  
	public DBRow seachPkcount(long num)throws Exception{
		try{
			String sql="select count(*) count from waybill_order where pkcount="+num+" and product_status=2 and status=0 and out_id is null";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb seachPkcount"+e);
		}
	}
	//包含多件数  
	public DBRow seachPkcountMuch(long num)throws Exception{
		try{
			String sql="select count(*) count from waybill_order where pkcount>="+num+" and product_status=2 and status=0 and out_id is null";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb seachPkcount"+e);
		}
	}
	
	//查询 运单详细没有产品线条件 detailed
	public DBRow[] seachWaybillDetailedNoLine(long sc_id,long ps_id,long all_weight,long pkcount,PageCtrl pc)throws Exception{
		try{
			String sql="select * from waybill_order where status=0 and product_status=2 and out_id is null";
			if(all_weight!=0){
				sql+=" and all_weight<"+all_weight;
			}
			if(sc_id!=0){
				sql+=" and sc_id="+sc_id;
			}
			if(ps_id!=0){
				sql+=" and ps_id="+ps_id;
			}
			if(pkcount!=0){
				sql+=" and pkcount ="+pkcount;
			}
			return this.dbUtilAutoTran.selectMutliple(sql,pc);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb seachWaybillDetailedNoLine"+e);
		}
	}
	//查询 运单详细没有产品线条件 detailed
	public DBRow[] seachWaybillDetailedNoLine(long sc_id,long ps_id,long all_weight,long pkcount)throws Exception{
		try{
			String sql="select * from waybill_order where status=0 and product_status=2 and out_id is null";
			if(all_weight!=0){
				sql+=" and all_weight<"+all_weight;
			}
			if(sc_id!=0){
				sql+=" and sc_id="+sc_id;
			}
			if(ps_id!=0){
				sql+=" and ps_id="+ps_id;
			}
			if(pkcount!=0){
				sql+=" and pkcount ="+pkcount;
			}
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb seachWaybillDetailedNoLine"+e);
		}
	}
	//查询 运单详细没有产品线条件 detailed
	public DBRow[] seachWaybillDetailedNoLineFour(long sc_id,long ps_id,long all_weight,long pkcount,PageCtrl pc)throws Exception{
		try{
			String sql="select * from waybill_order where status=0 and product_status=2 and out_id is null";
			if(all_weight!=0){
				sql+=" and all_weight<"+all_weight;
			}
			if(sc_id!=0){
				sql+=" and sc_id="+sc_id;
			}
			if(ps_id!=0){
				sql+=" and ps_id="+ps_id;
			}
			if(pkcount!=0){
				sql+=" and pkcount >="+pkcount;
			}
			return this.dbUtilAutoTran.selectMutliple(sql,pc);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb seachWaybillDetailedNoLine"+e);
		}
	}
	//查询 运单详细没有产品线条件 detailed
	public DBRow[] seachWaybillDetailedNoLineFour(long sc_id,long ps_id,long all_weight,long pkcount)throws Exception{
		try{
			String sql="select * from waybill_order where status=0 and product_status=2 and out_id is null";
			if(all_weight!=0){
				sql+=" and all_weight<"+all_weight;
			}
			if(sc_id!=0){
				sql+=" and sc_id="+sc_id;
			}
			if(ps_id!=0){
				sql+=" and ps_id="+ps_id;
			}
			if(pkcount!=0){
				sql+=" and pkcount >="+pkcount;
			}
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb seachWaybillDetailedNoLine"+e);
		}
	}
	
	//查询 运单详细有产品线条件 detailed
	public DBRow[] seachWaybillDetailed(long lineId,long sc_id,long ps_id,long all_weight,long pkcount,PageCtrl pc)throws Exception{
		try{
			String sql="select * from waybill_order o join waybill_order_item i"+
		    " on o.waybill_id=i.waybill_order_id where i.pc_id"+
		    " in(select p.pc_id from product p join product_catalog c"+ 
			" on p.catalog_id=c.id"+ 
			" join product_line_define l"+
			" on c.product_line_id=l.id where l.id="+lineId+") and o.product_status=2 and o.status=0 and o.out_id is null";
			if(all_weight!=0){
				sql+=" and o.all_weight<"+all_weight;
			}
			if(sc_id!=0){
				sql+=" and o.sc_id="+sc_id;
			}
			if(ps_id!=0){
				sql+=" and o.ps_id="+ps_id;
			}
			if(pkcount!=0){
				sql+=" and o.pkcount ="+pkcount;
			}
			sql+=" GROUP BY waybill_id";
			return this.dbUtilAutoTran.selectMutliple(sql,pc);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb seachWaybillDetailedNoLine"+e);
		}
	}
	//查询 运单详细有产品线条件 detailed
	public DBRow[] seachWaybillDetailed(long lineId,long sc_id,long ps_id,long all_weight,long pkcount)throws Exception{
		try{
			String sql="select * from waybill_order o join waybill_order_item i"+
		    " on o.waybill_id=i.waybill_order_id where i.pc_id"+
		    " in(select p.pc_id from product p join product_catalog c"+ 
			" on p.catalog_id=c.id"+ 
			" join product_line_define l"+
			" on c.product_line_id=l.id where l.id="+lineId+") and o.product_status=2 and o.status=0 and o.out_id is null";
			if(all_weight!=0){
				sql+=" and o.all_weight<"+all_weight;
			}
			if(sc_id!=0){
				sql+=" and o.sc_id="+sc_id;
			}
			if(ps_id!=0){
				sql+=" and o.ps_id="+ps_id;
			}
			if(pkcount!=0){
				sql+=" and o.pkcount ="+pkcount;
			}
			sql+=" GROUP BY waybill_id";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb seachWaybillDetailedNoLine"+e);
		}
	}
	
	//查询 运单详细有产品线条件 detailed
	public DBRow[] seachWaybillDetailedFour(long lineId,long sc_id,long ps_id,long all_weight,long pkcount,PageCtrl pc)throws Exception{
		try{
			String sql="select * from waybill_order o join waybill_order_item i"+
		    " on o.waybill_id=i.waybill_order_id where i.pc_id"+
		    " in(select p.pc_id from product p join product_catalog c"+ 
			" on p.catalog_id=c.id"+ 
			" join product_line_define l"+
			" on c.product_line_id=l.id where l.id="+lineId+") and o.product_status=2 and o.status=0 and o.out_id is null";
			if(all_weight!=0){
				sql+=" and o.all_weight<"+all_weight;
			}
			if(sc_id!=0){
				sql+=" and o.sc_id="+sc_id;
			}
			if(ps_id!=0){
				sql+=" and o.ps_id="+ps_id;
			}
			if(pkcount!=0){
				sql+=" and o.pkcount >="+pkcount;
			}
			sql+=" GROUP BY waybill_id";
			return this.dbUtilAutoTran.selectMutliple(sql,pc);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb seachWaybillDetailedNoLine"+e);
		}
	}
	//查询 运单详细有产品线条件 detailed
	public DBRow[] seachWaybillDetailedFour(long lineId,long sc_id,long ps_id,long all_weight,long pkcount)throws Exception{
		try{
			String sql="select * from waybill_order o join waybill_order_item i"+
		    " on o.waybill_id=i.waybill_order_id where i.pc_id"+
		    " in(select p.pc_id from product p join product_catalog c"+ 
			" on p.catalog_id=c.id"+ 
			" join product_line_define l"+
			" on c.product_line_id=l.id where l.id="+lineId+") and o.product_status=2 and o.status=0 and o.out_id is null";
			if(all_weight!=0){
				sql+=" and o.all_weight<"+all_weight;
			}
			if(sc_id!=0){
				sql+=" and o.sc_id="+sc_id;
			}
			if(ps_id!=0){
				sql+=" and o.ps_id="+ps_id;
			}
			if(pkcount!=0){
				sql+=" and o.pkcount >="+pkcount;
			}
			sql+=" GROUP BY waybill_id";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb seachWaybillDetailedNoLine"+e);
		}
	}
	
	//创建拣货单、 返回当前拣货单id
	public long addOutOrder(DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.insertReturnId("out_storebill_order",row);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb addOutOrder"+e);
		}
	}
	
	//更新转运单的outid
	public void updateWaybillOutId(DBRow row,String ids)throws Exception{
		try{
			this.dbUtilAutoTran.update("where waybill_id in("+ids+")", "waybill_order", row);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb updateWaybillOutId"+e);
		}
	}
	
	//根据id查询 拣货单
	public DBRow selectOutOrder(long out_id)throws Exception{
		try{
			String sql="select * from out_storebill_order where out_id="+out_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb selectOutOrder"+e);
		}
	}
	//根据拣货单id查询运单
	public DBRow[] findWaybillByOutId(long outId)throws Exception{
		try{
			String sql="select * from waybill_order where out_id="+outId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb findWaybillOutId"+e);
		}
	}
	
	//查询出库单 先查所有
	public DBRow[] selectAllOutOrder(long out_id,long ps_id,long outRorType,PageCtrl pc)throws Exception{
		try{
			String sql="select * from out_storebill_order where 1=1";
			if(out_id!=0){
				sql+=" and out_id="+out_id;
			}
			if(ps_id!=0){
				sql+=" and ps_id="+ps_id;
			}
			if(outRorType!=0){
				sql+=" and out_for_type="+outRorType;
			}
			sql+=" ORDER BY out_id desc";
			return this.dbUtilAutoTran.selectMutliple(sql, pc);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb selectAllOutOrder"+e);
		}
	}
	
	//判断 当前出库单 下 包含该货物的 运单
	public DBRow[] findWaybillByProduct(long productId,long outId)throws Exception{
		try{
			String sql="select * from waybill_order o join waybill_order_item i on o.waybill_id=i.waybill_order_id where i.pc_id="+productId+" and o.out_id="+outId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb findWaybillByProduct"+e);
		}
	}
	//查询该运单下 包含几件商品 及数量
	public DBRow findProductCountByWaybillId(long waybillId)throws Exception{
		try{
			String sql="select waybill_order_id,quantity,count(*) count from waybill_order_item where waybill_order_id="+waybillId;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb findProductCountByWaybillId"+e);
		}
	}
	//查询运单下 包含多件商品详细
	public DBRow[] findProductMuchByWaybillId(long waybillId)throws Exception{
		try{
			String sql="select * from waybill_order o join waybill_order_item i on o.waybill_id = i.waybill_order_id where i.waybill_order_id="+waybillId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb findProductMuchByWaybillId"+e);
		}
	}
	//查快递详细信息
	public DBRow findExpressDetailed(long sc_id)throws Exception{
		try{
			String sql="select * from shipping_company where sc_id="+sc_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb findExpressDetailed"+e);
		}
	}
	//出库单下运单的总数
	public DBRow sumCountWaybillByOutId(long out_id)throws Exception{
		try{
			String sql="select count(*) count from waybill_order where out_id="+out_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb sumCountWaybillByOutId"+e);
		}
	}
	//计算出库单下为打印的运单数量
	public DBRow sumCountPrint(long out_id)throws Exception{
		try{
			String sql="select count(*) count from waybill_order where (status=1 or status=2) and out_id="+out_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb sumCountPrint"+e);
		}
	}
	
	//根据拣货单id  查询拣货单详细
	public DBRow[] selectOutDetail(long out_id)throws Exception{
		try{
			String sql="select * from out_storebill_order_detail where out_storebill_order="+out_id;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb selectOutDetail"+e);
		}
	}
	
	//查询货物位置
	public DBRow[] findSlcId(long out_id)throws Exception{
		try{
			String sql="select DISTINCT(out_list_slc_id) from out_storebill_order_detail where out_storebill_order="+out_id;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb findSlcId"+e);
		}
	}
	//根据区域id查询位置 去重
	public DBRow[] findSlcByAreaId(long areaId,long out_id)throws Exception{
		try{
			String sql="select DISTINCT(out_list_slc_id) from out_storebill_order_detail where out_list_area_id="+areaId+" and out_storebill_order="+out_id;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb findSlcByAreaId"+e);
		}
	}
	//查询区域是2d 还是3d
	public DBRow findCollocationDetail(String areaName)throws Exception{
		try{
			String sql="select * from storage_location_catalog where slc_position_all='"+areaName+"'";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb findCollocationDetail"+e);
		}
	}
	
	//查询货物区域
	public DBRow[] findArea(long out_id)throws Exception{
		try{
			String sql="select DISTINCT(out_list_area_id) from out_storebill_order_detail where out_storebill_order="+out_id;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb findArea"+e);
		}
	}
	//商品
	public DBRow findProductById(long pc_id)throws Exception{
		try{
			String sql="select * from product where pc_id="+pc_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb findProductById"+e);
		}
	}
	
	//查询位置
	public DBRow findStorageLocationCatalogById(long slc_id)throws Exception{
		try{
			String sql="select * from storage_location_catalog where slc_id="+slc_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb findStorageLocationCatalogById"+e);
		}
	}
	//查询区域
	public DBRow findStorageLocationAreaById(long area_id)throws Exception{
		try{
			String sql="select * from storage_location_area where area_id="+area_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb findStorageLocationAreaById"+e);
		}
	}
	
	//拣完货，需要放到的门和位置  根据类型 是拣货单  和拣货单id
	public DBRow[] getStorageOrDoorByOutId(long rel_type,long rel_id)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_info where rel_type="+rel_type+" and rel_id="+rel_id;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb getStorageOrDoorByOutId"+e);
		}
	}
	
	//door
	public DBRow getDoorByRlId(long rl_id)throws Exception{
		try{
			String sql="select * from storage_door where sd_id="+rl_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb getDoorByRlId"+e);
		}
	}
	//weizhi
	public DBRow getStorageByRlId(long rl_id)throws Exception{
		try{
			String sql="select * from storage_load_unload_location where id="+rl_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb getStorageByRlId"+e);
		}
	}
	
	//运单创建拣货单分配人
	public DBRow[] getPeopleList(long ps_id)throws Exception{
		try{
			String sql="select * from admin where adgid=100014 and llock=0 and ps_id="+ps_id;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb getPeople"+e);
		}
	}
	
	public DBRow getPeople(long id)throws Exception{
		try{
			String sql="select * from admin where adid="+id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWaybillMgrZwb getPeople"+e);
		}
	}
	

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
}
