package com.cwc.app.floor.api.zr;

import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorClpTypeMgrZr {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public long addClpType(DBRow row) throws Exception  {
		try{
			String tableName =  ConfigBean.getStringValue("license_plate_type");
			return dbUtilAutoTran.insertReturnId(tableName, row);
 		}catch(Exception e){
			throw new Exception("FloorClpTypeMgrZr.addClpType(row):"+e);
		}
	}
	public void deleteClpType(long sku_lp_type_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("clp_type");	
			dbUtilAutoTran.delete(" where sku_lp_type_id = " + sku_lp_type_id, tableName);
		}catch (Exception e) {
			throw new Exception("FloorClpTypeMgrZr.deleteClpType(sku_lp_type_id):"+e);
		}		
	}
	public DBRow[] getCLpTypeBy(long ps_id ,long pc_id) throws Exception{
		try{
			 
			StringBuffer sql = new StringBuffer("select ct.* , bt.box_total , bt.box_total_piece, bt.box_inner_type from clp_type as ct ")
			.append(" join clp_ps as cps on cps.clp_type_id = ct.sku_lp_type_id ")
			.append(" left join box_type as bt on bt.box_type_id =  ct.sku_lp_box_type where 1=1  ")
			.append(" and ct.sku_lp_pc_id = ").append(pc_id);
			if(ps_id != 0l){
				sql.append(" and cps.to_ps_id =").append(ps_id);
			}
			sql.append(" order by ct.sku_lp_sort");//ct.sku_lp_type_id desc, 
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorClpTypeMgrZr.getCLpTypeBy(ps_id):"+e);
		}
	}
	public DBRow getClpTypeByTypeId(long sku_lp_type_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("clp_type");	
			StringBuffer sql = new StringBuffer("select * from ").append(tableName);
			sql.append(" where sku_lp_type_id =").append(sku_lp_type_id);
		 
			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorClpTypeMgrZr.getClpTypeByTypeId(sku_lp_type_id):"+e);
		}
	}
	public void updateCLpType(long sku_lp_type_id , DBRow row) throws Exception {
		try{
			dbUtilAutoTran.update(" where sku_lp_type_id ="+ sku_lp_type_id, ConfigBean.getStringValue("clp_type"), row);
		}catch (Exception e) {
			throw new Exception("FloorClpTypeMgrZr.updateCLpType(sku_lp_type_id,row):"+e);
		}
	}
	/**
	 * 查询的时候 。可以是条码 也可以是硬件号
	 * @param pc
	 * @param sku_lp_type_id
	 * @param search_key
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getClpContainerBySearchKey(PageCtrl pc, long sku_lp_type_id, String search_key ) throws Exception{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from  container where  1=1 ");
			if(search_key != null && search_key.trim().length() > 0){
				sql.append(" and (container like '%"+search_key+"%' or hardwareId like '%"+search_key+"%') ");
			}
			sql.append(" and container.type_id =").append(sku_lp_type_id)
			.append(" and container.container_type=").append(ContainerTypeKey.CLP);
 			sql.append(" order by con_id desc ");
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}catch (Exception e) {
			throw new Exception("FloorClpTypeMgrZr.getClpContainerBySearchKey(pc,sku_lp_type_id,search_key):"+e);
		}
	}
	public DBRow[] getClpContainerBy(PageCtrl pc, long sku_lp_type_id ) throws Exception{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from  container  where container.type_id ="+sku_lp_type_id+" and container.container_type ="+ContainerTypeKey.CLP);
			sql.append(" order by con_id desc ");
			return dbUtilAutoTran.selectMutliple(sql.toString(),pc) ;
		}catch (Exception e) {
			throw new Exception("FloorClpTypeMgrZr.getClpContainerBy(pc,sku_lp_type_id):"+e);
		}
	}
	public DBRow getClpTypeAndPsInfo(long sku_lp_type_id ) throws Exception {
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select ct.* ,p.p_name  from  license_plate_type as ct ")
			.append(" left join product  as p on ct.pc_id = p.pc_id ")
			.append(" where ct.lpt_id =").append(sku_lp_type_id);
 
 			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorClpTypeMgrZr.getClpTypeAndPsInfo(sku_lp_type_id):"+e);
		}
	}
	public long addCLpTypeContainer(DBRow row) throws Exception {
		try{
			 
			String tablename = ConfigBean.getStringValue("container");
			long containerId = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("container"));
			row.add("con_id", containerId);
			dbUtilAutoTran.insert(tablename, row);
 			return containerId ;
 		}catch (Exception e) {
			throw new Exception("FloorClpTypeMgrZr.addCLpTypeContainer(row):"+e);
		}
	}
	public void updateClpContainer(long con_id , DBRow row) throws Exception {
		try{
			dbUtilAutoTran.update(" where con_id =" +con_id ,ConfigBean.getStringValue("container"), row);
		}catch (Exception e) {
			throw new Exception("FloorClpTypeMgrZr.updateClpContainer(con_id,row):"+e);
		}
	}
	
	//查询clp最大 优先级
	 public DBRow findMaxSort(long ps_id, long sku_lp_pc_id, long title_id ,long customer_id) throws Exception
	  {
	    try {
	      String sql = "select max(ship_to_sort) sort from lp_title_ship_to where 1=1";
	      
	      if (ps_id > 0L) {
	    	  
	        sql = sql + " and ship_to_id = " + ps_id;
	      }
	      
	      if (sku_lp_pc_id > 0L) {
	    	  
	        sql = sql + " and lp_pc_id = " + sku_lp_pc_id;
	      }
	      
	      if (title_id > 0L) {
	    	  
	        sql = sql + " and title_id = " + title_id;
	      }
	      
	      if(customer_id > 0L){
	    	  
	    	  sql += " and customer_id = "+customer_id;
	      }
	      
	      return this.dbUtilAutoTran.selectSingle(sql); } catch (Exception e) {
	    	  throw new Exception("FloorBoxTypeZr.findMaxSort:" + e);
	    }
	    
	  }
	   
	   //添加clp与 仓库的关系
	   public long addClpPs(DBRow row)throws Exception{
		   try{
			   return this.dbUtilAutoTran.insertReturnId("lp_title_ship_to",row);
		   }catch(Exception e){
			   throw new Exception("FloorBoxTypeZr.addClpPs:"+e); 
		   }
	   }
	   public DBRow findContainerById(long id) throws Exception {
		   try{
			   String tablename = ConfigBean.getStringValue("container");
			   String sql="select * from "+tablename+" where con_id="+id;
			   return this.dbUtilAutoTran.selectSingle(sql);
		   }catch(Exception e){
			   throw new Exception("FloorBoxTypeZr.findMaxSort:"+e);
		   }
	   }
	  //查询容器下的商品
	   public DBRow findContainerProduct(long con_id)throws Exception{
		   try{
			   String sql="select * from container_product ct join product p on p.pc_id=ct.cp_pc_id where cp_lp_id="+con_id;
			   return this.dbUtilAutoTran.selectSingle(sql);
		   }catch(Exception e){
			   throw new Exception("FloorBoxTypeZr.findContainerProduct()"+e);
		   }
	   }
	   
	   //根据id查询 clp
	   public DBRow selectContainerClpById(long id)throws Exception{
		   try{
			   String sql="select ct.*,p.p_name,lp.* from license_plate_type lp join product p on lp.pc_id = p.pc_id" +
					"  join container_type ct on lp.basic_type_id=ct.type_id "+" where lp.lpt_id="+id;
			   return this.dbUtilAutoTran.selectSingle(sql);
		   }catch(Exception e){
			   throw new Exception("FloorBoxTypeZr.selectContainerClpById(long id)"+e);
		   }
	   }
	   //根据id查询blp
	   public DBRow selectContainerBlpById(long id)throws Exception{
		   try{
			   String sql="select * from license_plate_type  where lpt_id="+id;
			   return this.dbUtilAutoTran.selectSingle(sql);
		   }catch(Exception e){
			   throw new Exception("FloorBoxTypeZr.selectContainerBlpById(long id)"+e);
		   }
	   }
	   //根据id查询ilp
	   public DBRow selectContainerIlpById(long id)throws Exception{
		   try{
			   String sql="select * from license_plate_type where lpt_id="+id;
			   return this.dbUtilAutoTran.selectSingle(sql);
		   }catch(Exception e){
			   throw new Exception("FloorBoxTypeZr.selectContainerIlpById(long id)"+e);
		   }
	   }
	   
	   //根据clp id 查询仓库关系
	   public DBRow[] selectPsByClpId(long id)throws Exception{
		   try{
			   String sql="select * from lp_title_ship_to where lp_title_ship_id="+id;
			   return this.dbUtilAutoTran.selectMutliple(sql);
		   }catch(Exception e){
			   throw new Exception("FloorBoxTypeZr.selectContainerIlpById(long id)"+e);
		   }
	   }
	   public DBRow[] getClpTypeByPcid(long pc_id) throws Exception{
		   try{
			   String sql="select c.* ,ct.length ,ct.width ,ct.height from license_plate_type as c LEFT JOIN container_type as ct on ct.type_id = c.basic_type_id where c.pc_id = "+pc_id;
			   return this.dbUtilAutoTran.selectMutliple(sql);
		   }catch(Exception e){
			   throw new Exception("FloorBoxTypeZr.selectContainerIlpById(long id)"+e);
		   }
	   }
	   
	   
	   //根据IlpTypeId查询ilp和商品信息
	   public DBRow findIlpTypeProductByIlpTypeId(long id)throws Exception
	   {
		   try
		   {
			   String sql="select * from license_plate_type lp join product p on lp.pc_id = p.pc_id where lp.lpt_id="+id;
			   return this.dbUtilAutoTran.selectSingle(sql);
		   }
		   catch(Exception e)
		   {
			   throw new Exception("FloorBoxTypeZr.findIlpTypeProductByIlpTypeId(long id)"+e);
		   }
	   }
	   
	   
	   //根据BlpTypeId查询Blp和商品信息
	   public DBRow findBlpTypeProductByBlpTypeId(long id)throws Exception
	   {
		   try
		   {
			   String sql="select * from license_plate_type lp join product p on lp.pc_id = p.pc_id where lp.lpt_id="+id;
			   return this.dbUtilAutoTran.selectSingle(sql);
		   }
		   catch(Exception e)
		   {
			   throw new Exception("FloorBoxTypeZr.findBlpTypeProductByBlpTypeId(long id)"+e);
		   }
	   }
	   
	   
	   //根据ClpTypeId查询Clp和商品信息
	   public DBRow findClpTypeProductByClpTypeId(long id)throws Exception
	   {
		   try
		   {
			   String sql="select * from license_plate_type join product p on lp.pc_id = p.pc_id where lp.lpt_id="+id;
			   return this.dbUtilAutoTran.selectSingle(sql);
		   }
		   catch(Exception e)
		   {
			   throw new Exception("FloorBoxTypeZr.findClpTypeProductByClpTypeId(long id)"+e);
		   }
	   }
	   
	   public void updateCLpTypeShipTo(long sku_lp_type_id , DBRow row) throws Exception {
			try{
				dbUtilAutoTran.update(" where lp_title_ship_id ="+ sku_lp_type_id, ConfigBean.getStringValue("lp_title_ship_to"), row);
			}catch (Exception e) {
				throw new Exception("FloorClpTypeMgrZr.updateCLpType(sku_lp_type_id,row):"+e);
			}
		}
	   
	   
	   /**zyj
		 * 验证添加的BLP类型是否已经存在
		 * @param pc_id：商品ID
		 * @param basic_type：基础容器类型ID
		 * @param len：长
		 * @param wid：宽
		 * @param hei：高
		 * @return
		 * @throws Exception
		 */
		public int findClpTypeByBasicLWW(long pc_id,long basic_type, long innerType, int len, int wid, int hei)throws Exception
		{
			try{
				 String sql = "select count(*) cn from license_plate_type where pc_id = "+pc_id+" and stack_length_qty="+len
						 +" and stack_width_qty="+wid+" and stack_height_qty="+hei+" and inner_pc_or_lp="+innerType
						 +" and basic_type_id="+basic_type;
				 DBRow row = dbUtilAutoTran.selectSingle(sql);
				 if(null != row)
				 {
					 return row.get("cn", 0);
				 }
				 return 0;
			}catch(Exception e){
				throw new Exception("FloorBoxTypeZr.findClpTypeByBasicLWW:"+e);
			}
		}
		
		/*
		 * 查询容器下的title 与Ship to
		 * 
		 */
		public DBRow[] getLpTitleA1ndShipTo(long id,int title_id,int ship_to_value) throws Exception{
			try{
				
				String sql = "select * from lp_title_ship_to where lp_type_id = "+ id;
				if(title_id>0){
					
				}
			 
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}catch (Exception e) {
				throw new Exception("FloorClpTypeMgrZr.getLpTitleAndShipTo(lp_type_id):"+e);
			}
		}
		
		
		public DBRow[] getLpTitleAndShipTo(long id,int title_id,int ship_to_id) throws Exception
		{
			try
			{
				String sqls = " select * from lp_title_ship_to  where lp_type_id = "+id+" ";
				StringBuffer sql = new StringBuffer();
				sql.append(sqls);
				if(ship_to_id>0 && title_id>0){
					sql.append(" and ship_to_id = ").append(ship_to_id).append(" and title_id = ").append(title_id);
					sql.append(" union ");
					sql.append(sqls);
					sql.append(" and ((ship_to_id = ").append(ship_to_id).append(" and title_id = 0)");
					sql.append(" or (ship_to_id = 0").append(" and title_id = ").append(title_id).append("))");
				}
				else if(title_id>0 && ship_to_id==0 ){
					sql.append(" and  title_id = ").append(title_id);
				}
				else if(ship_to_id>0 && title_id==0){
					sql.append(" and  ship_to_id = ").append(ship_to_id);
				}
				return dbUtilAutoTran.selectMutliple(sql.toString());
	 		}catch (Exception e) {
				throw new Exception("FloorClpTypeMgrZr.getLpTitleAndShipTo(long id,int title_id,int ship_to_id):"+e);
			}
		}
		
		public DBRow[] getLpTitleAndShipTo(long id,int title_id,int customerId,int ship_to_id) throws Exception {
			
			try{
				
				StringBuffer sql = new StringBuffer();
				
				sql.append(" SELECT * FROM lp_title_ship_to WHERE lp_type_id = ").append(id);
				
				if(ship_to_id>0){
					sql.append(" and (ship_to_id = ").append(ship_to_id).append(" or ship_to_id=0)");
				}
				if(title_id > 0){
					sql.append(" and (title_id = ").append(title_id).append(" or title_id=0)");
				}
				if(customerId > 0){
					sql.append(" and (customer_id = ").append(customerId).append(" or customer_id=0)");
				}
				
				sql.append(" ORDER BY -1");
				
				if(ship_to_id>0){
					sql.append(" ,ship_to_id >0 desc ");
				}
				if(title_id > 0){
					sql.append(" ,title_id>0 desc ");
				}
				if(customerId > 0){
					sql.append(" ,customer_id>0 desc ");
				}
				
				if(ship_to_id <= 0 && title_id <= 0 && customerId <= 0){
					sql.append(" ,ship_to_id >0 desc,title_id>0 desc,customer_id>0 desc ");
				}
				
				/*String sqls = " select * from lp_title_ship_to where lp_type_id = "+id+" ";
				StringBuffer sql = new StringBuffer();
				sql.append(sqls);
				
				if(ship_to_id>0 && title_id>0 && customerId > 0){
					
					sql.append(" and ship_to_id = ").append(ship_to_id).append(" and title_id = ").append(title_id);
					
					sql.append(" union ");
					sql.append(sqls);
					sql.append(" and ((ship_to_id = ").append(ship_to_id).append(" and title_id = 0)");
					sql.append(" or (ship_to_id = 0").append(" and title_id = ").append(title_id).append("))");
				}
				
				if(ship_to_id>0){
					sql.append(" and (ship_to_id = ").append(ship_to_id).append(" or ship_to_id=0)");
				}
				
				if(title_id > 0){
					sql.append(" and (title_id = ").append(title_id).append(" or title_id=0)");
				}
				if(customerId > 0){
					sql.append(" and (customer_id = ").append(customerId).append(" or customer_id=0)");
				}*/
				/*
				else if(title_id>0 && ship_to_id==0 ){
					sql.append(" and  title_id = ").append(title_id);
				}
				else if(ship_to_id>0 && title_id==0){
					sql.append(" and  ship_to_id = ").append(ship_to_id);
				}
				*/
				return dbUtilAutoTran.selectMutliple(sql.toString());
	 		}catch (Exception e) {
				throw new Exception("FloorClpTypeMgrZr.getLpTitleAndShipTo(long id,int title_id,int ship_to_id):"+e);
			}
		}			
		
		//根据ID 删除一条记录（title and ship to）
		public void deleteLpTitleAndShipTo(long lp_title_ship_id) throws Exception {
			try{
				String tableName = ConfigBean.getStringValue("lp_title_ship_to");	
				dbUtilAutoTran.delete(" where lp_title_ship_id = " + lp_title_ship_id, tableName);
			}catch (Exception e) {
				throw new Exception("FloorClpTypeMgrZr.deleteLpTitleAndShipTo(lp_title_ship_id):"+e);
			}		
		}
		
		
		//查询title 是否存在
		   public DBRow[] findTitleAndShipToNo(long id, long title_id,long ship_to_id)throws Exception{
			   try{
				   DBRow para = new DBRow();
				   StringBuffer sql = new StringBuffer();
				   sql.append("select count(*) cn from lp_title_ship_to where lp_type_id = "+id );
				  
				  if(title_id>0 && ship_to_id>0){
					  sql.append("  and title_id=? and ship_to_id=? ");
					  para.add("title_id", title_id);
					  para.add("ship_to_id", ship_to_id); 
				  }else{
				   if(title_id>0){
					   sql.append(" and title_id=?");
					   para.add("title_id", title_id);
				   }
				   if(ship_to_id>0){
					   sql.append(" and ship_to_id=?");
					   para.add("ship_to_id", ship_to_id);   
				   }

				  }
		
				   return this.dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
				  
			   }catch(Exception e){
				   throw new Exception("FloorBoxTypeZr.findMaxSort:"+e);
			   }
		   }
		   
		 //查询ship to 是否存在
//		   public int findShipToNo(long id,long title_id, long ship_to_id)throws Exception{
//			   try{
//				   String sql="select count(*) cn from lp_title_ship_to where 1=1";
//				   if(title_id>0){
//					   sql.append(" and title_id = ").append(title_id);
//					  
//				   }
//				   if(ship_to_id>0){
//					   sql.append(" and ship_to_id = ").append(ship_to_id);
//					  
//				   }
//				   
//				   DBRow row = dbUtilAutoTran.selectSingle(sql);
//					 if(null != row)
//					 {
//						 return row.get("scn", 0);
//					 }
//					 return 0;
//			   }catch(Exception e){
//				   throw new Exception("FloorBoxTypeZr.findMaxSort:"+e);
//			   }
//		   }   
	   
		 /**
		  * 通过PackagingType查询CLP Type
		  * @param pkg_id
		  * @return
		  * @throws Exception
		  * @author:zyj
		  * @date: 2015年3月25日 下午2:23:23
		  */
		public int findClpTypeCnByPackagingType(long pkg_id) throws Exception
		{
			try
			{
				String sql = "select count(*) cn from license_plate_type where basic_type_id = " + pkg_id;
				DBRow result = dbUtilAutoTran.selectSingle(sql);
				if(null != result)
				{
					return result.get("cn", 0);
				}
				return 0;
			}
			catch(Exception e)
			{
			   throw new Exception("FloorClpTypeZr.findClpTypeCnByPackagingType:"+e);
			}
		}
		
		
		public DBRow[] getCLPTypeForShipTo(long clp_type_id, long ship_to_id, long title_id)
			    throws Exception
			  {
			    try
			    {
			     DBRow para = new DBRow();
				 StringBuffer sql = new StringBuffer();
				 sql.append( "select * from " + ConfigBean.getStringValue("lp_title_ship_to") + " where lp_type_id ="+clp_type_id);

			      if(title_id>0){
					   sql.append(" and title_id=?");
					   para.add("title_id", title_id);
				   }
				  if(ship_to_id>0){
					   sql.append(" and ship_to_id=?");
					   para.add("ship_to_id", ship_to_id);   
				   }
				   return this.dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
			    }
			    catch (Exception e) {
			    	   throw new Exception("FloorLPTypeMgrZJ getCLPTypeForShipTo error:" + e);
			    }
			 
			  }
	  
		  public void deleteClpTitleShipTo(long id) throws Exception {
				try{
					String tableName = ConfigBean.getStringValue("lp_title_ship_to");	
					dbUtilAutoTran.delete(" where lp_title_ship_id = " + id , tableName);
				}catch (Exception e) {
					throw new Exception("FloorClpTypeMgrZr.deleteClpType(sku_lp_type_id):"+e);
				}		
			}
 public DBRow[] getCLPTypeTitleShipTo(long clp_type_id, long ship_to_id, long title_id)
					    throws Exception
					  {
					    try
					    {
					     DBRow para = new DBRow();
						 StringBuffer sql = new StringBuffer();
						 sql.append( "select * from " + ConfigBean.getStringValue("lp_title_ship_to") + " where lp_type_id ="+clp_type_id);

//					      if(title_id>0){
							   sql.append(" and title_id=?");
							   para.add("title_id", title_id);
//						   }
//						  if(ship_to_id>0){
							   sql.append(" and ship_to_id=?");
							   para.add("ship_to_id", ship_to_id);   
//						   }
						   return this.dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
					    }
					    catch (Exception e) {
					    	   throw new Exception("FloorClpTypeMgrZr.getCLPTypeTitleShipTo error:" + e);
					    }
					 
					  }
 


			  
			  public DBRow[] getCLPTypeTitleAndShipTo(long clp_type_id, long ship_to_id, long title_id)
					    throws Exception
					  {
					    try
					    {
					     DBRow para = new DBRow();
						 StringBuffer sql = new StringBuffer();
						 sql.append( "select * from " + ConfigBean.getStringValue("lp_title_ship_to") + " where lp_type_id ="+clp_type_id);

					      if(title_id>0){
							   sql.append(" and ( title_id=? and ship_to_id= 0) ");
							   para.add("title_id", title_id);
						   }
						  if(ship_to_id>0){
							   sql.append(" or (ship_to_id=? and title_id=0) ");
							   para.add("ship_to_id", ship_to_id);   
						   }
						   return this.dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
					    }
					    catch (Exception e) {
					    	   throw new Exception("FloorClpTypeMgrZr.getCLPTypeTitleShipTo error:" + e);
					    }
					 
					  }

				public DBRow[] getCLPTypeTitleShipTo(long clp_type_id, long ship_to_id, long title_id,long customerId)
					    throws Exception
					  {
					    try
					    {
					     DBRow para = new DBRow();
						 StringBuffer sql = new StringBuffer();
						 sql.append( "select * from " + ConfigBean.getStringValue("lp_title_ship_to") + " where lp_type_id ="+clp_type_id);

							   sql.append(" and title_id=?");
							   para.add("title_id", title_id);
							   sql.append(" and ship_to_id=?");
							   para.add("ship_to_id", ship_to_id); 
							   sql.append(" and customer_id=?");
							   para.add("customer_id",customerId );
						   return this.dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
					    }
					    catch (Exception e) {
					    	   throw new Exception("FloorClpTypeMgrZr.getCLPTypeTitleShipTo error:" + e);
					    }
					 
				}			  
			  
			  /**
				 * 功能：获取所需clp及数量
				 * @param title
				 * @param shipToid
				 * @param pc_id
				 * @param orderQty
				 * @param pallets
				 * @return
			 * @throws Exception 
				 */	
				public DBRow[] getPoundPerPackage(long title, long shipToid, long pc_id,int orderQty, float pallets) throws Exception{
					DBRow[] recommands;
					
					StringBuffer sqlBuffer = new StringBuffer();
					
					sqlBuffer.append("SELECT p.pc_id AS pc_id, ")
							 .append("       p.weight AS p_weight, ")
							 .append("       p.weight AS p_weight, ")
							 .append("       lp.lpt_id AS clp_type_id, ")
							 .append("       lp.lp_name AS clp_type_name, ")
							 .append("       lp.total_weight AS clp_type_weight, ")
							 .append("       lp.weight_uom AS weight_uom, ")
							 .append("       lp.inner_total_pc AS pc_qty, ")
							 .append("       lp.ibt_height AS clp_packaging_weight ")
							 .append("	FROM lp_title_ship_to ltst ")
							 .append("	LEFT JOIN license_plate_type lp ON lp.pc_id = ltst.lp_pc_id ")
							 .append("	LEFT JOIN product p ON p.pc_id = ltst.lp_pc_id ")
							 .append(" WHERE lp.active = 1 ")
							 .append("	 AND lp.inner_total_pc * "+ pallets +" = "+ orderQty)
							 .append("	 AND ltst.lp_pc_id = "+ pc_id)
							 .append("	 AND ltst.title_id = ?")
							 .append("	 AND ltst.ship_to_id = ?")
							 .append(" ORDER BY ltst.lp_title_ship_id")
							 .append(" LIMIT 1");
					
					DBRow params = new DBRow();
					params.add("title_id", title);
					params.add("ship_to_id", shipToid);
					
					try {
						recommands = dbUtilAutoTran.selectPreMutliple(sqlBuffer.toString(), params);		
					} catch (Exception e) {
						recommands = null;
						throw new Exception("FloorClpTypeMgrZr.getPoundPerPackage error:" + e);
					}
					
					return recommands;
				}

				public DBRow[] findProductNode(long pc_id) throws Exception {
					   try{
							  //String tablename = ConfigBean.getStringValue("product");
							   String sql="select * from product p  where  p.pc_id="+pc_id;
							   return dbUtilAutoTran.selectMutliple(sql.toString());
							 //  return this.dbUtilAutoTran.selectSingle(sql);
						   }catch(Exception e){
							   throw new Exception("FloorClpTypeMgrZr.findProductNode:"+e);
						   }

				   }
				  
				  public DBRow findProductLineNode(long pc_id) throws Exception {
					  try{
							
						   String sql="select * from product p  "
						   		+ "LEFT JOIN product_catalog pc  ON p.catalog_id=pc.id "
						   		+ "where p.pc_id="+pc_id;
						  // return dbUtilAutoTran.selectMutliple(sql.toString());
						  return this.dbUtilAutoTran.selectSingle(sql);
					   }catch(Exception e){
						   throw new Exception("FloorClpTypeMgrZr.findProductLineNode:"+e);
					   }
				   }
  
				  public DBRow findContainerType(long type_id) throws Exception {
					   try{
						   String tablename = ConfigBean.getStringValue("container_type");
						   String sql="select * from "+tablename+" where type_id="+type_id;
						   return this.dbUtilAutoTran.selectSingle(sql);
					   }catch(Exception e){
						   throw new Exception("FloorBoxTypeZr.findContainerType:"+e);
					   }
				   }
				  
				  //查询容器属性
				   public DBRow findContainerProperty(long lpt_id)throws Exception{
					   try{
						   String sql="select * from license_plate_type  where lpt_id="+lpt_id;
						   return this.dbUtilAutoTran.selectSingle(sql);
					   }catch(Exception e){
						   throw new Exception("FloorClpTypeMgrZr.findContainerProperty()"+e);
					   }
				   }


				   /*
				    * 根据type_id 查询 license_plate_type  And container_type 数据
				    */
				   public DBRow findContainerTypeAndClpType(long type_id)throws Exception{
					   try{
						   String sql="select * from  license_plate_type  lp, container_type ct where  lp.basic_type_id = ct.type_id and lp.lpt_id ="+type_id;
						   return this.dbUtilAutoTran.selectSingle(sql);
					   }catch(Exception e){
						   throw new Exception("FloorClpTypeMgrZr.findContainerTypeAndClpType()"+e);
					   }
				   }

}
