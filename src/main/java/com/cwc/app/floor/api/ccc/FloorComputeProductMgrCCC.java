package com.cwc.app.floor.api.ccc;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorComputeProductMgrCCC {

	private DBUtilAutoTran dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	

	/**
	 * 交货单-计算库存成本
	 * @param delivery_order_id 交货单ID
	 * @throws Exception
	 */
	
	public String computePriceForDelivery(long delivery_order_id) throws Exception {

		String sql = "";
		DBRow[] dbrows = null;

		double computeResult = 0.0;
		double price = 0.0;// 采购价格
		double delivery_reap_count = 0.0;// 交货单数量
		double storage_unit_price = 0.0;// 库存价格/单个成本
		double store_count = 0.0;//库存数量
		double count = 0.0;
		double backup_count = 0.0;
		
		sql = "SELECT DISTINCT pd.price, dod.delivery_reap_count, pd.backup_count, ps.pid,ps.storage_unit_price,ps.storage_unit_freight,ps.store_count,dod.product_id,`do`.delivery_order_id,ps.pid,ps.cid " +
				"FROM delivery_order AS `do` " +
				"JOIN delivery_order_detail AS dod ON dod.delivery_order_id = `do`.delivery_order_id " +
				"JOIN purchase AS p ON  `do`.delivery_purchase_id = p.purchase_id " +
				"JOIN purchase_detail AS pd ON pd.purchase_id = p.purchase_id AND pd.product_id = dod.product_id " +
				"JOIN product_storage AS ps ON ps.pc_id = dod.product_id and ps.cid = p.ps_id " +
				"where `do`.delivery_order_id = "+delivery_order_id+"  and delivery_reap_count > 0 ";

		dbrows = dbUtilAutoTran.selectMutliple(sql);
		if(dbrows != null){
			for (int i = 0; i < dbrows.length; i++) {
				int pid = dbrows[i].get("pid", 0);
				// 采购单价格
				price = dbrows[i].get("price", 0d);
				// 交货单数量
				delivery_reap_count = dbrows[i].get("delivery_reap_count", 0d);
				storage_unit_price = dbrows[i].get("storage_unit_price", 0d);
				backup_count = dbrows[i].get("backup_count", 0d);
				store_count = dbrows[i].get("store_count", 0d);
				
				if(storage_unit_price == 0.0 || storage_unit_price == 0 || (store_count-delivery_reap_count)<=0)
				{
					if(delivery_reap_count == 0.0)
					{
						//delivery_reap_count = 1.0;
						return "交货单数量为0";
					}
					computeResult = price*(delivery_reap_count-backup_count)/delivery_reap_count;
				}else{
					count = store_count;
					if(count == 0.0){
						//count = 1.0;
						return "交货单数量和库存数量相加为0";
					}
					computeResult = (price*(delivery_reap_count-backup_count) + storage_unit_price
							* (store_count-delivery_reap_count))
							/ count;
				}
				String sql2 = "UPDATE product_storage SET storage_unit_price = "+(float)computeResult+" WHERE pid = "+pid;
				dbUtilAutoTran.updateInstallSQL(sql2);
			}
			
		}
		return "";
		
	}
	
	/**
	 * 交货单-计算库存运费
	 * @param delivery_order_id
	 * @throws Exception
	 */
	public String computeFreightForDelivery(long delivery_order_id) throws Exception{
		
		DBRow[] dbrows = null;
		DBRow dbrow = null;
		
		double unit_price = 0.0;//交货单单个运费
		double sum_price = 0.0;//交货单总运费
		double sum_weight = 0.0;//总重量
		double computeResult = 0.0;
		double storage_unit_freight = 0.0;
		
		String sql2 = "select sum(dfc_unit_price*dfc_unit_count*dfc_exchange_rate) AS sum_price from delivery_freight_cost where delivery_id = "+delivery_order_id;
		dbrow = dbUtilAutoTran.selectSingle(sql2);
		if(dbrow != null){
			sum_price = dbrow.get("sum_price", 0d);
		}
		
		String  sql = "SELECT DISTINCT pd.price, dod.delivery_reap_count, ps.storage_unit_freight, ps.store_count, dod.product_id, `do`.delivery_order_id, ps.pid, ps.cid, p1.weight " +
				"FROM delivery_order AS `do` " +
				"JOIN delivery_order_detail AS dod ON dod.delivery_order_id = `do`.delivery_order_id " +
				"JOIN purchase AS p ON  `do`.delivery_purchase_id = p.purchase_id " +
				"JOIN purchase_detail AS pd ON pd.purchase_id = p.purchase_id AND pd.product_id = dod.product_id " +
				"JOIN product_storage AS ps ON ps.pc_id = dod.product_id and ps.cid = p.ps_id " +
				"JOIN product AS p1 ON p1.pc_id = dod.product_id " +
				"where `do`.delivery_order_id = "+delivery_order_id + " and delivery_reap_count > 0";
		dbrows = dbUtilAutoTran.selectMutliple(sql);
		if(dbrows != null){
			for(int j = 0;j<dbrows.length; j++){
				sum_weight += dbrows[j].get("weight", 0d)*dbrows[j].get("delivery_reap_count",0f);
			}
			for(int i = 0;i<dbrows.length;i++){
				int pid = dbrows[i].get("pid", 0);
				unit_price = sum_price * (dbrows[i].get("weight", 0d)*dbrows[i].get("delivery_reap_count",0f)/sum_weight);
				storage_unit_freight = dbrows[i].get("storage_unit_freight", 0d);
				if(storage_unit_freight == 0 || storage_unit_freight == 0.0||(dbrows[i].get("store_count", 0f)-dbrows[i].get("delivery_reap_count", 0f)) <=0)
				{
					if(dbrows[i].get("delivery_reap_count",0f) == 0){
						//computeResult = unit_price/1;
						return "交货数量为0";
					}
					else
					{
						computeResult = unit_price/dbrows[i].get("delivery_reap_count", 0d);
					}
				}else{
					if(dbrows[i].get("store_count", 0d) == 0){
						//computeResult = (unit_price +(dbrows[i].get("storage_unit_freight", 0d)*dbrows[i].get("store_count", 0d)))/1;
						return "交货数量和库存数量相加为0";
					}
					else
					{
						computeResult = (unit_price +(dbrows[i].get("storage_unit_freight", 0d)*(dbrows[i].get("store_count", 0d)-dbrows[i].get("delivery_reap_count", 0d))))/(dbrows[i].get("store_count", 0d));
					}
				}
				
				String sql4 = "UPDATE product_storage SET storage_unit_freight = "+(float)computeResult+" WHERE pid = "+pid;
				dbUtilAutoTran.updateInstallSQL(sql4);
				
			}
			
		}
		return "";
	}
	
	/**
	 * 转运单库存成本
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public String computePriceForTransport(long transport_id) throws Exception {

		String sql = "";
		DBRow[] dbrows = null;

		float computeResult = 0f;
		float price = 0f;// 出库成本
		float transport_reap_count = 0f;// 转运单数量
		float unit_price = 0f;// 单个产品的价格
		float storage_unit_freight =0f;// 单个运费
		float storage_unit_price  = 0f;//库存价格/单个成本
		float store_count = 0f;//库存数量
		float count = 0f;

		sql = "SELECT DISTINCT dod.transport_send_price, dod.transport_reap_count, ps.storage_unit_freight, ps.storage_unit_price, ps.store_count, dod.transport_pc_id, tt.transport_id, ps.pid, ps.cid, p1.weight, p1.unit_price " +
				"FROM transport AS tt " +
				"JOIN transport_detail AS dod ON dod.transport_id = tt.transport_id " +
				"JOIN product_storage AS ps ON ps.pc_id = dod.transport_pc_id and ps.cid = tt.receive_psid " +
				"JOIN product AS p1 ON p1.pc_id = dod.transport_pc_id " +
				"where tt.transport_id = "+transport_id+" and `dod`.transport_reap_count>0 ";

		dbrows = dbUtilAutoTran.selectMutliple(sql);
		if(dbrows != null){
			for (int i = 0; i < dbrows.length; i++) {
				int pid = dbrows[i].get("pid", 0);
				price = dbrows[i].get("transport_send_price", 0f);
				transport_reap_count = dbrows[i].get("transport_reap_count", 0f);
				unit_price = dbrows[i].get("unit_price", 0f);
				storage_unit_freight = dbrows[i].get("storage_unit_freight", 0f);
				storage_unit_price = dbrows[i].get("storage_unit_price", 0f);
				
				store_count = dbrows[i].get("store_count", 0f);
				
				if(price == 0 ||price == 0.0){
					price = unit_price;
				}
				if(storage_unit_price == 0.0 || storage_unit_price == 0 || (store_count-transport_reap_count) <= 0){
					computeResult = price;
				}else{
					if((store_count-transport_reap_count) == 0.0){
						return "库存数量为0";
					}
					computeResult = (price * transport_reap_count + storage_unit_price
							* (store_count-transport_reap_count))
							/ store_count;
				}
				String sql2 = "UPDATE product_storage SET storage_unit_price = "+(float)computeResult+" WHERE pid = "+pid;
				dbUtilAutoTran.updateInstallSQL(sql2);

			}
			
		}
		return "";
		
	}
	
	/**
	 * 转运单库存运费
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public String computeFreightForTransport(long transport_id) throws Exception{
			
			DBRow[] dbrows = null;
			DBRow dbrow = null;
			
			double unit_price = 0.0;//转运单单个运费
			double sum_price = 0.0;//转运单总运费
			double sum_weight = 0.0;//总重量
			double computeResult = 0.0;
			double storage_unit_freight = 0.0;
			double total_count = 0.0;
			double transport_reap_count = 0.0;
			double stock_count = 0.0;
			
			String sql2 = "select sum(tfc_unit_price*tfc_unit_count*tfc_exchange_rate) AS sum_price from transport_freight_cost where transport_id = "+transport_id;
			dbrow = dbUtilAutoTran.selectSingle(sql2);
			if(dbrow != null){
				sum_price = dbrow.get("sum_price", 0d);
			}
			
			String sql = "SELECT DISTINCT dod.transport_send_price, dod.transport_reap_count, ps.storage_unit_freight, ps.storage_unit_price, ps.store_count, dod.transport_pc_id, tt.transport_id, ps.pid, ps.cid, p1.weight, p1.unit_price " +
				"FROM transport AS tt " +
				"JOIN transport_detail AS dod ON dod.transport_id = tt.transport_id " +
				"JOIN product_storage AS ps ON ps.pc_id = dod.transport_pc_id and ps.cid = tt.receive_psid " +
				"JOIN product AS p1 ON p1.pc_id = dod.transport_pc_id " +
				"where tt.transport_id = "+transport_id+" and `dod`.transport_reap_count>0 ";
			dbrows = dbUtilAutoTran.selectMutliple(sql);
			if(dbrows != null){
				for(int j = 0;j<dbrows.length; j++){
					sum_weight += dbrows[j].get("weight", 0d)*dbrows[j].get("transport_reap_count", 0d);
				}
				for(int i = 0;i<dbrows.length;i++){
					int pid = dbrows[i].get("pid", 0);
					unit_price = sum_price * (dbrows[i].get("weight", 0d)*dbrows[i].get("transport_reap_count", 0d)/sum_weight);
					storage_unit_freight = dbrows[i].get("storage_unit_freight", 0d);
					transport_reap_count = dbrows[i].get("transport_reap_count", 0d);
					total_count = dbrows[i].get("store_count", 0d);
					stock_count = total_count - transport_reap_count;
					
					if(storage_unit_freight == 0 || stock_count <= 0)
					{
						if(dbrows[i].get("transport_reap_count", 0d) == 0)
						{
							return "转运单数量为0";
						}else{
							computeResult = unit_price/transport_reap_count;
						}
					}else{
						computeResult = (unit_price +(storage_unit_freight*stock_count))/(total_count);
					}
					
					String sql4 = "UPDATE product_storage SET storage_unit_freight = "+(float)computeResult+" WHERE pid = "+pid;
					dbUtilAutoTran.updateInstallSQL(sql4);
				}
				
			}
			return "";
		}
	
	/**
	 * 获得转运单总运费
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getSumTransportFreightCost(long transport_id) 
		throws Exception 
	{
		String sql = "select sum(tfc_unit_price*tfc_unit_count*tfc_exchange_rate) AS sum_price from transport_freight_cost where transport_id = "+transport_id;
		return dbUtilAutoTran.selectSingle(sql);
	}
	
	public DBRow getSumDeliveryFreightCost(String delivery_order_id) throws Exception {
		String sql = "select sum(dfc_unit_price*dfc_unit_count*dfc_exchange_rate) AS sum_price from delivery_freight_cost where delivery_id = "+delivery_order_id;
		return dbUtilAutoTran.selectSingle(sql);
	}
}
