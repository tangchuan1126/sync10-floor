package com.cwc.app.floor.api.zr;


import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorTransportMgrZr {
	
	private DBUtilAutoTran dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public DBRow getSupplierWarehouse(long supplier_id) throws Exception {
		try{
			 String tableName =  ConfigBean.getStringValue("supplier");
			 return dbUtilAutoTran.selectSingle("select * from" + tableName + " where id="+supplier_id);
			  
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.getSupplierWarehouse(apply_money_id):"+e);
		}
	}
	public DBRow getSystemPs(long ps_id) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("product_storage_catalog");
			return dbUtilAutoTran.selectSingle("select * from " + tableName + " where id="+ps_id); 
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.getSystemPs(ps_id):"+e);
		}
	}
	public long addTransportLog(DBRow row) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("transport_logs");
			long logsId = dbUtilAutoTran.getSequance("transport_logs");
			row.add("logs_id",logsId );
			dbUtilAutoTran.insert("transport_logs", row);
			return logsId;
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.addTransportLog(row):"+e);
		}
	}
	public void updateTransportByIdAndDBRow(long transport_id ,DBRow row) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("transport");
			dbUtilAutoTran.update(" where transport_id=" + transport_id, tableName, row);
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.updateTransportByIdAndDBRow(transport_id,row):"+e);
		}
	}
	public long addTransportCertificateFile(DBRow row) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("file");
			 return dbUtilAutoTran.insertReturnId(tableName, row);
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.addTransportCertificateFile(row):"+e);
		}
	}
	public void deleteFileBy(long fileId , String tableName , String pk) throws Exception {
		try{
			 dbUtilAutoTran.delete(" where " + pk + " ="+fileId, tableName); 
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.deleteFileBy(fildId,tableName):"+e);
		}
	}
	public long addProductFile(DBRow row) throws Exception {
		try{
			return  dbUtilAutoTran.insertReturnId("product_file", row); 
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.addProductFile(DBRow):"+e);
		}
	}
	public int getNextFileIndex(long file_with_id , int file_with_type, int file_with_class) throws Exception 
	{
		try
		{
 			StringBuffer sql = new StringBuffer("select * from ").append("file");
			sql.append(" where file_with_id=").append(file_with_id);
			if(file_with_type != 0)
			{
				sql.append(" and file_with_type=").append(file_with_type);
			}
			if(file_with_class !=0 )
			{
				sql.append(" and file_with_class=").append(file_with_class);
			}
			sql.append(" order by file_id  desc limit 0,1");
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			if(row == null){
				return 0 ;
			}else
			{
				String fileName = row.getString("file_name");
				int indexOf = fileName.indexOf(".");
				fileName = fileName.substring(0,indexOf);
				String[] array = fileName.split("_");
				if(array !=null && array.length > 0)
				{
					if(Integer.parseInt( array[array.length-1]) > 10000){
						return 1;
					}
					return Integer.parseInt( array[array.length-1]) + 1;
				}else
				{
					return 1 ;
				} 
			}
		}catch (Exception e)
		{
			throw new Exception("FloorTransportMgrZr.getNextFileIndex( file_with_id ,  file_with_type,  file_with_class):"+e);
		}
	}
	public int getNextProductFileIndex(long pc_id ,int product_file_type , long file_with_id ) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("product_file");
			String sql = "select *  from " + tableName + " where pc_id="+pc_id + " and product_file_type="+product_file_type;
			sql += " and file_with_id="+file_with_id;
			sql += " order by pf_id  desc limit 0,1";
			DBRow row =  dbUtilAutoTran.selectSingle(sql);
			if(row == null){
				return 0 ;
			}else{
				String fileName = row.getString("file_name");
				int indexOf = fileName.indexOf(".");
				fileName = fileName.substring(0,indexOf);
				String[] array = fileName.split("_");
				if(array !=null && array.length > 0){
					return Integer.parseInt( array[array.length-1]) + 1;
				}else{
					return 1 ;
				} 
			}
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.getNextProductFileIndex(pc_id , product_file_type):"+e);
		}
		
	}
	public int getNextFileIndex(long file_with_id ,int file_with_type ) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("file");
			String sql = "select count(*) as sum_total from " + tableName + " where file_with_id="+file_with_id + " and file_with_type="+file_with_type;
			DBRow row =  dbUtilAutoTran.selectSingle(sql);
			if(row == null){
				return 0 ;
			}else{
				return row.get("sum_total", 0) + 1;
			}
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.getNextFileIndex(ile_with_id , file_with_type):"+e);
		}
	}
	public DBRow[] getAllProductFileByPcId(String pc_id, long file_with_type, long file_with_id) throws Exception {
		try{
 
			StringBuffer sql = new StringBuffer();
			sql.append("select product_file.* ,product.p_name from product_file left join product on product_file.pc_id = product.pc_id where product_file.pc_id in (").append(pc_id).append(")")
			.append(" and file_with_type=").append(file_with_type).append(" and file_with_id=").append(file_with_id);
			return dbUtilAutoTran.selectMutliple(sql.toString());
			 
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.getAllProductFileByPcId(DBRow):"+e);
		}
	}
	public int getTransportCertificateIndex(long transportId , int file_with_class ,int file_with_type) throws Exception {
		
		try{
			String tableName =  ConfigBean.getStringValue("file");
			StringBuffer sql = new StringBuffer();
 			sql.append("select * from ").append(tableName)
			.append(" where file_with_class=").append(file_with_class).append(" and file_with_type=").append(file_with_type)
			.append(" and file_with_id=").append(transportId).append(" order by file_id desc limit 0,1");
			
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			if(row == null){
				return 1 ;
			}else{
				String fileName = row.getString("file_name");
				int indexOf = fileName.indexOf(".");
				fileName = fileName.substring(0,indexOf);
				String[] array = fileName.split("_");
				if(array !=null && array.length > 0){
					return Integer.parseInt( array[array.length-1]) + 1;
				}else{
					return 1 ;
				} 
			}
			 
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.getTransportCertificateIndex(transportId,file_with_class,file_with_type):"+e);
		}
	}
	public void updateTransportByIdAndRow(long id , DBRow row) throws Exception{
		try{
			 dbUtilAutoTran.update(" where transport_id="+id, ConfigBean.getStringValue("transport"), row); 
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.updateTransportByIdAndRow(id,row):"+e);
		}
	}
	public DBRow getFileByFileWithIdAndFileWithClass(String tableName , long file_with_id , long file_with_class , int file_with_type) throws Exception {
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from ").append(tableName).append(" where file_with_id=").append(file_with_id).append(" and file_with_class=").append(file_with_class)
			.append(" and file_with_type=").append(file_with_type);
			return dbUtilAutoTran.selectSingle(sql.toString());
	 
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.updateTransportByIdAndRow(id,row):"+e);
		}
	}
	/**
	 * 插入到File表中的数据不是重复的数据。先查看有无。有的话那么就是更新。没有的话就是删除
	 * @param fileRow
	 * @throws Exception
	 */
	public void addFileNotExits(DBRow fileRow) throws Exception {
		try{
				// 更新
				//插入
				this.addTransportCertificateFile(fileRow);
		 
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.addFileNotExits(fileRow):"+e);
		}
	}
	public void updateFileRow(long fileId , DBRow row, String tableName) throws Exception {
		try{
			 dbUtilAutoTran.update(" where file_id="+fileId,tableName , row);
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.updateFileRow(fileRow):"+e);
		}
	}
	public DBRow getFirstTransportIngLogTime(long transport_id , int transport_type , int activity_id ) throws Exception {
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from transport_logs").append(" where transport_id=").append(transport_id).append(" and transport_type=").append(transport_type)
			.append(" and activity_id=").append(activity_id).append(" order by logs_id limit 0,1;");
			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.getFirstTransportClearanceLogTime(transport_id ,transport_type ,activity_id ):"+e);
		}
	}
	/**
	 * //获取仓库 ，获取角色
	 * 从floor层floorTransportMgrZr.getAllUserAndDept(proJsId,ps_id);copy的,但是加了个参数 long group.
	 * @param proJsId
	 * @param ps_id
	 * @param group 人员分组 角色
	 * @return
	 * @throws Exception
	 */
//	public DBRow[]  getAllUserAndDept(long proJsId, long ps_id, long group) throws Exception {
//		try {
//			StringBuffer sql = new StringBuffer(
//					"select * from admin left join admin_group on admin.adgid = admin_group.adgid where llock = 0 and  LENGTH(employe_name) > 0  ");
//			
//			if (ps_id > 0L) {
//				sql.append(" and ps_id=").append(ps_id);
//			}
//			//对group条件过滤
//			if (group > 0L) {
//				sql.append(" and admin_group.adgid=").append(group);
//			}
//			if ((proJsId > 0L) && (proJsId != 15L)) {
//				sql.append(" and proJsId=").append(proJsId);
//			}
//			if (proJsId == 15L) {
//				sql.append(" and proJsId >= ").append(5);
//			}
//			sql.append(" order by admin_group.adgid, admin.adid ");
//			return this.dbUtilAutoTran.selectMutliple(sql.toString());
//		} catch (Exception e) {
//			throw new Exception(
//			"FloorTransportMgrZr.getFirstTransportClearanceLogTime(transport_id ,transport_type ,activity_id ):" + e);
//		}
//	}
	
	public DBRow[] getAllUserAndDept(long proJsId , long ps_id) throws Exception {
		try{
			StringBuffer sql = new StringBuffer("select * from admin left join admin_group on admin.adgid = admin_group.adgid where llock = 0 and  LENGTH(employe_name) > 0  ");
			 
			if( ps_id >0l ){
				sql.append(" and ps_id=").append(ps_id);
			}
			if(proJsId > 0l && proJsId != 15){//其他情况该查询什么就是什么
				sql.append(" and proJsId=").append(proJsId);
			}
			if(proJsId == 15){	//查询主管和副主管
				sql.append(" and proJsId >= ").append(5);
			}
			sql.append(" order by admin_group.adgid, admin.adid ");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.getAllUserAndDept(transport_id ,transport_type ,activity_id ):"+e);
		}
	}
	public DBRow[] getFileByFilwWidthIdAndFileType(String tableName , long file_with_id, int file_with_type) throws Exception{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from ").append(tableName).append(" where file_with_id=").append(file_with_id)
			.append(" and file_with_type=").append(file_with_type);
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorTransportMgrZr.getFileWithIdAndFileWithClass(tableName ,file_with_id ,file_with_type ):"+e);
		}
	}
	public DBRow[] getProductFileByWithIdAndWithTypeFileType(long file_with_id,
			int file_with_type, int product_file_type, PageCtrl pc) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("product_file");
			StringBuffer sql = new StringBuffer("select product_file.*,product.p_name,admin.employe_name from product_file ,product,admin")	
			.append(" where product.pc_id = product_file.pc_id  and  admin.adid = product_file.upload_adid and product_file_type=").append(product_file_type)
			.append(" and file_with_type=").append(file_with_type).append(" and file_with_id=").append(file_with_id)
			.append(" order by product_file.pc_id");
			return dbUtilAutoTran.selectMutliple(sql.toString(),pc);
		}catch (Exception e) {
			throw new Exception("FloorTransportMgrZr.getProductFileByWithIdAndWithTypeFileType(file_with_id ,file_with_type ,product_file_type,pc ):"+e);
		}
	}
	public DBRow[] getTransportLogs(long transport_id , int number ) throws Exception 
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from ").append("transport_logs ").append(" where transport_id =").append(transport_id).append(" order by logs_id desc limit 0,4;");
			return dbUtilAutoTran.selectMutliple(sql.toString()) ;
		}catch (Exception e)
		{
			throw new Exception("FloorTransportMgrZr.getTransportLogs(transport_id ,number):"+e);
		}
	}
	public DBRow[] getProductByPcIds(String ids) throws Exception {
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product") +" where 1=1";
			if(!"".equals(ids))
			{
				String[] idArr = ids.split(",");
				if(idArr.length > 0)
				{
					sql += " and (pc_id = " + idArr[0];
					for (int i = 1; i < idArr.length; i++) {
						sql += " or pc_id = " + idArr[i];
					}
					sql += ")";
				}
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e)
		{
			throw new Exception("FloorTransportMgrZr.getProductByPcIds(ids):"+e);
		}
	}
	public DBRow getNumberFileByFileWithId(String table ,long file_with_id , int file_with_type) throws Exception 
	{
		try
		{
			String sql = "select count(*) as sum_file from  "+table+" where file_with_id="+file_with_id + " and file_with_type="+file_with_type ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e)
		{
			throw new Exception("FloorTransportMgrZr.getAllFileByFileWithId(ids):"+e);
		}
	}
	public int getIndexOfFilebyFileName(String fileName) throws Exception
	{
		try
		{
			String tableName = "file";
			StringBuffer sql = new StringBuffer("select * from ").append(tableName).append(" where file_name like '")
			.append(fileName).append("%' order by file_id desc limit 0,1");
			DBRow fileRow = dbUtilAutoTran.selectSingle(sql.toString());
			if(fileRow == null)
			{	
				return 0;
			}else
			{
				String name = fileRow.getString("file_name");
				String[] tempArray = name.split("_");
				int index = 1 ;
				String stringIndex = tempArray[tempArray.length -1];
				if(stringIndex.indexOf(".") != -1){
					stringIndex = stringIndex.substring(0, stringIndex.indexOf(".") );
				}
				try
				{
					index = Integer.parseInt(stringIndex) + 1;
				}catch (Exception e) {
				}finally{}
				return index ;
			}	
		}catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZr.getIndexOfFilebyFileName(fileName):"+e);
		}
	}
	public int getIndexOfApplyMoneyImageByFileName(String fileName) throws Exception {
		try
		{
			String tableName = "apply_images";
			StringBuffer sql = new StringBuffer("select * from ").append(tableName).append(" where path like '")
			.append(fileName).append("%' order by id desc limit 0,1");
			DBRow fileRow = dbUtilAutoTran.selectSingle(sql.toString());
			if(fileRow == null)
			{	
				return 0;
			}else
			{
				String name = fileRow.getString("path");
				String[] tempArray = name.split("_");
				int index = 1 ;
				String stringIndex = tempArray[tempArray.length -1];
				if(stringIndex.indexOf(".") != -1){
					stringIndex = stringIndex.substring(0, stringIndex.indexOf(".") );
				}
				try
				{
					//第一次的重复的时候会出错
					index = Integer.parseInt(stringIndex) + 1;
				}catch (Exception e) {
				}finally{}
				return index ;
			}	
		}catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZr.getIndexOfApplyMoneyImageByFileName(fileName):"+e);
		}
	}
	public int getIndexOfProductFileByFileName(String fileName) throws Exception 
	{
		try
		{
			String tableName = ConfigBean.getStringValue("product_file");
			StringBuffer sql = new StringBuffer("select * from ").append(tableName).append(" where file_name like '")
			.append(fileName).append("%' order by pf_id desc limit 0,1");
			DBRow fileRow = dbUtilAutoTran.selectSingle(sql.toString());
			if(fileRow == null)
			{	
				return 0;
			}else
			{
				String name = fileRow.getString("file_name");
				String[] tempArray = name.split("_");
				int index = 1 ;
				String stringIndex = tempArray[tempArray.length -1];
				if(stringIndex.indexOf(".") != -1){
					stringIndex = stringIndex.substring(0, stringIndex.indexOf(".") );
				}
				try
				{
					index = Integer.parseInt(stringIndex) + 1;
				}catch (Exception e) {
				}finally{}
				return index ;
			}	
		}catch (Exception e) {
			throw new Exception("FloorTransportMgrZr.getIndexOfProductFileByFileName(fileName):"+e);
		}
	}
	public DBRow[] getSchedule() throws Exception {
		try{
			String sql = "select * from schedule where associate_process in(25,26)" ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorTransportMgrZr.getSchedule():"+e);
		}
	}
	public DBRow getApplyMoneny(long associate_id , int key , int type) throws Exception 
	{
		try
		{
			String sql = "select * from apply_money where association_id=" + associate_id + " and types="+ type + " and association_type_id="+key;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorTransportMgrZr.getApplyMoneny():"+e);
		}
	}
	public void updateSchdule(DBRow row , long schedule_id) throws Exception {
		try{
			dbUtilAutoTran.update(" where schedule_id="+schedule_id, "schedule", row);
		}catch (Exception e) {
			throw new Exception("FloorTransportMgrZr.updateSchdule():"+e);
		}
	}
	public DBRow[] getFilesOfProduct(long pc_id , long file_with_id,int file_with_type) throws Exception {
		try
		{
			StringBuffer sql = new StringBuffer("select * from product_file where pc_id=")
			.append(pc_id).append(" and file_with_type=").append(file_with_type).append(" and file_with_id=").append(file_with_id);
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorTransportMgrZr.getFilesOfProduct():"+e);
		}
	}
	
	/**
	 * 通过部门，仓库，职位查询
	 * @param proJsId
	 * @param ps_id
	 * @param group
	 * @return
	 * @throws Exception
	 * @author:chaolijuan
	 * @date: 2015年2月2日 下午6:49:51
	 */
	public DBRow[] getAllUserAndDept(long proJsId, long ps_id, long group) throws Exception{
		try
		{
			StringBuffer sql = new StringBuffer(
					"select DISTINCT ad.department_id adgid,ag.name name,a.adid adid,a.employe_name employe_name from admin a ");
			sql.append(" inner join admin_department ad on a.adid = ad.adid inner join admin_group ag on ad.department_id = ag.adgid ");
			sql.append(" INNER JOIN admin_warehouse aw on aw.adid = a.adid");
			sql.append("  where llock = 0 and LENGTH(employe_name)>0");
			if (ps_id > 0L) {
//				sql.append(" and ps_id=").append(ps_id);
				sql.append(" AND aw.warehouse_id = ").append(ps_id);
			}
			if (group > 0L) {
				sql.append(" and ag.adgid=").append(group);
			}
			if ((proJsId > 0L) && (proJsId != 15L)) {
				sql.append(" and ad.post_id=").append(proJsId);
			}
			if (proJsId == 15L) {
				sql.append(" and ad.post_id >= ").append(5);
			}
			sql.append(" order by ag.adgid, a.adid ");
			//System.out.println(sql);
			return dbUtilAutoTran.selectMutliple(sql.toString());
 		}catch (Exception e) {
			throw new Exception("FloorTransportMgrZr.getAllUserAndDept():"+e);
		}
	}

	
}
