package com.cwc.app.floor.api;

import java.io.Writer;
import java.util.ArrayList;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.cwc.app.util.StrUtil;

public class FloorSystemConfig
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBRow getConfigValueByName(String confname)
		throws Exception
	{
		try
		{
			if ( confname==null || confname.equals("") )
			{
				return(new DBRow());
			}
			String sql = "select * from " + ConfigBean.getStringValue("config") + " where confname='" + confname + "'";
			
			return(dbUtilAutoTran.selectSingle(sql));
		}
		catch (Exception e)
		{
			throw new Exception("FloorSystemConfig.getConfigValueByName(row) error:" + e);
		}
	}
	
	public DBRow[] getAllConfig(PageCtrl pc)
		throws Exception
	{
		try
		{
			
			DBRow row[] = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("config"));		
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorSystemConfig.getAllConfig(row) error:" + e);
		}
	}

	public void modifySystemConfig(String confname,DBRow row)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.update("where confname='" + confname + "'",ConfigBean.getStringValue("config"),row);	
		}
		catch (Exception e)
		{
			throw new Exception("FloorSystemConfig.modifySystemConfig(row) error:" + e);
		}
	}
	
	public DBRow[] getTableData(String tableName)
		throws Exception
	{
		try
		{
			
			DBRow rows[] = dbUtilAutoTran.select(tableName);
			return(rows);
		}
		catch (Exception e)
		{
			throw new Exception("getBackupData(row) error:" + e);
		}
	}
	
	public String getTableDataSQL(String tableName)
		throws Exception
	{
		int i,j;
		String paraNames,paraValues;
		String valueStr = null;
		ArrayList fieldNames = new ArrayList();
		StringBuffer beWriteStr = new StringBuffer("");
		beWriteStr.append("delete from "+tableName+";\n");
		beWriteStr.append("LOCK TABLES "+tableName+" WRITE;\n");
	
		try {
			DBRow row[] = getTableData(tableName);
			for ( i=0; i<row.length; i++ )
			{
				StringBuffer strBuf1 = new StringBuffer("insert into " + tableName + " (");
				StringBuffer strBuf2 = new StringBuffer(") values(");
				StringBuffer colName = new StringBuffer("");
			
				fieldNames = row[i].getFieldNames();
				for ( j=0; j<fieldNames.size()-1; j++ )
				{
					colName.append(fieldNames.get(j).toString().toLowerCase());
					colName.append(",");
			
					strBuf2.append("'");
					valueStr = row[i].getString(fieldNames.get(j).toString());
					valueStr = handleChar(valueStr);
					strBuf2.append(valueStr);
					strBuf2.append("', ");
				}
				colName.append(fieldNames.get(j).toString().toLowerCase());
			
				strBuf2.append("'");
				strBuf2.append(handleChar( row[i].getString(fieldNames.get(j).toString()) ));
				strBuf2.append("'");
				strBuf2.append(");\n");
			
				beWriteStr.append(strBuf1.toString() + colName.toString() + strBuf2.toString());
				
				strBuf1 = null;
				strBuf2 = null;
				colName = null;
			}
			beWriteStr.append("UNLOCK TABLES;\n");
		}
		catch (Exception e)
		{
			throw new Exception("getBeWriteStr(row) error:" + e);
		}
		
		return(beWriteStr.toString());
	}
	
	public void getTableDataSQL(String tableName,Writer out)
		throws Exception
	{
		
		
		int i,j;
		String paraNames,paraValues;
		String valueStr = null;
		ArrayList fieldNames = new ArrayList();
		//StringBuffer beWriteStr = new StringBuffer("");
		out.write(dbUtilAutoTran.getMysqlCreateTableSQL(tableName));
		//out.write("LOCK TABLES "+tableName+" WRITE;\n");
	
		try {
			DBRow row[] = getTableData(tableName);
			for ( i=0; i<row.length; i++ )
			{
				StringBuffer strBuf1 = new StringBuffer("insert into " + tableName + " (");
				StringBuffer strBuf2 = new StringBuffer(") values(");
				StringBuffer colName = new StringBuffer("");
			
				fieldNames = row[i].getFieldNames();
				for ( j=0; j<fieldNames.size()-1; j++ )
				{
					colName.append(fieldNames.get(j).toString().toLowerCase());
					colName.append(",");
			
					strBuf2.append("'");
					valueStr = row[i].getString(fieldNames.get(j).toString());
					valueStr = handleChar(valueStr);
					strBuf2.append(valueStr);
					strBuf2.append("', ");
				}
				colName.append(fieldNames.get(j).toString().toLowerCase());
			
				strBuf2.append("'");
				strBuf2.append(handleChar( row[i].getString(fieldNames.get(j).toString()) ));
				strBuf2.append("'");
				strBuf2.append(");\n");
			
				out.write(strBuf1.toString() + colName.toString() + strBuf2.toString());
				
				strBuf1 = null;
				strBuf2 = null;
				colName = null;
			}
			//out.write("UNLOCK TABLES;\n");
		}
		catch (Exception e)
		{
			throw new Exception("getBeWriteStr(row) error:" + e);
		}
		
		//return(beWriteStr.toString());
	}
	
	private String handleChar(String c)
	{
		c = StrUtil.replaceString(c,"\n","\\n");
		c = StrUtil.replaceString(c,"\r","\\r");
		c = StrUtil.replaceString(c,"\"","\"");
		c = StrUtil.replaceString(c,"'","\\'");
		
		return(c);
	}
	
	public ArrayList getAllTables()
		throws Exception
	{
		try
		{
			
			return(dbUtilAutoTran.getAllTables());
		} 
		catch (Exception e)
		{
			throw new Exception("getAllTables(row) error:" + e);
		}
	}
	
	public String getAllTableDataSQL()
		throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			
			ArrayList al = dbUtilAutoTran.getAllTables();
			
			for (int i=0; i<al.size(); i++)
			{
				sb.append( getTableDataSQL(al.get(i).toString()) );
			}
			
			return(sb.toString());
		}
		catch (Exception e)
		{
			throw new Exception("getAllTableDataSQL(row) error:" + e);
		}
	}
	
	public void batchUpdate(String sql[])
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.batchUpdate(sql);
		} 
		catch (Exception e)
		{
			throw new Exception("batchUpdate(row) error:" + e);
		}
	}
	
	public DBRow[] getAllTemplate()
		throws Exception
	{
		try
		{
			
			DBRow rows[] = dbUtilAutoTran.selectMutliple("select * from "+ConfigBean.getStringValue("template") + " order by used desc");
			return(rows);
		}
		catch (Exception e)
		{
			throw new Exception("getAllTemplate(row) error:" + e);
		}
	}
	
	public DBRow getUsedTemplate()
		throws Exception
	{
		try
		{
//			DBRow rows = dbUtilAutoTran.selectSingleCache("select * from "+ConfigBean.getStringValue("template")+" where used=1",new String[]{ConfigBean.getStringValue("template")});
			DBRow rows = dbUtilAutoTran.selectSingle("select * from "+ConfigBean.getStringValue("template")+" where used=1");
			return(rows);
		}
		catch (Exception e)
		{
			throw new Exception("getUsedTemplate(row) error:" + e);
		}
	}
	
	public void addTemplate(DBRow row)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("template"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorSystemConfig.addTemplate(row) error:" + e);
		}
	}
	
	public void delAllTemplate()
		throws Exception
	{
		try
		{
				
			dbUtilAutoTran.delete("where 1=1",ConfigBean.getStringValue("template"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorSystemConfig.delAllTemplate(row) error:" + e);
		}
	}

	public void modTemplateByName(String name,DBRow row)
		throws Exception
	{
		try
		{
				
			dbUtilAutoTran.update("where name='"+name+"'",ConfigBean.getStringValue("template"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorSystemConfig.modTemplateByName(row) error:" + e);
		}
	}

	public void addSysLog(DBRow row)
		throws Exception
	{
		try
		{
//			long sl_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("sys_log"));
//			row.add("sl_id", sl_id);
//			dbUtilAutoTran.insert(ConfigBean.getStringValue("sys_log"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorSystemConfig.addSysLog(row) error:" + e);
		}
	}

	public DBRow[] getSysLogsByAdidDate(long st_datemark,long en_datemark,long adid,int operate_type,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("adid",adid);
			para.add("st_datemark",st_datemark);
			para.add("en_datemark",en_datemark);
			
			String sql = "select * from "+ConfigBean.getStringValue("sys_log") + " sl left join "+ConfigBean.getStringValue("admin_group") + " ag on sl.adgid=ag.adgid  where sl.adid=? and sl.datemark>=? and sl.datemark<=? order by sl.sl_id desc";
			
			if (operate_type>0)
			{
				para.add("operate_type",operate_type);
				sql = "select * from "+ConfigBean.getStringValue("sys_log") + " sl left join "+ConfigBean.getStringValue("admin_group") + " ag on sl.adgid=ag.adgid where sl.adid=? and sl.datemark>=? and sl.datemark<=? and sl.operate_type=? order by sl.sl_id desc";
			}
			DBRow rows[] = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			return(rows);
		}
		catch (Exception e)
		{
			throw new Exception("getSysLogsByAdidDate(row) error:" + e);
		}
	}

	public DBRow[] getSysLogsByAdgidDate(long st_datemark,long en_datemark,long adgid,int operate_type,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("adgid",adgid);
			para.add("st_datemark",st_datemark);
			para.add("en_datemark",en_datemark);
			
			String sql = "select * from "+ConfigBean.getStringValue("sys_log") + " sl left join "+ConfigBean.getStringValue("admin_group") + " ag on sl.adgid=ag.adgid where sl.adgid=? and sl.datemark>=? and sl.datemark<=? order by sl.sl_id desc";
			
			if (operate_type>0)
			{
				para.add("operate_type",operate_type);
				sql = "select * from "+ConfigBean.getStringValue("sys_log") + " sl left join "+ConfigBean.getStringValue("admin_group") + " ag on sl.adgid=ag.adgid where sl.adgid=? and sl.datemark>=? and sl.datemark<=? and sl.operate_type=? order by sl.sl_id desc";
			}
			
			DBRow rows[] = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			return(rows);
		}
		catch (Exception e)
		{
			throw new Exception("getSysLogsByAdgidDate(row) error:" + e);
		}
	}
	
	public DBRow[] getAllSysLogsByDate(long st_datemark,long en_datemark,int operate_type,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("st_datemark",st_datemark);
			para.add("en_datemark",en_datemark);
			
			String sql = "select * from "+ConfigBean.getStringValue("sys_log") + " sl left join "+ConfigBean.getStringValue("admin_group") + " ag on sl.adgid=ag.adgid where sl.datemark>=? and sl.datemark<=? order by sl.sl_id desc";
			
			if (operate_type>0)
			{
				para.add("operate_type",operate_type);
				sql = "select * from "+ConfigBean.getStringValue("sys_log") + " sl left join "+ConfigBean.getStringValue("admin_group") + " ag on sl.adgid=ag.adgid where sl.datemark>=? and sl.datemark<=? and sl.operate_type=? order by sl.sl_id desc";
			}
			
			DBRow rows[] = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			return(rows);
		}
		catch (Exception e)
		{
			throw new Exception("getAllSysLogsByDate(row) error:" + e);
		}
	}

	public DBRow getDetailSysLogBySlid(long slid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("slid",slid);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("sys_log") + " where sl_id=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailSysLogBySlid(row) error:" + e);
		}
	}
		
	
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
