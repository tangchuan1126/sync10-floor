package com.cwc.app.floor.api.zwb;

import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.key.FundStatusKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorPreparePurchaseFundsMgrZwb {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	//添加预申请
	public long addApplyMoney(DBRow row)
	throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance("prepare_purchase_funds");
			row.add("apply_id",id);
	
		    dbUtilAutoTran.insert("prepare_purchase_funds",row);
		    return (id);
		}
		catch (Exception e)
		{
			throw new Exception("FloorPreparePurchaseFundsMgrZwb.addApplyMoney(row) error:" + e);
		}
	}
	
	//根据采购单id查询 预申请表 是否申请过   预申请只能申请一次
	public DBRow getPreparePurchaseFunds(long purchaseId)
		throws Exception
	{
		try
		{
			String sql="select * from prepare_purchase_funds where association_id="+purchaseId;
			return this.dbUtilAutoTran.selectSingle(sql);
		}
		catch(Exception e)
		{
			throw new Exception("FloorPreparePurchaseFundsMgrZwb.getPreparePurchaseFunds(purchaseId) error:" + e);
		}
	}
	//根据id 查询预申请
	public DBRow getByApplyMoney(long id)throws Exception{
		try{		
			return this.dbUtilAutoTran.selectSingle("select * from prepare_purchase_funds where apply_id="+id);
		}catch(Exception e){
			throw new Exception("FloorPreparePurchaseFundsMgrZwb.updateApplyMoney(id) error:" + e);
		}
	}
	
	//根据id删除预申请
	public long detApplyMoney(long id)throws Exception{
		try{
			return dbUtilAutoTran.delete("where apply_id="+id,"prepare_purchase_funds");
		}catch(Exception e){
			throw new Exception("FloorPreparePurchaseFundsMgrZwb.detApplyMoney(id) error:" + e);
		}
	}
	
	//根据id更新预申请
	public long updateApplyMoney(long id,DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.update("where apply_id="+id, "prepare_purchase_funds", row);
		}catch(Exception e){
			throw new Exception("FloorPreparePurchaseFundsMgrZwb.detApplyMoney(id) error:" +e);
		}
	}
	
	
	//添加定金
	public long addPuerchaseDeposit(DBRow row)
	throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance("apply_money");
			row.add("apply_id",id);
	
		    dbUtilAutoTran.insert("apply_money",row);
		    return (id);
		}
		catch (Exception e)
		{
			throw new Exception("FloorPreparePurchaseFundsMgrZwb.addPuerchaseDeposit(row) error:" + e);
		}
	}
	
	//根据采购单id 查询是否申请了定金
	public long findPuerchaseDeposit(long purchaseId)throws Exception{
		try{
			String sql="select * from apply_money where association_id="+purchaseId+" and association_type_id=4 and types=100009";
			DBRow row=dbUtilAutoTran.selectSingle(sql);
			if(row==null){
				return 0;
			}else{
				return Integer.parseInt(row.getString("association"));
			}
		}catch(Exception e){
			throw new Exception("FloorPreparePurchaseFundsMgrZwb.findPuerchaseDeposit(purchaseId) error:" + e);
		}
	}
	
	
	//查询所有预申请
	 public DBRow[] getAllPreparePurchase(PageCtrl pc)
	    throws Exception
	    {
	    	try
			{
				String sql = "select * from prepare_purchase_funds order by apply_id DESC";
				
				if(pc!=null)
				{
					return (dbUtilAutoTran.selectMutliple(sql,pc));
				}
				else
				{
					
					return (dbUtilAutoTran.selectMutliple(sql));
				}
			}
			catch (Exception e)
			{
				throw new Exception("FloorPreparePurchaseFundsMgrZwb.getApplyMoney(pc) error:" + e);
			}
	    	
	    }
	 
	 
	 /**
	     * 根据类型、创建时间、状态查询资金申请
	     * @param startTime
	     * @param endTime
	     * @param types
	     * @param status
	     * @param pc
	     * @return
	     * @throws Exception
	     */
	    public DBRow[] getPreparePurchase(String startTime,String endTime,int types,int status,long productLineId,long centerAccountId,long association_type_id, PageCtrl pc)
	    throws Exception
	    {
	    	try
	    	{
	    		StringBuffer sqlCondition=new StringBuffer();
		    	sqlCondition.append("where 1=1");
		    	if(!startTime.equals("")) {
		    		sqlCondition.append(" and create_time>'"+startTime+" 00:00:00'");
		    	}
		    	if(!endTime.equals("")) {
		    		sqlCondition.append(" and create_time<'"+endTime+" 23:59:59'");
		    	}
		    	if(types!=0)
		    	{
		    		sqlCondition.append(" and types in (select category_id from "+ConfigBean.getStringValue("apply_money_category")+" where category_id="+types+" or prent_id="+types+")");
		    	}
		    	if(status!=-1)
		    	{
		    		sqlCondition.append(" and status="+status);
		    	}
		    	if(productLineId!=0) {
		    		sqlCondition.append(" and product_line_id="+productLineId);
		    	}
		    	if(centerAccountId!=0) {
		    		sqlCondition.append(" and center_account_id="+centerAccountId);
		    	}
		    	if(association_type_id!=-1)
		    	{
		    		sqlCondition.append(" and association_type_id="+association_type_id);
		    	}
	    		String sql = "select * from prepare_purchase_funds "+sqlCondition.toString()+" order by apply_id DESC ";
	    		
				if(pc!=null)
				{
					return (dbUtilAutoTran.selectMutliple(sql,pc));
				}
				else
				{
					
					return (dbUtilAutoTran.selectMutliple(sql));
				}
	    	}
	    	catch(Exception e)
	    	{
	    		throw new Exception("FloorPreparePurchaseFundsMgrZwb.getApplyMoneyByCondition(sql)"+e);
	    	}
	    }

	
	public DBRow[] getApplyMoneyByAssIdAndTypeHasApply(long association_id , long type) throws Exception {
		try
		{
			String sql = "select * from apply_money  where association_id ="+association_id + " and types="+type + " and status !="+FundStatusKey.CANCEL;
			return dbUtilAutoTran.selectMutliple(sql);
			 
		}
		catch (Exception e)
		{
			throw new Exception("FloorPreparePurchaseFundsMgrZwb.getApplyMoneyByAssIdAndType(pc) error:" + e);
		}
    	
	}
	public DBRow[] getApplyMoneysOfPurchase(long purchase_id) throws Exception {
		try
		{
			StringBuffer sql = new StringBuffer("select apply_money.amount , apply_money.standard_money from apply_money left join transport on transport.transport_id = association_id where ").append("association_type_id =").append(FinanceApplyTypeKey.DELIVERY_ORDER).append(" and types = 100001 and transport.purchase_id =")
			.append(purchase_id);
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorPreparePurchaseFundsMgrZwb.getApplyMoneysOfPurchase(purchase_id) error:" + e);
		}
		
	}
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	

}
