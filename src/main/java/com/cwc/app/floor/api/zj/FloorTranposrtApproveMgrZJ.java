package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorTranposrtApproveMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	/**
	 * 添加交货单审核
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addTransportApprove(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long ta_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("transport_approve"));
			
			dbrow.add("ta_id",ta_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("transport_approve"),dbrow);
			
			return (ta_id);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTranposrtApproveMgrZJ addTransportApprove error:"+e);
		}
	}
	
	/**
	 * 添加转运审核详细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addTransportApproveDetail(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long tad_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("transport_approve_detail"));
			
			dbrow.add("tad_id",tad_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("transport_approve_detail"),dbrow);
			
			return tad_id;
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTranposrtApproveMgrZJ addTransportApproveDetail error:"+e);
		}
	}
	
	/**
	 * 修改转运单审核
	 * @param ta_id
	 * @param para
	 * @throws Exception
	 */
	public void modTransportApprove(long ta_id,DBRow para)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where ta_id="+ta_id,ConfigBean.getStringValue("transport_approve"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTranposrtApproveMgrZJ modTransportApprove error:"+e);
		}
	}
	
	/**
	 * 修改转运单审核明细
	 * @param tad_id
	 * @param para
	 * @throws Exception
	 */
	public void modTransportApproveDetail(long tad_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where tad_id="+tad_id,ConfigBean.getStringValue("transport_approve_detail"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTranposrtApproveMgrZJ modTransportApproveDetail error:"+e);
		}
	}
	
	/**
	 * 过滤转运单审核
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param approve_status
	 * @param sorttype
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterTransportApprove(long send_psid, long receive_psid,PageCtrl pc, int approve_status, String sorttype)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select ta.*,send.title AS sendtitle,receive.title AS receivetitle from "+ConfigBean.getStringValue("transport_approve")+" as ta"
												+" left join "+ConfigBean.getStringValue("transport")+" as t on t.transport_id = ta.transport_id"
												+" left join "+ConfigBean.getStringValue("product_storage_catalog")+" as send on send.id = t.send_psid"
												+" left join "+ConfigBean.getStringValue("product_storage_catalog")+" as receive on receive.id = t.receive_psid"
												+" where 1=1");
			if(send_psid>0)
			{
				sql.append(" and t.send_psid = "+send_psid);
			}
			if(receive_psid>0)
			{
				sql.append(" and t.receive_psid = "+receive_psid);
			}
			
			if(approve_status>0)
			{
				sql.append(" and ta.approve_status = "+approve_status);
			}
			
			if(sorttype.equals("subdate"))
			{
				sql.append(" order by ta.commit_date desc");
			}
			else if(sorttype.equals("approvedate"))
			{
				sql.append(" order by ta.approve_date desc");
			}
			else
			{
				sql.append(" order by ta.ta_id desc");
			}
			return dbUtilAutoTran.selectMutliple(sql.toString(),pc);
			
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTranposrtApproveMgrZJ fillterTransportApprove error:"+e);
		}
	}
	
	/**
	 * 根据审核ID，获得审核明细
	 * @param ta_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApproveDetailByTaid(long ta_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_approve_detail")+" where ta_id = ?";
			
			DBRow para = new DBRow();
			para.add("ta_id",ta_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTranposrtApproveMgrZJ fillterTransportApprove error:"+e);
		}
	}
	
	/**
	 * 根据审核状态获得审核明细
	 * @param ta_id
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApproveTransportDetailWihtStatus(long ta_id,int status)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_approve_detail")+" where ta_id = ? and approve_status=?";
			
			DBRow para = new DBRow();
			para.add("ta_id",ta_id);
			para.add("approve_status",status);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTranposrtApproveMgrZJ getApproveTransportDetailWihtStatus error:"+e);
		}
	}
	
	/**
	 * 获得转运审核详细
	 * @param ta_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailApproveTransportById(long ta_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_approve")+" where ta_id=?";
			
			DBRow para = new DBRow();
			para.add("ta_id",ta_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTranposrtApproveMgrZJ getDetailApproveTransportById error:"+e);
		}
	}
	
	/**
	 * 添加转运到货不同SN级
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addTransportApproveDetailSN(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("transport_approve_detail_sn"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTranposrtApproveMgrZJ addTransportApproveDetailSN error:"+e);
		}
	}
	
	/**
	 * 修改转运到货序列号差异
	 * @param tads_id
	 * @param para
	 * @throws Exception
	 */
	public void modTransportApproveDetailSN(long tads_id,DBRow para)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where tads_id="+tads_id,ConfigBean.getStringValue("transport_approve_detail_sn"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTranposrtApproveMgrZJ modTransportApproveDetailSN error:"+e);
		}
	}
	
	/**
	 * 获得到货审核序列号差异
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportApproveDetailSNByTaId(long ta_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_approve_detail_sn")+" where ta_id = ? ";
			
			DBRow para = new DBRow();
			para.add("ta_id",ta_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTranposrtApproveMgrZJ getTransportApproveDetailSNByTransportId error:"+e);
		}
	}
	
	/**
	 * 根据状态过滤收货审核序列号差异
	 * @param ta_id
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApproveTransportDetailSNWihtStatus(long ta_id,int status)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_approve_detail_sn")+" where ta_id = ? and approve_status=?";
			
			DBRow para = new DBRow();
			para.add("ta_id",ta_id);
			para.add("approve_status",status);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTranposrtApproveMgrZJ getApproveTransportDetailWihtStatus error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
