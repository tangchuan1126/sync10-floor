package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorCheckReturnProductItemMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public long addCheckReturnProductItem(DBRow row) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("return_product_items_zr_check");
		 
			 return dbUtilAutoTran.insertReturnId(tableName, row);
 		}catch(Exception e){
			throw new Exception("FloorCheckReturnProductItemMgrZr.addCheckReturnProductItem(row):"+e);
		}
	}
	public void updateCheckReturnProductItem(long check_return_product_id , DBRow row) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("return_product_items_zr_check");
			dbUtilAutoTran.update(" where check_return_product_id=" + check_return_product_id, tableName, row);
		}catch (Exception e) {
			throw new Exception("FloorCheckReturnProductItemMgrZr.updateCheckReturnProductItem(check_return_product_id,row):"+e);
		}
	}
	public DBRow[] getCheckReturnProductItem(long rpi_id) throws Exception {
		try{
			StringBuffer sql = new StringBuffer("select * from ").append(ConfigBean.getStringValue("return_product_items_zr_check"))
			.append(" where rpi_id=").append(rpi_id);
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorCheckReturnProductItemMgrZr.getCheckReturnProductItem(rpi_id):"+e);
		}
	}
	public DBRow getCheckRetutrnProductItemById(long check_return_product_id) throws Exception {
		try{
			
			StringBuffer sql = new StringBuffer("select * from " ).append(ConfigBean.getStringValue("return_product_items_zr_check"))
			.append(" where check_return_product_id=").append(check_return_product_id);
			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorCheckReturnProductItemMgrZr.getCheckRetutrnProductItemById(check_return_product_id):"+e);
		}
	}
}
