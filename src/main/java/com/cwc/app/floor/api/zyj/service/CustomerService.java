package com.cwc.app.floor.api.zyj.service;

import java.util.List;

import com.cwc.app.floor.api.zyj.model.Customer;
import com.cwc.app.floor.api.zyj.model.Title;

/**
 * 该接口定义了访问Customer的各种方法
 * @author lujintao
 *
 */
public interface CustomerService {
	/**
	 * 获取所有的品牌商
	 * @return
	 */
	public List<Customer> getAll() ;
	
	/**
	 * 获取所有的品牌商
	 * @return
	 */
	public Customer getCustomer(int id) ;
	
	/**
	 * 获取与特定产品相关的所有Customer
	 * @param productId 产品ID
	 * @return
	 */
	public List<Customer>  getCustomers(int productId);
	
	
	/**
	 * 获取与特定product和title相关的所有Customer
	 * @param productId 产品ID
	 * @param titleId   
	 * @return
	 */
	public List<Customer>  getCustomers(int productId,int titleId);
	
	/**
	 * 获取与指定产品和CLP Type 的所有Title
	 * @param product
	 * @param clpTypeId
	 * @return
	 */
	public List<Customer> getCustomersByProdAndCLP(int productId,int clpTypeId);
	
	/**
	 * 获取与指定产品和CLP Type 的所有Title
	 * @param product
	 * @param clpTypeId
	 * @param titleId
	 * @return
	 */
	public List<Customer> getCustomersByProdAndCLP(int productId,int clpTypeId,int titleId);
}
