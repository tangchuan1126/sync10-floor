package com.cwc.app.floor.api.zj;

import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorOutboundOrderMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	/**
	 * 针对仓库计算补偿值
	 * @param ps_id
	 * @param status
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] compensationValueForPs(long ps_id,int[] status,long pc_id)
		throws Exception
	{
		String whereStatus = "";
		if(status.length>0)
		{
			whereStatus += " and (";
			for (int i = 0; i < status.length; i++) 
			{
				whereStatus += "wo.status="+status[i];
				
				if(i!=(status.length-1))
				{
					whereStatus+=" or ";
				}
			}
			whereStatus +=")";
		}
		
		String whereProduct = "";
		if (pc_id>0)
		{
			whereProduct = " and p.pc_id = "+pc_id+" ";
		}
		
		String sql = "select pc_id,p_name,sum(quantity) as compensationValue FROM(" 
					+"select p.pc_id,p.p_name,sum(woi.quantity) as quantity from waybill_order as wo "
					+"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id and (woi.product_type =1 or woi.product_type=2) "
					+"join product as p on woi.pc_id = p.pc_id "+whereProduct 
					+"where wo.ps_id="+ps_id+whereStatus
					+"group by p.pc_id "
					+"UNION "
					+"SELECt p.pc_id, p.p_name,sum(woi.quantity*pu.quantity) as quantity FROM waybill_order as wo "
					+"JOIN waybill_order_item woi  on wo.waybill_id = woi.waybill_order_id and (woi.product_type=3 or woi.product_type=4) "
					+"JOIN product_union as pu ON woi.pc_id = pu.set_pid "
					+"JOIN product as p ON pu.pid = p.pc_id "+whereProduct
					+"where wo.ps_id="+ps_id+whereStatus
					+"group by p.pc_id "
					+") as compensationValue GROUP BY pc_id";
		
		return (dbUtilAutoTran.selectMutliple(sql));
	}
	
	/**
	 * 根据仓库，状态，商品名查看补偿值
	 * @param ps_id
	 * @param status
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow compensationValueForPsAndP(long ps_id,int[] status,long pc_id)
		throws Exception
	{
		String whereStatus = "";
		if(status.length>0)
		{
			whereStatus += " and (";
			for (int i = 0; i < status.length; i++) 
			{
				whereStatus += "wo.status="+status[i];
				
				if(i!=(status.length-1))
				{
					whereStatus+=" or ";
				}
			}
			whereStatus +=")";
		}
		
		String whereProduct = "";
		if (pc_id>0)
		{
			whereProduct = " and p.pc_id = "+pc_id+" ";
		}
		
		String sql = "select pc_id,p_name,sum(quantity) as compensationValue FROM(" 
					+"select p.pc_id,p.p_name,sum(woi.quantity) as quantity from waybill_order as wo "
					+"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id and (woi.product_type =1 or woi.product_type=2) and woi.product_status ="+ProductStatusKey.IN_STORE+" "
					+"join product as p on woi.pc_id = p.pc_id "+whereProduct 
					+"where wo.ps_id="+ps_id+whereStatus
					+"UNION "
					+"SELECt p.pc_id, p.p_name,sum(woi.quantity*pu.quantity) as quantity FROM waybill_order as wo "
					+"JOIN waybill_order_item woi  on wo.waybill_id = woi.waybill_order_id and (woi.product_type=3 or woi.product_type=4) and woi.product_status ="+ProductStatusKey.IN_STORE+" "
					+"JOIN product_union as pu ON woi.pc_id = pu.set_pid "
					+"JOIN product as p ON pu.pid = p.pc_id "+whereProduct
					+"where wo.ps_id="+ps_id+whereStatus
					+") as compensationValue GROUP BY pc_id";
		
		
		return (dbUtilAutoTran.selectSingle(sql));
	}
	
	/**
	 * 根据仓库，状态，商品名查看出库值
	 * @param ps_id
	 * @param status
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow outStoreValueForPsAndP(long ps_id,int[] status,long pc_id)
		throws Exception
	{
		String whereStatus = "";
		if(status.length>0)
		{
			whereStatus += " and (";
			for (int i = 0; i < status.length; i++) 
			{
				whereStatus += "wo.status="+status[i];
				
				if(i!=(status.length-1))
				{
					whereStatus+=" or ";
				}
			}
			whereStatus +=")";
		}
		
		String whereProduct = "";
		if (pc_id>0)
		{
			whereProduct = " and p.pc_id = "+pc_id+" ";
		}
		
		String sql = "select pc_id,p_name,sum(quantity) as outStoreValue FROM(" 
					+"select p.pc_id,p.p_name,sum(woi.quantity) as quantity from waybill_order as wo "
					+"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id and (woi.product_type =1 or woi.product_type=2) "
					+"join product as p on woi.pc_id = p.pc_id "+whereProduct 
					+"where wo.ps_id="+ps_id+whereStatus
					+"UNION "
					+"SELECt p.pc_id, p.p_name,sum(woi.quantity*pu.quantity) as quantity FROM waybill_order as wo "
					+"JOIN waybill_order_item woi  on wo.waybill_id = woi.waybill_order_id and (woi.product_type=3 or woi.product_type=4) "
					+"JOIN product_union as pu ON woi.pc_id = pu.set_pid "
					+"JOIN product as p ON pu.pid = p.pc_id "+whereProduct
					+"where wo.ps_id="+ps_id+whereStatus
					+") as compensationValue GROUP BY pc_id";
		
		return (dbUtilAutoTran.selectSingle(sql));
	}
	
	public DBRow[] getOutOrderItem(long out_id,int[] status)
		throws Exception
	{
		String whereStatus = "";
		if(status.length>0)
		{
			whereStatus += " and (";
			for (int i = 0; i < status.length; i++) 
			{
				whereStatus += "wo.status="+status[i];
				
				if(i!=(status.length-1))
				{
					whereStatus+=" or ";
				}
			}
			whereStatus +=")";
		}
		
		
		String sql = "select pc_id,p_name,catalog_id,sum(quantity) as compensationValue FROM(" 
					+"select p.pc_id,p.p_name,p.catalog_id,sum(woi.quantity) as quantity from waybill_order as wo "
					+"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id and (woi.product_type =1 or woi.product_type=2) "
					+"join product as p on woi.pc_id = p.pc_id "
					+"where wo.out_id="+out_id+whereStatus
					+"group by p.pc_id "
					+"UNION ALL "
					+"select p.pc_id, p.p_name,p.catalog_id,sum(woi.quantity*pu.quantity) as quantity FROM waybill_order as wo "
					+"JOIN waybill_order_item woi  on wo.waybill_id = woi.waybill_order_id and (woi.product_type=3 or woi.product_type=4) "
					+"JOIN product_union as pu ON woi.pc_id = pu.set_pid "
					+"JOIN product as p ON pu.pid = p.pc_id "
					+"where wo.out_id="+out_id+whereStatus
					+"group by p.pc_id "
					+") as compensationValue GROUP BY pc_id order by catalog_id,p_name";
		
		
		return (dbUtilAutoTran.selectMutliple(sql));
	}
	
	/**
	 * 获得出库单货物地址
	 * @param out_id
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutOrderItemLocation(long out_id,int[] status)
		throws Exception
	{
		String whereStatus = "";
		if(status.length>0)
		{
			whereStatus += " and (";
			for (int i = 0; i < status.length; i++) 
			{
				whereStatus += "wo.status="+status[i];
				
				if(i!=(status.length-1))
				{
					whereStatus+=" or ";
				}
			}
			whereStatus +=")";
		}
		
		
		String sql ="select sl.position,p_name,sl.quantity,compensationValue from " 
			+"(" 
			+"select pc_id,p_name,catalog_id,ps_id,sum(quantity) as compensationValue FROM(" 
			+"select p.pc_id,p.p_name,p.catalog_id,wo.ps_id,sum(woi.quantity) as quantity from waybill_order as wo "
			+"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id and (woi.product_type =1 or woi.product_type=2) "
			+"join product as p on woi.pc_id = p.pc_id "
			+"where wo.out_id="+out_id+whereStatus
			+"group by p.pc_id "
			+"UNION ALL "
			+"select p.pc_id, p.p_name,p.catalog_id,wo.ps_id,sum(woi.quantity*pu.quantity) as quantity FROM waybill_order as wo "
			+"JOIN waybill_order_item woi  on wo.waybill_id = woi.waybill_order_id and (woi.product_type=3 or woi.product_type=4) "
			+"JOIN product_union as pu ON woi.pc_id = pu.set_pid "
			+"JOIN product as p ON pu.pid = p.pc_id "
			+"where wo.out_id="+out_id+whereStatus
			+"group by p.pc_id "
			+") as compensationValue GROUP BY pc_id order by catalog_id,p_name"
			+") as outList "
			+"left join storage_location as sl on sl.pc_id = outList.pc_id and sl.ps_id = outList.ps_id "
			+"order by sl.position asc";
		
		
		return (dbUtilAutoTran.selectMutliple(sql));
	}
	
	
	
	/**
	 * 根据ID获得出货单明细
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailOutboundById(long out_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("out_storebill_order")+" where out_id=?";
		
		DBRow para = new DBRow();
		para.add("out_id",out_id);
		
		return (dbUtilAutoTran.selectPreSingle(sql, para));
	}
	
	/**
	 * 返回某个出库单下某个商品所属运单
	 * @param out_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWayBillIdsByOutIdAndP(long out_id,long pc_id,int[] status)
		throws Exception
	{
		String whereStatus = "";
		if(status.length>0)
		{
			whereStatus += " and (";
			for (int i = 0; i < status.length; i++) 
			{
				whereStatus += "wo.status="+status[i];
				
				if(i!=(status.length-1))
				{
					whereStatus+=" or ";
				}
			}
			whereStatus +=")";
		}
		
		
		String sql = "select distinct waybill_id from waybill_order as wo "
					+"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id and (woi.product_type =1 or woi.product_type=2) "
					+"join product as p on woi.pc_id = p.pc_id and p.pc_id = ?"
					+"where wo.out_id= ? "+whereStatus
					+"UNION "
					+"select distinct waybill_id FROM waybill_order as wo "
					+"JOIN waybill_order_item woi  on wo.waybill_id = woi.waybill_order_id and (woi.product_type=3 or woi.product_type=4) "
					+"JOIN product_union as pu ON woi.pc_id = pu.set_pid "
					+"JOIN product as p ON pu.pid = p.pc_id and p.pc_id = ? "
					+"where wo.out_id= ? "+whereStatus;
		
		DBRow para = new DBRow();
		para.add("pc_id",pc_id);
		para.add("out_id",out_id);
		para.add("pc_id2",pc_id);
		para.add("out_id2",out_id);
		
		return (dbUtilAutoTran.selectPreMutliple(sql, para));
	}
	
	public void modOutboundOrder(long out_id,DBRow para)
		throws Exception
	{
		dbUtilAutoTran.update("where out_id = "+out_id,ConfigBean.getStringValue("out_storebill_order"),para);
	}
	
	/**
	 * 拣货单详细整合
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] summarizeWaybillOutListDetail(long out_id)
		throws Exception
	{
		try 
		{
			String sql = "select slc.slc_position_all,out_list_slc_id as slc_id,out_list_pc_id as pc_id,sum(pick_up_quantity) as quantity,slc.slc_area from "+ConfigBean.getStringValue("out_list_detail")+" as old "
						+"join "+ConfigBean.getStringValue("waybill_order")+" as wo on wo.waybill_id = old.system_bill_id and wo.out_id = ? and old.system_bill_type = ?"
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on slc.slc_id = old.out_list_slc_id "
						+"group by out_list_pc_id,out_list_slc_id order by slc.slc_position_all asc ";
			
			DBRow para = new DBRow();
			para.add("out_id",out_id);
			para.add("system_bill_type",ProductStoreBillKey.WAYBILL_ORDER);
			
			
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutbountOrderMgrZJ summarizeOutListDetail error:"+e);
		}
	}
	
	public DBRow[] summarizeTransportOutListDetail(long out_id)
		throws Exception
	{
		try 
		{
			String sql = "select slc.slc_position_all,out_list_slc_id as slc_id,out_list_pc_id as pc_id,sum(pick_up_quantity) as quantity,slc.slc_area,out_list_serial_number from "+ConfigBean.getStringValue("out_list_detail")+" as old "
						+"join "+ConfigBean.getStringValue("transport")+" as t on t.transport_id = old.system_bill_id and t.out_id = ? and old.system_bill_type = ?"
						+"join storage_location_catalog as slc on slc.slc_id = old.out_list_slc_id "
						+"group by out_list_pc_id,out_list_slc_id,out_list_serial_number order by slc.slc_position_all asc ";
			
			DBRow para = new DBRow();
			para.add("out_id",out_id);
			para.add("system_bill_type",ProductStoreBillKey.TRANSPORT_ORDER);
			
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutbountOrderMgrZJ summarizeOutListDetail error:"+e);
		}
	}
	
	/**
	 * 添加新拣货单明细
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addOutStoreBillOrderDetail(DBRow row)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("out_storebill_order_detail"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ addOutStoreBillOrderDetail error:"+e);
		}
	}
	
	public void modOutStoreBillOrderDetail(long osod_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where out_list_detail_id = "+osod_id,ConfigBean.getStringValue("out_storebill_order_detail"),para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ modOutStoreBillOrderDetail error:"+e);
		}
	}
	
	public DBRow getDetailOutStoreBillOrderDetail(long out_id,long slc_id,long pc_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("out_storebill_order_detail")+" where out_id = ? and slc_id = ? and pc_id=? and serial_number is null";
		
		DBRow para = new DBRow();
		para.add("out_id",out_id);
		para.add("slc_id",slc_id);
		para.add("pc_id",pc_id);
		
		return dbUtilAutoTran.selectPreSingle(sql, para);
	}
	
	/**
	 * 根据仓库ID获得拣货单
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutStoreOrderByPsid(long ps_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("out_storebill_order")+" where ps_id = ?";
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ getOutStoreOrderByPsid error:"+e);
		}
	}
	
	/**
	 * 根据拣货单号查询仓库和门
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDoorLocationByOutId(long outId)
		throws Exception
	{
		try 
		{     
			String sql = "select * from door_or_location_occupancy_info where rel_id = "+outId+" and rel_type="+ProductStoreBillKey.PICK_UP_ORDER;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ getOutStoreOrderByPsid error:"+e);
		}
	}
	
	public DBRow getDoorName(long doorId)
	throws Exception
	{
		try 
		{     
			String sql = "select * from storage_door where sd_id="+doorId;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ getOutStoreOrderByPsid error:"+e);
		}
	}
	public DBRow getLocationName(long locationId)
	throws Exception
	{
		try 
		{     
			String sql = "select * from storage_load_unload_location where id="+locationId;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ getOutStoreOrderByPsid error:"+e);
		}
	}
	
	/**
	 * 获得出库单详细
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutStoreBillDetail(long out_id)
		throws Exception
	{
		try 
		{
			String sql = "select osod.*,slc.slc_position_all from "+ConfigBean.getStringValue("out_storebill_order_detail")+" as osod "
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on osod.out_list_slc_id = slc.slc_id "
						+"where out_storebill_order = ? "
						+"order by slc_position_all";
			
			DBRow para = new DBRow();
			para.add("out_id",out_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FlooroutboundOrderMgrZJ getOutStoreBillDetail error:"+e);
		}
	}
	
	public DBRow[] getOutStoreBillLocation(long out_id)
		throws Exception
	{
		String sql = "select distinct osod.out_list_slc_id,slc.slc_position_all from "+ConfigBean.getStringValue("out_storebill_order_detail")+" as osod "
					+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on osod.out_list_slc_id = slc.slc_id "
					+"where out_storebill_order = ? "
					+"order by slc_position_all";

		DBRow para = new DBRow();
		para.add("out_id",out_id);
		
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	/**
	 * 根据位置ID拣货单ID获得拣货明细
	 * @param slc_id
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocationProductForOut(long slc_id,long out_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("out_storebill_order_detail")+" as osod " 
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on osod.out_list_slc_id= slc.slc_id "
						+"where osod.out_list_slc_id = ? and out_storebill_order = ? and osod.need_execut_quantity>0";
			
			DBRow para = new DBRow();
			para.add("slc_id",slc_id);
			para.add("out_id",out_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FlooroutboundOrderMgrZJ getLocationProductForOut error:"+e);
		}
	}
	
	/**
	 * 获得拣货单所需的商品信息
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductForOut(long out_id)
		throws Exception
	{
		try 
		{
			String sql = "select distinct p.* from "+ConfigBean.getStringValue("product")+" as p "
						+"join "+ConfigBean.getStringValue("out_storebill_order_detail")+" as osod on p.pc_id = out_list_pc_id and out_storebill_order = ? "
						+"order by p.pc_id";
			
			DBRow para = new DBRow();
			para.add("out_id",out_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FlooroutboundOrdermgrZJ getProductForOut error:"+e);
		}
	}
	
	/**
	 * 拣货单所需商品条码信息
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductCodeForOut(long out_id)
		throws Exception
	{
		String sql = "select distinct pcode.*,p.* from "+ConfigBean.getStringValue("product_code")+" as pcode "
					+"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = pcode.pc_id "
					+"join "+ConfigBean.getStringValue("out_storebill_order_detail")+" as osod on p.pc_id = out_list_pc_id and out_storebill_order = ? "
					+"order by pcode.pc_id";
		
		DBRow para = new DBRow();
		para.add("out_id",out_id);
		
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	public DBRow[] getSerialProductForOut(long out_id)
		throws Exception
	{
		try 
		{
			String sql = "select sp.* from "+ConfigBean.getStringValue("serial_product")+" as sp "
						+"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = sp.pc_id "
						+"join "+ConfigBean.getStringValue("out_storebill_order_detail")+" as osod on p.pc_id = out_list_pc_id and out_storebill_order = ? "
						+"where sp.in_time is not null and out_time is null "
						+"order by sp.pc_id";
			
			DBRow para = new DBRow();
			para.add("out_id",out_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ getSerialProduct");
		}
	}
	
	/**
	 * 根据商品ID，拣货单ID，位置ID，序列号获得拣货单明细
	 * @param pc_id
	 * @param out_id
	 * @param slc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getOutStorebillOrderDetailByPcOutSlcSN(long pc_id,long out_id,long slc_id,String serial_number)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("out_storebill_order_detail")+" where pc_id = ? and out_id = ? and slc_id = ? and serial_number = ? ";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("out_id",out_id);
			para.add("slc_id",slc_id);
			para.add("serial_number",serial_number);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ getOutStorebillOrderDetailByPcOutSlc error:"+e);
		}
	}
	
	/**
	 * 根据商品拣货单位置获得拣货单明细
	 * @param pc_id
	 * @param out_id
	 * @param slc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getOutStorebillOrderDetailByPcOutSlc(long pc_id,long out_id,long slc_id,int from_container_type,long from_container_type_id,long from_con_id,int pick_container_type,long pick_container_type_id,long pick_con_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("out_storebill_order_detail")
						+" where out_list_pc_id = ? and out_storebill_order = ? and out_list_slc_id = ? "
						+" and from_container_type = ? and from_container_type_id = ? and from_con_id = ? "
						+" and pick_container_type = ? and pick_container_type_id = ? and pick_con_id = ?";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("out_id",out_id);
			para.add("slc_id",slc_id);
			para.add("from_container_type",from_container_type);
			para.add("from_container_type_id",from_container_type_id);
			para.add("from_con_id",from_con_id);
			para.add("pick_container_type",pick_container_type);
			para.add("pick_container_type_id",pick_container_type_id);
			para.add("pick_con_id",pick_con_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ getOutStorebillOrderDetailByPcOutSlc error:"+e);
		}
	}
	
	/**
	 * 拣货单减少待执行数量
	 * @param osod_id
	 * @param quantity
	 * @throws Exception
	 */
	public void decOutboundOrderDetailNeedExecutQuantity(long osod_id,float quantity)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.decreaseFieldVal(ConfigBean.getStringValue("out_storebill_order_detail"),"where out_list_detail_id ="+osod_id,"need_execut_quantity",quantity);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ decOutboundOrderDetailNeedExecutQuantity error:"+e);
		}
	}
	
	/**
	 * 删除拣货单明细
	 * @param out_id
	 * @throws Exception
	 */
	public void delOutStoreBillOrderDetailByOutId(long out_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where out_id ="+out_id, ConfigBean.getStringValue("out_storebill_order_detail"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ delOutStoreBillOrderDetailByOutId error:"+e);
		}
	}
	
	/**
	 * 根据拣货单ID获得拣货单明细
	 * @param out_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutStoreBillOrderDetailByOutId(long out_id)
		throws Exception
	{
		try 
		{
			String sql = "select osod.* from "+ConfigBean.getStringValue("out_storebill_order_detail")+" as osod "
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on osod.out_list_slc_id = slc.slc_id "
						+"where out_storebill_order = ? and need_execut_quantity>0 "
						+"order by slc_position_all";
			
			DBRow para = new DBRow();
			para.add("out_id",out_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ getOutStoreBillOrderDetailByOutId error:"+e);
		}
	}
	
	/**
	 * 获得拣货单明细
	 * @param out_list_detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailOutStoreBillOrderDetailById(long out_list_detail_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("out_storebill_order_detail")+" as osod " 
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on slc.slc_id = osod.out_list_slc_id " 
						+"where osod.out_list_detail_id = ? ";
			
			DBRow para = new DBRow();
			para.add("out_list_detail_id",out_list_detail_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ getDetailOutStoreBillOrderDetailById error:"+e);
		}
	}
	
	public DBRow[] summarizeSystemBillForType(String ids,int bill_type)
		throws Exception
	{
		try 
		{
			String sql ="select *,count(*) as count,sum(pick_up_quantity) sum_quantity from out_list_detail " 
						+"GROUP BY out_list_area_id,out_list_slc_id,from_container_type,from_container_type_id,from_con_id,pick_container_type,pick_container_type_id,pick_con_id,system_bill_type " 
						+"HAVING system_bill_id in ("+ids+") and system_bill_type="+bill_type;
				 
			return this.dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ summarizeSystemBillForType error:"+e);
		}
	}
	
	public long addOutStorebillOrder(DBRow row)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("out_storebill_order"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOutboundOrderMgrZJ addOutStorebillOrder error:"+e);
		}
	}
	

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
