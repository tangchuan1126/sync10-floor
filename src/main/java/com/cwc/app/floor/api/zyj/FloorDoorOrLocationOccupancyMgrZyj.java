package com.cwc.app.floor.api.zyj;

import com.cwc.app.key.LoadUnloadOccupancyTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorDoorOrLocationOccupancyMgrZyj {

	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 添加门或位置的使用信息
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addDoorOrLocationOccupancy(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("door_or_location_occupancy_info"), row);
		}
		catch (Exception e) {
			throw new Exception("FloorDoorOrLocationOccupancyMgrZyj.addDoorOrLocationOccupancy(DBRow row):"+e);
		}
	}
	
	/**
	 * 更新门或位置的占用信息
	 * @param id
	 * @param row
	 * @throws Exception
	 */
	public void updateDoorOrLocationOccupancy(long id, DBRow row) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where id = " + id, ConfigBean.getStringValue("door_or_location_occupancy_info"), row);
		}
		catch (Exception e) {
			throw new Exception("FloorDoorOrLocationOccupancyMgrZyj.updateDoorOrLocationOccupancy(long id, DBRow row):"+e);
		}
	}
	
	/**
	 * 通过ID获取门或位置的占用信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDoorOrLocationOccupancyById(long id) throws Exception
	{
		try
		{
			String sql = "select * from door_or_location_occupancy_info where id = " + id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) {
			throw new Exception("FloorDoorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancyById(long id):"+e);
		}
	}
	
	/**
	 * 根据门或位置ID获取其使用信息
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDoorOrLocationOccupancys(int occupancy_type, long rl_id, int rel_type, long rel_id, int occupancy_status,
			String book_start_time, String book_end_time, String actual_start_time, String actual_end_time, long ps_id, int is_after_now, int rel_occupancy_use) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select * from door_or_location_occupancy_info where 1=1");
			if(0 != occupancy_type)
			{
				sb.append(" and occupancy_type = " + occupancy_type);
			}
			if(0 != rl_id)
			{
				sb.append(" and rl_id = " + rl_id);
			}
			if(0 != rel_type)
			{
				sb.append(" and rel_type = " + rel_type);
			}
			if(0 != rel_id)
			{
				sb.append(" and rel_id = " + rel_id);
			}
			if(0 != occupancy_status)
			{
				if(occupancy_status > 0)
				{
					sb.append(" and occupancy_status = " + occupancy_status);
				}
				else
				{
					sb.append(" and occupancy_status != " + Math.abs(occupancy_status));
				}
			}
			if(null != book_start_time && !"".equals(book_start_time))
			{
				sb.append(" and book_start_time >= '" + book_start_time +"'");
			}
			if(null != book_end_time && !"".equals(book_end_time))
			{
				sb.append(" and book_end_time <= '" + book_end_time + "'");
			}
			if(null != actual_start_time && !"".equals(actual_start_time))
			{
				sb.append(" and actual_start_time >= '" + actual_start_time + "'");
			}
			if(null != actual_end_time && !"".equals(actual_end_time))
			{
				sb.append(" and actual_end_time <= '" + actual_end_time + "'");
			}
			if(1 == is_after_now)
			{
				sb.append(" and ( book_end_time is null or book_end_time >= '" + DateUtil.NowStr() +"')");
			}
			if(0 != ps_id)
			{
				sb.append(" and ps_id = " + ps_id);
			}
			if(0 != rel_occupancy_use)
			{
				sb.append(" and rel_occupancy_use = " + rel_occupancy_use);
			}
			sb.append(" order by book_start_time asc");
//			System.out.println(sb.toString());
			return dbUtilAutoTran.selectMutliple(sb.toString());
		}
		catch (Exception e) {
			throw new Exception("FloorDoorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(int occupancy_type, int rel_type, long rel_id……):"+e);
		}
	}
	
	/**
	 * 根据门或位置ID获取其使用信息
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDoorOrLocationOccupancysByTime(int occupancy_type, long rl_id, int occupancy_status, String book_start_time, String book_end_time) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select * from door_or_location_occupancy_info where 1=1");
			if(0 != occupancy_type)
			{
				sb.append(" and occupancy_type = " + occupancy_type);
			}
			if(0 != rl_id)
			{
				sb.append(" and rl_id = " + rl_id);
			}
			if(0 != occupancy_status)
			{
				sb.append(" and occupancy_status = " + occupancy_status);
			}
			//A1在开始时之前，A2在开始时间之后且在结束前
			if(null != book_start_time && !"".equals(book_start_time))
			{
				sb.append(" and ((book_start_time >= '" + book_start_time +"'");
			}
			if(null != book_end_time && !"".equals(book_end_time))
			{
				sb.append(" and book_start_time <= '" + book_end_time +"'");
				sb.append(" and book_end_time >= '" + book_end_time + "')");
			}
			//A1在开始时之后，A2在结束时间之前
			if(null != book_start_time && !"".equals(book_start_time))
			{
				sb.append(" or (book_start_time <= '" + book_start_time + "'");
			}
			if(null != book_end_time && !"".equals(book_end_time))
			{
				sb.append(" and book_end_time >= '" + book_end_time + "')");
			}
			//A1在开始时之后且在结束前，A2在结束时间之后
			if(null != book_start_time && !"".equals(book_start_time))
			{
				sb.append(" or (book_start_time <= '" + book_start_time + "'");
				sb.append(" and book_end_time >= '" + book_start_time + "'");
			}
			if(null != book_end_time && !"".equals(book_end_time))
			{
				sb.append(" and book_end_time <= '" + book_end_time + "'))");
			}
			sb.append(" and book_end_time >= '" + DateUtil.NowStr() + "'");
			
			return dbUtilAutoTran.selectMutliple(sb.toString());
		}
		catch (Exception e)
		{
			throw new Exception("FloorDoorOrLocationOccupancyMgrZyj.getDoorOrLocationOccupancys(int occupancy_type, long rl_id……):"+e);
		}
	}
	
	/**
	 * 得到所有未过期的门占用信息
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBookDoorInfoEndTimeIsNotNull(long ps_id) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select * from storage_door d ,");
			sb.append(" (select c.rl_id, c.book_start_time, c.book_end_time");
			sb.append(" from door_or_location_occupancy_info c");
			sb.append(" left JOIN ");
			sb.append("	( select count(*) nullCount, rl_id from door_or_location_occupancy_info where occupancy_type = " + LoadUnloadOccupancyTypeKey.DOOR);
			sb.append("		and (book_end_time is null or book_end_time = '')  GROUP BY rl_id ) f");
			sb.append(" on c.rl_id  = f.rl_id");	
			sb.append(" where c.occupancy_type = " + LoadUnloadOccupancyTypeKey.DOOR);
			sb.append(" and ( f.nullCount = 0 or f.nullCount is null)");
			sb.append(" and c.ps_id = " + ps_id);
			sb.append(" order by c.rl_id desc, c.book_start_time asc) a");
			sb.append(" where d.sd_id = a.rl_id");
			sb.append(" and d.ps_id = " + ps_id);
			
//			sb.append("select * from storage_door d ,");
//			sb.append(" (select rl_id, book_start_time, book_end_time");
//			sb.append(" from door_or_location_occupancy_info");
//			sb.append(" where occupancy_type = " + LoadUnloadOccupancyTypeKey.DOOR);
////			sb.append(" and book_end_time <= '" + DateUtil.NowStr() +"'");
//			if(0 != doorId)
//			{
//				sb.append(" and rl_id = " + doorId);
//			}
//			sb.append(" order by rl_id desc, book_start_time asc) a");
//			sb.append(" where d.sd_id = a.rl_id");
//			sb.append(" and ps_id = " + ps_id);
			
//			System.out.println("door,can:" + sb.toString());
			return dbUtilAutoTran.selectMutliple(sb.toString());
		}
		catch (Exception e)
		{
			throw new Exception("FloorDoorOrLocationOccupancyMgrZyj.getBookDoorInfoBeforeExpired(long ps_id):"+e);
		}
	}
	
	/**
	 * 得到所有未过期的位置占用信息
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBookLocationInfoEndTimeIsNotNull(long ps_id) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select * from storage_load_unload_location d ,");
			sb.append(" (select c.rl_id, c.book_start_time, c.book_end_time");
			sb.append(" from door_or_location_occupancy_info c");
			sb.append(" left JOIN ");
			sb.append("	( select count(*) nullCount, rl_id from door_or_location_occupancy_info where occupancy_type = " + LoadUnloadOccupancyTypeKey.LOCATION);
			sb.append("		and (book_end_time is null or book_end_time = '')  GROUP BY rl_id ) f");
			sb.append(" on c.rl_id  = f.rl_id");	
			sb.append(" where c.occupancy_type = " + LoadUnloadOccupancyTypeKey.LOCATION);
			sb.append(" and ( f.nullCount = 0 or f.nullCount is null)");
			sb.append(" and c.ps_id = " + ps_id);
			sb.append(" order by c.rl_id desc, c.book_start_time asc) a");
			sb.append(" where d.id = a.rl_id");
			sb.append(" and d.psc_id = " + ps_id);
			
//			sb.append("select * from storage_load_unload_location d ,");
//			sb.append(" (select rl_id, book_start_time, book_end_time");
//			sb.append(" from door_or_location_occupancy_info");
//			sb.append(" where occupancy_type = " + LoadUnloadOccupancyTypeKey.LOCATION);
//			sb.append(" and book_end_time <= '" + DateUtil.NowStr() +"'");
//			if(0 != loc_id)
//			{
//				sb.append(" and rl_id = " + loc_id);
//			}
//			sb.append(" order by rl_id desc, book_start_time asc) a");
//			sb.append(" where d.id = a.rl_id");
//			sb.append(" and psc_id = " + ps_id);
//			System.out.println("loc,can:" + sb.toString());
			return dbUtilAutoTran.selectMutliple(sb.toString());
		}
		catch (Exception e)
		{
			throw new Exception("FloorDoorOrLocationOccupancyMgrZyj.getBookLocationInfoBeforeExpired(long ps_id):"+e);
		}
	}
	
	
	
	/**
	 * 得到未被占用的门
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNotOccupancyDoors(long ps_id) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select d.* from storage_door d LEFT JOIN"); 
			sb.append(" (select count(*) as info_count,rl_id, occupancy_type from door_or_location_occupancy_info where occupancy_type = "+LoadUnloadOccupancyTypeKey.DOOR);
//			if(0 != doorId)
//			{
//				sb.append(" and rl_id = " + doorId);
//			}
			sb.append(" GROUP BY rl_id)a ");
			sb.append(" on d.sd_id = a.rl_id where (a.info_count is null or a.info_count = 0 ) and ps_id= " + ps_id);
//			if(0 != doorId)
//			{
//				sb.append(" and d.sd_id = " + doorId);
//			}
			
//			System.out.println("door,not:" + sb.toString());
			return dbUtilAutoTran.selectMutliple(sb.toString());
		}
		catch (Exception e)
		{
			throw new Exception("FloorDoorOrLocationOccupancyMgrZyj.getNotOccupancyDoors():"+e);
		}
	}
	
	
	/**
	 * 得到未被占用的位置
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNotOccupancyLocations(long psc_id) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select d.* from storage_load_unload_location d LEFT JOIN "); 
			sb.append(" (select count(*) as info_count,rl_id, occupancy_type from door_or_location_occupancy_info where occupancy_type = "+LoadUnloadOccupancyTypeKey.LOCATION);
//			if(0 != loc_id)
//			{
//				sb.append(" and rl_id = " + loc_id);
//			}
			sb.append(" GROUP BY rl_id)a ");
			sb.append(" on d.id = a.rl_id where (a.info_count is null or a.info_count = 0 ) and psc_id = " + psc_id);
//			if(0 != loc_id)
//			{
//				sb.append(" and d.id = " + loc_id);
//			}
//			System.out.println("loc,not:" + sb.toString());
			return dbUtilAutoTran.selectMutliple(sb.toString());
		}
		catch (Exception e)
		{
			throw new Exception("FloorDoorOrLocationOccupancyMgrZyj.getNotOccupancyLocations():"+e);
		}
	}
	
	/**
	 * 查询门
	 * @param doorName
	 * @param ps_id
	 * @param occupancyType
	 * @param rel_type
	 * @param rel_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageDoorsByNamePsRel(String doorName, long ps_id, int occupancyType, int rel_type, long rel_id, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select b.*,c.title from (");
			sb.append(" select d.* from storage_door d ,");
			sb.append(" (select DISTINCT(rl_id) from door_or_location_occupancy_info where 1=1");
			if(0 != occupancyType)
			{
				sb.append(" and occupancy_type = " + occupancyType);
			}
			if(0 != rel_type && -1 != rel_type)
			{
				sb.append(" and rel_type = " + rel_type);
			}
			if(0 != rel_id)
			{
				sb.append(" and rel_id = " + rel_id);
			}
			sb.append(" order by rl_id desc) a");
			sb.append(" where d.sd_id = a.rl_id");
			if(0 != ps_id && -1 != ps_id)
			{
				sb.append(" and ps_id = " + ps_id);
			}
			if(null != doorName && !"".equals(doorName))
			{
				sb.append(" and d.doorId like '%" + doorName + "%'");
			}
			sb.append(" ) b LEFT JOIN product_storage_catalog c on b.ps_id = c.id");
			
			if(null != pc)
			{
				return dbUtilAutoTran.selectMutliple(sb.toString(),pc);
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sb.toString());
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorDoorOrLocationOccupancyMgrZyj.getStorageDoorsByNamePsRel(String doorName, long ps_id, int occupancyType, int rel_type, long rel_id):"+e);
		}
	}
	
	/**
	 * 查询位置
	 * @param locName
	 * @param ps_id
	 * @param occupancyType
	 * @param rel_type
	 * @param rel_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLocationsByNamePsRel(String locName, long ps_id, int occupancyType, int rel_type, long rel_id, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select b.*,c.title from (");
			sb.append(" select d.* from storage_load_unload_location d ,");
			sb.append(" (select DISTINCT(rl_id) from door_or_location_occupancy_info where 1=1");
			if(0 != occupancyType)
			{
				sb.append(" and occupancy_type = " + occupancyType);
			}
			if(0 != rel_type && -1 != rel_type)
			{
				sb.append(" and rel_type = " + rel_type);
			}
			if(0 != rel_id)
			{
				sb.append(" and rel_id = " + rel_id);
			}
			sb.append(" order by rl_id desc) a");
			sb.append(" where d.id = a.rl_id");
			if(0 != ps_id && -1 != ps_id)
			{
				sb.append(" and psc_id = " + ps_id);
			}
			if(null != locName && !"".equals(locName))
			{
				sb.append(" and d.location_name like '%" + locName + "%'");
			}
			sb.append(" ) b LEFT JOIN product_storage_catalog c on b.psc_id = c.id");
			
			
			if(null != pc)
			{
				return dbUtilAutoTran.selectMutliple(sb.toString(),pc);
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sb.toString());
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorDoorOrLocationOccupancyMgrZyj.getStorageLocationsByNamePsRel(String locName, long ps_id, int occupancyType, int rel_type, long rel_id):"+e);
		}
	}
	
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
	
}
