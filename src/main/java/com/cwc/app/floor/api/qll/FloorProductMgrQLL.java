package com.cwc.app.floor.api.qll;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorProductMgrQLL {

	private DBUtilAutoTran dbUtilAutoTran ;
	 
	/**
	 * 获得默认预警产品
	 * @param pc
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStockWarnDefult(PageCtrl pc,String startDate,String endDate)
		throws Exception 
	{
		try
		{
			    String sql = "select distinct a.product_id,a.product_title,a.plan_storage_id,a.storage_title ,a.store_count,a.avg_count,a.day_count ,a.catalog_id ,a.title as catalog_title,b.store_count_alert from ("+
					    	" select p.product_id ,c.p_name as product_title  ,c.catalog_id ,catalog.title,p.plan_storage_id, s.title as storage_title,storage.store_count ,round(sum(p.quantity)/30,2)  as avg_count, "+
					    	" round(storage.store_count/(round((sum(p.quantity)/30),2)*1),2) as day_count,storage.store_count_alert from plan_storage_sales p,product c,product_storage_catalog s,product_catalog catalog,"+
					    	" product_storage as storage  where c.catalog_id=catalog.id and c.orignal_pc_id=0 and  p.product_id = c.pc_id  and p.plan_storage_id = s.id  and  storage.pc_id = p.product_id "+
					    	" and  storage.cid = p.plan_storage_id  and p.delivery_date between '"+startDate+"' and  '"+endDate+"'  group by p.product_id, p.plan_storage_id ) as a ,product_storage  b"+
					    	" where a.product_id=b.pc_id and b.cid=a.plan_storage_id ";
			    if(pc!=null)
				{
					return (dbUtilAutoTran.selectMutliple(sql,pc));
				}
				else
				{
					return (dbUtilAutoTran.selectMutliple(sql));
				}
		}
		catch(Exception e)
		{
			throw new Exception("getStockWarnByCatalog(String idStr,String startDate,String endDate,String cmd,String cidf,String dayCountf,long categoryf,String coefficientf,String search_product_namef,String productLinef,String part_or_allf)error："+e);
		}
	}
	
	/**
	 * 根据仓库统计预警
	 * @param catalogIdStr
	 * @param startDate
	 * @param endDate
	 * @param cidf
	 * @param coefficientf
	 * @param part_or_allf
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStockWarnByCid(PageCtrl pc,int dayCount,String startDate,String endDate,String cidf,float coefficientf,String part_or_allf)
		throws Exception 
	{
		try
		{
			    String sql = "select  distinct a.product_id ,  a.product_title,a.product_title,a.plan_storage_id,a.storage_title ,a.store_count,a.avg_count,a.day_count ,a.catalog_id ,a.title as catalog_title,b.store_count_alert from ( "+
			    			 " select p.product_id ,c.p_name as product_title  ,c.catalog_id ,catalog.title,p.plan_storage_id, s.title as storage_title,storage.store_count ,round(sum(p.quantity)/"+dayCount+",2)  as avg_count,  "+
			    			 " round(storage.store_count/((sum(p.quantity)/"+dayCount+")*"+coefficientf+"),2)as day_count,storage.store_count_alert from product_catalog catalog, plan_storage_sales p,product c,product_storage_catalog s, "+
			    			 " product_storage as storage  where c.catalog_id=catalog.id and c.orignal_pc_id=0 and p.product_id = c.pc_id  and p.plan_storage_id = s.id and p.plan_storage_id="+cidf+" and  storage.pc_id = p.product_id  "+
			    			 " and  storage.cid = p.plan_storage_id  and p.delivery_date between '"+startDate+"' and '"+endDate+"'  group by p.product_id ) as a ,product_storage  b "+
			    			 " where a.product_id=b.pc_id and b.cid=a.plan_storage_id ";
				if(part_or_allf.equals("0"))//预警
				{
					sql+=" and a.day_count<=b.store_count_alert";
				}
				if(pc!=null)
				{
					return (dbUtilAutoTran.selectMutliple(sql,pc));
				}
				else
				{
					return (dbUtilAutoTran.selectMutliple(sql));
				}
		}
		catch(Exception e)
		{
			throw new Exception("getStockWarnByCatalog(String idStr,String startDate,String endDate,String cmd,String cidf,String dayCountf,long categoryf,String coefficientf,String search_product_namef,String productLinef,String part_or_allf)error："+e);
		}
	}
	/**
	 * 根据类别统计备货预警
	 * @param catalogIdStr
	 * @param startDate
	 * @param endDate
	 * @param cidf
	 * @param coefficientf
	 * @param part_or_allf
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStockWarnByCatalog(PageCtrl pc,int dayCount,String catalogIdStr,String startDate,String endDate,String cidf,float coefficientf,String part_or_allf)
		throws Exception 
	{
		try
		{
			    String sql = "select distinct a.product_id,a.product_title,a.product_title,a.plan_storage_id,a.storage_title ,a.store_count,a.avg_count,a.day_count ,a.catalog_id ,a.title as catalog_title,b.store_count_alert  "+
				"  from ( select p.product_id ,c.p_name as product_title  ,c.catalog_id ,catalog.title,p.plan_storage_id, s.title as storage_title, storage.store_count ,round(sum(p.quantity)/"+dayCount+",2)  as avg_count, "+
				"  round(storage.store_count/((sum(p.quantity)/"+dayCount+")*"+coefficientf+"),2) as day_count,storage.store_count_alert   "+
				" from plan_storage_sales p,product c,product_storage_catalog s,product_storage as storage, product_catalog catalog "+
				" where c.catalog_id=catalog.id and p.catalog_id in (" +catalogIdStr+") and   p.product_id = c.pc_id  and p.plan_storage_id = s.id "+
				" and p.plan_storage_id="+cidf+" and  storage.pc_id = p.product_id and     storage.cid = p.plan_storage_id  "+
				" and p.delivery_date between '"+startDate+"' and '"+endDate+"'  group by p.product_id )"+
				"  as a ,product_storage b where a.product_id=b.pc_id and b.cid=a.plan_storage_id ";
				if(part_or_allf.equals("0"))//预警
				{
					sql+=" and a.day_count<=b.store_count_alert";
				}
				if(pc!=null)
				{
					return (dbUtilAutoTran.selectMutliple(sql,pc));
				}
				else
				{
					return (dbUtilAutoTran.selectMutliple(sql));
				}
		}
		catch(Exception e)
		{
			throw new Exception("getStockWarnByCatalog(String idStr,String startDate,String endDate,String cmd,String cidf,String dayCountf,long categoryf,String coefficientf,String search_product_namef,String productLinef,String part_or_allf)error："+e);
		}
	}
	
	/**
	 * 通过产品条码统计缺货预警
	 * @param pc_id
	 * @param startDate
	 * @param endDate
	 * @param cidf
	 * @param coefficientf
	 * @param part_or_allf
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStockWarnByName(PageCtrl pc,int dayCount,long pc_id,String startDate,String endDate,String cidf,float coefficientf,String part_or_allf)
		throws Exception 
	{
		try
		{
			    String sql = "select distinct a.product_id,a.product_title,a.product_title,a.plan_storage_id,a.storage_title ,a.store_count,a.avg_count,a.day_count ,a.catalog_id ,a.title as catalog_title,b.store_count_alert  "+
				"  from ( select p.product_id ,c.p_name as product_title  ,c.catalog_id ,catalog.title,p.plan_storage_id, s.title as storage_title, storage.store_count ,round(sum(p.quantity)/"+dayCount+",2)  as avg_count, "+
				"  round(storage.store_count/((sum(p.quantity)/"+dayCount+")*"+coefficientf+"),2) as day_count,storage.store_count_alert   "+
				" from product_catalog catalog, plan_storage_sales p,product c,product_storage_catalog s,product_storage as storage  "+
				" where  c.catalog_id=catalog.id and  p.product_id = c.pc_id  and p.plan_storage_id = s.id and p.plan_storage_id="+cidf+" and  storage.pc_id = p.product_id "+
				"  and storage.cid = p.plan_storage_id and c.pc_id="+pc_id+" and p.delivery_date between '"+startDate+"' and '"+endDate+"'  group by p.product_id )"+
				"  as a ,product_storage b where a.product_id=b.pc_id and b.cid=a.plan_storage_id ";
				if(part_or_allf.equals("0"))//预警
				{
					sql+=" and a.day_count<=b.store_count_alert";
				}
				if(pc!=null)
				{
					return (dbUtilAutoTran.selectMutliple(sql,pc));
				}
				else
				{
					return (dbUtilAutoTran.selectMutliple(sql));
				}
		}
		catch(Exception e)
		{
			throw new Exception("getStockWarnByCatalog(String idStr,String startDate,String endDate,String cmd,String cidf,String dayCountf,long categoryf,String coefficientf,String search_product_namef,String productLinef,String part_or_allf)error："+e);
		}
	}
	
	/**
	 * 获取产品线的子类 通过function
	 * @param catalogID
	 * @return
	 * @throws Exception
	 */
	public DBRow getLineStr(long lineID)
		throws Exception
	{
		try
		{
			String sql="select  product_line_child_list("+lineID+") as idList";
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e)
		{
			throw new Exception("getLineStr(long catalogID)error："+e);
		}
	}	
	/**
	 * 获取类别的子类 通过function
	 * @param catalogID
	 * @return
	 * @throws Exception
	 */
	public DBRow getIdStr(long catalogID)
		throws Exception
	{
		try
		{
			String sql="select  product_catalog_child_list("+catalogID+") as idList";
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e)
		{
			throw new Exception("DBRow getStr(long catalogID)error："+e);
		}
	}
	
	/**
	 * 通过产品名称获取ID
	 * @param pName
	 * @return
	 * @throws Exception
	 */
	public DBRow  getPid(String pName)
		throws Exception 
	{
		try
		{
			DBRow param = new DBRow();
			String sql = "select pc_id from "+ConfigBean.getStringValue("product")+" where p_name = ? and orignal_pc_id = 0";
			param.add("p_name", pName);
			return (dbUtilAutoTran.selectPreSingle(sql, param));
		} 
		catch (Exception e)
		{
			throw new Exception("getPid(String pName)error："+e);
		}
	}
	
	/**
	 * 通过产品线ID称获取产品线名称
	 * @param pName
	 * @return
	 * @throws Exception
	 */
	public DBRow  getProductLineName(long productLineID)
		throws Exception 
	{
		try
		{
			DBRow param = new DBRow();
			String sql = "select name from "+ConfigBean.getStringValue("product_line_define")+" where id = ?";
			param.add("id", productLineID);
			return (dbUtilAutoTran.selectPreSingle(sql, param));
		} 
		catch (Exception e)
		{
			throw new Exception("getPid(String pName)error："+e);
		}
	}
	
	/**
	 * 获得备货计划分析
	 * @param start_date
	 * @param end_date
	 * @param catalog_id
	 * @param pro_line_id
	 * @param pc_id
	 * @param ps_id
	 * @param isAlert
	 * @param type
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] preparationPlanAnalysisNew(String start_date,String end_date,long catalog_id,long pro_line_id,long pc_id,long ps_id,int isAlert,float coefficient,PageCtrl pc)
		throws Exception
	{
		String whereProductLine = "";
		if(pro_line_id>0)
		{
			whereProductLine = " and pc.product_line_id = "+pro_line_id+" ";
		}
		
		String whereCatalog = "";
		if(catalog_id>0)
		{
			whereCatalog = " join pc_child_list as pcl on pcl.pc_id = p.catalog_id and pcl.search_rootid = "+catalog_id+" ";
		}
		
		String whereProduct = "";
		if(pc_id>0)
		{
			whereProduct = " and p.pc_id = "+pc_id+" ";
		}
		
		DBRow[] dateStr =getDate(start_date,end_date);
		String sql = "select * from (select product_id,p_name,product_line_title,product_catalog_title,quantity,basePss.post_date,psc.title,ps.store_count,round(sum(basePss.quantity)/"+dateStr.length+",2)AS avg_count,round(ps.store_count /(round((sum(basePss.quantity) / "+dateStr.length+"), 2)* "+coefficient+"),2)AS day_count,ps.store_count_alert,";
		
		for(int i=0; i<dateStr.length; i++)
		{
			sql +="  MAX(CASE basePss.post_date  WHEN '"+dateStr[i].getString("date")+"' THEN basePss.quantity ELSE 0  END)  date_"+i;
			if(i!=(dateStr.length-1))
			{
				sql +=" , ";
			}
		}
		
		sql+=" from(";
		
		sql += 	"select product_id,p_name,product_line_title,product_catalog_title,sum(quantity) as quantity,base.post_date,plan_ps_id from ("
				+"select p.pc_id as product_id,p.p_name,(pi.quantity*pu.quantity) as quantity,pld.`name` as product_line_title,pc.title as product_catalog_title,plan_ps_id,pi.modality,left(po.post_date,10) as post_date from porder_item as pi " 
				+"join porder as po on pi.oid=po.oid and (pi.modality = 2 or pi.product_type=4) "
				+"join product_union as pu on pi.pid = pu.set_pid "
				+"join product as p on p.pc_id= pu.pid "+whereProduct
				+whereCatalog
				+"JOIN product_catalog pc ON p.catalog_id = pc.id "+whereProductLine
				+"left join product_line_define pld on pld.id = pc.product_line_id "
				+"where po.post_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' and plan_ps_id="+ps_id+" "
				+"union all "
				+"select p.pc_id as product_id,p.p_name,pi.quantity as quantity,pld.`name` as product_line_title,pc.title as product_catalog_title,plan_ps_id,pi.modality,left(po.post_date,10) as post_date from porder_item as pi " 
				+"join porder as po on pi.oid=po.oid and (pi.modality = 1) "
				+"join product as p on p.pc_id= pi.pid "+whereProduct
				+whereCatalog
				+"JOIN product_catalog pc ON p.catalog_id = pc.id "+whereProductLine
				+"left join product_line_define pld on pld.id = pc.product_line_id "
				+"where po.post_date between '"+start_date+" 0:00:00' and '"+end_date+" 23:59:59' and plan_ps_id="+ps_id+" "
				+")AS base "
				+"GROUP BY base.product_id,base.plan_ps_id,base.post_date"
				+") as basePss "
				+"RIGHT JOIN product_storage ps on ps.pc_id = basePss.product_id and ps.cid = basePss.plan_ps_id "
				+"join product_storage_catalog psc on psc.id = basePss.plan_ps_id "
				+"group by basePss.product_id,basePss.plan_ps_id";
		
		String whereIsAlert = "";
		if(isAlert==1)
		{
			whereIsAlert = " where store_count_alert>day_count";
		}
		sql +=")as result"+whereIsAlert;
		
		return dbUtilAutoTran.selectMutliple(sql, pc);
	}
	
	public DBRow[] preparationPlanAnalysis(String start_date,String end_date,long catalog_id,long pro_line_id,long pc_id,long ps_id,int isAlert,float coefficient,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String whereProductLine = "";
			if(pro_line_id>0)
			{
				whereProductLine = " and pc.product_line_id = "+pro_line_id+" ";
			}
			
			String whereCatalog = "";
			if(catalog_id>0)
			{
				whereCatalog = " join pc_child_list as pcl on pcl.pc_id = pss.catalog_id and pcl.search_rootid = "+catalog_id+" ";
			}
			
			String whereProduct = "";
			if(pc_id>0)
			{
				whereProduct = " and pss.product_id = "+pc_id+" ";
			}
			
			DBRow[] dateStr =getDate(start_date,end_date);
			String sql = "SELECT * from (SELECT product_id,p_name,product_line_title,product_catalog_title,quantity,delivery_date,psc.title,ps.store_count,round(sum(basePss.quantity)/30,2)AS avg_count,round(ps.store_count /(round((sum(basePss.quantity) / 30), 2)* "+coefficient+"),2)AS day_count,ps.store_count_alert,";
			
			for(int i=0; i<dateStr.length; i++)
			{
				sql +="  MAX(CASE delivery_date  WHEN '"+dateStr[i].getString("date")+"' THEN quantity ELSE 0  END)  date_"+i;
				if(i!=(dateStr.length-1))
				{
					sql +=" , ";
				}
			}
			
			sql+=" from(";
			
			sql+="SELECT pss.product_id,p.p_name,pld.`name` as product_line_title,pc.title as product_catalog_title,pss.quantity,pss.plan_storage_id,LEFT(delivery_date,10) as delivery_date FROM plan_storage_sales AS pss "
				+"JOIN product AS p ON pss.product_id = p.pc_id "+whereProduct
				+whereCatalog
				+"JOIN product_catalog pc ON p.catalog_id = pc.id "+whereProductLine
				+"left join product_line_define pld on pld.id = pc.product_line_id "
				+"WHERE	pss.delivery_date BETWEEN '"+start_date+"'AND '"+end_date+"' and pss.plan_storage_id="+ps_id+" " 
				+"GROUP BY pss.product_id,pss.plan_storage_id,left(delivery_date,10)";
			
			sql+=") as basePss "
				+"RIGHT JOIN product_storage ps on ps.pc_id = basePss.product_id and ps.cid = basePss.plan_storage_id "
				+"join product_storage_catalog psc on psc.id = basePss.plan_storage_id "
				+"group by basePss.product_id,basePss.plan_storage_id ";
			
			String whereIsAlert = "";
			if(isAlert==1)
			{
				whereIsAlert = " where store_count_alert>day_count";
			}
			sql +=")as result"+whereIsAlert;
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgrQLL preparationPlanAnalysis error:"+e);
		}
	}
	
	public DBRow[]  getDate(String startDate,String endDate)
		throws Exception 
	{
	
		//缺少后面天大于 等于前面天的判断
		 long  DAY = 24L * 60L * 60L * 1000L;   
		 Date dateTemp = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
	     Date dateTemp2 = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
		 long n =  (dateTemp2.getTime() - dateTemp.getTime() ) / DAY   ;
		 int num = Long.valueOf(n).intValue();
		 DBRow[] row = new DBRow[num+1];
		 Calendar calendarTemp = Calendar.getInstance();
		 calendarTemp.setTime(dateTemp);
	     int i= 0;
	     while (calendarTemp.getTime().getTime()!= dateTemp2.getTime())
	     {
	            String temp = new SimpleDateFormat("yyyy-MM-dd").format(calendarTemp.getTime()) ;
	            row[i] = new DBRow();
	            row[i].add("date",temp );
	            calendarTemp.add(Calendar.DAY_OF_YEAR, 1);
	            i++;
	     }
	     row[i] = new DBRow();
	     row[i].add("date", new SimpleDateFormat("yyyy-MM-dd").format(dateTemp2.getTime()) );
		 return row;
	}
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
