package com.cwc.app.floor.api.zyj.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

import com.cwc.app.floor.api.zyj.model.Customer;
import com.cwc.app.floor.api.zyj.model.Title;
import com.cwc.app.floor.api.zyj.service.ProductTitleService;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class ProductTitleServiceImpl implements ProductTitleService{
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 获取与特定产品相关的所有Customer
	 * @param productId 产品ID
	 * @return
	 */
	public List<Customer>  getCustomers(int productId){
		List<Customer> list = new ArrayList<Customer>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple("SELECT distinct a.tp_customer_id customer_id,c.customer_id customer_name FROM title_product a LEFT JOIN customer_id c ON a.tp_customer_id= c.customer_key WHERE a.tp_pc_id =?",param_row);
			
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToCustomer(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;			
		
		
	}
	
	/**
	 * 获取与特定产品相关的所有Title
	 * @param productId 产品ID
	 * @return
	 */
	public List<Title>  getTitles(int productId){
		List<Title> list = new ArrayList<Title>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple(" SELECT distinct title_id,title_name FROM title_product p LEFT JOIN  title t on  p.tp_title_id=t.title_id where p.tp_pc_id=?",param_row);
			
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToTitle(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;			
		
		
	}
	
	/**
	 * 获取与特定产品相关的所有ProductCustomer
	 * @param productId
	 * @return
	 */
	public List<Customer>  getCustomers(int productId,int titleId){
		List<Customer> list = new ArrayList<Customer>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			param_row.add("tp_title_id", titleId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple("SELECT distinct a.tp_customer_id customer_id,c.customer_id customer_name FROM title_product a INNER JOIN customer_id c ON a.tp_customer_id= c.customer_key WHERE a.tp_pc_id =? and a.tp_title_id=?",param_row);
			
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToCustomer(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;		
		
	}	
	
	/**
	 * 获取与特定产品,特定品牌商相关的所有Title
	 * @param productId 产品ID
	 * @param titleId  生产商ID
	 * @return
	 */
	public List<Title>  getTitles(int productId,int customerId){
		List<Title> list = new ArrayList<Title>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_pc_id", productId);
			param_row.add("tp_customer_id", customerId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple("SELECT distinct a.tp_title_id title_id,t.title_name FROM title_product a LEFT JOIN title t ON a.tp_title_id= t.title_id WHERE a.tp_pc_id =? and a.tp_customer_id=?",param_row);
			
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToTitle(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;			
	}
	
	/**
	 * 获取与特定品牌商相关的所有Title
	 * @param customerId 品牌商ID
	 * @return
	 */
	public List<Title>  getTitlesByCustomerId(int customerId){
		List<Title> list = new ArrayList<Title>();
		try {
			DBRow param_row = new DBRow();
			param_row.add("tp_customer_id", customerId);
			DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple("select distinct a.tp_title_id title_id,t.title_name from title_product a left join title t on a.tp_title_id=t.title_id  where a.tp_customer_id=?",param_row);
			
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToTitle(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;			
	}
	
	/**
	 * 将DBRow 对象转变为对应的 CustomerId对象
	 * @param row
	 * @return
	 */
	private Customer convertToCustomer(DBRow row){
		Assert.notNull(row);
		Customer customer = new Customer();
		customer.setId(row.get("customer_id", 1));
		customer.setName( row.get("customer_name", "") );
		return customer;
	}
	
	/**
	 * 将DBRow 对象转变为对应的 Title对象
	 * @param row
	 * @return
	 */
	private Title convertToTitle(DBRow row){
		Assert.notNull(row);
		Title title = new Title();
		title.setId(row.get("title_id", 1));
		title.setName( row.get("title_name", "") );
		return title;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
