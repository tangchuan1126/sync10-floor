package com.cwc.app.floor.api.zj;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.key.WayBillOrderStatusKey;
import com.cwc.app.key.WaybillInternalTrackingKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorWayBillOrderMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 添加运单
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addWayBillOrder(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long waybill_id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("waybill_order"),dbrow);
			return waybill_id;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ addWayBillOrder error:"+e);
		}
	}
	
	/**
	 * 添加运单明细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addWayBillOrderItem(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long waybill_order_detail_id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("waybill_order_item"),dbrow);
			return waybill_order_detail_id;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ addWayBillOrderItem error:"+e);
		}
	}
	
	/**
	 * 根据订单ID获得所有与此订单有关的非取消运单
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWayBillOrderByOid(long oid)
		throws Exception
	{
		try 
		{
			String sql = "select DISTINCT wo.* from waybill_order as wo "
						+"join waybill_order_item as woi on woi.waybill_order_id = wo.waybill_id and woi.oid=? " 
						+"where `wo`.`status` !="+WayBillOrderStatusKey.CANCEL
						+" UNION "
						+"SELECT DISTINCT wo.* from waybill_order as wo "
						+"join porder as po on po.oid=? and wo.address_name = po.address_name and wo.ccid= po.ccid and wo.pro_id=po.pro_id and wo.address_city = po.address_city and wo.address_zip = po.address_zip "
						+"join waybill_order_item as woi on woi.waybill_order_id = wo.waybill_id "
						+"where `wo`.`status` !="+WayBillOrderStatusKey.CANCEL;
			
			
//			String sql = "select distinct wo.* from "+ConfigBean.getStringValue("waybill_order")+" as wo "
//						+"join waybill_order_item as woi on woi.waybill_order_id = wo.waybill_id "
//						+"join porder_item as pi on pi.iid = woi.order_item_id and pi.oid=? "
//						+"order by waybill_id desc";
			
			DBRow para = new DBRow();
			para.add("oid",oid);
			para.add("oid2",oid);
			return(dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getWayBillOrderByOid error:"+e);
		}
	}
	
	/**
	 * 根据oid数组查询运单数组
	 * @param oids
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWayBillOrdersByOids(long[] oids)
		throws Exception
	{
		try 
		{
			if(oids==null||oids.length==0)
			{
				throw new Exception();
			}
			
			String whereOid = " and (";
			for (int i = 0; i < oids.length; i++) 
			{
				whereOid +=" woi.oid="+oids[i];
				
				if(i<oids.length-1)
				{
					whereOid += " or ";
				}
			}
			whereOid += ") ";
			
			String sql = "select DISTINCT wo.* from waybill_order as wo "
				+"join waybill_order_item as woi on woi.waybill_order_id = wo.waybill_id " 
				+"where 1=1 "+whereOid;
			
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getWayBillOrdersByOids error:"+e);
		}
	}
	
	
	/**
	 * 根据运单ID获得运单明细
	 * @param wayBill_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWayBillOrderItemsByWayBillId(long wayBill_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("waybill_order_item")+" where waybill_order_id = ?";
			
			DBRow para = new DBRow();
			para.add("waybill_order_id",wayBill_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getWayBillOrderItemsByWayBillId error:"+e);
		}
	}
	
	/**
	 * 修改运单
	 * @param waybill_id
	 * @param para
	 * @throws Exception
	 */
	public void modWayBillOrderById(long waybill_id,DBRow para)
		throws Exception
	{
		dbUtilAutoTran.update("where waybill_id="+waybill_id,ConfigBean.getStringValue("waybill_order"),para);
	}
	
	public void modWayBIllOrderItemsByWayBillId(long waybill_id,DBRow para)
		throws Exception
	{
		dbUtilAutoTran.update("where waybill_order_id="+waybill_id,ConfigBean.getStringValue("waybill_order_item"),para);
	}
	
	/**
	 * 修改运单明细
	 * @param waybill_order_item_id
	 * @param para
	 * @throws Exception
	 */
	public void modWayBillOrderItemById(long waybill_order_item_id,DBRow para)
		throws Exception
	{
		dbUtilAutoTran.update("where waybill_order_item_id="+waybill_order_item_id,ConfigBean.getStringValue("waybill_order_item"),para);
	}
	
	public void cancelWayBillOrder(long waybill_id,long adid)
		throws Exception
	{
		DBRow wayBillPara = new DBRow();
		wayBillPara.add("status",WayBillOrderStatusKey.CANCEL);
		wayBillPara.add("cancel_date",DateUtil.NowStr());
		wayBillPara.add("cancel_account",adid);
		dbUtilAutoTran.update("where waybill_id="+waybill_id,ConfigBean.getStringValue("waybill_order"),wayBillPara);
		
		DBRow wayBillItemPara = new DBRow();
		wayBillItemPara.add("order_item_id",0l);
		dbUtilAutoTran.update("where waybill_order_id="+waybill_id,ConfigBean.getStringValue("waybill_order_item"),wayBillItemPara);
	}
	
	/**
	 * 根据运单ID删除运单明细
	 * @param waybill_id
	 * @throws Exception
	 */
	public void delWayBillOrderItems(long waybill_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where waybill_order_id="+waybill_id,ConfigBean.getStringValue("waybill_order_item"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ delWayBillOrderItems error:"+e);
		}
	}
	
	public void delWayBillOrderItemById(long waybill_order_item_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where waybill_order_item_id="+waybill_order_item_id,ConfigBean.getStringValue("waybill_order_item"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ delWayBillOrderItemById error :"+e);
		}
	}
	
	public DBRow getWayBillOrderById(long waybill_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("waybill_order")+" where waybill_id=?";
			
			DBRow para = new DBRow();
			para.add("waybill_id",waybill_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getWayBillOrderById error:"+e);
		}
	}
	
	public DBRow[] searchWayBillOrder(String st_date,String en_date,long pc_id,long product_line_id,long catalog_id,String order_source,long ps_id,long ca_id,long ccid,long pro_id,long out_id,PageCtrl pc,int status,long sc_id,int product_status,int internal_tracking_status)
		throws Exception
	{
		try 
		{
			String whereProductLineOrCatalog = "";
			
			if(catalog_id>0)
			{
				whereProductLineOrCatalog = "join product_catalog as pc on pc.id = p.catalog_id "
										   +"join pc_child_list as pcl on pcl.pc_id = pc.id and pcl.search_rootid = "+catalog_id+" ";
			}
			else if(product_line_id>0&&catalog_id==0)
			{
				whereProductLineOrCatalog = " join product_catalog as pc on pc.id = p.catalog_id and pc.product_line_id = "+product_line_id+" "; 
			}
			
			String whereStorage = "";
			if(ps_id>0)
			{
				whereStorage = " and wo.ps_id="+ps_id+" ";
			}
			
			String whereArea = "";
			if(ca_id>0)
			{
				whereArea = " and ca.ca_id = "+ca_id+" ";	
			}
			
			String whereCountry = "";
			if(ccid>0)
			{
				whereCountry = " and wo.ccid = "+ccid+" ";
			}
			
			String whereProvince = "";
			if(pro_id>0)
			{
				whereProvince = " and wo.pro_id = "+pro_id+" ";
			}
			
			String whereOrderSource = "";
			if(order_source!=null&&!order_source.trim().equals(""))
			{
				whereOrderSource = " and po.order_source='"+order_source+"' ";
			}
			
			String outBill = "";
			if(out_id>0)
			{
				outBill = " and wo.out_id="+out_id+" ";
			}
			
			String whereStatus = "";
			if(status>-1)
			{
				whereStatus = " and wo.status="+status+" ";
			}
			
			String whereCompany = "";
			if(sc_id>0)
			{
				whereCompany = "and wo.sc_id="+sc_id+" ";
			}
			
			String whereProductStatus = "";
			if(product_status>0)
			{
				whereProductStatus = " and wo.product_status = "+product_status+" ";
			}
			
			String whereCreateDate = "";
			if(!st_date.equals("")&&!en_date.equals(""))
			{
				whereCreateDate = " and create_date>'"+st_date+" 0:00:00' and create_date<'"+en_date+" 23:59:59' ";
			}
			
			String whereInterTrackingStatus = "";
			if(internal_tracking_status!=-1)
			{
				if(internal_tracking_status==WaybillInternalTrackingKey.NotYetDeliveryComplete)
				{
					whereInterTrackingStatus = "and (wo.internal_tracking_status != "+WaybillInternalTrackingKey.UnKnown+" and wo.internal_tracking_status != "+WaybillInternalTrackingKey.DeliveryCompleted+")";
				}
				else
				{
					whereInterTrackingStatus = "and wo.internal_tracking_status = "+internal_tracking_status+" ";
				}
			}

			String sql = "";
			
			
			if(pc_id>0)
			{
				/*
				 * 查询商品，单据上商品区分普通商品，套装商品，手动拼装，定制商品
				 * 套装商品，手动拼装，定制商品拆散后用包含输入商品也查询出来
				*/
				sql = "select distinct wo.* from waybill_order as wo "
					+"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id and (woi.product_type=1 or woi.product_type=2)"
					+"join porder as po on po.oid = woi.oid "+whereOrderSource
					+"join product as p on p.pc_id = woi.pc_id and woi.pc_id="+pc_id+" "
					+"join country_area_mapping as cam on cam.ccid = wo.ccid "+whereCountry
					+"join country_area as ca on ca.ca_id = cam.ca_id "+whereArea
					+"where 1=1 "+whereCreateDate+whereStorage+outBill+whereProvince+whereStatus+whereCompany+whereProductStatus+whereInterTrackingStatus
					+"union "
					+"select distinct wo.* from waybill_order as wo "
					+"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id and (woi.product_type=2 or woi.product_type=3 or woi.product_type=4)"
					+"join porder as po on po.oid = woi.oid "+whereOrderSource
					+"join product_union as pu on woi.pc_id = pu.set_pid "
					+"join product as p on pu.pid = p.pc_id and p.pc_id = "+pc_id+" "
					+"join country_area_mapping as cam on cam.ccid = wo.ccid "+whereCountry
					+"join country_area as ca on ca.ca_id = cam.ca_id "+whereArea
					+"where 1=1 "+whereCreateDate+whereStorage+outBill+whereProvince+whereStatus+whereCompany+whereProductStatus+whereInterTrackingStatus
					+"order by waybill_id asc";
			}
			else
			{
				sql = "select distinct wo.* from waybill_order as wo "
					+"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id "
					+"join porder as po on po.oid = woi.oid "+whereOrderSource
					+"join product as p on p.pc_id = woi.pc_id "
					+whereProductLineOrCatalog
					+"join country_area_mapping as cam on cam.ccid = wo.ccid "+whereCountry
					+"join country_area as ca on ca.ca_id = cam.ca_id "+whereArea
					+"where 1=1 "+whereCreateDate+whereStorage+outBill+whereProvince+whereStatus+whereCompany+whereProductStatus+whereInterTrackingStatus
					+"order by waybill_id asc";
			}
			return (dbUtilAutoTran.selectMutliple(sql,pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ searchWayBillOrder error:"+e);
		}
	}
	
	/**
	 * 根据运单ID返回tracking_number不为空的订单的ID（为上传trackingNumber到Ebay用）
	 * @param waybill_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOrderIdByWayBill(long waybill_id)
		throws Exception
	{
		try 
		{
			String sql = "select DISTINCT tracking_number,oid,order_source from "+ConfigBean.getStringValue("waybill_order_item")+" where waybill_order_id=?";
			
			DBRow para = new DBRow();
			para.add("waybill_id",waybill_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getOrderIdByWayBill error:"+e);
		}
	}
	
	/**
	 * 根据Oid查询所有有效的tracingNumber
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTracingNumberByOrderId(long oid)
		throws Exception
	{
		try 
		{	
			String sql = "select distinct woi.oid,woi.tracking_number,wo.sc_id from "+ConfigBean.getStringValue("waybill_order_item")+" as woi "
						+"join "+ConfigBean.getStringValue("waybill_order")+" as wo on woi.waybill_order_id = wo.waybill_id and wo.status !="+WayBillOrderStatusKey.CANCEL+" "
						+"where woi.oid = ?";
			
			DBRow para = new DBRow();
			para.add("oid",oid);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getTracingNumberByOrderId error:"+e);
		}
	}
	
	/**
	 * 根据订单号获得该订单所有非终结状态的运单
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getUnfinishedWayBillOrderByOid(long oid)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("waybill_order")+" as wo "
					+"join "+ConfigBean.getStringValue("waybill_order_item")+" as woi on woi.waybill_order_id = wo.waybill_id and wo.status !="+WayBillOrderStatusKey.CANCEL+" and wo.status !="+WayBillOrderStatusKey.SHIPPED+" "
					+"where woi.oid = ?";
		
		DBRow para = new DBRow();
		para.add("oid",oid);
		
		return (dbUtilAutoTran.selectPreMutliple(sql, para));
	}
	
	/**
	 * 进库商品查询对应仓库下覆盖的缺货运单明细（非拆散形式）
	 * @param pc_id
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLackingWayBillItemsNoManual(long pc_id,long ps_id)
		throws Exception
	{
		try 
		{
			String sql = "select woi.* from waybill_order as wo "
						+"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id and woi.product_type=2"
						+"where wo.ps_id= ? and woi.pc_id=? and woi.product_status= "+ProductStatusKey.STORE_OUT+" "
						+"group by wo.waybill_id asc";
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("pc_id",pc_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ addWayBillOrder error:"+e);
		}
	}
	
	/**
	 * 进库商品查询对应仓库下覆盖的缺货运单明细（拆散形式）
	 * @param pc_id
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLackingWayBillItemsManual(long pc_id,long ps_id)
		throws Exception
	{
		try 
		{
			String sql = "select woi.* from waybill_order as wo " 
						+"join waybill_order_item woi  on wo.waybill_id = woi.waybill_order_id and woi.product_type<>3 "
						+"join product_union as pu on woi.pc_id = pu.set_pid "
						+"join product as p on pu.pid = p.pc_id and p.pc_id = ? "
						+"where wo.ps_id= ? and woi.product_status="+ProductStatusKey.STORE_OUT+" "
						+"group by wo.waybill_id asc";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("ps_id",ps_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getLackingWayBillItemsManual error:"+e);
		}
	}
	
	/**
	 * 根据仓库获得缺货运单列表
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLackingWayBillOrdersByPsid(long ps_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("waybill_order")+" where product_status = ? and ps_id = ? order by waybill_id";
			
			DBRow para = new DBRow();
			para.add("product_status",ProductStatusKey.STORE_OUT);
			para.add("ps_id",ps_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getLackingWayBillOrdersByPsid error:"+e);
		}
	}
	
	/**
	 * 获得缺货运单明细
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLackingWayBillItems(long waybill_id)
		throws Exception
	{
		try 
		{
			String sql = "select woi.* from waybill_order as wo "
						+"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id and woi.product_status="+ProductStatusKey.STORE_OUT+" "
						+"where wo.waybill_id = ? "
						+"order by wo.waybill_id asc";
			DBRow para = new DBRow();
			para.add("waybill_id",waybill_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getLackingWayBillItems error:"+e);
		}
	}
	
	
	
	
	 
	public int updateWayBillStatus(long id,long split_count) throws Exception{
		int status = 0;
		try 
		{ 
			String tableName = ConfigBean.getStringValue("waybill_order");
			String countSumSql =  "select sum(waybill_order_item.quantity) as sum_quantity from waybill_order_item where  waybill_order_item.waybill_order_id  = " + id;
			DBRow countSumRow = dbUtilAutoTran.selectSingle(countSumSql);
			if(countSumRow.get("sum_quantity", 0.0) <= 0)
			{
				status = WayBillOrderStatusKey.CANCEL;
			}
			else
			{
				String selectSubWayBillSql = "select count(*) as sum_sub_way from waybill_order where parent_waybill_id = " + id;
				DBRow countSubWayBill = dbUtilAutoTran.selectSingle(selectSubWayBillSql);
				if( countSubWayBill.get("sum_sub_way", 0) <= 0 )
				{
					status = WayBillOrderStatusKey.WAITPRINT;
				}
				else
				{
					status = WayBillOrderStatusKey.SPLIT;
				}
			}
			DBRow updateRow = new DBRow();
			updateRow.add("status", status);
			updateRow.add("split_account", split_count);
			updateRow.add("split_date", DateUtil.NowStr());
			updateRow.add("trace_date",DateUtil.NowStr());
			if(status == WayBillOrderStatusKey.WAITPRINT)
			{
				updateRow.add("trace_flag",0);
			}
			
			dbUtilAutoTran.update(" where waybill_id = " + id , tableName, updateRow);
			return status;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ updateWayBillSetParentWayBillId error:"+e);
		}
	}
	
	public DBRow[] getAllItemInWayBillById(long wayBillId) throws Exception{
		try{
			 String sql = " select waybill_order_item.*,product.p_name , waybill_order_item.pc_id  from waybill_order_item LEFT JOIN  product on product.pc_id = waybill_order_item.pc_id   where  waybill_order_id = " + wayBillId;
			 return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorWayBillOrderMgrZJ.getAllItemInWayBillById():"+e);
		}
	}
	 
	 
	public void deleteWayBillById(long id) throws Exception {
		try{
			 dbUtilAutoTran.delete(" where waybill_id = " + id, ConfigBean.getStringValue("waybill_order"));
 		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.deleteWayBillById():"+e);
		}
	}
	public DBRow[] getWayBillByOid(long oid , PageCtrl pc) throws Exception{
		try
		{
			String sql = "select distinct wo.* from waybill_order as wo "
						+"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id and woi.oid ="+oid;
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorScheduleMgrZr.getWayBillByOid():"+e);
		}
	}
	public DBRow[] getWayBillByTrackingNumber(String number, PageCtrl pc)  throws Exception {
		try{
			String sql = " select * from waybill_order where tracking_number like '%"+number+"%' order by waybill_id desc ";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getWayBillByTrackingNumber():"+e);
		}
	}
	public DBRow getWayBillItemById(long wayBillItemId) throws Exception {
		try{
			String getOldWayItemSql = "select  *  from waybill_order_item where waybill_order_item.waybill_order_item_id = "+wayBillItemId+";" ;
			return dbUtilAutoTran.selectSingle(getOldWayItemSql);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getWayBillItemById():"+e);
		}
	}
	public void updateWayBillItemByIdRow(long wayBillItemId , DBRow row) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("waybill_order_item");
			dbUtilAutoTran.update(" where waybill_order_item_id = " + wayBillItemId, tableName, row);
		}catch (Exception e) {
			throw new Exception("FloorScheduleMgrZr.getWayBillItemById():"+e);
		}
	}
	
	/**
	 * 根据商品，仓库，状态数组查询所对应的运单集合
	 * @param pc_id
	 * @param ps_id
	 * @param product_status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWayBillOrderByPcid(long pc_id,long ps_id,int[] product_status)
		throws Exception
	{
		try 
		{
			String whereProductStatus = "";
			for (int i = 0; i < product_status.length; i++) 
			{
				if(i==0)
				{
					whereProductStatus +=" and (";
				}
				
				whereProductStatus +=" woi.product_status = "+product_status[i];
				
				if(i<product_status.length-1)
				{
					whereProductStatus += " or ";
				}
				if(i==product_status.length-1)
				{
					whereProductStatus += ") ";
				}
			}
			
			String sql = "select distinct wo.* from "+ConfigBean.getStringValue("waybill_order")+" as wo "
						+"join waybill_order_item as woi on woi.waybill_order_id = wo.waybill_id "
						+"where wo.ps_id = ? and woi.pc_id= ? "+whereProductStatus;
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("pc_id",pc_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getWayBillOrderByPcid");
		}
	}
	
	public DBRow[] error(String trackingNumber)
		throws Exception
	{
		String sql = "select DISTINCT woi.waybill_order_id as waybill_id from waybill_order_item as woi "
					+"join waybill_order as wo on woi.waybill_order_id = wo.waybill_id and wo.`status` !=3 "
					+"join shipping_company as sc on sc.sc_id = wo.sc_id "
					+"WHERE oid in (select oid from porder where ems_id like '%"+trackingNumber+"%' ORDER BY oid ASC) " 
					+"ORDER BY waybill_order_id ";
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 运单统计
	 * @param start_date
	 * @param end_date
	 * @param date_type
	 * @param sc_id
	 * @param differences
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] statisticsWayBillOrder(String start_date,String end_date,int date_type,long sc_id,float differences,int difference_type,int cost,int cost_type,long ps_id,int unfinished,int store_out_unfinished,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String dateType = "";
			
			switch (date_type)
			{
				case 1:
					dateType = "create_date";
				break;
				
				case 2:
					dateType = "print_date";
				break;
				
				case 3:
					dateType = "delivery_date";
				break;
				
				case 4:
					dateType = "split_date";
				break;
			}
			
			String costType = "";
			switch (cost_type)
			{
				case 1:
					costType = "print_cost";
				break;
				
				case 2:
					costType = "delivery_cost";
				break;
				
				case 3:
					costType = "lacking_cost";
				break;
			}
			
			String whereShipCompany = "";
			if(sc_id>0)
			{
				whereShipCompany = " and sc_id = "+sc_id+" ";
			}
			
			String whereDifference = "";
			if(differences>0)
			{
				switch (difference_type) 
				{
					case 1:			
						whereDifference = " and (abs(all_weight-delivery_weight))>"+differences;
					break;
					
					case 2:
						whereDifference = " and (all_weight-delivery_weight)>"+differences;
					break;
					
					case 3:
						whereDifference = " and (delivery_weight-all_weight)>"+differences;
					break;
				}
			}
			
			String whereCost = "";
			if(cost>-1)
			{
				whereCost = " and "+costType+"> "+cost*60*60*1000+" ";
			}
			
			String whereProductStore = "";
			if(ps_id>0)
			{
				whereProductStore = " and ps_id = "+ps_id;
			}
			
			String whereUnfinished = "";
			if(unfinished>-1)
			{
				
				TDate tdate = new TDate();
				tdate.addDay(unfinished*-1);
				whereUnfinished = " and create_date<'"+tdate.formatDate("yyyy-MM-dd HH:mm:ss")+"' and status !="+WayBillOrderStatusKey.CANCEL+" and status !="+WayBillOrderStatusKey.SHIPPED;
			}
			
			String whereStoreOutUnfinished = "";
			if(store_out_unfinished>-1)
			{
				TDate tdate = new TDate();
				tdate.addDay(unfinished*-1);
				whereStoreOutUnfinished = " and create_date<'"+tdate.formatDate("yyyy-MM-dd HH:mm:ss")+"' and product_status="+ProductStatusKey.STORE_OUT;
			}
			
			String sql = "select * from waybill_order where "+dateType+">'"+start_date+" 0:00:00' and "+dateType+"<'"+end_date+" 23:59:59'"+whereShipCompany
						+whereDifference+whereCost+whereProductStore+whereUnfinished+whereStoreOutUnfinished;
			
			return dbUtilAutoTran.selectMutliple(sql,pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ statisticsWayBillOrder error:"+e);
		}
	}
	
	/**
	 * 根据内部运单号查询
	 * @param waybill_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow getWayBillOrderByWayBillId(long waybill_id)
		throws Exception
	{
		try {
			String sql = "select * from "+ConfigBean.getStringValue("waybill_order")+" where waybill_id=?";
			
			DBRow para = new DBRow();
			para.add("waybill_id",waybill_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getWayBillOrderByWayBillId error:"+e);
		}
	}
	
	/**
	 * 返回运单的总销售额
	 * @param waybill_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getWayBillSaleroomByWayBillId(long waybill_id)
		throws Exception
	{
		try 
		{
			String sql = "select sum(saleroom) as all_saleroom from "+ConfigBean.getStringValue("waybill_order_item")+" where waybill_order_id = ?";
			
			DBRow para = new DBRow();
			para.add("waybill_id",waybill_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getWayBillSaleroomByWayBillId error:"+e);
		}
	}
	
	/**
	 * 根据商品ID，订单ID获得发货时间（）
	 * @param oid
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryDateByOidandPcid(long oid,long pc_id)
		throws Exception
	{
		try {
			String sql = "select wo.delivery_date from waybill_order  as wo "
						+"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id and (woi.product_type =1 or woi.product_type=2) "
						+"where woi.pc_id = ? and woi.oid=? "
						+" union all " 
						+"select wo.delivery_date from waybill_order as wo "
						+"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id and (woi.product_type =2 or woi.product_type =3 or woi.product_type=4) "
						+"join product_union as pu ON woi.pc_id = pu.set_pid " 
						+"where pu.pid = ? and woi.oid=? "
						+"order by delivery_date asc ";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("oid",oid);
			para.add("pid",pc_id);
			para.add("oid2",oid);
			
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getDeliveryDateByOidandPcid error:"+e);
		}
	}
	
	/**
	 * 添加运单跟进
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addWaybillNote(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("waybill_note"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBIllOrderMgrZJ addWaybillNote error:"+e);
		}
	}
	
	/**
	 * 获得超期缺货运单
	 * @param stockout_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStockoutWaybillOrders(int stockout_day,long ps_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("waybill_order")+" where product_status = "+ProductStatusKey.STORE_OUT+" and date_add(trace_date,interval "+stockout_day+" day)<=current_date";
			
			if(ps_id !=0)
			{
				sql += " and ps_id = "+ps_id;
			}
			
			sql += "  order by waybill_id asc ";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getStockoutWaybillOrders error:"+e);
		}
	}
	
	/**
	 * 获得超期缺货运单数量
	 * @param stockout_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public int getStockoutWaybillOrderCount(int stockout_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(waybill_id) as stockout_count from "+ConfigBean.getStringValue("waybill_order")+" where product_status = "+ProductStatusKey.STORE_OUT+" and date_add(trace_date,interval "+stockout_day+" day)<=current_date";
			
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			
			return row.get("stockout_count",0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getStockoutWaybillOrders error:"+e);
		}
	}
	
	/**
	 * 获得超期缺货运单数量根据仓库分组
	 * @param stockout_day
	 * @return
	 * @throws Exception
	 */
	public DBRow getStockoutWaybillOrderCountByPs(int stockout_day,long ps_id)
		throws Exception
	{
		try 
		{
			String sql = "select title,ps_id,count(waybill_id) as stockout_count from "+ConfigBean.getStringValue("waybill_order")+" wo "
						+"join product_storage_catalog as psc on wo.ps_id = psc.id "
						+" where product_status = "+ProductStatusKey.STORE_OUT+" and date_add(trace_date,interval "+stockout_day+" day)<=current_date and wo.ps_id = "+ps_id;
			
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getStockoutWaybillOrderCountGroupPs error:"+e);
		}
	}
	
	/**
	 * 超期正常未发货订单
	 * @param overdue_hour
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOverdueSendWaybillOrders(int overdue_hour,int track_day,long ps_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("waybill_order")+" where date_add(create_date,interval "+overdue_hour+" hour)<=CURRENT_TIMESTAMP and date_add(trace_date,interval "+track_day+" day)<=current_date and product_status = "+ProductStatusKey.IN_STORE+" and (status!="+WayBillOrderStatusKey.CANCEL+" and status !="+WayBillOrderStatusKey.SHIPPED+")";
			
			if(ps_id!=0)
			{
				sql +=" and ps_id = "+ps_id;
			}
			
			sql += " order by waybill_id asc ";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBillOrderMgrZJ getOverdueSendWaybillOrders error:"+e);
		}
	}
	
	/**
	 * 超期正常未发货订单数量
	 * @param overdue_hour
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getOverdueSendWaybillOrderCount(int overdue_hour,int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(waybill_id) as overdue_send_count from "+ConfigBean.getStringValue("waybill_order")+" where date_add(create_date,interval "+overdue_hour+" hour)<=CURRENT_TIMESTAMP and date_add(trace_date,interval "+track_day+" day)<=current_date and product_status = "+ProductStatusKey.IN_STORE+" and (status="+WayBillOrderStatusKey.PERINTED+" or status ="+WayBillOrderStatusKey.SPLIT+" or status = "+WayBillOrderStatusKey.WAITPRINT+")";
			
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			
			
			return row.get("overdue_send_count",0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBIllOrderMgrZJ getOverdueSendWaybillOrderCount error:"+e);
		}
	}
	
	/**
	 * 超期正常未发货订单数量按仓库分组
	 * @param overdue_hour
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public DBRow getOverdueSendWaybillOrderCountByPs(int overdue_hour,int track_day,long ps_id)
		throws Exception
	{
		try 
		{
			String sql = "select title,ps_id,count(waybill_id) as overdue_send_count from "+ConfigBean.getStringValue("waybill_order")+" wo "
						+"join product_storage_catalog as psc on wo.ps_id = psc.id "
						+" where date_add(create_date,interval "+overdue_hour+" hour)<=CURRENT_TIMESTAMP and date_add(trace_date,interval "+track_day+" day)<=current_date and product_status = "+ProductStatusKey.IN_STORE+" and (status!="+WayBillOrderStatusKey.CANCEL+" and status !="+WayBillOrderStatusKey.SHIPPED+") and wo.ps_id = "+ps_id;
			
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWayBIllOrderMgrZJ getOverdueSendWaybillOrderCountGroupByPs error:"+e);
		}
	}
	
	public DBRow[]  getDate(String startDate,String endDate)
	throws Exception 
   {
	 long  DAY = 24L * 60L * 60L * 1000L;   
	 Date dateTemp = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
     Date dateTemp2 = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
	 long n =  (dateTemp2.getTime() - dateTemp.getTime() ) / DAY   ;
	 int num = Long.valueOf(n).intValue();
	 DBRow[] row = new DBRow[num+1];
	 Calendar calendarTemp = Calendar.getInstance();
	 calendarTemp.setTime(dateTemp);
     int i= 0;
     while (calendarTemp.getTime().getTime()!= dateTemp2.getTime())
     {
            String temp = new SimpleDateFormat("yyyy-MM-dd").format(calendarTemp.getTime()) ;
            row[i] = new DBRow();
            row[i].add("date",temp );
            calendarTemp.add(Calendar.DAY_OF_YEAR, 1);
            i++;
     }
     row[i] = new DBRow();
     row[i].add("date", new SimpleDateFormat("yyyy-MM-dd").format(dateTemp2.getTime()) );
	 return row;
  }
	
	
	/**
	 * 运单数据统计
	 * @param st
	 * @param en
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	
	public DBRow[] getWayBillCount(String st,String en,long ps_id)
	throws Exception
{
	try 
	{
		DBRow[] dateStr =getDate(st,en);
		String sql="select title,sum(quantity) as sum_quantity,";
		for(int i=0; i<dateStr.length; i++)
		{
			sql+="  SUM(CASE delivery_date WHEN '"+dateStr[i].getString("date")+"' THEN quantity ELSE 0  END)  date_"+i;
			if(i!=(dateStr.length-1))
			{
				sql +=", ";
			}
		}
		String psIdWhere="";
		 if(ps_id!=0)
		{
			 psIdWhere=" and wo.ps_id = "+ps_id;
		}
		 sql += " from( select wo.ps_id,psc.title,woi.quantity as quantity,left(wo.delivery_date,10)as delivery_date from "
			 +ConfigBean.getStringValue("waybill_order")+" as wo " +
				"join "+ConfigBean.getStringValue("product_storage_catalog")+
		" as psc on wo.ps_id=psc.id  join "+ConfigBean.getStringValue("waybill_order_item")+
		" as woi on wo.waybill_id=woi.waybill_order_id  where  wo.delivery_date between '"+st+" 00:00:00' and'"
		+en+" 23:59:59'"+psIdWhere+"  order by wo.waybill_id asc  ) needs group by ps_id";
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	catch (Exception e) 
	{
		throw new Exception("FloorWayBillOrderMgrZJ getWayBillCount error:"+e);
	}
}
	
	
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
