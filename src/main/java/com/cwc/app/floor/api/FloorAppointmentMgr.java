package com.cwc.app.floor.api;

/*****************************************************
 * @author yuehaibo
 * 
 * Load, MasterOrder, Order相关数据库操作
 */

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;

import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;
import com.cwc.db.PageCtrl;

@SuppressWarnings({"unused"})
public class FloorAppointmentMgr {
	private DBUtilAutoTran_Optm dbUtilAutoTran;
	//DateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm");
	//DateFormat format2 = new SimpleDateFormat("MM/dd/yyyy");
	//DateFormat format3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat format4 = new SimpleDateFormat("yyyy-MM-dd");
	@SuppressWarnings("serial")
	private static Map<String,String> weeks = new HashMap<String,String>(){{
	    put("0", "WEEK:A");
	    put("1", "WEEK:B");
	    put("2", "WEEK:C");
	    put("3", "WEEK:D");
	    put("4", "WEEK:E");
	    put("5", "WEEK:F");
	    put("6", "WEEK:G");
	    put("-1","DAY");
	}};
	
	/**
	 * 获取SCAC
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getSCACById(long id) throws Exception {
		DBRow para = new DBRow();
		para.add("id", id);
		return this.dbUtilAutoTran.selectPreSingle("select * from carrier_scac_mcdot where id=?", para);
	}
	/**
	 * 
	 * @param storageId
	 * @param type
	 * @param appointment
	 * @throws Exception
	 */
	public void updWork(String storageId,String appointment,String customer_id) throws Exception{
		boolean rt  = this.sumOneWorkOut(appointment.substring(0,10), Integer.parseInt(appointment.substring(11,13)), Long.parseLong(storageId), customer_id,true);
		if (rt==false) {
			throw new Exception("can not appointment");
		}
	}
	
	/**
	 * 
	 * @param storageId
	 * @param type
	 * @param appointment
	 * @throws Exception
	 */
	public void updWorkForSub(String storageId,String appointment,String customer_id) throws Exception{
		boolean rt  = this.subOneWorkOut(appointment.substring(0,10), Integer.parseInt(appointment.substring(11,13)), Long.parseLong(storageId), customer_id,true);
		if (rt==false) {
			throw new Exception("can not appointment");
		}
	}
	/**
	 * 
	 * @param appointment
	 * @param storageId
	 * @param cid
	 * @return
	 * @throws Exception
	 */
	public DBRow getAppointExcep(String appointment,String storageId,String cid) throws Exception{
		try{
			String sql="select * from wms_appointment_excep where appointment_type='outbound' and storage_id="+storageId+" and work_date='"+appointment.substring(0,10)+"' and hour="+appointment.substring(11,13)+" and customer_id='"+cid+"'";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("getAppointExcep(String appointment,String storageId,String cid) error:" + e);
		}
	}
	/**
	 * 约车限制
	 * @param appointment
	 * @param storageId
	 * @return
	 * @throws Exception
	 */
	
	@SuppressWarnings("deprecation")
	public DBRow getAppointWork(String appointment,String storageId) throws Exception{
		DBRow rt = null;
		try{
			String sql="select (`limit_out`-`work_out`) as `limit`, work_out as work, `limit_out` as limit_, work_out, bid,storage_id, work_date, `hour` from wms_appointment_work_total where storage_id="+storageId+"  and work_date='"+appointment.substring(0,10)+"' and hour="+appointment.substring(11,13);
			rt = this.dbUtilAutoTran.selectSingle(sql);
			if (rt==null || rt.get("bid", 0)==0) {
				rt = new DBRow();
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				DateUtil.getTimeZone(Long.parseLong(storageId));
				int limit = getLimitDefault(DateUtil.StringToDate(appointment), appointment.substring(0,10), Integer.parseInt(appointment.substring(11,13)), format.parse(appointment).getDay(), Long.parseLong(storageId), "outbound");
				rt.put("limit", limit);
				rt.put("work", 0);
				rt.put("limit_", limit);
				rt.put("work_out", "0");
				rt.put("bid", 0);
				rt.put("storage_id", storageId);
			}
		}catch(Exception e){
			throw new Exception("getAppointWork(String appointment,String storageId) error:" + e);
		}
		return rt;
	}
	
	/**
	 * 仓库某个点已经预约的车辆
	 * @param appointment
	 * @param storageId
	 * @return
	 * @throws Exception
	 */
	public int getAppointCount(String appointment,String storageId) throws Exception{
		try{
			String sql="select count(1) as cnt from wms_appointment where status='open' and appointment_time='"+appointment+"' and storage_id="+storageId;
			DBRow row=this.dbUtilAutoTran.selectSingle(sql);
			return Integer.parseInt(row.getString("cnt"));
		}catch(Exception e){
			throw new Exception("getAppointCount(String appointment,String storageId) error:" + e);
		}
	}
	
	/**
	 * 比较pickup appointment是否发生变化
	 * @return
	 * @throws Exception 
	 */
	
	@SuppressWarnings("rawtypes")
	public List diffAppointment(String id,String pkapp,String type) throws Exception{
		List<Comparable> list=new ArrayList<Comparable>();
		
		String sql="select appointment_time from wms_appointment where id =(select aid from wms_appointment_invoice where invoice_id="+id+" and invoice_type='"+type+"') and status='open'";
		
		DBRow row=this.dbUtilAutoTran.selectSingle(sql);
		
		if(row==null && pkapp!=null && pkapp.trim().length()>0){
			list.add(1);
			list.add("");
			
			return list;
		}else if(row==null && (pkapp==null ||  pkapp.trim().length()==0)){ //都是空没变化
			list.add(4);
			list.add("");
			
			return list;
		}
		
		pkapp=pkapp==null?"":pkapp;
		
		String appointment_time=row.getString("appointment_time");
		
		if(appointment_time!=null){ //系统中该订单已经有约车时间
			String old_pk=appointment_time.substring(0,19);
			if(old_pk.equals(pkapp)){ //约车时间没有变化
				list.add(4);
				list.add("");
			}else{ //约车时间有变化
				list.add(3);
				list.add(old_pk);
			}
		}else{ //没有约车时间
			if(!StringUtils.isEmpty(pkapp)){
				list.add(1);
				list.add("");
			}else{
				list.add(4);
				list.add("");
			}
		}
		
		return list;
	}
	
	public DBRow getLoginStorageCatalogById(String id) throws Exception {
		try {
			String sql = "SELECT * FROM product_storage_catalog WHERE id = "
					+ id;
			// System.out.println(sql);
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("getLoginStorageCatalogById() error:" + e);
		}
	}

	/**
	 * 添加appointment日志
	 * 
	 * @param type
	 * @param id
	 * @param to
	 * @param userName
	 * @throws Exception
	 */
	public void addAppLog(String type, String id, String from, String to,
			String userName,String remark) throws Exception {
		try {
			// 1. type=load，从空到新aid，或从aid到空
			// 2. type=order，从空到aid，从aid到aid，从aid到空
			// 3. type=po，从空到新aid，或从aid到空
			// 根据type和id查询appointment_invoice表，查出order自己的aid和所属load的aid
			DBRow data = new DBRow();
			data.put("aid_from", from);
			data.put("aid_to", to);
			data.put("invoice_type", type);
			data.put("invoice", id);
			data.put("user_created", userName);
			data.put("date_created", new Date());
			data.put("remark", remark);
			dbUtilAutoTran.insert(
					ConfigBean.getStringValue("wms_appointment_log"), data);
		} catch (Exception e) {
			throw new Exception("addAppLog() error:" + e);
		}
	}

	/**
	 * 删除要去除的关联单据
	 * 
	 * @param aid
	 * @param idTypeStr
	 * @param userName
	 * @throws Exception
	 */
	public void delInvoiceByAidAndNotInList(String aid, String idTypeStr,
			String userName) throws Exception {
		try {
			String delSql = "SELECT CONCAT(invoice_id, invoice_type) conn, invoice_id, invoice_type FROM "
					+ ConfigBean.getStringValue("wms_appointment_invoice")
					+ " WHERE aid = "
					+ aid
					+ " AND CONCAT(invoice_id, invoice_type) NOT IN ("
					+ idTypeStr + ")";
			// System.out.println(delSql);
			DBRow[] rows = dbUtilAutoTran.selectMutliple(delSql);
			if (rows != null) {
				for (DBRow row : rows) {
					// 删掉多余的，还要记日志
					// 查询当前aid
					String id = row.getString("invoice_id");
					String type = row.getString("invoice_type");
					String aidFrom = getCurrentAid(type, id);

					String sqlWhere = " WHERE aid = " + aid
							+ " AND CONCAT(invoice_id, invoice_type) = '"
							+ row.getString("conn") + "'";
					dbUtilAutoTran.delete(sqlWhere, ConfigBean
							.getStringValue("wms_appointment_invoice"));

					String aidTo = getCurrentAid(type, id);

					addAppLog(type, id, aidFrom, aidTo, userName,type+":"+id+" cancel appointment");
				}
				
				String sql="select * from wms_appointment_invoice where aid="+aid;
				DBRow [] datas=this.dbUtilAutoTran.selectMutliple(sql);
				Set<String> set=new HashSet<String>();
				for(DBRow row:datas){
					set.add(row.getString("invoice_id"));
				}
				if(!set.isEmpty()){
					DBRow order=getB2bOrder(set);
					DBRow para=new DBRow();
					para.put("id", aid);
					String mabd=order.getString("mabd");
					String reqShipDate=order.getString("req_ship_date");
					if(!StringUtils.isEmpty(mabd))
						para.put("mabd", mabd);
					if(!StringUtils.isEmpty(reqShipDate))
						para.put("req_ship_date", reqShipDate);
					para.put("feight_type", order.getString("feight_type"));
					updateApp(para);
				}else{
					DBRow para=new DBRow();
					para.put("id", aid);
					para.put("mabd", null);
					para.put("req_ship_date", null);
					para.put("feight_type", "");
					updateApp(para);
				}
			}
		} catch (Exception e) {
			throw new Exception("delInvoiceByAidAndNotInList() error:" + e);
		}
	}

	/**
	 * 查询单据现在的有效aid load，PO 查自己的 order，先查所在load的，没有load再查自己的
	 * 
	 * @param type
	 * @param id
	 * @return
	 */
	public String getCurrentAid(String type, String id) throws Exception {
		try {
			String sql = "";
			DBRow para = new DBRow();
			if ("order".equals(type)) {
				sql = "SELECT (SELECT aid FROM wms_appointment_invoice WHERE invoice_type='load' AND invoice_id = (SELECT load_id FROM wms_load_order_master WHERE order_id = b.b2b_oid)) AS load_aid,"
						+ " (SELECT aid FROM wms_appointment_invoice WHERE invoice_type='order' AND invoice_id = b.b2b_oid) AS order_aid"
						+ " from b2b_order b where b.b2b_oid = ?";
				
				para.put("b.b2b_oid", id);
				DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
				if (result != null) {
					String loadAid = result.getString("load_aid");
					String orderAid = result.getString("order_aid");

					if (loadAid != null && !"".equals(loadAid)) { // 如果所在load有aid，返回load的aid
						return loadAid;
					} else if (orderAid != null && !"".equals(orderAid)) {
						return orderAid;
					}
				}
			} else {
				sql = "SELECT aid FROM "
						+ ConfigBean.getStringValue("wms_appointment_invoice")
						+ " WHERE invoice_type = ? AND invoice_id = ?";
				
				para.put("invoice_type", type);
				para.put("invoice_id", id);
				DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
				if (result != null) {
					String aid = result.getString("aid");
					if (aid != null && !"".equals(aid)) {
						return aid;
					}
				}
			}

			// 查不到返回NULL
			return null;
		} catch (Exception e) {
			throw new Exception("getCurrentAid() error:" + e);
		}
	}

	public DBRow[] getAppLogs(String aid) throws Exception {
		try {
			String sql = "SELECT DISTINCT t.invoice,t.remark, t.invoice_type, t.aid_from, t.aid_to, t.user_created, DATE_FORMAT(t.date_created,'%m/%d/%Y %H:%i') as date_created, "

					// 单据ID转NO
					+ "IF(t.invoice_type='order', (SELECT b2b_oid FROM b2b_order WHERE b2b_oid = t.invoice), "
					+ "IF(t.invoice_type='load', (SELECT load_no FROM wms_load WHERE load_id = t.invoice LIMIT 1), t.invoice)) AS invoice_no"
					+ " FROM "
					+ ConfigBean.getStringValue("wms_appointment_log")
					+ " AS t WHERE t.aid_from = " + aid + " OR t.aid_to = "
					+ aid + " ORDER BY date_created DESC";
			
			DBRow[] result = dbUtilAutoTran.selectMutliple(sql);
			return result;
		} catch (Exception e) {
			throw new Exception("getAppLogs() error:" + e);
		}
	}

	// 根据ID搜索order
	public DBRow[] getAppOrderById(String orderId) throws Exception {
		try {
			String sql = "SELECT t.order_number as num, t.b2b_oid as id, CONCAT(t.customer_id, '&', t.b2b_order_address) AS con, t.customer_id as customer"
					+ ", t.b2b_order_address as shipToAddress, 'order' as type,  t.mabd,"
					+ "(SELECT SUM(pallet_spaces) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid)  AS plateNum,"
					+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid)  AS volume,"
					+ "(SELECT SUM(total_weight) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid) AS weight "
					+ "FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " as t  where b2b_oid = " + orderId;

			// System.out.println(sql);
			return (dbUtilAutoTran.selectMutliple(sql));
		} catch (Exception e) {
			throw new Exception("getAppOrderById() error:" + e);
		}
	}

	// 根据ID搜索order
	public DBRow[] getAppOrdersLikeId(String orderId) throws Exception {
		try {
			String sql = "SELECT t.order_number as num, t.b2b_oid as id, CONCAT(t.customer_id, '&', t.b2b_order_address) AS con, t.customer_id as customer"
					+ ", t.b2b_order_address as shipToAddress, 'order' as type,  t.mabd,"
					+ "(SELECT SUM(pallet_spaces) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid)  AS plateNum,"
					+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid)  AS volume,"
					+ "(SELECT SUM(total_weight) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid) AS weight "
					+ "FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " as t  where b2b_oid like %" + orderId + "%";

			// System.out.println(sql);
			return (dbUtilAutoTran.selectMutliple(sql));
		} catch (Exception e) {
			throw new Exception("getAppOrdersLikeId() error:" + e);
		}
	}

	// 根据ID查询Load
	public DBRow[] getAppLoadById(String loadId) throws Exception {
		try {
			String sql = "SELECT t.load_no as num, t.load_id as id, '' AS con, t.customer_id as customer, t.carrier_id, '' as shipToAddress, 'load' as type, "
					+ "(SELECT DATE_FORMAT(MAX(mabd),'%m-%d-%Y %H:%i') FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id)) AS mabd,"
					+ "(SELECT SUM(pallet_spaces) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT order_id FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id))  AS plateNum,"
					+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT order_id FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id))  AS volume,"
					+ "(SELECT SUM(total_weight) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT order_id FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id)) AS weight "
					+ "FROM "
					+ ConfigBean.getStringValue("wms_load")
					+ " as t  where load_id = " + loadId;

			// System.out.println(sql);
			return (dbUtilAutoTran.selectMutliple(sql));
		} catch (Exception e) {
			throw new Exception("getAppLoadById() error:" + e);
		}
	}

	// 根据ID查询Load
	public DBRow[] getAppLoadsLikeId(String loadId) throws Exception {
		try {
			String sql = "SELECT t.load_no as num, t.load_id as id, '' AS con, t.customer_id as customer, '' as shipToAddress, 'load' as type, "
					+ "(SELECT DATE_FORMAT(MAX(mabd),'%m-%d-%Y %H:%i') FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id)) AS mabd,"
					+ "(SELECT SUM(pallet_spaces) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT order_id FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id))  AS plateNum,"
					+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT order_id FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id))  AS volume,"
					+ "(SELECT SUM(total_weight) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT order_id FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id)) AS weight "
					+ "FROM "
					+ ConfigBean.getStringValue("wms_load")
					+ " as t  where load_id like %" + loadId + "%";

			// System.out.println(sql);
			return (dbUtilAutoTran.selectMutliple(sql));
		} catch (Exception e) {
			throw new Exception("getAppLoadsLikeId() error:" + e);
		}
	}

	// 根据NO模糊查询PO
	public DBRow[] getAppPoByNo(String po) throws Exception {
		try {
			String sql = "SELECT t.retail_po as num, t.retail_po as id, '' AS con, '' as customer"
					+ ", '' as shipToAddress, 'po' as type,"
					+ "(SELECT DATE_FORMAT(MAX(mabd),'%m-%d-%Y %H:%i') FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po)) AS mabd,"
					+ "(SELECT SUM(pallet_spaces) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po))  AS plateNum,"
					+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po))  AS volume,"
					+ "(SELECT SUM(total_weight) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po)) AS weight "
					+ "FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " as t  where t.retail_po = " + po;

			// System.out.println(sql);
			return (dbUtilAutoTran.selectMutliple(sql));
		} catch (Exception e) {
			throw new Exception("getAppPoByNo() error:" + e);
		}
	}

	// 根据NO模糊查询PO
	public DBRow[] getAppPosLikeNo(String po) throws Exception {
		try {
			String sql = "SELECT t.retail_po as num, t.retail_po as id, '' AS con, '' as customer"
					+ ", '' as shipToAddress, 'po' as type,"
					+ "(SELECT DATE_FORMAT(MAX(mabd),'%m-%d-%Y %H:%i') FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po)) AS mabd,"
					+ "(SELECT SUM(pallet_spaces) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po))  AS plateNum,"
					+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po))  AS volume,"
					+ "(SELECT SUM(total_weight) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po)) AS weight "
					+ "FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " as t  where t.retail_po like %" + po + "%";

			// System.out.println(sql);
			return (dbUtilAutoTran.selectMutliple(sql));
		} catch (Exception e) {
			throw new Exception("getAppPosLikeNo() error:" + e);
		}
	}

	public long addAppInvoice(String aid, String type, String id,
			String uid) throws Exception {
		try {
			DBRow data = new DBRow();
			data.put("aid", aid);
			data.put("system_type", "1");
			data.put("order_type", "1");
			data.put("invoice_id", id);
			data.put("invoice_type", type);
			
			DBRow para=new DBRow();
			String docLevel = "2";
			if ("load".equals(type)) {
				docLevel = "3";
				
				String sql="select * from wms_load where load_id="+id;
				DBRow row=this.dbUtilAutoTran.selectSingle(sql);
				para.put("mabd", row.getString("mabd"));
				para.put("req_ship_date", row.getString("req_ship_date"));
				para.put("feight_type", row.getString("feight_type"));
			} else if ("order".equals(type)) {
				docLevel = "1";
				
				String sql="select * from wms_appointment_invoice where aid="+aid;
				DBRow [] rows=this.dbUtilAutoTran.selectMutliple(sql);
				Set<String> set=new HashSet<String>();
				for(DBRow row:rows){
					set.add(row.getString("invoice_id"));
				}
				set.add(id);
				DBRow order=getB2bOrder(set);
				para.put("mabd", order.getString("mabd"));
				para.put("req_ship_date", order.getString("req_ship_date"));
				para.put("feight_type", order.getString("feight_type"));
			}
			data.put("doc_level", docLevel);
			data.put("creator", uid);
			data.put("create_time", new Date());
			data.put("finish_time", new Date());
			long pkid=dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("wms_appointment_invoice"), data);
			para.put("id",pkid);
			updateApp(para);
			return pkid; 
		} catch (Exception e) {
			throw new Exception("addAppInvoice() error:" + e);
		}
	}

	public DBRow getB2bOrder(Collection<String> orderIdList) throws Exception{
		try{
			if(orderIdList.isEmpty())
				return null;
			String sql="select min(mabd) as mabd,requested_date as req_ship_date,freight_carrier as freight_type,freight_term as freight_term from b2b_order where b2b_oid in ("+StringUtils.join(orderIdList, ",")+")";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("getB2bOrder(Set<String> orderIdList) error:" + e);
		}
	}
	
	public long addApp(DBRow row) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(
					ConfigBean.getStringValue("wms_appointment"), row);
		} catch (Exception e) {
			throw new Exception("addApp() error:" + e);
		}
	}

	public void delApp(String aid) throws Exception {
		try {
			
			dbUtilAutoTran.delete(" WHERE id = " + aid,	ConfigBean.getStringValue("wms_appointment"));
		} catch (Exception e) {
			throw new Exception("delApp() error:" + e);
		}
	}
	
	public DBRow getAppInvoice(String id,String type) throws Exception{
		try {
			
			String sql="select * from wms_appointment_invoice where invoice_id = " + id +" and invoice_type='"+type+"'";
			return this.dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("getAppInvoice(String id,String type)  error:" + e);
		}
	}
	
	public String delAppInvoice(String id,String type,String userName) throws Exception{
		try {
			DBRow row=getAppInvoice(id,type);
			String aid=row.getString("aid");
			dbUtilAutoTran.delete(" WHERE invoice_id = " + id +" and invoice_type='"+type+"'",
					ConfigBean.getStringValue("wms_appointment_invoice"));
			
			if(row!=null)
				addAppLog(type, id, "", row.getString("aid"), userName,type+":"+id+" cancel appointment");
			
			return aid;
		} catch (Exception e) {
			throw new Exception("delAppInvoice(String id,String type)  error:" + e);
		}
	}
	
	/**
	 * 
	 * @param id oid or loadno
	 * @param type
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	public String delAppInvoiceAndApp(String id, String type,String userName) throws Exception{
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			DBRow row=getAppInvoice(id,type);
			if(row==null)
				return null;
			
			String aid=row.getString("aid","0");
			
			dbUtilAutoTran.delete(" WHERE invoice_id = " + id +" and invoice_type='"+type+"'",	ConfigBean.getStringValue("wms_appointment_invoice"));
			
			if(row!=null)
				addAppLog(type, id, "", row.getString("aid"), userName,type+":"+id+" cancel appointment");
			
			DBRow [] apps=getInvoiceByAID(aid);
			if(apps.length==0){
				DBRow old = this.getAppByAID(aid);
				delApp(aid);
				//删除预约次数。
				String oappointment = format2.format((Date)old.getValue("appointment_time"));
				String oldtype = old.getString("appointment_type");
				if ("inbound".equals(oldtype)) {
					subOneWorkIn(oappointment.substring(0,10), Integer.parseInt(oappointment.substring(11,13)), old.get("storage_id",0L), null,true);
				}
				if ("outbound".equals(oldtype)) {
					subOneWorkOut(oappointment.substring(0,10), Integer.parseInt(oappointment.substring(11,13)), old.get("storage_id",0L), null,true);
				}
			}
			return aid;
		} catch (Exception e) {
			throw new Exception("delAppInvoice(String id,String type)  error:" + e);
		}
	}
	
	public void delAppInvoice(String aid) throws Exception {
		try {
			dbUtilAutoTran.delete(" WHERE aid = " + aid,
					ConfigBean.getStringValue("wms_appointment_invoice"));
		} catch (Exception e) {
			throw new Exception("delAppInvoice() error:" + e);
		}
	}

	public void updateApp(DBRow row) throws Exception {
		try {
			String id = row.getString("id");
			row.remove("id");
			row.remove("key");
			dbUtilAutoTran.update(" WHERE id = " + id,
					ConfigBean.getStringValue("wms_appointment"), row);
		} catch (Exception e) {
			throw new Exception("updateApp() error:" + e);
		}
	}

	public DBRow[] getInvoicesByaid(String aid) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT t.load_no as num, t.load_id as id,t.carrier_id as customer, '' AS con, '' as shipToAddress, 'load' as type, "
					+ "(SELECT Min(mabd) FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE b2b_oid IN (SELECT order_id FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id)) AS mabd,"
					+ "(SELECT SUM(pallet_spaces) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT order_id FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id))  AS plateNum,"
					+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT order_id FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id))  AS volume,"
					+ "(SELECT SUM(total_weight) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT order_id FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id)) AS weight "
					+ "FROM "
					+ ConfigBean.getStringValue("wms_load")
					+ " as t  where t.load_id IN (SELECT invoice_id FROM "
					+ ConfigBean.getStringValue("wms_appointment_invoice")
					+ " WHERE invoice_type = 'load' AND aid = " + aid + ")");

			sql.append(" UNION ");

			sql.append("SELECT t.b2b_oid as num, t.b2b_oid as id,t.carriers as customer, CONCAT(t.customer_id, '&', t.b2b_order_address) AS con"
					+ ", t.b2b_order_address as shipToAddress, 'order' as type,  t.mabd,"
					+ "(SELECT SUM(pallet_spaces) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid)  AS plateNum,"
					+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid)  AS volume,"
					+ "(SELECT SUM(b2b_weight*b2b_delivery_count) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid) AS weight "
					+ "FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " as t  where t.b2b_oid IN (SELECT invoice_id FROM "
					+ ConfigBean.getStringValue("wms_appointment_invoice")
					+ " WHERE invoice_type = 'order' AND aid = " + aid + ")");

			sql.append(" UNION ");

			sql.append("SELECT t.retail_po as num, t.retail_po as id,t.carriers as customer, '' AS con"
					+ ", '' as shipToAddress, 'po' as type,"
					+ "(SELECT Min(mabd) FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po)) AS mabd,"
					+ "(SELECT SUM(pallet_spaces) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po))  AS plateNum,"
					+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po))  AS volume,"
					+ "(SELECT SUM(total_weight) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po)) AS weight "
					+ "FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " as t  where t.retail_po IN (SELECT invoice_id FROM "
					+ ConfigBean.getStringValue("wms_appointment_invoice")
					+ " WHERE invoice_type = 'po' AND aid = " + aid + ")");

			sql.append(" UNION ");
			
			sql.append("SELECT invoice_id as num, invoice_id as id,'' as customer, '' AS con, '' as shipToAddress, "
					+ "'rn' as type,'' as mabd,0 as plateNum,0 as volume,0 as weight FROM ").append(ConfigBean.getStringValue("wms_appointment_invoice"))
				.append(" where aid=").append(aid).append(" and invoice_type='rn'");
			
			// System.out.println(sql);

			return (dbUtilAutoTran.selectMutliple(sql.toString()));
		} catch (Exception e) {
			throw new Exception("getInvoicesByaid() error:" + e);
		}
	}

	// 分页查询Load
	public DBRow[] getAppLoadList(String loadNo, String start_time,
			String end_time, String checkIds, String storageId,PageCtrl pc) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select * from (SELECT t.load_no as num,date_created, t.load_id as id, '' AS con, (select carrier from carrier_scac_mcdot where scac=t.carrier_id) as customer,"
					+ " '' as shipToAddress, 'load' as type, (select distinct send_psid from b2b_order where b2b_oid in (select order_id from wms_load_order_master where load_id = t.load_id) limit 1) as send_psid,"
					+ "0 as jet_lag,(SELECT MIN(mabd) FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE b2b_oid IN (SELECT order_id FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id)) AS mabd,"
					+ "(SELECT SUM(pallet_spaces) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT order_id FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id))  AS plateNum,"
					+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT order_id FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id))  AS volume,"
					+ "(SELECT SUM(total_weight) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT order_id FROM "
					+ ConfigBean.getStringValue("wms_load_order_master")
					+ " WHERE load_id = t.load_id)) AS weight "
					+ "FROM "
					+ ConfigBean.getStringValue("wms_load")
					+ " as t  WHERE load_id NOT IN (SELECT invoice_id FROM "
					+ ConfigBean.getStringValue("wms_appointment_invoice")
					+ " WHERE invoice_type = 'load') ");

			DBRow para = new DBRow();

			if (checkValue(loadNo)) {
				
			} else{
				loadNo="";
			}
			sql.append(" AND load_id IN (SELECT load_id FROM " + ConfigBean.getStringValue("wms_load_order_master")	+ " WHERE load_no = '" + loadNo + "')");

			if (checkValue(checkIds)) {
				sql.append(" AND  load_id NOT IN (" + checkIds + ")");
			}
			
			if(!StrUtil.isBlankNullUndefined(storageId)){
				sql.append(" ) m where 1=1 and m.send_psid=" +storageId);
			}else{
				sql.append(") m where 1=1 ");
			}
			
			if(!StrUtil.isBlankNullUndefined(start_time)){
				sql.append(" AND  DATE_FORMAT(mabd,'%Y-%m-%d %H:00:00')>'" + DateUtil.showUTCTime(start_time, Long.parseLong(storageId)) + "'");
			}
			
			if(!StrUtil.isBlankNullUndefined(end_time)){
				sql.append(" AND  DATE_FORMAT(mabd,'%Y-%m-%d %H:00:00')<'" + DateUtil.showUTCTime(end_time, Long.parseLong(storageId)) + "'");
			}
			
			sql.append(" ORDER BY m.date_created desc");
			
			// System.out.println(sql);
			return ((para.size() == 0) ? dbUtilAutoTran.selectMutliple(
					sql.toString(), pc) : dbUtilAutoTran.selectPreMutliple(
					sql.toString(), para, pc));
		} catch (Exception e) {
			throw new Exception("getWmsLoadList() error:" + e);
		}
	}

	// 分页查询PO
	public DBRow[] getAppPoList(String po, String start_time, String end_time,
			String checkIds,String storageId, PageCtrl pc) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			String [] chids = checkIds.split(",");
			sql.append("SELECT t.retail_po as num, t.retail_po as id, '' AS con, '' as customer, t.carriers as carrier_id"
					+ ", '' as shipToAddress, 'po' as type,"
					+ "(SELECT min(mabd) FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po)) AS mabd,"
					+ "(SELECT SUM(pallet_spaces) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po))  AS plateNum,"
					+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po))  AS volume,"
					+ "(SELECT SUM(total_weight) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid IN (SELECT b2b_oid FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " WHERE retail_po = t.retail_po)) AS weight "
					+ "FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " as t  WHERE t.b2b_order_status not in (1,8,9,10) and t.retail_po NOT IN (SELECT invoice_id FROM "	+ ConfigBean.getStringValue("wms_appointment_invoice") + " WHERE invoice_type = 'po') "
					+ " and b2b_oid NOT IN (SELECT invoice_id FROM " + ConfigBean.getStringValue("wms_appointment_invoice") + " WHERE invoice_type = 'order' )");
			DBRow para = new DBRow();

			if (checkValue(po)) {
				sql.append(" AND t.retail_po like concat('%',?, '%')");
				para.put("po", po);
			}
			// if (checkValue(status)) {
			// para.put("status", status);
			// sql.append(" AND status = ?");
			// }
			// if (checkValue(start_time)) {
			// para.put("start_time", start_time);
			// sql.append(" AND date_created >= ?");
			// }
			// if (checkValue(end_time)) {
			// para.put("end_time", end_time);
			// sql.append(" AND date_created <= ?");
			// }
			
			if (checkValue(checkIds)) {
				sql.append(" AND  t.retail_po NOT IN (");
				int idx = 0;
				for (String ids : chids) {
					idx++;
					if (idx==1) {
						sql.append("?");
						para.put("idx"+idx, ids);
					} else {
						sql.append(",?");
						para.put("idx"+idx, ids);
					}
				}
				sql.append(")");
			}

			sql.append(" GROUP BY t.retail_po ORDER BY t.retail_po desc");
			// System.out.println(sql);
			return ((para.size() == 0) ? dbUtilAutoTran.selectMutliple(
					sql.toString(), pc) : dbUtilAutoTran.selectPreMutliple(
					sql.toString(), para, pc));
		} catch (Exception e) {
			throw new Exception("getAppPoList() error:" + e);
		}
	}
	
	/**
	 * 按PO查找Order
	 * 
	 * @param pono
	 * @param start_time
	 * @param end_time
	 * @param checkIds
	 * @param storageId
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAppOrderListByPo(String pono, String start_time, String end_time, String checkIds,String storageId, PageCtrl pc) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT t.b2b_oid as num, t.b2b_oid as id, CONCAT(m.brand_name, ' & ', t.b2b_order_address) AS con, t.customer_id as customer, t.carriers as carrier_id"
					+ ", t.b2b_order_address as shipToAddress, 'order' as type,  t.mabd, 0 as jet_lag,"
					+ "(SELECT SUM(pallet_spaces) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid)  AS plateNum,"
					+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid)  AS volume,"
					+ "(SELECT SUM(total_weight) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid) AS weight "
					+ "FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " as t ,customer_brand m WHERE t.customer_id=m.cb_id and b2b_order_status not in (1,8,9,10) and (t.load_no ='' or t.load_no is null) "
					+ " and b2b_oid NOT IN (SELECT invoice_id FROM " + ConfigBean.getStringValue("wms_appointment_invoice")	+ " WHERE invoice_type = 'order') "
					+ " and t.retail_po NOT IN (SELECT invoice_id FROM " + ConfigBean.getStringValue("wms_appointment_invoice") + " WHERE invoice_type = 'po')");

			DBRow para = new DBRow();
			if (checkValue(pono)) {
				
			} else {
				pono = "";
			}
			sql.append(" AND retail_po = '" + pono + "'");

			if (checkValue(checkIds)) {
				sql.append(" AND  b2b_oid NOT IN (" + checkIds + ")");
			}
			
			if(!StrUtil.isBlankNullUndefined(start_time)){
				sql.append(" AND  DATE_FORMAT(mabd,'%Y-%m-%d %H:00:00')>'" + DateUtil.showUTCTime(start_time, Long.parseLong(storageId)) + "'");
			}
			
			if(!StrUtil.isBlankNullUndefined(end_time)){
				sql.append(" AND  DATE_FORMAT(mabd,'%Y-%m-%d %H:00:00')<'" + DateUtil.showUTCTime(end_time, Long.parseLong(storageId)) + "'");
			}
			
			if(!StrUtil.isBlankNullUndefined(storageId)){
				sql.append(" and send_psid=" +storageId);
			}
			
			sql.append(" ORDER BY t.b2b_oid desc");

			// System.out.println(sql);
			return ((para.size() == 0) ? dbUtilAutoTran.selectMutliple(
					sql.toString(), pc) : dbUtilAutoTran.selectPreMutliple(
					sql.toString(), para, pc));
		} catch (Exception e) {
			throw new Exception("getAppOrderListByPo() error:" + e);
		}
	}
	// 分页查询Order
	public DBRow[] getAppOrderList(String orderno, String start_time,
			String end_time, String checkIds,String storageId, PageCtrl pc) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT t.b2b_oid as num, t.b2b_oid as id, CONCAT(m.brand_name, ' & ', t.b2b_order_address) AS con, t.customer_id as customer, t.carriers as carrier_id"
					+ ", t.b2b_order_address as shipToAddress, 'order' as type,  t.mabd, 0 as jet_lag,"
					+ "(SELECT SUM(pallet_spaces) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid)  AS plateNum,"
					+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid)  AS volume,"
					+ "(SELECT SUM(total_weight) FROM "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " WHERE b2b_oid = t.b2b_oid) AS weight "
					+ "FROM "
					+ ConfigBean.getStringValue("b2b_order")
					+ " as t ,customer_brand m WHERE t.customer_id=m.cb_id and b2b_order_status not in (1,8,9,10) and (t.load_no ='' or t.load_no is null) and b2b_oid NOT IN (SELECT invoice_id FROM "
					+ ConfigBean.getStringValue("wms_appointment_invoice")
					+ " WHERE invoice_type = 'order') and t.retail_po NOT IN (SELECT invoice_id FROM " + ConfigBean.getStringValue("wms_appointment_invoice") + " WHERE invoice_type = 'po')");

			DBRow para = new DBRow();
			if (checkValue(orderno)) {
				sql.append(" AND b2b_oid = '" + orderno + "'");
			}

			if (checkValue(checkIds)) {
				sql.append(" AND  b2b_oid NOT IN (" + checkIds + ")");
			}
			
			if(!StrUtil.isBlankNullUndefined(start_time)){
				sql.append(" AND  DATE_FORMAT(mabd,'%Y-%m-%d %H:00:00')>'" + DateUtil.showUTCTime(start_time, Long.parseLong(storageId)) + "'");
			}
			
			if(!StrUtil.isBlankNullUndefined(end_time)){
				sql.append(" AND  DATE_FORMAT(mabd,'%Y-%m-%d %H:00:00')<'" + DateUtil.showUTCTime(end_time, Long.parseLong(storageId)) + "'");
			}
			
			if(!StrUtil.isBlankNullUndefined(storageId)){
				sql.append(" and send_psid=" +storageId);
			}
			
			sql.append(" ORDER BY t.b2b_oid desc");

			// System.out.println(sql);
			return ((para.size() == 0) ? dbUtilAutoTran.selectMutliple(
					sql.toString(), pc) : dbUtilAutoTran.selectPreMutliple(
					sql.toString(), para, pc));
		} catch (Exception e) {
			throw new Exception("getAppOrderList() error:" + e);
		}
	}
	
	/**
	 * 获取预约关联列表
	 * 1先查下load。有值直接返回。
	 * 2按Order或PO查询。查询非load的记录
	 * 
	 * @param rfno
	 * @param loadId
	 * @param orderIds
	 * @param scac
	 * @param loadFlag 1:多load 0:单load
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAppLRList(String rfno, String loadId, String orderIds, String scac,int loadFlag) throws Exception {
		String sql = "select l.load_no as num, l.load_id as id, 'load' as type,"
				+ "l.carrier_id as scac,l.customer_id, CONCAT(m.brand_name,'') AS con, '' as shipToAddress,"
				+ "(select distinct send_psid from b2b_order where b2b_oid in (select order_id from wms_load_order_master m where m.load_id = l.load_id) limit 1) as send_psid,"
				+ "(SELECT MIN(mabd) FROM b2b_order WHERE b2b_oid IN (SELECT order_id FROM wms_load_order_master m WHERE m.load_id = l.load_id)) AS mabd,"
				+ "(SELECT SUM(pallet_spaces) FROM b2b_order_item WHERE b2b_oid IN (SELECT order_id FROM wms_load_order_master WHERE load_id = l.load_id)) AS pallets,"
				+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM b2b_order_item WHERE b2b_oid IN (SELECT order_id FROM wms_load_order_master WHERE load_id = l.load_id)) AS volumes,"
				+ "(SELECT SUM(total_weight) FROM b2b_order_item WHERE b2b_oid IN (SELECT order_id FROM wms_load_order_master WHERE load_id = l.load_id)) AS weights, l.load_no "
				+ "from wms_load l left join customer_brand m on l.customer_id=m.cb_id where load_no = ? and load_id NOT IN (SELECT invoice_id FROM wms_appointment_invoice WHERE invoice_type = 'load')";
		DBRow para = new DBRow();
		para.add("load_no", rfno);
		//近可上一个load
		if (loadId!=null && !"".equals(loadId)) {
			String[] ids = loadId.split(",");
			String sqlid = "";
			if (ids.length>0) {
				for (int i=0;i<ids.length;i++) {
					if (i==0) {
						sqlid = "?";
					} else {
						sqlid += ",?";
					}
					para.add("oid"+i, Long.parseLong(ids[i]));
				}
				sql += " and l.load_id not in  ("+ sqlid +")";
			}
			para.add("loadId", Long.parseLong(loadId));
		}
		
			
		DBRow[] datas = dbUtilAutoTran.selectPreMutliple(sql, para);
		
		if (datas!=null && datas.length>0) return datas;
		
		sql = "SELECT t.b2b_oid as num, t.b2b_oid as id, 'order' as type,"
				+ "t.carriers as scac,t.customer_id, CONCAT(m.brand_name,' & ',t.b2b_order_address) AS con,"
				+ "t.b2b_order_address as shipToAddress,t.send_psid,t.mabd,"
				+ "(SELECT SUM(pallet_spaces) FROM b2b_order_item WHERE b2b_oid = t.b2b_oid)  AS pallets,"
				+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM b2b_order_item WHERE b2b_oid = t.b2b_oid)  AS volumes,"
				+ "(SELECT SUM(total_weight) FROM b2b_order_item WHERE b2b_oid = t.b2b_oid) AS weights, t.load_no "
				+ "FROM b2b_order as t left join customer_brand m on t.customer_id = m.cb_id "
				+ " where b2b_order_status not in (1,8,9,10) and IFNULL(t.load_no,'')='' "
				//+ " and b2b_oid NOT IN (SELECT invoice_id FROM wms_appointment_invoice WHERE invoice_type = 'order') "
				+ " and NOT EXISTS (SELECT i.invoice_id FROM wms_appointment_invoice i WHERE i.invoice_type = 'order' and i.invoice_id = t.b2b_oid) "
				+ " and NOT EXISTS (SELECT m2.order_id FROM wms_load_order_master m2 WHERE m2.order_id = t.b2b_oid) "
				+ " and (t.customer_dn = ? or t.b2b_oid = ? or t.customer_dn = ? or (t.retail_po = ? and NOT EXISTS (select b2b_oid from b2b_order t where t.retail_po = ? and IFNULL(t.load_no,'')!='' and b2b_order_status not in (1,8,9,10))  ) )";
		
		para = new DBRow();
		para.add("customer_dn", rfno);
		para.add("b2b_oid", rfno);
		para.add("dn", rfno);
		para.add("retail_po", rfno);
		para.add("retail_po2", rfno);
		if (orderIds!=null && !"".equals(orderIds)) {
			String[] ids = orderIds.split(",");
			String sqlid = "";
			if (ids.length>0) {
				for (int i=0;i<ids.length;i++) {
					if (i==0) {
						sqlid = "?";
					} else {
						sqlid += ",?";
					}
					para.add("oid"+i, Long.parseLong(ids[i]));
				}
				sql += " and t.b2b_oid not in ("+ sqlid +")";
			}
		}
		datas = dbUtilAutoTran.selectPreMutliple(sql, para);
		return datas;
	}
	
	public DBRow [] getCityState(String refNo) throws SQLException{
		String sql="select ship_to_city as city,ship_to_state as state  from wms_master_order where load_id in (select load_id from wms_load where load_no=?) ";
		DBRow para=new DBRow();
		para.put("load_no", refNo);
		DBRow[] datas = dbUtilAutoTran.selectPreMutliple(sql, para);
		if(datas.length>=1){ //多条记录 city state 为空
			return datas;
		}else{ //没查出来
			sql="select deliver_city as city,address_state_deliver as state from b2b_order where (b2b_oid=? or customer_dn=?)";
			para.clear();
			para.put("b2b_oid", refNo);
			para.put("customer_dn", refNo);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
	}
	
	/**
	 * 获取预约关联列表
	 * 1先查下load。有值直接返回。
	 * 2按Order或PO查询。查询非load的记录
	 * 
	 * @param rfno
	 * @param loadId
	 * @param orderIds
	 * @param scac
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAppLRList(String rfno, String loadIds, String orderIds, String scac) throws Exception {
		String sql = "select l.load_no as num, l.load_id as id, 'load' as type,"
				+ "l.carrier_id as scac,l.customer_id, CONCAT(m.brand_name,'') AS con, '' as shipToAddress,"
				+ "(select distinct send_psid from b2b_order where b2b_oid in (select order_id from wms_load_order_master m where m.load_id = l.load_id) limit 1) as send_psid,"
				+ "(SELECT MIN(mabd) FROM b2b_order WHERE b2b_oid IN (SELECT order_id FROM wms_load_order_master m WHERE m.load_id = l.load_id)) AS mabd,"
				+ "(SELECT SUM(pallet_spaces) FROM b2b_order_item WHERE b2b_oid IN (SELECT order_id FROM wms_load_order_master WHERE load_id = l.load_id)) AS pallets,"
				+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM b2b_order_item WHERE b2b_oid IN (SELECT order_id FROM wms_load_order_master WHERE load_id = l.load_id)) AS volumes,"
				+ "(SELECT SUM(total_weight) FROM b2b_order_item WHERE b2b_oid IN (SELECT order_id FROM wms_load_order_master WHERE load_id = l.load_id)) AS weights, l.load_no "
				+ "from wms_load l left join customer_brand m on l.customer_id=m.cb_id where load_no = ? and load_id NOT IN (SELECT invoice_id FROM wms_appointment_invoice WHERE invoice_type = 'load')";
		DBRow para = new DBRow();
		para.add("load_no", rfno);
		//近可上一个load
		if (loadIds!= null && !"".equals(loadIds)) {
			sql += " and l.load_id not in ("+loadIds+")";
//			return null;
		}		
		DBRow[] datas = dbUtilAutoTran.selectPreMutliple(sql, para);
		
		if (datas!=null && datas.length>0) return datas;
		
		sql = "SELECT t.b2b_oid as num, t.b2b_oid as id, 'order' as type,"
				+ "t.carriers as scac,t.customer_id, CONCAT(m.brand_name,' & ',t.b2b_order_address) AS con,"
				+ "t.b2b_order_address as shipToAddress,t.send_psid,t.mabd,"
				+ "(SELECT SUM(pallet_spaces) FROM b2b_order_item WHERE b2b_oid = t.b2b_oid)  AS pallets,"
				+ "(SELECT SUM(b2b_volume*b2b_delivery_count) FROM b2b_order_item WHERE b2b_oid = t.b2b_oid)  AS volumes,"
				+ "(SELECT SUM(total_weight) FROM b2b_order_item WHERE b2b_oid = t.b2b_oid) AS weights, t.load_no "
				+ "FROM b2b_order as t left join customer_brand m on t.customer_id = m.cb_id "
				+ " where b2b_order_status not in (1,8,9,10) and IFNULL(t.load_no,'')='' "
				//+ " and b2b_oid NOT IN (SELECT invoice_id FROM wms_appointment_invoice WHERE invoice_type = 'order') "
				+ " and NOT EXISTS (SELECT i.invoice_id FROM wms_appointment_invoice i WHERE i.invoice_type = 'order' and i.invoice_id = t.b2b_oid) "
				+ " and NOT EXISTS (SELECT m2.order_id FROM wms_load_order_master m2 WHERE m2.order_id = t.b2b_oid) "
				+ " and (t.customer_dn = ? or t.b2b_oid = ? or t.customer_dn = ? or (t.retail_po = ? and NOT EXISTS (select b2b_oid from b2b_order t where t.retail_po = ? and IFNULL(t.load_no,'')!='' and b2b_order_status not in (1,8,9,10))  ) )";
		
		para = new DBRow();
		para.add("customer_dn", rfno);
		para.add("b2b_oid", rfno);
		para.add("dn", rfno);
		para.add("retail_po", rfno);
		para.add("retail_po2", rfno);
		if (orderIds!=null && !"".equals(orderIds)) {
			String[] ids = orderIds.split(",");
			String sqlid = "";
			if (ids.length>0) {
				for (int i=0;i<ids.length;i++) {
					if (i==0) {
						sqlid = "?";
					} else {
						sqlid += ",?";
					}
					para.add("oid"+i, Long.parseLong(ids[i]));
				}
				sql += " and t.b2b_oid not in ("+ sqlid +")";
			}
		}
		datas = dbUtilAutoTran.selectPreMutliple(sql, para);
		return datas;
	}
	
	public DBRow getAppByAID(String aid) throws Exception {
		try {
			String sql = "SELECT id,storage_id,storage_name,storage_linkman,storage_linkman_tel,carrier_id,carrier_name,"
					+ "appointment_time,"
					+ "etd,eta,carrier_linkman,carrier_linkman_tel,appointment_type,"
					+ "licenseplate,driver_license,driver_name, status, user_created, date_created, date_updated FROM "
					+ ConfigBean.getStringValue("wms_appointment")
					+ " WHERE id=?";
			DBRow para = new DBRow();
			para.put("id", aid);
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (Exception e) {
			throw new Exception("getAppByAID() error:" + e);
		}
	}

	/**
	 * 根据aid查询Invoice列表
	 * 
	 * @param aid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getInvoiceByAID(String aid) throws Exception {
		try {
			String sql = "SELECT *, IF(invoice_type='order', (SELECT b2b_oid as order_number FROM b2b_order WHERE b2b_oid = invoice_id), IF(invoice_type='load', (SELECT load_no FROM wms_load WHERE load_id = invoice_id LIMIT 1), invoice_id)) AS invoice_no FROM "
					+ ConfigBean.getStringValue("wms_appointment_invoice")
					+ " WHERE aid=?";
			DBRow para = new DBRow();
			para.put("aid", aid);
			// System.out.println(sql + "--" + aid);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("getInvoiceByAID() error:" + e);
		}
	}

	public DBRow[] getAppointmentByPage(String selectType, String number,
			String appTime, String carrier, String company, String boundType,PageCtrl pc)
			throws Exception {
		DateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		DateFormat format2 = new SimpleDateFormat("MM/dd/yyyy");
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT wms_appointment.id,storage_id,storage_name,storage_linkman,storage_linkman_tel,carrier_id,carrier_name,"
					+ "mabd,req_ship_date,gate_check_in,feight_type,"
					+ "appointment_time,"
					+ "DATE_FORMAT(etd,'%m/%d/%Y %H:%i') AS etd,"
					+ "DATE_FORMAT(eta,'%m/%d/%Y %H:%i') AS eta,"
					+ "carrier_linkman,carrier_linkman_tel,"
					+ "appointment_type,"
					+ "licenseplate,driver_license,driver_name, 0 as jet_lag ,"
					+ "case when UNIX_TIMESTAMP(appointment_time)>= UNIX_TIMESTAMP(CURDATE()) then 0 else 1 END as flag "
					+ " FROM " + ConfigBean.getStringValue("wms_appointment") + " WHERE status != 'Close'");

			if (checkValue(number)) {// 按照单据类型和单据号搜索
				sql.append(" AND wms_appointment.id IN (SELECT aid FROM "
						+ ConfigBean.getStringValue("wms_appointment_invoice")
						+ " WHERE ");
				sql.append("invoice_id IN (SELECT load_id FROM "
						+ ConfigBean.getStringValue("wms_load")
						+ " WHERE load_no like '%" + number + "%')");

				sql.append(" OR invoice_id IN (SELECT b2b_oid FROM "
						+ ConfigBean.getStringValue("b2b_order")
						+ " WHERE b2b_oid like '%" + number + "%')");

				sql.append(" OR invoice_id like '%" + number + "%'");
				sql.append(")");
			}

			if (checkValue(appTime)) {// 按照预约日期
				if (appTime.contains(":")) {
					Date date = format1.parse(appTime);
					sql.append(" AND DATE_FORMAT(appointment_time,'%Y-%m-%d %H:00:00') = '" + DateUtil.showUTCTime(date, Long.parseLong(company)) + "'");
					//sql.append(" AND DATE_FORMAT(appointment_time,'%m/%d/%Y %H:00') = '" + appTime + "'");
				} else {
					Date date1 = format2.parse(appTime);
					Date date2 = new Date(format2.parse(appTime).getTime()+ 24*3600*1000);
					sql.append(" AND DATE_FORMAT(appointment_time,'%Y-%m-%d %H:00:00') >= '" + DateUtil.showUTCTime(date1, Long.parseLong(company)) + "'");
					sql.append(" AND DATE_FORMAT(appointment_time,'%Y-%m-%d %H:00:00') < '" + DateUtil.showUTCTime(date2, Long.parseLong(company)) + "'");
					//sql.append(" AND DATE_FORMAT(appointment_time,'%m/%d/%Y') = '" + appTime + "'");
				}
			}

			if (checkValue(carrier)) {// 按照卡车公司
				sql.append(" AND carrier_id = '" + carrier + "'");
			}

			if (checkValue(company)) {// 按照预约仓库
				sql.append(" AND storage_id = '" + company + "'");
			}
			
			if(checkValue(boundType)){
				sql.append(" AND appointment_type='"+boundType+"'");
			}
				
			sql.append(" ORDER BY flag , wms_appointment.appointment_time ASC");

			// System.out.println(sql);
			
			DBRow[] result = dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			
			return result;
		} catch (Exception e) {
			throw new Exception("getAppointmentByPage() error:" + e);
		}
	}

	public DBUtilAutoTran_Optm getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	private boolean checkValue(String key) throws Exception {
		try {
			if (key == null) {
				return false;
			} else if ("".equals(key)) {
				return false;
			} else if (",".equals(key)) {
				return false;
			} else
				return true;
		} catch (Exception e) {
			throw new Exception("checkValue() error:" + e);
		}
	}

	public String listToString(List<String> stringList) throws Exception {
		try {
			if (stringList == null) {
				return null;
			}
			StringBuilder result = new StringBuilder();
			boolean flag = false;
			for (String string : stringList) {
				if (flag) {
					result.append(",");
				} else {
					flag = true;
				}
				result.append(string);
			}
			return result.toString();
		} catch (Exception e) {
			throw new Exception("listToString() error:" + e);
		}
	}
	/**
	 * 
	 * 过滤WmsAppointmentWork
	 * 
	 * @param dbRow
	 * @param pc
	 * @return
	 * @throws SQLException <b>Date:</b>2015年2月9日下午3:20:01<br>
	 * @author: cuicong
	 */
	public DBRow[] filterWmsAppointmentWork1(DBRow dbRow, PageCtrl pc) throws SQLException {
		String sql = "select * from wms_appointment_work where 1=1";
		if (dbRow.get("storage_id", 0) != 0) {
			sql += " and storage_id=" + dbRow.get("storage_id", 0);
		}
		if (!dbRow.get("appointment_type", "").equals("")) {
			sql += " and appointment_type='" + dbRow.get("appointment_type", "") + "'";
		}
		if (!dbRow.get("work_date_begin", "").equals("")) {
			sql += " and work_date>='" + dbRow.get("work_date", "") + "'";
		}
		if (!dbRow.get("work_date_end", "").equals("")) {
			sql += " and work_date<='" + dbRow.get("work_date", "") + "'";
		}
		return dbUtilAutoTran.selectMutliple(sql, pc);
	}
	
	/**
	 * 按日统计仓库下一时段预约的数量
	 * 
	 * @param start 开始时间 必填 YYYY-MM-DD
	 * @param end 结束时间 必填 YYYY-MM-DD
	 * @param storage_id 仓库id 必填
	 * @return
	 * @throws SQLException
	 * @author: wangcr
	 */
	public DBRow[] sumWorkByStartEndTotal(String start, String end, long storage_id) throws SQLException {
		DBRow para = new DBRow();
		String sql = "SELECT SUM(`work`+ `work_out`) as `work`, SUM(`limit_in`+`limit_out`) as `limit`,work_date `date`, SUM(`work_out`) `work_out` FROM "+ 
		ConfigBean.getStringValue("wms_appointment_work_total") +" where storage_id = ? and work_date >= ? and work_date <= ? "
				//+ " and appointment_type='inbound'"
				+ " group by work_date";
		para.add("storage_id", storage_id );
		para.add("start", start );
		para.add("end", end );
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	/**
	 * 
	 * @param appt
	 * @param storage_id
	 * @param scac
	 * @return
	 * @throws SQLException
	 */
	public DBRow findWorkByAppt(String appt,long storage_id,String scac,String freightType) throws SQLException{
		String sql="select * from "+ConfigBean.getStringValue("wms_appointment")+" where storage_id = ? and appointment_time=? and carrier_id=? and feight_type=?";
		DBRow para=new DBRow();
		para.put("storage_id", storage_id);
		para.put("appointment_time", appt);
		para.put("carrier_id", scac);
		para.put("freightType", freightType);
		return this.dbUtilAutoTran.selectPreSingle(sql, para);
	}
	
	/**
	 * 
	 * @param appt
	 * @param storage_id
	 * @param scac
	 * @param freightType
	 * @return
	 * @throws SQLException
	 */
	public DBRow findWorkByApptOid(String appt,long storage_id,String scac,String freightType,String id) throws SQLException{
		String sql="select * from "+ConfigBean.getStringValue("wms_appointment_invoice")+" where invoice_id=? and invoice_type='order' and aid in (select id from "+ConfigBean.getStringValue("wms_appointment")+" where storage_id = ? and appointment_time=? and carrier_id=? and feight_type=?)";
		DBRow para=new DBRow();
		para.put("invoice_id", id);
		para.put("storage_id", storage_id);
		para.put("appointment_time", appt);
		para.put("carrier_id", scac);
		para.put("freightType", freightType);
		return this.dbUtilAutoTran.selectPreSingle(sql, para);
	}
	
	/*
	public DBRow[] sumWorkByStartEnd(String start, String end, long storage_id) throws SQLException {
		DBRow para = new DBRow();
		String sql = "SELECT SUM(`work`) `work`, SUM(`limit`) `limit`,work_date `date`,appointment_type FROM "+ 
		ConfigBean.getStringValue("wms_appointment_work") +" where storage_id = ? and work_date >= ? and work_date <= ? group by work_date, appointment_type";
		para.add("storage_id", storage_id );
		para.add("start", start );
		para.add("end", end );
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}*/
	
	/**
	 * 按日统计仓库下一时段预约的数量,计算基于UTC时间
	 * @param start  开始时间 必填 YYYY-MM-DD HH:00:00
	 * @param end 结束时间 必填 YYYY-MM-DD HH:00:00
	 * @param storage_id
	 * @param hour 偏移小时数（正负）
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] sumWorkByStartEndTotal(String start, String end, long storage_id, int hour) throws SQLException {
		DBRow para = new DBRow();
		String sql = "SELECT SUM(`work`+`work_out`) as `work`,  SUM(`work`) as `work_in`, SUM(`work_out`) `work_out`, SUM(`limit_in`) as `limit_in`, SUM(`limit_out`) as `limit_out`, SUM(`limit_in`+`limit_out`) as `limit`, work_dt `date` FROM ( "+
			"SELECT `work`,`work_out`,`limit_in`,`limit_out`, DATE(DATE_ADD(DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00'),INTERVAL ? HOUR)) `work_dt` FROM "+ ConfigBean.getStringValue("wms_appointment_work_total") + " where"+
			" storage_id= ? and " +
			//"appointment_type='inbound' and "+
			" DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00') >= DATE_FORMAT(?,'%Y-%m-%d %H:00:00') and "+
			" DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00') < DATE_FORMAT(?,'%Y-%m-%d %H:00:00') "+
		") as t group by work_dt";
		para.add("hour", hour );
		para.add("storage_id", storage_id );
		para.add("start", start );
		para.add("end", end );
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	/*
	public DBRow[] sumWorkByStartEnd(String start, String end, long storage_id, int hour) throws SQLException {
		DBRow para = new DBRow();
		String sql = "SELECT SUM(`work`) `work`, SUM(`limit`) `limit`, work_dt `date`,appointment_type FROM ( "+
			"SELECT `work`, `limit`, DATE(DATE_ADD(DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00'),INTERVAL ? HOUR)) `work_dt`, appointment_type FROM "+ ConfigBean.getStringValue("wms_appointment_work") + " where"+
			" storage_id= ? and "+
			" DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00') >= DATE_FORMAT(?,'%Y-%m-%d %H:00:00') and "+
			" DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00') < DATE_FORMAT(?,'%Y-%m-%d %H:00:00') "+
		") as t group by work_dt, appointment_type";
		para.add("hour", hour );
		para.add("storage_id", storage_id );
		para.add("start", start );
		para.add("end", end );
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}*/
	
	/**
	 * 按小时统计仓库下某日预约的数量
	 * 
	 * @param start 开始时间 必填 YYYY-MM-DD HH:00:00
	 * @param end 结束时间 必填 YYYY-MM-DD HH:00:00
	 * @param storage_id
	 * @return
	 * @throws SQLException
	 * @author: wangcr
	 */
	public DBRow[] sumWorkByDayTotal(String start, String end, long storage_id) throws SQLException {
		DBRow para = new DBRow();
		String sql = "SELECT SUM(`work`+`work_out`) as `work`, SUM(`limit_in`+`limit_out`) as `limit`, SUM(`limit_in`) `limit_in`,SUM(`limit_out`) `limit_out`, work_date `date`,SUM(`work`) `work_in`, SUM(`work_out`) `work_out`,`hour` FROM "+ 
				ConfigBean.getStringValue("wms_appointment_work_total") +" WHERE storage_id=? and DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00') >= DATE_FORMAT(?,'%Y-%m-%d %H:00:00') and DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00') < DATE_FORMAT(?,'%Y-%m-%d %H:00:00') group by work_date, `hour`";
				//ConfigBean.getStringValue("wms_appointment_work") +" WHERE storage_id=? and work_date >= ? and work_date <= ? group by work_date, `hour`, appointment_type";
		para.add("storage_id", storage_id );
		para.add("start", start );
		para.add("end", end );
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}/*
	public DBRow[] sumWorkByDay(String start, String end, long storage_id) throws SQLException {
		DBRow para = new DBRow();
		String sql = "SELECT SUM(`work`) `work`, SUM(`limit`) `limit`, work_date `date`, appointment_type,`hour` FROM "+ 
				ConfigBean.getStringValue("wms_appointment_work") +" WHERE storage_id=? and DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00') >= DATE_FORMAT(?,'%Y-%m-%d %H:00:00') and DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00') < DATE_FORMAT(?,'%Y-%m-%d %H:00:00') group by work_date, `hour`, appointment_type";
				//ConfigBean.getStringValue("wms_appointment_work") +" WHERE storage_id=? and work_date >= ? and work_date <= ? group by work_date, `hour`, appointment_type";
		para.add("storage_id", storage_id );
		para.add("start", start );
		para.add("end", end );
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}*/
	/**
	 * 预约基础数据
	 * 
	 * @param start
	 * @param end
	 * @param storage_id
	 * @return
	 * @throws SQLException
	 * @author: wangcr
	 */
	public DBRow[] sumBaseByDayTotal(String start, String end, long storage_id) throws SQLException {
		DBRow para = new DBRow();
		String sql = 
		"select SUM(`limit_in`) `sumin`, SUM(`limit_out`) `sumout`, work_date, `hour` from "+ 
		ConfigBean.getStringValue("wms_appointment_work_total") +
		" where storage_id = ? and work_date >= ? and work_date <= ? group by work_date, `hour`";
		para.add("1storage_id", storage_id );
		para.add("1start", start );
		para.add("1end", end );
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}/*
	public DBRow[] sumBaseByDay(String start, String end, long storage_id) throws SQLException {
		DBRow para = new DBRow();
		String sql = 
		"select SUM(`inlimit`) `sumin`, SUM(`outlimit`) `sumout`, work_date, `hour` from ("+
		"SELECT SUM(`limit`) `inlimit`, 0 `outlimit`, work_date, `hour` FROM "+ ConfigBean.getStringValue("wms_appointment_work") +" where storage_id = ? and work_date >= ? and work_date <= ? and appointment_type='inbound' group by work_date, `hour`, appointment_type"+
		" UNION "+
		"SELECT 0 `inlimit` , SUM(`limit`) `outlimit`, work_date, `hour` FROM "+ ConfigBean.getStringValue("wms_appointment_work") +" where storage_id = ? and work_date >= ? and work_date <= ? and appointment_type='outbound' group by work_date, `hour`, appointment_type"+
		") t group by work_date, `hour`";
		para.add("1storage_id", storage_id );
		para.add("1start", start );
		para.add("1end", end );
		para.add("2storage_id", storage_id );
		para.add("2start", start );
		para.add("2end", end );
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}*/
	
	/**
	 * 预约基础数据，使用UTC计时
	 * 
	 * @param start 开始时间 必填 YYYY-MM-DD HH:00:00
	 * @param end 结束时间 必填 YYYY-MM-DD HH:00:00
	 * @param storage_id
	 * @param hour 偏移小时数（正负）
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] sumBaseByDayTotal(String start, String end, long storage_id, int hour) throws SQLException {
		DBRow para = new DBRow();
		String sql = "select SUM(`inlimit`) `sumin`, SUM(`outlimit`) `sumout`, work_date, `hour` from ( "+
		"select inlimit, outlimit, DATE(DATE_ADD(DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00'),INTERVAL ? HOUR)) as `work_date`, EXTRACT(HOUR FROM DATE_ADD(DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00'),INTERVAL ? HOUR)) as `hour` from ( "+
		  "SELECT SUM(`limit_in`) `inlimit`, SUM(`limit_out`) `outlimit`, work_date, `hour`  FROM "+ ConfigBean.getStringValue("wms_appointment_work_total") +" where storage_id = ? and DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00') >= DATE_FORMAT(?,'%Y-%m-%d %H:00:00') and DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00') < DATE_FORMAT(?,'%Y-%m-%d %H:00:00') group by work_date, `hour`"+
		") as ts "+
		") t group by work_date, `hour`";
		para.add("1hour", hour );
		para.add("2hour", hour );
		para.add("1storage_id", storage_id );
		para.add("1start", start );
		para.add("1end", end );

		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	/*
	public DBRow[] sumBaseByDay(String start, String end, long storage_id, int hour) throws SQLException {
		DBRow para = new DBRow();
		String sql = "select SUM(`inlimit`) `sumin`, SUM(`outlimit`) `sumout`, work_date, `hour` from ( "+
		"select inlimit, outlimit, DATE(DATE_ADD(DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00'),INTERVAL ? HOUR)) as `work_date`, EXTRACT(HOUR FROM DATE_ADD(DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00'),INTERVAL ? HOUR)) as `hour` from ( "+
		 "SELECT SUM(`limit`) `inlimit`, 0 `outlimit`, work_date, `hour`  FROM "+ ConfigBean.getStringValue("wms_appointment_work") +" where storage_id = ? and DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00') >= DATE_FORMAT(?,'%Y-%m-%d %H:00:00') and DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00') < DATE_FORMAT(?,'%Y-%m-%d %H:00:00') and appointment_type='inbound' group by work_date, `hour`, appointment_type"+
		  " UNION "+
		 "SELECT 0 `inlimit` , SUM(`limit`) `outlimit`, work_date, `hour` FROM "+ ConfigBean.getStringValue("wms_appointment_work") +" where storage_id = ? and DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00') >= DATE_FORMAT(?,'%Y-%m-%d %H:00:00') and DATE_FORMAT(concat(`work_date`,' ',`hour`,':00:00'),'%Y-%m-%d %H:00:00') < DATE_FORMAT(?,'%Y-%m-%d %H:00:00') and appointment_type='outbound' group by work_date, `hour`, appointment_type"+
		") as ts "+
		") t group by work_date, `hour`";
		para.add("1hour", hour );
		para.add("2hour", hour );
		para.add("1storage_id", storage_id );
		para.add("1start", start );
		para.add("1end", end );
		para.add("2storage_id", storage_id );
		para.add("2start", start );
		para.add("2end", end );
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}*/
	
	/**
	 * 对客户特批
	 * 
	 * @param start
	 * @param end
	 * @param hour
	 * @param storage_id
	 * @param customer_id
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getCustomerExcept(String start, String end, Integer hour, long storage_id, String customer_id) throws SQLException {
		DBRow para = new DBRow();
		String sql = "SELECT `work`, `limit`, work_date `date`, appointment_type,`hour`, customer_id FROM "+ ConfigBean.getStringValue("wms_appointment_excep") +" WHERE storage_id=? and work_date >= ? and work_date <= ?";
		para.add("storage_id", storage_id );
		para.add("start", start );
		para.add("end", end );
		if (hour!=null) {
			sql+= " and hour = ? ";
			para.add("hour", hour );
		}
		if (hour!=null) {
			sql+= " and customer_id = ? ";
			para.add("customer_id", customer_id );
		}
		sql+= " order by work_date, `hour`, customer_id";
		
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	/**
	 * 获取基础参数
	 * 
	 * @param storage_id
	 * @return
	 * @throws SQLException
	 * @author: wangcr
	 */
	public DBRow[] getBaseDefault(long storage_id) throws SQLException {
		DBRow para = new DBRow();
		String sql = "select * from "+ ConfigBean.getStringValue("wms_appointment_base") +" where storage_id = ? order by appointment_type";
		para.add("storage_id", storage_id );
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	/**
	 * 创建或修改预约基础数据
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public DBRow addOrUpdateBase(DBRow data) throws Exception {
		DBRow para = new DBRow();
		para.add("storage_id", data.get("storage_id",0L));
		para.add("appointment_type", data.get("appointment_type",""));
		
		DBRow row = dbUtilAutoTran.selectPreSingle("select * from "+ ConfigBean.getStringValue("wms_appointment_base") + " where storage_id =? and appointment_type=?", para);
		if (row==null || row.get("bid", 0L)==0) {
			long id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("wms_appointment_base"), data);
			data.add("bid", id);
			return data;
		}
		data.add("bid", row.get("bid", 0L));
		dbUtilAutoTran.update(" where bid="+ data.get("bid", 0L) , ConfigBean.getStringValue("wms_appointment_base"), data);
		return data;
	}
	/**
	 * 创建或修改预约
	 * 
	 * @param data
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public DBRow addOrUpdateWorkTotal(DBRow data) throws Exception {
		
		DBRow para = new DBRow();
		para.add("storage_id", data.get("storage_id",0L));
		//para.add("appointment_type", data.get("appointment_type",""));
		para.add("work_date", data.get("work_date",""));
		para.add("hour", data.get("`hour`", 0));
		
		
		DBRow row = dbUtilAutoTran.selectPreSingle("select * from "+ ConfigBean.getStringValue("wms_appointment_work_total") + " where storage_id =? and work_date = ? and `hour`=?", para);
		if (row==null || row.get("bid", 0L)==0) {
			long id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("wms_appointment_work_total"), data);
			data.add("bid", id);
			return data;
		}
		//in
		int iok = 0;
		if (data.get("`limit_in`", -1)>=0) {
			DBRow datan = new DBRow();
			datan.add("`limit_in`", data.get("`limit_in`", 0));
			
			iok = dbUtilAutoTran.update(" where bid="+ row.get("bid", 0L) +" and (work) <= "+datan.get("`limit_in`",0), ConfigBean.getStringValue("wms_appointment_work_total"), datan);
			if (iok==0) {
				//限制不能小于已预约数
				String sql = "update "+ConfigBean.getStringValue("wms_appointment_work_total")+" set `limit_in` = (work) where bid=?";
				para = new DBRow();
				para.add("bid", row.get("bid", 0L));
				iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_work_total"));
				if (iok==0) {
					throw new Exception("Error set inbound!");
				}
			}
		}
		//out
		if (data.get("`limit_out`", -1)>=0) {
			iok = 0;
			DBRow datan = new DBRow();
			datan.add("`limit_out`", data.get("`limit_out`", 0));
			iok = dbUtilAutoTran.update(" where bid="+ row.get("bid", 0L) +" and (work_out) <= "+datan.get("`limit_out`",0), ConfigBean.getStringValue("wms_appointment_work_total"), datan);
			if (iok==0) {
				//限制不能小于已预约数
				String sql = "update "+ConfigBean.getStringValue("wms_appointment_work_total")+" set `limit_out` = (work_out) where bid=?";
				para = new DBRow();
				para.add("bid", row.get("bid", 0L));
				iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_work_total"));
				if (iok==0) {
					throw new Exception("Error set outbound!");
				}
			}
		}
		if (iok==0) {
			return null;
		}
		return data;
	}
	/**
	 * 创建或修改预约特批
	 * 
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public DBRow addOrUpdateCustomWork(DBRow data) throws Exception {
		DBRow para = new DBRow();
		para.add("storage_id", data.get("storage_id",0L));
		para.add("appointment_type", data.get("appointment_type",""));
		para.add("work_date", data.get("work_date",""));
		para.add("hour", data.get("`hour`", 0));
		para.add("customer_id", data.get("customer_id", ""));
		DBRow row = dbUtilAutoTran.selectPreSingle("select * from "+ ConfigBean.getStringValue("wms_appointment_excep") + " where storage_id =? and appointment_type=? and work_date=? and `hour`=? and customer_id=?", para);
		if (row==null || row.get("bid", 0L)==0) {
			long id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("wms_appointment_excep"), data);
			data.add("bid", id);
			return data;
		}
		DBRow datan = new DBRow();
		datan.add("`limit`", data.get("`limit`", 0));
		int iok = dbUtilAutoTran.update(" where bid="+ row.get("bid", 0L) +" and work <= "+data.get("`limit`"), ConfigBean.getStringValue("wms_appointment_excep"), datan);
		if (iok==0) {
			//限制不能小于已预约数
			String sql = "update "+ConfigBean.getStringValue("wms_appointment_excep")+" set `limit` = work where bid=?";
			para = new DBRow();
			para.add("bid", row.get("bid", 0L));
			iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_excep"));
			if (iok==0) return null;
		}
		return data;
	}
	
	/**
	 * 出库预约累加
	 * 
	 * @param date
	 * @param hour
	 * @param storage_id
	 * @param customer_id
	 * @param cust_flag 是否可用客户特批  
	 * @return
	 * @throws Exception
	 */
	public boolean sumOneWorkOut(String date, int hour, long storage_id, String customer_id, boolean cust_flag) throws Exception {
		boolean r = this.addDefaultWhenSum(date, hour, getWeek(date, hour, storage_id), storage_id, "outbound");
		String sql = "update "+ConfigBean.getStringValue("wms_appointment_work_total")+" set work_out=work_out+1 where storage_id=? and work_date=? and `hour`=? and (work_out+1)<= `limit_out`";
		DBRow para = new DBRow();
		para.add("storage_id", storage_id);
		para.add("work_date", date);
		para.add("hour", hour);
		int iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_work_total"));
		if (iok>0) return true;
		return false;
	}	
	/**
	 * 出库预约累加, 无限制
	 * 
	 * @param date
	 * @param hour
	 * @param storage_id
	 * @param customer_id
	 * @param cust_flag 是否可用客户特批  
	 * @return
	 * @throws Exception
	 */
	public boolean sumOneWorkOutNoLimit(String date, int hour, long storage_id, String customer_id, boolean cust_flag) throws Exception {
		boolean r = this.addDefaultWhenSum(date, hour, getWeek(date, hour, storage_id), storage_id, "outbound");
		String sql = "update "+ConfigBean.getStringValue("wms_appointment_work_total")+" set work_out=work_out+1 where storage_id=? and work_date=? and `hour`=?";
		DBRow para = new DBRow();
		para.add("storage_id", storage_id);
		para.add("work_date", date);
		para.add("hour", hour);
		int iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_work_total"));
		if (iok>0) return true;
		return false;
	}
	/**
	 * 出库预约取消
	 * 
	 * @param date
	 * @param hour
	 * @param storage_id
	 * @param customer_id
	 * @param cust_flag 是否可用客户特批  
	 * @return
	 * @throws Exception
	 */
	public boolean subOneWorkOut(String date, int hour, long storage_id, String customer_id, boolean cust_flag) throws Exception {
		String sql = "update "+ConfigBean.getStringValue("wms_appointment_work_total")+" set work_out=work_out-1 where storage_id=? and work_date=? and `hour`=? and work_out>0";
		DBRow para = new DBRow();
		para.add("storage_id", storage_id);
		para.add("work_date", date);
		para.add("hour", hour);
		int iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_work_total"));
		if (iok>0) return true;
		return false;
	}
	
	/**
	 * 批量出库预约取消
	 * 
	 * @param date
	 * @param hour
	 * @param storage_id
	 * @param customer_id
	 * @param cust_flag
	 * @param cnt
	 * @return
	 * @throws Exception
	 */
	public boolean subOneWorkOut(String date, int hour, long storage_id, String customer_id, boolean cust_flag, int cnt) throws Exception {
		String sql = "update "+ConfigBean.getStringValue("wms_appointment_work_total")+" set work_out=work_out-? where storage_id=? and work_date=? and `hour`=? and work_out>=?";
		DBRow para = new DBRow();
		para.add("work_out", cnt);
		para.add("storage_id", storage_id);
		para.add("work_date", date);
		para.add("hour", hour);
		para.add("work_out_2", cnt);
		int iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_work_total"));
		if (iok>0) return true;
		return false;
	}
	
	/**
	 * 出库预约取消
	 * @param date
	 * @param hour
	 * @param storage_id
	 * @param customer_id
	 * @param cust_flag
	 * @return
	 * @throws Exception
	 */
	public boolean subOneWorkOutNoLimit(String date, int hour, long storage_id, String customer_id, boolean cust_flag) throws Exception {
		String sql = "update "+ConfigBean.getStringValue("wms_appointment_work_total")+" set work_out=work_out-1 where storage_id=? and work_date=? and `hour`=? and work_out>0";
		DBRow para = new DBRow();
		para.add("storage_id", storage_id);
		para.add("work_date", date);
		para.add("hour", hour);
		int iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_work_total"));
		if (iok==0) {
			DBRow row =  dbUtilAutoTran.selectPreSingle("select count(*) as cnt, IFNULL(`work_out`,0) as out_cnt from "+ConfigBean.getStringValue("wms_appointment_work_total")+" where storage_id =? and work_date=? and hour=?", para);
			if (row.get("cnt", -1)==0) {
				return true;
			} else if(row.get("cnt", -1)==1 &&  row.get("out_cnt", -1)==0) {
				return true;
			}
		}
		if (iok>0) return true;
		return false;
	}
	
	/**
	 * 批量出库预约取消
	 * 
	 * @param date
	 * @param hour
	 * @param storage_id
	 * @param customer_id
	 * @param cust_flag
	 * @param cnt
	 * @return
	 * @throws Exception
	 */
	public boolean subOneWorkOutNoLimit(String date, int hour, long storage_id, String customer_id, boolean cust_flag, int cnt) throws Exception {
		String sql = "update "+ConfigBean.getStringValue("wms_appointment_work_total")+" set work_out=work_out-? where storage_id=? and work_date=? and `hour`=? and work_out>=?";
		DBRow para = new DBRow();
		para.add("work_out", cnt);
		para.add("storage_id", storage_id);
		para.add("work_date", date);
		para.add("hour", hour);
		para.add("work_out_2", cnt);
		
		int iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_work_total"));
		if (iok>0) return true;
		return false;
	}
	/**
	 * 入库预约累加
	 * 
	 * @param date
	 * @param hour
	 * @param storage_id
	 * @param customer_id
	 * @param cust_flag 是否可用客户特批  
	 * @return
	 * @throws Exception
	 */
	public boolean sumOneWorkIn(String date, int hour, long storage_id, String customer_id, boolean cust_flag) throws Exception {
		//初始化
		boolean r = this.addDefaultWhenSum(date, hour, getWeek(date, hour, storage_id), storage_id, "inbound");
		String sql = "update "+ConfigBean.getStringValue("wms_appointment_work_total")+" set work=work+1 where storage_id=? and work_date=? and `hour`=? and (work+1)<= `limit_in`";
		DBRow para = new DBRow();
		para.add("storage_id", storage_id);
		para.add("work_date", date);
		para.add("hour", hour);
		int iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_work_total"));
		if (iok>0) return true;
		return false;
	}
	/**
	 * 入库预约累加, 无限制
	 * 
	 * @param date
	 * @param hour
	 * @param storage_id
	 * @param customer_id
	 * @param cust_flag 是否可用客户特批  
	 * @return
	 * @throws Exception
	 */
	public boolean sumOneWorkInNoLimit(String date, int hour, long storage_id, String customer_id, boolean cust_flag) throws Exception {
		//初始化
		boolean r = this.addDefaultWhenSum(date, hour, getWeek(date, hour, storage_id), storage_id, "inbound");
		
		String sql = "update "+ConfigBean.getStringValue("wms_appointment_work_total")+" set work=work+1 where storage_id=? and work_date=? and `hour`=?";
		DBRow para = new DBRow();
		para.add("storage_id", storage_id);
		para.add("work_date", date);
		para.add("hour", hour);
		int iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_work_total"));
		if (iok>0) return true;
		return false;
	}
	/**
	 * 入库预约取消
	 * 
	 * @param date
	 * @param hour
	 * @param storage_id
	 * @param customer_id
	 * @param cust_flag 是否可用客户特批
	 * @return
	 * @throws Exception
	 */
	public boolean subOneWorkIn(String date, int hour, long storage_id, String customer_id, boolean cust_flag) throws Exception {
		String sql = "update "+ConfigBean.getStringValue("wms_appointment_work_total")+" set work=work-1 where storage_id=? and work_date=? and `hour`=? and work>0";
		DBRow para = new DBRow();
		para.add("storage_id", storage_id);
		para.add("work_date", date);
		para.add("hour", hour);
		int iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_work_total"));
		if (iok>0) return true;
		return false;
	}
	/**
	 * 批量入库预约取消
	 * 
	 * @param date
	 * @param hour
	 * @param storage_id
	 * @param customer_id
	 * @param cust_flag
	 * @param cnt
	 * @return
	 * @throws Exception
	 */
	public boolean subOneWorkIn(String date, int hour, long storage_id, String customer_id, boolean cust_flag, int cnt) throws Exception {
		String sql = "update "+ConfigBean.getStringValue("wms_appointment_work_total")+" set work=work-? where storage_id=? and work_date=? and `hour`=? and work>=?";
		DBRow para = new DBRow();
		para.add("work_out", cnt);
		para.add("storage_id", storage_id);
		para.add("work_date", date);
		para.add("hour", hour);
		para.add("work_out_2", cnt);
		int iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_work_total"));
		if (iok>0) return true;
		return false;
	}
	
	/**
	 * 入库预约取消,无限制
	 * 
	 * @param date
	 * @param hour
	 * @param storage_id
	 * @param customer_id
	 * @param cust_flag
	 * @return
	 * @throws Exception
	 */
	public boolean subOneWorkInNoLimit(String date, int hour, long storage_id, String customer_id, boolean cust_flag) throws Exception {
		String sql = "";
		
		sql = "update "+ConfigBean.getStringValue("wms_appointment_work_total")+" set work=work-1 where storage_id=? and work_date=? and `hour`=? and work>0";
		DBRow para = new DBRow();
		para.add("storage_id", storage_id);
		para.add("work_date", date);
		para.add("hour", hour);
		int iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_work_total"));
		if (iok==0) {
			DBRow row =  dbUtilAutoTran.selectPreSingle("select count(*) as cnt, IFNULL(`work`,0) as in_cnt from "+ConfigBean.getStringValue("wms_appointment_work_total")+" where storage_id =? and work_date=? and hour=?", para);
			if (row.get("cnt", -1)==0) {
				return true;
			} else if(row.get("cnt", -1)==1 &&  row.get("in_cnt", -1)==0) {
				return true;
			}
		}
		if (iok>0) return true;
		return false;
	}
	
	public boolean subOneWorkInNoLimit(String date, int hour, long storage_id, String customer_id, boolean cust_flag, int cnt) throws Exception {
		String sql = "";
		sql = "update "+ConfigBean.getStringValue("wms_appointment_work_total")+" set work=work-? where storage_id=? and work_date=? and `hour`=? and work>=?";
		DBRow para = new DBRow();
		para.add("work_out", cnt);
		para.add("storage_id", storage_id);
		para.add("work_date", date);
		para.add("hour", hour);
		para.add("work_out_2", cnt);
		int iok = dbUtilAutoTran.updatePre(sql, para, ConfigBean.getStringValue("wms_appointment_work_total"));
		if (iok>0) return true;
		return false;
	}
	
	/**
	 * 获取预约限制默认值
	 * 
	 * @param storage_id
	 * @param appointment_type
	 * @param roleId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDefaultValue(long storage_id, Integer date_type) throws Exception {
		DBRow para = new DBRow();
		para.add("storage_id", storage_id);
		para.add("date_type", weeks.get(String.valueOf(date_type)));
		return dbUtilAutoTran.selectPreMutliple("select * from wms_appointment_role_line as t where storage_id = ? and date_type = ? and role_id =1 order by `hour`", para);
	}
	
	/**
	 * 新增或修改预约限制默认值
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public DBRow addOrUpdateDefaultValue(DBRow data) throws Exception {
		DBRow para = new DBRow();
		para.add("storage_id", data.get("storage_id",0L));
		para.add("date_type", data.get("date_type",""));
		para.add("hour", data.get("`hour`", 0));
		para.add("role_id", data.get("role_id", 0));
		
		
		DBRow row = dbUtilAutoTran.selectPreSingle("select * from "+ ConfigBean.getStringValue("wms_appointment_role_line") + " where storage_id =? and date_type=? and `hour`=? and role_id= ?", para);
		if (row==null || row.get("bid", 0L)==0) {
			long id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("wms_appointment_role_line"), data);
			data.add("bid", id);
			return data;
		}
		DBRow datan = new DBRow();
		datan.add("limit_in", data.get("limit_in", 0));
		datan.add("limit_out", data.get("limit_out", 0));
		//datan.add("bid", row.get("bid", 0L));
		int iok = dbUtilAutoTran.update(" where bid="+ row.get("bid", 0L), ConfigBean.getStringValue("wms_appointment_role_line"), datan);
		if (iok==0) return null;
		data.add("bid", row.get("bid", 0L));
		return data;
	}
	/**
	 * 新增或修改预约时段设置
	 * 
	 * @param start YYYY-MM-DD
	 * @param end YYYY-MM-DD
	 * @param type default, other
	 * @return
	 * @throws Exception
	 */
	public DBRow addOrUpdateRole(String start, String end, String type)  throws Exception {
		DBRow para = new DBRow();
		para.add("role_type", type);
		para.add("start_date", DateUtil.DatetoStr(DateUtil.StringToDate(start)));
		para.add("end_date", DateUtil.DatetoStr(DateUtil.StringToDate(end)));
		DBRow row = dbUtilAutoTran.selectPreSingle("select * from "+ ConfigBean.getStringValue("wms_appointment_role") + " where role_type =? and start_date=? and `end_date`=?", para);
		if (row==null || row.get("bid", 0L)==0) {
			long id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("wms_appointment_role"), para);
			para.add("bid", id);
			return para;
		}
		return row;
	}
	
	/**
	 * 没有设置预约限制的天初始化。
	 * 
	 * @param date 偏移后时间 UTC
	 * @param hour 偏移后时间 UTC
	 * @param storage_id
	 * @param appointment_type
	 * @return
	 * @throws Exception
	 */
	public boolean addDefaultWhenSum(String date, int hour, int week, long storage_id, String appointment_type) throws Exception {
		DateFormat format3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DBRow para = new DBRow();
		para.add("storage_id", storage_id);
		//para.add("appointment_type", "inbound".equalsIgnoreCase(appointment_type)?"inbound":"outbound");
		para.add("work_date", date);
		para.add("hour", hour);
		String sql = "select * from "+ConfigBean.getStringValue("wms_appointment_work_total")+" where storage_id =? and work_date=? and hour=?";
		DBRow row =  dbUtilAutoTran.selectPreSingle(sql, para);
		
		if (row==null || row.get("bid", 0L)==0) {
			//para.add("work", 1);
			Date utc = null;
			if (hour>9) {
				utc = format3.parse(date+" "+ hour+":00:00");
				//local = format2.parse(date+" "+ hour+":00:00");
			} else {
				utc = format3.parse(date+" 0"+ hour+":00:00");
				//local = format2.parse(date+" "+ hour+":00:00");
			}
			//local.getDay()
			int limit_in = getLimitDefault(utc,date,hour,week,storage_id,"inbound");
			int limit_out = getLimitDefault(utc,date,hour,week,storage_id,"outbound");
			long rt = 0;
			
			para.add("`limit_in`", limit_in);
			para.add("`limit_out`", limit_out);
			rt = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("wms_appointment_work_total"), para);
			if (rt>0) return true;
		}
		return false;
	}
	
	public void viewDefaultWhenSum(String date, int hour, int week, long storage_id, String appointment_type, List<DBRow> rows) throws Exception {
		DateFormat format3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DBRow para = new DBRow();
		para.add("storage_id", storage_id);
		para.add("work_date", date);
		para.add("hour", hour);
		String sql = "select * from "+ConfigBean.getStringValue("wms_appointment_work_total")+" where storage_id =? and work_date=? and hour=?";
		DBRow row =  dbUtilAutoTran.selectPreSingle(sql, para);
		if (row==null || row.get("bid", 0L)==0) {
			//para.add("work", 1);
			Date utc = null;
			
			if (hour>9) {
				utc = format3.parse(date+" "+ hour+":00:00");
			} else {
				utc = format3.parse(date+" 0"+ hour+":00:00");
			}
			//local.getDay()
			int limit_in = getLimitDefault(utc,date,hour,week,storage_id,"inbound");
			int limit_out = getLimitDefault(utc,date,hour,week,storage_id,"outbound");
			
			String localtime =  DateUtil.showLocationTime(utc, storage_id);
			para.add("work_date", localtime.substring(0, 10));
			para.add("hour", Integer.parseInt(localtime.substring(11, 13)));
			long rt = 0;
			if ("".equalsIgnoreCase(appointment_type)) {
				para.add("sumin", limit_in);
				para.add("sumout", limit_out);
				para.add("cntin", 0);
				para.add("cntout", 0);
				rows.add(para);
			} else if ("inbound".equalsIgnoreCase(appointment_type)) {
				para.add("cntin", 0);
				para.add("sumin", limit_in);
				rows.add(para);
			} else if ("outbound".equalsIgnoreCase(appointment_type)) {
				para.add("sumout", limit_out);
				para.add("cntout", 0);
				rows.add(para);
			}
		} else {
			Date utc = null;
			if (hour>9) {
				utc = format3.parse(date+" "+ hour+":00:00");
			} else {
				utc = format3.parse(date+" 0"+ hour+":00:00");
			}
			String localtime =  DateUtil.showLocationTime(utc, storage_id);
			para.add("work_date", localtime.substring(0, 10));
			para.add("hour", Integer.parseInt(localtime.substring(11, 13)));
			long rt = 0;
			if ("".equalsIgnoreCase(appointment_type)) {
				para.add("sumin", row.get("limit_in", 0));
				para.add("sumout", row.get("limit_out", 0));
				para.add("cntin", row.get("work", 0));
				para.add("cntout", row.get("work_out", 0));
				rows.add(para);
			} else if ("inbound".equalsIgnoreCase(appointment_type)) {
				para.add("cntin", row.get("work", 0));
				para.add("sumin", row.get("limit_in", 0));
				rows.add(para);
			} else if ("outbound".equalsIgnoreCase(appointment_type)) {
				para.add("sumout", row.get("limit_out", 0));
				para.add("cntout", row.get("work_out", 0));
				rows.add(para);
			}
		}
	}
	
	/**
	 * 获取限制值
	 * 
	 * @param UTCdate
	 * @param UTCDay
	 * @param UTCHour
	 * @param storage_id
	 * @param appointment_type
	 * @return
	 * @throws Exception
	 */
	private int getLimitDefault2(Date UTCDate, String UTCDay, int UTCHour, long storage_id, String appointment_type) throws Exception {
		String local = DateUtil.showLocationTime(UTCDate, storage_id);
		String localDate = local.substring(0,10);
		int localHour =  Integer.parseInt(local.substring(11, 13));
		DBRow para = new DBRow();
		//String type =  "inbound".equalsIgnoreCase(appointment_type)?"inbound":"outbound";
		
		para.add("storage_id1", storage_id);
		//para.add("appointment_type1", type);
		para.add("work_date", UTCDay);
		para.add("hour1", UTCHour);
		
		para.add("start", localDate);
		para.add("end", localDate);
		para.add("storage_id2", storage_id);
		para.add("appointment_type2", "inbound");
		para.add("hour2", localHour);
		
		para.add("storage_id3", storage_id);
		para.add("appointment_type3", "inbound");
		para.add("hour3", localHour);
		//1取当前设置，
		//2取时段设置
		//3取默认设置
		String sql = "(select t.`limit` from "+ConfigBean.getStringValue("wms_appointment_work_total")+" as t where storage_id = ? and t.work_date= ? and `hour`=? ) "+
					 " union all (select t.`limit` from wms_appointment_role_line as t left join wms_appointment_role r on t.role_id = r.bid where r.start_date<=? and r.end_date>=? and t.storage_id = ? and t.appointment_type = ? and t.`hour` = ? and t. role_id>1 order by t.bid desc)"+
					 " union all (select t.`limit` from wms_appointment_role_line as t where storage_id = ? and appointment_type = ? and t.`hour` = ? and t.role_id=1)";
		DBRow[] rows =  dbUtilAutoTran.selectPreMutliple(sql, para);
		if (rows!=null && rows.length>0) {
			return rows[0].get("limit", 0);
		}
		return 0;
	}
	
	/**
	 * 获取限制，周
	 * 
	 * @param UTCDate
	 * @param UTCDay
	 * @param UTCHour
	 * @param week 0~6
	 * @param storage_id
	 * @param appointment_type
	 * @return
	 * @throws Exception
	 */
	private int getLimitDefault(Date UTCDate, String UTCDay, int UTCHour, int week, long storage_id, String appointment_type) throws Exception {
		String local = DateUtil.showLocationTime(UTCDate, storage_id);
		String localDate = local.substring(0,10);
		int localHour =  Integer.parseInt(local.substring(11, 13));
		DBRow para = new DBRow();
		para.add("storage_id1", storage_id);
		para.add("work_date", UTCDay);
		para.add("hour1", UTCHour);
		
		para.add("start", localDate);
		para.add("end", localDate);
		para.add("storage_id2", storage_id);
		para.add("date_type2", weeks.get(""+week));
		para.add("hour2", localHour);
		
		para.add("storage_id3", storage_id);
		para.add("date_type3", weeks.get(""+week));
		para.add("hour3", localHour);
		String sql ="";
		if ("inbound".equals(appointment_type)) {
			sql = "(select t.`limit_in` from "+ConfigBean.getStringValue("wms_appointment_work_total")+" as t where storage_id = ? and t.work_date= ? and `hour`=? ) "+
					 " union all (select t.`limit_in` as `limit` from "+ConfigBean.getStringValue("wms_appointment_role_line")+" as t left join "+ConfigBean.getStringValue("wms_appointment_role")+" r on t.role_id = r.bid where r.start_date<=? and r.end_date>=? and t.storage_id = ? and t.date_type =? and t.`hour` = ? and t. role_id>1 order by t.bid desc)"+
					 " union all (select t.`limit_in` as `limit` from "+ConfigBean.getStringValue("wms_appointment_role_line")+" as t where storage_id = ? and t.date_type =? and t.`hour` = ? and t.role_id=1)";
		} else if ("outbound".equals(appointment_type)) {
			sql = "(select t.`limit_out` from "+ConfigBean.getStringValue("wms_appointment_work_total")+" as t where storage_id = ? and t.work_date= ? and `hour`=? ) "+
					 " union all (select t.`limit_out` as `limit` from "+ConfigBean.getStringValue("wms_appointment_role_line")+" as t left join "+ConfigBean.getStringValue("wms_appointment_role")+" r on t.role_id = r.bid where r.start_date<=? and r.end_date>=? and t.storage_id = ? and t.date_type =? and t.`hour` = ? and t. role_id>1 order by t.bid desc)"+
					 " union all (select t.`limit_out` as `limit` from "+ConfigBean.getStringValue("wms_appointment_role_line")+" as t where storage_id = ? and t.date_type =? and t.`hour` = ? and t.role_id=1)";
		} else {
			sql = "(select (t.`limit_out`+ t.`limit_in`) as `limit` from "+ConfigBean.getStringValue("wms_appointment_work_total")+" as t where storage_id = ? and t.work_date= ? and `hour`=? ) "+
					 " union all (select (t.`limit_out`+ t.`limit_in`) as `limit` from "+ConfigBean.getStringValue("wms_appointment_role_line")+" as t left join "+ConfigBean.getStringValue("wms_appointment_role")+" r on t.role_id = r.bid where r.start_date<=? and r.end_date>=? and t.storage_id = ? and t.date_type =? and t.`hour` = ? and t. role_id>1 order by t.bid desc)"+
					 " union all (select (t.`limit_out`+ t.`limit_in`) as `limit` from "+ConfigBean.getStringValue("wms_appointment_role_line")+" as t where storage_id = ? and t.date_type=? and t.`hour` = ? and t.role_id=1)";
		}
		
		DBRow[] rows =  dbUtilAutoTran.selectPreMutliple(sql, para);
		if (rows!=null && rows.length>0) {
			if ("inbound".equals(appointment_type)) {
				return rows[0].get("limit_in", 0);
			} else if ("outbound".equals(appointment_type)) {
				return rows[0].get("limit_out", 0);
			} else {
				return rows[0].get("limit", 0);
			}
		}
		return 0;
	}
	
	/**
	 * 初始化天
	 * 
	 * @param YYYYMMDD
	 * @param storage_id
	 * @return
	 * @throws Exception
	 */
	public boolean initDay(String YYYYMMDD, long storage_id) throws Exception {
		//TimeZone tz = DateUtil.getTimeZone(storage_id);
		for (int i=0;i<24;i++) {
			String HH ="";
			if (i<10) {
				HH = "0"+i;
			} else {
				HH = "" +i;
			}
			String utc = DateUtil.showUTCTime((YYYYMMDD+ " "+HH+ ":00:00"), storage_id);
			String date = utc.substring(0,10);
			int hour =  Integer.parseInt(utc.substring(11, 13));
			boolean r = this.addDefaultWhenSum(date, hour, getWeek(YYYYMMDD,hour,storage_id), storage_id, "");
		}
		return false;
	}
	
	public void vinitDay(String YYYYMMDD, long storage_id, List<DBRow> rows) throws Exception {
		for (int i=0;i<24;i++) {
			String HH ="";
			if (i<10) {
				HH = "0"+i;
			} else {
				HH = "" +i;
			}
			String utc = DateUtil.showUTCTime((YYYYMMDD+ " "+HH+ ":00:00"), storage_id);
			String date = utc.substring(0,10);
			int hour =  Integer.parseInt(utc.substring(11, 13));
			this.viewDefaultWhenSum(date, hour, getWeek(YYYYMMDD,hour,storage_id), storage_id, "", rows);
		}
	}
	
	/**
	 * 初始化周
	 * @param start
	 * @param end
	 * @param storage_id
	 * @return
	 * @throws Exception
	 */
	public boolean initWeek (String start, String end , long storage_id) throws Exception {
		java.text.SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(DateUtil.getTimeZone(storage_id));
		Date dt = sdf.parse(start);
		for (int i=0;i<7;i++) {
			String YYYYMMDD = sdf.format(new Date(dt.getTime()+ (86400000L*i)));
			initDay(YYYYMMDD, storage_id);
		}
		return false;
	}
	public DBRow[] initWeek2 (String start, String end, long storage_id) throws Exception {
		List<DBRow> rt = new ArrayList<DBRow>();
		java.text.SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(DateUtil.getTimeZone(storage_id));
		Date dt = sdf.parse(start);
		for (int i=0;i<7;i++) {
			String YYYYMMDD = sdf.format(new Date(dt.getTime()+ (86400000L*i)));
			vinitDay(YYYYMMDD, storage_id,rt);
		}
		return rt.toArray(new DBRow[0]);
	}
	/**
	 * 
	 * @param date YYYYMMDD
	 * @param hour 0~ 23
	 * @param storage_id
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private Integer getWeek(String date, Integer hour, long storage_id) {
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		TimeZone tz = DateUtil.getTimeZone(storage_id);
		format2.setTimeZone(tz);
		/*
		String HH ="";
		if (hour<10) {
			HH = "0"+hour;
		} else {
			HH = "" +hour;
		}
		*/
		try {
			return (format2.parse(""+date+" 00:00:00").getDay());
		} catch (ParseException e) {}
		return null;
	}
	
	/**
	 * 生成预约编号
	 * 
	 * @param storageId
	 * @param date YYYY-MM-DD
	 * @param hour 0-23
	 * @param type outbound inbound
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public String makeTicket(String storageId, String date, Integer hour, String type, String user) throws Exception {
		clearOutTimeTicket(storageId, date, hour);
		if (storageId==null || date==null || hour==null || type==null) return null;
		String uid = UUID.randomUUID().toString();
		DBRow data = new DBRow();
		data.add("storage_id", storageId);
		data.add("work_date", format4.parse(date));
		data.add("hour", hour);
		data.add("appointment_type", type);
		data.add("ticket", uid);
		data.add("date_created", new Date());
		data.add("user_created", user);
		this.dbUtilAutoTran.insertReturnId("wms_appointment_ticket", data);
		return uid;
	}
	
	/**
	 * 使用预约条
	 * 
	 * @param storageId
	 * @param date
	 * @param hour
	 * @param type
	 * @param ticket
	 * @return
	 * @throws Exception
	 */
	public boolean useTicket(String storageId, String date, Integer hour, String type, String ticket) throws Exception {
		//清除过期ticket
		//clearOutTimeTicket(null);
		if (storageId==null || date==null || hour==null || type==null || ticket==null) return false;
		DBRow para = new DBRow();
		para.add("storage_id", storageId);
		para.add("work_date", date);
		para.add("hour", hour);
		para.add("appointment_type", type);
		para.add("ticket", ticket);
		int t = dbUtilAutoTran.deletePre(" where storage_id =? and DATE_FORMAT(work_date,'%Y-%m-%d')= ? and hour = ? and appointment_type =? and ticket =?", para, "wms_appointment_ticket");
		if (t==1) {
			return true;
		}
		return false;
	}
	
	/**
	 * 删除预约条
	 * 
	 * @param storageId
	 * @param date
	 * @param hour
	 * @param type
	 * @param ticket
	 * @return
	 * @throws Exception
	 */
	public boolean cancelTicket(String storageId, String date, Integer hour, String type, String ticket) throws Exception {
		if (storageId==null || date==null || hour==null || type==null || ticket==null) return false;
		DBRow para = new DBRow();
		para.add("storage_id", storageId);
		para.add("work_date", date);
		para.add("hour", hour);
		para.add("appointment_type", type);
		para.add("ticket", ticket);
		int t = dbUtilAutoTran.deletePre(" where storage_id =? and DATE_FORMAT(work_date,'%Y-%m-%d')= ? and hour = ? and appointment_type =? and ticket =?", para, "wms_appointment_ticket");
		if (t==1) {
			return true;
		}
		return false;
	}
	
	/**
	 * 清除超时预约数和预约条
	 * 
	 * @param date
	 * @throws Exception
	 */
	public void clearOutTimeTicket(String date) {
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			if (date==null || "".equals(date)) {
				DBRow para = new DBRow();
				para.add("now", format2.format(new Date()));
				DBRow[] rows = dbUtilAutoTran.selectPreMutliple("select * from wms_appointment_ticket where (UNIX_TIMESTAMP(?)-UNIX_TIMESTAMP(date_created))>1800", para);
				for(DBRow row: rows) {
					if ("outbound".equals(row.getString("appointment_type"))) {
						boolean b = subOneWorkOutNoLimit(row.getString("work_date"), row.get("hour", 0), row.get("storage_id", 0L), "", false);
						if (b) dbUtilAutoTran.delete(" where id ="+ row.get("id", 0L) , "wms_appointment_ticket");
					} else if ("inbound".equals(row.getString("appointment_type"))) {
						boolean b = subOneWorkInNoLimit(row.getString("work_date"), row.get("hour", 0), row.get("storage_id", 0L), "", false);
						if (b) dbUtilAutoTran.delete(" where id ="+ row.get("id", 0L) , "wms_appointment_ticket");
					}
				}
			} else {
				DBRow para = new DBRow();
				para.add("work_date", date);
				para.add("now", format2.format(new Date()));
				DBRow[] rows = dbUtilAutoTran.selectPreMutliple("select * from wms_appointment_ticket where work_date=? and (UNIX_TIMESTAMP(?)-UNIX_TIMESTAMP(date_created))>1800", para);
				for(DBRow row: rows) {
					if ("outbound".equals(row.getString("appointment_type"))) {
						boolean b = subOneWorkOutNoLimit(row.getString("work_date"), row.get("hour", 0), row.get("storage_id", 0L), "", false);
						if (b) dbUtilAutoTran.delete(" where id ="+ row.get("id", 0L) , "wms_appointment_ticket");
					} else if ("inbound".equals(row.getString("appointment_type"))) {
						boolean b = subOneWorkInNoLimit(row.getString("work_date"), row.get("hour", 0), row.get("storage_id", 0L), "", false);
						if (b) dbUtilAutoTran.delete(" where id ="+ row.get("id", 0L) , "wms_appointment_ticket");
					}
				}
			}
		} catch (Exception e) {e.printStackTrace();}
	}
	/**
	 * 清除超时预约数和预约条, 指定日期及时间
	 * @param date
	 * @param hour
	 */
	public void clearOutTimeTicket(String storageId, String date, int hour) {
		
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			DBRow para = new DBRow();
			para.add("storage_id", storageId);
			para.add("work_date", date);
			para.add("hour", hour);
			para.add("now", format2.format(new Date()));
			DBRow[] rows = dbUtilAutoTran.selectPreMutliple("select * from wms_appointment_ticket where storage_id=? and work_date=? and `hour`=? and (UNIX_TIMESTAMP(?)-UNIX_TIMESTAMP(date_created))>1800", para);
			int incnt=0, outcnt=0;
			//TODO 累加后修改
			for(DBRow row: rows) {
				if ("outbound".equals(row.getString("appointment_type"))) {
					outcnt++;
					boolean b = subOneWorkOutNoLimit(row.getString("work_date"), row.get("hour", 0), row.get("storage_id", 0L), "", false);
					if (b) dbUtilAutoTran.delete(" where id ="+ row.get("id", 0L) , "wms_appointment_ticket");
				} else if ("inbound".equals(row.getString("appointment_type"))) {
					incnt++;
					boolean b = subOneWorkInNoLimit(row.getString("work_date"), row.get("hour", 0), row.get("storage_id", 0L), "", false);
					if (b) dbUtilAutoTran.delete(" where id ="+ row.get("id", 0L) , "wms_appointment_ticket");
				}
			}
			if (outcnt>0) {
				
			}
			if (incnt>0) {
				
			}
		} catch (Exception e) {e.printStackTrace();}
	}
}