package com.cwc.app.floor.api.zr;

import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.app.util.StrUtil;

public class FloorContainerMgrZr {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
//	public DBRow addContainer(DBRow insertRow) throws Exception 
//	{
//		try
//		{
//			  ContainerTypeKey containerTypeKey = new ContainerTypeKey();
//		      String tableName = ConfigBean.getStringValue("container");
//		      //long containerId = this.dbUtilAutoTran.getSequance(ConfigBean.getStringValue("container"));
//		      //insertRow.add("con_id", containerId);
//		      
//		      String fixPre = containerTypeKey.getContainerTypeKeyValue(insertRow.get("container_type", 0));
//		      String containerName = insertRow.getString("container");
//		      if (!StrUtil.isBlank(containerName)) {
//		        containerName = "TLP" + containerName;
//		        insertRow.add("container", containerName);
//		      }
//		      
//		      long containerId = this.dbUtilAutoTran.insertReturnId(tableName, insertRow);
//		      
//		      if (StrUtil.isBlank(containerName)) {
//		          containerName = fixPre + containerId;
//		          
//		          DBRow updateData = new DBRow();
//		          updateData.add("container", containerName);
//		          
//		          updateContainer(containerId, updateData);
//		          
//		          insertRow.add("container", containerName);
//		      } 
//		      insertRow.add("con_id", containerId);
//		      return insertRow;
//	     }
//		 catch(Exception e)
//		 {
//			throw new Exception("FloorContainerMgrZr.addContainer(insertRow):"+e);
//		}
//	}
	
	
	public DBRow addContainer(DBRow insertRow) throws Exception
	   {
	     try
	     {
//	       ContainerTypeKey containerTypeKey = new ContainerTypeKey();
	       String tableName = ConfigBean.getStringValue("container");
//	       String fixPre = containerTypeKey.getContainerTypeKeyValue(insertRow.get("container_type", 0));
	       String containerName = insertRow.getString("container");
	       if (!com.cwc.app.util.StrUtil.isBlank(containerName)) {
	         //containerName = "TLP" + containerName;  //Edit by Yuanxinyu
	         insertRow.add("container", containerName);
	       }
	       long containerId = dbUtilAutoTran.insertReturnId(tableName, insertRow);
	       if (com.cwc.app.util.StrUtil.isBlank(containerName)) {
	         //containerName = fixPre + containerId;  //Edit by Yuanxinyu
	         containerName = ""+containerId;
	         DBRow updateData = new DBRow();
	         updateData.add("container", containerName);
	         
	         updateContainer(containerId, updateData);
	         
	         insertRow.add("container", containerName);
	       }
	       insertRow.add("con_id", containerId);
	       return insertRow;
	     }
	     catch (Exception e)
	     {
	       throw new Exception("FloorContainerMgrZr.addContainer(insertRow):" + e);
	     }
	   }

	
	
 
	/*
	  *1.  首先去查询 container_type_id 查询是否包含子container，如果包含那么直接查询出来。
	  *DBRow{ 						//容器类型
	  *		containerTypeId :		//容器的类型ID
	  *		containerType :			//容器的类型
	  *		totalSubmit : 			//子容器总的数量
	  *		innerContainerTypeId:	//子容器类型ID
	  *		innerContainerType :	//总容器类型		
	  *		innerDBRow:DBRow		//子容器的类型
	  *		is_has_sn:		//是否包含SN
	  *	}
	  */
	public DBRow getfixILPType(long container_type_id) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("inner_box_type");
			String sql = "select * from " + tableName + " where ibt_id = ?";
			DBRow para = new DBRow();
			para.add("ibt_id", container_type_id);
			DBRow row = dbUtilAutoTran.selectPreSingle(sql, para );
			if(row != null){
				DBRow returnRow = new DBRow();
				returnRow.add("containerTypeId", container_type_id);
//				returnRow.add("containerType", ContainerTypeKey.ILP);//去掉ILP和BLP
				returnRow.add("is_has_sn", row.get("is_has_sn", 0));
				return returnRow ;
			}
			return null ;
		}catch(Exception e){
			throw new Exception("FloorContainerMgrZr.getfixILPType(container_type_id):"+e);
		}
	}
	
	 /*
	  *1.  首先去查询 container_type_id 查询是否包含子container，如果包含那么直接查询出来。
	  *DBRow{ 						//容器类型
	  *		containerTypeId :		//容器的类型ID
	  *		containerType :			//容器的类型
	  *		totalSubmit : 			//子容器总的数量
	  *		innerContainerTypeId:	//子容器类型ID
	  *		innerContainerType :	//总容器类型		
	  *		innerDBRow:DBRow		//子容器的类型
	  *		is_has_sn:		//是否包含SN
	  *	}
	  */
	public DBRow getfixBLPType(long container_type_id) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("box_type");
			String sql = "select * from " + tableName + " where box_type_id = ?";
			DBRow para = new DBRow();
			para.add("box_type_id", container_type_id);
			DBRow row = dbUtilAutoTran.selectPreSingle(sql, para );
			if(row != null){
				DBRow returnRow = new DBRow();
				returnRow.add("containerTypeId", container_type_id);
//				returnRow.add("containerType", ContainerTypeKey.BLP);//去掉ILP和BLP
				returnRow.add("is_has_sn", row.get("is_has_sn", 0));
				long innerType =  row.get("box_inner_type", 0l);
				if(innerType != 0l){
					returnRow.add("innerContainerTypeId", innerType);
//					returnRow.add("innerContainerType", ContainerTypeKey.ILP);//去掉ILP和BLP
					returnRow.add("totalInner", row.get("box_total", 0));
				}
				return returnRow ;
			}
			return null ;
		}catch(Exception e){
			throw new Exception("FloorContainerMgrZr.getfixBLPType(container_type_id):"+e);
		}
	}
	/*
	  *1.  首先去查询 container_type_id 查询是否包含子container，如果包含那么直接查询出来。
	  *DBRow{ 						//容器类型
	  *		containerTypeId :		//容器的类型ID
	  *		containerType :			//容器的类型
	  *		totalSubmit : 			//子容器总的数量
	  *		innerContainerTypeId:	//子容器类型ID
	  *		innerContainerType :	//总容器类型		
	  *		innerDBRow:DBRow		//子容器的类型
	  *		is_has_sn
	  *	}
	  */
	public DBRow getfixCLPType(long container_type_id) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("clp_type");
			String sql = "select * from " + tableName + " where sku_lp_type_id = ?";
			DBRow para = new DBRow();
			para.add("sku_lp_type_id", container_type_id);
			DBRow row = dbUtilAutoTran.selectPreSingle(sql, para );
			if(row != null){
				DBRow returnRow = new DBRow();
				returnRow.add("containerTypeId", container_type_id);
				returnRow.add("containerType", ContainerTypeKey.CLP);
				returnRow.add("is_has_sn", row.get("is_has_sn", 0));
				long innerType =  row.get("sku_lp_box_type", 0l);
				if(innerType != 0l){
					returnRow.add("innerContainerTypeId", innerType);
//					returnRow.add("innerContainerType", ContainerTypeKey.BLP);//去掉ILP和BLP
					returnRow.add("totalInner", row.get("sku_lp_total_box", 0));
				}
				return returnRow ;
			}
			return null ;
		}catch(Exception e){
			throw new Exception("FloorContainerMgrZr.getfixCLPType(container_type_id):"+e);
		}
	}
	public DBRow[] getContainerBaseTypes(int containerType) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("container_type");
			String sql = "select * from " + tableName + " where container_type = ? ";
			DBRow para = new DBRow();
			para.add("container_type", containerType);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch (Exception e) {
			throw new Exception("FloorContainerMgrZr.getContainerBaseTypes(containerType):"+e);
 		}
	}
	
	/**
	 * 根据容器ID获得容器明细
	 * @author zhanjie
	 * @param con_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailContainerById(long con_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("container")+" where con_id = ? ";
			
			DBRow para = new DBRow();
			para.add("con_id",con_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZr.getDetailContainerById:"+e);
		}
	}
	
	public void updateContainer(long container_id, DBRow updateRow) throws Exception {
		try {
			String tableName = ConfigBean.getStringValue("container");
			this.dbUtilAutoTran.update(" where con_id=" + container_id, tableName, updateRow);
		} catch (Exception e) {
			throw new Exception( "FloorContainerMgrZr.updateContainer(container_id , updateRow):" + e);
		}
	}

}
