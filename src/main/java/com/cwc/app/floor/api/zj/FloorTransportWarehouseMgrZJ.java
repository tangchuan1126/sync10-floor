package com.cwc.app.floor.api.zj;

import com.cwc.app.key.ProductStorePhysicalOperationKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorTransportWarehouseMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加转运伪入库
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addTransportWareHouse(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long tw_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("transport_warehouse"));
			
			dbrow.add("tw_id",tw_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("transport_warehouse"),dbrow);
			
			return tw_id;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportWarehouseMgrZJ addTransportWareHouse error:"+e);
		}
	}
	
	/**
	 * 根据转运单ID删除伪交货
	 * @param transport_id
	 * @throws Exception
	 */
	public void delTransportWareHouseByTransportId(long transport_id,String machine_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where tw_transport_id="+transport_id+" and machine_id= '"+machine_id+"'",ConfigBean.getStringValue("transport_warehouse"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportWarehouseMgrZJ delTransportWareHouseByTransportId error:"+e);
		}
	}
	
	/**
	 * 转运单到货比较
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] compareTransportByTransportId(long transport_id)
		throws Exception
	{

		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_warehouse_compare_view")+" where transport_send_count != real_count and  transport_id = ? ";
			
			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportWarehouseMgrZJ compareTransportByTransportId error:"+e);
		}
	}
	
	/**
	 * 转运单收货比较
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] compareTransportOutWareByTransportId(long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from (" 
						+"select to_machine_id as machine_id,to_pc_id as pc_id,`to`.to_serial_number as serial_number,`to`.to_count as send_count,`tw`.tw_count as receive_count,(`to`.to_count-COALESCE(`tw`.tw_count, 0)) as different_count,COALESCE('Lack') as different_type from transport_outbound `to` "
						+"left join transport_warehouse tw on `to`.to_transport_id = tw.tw_transport_id and `to`.to_serial_number = tw.tw_serial_number " 
						+"where `to`.to_serial_number is not NULL and `to`.to_transport_id =? "
						+"union all "
						+"select tw.machine_id as machine_id,tw.tw_product_id as pc_id,tw.tw_serial_number as serial_number,`to`.to_count as send_count,`tw`.tw_count as receive_count,(tw.tw_count-COALESCE(`to`.to_count, 0)) as different_count,COALESCE('More') as different_type from transport_warehouse tw "
						+"left join transport_outbound `to` on `to`.to_transport_id = tw.tw_transport_id and `to`.to_serial_number = tw.tw_serial_number " 
						+"where tw.tw_serial_number is not NULL and tw.tw_transport_id = ?"
						+") as differentView where different_count !=0";
			
			DBRow para = new DBRow();
			para.add("to_transport_id",transport_id);
			para.add("tw_transport_id",transport_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTranposrtWarehouseMgrZJ ");
		}
	}
	
	public DBRow[] getTransportWareHouseBySNNotMachine(String sn,String machine_id,long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_warehouse")+" where machine_id != ? and tw_serial_number = ? and tw_transport_id = ?";
			
			DBRow para = new DBRow();
			para.add("machine_id",machine_id);
			para.add("tw_serial_number",sn);
			para.add("transport_id",transport_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportWarehouseMgrZJ getTransportWareHouseBySNNotMachine error:"+e);
		}
		
	}
	
	/**
	 * 收货数量差异比较
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] transportReceiveCompareCount(long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from " 
						+"("
						+"	select sendView.pc_id,sendView.send_count,receiveView.receive_count,(sendView.send_count-COALESCE(receiveView.receive_count,0)) different,'Lack' as diiferent_type from " 
						+"	(select to_pc_id as pc_id,sum(to_count) as send_count from transport_outbound where to_transport_id = "+transport_id+" GROUP BY to_pc_id) as sendView "
						+"	left join " 
						+"	(select tw_product_id as pc_id,sum(tw_count) as receive_count from transport_warehouse where tw_transport_id = "+transport_id+" GROUP BY tw_product_id) as receiveView on receiveView.pc_id = sendView.pc_id "
						+"	union all " 
						+"	select receiveView.pc_id,sendView.send_count,receiveView.receive_count,(receiveView.receive_count-COALESCE(sendView.send_count,0))different,'More' as diiferent_type from " 
						+" (select tw_product_id as pc_id,sum(tw_count) as receive_count from transport_warehouse where tw_transport_id = "+transport_id+"  GROUP BY tw_product_id) as receiveView "
						+"	left join " 
						+"	(select to_pc_id as pc_id,sum(to_count) as send_count from transport_outbound where to_transport_id = "+transport_id+" GROUP BY to_pc_id) as sendView on receiveView.pc_id = sendView.pc_id "
						+") compare where different >0 ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportWarehouseMgrZJ transportReceiveCompareCount error:"+e);
		}
	}
	
	/**
	 * 转运单待放货比较
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] transportReceivePutLocationCompare(long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from  "
						+"("					
						+"	SELECT tw.tw_serial_number as serial_number,tw.tw_transport_id as transport_id,tw.tw_product_id as pc_id,(tw_count-COALESCE(pspl.quantity, 0))AS different_count,'Lack' as different_type "
						+"  FROM " 
						+"	transport_warehouse AS tw "
						+"	LEFT JOIN product_store_physical_log pspl ON tw.tw_product_id = pspl.pc_id "
						+"	AND tw.tw_serial_number = pspl.serial_number and pspl.operation_type = "+ProductStorePhysicalOperationKey.LayUp
						+"	AND pspl.system_bill_id = tw.tw_transport_id "
						+"	AND( "
						+"		pspl.system_bill_type = 1 "
						+"		OR pspl.system_bill_type = 2 "
						+"	) "
						+"  where tw.tw_serial_number is not NULL and tw.tw_transport_id ="+transport_id
						+"	UNION ALL "
						+"	SELECT "
						+"		pspl.serial_number,pspl.system_bill_id as transport_id,pspl.pc_id,(pspl.quantity - COALESCE(tw_count, 0))AS different_count,'More' as different_type "
						+"	FROM "
						+"	product_store_physical_log pspl "
						+"	LEFT JOIN transport_warehouse AS tw ON tw.tw_product_id = pspl.pc_id "
						+"	AND tw.tw_serial_number = pspl.serial_number AND pspl.system_bill_id = tw.tw_transport_id"
						+"  where pspl.operation_type = "+ProductStorePhysicalOperationKey.LayUp
						+"	and pspl.system_bill_id ="+transport_id
						+"	AND( "
						+"		pspl.system_bill_type = 1 "
						+"		OR pspl.system_bill_type = 2 "
						+"	) and pspl.serial_number is not NULL "
						+")differentView where different_count >0";
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportWarehouseMgrZJ transportReceivePutLocationCompare error:"+e);
		}
	}
	
	
	/**
	 * 获得转运入库
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportWarehouse(long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_warehouse")+" where tw_transport_id=?";
			
			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportWarehouseMgrZJ getTransportWarehouse error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
