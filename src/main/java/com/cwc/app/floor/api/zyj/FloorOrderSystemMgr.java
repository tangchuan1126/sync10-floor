package com.cwc.app.floor.api.zyj;

import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorOrderSystemMgr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加OrderSystem
	 * @param row
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月17日 下午2:06:24
	 */
	public long addOrderSystem(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("order_system"), row);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorOrderSystemMgr addOrderSystem"+e);
		}
	}
	
	/**
	 * 更新OrderSystem
	 * @param id
	 * @param row
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月17日 下午2:06:42
	 */
	public void updateOrderSystem(long id, DBRow row) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where id="+id, ConfigBean.getStringValue("order_system"), row);
		}
		catch(Exception e)
		{
			 throw new Exception("FloorOrderSystemMgr updateOrderSystem"+e);
		}
	}
	
	/**
	 * 查询单据
	 * @param system
	 * @param number
	 * @param number_type
	 * @param companyId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月17日 下午2:06:52
	 */
	public DBRow[] findOrderSystemByNumberTypeInfos(int system, String number, int doc_name, String companyId,String account_id, long ps_id, long receipt_no) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from order_system where 1=1");
			if(system > 0)
			{
				sql.append(" and system_type = ").append(system);
			}
			if(!StrUtil.isBlank(number))
			{
				sql.append(" and number = '").append(number).append("'");
			}
			if(doc_name > 0)
			{
				sql.append(" and doc_name = ").append(doc_name);
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and company_id = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(account_id))
			{
				sql.append(" and account_id = '").append(account_id).append("'");
			}
			if(ps_id > 0)
			{
				sql.append(" and ps_id=").append(ps_id);
			}
			if(receipt_no > 0)
			{
				sql.append(" and receipt_no=").append(receipt_no);
			}
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorOrderSystemMgr findOrderSystemByNumberTypeInfos"+e);
		}
	}
	
	
	/**
	 * 通过orderSystemId查询
	 * @param os_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月17日 下午3:51:15
	 */
	public DBRow findOrderSystemByNumberTypeInfos(long os_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from order_system where 1=1");
			if(os_id > 0)
			{
				sql.append(" and id = ").append(os_id);
			}
			return dbUtilAutoTran.selectSingle(sql.toString());
		}
		catch(Exception e)
		{
			 throw new Exception("FloorOrderSystemMgr findOrderSystemByNumberTypeInfos"+e);
		}
	}
	
	/**
	 * 通过id删除OrderSystem
	 * @param os_id
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月18日 下午3:21:47
	 */
	public void deleteOrderSystemByOsid(long os_id) throws Exception
	{
		try
		{
			dbUtilAutoTran.delete(" where id="+os_id, ConfigBean.getStringValue("order_system"));
		}
		catch(Exception e)
		{
			 throw new Exception("FloorOrderSystemMgr deleteOrderSystemByOsid"+e);
		}
	}
	/**
	 * 扫描pallet 验重
	 * @param company_id
	 * @param pallet_number
	 * @return
	 * @throws Exception
	 */
	public int validatePalletNumnberRepeat(String company_id,String pallet_number) throws Exception {
		try{
			DBRow result = dbUtilAutoTran.selectSingle("select count(*) as pallet_count from wms_load_order_pallet_type_count wlc join wms_load_order wlo on wlo.wms_order_id=wlc.wms_order_id "+
													   "where wlo.company_id='"+company_id+"' and wlc.wms_pallet_number='"+pallet_number+"'");
			int count = 0 ;
			if(result != null){
				count = result.get("pallet_count", 0);
			}
			return count ;
		}catch(Exception e){
			throw new Exception("validatePalletNumnberRepeat" + e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
