package com.cwc.app.floor.api.zyj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorShortMessageMgrZyj {
	
	private DBUtilAutoTran dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	/**
	 * 添加短信
	 * @param messageRow
	 * @param receiverRow
	 * @return
	 * @throws Exception
	 */
	public long addShortMessage(DBRow messageRow) throws Exception{
		try{
			//保存短信信息
			 String tableName1	=  ConfigBean.getStringValue("short_message_info");
			 return dbUtilAutoTran.insertReturnId(tableName1,messageRow );
		}catch(Exception e){
			throw new Exception("FloorShortMessageZyj.addShortMessage(messageRow):"+e);
		}
	}
	
	
	/**
	 * 添加收信人
	 * @param messageRow
	 * @param receiverRow
	 * @return
	 * @throws Exception
	 */
	public long addShortMessageReceiver(DBRow receiverRow) throws Exception{
		try{
			 //保存收信人信息
			 String tableName2	=  ConfigBean.getStringValue("short_message_receiver");
			 return dbUtilAutoTran.insertReturnId(tableName2,receiverRow );
		}catch(Exception e){
			throw new Exception("FloorShortMessageZyj.addShortMessage(receiverRow):"+e);
		}
	}
	
	
	
	/**
	 * 得到短信的列表
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getShortMessageList(PageCtrl pc, int moduleIdSearch, String receiverNameSearch, String receiverPhoneSearch, String messageContentSearch) throws Exception{
		try {
			
			String sql = "SELECT DISTINCT(s.id),s.module_id,s.business_id,s.business_data,s.business_table,s.business_column_name,s.business_table_key,s.content,s.send_user,s.send_time,s.isSuccess FROM short_message_info s";
			
			if(!"".equals(receiverNameSearch) || !"".equals(receiverPhoneSearch)){
				sql += " LEFT JOIN short_message_receiver r  ON  s.id = r.short_message_id WHERE 1=1";
			}else{
				sql += " where 1=1";
			}
			if(0 != moduleIdSearch){
				sql +=  " AND s.module_id = " + moduleIdSearch;
			}
			if(!"".equals(receiverNameSearch)){
				sql += " AND r.receiver_name LIKE '%" + receiverNameSearch  +"%'";
			}
			if(!"".equals(receiverPhoneSearch)){
				sql += " AND r.receiver_phone LIKE '%" + receiverPhoneSearch + "%'";
			}
			if(!"".equals(messageContentSearch)){
				sql += " AND s.content LIKE '%" + messageContentSearch + "%'";
			}
			sql += " order by s.send_time desc";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (Exception e) {
			throw new Exception("FloorShortMessageZyj.getShortMessageList(pc):"+e);
		}
	}
	
	
	/**
	 * 通过短信得到所有收信人
	 * @param messageId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getShortMessageReceivers(long messageId) throws Exception{
		try {
			String sql = "SELECT * FROM short_message_receiver where short_message_id = " + messageId;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorShortMessageZyj.getShortMessageReceivers(messageId):"+e);
		}
	}
	
	/**
	 * 通过群组Id,得到此机构下的所有用户
	 * @param adgId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getUserListByAdgId(long adgId) throws Exception{
		try {
			String sql = "SELECT adid as receiver_id, employe_name as receiver_name, mobilePhone as receiver_phone FROM admin WHERE adgid = " + adgId +" AND llock = 0";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorShortMessageZyj.getUserListByAdgId(adgId):"+e);
		}
		
	}
	
	
	/**
	 * 通过群组Id,角色Id,得到此机构下的该角色的用户
	 * @param adgId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getUserListByAdgIdRoleId(long adgId, long roleId) throws Exception{
		try {
			String sql = "SELECT adid as receiver_id, employe_name as receiver_name, mobilePhone as receiver_phone FROM admin WHERE adgid = " + adgId +" AND proJsId = " + roleId;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorShortMessageZyj.getUserListByAdgId(adgId):"+e);
		}
		
	}
	
	/**
	 * 通过群组Id,角色Id,得到此机构下的该角色的用户
	 * @param adgId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getUserListByAdgIdRoleIdArray(long adgId, long[] roleId) throws Exception{
		try {
			String sql = "SELECT adid as receiver_id, employe_name as receiver_name, mobilePhone as receiver_phone FROM admin WHERE adgid = " + adgId ;
			if(null != roleId && roleId.length > 0){
				if(roleId.length == 1){
					sql += " AND proJsId = " + roleId[0];
				}else{
					sql += " AND (";
					sql += " proJsId = " + roleId[0];
					for(int i = 1; i < roleId.length; i ++){
						sql += " OR proJsId = " + roleId[i];
					}
					sql += ")";
				}
			}
			sql += " AND llock = 0";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorShortMessageZyj.getUserListByAdgId(adgId):"+e);
		}
		
	}
	
	/**
	 * 通过用户ID得到用户信息
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public DBRow getUserInfoByUserId(long userId) throws Exception{
		try {
			String sql = "SELECT employe_name, mobilePhone FROM admin WHERE adid = " + userId;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorShortMessageZyj.getUserListByAdgId(adgId):"+e);
		}
	}
	
	/**
	 * 通过用户ID得到用户信息,reciver_id,receiver_name,receiver_phone
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public DBRow getUserRowByUserId(long userId) throws Exception{
		try {
			String sql = "SELECT adid as receiver_id, employe_name as receiver_name, mobilePhone as receiver_phone FROM admin WHERE adid = " + userId;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorShortMessageZyj.getUserRowByUserId(adid):"+e);
		}
	}
	
	public void updateSystemConfigShortMessage(DBRow row) throws Exception{
		try {
			dbUtilAutoTran.update("where confname = 'short_message_balance'",ConfigBean.getStringValue("config"),row);
		} catch (Exception e) {
			throw new Exception("FloorShortMessageZyj.updateSystemConfigShortMessage():"+e);
		}
	}
	
	public DBRow[] getMessageContentWordList() throws Exception{
		try {
			String sql = "select * from short_message_word_handle";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorShortMessageZyj.getMessageContentWordList():"+e);
		}
	}
}
