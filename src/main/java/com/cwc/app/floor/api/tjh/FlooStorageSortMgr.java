package com.cwc.app.floor.api.tjh;


import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
/**
 * 
 * @author Administrator
 *仓库分类业务实现类
 */
public class FlooStorageSortMgr {
	private DBUtilAutoTran dbUtilAutoTran;
	
	

	/**
	 * 添加发件人信息
	 * @param dbRow
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public long addStorageSort(DBRow dbRow)
			throws Exception 
	{
		try 
		{
			 long di_id= dbUtilAutoTran.getSequance(ConfigBean.getStringValue("deliverer_info"));
			 dbRow.add("di_id", di_id);
			 dbUtilAutoTran.insert(ConfigBean.getStringValue("deliverer_info"), dbRow);
			 return (di_id);      
		} 
		catch (Exception e) 
		{
			throw new Exception("addStorageSort() error:"+e);
		}
	}
   /**
    * 根据仓库ID 查找该仓库下的发件人基本信息
    * @param id
    * @return
    * @throws Exception
    */
	public DBRow[] getDetailStorageSortById(long id) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("deliverer_info")+" where ps_id="+id;
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailAssetsCategory(int id) error:"+e);
		}	
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
