package com.cwc.app.floor.api.qll;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorVertualTerminalMgrQLL {

	private DBUtilAutoTran dbUtilAutoTran ;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	 
	 
	public DBRow getPaymentStatus(long oid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("oid",oid);
			String sql = "select * from "+ConfigBean.getStringValue("porder")+" where oid= ?  ";
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorVertualTerminalMgrQLL.getAllVertualTerminal()error："+e);
		}
	}
	/**
	 * 增加VertualTerminal 
	 * @param row
	 * @throws Exception
	 */
	public void addVertualTerminal(DBRow row)
		throws Exception
	{
		long id;
		try 
		{
			id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("vertualterminal_hist"));
			row.add("id", id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("vertualterminal_hist"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("   FloorVertualTerminalMgrQLL. addVertualTerminal(DBRow row) error:"+e);
		}
	
	}
	
	/**
	 * 查询所有收款的操作记录
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllVertualTerminal(PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql="select * from "+ConfigBean.getStringValue("vertualterminal_hist")+" order by id desc";
			return (dbUtilAutoTran.selectMutliple(sql, pc));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorVertualTerminalMgrQLL.getAllVertualTerminal()error："+e);
		}
	}	
	public DBRow[] getPayment(PageCtrl pc , String txnId , String state) throws Exception {
		try{
		 
			StringBuffer sql = new StringBuffer( "select * from " + ConfigBean.getStringValue("vertualterminal_hist") + " where  1=1 " );
			if(txnId.length() > 0){
				sql.append(" and txn_id='").append(txnId).append("'");
			}
			
			if(state.equals("0")){
				sql.append(" and result='Success'");
			}else if(state.equals("1")){
				sql.append(" and result='SuccessWithWarning'");
			}else if(state.equals("2")){
				sql.append(" and result='Failure'");
			}
			
			return  dbUtilAutoTran.selectMutliple(sql.toString(), pc) ;
		}catch (Exception e) {
			throw new Exception("FloorVertualTerminalMgrQLL.getPayment()error："+e);
		}
	}
}
