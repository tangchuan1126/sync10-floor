package com.cwc.app.floor.api.sbb;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTranSQLServer;

public class FloorReceiptSqlServerMgrSbb{
	
	private DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer;
	
	public void setDbUtilAutoTranSQLServer(DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer) {
		this.dbUtilAutoTranSQLServer = dbUtilAutoTranSQLServer;
	}
	
	/** 
	 * UNSUCCESS
	 * 查询收货主单据
	 * @param DBRow[companyId,receiptNo]
	 * @return 收货主单据 全部字段
	 * @author subin
	 * */
	public DBRow getReceipts(DBRow param) throws Exception{
		
		try {
			
			String sql = "select * from receipts where companyid = ? and receiptno = ?";
			
			return dbUtilAutoTranSQLServer.selectPreSingle(sql, param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.getReceipts(DBRow param) error:" + e);
		}
	}
	
	/** 
	 * UNSUCCESS
	 * 查询收货主单据明细
	 * @param DBRow[companyId,receiptNo]
	 * @return 收货主单据明细 全部字段
	 * @author subin
	 * */
	public DBRow[] getReceiptLines(DBRow param) throws Exception{
		
		try {
			
			String sql = "select * from ReceiptLines where companyid = ? and receiptno = ?";
			
			return dbUtilAutoTranSQLServer.selectPreMutliple(sql, param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.getReceiptLines(DBRow param) error:" + e);
		}
	}
	
	/** 
	 * 查询Movements
	 * @param DBRow[companyId,receiptNo]
	 * @return Movements
	 * @author subin
	 * */
	public DBRow[] getMovements(DBRow param) throws Exception{
		
		try {
			
			String sql = "select mov.*  from ReceiptLinePlates rlp left join Movements mov on rlp.PlateNo = mov.PlateNo and rlp.CompanyID = mov.CompanyID where mov.Status = 'Open' and rlp.CompanyID = ? and rlp.ReceiptNo = ?";
			
			return dbUtilAutoTranSQLServer.selectPreMutliple(sql, param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.getMovements(DBRow param) error:" + e);
		}
	}
	
	/** 
	 * TEST SUCCESS
	 * 查询收货主单据
	 * @param DBRow[companyId,receiptNo]
	 * @return 收货主单据,以mysql数据库的字段名.DBRow[company_id,receipt_no,status...]
	 * @author subin
	 * 删除了 一个字段 rn.receiptticketprinted receipt_ticket_printed,
	 * */
	public DBRow getOldExpectedReceipt(DBRow param) throws Exception{
		
		try {
			
			String sql = "select rn.companyid company_id,rn.receiptno receipt_no,rn.status status,rn.customerid customer_id,rn.supplierid supplier_id,rn.entereddate entered_date,rn.warehouseid warehouse_id,rn.referenceno reference_no,rn.pono po_no,rn.bolno bol_no,rn.containerno container_no,rn.seals seals,rn.carrierid carrier_id,rn.scheduleddate schedule_date,rn.appointmentdate appointment_date,rn.inyarddate in_yard_date,rn.devanneddate devanner_date,rn.lastfreedate last_free_date,rn.receipttype receipt_type,rn.note note,rn.cartonlabelprinted carton_label_printed,rn.plateprinted plate_printed,rn.tallysheetprinted tally_sheet_printed,rn.receiptprinted receipt_printed,rn.linksequenceno link_sequence_no,rn.recontrolno re_control_no,rn.ediorder edi_order,rn.send944 send944,rn.send947 send947,rn.datecreated date_created,rn.usercreated user_created,rn.dateupdated date_updated,rn.userupdated user_updated from receipts rn where companyid = ? and receiptno = ?";
			
			return dbUtilAutoTranSQLServer.selectPreSingle(sql, param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.getOldExpectedReceipt(DBRow param) error:" + e);
		}
	}
	
	/**
	 * TEST SUCCESS
	 * 查询收货明细
	 * @param DBRow[companyId,receiptNo]
	 * @return 收货明细数组,以mysql数据库的字段名.DBRow[{company_id,receipt_no,line_no...}]
	 * @author subin
	 * */
	public DBRow[] getOldExpectedReceiptLines(DBRow param) throws Exception{
		
		try {
			
			String sql = "select rnl.companyid company_id,rnl.receiptno receipt_no,rnl.[lineno] line_no,rnl.itemid item_id,rnl.warehouseid warehouse_id,rnl.zoneid zone_id,rnl.locationid location_id,rnl.palletsizeid pallet_size,rnl.pono po_no,rnl.polineno po_line_no,rnl.lotno lot_no,rnl.expectedqty expected_qty,rnl.pallets pallets,rnl.note note,cis.qtyperunit qty_per_pallet,cis.validatedinboundserialno is_check_sn,cis.serialnolength sn_size,cis.serialnoscanlotnocheck is_check_lot_no,rn.supplierid supplier from receiptlines as rnl left join receipts as rn on rnl.companyid = rn.companyid and rnl.receiptno = rn.receiptno left join customeritems as cis on rnl.companyid = cis.companyid and rnl.itemid = cis.itemid and rn.customerid = cis.customerid where rnl.companyid = ? and rnl.receiptno = ?";
			
			return dbUtilAutoTranSQLServer.selectPreMutliple(sql, param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.getOldExpectedReceiptLines(DBRow param) error:" + e);
		}
	}
	
	/**
	 * TEST SUCCESS
	 * 插入表ReceiptLinePlates
	 * @param DBRow[CompanyID,ReceiptNo int,[LineNo] int,SupplierID,LotNo,PlateNo int,ReceivedQty int]
	 * @return null
	 * @author subin
	 * */
	public void insertReceiptLinePlates(DBRow param) throws Exception{
		
		try {
			
			dbUtilAutoTranSQLServer.insert("ReceiptLinePlates", param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.insertReceiptLinePlates(DBRow param) error:" + e);
		}
	}
	
	/**
	 * TEST SUCCESS
	 * 插入表ReceiptLineSerialNos
	 * @param DBRow[CompanyID,ReceiptNo int,[LineNo] int,SerialNoType,SerialNo,Qty int,UserID,TimeStamp]
	 * @return null
	 * @author subin
	 * */
	public void insertReceiptLineSerialNos(DBRow param) throws Exception{
		
		try {
			
			dbUtilAutoTranSQLServer.insert("receiptlineserialnos", param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.insertReceiptLineSerialNos(DBRow param) error:" + e);
		}
	}
	
	/**
	 * TEST SUCCESS
	 * 插入表Pallets
	 * @param DBRow[CompanyID,palletNo int,Status,LotNo,DigitLimit int,AllowedQty int,PalletPrinted 1/0,PalletLabelPrinted 1/0,DateCreated,UserCreated]
	 * @return null
	 * @author subin
	 * */
	public void insertPallets(DBRow param) throws Exception{
		
		try {
			
			dbUtilAutoTranSQLServer.insert("pallets", param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.insertPallets(DBRow param) error:" + e);
		}
	}
	
	/**
	 * TEST SUCCESS
	 * 插入表Movements
	 * @paramn String[UserCreated,company_id,Status,plate_no,locationId]
	 * @return MovementNo
	 * @author subin
	 * */
	public int insertMovements(String[] param) throws Exception{
		
		try {
			
			return dbUtilAutoTranSQLServer.CallStringSingleResultProcedure("sp_movement_addnew", param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.insertMovements(DBRow param) error:" + e);
		}
	}
	
	/**
	 * TEST SUCCESS
	 * 插入表PalletLines
	 * @param
	 * @return
	 * @author subin
	 * */
	public void insertPalletLines(DBRow param) throws Exception{
		
		try {
			
			dbUtilAutoTranSQLServer.insert("palletlines", param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.insertPalletLines(DBRow param) error:" + e);
		}
	}
	
	/**
	 * TEST SUCCESS
	 * 获取托盘号plate_no
	 * @param companyID
	 * @return DBRow[plate_no]
	 * @author subin
	 * */
	public int getPalletNo(String companyID) throws Exception{
		
		try {
			
			return dbUtilAutoTranSQLServer.CallStringSingleResultProcedure("sp_getnextplateno_1",companyID);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.getPalletNo(String companyID) error:" + e);
		}
	}
	
	/**
	 * TEST SUCCESS
	 * 查询CustomerItem
	 * @param DBRow[]
	 * @return DBRow
	 * @author subin
	 * */
	public DBRow getCustomerItems(DBRow param) throws Exception{
		
		try {
			
			String sql = "select * from customeritems where companyid = ? and customerid = ? and itemid = ?";
			
			return dbUtilAutoTranSQLServer.selectPreSingle(sql, param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.getCustomerItems(DBRow param) error:" + e);
		}
	}
	
	public DBRow getCustomerItemsForProduct(DBRow param) throws Exception{
		
		try {
			
			String sql = "select ItemID productName, CONVERT(decimal(10,3),ROUND(length,3)) length, CONVERT(decimal(10,3),ROUND(width,3)) width, CONVERT(decimal(10,3),ROUND(height,3)) height, SerialNoLength snLength from customeritems where companyid = ? and customerid = ? and itemid = ?";
			
			return dbUtilAutoTranSQLServer.selectPreSingle(sql, param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.getCustomerItems(DBRow param) error:" + e);
		}
	}
	
	/**
	 * UNTEST 
	 * 更新收货主单据
	 * @param DBRow[companyId,receiptNo][更新项]
	 * @return null
	 * @author subin
	 * */
	public void updateReceipts(DBRow param) throws Exception{
		
		try {
			
			String wherecond = "where companyid = '"
					+param.get("COMPANYID", "-1")
					+"' and receiptno = '"
					+param.get("RECEIPTNO", "-1")+"'";
			
			param.remove("companyId");
			param.remove("receiptNo");
			
			dbUtilAutoTranSQLServer.update(wherecond, "receipts", param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.updateReceipts(DBRow param) error:" + e);
		}
	}
	
	/**
	 * UNTEST 
	 * 更新收货单据明细
	 * @param DBRow[company_id,receipt_no,line_no][更新项]
	 * @return null
	 * @author subin
	 * */
	public void updateReceiptLines(DBRow param) throws Exception{
		
		try {
			
			String wherecond = "where companyid = '"
					+param.get("company_id", "-1")
					+"' and receiptno = '"
					+param.get("receipt_no", "-1")
					+"' and [LineNo] = '"
					+param.get("line_no", "-1")+"'";
			
			param.remove("company_id");
			param.remove("receipt_no");
			param.remove("line_no");
			
			dbUtilAutoTranSQLServer.update(wherecond, "ReceiptLines", param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.updateReceipts(DBRow param) error:" + e);
		}
	}

	/**
	 * UNTEST 
	 * 删除按照数量收货的记录ReceiptLinePlates
	 * @param DBRow[companyId,receiptNo][更新项]
	 * @return null
	 * @author subin
	 * */
	public void deleteReceiptLinePlates(DBRow param) throws Exception{
		
		try {
			
			String sql = "where companyid = '"
					+param.get("companyId", "-1")
					+"' and receiptno = '"
					+param.get("receiptNo", "-1")+"'";
			
			dbUtilAutoTranSQLServer.delete(sql, "ReceiptLinePlates");
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.deleteReceiptLinePlates(DBRow param) error:" + e);
		}
	}
	
	/**
	 * UNTEST 
	 * 查询ReceiptLinePlates数量
	 * @param DBRow[companyId,receiptNo]
	 * @return 查询结果数量
	 * @author subin
	 * */
	public DBRow getReceiptLinePlatesCnt(DBRow param) throws Exception{
		
		try {
			
			String sql = " select count(*) cnt from ReceiptLinePlates where companyid = '"
					+param.get("companyId", "-1")
					+"' and receiptno = '"
					+param.get("receiptNo", "-1")+"'";
			
			return dbUtilAutoTranSQLServer.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.getReceiptLinePlatesCnt(DBRow param) error:" + e);
		}
	}
	
	/**
	 * TEST SUCCESS
	 * 关闭按照数量收货的记录Movements
	 * @param
	 * @return null
	 * @author subin
	 * */
	public void updateMovements(DBRow whereRow,DBRow dataRow) throws Exception{
		
		try {
			
			String sql = "where companyid = '"
					+whereRow.get("CompanyID", "-1")
					+"' and PlateNo = '"
					+whereRow.get("PlateNo", "-1")+"'";
			
			dbUtilAutoTranSQLServer.update(sql, "Movements", dataRow);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.deleteMovements(DBRow param) error:" + e);
		}
	}
	
	/**
	 * TEST SUCCESS
	 * 关闭单据,调用WMS存储过程
	 * @paramn String[UserCreated,company_id,Status,plate_no,locationId]
	 * @return MovementNo
	 * @author subin
	 * */
	public void closeWmsRn(DBRow[] param) throws Exception{
		
		try {
			
			dbUtilAutoTranSQLServer.callProcedure("sp_receipt_update_status_to_closed", param);
			
		}catch (Exception e){
			throw new Exception("FloorReceiptSqlServerMgrSbb.insertMovements(DBRow param) error:" + e);
		}
	}
}