package com.cwc.app.floor.api.zr;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.cwc.app.key.BillStateKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorBillMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public DBRow getBillByBillId(long billId) throws Exception{
		try{
			 String tableName =  ConfigBean.getStringValue("bill_order");
			 return dbUtilAutoTran.selectSingle("select * from " + tableName + " where bill_id="+billId);
 		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getBillByBillId(billId):"+e);
		}
		
	}
	public long addBill(DBRow row) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("bill_order");
		 
			 return dbUtilAutoTran.insertReturnId(tableName, row);
 		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.addBill(row):"+e);
		}
	}
	
	public long addBillItem(DBRow row) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("bill_order_item"); 
			return  dbUtilAutoTran.insertReturnId("bill_order_item", row);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.addBillItem(row):"+e);
		}
	}
	public DBRow[] getAllBill(PageCtrl pc) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("bill_order"); 
			String sql = "select * from "  + tableName +" order by create_date desc";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getAllBill(pc):"+e);
		}
	}
	public DBRow[] getBillItemByBillOrderId(long billId) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("bill_order_item"); 
			String sql = "select * from "  + tableName + " where bill_id =" + billId;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getBillItemByBillOrderId(billId):"+e);
		}
	}
	//返回一个付款形式下面对应的账号
	public DBRow[] getAccountInfoList(long key) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("account_payee"); 
			String sql = "select * from "  + tableName + " where key_type =" + key;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getAccountInfoList(key):"+e);
		}
	}
	public void updateBill(long billId , DBRow billRow) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("bill_order"); 
			dbUtilAutoTran.update(" where bill_id =" + billId , tableName, billRow);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.updateBill(billId,billRow):"+e);
		}
	}
	// saveOrUpdateBillItem 添加或者是修改BillItem
	public long saveOrUpdateBillItem(long billItemId , DBRow billItem , long billId) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("bill_order_item"); 
			String sqlSql = "select count(*) as s from " + tableName +" where bill_item_id="+billItemId;
			DBRow item = dbUtilAutoTran.selectSingle(sqlSql);
			if(item.get("s", 0) <= 0){
				billItem.add("bill_id", billId);
				return this.addBillItem(billItem);
			}else{
				dbUtilAutoTran.update(" where bill_item_id="+billItemId, tableName, billItem);
				return billItemId; 
			}
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.updateBill(billId,billRow):"+e);
		}
	}
	
	public void deleteItemsByIds(String itemIds) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("bill_order_item"); 
			 dbUtilAutoTran.delete(" where bill_item_id in("+itemIds+")", tableName);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.deleteItemsByIds(itemIds):"+e);
		}
	}
	//  根据数据库中的Ids 和页面传递过来的Id值比较。找出没有再数据中的Id ,然后删除
	public void deleteItemsByNotInIdsAndUpdateBillQuantity(List<Long> idList , double quantity , long billId) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("bill_order_item"); 
			String billName = ConfigBean.getStringValue("bill_order"); 
			String idsSql = "select bill_order_item.bill_item_id from bill_order_item where bill_order_item.bill_id = " + billId;
			DBRow[] idRows =  dbUtilAutoTran.selectMutliple(idsSql);
		
			
			
			Set<Long> set = new HashSet<Long>();
			for(DBRow row : idRows){
				set.add(row.get("bill_item_id", 0l));
			}
			 for(long id : idList){
				 if(id !=0 && set.contains(id)){
					 set.remove(id);
				 }
			 }
			 if(set.size() > 0){
				 String idstr = "";
				for(Iterator<Long> it = set.iterator() ; it.hasNext() ;){
					idstr += ","+it.next();
				}
				//System.out.println(idstr);
				dbUtilAutoTran.delete(" where bill_item_id in("+idstr.substring(1)+") ", tableName);
			 }
			 DBRow data = new DBRow();
			 data.add("total_quantity", quantity);
			 dbUtilAutoTran.update(" where bill_id="+billId, billName, data);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.deleteItemsByNotInIds(ids):"+e);
		}
	}
	public void deleteBillById(long billId) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("bill_order"); 
			DBRow data = new DBRow();
			data.add("bill_status", BillStateKey.cancel);
			dbUtilAutoTran.update(" where bill_id="+billId, tableName, data);
			 
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.deleteBillById(billId):"+e);
		}
	}
	
	public void deleteItemsByBillId(long billId) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("bill_order_item"); 
			dbUtilAutoTran.delete(" where bill_id= "+billId, tableName);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.deleteItemsByBillId(billId):"+e);
		}
	}
	public DBRow getBillOrderByBillId(long billId) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("bill_order"); 
			String sql = "select * from " + tableName + " where bill_id =" + billId;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getBillOrderByBillId(billId):"+e);
		}
		
	}
	public DBRow getShippingInfoByScId(long scId) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("shipping_company"); 
			String sql = "select * from " + tableName + " where sc_id =" + scId;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getShippingInfoByScId(scId):"+e);
		}
	}
	public boolean isMainManager(long adid) throws Exception{
		try{
		 
			String sql  = "select count(*) as s from admin where adid = "+adid+" and adgid = (select admin_group.adgid from admin_group where admin_group.description like '%总经理%')"; 
			 DBRow row =   dbUtilAutoTran.selectSingle(sql);
			 boolean isFlag = false;
			 if(row != null && row.get("s", 0) > 0){
				 isFlag = true;
			 }
	 
			 return isFlag;
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.isMainManager(adid):"+e);
		}
	}
	// 根据adid得到这个部门中的主管或者是副主管
	public DBRow[] getUserByAdid(long adid) throws Exception{
		try{
			String sql = "select adid from admin where adgid = (select admin.adgid from admin where adid = "+adid+") and admin.proJsId >= 5 ";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getUserByAdid(adid):"+e);
		}
	}
	public DBRow getPaypalAccountById(long id) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("paypal_account");
			String sql = "select * from " + tableName + " where id ="+id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getPaypalAccountById(id):"+e);
		}
	}
	public DBRow[] filterBill(String st , String end ,long createAdid , int bill_status , long porder_id , String client_id , PageCtrl page,
			int key_type,int bill_type,int account_payee,String account,String account_name)throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("bill_order");
			StringBuffer sql =  new StringBuffer("select * from " + tableName + " where 1=1 ");
			if(st != null && st.trim().length() > 0){
				sql.append(" and create_date>='").append(st).append(" 00:00:00' ");
			}
			if(end != null && end.trim().length() > 0){
				sql.append(" and create_date<='").append(end).append(" 23:59:59' ");
			}
			if( createAdid != -1l){
				sql.append(" and create_adid =").append(createAdid);
			}
			if( bill_status > 0){
				sql.append(" and bill_status =").append(bill_status);
			}
			if( porder_id != 0l){
				sql.append(" and porder_id =").append(porder_id);
			}
			if(client_id != null && client_id.trim().length() > 0){
				sql.append(" and client_id ='").append(client_id).append("' ");
			}
			 if(bill_type != -1){
				 sql.append(" and bill_type =").append(bill_type);
			 }
			 if(key_type != -1){
				 sql.append(" and key_type =").append(key_type);
			 }
			 if(account_payee != 0 && account_payee != -2 ){
				 sql.append(" and account_payee =").append(account_payee);
			 }
			 if(account.length() > 0 && account.indexOf("*") == -1){
				 sql.append(" and account ='").append(account).append("'");
			 }
			 if(account_name.length() > 0 && account_name.indexOf("*") == -1){
				 sql.append(" and account_name ='").append(account_name).append("'");
			 }
			sql.append(" order by bill_id desc ");
			return  dbUtilAutoTran.selectMutliple(sql.toString(), page);
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.filterBill(id):"+e);
		}
	}
	public DBRow[]  getBillByBillId4Search(long[] ids) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("bill_order");
			if(ids == null  || (ids != null && ids.length < 1)){
				return (new DBRow[0]);
			}else{
				StringBuffer whereContion = new StringBuffer("");
					for(int index = 0 , count = ids.length ; index < count ; index++ ){
						if(index == 0){
							whereContion.append(" where bill_id=").append(ids[0]);
						}else{
							whereContion.append(" or bill_id=").append(ids[index]);
			}
				}
				return dbUtilAutoTran.selectMutliple("select * from " +tableName + whereContion.toString());
					
			}
		}catch(Exception e){
			throw new Exception("FloorBillMgrZr.getTradeByTradeId4Search(ids):"+e);
		}
	}
	public DBRow[] getUnoinProduct(long pcId) throws Exception{
		try{
			String sql = "select product.p_name , product.unit_name,product_union.*  from  product, product_union where product_union.set_pid = "+pcId+"  and product_union.pid = product.pc_id ";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorBillMgrZr.getUnoinProduct(pcId):"+e);
		}
	}
	public void updateOrder(long oid , DBRow row) throws Exception{
		try{
			 dbUtilAutoTran.update(" where oid=" + oid, ConfigBean.getStringValue("porder"), row);
		}catch (Exception e) {
			throw new Exception("FloorBillMgrZr.updateOrder(oid,row):"+e);
		}
	}
	
	// Pay for eBay Purchase
	// Pay for Invoice
	// 1. Make a Safety payment
	// 2. Pay for Invoice/eBay Purchase
	// 3. Select Payment Method
	
	//   Item Name -> Item Title
	public DBRow getBillItemByIdAndEmail(long billId , String clientId) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("bill_order");
			String sql = "select * from " + tableName + " where bill_id ="+billId + " and client_id='"+clientId+"'";
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorBillMgrZr.updateOrder(oid,row):"+e);
		}
	 
		
	}
	public DBRow getVertualterminalRowByTxnId(String txnId) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("vertualterminal_hist");
			String sql = "select * from "+ tableName + " where txn_id='" + txnId + "'";
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorBillMgrZr.getVertualterminalRowByTxnId(txnId):"+e);
		}
	}
}
