package com.cwc.app.floor.api.zyj;

import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.JqGridFilterKey;
import com.cwc.app.key.RepairCertificateKey;
import com.cwc.app.key.RepairClearanceKey;
import com.cwc.app.key.RepairDeclarationKey;
import com.cwc.app.key.RepairLogTypeKey;
import com.cwc.app.key.RepairOrderKey;
import com.cwc.app.key.RepairProductFileKey;
import com.cwc.app.key.RepairQualityInspectionKey;
import com.cwc.app.key.RepairTagKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorRepairOrderMgrZyj {
	
	private DBUtilAutoTran dbUtilAutoTran;
	private FloorProductMgr floorProductMgr;
	
	/**
	 * 过滤转运单（支持根据转运仓库、目的仓库、转运单状态过滤）
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterRepairOrder(long send_psid,long receive_psid,PageCtrl pc,int status, int declaration, int clearance,int invoice, int drawback, int day,int stock_in_set, long create_account_id)
		throws Exception
	{
		StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("repair_order")+" where 1=1");
		TDate tDate = new TDate();
		tDate.addDay(-day);
		if(day!=0)
		{
			sql.append(" and updatedate<= '"+tDate.formatDate("yyyy-MM-dd")+"' ");
		}
		
		if(send_psid !=0)//转运仓库
		{
			sql.append(" and send_psid="+send_psid);
		}
		
		if(receive_psid!=0)
		{
			sql.append(" and receive_psid="+receive_psid);
		}
		
		if(status!=0)
		{
			if(status==7)
				sql.append(" and repair_status in("+RepairOrderKey.READY + "," + RepairOrderKey.INTRANSIT + "," + RepairOrderKey.APPROVEING + "," + RepairOrderKey.PACKING + ")");
			else
				sql.append(" and repair_status="+status);
		}
		if(declaration!=0) {
			sql.append(" and IFNULL(declaration,1)="+declaration);
		}
		if(clearance!=0) {
			sql.append(" and IFNULL(clearance,1)="+clearance);
		}
		if(invoice!=0) {
			sql.append(" and IFNULL(invoice,1)="+invoice);
		}
		if(drawback!=0) {
				sql.append(" and IFNULL(drawback,1)="+drawback);
		}
		if(stock_in_set != 0) {
			sql.append(" and IFNULL(stock_in_set,1) ="+stock_in_set);
		}
		if(create_account_id != 0) {
			sql.append(" and create_account_id = " + create_account_id);
		}
		sql.append(" order by repair_date desc");
		
		return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
	}
	
	
	/**
	 * 需跟进的备货中的交货单
	 * @param product_line_id
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackReadyDelivery(long product_line_id,int track_day,PageCtrl pc)
		throws Exception
	{
		String sql = "select * from" 
					+"( "
					+"	select * from" 
					+"	( "
					+"  	select t.*,tl.repair_date as last_track_date from repair_order t "
					+" 		left join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.Goods+" and tl.activity_id ="+RepairOrderKey.READY+" "
					+"		join purchase p on p.purchase_id = t.purchase_id and p.product_line_id = ? "
					+"		left join product_line_define pld ON p.product_line_id = pld.id "
					+"		where t.purchase_id != 0 and t.repair_status = "+RepairOrderKey.READY+" order by tl.repair_date "
					+"	)as ttl group by repair_order_id "
					+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
		
		DBRow para = new DBRow();
		para.add("product_line_id",product_line_id);
		return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
	}
	
	public DBRow[] getNeedTrackSendRepairOrderByPsid(long ps_id,int track_day,int transort_status,PageCtrl pc)
	throws Exception
	{
		try 
		{
			String sql = "select * from " 
						+"( "
						+"	select * from" 
						+"		("
						+"		select t.*,tl.repair_date as last_track_date from repair_order t "
						+"		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.Goods+" and tl.activity_id ="+transort_status+" "
						+"		where t.purchase_id = 0 and t.send_psid = ? and t.repair_status = "+transort_status+" order by tl.repair_date "
						+"		)as ttl group by repair_order_id "
						+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			DBRow para  = new DBRow();
			para.add("send_psid",ps_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgyZyj.getNeedTrackSendRepairOrderByPsid error:"+e);
		}
	}
	
	public DBRow[] getNeedTrackReceiveRepairOrderByPsid(long ps_id,int track_day,int transort_status,PageCtrl pc)
	throws Exception
	{
		try 
		{
			String sql = "select * from " 
						+"( "
						+"	select * from" 
						+"		("
						+"		select t.*,tl.repair_date as last_track_date from repair_order t "
						+"		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.Goods+" and tl.activity_id ="+transort_status+" "
						+"		where t.receive_psid = ? and t.repair_status = "+transort_status+" order by tl.repair_date "
						+"		)as ttl group by repair_order_id "
						+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			DBRow para  = new DBRow();
			para.add("recevie_psid",ps_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackReceiveRepairOrderByPsid error:"+e);
		}
	}
	
	/**
	 * 获得转运单体积(因Spring容器生成顺序不得不写floor层)
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public float getRepairOrderVolume(long repair_order_id)
		throws Exception
	{
		DBRow repair = this.getDetailRepairOrderById(repair_order_id);
		String colum = "repair_total_count";
		if(repair.get("repair_status",0)==RepairOrderKey.FINISH)
		{
			colum = "repair_reap_count";
		}
		
		DBRow[] rows = this.getRepairOrderDetailByRepairId(repair_order_id, null, null, null, null);
		
		float volume = 0; 
		for (int i = 0; i < rows.length; i++) 
		{
			volume += rows[i].get("repair_volume",0f)*rows[i].get(colum,0f);
		}
		
		return MoneyUtil.round(volume,2);
	}
	
	/**
	 * 根据转运单ID获得转运单
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailRepairOrderById(long repair_order_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("repair_order")+" where repair_order_id = ?";
			
			DBRow para = new DBRow();
			para.add("repair_order_id",repair_order_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getDetailRepairOrderById error:"+e);
		}
	}
	
	/**
	 * 根据转运明细ID获得转运明细
	 * @param repair_detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getRepairDetailById(long repair_detail_id)
		throws Exception
	{
		try
		{
			String sql = "select * from  "+ConfigBean.getStringValue("repair_order_detail")+" as td left join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = td.repair_pc_id where td.repair_detail_id = ?";
			
			DBRow para = new DBRow();
			para.add("repair_detail_id",repair_detail_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairDetailById error:"+e);
		}
	}
	
	/**
	 * 根据转运单号查询转运单明细
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRepairOrderDetailByRepairId(long repair_order_id,PageCtrl pc,String sidx,String sord,FilterBean fillterBean)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from  "+ConfigBean.getStringValue("repair_order_detail")+" as td " 
											   +"left join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = td.repair_pc_id " 
											   +"left join "+ConfigBean.getStringValue("product_code")+" as pcode on pcode.pc_id = p.pc_id and pcode.code_type="+CodeTypeKey.Main+" "
											   +"where td.repair_order_id = ?");
			
			DBRow para = new DBRow();
			para.add("repair_order_id",repair_order_id);
			
			if(fillterBean !=null)
			{
				sql.append(new JqGridFilterKey().filterSQL(fillterBean));
			}
			
			if(sidx==null||sord==null)
			{
				sql.append(" order by repair_order_id desc");
			}
			else
			{
				sql.append(" order by "+sidx+" "+sord);
			}
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(), para,pc));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairOrderDetailByRepairId error:"+e);
		}
	}
	
	/**
	 * 根据转运单号查询转运单明细
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRepairOrderDetailByRepairId(long repair_order_id)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from  "+ConfigBean.getStringValue("repair_order_detail")+" as td " 
											   +"left join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = td.repair_pc_id " 
											   +"left join "+ConfigBean.getStringValue("product_code")+" as pcode on pcode.pc_id = p.pc_id and pcode.code_type="+CodeTypeKey.Main+" "
											   +"where td.repair_order_id = "+repair_order_id);
			
			return (dbUtilAutoTran.selectMutliple(sql.toString()));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairOrderDetailByRepairId error:"+e);
		}
	}
	
	/**
	 * 获得转运单的总重量
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public float getRepairWeight(long repair_order_id)
		throws Exception
	{
		try 
		{
			DBRow repair = this.getDetailRepairOrderById(repair_order_id);
			
			String colum = "repair_total_count";
			if(repair.get("repair_status",0)==RepairOrderKey.FINISH)
			{
				colum = "repair_reap_count";
			}
			
			DBRow[] repairDetails = this.getRepairOrderDetailByRepairId(repair_order_id, null, null, null, null);
			
			float weight = 0;
			for (int i = 0; i < repairDetails.length; i++) 
			{
				weight += repairDetails[i].get("repair_weight",0f)*repairDetails[i].get(colum,0f);
			}
			
			return MoneyUtil.round(weight,2);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairWeight error:"+e);
		}
	}
	
	/**
	 * 获得所有转运单出库金额
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public double getRepairSendPrice(long repair_order_id)
		throws Exception
	{
		try 
		{
			DBRow[] repairDetails = this.getRepairOrderDetailByRepairId(repair_order_id, null, null, null, null);
			
			double sendPrice = 0;
			for (int i = 0; i < repairDetails.length; i++) 
			{
				sendPrice += repairDetails[i].get("repair_send_price",0f)*repairDetails[i].get("repair_delivery_count",0f);
			}
			
			return MoneyUtil.round(sendPrice,2);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairSendPrice error:"+e);
		}
	}
	
	/**
	 * 修改转运单详细
	 * @param repair_detail_id
	 * @param para
	 * @throws Exception
	 */
	public void modRepairDetail(long repair_detail_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where repair_detail_id="+repair_detail_id,ConfigBean.getStringValue("repair_order_detail"),para);
		}
		catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.modRepairDetail error:"+e);
		}
	}
	
	/**
	 * 修改转运单
	 * @param repair_order_id
	 * @param para
	 * @throws Exception
	 */
	public void modRepairOrder(long repair_order_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where repair_order_id="+repair_order_id,ConfigBean.getStringValue("repair_order"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.modRepairOrder error:"+e);
		}
	}
	
	public void addRepairOrderLog(DBRow row) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("repair_order_logs");
			dbUtilAutoTran.insert(tableName, row);
		}catch(Exception e){
			throw new Exception("FloorRepairOrderMgrZyj.addRepairOrderLog(row):"+e);
		}
	}
	
	
	public void insertLogs(long repair_order_id ,String repair_content , long adid ,String employe_name, int repair_type , String timeComplete ,int stage) throws Exception{
		try{
			
			TDate tDate = new TDate();
			String currentTime = tDate.formatDate("yyyy-MM-dd HH:mm:ss");
			DBRow row = new DBRow();
			row.add("repair_order_id", repair_order_id);
			row.add("repair_content", repair_content);
			row.add("repairer_id", adid);
			row.add("repairer", employe_name);
			row.add("repair_type", repair_type);
			if(timeComplete != null && timeComplete.trim().length()> 0){
				row.add("time_complete", timeComplete);
			}
			row.add("activity_id", stage);
			row.add("repair_date", currentTime);
			
			this.addRepairOrderLog(row);
			
		}catch (Exception e) {
			throw new Exception("FloorRepairOrderMgrZyj.insertLogs():"+e);
		}
	}
	
	/**
	 * 获得所有交货单
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllDeliveryOrder(PageCtrl pc)
		throws Exception
	{
		String sql = "select do.*,psc.title as title from "+ConfigBean.getStringValue("repair_order")+" as do,"+ConfigBean.getStringValue("purchase")+" as p,"+ConfigBean.getStringValue("product_storage_catalog")+" as psc where p.purchase_id = do.delivery_purchase_id and psc.id = do.delivery_psid order by delivery_order_id desc";
		
		return (dbUtilAutoTran.selectMutliple(sql,pc));
	}
	
	/**
	 * 根据采购单号查询对应的所有运单
	 * @param purchase_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDelveryOrdersByPurchaseId(long purchase_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("delivery_order")+" where delivery_purchase_id =?";
			
			DBRow para = new DBRow();
			para.add("delvery_purchase_id",purchase_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorDelveryMgrZJ getDelveryOrdersByPurchaseId error:"+e);
		}
	}
	
	/**
	 * 生成交货单
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addDelveryOrder(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long delivery_order_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("delivery_order"));
			
			dbrow.add("delivery_order_id",delivery_order_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("delivery_order"),dbrow);
			
			return (delivery_order_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ addDelveryOrder error:"+e);
		}
	}
	
	/**
	 * 添加交货详细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addDeliveryOrderDetail(DBRow dbrow)
		throws Exception
	{
		try {
			long delivery_order_detail_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("delivery_order_detail"));
			
			dbrow.add("delivery_order_detail_id",delivery_order_detail_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("delivery_order_detail"),dbrow);
			
			return (delivery_order_detail_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDelveryMgrZJ addDeliveryOrderDetail error:"+e);
		}
	}
	
	public DBRow[] getAnalysis(String st, String en, int analysisType, int analysisStatus, int day, PageCtrl pc) throws Exception {
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("repair_order")+" where 1=1");
			
			if(!st.equals("")) {
				sql.append(" and repair_date >'"+st+"'");
			}
			
			if(!en.equals("")) {
				sql.append(" and repair_date <='"+en+"'");
			}
			
			if(analysisType == 1) {//报关 
				if(analysisStatus==1) {
					sql.append(" and declaration_over is null");
					sql.append(" and now() > ADDDATE(repair_date,INTERVAL "+day+" DAY)");
					sql.append(" and declaration in (2,3)");
				}
				else {
					sql.append(" and declaration_over is not null");
					sql.append(" and declaration_over>"+day);
				}
			}
			else if(analysisType == 2) {
				if(analysisStatus==1) {
					sql.append(" and clearance_over is null");
					sql.append(" and now() > ADDDATE(repair_date,INTERVAL "+day+" DAY)");
					sql.append(" and clearance in (2,3)");
				}
				else {
					sql.append(" and clearance_over is not null");
					sql.append(" and clearance_over>"+day);
				}
			}
			else if(analysisType == 3) {
				if(analysisStatus==1) {
					sql.append(" and drawback_over is null");
					sql.append(" and now() > ADDDATE(repair_date,INTERVAL "+day+" DAY)");
					sql.append(" and drawback in (2,3)");
				}
				else {
					sql.append(" and drawback_over is not null");
					sql.append(" and drawback_over>"+day);
				}
			}
			else if(analysisType == 4) {
				if(analysisStatus==1) {
					sql.append(" and invoice_over is null");
					sql.append(" and now() > ADDDATE(repair_date,INTERVAL "+day+" DAY)");
					sql.append(" and invoice in (2,3)");
				}
				else {
					sql.append(" and invoice_over is not null");
					sql.append(" and invoice_over>"+day);
				}
			}
			else if(analysisType == 5) {
				if(analysisStatus==1) {
					sql.append(" and all_over is null");
					sql.append(" and now() > ADDDATE(repair_date,INTERVAL "+day+" DAY)");
				}	
				else {
					sql.append(" and all_over is not null");
					sql.append(" and all_over>"+day);
				}
			}
			
			sql.append(" order by repair_order_id desc");
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj getAnalysis error:"+e);
		}
		
	}
	
	
	public DBRow getCountyById(String id) throws Exception {
		try 
		{
			String sql = "select * from country_code where ccid = " + id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj getCountyById error:"+e);
		}
	}
	
	public DBRow[] getAllAssetsCategory() throws Exception
	{
		try 
		{
			String sql = "select * from assets_category";
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj getCountyById error:"+e);
		}
	}
	
	public DBRow[] getAllAdminGroup() throws Exception
	{
		try 
		{
			String sql = "select * from admin_group";
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj getCountyById error:"+e);
		}
	}
	
	public DBRow[] getAllProductLineDefine() throws Exception
	{
		try 
		{
			String sql = "select * from product_line_define";
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj getCountyById error:"+e);
		}
	}
	
	public DBRow[] getAccountView() throws Exception
	{
		try 
		{
			String sql = "select * from account_view";
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj getAccountView error:"+e);
		}
	}
	
	public DBRow[] getRepairOrderLogs(long repair_order_id , int number ) throws Exception 
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from ").append("repair_order_logs ").append(" where repair_order_id =").append(repair_order_id).append(" order by logs_id desc limit 0,4;");
			return dbUtilAutoTran.selectMutliple(sql.toString()) ;
		}catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairOrderLogs(repair_order_id ,number):"+e);
		}
	}
	/**
	 * 根据当前登录者的Id，获取其上次创建的交货单的ID
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow getRepairLastTimeCreateByAdid(long adid) throws Exception
	{
		try 
		{
			String sql = "select repair_order_id from repair_order where create_account_id = "+adid+" ORDER BY repair_order_id desc LIMIT 0,1";
			return dbUtilAutoTran.selectSingle(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairLastTimeCreateByAdid():"+e);
		}
	}
	/**
	 * 添加转运单
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addRepairOrder(DBRow dbrow)
		throws Exception
	{
		try
		{
			String tableName = ConfigBean.getStringValue("repair_order");
			return dbUtilAutoTran.insertReturnId(tableName, dbrow);
//			long repair_order_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("transport"));
//			dbrow.add("repair_order_id",repair_order_id);
//			dbUtilAutoTran.insert(ConfigBean.getStringValue("transport"),dbrow);
//			return repair_order_id;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.addRepairOrder error:"+e);
		}
	}
	
	/**
	 * 删除转运单明细(根据转运单号批量删除)
	 * @param repair_order_id
	 * @throws Exception
	 */
	public void delRepairDetailByRepairId(long repair_order_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where repair_order_id="+repair_order_id,ConfigBean.getStringValue("repair_order_detail"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.delRepairDetailByRepairId error:"+e);
		}
	}
	
	/**
	 * 添加转运单详细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addRepairOrderDetail(DBRow dbrow)
		throws Exception
	{
		try 
		{
			String tablename = ConfigBean.getStringValue("repair_order_detail");
			return dbUtilAutoTran.insertReturnId(tablename, dbrow);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.addRepairOrderDetail error:"+e);
		}
	}
	
	/**
	 * 查看某一具体的供应商信息
	 * @param sid
	 * @throws Exception 
	 */
	public DBRow getDetailSupplier(long sid) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("supplier")+" where id="+sid;
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailSupplier(sid) error: " + e);
		}
	}
	
	/**
	 * 删除转运单的运费项目
	 * @param repair_order_id
	 * @throws Exception
	 */
	public void deleteRepairOrderFreightCostByRepairId(String repair_order_id) throws Exception {
		try
		{
			String cond = "where repair_order_id = " + repair_order_id;
			dbUtilAutoTran.delete(cond, ConfigBean.getStringValue("repair_freight_cost"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.deleteRepairOrderFreightCostByRepairId(repair_order_id) error: " + e);
		}
	}
	
	public void insertRepairOrderFreightCost(DBRow row) throws Exception
	{
		try 
		{
			dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("repair_freight_cost"), row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.insertRepairOrderFreightCost() error: " + e);
		}
	}
	
	public void updateRepairOrderFreightCost(DBRow row,long id) throws Exception
	{
		try 
		{
			String cond = "where tfc_id = " + id;
			dbUtilAutoTran.update(cond, ConfigBean.getStringValue("repair_freight_cost"), row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.updateRepairOrderFreightCost() error: " + e);
		}
	}
	
	public DBRow[] getRepairLogsByRepairIdAndType(long repair, int[] types) throws Exception{
		try{
			String sql = "select * from repair_order_logs";
			if(null != types && types.length > 0){
				if(1 == types.length){
					sql += " where repair_type = " + types[0];
				}else{
					String sqlWhere = " where ( repair_type = " + types[0];
					for(int i = 1; i < types.length; i ++){
						sqlWhere += " or repair_type = " + types[i];
					}
					sqlWhere += ")";
					sql += sqlWhere;
				}
				sql += " and repair_order_id = " + repair;
			}else{
				sql += " where repair_order_id = " + repair;
			}
			sql += " order by repair_type";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorRepairOrderMgrZyj.getRepairLogsByRepairIdAndType(long repair, int[] types):"+e);
		}	
	}
	
	public int getIndexOfFilebyFileName(String fileName) throws Exception
	{
		try
		{
			String tableName = "file";
			StringBuffer sql = new StringBuffer("select * from ").append(tableName).append(" where file_name like '")
			.append(fileName).append("%' order by file_id desc limit 0,1");
			DBRow fileRow = dbUtilAutoTran.selectSingle(sql.toString());
			if(fileRow == null)
			{	
				return 0;
			}else
			{
				String name = fileRow.getString("file_name");
				String[] tempArray = name.split("_");
				int index = 1 ;
				String stringIndex = tempArray[tempArray.length -1];
				if(stringIndex.indexOf(".") != -1){
					stringIndex = stringIndex.substring(0, stringIndex.indexOf(".") );
				}
				try
				{
					index = Integer.parseInt(stringIndex) + 1;
				}catch (Exception e) {
				}finally{}
				return index ;
			}	
		}catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZr.getIndexOfFilebyFileName(fileName):"+e);
		}
	}
	
	public DBRow getFirstTransportIngLogTime(long repair_order_id , int repair_type , int activity_id ) throws Exception {
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from repair_order_logs").append(" where repair_order_id=").append(repair_order_id).append(" and repair_type=").append(repair_type)
			.append(" and activity_id=").append(activity_id).append(" order by logs_id limit 0,1;");
			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.getFirstTransportClearanceLogTime(repair_order_id ,transport_type ,activity_id ):"+e);
		}
	}
	
	/**
	 * 插入到File表中的数据不是重复的数据。先查看有无。有的话那么就是更新。没有的话就是删除
	 * @param fileRow
	 * @throws Exception
	 */
	public void addFileNotExits(DBRow fileRow) throws Exception {
		try{
				// 更新
				//插入
				this.addTransportCertificateFile(fileRow);
		 
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.addFileNotExits(fileRow):"+e);
		}
	}
	
	public long addTransportCertificateFile(DBRow row) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("file");
			 return dbUtilAutoTran.insertReturnId(tableName, row);
		}catch(Exception e){
			throw new Exception("FloorTransportMgrZr.addTransportCertificateFile(row):"+e);
		}
	}
	
	/**
	 * 删除转运单
	 * @param repair_order_id
	 * @throws Exception
	 */
	public void delRepairOrder(long repair_order_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where repair_order_id="+repair_order_id,ConfigBean.getStringValue("repair_order"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.delRepairOrder error:"+e);
		}
	}

	/**
	 * 通过转运单ID，删除转运单的日志
	 * @param repairId
	 * @throws Exception
	 */
	public void deleteRepairLogsByRepairId(long repairId) throws Exception
	{
		try 
		{
			String sql = " where repair_order_id = " + repairId;
			dbUtilAutoTran.delete(sql, ConfigBean.getStringValue("repair_order_logs"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.deleteRepairLogsByRepairId(long repairId)"+e);
		}
	}
	
	/**
	 * 删除转运单的运费项目
	 * @param repairId
	 * @throws Exception
	 */
	public void deleteRepairFreightByRepairId(long repairId) throws Exception
	{
		try 
		{
			String sql = " where repair_order_id = " + repairId;
			dbUtilAutoTran.delete(sql, ConfigBean.getStringValue("repair_freight_cost"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.deleteRepairFreightByRepairId(long repairId)"+e);
		}
	}
	
	/**
	 * 根据转运单ID，文件的类型数组，删除文件
	 * @param repairId
	 * @param types
	 * @throws Exception
	 */
	public void deleteTransportFileByTransportIdAndTypes(long repairId, int[] types) throws Exception
	{
		try 
		{
			String sql = " where file_with_id = " + repairId;
			if(null != types && types.length > 0)
			{
				String sqlWhere = "";
				if(1 == types.length)
				{
					sqlWhere = " and file_with_type = " + types[0];
				}
				else
				{
					sqlWhere = " and (file_with_type = " + types[0];
					for (int i = 1; i < types.length; i++) {
						sqlWhere += " or file_with_type = " + types[i];
					}
					sqlWhere += ")";
				}
				sql += sqlWhere;
			}
			dbUtilAutoTran.delete(sql, ConfigBean.getStringValue("file"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.deleteTransportLogsByTransportId(long transportId, int[] types)"+e);
		}
	}
	
	/**
	 * 根据转运单ID，类型删除转运单下商品的相关文件
	 * @parrepairortId
	 * @param types
	 * @throws Exception
	 */
	public void deleteTransportProductFileByTransportIdAndTypes(long transportId, int[] types) throws Exception
	{
		try
		{
			String sql = " where file_with_id = " + transportId;
			if(null != types && types.length > 0)
			{
				String sqlWhere = "";
				if(1 == types.length)
				{
					sqlWhere = " and file_with_type = " + types[0];
				}
				else
				{
					sqlWhere = " and (file_with_type = " + types[0];
					for (int i = 1; i < types.length; i++) {
						sqlWhere += " or file_with_type = " + types[i];
					}
					sqlWhere += ")";
				}
				sql += sqlWhere;
			}
			dbUtilAutoTran.delete(sql, ConfigBean.getStringValue("product_file"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.deleteTransportProductFileByTransportIdAndTypes(long transportId, int[] types)"+e);
		}
	}
	
	/**
	 * 超期未跟进的尚备货中的转运单
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackReadyRepairCountByPsid(long ps_id,int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(repair_order_id) as need_track_count from " 
						+"( "
						+"	select * from("
						+"		select t.repair_order_id,t.purchase_id,tl.repair_date from repair_order t "
						+"		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.Goods+" and tl.activity_id ="+RepairOrderKey.READY+" "
						+"		where t.purchase_id = 0 and t.send_psid = ? and (t.repair_status = "+RepairOrderKey.READY+") order by tl.repair_date "
						+"		)as ttl group by repair_order_id "
						+")need_track where date_add(repair_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow para = new DBRow();
			para.add("send_psid",ps_id);
			DBRow row = dbUtilAutoTran.selectPreSingle(sql,para);
			
			int need_track_count = row.get("need_track_count",0);
			return need_track_count;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackReadyRepairCountByPsid error:"+e);
		}
	}
	
	/**
	 * 获得需跟进装货中转运单数量
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackPackingRepairCountByPsid(long ps_id,int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(repair_order_id) as need_track_count from " 
						+"( "
						+"	select * from("
						+"		select t.repair_order_id,t.purchase_id,tl.repair_date from repair_order t "
						+"		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.Goods+" and tl.activity_id ="+RepairOrderKey.PACKING+" "
						+"		where t.purchase_id = 0 and t.send_psid = "+ps_id+" and (t.repair_status="+RepairOrderKey.PACKING+") order by tl.repair_date "
						+"		)as ttl group by repair_order_id "
						+")need_track where date_add(repair_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			
			int need_track_count = row.get("need_track_count",0);
			return need_track_count;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackPackingRepairCountByPsid error:"+e);
		}
	}
	/**
	 * 需跟进制签转运单数量
	 * @param send_psid
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackTagCountByPS(long send_psid,int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(repair_order_id) as need_track_tag_count from" 
					+"( "
					+"	select * from" 
					+"	( "
					+"		select t.repair_order_id,t.purchase_id,tl.repair_date as last_track_date,t.send_psid from repair_order t "
					+"		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.Label+" and tl.activity_id ="+RepairTagKey.TAG+" "
					+"		join product_storage_catalog psc on t.send_psid = psc.id "
					+"		where t.purchase_id = 0 and t.send_psid = ? and t.tag = "+RepairTagKey.TAG+" order by tl.repair_date "
					+"		)as ttl group by repair_order_id "
					+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow para = new DBRow();
			para.add("send_psid",send_psid);
			
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			
			int need_track_tag_count = result.get("need_track_tag_count",0);
			return need_track_tag_count;
			
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackTagCountByPS error:"+e);
		}
	}
	
	/**
	 * 需跟进制签转运单
	 * @param send_psid
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackTagRepairOrderByPs(long send_psid,int track_day,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from" 
				+"( "
				+"	select * from" 
				+"	( "
				+"  	select t.*,tl.repair_date as last_track_date from repair_order t "
				+" 		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.Label+" and tl.activity_id ="+RepairTagKey.TAG+" "
				+"		join product_storage_catalog psc on t.send_psid = psc.id "
				+"		where t.purchase_id = 0 and t.send_psid = ? and t.tag = "+RepairTagKey.TAG+" order by tl.repair_date "
				+"	)as ttl group by repair_order_id "
				+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
	
			DBRow para = new DBRow();
			para.add("send_psid",send_psid);
			return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackTagRepairOrderByPs error:"+e);
		}
	}
	
	/**
	 * 需跟进实物图片转运单数量
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackProductFileCountByPS(long send_psid,int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(repair_order_id) as need_track_product_file_count from" 
						+"( "
						+" 		select * from" 
						+"		( "
						+"		select t.repair_order_id,t.purchase_id,tl.repair_date as last_track_date,t.send_psid from repair_order t "
						+"		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.ProductShot+" and tl.activity_id ="+RepairProductFileKey.PRODUCTFILE+" "
						+"		join product_storage_catalog psc on t.send_psid = psc.id "
						+"		where t.purchase_id = 0 and t.send_psid = ? and t.product_file = "+RepairProductFileKey.PRODUCTFILE+" order by tl.repair_date "
						+"		)as ttl group by repair_order_id "
						+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE "; 
			DBRow para = new DBRow();
			para.add("send_psid",send_psid);
			
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			int need_track_product_file_count = result.get("need_track_product_file_count",0);
			return need_track_product_file_count;
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackProductFileCountByPS error:"+e);
		}
	}
	/**
	 * 需跟进实物图片交货单
	 * @param send_psid
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackProductFileRepairOrder(long send_psid,int track_day,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from" 
				+"( "
				+"	select * from" 
				+"	( "
				+"  	select t.*,tl.repair_date as last_track_date from repair_order t "
				+" 		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.ProductShot+" and tl.activity_id ="+RepairProductFileKey.PRODUCTFILE+" "
				+"		join product_storage_catalog psc on t.send_psid = psc.id "
				+"		where t.purchase_id = 0 and t.send_psid = ? and t.product_file = "+RepairProductFileKey.PRODUCTFILE+" order by tl.repair_date "
				+"	)as ttl group by repair_order_id "
				+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
	
			DBRow para = new DBRow();
			para.add("send_psid",send_psid);
			return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackProductFileRepairOrder error:"+e);
		}
		
	}
	
	/**
	 * 需跟进质检转运单数量
	 * @param send_psid
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackQualityInspectionCountByPS(long send_psid,int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(repair_order_id) as need_track_quality_inspection_count from" 
						+"( "
						+" 	select * from" 
						+"	( "
						+"		select t.repair_order_id,t.purchase_id,tl.repair_date as last_track_date,t.send_psid from repair_order t "
						+"		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.QualityControl+" and tl.activity_id ="+RepairQualityInspectionKey.NEED_QUALITY+" "
						+"		join product_storage_catalog psc on t.send_psid = psc.id "
						+"		where t.purchase_id = 0 and t.send_psid = ? and t.quality_inspection = "+RepairQualityInspectionKey.NEED_QUALITY+" order by tl.repair_date "
						+"	)as ttl group by repair_order_id "
						+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE "; 
			
			DBRow para = new DBRow();
			para.add("send_psid",send_psid);
			
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			int need_track_quality_inspection_count = result.get("need_track_quality_inspection_count",0);
			return need_track_quality_inspection_count;
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackQualityInspectionCountByPS error:"+e);
		}
	}
	
	/**
	 * 需跟进质检转运单
	 * @param send_psid
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackQualityInspectionRepairOrder(long send_psid,int track_day,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from" 
						+"( "
						+"	select * from" 
						+"	( "
						+"  	select t.*,tl.repair_date as last_track_date from repair_order t "
						+" 		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.QualityControl+" and tl.activity_id ="+RepairQualityInspectionKey.NEED_QUALITY+" "
						+"		join product_storage_catalog psc on t.send_psid = psc.id "
						+"		where t.purchase_id = 0 and t.send_psid = ? and t.quality_inspection = "+RepairQualityInspectionKey.NEED_QUALITY+" order by tl.repair_date "
						+"	)as ttl group by repair_order_id "
						+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow para = new DBRow();
			para.add("send_psid",send_psid);
			
			return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackQualityInspectionRepairOrder error:"+e);
		}
	}
	
	/**
	 * 需跟进运输中的交货、转运单数量
	 * @param ps_id
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackIntransitRepairCountByPsid(long ps_id,int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(repair_order_id) as need_track_count from " 
						+"( "
						+"	select * from" 
						+"		("
						+"		select t.repair_order_id,t.purchase_id,tl.repair_date from repair_order t "
						+"		left join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.Goods+" and tl.activity_id ="+RepairOrderKey.INTRANSIT+" "
						+"		where t.receive_psid = ? and t.repair_status = "+RepairOrderKey.INTRANSIT+" order by tl.repair_date "
						+"		)as ttl group by repair_order_id "
						+")need_track where date_add(repair_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			DBRow para  = new DBRow();
			para.add("receive_psid",ps_id);
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql,para);
				
			int need_track_count = row.get("need_track_count",0);
			return need_track_count;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackReadyTransportCount error:"+e);
		}
	}
	/**
	 * 需跟进已收货的交货、转运单数量
	 * @param ps_id
	 * @param track_hour
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackAlreadyReciveRepairCount(long ps_id,int track_hour)
		throws Exception
	{
		try 
		{
			String sql = "select count(repair_order_id) as need_track_count from " 
						+"( "
						+"	select * from" 
						+"		("
						+"		select t.repair_order_id,t.purchase_id,tl.repair_date from repair_order t "
						+"		left join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.Goods+" and tl.activity_id ="+RepairOrderKey.AlREADYRECEIVE+" "
						+"		where t.receive_psid = ? and t.repair_status = "+RepairOrderKey.AlREADYRECEIVE+" order by tl.repair_date "
						+"		)as ttl group by repair_order_id "
						+")need_track where date_add(repair_date,INTERVAL "+track_hour+" hour)<= CURRENT_TIMESTAMP ";
			DBRow para = new DBRow();
			para.add("receive_psid",ps_id);
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql,para);
				
			int need_track_count = row.get("need_track_count",0);
			return need_track_count;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackAlreadyReciveRepairCount error:"+e);
		}
	}
	
	/**
	 * 需跟进单证的数量
	 */
	public int getNeedTrackCertificateCount(int track_day)
		throws Exception
	{
		try {
			String sql = "select count(repair_order_id) as need_track_certificate_count from " 
				+"( "
				+"	select * from" 
				+"		("
				+"		select t.repair_order_id,tl.repair_date as last_track_date from repair_order t "
				+"		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.Document+" and tl.activity_id ="+RepairCertificateKey.CERTIFICATE+" "
				+"		where t.certificate = "+RepairCertificateKey.CERTIFICATE+" order by tl.repair_date "
				+"		)as ttl group by repair_order_id "
				+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			
			int need_track_certificate_count = result.get("need_track_certificate_count",0);
			
			return need_track_certificate_count;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackCertificateCount error:"+e);
		}
	}
	/**
	 * 需跟进清关的数量
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackClearanceCount(int track_day)
		throws Exception
	{
		try {
			String sql = "select count(repair_order_id) as need_track_clearance_count from " 
				+"( "
				+"	select * from" 
				+"		("
				+"		select t.repair_order_id,tl.repair_date as last_track_date from repair_order t "
				+"		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.ImportCustoms+" and tl.activity_id ="+RepairClearanceKey.CLEARANCEING+" "
				+"		where t.clearance = "+RepairClearanceKey.CLEARANCEING+" order by tl.repair_date "
				+"		)as ttl group by repair_order_id "
				+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			
			int need_track_clearance_count = result.get("need_track_clearance_count",0);
			
			return need_track_clearance_count;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackClearanceCount error:"+e);
		}
	}
	
	/**
	 * 需跟进的报关数量
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackDeclarationCount(int track_day)
		throws Exception
	{
		try {
			String sql = "select count(repair_order_id) as need_track_declaration_count from " 
				+"( "
				+"	select * from" 
				+"		("
				+"		select t.repair_order_id,tl.repair_date as last_track_date from repair_order t "
				+"		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.ExportCustoms+" and tl.activity_id ="+RepairDeclarationKey.DELARATING+" "
				+"		where t.declaration = "+RepairDeclarationKey.DELARATING+" order by tl.repair_date "
				+"		)as ttl group by repair_order_id "
				+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			
			int need_track_declaration_count = result.get("need_track_declaration_count",0);
			
			return need_track_declaration_count;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackDeclarationCount error:"+e);
		}
	}
	
	
	/**
	 * 获得超期未跟进的备货中的交货单数量按产品线分组
	 * @param ps_id
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackReadDeliveryCountGroupByProductLine(int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select product_line_name,product_line_id,count(repair_order_id) as readyneedtrackcountforline from "
				+"( "
				+"	select * from" 
				+"	( "
				+" 		select * from" 
				+"		( "
				+"  	 	select t.repair_order_id,t.purchase_id,tl.repair_date,p.product_line_id,pld.`name` AS product_line_name from repair_order t "
				+" 		 	join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.Goods+" and tl.activity_id ="+RepairOrderKey.READY+" "
				+"		 	join purchase p on p.purchase_id = t.purchase_id "
				+"		 	left join product_line_define pld ON p.product_line_id = pld.id "
				+"		 	where t.purchase_id != 0 and t.repair_status = "+RepairOrderKey.READY+" order by tl.repair_date "
				+"		)as ttl group by repair_order_id "
				+"	)need_track where DATE_ADD(repair_date,INTERVAL "+track_day+" day)<= CURRENT_DATE " 
				+")as groupbyProductLine group by product_line_id ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackDeclarationCount error:"+e);
		}
	}
	/**
	 * 需跟进实物图片交货单产品线分组
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackProductFileCountGroupByProductLine(int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select product_line_name,product_line_id,count(repair_order_id) as need_track_product_file_count from "
				+"( "
				+"	select * from" 
				+"	( "
				+" 		select * from" 
				+"		( "
				+"		select t.repair_order_id,t.purchase_id,tl.repair_date as last_track_date,p.product_line_id,pld.`name` AS product_line_name from repair_order t "
				+"		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.ProductShot+" and tl.activity_id ="+RepairProductFileKey.PRODUCTFILE+" "
				+"		join purchase p on p.purchase_id = t.purchase_id "
				+"		left join product_line_define pld ON p.product_line_id = pld.id "
				+"		where t.purchase_id != 0 and t.product_file = "+RepairProductFileKey.PRODUCTFILE+" order by tl.repair_date "
				+"		)as ttl group by repair_order_id "
				+"	)need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE " 
				+")as groupbyProductLine group by product_line_id ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackProductFileCountGroupByProductLine error:"+e);
		}
	}
	
	/**
	 * 需跟进质检交货单产品线分组
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackQualityInspectionCountGroupByProductLine(int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select product_line_name,product_line_id,count(repair_order_id) as need_track_quality_inspection_count from "
				+"( "
				+"	select * from" 
				+"	( "
				+" 		select * from" 
				+"		( "
				+"		select t.repair_order_id,t.purchase_id,tl.repair_date as last_track_date,p.product_line_id,pld.`name` AS product_line_name from repair_order t "
				+"		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.QualityControl+" and tl.activity_id ="+RepairQualityInspectionKey.NEED_QUALITY+" "
				+"		join purchase p on p.purchase_id = t.purchase_id "
				+"		left join product_line_define pld ON p.product_line_id = pld.id "
				+"		where t.purchase_id != 0 and t.quality_inspection = "+RepairQualityInspectionKey.NEED_QUALITY+" order by tl.repair_date "
				+"		)as ttl group by repair_order_id "
				+"	)need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE " 
				+")as groupbyProductLine group by product_line_id ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackTagCountGroupByProductLine error:"+e);
		}
	}
	
	/**
	 * 需跟进制签交货单产品线分组
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackTagCountGroupByProductLine(int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select product_line_name,product_line_id,count(repair_order_id) as need_track_tag_count from "
				+"( "
				+"	select * from" 
				+"	( "
				+" 		select * from" 
				+"		( "
				+"		select t.repair_order_id,t.purchase_id,tl.repair_date as last_track_date,p.product_line_id,pld.`name` AS product_line_name from repair_order t "
				+"		join repair_order_logs tl on tl.repair_order_id = t.repair_order_id and tl.repair_type = "+RepairLogTypeKey.Label+" and tl.activity_id ="+RepairTagKey.TAG+" "
				+"		join purchase p on p.purchase_id = t.purchase_id "
				+"		left join product_line_define pld ON p.product_line_id = pld.id "
				+"		where t.purchase_id != 0 and t.tag = "+RepairTagKey.TAG+" order by tl.repair_date "
				+"		)as ttl group by repair_order_id "
				+"	)need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE " 
				+")as groupbyProductLine group by product_line_id ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getNeedTrackTagCountGroupByProductLine error:"+e);
		}
	}
	
	public DBRow[] getRepairLogsByType(long repair_order_id, int repair_type) throws Exception {
		DBRow para = new DBRow();
		para.add("repair_order_id", repair_order_id);
		if(repair_type != 0)
			para.add("repair_type", repair_type);
		return this.getRowsByPara(para, null, null, "logs_id desc", null);

	}
	
	/**
	 * @TODO 表查询
	 * @param para 表查询参数
	 * @param operator >,<,=,<>
	 * @param orAnd and,or
	 * @param orderBy order by PK desc ...
	 * @param pc 分页
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRowsByPara(DBRow para, DBRow operator,DBRow orAnd, String orderBy,PageCtrl pc) throws Exception {
		String sql = "";
		
		operator = operator==null?new DBRow():operator;
		orAnd = orAnd==null?new DBRow():orAnd;
		
		DBRow row = new DBRow();
		sql = "select * from repair_order_logs";
		int t = 0;
		for(int i=0;para!=null && i<para.getFieldNames().size();i++){
			String rowName = para.getFieldNames().get(i).toString();
			String rowValue = para.getValue(rowName)==null?"":para.getValue(rowName).toString();
			
			String operatorValue = operator.getValue(rowName)==null?"=":operator.getValue(rowName).toString();
			String orAndValue = orAnd.getValue(rowName)==null?"and":orAnd.getValue(rowName).toString();
			
			if(i==0 && orAndValue.equals("or"))
				sql += " where 1=2";
			else if(i==0 && orAndValue.equals("and"))
				sql += " where 1=1";
			
			if(!rowValue.equals("")) {
				sql += " " + orAndValue + " " + rowName + " " + operatorValue + " ?";
				row.add(rowName, rowValue);
				t++;
			}
		}
		
		if(!orderBy.equals("")) {
			sql += " order by "+orderBy;
		}
		
		if(pc!=null)
			if(t==0)
				return dbUtilAutoTran.selectMutliple(sql, pc);
			else
				return dbUtilAutoTran.selectPreMutliple(sql,row,pc);
		else
			if(t==0)
				return dbUtilAutoTran.selectMutliple(sql);
			else
				return dbUtilAutoTran.selectPreMutliple(sql, row);
	}
	
	public DBRow[] getProductFileByWithIdAndWithTypeFileType(long file_with_id,
			int file_with_type, int product_file_type, PageCtrl pc) throws Exception {
		try{
			StringBuffer sql = new StringBuffer("select product_file.*,product.p_name,admin.employe_name from product_file ,product,admin")	
			.append(" where product.pc_id = product_file.pc_id  and  admin.adid = product_file.upload_adid and product_file_type=").append(product_file_type)
			.append(" and file_with_type=").append(file_with_type).append(" and file_with_id=").append(file_with_id)
			.append(" order by product_file.pc_id");
			return dbUtilAutoTran.selectMutliple(sql.toString(),pc);
		}catch (Exception e) {
			throw new Exception("FloorRepairOrderMgrZyj.getProductFileByWithIdAndWithTypeFileType(file_with_id ,file_with_type ,product_file_type,pc ):"+e);
		}
	}
	
	public DBRow getSumRepairFreightCost(String repair_order_id) throws Exception {
		try 
		{
			String sql = "select sum(tfc_unit_price*tfc_unit_count*tfc_exchange_rate) AS sum_price from repair_freight_cost where repair_order_id = "+repair_order_id;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getSumRepairFreightCost():"+e);
		}
	}
	
	public DBRow[] getRepairFreightCostByRepairId(String repair_order_id) throws Exception{
		try
		{
			String sql = "select * from repair_freight_cost where repair_order_id = "+repair_order_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairFreightCostByRepairId():"+e);
		}
	}
	/**
	 * 获得采购单在途中订购数
	 * @param purchase_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public float getTransitDeliverCountForPurchase(long purchase_id,long pc_id)
		throws Exception
	{
		try {
			String sql = "select sum(td.repair_delivery_count) as transitCount from "+ConfigBean.getStringValue("repair_order")+" as t "
			+"join "+ConfigBean.getStringValue("repair_order_detail")+" as td on td.repair_order_id "
			+"where t.purchase_id = ? and (t.repair_status ="+RepairOrderKey.INTRANSIT+" or t.repair_status = "+RepairOrderKey.AlREADYRECEIVE+" ) and td.repair_pc_id = ? ";
			
			DBRow para = new DBRow();
			para.add("purchase_id",purchase_id);
			para.add("pc_id",pc_id);
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql, para);
			
			return (row.get("transitCount",0f));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj getTransitDeliverCountForPurchase error:"+e);
		}
	}
	/**
	 * 获得采购单在途中的备件数
	 * @param purchase_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public float getTransitBackupCountForPurchase(long purchase_id,long pc_id)
		throws Exception
	{
		try {
			String sql = "select sum(td.repair_backup_count) as transitCount from "+ConfigBean.getStringValue("repair_order")+" as t "
			+"join "+ConfigBean.getStringValue("repair_order_detail")+" as td on td.repair_order_id "
			+"where t.purchase_id = ? and (t.repair_status ="+RepairOrderKey.INTRANSIT+" or t.repair_status = "+RepairOrderKey.AlREADYRECEIVE+" ) and td.repair_pc_id = ? ";
			
			DBRow para = new DBRow();
			para.add("purchase_id",purchase_id);
			para.add("pc_id",pc_id);
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql, para);
			
			return (row.get("transitCount",0f));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj getTransitDeliverCountForPurchase error:"+e);
		}
	}
	/**
	 * 返修单出库
	 * @param repair_order_id
	 * @param product_names
	 * @param stockIn_counts
	 * @throws Exception
	 */
	public void insertRepairOutbound(String repair_order_id, String[] product_names, String[] stockIn_counts) throws Exception {
		try
		{
			for (int i = 0; i < product_names.length; i++) 
			{
				DBRow product = floorProductMgr.getDetailProductByPname(product_names[i]);
				if(product ==null)
				{
					throw new Exception();
				}
				DBRow db = new DBRow();
				db.add("to_repair_order_id",repair_order_id);
				db.add("to_pc_id",product.get("pc_id",0l));
				db.add("to_p_name",product.getString("p_name"));
				db.add("to_p_code",product.getString("p_code"));
				db.add("to_count",stockIn_counts[i]);
				
				this.repairOutBound(db);
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj insertRepairOutbound error:"+e);
		}
	}
	public void repairOutBound(DBRow row) throws Exception
	{
		try 
		{
			dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("repair_outbound"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.repairOutBound error:"+e);
		}
	}
	
	/**
	 * 获得转运装箱明细
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportOutboundByTransportId(long repair_order_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("repair_outbound")+" where to_repair_order_id=?";
			
			DBRow para = new DBRow();
			para.add("repair_order_id",repair_order_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getTransportOutboundByTransportId error:"+e);
		}
	}
	
	
	/**
	 * 获得转运装箱明细
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRepairOutboundByRepairAndPcId(long repair_order_id, long pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("repair_outbound")+" where to_repair_order_id=? and to_pc_id=?";
			
			DBRow para = new DBRow();
			para.add("repair_order_id",repair_order_id);
			para.add("to_pc_id",pc_id);
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairOutboundByRepairAndPcId error:"+e);
		}
	}
	
	/**
	 * 通过返修单详细ID删除
	 * @param repair_detail_id
	 * @throws Exception
	 */
	public void deleteRepairOrderDetailByDetailId(long repair_detail_id) throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete(" where repair_detail_id = " + repair_detail_id, ConfigBean.getStringValue("repair_order_detail"));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.deleteRepairOrderDetailByDetailId error:"+e);
		}
	}
	/**
	 * 转运单明细起运操作
	 * @param repair_order_id
	 * @param pc_id
	 * @param para
	 * @throws Exception
	 */
	public int repairDetailDelivery(long repair_order_id,long pc_id,DBRow para)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.update("where repair_order_id="+repair_order_id+" and repair_pc_id="+pc_id,ConfigBean.getStringValue("repair_order_detail"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.repairDetailDelivery error:"+e);
		}
	}
	
	/**
	 * 通过返修单ID和商品获取返修详细
	 * @param repair_order_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getRepairDetailDeliveryByRepairAndPc(long repair_order_id, long pc_id) throws Exception
	{
		try 
		{
			String sql = "select * from repair_order_detail where repair_order_id = "+repair_order_id+" and repair_pc_id = " + pc_id ;
			return dbUtilAutoTran.selectSingle(sql);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairDetailDeliveryByRepairAndPc error:"+e);
		}
	}
	/**
	 * 添加转运单详细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addRepairDetail(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("repair_order_detail"), dbrow);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.addRepairDetail error:"+e);
		}
	}
	
	/**
	 * 转运发货明细比较
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] compareRepairPacking(long repair_order_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("repair_outbound_compare_view")+" where real_send_count != repair_total_count and repair_order_id=?";
			
			DBRow para = new DBRow();
			para.add("repair_order_id",repair_order_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.compareRepairPacking error:"+e);
		}
	}
	/**
	 * 添加发货审核
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addSendApprove(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("repair_send_approve"), dbrow);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.addSendApprove error:"+e);
		}
	}
	
	/**
	 * 发货审核明细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addSendApproveDetail(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("repair_send_approve_detail"), dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.addSendApproveDetail error:"+e);
		}
	}
	
	/**
	 * 删除转运单出库详细
	 * @param repair_order_id
	 * @throws Exception
	 */
	public void delRepairOutBoundByRepairId(long repair_order_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where to_repair_order_id="+repair_order_id,ConfigBean.getStringValue("repair_outbound"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.delRepairOutBoundByRepairId error:"+e);
		}
	}
	
	public void insertRepairWarehouses(String repair_order_id, String[] product_names, String[] stockOut_counts) throws Exception {
		try 
		{
			for(int i=0; i<product_names.length; i++) {
				String product_name = product_names[i];
				DBRow product = floorProductMgr.getDetailProductByPname(product_name);
				DBRow db = new DBRow();
				db.add("tw_repair_order_id",repair_order_id);
				db.add("tw_product_id",product.get("pc_id",0l));
				db.add("tw_product_name",product.getString("p_name"));
				db.add("tw_product_barcode",product.getString("p_code"));
				db.add("tw_count",stockOut_counts[i]);
				db.add("machine_id","");
				db.add("tw_storage_location","");
				this.repairWarehouses(db);
			}
		} 
		catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.insertRepairWarehouses error:"+e);
		}
	}
	
	public void repairWarehouses(DBRow row) throws Exception
	{
		try 
		{
			dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("repair_warehouse"), row);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.repairWarehouses error:"+e);
		}
	}
	/**
	 * 根据返修单号得到入库信息
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRepairWarehousesByRepairId(String repair_order_id) throws Exception {
		try
		{
			String sql = "select * from repair_warehouse where tw_repair_order_id = " + repair_order_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairWarehousesByRepairId error:"+e);
		}
		
	}
	
	/**
	 * 验证转运单内是否已有此商品
	 * @param repair_order_id
	 * @param product_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getRepairDetailByTPId(long repair_order_id,long product_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("repair_order_detail")+" where repair_order_id = ? and repair_pc_id = ?";
			
			DBRow para = new DBRow();
			para.add("repair_order_id",repair_order_id);
			para.add("repair_pc_id",product_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairDetailByTPId error:"+e);
		}
	}
	
	/**
	 * 检查转运单到货与发货数量是否相符
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkRepairDetailApprove(long repair_order_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("repair_order_detail")+" where repair_send_count != repair_reap_count and repair_order_id=?";
			
			DBRow para = new DBRow();
			para.add("repair_order_id",repair_order_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.checkRepairDetailApprove error:"+e);
		}
	}
	
	/**
	 * 获得转运单的明细不同
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDifferentRepairDetailByRepairId(long repair_order_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("repair_order_detail")+" as td left join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = td.repair_pc_id where td.repair_send_count !=td.repair_reap_count and td.repair_order_id = ?";
			
			DBRow para = new DBRow();
			para.add("repair_order_id",repair_order_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getDifferentRepairDetailByRepairId error:"+e);
		}
	}
	
	/**
	 * 添加交货单审核
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addRepairApprove(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("repair_approve"), dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.addRepairApprove error:"+e);
		}
	}
	/**
	 * 添加转运审核详细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addRepairApproveDetail(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("repair_approve_detail"),dbrow);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.addRepairApproveDetail error:"+e);
		}
	}
	
	/**
	 * 获得转运入库
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRepairWarehouse(long repair_order_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("repair_warehouse")+" where tw_repair_order_id=?";
			DBRow para = new DBRow();
			para.add("repair_order_id",repair_order_id);
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairWarehouse error:"+e);
		}
	}
	/**
	 * 根据转运单ID删除伪交货
	 * @param repair_order_id
	 * @throws Exception
	 */
	public void delRepairWareHouseByRepairId(long repair_order_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where tw_repair_order_id="+repair_order_id,ConfigBean.getStringValue("repair_warehouse"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.delRepairWareHouseByRepairId error:"+e);
		}
	}
	
	/**
	 * 转运单库存成本
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public String computePriceForRepair(long repair_order_id) throws Exception {
		try 
		{
			String sql = "";
			DBRow[] dbrows = null;
			float computeResult = 0f;
			float price = 0f;// 出库成本
			float repair_reap_count = 0f;// 转运单数量
			float unit_price = 0f;// 单个产品的价格
			float storage_unit_freight =0f;// 单个运费
			float storage_unit_price  = 0f;//库存价格/单个成本
			float store_count = 0f;//库存数量
			float count = 0f;
			
			sql = "SELECT DISTINCT dod.repair_send_price, dod.repair_reap_count, ps.storage_unit_freight, ps.storage_unit_price," +
			" ps.store_count, dod.repair_pc_id, tt.repair_order_id, ps.pid, ps.cid, p1.weight, p1.unit_price " +
			"FROM repair_order AS tt " +
			"JOIN repair_order_detail AS dod ON dod.repair_order_id = tt.repair_order_id " +
			"JOIN product_storage AS ps ON ps.pc_id = dod.repair_pc_id and ps.cid = tt.receive_psid " +
			"JOIN product AS p1 ON p1.pc_id = dod.repair_pc_id " +
			"where tt.repair_order_id = "+repair_order_id+" and `dod`.repair_reap_count>0 ";
			
			dbrows = dbUtilAutoTran.selectMutliple(sql);
			if(dbrows != null){
				for (int i = 0; i < dbrows.length; i++) {
					int pid = dbrows[i].get("pid", 0);
					price = dbrows[i].get("repair_send_price", 0f);
					repair_reap_count = dbrows[i].get("repair_reap_count", 0f);
					unit_price = dbrows[i].get("unit_price", 0f);
					storage_unit_freight = dbrows[i].get("storage_unit_freight", 0f);
					storage_unit_price = dbrows[i].get("storage_unit_price", 0f);
					
					store_count = dbrows[i].get("store_count", 0f);
					
					if(price == 0 ||price == 0.0){
						price = unit_price;
					}
					if(storage_unit_price == 0.0 || storage_unit_price == 0 || (store_count-repair_reap_count) <= 0){
						computeResult = price;
					}else{
						if((store_count-repair_reap_count) == 0.0){
							return "库存数量为0";
						}
						computeResult = (price * repair_reap_count + storage_unit_price
								* (store_count-repair_reap_count))
								/ store_count;
					}
					String sql2 = "UPDATE product_storage SET storage_unit_price = "+(float)computeResult+" WHERE pid = "+pid;
					dbUtilAutoTran.updateInstallSQL(sql2);
				}
			}
			return "";
		} catch (Exception e) {
			throw new Exception("FloorRepairOrderMgrZyj.computePriceForRepair error:"+e);
		}
	}
	/**
	 * 转运单库存运费
	 * @param repair_order_id
	 * @return
	 * @throws Exception
	 */
	public String computeFreightForRepair(long repair_order_id) throws Exception
	{
		try 
		{
			DBRow[] dbrows = null;
			DBRow dbrow = null;
			double unit_price = 0.0;//转运单单个运费
			double sum_price = 0.0;//转运单总运费
			double sum_weight = 0.0;//总重量
			double computeResult = 0.0;
			double storage_unit_freight = 0.0;
			double total_count = 0.0;
			double repair_reap_count = 0.0;
			double stock_count = 0.0;
			
			String sql2 = "select sum(tfc_unit_price*tfc_unit_count*tfc_exchange_rate) AS sum_price from repair_freight_cost where repair_order_id = "+repair_order_id;
			dbrow = dbUtilAutoTran.selectSingle(sql2);
			if(dbrow != null){
				sum_price = dbrow.get("sum_price", 0d);
			}
			
			String sql = "SELECT DISTINCT dod.repair_send_price, dod.repair_reap_count, ps.storage_unit_freight, ps.storage_unit_price, ps.store_count, dod.repair_pc_id, tt.repair_order_id, ps.pid, ps.cid, p1.weight, p1.unit_price " +
			"FROM repair_order AS tt " +
			"JOIN repair_order_detail AS dod ON dod.repair_order_id = tt.repair_order_id " +
			"JOIN product_storage AS ps ON ps.pc_id = dod.repair_pc_id and ps.cid = tt.receive_psid " +
			"JOIN product AS p1 ON p1.pc_id = dod.repair_pc_id " +
			"where tt.repair_order_id = "+repair_order_id+" and `dod`.repair_reap_count>0 ";
			dbrows = dbUtilAutoTran.selectMutliple(sql);
			if(dbrows != null){
				for(int j = 0;j<dbrows.length; j++){
					sum_weight += dbrows[j].get("weight", 0d)*dbrows[j].get("repair_reap_count", 0d);
				}
				for(int i = 0;i<dbrows.length;i++){
					int pid = dbrows[i].get("pid", 0);
					unit_price = sum_price * (dbrows[i].get("weight", 0d)*dbrows[i].get("repair_reap_count", 0d)/sum_weight);
					storage_unit_freight = dbrows[i].get("storage_unit_freight", 0d);
					repair_reap_count = dbrows[i].get("repair_reap_count", 0d);
					total_count = dbrows[i].get("store_count", 0d);
					stock_count = total_count - repair_reap_count;
					
					if(storage_unit_freight == 0 || stock_count <= 0)
					{
						if(dbrows[i].get("repair_reap_count", 0d) == 0)
						{
							return "转运单数量为0";
						}else{
							computeResult = unit_price/repair_reap_count;
						}
					}else{
						computeResult = (unit_price +(storage_unit_freight*stock_count))/(total_count);
					}
					
					String sql4 = "UPDATE product_storage SET storage_unit_freight = "+(float)computeResult+" WHERE pid = "+pid;
					dbUtilAutoTran.updateInstallSQL(sql4);
				}
			}
			return "";
		}
		catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.computeFreightForRepair error:"+e);
		}
	}
	/**
	 * 得到所有的返修单详细
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllRepairOrderDetail()
	throws Exception
	{
		try 
		{
			return dbUtilAutoTran.select(ConfigBean.getStringValue("repair_order_detail"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getAllRepairOrderDetail error:"+e);
		}
	}
	/**
	 * 删除转运单详细
	 * @param repair_detail_id
	 * @throws Exception
	 */
	public void delRepairDetail(long repair_detail_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where repair_detail_id="+repair_detail_id,ConfigBean.getStringValue("repair_order_detail"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.delRepairDetail error:"+e);
		}
	}
	public int getIndexOfProductFileByFileName(String fileName) throws Exception 
	{
		try
		{
			String tableName = ConfigBean.getStringValue("product_file");
			StringBuffer sql = new StringBuffer("select * from ").append(tableName).append(" where file_name like '")
			.append(fileName).append("%' order by pf_id desc limit 0,1");
			DBRow fileRow = dbUtilAutoTran.selectSingle(sql.toString());
			if(fileRow == null)
			{	
				return 0;
			}else
			{
				String name = fileRow.getString("file_name");
				String[] tempArray = name.split("_");
				int index = 1 ;
				String stringIndex = tempArray[tempArray.length -1];
				if(stringIndex.indexOf(".") != -1){
					stringIndex = stringIndex.substring(0, stringIndex.indexOf(".") );
				}
				try
				{
					index = Integer.parseInt(stringIndex) + 1;
				}catch (Exception e) {
				}finally{}
				return index ;
			}	
		}catch (Exception e) {
			throw new Exception("FloorRepairOrderMgrZyj.getIndexOfProductFileByFileName(fileName):"+e);
		}
	}
	
	public long addProductFile(DBRow row) throws Exception {
		try{
			return  dbUtilAutoTran.insertReturnId("product_file", row); 
		}catch(Exception e){
			throw new Exception("FloorRepairOrderMgrZyj.addProductFile(DBRow):"+e);
		}
	}
	/**
	 * 通过返修单ID获取实际到货商品总数
	 * @param repairId
	 * @return
	 * @throws Exception
	 */
	public double getRepairDetailProductTotalByRepairId(long repairId) throws Exception
	{
		try 
		{
			String sql = "SELECT SUM(repair_reap_count) as repair_order_detail_total FROM repair_order_detail WHERE repair_order_id = "+repairId;
			return dbUtilAutoTran.selectSingle(sql).get("repair_order_detail_total", 0.0);
		}
		catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairDetailProductTotalByRepairId(long repairId):"+e);
		}
	}
	/**
	 * 根据返修单ID查询，看看返修单是否存在
	 * @param repairOrderId
	 * @return
	 * @throws Exception
	 */
	public boolean isRepairOrderExist(long repairOrderId) throws Exception
	{
		try 
		{
			String sql = "select count(repair_order_id) as repair_order_count from repair_order where repair_order_id = " + repairOrderId;
			int count = dbUtilAutoTran.selectSingle(sql).get("repair_order_count", 0);
			return 0==count?false:true;
		} 
		catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairDetailProductTotalByRepairId(long repairId):"+e);
		}
	}
	
	/**
	 * 过滤返修单审核
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param approve_status
	 * @param sorttype
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterTransportApprove(long send_psid, long receive_psid,PageCtrl pc, int approve_status, String sorttype)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select ta.*,send.title AS sendtitle,receive.title AS receivetitle from "+ConfigBean.getStringValue("repair_approve")+" as ta"
												+" left join "+ConfigBean.getStringValue("repair_order")+" as t on t.repair_order_id = ta.repair_order_id"
												+" left join "+ConfigBean.getStringValue("product_storage_catalog")+" as send on send.id = t.send_psid"
												+" left join "+ConfigBean.getStringValue("product_storage_catalog")+" as receive on receive.id = t.receive_psid"
												+" where 1=1");
			if(send_psid>0)
			{
				sql.append(" and t.send_psid = "+send_psid);
			}
			if(receive_psid>0)
			{
				sql.append(" and t.receive_psid = "+receive_psid);
			}
			
			if(approve_status>0)
			{
				sql.append(" and ta.approve_status = "+approve_status);
			}
			
			if(sorttype.equals("subdate"))
			{
				sql.append(" order by ta.commit_date desc");
			}
			else if(sorttype.equals("approvedate"))
			{
				sql.append(" order by ta.approve_date desc");
			}
			else
			{
				sql.append(" order by ta.ta_id desc");
			}
			return dbUtilAutoTran.selectMutliple(sql.toString(),pc);
			
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.fillterTransportApprove error:"+e);
		}
	}
	
	/**
	 * 根据审核ID，获得审核明细
	 * @param ta_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApproveDetailByTaid(long ta_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("repair_approve_detail")+" where ta_id = ?";
			
			DBRow para = new DBRow();
			para.add("ta_id",ta_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getApproveDetailByTaid error:"+e);
		}
	}
	
	/**
	 * 修改转运单审核明细
	 * @param tad_id
	 * @param para
	 * @throws Exception
	 */
	public void modRepairOrderApproveDetail(long tad_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where tad_id="+tad_id,ConfigBean.getStringValue("repair_approve_detail"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.modRepairOrderApproveDetail error:"+e);
		}
	}
	
	/**
	 * 根据审核状态获得审核明细
	 * @param ta_id
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApproveRepairDetailWihtStatus(long ta_id,int status)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("repair_approve_detail")+" where ta_id = ? and approve_status=?";
			
			DBRow para = new DBRow();
			para.add("ta_id",ta_id);
			para.add("approve_status",status);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getApproveRepairDetailWihtStatus error:"+e);
		}
	}
	
	/**
	 * 获得转运审核详细
	 * @param ta_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailApproveRepairById(long ta_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("repair_approve")+" where ta_id=?";
			
			DBRow para = new DBRow();
			para.add("ta_id",ta_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getDetailApproveRepairById error:"+e);
		}
	}
	
	/**
	 * 修改转运单审核
	 * @param ta_id
	 * @param para
	 * @throws Exception
	 */
	public void modRepairApprove(long ta_id,DBRow para)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where ta_id="+ta_id,ConfigBean.getStringValue("repair_approve"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.modRepairApprove error:"+e);
		}
	}
	/**
	 * 通过审核ID得到审核数据
	 * @param repair_approve_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getRepairOrderApproveById(long repair_approve_id) throws Exception
	{
		try 
		{
			String sql = "select * from repair_approve where ta_id = " + repair_approve_id;
			return dbUtilAutoTran.selectSingle(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.getRepairOrderApproveById error:"+e);
		}
	}
	/**
	 * 删除到货临时数据
	 * @param repair_order_id
	 * @throws Exception
	 */
	public void deleteRepairWarehouseByRepairId(long repair_order_id) throws Exception
	{
		try
		{
			String wherecond = " where tw_repair_order_id = " + repair_order_id;
			dbUtilAutoTran.delete(wherecond, ConfigBean.getStringValue("repair_warehouse"));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.deleteRepairWarehouseByRepairId error:"+e);
		}
	}
	/**
	 * 删除审核明细
	 * @param repair_approve_id
	 * @throws Exception
	 */
	public void deleteRepairApproveDetailByCond(long repair_approve_id) throws Exception
	{
		try 
		{
			String wherecond = " where ta_id = " + repair_approve_id;
			dbUtilAutoTran.delete(wherecond, ConfigBean.getStringValue("repair_approve_detail"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.deleteRepairApproveDetailByCond error:"+e);
		}
	}
	/**
	 * 删除审核
	 * @param repair_approve_id
	 * @throws Exception
	 */
	public void deleteRepairApproveById(long repair_approve_id) throws Exception
	{
		try 
		{
			String wherecond = " where ta_id = " + repair_approve_id;
			dbUtilAutoTran.delete(wherecond, ConfigBean.getStringValue("repair_approve"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorRepairOrderMgrZyj.deleteRepairApproveById error:"+e);
		}
	}
	/**
	 * 根据返修单ID更新所有的返修单明细
	 * @param repair_order_id
	 * @param row
	 * @throws Exception
	 */
	public void updateRepairOrderDetailByRepairId(long repair_order_id, DBRow row) throws Exception
	{
		try 
		{
			String wherecond = " where repair_order_id = " + repair_order_id;
			dbUtilAutoTran.update(wherecond, ConfigBean.getStringValue("repair_order_detail"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.updateRepairOrderDetailByRepairId error:"+e);
		}
	}
	
	/**
	 * 删除返修单明细(根据返修单详细ID批量删除)
	 * @param repair_order_id
	 * @throws Exception
	 */
	public void delRepairDetailByRepairDetailId(long repair_detail_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where repair_detail_id="+repair_detail_id,ConfigBean.getStringValue("repair_order_detail"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorRepairOrderMgrZyj.delRepairDetailByRepairDetailId error:"+e);
		}
	}
	
	/**
	 * 根据 商品名字检索
	 * @param repairId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRepairDetailByRepairProductName(long repair_order_id, String name)throws Exception{
		try{
			String sql="select * from repair_order_detail where repair_order_id="+repair_order_id+" and repair_p_name  like '%"+name+"%' ";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorRepairOrderMgrZyj.getRepairDetailByRepairProductName error:"+ e);
		}
	}
	
	/**
	 * 根据 商品名字检索
	 * @param repairId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRepairDetailsByRepairProductName(long repair_order_id, String name)throws Exception{
		try{
			String sql="select * from repair_order_detail where repair_order_id="+repair_order_id+" and repair_p_name  = '"+name+"'";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorRepairOrderMgrZyj.getRepairDetailsByRepairProductName error:"+ e);
		}
	}
	
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}


	public void setFloorProductMgr(FloorProductMgr floorProductMgr) {
		this.floorProductMgr = floorProductMgr;
	}

}
