package com.cwc.app.floor.api.zj;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.app.key.CheckInChildDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInLiveLoadOrDropOffKey;
import com.cwc.app.key.EquipmentTypeKey;
import com.cwc.app.key.CheckInMainDocumentsRelTypeKey;
import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.ScheduleFinishKey;
import com.cwc.app.key.SpaceRelationTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorCheckInMgrZJ {
	@Autowired
	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBRow[] findWmsStorage(long ps_id,int search_location) 
			throws Exception
	{
			try
			{
				String sql = "select * from "+ConfigBean.getStringValue("config_search_warehouse_location")+" where ps_id= ? and search_location= ? ";
				
				DBRow para = new DBRow();
				para.add("ps_id",ps_id);
				para.add("search_location",search_location);
				
				return dbUtilAutoTran.selectPreMutliple(sql,para);
			}
			catch (Exception e) 
			{
				 throw new Exception("FloorCheckInMgrZwb storageSearchPriority"+e);
			}
	}
	
	/**
	 * 未分配装卸工的任务
	 * WareHouse 左上	no assing labor
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] noAssignLabor(long ps_id,PageCtrl pc)
		throws Exception
	{
		String sql = "select ee.check_in_entry_id as dlo_id,CONCAT(CASE srr.resources_type WHEN "+OccupyTypeKey.DOOR+" THEN 'DOOR  ' WHEN "+OccupyTypeKey.SPOT+" THEN 'SPOT  ' END,IFNULL(syc.yc_no,sd.doorId)) as location,ee.equipment_number,IFNULL(ee.check_in_window_time,ee.check_in_time) as start_time,GROUP_CONCAT(dlod.number)as tasks,dlo.waiting,dlo.priority from "+ConfigBean.getStringValue("door_or_location_occupancy_details")+" as dlod "
					+"join "+ConfigBean.getStringValue("door_or_location_occupancy_main")+" as dlo on dlo.dlo_id = dlod.dlo_id "
					+"join "+ConfigBean.getStringValue("entry_equipment")+" as ee on dlod.equipment_id = ee.equipment_id "
					+"join "+ConfigBean.getStringValue("space_resources_relation")+" as srr on srr.relation_id = dlod.dlo_detail_id and srr.relation_type = "+SpaceRelationTypeKey.Task+" "
					+"left join "+ConfigBean.getStringValue("storage_yard_control")+" as syc on syc.yc_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.SPOT+" "
					+"left join "+ConfigBean.getStringValue("storage_door")+" as sd on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"join `"+ConfigBean.getStringValue("schedule")+"` as s on s.associate_id = dlod.dlo_detail_id and (s.associate_process = "+ProcessKey.CHECK_IN_WINDOW+" or s.associate_process = "+ProcessKey.GateNotifyWareHouse+") "
					+"WHERE	dlo.ps_id = "+ps_id+" and ee.equipment_status in ("+CheckInMainDocumentsStatusTypeKey.UNPROCESS+","+CheckInMainDocumentsStatusTypeKey.PROCESSING+","+CheckInMainDocumentsStatusTypeKey.INYARD+") and s.schedule_state <>"+ScheduleFinishKey.ScheduleFinish+ " "
					+"group by srr.resources_id,srr.resources_type,ee.equipment_id "
					+"order by start_time";
		
 		DBRow[] result = dbUtilAutoTran.selectMutliple(sql,pc);
		
		return startTimeUTCToLocation(result, ps_id);
	}
	
	/**
	 * 未开始的任务
	 * WareHouse 右上
	 * @author zhanjie
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] remainJobsDoor(long ps_id,PageCtrl pc)
		throws Exception
	{
		String sql = "select ee.check_in_entry_id as dlo_id,CONCAT(CASE srr.resources_type WHEN "+OccupyTypeKey.DOOR+" THEN 'DOOR  ' WHEN "+OccupyTypeKey.SPOT+" THEN 'SPOT  ' END,IFNULL(syc.yc_no,sd.doorId)) as location,ee.equipment_number,ad.employe_name as labor,GROUP_CONCAT(dlod.number)as tasks,min(laborS.create_time) as start_time from door_or_location_occupancy_details as dlod "
					+"join door_or_location_occupancy_main as dlo on dlod.dlo_id = dlo.dlo_id "
					+"join `schedule` as laborS on laborS.associate_id = dlod.dlo_detail_id and laborS.associate_process = "+ProcessKey.CHECK_IN_WAREHOUSE+" "
					+"join entry_equipment as ee on dlod.equipment_id = ee.equipment_id "
					+"join space_resources_relation as srr on srr.relation_id = dlod.dlo_detail_id and srr.relation_type = "+SpaceRelationTypeKey.Task+" "
					+"left join storage_yard_control as syc on syc.yc_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.SPOT+" "
					+"left join storage_door as sd on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"left join schedule_sub as ss on laborS.schedule_id = ss.schedule_id "
					+"left join admin as ad on ad.adid = ss.schedule_execute_id "
					+"where dlo.ps_id = "+ps_id+" and dlod.number_status = "+CheckInChildDocumentsStatusTypeKey.UNPROCESS+" "
					+"group by srr.resources_id,ee.equipment_id,ad.adid "
					+"order by start_time asc ";
		
		DBRow[] result = dbUtilAutoTran.selectMutliple(sql,pc);
		
 		return startTimeUTCToLocation(result, ps_id);
	}
	
	/**
	 * 个人未开始的任务
	 * @author zhanjie
	 * WareHouse
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] remainJobsLabor(long ps_id,PageCtrl pc)
		throws Exception
	{
		String sql = "select ad.employe_name as labor,GROUP_CONCAT(CONCAT(CASE srr.resources_type WHEN "+OccupyTypeKey.DOOR+" THEN 'DOOR  ' WHEN "+OccupyTypeKey.SPOT+" THEN 'SPOT  ' END,IFNULL(syc.yc_no,sd.doorId))) as locations,GROUP_CONCAT(ee.equipment_number)as equipments,GROUP_CONCAT(dlod.number)as tasks,count(dlod.number) as task_count,MIN(laborS.create_time)as start_time from door_or_location_occupancy_details as dlod "
					+"join door_or_location_occupancy_main as dlo on dlod.dlo_id = dlo.dlo_id "
					+"join `schedule` as laborS on laborS.associate_id = dlod.dlo_detail_id and laborS.associate_process = "+ProcessKey.CHECK_IN_WAREHOUSE+" "
					+"join entry_equipment as ee on dlod.equipment_id = ee.equipment_id "
					+"join space_resources_relation as srr on srr.relation_id = dlod.dlo_detail_id and srr.relation_type = "+SpaceRelationTypeKey.Task+" "
					+"left join storage_yard_control as syc on syc.yc_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.SPOT+" "
					+"left join storage_door as sd on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"left join schedule_sub as ss on laborS.schedule_id = ss.schedule_id "
					+"left join admin as ad on ad.adid = ss.schedule_execute_id "
					+"where dlo.ps_id = "+ps_id+" and ee.equipment_status in ("+CheckInMainDocumentsStatusTypeKey.UNPROCESS+","+CheckInMainDocumentsStatusTypeKey.PROCESSING+","+CheckInMainDocumentsStatusTypeKey.INYARD+") and dlod.number_status = "+CheckInChildDocumentsStatusTypeKey.UNPROCESS+" "
					+"group by ad.adid "
					+"order by start_time asc ";
		
		DBRow[] result = dbUtilAutoTran.selectMutliple(sql,pc);
		
 		return startTimeUTCToLocation(result, ps_id);
	}
	
	/**
	 * 没有任务的Labor
	 * @param ps_id
	 * @param adgids
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] noTaskLabor(long ps_id,long[] adgids,long[] adroles,PageCtrl pc)
		throws Exception
	{
		StringBuffer whereAdminGroup = new StringBuffer();
		if (adgids.length>0) 
		{
			whereAdminGroup.append(" and adp.department_id in (");
			for (int i = 0; i < adgids.length; i++) 
			{
				whereAdminGroup.append(adgids[i]);
				if (i<adgids.length-1)
				{
					whereAdminGroup.append(",");
				}
			}
			
			whereAdminGroup.append(") ");
		}
		
		StringBuffer whereAdminRole = new StringBuffer();
		if (adroles.length>0)
		{
			whereAdminRole.append(" and adp.post_id in (");
			for (int i = 0; i < adroles.length; i++) 
			{
				whereAdminRole.append(adroles[i]);
				if (i<adroles.length-1)
				{
					whereAdminRole.append(",");
				}
			}
			
			whereAdminRole.append(") ");
		}
		
		String sql = "select DISTINCT ad.adid,ad.account,ad.employe_name,adp.department_id adgid,MAX(s.end_time) as start_time from admin as ad "
					+ "join admin_department as adp on adp.adid = ad.adid "
					+ "join admin_warehouse as adw on adw.adid = ad.adid " 
					+"left join schedule_sub as ss on ss.schedule_execute_id = ad.adid "
					+"left join `schedule` as s on s.schedule_id = ss.schedule_id and s.associate_process = "+ProcessKey.CHECK_IN_WAREHOUSE+" "
					//+"left join door_or_location_occupancy_details as dlod on s.associate_id = dlod.dlo_detail_id "
					+"where adw.warehouse_id = "+ps_id+" "
					+ "AND (ss.schedule_state is null "
					+ "		or not EXISTS(select 1 from schedule_sub sub "
					+ "						join `schedule` sch on sub.schedule_id = sch.schedule_id  and sch.associate_process = 53 "
					+ "						where sub.schedule_state < 10"
					+ "						and sub.schedule_execute_id = ad.adid)) "
					+whereAdminGroup.toString()+whereAdminRole.toString()
					+" group by ad.adid";
		
 		DBRow[] result = null ;
 		if(pc != null)
 		{  //zhangrui 修改 添加PC == null
			 result = dbUtilAutoTran.selectMutliple(sql, pc);
			 return startTimeUTCToLocation(result, ps_id);
		}
 		else
 		{
			result = dbUtilAutoTran.selectMutliple(sql);
		}
		return startTimeUTCToLocation(result, ps_id);

	}
	
	public DBRow[] spotRemainJobs(long ps_id,PageCtrl pc)
		throws Exception
	{
			String sql = "select dlod.number_type,dlod.number,dlod.ctn_number,dlo.dlo_id,dlo.gate_liscense_plate,dlo.rel_type,dlod.ctn_number,IFNULL(window_check_in_time,gate_check_in_time) as start_time,CONCAT('SPOT  ',yc_no) as target from "+ConfigBean.getStringValue("door_or_location_occupancy_details")+" as dlod "
						+"join "+ConfigBean.getStringValue("door_or_location_occupancy_main")+" as dlo on dlod.dlo_id = dlo.dlo_id "
						+"join "+ConfigBean.getStringValue("storage_yard_control")+" as syc on syc.associate_id = dlo.dlo_id and syc.associate_type = "+ModuleKey.CHECK_IN+" and dlod.rl_id = syc.yc_id and dlod.occupancy_type = "+OccupyTypeKey.SPOT+" "
						+"where dlo.ps_id = ? and dlod.number_status = "+CheckInChildDocumentsStatusTypeKey.UNPROCESS+" and dlo.rel_type in ("+CheckInMainDocumentsRelTypeKey.DELIVERY+","+CheckInMainDocumentsRelTypeKey.PICK_UP+","+CheckInMainDocumentsRelTypeKey.BOTH+") "
						+"order by start_time asc ";
			
			DBRow para = new DBRow();
			para.add("1_ps_id",ps_id);
			
			DBRow[] result = dbUtilAutoTran.selectPreMutliple(sql, para, pc);
			
	 		return startTimeUTCToLocation(result, ps_id);
	}
	
	
	/**
	 * 正在进行中的任务 
	 * WareHouse 下部
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] workingOnWarehouse(long ps_id,int lr_type,PageCtrl pc)
		throws Exception
	{
		//zhangrui 添加一个lr_id的参数2015-01-15

		String sql = "select dlod.flag_help,dlod.dlo_id,dlod.dlo_detail_id,dlod.dlo_id,dlod.number_type,dlod.number,dlod.lr_id,dlod.receipt_no,dlod.company_id,ee.equipment_number,CONCAT(CASE srr.resources_type WHEN 1 THEN 'DOOR  ' WHEN 2 THEN 'SPOT  ' END,IFNULL(syc.yc_no, sd.doorId)) as location,dlod.handle_time as start_time,GROUP_CONCAT(DISTINCT ad.employe_name) as labor,ee.rel_type from door_or_location_occupancy_details as dlod " 
					+"join door_or_location_occupancy_main as dlo on dlod.dlo_id = dlo.dlo_id "
					+"join entry_equipment as ee on dlod.equipment_id = ee.equipment_id "
					+"left join space_resources_relation as srr on srr.relation_id = dlod.dlo_detail_id and srr.relation_type = "+SpaceRelationTypeKey.Task+" "
					+"left join storage_door as sd on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"left join storage_yard_control AS syc ON syc.yc_id = srr.resources_id AND srr.resources_type = "+OccupyTypeKey.SPOT+" "
					+"left join `schedule` as s on s.associate_id = dlod.dlo_detail_id and s.associate_process = "+ProcessKey.CHECK_IN_WAREHOUSE+" " 
					+"left join schedule_sub as ss on s.schedule_id = ss.schedule_id "
					+"left join admin as ad on ad.adid = ss.schedule_execute_id "
					+"where dlo.ps_id = "+ps_id+" and dlod.number_status = "+CheckInChildDocumentsStatusTypeKey.PROCESSING+" ";
					if(lr_type==CheckInMainDocumentsRelTypeKey.DELIVERY){
						sql+="and dlod.number_type in(12,13,14) ";
					}else{
						sql+="and dlod.number_type in(10,11,17,18) ";
					}
					
					sql+="GROUP BY dlo_detail_id "
					    +"order by start_time asc ";
   		DBRow[] result = dbUtilAutoTran.selectMutliple(sql,pc);
 		return startTimeUTCToLocation(result, ps_id); 
	}
	/**
	 * 获取lines数量
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public int getReceiptLinesCount(long receipt_no,String company_id)
		throws Exception
	{
		//zhangrui 添加一个lr_id的参数2015-01-15

		String sql ="select count(receipt_line_id) as linesQty from receipt_lines where receipt_no="+receipt_no+" and company_id='"+company_id+"'";
   		DBRow result = this.dbUtilAutoTran.selectSingle(sql);
   		int count =0;
   		if(result != null){
   			count = result.get("linesQty", 0);
   		}
   		
 		return count; 
	}
	/**
	 * 车位上满柜
	 * @author zhanjie
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] cargoOnSpot(long ps_id,PageCtrl pc)
		throws Exception
	{
		//张睿修改我需要一个总数2015-01-06
		String sql = "select ee.check_in_entry_id as dlo_id,ee.equipment_id,ee.equipment_number,syc.yc_no,tractor.equipment_number as tractor,ee.check_in_time as start_time,ee.rel_type from entry_equipment as ee " 
					+"join door_or_location_occupancy_details as dlod on dlod.equipment_id = ee.equipment_id " 
					+"left join entry_equipment as tractor on tractor.check_in_entry_id = dlod.dlo_id  and tractor.equipment_type = 1 "
					+"join space_resources_relation as srr on ee.equipment_id = srr.relation_id and srr.relation_type = 1 "
					+"join storage_yard_control as syc on syc.yc_id = srr.resources_id and srr.resources_type = 2 "
					+"where ((ee.rel_type = 1 and ee.equipment_status =1)or (ee.rel_type = 3 and ee.equipment_status in (1,3))) and syc.ps_id = "+ps_id+" "
					+"GROUP BY ee.equipment_id "
					+"union " 
					+"select ee.check_in_entry_id as dlo_id,ee.equipment_id,ee.equipment_number,syc.yc_no,tractor.equipment_number as tractor,ee.check_out_warehouse_time as start_time,ee.rel_type from entry_equipment as ee " 
					+"join door_or_location_occupancy_details as dlod on dlod.equipment_id = ee.equipment_id "
					+"left join entry_equipment as tractor on tractor.check_in_entry_id = dlod.dlo_id  and tractor.equipment_type = 1 "
					+"join space_resources_relation as srr on ee.equipment_id = srr.relation_id and srr.relation_type = 1 "
					+"join storage_yard_control as syc on syc.yc_id = srr.resources_id and srr.resources_type = 2 "
					+"where ee.rel_type = 2 and ee.equipment_status = 3 and syc.ps_id = "+ps_id+" "
					+"GROUP BY ee.equipment_id";
		
		//DBRow[] result = dbUtilAutoTran.selectMutliple(sql, pc);原有代码
		DBRow[] result = null ;
		if(pc != null){
			result = dbUtilAutoTran.selectMutliple(sql, pc);
		}else{
			result = dbUtilAutoTran.selectMutliple(sql);
		}
 		return startTimeUTCToLocation(result, ps_id);
	}
	
	/**
	 * 门上满货
	 * @author zhanjie
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] cargoOnDoor(long ps_id,PageCtrl pc)
		throws Exception
	{
		//zhangrui 2015-01-06 张睿修改需要一个总数
		String sql = "select ee.check_in_entry_id as dlo_id,ee.equipment_id,ee.equipment_number,sd.doorId,tractor.equipment_number as tractor,ee.check_in_time as start_time,ee.rel_type from entry_equipment as ee "
					+"join door_or_location_occupancy_details as dlod on dlod.equipment_id = ee.equipment_id " 
					+"left join entry_equipment as tractor on tractor.check_in_entry_id = dlod.dlo_id  and tractor.equipment_type = 1 "
					+"join space_resources_relation as srr on ee.equipment_id = srr.relation_id and srr.relation_type = 1 "
					+"join storage_door as sd on sd.sd_id = srr.resources_id and srr.resources_type = 1 "
					+"where ((ee.rel_type = 1 and ee.equipment_status =1)or (ee.rel_type = 3 and ee.equipment_status in (1,3))) and ee.ps_id = "+ps_id+" "
					+"group by ee.equipment_id "
					+"union " 
					+"select ee.check_in_entry_id as dlo_id,ee.equipment_id,ee.equipment_number,sd.doorId,tractor.equipment_number as tractor,ee.check_out_warehouse_time as start_time,ee.rel_type from entry_equipment as ee "
					+"join door_or_location_occupancy_details as dlod on dlod.equipment_id = ee.equipment_id "
					+"left join entry_equipment as tractor on tractor.check_in_entry_id = dlod.dlo_id  and tractor.equipment_type = 1 "
					+"join space_resources_relation as srr on ee.equipment_id = srr.relation_id and srr.relation_type = 1 "
					+"join storage_door as sd on sd.sd_id = srr.resources_id and srr.resources_type = 1 "
					+"where ee.rel_type = 2 and ee.equipment_status = 3 and ee.ps_id = "+ps_id+" "
					+"group by ee.equipment_id "
					+"order by CAST(doorId AS SIGNED)"; 
		
		//DBRow[] result = dbUtilAutoTran.selectMutliple(sql,pc); 原有代码
		DBRow[] result = null ;
		if(pc != null){
			 result = dbUtilAutoTran.selectMutliple(sql,pc);
		}else{
			 result = dbUtilAutoTran.selectMutliple(sql);
		}
		return startTimeUTCToLocation(result, ps_id);
	}
	
	public DBRow[] emptyCTNR(long ps_id,PageCtrl pc)
		throws Exception
	{
		String sql = "select ee.check_in_entry_id as dlo_id,ee.equipment_id,ee.equipment_number,CONCAT('DOOR  ',sd.doorId)as location,tractor.equipment_number as tractor,ee.check_in_time,ee.check_in_window_time,ee.check_in_warehouse_time,ee.check_out_warehouse_time,ee.rel_type from entry_equipment as ee " 
					+"join door_or_location_occupancy_details as dlod on dlod.equipment_id = ee.equipment_id "
					+"left join entry_equipment as tractor on tractor.check_in_entry_id = dlod.dlo_id and tractor.equipment_type = "+EquipmentTypeKey.Tractor+" "
					+"join space_resources_relation as srr on ee.equipment_id = srr.relation_id and srr.relation_type = "+SpaceRelationTypeKey.Equipment+" "
					+"join storage_door as sd on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"where ee.ps_id = "+ps_id+" and ((ee.rel_type = "+CheckInMainDocumentsRelTypeKey.DELIVERY+" and ee.equipment_status = "+CheckInMainDocumentsStatusTypeKey.INYARD+") or (ee.rel_type = "+CheckInMainDocumentsRelTypeKey.PICK_UP+" and ee.equipment_status in ("+CheckInMainDocumentsStatusTypeKey.UNPROCESS+"))) "
					+"GROUP BY ee.equipment_id "
					+"union " 
					+"select ee.check_in_entry_id as dlo_id,ee.equipment_id,ee.equipment_number,CONCAT('SPOT  ',yc_no)as location,tractor.equipment_number as tractor,ee.check_in_time,ee.check_in_window_time,ee.check_in_warehouse_time,ee.check_out_warehouse_time,ee.rel_type from entry_equipment as ee " 
					+"join door_or_location_occupancy_details as dlod on dlod.equipment_id = ee.equipment_id "
					+"left join entry_equipment as tractor on tractor.check_in_entry_id = dlod.dlo_id  and tractor.equipment_type = "+EquipmentTypeKey.Tractor+" "
					+"join space_resources_relation as srr on ee.equipment_id = srr.relation_id and srr.relation_type = "+SpaceRelationTypeKey.Equipment+" "
					+"join storage_yard_control as syc on syc.yc_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.SPOT+" "
					+"where ee.ps_id = "+ps_id+" and ((ee.rel_type ="+CheckInMainDocumentsRelTypeKey.DELIVERY+" and ee.equipment_status ="+CheckInMainDocumentsStatusTypeKey.INYARD+") or (ee.rel_type = "+CheckInMainDocumentsRelTypeKey.PICK_UP+" and ee.equipment_status in ("+CheckInMainDocumentsStatusTypeKey.UNPROCESS+"))) "
					+"GROUP BY ee.equipment_id "
					+"order by location";

		DBRow[] result = dbUtilAutoTran.selectMutliple(sql,pc);
		
		return startTimeUTCToLocation(result,ps_id);
	}
	
	public DBRow[] parking3rdPartyNone(long ps_id,PageCtrl pc)
		throws Exception
	{
		String sql = "select dlo.dlo_id,tractor.equipment_number,GROUP_CONCAT(ctnr.equipment_number)as ctnrs,dlo.gate_driver_liscense,dlo.gate_driver_name,dlo.company_name,CONCAT(CASE srr.resources_type WHEN 1 THEN 'DOOR  ' WHEN 2 THEN 'SPOT  ' END,IFNULL(syc.yc_no,sd.doorId)) as location from door_or_location_occupancy_main as dlo "
					+"join entry_equipment as tractor on dlo.dlo_id = tractor.check_in_entry_id and tractor.equipment_type = "+EquipmentTypeKey.Tractor+" "
					+"left join space_resources_relation as srr on srr.relation_id = tractor.equipment_id and srr.relation_type = "+SpaceRelationTypeKey.Equipment+" "
					+"left join storage_yard_control as syc on syc.yc_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.SPOT+" "
					+"left join storage_door as sd on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"left join entry_equipment as ctnr on dlo.dlo_id = ctnr.check_in_entry_id and ctnr.equipment_type = "+EquipmentTypeKey.Container+" "
					+"where dlo.ps_id = "+ps_id+" and tractor.rel_type  ="+CheckInMainDocumentsRelTypeKey.VISITOR+" and tractor.check_out_time is null "
					+"GROUP BY dlo.dlo_id"; 
		
		DBRow[] result = dbUtilAutoTran.selectMutliple(sql,pc);
		formateLocation(result);
		return result;
	}
	
	/**
	 * 需要去做windowCheckIn的
	 * Window 左上
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] goingToWindow(long ps_id,PageCtrl pc)
		throws Exception
	{
//		String sql = "select dlo.dlo_id,ee.equipment_id,tractor.equipment_number as tractor,ee.equipment_number,ee.rel_type,dlo.gate_check_in_time as start_time,CONCAT(CASE equipmentSrr.resources_type WHEN 1 THEN 'DOOR  ' WHEN 2 THEN 'SPOT  ' END,IFNULL(syc.yc_no,sd.doorId)) as location  from door_or_location_occupancy_main as dlo "
//					+"join entry_equipment as ee on dlo.dlo_id = ee.check_in_entry_id "
//					+"left join space_resources_relation as equipmentSrr on equipmentSrr.relation_id = ee.equipment_id and equipmentSrr.relation_type = 1 "
//					+"left join storage_yard_control as syc on syc.yc_id = equipmentSrr.resources_id and equipmentSrr.resources_type = 2 "
//					+"left join storage_door as sd on sd.sd_id = equipmentSrr.resources_id and equipmentSrr.resources_type = 1 "
//					+"left join entry_equipment as tractor on tractor.check_in_entry_id = dlo.dlo_id and tractor.equipment_type = 1 "
//					+"left join `schedule` as s on s.associate_main_id = dlo.dlo_id and (s.associate_process = 52 or s.associate_type = 57) "
//					+"where s.schedule_id is null and dlo.window_check_in_time is null and ee.rel_type in (1,2,3,4,5,6) "
//					+"group by dlo.dlo_id ";
 		
		//DBRow[] result = dbUtilAutoTran.selectMutliple(sql,pc); zhangrui修改我想使用总数
//		String sql = "SELECT dlo.dlo_id,dlo.gate_check_in_time AS start_time,"
//					+"GROUP_CONCAT(DISTINCT CONCAT(CASE equipmentSrr.resources_type WHEN 1 THEN 'DOOR  ' WHEN 2 THEN 'SPOT  ' END,IFNULL(syc.yc_no, sd.doorId)))AS locations,"
//					+"tractor.equipment_number AS tractor,GROUP_CONCAT(DISTINCT trailer.equipment_number ORDER BY trailer.equipment_id)as trailer,ee.rel_type,GROUP_CONCAT(DISTINCT dlod.number)as tasks "
//					+"FROM door_or_location_occupancy_main AS dlo "
//					+"JOIN entry_equipment AS ee ON dlo.dlo_id = ee.check_in_entry_id "
//					+"left join door_or_location_occupancy_details as dlod on dlod.dlo_id = dlo.dlo_id "
//					+"LEFT JOIN space_resources_relation AS equipmentSrr ON equipmentSrr.relation_id = ee.equipment_id AND equipmentSrr.relation_type = 1 "
//					+"LEFT JOIN storage_yard_control AS syc ON syc.yc_id = equipmentSrr.resources_id AND equipmentSrr.resources_type = 2 "
//					+"LEFT JOIN storage_door AS sd ON sd.sd_id = equipmentSrr.resources_id AND equipmentSrr.resources_type = 1 "
//					+"LEFT JOIN entry_equipment AS tractor ON tractor.check_in_entry_id = dlo.dlo_id AND tractor.equipment_type = 1 "
//					+"LEFT JOIN entry_equipment AS trailer ON trailer.check_in_entry_id = dlo.dlo_id AND trailer.equipment_type = 2 "
//					+"LEFT JOIN `schedule` AS s ON s.associate_id = dlod.dlo_detail_id AND(s.associate_process = 52 OR s.associate_process = 57 or s.associate_process = 53) "
//					+"WHERE	dlo.ps_id = "+ps_id+" and ee.equipment_status in (1,2,3) and s.schedule_id IS NULL AND dlo.window_check_in_time IS NULL AND ee.rel_type IN(1, 2, 3, 4, 5, 6) "
//					+"GROUP BY dlo.dlo_id ";
		String sql = "SELECT "
					+"		dlo.dlo_id,dlo.gate_check_in_time AS start_time,"
					+"		GROUP_CONCAT(DISTINCT CONCAT(CASE equipmentSrr.resources_type	WHEN 1 THEN	'DOOR  ' WHEN 2 THEN 'SPOT  '	END,IFNULL(syc.yc_no, sd.doorId)))AS locations,"
					+"		tractor.equipment_number AS tractor,"
					+"		GROUP_CONCAT(DISTINCT trailer.equipment_number ORDER BY	trailer.equipment_id)AS trailer,"
					//+"		ee.rel_type,"
					+"		IFNULL(trailer.rel_type, ee.rel_type) rel_type,IFNULL(trailer.equipment_purpose ,tractor.equipment_purpose) equipment_purpose , "	//以车尾为主
					+"		GROUP_CONCAT(DISTINCT dlod.number)AS tasks "
					+"	FROM door_or_location_occupancy_main AS dlo "
					+"	JOIN entry_equipment AS ee ON dlo.dlo_id = ee.check_in_entry_id "
					+"	LEFT JOIN (SELECT dd.* FROM door_or_location_occupancy_details dd"
					+ "             JOIN entry_equipment ee ON ee.equipment_id = dd.equipment_id"
					+ "             WHERE IF(ee.equipment_purpose ="+CheckInLiveLoadOrDropOffKey.DROP+",dd.number_type NOT IN ("+ModuleKey.CHECK_IN_DELIVERY_ORTHERS+","+ModuleKey.CHECK_IN_PICKUP_ORTHERS+"),'1=1') != 0)"
					+ "   AS dlod ON dlod.dlo_id = dlo.dlo_id"
//					+ " door_or_location_occupancy_details AS dlod ON dlod.dlo_id = dlo.dlo_id "
					+"	LEFT JOIN space_resources_relation AS equipmentSrr ON equipmentSrr.relation_id = ee.equipment_id AND equipmentSrr.relation_type = 1 "
					+"	LEFT JOIN storage_yard_control AS syc ON syc.yc_id = equipmentSrr.resources_id AND equipmentSrr.resources_type = 2 "
					+"	LEFT JOIN storage_door AS sd ON sd.sd_id = equipmentSrr.resources_id AND equipmentSrr.resources_type = 1 "
					+"	LEFT JOIN entry_equipment AS tractor ON tractor.check_in_entry_id = dlo.dlo_id AND tractor.equipment_type = 1 "
					+"	LEFT JOIN entry_equipment AS trailer ON trailer.check_in_entry_id = dlo.dlo_id AND trailer.equipment_type = 2 "
					+"	LEFT JOIN `schedule` AS s ON s.associate_id = dlod.dlo_detail_id  and s.associate_process = 56 "
					+"	LEFT JOIN `schedule` AS nos ON nos.associate_id = dlo.dlo_id  and nos.associate_process = 59 "
					+"	WHERE ((s.associate_process = 56 and s.schedule_state <>"+ScheduleFinishKey.ScheduleFinish+") "
					+"         or (nos.associate_process = 59 and nos.schedule_state <>"+ScheduleFinishKey.ScheduleFinish+")) "
					+"  and dlo.ps_id = "+ps_id+" "
					+ " AND ee.equipment_status IN(1, 2, 3) "
					+ " AND ee.rel_type IN(1, 2, 3, 4, 5, 6) "
					+"	GROUP BY dlo.dlo_id "
//					+"	union " 
//					+"	SELECT "
//					+"		dlo.dlo_id,"
//					+"		dlo.gate_check_in_time AS start_time,"
//					+"		GROUP_CONCAT(DISTINCT CONCAT(CASE srr.resources_type WHEN 1 THEN 'DOOR  '	WHEN 2 THEN	'SPOT  'END,IFNULL(syc.yc_no, sd.doorId)))AS locations,"
//					+"		tractor.equipment_number AS tractor,GROUP_CONCAT(DISTINCT trailer.equipment_number ORDER BY	trailer.equipment_id)AS trailer,ee.rel_type,null as tasks "
//					+"	FROM door_or_location_occupancy_main AS dlo "
//					+"	JOIN entry_equipment AS ee ON ee.check_in_entry_id = dlo.dlo_id AND ee.rel_type IN(1, 2, 3, 4, 5, 6) AND ee.equipment_status = 1 "
//					+"	LEFT JOIN entry_equipment AS tractor ON tractor.check_in_entry_id = dlo.dlo_id AND tractor.equipment_type = 1 "
//					+"	LEFT JOIN entry_equipment AS trailer ON trailer.check_in_entry_id = dlo.dlo_id AND trailer.equipment_type = 2 "
//					+"	LEFT JOIN door_or_location_occupancy_details AS dlod ON dlo.dlo_id = dlod.dlo_id "
//					+"	LEFT JOIN space_resources_relation AS srr ON ee.equipment_id = srr.relation_id AND srr.relation_type = 1 " 
//					+"	LEFT JOIN storage_yard_control AS syc ON syc.yc_id = srr.resources_id AND srr.resources_type = 2 "
//					+"	LEFT JOIN storage_door AS sd ON sd.sd_id = srr.resources_id AND srr.resources_type = 1 "
//					+"	WHERE	dlo.ps_id = "+ps_id+" AND dlod.dlo_detail_id IS NULL AND dlo.window_check_in_time IS NULL "
//					+"	GROUP BY dlo.dlo_id "
					+"	ORDER BY start_time";
		
		
 		DBRow[] result  = null ;
		if(pc != null){
			result = dbUtilAutoTran.selectMutliple(sql,pc);
		}else{
			result = dbUtilAutoTran.selectMutliple(sql);
		}
		
 		return startTimeUTCToLocation(result, ps_id);
	}
	
	public DBRow[] noTaskEntry(long ps_id,PageCtrl pc)
		throws Exception
	{
		String sql = "select dlo.dlo_id,dlo.gate_check_in_time as start_time,GROUP_CONCAT(DISTINCT CONCAT(CASE srr.resources_type WHEN 1 THEN 'DOOR  ' WHEN 2 THEN 'SPOT  ' END,IFNULL(syc.yc_no,sd.doorId))) as locations,tractor.equipment_number as tractor,GROUP_CONCAT(DISTINCT trailer.equipment_number order by trailer.equipment_id) as trailer from door_or_location_occupancy_main as dlo "
					+"join entry_equipment as ee on ee.check_in_entry_id = dlo.dlo_id and ee.rel_type in (1,2,3,4,5,6) and ee.equipment_status = 1 "
					+"left join entry_equipment as tractor on tractor.check_in_entry_id = dlo.dlo_id and tractor.equipment_type = 1 "
					+"left join entry_equipment as trailer on trailer.check_in_entry_id = dlo.dlo_id and trailer.equipment_type = 2 "
					+"left join door_or_location_occupancy_details as dlod on dlo.dlo_id = dlod.dlo_id " 
					+"left join space_resources_relation as srr on ee.equipment_id = srr.relation_id and srr.relation_type = 1 "
					+"LEFT JOIN storage_yard_control AS syc ON syc.yc_id = srr.resources_id AND srr.resources_type = 2 "
					+"LEFT JOIN storage_door AS sd ON sd.sd_id = srr.resources_id AND srr.resources_type = 1 "
					+"where dlo.ps_id = "+ps_id+" and dlod.dlo_detail_id is null and dlo.window_check_in_time is null "
					+"GROUP BY dlo.dlo_id "
					+"order by start_time ";
		
		DBRow[] result = dbUtilAutoTran.selectMutliple(sql, pc);
		
		return startTimeUTCToLocation(result, ps_id);
	}

	/**
	 * 等待列表
	 * Window 右上
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] waitingList(long ps_id,PageCtrl pc)
		throws Exception
	{
		String sql = "select dlo.dlo_id,dlo.waiting,dlo.priority,dlo.gate_driver_name,dlo.gate_driver_liscense,ee.equipment_number,GROUP_CONCAT(dlod.number) as taskNumbers,window_check_in_time as start_time from door_or_location_occupancy_main as dlo " 
					+"join door_or_location_occupancy_details as dlod on dlo.dlo_id = dlod.dlo_id "
					+"left join entry_equipment as ee on ee.equipment_id = dlod.equipment_id  "
					+"where dlo.ps_id = "+ps_id+" and dlo.waiting in (1,2,3,4) and dlo.warehouse_check_in_time is NULL and ee.check_out_time is null "
					+"group by dlo.dlo_id "
					+"order by start_time asc";
		
		DBRow[] result = dbUtilAutoTran.selectMutliple(sql,pc);
		
		return startTimeUTCToLocation(result, ps_id);
	}
	
	/**
	 * 已经checkout了，忘记close的task
	 * @author zhanjie
	 * Window3
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] forgetCloseTask(long ps_id,PageCtrl pc)
		throws Exception
	{
 		String sql = "select dlod.number_type,dlod.number,ee.rel_type,dlo.dlo_id,GROUP_CONCAT(DISTINCT ad.employe_name order by ad.adid) as labor from door_or_location_occupancy_details as dlod " 
					+"join door_or_location_occupancy_main as dlo on dlod.dlo_id = dlo.dlo_id " 
					+"left join entry_equipment as ee on ee.equipment_id  = dlod.equipment_id "
					+"left join `schedule` as s on s.associate_id = dlod.dlo_detail_id and s.associate_process = "+ProcessKey.CHECK_IN_WAREHOUSE+" "
					+"left join schedule_sub as ss on s.schedule_id = ss.schedule_id "
					+"left join admin as ad on ad.adid = ss.schedule_execute_id "
					+"where dlo.ps_id = "+ps_id+" and dlod.number_status in ("+CheckInChildDocumentsStatusTypeKey.UNPROCESS+","+CheckInChildDocumentsStatusTypeKey.PROCESSING+") and dlod.is_forget_task = 1 "
					+"GROUP BY dlod.dlo_detail_id";

		DBRow[] result = dbUtilAutoTran.selectMutliple(sql,pc);
		 if(result != null){
			 for(DBRow temp : result){
				 InOrOut(temp);
			 }
		 }
		return result;
	}
	
	public DBRow[] leaving(long ps_id,PageCtrl pc)
		throws Exception
	{
		String sql = "select dlo.dlo_id,dlo.rel_id,dlo.out_seal,GROUP_CONCAT(sd.doorId order by sd.sd_id asc) as doorIds,dlo.check_out_time as start_time from door_or_location_occupancy_main as dlo " 
					+"join door_or_location_occupancy_details as dlod on dlo.dlo_id = dlod.dlo_detail_id "
					+"join storage_door as sd on sd.sd_id = dlod.rl_id and dlod.occupancy_type = "+OccupyTypeKey.DOOR+" "
					+"join ( select dlo_id,"
					+ "		MAX(CASE number_status WHEN 'closed' THEN status_count END ) closeCount,"
					+"		MAX(CASE number_status WHEN 'all' THEN status_count END ) allCount "
					+"from ( "
					+"select dlod.dlo_id,dlo.gate_liscense_plate,dlo.check_out_time,count(dlo_detail_id) as status_count,GROUP_CONCAT(dlod.number order by dlod.dlo_detail_id asc) as taskNumber,'closed' as number_status from door_or_location_occupancy_details as dlod "
					+"join door_or_location_occupancy_main as dlo on dlod.dlo_id = dlo.dlo_id and dlo.ps_id = ? "
					+"where number_status not in ("+CheckInChildDocumentsStatusTypeKey.UNPROCESS+","+CheckInChildDocumentsStatusTypeKey.PROCESSING+") " 
					+"GROUP BY dlo_id "
					+"union all " 
					+"select dlod.dlo_id,dlo.gate_liscense_plate,dlo.check_out_time,count(dlo_detail_id) as status_count,GROUP_CONCAT(dlod.number order by dlod.dlo_detail_id asc) as taskNumber,'all' as number_status from door_or_location_occupancy_details as dlod "
					+" join door_or_location_occupancy_main as dlo on dlod.dlo_id = dlo.dlo_id and dlo.ps_id = ? "
					+"where dlo.gate_check_in_time is not null and dlo.window_check_in_time is not NULL and dlo.check_out_time is null "
					+"GROUP BY dlo_id order by dlo_id "
					+")as allStatusView GROUP BY dlo_id HAVING closeCount = allCount " 
					+") as leavingView on dlo.dlo_id = leavingView.dlo_id "
					+"GROUP BY dlo.dlo_id"; 
		
		DBRow para = new DBRow();
		para.add("1_ps_id",ps_id);
		para.add("2_ps_id",ps_id);
		
		DBRow[] result = dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		return startTimeUTCToLocation(result, ps_id);
	}
	
	
	/**
	 * title miss location or remove
	 * Window4
	 * @param ps_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] forgetCheckOut(long ps_id,PageCtrl pc)
		throws Exception
	{
		String sql = "select forgetView.dlo_id,gate_container_no,taskNumbers,Max(cil.operator_time)as lastOperationTime,patrol_create,patrol_lost from (" 
					+"select noDoorView.dlo_id,noDoorView.gate_container_no,noDoorView.taskNumbers,noDoorView.patrol_create,noDoorView.patrol_lost from " 
					+"( "
					+"select dlo.dlo_id,dlo.gate_container_no,GROUP_CONCAT(dlod.number order by dlod.dlo_detail_id asc)as taskNumbers,dlo.patrol_create,dlo.patrol_lost from door_or_location_occupancy_main as dlo "
					+"left join door_or_location_occupancy_details as dlod on dlo.dlo_id = dlod.dlo_id "
					+"left join storage_door as sd on dlo.dlo_id = sd.associate_id and sd.associate_type = "+ModuleKey.CHECK_IN+" "
					+"where dlo.`status` in ("+CheckInMainDocumentsStatusTypeKey.UNPROCESS+","+CheckInMainDocumentsStatusTypeKey.PROCESSING+","+CheckInMainDocumentsStatusTypeKey.INYARD+") and dlo.ps_id = ?  and sd.sd_id is null and dlo.rel_type in ("+CheckInMainDocumentsRelTypeKey.DELIVERY+","+CheckInMainDocumentsRelTypeKey.PICK_UP+","+CheckInMainDocumentsRelTypeKey.BOTH+","+CheckInMainDocumentsRelTypeKey.CTNR+","+CheckInMainDocumentsRelTypeKey.PATROL+") "
					+"group by dlo.dlo_id "
					+") as noDoorView "
					+"join ( "
					+"select dlo.dlo_id,dlo.gate_container_no,GROUP_CONCAT(dlod.number order by dlod.dlo_detail_id asc)as taskNumbers,dlo.patrol_create,dlo.patrol_lost from door_or_location_occupancy_main as dlo "
					+"left join door_or_location_occupancy_details as dlod on dlo.dlo_id = dlod.dlo_id "
					+"left join storage_yard_control as syc on dlo.dlo_id = syc.associate_id and syc.associate_type = "+ModuleKey.CHECK_IN+" "
					+"where dlo.`status` in ("+CheckInMainDocumentsStatusTypeKey.UNPROCESS+","+CheckInMainDocumentsStatusTypeKey.PROCESSING+","+CheckInMainDocumentsStatusTypeKey.INYARD+") and dlo.ps_id = ?  and syc.yc_id is null and dlo.rel_type in ("+CheckInMainDocumentsRelTypeKey.DELIVERY+","+CheckInMainDocumentsRelTypeKey.PICK_UP+","+CheckInMainDocumentsRelTypeKey.BOTH+","+CheckInMainDocumentsRelTypeKey.CTNR+","+CheckInMainDocumentsRelTypeKey.PATROL+") "
					+") as noSpotView on noDoorView.dlo_id = noSpotView.dlo_id"
					+") as forgetView "
					+"join "+ConfigBean.getStringValue("check_in_log")+" as cil on cil.dlo_id = forgetView.dlo_id "
					+"GROUP BY forgetView.dlo_id";
		
		DBRow para = new DBRow();
		para.add("1_ps_id",ps_id);
		para.add("2_ps_id",ps_id);
		
		DBRow[] result = dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		return lastOperationUTCToLocation(result, ps_id);
	}
	/**
	 * 获得所有没占用资源未离开的设备
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] noSpaceRelationEquipment(long ps_id,PageCtrl pc)
		throws Exception
	{
		String whereWareHouse = "";
		
		if (ps_id!=0)
		{
			whereWareHouse = " and dlo.ps_id = "+ps_id+" ";
		}
		
		String sql = "select ee.*,ee.check_in_time as start_time ,CONCAT(CASE ee.original_resource_type WHEN "+OccupyTypeKey.DOOR+" THEN 'DOOR  ' WHEN "+OccupyTypeKey.SPOT+" THEN 'SPOT  ' END,IFNULL(syc.yc_no,sd.doorId)) as location from entry_equipment as ee "
					+"join door_or_location_occupancy_main as dlo on ee.check_in_entry_id = dlo.dlo_id "
					+"left join storage_yard_control syc on ee.original_resource_id=syc.yc_id and ee.original_resource_type="+OccupyTypeKey.SPOT+" "
					+"left join storage_door sd on ee.original_resource_id=sd.sd_id and ee.original_resource_type="+OccupyTypeKey.DOOR+" "
					+"left join space_resources_relation as srr on srr.relation_id = ee.equipment_id and srr.relation_type = "+SpaceRelationTypeKey.Equipment+" "
					+"where ee.equipment_status in ("+CheckInMainDocumentsStatusTypeKey.UNPROCESS+","+CheckInMainDocumentsStatusTypeKey.PROCESSING+","+CheckInMainDocumentsStatusTypeKey.INYARD+","+CheckInMainDocumentsStatusTypeKey.LEAVING+") "
					+"and ee.equipment_purpose in (1,2,4) and srr.srr_id is NULL "+whereWareHouse+" "
					+"order by ee.check_in_time asc";
		DBRow[] rows = dbUtilAutoTran.selectMutliple(sql,pc);
		for(DBRow row : rows){
			eqiupmentStatus(row);
		}
		return startTimeUTCToLocation(rows,ps_id);
	}
	private DBRow[] lastOperationUTCToLocation(DBRow[] result,long ps_id)
		throws Exception
	{
		for (int i = 0; i < result.length; i++) 
		{
			result[i].add("lastOperationTime",DateUtil.showLocalparseDateTo24Hours(result[i].getString("start_time"),ps_id));
			InOrOut(result[i]);
		}
		
		return result;
	}
	
	private void InOrOut(DBRow temp){
		/*public static int DELIVERY = 1;
		public static int PICK_UP = 2;
		public static int BOTH = 3;
		public static int NONE = 4;
		public static int PICKUP_CTNR = 5;
		public static int PATROL = 6;*/
		
		if(temp.containsKey("rel_type")){
			String strRelType = "" ;
			int rel_type = temp.get("rel_type", 0);
			switch (rel_type) {
				case 1: strRelType = "In" ; break;
				case 2: strRelType = "Out" ; break;
				case 3: strRelType = "I/O"; break;
				case 4: strRelType = "Unknown" ;break;
				case 5: strRelType = "CTNR" ; break;
				case 6: strRelType = "??" ;break;
				case 7: strRelType = "Visitor/Parking" ;break;
				default: break;
			}
			temp.add("rel_type", strRelType);
		}
	}
	private void eqiupmentStatus(DBRow temp){
		CheckInMainDocumentsStatusTypeKey  checkInMainDocumentsStatusTypeKey = new CheckInMainDocumentsStatusTypeKey();
		if(temp.containsKey("equipment_status")){
			int equipmentStatus = temp.get("equipment_status", 0);
			temp.add("equipment_status", checkInMainDocumentsStatusTypeKey.getContainerTypeKeyValue(equipmentStatus));
		}
	}
	private DBRow[] startTimeUTCToLocation(DBRow[] result,long ps_id)
		throws Exception
	{
		for (int i = 0; result != null && i < result.length; i++) 
		{
			InOrOut(result[i]);
			if(result[i].containsKey("Location")){
				String fixLocation = formateLocation(result[i].getString("Location"));
				result[i].add("Location", fixLocation);
 			}
			result[i].add("total_time", costMinute(result[i].getString("start_time")));
			result[i].remove("start_time");
		}
		
		return result;
	}
	private void formateLocation(DBRow[] result){
		if(result != null && result.length > 0){
			for(DBRow temp : result ){
				if(temp.containsKey("Location")){
					String fixLocation = formateLocation(temp.getString("Location"));
					temp.add("Location", fixLocation);
	 			}
			}
		}
 	}
	
	private String formateLocation(String location) {
		if(StringUtils.isNotBlank(location)){
			String target = location.replaceAll("\\d", "");
			String number = location.replaceAll("\\D", "");
			String  prex = StringUtils.isNotBlank(target)?  String.format("%8s", target)+" " : "";
			return prex + number ;
		}
		return location ;
	}
	private long costMinute(String start_time){
		try{
			//如果start_time 为null ||  或者时间格式错误 那么返回-1
			long currentLongTime =	new Date().getTime() ;
			long startLongTime =  DateUtil.createDateTime(start_time).getTime() ;
			long lastTime = (currentLongTime - startLongTime)/60000;
			return lastTime;
		}catch(Exception e){
			
		}
		return -1l;
	}
	
	/**
	 * 门进出日志报表
	 * @author zhanjie
	 * @param ps_id
	 * @param start_date
	 * @return
	 * @throws Exception
	 */
	public DBRow[] gateActivityReport(long ps_id,String start_date,String end_date)
		throws Exception
	{
		String sql = "select DISTINCT dlo.gate_check_in_time as activity_time,ee.check_in_entry_id as entry_id,'In' as 'in_out',tractor.equipment_number as tractor,dlo.company_name as carrier,dlo.gate_driver_liscense as driver_liscense,dlo.gate_driver_name as driver_name,trailer.equipment_number as trailer " 
					+"from door_or_location_occupancy_main as dlo "
					+"join entry_equipment as ee on ee.check_in_entry_id = dlo.dlo_id "
					+"left join entry_equipment as tractor on tractor.check_in_entry_id = dlo.dlo_id and tractor.equipment_type = 1 "
					+"left join entry_equipment as trailer on trailer.check_in_entry_id = dlo.dlo_id and trailer.equipment_type = 2 "
					+"where ee.ps_id = "+ps_id+" " 
					+"having activity_time between STR_TO_DATE('"+start_date+"','%Y-%m-%d %H:%i:%s') and STR_TO_DATE('"+end_date+"','%Y-%m-%d %H:%i:%s') "
					+"union " 
					+"select DISTINCT dlo.check_out_time as activity_time,ee.check_in_entry_id as entry_id,'Out' as 'in_out',tractor.equipment_number as tractor,dlo.company_name as carrier,dlo.gate_driver_liscense as driver_liscense,dlo.gate_driver_name as driver_name,trailer.equipment_number as trailer " 
					+"from door_or_location_occupancy_main as dlo "
					+"join entry_equipment as ee on ee.check_in_entry_id = dlo.dlo_id "
					+"left join entry_equipment as tractor on tractor.check_out_entry_id = dlo.dlo_id and tractor.equipment_type = 1 "
					+"left join entry_equipment as trailer on trailer.check_out_entry_id = dlo.dlo_id and trailer.equipment_type = 2 "
					+"where ee.ps_id = "+ps_id+" " 
					+"having activity_time between STR_TO_DATE('"+start_date+"','%Y-%m-%d %H:%i:%s') and STR_TO_DATE('"+end_date+"','%Y-%m-%d %H:%i:%s') "
					+"order by activity_time asc ";
		
		DBRow[] rows = dbUtilAutoTran.selectMutliple(sql); 
		
		for (int i = 0; i < rows.length; i++) 
		{
			rows[i].add("activity_time",DateUtil.locationZoneTimeString(rows[i].getString("activity_time"), ps_id));
		}
		
		return rows; 
	}
	
	/**
	 * 任务活动报表
	 * @author zhanjie
	 * @param ps_id
	 * @param start_date
	 * @return
	 * @throws Exception
	 */
	public DBRow[] taskActivityReport(long ps_id,String start_date,String end_date)
		throws Exception
	{
		String sql = "SELECT "
					+"	dlod.customer_id,"
					+"	dlod.number as order_no,dlod.number_type as order_type,"
					+"  dlod.number_status,dlod.receipt_no,"
					+"  dlod.dlo_detail_id,"
					+"	ee.equipment_number,"
					+"	ee.equipment_id,"
					+"	dlo.waiting,"
					+"	ee.check_in_window_time as equipment_window_in_time,"
					+"	ee.check_in_warehouse_time AS equipment_dock_in_time,"
					+"	ee.check_out_warehouse_time AS equipment_dock_out_time,"
					+"	CONCAT(CASE dlod.occupancy_type WHEN 1 THEN 'DOOR' WHEN 2 THEN 'SPOT' END,IFNULL(syc.yc_no, sd.doorId)) as doorId,"
					+"	dlod.handle_time AS task_start_time,"
					+"	dlod.finish_time AS task_finish_time,"
					+"	timestampdiff(SECOND,dlod.handle_time,dlod.finish_time)AS task_use_time,"
					+"	IFNULL(sum(wpc.wms_pallet_type_count) ,count(rlc.receipt_rel_container_id)) as task_total_pallet_count,"
					+"	FLOOR((timestampdiff(SECOND,dlod.handle_time,dlod.finish_time)/IFNULL(sum(wpc.wms_pallet_type_count) ,count(rlc.receipt_rel_container_id))))as avg_pallet_use_time,"
					+"	GROUP_CONCAT(DISTINCT ad.employe_name order by ad.adid asc) as labor "
					+"FROM "+ConfigBean.getStringValue("door_or_location_occupancy_details")+" AS dlod "
					+"join "+ConfigBean.getStringValue("door_or_location_occupancy_main")+" as dlo on dlo.dlo_id = dlod.dlo_id "
					+"left join "+ConfigBean.getStringValue("entry_equipment")+" as ee on dlod.equipment_id = ee.equipment_id "
					+"left join `"+ConfigBean.getStringValue("schedule")+"` as s on s.associate_id = dlod.dlo_detail_id and (s.associate_process = "+ProcessKey.CHECK_IN_ANDROID_WAREHOUSE+")"
					+"left join "+ConfigBean.getStringValue("schedule_sub")+" as ss on s.schedule_id = ss.schedule_id " 
					+"left join "+ConfigBean.getStringValue("admin")+" as ad on ad.adid = ss.schedule_execute_id " 
					+"left join "+ConfigBean.getStringValue("storage_door")+" as sd on dlod.rl_id = sd.sd_id and dlod.occupancy_type = "+OccupyTypeKey.DOOR+" "
					+"left join "+ConfigBean.getStringValue("storage_yard_control")+" as syc on dlod.rl_id = syc.yc_id and dlod.occupancy_type = "+OccupyTypeKey.SPOT+" "
//					+"left join "+ConfigBean.getStringValue("wms_load_order")+" as wlo on dlod.dlo_detail_id = wlo.dlo_detail_id "
					+"left join "+ConfigBean.getStringValue("wms_load_order_pallet_type_count")+" as wpc  on wpc.dlo_detail_id = dlod.dlo_detail_id "
					+"left join receipt_rel_container as rlc on rlc.detail_id=dlod.dlo_detail_id and (rlc.damage_qty!=0 or rlc.normal_qty!=0 ) "
					+"where dlod.handle_time BETWEEN STR_TO_DATE('"+start_date+"','%Y-%m-%d %H:%i:%s') and STR_TO_DATE('"+end_date+"','%Y-%m-%d %H:%i:%s') and  dlo.ps_id="+ps_id+" "
					+"group by dlod.dlo_detail_id "
					+"order by ee.check_in_warehouse_time";
//					+"order by task_start_time";
		DBRow[] rows = dbUtilAutoTran.selectMutliple(sql);
		
		ModuleKey moduleKey = new ModuleKey();
		CheckInChildDocumentsStatusTypeKey numberStatus = new CheckInChildDocumentsStatusTypeKey();
		for (int i = 0; i < rows.length; i++) 
		{
			rows[i].add("equipment_window_in_time",DateUtil.locationZoneTimeString(rows[i].getString("equipment_window_in_time"), ps_id));
			rows[i].add("equipment_dock_in_time",DateUtil.locationZoneTimeString(rows[i].getString("equipment_dock_in_time"), ps_id));
			rows[i].add("equipment_dock_out_time",DateUtil.locationZoneTimeString(rows[i].getString("equipment_dock_out_time"), ps_id));
			rows[i].add("task_start_time",DateUtil.locationZoneTimeString(rows[i].getString("task_start_time"), ps_id));
			rows[i].add("task_finish_time",DateUtil.locationZoneTimeString(rows[i].getString("task_finish_time"), ps_id));
			rows[i].add("ordertype",rows[i].get("order_type",0));
			rows[i].add("order_type",moduleKey.getModuleName(rows[i].get("order_type",0)));
			rows[i].add("number_status",numberStatus.getContainerTypeKeyValue(rows[i].get("number_status",0)));
			
		}
		
		return rows;
	}
	
	/**
	 * 设备报表
	 * @zhanjie
	 * @param ps_id
	 * @param start_date
	 * @return
	 * @throws Exception
	 */
	public DBRow[] equipmentReport(long ps_id,String start_date,String end_date)
		throws Exception
	{
		String sql = "select dlo.dlo_id,GROUP_CONCAT(DISTINCT dlod.customer_id)as customers,'IN' as 'IN/OUT',1 as order_column,ee.equipment_type,ee.equipment_number,ee.check_in_entry_id,ee.check_in_time,ee.check_out_entry_id,ee.check_out_time from entry_equipment as ee " 
					+"join door_or_location_occupancy_main as dlo on ee.check_in_entry_id = dlo.dlo_id "
					+"left join door_or_location_occupancy_details as dlod on dlod.dlo_id = dlo.dlo_id "
					+"where  "
					+ "("
					+"		((ee.check_in_time BETWEEN STR_TO_DATE('"+start_date+"','%Y-%m-%d %H:%i:%s')AND STR_TO_DATE('"+end_date+"','%Y-%m-%d %H:%i:%s')) "
					+"		or " 
					+"		(ee.check_out_time BETWEEN STR_TO_DATE('"+start_date+"','%Y-%m-%d %H:%i:%s')AND STR_TO_DATE('"+end_date+"','%Y-%m-%d %H:%i:%s'))) and ee.ps_id= "+ps_id
					+"	)"
					+"group by ee.equipment_id "
					+"union " 
					+"select dlo.dlo_id,GROUP_CONCAT(DISTINCT dlod.customer_id)as customers,'OUT' as 'IN/OUT',2 as order_column,ee.equipment_type,ee.equipment_number,ee.check_in_entry_id,ee.check_in_time,ee.check_out_entry_id,ee.check_out_time from entry_equipment as ee " 
					+"join door_or_location_occupancy_main as dlo on ee.check_out_entry_id = dlo.dlo_id "
					+"left join door_or_location_occupancy_details as dlod on dlod.dlo_id = dlo.dlo_id "
					+"where  "
					+ "("
//					+"		(ee.check_in_time BETWEEN STR_TO_DATE('"+start_date+"','%Y-%m-%d %H:%i:%s')AND STR_TO_DATE('"+end_date+"','%Y-%m-%d %H:%i:%s')) "
//					+"		or " 
					+"		(ee.check_out_time BETWEEN STR_TO_DATE('"+start_date+"','%Y-%m-%d %H:%i:%s')AND STR_TO_DATE('"+end_date+"','%Y-%m-%d %H:%i:%s')) and ee.ps_id= "+ps_id
					+"	)"
					+"GROUP BY ee.equipment_id "
					+"order by dlo_id asc,order_column asc,equipment_type asc"; 
		DBRow[] rows = dbUtilAutoTran.selectMutliple(sql); 
		
		for (int i = 0; i < rows.length; i++) 
		{
			rows[i].add("check_in_time",DateUtil.locationZoneTimeString(rows[i].getString("check_in_time"), ps_id));
			rows[i].add("check_out_time",DateUtil.locationZoneTimeString(rows[i].getString("check_out_time"), ps_id));
		}
		
		
		return rows;
	}
	
	/**
	 * 获得任务的明细基于任务的开始时间
	 * @param start_date
	 * @param end_date
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTasksByStartDate(String start_date,String end_date)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("door_or_location_occupancy_details")+" where handle_time BETWEEN STR_TO_DATE('"+start_date+"','%Y-%m-%d %H:%i:%s') and STR_TO_DATE('"+end_date+"','%Y-%m-%d %H:%i:%s') ";
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	public DBRow[] scordOutBoundReport(long ps_id,String start_time,String end_time,PageCtrl pc)
		throws Exception
	{		
						//WMS有Appointment，我们没有操作记录
		String sql = "select orderView.ps_id,orderView.company_id,orderView.customer_id,orderView.supplier,orderView.ship_to,orderView.load_no,orderView.pono,orderView.order_no,CASE WHEN dlod.number_type = 18 THEN dlod.number END AS notfound,orderView.carrier,orderView.appointment_time,dlod.number_status, "
				    +"orderView.ship_to_zip_code,orderView.ship_to_address,orderView.ship_to_state ,"  
					+"dlo.mc_dot,dlo.company_name,dlo.waiting,ee.check_in_time,ee.check_in_window_time,ee.check_in_warehouse_time,ee.check_out_warehouse_time,ee.check_out_time,ee.equipment_number from " 
					+"( "
					+"	select orderPalletsView.ps_id,orderPalletsView.company_id,GROUP_CONCAT(DISTINCT pallets.customer_id) as customer_id,GROUP_CONCAT(DISTINCT pallets.supplier_id) as supplier,orderPalletsView.ship_to,orderPalletsView.load_no,orderPalletsView.pono,orderPalletsView.order_no,orderPalletsView.pallets,orderPalletsView.carrier,pallets.appointment_time ,"
					+"  orderPalletsView.ship_to_zip_code,orderPalletsView.ship_to_address,orderPalletsView.ship_to_state from "
					+"	("
					+"		select orderLineView.ps_id,orderLineView.company_id,orderLineView.customer_id,GROUP_CONCAT(DISTINCT orderLineView.supplier_id) as suppliers,orderLineView.ship_to,orderLineView.load_no,orderLineView.pono,orderLineView.order_no,SUM(orderLineView.line_pallets) as pallets,orderLineView.carrier, "
					+"      orderLineView.ship_to_zip_code,orderLineView.ship_to_address,orderLineView.ship_to_state from "  
					+"		( "
					+"			select apt.appointment_time,apt.bol_no,apt.carrier,apt.company_id,apt.container_no,apt.customer_id,apt.in_out,apt.line_no,apt.line_pallets,apt.load_no,apt.masterbol_no,apt.order_no,apt.pono,apt.ps_id,apt.ship_to,GROUP_CONCAT(DISTINCT apt.supplier_id order by supplier_id asc ) as supplier_id, "
					+"          apt.ship_to_zip_code,apt.ship_to_address,apt.ship_to_state from appointment_temp as apt "
					+"			where in_out = 1 and ps_id = "+ps_id+" " 
					+"			group by apt.company_id,apt.order_no,apt.line_no "
					+"		)as orderLineView "
					+"		group by company_id,order_no "
					+"	)as orderPalletsView "
					+"join appointment_temp as pallets on orderPalletsView.company_id = pallets.company_id and orderPalletsView.order_no = pallets.order_no "
					+"group by company_id,order_no,appointment_time "
					+") as orderView " 
					+"left join door_or_location_occupancy_details as dlod " 
					+"on orderView.company_id = dlod.company_id and ((orderView.load_no = dlod.number and orderView.load_no is not null and dlod.number_type = 10 ) or (orderView.order_no = dlod.number and dlod.number_type = 11 ) or (orderView.pono = dlod.number and dlod.number_type = 17 )) "
					+"left join entry_equipment as ee on ee.equipment_id = dlod.equipment_id "
					+"left join door_or_location_occupancy_main as dlo on dlo.dlo_id = dlod.dlo_id "
					+"where (orderView.appointment_time >='"+start_time+"' and orderView.appointment_time<='"+end_time+"') or (dlo.gate_check_in_time >='"+start_time+"' and dlo.gate_check_in_time <='"+end_time+"') "
					+"group by orderView.company_id,orderView.order_no,ee.equipment_id "
					+"union " 
				//我们有操作记录WMS没有Appointment的
					+"select DISTINCT dlo.ps_id,dlod.company_id,dlod.customer_id,dlod.supplier_id,null as ship_to, "
					+"CASE WHEN dlod.number_type = 10 THEN dlod.number END as load_no, "
					+"CASE WHEN dlod.number_type = 17 THEN dlod.number END as pono, "
					+"CASE WHEN dlod.number_type = 11 THEN dlod.number END as order_no, "
					+"CASE WHEN dlod.number_type = 18 THEN dlod.number END AS notfound, "
					+"dlo.company_name as carrier,dlod.APPOINTMENT_DATE as appointment_time,dlod.number_status,"
					+"null as ship_to_zip_code,null as ship_to_address,null as ship_to_state ,dlo.mc_dot, dlo.company_name, "
					+"dlo.waiting,ee.check_in_time,ee.check_in_window_time,ee.check_in_warehouse_time,ee.check_out_warehouse_time,ee.check_out_time,ee.equipment_number " 
					+"from door_or_location_occupancy_details as dlod " 
					+"join entry_equipment as ee on ee.equipment_id = dlod.equipment_id "
					+"join door_or_location_occupancy_main as dlo on dlo.dlo_id = dlod.dlo_id "
					+"left join appointment_temp as orderLineView on dlod.company_id = orderLineView.company_id and orderLineView.in_out = 1 and ((orderLineView.load_no = dlod.number and orderLineView.load_no is not null and dlod.number_type = 10) or (orderLineView.order_no = dlod.number and dlod.number_type = 11) or (orderLineView.pono = dlod.number and dlod.number_type = 17)) "
					+"where dlo.ps_id = "+ps_id+" and dlo.gate_check_in_time >='"+start_time+"' and dlo.gate_check_in_time <= '"+end_time+"' and dlod.number_type in (10,11,17) and orderLineView.order_no is null "
					+"group by dlod.company_id,dlod.number,dlod.number_type,ee.equipment_id "
					+"order by appointment_time asc,load_no,order_no ";  
		
		DBRow[] rows = dbUtilAutoTran.selectMutliple(sql, pc);
		
		for (int i = 0; i < rows.length; i++) 
		{
			rows[i].add("appointment_time",DateUtil.locationZoneTimeString(rows[i].getString("appointment_time"), ps_id));
			rows[i].add("check_in_time",DateUtil.locationZoneTimeString(rows[i].getString("check_in_time"), ps_id));
			rows[i].add("check_in_window_time",DateUtil.locationZoneTimeString(rows[i].getString("check_in_window_time"), ps_id));
			rows[i].add("check_in_warehouse_time",DateUtil.locationZoneTimeString(rows[i].getString("check_in_warehouse_time"), ps_id));
			rows[i].add("check_out_warehouse_time",DateUtil.locationZoneTimeString(rows[i].getString("check_out_warehouse_time"), ps_id));
			rows[i].add("check_out_time",DateUtil.locationZoneTimeString(rows[i].getString("check_out_time"), ps_id));
		}
		
		return rows;
	}
	
	public DBRow[] scordOutBoundReport(long ps_id,String start_time,String end_time)throws Exception{		
			String sql = "CALL p_outbound_report(" + ps_id + ",'" + start_time + "','" + end_time + "')";
			System.out.println(sql);
			DBRow[] rows = dbUtilAutoTran.selectMutliple(sql);
			for (int i = 0; i < rows.length; i++) {
				rows[i].add("appointment_time",DateUtil.locationZoneTimeString(rows[i].getString("appointment_time"), ps_id));
				rows[i].add("check_in_time",DateUtil.locationZoneTimeString(rows[i].getString("check_in_time"), ps_id));
				rows[i].add("check_in_window_time",DateUtil.locationZoneTimeString(rows[i].getString("check_in_window_time"), ps_id));
				rows[i].add("check_in_warehouse_time",DateUtil.locationZoneTimeString(rows[i].getString("check_in_warehouse_time"), ps_id));
				rows[i].add("check_out_warehouse_time",DateUtil.locationZoneTimeString(rows[i].getString("check_out_warehouse_time"), ps_id));
				rows[i].add("check_out_time",DateUtil.locationZoneTimeString(rows[i].getString("check_out_time"), ps_id));
			}
			
			return rows;
		}
	
	
	public DBRow[] scordInBoundReport(long ps_id,String start_time,String end_time,PageCtrl pc)
		throws Exception
	{
		String sql = "select apt.company_id,apt.customer_id,apt.supplier_id,apt.receipt_no,dlod.number_status, apt.container_no,apt.bol_no,"
				    +"CASE WHEN dlod.number_type = 14 THEN dlod.number END AS notfound,  apt.reference_no, "
				    +"apt.carrier,apt.appointment_time,dlo.company_name,dlo.waiting,ee.check_in_time,ee.check_in_window_time,ee.check_in_warehouse_time, "
				    +"ee.check_out_warehouse_time,ee.check_out_time,ee.equipment_number  from appointment_temp as apt " 
					+"left join door_or_location_occupancy_details as dlod on dlod.receipt_no = apt.receipt_no and dlod.number_type in (12,13,14) "
					+"left join entry_equipment as ee on ee.equipment_id = dlod.equipment_id "
					+"left join door_or_location_occupancy_main as dlo on dlo.dlo_id = dlod.dlo_id "
					+"where apt.in_out = 2 and apt.ps_id = "+ps_id+" and apt.appointment_time >= '"+start_time+"' and apt.appointment_time <='"+end_time+"' "
					+"group by apt.company_id,apt.receipt_no,ee.equipment_id "
					+"union " 
					+"select dlod.company_id,dlod.customer_id,dlod.supplier_id,dlod.receipt_no,dlod.number_status, "
					+"CASE WHEN dlod.number_type = 12 THEN dlod.number END as container_no, "
					+"CASE WHEN dlod.number_type = 13 THEN dlod.number END as bol_no, "
					+"CASE WHEN dlod.number_type = 14 THEN dlod.number END AS notfound, "
					+"apt.reference_no,dlod.number,dlod.APPOINTMENT_DATE,dlo.company_name,dlo.waiting,ee.check_in_time,ee.check_in_window_time,"
					+"ee.check_in_warehouse_time,ee.check_out_warehouse_time,ee.check_out_time,ee.equipment_number "
					+"from door_or_location_occupancy_details as dlod "
					+"join entry_equipment as ee on dlod.equipment_id = ee.equipment_id "
					+"join door_or_location_occupancy_main as dlo on dlo.dlo_id = dlod.dlo_id "
					+"left join appointment_temp as apt on dlod.receipt_no = apt.receipt_no and dlod.company_id = apt.company_id "
					+"where apt.appointment_id is null and dlod.number_type in (12,13,14) and dlo.ps_id = "+ps_id+" and dlo.gate_check_in_time >= '"+start_time+"' and dlo.gate_check_in_time <='"+end_time+"' "
					+"group by dlod.company_id,dlod.number,dlod.number_type,ee.equipment_id "
					+"order by appointment_time asc,receipt_no";
		
		DBRow[] rows = dbUtilAutoTran.selectMutliple(sql,pc); 
		
		for (int i = 0; i < rows.length; i++) 
		{
			rows[i].add("appointment_time",DateUtil.locationZoneTimeString(rows[i].getString("appointment_time"), ps_id));
			rows[i].add("check_in_time",DateUtil.locationZoneTimeString(rows[i].getString("check_in_time"), ps_id));
			rows[i].add("check_in_window_time",DateUtil.locationZoneTimeString(rows[i].getString("check_in_window_time"), ps_id));
			rows[i].add("check_in_warehouse_time",DateUtil.locationZoneTimeString(rows[i].getString("check_in_warehouse_time"), ps_id));
			rows[i].add("check_out_time",DateUtil.locationZoneTimeString(rows[i].getString("check_out_time"), ps_id));
		}
		
		return rows;
	}
	
	public DBRow[] scordInBoundReport(long ps_id,String start_time,String end_time)throws Exception{
			String sql = "CALL p_inbound_report(" + ps_id + ",'" + start_time + "','" + end_time + "')";
			System.out.println(sql);
			DBRow[] rows = dbUtilAutoTran.selectMutliple(sql); 
			
			for (int i = 0; i < rows.length; i++) {
				rows[i].add("appointment_time",DateUtil.locationZoneTimeString(rows[i].getString("appointment_time"), ps_id));
				rows[i].add("check_in_time",DateUtil.locationZoneTimeString(rows[i].getString("check_in_time"), ps_id));
				rows[i].add("check_in_window_time",DateUtil.locationZoneTimeString(rows[i].getString("check_in_window_time"), ps_id));
				rows[i].add("check_in_warehouse_time",DateUtil.locationZoneTimeString(rows[i].getString("check_in_warehouse_time"), ps_id));
				rows[i].add("check_out_time",DateUtil.locationZoneTimeString(rows[i].getString("check_out_time"), ps_id));
			}
			
			return rows;
		}
		
	
	public DBRow[] scheduleOutboundTomorrow(long ps_id,String start_time,String end_time,PageCtrl pc)
		throws Exception{
		String sql = "select orderView.ps_id,orderView.company_id,orderView.customer_id,orderView.supplier,orderView.ship_to,orderView.load_no,orderView.pono,orderView.order_no,orderView.reference_no,orderView.carrier,orderView.appointment_time,orderView.pallets from "
					+"("
					+"	select orderPalletsView.ps_id,orderPalletsView.company_id,GROUP_CONCAT(DISTINCT pallets.customer_id) as customer_id,GROUP_CONCAT(DISTINCT pallets.supplier_id) as supplier,orderPalletsView.ship_to,orderPalletsView.load_no,orderPalletsView.pono,orderPalletsView.order_no,orderPalletsView.reference_no,orderPalletsView.pallets,orderPalletsView.carrier,pallets.appointment_time from " 
					+"	(" 
					+"		select orderLineView.ps_id,orderLineView.company_id,orderLineView.customer_id,GROUP_CONCAT(DISTINCT orderLineView.supplier_id) as suppliers,orderLineView.ship_to,orderLineView.load_no,orderLineView.pono,orderLineView.order_no,orderLineView.reference_no,SUM(orderLineView.line_pallets) as pallets,orderLineView.carrier from " 
					+"		("
					+"			select apt.appointment_time,apt.bol_no,apt.carrier,apt.company_id,apt.container_no,apt.customer_id,apt.in_out,apt.line_no,apt.line_pallets,apt.load_no,apt.masterbol_no,apt.order_no,apt.pono,apt.reference_no,apt.ps_id,apt.ship_to,GROUP_CONCAT(DISTINCT apt.supplier_id order by supplier_id asc ) as supplier_id from appointment_temp as apt "
					+"			where in_out = 1  and ps_id = "+ps_id+" " 
					+"			group by apt.company_id,apt.order_no,apt.line_no "
					+"		)as orderLineView "
					+"	group by company_id,order_no "
					+"	)as orderPalletsView "
					+"	join appointment_temp as pallets on orderPalletsView.company_id = pallets.company_id and orderPalletsView.order_no = pallets.order_no "
					+"	group by company_id,order_no "
					+") as orderView " 
					+"where orderView.appointment_time >='"+start_time+"' and orderView.appointment_time<='"+end_time+"' "
					+"group by orderView.company_id,orderView.order_no "
					+ "order by appointment_time asc ";
		System.out.println(sql);
		DBRow[] rows = dbUtilAutoTran.selectMutliple(sql,pc); 
		
		for (int i = 0; i < rows.length; i++) 
		{
			rows[i].add("appointment_time",DateUtil.locationZoneTimeString(rows[i].getString("appointment_time"), ps_id));
		}
		
		return rows;
	}
	
	public DBRow[] scheduleInboundTomorrow(long ps_id,String start_time,String end_time,PageCtrl pc)
		throws Exception
	{
		String sql = "select apt.company_id,apt.customer_id,apt.supplier_id,apt.receipt_no,apt.container_no,apt.bol_no,apt.reference_no,apt.carrier,apt.appointment_time,SUM(apt.line_pallets) as pallets from appointment_temp as apt " 
					+"where apt.in_out = 2 and apt.ps_id = "+ps_id+" and apt.appointment_time >= '"+start_time+"' and apt.appointment_time <='"+end_time+"' "
					+"group by apt.company_id,apt.receipt_no "
					+"order by appointment_time asc ";
		
		DBRow[] rows = dbUtilAutoTran.selectMutliple(sql,pc); 
		
		for (int i = 0; i < rows.length; i++) 
		{
			rows[i].add("appointment_time",DateUtil.locationZoneTimeString(rows[i].getString("appointment_time"), ps_id));
		}
		
		return rows;
	}
	public DBRow findlaborsByTaskId(long detail_id)
			throws Exception
		{
			String sql = "select GROUP_CONCAT(DISTINCT ad.employe_name order by ad.adid asc) as labor from wms_load_order_pallet_type_count wloc "
					   + "left join admin ad on ad.adid=wloc.scan_adid where dlo_detail_id="+detail_id;
			
			
			return dbUtilAutoTran.selectSingle(sql);
		}
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
}
