package com.cwc.app.floor.api.zj;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.key.ClearanceKey;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.DeclarationKey;
import com.cwc.app.key.DifferentKey;
import com.cwc.app.key.JqGridFilterKey;
import com.cwc.app.key.ProductStorageTypeKey;
import com.cwc.app.key.SimulationTransportKey;
import com.cwc.app.key.TransportCertificateKey;
import com.cwc.app.key.TransportLogTypeKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.key.TransportProductFileKey;
import com.cwc.app.key.TransportQualityInspectionKey;
import com.cwc.app.key.TransportTagKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorTransportMgrZJ {
	static Logger log = Logger.getLogger("ACTION");
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加转运单
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addTransport(DBRow dbrow)
		throws Exception
	{
		try
		{
			long transport_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("transport"));
			dbrow.add("transport_id",transport_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("transport"),dbrow);
			
			return transport_id;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ addTransport error:"+e);
		}
	}
	/**
	 * 修改转运单
	 * @param transport_id
	 * @param para
	 * @throws Exception
	 */
	public void modTransport(long transport_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where transport_id="+transport_id,ConfigBean.getStringValue("transport"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ modTransport error:"+e);
		}
	}
	
	/**
	 * 删除转运单
	 * @param transport_id
	 * @throws Exception
	 */
	public void delTransport(long transport_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where transport_id="+transport_id,ConfigBean.getStringValue("transport"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ delTransport error:"+e);
		}
	}
	
	/**
	 * 添加转运单详细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addTransportDetail(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long transport_dettail_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("transport_detail"));
			
			dbrow.add("transport_detail_id",transport_dettail_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("transport_detail"),dbrow);
			
			return (transport_dettail_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ addTransportDetail error:"+e);
		}
	}
	
	/**
	 * 修改转运单详细
	 * @param transport_detail_id
	 * @param para
	 * @throws Exception
	 */
	public void modTransportDetail(long transport_detail_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where transport_detail_id="+transport_detail_id,ConfigBean.getStringValue("transport_detail"),para);
		}
		catch (Exception e)
		{
			throw new Exception("FloorTransportMgrZJ modTransportDetail error:"+e);
		}
	}
	
	/**
	 * 删除转运单详细
	 * @param transport_detail_id
	 * @throws Exception
	 */
	public void delTransportDetail(long transport_detail_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where transport_detail_id="+transport_detail_id,ConfigBean.getStringValue("transport_detail"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ delTransportDetail error:"+e);
		}
	}
	
	/**
	 * 删除转运单明细(根据转运单号批量删除)
	 * @param transport_id
	 * @throws Exception
	 */
	public void delTransportDetailByTransportId(long transport_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where transport_id="+transport_id,ConfigBean.getStringValue("transport_detail"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ delTransportDetailByTransportId error:"+e);
		}
	}
	
	/**
	 * 过滤转运单（支持根据转运仓库、目的仓库、转运单状态过滤）
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterTransport(long send_psid,long receive_psid,PageCtrl pc,int status, int declaration, int clearance,int invoice, int drawback, int day,int stock_in_set, long create_account_id)
		throws Exception
	{
		StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("transport")+" where 1=1");
		TDate tDate = new TDate();
		tDate.addDay(-day);
		if(day!=0)
		{
			sql.append(" and updatedate<= '"+tDate.formatDate("yyyy-MM-dd")+"' ");
		}
		
		if(send_psid !=0)//转运仓库
		{
			sql.append(" and send_psid="+send_psid);
		}
		
		if(receive_psid!=0)
		{
			sql.append(" and receive_psid="+receive_psid);
		}
		
		if(status!=0 )
		{
			if(status==7)
				sql.append(" and transport_status in("+TransportOrderKey.READY + "," + TransportOrderKey.INTRANSIT + "," + TransportOrderKey.APPROVEING + "," + TransportOrderKey.PACKING + ")");
			else
				sql.append(" and transport_status="+status);
		}
		
		if(declaration!=0) {
			sql.append(" and IFNULL(declaration,1)="+declaration);
		}
		if(clearance!=0) {
			sql.append(" and IFNULL(clearance,1)="+clearance);
		}
		if(invoice!=0) {
			sql.append(" and IFNULL(invoice,1)="+invoice);
		}
		if(drawback!=0) {
				sql.append(" and IFNULL(drawback,1)="+drawback);
		}
		if(stock_in_set != 0) {
			sql.append(" and IFNULL(stock_in_set,1) ="+stock_in_set);
		}
		if(create_account_id != 0) {
			sql.append(" and create_account_id = " + create_account_id);
		}
		sql.append(" order by transport_date desc");
		
		return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
	}
	
	/**
	 * 根据转运单ID获得转运单
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailTransportById(long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport")+" where transport_id = ?";
			
			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ delTransportDetail error:"+e);
		}
	}
	
	/**
	 * 搜索转运单
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchTransportById(String key,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String transportId = "";
			if (key.substring(0,1).toUpperCase().equals("T"))
			{
				String regEx = "[^0-9]";
				Pattern p = Pattern.compile(regEx);
				Matcher m = p.matcher(key);
				if (!m.replaceAll("").equals("")) 
				{
					transportId = m.replaceAll("");
				}
				
				if(transportId.equals("")|| (key.length() - transportId.length()) != 1) 
				{
					transportId = key;
				}
			}
			else
			{
				transportId = key;
			}
			
			String sql = "select * from "+ConfigBean.getStringValue("transport")+" where transport_id = ?";
			
			DBRow para = new DBRow();
			para.add("transport_id",transportId);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para,pc));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ searchTransportById error:"+e);
		}
	}
	
	/**
	 * 根据转运单号查询转运单明细
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportDetailByTransportId(long transport_id,PageCtrl pc,String sidx,String sord,FilterBean fillterBean)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from  "+ConfigBean.getStringValue("transport_detail")+" as td " 
											   +"left join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = td.transport_pc_id " 
											   +"left join "+ConfigBean.getStringValue("product_code")+" as pcode on pcode.pc_id = p.pc_id and pcode.code_type="+CodeTypeKey.Main+" "
											   +"where td.transport_id = ?");
			
			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
			
			if(fillterBean !=null)
			{
				sql.append(new JqGridFilterKey().filterSQL(fillterBean));
			}
			
			if(sidx==null||sord==null)
			{
				sql.append(" order by transport_id desc");
			}
			else
			{
				sql.append(" order by "+sidx+" "+sord);
			}
			
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(), para,pc));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTransportDetailByTransportId error:"+e);
		}
	}
	//查询转运单差异明细LL
	 public DBRow[] getTransportDetailDiffByTransportId(long transport_id)
	 	throws Exception
	 {
		  try 
		  {
			   StringBuffer sql = new StringBuffer("select * from  "+ConfigBean.getStringValue("transport_detail")+" as td left join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = td.transport_pc_id where transport_count <> transport_send_count and td.transport_id = ?");
			   
			   DBRow para = new DBRow();
			   para.add("transport_id",transport_id);
			   
			   return (dbUtilAutoTran.selectPreMutliple(sql.toString(), para));
		  }
		  catch (Exception e) 
		  {
			throw new Exception("FloorTransportMgrZJ getTransportDetailByTransportId error:"+e);
		  }
	 }
	
	/**
	 * 根据转运明细ID获得转运明细
	 * @param transport_detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getTransportDetailById(long transport_detail_id)
		throws Exception
	{
		try
		{
			String sql = "select * from  "+ConfigBean.getStringValue("transport_detail")+" as td " 
						+"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = td.transport_pc_id " 
						+"join "+ConfigBean.getStringValue("product_code")+" as pcode_main ON p.pc_id = pcode_main.pc_id and pcode_main.code_type = 1 "
						+"where td.transport_detail_id = ?";
			
			DBRow para = new DBRow();
			para.add("transport_detail_id",transport_detail_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTransportDetailById error:"+e);
		}
	}
	
	/**
	 * 验证转运单内是否已有此商品
	 * @param transport_id
	 * @param product_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getTransportDetailByTPId(long transport_id,long product_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_detail")+" where transport_id = ? and transport_pc_id = ?";
			
			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
			para.add("transport_pc_id",product_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTransportDetailByTPId error:"+e);
		}
	}
	
	public DBRow getTransportDetailByTPId(long transport_id,long product_id,String serial_number)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_detail")+" where transport_id = ? and transport_pc_id = ? and transport_product_serial_number = ? ";
			
			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
			para.add("transport_pc_id",product_id);
			para.add("transport_product_serial_number",serial_number);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTransportDetailByTPId error:"+e);
		}
	}
	
	/**
	 * 获得转运单的明细不同
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDifferentTransportDetailByTransportId(long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_detail")+" as td left join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = td.transport_pc_id where td.transport_send_count !=td.transport_reap_count and td.transport_id = ?";
			
			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getDifferentTransportDetailByTransportId error:"+e);
		}
	}
	
	/**
	 * 下载转运单详细，支持转运仓库、接收仓库，转运单状态过滤下载
	 * @param send_psid
	 * @param receive_psid
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportDetailByPsWithStatus(long send_psid,long receive_psid,int status)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select td.* from "+ConfigBean.getStringValue("transport_detail")+" as td,"+ConfigBean.getStringValue("transport")+" as t where td.transport_id=t.transport_id");
			
			if(send_psid>0)
			{
				sql.append(" and t.send_psid="+send_psid);
			}
			
			if(receive_psid>0)
			{
				sql.append(" and t.receive_psid="+receive_psid);
			}
			
			if(status>0)
			{
				if(status==TransportOrderKey.CANINSTORE)
				{
					sql.append(" and (t.transport_status="+TransportOrderKey.INTRANSIT+" or t.transport_status="+TransportOrderKey.AlREADYARRIAL+")");
				}
				else
				{
					sql.append(" and t.transport_status="+status);
				}
				
			}
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTransportDetailByPsWithStatus error:"+e);
		}
	}
	
	/**
	 * 转运单明细起运操作
	 * @param transport_id
	 * @param pc_id
	 * @param para
	 * @throws Exception
	 */
	public int transportDetailDelivery(long transport_id,long pc_id,DBRow para)
		throws Exception
	{
		try 
		{
			log.info("where transport_id="+transport_id+" and transport_pc_id="+pc_id+"-"+ConfigBean.getStringValue("transport_detail"));
			return dbUtilAutoTran.update("where transport_id="+transport_id+" and transport_pc_id="+pc_id,ConfigBean.getStringValue("transport_detail"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ transportDetailDelivery error:"+e);
		}
	}
	
	/**
	 * 检查转运单到货与发货数量是否相符
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkTransportDetailApprove(long transport_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_detail")+" where transport_send_count != transport_reap_count and transport_id=?";
			
			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ checkTransportDetailApprove error:"+e);
		}
	}
	public DBRow[] getBillByBillId4Search(long[] ids) throws Exception {
		try
		{
			String tableName =  ConfigBean.getStringValue("transport");
			if(ids == null  || (ids != null && ids.length < 1)){
				return (new DBRow[0]);
			}else{
				StringBuffer whereContion = new StringBuffer("");
					for(int index = 0 , count = ids.length ; index < count ; index++ ){
						if(index == 0){
							whereContion.append(" where transport_id=").append(ids[0]);
						}else{
							whereContion.append(" or transport_id=").append(ids[index]);
					}
				}
				return dbUtilAutoTran.selectMutliple("select * from " +tableName + whereContion.toString());
			}
		}catch (Exception e){
			throw new Exception("FloorTransportMgrZJ getBillByBillId4Search error:"+e);
		}
	}
	
	/**
	 * 获得采购单内某商品的在途数（运输中和已收货的数量为在途数）
	 * @param purchase_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public float getTransitCountForPurchase(long purchase_id,long pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select sum(td.transport_count) as transitCount from "+ConfigBean.getStringValue("transport")+" as t "
						+"join "+ConfigBean.getStringValue("transport_detail")+" as td on td.transport_id = t.transport_id "
						+"where t.purchase_id = ? and (t.transport_status ="+TransportOrderKey.INTRANSIT+" or t.transport_status = "+TransportOrderKey.AlREADYARRIAL+" ) and td.transport_pc_id = ? ";
			DBRow para = new DBRow();
			para.add("purchase_id",purchase_id);
			para.add("pc_id",pc_id);
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql, para);
			
			return (row.get("transitCount",0f));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTransitCountForPurchase error:"+e);
		}
	}
	
	/**
	 * 已起运，已收货，已入库的采购单商品数
	 * @param purchase_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public float getAlreadyGetCountForPurchase(long purchase_id,long pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select sum(td.transport_count) as transitCount from "+ConfigBean.getStringValue("transport")+" as t "
						+"join "+ConfigBean.getStringValue("transport_detail")+" as td on td.transport_id = t.transport_id "
						+"where t.purchase_id = ? and (t.transport_status ="+TransportOrderKey.INTRANSIT+" or t.transport_status = "+TransportOrderKey.AlREADYARRIAL+" or t.transport_status ="+TransportOrderKey.FINISH+" ) and td.transport_pc_id = ? ";
			DBRow para = new DBRow();
			para.add("purchase_id",purchase_id);
			para.add("pc_id",pc_id);
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql, para);
			
			return (row.get("transitCount",0f));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTransitCountForPurchase error:"+e);
		}
	}
	
	/**
	 * 获得采购单在途中订购数
	 * @param purchase_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public float getTransitDeliverCountForPurchase(long purchase_id,long pc_id)
		throws Exception
	{
		try {
			String sql = "select sum(td.transport_delivery_count) as transitCount from "+ConfigBean.getStringValue("transport")+" as t "
			+"join "+ConfigBean.getStringValue("transport_detail")+" as td on td.transport_id "
			+"where t.purchase_id = ? and (t.transport_status ="+TransportOrderKey.INTRANSIT+" or t.transport_status = "+TransportOrderKey.AlREADYARRIAL+" or t.transport_status ="+TransportOrderKey.AlREADYRECEIVE+") and td.transport_pc_id = ? ";
			
			DBRow para = new DBRow();
			para.add("purchase_id",purchase_id);
			para.add("pc_id",pc_id);
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql, para);
			
			return (row.get("transitCount",0f));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTransitDeliverCountForPurchase error:"+e);
		}
	}
	
	/**
	 * 获得采购单在途中的备件数
	 * @param purchase_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public float getTransitBackupCountForPurchase(long purchase_id,long pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select sum(td.transport_backup_count) as transitCount from "+ConfigBean.getStringValue("transport")+" as t "
			+"join "+ConfigBean.getStringValue("transport_detail")+" as td on td.transport_id "
			+"where t.purchase_id = ? and (t.transport_status ="+TransportOrderKey.INTRANSIT+" or t.transport_status = "+TransportOrderKey.AlREADYARRIAL+" or t.transport_status = "+TransportOrderKey.AlREADYRECEIVE+") and td.transport_pc_id = ? ";
			
			DBRow para = new DBRow();
			para.add("purchase_id",purchase_id);
			para.add("pc_id",pc_id);
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql, para);
			
			return (row.get("transitCount",0f));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTransitDeliverCountForPurchase error:"+e);
		}
	}
	
	public DBRow[] getTranposrtOrdersByPurchaseId(long purchase_id) 
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport")+" where purchase_id = ?";
			
			DBRow para = new DBRow();
			para.add("purchase_id",purchase_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTranposrtOrdersByPurchaseId error:"+e);
		}
	}
	
	/**
	 * 根据供应商ID获得从供应商提货的转运单
	 * @param supplier_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportBySupplierId(long supplier_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport")+" where send_psid = ? and from_ps_type = "+ProductStorageTypeKey.SupplierWarehouse+" order by transport_date desc ";
			
			DBRow para = new DBRow();
			para.add("send_psid",supplier_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTransportBySupplierId error:"+e);
		}
	}
	
	/**
	 * 供应商搜索
	 * @param supplier_id
	 * @param type
	 * @param id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] supplierSearch(long supplier_id,String type,long id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String colume = "";
			if(type.toUpperCase().equals("P"))
			{
				colume = "purchase_id";
			}
			if(type.toUpperCase().equals("T"))
			{
				colume = "transport_id";
			}
			String sql = "select * from "+ConfigBean.getStringValue("transport")+" where send_psid = ? and from_ps_type = "+ProductStorageTypeKey.SupplierWarehouse+" and "+colume+"= ? order by transport_date desc";
			
			DBRow para = new DBRow();
			para.add("send_psid",supplier_id);
			para.add("id",id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportmgrZJ supplierSearch error:"+e);
		}
	}
	
	/**
	 * 获得转运单体积(因Spring容器生成顺序不得不写floor层)
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public float getTransportVolume(long transport_id)
		throws Exception
	{
		DBRow transport = this.getDetailTransportById(transport_id);
		
		String colum = "transport_count";
		if(transport.get("transport_status",0)==TransportOrderKey.FINISH)
		{
			colum = "transport_reap_count";
		}
		
		DBRow[] rows = this.getTransportDetailByTransportId(transport_id, null, null, null, null);
		
		float volume = 0; 
		for (int i = 0; i < rows.length; i++) 
		{
			volume += rows[i].get("transport_volume",0f)*rows[i].get(colum,0f);
		}
		
		return MoneyUtil.round(volume,2);
	}

//交货单需跟进开始
	/**
	 * 获得超期未跟进的备货中的交货单数量按产品线分组
	 * @param ps_id
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackReadDeliveryCountGroupByProductLine(int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select product_line_name,product_line_id,count(transport_id) as readyneedtrackcountforline from "
				+"( "
				+"	select * from" 
				+"	( "
				+" 		select * from" 
				+"		( "
				+"  	 	select t.transport_id,t.purchase_id,tl.transport_date,p.product_line_id,pld.`name` AS product_line_name from transport t "
				+" 		 	join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Goods+" and tl.activity_id ="+TransportOrderKey.READY+" "
				+"		 	join purchase p on p.purchase_id = t.purchase_id "
				+"		 	left join product_line_define pld ON p.product_line_id = pld.id "
				+"		 	where t.purchase_id != 0 and t.transport_status = "+TransportOrderKey.READY+" order by tl.transport_date desc"
				+"		)as ttl group by transport_id "
				+"	)need_track where DATE_ADD(transport_date,INTERVAL "+track_day+" day)<= CURRENT_DATE " 
				+")as groupbyProductLine group by product_line_id ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackReadDeliveryCountGroupByProductLine error:"+e);
		}
	}


	/**
	 * 需跟进的备货中的交货单
	 * @param product_line_id
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackReadyDelivery(long product_line_id,int track_day,PageCtrl pc)
		throws Exception
	{
		String sql = "select * from" 
					+"( "
					+"	select * from" 
					+"	( "
					+"  	select t.*,tl.transport_date as last_track_date from transport t "
					+" 		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Goods+" and tl.activity_id ="+TransportOrderKey.READY+" "
					+"		join purchase p on p.purchase_id = t.purchase_id and p.product_line_id = ? "
					+"		left join product_line_define pld ON p.product_line_id = pld.id "
					+"		where t.purchase_id != 0 and t.transport_status = "+TransportOrderKey.READY+" order by tl.transport_date desc"
					+"	)as ttl group by transport_id "
					+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
		
		DBRow para = new DBRow();
		para.add("product_line_id",product_line_id);
		return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
	}
	
	/**
	 * 获得超期未跟进备货中的交货单数量
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackReadyDeliveryCount(int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(transport_id) as need_track_ready_delivery_count from " 
						+"( "
						+"	select * from("
						+"		select t.transport_id,t.purchase_id,tl.transport_date as last_track_date,p.product_line_id,pld.`name` AS product_line_name from transport t "
						+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Goods+" and tl.activity_id ="+TransportOrderKey.READY+" "
						+"		join purchase p on p.purchase_id = t.purchase_id "
						+"		left join product_line_define pld ON p.product_line_id = pld.id "
						+"		where t.purchase_id != 0 and t.transport_status = "+TransportOrderKey.READY+" order by tl.transport_date desc"
						+"		)as ttl group by transport_id "
						+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			
			int need_track_count = row.get("need_track_ready_delivery_count",0);
			return need_track_count;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackReadDeliveryCount error:"+e);
		}
	}
	
	/**
	 * 需跟进内部标签交货单产品线分组
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackTagCountGroupByProductLine(int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select product_line_name,product_line_id,count(transport_id) as need_track_tag_count from "
				+"( "
				+"	select * from" 
				+"	( "
				+" 		select * from" 
				+"		( "
				+"		select t.transport_id,t.purchase_id,tl.transport_date as last_track_date,p.product_line_id,pld.`name` AS product_line_name from transport t "
				+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Label+" and tl.activity_id ="+TransportTagKey.TAG+" "
				+"		join purchase p on p.purchase_id = t.purchase_id "
				+"		left join product_line_define pld ON p.product_line_id = pld.id "
				+"		where t.purchase_id != 0 and t.tag = "+TransportTagKey.TAG+" order by tl.transport_date desc"
				+"		)as ttl group by transport_id "
				+"	)need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE " 
				+")as groupbyProductLine group by product_line_id ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackTagCountGroupByProductLine error:"+e);
		}
	}
	
	/**
	 * 需跟进内部标签交货单产品线分组
	 * @param product_line_id
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackTagDelivery(long product_line_id,int track_day,PageCtrl pc)
		throws Exception
	{
		String sql = "select * from" 
					+"( "
					+"	select * from" 
					+"	( "
					+"  	select t.*,tl.transport_date as last_track_date from transport t "
					+" 		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Label+" and tl.activity_id ="+TransportTagKey.TAG+" "
					+"		join purchase p on p.purchase_id = t.purchase_id and p.product_line_id = ? "
					+"		left join product_line_define pld ON p.product_line_id = pld.id "
					+"		where t.purchase_id != 0 and t.tag = "+TransportTagKey.TAG+" order by tl.transport_date desc"
					+"	)as ttl group by transport_id "
					+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
		
		DBRow para = new DBRow();
		para.add("product_line_id",product_line_id);
		return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
	}
	
	/**
	 * 需跟进第三方标签交货单产品线分组
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackThirdTagCountGroupByProductLine(int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select product_line_name,product_line_id,count(transport_id) as need_track_tag_count from "
				+"( "
				+"	select * from" 
				+"	( "
				+" 		select * from" 
				+"		( "
				+"		select t.transport_id,t.purchase_id,tl.transport_date as last_track_date,p.product_line_id,pld.`name` AS product_line_name from transport t "
				+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.THIRD_TAG+" and tl.activity_id ="+TransportTagKey.TAG+" "
				+"		join purchase p on p.purchase_id = t.purchase_id "
				+"		left join product_line_define pld ON p.product_line_id = pld.id "
				+"		where t.purchase_id != 0 and t.tag_third = "+TransportTagKey.TAG+" order by tl.transport_date desc"
				+"		)as ttl group by transport_id "
				+"	)need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE " 
				+")as groupbyProductLine group by product_line_id ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackTagCountGroupByProductLine error:"+e);
		}
	}
	
	/**
	 * 需跟进第三方标签交货单产品线分组
	 * @param product_line_id
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackThirdTagDelivery(long product_line_id,int track_day,PageCtrl pc)
		throws Exception
	{
		String sql = "select * from" 
					+"( "
					+"	select * from" 
					+"	( "
					+"  	select t.*,tl.transport_date as last_track_date from transport t "
					+" 		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.THIRD_TAG+" and tl.activity_id ="+TransportTagKey.TAG+" "
					+"		join purchase p on p.purchase_id = t.purchase_id and p.product_line_id = ? "
					+"		left join product_line_define pld ON p.product_line_id = pld.id "
					+"		where t.purchase_id != 0 and t.tag_third = "+TransportTagKey.TAG+" order by tl.transport_date desc"
					+"	)as ttl group by transport_id "
					+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
		
		DBRow para = new DBRow();
		para.add("product_line_id",product_line_id);
		return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
	}
	
	/**
	 * 需跟进质检交货单产品线分组
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackQualityInspectionCountGroupByProductLine(int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select product_line_name,product_line_id,count(transport_id) as need_track_quality_inspection_count from "
				+"( "
				+"	select * from" 
				+"	( "
				+" 		select * from" 
				+"		( "
				+"		select t.transport_id,t.purchase_id,tl.transport_date as last_track_date,p.product_line_id,pld.`name` AS product_line_name from transport t "
				+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.QualityControl+" and tl.activity_id ="+TransportQualityInspectionKey.NEED_QUALITY+" "
				+"		join purchase p on p.purchase_id = t.purchase_id "
				+"		left join product_line_define pld ON p.product_line_id = pld.id "
				+"		where t.purchase_id != 0 and t.quality_inspection = "+TransportQualityInspectionKey.NEED_QUALITY+" order by tl.transport_date desc"
				+"		)as ttl group by transport_id "
				+"	)need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE " 
				+")as groupbyProductLine group by product_line_id ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackTagCountGroupByProductLine error:"+e);
		}
	}
	
	/**
	 * 需跟进质检交货单
	 * @param product_line_id
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackQualityInspectionDelivery(long product_line_id,int track_day,PageCtrl pc)
		throws Exception
	{
		String sql = "select * from" 
					+"( "
					+"	select * from" 
					+"	( "
					+"  	select t.*,tl.transport_date as last_track_date from transport t "
					+" 		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.QualityControl+" and tl.activity_id ="+TransportQualityInspectionKey.NEED_QUALITY+" "
					+"		join purchase p on p.purchase_id = t.purchase_id and p.product_line_id = ? "
					+"		left join product_line_define pld ON p.product_line_id = pld.id "
					+"		where t.purchase_id != 0 and t.quality_inspection = "+TransportQualityInspectionKey.NEED_QUALITY+" order by tl.transport_date desc"
					+"	)as ttl group by transport_id "
					+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
		
		DBRow para = new DBRow();
		para.add("product_line_id",product_line_id);
		return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
	}
	
	/**
	 * 需跟进实物图片交货单产品线分组
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackProductFileCountGroupByProductLine(int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select product_line_name,product_line_id,count(transport_id) as need_track_product_file_count from "
				+"( "
				+"	select * from" 
				+"	( "
				+" 		select * from" 
				+"		( "
				+"		select t.transport_id,t.purchase_id,tl.transport_date as last_track_date,p.product_line_id,pld.`name` AS product_line_name from transport t "
				+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.ProductShot+" and tl.activity_id ="+TransportProductFileKey.PRODUCTFILE+" "
				+"		join purchase p on p.purchase_id = t.purchase_id "
				+"		left join product_line_define pld ON p.product_line_id = pld.id "
				+"		where t.purchase_id != 0 and t.product_file = "+TransportProductFileKey.PRODUCTFILE+" order by tl.transport_date desc"
				+"		)as ttl group by transport_id "
				+"	)need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE " 
				+")as groupbyProductLine group by product_line_id ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackTagCountGroupByProductLine error:"+e);
		}
	}
	
	/**
	 * 需跟进实物图片交货单
	 * @param product_line_id
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackProductFileDelivery(long product_line_id,int track_day,PageCtrl pc)
		throws Exception
	{
		String sql = "select * from" 
					+"( "
					+"	select * from" 
					+"	( "
					+"  	select t.*,tl.transport_date as last_track_date from transport t "
					+" 		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.ProductShot+" and tl.activity_id ="+TransportProductFileKey.PRODUCTFILE+" "
					+"		join purchase p on p.purchase_id = t.purchase_id and p.product_line_id = ? "
					+"		left join product_line_define pld ON p.product_line_id = pld.id "
					+"		where t.purchase_id != 0 and t.product_file = "+TransportProductFileKey.PRODUCTFILE+" order by tl.transport_date desc"
					+"	)as ttl group by transport_id "
					+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
		
		DBRow para = new DBRow();
		para.add("product_line_id",product_line_id);
		return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
	}
//交货单需跟进结束

//仓库发货需跟进开始
	/**
	 * 超期未跟进的尚备货中的转运单
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackReadyTransportCountByPsid(long ps_id,int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(transport_id) as need_track_count from " 
						+"( "
						+"	select * from("
						+"		select t.transport_id,t.purchase_id,tl.transport_date as last_track_date from transport t "
						+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Goods+" and tl.activity_id ="+TransportOrderKey.READY+" "
						+"		where t.purchase_id = 0 and t.send_psid = ? and (t.transport_status = "+TransportOrderKey.READY+") order by tl.transport_date desc"
						+"		)as ttl group by transport_id "
						+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow para = new DBRow();
			para.add("send_psid",ps_id);
			DBRow row = dbUtilAutoTran.selectPreSingle(sql,para);
				
			int need_track_count = row.get("need_track_count",0);
			
			return need_track_count;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackReadyTransportCount error:"+e);
		}
	}
	
	
	
	/**
	 * 获得需跟进装货中转运单数量
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackPackingTransportCountByPsid(long ps_id,int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(transport_id) as need_track_count from " 
						+"( "
						+"	select * from("
						+"		select t.transport_id,t.purchase_id,tl.transport_date as last_track_date from transport t "
						+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Goods+" and tl.activity_id ="+TransportOrderKey.PACKING+" "
						+"		where t.purchase_id = 0 and t.send_psid = "+ps_id+" and (t.transport_status="+TransportOrderKey.PACKING+") order by tl.transport_date desc"
						+"		)as ttl group by transport_id "
						+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			
			int need_track_count = row.get("need_track_count",0);
			
			return need_track_count;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackReadyTransportCount error:"+e);
		}
	}
	
	/**
	 * 需跟进内部标签的转运单数量
	 * @param send_psid
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackTagCountByPS(long send_psid,int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(transport_id) as need_track_tag_count from" 
					+"( "
					+"	select * from" 
					+"	( "
					+"		select t.transport_id,t.purchase_id,tl.transport_date as last_track_date,t.send_psid from transport t "
					+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Label+" and tl.activity_id ="+TransportTagKey.TAG+" "
					+"		join product_storage_catalog psc on t.send_psid = psc.id "
					+"		where t.purchase_id = 0 and t.send_psid = ? and t.tag = "+TransportTagKey.TAG+" order by tl.transport_date desc"
					+"		)as ttl group by transport_id "
					+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow para = new DBRow();
			para.add("send_psid",send_psid);
			
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			
			int need_track_tag_count = result.get("need_track_tag_count",0);
			
			return need_track_tag_count;
			
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackTagCountByPS error:"+e);
		}
	}
	
	/**
	 * 需跟进内部标签的转运单
	 * @param send_psid
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackTagTransportByPs(long send_psid,int track_day,PageCtrl pc)
		throws Exception
	{
		String sql = "select * from" 
					+"( "
					+"	select * from" 
					+"	( "
					+"  	select t.*,tl.transport_date as last_track_date from transport t "
					+" 		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Label+" and tl.activity_id ="+TransportTagKey.TAG+" "
					+"		join product_storage_catalog psc on t.send_psid = psc.id "
					+"		where t.purchase_id = 0 and t.send_psid = ? and t.tag = "+TransportTagKey.TAG+" order by tl.transport_date desc"
					+"	)as ttl group by transport_id "
					+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
		
		DBRow para = new DBRow();
		para.add("send_psid",send_psid);
		return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
	}
	
	/**
	 * 需跟进第三方标签转运单数量
	 * @param send_psid
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackThirdTagCountByPS(long send_psid,int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(transport_id) as need_track_third_tag_count from" 
					+"( "
					+"	select * from" 
					+"	( "
					+"		select t.transport_id,t.purchase_id,tl.transport_date as last_track_date,t.send_psid from transport t "
					+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.THIRD_TAG+" and tl.activity_id ="+TransportTagKey.TAG+" "
					+"		join product_storage_catalog psc on t.send_psid = psc.id "
					+"		where t.purchase_id = 0 and t.send_psid = ? and t.tag_third = "+TransportTagKey.TAG+" order by tl.transport_date desc"
					+"		)as ttl group by transport_id "
					+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow para = new DBRow();
			para.add("send_psid",send_psid);
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			
			int need_track_tag_count = result.get("need_track_third_tag_count",0);
			
			return need_track_tag_count;
			
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackTagCountByPS error:"+e);
		}
	}
	
	/**
	 * 需跟进第三方标签的转运单
	 * @param send_psid
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackThirdTagTransportByPs(long send_psid,int track_day,PageCtrl pc)
		throws Exception
	{
		String sql = "select * from" 
					+"( "
					+"	select * from" 
					+"	( "
					+"  	select t.*,tl.transport_date as last_track_date from transport t "
					+" 		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.THIRD_TAG+" and tl.activity_id ="+TransportTagKey.TAG+" "
					+"		join product_storage_catalog psc on t.send_psid = psc.id "
					+"		where t.purchase_id = 0 and t.send_psid = ? and t.tag_third = "+TransportTagKey.TAG+" order by tl.transport_date desc"
					+"	)as ttl group by transport_id "
					+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
		
		DBRow para = new DBRow();
		para.add("send_psid",send_psid);
		return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
	}
	
	/**
	 * 需跟进质检转运单数量
	 * @param send_psid
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackQualityInspectionCountByPS(long send_psid,int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(transport_id) as need_track_quality_inspection_count from" 
						+"( "
						+" 	select * from" 
						+"	( "
						+"		select t.transport_id,t.purchase_id,tl.transport_date as last_track_date,t.send_psid from transport t "
						+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.QualityControl+" and tl.activity_id ="+TransportQualityInspectionKey.NEED_QUALITY+" "
						+"		join product_storage_catalog psc on t.send_psid = psc.id "
						+"		where t.purchase_id = 0 and t.send_psid = ? and t.quality_inspection = "+TransportQualityInspectionKey.NEED_QUALITY+" order by tl.transport_date desc"
						+"	)as ttl group by transport_id "
						+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE "; 
			
			DBRow para = new DBRow();
			para.add("send_psid",send_psid);
			
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			int need_track_quality_inspection_count = result.get("need_track_quality_inspection_count",0);
			
			return need_track_quality_inspection_count;
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackQualityInspectionCountByPS error:"+e);
		}
	}
	
	/**
	 * 需跟进质检转运单
	 * @param send_psid
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackQualityInspectionTransport(long send_psid,int track_day,PageCtrl pc)
		throws Exception
	{
		try{
			String sql = "select * from" 
						+"( "
						+"	select * from" 
						+"	( "
						+"  	select t.*,tl.transport_date as last_track_date from transport t "
						+" 		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.QualityControl+" and tl.activity_id ="+TransportQualityInspectionKey.NEED_QUALITY+" "
						+"		join product_storage_catalog psc on t.send_psid = psc.id "
						+"		where t.purchase_id = 0 and t.send_psid = ? and t.quality_inspection = "+TransportQualityInspectionKey.NEED_QUALITY+" order by tl.transport_date desc"
						+"	)as ttl group by transport_id "
						+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow para = new DBRow();
			para.add("send_psid",send_psid);
			
			return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
		}
		catch (Exception e) 
		{
			log.error("getNeedTrackQualityInspectionTransport", e);
			throw e;
		}
	}
	
	/**
	 * 需跟进实物图片转运单数量
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackProductFileCountByPS(long send_psid,int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(transport_id) as need_track_product_file_count from" 
						+"( "
						+" 		select * from" 
						+"		( "
						+"		select t.transport_id,t.purchase_id,tl.transport_date as last_track_date,t.send_psid from transport t "
						+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.ProductShot+" and tl.activity_id ="+TransportProductFileKey.PRODUCTFILE+" "
						+"		join product_storage_catalog psc on t.send_psid = psc.id "
						+"		where t.purchase_id = 0 and t.send_psid = ? and t.product_file = "+TransportProductFileKey.PRODUCTFILE+" order by tl.transport_date desc"
						+"		)as ttl group by transport_id "
						+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE "; 
			DBRow para = new DBRow();
			para.add("send_psid",send_psid);
			
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			
			int need_track_product_file_count = result.get("need_track_product_file_count",0);
			return need_track_product_file_count;
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackProductFileCountByPS error:"+e);
		}
	}
	
	/**
	 * 需跟进实物图片交货单
	 * @param send_psid
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackProductFileTransport(long send_psid,int track_day,PageCtrl pc)
		throws Exception
	{
		String sql = "select * from" 
					+"( "
					+"	select * from" 
					+"	( "
					+"  	select t.*,tl.transport_date as last_track_date from transport t "
					+" 		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.ProductShot+" and tl.activity_id ="+TransportProductFileKey.PRODUCTFILE+" "
					+"		join product_storage_catalog psc on t.send_psid = psc.id "
					+"		where t.purchase_id = 0 and t.send_psid = ? and t.product_file = "+TransportProductFileKey.PRODUCTFILE+" order by tl.transport_date desc"
					+"	)as ttl group by transport_id "
					+")need_track where DATE_ADD(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
		
		DBRow para = new DBRow();
		para.add("send_psid",send_psid);
		return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
	}
	
	/**
	 * 需跟进备货中,装箱中的转运单
	 * @param ps_id
	 * @param track_day
	 * @param transort_status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackSendTransportByPsid(long ps_id,int track_day,int transort_status,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from " 
						+"( "
						+"	select * from" 
						+"		("
						+"		select t.*,tl.transport_date as last_track_date from transport t "
						+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Goods+" and tl.activity_id ="+transort_status+" "
						+"		where t.purchase_id = 0 and t.send_psid = ? and t.transport_status = "+transort_status+" order by tl.transport_date desc"
						+"		)as ttl group by transport_id "
						+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			DBRow para  = new DBRow();
			para.add("send_psid",ps_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackReadyTransportCount error:"+e);
		}
	}

//需跟进仓库发货完

//需跟进仓库收货开始
	/**
	 * 需跟进运输中的交货、转运单数量
	 * @param ps_id
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackIntransitTransportCountByPsid(long ps_id,int track_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(transport_id) as need_track_count from " 
						+"( "
						+"	select * from" 
						+"		("
						+"		select t.transport_id,t.purchase_id,tl.transport_date from transport t "
						+"		left join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Goods+" and tl.activity_id ="+TransportOrderKey.INTRANSIT+" "
						+"		where t.receive_psid = ? and t.transport_status = "+TransportOrderKey.INTRANSIT+" order by tl.transport_date desc"
						+"		)as ttl group by transport_id "
						+")need_track where date_add(transport_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			DBRow para  = new DBRow();
			para.add("receive_psid",ps_id);
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql,para);
				
			int need_track_count = row.get("need_track_count",0);
			
			return need_track_count;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackReadyTransportCount error:"+e);
		}
	}
	
	/**
	 * 需跟进已收货的交货、转运单数量
	 * @param ps_id
	 * @param track_hour
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackAlreadyReciveTransportCount(long ps_id,int track_hour)
		throws Exception
	{
		try 
		{
			String sql = "select count(transport_id) as need_track_count from " 
						+"( "
						+"	select * from" 
						+"		("
						+"		select t.transport_id,t.purchase_id,tl.transport_date from transport t "
						+"		left join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Goods+" and tl.activity_id ="+TransportOrderKey.AlREADYARRIAL+" "
						+"		where t.receive_psid = ? and t.transport_status = "+TransportOrderKey.AlREADYARRIAL+" order by tl.transport_date desc"
						+"		)as ttl group by transport_id "
						+")need_track where date_add(transport_date,INTERVAL "+track_hour+" hour)<= CURRENT_TIMESTAMP ";
			DBRow para = new DBRow();
			para.add("receive_psid",ps_id);
			
			DBRow row = dbUtilAutoTran.selectPreSingle(sql,para);
				
			int need_track_count = row.get("need_track_count",0);
			
			return need_track_count;
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackReadyTransportCount error:"+e);
		}
	}
	
	/**
	 * 需跟进的仓库收货交货、转运单
	 * @param ps_id
	 * @param track_day
	 * @param transort_status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackReceiveTransportByPsid(long ps_id,int track_day,int transort_status,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from " 
						+"( "
						+"	select * from" 
						+"		("
						+"		select t.*,tl.transport_date as last_track_date from transport t "
						+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Goods+" and tl.activity_id ="+transort_status+" "
						+"		where t.receive_psid = ? and t.transport_status = "+transort_status+" order by tl.transport_date desc"
						+"		)as ttl group by transport_id "
						+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			DBRow para  = new DBRow();
			para.add("recevie_psid",ps_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getNeedTrackReadyTransportCount error:"+e);
		}
	}
//需跟进的仓库收货交货、转运单结束

//需跟进的海运开始
	/**
	 * 需跟进单证的数量
	 */
	public int getNeedTrackCertificateCount(int track_day)
		throws Exception
	{
		try {
			String sql = "select count(transport_id) as need_track_certificate_count from " 
				+"( "
				+"	select * from" 
				+"		("
				+"		select t.transport_id,tl.transport_date as last_track_date from transport t "
				+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Document+" and tl.activity_id ="+TransportCertificateKey.CERTIFICATE+" "
				+"		where t.certificate = "+TransportCertificateKey.CERTIFICATE+" order by tl.transport_date desc"
				+"		)as ttl group by transport_id "
				+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			
			int need_track_certificate_count = result.get("need_track_certificate_count",0);
			
			return need_track_certificate_count;
		}
		catch (Exception e) 
		{
			log.error("getNeedTrackCertificateCount", e);
			throw new Exception(e);
		}
	}
	
	/**
	 * 需跟进单证的交货和转运单
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackCertificateTransport(int track_day,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from " 
				+"( "
				+"	select * from" 
				+"		("
				+"		select t.*,tl.transport_date as last_track_date from transport t "
				+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.Document+" and tl.activity_id ="+TransportCertificateKey.CERTIFICATE+" "
				+"		where t.certificate = "+TransportCertificateKey.CERTIFICATE+" order by tl.transport_date desc"
				+"		)as ttl group by transport_id "
				+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch (Exception e) 
		{
			log.error("getNeedTrackCertificateCount", e);
			throw new Exception(e);
		}
	}
	
	/**
	 * 需跟进的报关数量
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackDeclarationCount(int track_day)
		throws Exception
	{
		try {
			String sql = "select count(transport_id) as need_track_declaration_count from " 
				+"( "
				+"	select * from" 
				+"		("
				+"		select t.transport_id,tl.transport_date as last_track_date from transport t "
				+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.ExportCustoms+" and tl.activity_id ="+DeclarationKey.DELARATING+" "
				+"		where t.declaration = "+DeclarationKey.DELARATING+" order by tl.transport_date desc"
				+"		)as ttl group by transport_id "
				+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			
			int need_track_declaration_count = result.get("need_track_declaration_count",0);
			
			return need_track_declaration_count;
		}
		catch (Exception e) 
		{
			log.error("getNeedTrackDeclarationCount",e);
			throw new Exception(e);
		}
	}
	
	/**
	 * 需跟进报关的交货和转运单
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackDeclarationTransport(int track_day,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from " 
				+"( "
				+"	select * from" 
				+"		("
				+"		select t.*,tl.transport_date as last_track_date from transport t "
				+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.ExportCustoms+" and tl.activity_id ="+DeclarationKey.DELARATING+" "
				+"		where t.declaration = "+DeclarationKey.DELARATING+" order by tl.transport_date desc"
				+"		)as ttl group by transport_id "
				+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			return dbUtilAutoTran.selectMutliple(sql,pc);
		}
		catch (Exception e) 
		{
			log.error("getNeedTrackDeclarationTransport", e);
			throw new Exception(e);
		}
	}
	
	/**
	 * 需跟进清关的数量
	 * @param track_day
	 * @return
	 * @throws Exception
	 */
	public int getNeedTrackClearanceCount(int track_day)
		throws Exception
	{
		try {
			String sql = "select count(transport_id) as need_track_clearance_count from " 
				+"( "
				+"	select * from" 
				+"		("
				+"		select t.transport_id,tl.transport_date as last_track_date from transport t "
				+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.ImportCustoms+" and tl.activity_id ="+ClearanceKey.CLEARANCEING+" "
				+"		where t.clearance = "+ClearanceKey.CLEARANCEING+" order by tl.transport_date desc"
				+"		)as ttl group by transport_id "
				+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			
			int need_track_clearance_count = result.get("need_track_clearance_count",0);
			
			return need_track_clearance_count;
		}
		catch (Exception e) 
		{
			log.error("getNeedTrackDeclarationCount",e);
			throw new Exception(e);
		}
	}
	
	/**
	 * 需跟进清关的交货和转运单
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackClearanceTransport(int track_day,PageCtrl pc)
		throws Exception
	{
		try {
			String sql = "select * from " 
				+"( "
				+"	select * from" 
				+"		("
				+"		select t.*,tl.transport_date as last_track_date from transport t "
				+"		join transport_logs tl on tl.transport_id = t.transport_id and tl.transport_type = "+TransportLogTypeKey.ImportCustoms+" and tl.activity_id ="+ClearanceKey.CLEARANCEING+" "
				+"		where t.clearance = "+ClearanceKey.CLEARANCEING+" order by tl.transport_date desc"
				+"		)as ttl group by transport_id "
				+")need_track where date_add(last_track_date,INTERVAL "+track_day+" day)<= CURRENT_DATE ";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch (Exception e) 
		{
			log.error("getNeedTrackDeclarationCount",e);
			throw new Exception(e);
		}
	}
//海运结束
	
	/**
	 * 获得转运单的总重量
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public float getTransportWeight(long transport_id)
		throws Exception
	{
		try 
		{
			DBRow transport = this.getDetailTransportById(transport_id);
			
			String colum = "transport_count";
			if(transport.get("transport_status",0)==TransportOrderKey.FINISH)
			{
				colum = "transport_reap_count";
			}
			
			DBRow[] transportDetails = this.getTransportDetailByTransportId(transport_id, null, null, null, null);
			
			float weight = 0;
			for (int i = 0; i < transportDetails.length; i++) 
			{
				weight += transportDetails[i].get("transport_weight",0f)*transportDetails[i].get(colum,0f);
			}
			
			return MoneyUtil.round(weight,2);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTransportWeight error:"+e);
		}
	}
	
	/**
	 * 获得所有转运单出库金额
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public double getTransportSendPrice(long transport_id)
		throws Exception
	{
		try 
		{
			DBRow[] transportDetails = this.getTransportDetailByTransportId(transport_id, null, null, null, null);
			
			double sendPrice = 0;
			for (int i = 0; i < transportDetails.length; i++) 
			{
				sendPrice += transportDetails[i].get("transport_send_price",0f)*transportDetails[i].get("transport_delivery_count",0f);
			}
			
			return MoneyUtil.round(sendPrice,2);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTransportSendPrice error:"+e);
		}
	}
	
	/**
	 * 根据检查单据内是否已包含此序列号
	 * @param transport_id
	 * @param serial_number
	 * @return
	 * @throws Exception
	 */
	public DBRow getTransportDetailBySN(long transport_id,String serial_number)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_detail")+" where transport_id = ? and transport_product_serial_number = ? ";
			
			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
			para.add("serial_number",serial_number);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTransportDetailBySN error:"+e);
		}
	}
	
	public DBRow[] getAllTransportDetail()
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.select(ConfigBean.getStringValue("transport_detail"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getAllTransportDetail error:"+e);
		}
	}
	
	public DBRow[] getAllTransport()
		throws Exception
	{
		return dbUtilAutoTran.select(ConfigBean.getStringValue("transport"));
	}
	
	/**
	 * ----------------------导入转运单明细--------------------------
	 */
	public long addTransportImportDetail(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("transport_import_detail"), dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ addTransportImportDetail error:"+e);
		}
	}
	
	public DBRow[] errorImportTransportDetail(String fileName)
		throws Exception
	{
		try 
		{
			String sql = "select line_number as number,import_p_name as p_name,transport_pc_id,transport_delivery_count,transport_backup_count,transport_detail_serial_number,transport_detail_box,lot_number,'ProductNotExist' as error_type from "+ConfigBean.getStringValue("transport_import_detail")+" where transport_pc_id = 0 and file_name ='"+fileName+"' "
						+"union "
						+"select line_number as number,import_p_name as p_name,transport_pc_id,transport_delivery_count,transport_backup_count,transport_detail_serial_number,transport_detail_box,lot_number,'CountNoOne' as error_type from "+ConfigBean.getStringValue("transport_import_detail")+" where transport_detail_serial_number is not null and transport_count !=1 and file_name ='"+fileName+"' "
						+"union "
						+"select line_number as number,import_p_name as p_name,transport_pc_id,transport_delivery_count,transport_backup_count,transport_detail_serial_number,transport_detail_box,lot_number,'SerialNumberRepeat' as error_type from transport_import_detail tid "
						+"join " 
						+"("
						+"	select * from ("
						+"		select transport_detail_serial_number as serial_number,count(transport_import_detail_id)as times from transport_import_detail where transport_detail_serial_number is not NULL and file_name ='"+fileName+"' GROUP BY transport_detail_serial_number "
						+"	) as importSerialNumberRepeat where times <>1 "
						+")as isnr on tid.transport_detail_serial_number = isnr.serial_number";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ errorImportTransportDetail error:"+e);
		}
	}
	
	/**
	 * 根据文件名获得导入转运单明细
	 * @param import_name
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getImportTransportDetail(String import_name)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_import_detail")+" where file_name = ? ";
			
			DBRow para = new DBRow();
			para.add("file_name",import_name);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getImportTransportDetail error:"+e);
		}
	}
	
	/**
	 * 发货与收货差异收货差异
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] sendReceiveDifferents(long transport_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "
						+"("
						+"select sendView.send_pc_id,sendView.send_p_name,sendView.send_count,COALESCE(receiveView.receive_count,0) as receive_count,(sendView.send_count-COALESCE(receiveView.receive_count,0)) as different_count,CASE WHEN(sendView.send_count-COALESCE(receiveView.receive_count,0))  >0 THEN "+DifferentKey.Lack+" WHEN(sendView.send_count-COALESCE(receiveView.receive_count,0))=0 THEN "+DifferentKey.Equal+" END as different_type from " 
						+"	(select `to`.to_pc_id as send_pc_id,`to`.to_p_name as send_p_name,sum(to_count)as send_count from transport_outbound `to` where `to`.to_transport_id = "+transport_id+" GROUP BY `to`.to_pc_id) as sendView "
						+"	left join(select tw.tw_product_id as receive_pc_id,tw.tw_product_name as receive_p_name,sum(tw_count) as receive_count from transport_warehouse as tw where tw.tw_transport_id = "+transport_id+" GROUP BY tw.tw_product_id) as receiveView on sendView.send_pc_id = receiveView.receive_pc_id "
						+" union "
						+"select receiveView.receive_pc_id,receiveView.receive_p_name,COALESCE(sendView.send_count,0) as send_count,receiveView.receive_count,(receiveView.receive_count-COALESCE(sendView.send_count,0))as different_count,CASE WHEN(receiveView.receive_count-COALESCE(sendView.send_count,0))>0 THEN "+DifferentKey.More+" WHEN(receiveView.receive_count-COALESCE(sendView.send_count,0))=0 THEN "+DifferentKey.Equal+" END as different_type from " 
						+"	(select tw.tw_product_id as receive_pc_id,tw.tw_product_name as receive_p_name,sum(tw_count) as receive_count from transport_warehouse as tw where tw.tw_transport_id = "+transport_id+" GROUP BY tw.tw_product_id) as receiveView "
						+"	left join (select `to`.to_pc_id as send_pc_id,`to`.to_p_name as send_p_name,sum(COALESCE(to_count)) as send_count from transport_outbound `to` where `to`.to_transport_id = "+transport_id+" GROUP BY `to`.to_pc_id) as sendView on receiveView.receive_pc_id = sendView.send_pc_id "  
						+") as sendReceiveDifferentView "
						+"where different_count>=0 order by different_type";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ sendReceiveDifferents error:"+e);
		}
	}
	
	/**
	 * 计划与发货差异
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] planSendDifferents(long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "
						+"("
						+"	select planView.plan_pc_id,planView.plan_p_name,sendView.to_pc_id,sendView.to_p_name,planView.plan_count,COALESCE(sendView.send_count,0) as send_count,(planView.plan_count-COALESCE(sendView.send_count,0)) as different_count,CASE WHEN(planView.plan_count-COALESCE(sendView.send_count,0))>0 THEN "+DifferentKey.Lack+" WHEN (planView.plan_count-COALESCE(sendView.send_count,0))=0 THEN "+DifferentKey.Equal+" END as different_type from " 
						+"	("
						+"		(select td.transport_pc_id as plan_pc_id,td.transport_p_name as plan_p_name,sum(td.transport_count) as plan_count from transport_detail as td where td.transport_id = "+transport_id+" GROUP BY td.transport_pc_id) as planView "
						+"		left join(select `to`.to_pc_id,`to`.to_p_name,sum(`to`.to_count)as send_count from transport_outbound as `to` where `to`.to_transport_id = "+transport_id+" GROUP BY `to`.to_pc_id) as sendView on sendView.to_pc_id = planView.plan_pc_id "
						+"	) "
						+"	union "
						+"	select planView.plan_pc_id,planView.plan_p_name,sendView.to_pc_id,sendView.to_p_name,COALESCE(planView.plan_count,0) as plan_count,sendView.send_count,(sendView.send_count-COALESCE(planView.plan_count,0)) as different_count,CASE WHEN(sendView.send_count-COALESCE(planView.plan_count,0))>0 THEN "+DifferentKey.More+" WHEN (sendView.send_count-COALESCE(planView.plan_count,0))=0 THEN "+DifferentKey.Equal+" END as different_type from " 
						+"	("
						+"		(select `to`.to_pc_id,`to`.to_p_name,sum(`to`.to_count)as send_count from transport_outbound as `to` where `to`.to_transport_id= "+transport_id+" GROUP BY `to`.to_pc_id) as sendView "
						+"		left join(select td.transport_pc_id as plan_pc_id,td.transport_p_name plan_p_name,sum(td.transport_count) as plan_count from transport_detail as td where td.transport_id = "+transport_id+" GROUP BY td.transport_pc_id) as planView on sendView.to_pc_id = planView.plan_pc_id "
						+"	)"
						+") as planSendDifferents " 
						+"where different_count >=0 order by different_type";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ planSendDifferents error:"+e);
		}
	}
	
	/**
	 * 发货收货比较（序列号版本）
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] sendReceiveDifferentsSN(long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "
						+"( "
						+"	select to_pc_id as pc_id,`to`.to_p_name as p_name,`to`.to_serial_number as serial_number,`to`.to_count as send_count,COALESCE(`tw`.tw_count,0) as receive_count,(`to`.to_count-COALESCE(`tw`.tw_count, 0)) as different_count,CASE WHEN(`to`.to_count-COALESCE(`tw`.tw_count, 0))>0 THEN "+DifferentKey.Lack+" WHEN (`to`.to_count-COALESCE(`tw`.tw_count, 0))=0 THEN "+DifferentKey.Equal+" END as different_type from transport_outbound `to` " 
						+"	left join transport_warehouse tw on `to`.to_transport_id = tw.tw_transport_id and `to`.to_serial_number = tw.tw_serial_number and `to`.to_pc_id = tw.tw_product_id " 
						+"	where `to`.to_serial_number is not NULL and `to`.to_transport_id =? "
						+"	union "  
						+"	select tw.tw_product_id as pc_id,tw.tw_product_name as p_name,tw.tw_serial_number as serial_number,`to`.to_count as send_count,`tw`.tw_count as receive_count,(tw.tw_count-COALESCE(`to`.to_count, 0)) as different_count,CASE WHEN(tw.tw_count-COALESCE(`to`.to_count,0))>0 THEN "+DifferentKey.More+" WHEN (tw.tw_count-COALESCE(`to`.to_count,0))=0 THEN "+DifferentKey.Equal+" END  as different_type from transport_warehouse tw "
						+"	left join transport_outbound `to` on `to`.to_transport_id = tw.tw_transport_id and `to`.to_serial_number = tw.tw_serial_number and `to`.to_pc_id = tw.tw_product_id "
						+"	where tw.tw_serial_number is not NULL and tw.tw_transport_id = ? "
						+") as differentView where different_count >=0 order by different_type";
			
			DBRow para = new DBRow();
			para.add("to_transport_id",transport_id);
			para.add("tw_transport_id",transport_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ sendReceiveDifferentsSN error:"+e);
		}
	}
	
	/**
	 * 计划发货不同（序列号）
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] planSendDifferentsSN(long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select td.transport_pc_id as plan_pc_id,`to`.to_pc_id as send_pc_id,td.transport_p_name as plan_p_name,`to`.to_p_name send_p_name,td.transport_product_serial_number as plan_serial_number,`to`.to_serial_number as send_serial_number,td.transport_count as plan_count,COALESCE(`to`.to_count,0) as send_count,(td.transport_count-COALESCE(`to`.to_count,0)) as different_count,CASE WHEN (td.transport_count-COALESCE(`to`.to_count,0))>0 THEN "+DifferentKey.Lack+" WHEN (td.transport_count-COALESCE(`to`.to_count,0))=0 THEN "+DifferentKey.Equal+" END as different_type from transport_detail as td "
						+"left join transport_outbound as `to` on td.transport_pc_id = `to`.to_pc_id and `to`.to_serial_number = td.transport_product_serial_number and `to`.to_transport_id = td.transport_id "
						+"where td.transport_id = "+transport_id+" and td.transport_product_serial_number is not null"
						+" union " 
						+"select td.transport_pc_id as plan_pc_id,`to`.to_pc_id as send_pc_id,td.transport_p_name as plan_p_name,`to`.to_p_name send_p_name,td.transport_product_serial_number as plan_serial_number,`to`.to_serial_number as send_serial_number,COALESCE(td.transport_count,0) as plan_count,`to`.to_count as send_count,(`to`.to_count-COALESCE(td.transport_count,0)) as different_count,CASE WHEN (`to`.to_count-COALESCE(td.transport_count,0))>0 THEN "+DifferentKey.More+" WHEN (`to`.to_count-COALESCE(td.transport_count,0))=0 THEN "+DifferentKey.Equal+" END as different_type from transport_outbound as `to` " 
						+"left join transport_detail as td on `to`.to_pc_id = td.transport_pc_id and `to`.to_serial_number = td.transport_product_serial_number and td.transport_id = `to`.to_transport_id " 
						+" where `to`.to_transport_id = "+transport_id+" and `to`.to_serial_number is not null "
						+" order by different_type";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ planSendDifferentsSN error:"+e);
		}
	}
	
	/**
	 * 模拟装货收货添加
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addSimulationOutIn(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("simulation_out_in"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ addSimulationOutIn error:"+e);
		}
	}
	
	public void delSimulationOutIn(long transport_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where simulation_transport_id ="+transport_id,ConfigBean.getStringValue("simulation_out_in"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ delSimulationOutIn error:"+e);
		}
	}
	
	/**
	 * 根据文件名查询模拟出入库数据
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSimulationOutInByFileName(String fileName)
		throws Exception
	{
		try 
		{
			String sql = " select * from "+ConfigBean.getStringValue("simulation_out_in")+" where simulation_file_name = ? ";
			
			DBRow para = new DBRow();
			para.add("simulation_file_name",fileName);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getSimulationOutInByFileName error:"+e);
		}
	}
	
	/**
	 * 模拟发货文件错误
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] simulationTransportOutError(String fileName)
		throws Exception
	{
		try 
		{
			String sql = "select soiProduct.simulation_line_number as line_number,soiProduct.simulation_transport_id as transport_id,soiProduct.simulation_pc_id as pc_id,soiProduct.simulation_p_name as p_name,soiProduct.simulation_count as count,soiProduct.simulation_serial_number as serial_number,'ProductNotExist' as error_type,soiProduct.simulation_type from "+ConfigBean.getStringValue("simulation_out_in")+" as soiProduct where simulation_pc_id = 0 and soiProduct.simulation_file_name ='"+fileName+"' and soiProduct.simulation_type = "+SimulationTransportKey.Out+" "
						+"union " 
						+"select soiCount.simulation_line_number as line_number,soiCount.simulation_transport_id as transport_id,soiCount.simulation_pc_id as pc_id,soiCount.simulation_p_name as p_name,soiCount.simulation_count as count,soiCount.simulation_serial_number as serial_number,'CountNoOne' as error_type,soiCount.simulation_type from "+ConfigBean.getStringValue("simulation_out_in")+" as soiCount where soiCount.simulation_serial_number is not null and soiCount.simulation_count !=1 and soiCount.simulation_file_name ='"+fileName+"' and soiCount.simulation_type = "+SimulationTransportKey.Out+" "
						+"union " 
						+"select soi.simulation_line_number as line_number,soi.simulation_transport_id as transport_id,soi.simulation_pc_id as pc_id,soi.simulation_p_name as p_name,soi.simulation_count as count,soi.simulation_serial_number as serial_number,'SerialNumberRepeat' as error_type,soi.simulation_type from "+ConfigBean.getStringValue("simulation_out_in")+" as soi "
						+"join " 
						+"("
						+"	select * from " 
						+"	("
						+"		select simulation_serial_number as serial_number,count(simulation_out_in_id)as times from "+ConfigBean.getStringValue("simulation_out_in")+" where simulation_serial_number is not NULL and simulation_file_name ='"+fileName+"' and soiCount.simulation_type = "+SimulationTransportKey.Out+" GROUP BY simulation_serial_number "
						+"	) as importSerialNumberRepeat where times <>1 " 
						+")as isnr on soi.simulation_serial_number = isnr.serial_number " 
						+"where soi.simulation_file_name ='"+fileName+"' and soiCount.simulation_type = "+SimulationTransportKey.Out+" ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ simulationTransportOutError error:"+e);
		}
	}
	
	/**
	 * 模拟收货文件错误
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] simulationTransportInError(String fileName)
		throws Exception
	{
		try 
		{
			String sql = "select soiProduct.simulation_line_number as line_number,soiProduct.simulation_transport_id as transport_id,soiProduct.simulation_pc_id as pc_id,soiProduct.simulation_p_name as p_name,soiProduct.simulation_count as count,soiProduct.simulation_serial_number as serial_number,soiProduct.simulation_lp as lp,'ProductNotExist' as error_type,soiProduct.simulation_type from simulation_out_in as soiProduct where simulation_pc_id = 0 and soiProduct.simulation_file_name ='"+fileName+"' and soiProduct.simulation_type = "+SimulationTransportKey.In+" "
						+"union " 
						+"select soiCount.simulation_line_number as line_number,soiCount.simulation_transport_id as transport_id,soiCount.simulation_pc_id as pc_id,soiCount.simulation_p_name as p_name,soiCount.simulation_count as count,soiCount.simulation_serial_number as serial_number,soiCount.simulation_lp as lp,'CountNoOne' as error_type,soiCount.simulation_type from simulation_out_in as soiCount where soiCount.simulation_serial_number is not null and soiCount.simulation_count !=1 and soiCount.simulation_file_name ='"+fileName+"' and soiCount.simulation_type = "+SimulationTransportKey.In+" "
						+"union "
						+"select soiLPNull.simulation_line_number as line_number,soiLPNull.simulation_transport_id as transport_id,soiLPNull.simulation_pc_id as pc_id,soiLPNull.simulation_p_name as p_name,soiLPNull.simulation_count as count,soiLPNull.simulation_serial_number as serial_number,soiLPNull.simulation_lp as lp,'ProductNotInPlate' as error_type,soiLPNull.simulation_type from simulation_out_in as soiLPNull where soiLPNull.simulation_lp is null and soiLPNull.simulation_file_name ='"+fileName+"' and soiLPNull.simulation_type = "+SimulationTransportKey.In+" "
						+"union " 
						+"select soi.simulation_line_number as line_number,soi.simulation_transport_id as transport_id,soi.simulation_pc_id as pc_id,soi.simulation_p_name as p_name,soi.simulation_count as count,soi.simulation_serial_number as serial_number,soi.simulation_lp as lp,'SerialNumberRepeat' as error_type,soi.simulation_type from simulation_out_in as soi "
						+"join " 
						+"( "
						+"	select * from " 
						+"	("
						+"			select simulation_serial_number as serial_number,count(simulation_out_in_id)as times from simulation_out_in where simulation_serial_number is not NULL and simulation_file_name ='"+fileName+"' and simulation_type = "+SimulationTransportKey.In+" GROUP BY simulation_serial_number "
						+"	) as importSerialNumberRepeat where times <>1 "
						+")as isnr on soi.simulation_serial_number = isnr.serial_number "
						+"where soi.simulation_file_name ='"+fileName+"' and soi.simulation_type = "+SimulationTransportKey.In+" "
						+"order by line_number ";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ simulationTransportInError error:"+e);
		}
	}
	
	/**
	 * 根据B2B订单ID获得B2B运单
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDetailTransportsByB2BOid(long b2b_oid)
		throws Exception
	{
		try 
		{
			String sql = "select distinct t.* from transport as t "
						+"join transport_detail as td on t.transport_id= td.transport_id "
						+"where td.b2b_oid = ? ";
			
			DBRow para = new DBRow();
			para.add("b2b_oid",b2b_oid);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getDetailTransportsByB2BOid error:"+e);
		}
	}
	public DBRow[] getTransportOrderOutList(long transport_id,int system_bill_type)
		throws Exception
	{
		try 
		{  
			String sql ="SELECT slc.slc_position_all,old.out_list_pc_id,old.pick_up_quantity,old.from_container_type,old.from_container_type_id," +
					"old.from_con_id,old.pick_container_type,old.pick_container_type_id,old.pick_con_id FROM out_list_detail old " +
					"JOIN storage_location_catalog slc ON slc.slc_id = old.out_list_slc_id " +
					"WHERE old.system_bill_id= "+transport_id+"  AND old.system_bill_type ="+system_bill_type+" order by slc.slc_position_all asc";
		
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportMgrZJ getTransportOrderOutList error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
}
