package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorTransportOutboundMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加转运出库明细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addTransportOutBound(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("transport_outbound"),dbrow);
			
			
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportOutboundMgrZJ addTransportOutBound error:"+e);
		}
	}
	
	/**
	 * 删除转运单出库详细
	 * @param transport_id
	 * @throws Exception
	 */
	public void delTransportOutBoundByTransportId(long transport_id,String machine_id)
		throws Exception
	{
		try 
		{
			String sql = "where to_transport_id="+transport_id;
			if(null != machine_id && !"".equals(machine_id))
			{
				sql+=" and to_machine_id= '"+machine_id+"'";
			}
			dbUtilAutoTran.delete(sql,ConfigBean.getStringValue("transport_outbound"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportOutboundMgrZJ delTransportOutBoundByTransportId error:"+e);
		}
	}
	
	/**
	 * 转运发货明细比较
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] compareTransportPacking(long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_outbound_compare_view")+" where real_send_count != transport_count and transport_id=?";
			
			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportOutboundMgrZJ compareTransportPacking error:"+e);
		}
	}
	
	/**
	 * 获得转运装箱明细
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportOutboundByTransportId(long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_outbound")+" where to_transport_id=?";
			
			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportOutboundMgrZJ getTransportOutboundByTransportId error:"+e);
		}
	}
	
	/**
	 * 计划转运与转运发货差异
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] compareTransportDetailOutByTransportId(long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from ( "
						+"select td.transport_pc_id as pc_id,td.transport_product_serial_number as serial_number,td.transport_count as plan_count,`to`.to_count as out_count,(td.transport_count - COALESCE(`to`.to_count, 0)) as different_count,COALESCE('Lack') as different_type,`to`.to_machine_id as machine_id from transport_detail as td "
						+"left join transport_outbound as `to` on td.transport_id = `to`.to_transport_id and td.transport_product_serial_number = `to`.to_serial_number "
						+"where td.transport_product_serial_number is not NULL and td.transport_id = ? "
						+"union all "
						+"select `to`.to_pc_id as pc_id,`to`.to_serial_number as serial_number,td.transport_count as plan_count,`to`.to_count as out_count,(`to`.to_count - COALESCE(td.transport_count, 0)) as different_count,COALESCE('More') as different_type,`to`.to_machine_id as machine_id from transport_outbound as `to` "
						+"LEFT JOIN transport_detail td on `to`.to_pc_id = td.transport_pc_id and `to`.to_serial_number = td.transport_product_serial_number "
						+"where `to`.to_serial_number is not NULL and `to`.to_transport_id = ? "
						+") as differentView where different_count >0";
			
			DBRow para = new DBRow();
			para.add("transport_id",transport_id);
			para.add("to_transport_id",transport_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportOutboundMgrZJ compareTransportDetailOutByTransportId error:"+e);
		}
	}
	
	/**
	 * 转运单发货数量比较
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] transportDetailOutCompareCountByTransportId(long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from ( "
						+"select planView.pc_id,plan_count,out_count,(planView.plan_count-COALESCE(outView.out_count,0)) different,'Lack' as diiferent_type from " 
						+"(select transport_pc_id as pc_id,sum(transport_count) as plan_count  from transport_detail where transport_id = "+transport_id+" GROUP BY transport_pc_id) as planView "
						+"LEFT JOIN (select to_pc_id as pc_id,sum(to_count) as out_count from transport_outbound where to_transport_id = "+transport_id+" group by to_pc_id)as outView on planView.pc_id = outView.pc_id "
						+"union all "
						+"select planView.pc_id,plan_count,out_count,(outView.out_count-COALESCE(planView.plan_count,0)) different,'More' as diiferent_type from " 
						+"(select to_pc_id as pc_id,sum(to_count) as out_count from transport_outbound where to_transport_id = "+transport_id+" group by to_pc_id)as outView "
						+"LEFT JOIN (select transport_pc_id as pc_id,sum(transport_count) as plan_count  from transport_detail where transport_id = "+transport_id+" GROUP BY transport_pc_id) as planView on planView.pc_id = outView.pc_id "
						+") as compareView where different>0";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportOutboundMgrZJ transportDetailOutCompareCountByTransportId error:"+e);
		}
	}
	
	public DBRow[] getTransportOutboundBySNNotMachine(String sn,String machine_id,long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_outbound")+" where to_machine_id != ? and to_serial_number = ? and to_transport_id = ?";
			
			DBRow para = new DBRow();
			para.add("to_machine_id",machine_id);
			para.add("to_serial_number",sn);
			para.add("to_transport_id",transport_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportOutbound getTransportOutboundBySNNotMachine error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
