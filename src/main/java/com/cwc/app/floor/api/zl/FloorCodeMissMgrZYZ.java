package com.cwc.app.floor.api.zl;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;


public class FloorCodeMissMgrZYZ {
	
	private DBUtilAutoTran dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	//查询所有不重复的miss_code
	public DBRow[] getAllCodeMiss(PageCtrl pc) throws Exception{
		try {
			String sql = "select DISTINCT(miss_code) from "+ConfigBean.getStringValue("code_miss");
			if(pc !=null)
				return dbUtilAutoTran.selectMutliple(sql, pc);
			else
				return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorCodeMissMgrZYZ getAllCodeMiss error:" + e);
		}
	}
	//根据miss_code获取ItemNumber
	public DBRow[] getItemNumber(String miss_code) throws Exception{
		
		try {
				String sql = "select DISTINCT(item_number) from "+ConfigBean.getStringValue("code_miss")+" c join  "+ConfigBean.getStringValue("trade_item")+" t on c.miss_oid=t.oid  where c.miss_code='"+miss_code+"'";
				return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorCodeMissMgrZYZ getItemNumber error:" + e);
		}
	}
	//根据miss_code获取Miss_id
	public DBRow[] getMissID(String miss_code) throws Exception{
		try {
			String sql = "select miss_id from "+ConfigBean.getStringValue("code_miss")+" where miss_code =(select miss_code from "+ConfigBean.getStringValue("code_miss")+" where miss_code='"+miss_code+"' LIMIT 1)";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorCodeMissMgrZYZ getMissID error:" + e);
		}
	}
}
	