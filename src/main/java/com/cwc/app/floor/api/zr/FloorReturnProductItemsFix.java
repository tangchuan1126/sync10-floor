package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorReturnProductItemsFix
{

	private DBUtilAutoTran dbUtilAutoTran;
	
	 
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public long addReturnProdcutItemsFix(DBRow row) throws Exception {
		try{
			
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("return_product_items_fix"), row);
		}catch (Exception e) {
			throw new Exception("FloorReturnProductItemsFix.addReturnProdcutItemsFix(DBRow):"+e);
		}
	}
	public DBRow[] getReturnProdcutItemsFixByRPId(long rp_id) throws Exception {
		try{
			String sql = "select * from " + ConfigBean.getStringValue("return_product_items_fix") + " where rp_id=" + rp_id ;
			return dbUtilAutoTran.selectMutliple(sql);
		 
		}catch (Exception e) {
			throw new Exception("FloorReturnProductItemsFix.getReturnProdcutItemsFixByRPId(rp_id):"+e);
		}
	}
	public long addReturnProductItemReason(DBRow row) throws Exception {
		try{
			 String tableName = ConfigBean.getStringValue("return_product_reason_zr") ;
			 return dbUtilAutoTran.insertReturnId(tableName, row);
		}catch (Exception e) {
			throw new Exception("FloorReturnProductItemsFix.addReturnProductItemReason(row):"+e);
		}
	}
	public DBRow[] getReturnProductReasonByRpId(long rp_id) throws Exception {
		try{
			 String tableName = ConfigBean.getStringValue("return_product_reason_zr") ;
			 String sql = "select * from "  + tableName + " where rp_id=" + rp_id;
			 return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorReturnProductItemsFix.addReturnProductItemReason(row):"+e);
		}
		
	}
	public long addReturnProductItem(DBRow row) throws Exception {
		try{
			 String tableName = ConfigBean.getStringValue("return_product_items") ;
			 long rpi_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("return_product_items"));
			 row.add("rpi_id",rpi_id);
			 dbUtilAutoTran.insertReturnId(tableName, row);
			 return rpi_id;
		}catch (Exception e) {
			throw new Exception("FloorReturnProductItemsFix.addReturnProductItem(row):"+e);
		}
	}
	public void updateReturnProductItemFix(DBRow row , long rpi_id) throws Exception{
		try{
			 String tableName = ConfigBean.getStringValue("return_product_items_fix") ;
			 dbUtilAutoTran.update(" where fix_rpi_id=" + rpi_id, tableName, row);
		}catch (Exception e) {
			throw new Exception("FloorReturnProductItemsFix.updateReturnProductItemFix(row,rpi_id):"+e);
		}
	}
	public DBRow[] getReturnProductItemByRpIdAndFixId(long items_fix_rpi_id,long rp_id) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("return_product_items")   + " where items_fix_rpi_id=" +items_fix_rpi_id  + " and rp_id="+rp_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorReturnProductItemsFix.getReturnProductItemByRpIdAndFixId(items_fix_rpi_id,rpi_id):"+e);
		}
	}
	public DBRow[] getReturnProductItemByRpId(long rp_id) throws Exception {
		try{
			String sql = "select * from " + ConfigBean.getStringValue("return_product_items")   + " where rp_id=" +rp_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorReturnProductItemsFix.getReturnProductItemByRpId(rp_id):"+e);
		}
	}
	public void updateReturnProductItem(DBRow row , long rpi_id)throws Exception {
		try{
 			 dbUtilAutoTran.update(" where rpi_id=" + rpi_id, ConfigBean.getStringValue("return_product_items"), row);
		}catch (Exception e) {
			throw new Exception("FloorReturnProductItemsFix.updateReturnProductItem(row):"+e);
		}
	}
	public void deleteReturnProductItem(long rpi_id) throws Exception {
		try{
			dbUtilAutoTran.delete(" where rpi_id="+rpi_id, ConfigBean.getStringValue("return_product_items"));
 		}catch (Exception e) {
			throw new Exception("FloorReturnProductItemsFix.deleteReturnProductItem(row):"+e);
		}
	}
	public void deleteReturnProductItemsFix(long fix_rpi_id) throws Exception {
		try{
			dbUtilAutoTran.delete(" where fix_rpi_id="+ fix_rpi_id, ConfigBean.getStringValue("return_product_items_fix"));
		}catch (Exception e) {
			throw new Exception("FloorReturnProductItemsFix.deleteReturnProductItemsFix(fix_rpi_id):"+e);
		}
	}
	public DBRow getReturnProductItemByRpiId(long rpi_id) throws Exception {
		try{
			return dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("return_product_items") +" where rpi_id=" + rpi_id);
		}catch (Exception e) {
			throw new Exception("FloorReturnProductItemsFix.getReturnProductItemByRpiId(rpi_id):"+e);

		}
	}
	public DBRow[] getCheckReturnProductItem(long rpi_id) throws Exception {
		try{
			StringBuffer sql = new StringBuffer("select * from ").append(ConfigBean.getStringValue("return_product_items_zr_check"))
			.append(" where rpi_id=").append(rpi_id);
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorCheckReturnProductItemMgrZr.getCheckReturnProductItem(rpi_id):"+e);
		}
	}
	public void deleteCheckReturnProduct(long check_return_product_id) throws Exception {
		try{
				
			dbUtilAutoTran.delete(" where check_return_product_id="+check_return_product_id, ConfigBean.getStringValue("return_product_items_zr_check"));
		}catch (Exception e) {
			throw new Exception("FloorCheckReturnProductItemMgrZr.deleteCheckReturnProduct(check_return_product_id):"+e);
		}
	}
	public int countCheckReturnProductByRpId(long rpId) throws Exception {
		try{
			
			String sql = "select count(*) as sum_count from " +   ConfigBean.getStringValue("return_product_items_zr_check") + " where rp_id="+rpId +" and return_product_check = 1";
			return dbUtilAutoTran.selectSingle(sql).get("sum_count", 0);
		}catch (Exception e) {
			throw new Exception("FloorCheckReturnProductItemMgrZr.countCheckReturnProductByRpId(rpId):"+e);

		}
	}
	public DBRow[] getReturnProductCheckByRPId(long rp_id) throws Exception {
		try{
			String sql = "select * from " + ConfigBean.getStringValue("return_product_items_zr_check") + " where rp_id=" +rp_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorCheckReturnProductItemMgrZr.getReturnProductCheckByRPId(rp_id):"+e);
		}
	}
	public DBRow[] getReturnProductItemFixByRpiId(long rpi_id) throws Exception {
		try{
			String sql = "select * from " +ConfigBean.getStringValue("return_product_items_fix") + " where rpi_id="+rpi_id ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorCheckReturnProductItemMgrZr.getReturnProductItemFixByRpiId(rpi_id):"+e);
		}
	}
	public DBRow gerReturnProductItemFixByFixRpiId(long fix_rpi_id) throws Exception {
		try{
			StringBuffer sql = new StringBuffer("select * from ").append( ConfigBean.getStringValue("return_product_items_fix")).append(" where fix_rpi_id=").append(fix_rpi_id);
			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorCheckReturnProductItemMgrZr.gerReturnProductItemFixByFixRpiId(fix_rpi_id):"+e);
		}
	}
	public void updateReturnProduct(long rp_id , DBRow updateRow) throws Exception {
		try{
			dbUtilAutoTran.update(" where rp_id="+rp_id, ConfigBean.getStringValue("return_product"), updateRow);
		}catch (Exception e) {
			throw new Exception("FloorCheckReturnProductItemMgrZr.updateReturnProduct(rp_id,row):"+e);
		}
	}
}
