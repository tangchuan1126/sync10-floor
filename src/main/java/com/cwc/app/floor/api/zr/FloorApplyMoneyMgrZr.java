package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorApplyMoneyMgrZr {
	
	private DBUtilAutoTran dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public DBRow[] getAllTransMoneyIds(long apply_money_id) throws Exception {
		try{
			 String tableName =  ConfigBean.getStringValue("apply_transfer");
			 String sql = "select * from "+tableName+" where apply_money_id =" + apply_money_id;
			 return dbUtilAutoTran.selectMutliple(sql) ;
		}catch(Exception e){
			throw new Exception("FloorApplyMoneyMgrZr.getAllTransIds(apply_money_id):"+e);
		}
	}
	public DBRow  getUserNameFromAdid(long adid) throws Exception {
		try{
			 String tableName =  ConfigBean.getStringValue("admin");
			 if( adid == 0l){
				 return null ;
			 }
			 String sql = "select employe_name from "+tableName+" where adid='"+adid+"'";
			 return dbUtilAutoTran.selectSingle(sql) ;
		}catch(Exception e){
			throw new Exception("FloorApplyMoneyMgrZr.getUserNameFromAdid(adid):"+e);
		}
		
	}
	public DBRow[] getApplyMoneyById4Search(long[] ids) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("apply_money");
			if(ids == null  || (ids != null && ids.length < 1)){
				return (new DBRow[0]);
			}else{
				StringBuffer whereContion = new StringBuffer("");
				
					for(int index = 0 , count = ids.length ; index < count ; index++ ){
						if(index == 0){
							whereContion.append(" where apply_id=").append(ids[0]);
						}else{
							whereContion.append(" or apply_id=").append(ids[index]);
					}
				}
				return dbUtilAutoTran.selectMutliple("select * from " +tableName + whereContion.toString() + " order by apply_id desc");
					
			}
			 
		}catch(Exception e){
			throw new Exception("FloorApplyMoneyMgrZr.getBillByBillId4Search(ids):"+e);
		}
	}

	public DBRow[] getApplyTransferById4Search(long[] ids) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("apply_transfer");
			if(ids == null  || (ids != null && ids.length < 1)){
				return (new DBRow[0]);
			}else{
				StringBuffer whereContion = new StringBuffer("");
				
					for(int index = 0 , count = ids.length ; index < count ; index++ ){
						if(index == 0){
							whereContion.append(" where transfer_id=").append(ids[0]);
						}else{
							whereContion.append(" or transfer_id=").append(ids[index]);
					}
				}
				return dbUtilAutoTran.selectMutliple("select * from " +tableName + whereContion.toString());
					
			}
			 
		}catch(Exception e){
			throw new Exception("FloorApplyMoneyMgrZr.getApplyTransferById4Search(ids):"+e);
		}
	}
	
 
}
