package com.cwc.app.floor.api;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorCustomerIdMgr {
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public DBRow[] fillterCustomerId(DBRow dbRow, PageCtrl ctrl) throws Exception {
		String sql = "select * from " + "customer_id" + " where 1=1";
		if (!dbRow.get("customer_id", "").equals("")) {
			sql += " and customer_id=" + dbRow.get("customer_id", "");
		}
		if (!dbRow.get("customer_key", "").equals("")) {
			sql += " and customer_key=" + dbRow.get("customer_key", "");
		}
		return this.dbUtilAutoTran.selectMutliple(sql, ctrl);
	}
	
	/**
	 * 客户品牌
	 * 
	 * @param dbRow
	 * @param ctrl
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterCustomerBrand(DBRow dbRow, PageCtrl ctrl) throws Exception {
		String sql = "select * from  customer_brand where 1=1";
		DBRow para = new DBRow();
		
		if (dbRow.get("cb_id", 0)>0) {
			sql += " and cb_id=?";
			para.put("cb_id", dbRow.get("cb_id", 0L));
		}
		if (!dbRow.get("brand_name", "").equals("")) {
			sql += " and brand_name=?";
			para.put("brand_name", dbRow.get("brand_name", ""));
		}
		if (para.getFieldNames().size()==0) {
			return this.dbUtilAutoTran.selectMutliple(sql, ctrl);
		} else {
			return this.dbUtilAutoTran.selectPreMutliple(sql, para, ctrl);
		}
	}
}
