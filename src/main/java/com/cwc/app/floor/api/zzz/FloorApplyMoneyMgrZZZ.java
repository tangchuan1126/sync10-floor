package com.cwc.app.floor.api.zzz;

import com.cwc.app.key.ApplyStatusKey;
import com.cwc.app.key.FundStatusKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorApplyMoneyMgrZZZ 
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	/**
	 * 申请资金单的添加
	 * @param row
	 * @throws Exception
	 */
	public long addApplyMoney(DBRow row)
	throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("apply_money"));
			row.add("apply_id",id);
			
		    dbUtilAutoTran.insert(ConfigBean.getStringValue("apply_money"),row);
		    return (id);
		}
		catch (Exception e)
		{
			throw new Exception("FloorApplyMoneyMgrZZZ.addApplyMoney(row) error:" + e);
		}
	}
	
	/**
	 * 最迟转款时间排序查询
	 */
	public DBRow[] getApplyMoneyLastTimeSort(PageCtrl pc)throws Exception{
		try{
			String sql="select * from apply_money ORDER BY last_time DESC";
			return this.dbUtilAutoTran.selectMutliple(sql, pc);
		}catch(Exception e){
			throw new Exception("FloorApplyMoneyMgrZZZ.getApplyMoneySort(pc)"+e);
		}
	}
	/**
	 * 按创建时间排序
	 * @param pc 分页参数
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApplyMoneyCreateTimeSort(PageCtrl pc)throws Exception{
		try{
			String sql="select * from apply_money ORDER BY create_time DESC";
			return this.dbUtilAutoTran.selectMutliple(sql, pc);
		}catch(Exception e){
			throw new Exception("FloorApplyMoneyMgrZZZ.getApplyMoneySort(pc)"+e);
		}
	}
	
	/**
	 * 图片上传的添加
	 * @param row
	 * @throws Exception
	 */
	public long addUploadImage(DBRow row) throws Exception{
		try {
			long id = dbUtilAutoTran.getSequance("apply_images");
			row.add("id", id);
			dbUtilAutoTran.insert("apply_images", row);
			return (id);
		} catch (Exception e) {
			throw new Exception("FloorApplyMoneyMgrZZZ.addUploadImage(row) error:" + e);
		}
	}
	
	/**
	 * 对资金申请单进行全查
	 * @param pc
	 * @return
	 * @throws Exception
	 */
    public DBRow[] getApplyMoney(PageCtrl pc)
    throws Exception
    {
    	try
		{
			String sql = "select * from "
				+ ConfigBean.getStringValue("apply_money")+" where types in (select category_id from " + ConfigBean.getStringValue("apply_money_category")+" ) order by apply_id DESC";
			
			
			if(pc!=null)
			{
				return (dbUtilAutoTran.selectMutliple(sql,pc));
			}
			else
			{
				
				return (dbUtilAutoTran.selectMutliple(sql));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorApplyMoneyMgrZZZ.getApplyMoney(pc) error:" + e);
		}
    	
    }
    /**
     * 根据ID查询资金申请记录
     * @param id
     * @return
     * @throws Exception
     */
    public DBRow[] getApplyMoneyById(long id)
    	throws Exception
    {
    	
    	try
    	{
    		String sql="select * from "+ConfigBean.getStringValue("apply_money")+" where apply_id="+id;
    		return (dbUtilAutoTran.selectMutliple(sql));
    	}
    	catch(Exception e)
    	{
    		
    		throw new Exception("FloorApplyMoneyMgrZZZ.getApplyMoneyById(id)"+e);
    	}
    }
    
    public DBRow getDetailApplyMoneyById(long id)
		throws Exception
	{
		
		try
		{
			String sql="select * from "+ConfigBean.getStringValue("apply_money")+" where apply_id=?";
			
			DBRow para =new DBRow();
			para.add("apply_id",id);
			
			return (dbUtilAutoTran.selectPreSingle(sql,para));
		}
		catch(Exception e)
		{
			
			throw new Exception("FloorApplyMoneyMgrZZZ.getDetailApplyMoneyById(id)"+e);
		}
	}
    /**
     * 根据资金申请ID修改记录
     * @param id
     * @param row
     * @throws Exception
     */
    public void updateApplyMoneyById(long id,DBRow row)
    throws Exception
    {
    	try
    	{
    		dbUtilAutoTran.update("where apply_id="+id, ConfigBean.getStringValue("apply_money"), row);
    	}
    	catch
    	(Exception e)
    	{
    	    throw new  Exception("FloorApplyMoneyMgrZZZ.updateApplyMoneyById(id,row)"+e);	
    	}
    }
    /**
     * 根据类型、创建时间、状态查询资金申请
     * @param startTime
     * @param endTime
     * @param types
     * @param status
     * @param pc
     * @return
     * @throws Exception
     */
    public DBRow[] getApplyMoneyByCondition(String selectSortTime,String startTime,String endTime,int types,int status,long productLineId,long centerAccountId,long association_type_id, PageCtrl pc)
    throws Exception
    {
    	try
    	{
    		StringBuffer sqlCondition=new StringBuffer();
	    	sqlCondition.append("where 1=1");
	    	if(!startTime.equals("")) {
	    		sqlCondition.append(" and create_time>'"+startTime+" 00:00:00'");
	    	}
	    	if(!endTime.equals("")) {
	    		sqlCondition.append(" and create_time<'"+endTime+" 23:59:59'");
	    	}
	    	if(types!=0)
	    	{
	    		sqlCondition.append(" and types in (select category_id from "+ConfigBean.getStringValue("apply_money_category")+" where category_id="+types+" or prent_id="+types+")");
	    	}
	    	if(status!=-1)
	    	{
	    		sqlCondition.append(" and status="+status);
	    	}
	    	if(productLineId!=0) {
	    		sqlCondition.append(" and product_line_id="+productLineId);
	    	}
	    	if(centerAccountId!=0) {
	    		sqlCondition.append(" and center_account_id="+centerAccountId);
	    	}
	    	if(association_type_id!=-1)
	    	{
	    		sqlCondition.append(" and association_type_id="+association_type_id);
	    	}
    		String sql = "select * from "
				+ ConfigBean.getStringValue("apply_money")+" "+sqlCondition.toString()+" order by "+selectSortTime+" DESC ";
    		
			if(pc!=null)
			{
				return (dbUtilAutoTran.selectMutliple(sql,pc));
			}
			else
			{
				
				return (dbUtilAutoTran.selectMutliple(sql));
			}
    	}
    	catch(Exception e)
    	{
    		throw new Exception("FloorApplyMoneyMgrZZZ.getApplyMoneyByCondition(sql)"+e);
    	}
    }
    /**
	 * 根据关联ID和类型查询资金申请
	 * @param types
	 * @param id
	 * @return
	 * @throws Exception
	 */
    public DBRow getApplyMoneyByTypeAndAssociationId(int types,long id)
    throws Exception
    {
    	try
    	{
    		String sql="select * from "+ConfigBean.getStringValue("apply_money")+" where association_id="+id+
    		" and types="+types;
    		return (dbUtilAutoTran.selectSingle(sql));
    	}
    	catch(Exception e)
    	{
    		
    		throw new Exception("FloorApplyMoneyMgrZZZ.getApplyMoneyByTypeAndAssociationId(types,id)"+e);
    	}
    }
    /**
     * 根据ID修改状态为已付款
     * @param id
     * @param row
     * @throws Exception
     */
    public void updateApplyMoney(long id,DBRow row)
    throws Exception
    {
    	try
    	{
    		dbUtilAutoTran.update("where apply_id="+id, ConfigBean.getStringValue("apply_money"), row);
    	}
    	catch(Exception e)
    	{
    		throw new Exception("FloorApplyMoneyMgrZZZ.updateApplyMoney(row)"+e);
    	}
    }
    /**
     * 修改备注，根据ID
     * @param id
     * @param row
     */
    public void updateRemarkByApplyId(long id,DBRow row)
    throws Exception
    {
    	try
    	{
    		dbUtilAutoTran.update("where apply_id="+id, ConfigBean.getStringValue("apply_money"), row);
    	}
    	catch(Exception e)
    	{
    		throw new Exception("FloorApplyMoneyMgrZZZ.updateRemarkByApplyId(id,row):"+e);
    	}
    }
    /**
     * 根据申请资金ID删除
     * @param id
     * @throws Exception
     */
    public void deleteApplyMoney(long id)
    throws Exception
    {
    	try
    	{
    		dbUtilAutoTran.delete("where apply_id="+id, ConfigBean.getStringValue("apply_money"));
    	}
    	catch(Exception e)
    	{
    		throw new Exception("FloorApplyMoneyMgrZZZ.deleteApplyMoney(id):"+e);
    	}
    }
    /**
     * 根据信息进行模糊查询
     * @return
     * @throws Exception
     */
    public DBRow[] getApplyMoneyByInfo(String info,PageCtrl pc)
    throws Exception
    {
    	try
    	{
    		info = "%"+info+"%";
    		DBRow para = new DBRow();
    		para.add("apply_id", info);
    		para.add("payee", info);
    		para.add("association_id", info);
    		para.add("payment_information", info);
    		para.add("remark", info);
    		para.add("creater", info);
    		
    		String sql = "select * from "+ConfigBean.getStringValue("apply_money")+" where apply_id like '"+info+"' " +
    				"or payee like '"+info+"' or association_id like '"+info+"' or payment_information like '"+info+"' or remark like '"+info+"' or creater like '"+info+"' order by apply_id DESC";
    		
    		return dbUtilAutoTran.selectMutliple(sql, pc);
    	}
    	catch(Exception e)
    	{
    		throw new Exception("FloorApplyMoneyMgrZZZ.getApplyMoneyByInfo(String info,PageCtrl pc):"+e);
    	}
    }
    
    /**
     * 交货单转换为转运单使用
     * @param associationType
     * @param associationId
     * @param para
     * @throws Exception
     */
    public void updateApplyMoneyByAssociationTypeAndAssociationId(int associationType,long associationId,DBRow para)
    	throws Exception
    {
    	try 
    	{
			dbUtilAutoTran.update("where association_id = "+associationId+" and association_type_id = "+associationId,ConfigBean.getStringValue("apply_money"),para);
		}
    	catch (Exception e) 
    	{
			throw new Exception("Floor");
		}
    }
    
    /**
     * 根据资金申请类型，关联单据查询资金申请
     * @author zhanjie
     * @param types
     * @param association_id
     * @param association_type_id
     * @return
     * @throws Exception
     */
    public DBRow[] getApplyMoneyByTypeAndAssociationIdAndAssociationNoCancel(int types,long association_id,int association_type_id)
		throws Exception
	{
    	try
    	{
			String sql = "select * from "+ConfigBean.getStringValue("apply_money")+" where `status` !="+ApplyStatusKey.CANCEL+" and types = ? and association_id = ? and association_type_id =?";
			
			DBRow para = new DBRow();
			para.add("types",types);
			para.add("association_id",association_id);
			para.add("association_type_id",association_type_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
    	catch (Exception e) 
    	{
			throw new Exception("FloorApplyMoneyMgrZZZ getApplyMoneyByTypeAndAssociationIdAndAssociation error:"+e);
		}
	}
    
    /**
     * 通过关联Id,关联类型，查询资金
     * @param types
     * @param association_id
     * @param association_type_id
     * @return
     * @throws Exception
     */
    public DBRow[] getApplyMoneyByAssociationIdAndAssociationTypeIdsNoCancel(int[] types,long association_id,int association_type_id)
	throws Exception
	{
	try
	{
		String sql = "select * from "+ConfigBean.getStringValue("apply_money")+" where `status` !="+ApplyStatusKey.CANCEL+" and association_id = ? and association_type_id = ?";
		
		if(null != types && types.length > 0)
		{
			String sqlWhere = "";
			if(1 == types.length)
			{
				sqlWhere = " and types = " + types[0];
			}
			else
			{
				sqlWhere = " and (types = " + types[0];
				for (int i = 1; i < types.length; i++) {
					sqlWhere += " or types = " + types[i];
				}
				sqlWhere += ")";
			}
			sql += sqlWhere;
		}
		DBRow para = new DBRow();
		para.add("association_id",association_id);
		para.add("association_type_id",association_type_id);
		
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	catch (Exception e) 
	{
		throw new Exception("FloorApplyMoneyMgrZZZ getApplyMoneyByTypeAndAssociationIdAndAssociation error:"+e);
	}
}
    
    /**
     * 通过资金关联ID,类型，项目，查询转款总额
     * @param types
     * @param association_id
     * @param association_type_id
     * @return
     * @throws Exception
     */
    public DBRow getApplyMoneyTransferTotalByTypeAndAssociationIdAndAssociationNoCancel(int types,long association_id,int association_type_id) throws Exception
    {
    	try {
    		String sql = "select sum(amount_transfer) as amount_transfer_total from "+ConfigBean.getStringValue("apply_money")+" where `status` !="+FundStatusKey.CANCEL+" and types = "+types+" and association_id = "+association_id+" and association_type_id ="+association_type_id;
    		return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorApplyMoneyMgrZZZ getApplyMoneyTransferTotalByTypeAndAssociationIdAndAssociationNoCancel error:"+e);
		}
    }
}
