package com.cwc.app.floor.api.ll;

import com.cwc.app.beans.ll.ApplyMoneyCategoryBeanLL;
import com.cwc.app.beans.ll.ApplyMoneyLogs;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;

public class FloorApplyMoneyLogsMgrLL {
	private ApplyMoneyLogs applyMoneyLogs;
	
	public ApplyMoneyLogs getApplyMoneyLogs() {
		return applyMoneyLogs;
	}

	public void setApplyMoneyLogs(ApplyMoneyLogs applyMoneyLogs) {
		this.applyMoneyLogs = applyMoneyLogs;
	}
	
	private DBRow insertApplyMoneyLogs(DBRow row) throws Exception {
		return applyMoneyLogs.insertRow(row);
	}
	
	public void insertLogs(String applyId, String content, long oprator_id, String oprator) throws Exception {
		TDate tDate = new TDate();		
		DBRow logRow = new DBRow();
		logRow.add("applyid", applyId);
		logRow.add("context", content);
		logRow.add("userName_id", oprator_id);
		logRow.add("userName", oprator==null?"":oprator);
		logRow.add("createDate", tDate.formatDate("yyyy-MM-dd HH:mm:ss"));
		
		insertApplyMoneyLogs(logRow);
	}

}
