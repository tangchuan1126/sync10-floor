package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorProductLogsMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加商品日志
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addProductLogs(DBRow row)
		throws Exception
	{
		try 
		{
			return (dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_edit_logs"),row));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductLogs addProductLogs error:"+e);
		}
	}
	
	/**
	 * 搜索商品修改日志
	 * @param pc_id
	 * @param start
	 * @param end
	 * @param adid
	 * @param edit_type
	 * @param edit_reason
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchProductLogs(long pc_id,String start,String end,long adid,int edit_type,int edit_reason,PageCtrl pc)
		throws Exception
	{
		try {
			String whereProduct = "";
			if(pc_id>0)
			{
				whereProduct = " and pc_id = "+pc_id;
			}
			
			String whereEditType = "";
			if(edit_type>0)
			{
				whereEditType = " and edit_type = "+edit_type;
			}
			
			String whereEditReason = "";
			if(edit_reason>0)
			{
				whereEditReason = " and edit_reason = "+edit_reason;
			}
			if(adid>0){
				whereEditReason = " and account = "+adid;
			}
			String sql = "select * from "+ConfigBean.getStringValue("product_edit_logs")+" where post_date between '"+start+" 0:00:00' and '"+end+" 23:59:59'"
						+whereProduct+whereEditType+whereEditReason
						+" order by post_date desc";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductLogsMgrZJ searchProductLogs error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
