package com.cwc.app.floor.api.fa;

import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;
import com.cwc.db.PageCtrl;
import com.cwc.app.floor.api.fa.customer.FloorCustomerMgrIFace;

/**
 * @author Zhangchangjiang
 *
 */
public class FloorCustomerMgr implements FloorCustomerMgrIFace {

	private DBUtilAutoTran_Optm dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	/**
	 * 品牌商AddressName的唯一性效验
	 * 
	 * @param addressName
	 * @return false-->AddressName重复
	 * @throws Exception
	 */
	public boolean CheckAddressNameUnique(String addressName) throws Exception {
		boolean flag = true;
		try {
			String sql = "select * from  product_storage_catalog where  address_name = '"
					+ addressName + "'";
			if (dbUtilAutoTran.selectSingle(sql) != null)
				flag = false;
		} catch (Exception e) {
			throw new Exception("FloorCustomerMgrZcj.CheckAddressNameUnique"
					+ e);
		}
		return flag;
	}

	/**
	 * 品牌商customer_id的唯一性效验
	 * 
	 * @param dbRow
	 * @return false-->customer_id重复
	 * @throws Exception
	 */
	public boolean CheckCustomerIdUnique(String customerId) throws Exception {
		boolean flag = true;
		try {
			String sql = "select * from  customer_id where customer_id = '"
					+ customerId + "'";
			if (dbUtilAutoTran.selectSingle(sql) != null)
				flag = false;
		} catch (Exception e) {
			throw new Exception("FloorCustomerMgrZcj.CheckCustomerIdUnique" + e);
		}
		return flag;
	}

	/**
	 * 品牌商customer_name的唯一性效验
	 * 
	 * @param dbRow
	 * @return false-->customer_name重复
	 * @throws Exception
	 */
	public boolean CheckCustomerNameUnique(String customerName)
			throws Exception {
		boolean flag = true;
		try {
			String sql = "select * from  customer_id where  customer_name = '"
					+ customerName + "'";
			if (dbUtilAutoTran.selectSingle(sql) != null)
				flag = false;
		} catch (Exception e) {
			throw new Exception(
					"FloorCustomerMgrZcj.CheckCustomerNameUnique (customerName) error:"
							+ e);
		}
		return flag;
	}

	/**
	 * 品牌商管理-插入品牌商
	 * 
	 * @param 品牌商信息
	 * @return 品牌商customer_key
	 * @throws Exception
	 */
	public long insertCustomer(DBRow dbRow) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId("customer_id", dbRow);
		} catch (Exception e) {
			throw new Exception(
					"FloorCustomerMgrZcj.insertCustomer(DBRow dbRow) error :"
							+ e);
		}
	}

	/**
	 * 品牌商管理-插入产品存储类别
	 * 
	 * @param 产品存储类别信息
	 * @return ID
	 * @throws Exception
	 */
	public long insertProductStorageCatalog(DBRow dbRow) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId("product_storage_catalog",
					dbRow);
		} catch (Exception e) {
			throw new Exception(
					"FloorCustomerMgrZcj.insertProductStorageCatalog(DBRow dbRow) error :"
							+ e);
		}
	}

	/**
	 * 品牌商管理-根据 customer_key 查询品牌商基本信息
	 * 
	 * @param customer_key,
	 * @return 品牌商
	 * @throws Exception
	 */
	public DBRow getCustomerByCustomerKey(DBRow dbRow) throws Exception {
		try {
//			dbRow.add("storage_type_id", dbRow.get("storage_type_id"));
//			dbRow.add("storage_type", dbRow.get("storage_type"));
//			String sql = "select custom.*,psc.* from customer_id custom left join product_storage_catalog psc on custom.customer_key = psc.storage_type_id where psc.storage_type_id = ? and psc.storage_type= ?";
//			return dbUtilAutoTran.selectPreSingle(sql, dbRow);
	       String sql = "select custom.*,psc.* from customer_id custom "
	       		+ " left join product_storage_catalog psc on custom.customer_key = psc.storage_type_id "
	       		+ " where psc.storage_type_id = "+dbRow.get("storage_type_id")
	       		+" and psc.storage_type = "+dbRow.get("storage_type");
	       return dbUtilAutoTran.selectSingle(sql);

		} catch (Exception e) {
			throw new Exception(
					"FloorCustomerMgrZcj.getCustomerByCustomerKey(DBRow dbRow) error : 	"
							+ e);
		}
	}

	/**
	 * 品牌商管理-根据 pro_id 查询省份信息
	 * 
	 * @param proId
	 * @return 省份详情
	 * @throws Exception
	 */
	public DBRow getProvinceByProId(long proId) throws Exception {
		try {
			String sql = "select * from country_province where pro_id ="
					+ proId;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorCustomerMgrZcj.getProvinceByProId(long proId) error : 	"
							+ e);
		}
	}

	/**
	 * 品牌商管理--修改customer_id
	 * 
	 * @param 品牌商
	 * @return 是否成功
	 * @throws Exception
	 */
	public int updateCustomerID(DBRow dbRow) throws Exception {
		try {
			String sql = "where customer_key = " + dbRow.get("customer_key");
			return dbUtilAutoTran.update(sql, "customer_id", dbRow);
		} catch (Exception e) {
			throw new Exception(
					"FloorCustomerMgrZcj.updateCustomerID(DBRow dbRow) error : "
							+ e);
		}
	}

	/**
	 * 品牌商管理-修改product_storage_catalog
	 * 
	 * @param 商品存储信息
	 * @return 是否成功
	 * @throws Exception
	 */
	public int updateProductStorageCatalog(DBRow dbRow) throws Exception {
		try {
			String sql = "where storage_type_id = "
					+ dbRow.get("storage_type_id", -1L);
			return dbUtilAutoTran.update(sql, "product_storage_catalog", dbRow);
		} catch (Exception e) {
			throw new Exception(
					"FloorCustomerMgrZcj.updateProductStorageCatalog(DBRow dbRow) error : "
							+ e);
		}
	}

	/**
	 * 品牌管理--根据storage_type_id,查询所有
	 * 
	 * @param 品牌商,分页
	 * @return 品牌商[]
	 * @throws Exception
	 */
	public DBRow[] getAllCustomerList(DBRow dbRow, PageCtrl pc)
			throws Exception {
		try {
			DBRow rows[];
			
			StringBuilder sb = new StringBuilder(
					"select cp.pro_name,custom.*,psc.*, cc.c_country from  customer_id custom left join product_storage_catalog psc on custom.customer_key = psc.storage_type_id "
							
							+" and psc.storage_type = ? ");
			
			sb.append(" left join country_province cp ON psc.pro_id = cp.pro_id  "
				+ "LEFT JOIN country_code cc ON psc.native = cc.ccid "
				+ " where  1=1 ");
			
			if(dbRow.get("active",-1) == 1){
				
				sb.append(" AND ( psc.active = ? or psc.active is null )");
				
			} else if(dbRow.get("active",-1) == 0){
				
				sb.append(" AND psc.active = ? ");
			}
			
			if (!"".equals(dbRow.getString("customer_key"))) {
				sb.append("and custom.customer_key = ? ");
			}
			
			if (!"".equals(dbRow.getString("customer_id"))) {
				sb.append("and custom.customer_id = ? ");
			}
			if (!"".equals(dbRow.getString("customer_name"))) {
				sb.append("and custom.customer_name = ? ");
			}
			if (!"".equals(dbRow.getString("c_country"))) {
				sb.append("and cc.c_country = ? ");
			}
			if (!"".equals(dbRow.getString("pro_name"))) {
				sb.append("and cp.pro_name = ? ");
			}
			
//			if(dbRow.get("active", -1) > -1){
//				sb.append(" AND psc.active = ?");
//			}
			sb.append(" order by custom.customer_key desc ");

			
			rows = dbUtilAutoTran.selectPreMutliple(sb.toString(), dbRow, pc);
			
			return rows;
		} catch (Exception e) {
			throw new Exception(
					"FloorCustomerMgrZcj.getAllCustomerList(DBRow dbRow, PageCtrl pc) error : "
							+ e);
		}
	}

	/**
	 * 品牌商管理-查找国家对应的所有省份
	 * 
	 * @param ccid
	 * @return 国家对应的省份[]
	 * @throws Exception
	 */
	public DBRow[] getStorageProvinceByCcid(long ccid) throws Exception {
		try {
			String sql = "select * from "
					+ ConfigBean.getStringValue("country_province")
					+ " where nation_id=? order by p_code,pro_name asc";
			DBRow para = new DBRow();
			para.add("ccid", ccid);
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} catch (Exception e) {
			throw new Exception(
					"FloorCustomerMgrZcj.getStorageProvinceByCcid(long ccid) error:"
							+ e);
		}
	}

	/**
	 * 品牌商管里-查找所有国家
	 * 
	 * @return 国家[]
	 * @throws Exception
	 */

	public DBRow[] getAllCountryCode() throws Exception {
		try {
			String sql = (new StringBuilder()).append("SELECT *  FROM ")
					.append(ConfigBean.getStringValue("country_code"))
					.append(" c,")
					.append(ConfigBean.getStringValue("storage_country"))
					.append(" sc where c.ccid=sc.cid order by c_country asc")
					.toString();
			DBRow country[] = dbUtilAutoTran.selectMutliple(sql);
			return country;
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorCustomerMgrZcj.getAllCountryCode() error:")
					.append(e).toString());
		}
	}
	
	public DBRow[] getTitleProductByCustomer(long id) throws Exception {
		
		try {
			
			String sql = "select DISTINCT title_name from title_product tp LEFT JOIN title t on tp.tp_title_id = t.title_id where tp.tp_customer_id ="+id;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			throw new Exception((new StringBuilder()).append("FloorCustomerMgrZcj.getTitleProductByCustomer() error:").append(e).toString());
		}
	}
}
