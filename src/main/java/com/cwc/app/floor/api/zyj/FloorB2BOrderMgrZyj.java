package com.cwc.app.floor.api.zyj;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.floor.api.qll.FloorProductMgrQLL;
import com.cwc.app.key.B2BOrderCertificateKey;
import com.cwc.app.key.B2BOrderClearanceKey;
import com.cwc.app.key.B2BOrderDeclarationKey;
import com.cwc.app.key.B2BOrderKey;
import com.cwc.app.key.B2BOrderLogTypeKey;
import com.cwc.app.key.B2BOrderProductFileKey;
import com.cwc.app.key.B2BOrderTagKey;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.JqGridFilterKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.MoneyUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

@SuppressWarnings("all")
public class FloorB2BOrderMgrZyj {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	private FloorProductMgrQLL floorProductMgrQLL;
	
	/**
	 * 添加订单
	 * 
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addB2BOrder(DBRow dbrow) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("b2b_order"), dbrow);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj addB2BOrder error:" + e);
		}
	}
	
	/**
	 * 修改订单
	 * 
	 * @param b2b_order_id
	 * @param para
	 * @throws Exception
	 */
	public void modB2BOrder(long b2b_oid, DBRow para) throws Exception {
		try {
			dbUtilAutoTran.update("where b2b_oid = " + b2b_oid, ConfigBean.getStringValue("b2b_order"), para);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj modB2BOrder error:" + e);
		}
	}
	
	/**
	 * 删除订单
	 * 
	 * @param b2b_oid
	 * @throws Exception
	 */
	public void delB2BOrder(long b2b_oid) throws Exception {
		try {
			dbUtilAutoTran.delete("where b2b_oid=" + b2b_oid, ConfigBean.getStringValue("b2b_order"));
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj delB2BOrder error:" + e);
		}
	}
	
	/**
	 * 添加订单详细
	 * 
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addB2BOrderItem(DBRow dbrow) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("b2b_order_item"), dbrow);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj addB2BOrderItem error:" + e);
		}
	}
	
	/**
	 * 修改订单详细
	 * 
	 * @param b2b_order_detail_id
	 * @param para
	 * @throws Exception
	 */
	public void modB2BOrderItem(long b2b_detail_id, DBRow para) throws Exception {
		try {
			dbUtilAutoTran.update("where b2b_detail_id=" + b2b_detail_id, ConfigBean.getStringValue("b2b_order_item"),
					para);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj modB2BOrderItem error:" + e);
		}
	}
	
	/**
	 * 删除订单详细
	 * 
	 * @param b2b_detail_id
	 * @throws Exception
	 */
	public void delB2BOrderItem(long b2b_detail_id) throws Exception {
		try {
			dbUtilAutoTran.delete("where b2b_detail_id=" + b2b_detail_id, ConfigBean.getStringValue("b2b_order_item"));
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj delB2BOrderItem error:" + e);
		}
	}
	
	/**
	 * 删除订单明细(根据订单号批量删除)
	 * 
	 * @param b2b_oid
	 * @throws Exception
	 */
	public void delB2BOrderItemByB2BOrderId(long b2b_oid) throws Exception {
		try {
			dbUtilAutoTran.delete("where b2b_oid=" + b2b_oid, ConfigBean.getStringValue("b2b_order_item"));
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj delB2BOrderItemByB2BOrderId error:" + e);
		}
	}
	
	/**
	 * 根据订单ID获得订单
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailB2BOrderById(long b2b_oid) throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("b2b_order") + " where b2b_oid = ?";
			
			DBRow para = new DBRow();
			para.add("b2b_oid", b2b_oid);
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getDetailB2BOrderById error:" + e);
		}
	}
	
	/**
	 * 根据订单ID获得订单
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public DBRow getB2BOrder(long b2b_oid) throws Exception {
		try {
			String sql = "select a.*,b.carrier,"
					+ "(select group_concat(distinct i.retail_po separator ' ') from b2b_order_item i where i.b2b_oid=a.b2b_oid) as po,"
					+ "(select customer_key from customer_brand t where cb_id = a.customer_id) as customer_key,"
					+ "(select sum(i.total_weight) from b2b_order_item  i where i.b2b_oid= a.b2b_oid) total_weight,"
					+ "(select sum(i.boxes) from b2b_order_item i where i.b2b_oid= a.b2b_oid) total_boxes,"
					+ "(select sum(i.pallet_spaces) from b2b_order_item  i where i.b2b_oid= a.b2b_oid) total_pallets"
					+ " from " + ConfigBean.getStringValue("b2b_order") + " a left join carrier_scac_mcdot b on a.carriers=b.scac where b2b_oid = ?";
			
			DBRow para = new DBRow();
			para.add("b2b_oid", b2b_oid);
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getDetailB2BOrderById error:" + e);
		}
	}
	
	/**
	 * 根据文件名获得导入订单明细
	 * 
	 * @param import_name
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getImportB2BOrderItem(String import_name) throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("b2b_order_import_item")
					+ " where file_name = ? ";
			
			DBRow para = new DBRow();
			para.add("file_name", import_name);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getImportB2BOrderItem error:" + e);
		}
	}
	
	/**
	 * 获得订单体积(因Spring容器生成顺序不得不写floor层)
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public float getB2BOrderVolume(long b2b_oid) throws Exception {
		try {
			DBRow b2BOrderRow = this.getDetailB2BOrderById(b2b_oid);
			
			String colum = "b2b_count";
			if (b2BOrderRow.get("b2b_order_status", 0) == B2BOrderKey.FINISH) {
				colum = "b2b_reap_count";
			}
			
			DBRow[] rows = this.getB2BOrderItemByB2BOrderId(b2b_oid, null, null, null, null);
			
			float volume = 0;
			for (int i = 0; i < rows.length; i++) {
				volume += rows[i].get("b2b_volume", 0f) * rows[i].get(colum, 0f);
			}
			
			return MoneyUtil.round(volume, 2);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getB2BOrderVolume error:" + e);
		}
	}
	
	/**
	 * 根据订单号查询订单明细
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getB2BOrderItemByB2BOrderId(long b2b_oid, PageCtrl pc, String sidx, String sord,
			FilterBean fillterBean) throws Exception {
		try {
			StringBuffer sql = new StringBuffer("select * from  " + ConfigBean.getStringValue("b2b_order_item")
					+ " as td " + "left join " + ConfigBean.getStringValue("product")
					+ " as p on p.pc_id = td.b2b_pc_id " + "left join " + ConfigBean.getStringValue("product_code")
					+ " as pcode on pcode.pc_id = p.pc_id and pcode.code_type=" + CodeTypeKey.Main + " ");
			DBRow para = new DBRow();
			if (b2b_oid > 0) {
				sql.append("where td.b2b_oid = ?");
				para.add("b2b_oid", b2b_oid);
			}
			if (fillterBean != null) {
				sql.append(new JqGridFilterKey().filterSQL(fillterBean));
			}
			
			if (sidx == null || sord == null) {
				sql.append(" order by b2b_oid desc");
			} else {
				sql.append(" order by " + sidx + " " + sord);
			}
			
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(), para, pc));
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj getB2BOrderItemByB2BOrderId error:" + e);
		}
	}
	
	/**
	 * 添加订单日志
	 * 
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addB2BOrderLog(DBRow row) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("b2b_order_log"), row);
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj.addB2BOrderLog(row):" + e);
		}
	}
	
	/**
	 * 通过订单ID删除运费项目
	 * 
	 * @param b2b_oid
	 * @throws Exception
	 */
	public void deleteB2BOrderFreghtCostByB2BOrderId(long b2b_oid) throws Exception {
		try {
			dbUtilAutoTran.delete(" where b2b_oid = " + b2b_oid, ConfigBean.getStringValue("b2b_order_freight_cost"));
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj.deleteB2BOrderFreghtCostByB2BOrderId(b2b_oid,row):" + e);
		}
	}
	
	/**
	 * 添加运费项目
	 * 
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addB2BOrderFreghtCost(DBRow dbrow) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("b2b_order_freight_cost"), dbrow);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj addB2BOrderFreghtCost error:" + e);
		}
	}
	
	/**
	 * 更新运费项目
	 * 
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public void updateB2BOrderFreghtCost(long tfc_id, DBRow dbrow) throws Exception {
		try {
			dbUtilAutoTran.update(" where tfc_id = " + tfc_id, ConfigBean.getStringValue("b2b_order_freight_cost"),
					dbrow);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj updateB2BOrderFreghtCost error:" + e);
		}
	}
	
	/**
	 * 过滤订单（支持根据转运仓库、目的仓库、订单状态过滤）
	 * 
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterB2BOrder(long send_psid, long receive_psid, PageCtrl pc, int status, int declaration,
			int clearance, int invoice, int drawback, int day, int stock_in_set, long create_account_id)
			throws Exception {
		try {
			StringBuffer sql = new StringBuffer("select * from " + ConfigBean.getStringValue("b2b_order")
					+ " where 1=1");
			TDate tDate = new TDate();
			tDate.addDay(-day);
			if (day != 0) {
				sql.append(" and updatedate<= '" + tDate.formatDate("yyyy-MM-dd") + "' ");
			}
			
			if (send_psid != 0)// 转运仓库
			{
				sql.append(" and send_psid=" + send_psid);
			}
			
			if (receive_psid != 0) {
				sql.append(" and receive_psid=" + receive_psid);
			}
			
			if (status != 0) {
				if (status == 7)
					sql.append(" and b2b_order_status in(" + B2BOrderKey.READY + "," + B2BOrderKey.INTRANSIT + ","
							+ B2BOrderKey.APPROVEING + "," + B2BOrderKey.PACKING + ")");
				else
					sql.append(" and b2b_order_status=" + status);
			}
			if (declaration != 0) {
				sql.append(" and IFNULL(declaration,1)=" + declaration);
			}
			if (clearance != 0) {
				sql.append(" and IFNULL(clearance,1)=" + clearance);
			}
			if (invoice != 0) {
				sql.append(" and IFNULL(invoice,1)=" + invoice);
			}
			if (drawback != 0) {
				sql.append(" and IFNULL(drawback,1)=" + drawback);
			}
			if (stock_in_set != 0) {
				sql.append(" and IFNULL(stock_in_set,1) =" + stock_in_set);
			}
			if (create_account_id != 0) {
				sql.append(" and create_account_id = " + create_account_id);
			}
			sql.append(" order by etd desc");
			
			return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj.fillterB2BOrder error:" + e);
		}
	}
	
	/**
	 * 
	 * @param st
	 * @param en
	 * @param analysisType
	 * @param analysisStatus
	 * @param day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAnalysis(String st, String en, int analysisType, int analysisStatus, int day, PageCtrl pc)
			throws Exception {
		try {
			StringBuffer sql = new StringBuffer("select * from " + ConfigBean.getStringValue("b2b_order")
					+ " where 1=1");
			
			if (!st.equals("")) {
				sql.append(" and b2b_order_date >'" + st + "'");
			}
			
			if (!en.equals("")) {
				sql.append(" and b2b_order_date <='" + en + "'");
			}
			
			if (analysisType == 1) {// 报关
				if (analysisStatus == 1) {
					sql.append(" and declaration_over is null");
					sql.append(" and now() > ADDDATE(b2b_order_date,INTERVAL " + day + " DAY)");
					sql.append(" and declaration in (2,3)");
				} else {
					sql.append(" and declaration_over is not null");
					sql.append(" and declaration_over>" + day);
				}
			} else if (analysisType == 2) {
				if (analysisStatus == 1) {
					sql.append(" and clearance_over is null");
					sql.append(" and now() > ADDDATE(b2b_order_date,INTERVAL " + day + " DAY)");
					sql.append(" and clearance in (2,3)");
				} else {
					sql.append(" and clearance_over is not null");
					sql.append(" and clearance_over>" + day);
				}
			} else if (analysisType == 3) {
				if (analysisStatus == 1) {
					sql.append(" and drawback_over is null");
					sql.append(" and now() > ADDDATE(b2b_order_date,INTERVAL " + day + " DAY)");
					sql.append(" and drawback in (2,3)");
				} else {
					sql.append(" and drawback_over is not null");
					sql.append(" and drawback_over>" + day);
				}
			} else if (analysisType == 4) {
				if (analysisStatus == 1) {
					sql.append(" and invoice_over is null");
					sql.append(" and now() > ADDDATE(b2b_order_date,INTERVAL " + day + " DAY)");
					sql.append(" and invoice in (2,3)");
				} else {
					sql.append(" and invoice_over is not null");
					sql.append(" and invoice_over>" + day);
				}
			} else if (analysisType == 5) {
				if (analysisStatus == 1) {
					sql.append(" and all_over is null");
					sql.append(" and now() > ADDDATE(b2b_order_date,INTERVAL " + day + " DAY)");
				} else {
					sql.append(" and all_over is not null");
					sql.append(" and all_over>" + day);
				}
			}
			
			sql.append(" order by b2b_oid desc");
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj.getAnalysis error:" + e);
		}
	}
	
	/**
	 * 需跟进的备货中的订单
	 * 
	 * @param product_line_id
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackReadyB2BOrder(long product_line_id, int track_day, PageCtrl pc) throws Exception {
		try {
			String sql = "select * from" + "( " + "	select * from" + "	( "
					+ "  	select t.*,tl.b2b_order_date as last_track_date from b2b_order t "
					+ " 		join b2b_order_log tl on tl.b2b_oid = t.b2b_oid and tl.b2b_type = "
					+ B2BOrderLogTypeKey.Goods + " and tl.activity_id =" + B2BOrderKey.READY + " "
					+ "		left join product_line_define pld ON p.product_line_id = pld.id and pld.id = ?"
					+ "		where  t.b2b_order_status = " + B2BOrderKey.READY + " order by tl.b2b_order_date desc"
					+ "	)as ttl group by b2b_oid " + ")need_track where DATE_ADD(last_track_date,INTERVAL " + track_day
					+ " day)<= CURRENT_DATE ";
			
			DBRow para = new DBRow();
			para.add("id", product_line_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj.getNeedTrackReadyB2BOrder error:" + e);
		}
	}
	
	/**
	 * 需跟进内部标签订单产品线分组
	 * 
	 * @param product_line_id
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackTagB2BOrder(long product_line_id, int track_day, PageCtrl pc) throws Exception {
		try {
			String sql = "select * from" + "( " + "	select * from" + "	( "
					+ "  	select t.*,tl.b2b_order_date as last_track_date from b2b_order t "
					+ " 		join b2b_order_log tl on tl.b2b_oid = t.b2b_oid and tl.b2b_type = "
					+ B2BOrderLogTypeKey.Label + " and tl.activity_id =" + B2BOrderTagKey.TAG + " "
					+ "		left join product_line_define pld ON p.product_line_id = pld.id and pld.id = ?"
					+ "		where t.tag = " + B2BOrderTagKey.TAG + " order by tl.b2b_order_date desc"
					+ "	)as ttl group by b2b_oid " + ")need_track where DATE_ADD(last_track_date,INTERVAL " + track_day
					+ " day)<= CURRENT_DATE ";
			
			DBRow para = new DBRow();
			para.add("id", product_line_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj.getNeedTrackTagB2BOrder error:" + e);
		}
	}
	
	/**
	 * 需跟进第三方标签订单产品线分组
	 * 
	 * @param product_line_id
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackThirdTagB2BOrder(long product_line_id, int track_day, PageCtrl pc) throws Exception {
		try {
			String sql = "select * from" + "( " + "	select * from" + "	( "
					+ "  	select t.*,tl.b2b_order_date as last_track_date from b2b_order t "
					+ " 		join b2b_order_log tl on tl.b2b_oid = t.b2b_oid and tl.b2b_type = "
					+ B2BOrderLogTypeKey.THIRD_TAG + " and tl.activity_id =" + B2BOrderTagKey.TAG + " "
					+ "		left join product_line_define pld ON p.product_line_id = pld.id and pld.id = ?"
					+ "		where t.tag_third = " + B2BOrderTagKey.TAG + " order by tl.b2b_order_date desc"
					+ "	)as ttl group by b2b_oid " + ")need_track where DATE_ADD(last_track_date,INTERVAL " + track_day
					+ " day)<= CURRENT_DATE ";
			
			DBRow para = new DBRow();
			para.add("id", product_line_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj.getNeedTrackThirdTagB2BOrder error:" + e);
		}
	}
	
	/**
	 * 需跟进质检订单
	 * 
	 * @param product_line_id
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackQualityInspectionB2BOrder(long product_line_id, int track_day, PageCtrl pc)
			throws Exception {
		try {
			String sql = "select * from" + "( " + "	select * from" + "	( "
					+ "  	select t.*,tl.b2b_order_date as last_track_date from b2b_order t "
					+ " 		join b2b_order_log tl on tl.b2b_oid = t.b2b_oid and tl.b2b_type = "
					+ B2BOrderLogTypeKey.ProductShot + " and tl.activity_id =" + B2BOrderProductFileKey.PRODUCTFILE
					+ " " + "		left join product_line_define pld ON p.product_line_id = pld.id pld.id = ?"
					+ "		where t.product_file = " + B2BOrderProductFileKey.PRODUCTFILE
					+ " order by tl.b2b_order_date desc" + "	)as ttl group by b2b_oid "
					+ ")need_track where DATE_ADD(last_track_date,INTERVAL " + track_day + " day)<= CURRENT_DATE ";
			
			DBRow para = new DBRow();
			para.add("id", product_line_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj.getNeedTrackQualityInspectionB2BOrder error:" + e);
		}
	}
	
	/**
	 * 需跟进实物图片订单
	 * 
	 * @param product_line_id
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackProductFileB2BOrder(long product_line_id, int track_day, PageCtrl pc) throws Exception {
		try {
			String sql = "select * from" + "( " + "	select * from" + "	( "
					+ "  	select t.*,tl.b2b_order_date as last_track_date from b2b_order t "
					+ " 		join b2b_order_log tl on tl.b2b_oid = t.b2b_oid and tl.b2b_type = "
					+ B2BOrderLogTypeKey.ProductShot + " and tl.activity_id =" + B2BOrderProductFileKey.PRODUCTFILE
					+ " " + "		left join product_line_define pld ON p.product_line_id = pld.id and pld.id = ?"
					+ "		where t.product_file = " + B2BOrderProductFileKey.PRODUCTFILE
					+ " order by tl.b2b_order_date desc" + "	)as ttl group by b2b_oid "
					+ ")need_track where DATE_ADD(last_track_date,INTERVAL " + track_day + " day)<= CURRENT_DATE ";
			
			DBRow para = new DBRow();
			para.add("id", product_line_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj.getNeedTrackProductFileB2BOrder error:" + e);
		}
	}
	
	/**
	 * 需跟进备货中,装箱中的订单
	 * 
	 * @param ps_id
	 * @param track_day
	 * @param transort_status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackSendB2BOrderByPsid(long ps_id, int track_day, int transort_status, PageCtrl pc)
			throws Exception {
		try {
			String sql = "select * from " + "( " + "	select * from" + "		("
					+ "		select t.*,tl.b2b_order_date as last_track_date from b2b_order t "
					+ "		join b2b_order_log tl on tl.b2b_oid = t.b2b_oid and tl.b2b_type = " + B2BOrderLogTypeKey.Goods
					+ " and tl.activity_id =" + transort_status + " "
					+ "		where t.send_psid = ? and t.b2b_order_status = " + transort_status
					+ " order by tl.b2b_order_date desc" + "		)as ttl group by b2b_oid "
					+ ")need_track where date_add(last_track_date,INTERVAL " + track_day + " day)<= CURRENT_DATE ";
			DBRow para = new DBRow();
			para.add("send_psid", ps_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj getNeedTrackSendB2BOrderByPsid error:" + e);
		}
	}
	
	/**
	 * 需跟进内部标签的订单
	 * 
	 * @param send_psid
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackTagB2BOrderByPs(long send_psid, int track_day, PageCtrl pc) throws Exception {
		try {
			String sql = "select * from" + "( " + "	select * from" + "	( "
					+ "  	select t.*,tl.b2b_order_date as last_track_date from b2b_order t "
					+ " 		join b2b_order_log tl on tl.b2b_oid = t.b2b_oid and tl.b2b_type = "
					+ B2BOrderLogTypeKey.Label + " and tl.activity_id =" + B2BOrderTagKey.TAG + " "
					+ "		join product_storage_catalog psc on t.send_psid = psc.id "
					+ "		where t.send_psid = ? and t.tag = " + B2BOrderTagKey.TAG + " order by tl.b2b_order_date desc"
					+ "	)as ttl group by b2b_oid " + ")need_track where DATE_ADD(last_track_date,INTERVAL " + track_day
					+ " day)<= CURRENT_DATE ";
			
			DBRow para = new DBRow();
			para.add("send_psid", send_psid);
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj getNeedTrackTagB2BOrderByPs error:" + e);
		}
	}
	
	/**
	 * 需跟进的仓库收货订单
	 * 
	 * @param ps_id
	 * @param track_day
	 * @param transort_status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackReceiveB2BOrderByPsid(long ps_id, int track_day, int transort_status, PageCtrl pc)
			throws Exception {
		try {
			String sql = "select * from " + "( " + "	select * from" + "		("
					+ "		select t.*,tl.b2b_order_date as last_track_date from b2b_order t "
					+ "		join b2b_order_log tl on tl.b2b_oid = t.b2b_oid and tl.b2b_type = " + B2BOrderLogTypeKey.Goods
					+ " and tl.activity_id =" + transort_status + " "
					+ "		where t.receive_psid = ? and t.b2b_order_status = " + transort_status
					+ " order by tl.b2b_order_date desc" + "		)as ttl group by b2b_oid "
					+ ")need_track where date_add(last_track_date,INTERVAL " + track_day + " day)<= CURRENT_DATE ";
			DBRow para = new DBRow();
			para.add("recevie_psid", ps_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj getNeedTrackReceiveB2BOrderByPsid error:" + e);
		}
	}
	
	/**
	 * 需跟进单证的订单
	 * 
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackCertificateB2BOrder(int track_day, PageCtrl pc) throws Exception {
		try {
			String sql = "select * from " + "( " + "	select * from" + "		("
					+ "		select t.*,tl.b2b_order_date as last_track_date from b2b_order t "
					+ "		join b2b_order_log tl on tl.b2b_oid = t.b2b_oid and tl.b2b_type = "
					+ B2BOrderLogTypeKey.Document + " and tl.activity_id =" + B2BOrderCertificateKey.CERTIFICATE + " "
					+ "		where t.certificate = " + B2BOrderCertificateKey.CERTIFICATE
					+ " order by tl.b2b_order_date desc" + "		)as ttl group by b2b_oid "
					+ ")need_track where date_add(last_track_date,INTERVAL " + track_day + " day)<= CURRENT_DATE ";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getNeedTrackCertificateB2BOrder error:" + e);
		}
	}
	
	/**
	 * 需跟进清关的订单
	 * 
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackClearanceB2BOrder(int track_day, PageCtrl pc) throws Exception {
		try {
			String sql = "select * from " + "( " + "	select * from" + "		("
					+ "		select t.*,tl.b2b_order_date as last_track_date from b2b_order t "
					+ "		join b2b_order_log tl on tl.b2b_oid = t.b2b_oid and tl.b2b_type = "
					+ B2BOrderLogTypeKey.ImportCustoms + " and tl.activity_id =" + B2BOrderClearanceKey.CLEARANCEING
					+ " " + "		where t.clearance = " + B2BOrderClearanceKey.CLEARANCEING
					+ " order by tl.b2b_order_date desc" + "		)as ttl group by b2b_oid "
					+ ")need_track where date_add(last_track_date,INTERVAL " + track_day + " day)<= CURRENT_DATE ";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getNeedTrackClearanceB2BOrder error:" + e);
		}
	}
	
	/**
	 * 需跟进报关的订单
	 * 
	 * @param track_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNeedTrackDeclarationB2BOrder(int track_day, PageCtrl pc) throws Exception {
		try {
			String sql = "select * from " + "( " + "	select * from" + "		("
					+ "		select t.*,tl.b2b_order_date as last_track_date from b2b_order t "
					+ "		join b2b_order_log tl on tl.b2b_oid = t.b2b_oid and tl.b2b_type = "
					+ B2BOrderLogTypeKey.ExportCustoms + " and tl.activity_id =" + B2BOrderDeclarationKey.DELARATING
					+ " " + "		where t.declaration = " + B2BOrderDeclarationKey.DELARATING
					+ " order by tl.b2b_order_date desc" + "		)as ttl group by b2b_oid "
					+ ")need_track where date_add(last_track_date,INTERVAL " + track_day + " day)<= CURRENT_DATE ";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getNeedTrackDeclarationB2BOrder");
		}
	}
	
	/**
	 * 获得所有订单
	 * 
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllB2BOrder(PageCtrl pc) throws Exception {
		try {
			String sql = "select do.*,psc.title as title from " + ConfigBean.getStringValue("b2b_order") + " as do,"
					+ ConfigBean.getStringValue("product_storage_catalog")
					+ " as psc where psc.id = do.recevie_psid order by b2b_oid desc";
			return (dbUtilAutoTran.selectMutliple(sql, pc));
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getAllB2BOrder");
		}
	}
	
	/**
	 * 根据当前登录者的Id，获取其上次创建的订单的ID
	 * 
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow getB2BOrderLastTimeCreateByAdid(long adid) throws Exception {
		try {
			String sql = "select b2b_oid from b2b_order where create_account_id = " + adid
					+ " ORDER BY b2b_order_date desc LIMIT 0,1";
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj.getB2BOrderLastTimeCreateByAdid():" + e);
		}
	}
	
	/**
	 * 保存导入订单明细
	 * 
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addB2BOrderImportDetail(DBRow dbrow) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("b2b_order_import_item"), dbrow);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj addB2BOrderImportDetail error:" + e);
		}
	}
	
	/**
	 * 上传订单明细的错误信息
	 * 
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] errorImportB2BOrderDetail(String fileName) throws Exception {
		try {
			String sql = "select line_number as number,import_p_name as p_name,b2b_pc_id,b2b_delivery_count,b2b_backup_count,b2b_detail_serial_number,b2b_detail_box,lot_number,'ProductNotExist' as error_type from "
					+ ConfigBean.getStringValue("b2b_order_import_item")
					+ " where b2b_pc_id = 0 and file_name ='"
					+ fileName
					+ "' "
					+ "union "
					+ "select line_number as number,import_p_name as p_name,b2b_pc_id,b2b_delivery_count,b2b_backup_count,b2b_detail_serial_number,b2b_detail_box,lot_number,'CountNoOne' as error_type from "
					+ ConfigBean.getStringValue("b2b_order_import_item")
					+ " where b2b_detail_serial_number is not null and b2b_count !=1 and file_name ='"
					+ fileName
					+ "' "
					+ "union "
					+ "select line_number as number,import_p_name as p_name,b2b_pc_id,b2b_delivery_count,b2b_backup_count,b2b_detail_serial_number,b2b_detail_box,lot_number,'SerialNumberRepeat' as error_type from b2b_order_import_item tid "
					+ "join "
					+ "("
					+ "	select * from ("
					+ "		select b2b_detail_serial_number as serial_number,count(b2b_import_detail_id)as times from b2b_order_import_item where b2b_detail_serial_number is not NULL and file_name ='"
					+ fileName
					+ "' GROUP BY b2b_detail_serial_number "
					+ "	) as importSerialNumberRepeat where times <>1 "
					+ ")as isnr on tid.b2b_detail_serial_number = isnr.serial_number";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj errorImportB2BOrderDetail error:" + e);
		}
	}
	
	/**
	 * 上传订单明细的全部信息
	 * 
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public DBRow[] allImportB2BOrderDetail(String fileName) throws Exception {
		try {
			String sql = "select line_number as number,import_p_name as p_name,b2b_pc_id,b2b_delivery_count,b2b_backup_count,b2b_detail_serial_number,b2b_detail_box,lot_number,'ProductNotExist' as all_type from "
					+ ConfigBean.getStringValue("b2b_order_import_item")
					+ " where b2b_pc_id = 0 and file_name ='"
					+ fileName
					+ "' "
					+ " UNION SELECT line_number AS number, import_p_name AS p_name, b2b_pc_id,b2b_delivery_count,b2b_backup_count,b2b_detail_serial_number,b2b_detail_box,lot_number ,'Correct' as all_type"
					+ " FROM b2b_order_import_item WHERE b2b_pc_id != 0 AND ( b2b_detail_serial_number IS  NULL or (b2b_detail_serial_number IS NOT  NULL and b2b_count = 1) )and file_name = '"
					+ fileName + "' ";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj allImportB2BOrderDetail error:" + e);
		}
	}
	
	/**
	 * 获得订单的总重量
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public float getB2BOrderWeight(long b2b_oid) throws Exception {
		try {
			DBRow b2BOrder = this.getDetailB2BOrderById(b2b_oid);
			
			String colum = "b2b_count";
			if (b2BOrder.get("b2b_order_status", 0) == B2BOrderKey.FINISH) {
				colum = "b2b_reap_count";
			}
			
			DBRow[] b2BOrderItem = this.getB2BOrderItemByB2BOrderId(b2b_oid, null, null, null, null);
			
			float weight = 0;
			for (int i = 0; i < b2BOrderItem.length; i++) {
				weight += b2BOrderItem[i].get("b2b_weight", 0f) * b2BOrderItem[i].get(colum, 0f);
			}
			
			return MoneyUtil.round(weight, 2);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getB2BOrderWeight error:" + e);
		}
	}
	
	/**
	 * 获得所有订单出库金额
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public double getB2BOrderSendPrice(long b2b_oid) throws Exception {
		try {
			DBRow[] b2BOrderItem = this.getB2BOrderItemByB2BOrderId(b2b_oid, null, null, null, null);
			
			double sendPrice = 0;
			for (int i = 0; i < b2BOrderItem.length; i++) {
				sendPrice += b2BOrderItem[i].get("b2b_send_price", 0f) * b2BOrderItem[i].get("b2b_delivery_count", 0f);
			}
			
			return MoneyUtil.round(sendPrice, 2);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getB2BOrderSendPrice error:" + e);
		}
	}
	
	/**
	 * 通过订单ID和结果数量查询日志
	 * 
	 * @param b2b_oid
	 * @param number
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getB2BOrderLogs(long b2b_oid, int number) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select * from ").append("b2b_order_log ").append(" where b2b_oid =").append(b2b_oid)
					.append(" order by logs_id desc limit 0,4;");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj.getB2BOrderLogs(b2b_oid ,number):" + e);
		}
	}
	
	/**
	 * 获得订单总运费
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public DBRow getSumB2BOrderFreightCost(long b2b_oid) throws Exception {
		try {
			String sql = "select sum(tfc_unit_price*tfc_unit_count*tfc_exchange_rate) AS sum_price from b2b_order_freight_cost where b2b_oid = "
					+ b2b_oid;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj.getSumB2BOrderFreightCost(b2b_oid):" + e);
		}
	}
	
	/**
	 * 通过订单ID查询运费项目
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findB2BOrderFreightCostByB2BOrderId(long b2b_oid) throws Exception {
		try {
			return dbUtilAutoTran.selectMutliple("select * from b2b_order_freight_cost where b2b_oid = " + b2b_oid);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj.findB2BOrderFreightCostByB2BOrderId(long b2b_oid):" + e);
		}
	}
	
	/**
	 * 通过订单ID和日志类型查询日志
	 * 
	 * @param b2b_oid
	 * @param types
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getB2BOrderLogsByB2BOrderIdAndType(long b2b_oid, int[] types) throws Exception {
		try {
			String sql = "select * from b2b_order_log";
			sql += " where b2b_oid = " + b2b_oid;
			if (null != types && types.length > 0) {
				if (0 != types[0]) {
					String sqlWhere = " and ( b2b_type = " + types[0];
					for (int i = 1; i < types.length; i++) {
						sqlWhere += " or b2b_type = " + types[i];
					}
					sqlWhere += ")";
					sql += sqlWhere;
				}
			}
			sql += " order by logs_id";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj.getB2BOrderLogsByB2BOrderIdAndType(long b2b_oid, int[] types):"
					+ e);
		}
	}
	
	/**
	 * 通过订单明细ID查询订单明细信息
	 * 
	 * @param b2b_detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findB2BOrderItemByItemId(long b2b_detail_id) throws Exception {
		try {
			String sql = "select * from b2b_order_item where b2b_detail_id = " + b2b_detail_id;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj.findB2BOrderItemByItemId(long b2b_detail_id):" + e);
		}
	}
	
	/**
	 * 根据检查单据内是否已包含此序列号
	 * 
	 * @param b2b_oid
	 * @param serial_number
	 * @return
	 * @throws Exception
	 */
	public DBRow getB2BOrderItemBySN(long b2b_oid, String serial_number) throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("b2b_order_item")
					+ " where b2b_oid = ? and b2b_product_serial_number = ? ";
			
			DBRow para = new DBRow();
			para.add("b2b_oid", b2b_oid);
			para.add("b2b_product_serial_number", serial_number);
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getB2BOrderItemBySN error:" + e);
		}
	}
	
	/**
	 * 通过订单ID,商品id,序列号查询订单明细
	 * 
	 * @param b2b_oid
	 * @param product_id
	 * @param serial_number
	 * @return
	 * @throws Exception
	 */
	public DBRow getB2BOrderItemByB2BOidPcIdSerial(long b2b_oid, long product_id, String serial_number)
			throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("b2b_order_item")
					+ " where b2b_oid = ? and b2b_pc_id = ? and b2b_product_serial_number = ? ";
			
			DBRow para = new DBRow();
			para.add("b2b_oid", b2b_oid);
			para.add("b2b_pc_id", product_id);
			para.add("b2b_product_serial_number", serial_number);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getB2BOrderItemByB2BOidPcIdSerial error:" + e);
		}
	}
	
	/**
	 * 通过订单ID，pc查询运费项目
	 * 
	 * @param b2b_oid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getB2BOrderFreightCostByB2BOrderId(long b2b_oid, PageCtrl pc) throws Exception {
		try {
			String sql = "select * from b2b_order_freight_cost where b2b_oid = " + b2b_oid;
			if (null == pc)
				return dbUtilAutoTran.selectMutliple(sql);
			else
				return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getB2BOrderFreightCostByB2BOrderId(long b2b_oid) error:" + e);
		}
	}
	
	/**
	 * 通过订单ID，子流程KEY值，子流程状态查询日志
	 * 
	 * @param b2b_oid
	 * @param b2b_type
	 * @param activity_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getFirstB2BOrderIngLogTime(long b2b_oid, int b2b_type, int activity_id) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select * from b2b_order_log").append(" where b2b_oid=").append(b2b_oid)
					.append(" and b2b_type=").append(b2b_type).append(" and activity_id=").append(activity_id)
					.append(" order by logs_id limit 0,1;");
			return dbUtilAutoTran.selectSingle(sql.toString());
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj.getFirstB2BOrderIngLogTime(b2b_oid ,b2b_type ,activity_id ):" + e);
		}
	}
	
	/**
	 * 通过订单ID，删除订单的日志
	 * 
	 * @param b2b_oid
	 * @throws Exception
	 */
	public void deleteB2BOrderLogsByB2BOrderId(long b2b_oid) throws Exception {
		try {
			String sql = " where b2b_oid = " + b2b_oid;
			dbUtilAutoTran.delete(sql, ConfigBean.getStringValue("b2b_order_log"));
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj.deleteB2BOrderLogsByB2BOrderId(long b2b_oid)" + e);
		}
	}
	
	/**
	 * 根据 商品名字检索
	 * 
	 * @param b2b_orderId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getB2BOrderProductByName(long b2b_oid, String name) throws Exception {
		try {
			String sql = "select * from b2b_order_item where b2b_oid=" + b2b_oid;
			if (!StrUtil.isBlank(name)) {
				sql += " and b2b_p_name  like '%" + name + "%' ";
			}
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getB2BOrderProductByName error:" + e);
		}
	}
	
	public DBRow[] fillterB2BOrderItem(long b2b_oid, String name,String title) throws Exception {
		try {
			String sql = "select * from b2b_order_item where b2b_oid=" + b2b_oid;
			if (!StrUtil.isBlank(name)) {
				sql += " and b2b_p_name  = '" + name + "' ";
			}
			if(!StrUtil.isBlank(title))
				sql += " and title  = '" + title + "' ";
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj fillterB2BOrderItem error:" + e);
		}
	}
	
	/**
	 * 通过订单ID,商品id,序列号查询订单明细
	 * 
	 * @param b2b_oid
	 * @param product_id
	 * @param serial_number
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getB2BOrderItemByB2BOidPcId(long b2b_oid, long product_id) throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("b2b_order_item") + " where b2b_oid = " + b2b_oid
					+ " and b2b_pc_id = " + product_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj getB2BOrderItemByB2BOidPcIdSerial error:" + e);
		}
	}
	
	/**
	 * 获取谁在某国或地区出多少货
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getB2BOrderOrProductCountGroupCountry(long product_line, long product_catalog, String pc_name,
			long title_id, String start_time, String end_time) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select sum(a.b2b_count) product_count, a.deliver_ccid from (");
			sql.append(" select boi.b2b_count, b2o.deliver_ccid from b2b_order b2o ");
			sql.append(" join b2b_order_item boi on boi.b2b_oid = b2o.b2b_oid");
			sql.append(" join product p on boi.b2b_pc_id = p.pc_id");
			sql.append(" join product_catalog pc on pc.id = p.catalog_id");
			sql.append(" join pc_child_list pcl ON pcl.pc_id = p.catalog_id");
			sql.append(" where 1=1");
			if (!StrUtil.isBlank(start_time) && !StrUtil.isBlank(end_time)) {
				sql.append(" and (b2o.b2b_order_date > '" + start_time + " 0:00:00' and b2o.b2b_order_date < '"
						+ end_time + " 23:59:59')");
			} else if (!StrUtil.isBlank(start_time)) {
				sql.append(" and b2o.b2b_order_date > '" + start_time + " 0:00:00'");
			} else if (!StrUtil.isBlank(end_time)) {
				sql.append(" and b2o.b2b_order_date < '" + end_time + " 23:59:59'");
			}
			if (product_catalog > 0) {
				sql.append(" and pcl.search_rootid = " + product_catalog);
			}
			if (product_line > 0) {
				sql.append(" and pc.product_line_id = " + product_line);
			}
			sql.append(" and p.p_name like '%" + pc_name + "%'");
			if (title_id > 0) {
				sql.append(" and b2o.title_id = " + title_id);
			}
			sql.append(" group by boi.b2b_detail_id) a");
			sql.append(" group by a.deliver_ccid");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj.getB2BOrderOrProductCountGroupCountry error:" + e);
		}
	}
	
	/**
	 * 获取谁在某国或地区出多少货
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getB2BOrderOrProductCountGroupProvince(long product_line, long product_catalog, String pc_name,
			long title_id, long ccid, String start_time, String end_time) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select sum(a.b2b_count) product_count, a.deliver_pro_id from(");
			sql.append(" select boi.b2b_count, b2o.deliver_pro_id from b2b_order b2o ");
			sql.append(" join b2b_order_item boi on boi.b2b_oid = b2o.b2b_oid");
			sql.append(" join product p on boi.b2b_pc_id = p.pc_id");
			sql.append(" join product_catalog pc on pc.id = p.catalog_id");
			sql.append(" join pc_child_list pcl ON pcl.pc_id = p.catalog_id");
			sql.append(" where 1=1");
			if (!StrUtil.isBlank(start_time) && !StrUtil.isBlank(end_time)) {
				sql.append(" and (b2o.b2b_order_date > '" + start_time + " 0:00:00' and b2o.b2b_order_date < '"
						+ end_time + " 23:59:59')");
			} else if (!StrUtil.isBlank(start_time)) {
				sql.append(" and b2o.b2b_order_date > '" + start_time + " 0:00:00'");
			} else if (!StrUtil.isBlank(end_time)) {
				sql.append(" and b2o.b2b_order_date < '" + end_time + " 23:59:59'");
			}
			if (product_catalog > 0) {
				sql.append(" and pcl.search_rootid = " + product_catalog);
			}
			if (product_line > 0) {
				sql.append(" and pc.product_line_id = " + product_line);
			}
			if (!StrUtil.isBlank(pc_name)) {
				sql.append(" and p.p_name like '%" + pc_name + "%'");
			}
			if (title_id > 0) {
				sql.append(" and b2o.title_id = " + title_id);
			}
			if (ccid > 0) {
				sql.append(" and b2o.deliver_ccid = " + ccid);
			}
			sql.append(" group by boi.b2b_detail_id) a");
			sql.append(" group by a.deliver_pro_id");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj.getB2BOrderOrProductCountGroupProvince error:" + e);
		}
	}
	
	public DBRow[] getB2BOrderOrProductCountAnalyze(long product_line, long product_catalog, String pc_name,
			long title_id, long ccid, String start_time, String end_time, long area_id, long pro_id) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			DBRow[] rows = floorProductMgrQLL.getDate(start_time, end_time);
			sql.append(" select a.deliver_pro_id,");
			for (int i = 0; i < rows.length; i++) {
				sql.append(" SUM(CASE a.b2b_order_date WHEN '" + rows[i].getString("date")
						+ "' THEN a.b2b_count ELSE 0 END) date_" + i);
				if (i != (rows.length - 1)) {
					sql.append(",");
				}
			}
			sql.append(" from (select boi.b2b_count, b2o.deliver_pro_id, LEFT(b2o.b2b_order_date, 10) b2b_order_date from b2b_order b2o ");
			sql.append(" join b2b_order_item boi on boi.b2b_oid = b2o.b2b_oid");
			sql.append(" join product p on boi.b2b_pc_id = p.pc_id");
			sql.append(" join product_catalog pc on pc.id = p.catalog_id");
			sql.append(" join pc_child_list pcl ON pcl.pc_id = p.catalog_id");
			sql.append("join country_area_mapping as cam on cam.ccid = ps.country_id " + " and ps.country_id = " + ccid
					+ " ");
			sql.append("join country_area as ca on ca.ca_id = cam.ca_id " + " and ca.ca_id = " + area_id + " ");
			sql.append("join country_code as cc on cc.ccid = cam.ccid ");
			sql.append("left join country_province as cp on cp.pro_id = ps.province_id " + " and ps.province_id = "
					+ pro_id + " ");// 左联，省份有可能为others-1
			sql.append("left join product_line_define as pld on pld.id = pc.product_line_id ");// 左联有可能商品未定义产品线
			sql.append(" where 1=1");
			if (!StrUtil.isBlank(start_time) && !StrUtil.isBlank(end_time)) {
				sql.append(" and (b2o.b2b_order_date > '" + start_time + " 0:00:00' and b2o.b2b_order_date < '"
						+ end_time + " 23:59:59')");
			} else if (!StrUtil.isBlank(start_time)) {
				sql.append(" and b2o.b2b_order_date > '" + start_time + " 0:00:00'");
			} else if (!StrUtil.isBlank(end_time)) {
				sql.append(" and b2o.b2b_order_date < '" + end_time + " 23:59:59'");
			}
			if (product_catalog > 0) {
				sql.append(" and pcl.search_rootid = " + product_catalog);
			}
			if (product_line > 0) {
				sql.append(" and pc.product_line_id = " + product_line);
			}
			if (!StrUtil.isBlank(pc_name)) {
				sql.append(" and p.p_name like '%" + pc_name + "%'");
			}
			if (title_id > 0) {
				sql.append(" and b2o.title_id = " + title_id);
			}
			if (ccid > 0) {
				sql.append(" and b2o.deliver_ccid = " + ccid);
			}
			sql.append(" group by boi.b2b_detail_id)a;");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception("floorB2BOrderMgrZyj.getB2BOrderOrProductCountGroupCountryForDate error:" + e);
		}
	}
	
	public DBRow[] getB2BOrderOrProductCountAnalyze(String start_date, String end_date, String catalog_id,
			String pro_line_id, String pc_name, long ca_id, long ccid, long pro_id, long cid, int type,
			String title_id, PageCtrl pc) throws Exception {
		// type 1代表求和计算，group by以选中的地域层级 2代表分组求和group by 选中级别的下一级别
		Long[] productLineRows = parseStringToLongArray(pro_line_id);
		Long[] productCatalogRows = parseStringToLongArray(catalog_id);
		Long[] titleRows = parseStringToLongArray(title_id);
		
		String formGroupBy = "";// 分日期统计group by条件
		String whereProductLine = "";
		if (productLineRows.length > 0) {
			whereProductLine += " and (pc.product_line_id = " + productLineRows[0] + " ";
			for (int i = 1; i < productLineRows.length; i++) {
				whereProductLine += " or pc.product_line_id = " + productLineRows[i] + " ";
			}
			whereProductLine += ") ";
		}
		
		String whereCatalog = "";
		if (productCatalogRows.length > 0) {
			whereCatalog += " join pc_child_list as pcl on pcl.pc_id = p.catalog_id ";
			whereCatalog += " and (pcl.search_rootid = " + productCatalogRows[0] + " ";
			for (int i = 1; i < productCatalogRows.length; i++) {
				whereCatalog += " or pcl.search_rootid = " + productCatalogRows[i] + " ";
			}
			whereCatalog += ") ";
			
		}
		
		String whereStorage = "";
		if (cid > 0) {
			whereStorage = " and psc.id=" + cid + " ";
		}
		
		String whereProduct = "";
		if (!StrUtil.isBlank(pc_name)) {
			whereProduct = " and p.p_name = '" + pc_name.toUpperCase() + "' ";
		}
		
		String whereArea = "";
		if (ca_id > 0) {
			whereArea = " and ca.ca_id = " + ca_id + " ";
		} else {
			if (type == 2)// 求和时销售区域已经自动求和
			{
				formGroupBy = ",ca_id ";
			}
		}
		
		String whereCountry = "";
		if (ccid > 0) {
			whereCountry = " and b2o.deliver_ccid = " + ccid + " ";
		} else {
			if (type == 2 && ca_id > 0) {
				formGroupBy = ",nation_id ";
			}
		}
		
		String whereProvince = "";
		if (pro_id > 0) {
			whereProvince = " and b2o.deliver_pro_id = " + pro_id + " ";
		} else {
			if (type == 2 && ccid > 0) {
				formGroupBy = ",deliver_pro_id ";
			}
		}
		
		String whereTitle = "";
		
		if (titleRows.length > 0) {
			whereTitle += " and (b2o.title_id =  " + titleRows[0] + " ";
			for (int i = 1; i < titleRows.length; i++) {
				whereTitle += " or b2o.title_id = " + titleRows[i] + " ";
			}
			whereTitle += ") ";
		}
		
		DBRow[] dateStr = floorProductMgrQLL.getDate(start_date, end_date);
		
		String sql = " SELECT product_id,p_name,pl_name,title,storage_title,area_name,c_country,pro_name, ca_id, nation_id, deliver_pro_id,  sum(b2b_count) sum_quantity,";
		
		for (int i = 0; i < dateStr.length; i++) {
			sql += "  SUM(CASE delivery_date  WHEN '" + dateStr[i].getString("date")
					+ "' THEN b2b_count ELSE 0  END)  date_" + i;
			if (i != (dateStr.length - 1)) {
				sql += " , ";
			}
		}
		
		sql += " FROM (";
		sql += "select p.pc_id as product_id,p.p_name,pld.`name` as pl_name,pc.title,"
				+ "psc.title as storage_title,ca.ca_id,ca.`name` as area_name,b2o.deliver_ccid as nation_id,"
				+ "cc.c_country,b2o.deliver_pro_id,cp.pro_name,left(b2o.b2b_order_date,10) as delivery_date, boi.b2b_count "
				+ "from b2b_order as b2o " + "join b2b_order_item boi on boi.b2b_oid = b2o.b2b_oid "
				+ "join product as p on boi.b2b_pc_id = p.pc_id "
				+ whereProduct
				+ "join product_catalog as pc on pc.id = p.catalog_id "
				+ whereProductLine
				+ whereCatalog
				+ "join country_area_mapping as cam on cam.ccid = b2o.deliver_ccid "
				+ whereCountry
				+ "join country_area as ca on ca.ca_id = cam.ca_id "
				+ whereArea
				+ "join country_code as cc on cc.ccid = cam.ccid "
				+ "left join country_province as cp on cp.pro_id = b2o.deliver_pro_id "// 左联，省份有可能为others-1
				+ "left join product_line_define as pld on pld.id = pc.product_line_id "// 左联有可能商品未定义产品线
				+ "join product_storage_catalog as psc on psc.id = b2o.receive_psid "
				+ whereStorage
				+ "where 1=1 "
				+ whereTitle
				+ whereProvince
				+ "and b2o.b2b_order_date BETWEEN '"
				+ start_date
				+ " 00:00:00' AND '"
				+ end_date + " 23:59:59' " + " group by boi.b2b_detail_id  ";
		sql += " ) as basedata GROUP BY product_id " + formGroupBy;
		sql += " ORDER BY pl_name,title,p_name ";
		
		if (null == pc) {
			return dbUtilAutoTran.selectMutliple(sql);
		}
		return (dbUtilAutoTran.selectMutliple(sql, pc));
	}
	
	public DBRow[] getDate(String start_date, String end_date) throws Exception {
		return floorProductMgrQLL.getDate(start_date, end_date);
	}
	
	private Long[] parseStringToLongArray(String str) {
		List<Long> list = new ArrayList<Long>();
		if (!StrUtil.isBlank(str)) {
			String[] strArr = str.split(",");
			if (null != strArr && strArr.length > 0) {
				for (int i = 0; i < strArr.length; i++) {
					if (!StrUtil.isBlank(strArr[i]) && !"0".equals(strArr[i])) {
						list.add(Long.valueOf(strArr[i]));
					}
				}
			}
		}
		return list.toArray(new Long[0]);
	}
	
	/**
	 * 根据订单id获取运单信息
	 * 
	 * @param b2b_oid
	 * @return <b>Date:</b>2014-8-15下午3:36:27<br>
	 * @author: cuicong
	 * @throws SQLException
	 */
	public DBRow[] getTransportByOderId(Long b2b_oid) throws Exception {
		
		try {
			String select = "select DISTINCT c.* from b2b_order a,b2b_order_item b,transport c,transport_detail d ";
			String where = "Where 	a.b2b_oid = b.b2b_oid AND b.b2b_detail_id=d.b2b_detail_id AND d.transport_id = c.transport_id and a.b2b_oid="
					+ b2b_oid;
			String sql = select + where;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj getTransportByOderId error:" + e);
		}
	}
	
	/**
	 * @author zhanjie
	 * @param b2b_oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWaitAllocateB2bOrderItem(long b2b_oid) throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("b2b_order_item")
					+ " where b2b_oid = ? and b2b_wait_count>0";
			
			DBRow para = new DBRow();
			para.add("b2b_oid", b2b_oid);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj getWaitAllocateB2bOrderItem error:" + e);
		}
	}
	
	/**
	 * 
	 * 根据b2b_oid 获取明细列表
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-24下午5:49:39<br>
	 * @author: cuicong
	 */
	public DBRow[] getB2BOrderItems(long b2b_oid) throws Exception {
		try {
			// String sql = "select * from " + ConfigBean.getStringValue("b2b_order_item") + " where b2b_oid = ? ";
			// 加入商品 upc信息 wangcr 2015.1.29
			String sql = "select td.*, pcode.p_code, p.length, p.width, p.heigth, p.freight_class, p.nmfc_code,a.lp_name, td.retail_po, td.so from "
					+ ConfigBean.getStringValue("b2b_order_item") + " as td left join "
					+ ConfigBean.getStringValue("product") + " as p on p.pc_id = td.b2b_pc_id left join "
					+ ConfigBean.getStringValue("product_code")
					+ " as pcode on pcode.pc_id = p.pc_id and pcode.code_type= " + CodeTypeKey.UPC
					+ " left join license_plate_type a on a.pc_id=p.pc_id and a.lpt_id= td.clp_type_id where td.b2b_oid = ? order by td.b2b_detail_id";
			DBRow para = new DBRow();
			para.add("b2b_oid", b2b_oid);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj getB2BOrderItems error:" + e);
		}
	}
	
	/**
	 * 
	 * 根据b2b_detail_id 获取明细
	 * 
	 * @param b2b_oid
	 * @return
	 * @throws Exception <b>Date:</b>2014-12-24下午5:49:39<br>
	 * @author: cuicong
	 */
	public DBRow getB2BOrderItem(long b2b_detail_id) throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("b2b_order_item") + " where b2b_detail_id = ? ";
			DBRow para = new DBRow();
			para.add("b2b_detail_id", b2b_detail_id);
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj getB2BOrderItem error:" + e);
		}
	}
	
	/**
	 * @author zhanjie
	 * @param b2b_order_detail_id
	 * @param updatePara
	 * @throws Exception
	 */
	public void updateB2bOrderDetail(long b2b_order_detail_id, DBRow updatePara) throws Exception {
		try {
			dbUtilAutoTran.update("where b2b_detail_id = " + b2b_order_detail_id,
					ConfigBean.getStringValue("b2b_order_item"), updatePara);
		} catch (Exception e) {
			throw new Exception("FloorB2BOrderMgrZyj updateB2bOrderDetail error:" + e);
		}
	}
	
	/**
	 * 获取dn号
	 * 
	 * @param cid
	 * @param carriers
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getB2bOrderDn(String cid, String carriers, String payType) throws Exception {
		try {
			String sql = "select customer_dn as dn from b2b_order where customer_id='" + cid + "' and carriers='"
					+ carriers + "' and freight_term='" + payType + "'";
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("getB2bOrderDn(String cid,String carriers,String payType) error:" + e);
		}
	}
	
	/**
	 * 根据dn查找出order及item明细
	 * 
	 * @param dn
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOrderAndDetail(String dns, String customer_id) throws Exception {
		try {
			StringBuilder sql = new StringBuilder()
					.append("select a.customer_order_date as a0,(select brand_name from customer_brand where cb_id='"+customer_id+"') as a1,a.company_id as a2,a.customer_dn as a3,"
							+ "a.order_number as a4,a.retail_po as a5,b.title as a6,")
					.append("b.item_id as a7,b.material_number as a8,b.b2b_delivery_count as a9,b.total_weight as a10,b.boxes as a11,b.pallet_spaces as a12,a.order_type as a13"
							+ ",a.ship_to_party_name as a14,a.b2b_order_address as a15,a.deliver_city as a16,")
					.append("a.address_state_deliver as a17,a.deliver_zip_code as a18,c.c_country as a19,a.ship_not_before as a20,a.ship_not_later as a21,a.requested_date as a22"
							+ ",a.mabd as a23,a.carriers as a24,a.freight_term as a25,a.bill_to_id as a26,")
					.append("a.load_no as a27,a.freight_carrier as a28,b.note as a29,a.delivery_appointment as a30,a.pickup_appointment as a31,")
					.append("a.deliveryed_date as a32,a.b2b_order_status as a33,a.bol_tracking as a34,a.send_psid ")
					.append(" from b2b_order a,b2b_order_item b,country_code c where c.ccid=a.deliver_ccid and a.b2b_oid=b.b2b_oid and a.customer_dn in (")
					.append(dns).append(") and b2b_order_status<>6 and customer_id='").append(customer_id).append("' order by a.load_no");
			DBRow[] rows = this.dbUtilAutoTran.selectMutliple(sql.toString());
			
			final Map<String,String> ORDER_STATUS=new HashMap<String,String>();
			
			ORDER_STATUS.put("1", "Imported");
			ORDER_STATUS.put("2", "Committed");
			ORDER_STATUS.put("3", "Picking");
			ORDER_STATUS.put("4", "Packing");
			ORDER_STATUS.put("5", "Staging");
			ORDER_STATUS.put("6", "Staged");
			ORDER_STATUS.put("7", "Loading");
			ORDER_STATUS.put("8", "Closed");
			ORDER_STATUS.put("9", "Cancel");
			
			DBRow para=new DBRow();
			for (DBRow row : rows) {
				int ps_id = (Integer) row.getValue("send_psid");
				
				Date a0 = (Date) row.getValue("a0");
				if (a0 != null)
					row.put("a0", DateUtil.showLocationTime(a0, ps_id,"MM/dd/yyyy HH:mm:ss").substring(0, 10));
				
				Date a20 = (Date) row.getValue("a20");
				if (a20 != null)
					row.put("a20", DateUtil.showLocationTime(a20, ps_id,"MM/dd/yyyy HH:mm:ss").substring(0, 10));
				
				Date a21 = (Date) row.getValue("a21");
				if (a21 != null)
					row.put("a21", DateUtil.showLocationTime(a21, ps_id,"MM/dd/yyyy HH:mm:ss").substring(0, 10));
				
				Date a22 = (Date) row.getValue("a22");
				if (a22 != null)
					row.put("a22", DateUtil.showLocationTime(a22, ps_id,"MM/dd/yyyy HH:mm:ss").substring(0, 10));
				
				Date a23 = (Date) row.getValue("a23");
				if (a23 != null)
					row.put("a23", DateUtil.showLocationTime(a23, ps_id,"MM/dd/yyyy HH:mm:ss").substring(0, 10));
				
				Date a31 = (Date) row.getValue("a31");
				if (a31 != null){
					row.put("a30", DateUtil.showLocationTime(a31, ps_id,"MM/dd/yyyy HH:mm:ss").substring(0, 10));
					row.put("a31", DateUtil.showLocationTime(a31, ps_id,"MM/dd/yyyy HH:mm:ss").substring(11, 16));
				}else{
					String a30 = row.getString("a30");
					row.put("a30", a30);
				}
				
				Date a32 = (Date) row.getValue("a32");
				if (a32 != null)
					row.put("a32", DateUtil.showLocationTime((Date) a32, ps_id,"MM/dd/yyyy HH:mm:ss").substring(0, 10));
				
				String order_status=row.getString("a33");
				row.put("a33", ORDER_STATUS.get(order_status));
			}
			
			return rows;
		} catch (Exception e) {
			throw new Exception("getOrderAndDetail(String dns, String customer_id) error:" + e);
		}
	}
	
	/**
	 * 查找出order表多余的dn数据
	 * 
	 * @param cid
	 * @param carriers
	 * @param dns
	 * @return
	 * @throws Exception
	 */
	
	public DBRow[] getLeftOrder(String cid, long importer) throws Exception {
		try {
			String sql = "select a.customer_dn from ";
			sql = sql + " b2b_order a LEFT JOIN  (select * from wms_order_lines_tmp where importer=? and customer_id='"
					+ cid + "') b ";
			sql = sql + " on a.customer_dn=b.dn and a.customer_id=b.customer_id ";
			sql += " where a.customer_id=? and a.b2b_order_status>=2 and b.dn is null ";
			DBRow row = new DBRow();
			row.add("importer", importer);
			row.add("customer_id", cid);
			return this.dbUtilAutoTran.selectPreMutliple(sql, row);
		} catch (Exception e) {
			throw new Exception("getLeftOrder(String cid,String carriers) error:" + e);
		}
	}
	
	/**
	 * 查找可以操作的dn
	 * 
	 * @param cid
	 * @param carriers
	 * @param dns
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOrder(String cid, String dns, long importer) throws Exception {
		try {
			String sql = "select distinct a.customer_dn,a.b2b_order_status from wms_order_lines_tmp b INNER JOIN b2b_order a on a.customer_dn=b.dn and a.customer_id=b.customer_id where b.customer_id=? and b.importer=? and a.b2b_order_status<>6";
			if (null != dns && dns.trim().length() > 0) {
				sql = sql + " and b.dn not in (" + dns + ")";
			}
			DBRow row = new DBRow();
			row.add("customer_id", cid);
			row.add("importer", importer);
			return this.dbUtilAutoTran.selectPreMutliple(sql, row);
		} catch (Exception e) {
			throw new Exception("getInnerOrder(String cid,String carriers) error:" + e);
		}
	}
	
	/**
	 * 查找不能处理的dn
	 * 
	 * @param cid
	 * @param carriers
	 * @param dns
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOrderNoDeal(String cid, long importer) throws Exception {
		try {
			String sql = "select a.customer_dn,a.b2b_order_status from wms_order_lines_tmp b INNER JOIN b2b_order a on a.customer_dn=b.dn and a.customer_id=b.customer_id where b.customer_id=? and importer=? and a.b2b_order_status=6";
			DBRow row = new DBRow();
			row.add("customer_id", cid);
			row.add("importer", importer);
			return this.dbUtilAutoTran.selectPreMutliple(sql, row);
		} catch (Exception e) {
			throw new Exception("getInnerOrder(String cid,String carriers,String dns) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param dn
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getB2bOrderItemId(String dn, String cid) throws Exception {
		try {
			String sql = "select DISTINCT item_id,material_number from b2b_order_item where b2b_oid=(select b2b_oid from b2b_order where customer_dn='"
					+ dn + "' and customer_id='" + cid + "')";
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("getB2bOrderItemId(String dn) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param para
	 * @param fields
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getB2bOrder(DBRow para, String[] fields, PageCtrl ctrl) throws Exception {
		if (null == ctrl)
			return getB2bOrder(para, fields);
		
		if (null == fields || fields.length <= 0)
			return getB2bOrder(para);
		
		try {
			StringBuilder sb = new StringBuilder("select ").append(StringUtils.join(fields, ",")).append(" from ")
					.append(ConfigBean.getStringValue("b2b_order"));
			
			@SuppressWarnings("unchecked")
			ArrayList<String> list = para.getFieldNames();
			
			if (list.size() > 0) {
				sb.append(" where ");
				for (String fn : list) {
					sb.append(fn).append("=? and ");
				}
			}
			
			String sql = sb.toString().substring(0, sb.toString().length() - 5);
			
			return this.dbUtilAutoTran.selectPreMutliple(sql, para, ctrl);
		} catch (Exception e) {
			throw new Exception("getB2bOrder(DBRow para,String [] fields) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param params
	 * @param fields
	 * @return
	 * @throws Exception
	 * @author: ye
	 */
	public DBRow[] getB2bOrder(DBRow para, String[] fields) throws Exception {
		if (null == fields || fields.length <= 0)
			return getB2bOrder(para);
		
		try {
			StringBuilder sb = new StringBuilder("select ").append(StringUtils.join(fields, ",")).append(" from ")
					.append(ConfigBean.getStringValue("b2b_order"));
			
			@SuppressWarnings("unchecked")
			ArrayList<String> list = para.getFieldNames();
			
			if (list.size() > 0) {
				sb.append(" where ");
				for (String fn : list) {
					sb.append(fn).append("=? and ");
				}
			}
			
			String sql = sb.toString().substring(0, sb.toString().length() - 5);
			
			return this.dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("getB2bOrder(DBRow para,String [] fields) error:" + e);
		}
	}
	
	/**
	 * 分页
	 * 
	 * @param para
	 * @param ctrl
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getB2bOrder(DBRow para, PageCtrl ctrl) throws Exception {
		try {
			StringBuilder sb = new StringBuilder("select * from " + ConfigBean.getStringValue("b2b_order"));
			
			@SuppressWarnings("unchecked")
			ArrayList<String> list = para.getFieldNames();
			
			if (list.size() > 0) {
				sb.append(" where ");
				for (String fn : list) {
					sb.append(fn).append("=? and ");
				}
			}
			
			String sql = sb.toString().substring(0, sb.toString().length() - 5);
			
			return this.dbUtilAutoTran.selectPreMutliple(sql, para, ctrl);
		} catch (Exception e) {
			throw new Exception("getB2bOrder(DBRow para) error:" + e);
		}
	}
	
	/**
	 * 查找b2b_order
	 * 
	 * @param para
	 * @return
	 * @throws Exception
	 * @author: ye
	 */
	public DBRow[] getB2bOrder(DBRow para) throws Exception {
		try {
			StringBuilder sb = new StringBuilder("select * from " + ConfigBean.getStringValue("b2b_order"));
			
			@SuppressWarnings("unchecked")
			ArrayList<String> list = para.getFieldNames();
			
			if (list.size() > 0) {
				sb.append(" where ");
				for (String fn : list) {
					sb.append(fn).append("=? and ");
				}
			}
			
			String sql = sb.toString().substring(0, sb.toString().length() - 5);
			
			return this.dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("getB2bOrder(DBRow para) error:" + e);
		}
	}
	
	/**
	 * 查找b2b_order_item
	 * 
	 * @param para
	 * @return
	 * @throws Exception
	 * @author: ye
	 */
	public DBRow[] getB2bOrderItem(DBRow para, String[] fields) throws Exception {
		if (null == fields || fields.length <= 0)
			return getB2bOrder(para);
		
		try {
			StringBuilder sb = new StringBuilder("select ").append(StringUtils.join(fields, ",")).append(" from ")
					.append(ConfigBean.getStringValue("b2b_order_item"));
			
			@SuppressWarnings("unchecked")
			ArrayList<String> list = para.getFieldNames();
			
			if (list.size() > 0) {
				sb.append(" where ");
				for (String fn : list) {
					sb.append(fn).append("=? and ");
				}
			}
			
			String sql = sb.toString().substring(0, sb.toString().length() - 5);
			
			return this.dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("getB2bOrderItem(DBRow para,String [] fields) error:" + e);
		}
	}
	
	/**
	 * 查找b2b_order
	 * 
	 * @param para
	 * @return
	 * @throws Exception
	 * @author: ye
	 */
	public DBRow[] getB2bOrderItem(DBRow para) throws Exception {
		try {
			StringBuilder sb = new StringBuilder("select * from " + ConfigBean.getStringValue("b2b_order_item"));
			
			@SuppressWarnings("unchecked")
			ArrayList<String> list = para.getFieldNames();
			
			if (list.size() > 0) {
				sb.append(" where ");
				for (String fn : list) {
					sb.append(fn).append("=? and ");
				}
			}
			
			String sql = sb.toString().substring(0, sb.toString().length() - 5);
			
			return this.dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("getB2bOrderItem(DBRow para) error:" + e);
		}
	}
	
	/**
	 * 
	 * @param para
	 * @param item_id
	 * @param b2b_oid
	 * @throws Exception
	 */
	public void updateB2bOrderItem(DBRow para, int b2b_detail_id) throws Exception {
		String whereSql = "where b2b_detail_id=" + b2b_detail_id;
		this.dbUtilAutoTran.update(whereSql, ConfigBean.getStringValue("b2b_order_item"), para);
	}
	
	public DBRow[] fillterB2BOrder(DBRow dbRow, PageCtrl ctrl) throws Exception {
		try {
			String sql = "select k.carrier,n.master_id as mbol,(SELECT c.brand_name FROM	customer_brand c where c.cb_id=a.customer_id) customer_name,r.title send_psname,s.title receive_psname, "
					+ "(select sum(i.total_weight) from "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ "  i where i.b2b_oid= a.b2b_oid) total_weight,"
					+ "(select sum(i.boxes) from "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ " i where i.b2b_oid= a.b2b_oid) total_boxes,"
					+ "(select sum(i.pallet_spaces) from "
					+ ConfigBean.getStringValue("b2b_order_item")
					+ "  i where i.b2b_oid= a.b2b_oid) total_pallets,"
					+ " a.*,m.c_country as DELIVER_CCID_NAME from "
					+ ConfigBean.getStringValue("b2b_order")
					+ " a LEFT JOIN product_storage_catalog r ON a.send_psid = r.id LEFT JOIN product_storage_catalog s ON a.receive_psid = s.id  left join country_code m on m.ccid=a.deliver_ccid left join wms_load_order_master n on n.order_id=a.b2b_oid left join carrier_scac_mcdot k on k.scac=a.carriers where 1=1";
			if (!dbRow.get("send_psid", "").equals("")) {
				sql += " and send_psid=" + dbRow.get("send_psid", "");
			}
			if (!dbRow.get("receive_psid", "").equals("")) {
				sql += " and receive_psid=" + dbRow.get("receive_psid", "");
			}
			//查询待确认的
			if (dbRow.get("b2b_order_status","").equals("11")) {
				sql += " and b2b_order_status in ('10','1')";//imported+open
			} else if (!dbRow.get("b2b_order_status", "").equals("")) {
				sql += " and b2b_order_status=" + dbRow.get("b2b_order_status", "");
			}
			if (!dbRow.get("source_type", "").equals("")) {
				sql += " and source_type=" + dbRow.get("source_type", "");
			}
			if (!dbRow.get("freight_term", "").equals("")) {
				sql += " and freight_term='" + dbRow.get("freight_term", "") + "'";
			}
			if (!dbRow.get("customer_id", "").equals("")) {
				sql += " and customer_id='" + dbRow.get("customer_id", "") + "'";
			}
			if (!dbRow.get("load_no", "").equals("")) {
				sql += " and load_no='" + dbRow.get("load_no", "") + "'";
			}
			if (dbRow.get("load_no_flag", "").equals("1")) {
				sql += " and load_no is not null";
			}
			if (dbRow.get("load_no_flag", "").equals("0")) {
				sql += " and load_no is  null";
			}
			if (!dbRow.get("customer_dn", "").equals("")) {
				sql += " and customer_dn='" + dbRow.get("customer_dn", "") + "'";
			}
			if (!dbRow.get("retail_po", "").equals("")) {
				sql += " and retail_po='" + dbRow.get("retail_po", "") + "'";
			}
			if (!dbRow.get("create_account_id", "").equals("") && !dbRow.get("create_account_id", "").equals("0")) {
				sql += " and create_account_id=" + dbRow.get("create_account_id", "");
			}
			if (!dbRow.get("retailer", "").equals("")) {
				sql += " and receive_psid in (select id from product_storage_catalog where ship_to_id = "+dbRow.get("retailer", 0)+")";
			}
			
			if(!dbRow.get("b2b_oid", "").equals("")){
				sql += " and a.b2b_oid in ("+dbRow.get("b2b_oid", "")+")";
			}
			
			String timeType=dbRow.get("timeType", "");
			long ps_id =  dbRow.get("send_psid", 0L);
			if (!timeType.equals("")) {
				Date t = new Date();
				String start=dbRow.getString("startTime");
				if(!StringUtils.isEmpty(start)){
					if (ps_id>0) {
						t = DateUtil.utcTime(start+" 00:00:00", ps_id);
					} else {
						t= DateUtil.formateDate(start+" 00:00:00");
					}
					
				} else {
					t.setTime(0l);
				}
				String end=dbRow.getString("endTime");
				Date e=new Date();
				if(!StringUtils.isEmpty(end)){
					if (ps_id>0) {
						e = DateUtil.utcTime(end+" 23:59:59", ps_id);
					} else {
						e= DateUtil.formateDate(end+" 23:59:59");
					}
				} else {
					e.setTime(0l);
				}
				
				switch(timeType){
					case "mabd":
						if(t.getTime()>0)
							sql += " and mabd>='" + DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss", t) + "'";
						if(e.getTime()>0)
							sql += " and mabd<='" + DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss", e) + "'";
						break;
					case "orderDate":
						if(t.getTime()>0)
							sql += " and customer_order_date>='" + DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss", t) + "'";
						if(e.getTime()>0)
							sql += " and customer_order_date<='" + DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss", e) + "'";
						break;
					case "reqShipDate":
						if(t.getTime()>0)
							sql += " and requested_date>='" + DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss", t) + "'";
						if(e.getTime()>0)
							sql += " and requested_date<='" + DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss", e) + "'";
						break;
				}
			
			}
			
			sql += " order by b2b_oid desc";
			
			return this.dbUtilAutoTran.selectMutliple(sql, ctrl);
		} catch (Exception e) {
			throw new Exception("fillterB2BOrder(DBRow para) error:" + e);
		}
	}
	
	public DBRow[] getB2bOrderLines(String customer_dn) throws Exception {
		try {
			String sql = "select a.customer_dn,b.item_id from b2b_order a,b2b_order_item b where a.b2b_oid=b.b2b_oid and a.customer_dn=?";
			DBRow row = new DBRow();
			row.add("customer_dn", customer_dn);
			return this.dbUtilAutoTran.selectPreMutliple(sql, row);
		} catch (Exception e) {
			throw new Exception(" getB2bOrderLines(String customer_dn) error:" + e);
		}
	}
	
	/**
	 * 根据id获取 order索引信息
	 * 
	 * @param ids
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getB2BOrders(List<Long> ids) throws SQLException {
		StringBuffer sb = new StringBuffer("");
		for (Long id: ids) {
			if (id!=null) {
				sb.append(",").append(id);
			}
		}
		String sql = "select t.b2b_oid, (select p.title from product_storage_catalog p where p.id = t.send_psid) as send_psname, t.ship_to_party_name as receive_psname, b2b_order_waybill_number, b2b_order_waybill_name, carriers, customer_dn, (select group_concat(distinct i.retail_po) from b2b_order_item i where i.b2b_oid = t.b2b_oid order by i.line_no) as retail_po from b2b_order t where t.b2b_oid in ";
		if (sb.length()>1) {
			sql+= " ("+ sb.substring(1)+")";
		} else {
			sql+=" ()";
		}
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	public void update(long b2b_oid, DBRow row) throws Exception {
		try{
			String whereSql = "where b2b_oid=" + b2b_oid;
			this.dbUtilAutoTran.update(whereSql, ConfigBean.getStringValue("b2b_order"), row);
		}catch(Exception e){
			throw new Exception("update(long b2b_oid, DBRow row) error:" + e);
		}
	}
	
	public void updateAppointment(String loadno,String cid,String app) throws Exception{
		try{
			String whereSql = "where load_no='" + loadno +"' and customer_id='"+cid+"'";
			DBRow para=new DBRow();
			para.add("pickup_appointment", app);
			this.dbUtilAutoTran.update(whereSql, ConfigBean.getStringValue("b2b_order"), para);
		}catch(Exception e){
			throw new Exception("updateAppointment(String loadno,String cid,String app) error:" + e);
		}
	}
	  
	/**
	 * 插入前的检查
	 * 
	 * @param dn
	 * @param customerid
	 * @param importer
	 * @return
	 * @throws Exception
	 */
	public boolean checkOrder(String dn, String customerid, long importer) throws Exception {
		try {
			String sql = "select count(1) as cnt from b2b_order where customer_dn='" + dn + "' and customer_id='"
					+ customerid + "'";
			DBRow row = this.dbUtilAutoTran.selectSingle(sql);
			
			int cnt = Integer.parseInt(row.getString("cnt"));
			
			if (cnt > 0) {
				sql = "update " + ConfigBean.getStringValue("wms_order_lines_tmp") + " set correct=12 where dn='" + dn
						+ "' and customer_id='" + customerid + "' and importer=" + importer + " and corrent in (4,13)";
				
				this.dbUtilAutoTran.executeSQL(sql);
				return false;
			}
			
			return true;
		} catch (Exception e) {
			throw new Exception("checkOrder(String dn,String customerid,long importer) error:" + e);
		}
	}
	
	/**
	 * 查找整条明细
	 * 
	 * @param customerId
	 * @param dn
	 * @param itemId
	 * @param lot
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findB2bLines(String customerId, String dn, String itemId, String lot) throws Exception {
		try {
			String sql = "select a.send_psid,a.company_id,a.customer_dn,a.retail_po,a.order_number,a.customer_order_date,a.freight_term,"
					+ "a.ship_to_party_name,a.deliver_house_number,a.deliver_city,a.address_state_deliver,a.deliver_zip_code,a.deliver_ccid,"
					+ "a.mabd,a.etd,a.load_no,a.pickup_appointment,a.delivery_appointment,a.deliveryed_date,a.bol_tracking,a.carriers,a.ship_not_before,a.ship_not_later,a.requested_date,a.bill_to_id,"
					+ "b.lot_number,b.b2b_count,b.item_id,b.total_weight,b.pallet_spaces,b.boxes from b2b_order a,b2b_order_item b where a.b2b_oid=b.b2b_oid "
					+ "and a.customer_id='" + customerId + "' and a.customer_dn='" + dn + "' and b.item_id='" + itemId+"' ";
//					sql+= "' and b.lot_number='" + lot + "' ";
	
			return this.dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("findB2bLines(String customerId,String dn,String itemId,String lot) error:" + e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public void setFloorProductMgrQLL(FloorProductMgrQLL floorProductMgrQLL) {
		this.floorProductMgrQLL = floorProductMgrQLL;
	}
}
