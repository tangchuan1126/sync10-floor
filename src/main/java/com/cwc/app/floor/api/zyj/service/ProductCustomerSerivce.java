package com.cwc.app.floor.api.zyj.service;

import java.util.List;

import com.cwc.app.floor.api.zyj.model.Customer;
import com.cwc.app.floor.api.zyj.model.ProductCustomer;
import com.cwc.app.floor.api.zyj.model.Title;
import com.cwc.db.PageCtrl;

/**
 * 该接口定义的方法是为了维护产品与品牌商、生产商之间的关系
 * @author Administrator
 *
 */
public interface ProductCustomerSerivce {
	
	/**
	 * 新增;返回新增记录的ID
	 * @param productCustomer
	 * @return  如果新增 成功，返回新增记录的ID;否则，返回0
	 */
	public boolean add(ProductCustomer productCustomer);
	
	/**
	 * 新增
	 * @param productCustomer
	 * @return
	 */
	public boolean update(ProductCustomer productCustomer);
	
	/**
	 * 删除指定产品与特定品牌商的关系
	 * @param productId    产品ID
	 * @param customerId   品牌商ID
	 * @return
	 */
	public boolean deleteByTitle(int productId,int titleId);
	
	/**
	 * 为产品的特定品牌商移除特定的生产商
	 * @param productId    产品ID
	 * @param customerId   品牌商ID 
	 * @param titleId	         生产商ID
	 * @return
	 */
	public boolean deleteByTitle(int productId,int customerId,int titleId);
	
	/**
	 * 判断指定的产品和指定的品牌商是否已建立关系
	 * @param productId   产品ID
	 * @param titleId  生产商ID
	 * @return
	 */
	public boolean  exist(int productId,int titleId);
	
	/**
	 * 判断指定的产品和指定的生产商、品牌商是否已建立关系
	 * @param productId   产品ID
	 * @param titleId     生产商ID
	 * @param customerId  品牌商ID
	 * @return
	 */
	public boolean  exist(int productId,int titleId,int customerId);
	
	/**
	 * 查询与特定产品相关的ProductCustomer
	 * @param productId
	 * @return
	 */
	public List<ProductCustomer>  getByProduct(int productId);
	
	/**
	 * 获取与特定产品,特定生产商相关的所有Customer
	 * @param productId 产品ID
	 * @param titleId  生产商ID
	 * @return
	 */
	public List<Customer>  getCustomers(int productId,int titleId);
	
	/**
	 * 获取与特定产品相关的所有Customer
	 * @param productId 产品ID
	 * @return
	 */
	public List<Customer>  getCustomers(int productId);
	
	/**
	 * 获取与特定产品相关的所有Title
	 * @param productId 产品ID
	 * @return
	 */
	public List<Title>  getTitles(int productId);	
	
	/**
	 * 获取与特定产品,特定品牌商相关的所有Title
	 * @param productId 产品ID
	 * @param titleId  生产商ID
	 * @return
	 */
	public List<Title>  getTitles(int productId,int customerId);
	
	/**
	 * 获取与特定品牌商相关的所有Title
	 * @param customerId 品牌商ID
	 * @return
	 */
	public List<Title>  getTitlesByCustomerId(int customerId);
	
	/**
	 * 获取与特定产品,品牌相关的所有ProductCustomer
	 * @param productId
	 * @param customerId  品牌商ID
	 * @return
	 */
	public List<ProductCustomer>  getByProductAndCustomer(int productId,int customerId);
	
	/**
	 * 获取与特定产品,生产商相关的所有ProductCustomer
	 * @param productId
	 * @param titleId  生产商ID
	 * @return
	 */
	public List<ProductCustomer>  getByProductAndTitle(int productId,int titleId);
	
}
