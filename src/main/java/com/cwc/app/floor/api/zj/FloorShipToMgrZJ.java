package com.cwc.app.floor.api.zj;

import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorShipToMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	public long addShipTo(DBRow row) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("ship_to"), row);
		} catch (Exception e) {
			throw new Exception("FloorShipToMgrZJ addShipTo error:" + e);
		}
	}
	
	public DBRow getDetailShipTo(String ship_to_name) throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("ship_to") + " where ship_to_name = ? ";
			
			DBRow para = new DBRow();
			para.add("ship_to_name", ship_to_name);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (Exception e) {
			throw new Exception("FloorShipToMgrZJ getDetailShipTo error:" + e);
		}
	}
	
	public DBRow[] getShipTosWithCLP(long clp_type_id) throws Exception {
		try {
			String sql = " select st.* from " + ConfigBean.getStringValue("ship_to") + " as st " + " join "
					+ ConfigBean.getStringValue("clp_ps") + " as cp on cp.clp_ship_to_id = st.ship_to_id " + " join "
					+ ConfigBean.getStringValue("clp_type") + " as ct on ct.sku_lp_type_id = cp.clp_type_id "
					+ " where ct.sku_lp_type_id = ? ";
			
			DBRow para = new DBRow();
			para.add("clp_type_id", clp_type_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("FloorShipToMgrZJ getShipTosWithCLP error:" + e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public DBRow[] getAllShipTo() throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("ship_to")+" order by ship_to_id desc";
			
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorShipToMgrZJ getAllShipTo error:" + e);
		}
	}
	

	/**
	 * 
	 * 获取所有的客户仓库
	 * 
	 * @param typeId
	 * @param storage_type_id
	 * @return
	 * @throws Exception <b>Date:</b>2014-8-5下午3:13:38<br>
	 * @author: cuicong
	 */
	public DBRow[] getAllShipToCustomer(long typeId, Long storage_type_id) throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog")
					+ " where storage_type = ?  and storage_type_id=?";
			DBRow para = new DBRow();
			
			para.add("storage_type", typeId);
			para.add("storage_type_id", storage_type_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("FloorShipToMgrZJ getDetailShipTo error:" + e);
		}
	}
	
	public DBRow[] getAllShipToCustomer0(long typeId, Long storage_type_id) throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog")
					+ " where storage_type in (7,8)  and storage_type_id=?";
			DBRow para = new DBRow();
			
			para.add("storage_type", typeId);
			para.add("storage_type_id", storage_type_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception("FloorShipToMgrZJ getDetailShipTo error:" + e);
		}
	}
	
	
	public DBRow[] getAllShipToOrderByName(PageCtrl pc) throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("ship_to") +" order by ship_to_id desc";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (Exception e) {
			throw new Exception("FloorShipToMgrZJ getAllShipTo error:" + e);
		}
	}
	
	public DBRow getDetailShipToById(long id) throws Exception{
		try {
			String sql = "select * from " + ConfigBean.getStringValue("ship_to") + " where ship_to_id = ? ";
			
			DBRow para = new DBRow();
			para.add("ship_to_id", id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (Exception e) {
			throw new Exception("FloorShipToMgrZJ getDetailShipTo error:" + e);
		}
		
	}
	public boolean deleteShipTo(long id) throws Exception{
		try {
			dbUtilAutoTran.delete("where ship_to_id="+id,ConfigBean.getStringValue("ship_to"));
			return true;
		} catch (Exception e) {
			throw new Exception("FloorShipToMgrZJ deleteShipTo error:" + e);
		}
	}
	
	public int updateShipTo(DBRow row, long id) throws Exception{
		try {
			return dbUtilAutoTran.update("where ship_to_id="+id,ConfigBean.getStringValue("ship_to"),row);
			
		} catch (Exception e) {
			throw new Exception("FloorShipToMgrZJ updateShipTo error:" + e);
		}
	}
	
	public DBRow[] getStorageCatalogOfShipToByTitle(String title,long storage_type_id, int storage_type)throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") 
					+ " where 1=1";
			if(!StrUtil.isBlank(title))
			{
				sql += " and title = '"+title+"'";
			}
			if(storage_type_id > 0)
			{
				sql += " and storage_type_id = " + storage_type_id;
			}
			if(storage_type > 0)
			{
				sql += " and storage_type = " + storage_type;
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorShipToMgrZJ.getProductStorageDetailCatalogByTitle(row) error:" + e);
		}
	}
	
	public long addStorageCatalog(DBRow row) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_storage_catalog"), row);
		} catch (Exception e) {
			throw new Exception("FloorShipToMgrZJ addStorageCatalog error:" + e);
		}
	}
	
	public DBRow getDetailStorageCatalogById(long id) throws Exception{
		try {
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where id = ? ";
			
			DBRow para = new DBRow();
			para.add("id", id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} catch (Exception e) {
			throw new Exception("FloorShipToMgrZJ getDetailStorageCatalogById error:" + e);
		}
		
	}
	
	public DBRow[] getStorageCatalogsOfShipTo(long storage_type_id, int storage_type)
			throws Exception
	{
		try 
		{
			String sql = "select psc.id,psc.title,psc.send_house_number,psc.send_street,psc.city,psc.send_zip_code,psc.native,"
					+ "psc.pro_id,psc.send_pro_input,psc.contact,psc.phone,cp.pro_name, cc.c_country,psc.active"
					+ " from "+ConfigBean.getStringValue("product_storage_catalog")+" psc LEFT JOIN "
					+ ConfigBean.getStringValue("country_province") + " cp ON psc.pro_id = cp.pro_id "
					+ "LEFT JOIN "+ConfigBean.getStringValue("country_code")+" cc ON psc.native = cc.ccid "
							+ "WHERE storage_type_id = ? and storage_type = ? order by psc.id desc";
			
			DBRow para = new DBRow();
			para.add("storage_type_id",storage_type_id);
			para.add("storage_type", storage_type);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorShipToMgrZJ getStorageCatalogsOfShipTo error:"+e);
		}
	}
	
	public boolean deleteStorageCatalog(long id) throws Exception{
		try {
			dbUtilAutoTran.delete("where id="+id,ConfigBean.getStringValue("product_storage_catalog"));
			return true;
		} catch (Exception e) {
			throw new Exception("FloorShipToMgrZJ deleteStorageCatalog error:" + e);
		}
	}
	
	public int updateStorageCatalog(DBRow row, long id) throws Exception{
		try {
			return dbUtilAutoTran.update("where id="+id,ConfigBean.getStringValue("product_storage_catalog"),row);
			
		} catch (Exception e) {
			throw new Exception("FloorShipToMgrZJ updateStorageCatalog error:" + e);
		}
	}
	
	public DBRow getStorageCatalogInfoById(long storageId)
			throws Exception
	{
		try 
		{
			String sql = "select psc.id,psc.title,psc.send_house_number,psc.send_street,psc.city,psc.send_zip_code,psc.native,"
					+ "psc.pro_id,psc.send_pro_input,psc.contact,psc.phone,psc.storage_type_id,cp.pro_name, cc.c_country"
					+ " from "+ConfigBean.getStringValue("product_storage_catalog")+" psc LEFT JOIN "
					+ ConfigBean.getStringValue("country_province") + " cp ON psc.pro_id = cp.pro_id "
					+ "LEFT JOIN "+ConfigBean.getStringValue("country_code")+" cc ON psc.native = cc.ccid "
							+ "WHERE id = ? ";
			
			DBRow para = new DBRow();
			para.add("id",storageId);
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorShipToMgrZJ getStorageCatalogsOfShipTo error:"+e);
		}
	}
	
	public DBRow[] getShipToListByIds(String idsList,PageCtrl pc) throws Exception
	{
		try 
		{
			String sql = "select * from " + ConfigBean.getStringValue("ship_to") +" WHERE ship_to_id IN ( "+idsList+" ) ";
			
			DBRow para = new DBRow();
			//para.add("ship_to_id",idsList);
			return dbUtilAutoTran.selectMutliple(sql,pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorShipToMgrZJ getShipToListByIds error:"+e);
		}
	}
	
	public DBRow[] getShipToStorageListByIds(String idsList,PageCtrl pc) throws Exception
	{
		try 
		{
//			String sql = "select psc.id,psc.title,psc.send_house_number,psc.send_street,psc.city,psc.send_zip_code,psc.native,"
//					+ "psc.pro_id,psc.send_pro_input,psc.contact,psc.phone,cp.pro_name, cc.c_country"
//					+ " from "+ConfigBean.getStringValue("product_storage_catalog")+" psc LEFT JOIN "
//					+ ConfigBean.getStringValue("country_province") + " cp ON psc.pro_id = cp.pro_id "
//					+ "LEFT JOIN "+ConfigBean.getStringValue("country_code")+" cc ON psc.native = cc.ccid "
//							+ "WHERE psc.id IN ( "+idsList+" ) ";
			String sql = (new StringBuilder()).append("select  ship.ship_to_name,psc.id,psc.title,psc.send_house_number,psc.send_street,psc.city,psc.send_zip_code,psc.native,psc.pro_id,psc.send_pro_input,psc.contact,psc.phone,cp.pro_name, cc.c_country from  ")
					.append(ConfigBean.getStringValue("product_storage_catalog")).append(" psc LEFT JOIN ")
					.append(ConfigBean.getStringValue("ship_to")).append(" ship ON psc.storage_type_id = ship.ship_to_id ")
					.append("  LEFT JOIN ").append(ConfigBean.getStringValue("country_province"))
					.append(" cp ON psc.pro_id = cp.pro_id ").append("LEFT JOIN ")
					.append(ConfigBean.getStringValue("country_code"))
					.append(" cc ON  psc.native = cc.ccid ")
					.append("WHERE psc.id IN ( ").append(idsList).append(" ) ").toString();


			
			DBRow para = new DBRow();
			//para.add("ship_to_id",idsList);
			return dbUtilAutoTran.selectMutliple(sql,pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorShipToMgrZJ getShipToListByIds error:"+e);
		}
	}
	
	public DBRow getDetailProvinceByProId(long pro_id)
			throws Exception
		{
			try
			{
				String sql = "select * from " + ConfigBean.getStringValue("country_province") + " where pro_id=?";
		
				DBRow para = new DBRow();
				para.add("pro_id",pro_id);
				
				return(dbUtilAutoTran.selectPreSingle(sql,para));
			}
			catch (Exception e)
			{
				throw new Exception("FloorShipToMgrZJ.getDetailProvinceByProId(pro_id) error:" + e);
			}
		}
	
	/**
     * shipTo导出excel
     * @author Yuanxinyu
     **/
    public DBRow[] getAllShipToDetail() throws Exception{

        try {

            String sql = "SELECT ship.ship_to_name,psc.id,psc.title,psc.send_house_number,psc.send_street,psc.city,psc.send_zip_code,psc.native,psc.pro_id,psc.send_pro_input,psc.contact,psc.phone,cp.pro_name,cc.c_country "
            		+ " FROM product_storage_catalog psc "
            		+ " LEFT JOIN country_province cp ON psc.pro_id = cp.pro_id "
            		+ " LEFT JOIN country_code cc ON psc.native = cc.ccid "
            		+ " JOIN ship_to ship ON ship.ship_to_id = psc.storage_type_id "
            		+ " where psc.storage_type = " + StorageTypeKey.RETAILER
            		+ " ORDER BY psc.id DESC";

            return dbUtilAutoTran.selectMutliple(sql);

        }catch (Exception e){
            throw new Exception("floorShipToMgrZJ.getAllShipToDetail() error:" + e);
        }
    }
    
    public DBRow getShipToById(long id) throws Exception{
    	
    	try {

            String sql = "select * from ship_to where ship_to_id = "+id;

            return dbUtilAutoTran.selectSingle(sql);

        }catch (Exception e){
            throw new Exception("floorShipToMgrZJ.getShipToById(long id) error:" + e);
        }
    }

    public DBRow[] getShipToDetailedById(String ids) throws Exception{
    	
    	try {

            String sql = "SELECT ship.ship_to_id,ship.ship_to_name,psc.id,psc.title,psc.send_house_number,psc.send_street,psc.city,psc.send_zip_code,psc.native,psc.pro_id,psc.send_pro_input,psc.contact,psc.phone,cp.pro_name,cc.c_country FROM product_storage_catalog psc LEFT JOIN country_province cp ON psc.pro_id = cp.pro_id LEFT JOIN country_code cc ON psc.native = cc.ccid JOIN ship_to ship ON ship.ship_to_id = psc.storage_type_id where ship.ship_to_id in ("+ids+")";

            return dbUtilAutoTran.selectMutliple(sql);

        }catch (Exception e){
            throw new Exception("floorShipToMgrZJ.getShipToDetailedById(long id) error:" + e);
        }
    }
    
    /**
	 * ship to advanced search
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	public DBRow[] ShipToAdvanceSearch(DBRow filter,PageCtrl pc) throws Exception{
		try
	    {
			String sql = "SELECT st.*,psc.id,psc.title,psc.send_house_number,psc.send_street,psc.city,psc.send_zip_code,psc.native,psc.pro_id,psc.send_pro_input,psc.contact,psc.phone,cp.pro_name, cc.c_country,psc.active "
					+ " FROM ship_to st "
					+ " LEFT JOIN product_storage_catalog psc "
					+ " ON st.ship_to_id = psc.storage_type_id AND psc.storage_type = " + StorageTypeKey.RETAILER
					+ " LEFT JOIN country_code cc  ON cc.ccid = psc.native "
					+ " LEFT JOIN country_province cp on cp.pro_id = psc.pro_id "
					+ " WHERE 1 = 1 ";
			
			if(!"".equals(filter.getString("country", "").trim())){
				sql += " AND cc.c_country = '" + filter.getString("country") + "' ";
			}
			if(!"".equals(filter.getString("state", "").trim())){
				sql = sql + " AND ( cp.pro_name = '" + filter.getString("state") + "' or psc.send_pro_input = '" + filter.getString("state") + "' ) ";
			}
			if(!"".equals(filter.getString("ship_to_name", "").trim())){
				sql += " AND st.ship_to_name LIKE '%"+filter.getString("ship_to_name")+"%' ";
			}
			if(!"".equals(filter.getString("title", "").trim())){
				sql += " AND psc.title LIKE '%"+filter.getString("title")+"%' ";
			}
			
			
			if (filter.get("ship_to_id", 0) > 0) {
		        sql = sql + " AND st.ship_to_id = " + filter.get("ship_to_id", 0) + " ";
			}
			sql = sql + filter.getString("group") +" " + " ORDER BY st.ship_to_id DESC ";
			
			return dbUtilAutoTran.selectMutliple(sql,pc);
	    }
	    catch (Exception e)
	    {
	      throw new Exception("FloorShipToMgrZJ ShipToAdvanceSearch error:" + e);
	    }
	}
	
	
	/**
	 * 查询AddressBook
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAddressBook(long storage_type_id, int storage_type, PageCtrl pc) throws Exception
	{
		try 
		{
			String sql = "select * "
					+ " from "+ConfigBean.getStringValue("product_storage_catalog")+" psc LEFT JOIN "
					+ ConfigBean.getStringValue("country_province") + " cp ON psc.pro_id = cp.pro_id "
					+ "LEFT JOIN "+ConfigBean.getStringValue("country_code")+" cc ON psc.native = cc.ccid "
							+ "WHERE storage_type_id = "+storage_type_id+" and storage_type = "+storage_type+" order by psc.id desc";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorAddressBookMgr getAddressBook error:"+e);
		}
	}

}
