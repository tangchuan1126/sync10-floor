package com.cwc.app.floor.api.fa.basicdata;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

/**
 * 
 * @ProjectName: [basicdata]
 * @Package: [com.oso.basicdata.floor.FloorRequirementItemsDetailsMgrZYY.java]
 * @ClassName: [FloorRequirementItemsDetailsMgrZYY]
 * @Description: [一句话描述该类的功能]
 * @Author: [赵永亚]
 * @CreateDate: [2015年4月5日 上午10:16:54]
 * @UpdateUser: [赵永亚]
 * @UpdateDate: [2015年4月5日 上午10:16:54]
 * @UpdateRemark: [说明本次修改内容]
 * @Version: [v1.0]
 * 
 */
public class FloorRequirementItemsDetailsMgrZYY {
	private DBUtilAutoTran dbUtilAutoTran;
	private final static String TABLE_DETAILS = "requirement_item_details";

	/**
	 * 
	 * @param id
	 * @param flag
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getByRiId(int rIid) throws Exception {
		try {

			String _sql = "select * from " + TABLE_DETAILS
					+ " where 1=1 and ri_id = " + rIid;
			DBRow[] values = dbUtilAutoTran.selectMutliple(_sql);
			return values;
		} catch (Exception e) {
			throw new Exception("FloorRequirementItemsMgrZYY->getById" + e);
		}

	}
	/**
	 * 
	 * @param rids
	 * @param flag
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getByRiId(int[] rids) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("select * from ").append( TABLE_DETAILS )
				.append(" where 1=1 ");
			if(null!=rids && rids.length>0){
				sql.append(" AND (");
			}
			for(int i =0,j=rids.length;i<j;i++){
				if(i==0){
					sql.append(" ri_id = ").append(rids[i]);
				}else{
					sql.append(" or ri_id = ").append(rids[i]);
				}
			}
			if(null!=rids && rids.length>0){
				sql.append(")");
			}
			DBRow[] values = dbUtilAutoTran.selectMutliple(sql.toString());
			return values;
		} catch (Exception e) {
			throw new Exception("FloorRequirementItemsMgrZYY->getById" + e);
		}

	}
	
	public int deleteById(int rid_id) throws Exception {
		try {
			String sql = " where rid_id = " + rid_id;
			return dbUtilAutoTran.delete(sql, TABLE_DETAILS);
		} catch (Exception e) {
			throw new Exception("FloorTitleShipToMgrZYY.deleteById(long id) error:"
					+ e);
		}
	}
	
	public int deleteByRiId (int ri_id) throws Exception {
		try {
			String sql = " where ri_id = " + ri_id;
			return dbUtilAutoTran.delete(sql, TABLE_DETAILS);
		} catch (Exception e) {
			throw new Exception("FloorTitleShipToMgrZYY.deleteByRiId(long id) error:"
					+ e);
		}
	}
	
	public DBUtilAutoTran getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

}
