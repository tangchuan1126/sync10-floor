package com.cwc.app.floor.api.fa.basicdata;

import com.cwc.db.DBRow;
import com.cwc.app.floor.api.fa.basicdata.BasicBean;

/**  
 * 
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.beans.RequirementItemDetails.java] 
 * @ClassName:    [RequirementItemDetails]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年4月2日 上午10:44:42]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年4月2日 上午10:44:42]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
public class RequirementItemDetails implements BasicBean {

	
	public static String REQUIREMENT_DETAILS_ID = "rid_id";
	public static String REQUIREMENT_ID = "ri_id";
	public static String ITEM_NAME= "item_name";
	public static String ITEM_VALUE = "item_value";
	public static String SORT = "sort";
	
	private int rid_id;
	private int ri_id;
	private String item_name;
	private int item_value;
	private int sort;
	@Override
	public DBRow toDBRow(boolean flag) {
		DBRow row = new DBRow();
		if (getRid_id() != 0 || flag) {
			row.add(REQUIREMENT_DETAILS_ID, getRid_id());
		}
		row.add(REQUIREMENT_ID, ri_id);
		row.add(ITEM_NAME, item_name);
		row.add(ITEM_VALUE, item_value);
		row.add(SORT, sort);
		return row;
	}

	public int getRid_id() {
		return rid_id;
	}

	public void setRid_id(int rid_id) {
		this.rid_id = rid_id;
	}

	public int getRi_id() {
		return ri_id;
	}

	public void setRi_id(int ri_id) {
		this.ri_id = ri_id;
	}

	public String getItem_name() {
		return item_name;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public int getItem_value() {
		return item_value;
	}

	public void setItem_value(int item_value) {
		this.item_value = item_value;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}
	
	

}
