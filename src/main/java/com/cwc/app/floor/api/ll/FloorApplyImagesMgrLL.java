package com.cwc.app.floor.api.ll;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.cwc.app.beans.ll.ApplyImagesBeanLL;
import com.cwc.db.DBRow;

public class FloorApplyImagesMgrLL {
	private ApplyImagesBeanLL applyImagesBeanLL;

	public ApplyImagesBeanLL getApplyImagesBeanLL() {
		return applyImagesBeanLL;
	}

	public void setApplyImagesBeanLL(ApplyImagesBeanLL applyImagesBeanLL) {
		this.applyImagesBeanLL = applyImagesBeanLL;
	}
	
	public DBRow insertApplyImages(DBRow row) throws Exception {
		return applyImagesBeanLL.insertRow(row);
	}
	
	public DBRow updateApplyImages(DBRow row) throws Exception {
		return applyImagesBeanLL.updateRow(row);
	}
	public List getImageList(String association_id, String association_type) throws Exception {
		DBRow para = new DBRow();
		para.add("association_id", association_id);
		para.add("association_type", association_type);
		DBRow[] rows = applyImagesBeanLL.getRowsByPara(para, null, null, "",null);
		List imageList = new ArrayList();
		if(rows != null && rows.length>0) {
			for(int i=0;i<rows.length;i++) {
				HashMap imageMap = new HashMap();
				imageMap.put("id", rows[i].getString("id"));
				imageMap.put("association_id", association_id);
				imageMap.put("association_type", association_type);
				imageMap.put("path", rows[i].getString("path"));
				imageList.add(imageMap);
			}
		}
		return imageList;
	}
	
	public void deleteImage(String id) throws Exception {
		String[] ids = new String[1];
		ids[0] = id;
		applyImagesBeanLL.delete(ids);
	}
}
