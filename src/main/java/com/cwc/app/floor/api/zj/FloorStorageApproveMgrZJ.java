package com.cwc.app.floor.api.zj;

import com.cwc.app.key.ApproveStatusKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorStorageApproveMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加区域审核
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addStorageApproveArea(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("storage_approve_area"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ addStorageApproveArea error:"+e);
		}
	}
	
	/**
	 * 获得区域审核明细
	 * @param saa_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailStorageApproveArea(long saa_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_approve_area")+" where saa_id =?";
			
			DBRow para = new DBRow();
			para.add("saa_id",saa_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ getDetailStorageApproveArea error:"+e);
		}
	}
	
	public DBRow[] getWaitStorageApproveArea(long area_id)
			throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_approve_area")+" where area_id = ? and approve_status = "+ApproveStatusKey.WAITAPPROVE;
			
			DBRow para = new DBRow();
			para.add("area_id",area_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ getWaitStorageApproveArea error:"+e);
		}
	}
	
	/**
	 * 修改仓库区域审核
	 * @param saa_id
	 * @param para
	 * @throws Exception
	 */
	public void modStorageApproveArea(long saa_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where saa_id = "+saa_id,ConfigBean.getStringValue("storage_approve_area"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ modStorageApproveArea error:"+e);
		}
	}
	
	/**
	 * 添加位置审核
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addStorageApproveLocation(DBRow row)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("storage_approve_location"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ addStorageApproveLocation error:"+e);
		}
	}
	
	/**
	 * 获得位置审核详细
	 * @param sal_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailStorageApproveLocation(long sal_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_approve_location")+" where sal_id = ?";
			
			DBRow para = new DBRow();
			para.add("sal_id",sal_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ getDetailStorageApproveLocation error:"+e);
		}
	}
	
	/**
	 * 修改位置审核
	 * @param sal_id
	 * @param row
	 * @throws Exception
	 */
	public void modStorageApproveLocation(long sal_id,DBRow row)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where sal_id = "+sal_id,ConfigBean.getStringValue("storage_approve_location"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ modStorageApproveLocation error:"+e);
		}
	}
	
	
	public long addStorageApproveContainer(DBRow row)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("storage_approve_container"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ addStorageApproveContainer error:"+e);
		}
	}
	
	public void modStorageApproveContainer(long sac_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where sac_id = "+sac_id,ConfigBean.getStringValue("storage_approve_container"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ modStorageApproveContainer error:"+e);
		}
	}
	
	/**
	 * 添加位置商品不同
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addStorageLocationDifferents(DBRow row)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("storage_location_differents"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ addStorageLocationDifferents error:"+e);
		}
	}
	
	/**
	 * 添加关系不同容器
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addStorageLocationDifferentsContainer(DBRow row)
			throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("storage_location_differents_container"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ addStorageLocationDifferentsContainer error:"+e);
		}
	}
	
	public DBRow getDetailStorageLocationDifferentsContainer(long saa_id,long con_id)
			throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_location_differents_container")+" where saa_id = ? and con_id = ? ";
			
			DBRow para = new DBRow();
			para.add("saa_id",saa_id);
			para.add("con_id",con_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ getDetailStorageLocationDifferentsContainer error:"+e);
		}
	}
	
	public DBRow getDetailStorageLocationDifferent(long sld_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_location_differents")+" where sld_id = ?";
			
			DBRow para = new DBRow();
			para.add("sld_id",sld_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ getDetailStorageLocationDifferent error:"+e);
		}
	}
	
	/**
	 * 修改位置上的不同
	 * @param sld_id
	 * @param row
	 * @throws Exception
	 */
	public void modStorageLocationDifferent(long sld_id,DBRow row)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where sld_id = "+sld_id,ConfigBean.getStringValue("storage_location_differents"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ modStorageLocationDifferent error:"+e);
		}
	}
	
	public void modStorageLocationDifferentSac(long saa_id,long con_id,long sac_id)
		throws Exception
	{
		try
		{
			DBRow row = new DBRow();
			row.add("sac_id",sac_id);
			dbUtilAutoTran.update("where saa_id = "+saa_id+" and con_id = "+con_id,ConfigBean.getStringValue("storage_location_differents"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ modStorageLocationDifferent error:"+e);
		}
	}
	
	/**
	 * 根据仓库位置审核获得不同明细
	 * @param sal_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLocationDifferentsBySal(long sal_id)
		throws Exception
	{
		try 
		{
			String sql = "select sld.*,p.p_name from "+ConfigBean.getStringValue("storage_location_differents")+" as sld "
						+"join "+ConfigBean.getStringValue("product")+" as p on sld.pc_id = p.pc_id "
						+"where sld.sal_id = ? ";
			
			DBRow para = new DBRow();
			para.add("sal_id",sal_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ getStorageLocationDifferentsBySal error:"+e);
		}
	}
	
	
	/**
	 * 根据区域ID获得有哪些位置有不同
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLocationSlcDifferent(long area_id)
		throws Exception
	{
		try
		{
			String sql = "select distinct slcr.* from ( "
				+"select distinct slc.*,COALESCE(psl.physical_quantity,0)as physical_quantity,sum(sl.quantity) as takestock_quantity from storage_location as sl " 
				+"left join product_store_location as psl on sl.pc_id = psl.pc_id and sl.ps_id = psl.ps_id and sl.slc_id = psl.slc_id " 
				+"join storage_location_catalog slc on sl.slc_id = slc.slc_id and slc.slc_area = ? "
				+"group by slc.slc_id,sl.pc_id "
				+"union "
				+"select distinct slc.*,psl.physical_quantity,COALESCE(sl.quantity,0) as takestock_quantity from product_store_location as psl "
				+"left join storage_location as sl on sl.pc_id = psl.pc_id and sl.ps_id = psl.ps_id and sl.slc_id = psl.slc_id " 
				+"join storage_location_catalog slc on psl.slc_id = slc.slc_id and slc.slc_area = ? "
				+"group by slc.slc_id,psl.pc_id "
				+")differentsSlcView " 
				+"join storage_location_catalog as slcr on differentsSlcView.slc_id = slcr.slc_id "
				+"where physical_quantity <> takestock_quantity";
			
			DBRow para = new DBRow();
			para.add("sl_slc_area",area_id);
			para.add("psl_slc_area",area_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ getStorageLocationSlcDifferent error:"+e);
		}
	}
	
	/**
	 * 获得位置上盘点数量不同的商品
	 * @param slc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLocationDifferentsBySlc(long slc_id)
		throws Exception
	{
			try 
			{
				String sql = "select distinct * from ( "
					+"select slc.slc_area,slc.slc_id,sl.pc_id,COALESCE(psl.physical_quantity,0)as physical_quantity,sum(sl.quantity) as takestock_quantity from storage_location as sl " 
					+"left join product_store_location as psl on sl.pc_id = psl.pc_id and sl.ps_id = psl.ps_id and sl.slc_id = psl.slc_id " 
					+"join storage_location_catalog slc on sl.slc_id = slc.slc_id and slc.slc_id = ? "
					+"group by slc.slc_id,sl.pc_id "
					+"union all "
					+"select slc.slc_area,slc.slc_id,psl.pc_id,psl.physical_quantity,sum(COALESCE(sl.quantity,0)) as takestock_quantity from product_store_location as psl "
					+"left join storage_location as sl on sl.pc_id = psl.pc_id and sl.ps_id = psl.ps_id and sl.slc_id = psl.slc_id " 
					+"join storage_location_catalog slc on psl.slc_id = slc.slc_id and slc.slc_id = ? "
					+"group by slc.slc_id,psl.pc_id "
					+")differentsView where physical_quantity <> takestock_quantity";

				DBRow para = new DBRow();
				para.add("sl_slc_id",slc_id);
				para.add("psl_slc_id",slc_id);
				
				return dbUtilAutoTran.selectPreMutliple(sql, para);
			} 
			catch (Exception e) 
			{
				throw new Exception("FloorStorageApproveMgrZJ getStorageLocationDifferentsBySlc error:"+e);
			}
	}
	
	
	/**
	 * 根据区域获得说有差异
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLocationAreaDifferents(long area_id)
		throws Exception
	{
		try 
		{
			String sql = "select distinct * from ( "
						+"select slc.slc_area,slc.slc_id,slc.slc_position_all as location,sl.pc_id,p.p_name,COALESCE(psl.physical_quantity,0)as physical_quantity,sum(sl.quantity) as takestock_quantity from storage_location as sl " 
						+"left join product_store_location as psl on sl.pc_id = psl.pc_id and sl.ps_id = psl.ps_id and sl.slc_id = psl.slc_id " 
						+"join storage_location_catalog slc on sl.slc_id = slc.slc_id and slc.slc_area = ? "
						+"join product as p on sl.pc_id = p.pc_id "
						+"group by sl.slc_id,sl.pc_id "
						+"union all "
						+"select slc.slc_area,slc.slc_id,slc.slc_position_all as location,psl.pc_id,p.p_name,psl.physical_quantity,sum(COALESCE(sl.quantity,0)) as takestock_quantity from product_store_location as psl "
						+"left join storage_location as sl on sl.pc_id = psl.pc_id and sl.ps_id = psl.ps_id and sl.slc_id = psl.slc_id " 
						+"join storage_location_catalog slc on psl.slc_id = slc.slc_id and slc.slc_area = ? "
						+"join product as p on psl.pc_id = p.pc_id "
						+"group by psl.slc_id,psl.pc_id "
						+")differentsView where physical_quantity <> takestock_quantity " 
						+"order by slc_id";
			
			DBRow para = new DBRow();
			para.add("sl_slc_area",area_id);
			para.add("psl_slc_area",area_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ getStorageLocationDifferents error:"+e);
		}
	}
	
	/**
	 * 区域审核过滤
	 * @param ps_id
	 * @param area_id
	 * @param status
	 * @param sortby
	 * @param sort
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] filterStorageApproveArea(long ps_id,long area_id,int status,String sortby,boolean sort,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String whereString = "";
			if (ps_id>0) 
			{
				whereString += " and saa.ps_id = "+ps_id;
			}
			if (area_id>0) 
			{
				whereString += " and saa.area_id = "+area_id;
			}
			if (status==ApproveStatusKey.WAITAPPROVE) 
			{
				whereString +=" and saa.approve_status = "+ApproveStatusKey.WAITAPPROVE;
			}
			else if(status==ApproveStatusKey.APPROVE)
			{
				whereString +=" and saa.approve_status = "+ApproveStatusKey.APPROVE;
			}
			
			String orderString = "";
			if (sort) 
			{
				orderString = " order by saa."+sortby+" asc ";
			}
			else
			{
				orderString = " order by saa."+sortby+" desc ";
			}
			
			String sql = "select saa.*,adcommit.employe_name as commit_name,apcommit.employe_name as approve_name,concat(psc.title,sla.area_name) as area_name from "+ConfigBean.getStringValue("storage_approve_area")+" as saa "
						+"join "+ConfigBean.getStringValue("storage_location_area")+" as sla on saa.area_id = sla.area_id "
						+"join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on saa.ps_id = psc.id "
						+"join "+ConfigBean.getStringValue("admin")+" as adcommit on saa.commit_account = adcommit.adid "
						+"left join "+ConfigBean.getStringValue("admin")+" as apcommit on saa.approve_account = apcommit.adid "
						+"where 1=1 "+whereString+orderString;
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApproveMgrZJ filterStorageApproveArea error:"+e);
		}
	}
	
	/**
	 * 获得根据位置审核获得区域审核
	 * @param saa_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageApproveLocationBySaaId(long saa_id)
		throws Exception
	{
		try 
		{
			String sql = "select sal.*,slc.slc_ps_title,slc_position,slc_position_all from "+ConfigBean.getStringValue("storage_approve_location")+" as sal " 
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on sal.slc_id = slc.slc_id "
						+" where sal.saa_id = ? ";
			
			DBRow para = new DBRow();
			para.add("saa_id",saa_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApprove getStorageApproveLocationBySaaId error:"+e);
		}
	}
	
	public DBRow[] getStorageLocationBySlc(long slc_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_location")+" where slc_id = ? ";
			
			DBRow para = new DBRow();
			para.add("slc_id",slc_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApprove getStorageLocationBySlc error:"+e);
		}
	}
	
	/**
	 * 根据位置获得盘点提交的容器
	 * @param slc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLocationContainerBySlc(long slc_id)
		throws Exception
	{
		try
		{
			String sql = "select distinct con_id from "+ConfigBean.getStringValue("storage_location")+" where slc_id = ? ";
			
			DBRow para = new DBRow();
			para.add("slc_id",slc_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApprove getStorageLocationContainerBySlc error:"+e);
		}
	}
	
	
	public DBRow[] getStorageLocationByConId(long con_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_location")+" where con_id = ? ";
			
			DBRow para = new DBRow();
			para.add("con_id",con_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApprove getStorageLocationByConId error:"+e);
		}
	}
	
	/**
	 * 根据审核单ID获得需要审核的位置
	 * @param saa_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocationDifferentSlc(long saa_id)
		throws Exception
	{
		try 
		{
//			String sql = "select distinct slc_id from "+ConfigBean.getStringValue("storage_location_differents")+" where saa_id = ? ";
			
			String sql = "select distinct slc_id from "+ConfigBean.getStringValue("storage_location_differents")+" where saa_id = ? "
						+"union "
						+"select distinct slc_id from "+ConfigBean.getStringValue("storage_location_differents_container")+" where saa_id = ? ";
			
			DBRow para = new DBRow();
			para.add("saa_id",saa_id);
			para.add("con_saa_id",saa_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApprove getLocationDifferentSlc error:"+e);
		}
	}
	
	public DBRow[] getLocationDifferentContainer(long saa_id,long slc_id)
		throws Exception
	{
		try 
		{
//			String sql = "select distinct con_id from "+ConfigBean.getStringValue("storage_location_differents")+" where saa_id = ? and slc_id = ? ";
			
			String sql = "select distinct con_id from "+ConfigBean.getStringValue("storage_location_differents")+" where saa_id = ? and slc_id = ? "
						+" union "
						+"select distinct con_id from "+ConfigBean.getStringValue("storage_location_differents_container")+" where saa_id = ? and slc_id = ? ";
			
			DBRow para = new DBRow();
			para.add("saa_id",saa_id);
			para.add("slc_id",slc_id);
			para.add("con_saa_id",saa_id);
			para.add("con_slc_id",slc_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApprove getLocationDifferentContainer error:"+e);
		}
	}
	
	/**
	 * 根据审核事件与容器获得审核商品差异
	 * @param saa_id
	 * @param con_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocationDifferents(long saa_id,long con_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_location_differents")+" as sld "
						+"join "+ConfigBean.getStringValue("product")+" as p on sld.pc_id = p.pc_id "
						+" where saa_id = ? and con_id = ? ";
			
			DBRow para = new DBRow();
			para.add("saa_id",saa_id);
			para.add("con_id",con_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApprove getLocationDifferents error:"+e);
		}
	}
	
	public DBRow[] getStorageApproveContainers(long saa_id,long slc_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_approve_container")+" as sac " 
						+"join "+ConfigBean.getStringValue("temp_container")+" as tc on sac.approve_con_id = tc.con_id " 
						+" where sac.saa_id = ? and sac.slc_id = ?";
			
			DBRow para = new DBRow();
			para.add("saa_id",saa_id);
			para.add("slc_id",slc_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApprove getStorageApproveContainers error:"+e);
		}
	}
	
	public DBRow[] getStorageApproveContainers(long sal_id)
			throws Exception
		{
			try
			{
				String sql = "select * from "+ConfigBean.getStringValue("storage_approve_container")+" as sac " 
							+"join "+ConfigBean.getStringValue("temp_container")+" as tc on sac.approve_con_id = tc.con_id " 
							+" where sac.sal_id = ? ";
				
				DBRow para = new DBRow();
				para.add("sal_id",sal_id);
				
				return dbUtilAutoTran.selectPreMutliple(sql, para);
			}
			catch (Exception e) 
			{
				throw new Exception("FloorStorageApprove getStorageApproveContainers error:"+e);
			}
		}
	
	public DBRow getDetailStorageApproveContainer(long sac_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_approve_container")+" where sac_id = ? ";
			
			DBRow para = new DBRow();
			para.add("sac_id",sac_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorStorageApprove getDetailStorageApproveContainer error:"+e);
		}
	}
	
	public int getStorageApproveContainerCountBySal(long sal_id)
		throws Exception
	{
		String sql = "select count(*) as approved_count from "+ConfigBean.getStringValue("storage_approve_container")+" where sal_id = ? and approve_status = "+ApproveStatusKey.APPROVE;
		
		DBRow para = new DBRow();
		para.add("sal_id",sal_id);
		
		DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
		
		return result.get("approved_count",0);
	}
	
	public int getStorageApproveContainerCountBySaa(long saa_id)
		throws Exception
	{
		String sql = "select count(*) as approved_count from "+ConfigBean.getStringValue("storage_approve_location")+" where saa_id = ? and approve_status = "+ApproveStatusKey.APPROVE;
		
		DBRow para = new DBRow();
		para.add("saa_id",saa_id);
		
		DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
		
		return result.get("approved_count",0);
	}
	
	/**
	 * @author zhanjie
	 * @param sac_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageApproveDifferents(long sac_id)
		throws Exception
	{
		String sql = "select sld.*,p.p_name from "+ConfigBean.getStringValue("storage_location_differents")+" as sld "
					+" join "+ConfigBean.getStringValue("product") +" as p on sld.pc_id = p.pc_id "
				+" where sac_id = ? ";
		
		DBRow para = new DBRow();
		para.add("sac_id",sac_id);
		
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	
	
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
