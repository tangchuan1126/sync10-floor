package com.cwc.app.floor.api.tjh;


import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.cwc.app.util.StrUtil;

public class FloorSupplierMgrTJH {
	
	private DBUtilAutoTran dbUtilAutoTran;

	
	/**
	 * 查询所有的供应商信息
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllSupplier(PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("supplier") +" order by id desc";
			if (pc != null) 
			{
				return (dbUtilAutoTran.selectMutliple(sql, pc));
			}
			else
			{
				return (dbUtilAutoTran.selectMutliple(sql));
			}
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllSupplier(pc) error:"+e);
		}
	}
	
	/**
	 * 增加供应商的相关信息
	 * @param row
	 * @throws Exception 
	 */
	public void addSupplier(DBRow row) 
		throws Exception 
	{
		try 
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("supplier"));
			row.add("id", id);
			row.add("password",StrUtil.getMD5(String.valueOf(id)));
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("supplier"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("addSupplier(row) error:"+e);
		}
		
	}

	/**
	 * 删除供应商信息
	 * @param s_id
	 * @throws Exception
	 */
	public void delSupplier(long s_id) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.delete("where id="+s_id, ConfigBean.getStringValue("supplier"));
		} 
		catch (Exception e) 
		{
			throw new Exception("delSupplier(s_id) error: "+e);
		}
		
	}
	/**
	 * 查看某一具体的供应商信息
	 * @param sid
	 * @throws Exception 
	 */
	public DBRow getDetailSupplier(long sid) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("supplier")+" where id="+sid;
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailSupplier(sid) error: " + e);
		}
	}
	
	/**
	 * 修改某一供应商信息
	 * @param row
	 * @param sid
	 * @throws Exception 
	 */
	public void modSupplier(DBRow row, long sid) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.update("where id="+sid, ConfigBean.getStringValue("supplier"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("modSupplier(row,sid) error: "+e);
		}
		
	}
	
	/**
	 * 增加供应商的资质信息
	 * @param row
	 * @throws Exception
	 */
	public void addAptitude(DBRow row) 
		throws Exception 
	{
		try 
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("aptitude"));
			row.add("id", id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("aptitude"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("addAptitude(row) error: "+e);
		}
		
	}

	/**
	 * 根据供应商id查询该供应商的所有的资质文件
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAptitudeBySupId(long id) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("aptitude") + " where sup_id="+id;
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("getAptitudeBySupId(id) error :" + e);
		}
	}
	
	
	/**
	 * 删除供应商的资质文件信息
	 * @param ap_id
	 * @throws Exception
	 */
	public void delSupplierOfAptitude(long ap_id) 
		throws Exception 
	{
		try 
		{
			dbUtilAutoTran.delete("where id="+ap_id, ConfigBean.getStringValue("aptitude"));
		} 
		catch (Exception e) 
		{
			throw new Exception("delSupplierOfAptitude(ap_id) error :"+e);
		}
		
	}
	
	/**
	 * 模糊查询供应商信息
	 * @param key
	 * @throws Exception 
	 */
	public DBRow [] getSearchSupplier(String key,PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("supplier")+" where sup_name like '%"+key+"%'";
			return (dbUtilAutoTran.selectMutliple(sql, pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("getSearchSupplier(key) error: "+e);
		}
	}

	/**
	 * 获取全部的国家
	 * @return
	 * @throws Exception 
	 */
	public DBRow[] getAllCountryCode() 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("country_code");
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getAllCountryCode() error"+e);
		}
	}
	
	/**
	 * 根据国家id查询该国家下的所有省、州
	 * @param nation_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProvinceByCountryId(long nation_id) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("country_province")+" where nation_id="+nation_id;
			return (dbUtilAutoTran.selectMutliple(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getProvinceByCountryId(long nation_id) error:"+e);
		}
	}

	/**
	 * 根据id获取国家的具体信息
	 * @param nation_id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getDetailCountry(long nation_id) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("country_code")+" where ccid="+nation_id;
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailCountry(long nation_id) error:"+e);
		}
	}

	/**
	 * 根据id获取省份的具体信息
	 * @param provinceId
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProvinceById(long provinceId) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("country_province")+" where pro_id="+provinceId;
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailProvinceById(long provinceId) error:"+e);
		}
	}

	/**
	 * 获取供应商资质文件的详细信息
	 * @param ap_id
	 * @return
	 * @throws Exception 
	 */
	public DBRow getDetailSupplierOfAptitude(long ap_id) 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("aptitude")+" where id="+ap_id;
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getDetailSupplierOfAptitude(long ap_id)"+e);
		}
	}

	

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	
	
}
