package com.cwc.app.floor.api.zr;

import com.cwc.app.key.OccupyStatusTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

/**
 * 这个类查询的门肯定是好的门
 * @author win7zr
 *
 */
public class FloorStorageYardControlZr {

	private static final String appendSpotOk = "";
	private static final String statusField = "yc_status" ;
	private DBUtilAutoTran dbUtilAutoTran;
	
 	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
	/**
	 * 根据psId 和 门的状态去查询门
	 * @param ps_id
	 * @param occupied_status
	 * @param zone_id(可以为0)
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public DBRow[] getSpotByPsIdAndOccupiedStatusZoneId(long ps_id , int occupied_status ,long zone_id) throws Exception{
		try{
			String sql = "select * from storage_yard_control where ps_id="+ ps_id + appendSpotOk + " and "+statusField+"="+ occupied_status ;
			if(zone_id != 0l){
				sql += " and area_id ="+zone_id ;
			}
			return dbUtilAutoTran.selectMutliple(sql);
 		}catch (Exception e) {
			throw new Exception("FloorStorageYardControlZr.getFreeSpot(ps_id):"+e);
		}
	}
	
	/**
	 * 更新门
	 * @param sd_id
	 * @param row
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月15日
	 */
	public void updateSpot(long yc_id , DBRow row) throws Exception{
		try{
			dbUtilAutoTran.update(" where yc_id="+yc_id, "storage_yard_control", row);
		}catch(Exception e){
			throw new Exception("FloorStorageYardControlZr.updateSpot(yc_id,row):"+e);
		}
	}
	
	public DBRow[] getSpotsBySelf(long associate_id ,  int associate_type , long zone_id) throws Exception{
		try{
			String sql  = "select * from storage_yard_control where associate_type=" +associate_type + " and associate_id="+associate_id ;
			if( zone_id != 0l){
				sql += " and area_id="+zone_id ;
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorStorageYardControlZr.getSpotsBy(associate_id,associate_type):"+e);
		}
	}
	public DBRow[] getUsedSpots(long ps_id , long zone_id) throws Exception{
		try{
			String sql = "select * from storage_yard_control where ("+statusField+"="+ OccupyStatusTypeKey.OCUPIED + " or "+statusField+"="+OccupyStatusTypeKey.RESERVERED+")" ;
			sql += (" and ps_id="+ps_id );
			if(zone_id != 0l){
				sql += " and area_id="+zone_id ;
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorStorageYardControlZr.getUsedSpots(ps_id,zone_id):"+e);
		}
	}
	public DBRow getCheckInModuleSpotInfo(long yc_id ) throws Exception{
		try{
			String sql = "SELECT main.* FROM	storage_yard_control AS sd LEFT JOIN door_or_location_occupancy_main AS main ON sd.associate_id = main.dlo_id "  ;
			      sql += " WHERE sd.associate_type = "+ModuleKey.CHECK_IN+" and sd.yc_status <> "+OccupyStatusTypeKey.RELEASED+" and  sd.yc_id ="+yc_id ;
		   return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorStorageYardControlZr.getCheckInModuleSpotInfo(yc_id):"+e);
		}
	}
	public DBRow getSpotInfoBySdId(long yc_id) throws Exception{
		try{
			return dbUtilAutoTran.selectSingle("select * from storage_yard_control where yc_id=" +yc_id );
		}catch(Exception e){
			throw new Exception("FloorStorageYardControlZr.getSpotInfoBySdId(yc_id):"+e);
		}
	}
	/**
	 * 当前associate_id +　associate_type　占用 
	 * @param associate_id
	 * @param associate_type
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月17日
	 */
	public DBRow[] getUseSpotByAssociateIdAndType(long associate_id ,  int associate_type ) throws Exception{
		try{
			String sql = "select * from storage_yard_control where associate_type = " +ModuleKey.CHECK_IN + ""
					+ " and ( "+statusField+" = "+OccupyStatusTypeKey.OCUPIED +") and associate_id="+associate_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorStorageYardControlZr.getUseSpotBy(associate_id + associate_type):"+e);
		}
	}
	public void freeSpotsByAssociateId(long associate_id , int associate_type , DBRow updateRow) throws Exception{
		try{
			dbUtilAutoTran.update("where associate_type ="+ associate_type + " and associate_id="+associate_id, "storage_yard_control", updateRow);
		}catch(Exception e){
			throw new Exception("FloorStorageYardControlZr.freeSpotsByAssociateId(associate_id + associate_type):"+e);
		}
	}
	
	/**
	 * 通过spotId、关联信息获取停车位信息
	 * @param spotId
	 * @param associate_type
	 * @param associate_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月20日 上午9:13:36
	 */
	public DBRow findSpotsBySpotIdAssociateId(long spotId , int associate_type, long associate_id) throws Exception{
		try
		{
			return dbUtilAutoTran.selectSingle("select * from storage_yard_control where associate_type ="+ associate_type + " and associate_id="+associate_id+" and yc_id = "+spotId);
		}
		catch(Exception e)
		{
			throw new Exception("FloorStorageYardControlZr.findSpotsBySpotIdAssociateId:"+e);
		}
	}
	public long addCheckInLog(DBRow row) throws Exception {
		try {
 			return  dbUtilAutoTran.insertReturnId("check_in_log", row);
		} catch (Exception e) {
			throw new Exception("FloorStorageYardControlZr addCheckInLog error:"+e);
		}
	}

	public DBRow[] getSpotsCheckInModuleByAssociateId(long associate_id,
			int occupancy_status ) throws Exception{
		try{
			String sql = "select * from storage_yard_control where  associate_type="+ModuleKey.CHECK_IN+" and "+statusField+"="+occupancy_status+" and associate_id="+associate_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorStorageDoorZr getSpotsCheckInModuleByAssociateId error:"+e);
		}
	}
}
