package com.cwc.app.floor.api.zyj;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.DataOrCountKey;
import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorProprietaryMgrZyj{
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 添加title
	 * @author Administrator
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addProprietary(DBRow row) throws Exception{
		try{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("title"), row);
		}catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.addProprietary(DBRow row):"+e);
		}
	}
	
	/**
	 * 更新title
	 * @author Administrator
	 * @param id
	 * @param row
	 * @throws Exception
	 */
	public void updateProprietary(long id, DBRow row) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where title_id = " + id, ConfigBean.getStringValue("title"), row);
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.updateProprietary(long id, DBRow row):"+e);
		}
	}
	
	
	/**
	 * 通过ID删除title
	 * @author Administrator
	 * @param id
	 * @throws Exception
	 */
	public void deleteProprietary(long title_id) throws Exception
	{
		try
		{
			dbUtilAutoTran.delete(" where title_id = " + title_id, ConfigBean.getStringValue("title"));
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.deleteProprietary(long title_id):"+e);
		}
	}
	
	
	/**
	 * 通过title_id查询title记录
	 * @author Administrator
	 * @param title_id
	 * @throws Exception
	 */
	public DBRow findProprietaryByTitleId(long title_id) throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("title")+" where title_id="+title_id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryByTitleId(long title_id):"+e);
		}
	}
	
	/**
	 * 通过title_name查询title记录数
	 * @author Administrator
	 * @param title_name
	 * @throws Exception
	 */
	public int findProprietaryCountByTitleName(String title_name) throws Exception
	{
		try
		{
			String sql = "select count(*) as propCount from "+ConfigBean.getStringValue("title")+" where title_name='"+title_name+"'";
			return dbUtilAutoTran.selectSingle(sql).get("propCount", 0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryCountByTitleName(String title_name):"+e);
		}
	}
	
	/**
	 * 通过title_name查询title记录
	 * @author Administrator
	 * @param id
	 * @param row
	 * @throws Exception
	 */
	public DBRow findProprietaryByTitleName(String title_name) throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("title")+" where title_name='"+title_name+"'";
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryByTitleName(String title_name):"+e);
		}
	}
	
	/**
	 * 根据title_id获得一个title
	 * @param title_id
	 * @return
	 * @author 詹洁
	 * @throws Exception
	 */
	public DBRow getDetailTitleByTitleId(long title_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("title")+" where title_id = ? ";
			
			DBRow para = new DBRow();
			para.add("title_id",title_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj getDetailTitleByTitleId error:"+e);
		}
	}
	
	/**
	 * 根据用户ID查询title
	 * @author Administrator
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProprietaryAllOrByAdidTitleId(long adid, Long[] title_id, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from title p");
			if(adid > 0)
			{
				sql.append(" JOIN title_admin pa ON p.title_id = pa.title_admin_title_id AND pa.title_admin_adid = " + adid);
			}
			if(null != title_id && title_id.length > 0)
			{
				sql.append(" AND (p.title_id = " + title_id[0]);
				for (int i = 1; i < title_id.length; i++) 
				{
					sql.append(" OR p.title_id = " + title_id[i]);
				}
				sql.append(" )");
			}
			if(adid > 0)
			{
				sql.append(" ORDER BY IFNULL(pa.title_admin_sort, 999999), p.title_id asc");
			}
			else
			{
				sql.append(" ORDER BY p.title_id asc");
			}
			
			if(null != pc)
			{
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(long adid, long title_id, PageCtrl pc):"+e);
		}
	}
	
	public boolean findAdminAndTitleExists(long adid, long title_id) throws Exception{
		try{
			boolean flag = false;
			
			String sql = "select * from title_admin where title_admin_adid = "+adid+" and title_admin_title_id = "+title_id;
			
			DBRow[] result = dbUtilAutoTran.selectMutliple(sql);
			
			if(result!=null && result.length != 0){
				flag = true;
			}
			
			return flag;
		}
		catch (Exception e) {
			throw new Exception(""+e);
		}
	}
	
	/**
	 * 根据用户ID查询title
	 * @author Administrator
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProprietaryAllByAdid(long adid, PageCtrl pc) throws Exception
	{
		try
		{
			String sql = "select * from title p "; 
			if(adid > 0)
			{
				sql += " LEFT JOIN title_admin pa ON p.title_id = pa.title_admin_title_id";
				sql += " AND pa.title_admin_adid = " + adid;
			}
			sql+= " where 1=1";
			if(adid > 0)
			{
//				sql += " AND pa.title_admin_adid = " + adid;
				sql += " ORDER BY IFNULL(pa.title_admin_sort, 999999), p.title_id asc";
			}
			if(null != pc)
			{
				return dbUtilAutoTran.selectMutliple(sql, pc);
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql);
			}
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryAllByAdid(long adid, PageCtrl pc):"+e);
		}
	}
	
	
	/**
	 * 添加title与用户关系
	 * @author Administrator
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addProprietaryAdmin(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("title_admin"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.addProprietaryAdmin(DBRow row):"+e);
		}
	}
	
	
	/**
	 * 更新title_admin
	 * @author Administrator
	 * @param id
	 * @param row
	 * @throws Exception
	 */
	public void updateProprietaryAdminByTitleId(long title_admin_title_id, long user_adid, DBRow row) throws Exception
	{
		try
		{
			String where = " where title_admin_title_id = " + title_admin_title_id + " and title_admin_adid = " + user_adid;
			dbUtilAutoTran.update(where, ConfigBean.getStringValue("title_admin"), row);
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.updateProprietaryAdminByTitleId(long title_admin_title_id, DBRow row):"+e);
		}
	}
	
	/**
	 * 删除title与用户关系
	 * @author Administrator
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public void deleteProprietaryAdminByAdid(long adid) throws Exception
	{
		try
		{
			dbUtilAutoTran.delete(" where title_admin_adid = " + adid, ConfigBean.getStringValue("title_admin"));
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.deleteProprietaryAdminByAdid(long adid):"+e);
		}
	}
	
	/**
	 * 通过ID删除title_admin
	 * @author Administrator
	 * @param id
	 * @throws Exception
	 */
	public void deleteProprietaryAdminById(long title_admin_id, long adid, long title_admin_title_id) throws Exception{
		
		try{
			//查询需要更新的列
			String sql ="select title_admin_id " +
					" from title_admin " +
					" where title_admin_adid = "+"(select title_admin_adid from title_admin where title_admin_id = "+title_admin_id+")"+
					" and title_admin_sort > (" +
						" select title_admin_sort " +
						" from title_admin " +
						" where title_admin_id = "+title_admin_id+")";
			
			DBRow[] titleAdminId = dbUtilAutoTran.selectMutliple(sql);
			
			//更新查询后的列
			for(DBRow result : titleAdminId){
				//排序大于被删除的字段的时候,结果集减一
				dbUtilAutoTran.addSubFieldVal(ConfigBean.getStringValue("title_admin")," where title_admin_id = "+result.getString("title_admin_id"),"title_admin_sort", -1);
			}
			
			String where = " where 1=1";
			if(title_admin_id > 0)
			{
				where += " and title_admin_id = " + title_admin_id;
			}
			if(adid > 0)
			{
				where += " and title_admin_adid = " + adid;
			}
			if(title_admin_title_id > 0)
			{
				where += " and title_admin_title_id = " + title_admin_title_id;
			}
			
			dbUtilAutoTran.delete(where, ConfigBean.getStringValue("title_admin"));
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.deleteProprietaryAdminById(long title_admin_id, long adid, long title_admin_title_id):"+e);
		}
	}
	
	
	public void deleteTitleAdminByAdidAndTitleId(long adid, String titleId) throws Exception{
		try{
			//查询需要更新的列
			String sql ="select title_admin_id " +
					" from title_admin " +
					" where title_admin_adid = "+adid+
					" and title_admin_sort > (" +
						" select title_admin_sort " +
						" from title_admin " +
						" where title_admin_adid = "+adid+
						" and title_admin_title_id ="+titleId+")";
			
			DBRow[] titleAdminId = dbUtilAutoTran.selectMutliple(sql);
			
			//更新查询后的列
			for(DBRow result : titleAdminId){
				//排序大于被删除的字段的时候,结果集减一
				dbUtilAutoTran.addSubFieldVal(ConfigBean.getStringValue("title_admin")," where title_admin_id = "+result.getString("title_admin_id"),"title_admin_sort", -1);
			}
			
			//删除
			String where = " where title_admin_adid = "+adid+" and title_admin_title_id = " +titleId;
			dbUtilAutoTran.delete(where, ConfigBean.getStringValue("title_admin"));
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.deleteTitleAdminByAdidAndTitleId"+e);
		}
	}
	
	public void insertTitleAdmin(long adid, String titleId) throws Exception{
		try{
			
			String sql = "select MAX(title_admin_sort) as title_admin_sort from title_admin where title_admin_adid ="+adid;
			DBRow maxSort = dbUtilAutoTran.selectSingle(sql);
			
			DBRow param = new DBRow();
			param.add("title_admin_adid",adid);
			param.add("title_admin_title_id",titleId);
			param.add("title_admin_sort",maxSort.get("title_admin_sort",0)+1);
			//添加用戶與TITLE關係
			dbUtilAutoTran.insert(ConfigBean.getStringValue("title_admin"), param);
		}
		catch (Exception e) {
			throw new Exception(""+e);
		}
	}
	/**
	 * 通过用户，titleID，title优先级查询是否有这样的关系记录
	 * @author Administrator
	 * @return
	 * @throws Exception
	 */
	public int findProprietaryAdminByAdidAndPriority(long adid, long title_id, int title_priority) throws Exception
	{
		try
		{
			String sql = "select count(*) count from title_admin pa where 1=1"; 
			if(adid > 0)
			{
				sql += "  AND pa.title_admin_adid = " + adid;
			}
			if(title_id > 0)
			{
				sql += " AND pa.title_admin_title_id = " + title_id;
			}
			if(title_priority > 0)
			{
				sql += " AND pa.title_admin_sort = " + title_priority;
			}
			return dbUtilAutoTran.selectSingle(sql).get("count", 0);
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryAdminByAdidAndPriority(long adid, long title_id, int title_priority):"+e);
		}
	}
	
	/**
	 * 根据用户及商品分类查询title
	 * @param adid
	 * @param product_line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProprietaryAllByAdminProduct(long adid, long pc_id, PageCtrl pc)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT t.*");
			if(adid > 0)
			{
				sql.append(", ta.*");
			}
			if(pc_id > 0)
			{
				sql.append(" , tp.*");
			}
			sql.append(" FROM title t");
			
			sql.append(" LEFT JOIN title_admin ta ON ta.title_admin_title_id = t.title_id ");
			
			sql.append(" LEFT JOIN title_product tp ON tp.tp_title_id = t.title_id");
			if(pc_id > 0)
			{
				sql.append(" AND tp.tp_pc_id = " + pc_id);
			}
			sql.append(" WHERE 1=1");
			if(adid > 0)
			{
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			if(pc_id > 0)
			{
//				sql.append(" AND tp.tp_pc_id = " + pc_id);
				sql.append(" GROUP BY t.title_id ORDER BY IFNULL(tp.tp_title_id,999999), t.title_id");
			}
			else
			{
				sql.append(" GROUP BY t.title_id ORDER BY IFNULL(ta.title_admin_sort, 999999), t.title_id");
			}
			
			if(null == pc)
			{
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryAllByAdminProduct(long adid, long pc_id, PageCtrl pc):"+e);
		}
	}
	
	
	/**
	 * 根据用户及商品分类查询title
	 * @param adid
	 * @param product_line_id
	 * @param dataOrCount:1，返回数据数据；2，返回数量
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProprietaryByAdminProduct(long adid, long pc_id, int begin, int length, PageCtrl pc, int dataOrCount)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			if(DataOrCountKey.DATA == dataOrCount)
			{
				sql.append("SELECT t.*");
				if(adid > 0)
				{
					sql.append(", ta.*");
				}
				if(pc_id > 0)
				{
					sql.append(" , tp.*");
				}
			}
			else
			{
				sql.append("select count(*) cn");
			}
			sql.append(" FROM title t");
			
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = t.title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			
			if(pc_id > 0)
			{
				sql.append(" JOIN title_product tp ON tp.tp_title_id = t.title_id AND tp.tp_pc_id = " + pc_id);
				
			}
//			if(adid > 0)
//			{
//				sql.append(" ORDER BY IFNULL(tp.tp_title_id,999999), t.title_id");
//			}
//			else
//			{
//				
//			}
			sql.append(" ORDER BY t.title_name, t.title_id");
			
			if(begin >= 0)
			{
				if(length > 0)
				{
					sql.append(" LIMIT "+begin+","+length);
				}
			}
			else
			{
				if(length > 0)
				{
					sql.append(" LIMIT "+length);
				}
			}
			
			if(null == pc)
			{
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryByAdminProduct(long adid, long pc_id, int begin, int length, PageCtrl pc):"+e);
		}

	}
	
	/**
	 * 根据用户及商品线查询title
	 * @param adid
	 * @param product_line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProprietaryAllByAdminProductLine(long adid, long product_line_id, long pc_id, PageCtrl pc)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT t.*");
			if(pc_id > 0)
			{
				sql.append(" , tp.*");
			}
			sql.append(" FROM title t");
			
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = t.title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			
			if(product_line_id > 0)
			{
				sql.append(" JOIN title_product_line tpl ON tpl.tpl_title_id = t.title_id");
				sql.append(" AND tpl.tpl_product_line_id = " + product_line_id);
			}
			if(pc_id > 0)
			{
				sql.append(" LEFT JOIN title_product tp ON tp.tp_title_id = t.title_id");
				sql.append(" AND tp.tp_pc_id = " + pc_id);
				sql.append(" ORDER BY IFNULL(tp.tp_title_id,999999), t.title_id");
			}
			
			if(null == pc)
			{
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryAllByAdminProductLine(long adid, long product_line_id, long pc_id, PageCtrl pc):"+e);
		}
	}
	
	/**
	 * 根据用户及商品线查询title
	 * @param adid
	 * @param product_line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProprietaryByAdminProductLine(long adid, long product_line_id, long pc_id, PageCtrl pc)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT * ");
			
			sql.append(" FROM title t");
			
			if(adid > 0) {
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = t.title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			
			if(product_line_id > 0) {
				sql.append(" JOIN title_product_line tpl ON tpl.tpl_title_id = t.title_id JOIN customer_id ci on ci.customer_key = tpl.tpl_customer_id ");
				sql.append(" AND tpl.tpl_product_line_id = " + product_line_id);
			}
			
			if(pc_id > 0) {
				sql.append(" JOIN title_product tp ON tp.tp_title_id = t.title_id AND tp.tp_pc_id = " + pc_id);
				sql.append(" ORDER BY IFNULL(tp.tp_title_id,999999), t.title_id");
			}else{
				sql.append(" ORDER BY t.title_name asc");
			}
			
			if(null == pc)
			{
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryByAdminProductLine(long adid, long product_line_id, long pc_id, PageCtrl pc):"+e);
		}
	}
	
	/**
	 * 根据用户及商品分类查询title
	 * @param adid
	 * @param product_line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProprietaryAllByAdminProductCatagory(long adid, long product_catagory_id, long pc_id, PageCtrl pc)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT t.*");
			if(pc_id > 0)
			{
				sql.append(" , tp.*");
			}
			sql.append(" FROM title t");
			
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = t.title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			
			if(product_catagory_id > 0)
			{
				sql.append(" JOIN title_product_catalog tpc ON tpc.tpc_title_id = t.title_id");
				sql.append(" AND tpc.tpc_product_catalog_id = " + product_catagory_id);
			}
			if(pc_id > 0)
			{
				sql.append(" LEFT JOIN title_product tp ON tp.tp_title_id = t.title_id");
				sql.append(" AND tp.tp_pc_id = " + pc_id);
				sql.append(" ORDER BY IFNULL(tp.tp_title_id,999999), t.title_id");
			}
			if(null == pc)
			{
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryAllByAdminProductCatagory(long adid, long product_catagory_id, long pc_id, PageCtrl pc):"+e);
		}
	}
	
	/**
	 * 根据用户及商品分类查询title
	 * @param adid
	 * @param product_line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProprietaryByAdminProductCatagory(long adid, long product_catagory_id, long pc_id, PageCtrl pc)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT t.*");
			
			if(pc_id > 0)
			{
				sql.append(" , tp.*");
			}
			sql.append(" FROM title t");
			
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = t.title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			
			if(product_catagory_id > 0)
			{
				sql.append(" JOIN title_product_catalog tpc ON tpc.tpc_title_id = t.title_id");
				sql.append(" AND tpc.tpc_product_catalog_id = " + product_catagory_id);
			}
			if(pc_id > 0)
			{
				sql.append(" JOIN title_product tp ON tp.tp_title_id = t.title_id AND tp.tp_pc_id = " + pc_id);
				sql.append(" ORDER BY IFNULL(tp.tp_title_id,999999), t.title_id");
			}else{
				sql.append(" ORDER BY t.title_name asc");
			}
			
			if(null == pc) {
				
				return dbUtilAutoTran.selectMutliple(sql.toString());
			} else {
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryByAdminProductCatagory(long adid, long product_catagory_id, long pc_id, PageCtrl pc):"+e);
		}
	}
	
	public DBRow[] getCustomerAndTitleByCategory(long adid, long product_catagory_id, long pc_id, PageCtrl pc)throws Exception {
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT t.* , ci.* ");
			
			if(pc_id > 0) {
				sql.append(" , tp.*");
			}
			
			sql.append(" FROM title t");
			
			if(adid > 0) {
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = t.title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			
			if(product_catagory_id > 0) {
				sql.append(" JOIN title_product_catalog tpc ON tpc.tpc_title_id = t.title_id");
				sql.append(" AND tpc.tpc_product_catalog_id = " + product_catagory_id);
				sql.append(" left join customer_id ci on tpc.tpc_customer_id = ci.customer_key ");
			}
			if(pc_id > 0)
			{
				sql.append(" JOIN title_product tp ON tp.tp_title_id = t.title_id AND tp.tp_pc_id = " + pc_id);
				sql.append(" ORDER BY IFNULL(tp.tp_title_id,999999), t.title_id");
			}else{
				sql.append(" ORDER BY t.title_name asc");
			}
			
			if(null == pc) {
				
				return dbUtilAutoTran.selectMutliple(sql.toString());
			} else {
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryByAdminProductCatagory(long adid, long product_catagory_id, long pc_id, PageCtrl pc):"+e);
		}
	}
	
	/**
	 * 查询商品本身的title，即其所在的产品线，产品分类中没有
	 * @param adid
	 * @param product_line_id
	 * @param product_catagory_id
	 * @param pc_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProprietaryByPcSelf(long adid, long product_line_id, long product_catagory_id, long pc_id, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT t.*");
			if(pc_id > 0)
			{
				sql.append(" , tp.*");
			}
			sql.append(" FROM title t");
			
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = t.title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
//			if(product_line_id > 0)
//			{
//				sql.append(" JOIN title_product_line tpl ON tpl.tpl_title_id = t.title_id");
//				sql.append(" AND tpl.tpl_product_line_id = " + product_line_id);
//			}
//			if(product_catagory_id > 0)
//			{
//				sql.append(" JOIN title_product_catalog tpc ON tpc.tpc_title_id = t.title_id");
//				sql.append(" AND tpc.tpc_product_catalog_id = " + product_catagory_id);
//			}
			if(pc_id > 0)
			{
				sql.append(" JOIN title_product tp ON tp.tp_title_id = t.title_id AND tp.tp_pc_id = " + pc_id);
				sql.append(" ORDER BY IFNULL(tp.tp_title_id,999999), t.title_id");
			}
			
			if(null == pc)
			{
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryByPcSelf(long adid, long product_line_id, long product_catagory_id, long pc_id, PageCtrl pc):"+e);
		}
	}
	
	
	/**
	 * 通过ID删除title_product
	 * @author Administrator
	 * @param id
	 * @throws Exception
	 */
	public void deleteProprietaryProductById(long tp_id, long tp_pc_id, long tp_title_id) throws Exception
	{
		try
		{
			String where = " where 1=1";
			if(tp_id > 0)
			{
				where += " and tp_id = " + tp_id;
			}
			if(tp_pc_id > 0)
			{
				where += " and tp_pc_id = " + tp_pc_id;
			}
			if(tp_title_id > 0)
			{
				where += " and tp_title_id = " + tp_title_id;
			}
			dbUtilAutoTran.delete(where, ConfigBean.getStringValue("title_product"));
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.deleteProprietaryProductById(long tp_id, long tp_pc_id, long tp_title_id):"+e);
		}
	}
	
	/**
	 * 添加title与商品关系
	 * @author Administrator
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addProprietaryProduct(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("title_product"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.addProprietaryProduct(DBRow row):"+e);
		}
	}
	
	/**
	 * 通过ID删除title_product_catalog
	 * @author Administrator
	 * @param id
	 * @throws Exception
	 */
	public void deleteProprietaryProductCatagoryById(long tpc_id, long tpc_title_id, long tpc_product_catalog_id) throws Exception
	{
		try
		{
			String where = " where 1=1";
			if(tpc_id > 0)
			{
				where += " and tpc_id = " + tpc_id;
			}
			if(tpc_title_id > 0)
			{
				where += " and tpc_title_id = " + tpc_title_id;
			}
			if(tpc_product_catalog_id > 0)
			{
				where += " and tpc_product_catalog_id = " + tpc_product_catalog_id;
			}
			dbUtilAutoTran.delete(where, ConfigBean.getStringValue("title_product_catalog"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProprietaryMgrZyj.deleteProprietaryProductCatagoryById(long tpc_id, long tpc_title_id, long tpc_product_catalog_id):"+e);
		}
	}
	
	/**
	 * 通过ID删除title_product_line
	 * @author Administrator
	 * @param id
	 * @throws Exception
	 */
	public void deleteProprietaryProductLineById(long tpl_id, long tpl_title_id, long tpl_product_line_id) throws Exception
	{
		try
		{
			String where = " where 1=1";
			if(tpl_id > 0)
			{
				where += " and tpl_id = " + tpl_id;
			}
			if(tpl_title_id > 0)
			{
				where += " and tpl_title_id = " + tpl_title_id;
			}
			if(tpl_product_line_id > 0)
			{
				where += " and tpl_product_line_id = " + tpl_product_line_id;
			}
			dbUtilAutoTran.delete(where, ConfigBean.getStringValue("title_product_line"));
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.deleteProprietaryProductLineById(long tpl_id, long tpl_title_id, long tpl_product_line_id):"+e);
		}
	}
	
	/**
	 * 添加title与商品关系
	 * @author Administrator
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addProprietaryProductLine(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("title_product_line"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.addProprietaryProductLine(DBRow row):"+e);
		}
	}
	
	/**
	 * 添加title与商品关系
	 * @author Administrator
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addProprietaryProductCatagory(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("title_product_catalog"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.addProprietaryProductCatagory(DBRow row):"+e);
		}
	}
	
	/**
	 * 根据titleID查询用户
	 * @author Administrator
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAdminsByTitleId(long adid, long title_id, int begin, int length, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT a.*");
			if(title_id > 0)
			{
				sql.append(", ta.title_admin_sort");
			}
			sql.append("  FROM admin a ");
			if(title_id > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_adid = a.adid");
				sql.append(" AND ta.title_admin_title_id = "+title_id);
			}
			sql.append(" WHERE 1=1");
			if(adid > 0)
			{
				sql.append(" AND a.adid = "+adid);
			}
			
			sql.append(" GROUP BY a.adid");
			sql.append(" ORDER BY a.adid");
			if(begin >= 0)
			{
				if(length > 0)
				{
					sql.append(" LIMIT "+begin+","+length);
				}
			}
			else
			{
				if(length > 0)
				{
					sql.append(" LIMIT "+length);
				}
			}
			
			if(null != pc)
			{
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findAdminsByTitleId(long adid, long title_id, int begin, int length, PageCtrl pc):"+e);
		}
	}
	
	/**
	 * 根据titleID查询产品线
	 * @author Administrator
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProductLinesByTitleId(long adid, Long[] pc_line_ids, Long[] title_ids, int begin, int length, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pld.* FROM product_line_define pld");
			sql.append(" LEFT JOIN title_product_line tpl ON pld.id = tpl.tpl_product_line_id");
			
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = tpl.tpl_title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			sql.append(" WHERE 1=1");
			if(null != pc_line_ids && pc_line_ids.length > 0)
			{
				sql.append(" AND (pld.id = " + pc_line_ids[0]);
				for (int i = 1; i < pc_line_ids.length; i++) 
				{
					sql.append(" OR pld.id = " + pc_line_ids[i]);
				}
				sql.append(" )");
			}
			if(null != title_ids && title_ids.length > 0)
			{
				sql.append(" AND (tpl.tpl_title_id = " + title_ids[0]);
				for (int i = 1; i < title_ids.length; i++) 
				{
					sql.append(" OR tpl.tpl_title_id = " + title_ids[i]);
				}
				sql.append(" )");
			}
			
			sql.append(" GROUP BY pld.id");
			sql.append(" ORDER BY pld.id");
			if(begin >= 0)
			{
				if(length > 0)
				{
					sql.append(" LIMIT "+begin+","+length);
				}
			}
			else
			{
				if(length > 0)
				{
					sql.append(" LIMIT "+length);
				}
			}
			
			if(null != pc)
			{
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProductLinesByTitleId(long adid, long pc_line_id, long title_id, int begin, int length, PageCtrl pc):"+e);
		}
	}
	
	/**
	 * 根据titleID查询产品线
	 * @author Administrator
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public int findProdcutLinesCountByTitleId(long adid, long pc_line_id, long title_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT COUNT(*) as count FROM product_line_define pld");
			if(title_id > 0)
			{
				sql.append(" JOIN title_product_line tpl ON pld.id = tpl.tpl_product_line_id");
				sql.append(" AND tpl.tpl_title_id = "+title_id);
			}
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = tpl.tpl_title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			sql.append(" WHERE 1=1");
			if(pc_line_id >0)
			{
				sql.append(" AND pld.id = " + pc_line_id);
			}
			sql.append(" GROUP BY pld.id");
			sql.append(" ORDER BY pld.id");
			
			return dbUtilAutoTran.selectSingle(sql.toString()).get("count", 0);
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProdcutLinesCountByTitleId(long adid, long pc_line_id, long title_id):"+e);
		}
	}
	
	/**
	 * 根据titleID查询产品分类
	 * @author Administrator
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProductCatagorysByTitleId(long adid, Long[] pc_line_id, Long[] category_parent_id, Long[] pc_cata_id, Long[] title_id, int begin, int length, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pc.* FROM product_catalog pc ");
			sql.append(" JOIN pc_child_list pcl ON pcl.pc_id = pc.id");
			sql.append(" LEFT JOIN title_product_catalog tpc ON tpc.tpc_product_catalog_id = pc.id");
			
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = tpc.tpc_title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			sql.append(" WHERE 1=1");
			if(null != category_parent_id && category_parent_id.length > 0)
			{
				if(-1 != category_parent_id[0])
				{
					sql.append(" AND (pcl.search_rootid = " + category_parent_id[0]);
				}
				else
				{
					sql.append(" AND (pcl.search_rootid = 0");
				}
				for (int i = 1; i < category_parent_id.length; i++) 
				{
					sql.append(" OR pcl.search_rootid = " + category_parent_id[i]);
				}
				sql.append(" )");
			}
			if(null != pc_line_id && pc_line_id.length > 0)
			{
				sql.append(" AND (pc.product_line_id = " + pc_line_id[0]);
				for (int i = 1; i < pc_line_id.length; i++) 
				{
					sql.append(" OR pc.product_line_id = " + pc_line_id[i]);
				}
				sql.append(" )");
			}
			if(null != title_id && title_id.length > 0)
			{
				sql.append(" AND (tpc.tpc_title_id = " + title_id[0]);
				for (int i = 1; i < title_id.length; i++) 
				{
					sql.append(" OR tpc.tpc_title_id = " + title_id[i]);
				}
				sql.append(" )");
			}
			sql.append(" GROUP BY pc.id");
			sql.append(" ORDER BY pc.id");
			if(begin >= 0)
			{
				if(length > 0)
				{
					sql.append(" LIMIT "+begin+","+length);
				}
			}
			else
			{
				if(length > 0)
				{
					sql.append(" LIMIT "+length);
				}
			}
			
			
			if(null != pc)
			{
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProductCatagorysByTitleId(long adid, long pc_line_id, long category_parent_id, long pc_cata_id, long title_id, int begin, int length, PageCtrl pc):"+e);
		}
	}
	
	public DBRow[] findProductCatagoryParentsByTitleId(long adid, Long[] pc_line_id, Long[] category_parent_id, Long[] pc_cata_id, Long[] title_id, int begin, int length, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select pc.* from product_catalog pc ");
			sql.append(" LEFT JOIN title_product_catalog tpc ON tpc.tpc_product_catalog_id = pc.id");
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = tpc.tpc_title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			sql.append(" WHERE 1=1");
			if(null != category_parent_id && category_parent_id.length > 0)
			{
				sql.append(" AND (pc.parentid = " + category_parent_id[0]);
				for (int i = 1; i < category_parent_id.length; i++)
				{
					sql.append(" OR pc.parentid = " + category_parent_id[0]);
				}
				sql.append(" )");
			}
			if(null != pc_line_id && pc_line_id.length > 0)
			{
				sql.append(" AND (pc.product_line_id = " + pc_line_id[0]);
				for (int i = 1; i < pc_line_id.length; i++)
				{
					sql.append(" OR pc.product_line_id = " + pc_line_id[i]);
				}
				sql.append(" )");
			}
			if(null != title_id && title_id.length > 0)
			{
				sql.append(" AND (tpc.tpc_title_id = "+title_id[0]);
				for (int i = 1; i < title_id.length; i++)
				{
					sql.append(" OR tpc.tpc_title_id = "+title_id[i]);	
				}
				sql.append(" )");
			}
			sql.append(" GROUP BY pc.id");
			sql.append(" order by pc.sort asc");
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProprietaryMgrZyj.findProductCatagoryParentsByTitleId(long adid, long[] pc_line_id, long[] category_parent_id, long[] pc_cata_id, long[] title_id, int begin, int length, PageCtrl pc):"+e);
		}
		
	}
	
	/**
	 * 根据titleID查询产品分类
	 * @author Administrator
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public int findProductCatagorysCountByTitleId(long adid, long pc_cata_id, long title_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT COUNT(*) as count FROM product_catalog pc ");
			sql.append(" LEFT JOIN title_product_catalog tpc ON pc.id = tpc.tpc_product_catalog_id");
			
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = tpc.tpc_title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			sql.append(" WHERE 1=1");
			if(pc_cata_id >0)
			{
				sql.append(" AND pc.id = " + pc_cata_id);
			}
			if(title_id > 0)
			{
				sql.append(" AND tpc.tpc_title_id = "+title_id);
			}
			sql.append(" GROUP BY pc.id");
			sql.append(" ORDER BY pc.id");
			
			return dbUtilAutoTran.selectSingle(sql.toString()).get("count", 0);
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProductCatagorysCountByTitleId(long adid, long pc_cata_id, long title_id):"+e);
		}
	}
	
	/**
	 * 根据titleID查询产品分类
	 * @author Administrator
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProductsByTitleId(long adid, long pc_id, String pc_name, long title_id, int begin, int length, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT p.pc_id, p.p_name FROM product p ");
			sql.append(" LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			sql.append(" WHERE 1=1");
			if(pc_id >0)
			{
				sql.append(" AND p.pc_id = " + pc_id);
			}
			if(null != pc_name && !"".equals(pc_name))
			{
				sql.append(" AND p.p_name like '%"+pc_name+"%'");
			}
			if(title_id > 0)
			{
				sql.append(" AND tp.tp_title_id = "+title_id);
			}
			sql.append(" GROUP BY p.pc_id");
			sql.append(" ORDER BY p.pc_id");
			if(begin >= 0)
			{
				if(length > 0)
				{
					sql.append(" LIMIT "+begin+","+length);
				}
			}
			else
			{
				if(length > 0)
				{
					sql.append(" LIMIT "+length);
				}
			}
			if(null != pc)
			{
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProductsByTitleId(long adid, long pc_id, String pc_name, long title_id, int begin, int length, PageCtrl pc):"+e);
		}
	}
	
	/**
	 * 根据titleID查询产品分类
	 * @author Administrator
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public int findProductsCountByTitleId(long adid, long pc_id, long title_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT COUNT(*) as count FROM product p ");
			sql.append(" LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
			
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			sql.append(" WHERE 1=1");
			if(pc_id >0)
			{
				sql.append(" AND p.pc_id = " + pc_id);
			}
			if(title_id > 0)
			{
				sql.append(" AND tp.tp_title_id = "+title_id);
			}
			sql.append(" GROUP BY p.pc_id");
			sql.append(" ORDER BY p.pc_id");
			
			return dbUtilAutoTran.selectSingle(sql.toString()).get("count", 0);
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProductsCountByTitleId(long adid, long pc_id, long title_id):"+e);
		}
	}
	
	/**
	 * 根据用户、产品线、商品分类、产品ID查询title
	 * @param adid
	 * @param product_line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProprietaryAllByAdminProductNameLineCatagory(long adid, Long[] product_line_ids, Long[] product_catagory_ids, String pc_name, PageCtrl pc)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM title t");
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = t.title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			if(null != product_line_ids && product_line_ids.length > 0)
			{
				sql.append(" JOIN title_product_line tpl ON tpl.tpl_title_id = t.title_id");
				sql.append(" AND (tpl.tpl_product_line_id = " + product_line_ids[0]);
				for (int i = 1; i < product_line_ids.length; i++) 
				{
					sql.append(" OR tpl.tpl_product_line_id = " + product_line_ids[i]);
				}
				sql.append(" )");
			}
			if(null != product_catagory_ids && product_catagory_ids.length > 0)
			{
				sql.append(" JOIN title_product_catalog tpc ON tpc.tpc_title_id = t.title_id");
				sql.append(" JOIN pc_child_list pcl ON pcl.pc_id = tpc.tpc_product_catalog_id");
				if(-1 != product_catagory_ids[0])
				{
					sql.append(" AND (pcl.search_rootid = " + product_catagory_ids[0]);
				}
				else
				{
					sql.append(" AND (pcl.search_rootid = 0");
				}
				for (int i = 1; i < product_catagory_ids.length; i++)
				{
					sql.append(" OR pcl.search_rootid = " + product_catagory_ids[i]);
				}
				sql.append(" )");
			}
			if(null != pc_name && !"".equals(pc_name) && !"".equals(pc_name.trim()))
			{
				sql.append(" JOIN title_product tp ON tp.tp_title_id = t.title_id ");
				sql.append(" JOIN product p ON p.pc_id = tp.tp_pc_id AND p.p_name like '%"+pc_name+"%'");
				sql.append(" GROUP BY t.title_id");
				sql.append(" ORDER BY IFNULL(tp.tp_title_id,999999), t.title_id");
			}
			else
			{
				sql.append(" GROUP BY t.title_id");
				sql.append(" ORDER BY t.title_id desc");
			}
			if(null == pc)
			{
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryAllByAdminProductNameLineCatagory(long adid, long product_line_id, long product_catagory_id, String pc_name, PageCtrl pc):"+e);
		}
	}
	
	public DBRow[] filterProductByPcLineCategoryTitleId(Long[] catalog_id, Long[] product_line_id, int union_flag, int product_file_type, int product_upload_status, Long[] title_id, long adid, int equal_or_not, long product_id, int active, PageCtrl pc) throws Exception {
		return filterProductByPcLineCategoryTitleId(catalog_id, product_line_id, union_flag, product_file_type, product_upload_status, title_id, adid, equal_or_not, product_id, pc, active);
	}
	
	/**
	 * 查询商品
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] filterProductByPcLineCategoryTitleId(Long[] catalog_id, Long[] product_line_id, int union_flag, int product_file_type, int product_upload_status, Long[] title_id, long adid, int equal_or_not, long product_id, PageCtrl pc, long active) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			//商品某一类型的文件是否上传了
			if(0 != product_file_type)
			{
				//-- 商品照片某个类型未全(全)的,AND m.count_product IS NUL不加就不管是否上传了
				sb.append(" select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc");
				sb.append(" from (select s.* from ");
				sb.append(" 		( select p.pc_id , p.alive, p.catalog_id, p.heigth, p.length,");
				sb.append(" 						 p.orignal_pc_id, p.p_name, p.union_flag, p.unit_name,");
				sb.append(" 						 p.unit_price, p.volume, p.weight, p.width,p.sn_size");
				sb.append(" 						,p.length_uom,p.weight_uom,p.price_uom");
				sb.append(" 			from product p ");
				sb.append(" 			LEFT JOIN (select pc_id , count(pc_id) as count_product from product_file ");
				sb.append(" 							where file_with_type = ").append(FileWithTypeKey.PRODUCT_SELF_FILE);
				sb.append("								and product_file_type = ").append(product_file_type);
				sb.append(" 							GROUP BY pc_id, product_file_type)m");
				sb.append(" 			ON m.pc_id = p.pc_id ");
				sb.append(" 			WHERE p.orignal_pc_id = 0");
				
				if(active > -1){
					sb.append(" AND p.alive = " + active);
				}
				
				if(union_flag > -1)
				{
					sb.append(" 		AND p.union_flag = ").append(union_flag);
				}
				
				if(1 == product_upload_status)
				{
					sb.append("				AND m.count_product IS NULL");
				}
				else if(2 == product_upload_status)
				{
					sb.append("				AND m.count_product IS NOT NULL");
				}
				if(equal_or_not == YesOrNotKey.NO)
				{
					sb.append("				AND p.pc_id !="+product_id);
				}
				
				sb.append("				)s");
				
				sb.append(" 	LEFT JOIN product_catalog c ON s.catalog_id = c.id");
				sb.append("		LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
				
				sb.append(" LEFT JOIN title_product tp ON s.pc_id = tp.tp_pc_id");
				if(adid > 0)
				{
					sb.append("		JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
				}
//				sb.append("		JOIN title_product_catalog tpc ON c.id = tpc.tpc_product_catalog_id ");
//				sb.append("		JOIN title_product_line tpl ON tpl.tpl_product_line_id = l.id ");
				if(null != catalog_id && catalog_id.length > 0)
				{
					sb.append("     LEFT JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				sb.append(" 	WHERE 1=1 ");
				if(null != product_line_id && product_line_id.length > 0)
				{
					sb.append(" AND ( l.id = " + product_line_id[0]);
					for (int i = 1; i < product_line_id.length; i++) 
					{
						sb.append(" OR l.id = " + product_line_id[i]);
					}
					sb.append(" )");
				}
				if(null != title_id && title_id.length > 0)
				{
					sb.append(" AND ( tp.tp_title_id = " + title_id[0]);
					for (int i = 1; i < title_id.length; i++) 
					{
						sb.append(" OR tp.tp_title_id = " + title_id[i]);
					}
					sb.append(" )");
				}
				if(null != catalog_id && catalog_id.length > 0)
				{
					if(-1 != catalog_id[0])
					{
						sb.append(" AND ( pcl.search_rootid = " + catalog_id[0]);
					}
					else
					{
						sb.append(" AND ( pcl.search_rootid = 0");
					}
					for (int i = 1; i < catalog_id.length; i++) 
					{
						sb.append(" OR pcl.search_rootid = " + catalog_id[i]);
					}
					sb.append(" )");
				}
				sb.append("  GROUP BY s.pc_id");
				sb.append(" 	) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append("	LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
				sb.append(" order by z.pc_id desc");
			}
			//商品未完成上传文件
			else if(0 == product_file_type && 1 == product_upload_status)
			{
				sb.append(" select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc from ");
				sb.append("  (select s.* from ");
				sb.append("  	(select w.pc_id , w.alive, w.catalog_id, w.heigth, w.length,");
				sb.append("  				w.orignal_pc_id, w.p_name, w.union_flag, w.unit_name,");
				sb.append("  				w.unit_price, w.volume, w.weight, w.width,w.sn_size");
				sb.append(" 				,w.length_uom,w.weight_uom,w.price_uom");
				sb.append("  	 from product w left JOIN");
				sb.append("  		(select p.pc_id , n.count_product from product p LEFT JOIN");
				sb.append("  			(select count(*) as count_product,pc_id  ");
				sb.append("  					from (select * from product_file where file_with_type = ").append(FileWithTypeKey.PRODUCT_SELF_FILE);
				sb.append(						" GROUP BY pc_id, product_file_type) m GROUP BY pc_id)n");
				sb.append("  			on n.pc_id = p.pc_id ");
				sb.append("  			where p.orignal_pc_id = 0");
				sb.append("  			and n.count_product = 5 ");
				sb.append("  			and p.orignal_pc_id = 0)q");
				sb.append("  	ON w.pc_id = q.pc_id ");
				sb.append("  	where w.orignal_pc_id = 0 ");
				
				if(active > -1){
					sb.append(" AND w.alive = " + active);
				}
				
				if(union_flag > -1)
				{
					sb.append("		AND w.union_flag = ").append(union_flag);
				}
				if(equal_or_not == YesOrNotKey.NO)
				{
					sb.append("				AND w.pc_id !="+product_id);
				}
				sb.append("  	AND (q.count_product is null or q.count_product < 5) )s");
				sb.append(" 	LEFT JOIN product_catalog c ON s.catalog_id = c.id");
				sb.append("		LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
				
				sb.append(" LEFT	JOIN title_product tp ON s.pc_id = tp.tp_pc_id");
				if(adid > 0)
				{
					sb.append(" JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
				}
//				sb.append("		JOIN title_product_catalog tpc ON c.id = tpc.tpc_product_catalog_id ");
//				sb.append("		JOIN title_product_line tpl ON tpl.tpl_product_line_id = l.id ");
				
				if(null != catalog_id && catalog_id.length > 0)
				{
					sb.append("     LEFT JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				sb.append(" 	WHERE 1=1 ");
				if(null != product_line_id && product_line_id.length > 0)
				{
					sb.append(" AND ( l.id = " + product_line_id[0]);
					for (int i = 1; i < product_line_id.length; i++) 
					{
						sb.append(" OR l.id = " + product_line_id[i]);
					}
					sb.append(" )");
				}
				if(null != title_id && title_id.length > 0)
				{
					sb.append(" AND ( tp.tp_title_id = " + title_id[0]);
					for (int i = 1; i < title_id.length; i++) 
					{
						sb.append(" OR tp.tp_title_id = " + title_id[i]);
					}
					sb.append(" )");
				}
				if(null != catalog_id && catalog_id.length > 0)
				{
					if(-1 != catalog_id[0])
					{
						sb.append(" AND ( pcl.search_rootid = " + catalog_id[0]);
					}
					else
					{
						sb.append(" AND ( pcl.search_rootid = 0");
					}
					for (int i = 1; i < catalog_id.length; i++) 
					{
						sb.append(" OR pcl.search_rootid = " + catalog_id[i]);
					}
					sb.append(" )");
				}
				sb.append("  GROUP BY s.pc_id ORDER BY s.pc_id");
				sb.append("	 ) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
				sb.append(" order by z.pc_id desc");
			}
			//商品已完成上传文件
			else if(0 == product_file_type && 2 == product_upload_status)
			{
				sb.append("select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc ");
				sb.append("   from(select q.* from ");
				sb.append(" 		(select p.pc_id ,p.alive, p.catalog_id, p.heigth, p.length,");
				sb.append(" 				p.orignal_pc_id, p.p_name, p.union_flag, p.unit_name,");
				sb.append(" 				p.unit_price, p.volume, p.weight, p.width,p.sn_size");
				sb.append(" 				,p.length_uom,p.weight_uom,p.price_uom");
				sb.append(" 		   from product p LEFT JOIN");
				sb.append(" 			(select count(*) as count_product,pc_id  ");
				sb.append(" 					from (select * from product_file where file_with_type = ").append(FileWithTypeKey.PRODUCT_SELF_FILE);
				sb.append("						GROUP BY pc_id, product_file_type) m GROUP BY pc_id)n");
				sb.append(" 			on n.pc_id = p.pc_id ");
				sb.append(" 			where p.orignal_pc_id = 0");
				sb.append(" 			and n.count_product = 5 ");
				
				if(active > -1){
					sb.append(" AND p.alive = " + active);
				}
				if(union_flag > -1)
				{
					sb.append(" 		AND p.union_flag = ").append(union_flag);
				}
				if(equal_or_not == YesOrNotKey.NO)
				{
					sb.append("				AND p.pc_id !="+product_id);
				}
				sb.append(" 			and p.orignal_pc_id = 0)q");
				sb.append(" 	LEFT JOIN product_catalog c ON q.catalog_id = c.id");
				sb.append("		LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
				
				sb.append(" LEFT JOIN title_product tp ON q.pc_id = tp.tp_pc_id");
				if(adid > 0)
				{
					sb.append(" JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
				}
//				sb.append("		JOIN title_product_catalog tpc ON c.id = tpc.tpc_product_catalog_id ");
//				sb.append("		JOIN title_product_line tpl ON tpl.tpl_product_line_id = l.id ");
				if(null != catalog_id && catalog_id.length > 0)
				{
					sb.append("     LEFT JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				sb.append(" 	WHERE 1=1 ");
				if(null != product_line_id && product_line_id.length > 0)
				{
					sb.append(" AND ( l.id = " + product_line_id[0]);
					for (int i = 1; i < product_line_id.length; i++) 
					{
						sb.append(" OR l.id = " + product_line_id[i]);
					}
					sb.append(" )");
				}
				if(null != title_id && title_id.length > 0)
				{
					sb.append(" AND ( tp.tp_title_id = " + title_id[0]);
					for (int i = 1; i < title_id.length; i++) 
					{
						sb.append(" OR tp.tp_title_id = " + title_id[i]);
					}
					sb.append(" )");
				}
				if(null != catalog_id && catalog_id.length > 0)
				{
					if(-1 != catalog_id[0])
					{
						sb.append(" AND ( pcl.search_rootid = " + catalog_id[0]);
					}
					else
					{
						sb.append(" AND ( pcl.search_rootid = 0");
					}
					for (int i = 1; i < catalog_id.length; i++) 
					{
						sb.append(" OR pcl.search_rootid = " + catalog_id[i]);
					}
					sb.append(" )");
				}
				sb.append("  GROUP BY q.pc_id ORDER BY q.pc_id");
				sb.append("		 ) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
				sb.append(" order by z.pc_id  desc");
				
			}
			//不管商品是否上传了文件
			else
			{
				//-- 不管商品是否上传了文件
				sb.append("select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc from ");
				sb.append("	(select p.pc_id , p.alive, p.catalog_id, p.heigth, p.length,");
				sb.append(" p.orignal_pc_id, p.p_name, p.union_flag, p.unit_name,p.unit_price, p.volume, p.weight, p.width,p.sn_size ,p.length_uom,p.weight_uom,p.price_uom,c.title");
				sb.append(" FROM product p LEFT JOIN product_catalog c ON p.catalog_id = c.id LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
				
				if(null != catalog_id && catalog_id.length > 0) {
					
					sb.append(" LEFT JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				
				sb.append(" LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
				if(adid > 0)
				{
					sb.append(" JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
				}
//				sb.append("  JOIN title_product_catalog tpc ON c.id = tpc.tpc_product_catalog_id ");
//				sb.append("  JOIN title_product_line tpl ON tpl.tpl_product_line_id = l.id ");
				
				sb.append("	WHERE 1=1 ");
				sb.append("	AND p.orignal_pc_id = 0 ");
				
				if(active > -1){
					sb.append(" AND p.alive = " + active);
				}
				
				if(null != product_line_id && product_line_id.length > 0)
				{
					sb.append(" AND ( l.id = " + product_line_id[0]);
					for (int i = 1; i < product_line_id.length; i++) 
					{
						sb.append(" OR l.id = " + product_line_id[i]);
					}
					sb.append(" )");
				}
				if(null != title_id && title_id.length > 0)
				{
					sb.append(" AND ( tp.tp_title_id = " + title_id[0]);
					for (int i = 1; i < title_id.length; i++) 
					{
						sb.append(" OR tp.tp_title_id = " + title_id[i]);
					}
					sb.append(" )");
				}
				if(null != catalog_id && catalog_id.length > 0)
				{
					if(-1 != catalog_id[0])
					{
						sb.append(" AND ( pcl.search_rootid = " + catalog_id[0]);
					}
					else
					{
						sb.append(" AND ( pcl.search_rootid = 0");
					}
					for (int i = 1; i < catalog_id.length; i++) 
					{
						sb.append(" OR pcl.search_rootid = " + catalog_id[i]);
					}
					sb.append(" )");
				}
				if(union_flag > -1)
				{
					sb.append("  AND p.union_flag = ").append(union_flag);
				}
				if(equal_or_not == YesOrNotKey.NO)
				{
					sb.append("				AND p.pc_id !="+product_id);
				}
				sb.append("  GROUP BY p.pc_id ORDER BY p.pc_id");
				sb.append("	 ) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
				sb.append(" order by z.pc_id desc");
				
			}
			return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.filterProductByPcLineCategoryTitleId(long catalog_id, long product_line_id, int union_flag, int product_file_type, int product_upload_status, long title_id, long adid, PageCtrl pc, long active):"+e);
		}
	}
	
	public DBRow[] findDetailProductLikeSearch(String key, Long[] title_id, long adid, int union_flag, int equal_or_not, long product_id,int active, PageCtrl pc) throws Exception {
		return findDetailProductLikeSearch(key, title_id, adid, union_flag, equal_or_not, product_id, pc, active);
	}
	
	/**
	 * 模糊查询商品明细
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findDetailProductLikeSearch(String key,Long[] title_id, long adid, int union_flag, int equal_or_not, long product_id,PageCtrl pc, long active)throws Exception {
		
		try {
			
			StringBuffer sql = new StringBuffer();
			sql.append(" select p.pc_id, p.alive, p.catalog_id, p.heigth, p.length, p.orignal_pc_id, pcode_main.p_code as p_code, p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width,p.sn_size ,pcode_amazon.p_code AS p_code2, pcode_upc.p_code AS upc, p.length_uom,p.weight_uom,p.price_uom");
			sql.append(" from product as p ");
			sql.append(" JOIN product_code AS pcode_main ON p.pc_id = pcode_main.pc_id AND pcode_main.code_type= ").append(CodeTypeKey.Main);
			sql.append(" LEFT JOIN product_code AS pcode_amazon ON p.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = ").append(CodeTypeKey.Amazon);
			sql.append(" LEFT JOIN product_code AS pcode_upc ON p.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = ").append(CodeTypeKey.UPC);
			
			sql.append(" where ( p_name like '%"+key+"%' or pcode_main.p_code like '%"+key+"%' ) ");
			
			if(null != title_id && title_id.length > 0) {
				
				StringBuffer inSql = new StringBuffer();
				
				for(Long one : title_id ){
					
					inSql.append(","+one);
				}
				
				sql.append(" and exists ( select * from title_product tp where tp.tp_pc_id = p.pc_id and ( tp.tp_title_id in ( "+inSql.substring(1)+" ) ) )");
			}
			
			/*if(adid > 0) {
				
				sql.append(" and exists ( select * from title_admin ta left join title_product tp on ta.title_admin_title_id = tp.tp_title_id where ta.title_admin_adid = " + adid +" and tp.tp_pc_id = p.pc_id )");
			}*/
			
			if(union_flag > -1 ){
				
				sql.append(" AND p.union_flag = ").append(union_flag);
			}
			
			if(equal_or_not == YesOrNotKey.NO) {
				
				sql.append(" AND p.pc_id !="+product_id);
			}
			
			if(active > -1){
				
				sql.append(" AND p.alive = " + active);
			}
			
			sql.append(" order by p.pc_id desc");
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			
		} catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findDetailProductLikeSearch(String key,long title_id, long adid,PageCtrl pc, long active) error:"+e);
		}
	}
	
	/**
	 * 模糊查询商品明细
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findDetailProductLikeSearch(String key,Long[] title_id, long adid, int union_flag, int equal_or_not, long product_id,PageCtrl pc, long active, AdminLoginBean adminLoggerBean)throws Exception {
		
		try {
			
			StringBuffer sql = new StringBuffer();
			sql.append(" select p.pc_id, p.alive, p.catalog_id, p.heigth, p.length, p.orignal_pc_id, pcode_main.p_code as p_code, p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width,p.sn_size ,pcode_amazon.p_code AS p_code2, pcode_upc.p_code AS upc, p.length_uom,p.weight_uom,p.price_uom");
			sql.append(" from product as p ");
			sql.append(" JOIN product_code AS pcode_main ON p.pc_id = pcode_main.pc_id AND pcode_main.code_type= ").append(CodeTypeKey.Main);
			sql.append(" LEFT JOIN product_code AS pcode_amazon ON p.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = ").append(CodeTypeKey.Amazon);
			sql.append(" LEFT JOIN product_code AS pcode_upc ON p.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = ").append(CodeTypeKey.UPC);
			
			sql.append(" where ( p_name like '%"+key+"%' or pcode_main.p_code like '%"+key+"%' ) ");
			
			if(null != title_id && title_id.length > 0) {
				
				StringBuffer inSql = new StringBuffer();
				
				for(Long one : title_id ){
					
					inSql.append(","+one);
				}
				
				sql.append(" and exists ( select * from title_product tp where tp.tp_pc_id = p.pc_id and ( tp.tp_title_id in ( "+inSql.substring(1)+" ) ) )");
			}
			
			//0:正常登录  1：customer 2：carrier
			if(adminLoggerBean.getCorporationType() == 1){
				
				sql.append(" and exists ( select * from title_product tp where tp.tp_pc_id = p.pc_id and tp.tp_customer_id = " + adminLoggerBean.getCorporationId()  + ") ");
			}
			
			if(union_flag > -1 ){
				
				sql.append(" AND p.union_flag = ").append(union_flag);
			}
			
			if(equal_or_not == YesOrNotKey.NO) {
				
				sql.append(" AND p.pc_id !="+product_id);
			}
			
			if(active > -1){
				
				sql.append(" AND p.alive = " + active);
			}
			
			sql.append(" order by p.pc_id desc");
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			
		} catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findDetailProductLikeSearch(String key,long title_id, long adid,PageCtrl pc, long active) error:"+e);
		}
	}
	
	public DBRow[] findProductInfosProductLineProductCodeByLineId(Long[] pc_line_id, Long[] title_id, long adid, PageCtrl pc) throws Exception {
		return findProductInfosProductLineProductCodeByLineId(pc_line_id, title_id, adid, pc, -1);
	}
	
	/**
	 * 将商品、商品线、条码关联，根据产品线查询
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProductInfosProductLineProductCodeByLineId(Long[] pc_line_id, Long[] title_id, long adid, PageCtrl pc, long active) throws Exception
	{
		try 
		{
			StringBuffer sb = new StringBuffer();
			
			sb.append("select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc from ");
			sb.append("(select s.* from ");
			sb.append("	(select w.pc_id , w.alive, w.catalog_id, w.heigth, w.length,");
			sb.append("				w.orignal_pc_id, w.p_name, w.union_flag, w.unit_name,");
			sb.append("				w.unit_price, w.volume, w.weight, w.width, w.sn_size");
			sb.append("				 ,w.length_uom,w.weight_uom,w.price_uom");
			sb.append("	 from product w left JOIN");
			sb.append("		(select p.pc_id , n.count_product from product p LEFT JOIN");
			sb.append("			(select count(*) as count_product,pc_id  ");
			sb.append("					from (select * from product_file where file_with_type = 31 GROUP BY pc_id, product_file_type) m GROUP BY pc_id)n");
			sb.append("			on n.pc_id = p.pc_id ");
			sb.append("			where p.orignal_pc_id = 0");
			sb.append("			and n.count_product = 5 ");
			sb.append("			and p.orignal_pc_id = 0)q");
			sb.append("	ON w.pc_id = q.pc_id where w.orignal_pc_id = 0 ").append(active > -1 ? "AND w.alive = " + active : "").append(" and (q.count_product is null or q.count_product < 5) )s");
			
			sb.append("	 LEFT JOIN product_catalog c ON s.catalog_id = c.id");
			sb.append("  LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
			
			sb.append(" LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
			
			if(adid > 0)
			{
				sb.append(" JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
			}
//			sb.append("  JOIN title_product_line tpl ON tpl.tpl_product_line_id = l.id ");
			sb.append(" WHERE 1=1 ");
			
			if(null != pc_line_id && pc_line_id.length > 0)
			{
				sb.append(" AND (l.id = " + pc_line_id[0]);
				for (int i = 1; i < pc_line_id.length; i++) 
				{
					sb.append(" OR l.id = " + pc_line_id[i]);
				}
				sb.append(" )");
			}
			if(null != title_id && title_id.length > 0)
			{
				sb.append(" AND (tp.tp_title_id = "+title_id[0]);
				for (int i = 1; i < title_id.length; i++)
				{
					sb.append(" OR tp.tp_title_id = "+title_id[i]);	
				}
				sb.append(" )");
			}
			sb.append("  GROUP BY q.pc_id ORDER BY q.pc_id");
			sb.append("		 ) z");
			sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
			sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
			sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
			sb.append(" order by z.pc_id desc") ;
			
			return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findProductInfosProductLineProductCodeByLineId(long pc_line_id, long title_id, long adid, PageCtrl pc, long active):"+e);
		}
	}
	
	public DBRow[] findAllProductsByTitleAdid(Long[] title_id, long adid, PageCtrl pc) throws Exception {
		return findAllProductsByTitleAdid(title_id, adid, pc, -1);
	}
	
	public DBRow[] findAllProductsByTitleAdid(Long[] title_id, long adid, PageCtrl pc, long active) throws Exception {
		
		try { 
			
			StringBuffer sql = new StringBuffer();
			sql.append("select p.pc_id, p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,");
			sql.append(" pcode_main.p_code as p_code,pcode_amazon.p_code as p_code2,pcode_upc.p_code as upc,");
			sql.append(" p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width,p.sn_size ");
			sql.append(" ,length_uom,weight_uom,price_uom,p.freight_class,p.nmfc_code,p.sku ,p.lot_no ");
			sql.append(" from product as p ");
			sql.append(" LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
			
			/*if(adid > 0)
			{
				sql.append("  JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
			}*/
			sql.append(" join product_code as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type ="+CodeTypeKey.Main);
			sql.append(" left join product_code as pcode_amazon on p.pc_id = pcode_amazon.pc_id and pcode_amazon.code_type ="+CodeTypeKey.Amazon);
			sql.append(" left join product_code as pcode_upc on p.pc_id = pcode_upc.pc_id and pcode_upc.code_type ="+CodeTypeKey.UPC);
			sql.append(" where orignal_pc_id = 0");
			
			if(active > -1){
				sql.append(" AND p.alive = " + active);
			}	
			
			if(null != title_id && title_id.length > 0)
			{
				sql.append(" AND (tp.tp_title_id = "+title_id[0]);
				for (int i = 1; i < title_id.length; i++)
				{
					sql.append(" OR tp.tp_title_id = "+title_id[i]);	
				}
				sql.append(" )");
			}
			sql.append(" GROUP BY p.pc_id");
			sql.append(" order by p.pc_id desc ,catalog_id desc,p_name asc");
			
			if ( pc!=null )
			{
				return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
			}
			else
			{
				return (dbUtilAutoTran.selectMutliple(sql.toString()));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProprietaryMgrZyj.findAllProductsByTitleAdid(long title_id, long adid, PageCtrl pc, long active) error:" + e);
		}
	}
	
	/**
	 * 查询商品
	 * 
	 * @author subin
	 * */
	public DBRow[] findAllProductsByTitleAdid(Long[] title_id, long adid, PageCtrl pc, long active, AdminLoginBean adminLoggerBean) throws Exception {
		
		try { 
			
			StringBuffer sql = new StringBuffer();
			sql.append("select p.pc_id, p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,");
			sql.append(" pcode_main.p_code as p_code,pcode_amazon.p_code as p_code2,pcode_upc.p_code as upc,");
			sql.append(" p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width,p.sn_size ");
			sql.append(" ,length_uom,weight_uom,price_uom,p.freight_class,p.nmfc_code,p.sku ,p.lot_no ");
			sql.append(" from product as p ");
			sql.append(" LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
			
			sql.append(" join product_code as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type ="+CodeTypeKey.Main);
			sql.append(" left join product_code as pcode_amazon on p.pc_id = pcode_amazon.pc_id and pcode_amazon.code_type ="+CodeTypeKey.Amazon);
			sql.append(" left join product_code as pcode_upc on p.pc_id = pcode_upc.pc_id and pcode_upc.code_type ="+CodeTypeKey.UPC);
			
			sql.append(" where orignal_pc_id = 0 ");
			
			//0:正常登录  1：customer 2：carrier
			if(adminLoggerBean.getCorporationType() == 1){
				
				sql.append(" and exists ( select * from title_product tp where tp.tp_pc_id = p.pc_id and tp.tp_customer_id = " + adminLoggerBean.getCorporationId()  + ") ");
			}

			if(active > -1){
				
				sql.append(" AND p.alive = " + active);
			}	
			
			if(null != title_id && title_id.length > 0){
				
				sql.append(" AND (tp.tp_title_id = "+title_id[0]);
				for (int i = 1; i < title_id.length; i++)
				{
					sql.append(" OR tp.tp_title_id = "+title_id[i]);	
				}
				sql.append(" )");
			}
			sql.append(" GROUP BY p.pc_id");
			sql.append(" order by p.pc_id desc ,catalog_id desc,p_name asc");
			
			if ( pc!=null ) {
				
				return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
				
			} else {
				
				return (dbUtilAutoTran.selectMutliple(sql.toString()));
			}
			
		} catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findAllProductsByTitleAdid() error:" + e);
		}
	}
	
	/**
	 * 查询用户
	 * @author Administrator
	 * @param 仓库   角色   部门   办公地点   状态
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAdminsTitleByAdminNameAdgidStorageStatus(String cmd, long proPsId, String proJsId, long proAdgid, long filter_lid, int lock_state, String account, PageCtrl pc) throws Exception
	{
		try
		{
			if("search".equals(cmd))
			{
				String sql = "SELECT m.adid, m.account, t.title_name, ta.title_admin_sort FROM ("
					+" SELECT adid, account from "+ConfigBean.getStringValue("admin")+" where 1=1 ";
				if(null != account && !"".equals(account))
				{
					sql += " AND account = '" + account + "'";
				}
				sql+="  order by adid desc) m LEFT JOIN title_admin ta ON ta.title_admin_adid = m.adid"
				+" LEFT JOIN title t ON ta.title_admin_title_id = t.title_id";
				return dbUtilAutoTran.selectMutliple(sql, pc);
			}
			else if("".equals(cmd))
			{
				String sql = "SELECT m.adid, m.account, t.title_name, ta.title_admin_sort FROM ("
					+" SELECT adid, account from "+ConfigBean.getStringValue("admin")+" where 1=1 ";
				sql+="  order by adid desc) m LEFT JOIN title_admin ta ON ta.title_admin_adid = m.adid"
				+" LEFT JOIN title t ON ta.title_admin_title_id = t.title_id";
				return dbUtilAutoTran.selectMutliple(sql, pc);
			}
				
			DBRow para = new DBRow();
			
			DBRow row[] = null;
			String sql = "SELECT m.adid, m.account, t.title_name, ta.title_admin_sort FROM ("
				+" SELECT adid, account from "+ConfigBean.getStringValue("admin")+" where 1=1 ";
			if(proPsId == 0 && proJsId.equals("0") && proAdgid == 0 && filter_lid == 0 && -1 == lock_state && (null==account || "".equals(account))){
				sql = " SELECT m.account, t.title_name, ta.title_admin_sort FROM ("
					+" 		select adid, account from "+ConfigBean.getStringValue("admin")+" order by adid desc"
					+"  ) m LEFT JOIN title_admin ta ON ta.title_admin_adid = m.adid"
					+" LEFT JOIN title t ON ta.title_admin_title_id = t.title_id";
				row = dbUtilAutoTran.selectMutliple(sql, pc);
				return row;
			}
			if(proPsId != 0){
				para.add("ps_id",proPsId);
				sql += "and ps_id=? ";
			}
			if(!proJsId.equals("0")){
				para.add("proJsId",proJsId);
				sql += "and adid in (select adid from " + ConfigBean.getStringValue("admin_group_relation") + " where proJsId = ?)";
			}
			if(1 == lock_state){
				sql += "and llock=? ";
				para.add("llock", lock_state);
			}else if(2 == lock_state){
				sql += "and llock=? ";
				para.add("llock", 0);
			}
			//如果不是从页面上以超链接形式查询(根据办公地点id)说明传来的proJsId默认值是空，那么查询条件就只有areaId一个。
			if (proJsId.equals("")) {
				para.remove("proJsId");// 移除上面添加的proJsId字段
				para.add("parentid", filter_lid);
				para.add("id", filter_lid);
				para.add("lid", filter_lid);
				sql ="SELECT m.account, t.title_name, ta.title_admin_sort FROM (" 
					+" select adid, account from "+ConfigBean.getStringValue("admin")
					+" where adid in(select adid from " + ConfigBean.getStringValue("admin_group_relation") + " a" 
					+" join (select * from "+ ConfigBean.getStringValue("office_location") 
					+" where parentid in(select id from "+ ConfigBean.getStringValue("office_location") 
					+" where parentid = ? or id= ? ) or id= ? ) o on a.AreaId = o.id )";
				if(null != account && !"".equals(account))
				{
					sql += " AND account = '" + account + "'";
				}
				sql+="  order by adid desc) m LEFT JOIN title_admin ta ON ta.title_admin_adid = m.adid"
				+" LEFT JOIN title t ON ta.title_admin_title_id = t.title_id";
			
				row = dbUtilAutoTran.selectPreMutliple(sql, para, pc);
				return row;
			}
			if(proAdgid != 0){
				para.add("adgid", proAdgid);
				sql += "and adid in (SELECT adid from "+ConfigBean.getStringValue("admin_group_relation")+" where adgid = ?)";
			}
			if(filter_lid != 0){
				para.add("parentid", filter_lid);
				para.add("id", filter_lid);
				para.add("lid", filter_lid);
				sql += "and adid in(select adid from " + ConfigBean.getStringValue("admin_group_relation") + " a join (select * from "+ ConfigBean.getStringValue("office_location") +" where parentid in(select id from "+ ConfigBean.getStringValue("office_location") +" where parentid = ? or id= ? ) or id= ? ) o on a.AreaId = o.id )";
			}
			if(null != account && !"".equals(account))
			{
				sql += " AND account = '" + account + "'";
			}
			sql += "order by adid desc"
				+"  ) m LEFT JOIN title_admin ta ON ta.title_admin_adid = m.adid"
				+" LEFT JOIN title t ON ta.title_admin_title_id = t.title_id";
			if (null != pc){
				row = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			}
			else{
				row = dbUtilAutoTran.selectPreMutliple(sql,para);
			}
			return(row);
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findAdminsTitleByAdminNameAdgidStorageStatus(String cmd, long proPsId, String proJsId, long proAdgid, long filter_lid, int lock_state, String account, PageCtrl pc):"+e);
		}
	}
	
	/**
	 * 查询商品
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] filterExportProductTitlesByPcLineCategoryTitleId(long catalog_id, long product_line_id, int union_flag, int product_file_type, int product_upload_status, long title_id, long adid, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			//商品某一类型的文件是否上传了
			if(0 != product_file_type)
			{
				//-- 商品照片某个类型未全(全)的,AND m.count_product IS NUL不加就不管是否上传了
				sb.append(" select pcode_main.p_code AS p_code, t.title_name");
				sb.append(" from (select s.pc_id, tp.tp_title_id from ");
				sb.append(" 		( select p.pc_id , p.catalog_id ");
				sb.append(" 			from product p ");
				sb.append(" 			LEFT JOIN (select pc_id , count(pc_id) as count_product from product_file ");
				sb.append(" 							where file_with_type = ").append(FileWithTypeKey.PRODUCT_SELF_FILE);
				sb.append("								and product_file_type = ").append(product_file_type);
				sb.append(" 							GROUP BY pc_id, product_file_type)m");
				sb.append(" 			ON m.pc_id = p.pc_id ");
				sb.append(" 			WHERE p.orignal_pc_id = 0");
				if(union_flag > 0)
				{
					sb.append(" 		AND p.union_flag = ").append(union_flag);
				}
				
				if(1 == product_upload_status)
				{
					sb.append("				AND m.count_product IS NULL");
				}
				else if(2 == product_upload_status)
				{
					sb.append("				AND m.count_product IS NOT NULL");
				}
				
				sb.append("				)s");
				
				sb.append(" 	JOIN product_catalog c ON s.catalog_id = c.id");
				sb.append("		JOIN product_line_define l ON l.id = c.product_line_id ");
				sb.append("		LEFT JOIN title_product tp ON s.pc_id = tp.tp_pc_id");
				
				if(adid > 0)
				{
					sb.append(" JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
				}
//				sb.append("		JOIN title_product_catalog tpc ON c.id = tpc.tpc_product_catalog_id ");
//				sb.append("		JOIN title_product_line tpl ON tpl.tpl_product_line_id = l.id ");
				
				
				if(0 != catalog_id)
				{
					sb.append("     JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				sb.append(" 	WHERE l.id IS NOT NULL ");
				if(0 != product_line_id)
				{
					sb.append(" AND l.id = ").append(product_line_id);
				}
				if(title_id > 0)
				{
					sb.append(" AND tp.tp_title_id = " + title_id);
				}
				if(0 != catalog_id)
				{
					sb.append(" 	AND pcl.search_rootid = ").append(catalog_id);
				}
//				sb.append("  GROUP BY s.pc_id ");
				sb.append("  ORDER BY s.pc_id");
				sb.append(" 	) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append("	LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
				sb.append(" LEFT JOIN title t ON t.title_id = z.tp_title_id");
			}
			//商品未完成上传文件
			else if(0 == product_file_type && 1 == product_upload_status)
			{
				sb.append(" select pcode_main.p_code AS p_code, t.title_name ");
				sb.append("   from (select s.pc_id, tp.tp_title_id from ");
				sb.append("  	(select w.pc_id, w.catalog_id");
				sb.append("  	 from product w left JOIN");
				sb.append("  		(select p.pc_id , n.count_product from product p LEFT JOIN");
				sb.append("  			(select count(*) as count_product,pc_id  ");
				sb.append("  					from (select * from product_file where file_with_type = ").append(FileWithTypeKey.PRODUCT_SELF_FILE);
				sb.append(						" GROUP BY pc_id, product_file_type) m GROUP BY pc_id)n");
				sb.append("  			on n.pc_id = p.pc_id ");
				sb.append("  			where p.orignal_pc_id = 0");
				sb.append("  			and n.count_product = 5 ");
				sb.append("  			and p.orignal_pc_id = 0)q");
				sb.append("  	ON w.pc_id = q.pc_id ");
				sb.append("  	where w.orignal_pc_id = 0 ");
				if(union_flag > 0)
				{
					sb.append("		AND w.union_flag = 0");
				}
				sb.append("  	and (q.count_product is null or q.count_product < 5) )s");
				sb.append(" 	JOIN product_catalog c ON s.catalog_id = c.id");
				sb.append("		JOIN product_line_define l ON l.id = c.product_line_id ");
				
				sb.append("		LEFT JOIN title_product tp ON s.pc_id = tp.tp_pc_id");
				
				if(adid > 0)
				{
					sb.append("		JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
				}
//				sb.append("		JOIN title_product_catalog tpc ON c.id = tpc.tpc_product_catalog_id ");
//				sb.append("		JOIN title_product_line tpl ON tpl.tpl_product_line_id = l.id ");
				
				
				
				if(0 != catalog_id)
				{
					sb.append("  JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				sb.append("  WHERE l.id IS NOT NULL ");
				if(0 != product_line_id)
				{
					sb.append("AND l.id = ").append(product_line_id);
				}
				if(title_id > 0)
				{
					sb.append(" AND tp.tp_title_id = " + title_id);
				}
				if(0 != catalog_id)
				{
					sb.append("  AND pcl.search_rootid = ").append(catalog_id);
				}
//				sb.append("  GROUP BY s.pc_id ");
				sb.append("  ORDER BY s.pc_id");
				sb.append("	 ) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
				sb.append(" LEFT JOIN title t ON t.title_id = z.tp_title_id");
			}
			//商品已完成上传文件
			else if(0 == product_file_type && 2 == product_upload_status)
			{
				sb.append("select pcode_main.p_code AS p_code, t.title_name ");
				sb.append("   from(select q.pc_id, tp.tp_title_id from ");
				sb.append(" 		(select p.pc_id, p.catalog_id");
				sb.append(" 		   from product p LEFT JOIN");
				sb.append(" 			(select count(*) as count_product,pc_id  ");
				sb.append(" 					from (select * from product_file where file_with_type = ").append(FileWithTypeKey.PRODUCT_SELF_FILE);
				sb.append("						GROUP BY pc_id, product_file_type) m GROUP BY pc_id)n");
				sb.append(" 			on n.pc_id = p.pc_id ");
				sb.append(" 			where p.orignal_pc_id = 0");
				sb.append(" 			and n.count_product = 5 ");
				if(union_flag > 0)
				{
					sb.append(" 		AND p.union_flag = ").append(union_flag);
				}
				sb.append(" 			and p.orignal_pc_id = 0)q");
				sb.append(" 	JOIN product_catalog c ON q.catalog_id = c.id");
				sb.append("		JOIN product_line_define l ON l.id = c.product_line_id ");
				
				sb.append("		LEFT JOIN title_product tp ON q.pc_id = tp.tp_pc_id");
			
				if(adid > 0)
				{
					sb.append("		JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
				}
//				sb.append("		JOIN title_product_catalog tpc ON c.id = tpc.tpc_product_catalog_id ");
//				sb.append("		JOIN title_product_line tpl ON tpl.tpl_product_line_id = l.id ");
				if(0 != catalog_id)
				{
					sb.append(" 	JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				sb.append(" 	WHERE l.id IS NOT NULL ");
				if(0 != product_line_id)
				{
					sb.append("		AND l.id = ").append(product_line_id);
				}
				if(title_id > 0)
				{
					sb.append(" AND tp.tp_title_id = " + title_id);
				}
				if(0 != catalog_id)
				{
					sb.append(" 	AND pcl.search_rootid = ").append(catalog_id);
				}
//				sb.append("  GROUP BY q.pc_id ");
				sb.append("  ORDER BY q.pc_id");
				sb.append("		 ) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
				sb.append(" LEFT JOIN title t ON t.title_id = z.tp_title_id");
				
			}
			//不管商品是否上传了文件
			else
			{
				//-- 不管商品是否上传了文件
				sb.append("select pcode_main.p_code AS p_code, t.title_name ");
				sb.append("	from(select p.pc_id, p.catalog_id, tp.tp_title_id");
				sb.append("	 FROM product p ");
				sb.append("	 JOIN product_catalog c ON p.catalog_id = c.id");
				sb.append("  JOIN product_line_define l ON l.id = c.product_line_id ");
				if(0 != catalog_id)
				{
					sb.append("  JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				
				sb.append("		LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
				
				if(adid > 0)
				{
					sb.append("  JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
				}
//				sb.append("  JOIN title_product_catalog tpc ON c.id = tpc.tpc_product_catalog_id ");
//				sb.append("  JOIN title_product_line tpl ON tpl.tpl_product_line_id = l.id ");
				
				
				sb.append("	 WHERE l.id IS NOT NULL ");
				sb.append(" AND p.orignal_pc_id = 0 ");
				if(0 != product_line_id)
				{
					sb.append("  AND l.id = ").append(product_line_id);
				}
				if(0 != catalog_id)
				{
					sb.append("	 AND pcl.search_rootid = ").append(catalog_id);
				}
				if(title_id > 0)
				{
					sb.append(" AND tp.tp_title_id = " + title_id);
				}
				if(union_flag > 0)
				{
					sb.append("  AND p.union_flag = ").append(union_flag);
				}
//				sb.append("  GROUP BY p.pc_id ");
				sb.append("  ORDER BY p.pc_id");
				sb.append("	 ) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
				sb.append(" LEFT JOIN title t ON t.title_id = z.tp_title_id");	
				
			}
			return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.filterExportProductTitlesByPcLineCategoryTitleId(long catalog_id, long product_line_id, int union_flag, int product_file_type, int product_upload_status, long title_id, long adid, PageCtrl pc):"+e);
		}
	}
	
	/**
	 * 模糊查询商品明细
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findExportDetailProductLikeSearch(String key,long title_id, long adid,PageCtrl pc)throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM(");
			sql.append(" select pcode_main.p_code AS p_code, t.title_name , p.pc_id");
			sql.append(" from product as p ");
			sql.append(" join "+ConfigBean.getStringValue("product_code")+" as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type="+CodeTypeKey.Main);
			sql.append(" LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
			
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
			}
			sql.append(" LEFT JOIN title t ON t.title_id = tp.tp_title_id");
			sql.append(" where p_name like '%"+key+"%' " );
			sql.append(" union ");
			sql.append(" select pcode_main.p_code AS p_code, t.title_name, p.pc_id ");
			sql.append(" from product as p ");
			sql.append(" JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
			if(title_id > 0)
			{
				sql.append(" AND tp.tp_title_id = " + title_id);
			}
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
			}
			sql.append(" join product_code as pcode on p.pc_id = pcode.pc_id");
			sql.append(" join "+ConfigBean.getStringValue("product_code")+" as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type="+CodeTypeKey.Main);
			sql.append(" LEFT JOIN title t ON t.title_id = tp.tp_title_id");
			sql.append(" WHERE pcode.p_code like '%"+key+"%' ");
			if(title_id > 0)
			{
				sql.append(" AND tp.tp_title_id = " + title_id);
			}
			sql.append(")m");
			sql.append(" ORDER BY m.pc_id");
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findExportDetailProductLikeSearch(String key,long title_id, long adid,PageCtrl pc) error:"+e);
		}
	}
	
	
	/**
	 * 将商品、商品线、条码关联，根据产品线查询
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findExportProductInfosProductLineProductCodeByLineId(long pc_line_id, long title_id, long adid, PageCtrl pc) throws Exception
	{
		try 
		{
			StringBuffer sb = new StringBuffer();
			
			sb.append("select  pcode_main.p_code AS p_code, t.title_name ");
			sb.append(" from(select s.pc_id, tp.tp_title_id from ");
			sb.append("	(select w.pc_id, w.catalog_id");
			sb.append("	 from product w left JOIN");
			sb.append("		(select p.pc_id , n.count_product from product p LEFT JOIN");
			sb.append("			(select count(*) as count_product,pc_id  ");
			sb.append("					from (select * from product_file where file_with_type = 31 GROUP BY pc_id, product_file_type) m GROUP BY pc_id)n");
			sb.append("			on n.pc_id = p.pc_id ");
			sb.append("			where p.orignal_pc_id = 0");
			sb.append("			and n.count_product = 5 ");
			sb.append("			and p.orignal_pc_id = 0)q");
			sb.append("	ON w.pc_id = q.pc_id where w.orignal_pc_id = 0 and (q.count_product is null or q.count_product < 5) )s");
			
			sb.append("	 JOIN product_catalog c ON s.catalog_id = c.id");
			sb.append("  JOIN product_line_define l ON l.id = c.product_line_id ");
			
			sb.append("  LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
			
			if(adid > 0)
			{
				sb.append("  JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
			}
//			sb.append("  JOIN title_product_line tpl ON tpl.tpl_product_line_id = l.id ");
			sb.append(" WHERE l.id IS NOT NULL AND l.id = ").append(pc_line_id);
			if(title_id > 0)
			{
				sb.append(" AND tp.tp_title_id = " + title_id);
			}
//			sb.append("  GROUP BY q.pc_id ");
			sb.append("  ORDER BY q.pc_id");
			sb.append("		 ) z");
			sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
			sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
			sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
			sb.append(" LEFT JOIN title t ON t.title_id = z.tp_title_id");	
			
			return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.findExportProductInfosProductLineProductCodeByLineId(long pc_line_id, long title_id, long adid, PageCtrl pc):"+e);
		}
	}
	
	public DBRow[] findExportAllProductsByTitleAdid(long title_id, long adid, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select  pcode_main.p_code AS p_code, t.title_name ");
			sql.append(" from product as p ");
			sql.append(" LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
			
			if(adid > 0)
			{
				sql.append("  JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id  AND ta.title_admin_adid = " + adid);
			}
			sql.append(" join product_code as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type ="+CodeTypeKey.Main);
			sql.append(" left join product_code as pcode_amazon on p.pc_id = pcode_amazon.pc_id and pcode_amazon.code_type ="+CodeTypeKey.Amazon);
			sql.append(" left join product_code as pcode_upc on p.pc_id = pcode_upc.pc_id and pcode_upc.code_type ="+CodeTypeKey.UPC);
			sql.append(" LEFT JOIN title t ON t.title_id = tp.tp_title_id");
			sql.append(" where orignal_pc_id = 0");
			if(title_id > 0)
			{
				sql.append(" AND tp.tp_title_id = " + title_id);
			}
//			sql.append(" GROUP BY p.pc_id");
			sql.append(" order by p.pc_id,catalog_id desc,p_name asc");
			
			if ( pc!=null )
			{
				return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
			}
			else
			{
				return (dbUtilAutoTran.selectMutliple(sql.toString()));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProprietaryMgrZyj.findExportAllProductsByTitleAdid(long title_id, long adid, PageCtrl pc) error:" + e);
		}
	}
	
	/**
	 * 通过用户ID获取用户优先级最高的titleID
	 * @return
	 * @throws Exception
	 */
	public long findTitleIdByAdidTopPriority(long adid) throws Exception
	{
		try
		{
			String sql = " select t.title_admin_title_id from title_admin t where t.title_admin_sort = ";
			sql += " (SELECT min(ta.title_admin_sort) FROM title_admin ta";
			sql += " WHERE ta.title_admin_adid = "+adid+" order BY ta.title_admin_adid)";
			sql += " and t.title_admin_adid = "+adid;
			return dbUtilAutoTran.selectSingle(sql).get("title_admin_title_id", 0L);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProprietaryMgrZyj.findTitleIdByAdidTopPriority(long adid) error:" + e);
		}
	}
	
	/**
	 * 通过用户ID和titleName查询title是否存在
	 * @param adid
	 * @param title_name
	 * @return
	 * @throws Exception
	 */
	public DBRow findTitleAdminByAdidTitleName(long adid, String title_name) throws Exception
	{
		try
		{
			String sql = "select * from title t join title_admin ta on t.title_id = ta.title_admin_title_id where 1=1";
			if(adid > 0)
			{
				sql += " and ta.title_admin_adid = " + adid;
			}
			if(null != title_name && !"".equals(title_name))
			{
				sql += " and t.title_name = '"+title_name+"'";
			}
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProprietaryMgrZyj.findTitleAdminByAdidTitleName(long adid, String title_name) error:" + e);
		}
	}
	
	/**
	 * 通过用户ID，部门为客户的用户个数，即查询此用户是否是客户
	 * @param adminId
	 * @return
	 * @throws Exception
	 * zyj
	 */
	public DBRow findAdminByGroupClientAdid(long adminId) throws Exception
	{
		try{
			String sql="select * from admin where adid="+adminId+" and adgid=100026";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorProprietaryMgrZyj.findAdminByGroupClientAdid(long adminId)"+e);
		}
	}
	
	/**
	 * 通过用户ID，查询最大优先级
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public long findAdminMaxPriorityByAdid(long adid) throws Exception
	{
		try
		{
			long titleId = 0L;
			String sql = "select ta.title_admin_title_id from title_admin ta where ta.title_admin_adid = " + adid + " ORDER BY ta.title_admin_sort LIMIT 1";
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			if(null != row)
			{
				titleId = row.get("title_admin_title_id", 0L);
			}
			return titleId;
		}
		catch(Exception e)
		{
			throw new Exception("FloorProprietaryMgrZyj.findAdminMaxPriorityByAdid(long adminId)"+e);
		}
	}
	
	/**
	 * 通过titleId,产品线ID查询title_product_line
	 * @param tpl_title_id
	 * @param tpl_product_line_id
	 * @return
	 * @throws Exception
	 */
	public int findProprietaryPcLineCountByTitleIdPcId(long tpl_title_id, long tpl_product_line_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select count(*) as titlePcLineCount from ");
			sql.append(ConfigBean.getStringValue("title_product_line"));
			sql.append(" where 1=1");
			if(tpl_title_id > 0)
			{
				sql.append(" and tpl_title_id = ").append(tpl_title_id);
			}
			if(tpl_product_line_id > 0)
			{
				sql.append(" and tpl_product_line_id = ").append(tpl_product_line_id);
			}
			return dbUtilAutoTran.selectSingle(sql.toString()).get("titlePcLineCount", 0);
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryPcLineCountByTitleIdPcId(long tpl_title_id, long tpl_product_line_id):"+e);
		}
	}
	
	
	/**
	 * 通过titleId,分类ID查询title_product_line
	 * @param tpc_title_id
	 * @param tpc_product_catalog_id
	 * @return
	 * @throws Exception
	 */
	public int findProprietaryCatagoryCountByTitleIdPcId(long tpc_title_id, long tpc_product_catalog_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select count(*) as titleCategoryCount from ");
			sql.append(ConfigBean.getStringValue("title_product_catalog"));
			sql.append(" where 1=1");
			if(tpc_title_id > 0)
			{
				sql.append(" and tpc_title_id = ").append(tpc_title_id);
			}
			if(tpc_product_catalog_id > 0)
			{
				sql.append(" and tpc_product_catalog_id = ").append(tpc_product_catalog_id);
			}
			return dbUtilAutoTran.selectSingle(sql.toString()).get("titleCategoryCount", 0);
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryCatagoryCountByTitleIdPcId(long tpc_title_id, long tpc_product_catalog_id):"+e);
		}
	}
	
	
	/**
	 * 通过titleId,产品ID查询title_product
	 * @param tp_title_id
	 * @param tp_pc_id
	 * @return
	 * @throws Exception
	 */
	public int findProprietaryProductCountByTitleIdPcId(long tp_title_id, long tp_pc_id) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select count(*) as titleProductCount from ");
			sql.append(ConfigBean.getStringValue("title_product"));
			sql.append(" where 1=1");
			if(tp_pc_id > 0)
			{
				sql.append(" and tp_pc_id = ").append(tp_pc_id);
			}
			if(tp_title_id > 0)
			{
				sql.append(" and tp_title_id = ").append(tp_title_id);
			}
			return dbUtilAutoTran.selectSingle(sql.toString()).get("titleProductCount", 0);
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryProductCountByTitleIdPcId(long tp_title_id, long tp_pc_id):"+e);
		}
	}
	
	
	/**
	 * 根据titleID查询产品线
	 * @author Administrator
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProductLinesIdAndNameByTitleId(long adid, Long[] pc_line_ids, Long[] title_ids, int begin, int length, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pld.id, pld.name FROM product_line_define pld");
			sql.append(" LEFT JOIN title_product_line tpl ON pld.id = tpl.tpl_product_line_id");
			
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = tpl.tpl_title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			sql.append(" WHERE 1=1");
			if(null != pc_line_ids && pc_line_ids.length > 0)
			{
				sql.append(" AND (pld.id = " + pc_line_ids[0]);
				for (int i = 1; i < pc_line_ids.length; i++) 
				{
					sql.append(" OR pld.id = " + pc_line_ids[i]);
				}
				sql.append(" )");
			}
			if(null != title_ids && title_ids.length > 0)
			{
				sql.append(" AND (tpl.tpl_title_id = " + title_ids[0]);
				for (int i = 1; i < title_ids.length; i++) 
				{
					sql.append(" OR tpl.tpl_title_id = " + title_ids[i]);
				}
				sql.append(" )");
			}
			
			sql.append(" GROUP BY pld.id");
			sql.append(" ORDER BY pld.id");
			if(begin >= 0)
			{
				if(length > 0)
				{
					sql.append(" LIMIT "+begin+","+length);
				}
			}
			else
			{
				if(length > 0)
				{
					sql.append(" LIMIT "+length);
				}
			}
			
			if(null != pc)
			{
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProductLinesIdAndNameByTitleId(long adid, long pc_line_id, long title_id, int begin, int length, PageCtrl pc):"+e);
		}
	}
	
	
	public DBRow[] findProductCatagoryParentsIdAndNameByTitleId(long adid, Long[] pc_line_id, Long[] category_parent_id, Long[] pc_cata_id, Long[] title_id, int begin, int length, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select pc.id, pc.title name,pc.parentid from product_catalog pc ");
			sql.append(" LEFT JOIN title_product_catalog tpc ON tpc.tpc_product_catalog_id = pc.id");
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = tpc.tpc_title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			sql.append(" WHERE 1=1");
			if(null != category_parent_id && category_parent_id.length > 0)
			{
				if(-1 == category_parent_id[0])
				{
					sql.append(" AND (pc.parentid = 0");
				}
				else
				{
					sql.append(" AND (pc.parentid = " + category_parent_id[0]);
				}
				for (int i = 1; i < category_parent_id.length; i++)
				{
					//修改：将 category_parent_id[0]改为category_parent_id[i]  lujintao  2015-07-27 10:52
					sql.append(" OR pc.parentid = " + category_parent_id[i]);
				}
				sql.append(" )");
			}
			else
			{
				sql.append(" AND pc.parentid = 0");
			}
			if(null != pc_line_id && pc_line_id.length > 0)
			{
				sql.append(" AND (pc.product_line_id = " + pc_line_id[0]);
				for (int i = 1; i < pc_line_id.length; i++)
				{
					sql.append(" OR pc.product_line_id = " + pc_line_id[i]);
				}
				sql.append(" )");
			}
			if(null != title_id && title_id.length > 0)
			{
				sql.append(" AND (tpc.tpc_title_id = "+title_id[0]);
				for (int i = 1; i < title_id.length; i++)
				{
					sql.append(" OR tpc.tpc_title_id = "+title_id[i]);	
				}
				sql.append(" )");
			}
			sql.append(" GROUP BY pc.id");
			sql.append(" order by pc.title asc");
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProprietaryMgrZyj.findProductCatagoryParentsIdAndNameByTitleId(long adid, long[] pc_line_id, long[] category_parent_id, long[] pc_cata_id, long[] title_id, int begin, int length, PageCtrl pc):"+e);
		}
	}
	
	/**
	 * 根据商品ID，TitleID获得商品与Title关系
	 * @param pc_id
	 * @param title_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailTilteProductByPCTitle(long pc_id, long title_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("title_product")+" where tp_pc_id = ? and tp_title_id = ? ";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("title_id",title_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj getDetailProductTilteByPCTitle error:"+e);
		}
	}
	
	public DBRow getDetailTilteProductByPCTitle(long pc_id, long title_id, long customer_id)
			throws Exception
		{
			try 
			{
				String sql = "select * from "+ConfigBean.getStringValue("title_product")+" where tp_pc_id = ? and tp_title_id = ? and tp_customer_id = ?";
				
				DBRow para = new DBRow();
				para.add("pc_id",pc_id);
				para.add("title_id",title_id);
				para.add("customer_id",customer_id);
				
				return dbUtilAutoTran.selectPreSingle(sql, para);
			}
			catch (Exception e) 
			{
				throw new Exception("FloorProprietaryMgrZyj getDetailProductTilteByPCTitle error:"+e);
			}
		}
	
	public DBRow getDetailTitleCatalogByTitleCatalog(long catalog_id,long title_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("title_product_catalog")+" where tpc_title_id = ? and tpc_product_catalog_id = ? ";
			
			DBRow para = new DBRow();
			para.add("title_id",title_id);
			para.add("catalog_id",catalog_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj getDetailTitleCatalogByTitleCatalog error:"+e);
		}
	}
	
	/**
	 * 商品已有的Title关系
	 * @author Administrator
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProudctHasTitleList(long pc_id) throws Exception{
		try{
			
			String sql = "SELECT * FROM title t LEFT OUTER JOIN( SELECT * FROM title_product tp WHERE tp.tp_pc_id= "+pc_id+") as tp ON t.title_id= tp.tp_title_id WHERE tp.tp_pc_id="+pc_id+" ORDER BY t.title_name";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception(""+e);
		}
	}
	
	public DBRow[] getProudctUnhasTitleList(long pc_id) throws Exception{
		try{
			
			//String sql = "SELECT * FROM title t LEFT OUTER JOIN( SELECT * FROM title_admin ta WHERE ta.title_admin_adid = "+adid+") AS ta ON t.title_id = ta.title_admin_title_id where ta.title_admin_adid IS NULL";
			String sql = "SELECT * FROM title t LEFT OUTER JOIN( SELECT * FROM title_product tp WHERE tp.tp_pc_id= "+pc_id+") as tp ON t.title_id= tp.tp_title_id WHERE tp.tp_pc_id IS NULL";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception(""+e);
		}
	}
	

	/**
	 * 添加商品与Title关系
	 */
	
	public void insertTitleProduct(long pc_id, String titleId) throws Exception{
		try{
			
			
			DBRow param = new DBRow();
			param.add("tp_pc_id",pc_id);
			param.add("tp_title_id",titleId);
			
			//添加用戶與TITLE關係
			dbUtilAutoTran.insert(ConfigBean.getStringValue("title_product"), param);
		}
		catch (Exception e) {
			throw new Exception(""+e);
		}
	}
	
	/**
	 * 删除商品与Title关系
	 */
	public void deleteTitleProductByPcidAndTitleId(long pc_id, String titleId) throws Exception{
		try{

		
			//删除
			String where = " where tp_pc_id = "+pc_id+" and tp_title_id = " +titleId;
			dbUtilAutoTran.delete(where, ConfigBean.getStringValue("title_product"));
		}
		
		catch (Exception e) {
			throw new Exception(""+e);
		}
	}

	/**
	 * 通过商品ID，titleid查出产品线中是否存在title
	 * @param flag 
	 */
	public DBRow findProductTitleParentNode(long pc_id,String titleId) throws Exception
	{
		try{
			
			String sql="select  count(*) as whether_or_not,title_name  FROM title_product_catalog pc,title t WHERE  tpc_product_catalog_id= (select  catalog_id FROM "+ConfigBean.getStringValue("product")+"  WHERE pc_id="+pc_id+" ) and pc.tpc_title_id="+titleId+" and pc.tpc_title_id=t.title_id";
			
		
			return dbUtilAutoTran.selectSingle(sql);
			//return dbUtilAutoTran.selectMutliple(sql.toString());
			}catch(Exception e){
			throw new Exception("FloorProprietaryMgrZyj.findProductTitleParentNode(long pc_id,String[] titleId)"+e);
		}
		
	}
	
	/**
	 * 根据titleID查询产品分类
	 * @author Administrator
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findParentProductCatagorysByTitleId(long adid, Long[] pc_line_id, Long[] title_id, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pc.*,pld.name product_line_name FROM product_catalog pc LEFT JOIN product_line_define pld on pc.product_line_id = pld.id ");
			
			if(title_id.length > 0 || adid > 0) {
				
				sql.append(" LEFT JOIN title_product_catalog tpc ON tpc.tpc_product_catalog_id = pc.id");
			}
			
			if(adid > 0) {
				
				sql.append(" JOIN title_admin ta ON ta.title_admin_title_id = tpc.tpc_title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			
			sql.append(" WHERE 1=1");
			
			if(null != pc_line_id && pc_line_id.length > 0) {
				
				sql.append(" AND (pc.product_line_id = " + pc_line_id[0]);
				
				for (int i = 1; i < pc_line_id.length; i++) {
					
					sql.append(" OR pc.product_line_id = " + pc_line_id[i]);
				}
				sql.append(" )");
			}
			
			if(null != title_id && title_id.length > 0) {
				
				sql.append(" AND (tpc.tpc_title_id = " + title_id[0]);
				
				for (int i = 1; i < title_id.length; i++){
					
					sql.append(" OR tpc.tpc_title_id = " + title_id[i]);
				}
				sql.append(" )");
			}
			
			sql.append(" ORDER BY pc.id");
			
			if(null != pc) {
				
				return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
				
			} else {
				
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}
		} catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findParentProductCatagorysByTitleId(long adid, Long[] pc_line_id, Long[] title_id, PageCtrl pc):"+e);
		}
	}
	
	public int findProprietaryProductByPcidTitleID(long pc_id, long title_id)throws Exception
	{
		try
		{
			String sql = "select count(*) cn from title_product where tp_pc_id = "+pc_id+" and tp_title_id ="+title_id;
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			if(null != row)
			{
				return row.get("cn", 0);
			}
			else
			{
				return 0;
			}
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryProductByPcidTitleID(long pc_id, long title_id):"+e);
		}
	}
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public DBRow findProprietaryByCategoryId(long categoryId) throws Exception{
		try
		{
			String sql = "select pl.* from product_line_define pl join product_catalog pc on pc.product_line_id = pl.id where pc.id = "+categoryId;
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			return row;
		}
		catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findProprietaryProductByPcidTitleID(long pc_id, long title_id):"+e);
		}
	}

	
	 /**
     * 根据pc_id查询出 sn的信息
     *
     * @param 职务名
     * @return 职务
     * @since Sync10
     * @author Yuanxinyu
     **/
    public DBRow[] getProductSnByPcId(long pc_id) throws Exception{

        try {

            String sql = "select * from product_sn where pc_id = "+pc_id;

            return dbUtilAutoTran.selectMutliple(sql);

        }catch (Exception e){
            throw new Exception("FloorProprietaryMgrZyj.getProductSnByPcId(long pc_id) error:" + e);
        }
    }
	
	
	/**
	 * 根据商品和用户查询title
	 */
	public DBRow[] findTitlesByAdidAndProduct(long adid, long pc_id)throws Exception{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select t.* from title t");
			sql.append(" join title_product tp on tp.tp_title_id=t.title_id");
			if(adid > 0){
				sql.append(" join title_admin ta on ta.title_admin_title_id=t.title_id");
				sql.append(" and ta.title_admin_adid="+adid);
			}
			sql.append(" where tp.tp_pc_id="+pc_id);
			
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorProprietaryMgrZyj.findTitlesByAdidAndProduct(long adid, long pc_id):" + e);
		}
	}
	
	
	
	/**
	 * 查询不包含当前productLineId的其他产品线
	 * @param lineId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProductLinesUnExistLineId(long lineId) throws Exception {
		try{
			String sql="select * from product_line_define pl where pl.id!="+lineId+"";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("FloorProprietaryMgrZyj.findProductLinesUnExistLineId(long lineId)"+e);
		}
	}
	
	/**
     * 根据pcid查询product信息
     * @author Yuanxinyu
     **/
    public DBRow getProductDetailByPcId(long pc_id) throws Exception{

        try {

            String sql = "select * from product where pc_id = "+pc_id;

            return dbUtilAutoTran.selectSingle(sql);

        }catch (Exception e){
            throw new Exception("FloorProprietaryMgrZyj.getProductDetailByPcId(long pc_id) error:" + e);
        }
    }


    /**
     * 根据id查询catalog信息
     * @author Yuanxinyu
     **/
    public DBRow getProductCatalogById(long id) throws Exception{

        try {

            String sql = "select * from product_catalog where id = "+id;

            return dbUtilAutoTran.selectSingle(sql);

        }catch (Exception e){
            throw new Exception("FloorProprietaryMgrZyj.getProductCatalogById(long id) error:" + e);
        }
    }

    
    /**
     * 根据pc id查询title信息
     * @author Yuanxinyu
     **/
    public DBRow[] getProductTitleByPcId(long pc_id) throws Exception{

        try {

            String sql = "select t.* from title t LEFT JOIN title_product tp on t.title_id = tp.tp_title_id where tp.tp_pc_id = "+pc_id;

            return dbUtilAutoTran.selectMutliple(sql);

        }catch (Exception e){
            throw new Exception("FloorProprietaryMgrZyj.getProductTitleByTitleId(long title_id) error:" + e);
        }
    }

    /**
     * 通过商品与title的关系重新计算产品分类、产品线与title的关系
     * @param pc_id
     * @throws Exception
     * @author:zyj
     * @date: 2015年3月22日 下午6:46:43
     */
    public void calculateCatelogAndLineTitle(long pc_id) throws Exception
    {
    	 try 
    	 {
    		 String sql = "call product_catalog_title_link("+pc_id+")";
             dbUtilAutoTran.CallStringSingleResultProcedure(sql);

         }
    	 catch (Exception e)
    	 {
             throw new Exception("FloorProprietaryMgrZyj.calculateCatelogAndLineTitle(long pc_id) error:" + e);
         }
    }
    
    /**
     * 查询商品归属的title
     * @param pc_id
     * @param adid
     * @return
     * @throws Exception
     * @author:zyj
     * @date: 2015年4月7日 下午4:32:18
     */
    public DBRow[] getProudctHasTitleListByPcAdmin(long pc_id, long adid) throws Exception{
		try{
//			String sql = "SELECT * FROM title t LEFT OUTER JOIN( SELECT * FROM title_product tp WHERE tp.tp_pc_id= "+pc_id+") as tp ON t.title_id= tp.tp_title_id WHERE tp.tp_pc_id IS NULL";
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM title t ");
			if(adid > 0)
			{
				sql.append(" left join title_admin ta on ta.title_admin_title_id = t.title_id");
			}
			sql.append(" LEFT OUTER JOIN( SELECT * FROM title_product tp WHERE tp.tp_pc_id= ").append(pc_id);
			sql.append(" ) as tp ON t.title_id= tp.tp_title_id ");
			sql.append(" WHERE 1=1");
			sql.append(" and tp.tp_pc_id=").append(pc_id);
			if(adid > 0)
			{
				sql.append(" and ta.title_admin_adid = ").append(adid);
			}
			sql.append(" GROUP BY t.title_id");
			sql.append(" ORDER BY t.title_name");
			
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception(""+e);
		}
	}
    
    /**
     * 商品不归属的title
     * @param pc_id
     * @param adid
     * @return
     * @throws Exception
     * @author:zyj
     * @date: 2015年4月7日 下午4:32:42
     */
    public DBRow[] getProudctUnHasTitleListByPcAdmin(long pc_id, long adid) throws Exception{
		try{
//			String sql = "SELECT * FROM title t LEFT OUTER JOIN( SELECT * FROM title_product tp WHERE tp.tp_pc_id= "+pc_id+") as tp ON t.title_id= tp.tp_title_id WHERE tp.tp_pc_id IS NULL";
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM title t ");
			if(adid > 0)
			{
				sql.append(" left join title_admin ta on ta.title_admin_title_id = t.title_id");
			}
			sql.append(" LEFT OUTER JOIN( SELECT * FROM title_product tp WHERE tp.tp_pc_id= ").append(pc_id);
			sql.append(" ) as tp ON t.title_id= tp.tp_title_id ");
			sql.append(" WHERE 1=1");
			sql.append(" and  tp.tp_pc_id IS NULL");
			if(adid > 0)
			{
				sql.append(" and ta.title_admin_adid = ").append(adid);
			}
			sql.append(" GROUP BY t.title_id");
			
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch (Exception e) {
			throw new Exception(""+e);
		}
	}
    
    public DBRow getTitleCount() throws Exception {
    	
    	try{
    		
    		String sql = " select count(*) count from title ";
    		
    		return dbUtilAutoTran.selectSingle(sql);
    		
    	} catch (Exception e){
    		
    		throw new Exception(""+e);
    	}
    }
    
    public DBRow getCustomerByName(String customer_id) throws Exception{
    	
    	try{
    			
			String sql = " select * from customer_id where customer_id = ?";
    		
    		DBRow param = new DBRow();
    		param.put("customer_id", customer_id);
    		
    		return dbUtilAutoTran.selectPreSingle(sql, param);
        		
    	} catch (Exception e){
    		
    		throw new Exception(""+e);
    	}
    }
    
    /**
     * 此商品是否与此customer有关系
     * 
     * @author subin
     * */
    public DBRow getCustomerByName(String customer_id, long pc_id) throws Exception{
    	
    	try{
    			
			String sql = " select * from customer_id ci where ci.customer_id = ? and EXISTS (select * from title_product tp where tp.tp_pc_id = ? and ci.customer_key = tp.tp_customer_id ) ";
    		
    		DBRow param = new DBRow();
    		param.put("customer_id", customer_id);
    		param.put("pc_id", pc_id);
    		
    		return dbUtilAutoTran.selectPreSingle(sql, param);
        		
    	} catch (Exception e){
    		
    		throw new Exception(""+e);
    	}
    }
    
    /**
	 * 查询商品
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] filterProductByPcLineCategoryTitleId(Long[] catalog_id, Long[] product_line_id, int union_flag, int product_file_type, int product_upload_status, Long[] title_id, long adid, int equal_or_not, long product_id, PageCtrl pc, long active, AdminLoginBean adminLoggerBean) throws Exception {
		
		try {
			
			StringBuffer sb = new StringBuffer();
			//商品某一类型的文件是否上传了
			if(0 != product_file_type) {
				
				//-- 商品照片某个类型未全(全)的,AND m.count_product IS NUL不加就不管是否上传了
				sb.append(" select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc");
				sb.append(" from (select s.* from ");
				sb.append(" 		( select p.pc_id , p.alive, p.catalog_id, p.heigth, p.length,");
				sb.append(" 						 p.orignal_pc_id, p.p_name, p.union_flag, p.unit_name,");
				sb.append(" 						 p.unit_price, p.volume, p.weight, p.width,p.sn_size");
				sb.append(" 						,p.length_uom,p.weight_uom,p.price_uom");
				sb.append(" 			from product p ");
				sb.append(" 			LEFT JOIN (select pc_id , count(pc_id) as count_product from product_file ");
				sb.append(" 							where file_with_type = ").append(FileWithTypeKey.PRODUCT_SELF_FILE);
				sb.append("								and product_file_type = ").append(product_file_type);
				sb.append(" 							GROUP BY pc_id, product_file_type)m");
				sb.append(" 			ON m.pc_id = p.pc_id ");
				sb.append(" 			WHERE p.orignal_pc_id = 0");
				
				if(active > -1){
					sb.append(" AND p.alive = " + active);
				}
				
				if(union_flag > -1)
				{
					sb.append(" 		AND p.union_flag = ").append(union_flag);
				}
				
				if(1 == product_upload_status)
				{
					sb.append("				AND m.count_product IS NULL");
				}
				else if(2 == product_upload_status)
				{
					sb.append("				AND m.count_product IS NOT NULL");
				}
				if(equal_or_not == YesOrNotKey.NO)
				{
					sb.append("				AND p.pc_id !="+product_id);
				}
				
				sb.append("				)s");
				
				sb.append(" 	LEFT JOIN product_catalog c ON s.catalog_id = c.id");
				sb.append("		LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
				
				sb.append(" LEFT JOIN title_product tp ON s.pc_id = tp.tp_pc_id");
				
				if(null != catalog_id && catalog_id.length > 0)
				{
					sb.append("     LEFT JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				sb.append(" 	WHERE 1=1 ");
				
				//0:正常登录  1：customer 2：carrier
				if(adminLoggerBean.getCorporationType() == 1){
					
					sb.append(" and exists ( select * from title_product tp where tp.tp_pc_id = s.pc_id and tp.tp_customer_id = " + adminLoggerBean.getCorporationId()  + ") ");
				}
				
				if(null != product_line_id && product_line_id.length > 0)
				{
					sb.append(" AND ( l.id = " + product_line_id[0]);
					for (int i = 1; i < product_line_id.length; i++) 
					{
						sb.append(" OR l.id = " + product_line_id[i]);
					}
					sb.append(" )");
				}
				if(null != title_id && title_id.length > 0)
				{
					sb.append(" AND ( tp.tp_title_id = " + title_id[0]);
					for (int i = 1; i < title_id.length; i++) 
					{
						sb.append(" OR tp.tp_title_id = " + title_id[i]);
					}
					sb.append(" )");
				}
				if(null != catalog_id && catalog_id.length > 0)
				{
					if(-1 != catalog_id[0])
					{
						sb.append(" AND ( pcl.search_rootid = " + catalog_id[0]);
					}
					else
					{
						sb.append(" AND ( pcl.search_rootid = 0");
					}
					for (int i = 1; i < catalog_id.length; i++) 
					{
						sb.append(" OR pcl.search_rootid = " + catalog_id[i]);
					}
					sb.append(" )");
				}
				sb.append("  GROUP BY s.pc_id");
				sb.append(" 	) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append("	LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
				sb.append(" order by z.pc_id desc");
			}
			//商品未完成上传文件
			else if(0 == product_file_type && 1 == product_upload_status)
			{
				sb.append(" select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc from ");
				sb.append("  (select s.* from ");
				sb.append("  	(select w.pc_id , w.alive, w.catalog_id, w.heigth, w.length,");
				sb.append("  				w.orignal_pc_id, w.p_name, w.union_flag, w.unit_name,");
				sb.append("  				w.unit_price, w.volume, w.weight, w.width,w.sn_size");
				sb.append(" 				,w.length_uom,w.weight_uom,w.price_uom");
				sb.append("  	 from product w left JOIN");
				sb.append("  		(select p.pc_id , n.count_product from product p LEFT JOIN");
				sb.append("  			(select count(*) as count_product,pc_id  ");
				sb.append("  					from (select * from product_file where file_with_type = ").append(FileWithTypeKey.PRODUCT_SELF_FILE);
				sb.append(						" GROUP BY pc_id, product_file_type) m GROUP BY pc_id)n");
				sb.append("  			on n.pc_id = p.pc_id ");
				sb.append("  			where p.orignal_pc_id = 0");
				sb.append("  			and n.count_product = 5 ");
				sb.append("  			and p.orignal_pc_id = 0)q");
				sb.append("  	ON w.pc_id = q.pc_id ");
				sb.append("  	where w.orignal_pc_id = 0 ");
				
				if(active > -1){
					sb.append(" AND w.alive = " + active);
				}
				
				if(union_flag > -1)
				{
					sb.append("		AND w.union_flag = ").append(union_flag);
				}
				if(equal_or_not == YesOrNotKey.NO)
				{
					sb.append("				AND w.pc_id !="+product_id);
				}
				sb.append("  	AND (q.count_product is null or q.count_product < 5) )s");
				sb.append(" 	LEFT JOIN product_catalog c ON s.catalog_id = c.id");
				sb.append("		LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
				
				sb.append(" LEFT	JOIN title_product tp ON s.pc_id = tp.tp_pc_id");
				
				if(null != catalog_id && catalog_id.length > 0)
				{
					sb.append("     LEFT JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				sb.append(" 	WHERE 1=1 ");
				
				//0:正常登录  1：customer 2：carrier
				if(adminLoggerBean.getCorporationType() == 1){
					
					sb.append(" and exists ( select * from title_product tp where tp.tp_pc_id = s.pc_id and tp.tp_customer_id = " + adminLoggerBean.getCorporationId()  + ") ");
				}
				
				if(null != product_line_id && product_line_id.length > 0)
				{
					sb.append(" AND ( l.id = " + product_line_id[0]);
					for (int i = 1; i < product_line_id.length; i++) 
					{
						sb.append(" OR l.id = " + product_line_id[i]);
					}
					sb.append(" )");
				}
				if(null != title_id && title_id.length > 0)
				{
					sb.append(" AND ( tp.tp_title_id = " + title_id[0]);
					for (int i = 1; i < title_id.length; i++) 
					{
						sb.append(" OR tp.tp_title_id = " + title_id[i]);
					}
					sb.append(" )");
				}
				if(null != catalog_id && catalog_id.length > 0)
				{
					if(-1 != catalog_id[0])
					{
						sb.append(" AND ( pcl.search_rootid = " + catalog_id[0]);
					}
					else
					{
						sb.append(" AND ( pcl.search_rootid = 0");
					}
					for (int i = 1; i < catalog_id.length; i++) 
					{
						sb.append(" OR pcl.search_rootid = " + catalog_id[i]);
					}
					sb.append(" )");
				}
				sb.append("  GROUP BY s.pc_id ORDER BY s.pc_id");
				sb.append("	 ) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
				sb.append(" order by z.pc_id desc");
			}
			//商品已完成上传文件
			else if(0 == product_file_type && 2 == product_upload_status)
			{
				sb.append("select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc ");
				sb.append("   from(select q.* from ");
				sb.append(" 		(select p.pc_id ,p.alive, p.catalog_id, p.heigth, p.length,");
				sb.append(" 				p.orignal_pc_id, p.p_name, p.union_flag, p.unit_name,");
				sb.append(" 				p.unit_price, p.volume, p.weight, p.width,p.sn_size");
				sb.append(" 				,p.length_uom,p.weight_uom,p.price_uom");
				sb.append(" 		   from product p LEFT JOIN");
				sb.append(" 			(select count(*) as count_product,pc_id  ");
				sb.append(" 					from (select * from product_file where file_with_type = ").append(FileWithTypeKey.PRODUCT_SELF_FILE);
				sb.append("						GROUP BY pc_id, product_file_type) m GROUP BY pc_id)n");
				sb.append(" 			on n.pc_id = p.pc_id ");
				sb.append(" 			where p.orignal_pc_id = 0");
				sb.append(" 			and n.count_product = 5 ");
				
				if(active > -1){
					sb.append(" AND p.alive = " + active);
				}
				if(union_flag > -1)
				{
					sb.append(" 		AND p.union_flag = ").append(union_flag);
				}
				if(equal_or_not == YesOrNotKey.NO)
				{
					sb.append("				AND p.pc_id !="+product_id);
				}
				sb.append(" 			and p.orignal_pc_id = 0)q");
				sb.append(" 	LEFT JOIN product_catalog c ON q.catalog_id = c.id");
				sb.append("		LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
				
				sb.append(" LEFT JOIN title_product tp ON q.pc_id = tp.tp_pc_id");
				
				if(null != catalog_id && catalog_id.length > 0)
				{
					sb.append("     LEFT JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				sb.append(" 	WHERE 1=1 ");
				
				//0:正常登录  1：customer 2：carrier
				if(adminLoggerBean.getCorporationType() == 1){
					
					sb.append(" and exists ( select * from title_product tp where tp.tp_pc_id = q.pc_id and tp.tp_customer_id = " + adminLoggerBean.getCorporationId()  + ") ");
				}
				
				if(null != product_line_id && product_line_id.length > 0)
				{
					sb.append(" AND ( l.id = " + product_line_id[0]);
					for (int i = 1; i < product_line_id.length; i++) 
					{
						sb.append(" OR l.id = " + product_line_id[i]);
					}
					sb.append(" )");
				}
				if(null != title_id && title_id.length > 0)
				{
					sb.append(" AND ( tp.tp_title_id = " + title_id[0]);
					for (int i = 1; i < title_id.length; i++) 
					{
						sb.append(" OR tp.tp_title_id = " + title_id[i]);
					}
					sb.append(" )");
				}
				if(null != catalog_id && catalog_id.length > 0)
				{
					if(-1 != catalog_id[0])
					{
						sb.append(" AND ( pcl.search_rootid = " + catalog_id[0]);
					}
					else
					{
						sb.append(" AND ( pcl.search_rootid = 0");
					}
					for (int i = 1; i < catalog_id.length; i++) 
					{
						sb.append(" OR pcl.search_rootid = " + catalog_id[i]);
					}
					sb.append(" )");
				}
				sb.append("  GROUP BY q.pc_id ORDER BY q.pc_id");
				sb.append("		 ) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
				sb.append(" order by z.pc_id  desc");
				
			}
			//不管商品是否上传了文件
			else
			{
				//-- 不管商品是否上传了文件
				sb.append("select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc from ");
				sb.append("	(select p.pc_id , p.alive, p.catalog_id, p.heigth, p.length,");
				sb.append(" p.orignal_pc_id, p.p_name, p.union_flag, p.unit_name,p.unit_price, p.volume, p.weight, p.width,p.sn_size ,p.length_uom,p.weight_uom,p.price_uom,c.title");
				sb.append(" FROM product p LEFT JOIN product_catalog c ON p.catalog_id = c.id LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
				
				if(null != catalog_id && catalog_id.length > 0) {
					
					sb.append(" LEFT JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
				}
				
				sb.append(" LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
				
				sb.append("	WHERE 1=1 ");
				
				sb.append("	AND p.orignal_pc_id = 0 ");
				
				//0:正常登录  1：customer 2：carrier
				if(adminLoggerBean.getCorporationType() == 1){
					
					sb.append(" and exists ( select * from title_product tp where tp.tp_pc_id = p.pc_id and tp.tp_customer_id = " + adminLoggerBean.getCorporationId()  + ") ");
				}
				
				
				if(active > -1){
					sb.append(" AND p.alive = " + active);
				}
				
				if(null != product_line_id && product_line_id.length > 0)
				{
					sb.append(" AND ( l.id = " + product_line_id[0]);
					for (int i = 1; i < product_line_id.length; i++) 
					{
						sb.append(" OR l.id = " + product_line_id[i]);
					}
					sb.append(" )");
				}
				if(null != title_id && title_id.length > 0)
				{
					sb.append(" AND ( tp.tp_title_id = " + title_id[0]);
					for (int i = 1; i < title_id.length; i++) 
					{
						sb.append(" OR tp.tp_title_id = " + title_id[i]);
					}
					sb.append(" )");
				}
				if(null != catalog_id && catalog_id.length > 0)
				{
					if(-1 != catalog_id[0])
					{
						sb.append(" AND ( pcl.search_rootid = " + catalog_id[0]);
					}
					else
					{
						sb.append(" AND ( pcl.search_rootid = 0");
					}
					for (int i = 1; i < catalog_id.length; i++) 
					{
						sb.append(" OR pcl.search_rootid = " + catalog_id[i]);
					}
					sb.append(" )");
				}
				if(union_flag > -1)
				{
					sb.append("  AND p.union_flag = ").append(union_flag);
				}
				if(equal_or_not == YesOrNotKey.NO)
				{
					sb.append("				AND p.pc_id !="+product_id);
				}
				sb.append("  GROUP BY p.pc_id ORDER BY p.pc_id");
				sb.append("	 ) z");
				sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
				sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
				sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
				sb.append(" order by z.pc_id desc");
				
			}
			return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProprietaryMgrZyj.filterProductByPcLineCategoryTitleId(long catalog_id, long product_line_id, int union_flag, int product_file_type, int product_upload_status, long title_id, long adid, PageCtrl pc, long active):"+e);
		}
	}
	
	   /**
	    * 根据产品线，Customer,Title 检索产品分类
	    * @param pc_line_id
	    * @param customerId
	    * @param title_id
	    * @param pc
	    * @return
	    * @throws Exception
	    */
	   public DBRow[] findProductCatagorysByTitleIdAndCustomer(Long[] pc_line_id, long customerId,Long[] title_id, PageCtrl pc)
			     throws Exception
			   {
			     try
			     {
			       StringBuffer sql = new StringBuffer();
			       sql.append("SELECT pc.*,pld.name product_line_name FROM product_catalog pc LEFT JOIN product_line_define pld on pc.product_line_id = pld.id ");
			 
			       if ((title_id.length > 0 || customerId > 0))
			       {
			         sql.append(" LEFT JOIN title_product_catalog tpc ON tpc.tpc_product_catalog_id = pc.id");
			       }
			 
			       sql.append(" WHERE 1=1");
			 
			       if ((null != pc_line_id) && (pc_line_id.length > 0))
			       {
			         sql.append(" AND (pc.product_line_id = " + pc_line_id[0]);
			 
			         for (int i = 1; i < pc_line_id.length; ++i)
			         {
			           sql.append(" OR pc.product_line_id = " + pc_line_id[i]);
			         }
			         sql.append(" )");
			       }
			 
			       if ((null != title_id) && (title_id.length > 0))
			       {
			         sql.append(" AND (tpc.tpc_title_id = " + title_id[0]);
			 
			         for (int i = 1; i < title_id.length; ++i)
			         {
			           sql.append(" OR tpc.tpc_title_id = " + title_id[i]);
			         }
			         sql.append(" )");
			       }
			       if(customerId > 0){
			    	   sql.append(" AND tpc.tpc_customer_id =" + customerId);
			       }
			       sql.append(" ORDER BY pc.id");
			 
			       if (null != pc)
			       {
			         return this.dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			       }
			 
			       return this.dbUtilAutoTran.selectMutliple(sql.toString());
			     }
			     catch (Exception e) {
			       throw new Exception("FloorProprietaryMgrZyj.findParentProductCatagorysByTitleId(long adid, Long[] pc_line_id, Long[] title_id, PageCtrl pc):" + e);
			     }
			   }   
	   
	   /**
	    * 根据产品线，父产品分类，Customer,Title 检索产品分类
	    * @param adid
	    * @param pc_line_id
	    * @param category_parent_id
	    * @param title_id
	    * @param customerId
	    * @param begin
	    * @param length
	    * @param pc
	    * @return
	    * @throws Exception
	    */
	   public DBRow[] findProductCatagoryByLine(long adid, Long[] pc_line_id, Long[] category_parent_id,Long[] title_id, long customerId, int begin, int length, PageCtrl pc)
			     throws Exception
			   {
			     try
			     {
			       StringBuffer sql = new StringBuffer();
			       sql.append("select pc.id, pc.title name,pc.parentid from product_catalog pc ");
			       sql.append(" LEFT JOIN title_product_catalog tpc ON tpc.tpc_product_catalog_id = pc.id");
			       sql.append(" WHERE 1=1");
			       if ((null != category_parent_id) && (category_parent_id.length > 0))
			       {
			         if (-1L == category_parent_id[0].longValue())
			         {
			           sql.append(" AND (pc.parentid = 0");
			         }
			         else
			         {
			           sql.append(" AND (pc.parentid = " + category_parent_id[0]);
			         }
			         for (int i = 1; i < category_parent_id.length; ++i)
			         {
			           sql.append(" OR pc.parentid = " + category_parent_id[0]);
			         }
			         sql.append(" )");
			       }
			       else
			       {
			         sql.append(" AND pc.parentid = 0");
			       }
			       if ((null != pc_line_id) && (pc_line_id.length > 0))
			       {
			         sql.append(" AND (pc.product_line_id = " + pc_line_id[0]);
			         for (int i = 1; i < pc_line_id.length; ++i)
			         {
			           sql.append(" OR pc.product_line_id = " + pc_line_id[i]);
			         }
			         sql.append(" )");
			       }
			       if ((null != title_id) && (title_id.length > 0))
			       {
			         sql.append(" AND (tpc.tpc_title_id = " + title_id[0]);
			         for (int i = 1; i < title_id.length; ++i)
			         {
			           sql.append(" OR tpc.tpc_title_id = " + title_id[i]);
			         }
			         sql.append(" )");
			       }
			       if(customerId > 0){
			    	   sql.append(" AND tpc.tpc_customer_id = " + customerId);
			       }
			       
			       sql.append(" GROUP BY pc.id");
			       sql.append(" order by pc.title asc");
			 
			       return this.dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			     }
			     catch (Exception e)
			     {
			       throw new Exception("FloorProprietaryMgrZyj.findProductCatagoryParentsIdAndNameByTitleId(long adid, long[] pc_line_id, long[] category_parent_id, long[] pc_cata_id, long[] title_id, int begin, int length, PageCtrl pc):" + e);
			     }
			   } 	
	   
	   /**
	    * 分页查询与Customer,Title相关的，且产品名称包含关键字key的产品
	    * @param key
	    * @param customerId
	    * @param title_id
	    * @param pc
	    * @return
	    * @throws Exception
	    */
	   public DBRow[] findDetailProductLikeSearch(String key, Long customerId,Long[] title_id,  PageCtrl pc) throws Exception {
		     try
		     {
		       StringBuffer sql = new StringBuffer();
		       sql.append(" select p.pc_id, p.alive, p.catalog_id, p.heigth, p.length, p.orignal_pc_id, pcode_main.p_code as p_code, p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width,p.sn_size ,pcode_amazon.p_code AS p_code2, pcode_upc.p_code AS upc, p.length_uom,p.weight_uom,p.price_uom");
		       sql.append(" from product as p ");
		       sql.append(" JOIN product_code AS pcode_main ON p.pc_id = pcode_main.pc_id AND pcode_main.code_type= ").append(CodeTypeKey.Main);
		       sql.append(" LEFT JOIN product_code AS pcode_amazon ON p.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = ").append(CodeTypeKey.Amazon);
		       sql.append(" LEFT JOIN product_code AS pcode_upc ON p.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = ").append(CodeTypeKey.UPC);
		 
		       sql.append(" where ( p_name like '%" + key + "%' or pcode_main.p_code like '%" + key + "%' ) ");
		 
		       if ((null != title_id) && (title_id.length > 0))
		       {
		         StringBuffer inSql = new StringBuffer();
		 
		         for (Long one : title_id)
		         {
		           inSql.append("," + one);
		         }
		 
		         if(customerId > 0){
			         sql.append(" and exists ( select * from title_product tp where tp.tp_pc_id = p.pc_id and ( tp.tp_title_id in ( " + inSql.substring(1) + " ) ) and tp.tp_customer_id=" + customerId + " )");
		         }else{
			         sql.append(" and exists ( select * from title_product tp where tp.tp_pc_id = p.pc_id and ( tp.tp_title_id in ( " + inSql.substring(1) + " ) ) )");
		         }
		         
	          }else if(customerId > 0){
	        	  sql.append(" and exists ( select * from title_product tp where tp.tp_pc_id = p.pc_id and tp.tp_customer_id=" + customerId + " )");
	          }
		       sql.append(" order by p.pc_id desc");
		 
		       return this.dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		     }
		     catch (Exception e) {
		       throw new Exception("FloorProprietaryMgrZyj.findDetailProductLikeSearch(String key,long title_id, long adid,PageCtrl pc, long active) error:" + e);
		     }
	   }
	   
	   /**
	    * 找到与customer,title 相关的ProductLine
	    * @param pc_line_ids
	    * @param customerId
	    * @param title_ids
	    * @param begin
	    * @param length
	    * @param pc
	    * @return
	    * @throws Exception
	    */
	   public DBRow[] findProductLinesIdAndNameByTitleId(Long[] pc_line_ids,long customerId, Long[] title_ids, int begin, int length, PageCtrl pc)
			     throws Exception
			   {
			     try
			     {
			       StringBuffer sql = new StringBuffer();
			       sql.append("SELECT pld.id, pld.name FROM product_line_define pld");
			       sql.append(" LEFT JOIN title_product_line tpl ON pld.id = tpl.tpl_product_line_id");
			 
			       sql.append(" WHERE 1=1");
			       if ((null != pc_line_ids) && (pc_line_ids.length > 0))
			       {
			         sql.append(" AND (pld.id = " + pc_line_ids[0]);
			         for (int i = 1; i < pc_line_ids.length; ++i)
			         {
			           sql.append(" OR pld.id = " + pc_line_ids[i]);
			         }
			         sql.append(" )");
			       }
			       if ((null != title_ids) && (title_ids.length > 0))
			       {
			         sql.append(" AND (tpl.tpl_title_id = " + title_ids[0]);
			         for (int i = 1; i < title_ids.length; ++i)
			         {
			           sql.append(" OR tpl.tpl_title_id = " + title_ids[i]);
			         }
			         sql.append(" )");
			       }
			       if(customerId > 0){
			    	   sql.append(" AND tpl.tpl_customer_id = " + customerId);
			       }
			 
			       sql.append(" GROUP BY pld.id");
			       sql.append(" ORDER BY pld.id");
			       if (begin >= 0)
			       {
			         if (length > 0)
			         {
			           sql.append(" LIMIT " + begin + "," + length);
			         }
			 
			       }
			       else if (length > 0)
			       {
			         sql.append(" LIMIT " + length);
			       }
			 
			       if (null != pc)
			       {
			         return this.dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			       }
			 
			       return this.dbUtilAutoTran.selectMutliple(sql.toString());
			     }
			     catch (Exception e)
			     {
			       throw new Exception("FloorProprietaryMgrZyj.findProductLinesIdAndNameByTitleId(long adid, long pc_line_id, long title_id, int begin, int length, PageCtrl pc):" + e);
			     }
			   }
	   
	   /**
	    * 分页获取与产品线，产品分类，Customer,title 相关的产品
	    * @param catalog_id
	    * @param product_line_id
	    * @param customerId
	    * @param title_id
	    * @param pc
	    * @return
	    * @throws Exception
	    */
	   public DBRow[] filterProductByPcLineCategoryTitleId(Long[] catalog_id, Long[] product_line_id,long customerId, Long[] title_id, PageCtrl pc) throws Exception {

		     try
		     {
		       StringBuffer sb = new StringBuffer();
		 

		         sb.append("select z.*,pcode_main.p_code AS p_code, pcode_amazon.p_code AS p_code2,  pcode_upc.p_code AS upc from ");
		         sb.append("\t(select p.pc_id , p.alive, p.catalog_id, p.heigth, p.length,");
		         sb.append(" p.orignal_pc_id, p.p_name, p.union_flag, p.unit_name,p.unit_price, p.volume, p.weight, p.width,p.sn_size ,p.length_uom,p.weight_uom,p.price_uom,c.title");
		         sb.append(" FROM product p LEFT JOIN product_catalog c ON p.catalog_id = c.id LEFT JOIN product_line_define l ON l.id = c.product_line_id ");
		 
		         if ((null != catalog_id) && (catalog_id.length > 0))
		         {
		           sb.append(" LEFT JOIN pc_child_list AS pcl ON c.id = pcl.pc_id");
		         }
		 
		         sb.append(" LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
		 
		         sb.append("\tWHERE 1=1 ");
		         sb.append("\tAND p.orignal_pc_id = 0 ");
		 
		 
		         if ((null != product_line_id) && (product_line_id.length > 0))
		         {
		           sb.append(" AND ( l.id = " + product_line_id[0]);
		           for (int i = 1; i < product_line_id.length; ++i)
		           {
		             sb.append(" OR l.id = " + product_line_id[i]);
		           }
		           sb.append(" )");
		         }
		         if ((null != title_id) && (title_id.length > 0))
		         {
		           sb.append(" AND ( tp.tp_title_id = " + title_id[0]);
		           for (int i = 1; i < title_id.length; ++i)
		           {
		             sb.append(" OR tp.tp_title_id = " + title_id[i]);
		           }
		           sb.append(" )");
		         }
		         
		         if(customerId > 0){
		        	 sb.append(" AND tp.tp_customer_id=" + customerId);
		         }
		         
		         
		         if ((null != catalog_id) && (catalog_id.length > 0))
		         {
		           if (-1L != catalog_id[0].longValue())
		           {
		             sb.append(" AND ( pcl.search_rootid = " + catalog_id[0]);
		           }
		           else
		           {
		             sb.append(" AND ( pcl.search_rootid = 0");
		           }
		           for (int i = 1; i < catalog_id.length; ++i)
		           {
		             sb.append(" OR pcl.search_rootid = " + catalog_id[i]);
		           }
		           sb.append(" )");
		         }
		         sb.append("  GROUP BY p.pc_id ORDER BY p.pc_id");
		         sb.append("\t ) z");
		         sb.append(" JOIN product_code AS pcode_main ON z.pc_id = pcode_main.pc_id AND pcode_main.code_type = 1");
		         sb.append(" LEFT JOIN product_code AS pcode_amazon ON z.pc_id = pcode_amazon.pc_id AND pcode_amazon.code_type = 3");
		         sb.append(" LEFT JOIN product_code AS pcode_upc ON z.pc_id = pcode_upc.pc_id AND pcode_upc.code_type = 2");
		         sb.append(" order by z.pc_id desc");
		 
		       return this.dbUtilAutoTran.selectMutliple(sb.toString(), pc);
		     }
		     catch (Exception e)
		     {
		       throw new Exception("FloorProprietaryMgrZyj.filterProductByPcLineCategoryTitleId(long catalog_id, long product_line_id, int union_flag, int product_file_type, int product_upload_status, long title_id, long adid, PageCtrl pc, long active):" + e);
		     }
	   
	   }	   
}
