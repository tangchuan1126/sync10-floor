package com.cwc.app.floor.api.wfh;

import com.cwc.app.key.CheckInChildDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInTractorOrTrailerTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.SpaceRelationTypeKey;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.sun.org.apache.xpath.internal.operations.And;

public class FloorCheckInMgrWfh {
	
	private DBUtilAutoTran dbUtilAutoTran;
		
	
	/**
	 * 通过AccountId获取Packlist打印的模板
	 * @param accountId
	 * @param companyId
	 * @return 如果AccountId对应的模板存在则返回配置的模板，否则返回默认的模板
	 * @throws Exception
	 */
	public DBRow getPacklistPrintTemplate(String accountId,String companyId)throws Exception
	{
		try
		{
			String sql ="select * from config_account_print_template ";
			String condition =" account_id='"+accountId+"' ";
			if(companyId!=null && !"".equals(companyId)){
				condition+=" and company_id='"+companyId+"'";
			}
			sql+=" where ("+condition +") OR account_id is null";
			sql+=" order by id desc limit 1 ";
			DBRow drTemplate= dbUtilAutoTran.selectSingle(sql);
			sql ="select * from config_account_logo where account_id = '"+accountId+"'";
			DBRow drLogo= dbUtilAutoTran.selectSingle(sql);
			
			if(drLogo!=null){
				drTemplate.put("image_url", drLogo.getString("image_url"));
				drTemplate.put("width", drLogo.getString("width"));
				drTemplate.put("height", drLogo.getString("height"));
			}
			return drTemplate;
		}
		catch(Exception e)
		{
			 throw new Exception("FloorCheckInMgrTc getPacklistPrintTemplate"+e);
		}
	}
	
		/**
		 * 返回titleID
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public DBRow selectTitleIdByTitleName(String title_name)throws Exception
		{
			try
			{
				String sql ="select * from title where title_name='"+title_name+"'";
				
				return dbUtilAutoTran.selectSingle(sql);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb selectTitleIdByTitleName"+e);
			}
		}
		
		/**
		 * pallet Inventory
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public DBRow[]  getAllPalletInventory(long ps_id,int invenStatus,int title_id,PageCtrl pc)throws Exception
		{
			try
			{
				String sql = "SELECT pallet.pallet_id, pallet.pallet_type, pallet.pallet_count, pallet.damaged_count, pallet.ps_id, `storage`.title "
						+ " FROM pallet_inventroy pallet LEFT JOIN product_storage_catalog STORAGE ON pallet.ps_id = `storage`.id ";
				
				if(title_id!=0){
					sql += "INNER JOIN pallet_inventroy_title palletTitle ON palletTitle.pallet_id = pallet.pallet_id ";
				}
				
				if(invenStatus!=0){
					sql += " , (SELECT sum(pt.pallet_count) pallet_count, pt.pallet_id ,pt.title_id FROM pallet_inventroy_title pt INNER JOIN pallet_inventroy pallet ON pt.pallet_id = pallet.pallet_id  ";
					sql += " WHERE 1=1";
					if(ps_id!=0){
						sql += " AND pallet.ps_id = "+ps_id;
					}
					if(title_id!=0){
						sql += " AND pt.title_id = "+title_id;
					}
					sql += " GROUP BY pt.pallet_id ) p";
				}
				
				sql += " WHERE 1=1";
				
				if(invenStatus==1){
					sql += " and p.pallet_count > 0 AND pallet.pallet_id = p.pallet_id";
				}else if(invenStatus==2){
					sql += " and p.pallet_count = 0 AND pallet.pallet_id = p.pallet_id";
				}
				if( ps_id > 0 ){
					sql += " AND pallet.ps_id = "+ps_id;
				}
				if( title_id > 0 ){
					sql += " AND palletTitle.title_id = "+title_id;
				}
				sql +=" order by pallet.post_date desc";

				return dbUtilAutoTran.selectMutliple(sql,pc);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb getAllPalletInventory"+e);
			}
		}
		/**
		 * add pallet Inventory
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public long addPalletInventory(DBRow row) throws Exception {
			try {
				
				return  dbUtilAutoTran.insertReturnId("pallet_inventroy", row);

			} catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb addPalletInventory error:"+e);
			}
		}
		/**
		 * update pallet Inventory
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public long updatePalletInventory(long pallet_id,DBRow row)throws Exception{
			try{
				return this.dbUtilAutoTran.update("where pallet_id="+pallet_id, "pallet_inventroy", row);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb updatePalletInventory"+e);
			}
		}
		/**
		 * 查询pallet
		 * @param palletId
		 * @return
		 * @throws Exception
		 */
		public DBRow findPalletInventoryById(int palletId)throws Exception
		{
			try
			{
				String sql ="SELECT pallet.pallet_id,pallet.pallet_type,pallet.pallet_count,pallet.damaged_count,pallet.ps_id,`storage`.title from pallet_inventroy pallet LEFT JOIN product_storage_catalog storage on pallet.ps_id = `storage`.id where pallet_id ="+palletId;
				
				return dbUtilAutoTran.selectSingle(sql);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb findPalletInventoryById"+e);
			}
		}
		/**
		 * add pallet Inventory log
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public long addPalletInventoryLogs(DBRow row) throws Exception {
			try {
				
				return  dbUtilAutoTran.insertReturnId("pallet_inventroy_logs", row);

			} catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb addPalletInventoryLogs error:"+e);
			}
		}
		public long addPalletInventroyDetail(DBRow row) throws Exception {
			try {
				
				return  dbUtilAutoTran.insertReturnId("pallet_inventroy_title", row);

			} catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb addPalletInventroyDetail error:"+e);
			}
		}
		public long updatePalletInventoryDetail(long pallet_id,long titleId,DBRow row)throws Exception{
			try{
				return this.dbUtilAutoTran.update("where pallet_id="+pallet_id+" and title_id="+titleId, "pallet_inventroy_title", row);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb updatePalletInventory"+e);
			}
		}
		/**
		 * 查询pallet操作日志
		 * @param start_time
		 * @param end_time
		 * @param pallet_type
		 * @param ps_id
		 * @param pc
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findPalletInventoryLog(String start_time,String end_time,String pallet_type,long ps_id,PageCtrl pc)throws Exception
		{
			try
			{
				String sql ="SELECT log.pal_id, log.operation, log.post_date, log.damaged_quantity, log.quantity, log.ps_id, log.pallet_id, log.adid, pallet.pallet_type, `STORAGE`.title, t.title_id,t.title_name"
						+ " ,ad.employe_name FROM pallet_inventroy_logs log LEFT JOIN pallet_inventroy pallet ON log.pallet_id = pallet.pallet_id "
						+ " LEFT JOIN product_storage_catalog STORAGE ON `storage`.id = log.ps_id LEFT JOIN admin ad ON ad.adid = log.adid "
						+ " left join title t on t.title_id=log.title_id where 1=1 ";
				if(!StrUtil.isBlank(start_time)){
					sql +=" and log.post_date >='"+start_time+"'" ;
				}
				if(!StrUtil.isBlank(end_time)){
					sql +=" and log.post_date <='"+end_time+"'" ;
				}
				if(!StrUtil.isBlank(pallet_type)){
					sql +=" and pallet.pallet_type ="+pallet_type ;
				}
				if(ps_id>0){
					sql +=" and log.ps_id ="+ps_id;
				}
				sql +=" order by log.post_date desc";
				return dbUtilAutoTran.selectMutliple(sql, pc);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb findPalletInventoryLog"+e);
			}
		}
		public DBRow[] findPalletType()throws Exception
		{
			try
			{
				String sql ="select distinct pallet_type from pallet_inventroy";
				
				return dbUtilAutoTran.selectMutliple(sql);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb findPalletType"+e);
			}
		}
		public DBRow findPalletInventoryByPsIdAndPalletType(long ps_id, String palletType )throws Exception
		{
			try
			{
				String sql ="SELECT pallet_id,pallet_type,pallet_count,damaged_count,ps_id FROM pallet_inventroy where pallet_type = "+palletType+" AND ps_id = "+ps_id;
				
				return dbUtilAutoTran.selectSingle(sql);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb findPalletInventoryByPsIdAndPalletType"+e);
			}
		}
		
		public DBRow[] findPalletForTitleByStorageAndType(int palletId, long ps_id, int titleId,int invenStatus) throws Exception{ 
			
			try{
				String sql = "SELECT pallet.pit_id, pallet.title_id, t.title_name, pallet.pallet_count, pallet.damaged_count ,pallet.post_date "
						+  " FROM pallet_inventroy_title pallet LEFT JOIN title t ON t.title_id = pallet.title_id WHERE 1=1";
				if(palletId!=0){
					sql += " AND pallet.pallet_id = '"+palletId+"'";
				}
				if(titleId!=0){
					sql += " AND t.title_id = "+titleId;
				}
				
				if(invenStatus==1){
					sql += " AND pallet.pallet_count >"+0;
				}
				if(invenStatus == 2){
					sql += " AND pallet.pallet_count ="+0;
				}
				
				return  dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e){
				throw new Exception("findPalletForTitleByStorageAndType"+e);
			}
		}
		public DBRow findPalletCountByIdTypeTitle(long ps_id, String palletType, long titleId) throws Exception{
			try{
				String sql = "SELECT pallet.pallet_id, pallet.pallet_type,pallet.ps_id, pallet.pallet_count pallet_totality, pallet.damaged_count damaged_totality,p_title.title_id,p_title.pallet_count ,p_title.damaged_count "
						+ "FROM pallet_inventroy pallet INNER JOIN pallet_inventroy_title p_title ON p_title.pallet_id = pallet.pallet_id "
						+ "WHERE 1=1 ";
				if(ps_id!=0){
					sql += " And pallet.ps_id = "+ps_id;
				}
				if(!StrUtil.isBlank(palletType)){
					sql += " AND  pallet.pallet_type = '"+palletType+"'";
				}
				if(titleId!=0){
					sql += " AND p_title.title_id = "+titleId;
				}
				return  dbUtilAutoTran.selectSingle(sql);
			}catch (Exception e){
				throw new Exception("findPalletCountByIdTypeTitle"+e);
			}
			
		}
		public DBRow[] findAllTitle(String palletType, long ps_id) throws Exception {
			
				
			try{
				String sql = "SELECT t.title_id, t.title_name "
						+ "FROM title t ";
				    if(!(StrUtil.isBlank(palletType))||ps_id!=0){
					sql += " INNER JOIN pallet_inventroy_title pt ON pt.title_id = t.title_id";
					sql += " INNER JOIN pallet_inventroy pallet ON pt.pallet_id = pallet.pallet_id";
					if(!(StrUtil.isBlank(palletType))){
						sql += " AND pallet.pallet_type = '"+palletType+"'";
					}
					if(ps_id!=0){
						sql += "AND pallet.ps_id = "+ps_id;
					}
				}

					
					return  dbUtilAutoTran.selectMutliple(sql);
				}catch (Exception e){
					throw new Exception("findAllTitle"+e);
				}
				
		 }
		
		public DBRow findLoadBar(long load_bar_id)throws Exception{
			try{
				String sql="select * from load_bar where load_bar_id="+load_bar_id;
				return this.dbUtilAutoTran.selectSingle(sql);
			}catch(Exception e){
				throw new Exception("findLoadBar"+e);
			}
		}
	
		public DBRow[] getSealInventory(DBRow para, PageCtrl pc) throws Exception{
			try{
			String sql = "SELECT seal.seal_id, seal.seal_no, seal.seal_status, seal.import_time, "
					+ "(SELECT admin.account FROM admin WHERE seal.import_user = admin.adid ) import_user, "
					+ "seal.used_time, seal.assign_time, (SELECT admin.account FROM admin WHERE seal.assign_user = admin.adid ) assign_user,"
					+ " seal.return_time, (SELECT admin.account FROM admin WHERE seal.return_user = admin.adid ) return_user, ps.title, seal.ps_id "
					+ "FROM import_seal seal LEFT JOIN product_storage_catalog ps ON ps.id = seal.ps_id WHERE 1=1";
			
			int seal_status = para.get("seal_status", 0);
			long ps_id = para.get("ps_id", 0l);
			String import_start_time = para.get("import_start_time", "");
			String import_end_time = para.get("import_end_time", "");
			String used_start_time = para.get("used_start_time", "");
			String used_end_time = para.get("used_end_time", "");
			String return_start_time = para.get("return_start_time", "");
			String return_end_time = para.get("return_end_time", "");
			if(seal_status>0){
				sql += " AND seal.seal_status = "+seal_status;
			}
			if(ps_id>0){
				sql += " AND seal.ps_id = "+ps_id;
			}
			if(!StrUtil.isBlank(import_start_time)){
				sql += " AND seal.import_time > '"+import_start_time+"'";
			}
			if(!StrUtil.isBlank(import_end_time)){
				sql += " AND seal.import_time < '"+import_end_time+"'";
			}
			if(!StrUtil.isBlank(used_start_time)){
				sql += " AND seal.used_time > '"+used_start_time+"'";
			}
			if(!StrUtil.isBlank(used_end_time)){
				sql += " AND seal.used_time < '"+used_end_time+"'";
			}
			if(!StrUtil.isBlank(return_start_time)){
				sql += " AND seal.return_time > '"+return_start_time+"'";
			}
			if(!StrUtil.isBlank(return_end_time)){
				sql += " AND seal.return _time < '"+return_end_time+"'";
			}
			sql+=" ORDER BY seal.import_time DESC";
			return dbUtilAutoTran.selectMutliple(sql,pc);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb getSealInventory"+e);
			}
		}
		public DBRow[] getAllSealInventory(DBRow para) throws Exception{


			try{
			String sql = "SELECT seal.seal_id, seal.seal_no, seal.seal_status, seal.import_time, "
							+ "(SELECT admin.account FROM admin WHERE seal.import_user = admin.adid ) import_user, "
							+ "seal.used_time, seal.assign_time, (SELECT admin.account FROM admin WHERE seal.assign_user = admin.adid ) assign_user,"
							+ " seal.return_time, (SELECT admin.account FROM admin WHERE seal.return_user = admin.adid ) return_user, ps.title, seal.ps_id "
							+ "FROM import_seal seal LEFT JOIN product_storage_catalog ps ON ps.id = seal.ps_id WHERE 1=1";
					
					int seal_status = para.get("seal_status", 0);
					long ps_id = para.get("ps_id", 0l);
					String import_start_time = para.get("import_start_time", "");
					String import_end_time = para.get("import_end_time", "");
					String used_start_time = para.get("used_start_time", "");
					String used_end_time = para.get("used_end_time", "");
					String return_start_time = para.get("return_start_time", "");
					String return_end_time = para.get("return_end_time", "");
					if(seal_status>0){
						sql += " AND seal.seal_status = "+seal_status;
					}
					if(ps_id>0){
						sql += " AND seal.ps_id = "+ps_id;
					}
					if(!StrUtil.isBlank(import_start_time)){
						sql += " AND seal.import_time > '"+import_start_time+"'";
					}
					if(!StrUtil.isBlank(import_end_time)){
						sql += " AND seal.import_time < '"+import_end_time+"'";
					}
					if(!StrUtil.isBlank(used_start_time)){
						sql += " AND seal.used_time > '"+used_start_time+"'";
					}
					if(!StrUtil.isBlank(used_end_time)){
						sql += " AND seal.used_time < '"+used_end_time+"'";
					}
					if(!StrUtil.isBlank(return_start_time)){
						sql += " AND seal.return_time > '"+return_start_time+"'";
					}
					if(!StrUtil.isBlank(return_end_time)){
						sql += " AND seal.return _time < '"+return_end_time+"'";
					}
					
					sql+=" ORDER BY seal.import_time DESC";
					return dbUtilAutoTran.selectMutliple(sql);
				}catch(Exception e){
					throw new Exception("FloorCheckInMgrZwb getSealInventory"+e);
				}
		}
			
		/**
		 *添加seal inventory
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public long addSealInventory(DBRow row) throws Exception {
			try {
				
				return  dbUtilAutoTran.insertReturnId("import_seal", row);
				
			} catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb addSealInventory error:"+e);
			}
		}
		/**修改seal inventory
		 *  
		 * 
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public long updateSealInventory(long seal_id,DBRow row) throws Exception {
			try {
				
				return  dbUtilAutoTran.update(" WHERE seal_id="+seal_id, "import_seal", row);
				
			} catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb updateSealInventory error:"+e);
			}
		}

		//当当前的detail所属的资源为占用或者保留则查询space表中本资源下所有的task
		public DBRow[] selectSpaceBySpaceAndEntryId(int resources_id, long entry_id,int resources_type, int equipmentId)throws Exception{
			
			try{
				String sql = "SELECT details.* FROM space_resources_relation space JOIN door_or_location_occupancy_details details ON details.dlo_detail_id = space.relation_id WHERE space.relation_type="+SpaceRelationTypeKey.Task;

				if(resources_id>0){
					sql += " AND space.resources_id = "+resources_id;
				}
				if(entry_id>0){
					sql += " AND space.module_id = "+entry_id;
				}
				if(resources_type>0){
					sql += " AND space.resources_type = "+resources_type;
				}
				if(equipmentId>0){
					sql += " AND details.equipment_id="+equipmentId;
				}
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb selectSpaceBySpaceAndEntryId error:"+e);
			}
		}
		
		//查询为关闭的task
		public DBRow[] findNotCloseTaskByEquipmentId( long entry_id,int equipmentId)throws Exception{
					
			try{
				String sql = "SELECT details.* FROM  door_or_location_occupancy_details details WHERE 1=1";

				if(entry_id>0){
					sql += " AND details.dlo_id = "+entry_id;
				}
				if(equipmentId>0){
					sql += " AND details.equipment_id="+equipmentId;
				}
				sql += " AND details.number_status!="+CheckInChildDocumentsStatusTypeKey.CLOSE;
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findNotCloseTaskByEquipmentId error:"+e);
			}
		}

		//按照资源id 查找门的号码
		public DBRow findDoorByResourcesId(int resources_id)throws Exception{
			
			try{
				String sql = "SELECT door.* FROM storage_door door WHERE door.sd_id ="+resources_id;
				return this.dbUtilAutoTran.selectSingle(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findDoorByResourcesId error:"+e);
			}
			
		}


		//按照资源id 查找停车位的号码
		public DBRow findSpotByResourcesId(int resources_id)throws Exception{
			
			try{
				String sql = "SELECT yard.* FROM storage_yard_control yard WHERE yard.yc_id ="+resources_id;
				return this.dbUtilAutoTran.selectSingle(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findSpotByResourcesId error:"+e);
			}
		}
		//按照mainID查找设备（车头车尾
		public DBRow[] findTractorByEntryId(long entry_id, int equipment_type) throws Exception{
			
			try{
				String sql = "select space.srr_id, space.resources_type, space.resources_id,"
						+" IFNULL(syc.yc_no,sd.doorId) AS resources_id_name,"
						+ " entry.* FROM entry_equipment entry "
						+ " LEFT JOIN space_resources_relation space ON space.relation_id = entry.equipment_id AND space.relation_type = "+SpaceRelationTypeKey.Equipment
						+" left join storage_yard_control as syc on syc.yc_id = space.resources_id and space.resources_type ="+OccupyTypeKey.SPOT
						+" left join storage_door as sd on sd.sd_id = space.resources_id and space.resources_type = "+OccupyTypeKey.DOOR 
						+" WHERE 1=1";
				if(entry_id>0){
					sql += " AND entry.check_in_entry_id = "+entry_id;
				}
				if(equipment_type>0){
					sql += " AND entry.equipment_type="+equipment_type;
				}
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findTractorByEntryId error:"+e);
			}
		}
		
		//按照mainID查找设备（车头车尾
			public DBRow findTractorByEntryId(long entry_id, String equipment_no) throws Exception{
				
				try{
					String sql = "select space.srr_id, space.resources_type, space.resources_id,"
							+" IFNULL(syc.yc_no,sd.doorId) AS resources_id_name,"
							+ " entry.* FROM entry_equipment entry "
							+ " LEFT JOIN space_resources_relation space ON space.relation_id = entry.equipment_id AND space.relation_type = "+SpaceRelationTypeKey.Equipment
							+" left join storage_yard_control as syc on syc.yc_id = space.resources_id and space.resources_type ="+OccupyTypeKey.SPOT
							+" left join storage_door as sd on sd.sd_id = space.resources_id and space.resources_type = "+OccupyTypeKey.DOOR 
							+" WHERE 1=1";
					if (entry_id > 0) {
						sql += " AND entry.check_in_entry_id = " + entry_id;
					}
					if (equipment_no != null && !"".equals(equipment_no)) {
						sql += " AND entry.equipment_number='" + equipment_no + "'";
					}
					sql +=" ORDER BY entry.equipment_id DESC LIMIT 1 ";
					return this.dbUtilAutoTran.selectSingle(sql);
				}catch (Exception e) {
					throw new Exception("FloorCheckInMgrZwb findTractorByEntryId error:"+e);
				}
			}


		//查询所有已经释放的资源
		public DBRow[] findReleaseSpaceByMainId(long entry_id,int equipmentId) throws Exception{
			
			try{
				String sql = "SELECT details.dlo_detail_id,details.dlo_id,details.equipment_id,details.occupancy_type resources_type,details.rl_id resources_id,details.finish_time,details.occupancy_status FROM door_or_location_occupancy_details details WHERE 1=1 ";
				if(entry_id>0){
					sql += " AND details.dlo_id = "+entry_id;
				}
				if(equipmentId>0){
					sql += " AND details.equipment_id ="+equipmentId;
				}
				
				sql += " AND details.rl_id is not null GROUP BY details.occupancy_type,details.rl_id";
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findReleaseSpaceByMainId error:"+e);
			}
		}

		public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
			this.dbUtilAutoTran = dbUtilAutoTran;
		}
		
		//查询设备下指定设备占用的资源
		public DBRow[] findOccupancyEquipmentSpaceByMainId(long entry_id,int equipmentId) throws Exception{
			try{
				String sql = "SELECT space.srr_id, space.module_id,space.relation_type,space.relation_id,"
						+ " space.resources_id,"
						+ " space.resources_type,"
//						+ " case space.resources_type"
//						+ " WHEN "+OccupyTypeKey.DOOR+" THEN (SELECT door.doorId FROM storage_door door WHERE door.sd_id = space.resources_id)"
//						+ " WHEN "+OccupyTypeKey.SPOT+" THEN (SELECT yard.yc_no FROM storage_yard_control yard WHERE yard.yc_id = space.resources_id) ELSE NULL END AS resources_id_name ,"
						+ " IFNULL(syc.yc_no,sd.doorId) AS resources_id_name,"
						+ " space.occupy_status "
						+ " FROM space_resources_relation space "
						+ " JOIN entry_equipment entry ON space.relation_id = entry.equipment_id"
						+"  left join storage_yard_control as syc on syc.yc_id = space.resources_id and space.resources_type ="+OccupyTypeKey.SPOT
						+"  left join storage_door as sd on sd.sd_id = space.resources_id and space.resources_type = "+OccupyTypeKey.DOOR 
						+ " WHERE space.relation_type="+SpaceRelationTypeKey.Equipment;
				if(entry_id>0){
					sql += " AND space.module_id = "+entry_id;
				}
				if(equipmentId>0){
					sql += " AND entry.equipment_id ="+equipmentId;
				}
			 
				sql += " GROUP BY space.resources_type,space.resources_id";
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findOccupancyEquipmentSpaceByMainId error:"+e);
			}
		}
		//查询设备下指定设备占用的资源
		public DBRow[] findOccupancyTaskSpaceByMainId(long entry_id,int equipmentId) throws Exception{
			try{
				String sql = "SELECT space.srr_id,space.module_id,space.relation_type,space.relation_id,"
						+ " space.resources_type,"
						+ " space.resources_id,"
//						+ " case space.resources_type"
//						+ " WHEN "+OccupyTypeKey.DOOR+" THEN (SELECT door.doorId FROM storage_door door WHERE door.sd_id = space.resources_id)"
//						+ " WHEN "+OccupyTypeKey.SPOT+" THEN (SELECT yard.yc_no FROM storage_yard_control yard WHERE yard.yc_id = space.resources_id) ELSE NULL END AS resources_id_name ,"
						+ " IFNULL(syc.yc_no,sd.doorId) AS resources_id_name,"
						+ " space.occupy_status "
						+ " FROM space_resources_relation space JOIN door_or_location_occupancy_details details ON space.relation_id = details.dlo_detail_id"
						+ " left join storage_yard_control as syc on syc.yc_id = space.resources_id and space.resources_type ="+OccupyTypeKey.SPOT
						+ " left join storage_door as sd on sd.sd_id = space.resources_id and space.resources_type = "+OccupyTypeKey.DOOR 
						+ " WHERE 1=1 AND space.relation_type="+SpaceRelationTypeKey.Task;
				if(entry_id>0){
					sql += " AND details.dlo_id ="+entry_id;
				}
				if(equipmentId>0){
					sql += " AND details.equipment_id ="+equipmentId;
				}
				sql += " GROUP BY space.resources_type,space.resources_id";

				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findOccupancyTaskSpaceByMainId error:"+e);
			}
		}
		

		//按照设备查找所占用的资源
		public DBRow findEquipmentOccupancySpace(int equipmentId) throws Exception{
			
			try{
				String sql = "SELECT space.* FROM space_resources_relation space WHERE  space.relation_id ="+equipmentId+" and space.relation_type = "+SpaceRelationTypeKey.Equipment;

				return this.dbUtilAutoTran.selectSingle(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findEquipmentOccupancySpace error:"+e);
			}
		}




		//查找资源下task的个数

		public DBRow findTaskCountBySpace(int resourcesType, int resourcesId) throws Exception{
			
			try{
				String sql = "SELECT count(space.srr_id) FROM space_resources_relation space WHERE space.relation_type = "+SpaceRelationTypeKey.Task;
				if(resourcesType>0){
					sql += " AND space.resources_type ="+resourcesType;
				}
				if(resourcesId>0){
					sql += " AND space.resources_id ="+resourcesId;
				}

				return this.dbUtilAutoTran.selectSingle(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findTaskCountBySpace error:"+e);
			}
		}
		
		// 查找checkOut时带走的设备

		public DBRow[] findCtnrByCheckOutId(long entry_id) throws Exception{
			try{
//				String sql = "SELECT entry.* FROM entry_equipment entry WHERE"
//						 + "  entry.check_out_entry_id = "+entry_id ;// group by entry.check_in_entry_id";
				String sql = "select space.srr_id, space.resources_type, space.resources_id,"
						+" IFNULL(syc.yc_no,sd.doorId) AS resources_id_name,"
						+ " entry.* FROM entry_equipment entry "
						+ " LEFT JOIN space_resources_relation space ON space.relation_id = entry.equipment_id AND space.relation_type = "+SpaceRelationTypeKey.Equipment
						+" left join storage_yard_control as syc on syc.yc_id = space.resources_id and space.resources_type ="+OccupyTypeKey.SPOT
						+" left join storage_door as sd on sd.sd_id = space.resources_id and space.resources_type = "+OccupyTypeKey.DOOR 
						+" WHERE entry.equipment_type = "+CheckInTractorOrTrailerTypeKey.TRAILER
						+" AND entry.check_out_entry_id = "+entry_id;
				//+" and patrol_lost is null"

				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findCtnrByCheckOutId error:"+e);
			}
			
		}
		
		//查找每个order对应的palletCount
		public DBRow[] findWMSOrderPalletCount(long ps_id, String startTime, String endTime) throws Exception{
			
			try{
				String sql = "SELECT a.employe_name,dm.dlo_id,dd.dlo_detail_id,wlo.wms_order_id,wlo.order_numbers,  "
						   + "wlc.wms_pallet_type,sum(wlc.wms_pallet_type_count) AS wms_pallet_type_count,dd.company_id, "
						   + "dd.customer_id,dd.finish_time,wlo.reference_no,wlo.po_no,dd.number_type,dd.number, "
						   + "wlo.ship_to_name AS ship_to,CONCAT(wlo.ship_to_address1,' ',wlo.ship_to_address2) AS address, "
						   + "wlo.ship_to_city AS city,wlo.ship_to_state AS state,wlo.ship_to_zip_code AS postal_code,"
						   + "wlo.ship_to_country AS country,wlo.supplier_id,wlo.account_id FROM door_or_location_occupancy_main dm "
						   + "JOIN door_or_location_occupancy_details dd ON dm.dlo_id = dd.dlo_id "
						   + "JOIN wms_load_order_pallet_type_count wlc ON wlc.dlo_detail_id = dd.dlo_detail_id "
						   + "JOIN wms_load_order wlo ON wlo.wms_order_id = wlc.wms_order_id "
						   + "JOIN admin a ON a.adid = wlc.scan_adid WHERE wlo.`status` = 'closed' AND dm.ps_id = "+ps_id+" and wlc.wms_scan_number_type is null "
						   + "AND dd.finish_time >= '"+startTime+"' AND dd.finish_time <= '"+endTime+"'"+" "
						   + "GROUP BY dd.dlo_detail_id,wlo.wms_order_id,wlc.wms_pallet_type "+" "
						   + "ORDER BY dm.dlo_id,dd.dlo_detail_id,wlo.wms_order_id";
						
				
//				if(ps_id>0){
//					sql += " AND main.ps_id = "+ps_id;
//				}
//				if(!StrUtil.isBlank(startTime)){
//					sql += " AND details.finish_time > '"+startTime+"'";
//				}
//				if(!StrUtil.isBlank(endTime)){
//					sql += " AND details.finish_time <'"+endTime+"'";
//				}
//				sql += " GROUP BY  pallet.wms_order_id,pallet.wms_pallet_type "
//					+ " ORDER BY orders.wms_order_id DESC ";

				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findWMSOrderPalletCount error:"+e);
			}
			
			

		}
		
		//查找每个order对应的palletCount
		public DBRow[] findWMSOrderPalletCountOld(long ps_id, String startTime, String endTime) throws Exception{
			
			try{
				String sql = "SELECT ad.employe_name, main.dlo_id,details.dlo_detail_id,orders.wms_order_id,pallet.wms_pallet_type,"
						+ " sum(pallet.wms_pallet_type_count) wms_pallet_type_count ,details.company_id,"
						+ " details.customer_id,details.account_id,details.supplier_id,details.finish_time,"
						+ " orders.reference_no,orders.po_no,orders.order_numbers,details.number_type,details.number "
						+ " FROM door_or_location_occupancy_main main "
						+ " INNER JOIN door_or_location_occupancy_details details ON main.dlo_id = details.dlo_id "
						+ " INNER JOIN wms_load_order orders ON details.dlo_detail_id = orders.dlo_detail_id "
						+ " INNER JOIN wms_load_order_pallet_type_count pallet ON pallet.wms_order_id = orders.wms_order_id"
						+ " left JOIN `schedule` s ON s.associate_id = details.dlo_detail_id AND s.associate_process = 54 "
						+ " left JOIN schedule_sub ss ON s.schedule_id = ss.schedule_id"
						+ " left JOIN admin ad ON ss.schedule_execute_id = ad.adid"
						+ " WHERE main.dlo_id = details.dlo_id AND details.dlo_detail_id = orders.dlo_detail_id "
						+ " AND orders.`status` = 'closed' ";
						
				
				if(ps_id>0){
					sql += " AND main.ps_id = "+ps_id;
				}
				if(!StrUtil.isBlank(startTime)){
					sql += " AND details.finish_time > '"+startTime+"'";
				}
				if(!StrUtil.isBlank(endTime)){
					sql += " AND details.finish_time <'"+endTime+"'";
				}
				sql += " GROUP BY  pallet.wms_order_id,pallet.wms_pallet_type "
					+ " ORDER BY orders.wms_order_id DESC ";

				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findWMSOrderPalletCount error:"+e);
			}
			
			

		}

		//查找palletCount
		public DBRow[] findWMSPalletTypeCount(int orderId) throws Exception{
			
			try{
				String sql = "SELECT pallet.wms_order_id, pallet.wms_pallet_type, sum(pallet.wms_pallet_type_count)wms_pallet_type_count "
						+ " FROM wms_load_order_pallet_type_count pallet WHERE 1=1"
				 + " AND pallet.wms_order_id = "+orderId+" GROUP BY pallet.wms_pallet_type";
				
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findWMSPalletTypeCount error:"+e);
			}
		}

		//查找关闭资源下的task
		public DBRow[] findTaskClosedByEntry(long entry_id,int equipmentId) throws Exception {
			try{
				String sql = "SELECT details.*,"
//						+ " case details.occupancy_type"
//						+ " WHEN "+OccupyTypeKey.DOOR+" THEN (SELECT door.doorId FROM storage_door door WHERE door.sd_id = details.rl_id)"
//						+ " WHEN "+OccupyTypeKey.SPOT+" THEN (SELECT yard.yc_no FROM storage_yard_control yard WHERE yard.yc_id = details.rl_id) ELSE NULL END AS rl_id_name "
						+ " IFNULL(syc.yc_no,sd.doorId) AS rl_id_name"
						+ " FROM door_or_location_occupancy_details details "
						+ " left join storage_yard_control as syc on syc.yc_id = details.rl_id and details.occupancy_type ="+OccupyTypeKey.SPOT
						+ " left join storage_door as sd on sd.sd_id = details.rl_id and details.occupancy_type = "+OccupyTypeKey.DOOR
						+ " WHERE  details.rl_id is not null AND details.dlo_id = "+entry_id+" AND details.equipment_id = "+equipmentId+" ORDER BY details.occupancy_type,details.rl_id";

				
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findTaskClosedByEntry error:"+e);
			}
		}
		
		//查找设备下所有的customer或者title
		public DBRow[] findCustomerOrTitleByEntryEquipment(long entry_id,int equipmentId) throws Exception {
			
			try{
				String sql = "SELECT DISTINCT dd.dlo_id,dd.dlo_detail_id,dd.equipment_id,dd.customer_id,' ' AS supplier_id FROM door_or_location_occupancy_details dd"
						+ " WHERE dd.dlo_id = "+entry_id+" AND dd.equipment_id = "+equipmentId+" AND dd.customer_id != '' GROUP BY dd.customer_id"
						+ " UNION "
						+ " SELECT DISTINCT dd.dlo_id,dd.dlo_detail_id,dd.equipment_id,'' AS customer_id,dd.supplier_id FROM door_or_location_occupancy_details dd"
						+ " WHERE dd.dlo_id = "+entry_id+" AND dd.equipment_id = "+equipmentId+" AND dd.supplier_id !='' GROUP BY dd.supplier_id";
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findCustomerOrTitleByEntryEquipment error:"+e);
			}
		}	
		//查找设备下的task 个数
		public DBRow findTaskCountByEquipmentId(long entry_id,int equipment_id)  throws Exception{
			try{
				String sql = "SELECT  count(details.dlo_detail_id) count "
						+ " FROM door_or_location_occupancy_details details "
						+ " where details.equipment_id = "+equipment_id;
				return this.dbUtilAutoTran.selectSingle(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findTaskCountByEquipmentId error:"+e);
			}
		}
		/**
		 * 添加loadbar
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public long saveLoadBar(DBRow para) throws Exception{
			try{
				
				return this.dbUtilAutoTran.insertReturnId("load_bar_use", para);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb saveLoadBar error:"+e);
			}
		}
		
		//查找设备下的loadBar
		public DBRow[] findLoadBarByMainId(long dlo_id,int equipment_id) throws Exception{
			try{
				String sql = "SELECT lbu.*,lb.load_bar_name FROM load_bar_use lbu "
						+ "INNER JOIN load_bar lb ON lb.load_bar_id = lbu.load_bar_id "
						+ "WHERE lbu.dlo_id = "+dlo_id+" AND lbu.equipment_id= "+equipment_id;
				
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findLoadBarByMainId error:"+e);
			}
		}

		//查询所有的taskAppointment
		public DBRow[] findAllTaskByMainId(long entry_id) throws Exception{
			try{
				String sql = "SELECT details.* "
						   + " FROM door_or_location_occupancy_details details "
						   + " WHERE details.dlo_id = "+entry_id+"  "
						   + "GROUP BY details.number order by details.dlo_detail_id";
				
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findAllTaskByMainId error:"+e);
			}
			
		}
		//查询所有的taskAppointment
		public DBRow[] findAllTaskCountByMainId(long entry_id) throws Exception{
			try{
				String sql = "SELECT details.* "
						   + " FROM door_or_location_occupancy_details details "
						   + " WHERE details.number_status in("+CheckInChildDocumentsStatusTypeKey.UNPROCESS+","+CheckInChildDocumentsStatusTypeKey.PROCESSING+") "
						   + " and details.dlo_id = "+entry_id+" and( details.is_forget_task IS NULL OR  details.is_forget_task <> 1)";
						  
				
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findAllTaskByMainId error:"+e);
			}
			
		}
		//查询所有的taskAppointment
		public DBRow[] findTaskAppointmentByMainId(long entry_id) throws Exception{
			try{
				String sql = "SELECT details.* "
						   + " FROM door_or_location_occupancy_details details "
						   + " WHERE details.dlo_id = "+entry_id+"  "
						   + " GROUP BY details.number order by details.appointment_date";
				
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findTaskAppointmentByMainId error:"+e);
			}
			
		}
		// 查询忘了checkout的设备（车尾
		public DBRow[] findForgetCheckOutEquipmentByCtnr(String ctnr, String lp,long dlo_id,long ps_id) throws Exception{
			try{
				String sql ="SELECT entry.*,space.* "
						+ " FROM entry_equipment entry "
						+ " LEFT JOIN space_resources_relation space ON space.relation_id = entry.equipment_id AND space.relation_type="+SpaceRelationTypeKey.Equipment
						+ " WHERE entry.equipment_status!="+CheckInMainDocumentsStatusTypeKey.LEFT
						+ " AND entry.check_in_entry_id !="+dlo_id
						+ " AND entry.ps_id="+ps_id;
				sql += " and ((1=1)";
				if(!StrUtil.isBlank(lp)){
					sql += " and (entry.equipment_number ='"+lp+"' and entry.equipment_type = "+CheckInTractorOrTrailerTypeKey.TRACTOR+")";
				}
				if(!StrUtil.isBlank(ctnr)){
					if(!StrUtil.isBlank(lp)){
						sql += " or ";
					}else{
						sql +=" and ";
					}
					sql += " entry.equipment_number ='"+ctnr+"' and entry.equipment_type = "+CheckInTractorOrTrailerTypeKey.TRAILER;
				}
				sql+=" )";
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findAllTaskByMainId error:"+e);
			}
		}
		// 查询忘了checkout的设备（车尾
				public DBRow[] findForgetCheckOutEquipmentByEquipmentIds(String equipmentIds, long dlo_id,long ps_id) throws Exception{
					try{
						String sql ="SELECT entry.*,space.* "
								+ " FROM entry_equipment entry "
								+ " LEFT JOIN space_resources_relation space ON space.relation_id = entry.equipment_id AND space.relation_type="+SpaceRelationTypeKey.Equipment
								+ " WHERE entry.equipment_status!="+CheckInMainDocumentsStatusTypeKey.LEFT
								+ " AND entry.equipment_id in ("+equipmentIds+")"
								+ " AND entry.ps_id="+ps_id;
						return this.dbUtilAutoTran.selectMutliple(sql);
					}catch (Exception e) {
						throw new Exception("FloorCheckInMgrZwb findAllTaskByMainId error:"+e);
					}
				}
		
		//查询通知
		public DBRow[] findScheduleByMainId(long entry_id , int scheduleType) throws Exception{
			
			try{
				String sql = "SELECT sc.*,details.* FROM `schedule` sc "
						+ "LEFT JOIN door_or_location_occupancy_details details ON details.dlo_detail_id = sc.associate_id "
						+ "LEFT JOIN door_or_location_occupancy_main main ON main.dlo_id = sc.associate_id "
						+ " WHERE sc.associate_main_id = "+entry_id+" and sc.associate_type="+ModuleKey.CHECK_IN;
				if(scheduleType==53||scheduleType==52)
				{
					sql += " AND sc.associate_process = "+scheduleType;
				}
				else if(scheduleType==56||scheduleType==57)
				{ //gate 发给windos 56  |||| gate 发给warehouse  57
					sql += " AND sc.associate_process in (56,57,59)  order by sc.associate_process";
				}

				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findScheduleByMainId error:"+e);
			}
		}
		//查询通知
		public DBRow[] findScheduleCountByMainId(long entry_id , int scheduleType) throws Exception{
			
			try{
				String sql = "SELECT sc.* FROM `schedule` sc "
						+ " join door_or_location_occupancy_details d on d.dlo_detail_id = sc.associate_id"
						+ " WHERE sc.associate_main_id = "+entry_id+" and sc.associate_type="+ModuleKey.CHECK_IN
						+ " and sc.associate_process in (52,57)"
						+ " group by associate_id";
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findScheduleByMainId error:"+e);
			}
		}
		//查询通知的通知人
		public DBRow[] findScheduleUserByScheduleId(int schdule_id) throws Exception{
			
			try{
//				String sql = "SELECT ss.schedule_id,ss.schedule_execute_id, "
//						+ " (SELECT a.employe_name FROM admin a WHERE a.adid = ss.schedule_execute_id) schedule_execute_name"
//						+ " ,ss.schedule_state,ss.schedule_sub_id,ss.is_task_finish,ss.schedule_finish_adid"
//						+ " , (SELECT a.employe_name FROM admin a WHERE a.adid = ss.schedule_finish_adid) schedule_finish_name "
//						+ " FROM schedule_sub ss WHERE ss.schedule_id ="+schdule_id;
				
				String sql = "SELECT "
							+"ss.schedule_id,"
							+"ss.schedule_execute_id,"
							+"executeAdmin.employe_name as schedule_execute_name,"
							+"ss.schedule_state,"
							+"ss.schedule_sub_id,"
							+"ss.is_task_finish,"
							+"ss.schedule_finish_adid,"
							+"finishAdmin.employe_name as schedule_finish_name "
						+"FROM schedule_sub ss "
						+"join admin as executeAdmin on executeAdmin.adid = ss.schedule_execute_id "
						+"left join admin as finishAdmin on finishAdmin.adid = ss.schedule_finish_adid "
						+" WHERE ss.schedule_id ="+schdule_id;
				
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb findScheduleUserByScheduleId error:"+e);
			}
		}
		
		public DBRow findScheduleByScheduleId(int scheduleId) throws Exception{
			try{
				String sql = "SELECT sc.*,details.* FROM `schedule` sc "
						+ "LEFT JOIN door_or_location_occupancy_details details ON details.dlo_detail_id = sc.associate_id "
						+ "LEFT JOIN door_or_location_occupancy_main main ON main.dlo_id = sc.associate_id "
						+ " WHERE sc.schedule_id = "+scheduleId;
				  return this.dbUtilAutoTran.selectSingle(sql);
			   }catch (Exception e) {
				   throw new Exception("FloorCheckInMgrWfh findScheduleByScheduleId error:"+e);
			   }
		}

//		* 按照taskId查询warehouse的通知
		public DBRow[] findScheduleByDetailId(int detail_id, int scheduleType) throws Exception{
			
			try{
	    		
				String sql = "SELECT sc.*,detail.*,"
						+ " finishAdmin.employe_name schedule_finish_name,"
						+ " executeAdmin.employe_name schedule_execute_name "
						+ " FROM `schedule` sc INNER JOIN schedule_sub ss ON ss.schedule_id = sc.schedule_id "
						+ " JOIN door_or_location_occupancy_details detail ON detail.dlo_detail_id = sc.associate_id"
						+ " JOIN admin executeAdmin ON executeAdmin.adid = ss.schedule_execute_id"
						+ " LEFT JOIN admin finishAdmin ON finishAdmin.adid = ss.schedule_finish_adid"
						+ " WHERE sc.associate_process = "+scheduleType+" AND sc.associate_id = "+detail_id;
	    		return this.dbUtilAutoTran.selectMutliple(sql);
	    	}catch(Exception e){
	    		throw new Exception("FloorCheckInMgrZwb findScheduleByDetailId"+e);
	    	}
		}
		
		//	按照设备ID查找设备
		public DBRow findhEquipmentByEquipmentId(int equipmentId) throws Exception {
			try{
				String sql = "select e.* from entry_equipment e where e.equipment_id="+equipmentId;

				  return this.dbUtilAutoTran.selectSingle(sql);
			   }catch (Exception e) {
				   throw new Exception("FloorCheckInMgrWfh findhEquipmentByEquipmentId error:"+e);
			   }
		}
		
		//查找所有的loadBarInventory
		public DBRow[] getLoadBarInventory(DBRow para, PageCtrl pc) throws Exception{
			
			
			try{
					long ps_id = para.get("ps_id", 0L);
					int customer_key = para.get("customer_key", 0);
					int load_bar_id = para.get("load_bar_id", 0);
					String sql = "select lbi.*,lb.load_bar_name,ci.customer_id,storage.title "
							+ " FROM load_bar_inventory lbi JOIN load_bar lb ON lb.load_bar_id = lbi.load_bar_id "
							+ " LEFT JOIN product_storage_catalog storage on storage.id = lbi.ps_id "
							+ " LEFT JOIN customer_id ci ON ci.customer_key = lbi.customer_key WHERE 1=1";
					if(ps_id>0){
						sql += " AND lbi.ps_id = "+ps_id;
					}
					if(customer_key>0){
						sql += " AND lbi.customer_key = "+customer_key;
					}
					if(load_bar_id>0){
						sql += " AND lbi.load_bar_id ="+load_bar_id;
					}
					sql += " ORDER BY lbi.load_bar_inventory_id DESC";
				  return this.dbUtilAutoTran.selectMutliple(sql, pc);
			   }catch (Exception e) {
				   throw new Exception("FloorCheckInMgrWfh getLoadBarInventory error:"+e);
			   }
		}
		
		//查找所有的customer
		public DBRow[] findAllCustomer() throws Exception{
			try{
				String sql = "SELECT c.* FROM customer_id c";

				  return this.dbUtilAutoTran.selectMutliple(sql);
			   }catch (Exception e) {
				   throw new Exception("FloorCheckInMgrWfh findAllCustomer error:"+e);
			   }
		}
		
		//按照loadBarName查找load_bar
		public DBRow findLoadBarByName(String loadBarName) throws Exception{
			try{
					String sql = "SELECT lb.* FROM load_bar lb WHERE lb.load_bar_name='"+loadBarName+"'";
				  return this.dbUtilAutoTran.selectSingle(sql);
			   }catch (Exception e) {
				   throw new Exception("FloorCheckInMgrWfh findLoadBarByName error:"+e);
			   }
			
		}
		
		
		//按照loadBarId、customerId查找是否有这种loadBarInventory
		public DBRow findLoadBarInventoryByLoadBarId(int loadBarId,int customerKey) throws Exception{
			try{
				String sql = "SELECT lbi.* FROM load_bar_inventory lbi WHERE lbi.load_bar_id = "+loadBarId+" AND lbi.customer_key = "+customerKey;

				  return this.dbUtilAutoTran.selectSingle(sql);
			   }catch (Exception e) {
				   throw new Exception("FloorCheckInMgrWfh findLoadBarInventoryByLoadBarId error:"+e);
			   }
		}
		
		
		//添加load_bar    
		public long addLoadBar(DBRow data) throws Exception{
			try{
				return this.dbUtilAutoTran.insertReturnId("load_bar", data);
			   }catch (Exception e) {
				   throw new Exception("FloorCheckInMgrWfh addLoadBar error:"+e);
			   }
		}
		
		//update  loadBarInventory
		public long updateLoadBarInventory(int loadBarInventoryId, DBRow data) throws Exception{
			try{
				return this.dbUtilAutoTran.update(" where load_bar_inventory_id = "+loadBarInventoryId, "load_bar_inventory", data);
			   }catch (Exception e) {
				   throw new Exception("FloorCheckInMgrWfh updateLoadBarInventory error:"+e);
			   }
		}
		
		//add  loadBarInventory
		public long addLoadBarInventory(DBRow date) throws Exception{
			try{
				return this.dbUtilAutoTran.insertReturnId("load_bar_inventory", date);
			   }catch (Exception e) {
				   throw new Exception("FloorCheckInMgrWfh addLoadBarInventory error:"+e);
			   }
		}
		//上传report
		public int upLoadReport(String ids,DBRow row) throws Exception{
			try{
				return this.dbUtilAutoTran.update(" where file_id in("+ids+")", "file", row);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh upLoadReport error:"+e);
			}
		}
		//下载report
		public DBRow downLoadReport(long fileId) throws Exception{
			try{
				return this.dbUtilAutoTran.selectSingle("select * from file where file_id = "+fileId);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh upLoadReport error:"+e);
			}
		}
		//查询report
		public DBRow[] getAllReport(int fileWithType) throws Exception{
			try{
				return this.dbUtilAutoTran.selectMutliple("select * from file where file_with_type = "+fileWithType);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh upLoadReport error:"+e);
			}
		}
		//查询companyId
		public DBRow[] findCompanyIdByPsId(long ps_id) throws Exception{
			try{
				return this.dbUtilAutoTran.selectMutliple("SELECT * FROM config_search_warehouse_location WHERE ps_id ="+ps_id);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh upLoadReport error:"+e);
			}
		}
		public DBRow[] findTaskByNumber(String number,int number_type) throws Exception{
			try{
				String sql = "SELECT * FROM door_or_location_occupancy_details dd WHERE dd.number = '"+number+"' and dd.number_type="+number_type;

				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh findTaskByNumber error:"+e);
			}
		}
		//查找tasks
		public DBRow[] findTaskByIds(String details) throws Exception{
			try{
				String sql = "SELECT * FROM door_or_location_occupancy_details dd WHERE dd.dlo_detail_id in("+details+")";

				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh findTaskByIds error:"+e);
			}
		}
		//查找tasks
		public DBRow[] getAllCustomer() throws Exception{
			try{
				String sql="SELECT DISTINCT dd.customer_id FROM door_or_location_occupancy_details dd WHERE dd.customer_id != ''";

				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh getAllCustomer error:"+e);
			}
		}

		public DBRow[] getReceiveLinesInfoByDetailId(String receiptNo, String companyID) throws Exception {
			try{
				String sql="SELECT rl.item_id,lot_no,count(con.con_id) as pallets,(goods_total + IFNULL(damage_total,0)) as received_qty,damage_total,goods_total, "+
						    "expected_qty,rl.receipt_line_id ,is_check_sn,rl.status,scan_start_time FROM receipt_lines rl "+
							"JOIN receipt_rel_container rlc on rl.receipt_line_id=rlc.receipt_line_id "+
							"JOIN container con ON con.con_id=rlc.plate_no AND con.is_delete=0 "+
							"WHERE rl.company_id = '"+companyID+"' and rl.receipt_no= '"+receiptNo+"' and (rlc.damage_qty!=0 or rlc.normal_qty!=0 ) "+
						    "GROUP BY receipt_line_id";

				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh getReceiveLinesInfoByDetailId error:"+e);
			}
			
		}

		public DBRow[] getReceiptByDetailId(String receiptNo, String companyID) throws Exception {
			try{
				String sql="SELECT receipt_no,count(rlc.plate_no) as total_pallets,expected_qty,(goods_total + IFNULL(damage_total,0)) as received_qty FROM receipt_lines rl "+
							"JOIN receipt_rel_container rlc on rl.receipt_line_id=rlc.receipt_line_id "+
						    "WHERE rl.company_id = '"+companyID+"' and rl.receipt_no= '"+receiptNo+"' and (rlc.damage_qty!=0 or rlc.normal_qty!=0 ) "+
						    "GROUP BY rl.receipt_line_id";
				
				
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh getReceiptByDetailId error:"+e);
			}
		}
		public DBRow[] getReceivePalletsInfoByDetailId(long receipt_line_id) throws Exception {
			try{
				String sql="SELECT container as plate_no "
						+ ",pallet_type,length,width,height,normal_qty,damage_qty,stock_out_qty,rlc.detail_id,goods_type,is_partial,con.con_id "
						+ ",con.container_type,c.quantity "+
							"FROM receipt_lines rl "+
							"JOIN receipt_rel_container rlc ON rl.receipt_line_id = rlc.receipt_line_id "+
							"JOIN container_config cc on cc.container_config_id=rlc.container_config_id  "+
							"JOIN container con on con.con_id=rlc.plate_no AND con.is_delete=0 "
							+" LEFT JOIN cc_from c ON c.container_id = con.con_id "+
							"WHERE rl.receipt_line_id = "+receipt_line_id+" and (rlc.damage_qty!=0 or rlc.normal_qty!=0 ) order by goods_type,container";

				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh getReceivePalletsInfoByDetailId error:"+e);
			}
			
		}

		/**
		 * 查找wms_load_order
		 * @param detail_id
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findWMSLoadOrder(String detail_id) throws Exception{
			try{
				String sql = "SELECT DISTINCT wlo.* FROM wms_load_order_pallet_type_count wlc"
						+ " JOIN wms_load_order wlo ON wlo.wms_order_id = wlc.wms_order_id WHERE wlc.dlo_detail_id in( "+detail_id+")";
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh findWMSLoadOrder error:"+e);
			}
		}
		/**
		 * 查找wms_load_order
		 * @param detail_id
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findWMSLoadOrderMasterBol(String detail_ids) throws Exception{
			try{
				String sql = "SELECT DISTINCT wlb.* FROM wms_load_order_pallet_type_count wlc"
						+ " JOIN wms_load_order wlo ON wlo.wms_order_id = wlc.wms_order_id"
						+ " JOIN wms_load_master_bol wlb ON wlb.wms_master_bol_id = wlo.master_bol_id"
						+ " WHERE wlc.dlo_detail_id in ("+detail_ids+")";
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh findWMSLoadOrderMasterBol error:"+e);
			}
		}
		/**
		 * 更新wms_load_order
		 * @param orderIds
		 * @param upRow
		 * @return
		 * @throws Exception
		 */
		public long updateWMSLoadOrder(String orderIds,DBRow upRow) throws Exception{
			try{
				return this.dbUtilAutoTran.update("where wms_order_id in("+orderIds+")", "wms_load_order", upRow);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh updateWMSLoadOrder error:"+e);
			}
		}
		/**
		 * 更新wms_order_master_bol
		 * @param masterBolIds
		 * @param upRow
		 * @return
		 * @throws Exception
		 */
		public long updateWMSLoadOrderMasterBOL(String masterBolIds,DBRow upRow) throws Exception{
			try{
				return this.dbUtilAutoTran.update("where wms_master_bol_id in("+masterBolIds+")", "wms_load_master_bol", upRow);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh updateWMSLoadOrder error:"+e);
			}
		}
		/**
		 * 查找wms_load_order
		 * @param detail_id
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findWMSLoadOrderByMasterBOL(String master_bol_ids) throws Exception{
			try{
				String sql = "SELECT DISTINCT wlo.* FROM wms_load_master_bol wlb"
						+ " JOIN wms_load_order wlo ON wlo.master_bol_id = wlb.wms_master_bol_id WHERE wlb.wms_master_bol_id in("+master_bol_ids+")";
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh findWMSLoadOrderByMasterBOL error:"+e);
			}
		}
		
		/**
		 * 查找扫过托盘的load按照load号
		 * @param number
		 * @param number_type
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findLoadOnLoadingByLoadNo(String number,int number_type) throws Exception{
			try{
				String sql = "SELECT DISTINCT dd.* FROM door_or_location_occupancy_details dd "
						+ " JOIN wms_load_order_pallet_type_count wlc ON wlc.dlo_detail_id = dd.dlo_detail_id"
						+ " WHERE dd.number_status = "+number_type+" AND dd.number = '"+number+"'";
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh findLoadOnLoadingByLoadNo error:"+e);
			}
		}
		
		/**
		 * 查找appointment_temp 是否已存在相同的rn
		 * @param number
		 * @param number_type
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findAppointmentInBoundByRN(String number,String company_id) throws Exception{
			try{
				String sql = "SELECT apt.* FROM appointment_temp apt WHERE apt.receipt_no = '"+number+"' AND company_id = '"+company_id+"'";
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh findAppointmentInBoundByRN error:"+e);
			}
		}
		/**
		 * 查找appointment_temp 是否已存在相同的load
		 * @param number
		 * @param number_type
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findAppointmentOutBoundByLoadNo(String number,String company_id) throws Exception{
			try{
				String sql = "SELECT apt.* FROM appointment_temp apt WHERE apt.load_no = '"+number+"' AND company_id = '"+company_id+"'";
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh findAppointmentOutBoundByLoadNo error:"+e);
			}
		}
		/**
		 * 删除appointment_temp
		 * @param number
		 * @param number_type
		 * @return
		 * @throws Exception
		 */
		public long deleteAppointmentTemp(long appointment_id) throws Exception{
			try{
				return this.dbUtilAutoTran.delete("where appointment_id = "+appointment_id, "appointment_temp");
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh findAppointmentOutBoundByLoadNo error:"+e);
			}
		}
		/**
		 * 查找sn
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findSNByConID(long con_id,PageCtrl pc) throws Exception{
			try{
				String sql = "SELECT sp.*,ad.employe_name FROM serial_product sp LEFT JOIN admin ad ON ad.adid = sp.update_user WHERE at_lp_id ="+con_id;
				return this.dbUtilAutoTran.selectMutliple(sql, pc);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh findAppointmentOutBoundByLoadNo error:"+e);
			}
		}

		/**
		 * 查找exception并且扫描过的rn
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findReceivedRNByRn(String rn_no,int number_type) throws Exception{
			try{
				String sql = "SELECT dd.* FROM door_or_location_occupancy_details dd"
						+ " JOIN receipt_lines rl ON rl.receipt_no = dd.receipt_no"
						+ " JOIN receipt_rel_container rrc ON rrc.receipt_line_id = rl.receipt_line_id"
						+ " WHERE rrc.damage_qty+rrc.normal_qty > 0 AND rl.receipt_no = '"+rn_no+"' AND dd.number_type = "+number_type;
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrWfh findReceivedRNByRn error:"+e);
			}
		}
		
		//查找customer 
		public DBRow[] getCustomerByCustomer() throws Exception{
			try{
				String sql = "SELECT ci.* FROM customer_id ci";
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.getCustomerByCustomer error:" + e);
			}
		}
		//查找small parcel carrier
		public DBRow[] getSmallParcelCarrier() throws Exception{
			try{
				String sql = "SELECT spc.* FROM small_parcel_carrier spc ";
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.getSmallParcelCarrier error:" + e);
			}
		}

		/**
		 * 更新equipment
		 * @param entry_id
		 * @param equipment_type
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public long updateEquipmentPurposeByEntryID(long entry_id,int equipment_type,DBRow para) throws Exception{
			try{
				return dbUtilAutoTran.update("where check_in_entry_id ="+entry_id+" and equipment_type = "+equipment_type, "entry_equipment", para);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.updateEquipmentPurposeByEntryID error:" + e);
			}
		}
		/**
		 * 更新equipment
		 * @param entry_id
		 * @param equipment_type
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public long updateEquipmentPurposeByEquipmentID(String equipment_id,int equipment_type,DBRow para) throws Exception{
			try{
				return dbUtilAutoTran.update("where equipment_id in ("+equipment_id+") and equipment_type = "+equipment_type, "entry_equipment", para);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.updateEquipmentPurposeByEntryID error:" + e);
			}
		}
		
		/**
		 * 查询task占用的资源
		 * @param entry_id
		 * @param equipment_type
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findTaskOccupanyResources(long detail_id) throws Exception{
			try{
				String sql = "SELECT srr.* FROM space_resources_relation srr WHERE srr.relation_type = 2 AND relation_id ="+detail_id;
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.findTaskOccupanyResources error:" + e);
			}
		}
		
		/**
		 * 更新 wms_load_order_pallet_type
		 * @param entry_id
		 * @param equipment_type
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public long updateWMSLoadOrderPalletBylrId(long lr_id,DBRow para) throws Exception{
			try{
				return dbUtilAutoTran.update("where lr_id = "+lr_id, "wms_load_order_pallet_type_count", para);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.updateWMSLoadOrderPalletBylrId error:" + e);
			}
		}
		
		/**
		 * 更新 wms_load_order_pallet_type
		 * @param entry_id
		 * @param equipment_type
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public long updateWMSLoadOrderPalletByWmsOrderTypeId(long wms_order_type_id,DBRow para) throws Exception{
			try{
				return dbUtilAutoTran.update("where wms_order_type_id = "+wms_order_type_id, "wms_load_order_pallet_type_count", para);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.updateWMSLoadOrderPalletByWmsOrderTypeId error:" + e);
			}
		}
		
		/**
		 * 删除扫描的order
		 * @param entry_id
		 * @param equipment_type
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public void deleteNotScanOrderByLRID(long lr_id) throws Exception{
			try{
				String sql = "DELETE wlo.* FROM wms_load_order_pallet_type_count wlc"
						+ " JOIN wms_load_order wlo ON wlo.wms_order_id = wlc.wms_order_id"
						+ " WHERE wlc.scan_adid ='' AND wlc.lr_id = "+lr_id;
				dbUtilAutoTran.executeSQL(sql);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.deleteOrderByLRID error:" + e);
			}
		}
		/**
		 * 删除所有的order
		 * @param entry_id
		 * @param equipment_type
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public void deleteAllOrderByLRID(long lr_id) throws Exception{
			try{
				dbUtilAutoTran.delete("where lr_id="+lr_id, "wms_load_order");
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.deleteAllOrderByLRID error:" + e);
			}
		}
		/**
		 * 删除所有的pallet
		 * @param entry_id
		 * @param equipment_type
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public void deleteAllPalletByLRID(long lr_id) throws Exception{
			try{
				dbUtilAutoTran.delete("where lr_id="+lr_id, "wms_load_order_pallet_type_count");
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.deleteAllPalletByLRID error:" + e);
			}
		}
		/**
		 * 删除未扫描的pallet
		 * @param entry_id
		 * @param equipment_type
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public long deleteNotScanPalletByLRID(long lr_id) throws Exception{
			try{
				return dbUtilAutoTran.delete("where scan_adid ='' AND lr_id="+lr_id, "wms_load_order_pallet_type_count");
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.deleteNotScanPalletByLRID error:" + e);
			}
		}
		/**
		 * 查找loadOrder
		 * @param entry_id
		 * @param equipment_type
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findWMSLoadOrderByCarrierSub(String carrierSub , String companys ,String shippDateStart, String shippDateEnd,int status)throws Exception{
			try{
				String sql =  "SELECT * FROM wms_load_order wlo WHERE wlo.carrier_id in('"+carrierSub+"') AND wlo.company_id in ('"+companys+"') "
						    + "AND wlo.shipped_date >= '"+shippDateStart+"'  ";
							if(status==1){
								sql+=" and status <> 'Closed'" ;
							}
							sql+=" order by wlo.shipped_date ";

				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.findWMSLoadOrderByLRID error:" + e);
			}
		}
		/**
		 * 查找loadOrderPalet
		 * @param entry_id
		 * @param equipment_type
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findwmsLoadOrderPallet(long wms_order_id) throws Exception{
			try{
				String sql = "SELECT wlc.*,ad.employe_name AS scan_name"
						+ " FROM wms_load_order_pallet_type_count wlc"
						+ " LEFT JOIN admin ad ON ad.adid = wlc.scan_adid WHERE wlc.wms_order_id ="+wms_order_id
						+ "  ORDER BY wlc.wms_scan_number_type DESC,wlc.scan_time";
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.findWMSLoadOrderByLRID error:" + e);
			}
		}
		/**
		 * 插入wms_load_order
		 * @param entry_id
		 * @param equipment_type
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public long addWmsLoadOrder(DBRow upRow) throws Exception{
			try{
				return dbUtilAutoTran.insertReturnId("wms_load_order", upRow);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.addWmsLoadOrder error:" + e);
			}
		}
		/**
		 * 插入wms_load_order_pallet_type_count
		 * @param entry_id
		 * @param equipment_type
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public long addWmsLoadOrderPalletTypeCount(DBRow upRow) throws Exception{
			try{
				return dbUtilAutoTran.insertReturnId("wms_load_order_pallet_type_count", upRow);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.addWmsLoadOrderPalletTypeCount error:" + e);
			}
		}
		
		
		/**
		 * 删除wms_load_order
		 * @param carrier_id
		 * @return
		 * @throws Exception
		 */
		public long deleteWMSLoadOrderByOrderID(String order_id) throws Exception{
			try{
				return dbUtilAutoTran.delete("where wms_order_id in("+order_id+")", "wms_load_order");
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.deleteWMSLoadOrderByOrderID error:" + e);
			}
		}
		/**
		 * 删除wms_load_order_pallet_type_count
		 * @param carrier_id
		 * @return
		 * @throws Exception
		 */
		public long deleteWMSLoadOrderPalletCountByPalletID(String pallet_id) throws Exception{
			try{
				return dbUtilAutoTran.delete("where wms_order_type_id in("+pallet_id+")", "wms_load_order_pallet_type_count");
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.deleteWMSLoadOrderPalletCountByPalletID error:" + e);
			}
		}
		//查找order
		public DBRow findWMSLoadOrderByOrderNo(String orderNo,String company_id,long lr_id) throws Exception{
			try{
				String sql = "SELECT * FROM wms_load_order wlo"
						+ " WHERE wlo.order_numbers = '"+orderNo+"' AND wlo.company_id = '"+company_id+"'";
				if(lr_id>0){
					sql += " AND wlo.lr_id = "+lr_id;
				}
				return dbUtilAutoTran.selectSingle(sql);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.findWMSLoadOrderByOrderNo error:" + e);
			}
		}

		/**
		 * 查询carrier_sub
		 * @param carrier_id
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findSmallParcelCarrierSubByCarrierId(long carrier_id) throws Exception{
			try{
				String sql = "SELECT * FROM small_parcel_carrier_sub scs WHERE scs.carrier_id ="+carrier_id;
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.findWMSLoadOrderByLRID error:" + e);
			}
		}
		//更新detail
		public long updateDetailByID(long detail_id,DBRow upRow) throws Exception{
			try{
				return dbUtilAutoTran.update("where dlo_detail_id = "+detail_id, "door_or_location_occupancy_details", upRow) ;
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.findWMSLoadOrderByOrderNo error:" + e);
			}
		}
		
		/**
		 * 查找loadOrderPalet
		 * @param entry_id
		 * @param equipment_type
		 * @param para
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findwmsLoadOrderPalletByPalletNumber(String wms_pallet_number,int wms_scan_numbet_type,long lr_id) throws Exception{
			try{
				String sql = "SELECT wlc.*,ad.employe_name AS scan_name"
						+ " FROM wms_load_order_pallet_type_count wlc"
						+ " LEFT JOIN admin ad ON ad.adid = wlc.scan_adid"
						+ " WHERE wlc.wms_pallet_number ='"+wms_pallet_number+"'"
						+ " AND wlc.wms_scan_number_type ="+wms_scan_numbet_type;
				if(lr_id>0){
					sql += " AND wlc.lr_id = "+lr_id;
				}
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.findwmsLoadOrderPalletByPalletNumber error:" + e);
			}
		}


		/**
		 * 查询order
		 * @param wms_pallet_number
		 * @param wms_scan_numbet_type
		 * @param lr_id
		 * @return
		 * @throws Exception
		 */
		public DBRow findWMSLoadOrderByOrderID(long order_id) throws Exception{
			try{
				String sql = "SELECT * FROM wms_load_order WHERE wms_order_id = "+order_id;
				return dbUtilAutoTran.selectSingle(sql);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.findWMSLoadOrderByOrderID error:" + e);
			}
		}
		/**
		 * 更新order下所有的pallet  trackNO
		 * @param wms_order_id
		 * @param upRow
		 * @return
		 * @throws Exception
		 */
		public long updateWMSLoadOrderPalletCountByOrderID(long wms_order_id,DBRow upRow) throws Exception{
			try{
				return dbUtilAutoTran.update("where wms_order_id = "+wms_order_id, "wms_load_order_pallet_type_count", upRow);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.updateWMSLoadOrderPalletCountByOrderID error:" + e);
			}
		}
		//查找lr下的所有的pallet
		public DBRow[] findwmsLoadOrderPalletByLRID(long lr_id) throws Exception{
			try{
				String sql = "SELECT wlc.*,ad.employe_name AS scan_name"
						+ " FROM wms_load_order_pallet_type_count wlc"
						+ " LEFT JOIN admin ad ON ad.adid = wlc.scan_adid WHERE wlc.lr_id ="+lr_id
						+ "  ORDER BY wlc.wms_scan_number_type DESC,wlc.scan_time";
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.findwmsLoadOrderPalletByLRID error:" + e);
			}
		} 
		
		//查找pallet
		public DBRow findwmsLoadOrderPalletByWMSOrderTypeID(long wms_order_type_id) throws Exception{
			try{
				String sql = "SELECT wlc.*,ad.employe_name AS scan_name"
						+ " FROM wms_load_order_pallet_type_count wlc"
						+ " LEFT JOIN admin ad ON ad.adid = wlc.scan_adid WHERE wlc.wms_order_type_id ="+wms_order_type_id;
				return dbUtilAutoTran.selectSingle(sql);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.findwmsLoadOrderPalletByWMSOrderTypeID error:" + e);
			}
		}
		
		//更新pallet
		public long updateWMSLoadOrderPalletByIds(String wms_order_type_ids,DBRow upRow) throws Exception{
			try{
				return dbUtilAutoTran.update("where wms_order_type_id in("+wms_order_type_ids+")", "wms_load_order_pallet_type_count", upRow);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.findwmsLoadOrderPalletByWMSOrderTypeID error:" + e);
			}
		}
		//删除pallet
		public long deleteWMSLoadOrderPalletCountByOrderIDS(String wms_order_ids) throws Exception{
			try{
				return dbUtilAutoTran.delete("where wms_order_id in("+wms_order_ids+")", "wms_load_order_pallet_type_count");
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.findwmsLoadOrderPalletByWMSOrderTypeID error:" + e);
			}
		}
		
		//查找detail下的所有的pallet
		public DBRow[] findwmsLoadOrderPalletByDetailID(long detail_id) throws Exception{
			try{
				String sql = "SELECT wlc.*,ad.employe_name AS scan_name FROM wms_load_order_pallet_type_count wlc"
						+ " LEFT JOIN admin ad ON ad.adid = wlc.scan_adid WHERE wlc.dlo_detail_id ="+detail_id;
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("floorReceiptsMgrWfh.findwmsLoadOrderPalletByDetailID error:" + e);
			}
		}

		/**
		 * 查询task扫描的其他的order
		 * @param order_id
		 * @param upRow
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findTaskOtherCarrierOrder(String carrier_sub,long detail_id) throws Exception{
			try{
				String sql = "SELECT DISTINCT wlo.* FROM wms_load_order wlo"
						+ " JOIN wms_load_order_pallet_type_count wlc ON wlo.wms_order_id = wlc.wms_order_id"
						+ " WHERE wlo.carrier_id NOT in('"+carrier_sub+"') AND wlc.dlo_detail_id= "+detail_id;
				return dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e){
				throw new Exception("FloorReceiptsMgrWfh.findTaskOtherCarrierOrder error:" + e);
			}
		}
		/**
		 * 根据ctnr查询车头/driver的信息
		 * @param order_id
		 * @param upRow
		 * @return
		 * @throws Exception
		 */
		public DBRow getCheckInMessage(String ctnr) throws Exception{
			try{
				String sql = "SELECT ee.check_in_entry_id,ee.seal_pick_up,dm.mc_dot,dm.gate_driver_name"
						+ ",dm.gate_driver_liscense,dm.company_name,sc.yc_id,sc.yc_no,sc.area_id,sa.area_name"
						+ " FROM entry_equipment ee"
						+ " JOIN door_or_location_occupancy_main dm ON dm.dlo_id = ee.check_in_entry_id"
						+ " LEFT JOIN space_resources_relation srr ON srr.relation_type=1 AND srr.relation_id = ee.equipment_id"
						+ " LEFT JOIN storage_yard_control sc"
						+ "  ON srr.resources_type="+OccupyTypeKey.SPOT+" AND srr.resources_id=sc.yc_id"
						+ " LEFT JOIN storage_location_area sa ON sc.area_id=sa.area_id"
						+ " WHERE ee.equipment_type = "+CheckInTractorOrTrailerTypeKey.TRAILER+" AND ee.equipment_number = \""+ctnr+"\""
						+ " ORDER BY ee.check_in_time DESC LIMIT 0,1";
				return dbUtilAutoTran.selectSingle(sql);
			}catch (Exception e){
				throw new Exception("FloorReceiptsMgrWfh.getCheckInMessage error:" + e);
			}
		}
		/**
		 * 查询spot的信息
		 * @param order_id
		 * @param upRow
		 * @return
		 * @throws Exception
		 */
		public DBRow findSpotBySpotID(long yc_id) throws Exception{
			try{
				String sql = "SELECT sc.yc_id,sc.yc_no,sc.area_id,sa.area_name,sa.area_psid"
						+ " FROM storage_yard_control sc"
						+ " JOIN storage_location_area sa ON sa.area_id = sc.area_id"
						+ " WHERE sc.yc_id ="+yc_id;
				return dbUtilAutoTran.selectSingle(sql);
			}catch (Exception e){
				throw new Exception("FloorReceiptsMgrWfh.findSpotBySpotID error:" + e);
			}
		}
		
		/**
		 * 查找院内所有的ctnr
		 * @param entry_id
		 * @param equipment_id
		 * @return
		 * @throws Exception
		 */

		public DBRow[] findYardCtnr(long ps_id) throws Exception{
			try{
				String sql ="SELECT ee.equipment_id,dm.company_name,dm.mc_dot,dm.gate_driver_name,dm.gate_check_in_time,dm.gate_driver_liscense"
						+ ",ee.equipment_number AS ctnr,tractor.equipment_number AS lp,ee.check_in_entry_id AS dlo_id"
						+ ",ee.rel_type AS equipment_type,srr.resources_type,srr.resources_id,IFNULL(door.doorId,spot.yc_no) AS resources_name"
						+ ",IFNULL(door.area_id,spot.area_id) AS area_id,ee.equipment_status"
						+ " FROM entry_equipment ee"
						+ " JOIN door_or_location_occupancy_main dm ON dm.dlo_id = ee.check_in_entry_id"
						+ " LEFT JOIN entry_equipment tractor ON tractor.equipment_type = "+CheckInTractorOrTrailerTypeKey.TRACTOR+" AND tractor.check_in_entry_id = ee.check_in_entry_id"
						+ " LEFT JOIN space_resources_relation srr ON srr.relation_type=1 AND srr.relation_id=ee.equipment_id"
						+ " LEFT JOIN storage_door door ON srr.resources_type="+OccupyTypeKey.DOOR+" AND door.sd_id=srr.resources_id"
						+ " LEFT JOIN storage_yard_control spot ON srr.resources_type="+OccupyTypeKey.SPOT+" AND spot.yc_id=srr.resources_id"
						+ " WHERE ee.equipment_type = "+CheckInTractorOrTrailerTypeKey.TRAILER+" AND ee.equipment_status !="+CheckInMainDocumentsStatusTypeKey.LEFT+" AND ee.ps_id ="+ps_id
						+ " ORDER BY resources_type DESC,cast(resources_name as int) ASC";
				return dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e){
				throw new Exception("FloorReceiptsMgrWfh.findYardCtnr error:" + e);
			}
		}
			/**
			 * 查找设备下没有资源的task
			 * @param entry_id
			 * @param equipment_id
			 * @return
			 * @throws Exception
			 */

		public DBRow[] findNoSpaceTask(long entry_id,long equipment_id) throws Exception{
			try{
				String sql ="SELECT dd.* FROM door_or_location_occupancy_details dd"
						+ " LEFT JOIN space_resources_relation srr"
						+ "  ON srr.relation_type = "+SpaceRelationTypeKey.Task+" AND srr.relation_id=dd.dlo_detail_id"
						+ " WHERE dd.rl_id is NULL AND srr.resources_id IS NULL"
						+ "  AND dd.dlo_id = "+entry_id+" AND dd.equipment_id = "+equipment_id;
				return dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e){
				throw new Exception("FloorReceiptsMgrWfh.findNoSpaceTask error:" + e);
			}
		}
		
		/**
		 * 查找这个任务扫描的pallet
		 * @param detail_id
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findReceivePallet(long detail_id) throws Exception{
			try{
				String sql="SELECT con.con_id,con.container"
						+ " ,IFNULL(srr.resources_type,dd.occupancy_type) AS resources_type"
						+ " ,IFNULL(srr.resources_id,dd.rl_id) AS resources_id"
						+ " ,IF( cc.length!=0, CONCAT(cc.length,\"x\",cc.width,\"x\",cc.height),\"\") AS pallet_type"
						+ " ,IFNULL(rl.scan_end_time,rl.unloading_finish_time) AS scan_time"
						+ " ,rrc.normal_qty,rrc.damage_qty,cp.cp_quantity"
						+ " FROM receipt_rel_container rrc"
						+ " JOIN container con ON con.con_id = rrc.plate_no"
						+ " JOIN receipt_lines rl ON rl.receipt_line_id = rrc.receipt_line_id"
						+ " JOIN container_config cc ON cc.container_config_id=rrc.container_config_id"
						+ " JOIN door_or_location_occupancy_details dd ON dd.dlo_detail_id = rrc.detail_id"
						+ " LEFT JOIN space_resources_relation srr ON srr.relation_type=2 AND srr.relation_id=dd.dlo_detail_id"
						+ " LEFT JOIN container_product cp ON cp.cp_lp_id = con.con_id"
						+ " WHERE rrc.detail_id="+detail_id;
				
				
				return dbUtilAutoTran.selectMutliple(sql);
					
			}catch (Exception e){
				throw new Exception("FloorReceiptsMgrWfh.findReceivePallet error:" + e);
			}
		}
		/**
		 * 查找这个任务扫描的pallet
		 * @param detail_id
		 * @return
		 * @throws Exception
		 */
		public DBRow findEntryInfoForTmsPush(long detail_id) throws Exception{
			try{
				String sql="SELECT DISTINCT dm.gate_driver_liscense,dm.gate_driver_name,dm.mc_dot,dm.company_name,yc.yc_no"
						+ ",sa.area_name,ee.ps_id"
						+ " FROM entry_equipment ee"
						+ " JOIN door_or_location_occupancy_main dm ON dm.dlo_id=ee.check_in_entry_id"
						+ " LEFT JOIN space_resources_relation srr ON srr.relation_type=1"
							+ " AND srr.resources_type="+OccupyTypeKey.SPOT+" AND srr.relation_id=ee.equipment_id"
						+ " LEFT JOIN storage_yard_control yc ON yc.yc_id = srr.resources_id"
						+ " LEFT JOIN storage_location_area sa ON sa.area_id = yc.area_id"
						+ " WHERE ee.equipment_id ="+detail_id;
				return dbUtilAutoTran.selectSingle(sql);
					
			}catch (Exception e){
				throw new Exception("FloorReceiptsMgrWfh.findReceivePallet error:" + e);
			}
		}
		
		public DBRow findPsNameByPsId(long ps_id) throws Exception{
			try{
				String sql = "SELECT * FROM product_storage_catalog ps WHERE ps.id ="+ps_id;
				
				return dbUtilAutoTran.selectSingle(sql);
			}catch(Exception e){
				throw new Exception("FloorReceiptsMgrWfh.findPsNameByPsId error:");
			}
		}
		
		/**
		 * 查找line下break的pallet
		 * @param line_id
		 * @return
		 * @throws Exception
		 */
		public DBRow[] getReceiveBreakPalletsInfoByDetailId(long line_id) throws Exception{
			try{
				String sql = "SELECT rrc.plate_no AS con_id ,con.container AS palte_no,cc.quantity,c.length,c.width,c.height,rrc.detail_id"
						+ " FROM cc_from cc"
						+ " JOIN receipt_rel_container rrc ON rrc.plate_no = cc.container_id"
						+ " JOIN container_config c ON c.container_config_id = rrc.container_config_id"
						+ " JOIN container con ON con.con_id = rrc.plate_no AND con.is_delete = 1"
						+ " WHERE cc.receipt_line_id ="+line_id;
				
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("FloorReceiptsMgrWfh.getReceiveBreakPalletsInfoByDetailId error:");
			}
		}
		 
		
		/**
		 * 查找detail rn id
		 * @param line_id
		 * @return
		 * @throws Exception
		 */
		public DBRow findReceiptNoIdByDetailId(long detail_id) throws Exception{
			try{
				String sql = "SELECT DISTINCT rl.receipt_id"
						+ " FROM receipt_rel_container rrc"
						+ " JOIN receipt_lines rl ON rl.receipt_line_id = rrc.receipt_line_id"
						+ " WHERE rrc.detail_id ="+detail_id;
				return dbUtilAutoTran.selectSingle(sql);
			}catch(Exception e){
				throw new Exception("FloorReceiptsMgrWfh.findReceiptNoIdByDetailId error:");
			}
		}
		
		/**
		 * 查找rn下所有的托盘
		 * @param receipt_id
		 * @return
		 * @throws Exception
		 */
		public DBRow findReceivePalletConutByRnId(long receipt_id) throws Exception{
			try{
				String sql = "SELECT count(rrc.plate_no) AS pallet_count"
						+ " FROM receipt_rel_container rrc"
						+ " JOIN container con ON con.con_id = rrc.plate_no AND con.is_delete = 0"
						+ " JOIN receipt_lines rl ON rl.receipt_line_id = rrc.receipt_line_id"
						+ " WHERE rl.receipt_id ="+receipt_id;
				
				return dbUtilAutoTran.selectSingle(sql);
			}catch(Exception e){
				throw new Exception("FloorReceiptsMgrWfh.findReceiptNoIdByDetailId error:");
			}
		}
		
		public DBRow findSpotByNo(long ps_id,String spot_no) throws Exception{
			try{
				String sql = "SELECT sc.yc_id,sc.yc_no,sc.area_id,sa.area_name,sa.area_psid"
						+ " FROM storage_yard_control sc"
						+ " JOIN storage_location_area sa ON sa.area_id = sc.area_id"
						+ " WHERE sc.ps_id =" + ps_id + " and yc_no='" + spot_no + "'";
				return dbUtilAutoTran.selectSingle(sql);
			}catch (Exception e){
				throw new Exception("FloorReceiptsMgrWfh.findSpotBySpotID error:" + e);
			}
		}
		
		public DBRow findDoorByDoorId(long ps_id,String doorId) throws Exception{
			try{
				String sql = "SELECT * from storage_door where ps_id=" + ps_id + " and doorId= '" + doorId + "' ";
				return dbUtilAutoTran.selectSingle(sql);
			}catch (Exception e){
				throw new Exception("FloorReceiptsMgrWfh.findSpotBySpotID error:" + e);
			}
		}
		
		
		
}

