package com.cwc.app.floor.api.ll;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorBase {
	private String tableName;
	private String PK;
	private DBUtilAutoTran dbUtilAutoTran;

	public String getTableName() {
		return tableName;
	}

	public String getPK() {
		return PK;
	}

	public DBUtilAutoTran getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public FloorBase() {

	}

	public FloorBase(String tableName, String PK) {
		this.tableName = tableName;
		this.PK = PK;
	}

	public DBRow[] getAll() throws Exception {
		String sql = "select * from " + tableName;
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	public DBRow getRowById(String id) throws Exception {
		String sql = "select * from " + tableName + " where " + PK + " = " + id;
		return dbUtilAutoTran.selectSingle(sql);
	}
	
	public DBRow[] getRowsByPara(DBRow para,DBRow likePara,PageCtrl pc, String orderBy) throws Exception {
		DBRow row = new DBRow();
		String sql = "select * from " + tableName + " where 1=1 ";
		int t = 0;
		for(int i=0;para!=null && i<para.getFieldNames().size();i++){
			String rowName = para.getFieldNames().get(i).toString();
			String rowValue = para.getValue(rowName)==null?"":para.getValue(rowName).toString();
			if(!rowValue.equals("")) {
				sql += " and " + rowName + " = ?";
				row.add(rowName, rowValue);
				t++;
			}
		}
		sql += likePara==null?"":" and (";
		for(int i=0;likePara!=null && i<likePara.getFieldNames().size();i++){
			String rowName = likePara.getFieldNames().get(i).toString();
			String rowValue = "%" + likePara.getValue(rowName)==null?"":likePara.getValue(rowName).toString() + "%";
			if(!rowValue.equals("")) {
				sql += i==0?rowName + " like ?":" or " + rowName + " like ?";
				row.add(rowName, rowValue);
				t++;
			}
		}
		sql += likePara==null?"": ") ";
		
		if(!orderBy.equals("")) {
			sql += " order by "+orderBy;
		}
		
		if(pc!=null)
			if(t==0)
				return dbUtilAutoTran.selectMutliple(sql, pc);
			else
				return dbUtilAutoTran.selectPreMutliple(sql,row,pc);
		else
			if(t==0)
				return dbUtilAutoTran.selectMutliple(sql);
			else
				return dbUtilAutoTran.selectPreMutliple(sql, row);
	}
	
	public DBRow getRowByFPK(DBRow row, String FPKFieldName, String associateTable, String associateId, String[] searchField) throws Exception{
			String FPK = "0";
			
			String sql = "select " + associateId;
			for(int ii=0; ii<searchField.length; ii++) {
				sql += "," + searchField[ii];
			}
			FPK = row.getValue(FPKFieldName).toString();
			sql += " from " + associateTable + "b where " + associateId + " = '" + FPK + "'";
			DBRow aRow = dbUtilAutoTran.selectSingle(sql);
			
			for(int ii=0; ii<searchField.length; ii++) {
				row.add(searchField[ii], aRow.getValue(searchField[ii]).toString());
			}

		return row;
	}
	
	public DBRow[] getRowsByFPK(DBRow[] rows, String FPKFieldName, String associateTable, String associateId, String[] searchField) throws Exception{
		for(int i=0;i<rows.length;i++) {
			String FPK = "0";
			
			String sql = "select " + associateId;
			for(int ii=0; ii<searchField.length; ii++) {
				sql += "," + searchField[ii];
			}
			FPK = rows[i].getValue(FPKFieldName).toString();
			sql += " from " + associateTable + "b where " + associateId + " = '" + FPK + "'";
			DBRow aRow = dbUtilAutoTran.selectSingle(sql);
			
			for(int ii=0; ii<searchField.length; ii++) {
				rows[i].add(searchField[ii], aRow.getValue(searchField[ii]).toString());
			}
			
		}

		return rows;
	}
	
	public DBRow[] getDetailsByFPK(DBRow row, String FPKFieldName, String associateTable, String associateId, String[] searchField) throws Exception{
		String FPK = "0";
		
		String sql = "select " + associateId;
		for(int ii=0; ii<searchField.length; ii++) {
			sql += "," + searchField[ii];
		}
		FPK = row.getValue(FPKFieldName).toString();
		sql += " from " + associateTable + "b where " + associateId + " = '" + FPK + "'";
		DBRow[] aRow = dbUtilAutoTran.selectMutliple(sql);
		DBRow[] rows = new DBRow[aRow.length];			
		for(int i=0; i<aRow.length;i++){
			for(int ii=0; ii<searchField.length; ii++) {
				rows[i].add(searchField[ii], aRow[i].getValue(searchField[ii]).toString());
			}
		}

		return rows;
	} 
	
	public DBRow insertRow(DBRow data) throws Exception {
		long PKValue = Long.parseLong(data.getValue(PK).toString()); 
		if(PKValue == 0) {
			PKValue = dbUtilAutoTran.getSequance(tableName);
			data.add(PK, PKValue);
		}
		if(dbUtilAutoTran.insert(tableName, data))
			return getRowById(Long.toString(PKValue));
		else
			return null;
	}
	
	public DBRow updateRow(DBRow data) throws Exception {
		
		if(dbUtilAutoTran.update(" where "+PK+"='"+data.getValue(PK).toString()+"'", tableName, data)==1)
			return getRowById(data.getValue(PK).toString());
		else
			return null;
			
	}
	
	public int delete(String[] ids) throws Exception {
		String PKsValue = "0";
		for(int i=0;i<ids.length;i++){
			PKsValue += ","+ids[i];
		}
		
		return dbUtilAutoTran.delete("where "+PK+" in("+PKsValue+")", tableName);
	}
}
