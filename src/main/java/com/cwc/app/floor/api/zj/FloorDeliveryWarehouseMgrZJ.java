package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorDeliveryWarehouseMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 添加交货临时表
	 * @param db
	 * @return
	 * @throws Exception
	 */
	public long addDeliveryWareHouse(DBRow db)
		throws Exception
	{
		try {
			long dw_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("delivery_warehouse"));
			db.add("dw_id",dw_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("delivery_warehouse"),db);
			
			return (dw_id);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDeliveryWarehouseMgrZJ addDeliveryWareHouse error:"+e);
		}
	}
	
	/**
	 * 根据交货单号删除交货临时表数据
	 * @param delivery_order_number
	 * @throws Exception
	 */
	public void delDeliveryWareHouseByDeliveryOrderNumber(String delivery_order_number)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where dw_deliveryOrder_number = '"+delivery_order_number+"'",ConfigBean.getStringValue("delivery_warehouse"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDeliveryWarehouseMgrZJ delDeliveryWareHouseByDeliveryOrderNumber error:"+e);
		}
	}
	
	/**
	 * 交货比较
	 * @param delivery_order_number
	 * @return
	 * @throws Exception
	 */
	public DBRow[] compareDeliveryByDeliveryOrderNumber(long delivery_order_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("delivery_warehouse_compare_view")+" where dcount != dw_count and  dw_deliveryOrder_id = ? ";
			
			DBRow para = new DBRow();
			para.add("delivery_order_id",delivery_order_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorDeliveryWarehouseMgrZJ compareDeliveryByDeliveryOrderNumber error:"+e);
		}
	}
	
	/**
	 * 根据交货单ID获得虚拟交货数据
	 * @param delivery_order_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryByDeliveryOrderId(long delivery_order_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("delivery_warehouse")+" where dw_deliveryOrder_id = ?";
			
			DBRow para = new DBRow();
			para.add("dw_deliveryOrder_id",delivery_order_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDeliveryWarehouseMgrZJ getDeliveryByDeliveryOrderId error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
