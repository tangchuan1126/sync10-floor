package com.cwc.app.floor.api.wp;

import org.apache.log4j.Logger;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorKmlInfoMgrWp {
	static Logger log = Logger.getLogger("ACTION");
	
	private DBUtilAutoTran dbUtilAutoTran;

	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	/**
	 * 根据位置查询LOCATION信息
	 * @param position
	 * @return
	 * @throws Exception
	 */
	public DBRow getLocationByPosition(long psId, String position) throws Exception{
		try {
			String sql="select c.*  from "
					+ConfigBean.getStringValue("storage_location_catalog")+" c" +
					" where (slc_position_all = '"+position+"' or slc_position = '"+position+"') and slc_psid="+psId;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("QueryKmlInfoMgrWp.getLocationByPosition():"+e);
		}
	}
	/**
	 * 根据位置查询door信息
	 * @param psId
	 * @param position
	 * @return
	 * @throws Exception
	 */
	public DBRow getDoorByPosition(long psId, String position) throws Exception{
		try {
			String sql="select * from "+ConfigBean.getStringValue("storage_door")+" s where s.doorId = '"+position+"' and s.ps_id="+psId;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("QueryKmlInfoMgrWp.getDoorByPosition():"+e);
		}
	}
	/**
	 * 根据位置查询staging信息
	 * @param psId
	 * @param position
	 * @return
	 * @throws Exception
	 */
	public DBRow getStagingByPosition(long psId, String position) throws Exception{
		try {
			String sql="select * from "+ConfigBean.getStringValue("storage_load_unload_location")+" s where s.location_name = '"+position+"' and s.psc_id="+psId;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("QueryKmlInfoMgrWp.getStagingByPosition():"+e);
		}
	}
	/**
	 * 根据位置查询parking信息
	 * @param psId
	 * @param position
	 * @return
	 * @throws Exception
	 */
	public DBRow getParkingByPosition(long psId, String position) throws Exception{
		try {
			String sql="select * from "+ConfigBean.getStringValue("storage_yard_control")+" s where s.yc_no = '"+position+"' and s.ps_id="+psId;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("QueryKmlInfoMgrWp.getParkingByPosition():"+e);
		}
	}
	/**
	 * 根据位置查询area信息
	 * @param psId
	 * @param position
	 * @return
	 * @throws Exception
	 */
	public DBRow getAreaByPosition(long psId, String position) throws Exception{
		try {
			String sql="select * from "+ConfigBean.getStringValue("storage_location_area")+" s where s.area_name = '"+position+"' and s.area_psid="+psId;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("QueryKmlInfoMgrWp.getParkingByPosition():"+e);
		}
	}
	
	public DBRow[] queryStorageKml()throws  Exception{
		
		try {
			String tableName=ConfigBean.getStringValue("storage_kml");
			return 	dbUtilAutoTran.select(tableName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("QueryKmlInfoMgrWp.queryStorageKml():"+e);
		}
		
	}
	

}
