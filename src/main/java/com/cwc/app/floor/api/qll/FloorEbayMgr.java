package com.cwc.app.floor.api.qll;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorEbayMgr {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 获得账号token
	 * @param userID
	 * @return
	 * @throws Exception
	 */
	public DBRow  getUserToken(String  userID) 
		throws Exception 
	{
		try {
			DBRow para = new DBRow();
			para.add("parentid",userID);
			String sql = "select * from "+ConfigBean.getStringValue("ebay_token")+" where user_id= ?  ";
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("getUserToken(String  userID) error:"+e);
		}
	}
	
	/**
	 * 在不能直接多的token的时候选择使用数据库中的第一个账号的token
	 * @return
	 * @throws Exception
	 */
	public DBRow  getFirstUserToken() 
		throws Exception 
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("ebay_token")+" where id= 1  ";
			return (dbUtilAutoTran.selectSingle(sql));
		} 
		catch (Exception e) 
		{
			throw new Exception("getUserToken(String  userID) error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
