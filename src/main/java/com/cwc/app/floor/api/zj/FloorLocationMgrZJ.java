package com.cwc.app.floor.api.zj;


import com.cwc.app.key.ApproveStatusKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorLocationMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	/**
	 * 增加仓库区域
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addLocationArea(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long area_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("storage_location_area"));
			dbrow.add("area_id",area_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("storage_location_area"),dbrow);
			
			return area_id;
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorLocationMgrZJ addLocationArea error:"+e);
		}
	}
	/**
	 * 添加区域卸货门或装卸位置关系
	 * @param drow
	 * @return
	 * @throws Exception
	 */
	public long addAreaDoorOrLocation(DBRow drow)throws Exception{
			
			try {
				long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("area_door_location_relation"));
				drow.add("id", id);
				dbUtilAutoTran.insert(ConfigBean.getStringValue("area_door_location_relation"), drow);
				return id;
				
			} catch (Exception e) {
				throw new Exception("FloorLocationMgrZJ addAreaDoorOrLocation error:"+e);
			}
	}
	/**
	 * 获取区域所对应的卸货门或装卸位置
	 * @param area_id
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAreaDoorOrLocationByAreaId(long area_id,long type)throws Exception{
		
		try {
			String sql = "select * from "+ConfigBean.getStringValue("area_door_location_relation")+" where area_id="+area_id+" and dl_type="+type;
			return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorLocationMgrZJ getAreaDoorOrLocationByAreaId error:"+e);
		}
   }
	
	/**
	 * 修改仓库区域
	 * @param area_id
	 * @param para
	 * @throws Exception
	 */
	public void modLocationArea(long area_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where area_id="+area_id,ConfigBean.getStringValue("storage_location_area"),para);
		}
		catch (Exception e)
		{
			throw new Exception("FloorLocationMgrZJ modLocationArea error:"+e);
		}
	}
	
	/**
	 * 删除区域
	 * @param area_id
	 * @throws Exception
	 */
	public void delLocationArea(long area_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where area_id ="+area_id,ConfigBean.getStringValue("storage_location_area"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLocationMgrZJ delLocationArea error:"+e);
		}
	}
	/**
	 * 删除区域所对应的所有卸货门和装卸位置
	 * @param area_id
	 * @throws Exception
	 */
	public void deleteAllAreaDoorAndLocation(long area_id)throws Exception{
		
		try {
			dbUtilAutoTran.delete("where area_id="+area_id, ConfigBean.getStringValue("area_door_location_relation"));
		
		} catch (Exception e) {
			throw new Exception("FloorLocationMgrZJ deleteAllAreaDoorAndLocation error:"+e);
		}
	}
	/**
	 * 删除区域所对应的卸货门或装卸位置
	 * @param area_id
	 * @param type
	 * @throws Exception
	 */
	public void deleteAreaDoorOrLocation(long area_id,long type)throws Exception{
			
		try {
			dbUtilAutoTran.delete("where area_id="+area_id+" and dl_type="+type, ConfigBean.getStringValue("area_door_location_relation"));
		
		} catch (Exception e) {
			throw new Exception("FloorLocationMgrZJ deleteAreaDoorOrLocation error:"+e);
		}
	}
	/**
	 * 根据仓库获得仓库所有区域
	 * @param psid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocationAreaByPsid(long psid)
		throws Exception
	{
		try 
		{
			String sql = "select area.*,ps.title as title from "+ConfigBean.getStringValue("storage_location_area")+" as area,"+ConfigBean.getStringValue("product_storage_catalog")+" as ps where area.area_psid=? and area.area_psid = ps.id";
			DBRow para = new DBRow();
			para.add("area_psid",psid);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorLocationMgrZJ getLocationAreaByPsid error:"+e);
		}
	}
	
	/**
	 * 根据仓库获得仓库所有区域
	 * @param psid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocationAreaByPsidAndTitleId(long psid, long title_id)
		throws Exception
	{
		try 
		{
			String sql = "select area.*,ps.title as title from "+ConfigBean.getStringValue("storage_location_area")+" as area,"
				+ConfigBean.getStringValue("product_storage_catalog")+" as ps, "+ConfigBean.getStringValue("check_in_zone_with_title")+" as rel "
					+ "where area.area_psid=? and area.area_psid = ps.id AND area.area_id = rel.area_id AND rel.title_id = ? ";
			DBRow para = new DBRow();
			para.add("area_psid",psid);
			para.add("title_id",title_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorLocationMgrZJ getLocationAreaByPsid error:"+e);
		}
	}
	
	/**
	 * 根据仓库ID，区域名获得区域详细
	 * @param psid
	 * @param area_name
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailLocationArea(long psid,String area_name)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_location_area")+" where area_psid=? and area_name = ?";
			
			DBRow para = new DBRow();
			para.add("area_psid",psid);
			para.add("area_name",area_name);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLocationMgrZJ getDetailLocationArea error:"+e);
		}
	}
	
	/**
	 * 根据区域ID获得区域详细
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailLocationAreaById(long area_id)
		throws Exception
	{
		try 
		{
			String sql = "select area.*,ps.title as title from "+ConfigBean.getStringValue("storage_location_area")+" as area,"+ConfigBean.getStringValue("product_storage_catalog")+" as ps where area_id = ? and ps.id = area.area_psid";
			
			DBRow para = new DBRow();
			para.add("area_id",area_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorLocationMgrZJ getDetailLocationAreaById error:"+e);
		}
	}
	
	/**
	 * 根据区域ID，获得区域下的位置
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocationCatalogsByAreaId(long area_id,PageCtrl pc,String position,String type)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("storage_location_catalog")+" where slc_area = ?");
			
			DBRow para = new DBRow();
			para.add("slc_area",area_id);
			
			if(type!=null&&!type.trim().equals(""))
			{
				sql.append(" and slc_type = ?");
				para.add("slc_type",type);
			}
			
			if(position!=null&&!position.trim().equals(""))
			{
				sql.append(" and slc_position like '%"+position+"%'");
			}
			
			sql.append(" order by slc_position_all");
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), para, pc);
		} 
		catch (Exception e) 
		{
			throw new Exception(e);
		}
	}
	
	/**
	 * 添加位置
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addLocationCatalog(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long slc_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("storage_location_catalog"));
			
			dbrow.add("slc_id",slc_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("storage_location_catalog"),dbrow);
			
			return (slc_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorLocationMgrZJ addLocationCatalog error:"+e);
		}
	}
	
	/**
	 * 修改位置
	 * @param slc_id
	 * @param para
	 * @throws Exception
	 */
	public void modLocationCatalog(long slc_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where slc_id="+slc_id,ConfigBean.getStringValue("storage_location_catalog"),para);
		} catch (Exception e) 
		{
			throw new Exception("FloorLocationMgrZJ modLocationCatalog error:"+e);
		}
	}
	
	/**
	 * 删除位置
	 * @param slc_id
	 * @throws Exception
	 */
	public void delLocationCatalog(long slc_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where slc_id="+slc_id, ConfigBean.getStringValue("storage_location_catalog"));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorLocationMgrZJ delLocationCatalog error:"+e);
		}
	}
	
	/**
	 * 批量删除
	 * @param slc_ids
	 * @throws Exception
	 */
	public void delLocationCatalogs(String slc_ids)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where slc_id in ("+slc_ids+")",ConfigBean.getStringValue("storage_location_catalog"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLocationMgrZJ delLocationCatalogs error:"+e);
		}
	}
	
	/**
	 * 根据ID获得位置详细
	 * @param slc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailLocationCatalogById(long slc_id)
		throws Exception
	{
		try 
		{
			String sql = "select locationCatalog.*,locationArea.area_name from "+ConfigBean.getStringValue("storage_location_catalog")+" as locationCatalog,"+ConfigBean.getStringValue("storage_location_area")+" as locationArea where slc_id =? and locationCatalog.slc_area = locationArea.area_id";
			DBRow para = new DBRow();
			para.add("slc_id",slc_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorLocationMgrZJ getDetailLocationCatalogById error:"+e);
		}
	}
	
	/**
	 * 查询区域内是否对应编号的位置
	 * @param slc_area
	 * @param slc_position
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailLocationCatalog(long slc_area,String slc_position)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_location_catalog")+" where slc_area = ? and slc_position = ?";
			
			DBRow para = new DBRow();
			para.add("slc_area",slc_area);
			para.add("slc_position",slc_position);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorLocationMgrZJ getDetailLocationCatalog error:"+e);
		}
	}
	
	/**
	 * 根据仓库ID获得仓库下所有位置附带区域信息
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocationCatalogAreaByPsid(long ps_id)
		throws Exception
	{
		try
		{
			String sql = "select locationCatalog.*,locationArea.area_name as area from "+ConfigBean.getStringValue("storage_location_catalog")+" as locationCatalog,"+ConfigBean.getStringValue("storage_location_area")+" as locationArea where locationCatalog.slc_psid = ? and locationCatalog.slc_area = locationArea.area_id";
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorLocationMgrZJ getLocationCatalogAreaByPsid error:"+e);
		}
	}
	
	/**
	 * 根据仓库ID，位置编码获得位置信息
	 * @param ps_id
	 * @param location
	 * @return
	 * @throws Exception
	 */
	public DBRow getLocationCatalogByPsidAndLocation(long ps_id,String location)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("storage_location_catalog")+" where slc_psid =? and slc_position_all = ?";
			
			DBRow para = new DBRow();
			para.add("slc_psid",ps_id);
			para.add("slc_position_all",location);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLocationMgrZJ getLocationCatalogByPsidAndLocation error:"+e);
		}
	}
	/**
	 * 查询当前Area， 待审核的数量
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public int countAreaWaiting(Long area_id) throws Exception{
		try{
			String sql = "select count(*) as count_sum from " + ConfigBean.getStringValue("storage_approve_area")+ " where area_id="+area_id+" and approve_status="+ApproveStatusKey.WAITAPPROVE;
		/*	DBRow para = new DBRow();
			para.add("area_id", area_id);
			para.add("approve_status", ApproveStatusKey.WAITAPPROVE);
			DBRow queryRow = dbUtilAutoTran.selectPreSingle(sql, para );*/
			DBRow queryRow = dbUtilAutoTran.selectSingle(sql);
			if(queryRow != null){
				return queryRow.get("count_sum", 0);
			}
			return 0 ;
		}catch (Exception e) {
			throw new Exception("FloorLocationMgrZJ countAreaWaiting error:"+e);
 		}
	}  
	
	/**
	 * 根据区域ID，获得区域下的位置
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllLocationCatalogsByAreaId(long area_id)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("storage_location_catalog")+" where slc_area = ?");
			
			DBRow para = new DBRow();
			para.add("slc_area",area_id);
			
			
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
		} 
		catch (Exception e) 
		{
			throw new Exception(e);
		}
	}
	
	public DBRow[] getUsersOfWarehouse(long ps_id)
			throws Exception
		{
			try 
			{
				StringBuffer sql = new StringBuffer("select adid, employe_name,areaid from "+ConfigBean.getStringValue("admin")+" where ps_id = ?");
				
				DBRow para = new DBRow();
				para.add("ps_id",ps_id);
				
				
				return dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
			} 
			catch (Exception e) 
			{
				throw new Exception(e);
			}
		}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
