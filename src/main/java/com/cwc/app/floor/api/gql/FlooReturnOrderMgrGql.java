package com.cwc.app.floor.api.gql;

import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FlooReturnOrderMgrGql {
	private DBUtilAutoTran dbUtilAutoTran;
	

    public DBRow[] getReturnOrdet(PageCtrl pc)throws Exception
    {
    	try{
    		String input_st_date = DateUtil.LastMonthDatetime("yyyy-MM-dd");//获取上个月时间
    		String input_en_date = DateUtil.FormatDatetime("yyyy-MM-dd");//获取当前时间
    		
    		
    		StringBuffer sql = new StringBuffer("SELECT * FROM ").append(ConfigBean.getStringValue("return_receiving_view"));
    		sql.append(" WHERE 1=1");
			sql.append(" AND dock_receive_date>='").append(input_st_date).append("'");
			sql.append(" AND dock_receive_date<='").append(input_en_date).append(" 23:59:59'");
    		
    		return this.dbUtilAutoTran.selectMutliple(sql.toString(), pc);
    	}catch(Exception e){
    		throw new Exception("FlooReturnOrderMgrGQL getReturnOrdet"+e);
    	}
    }
    
    
    public DBRow[] getReturnOrdersById(long id,PageCtrl pc)throws Exception
    {
    	try{
    		String sql="select * from "+ConfigBean.getStringValue("return_receiving_view")+" where receive_id like '%"+id+"%'";
    		
    		
    		if(pc==null)
    		{
    			return this.dbUtilAutoTran.selectMutliple(sql);

    		}else{
    			
    			return this.dbUtilAutoTran.selectMutliple(sql, pc);
    		}
    	}catch(Exception e){
    		throw new Exception("FlooReturnOrderMgrGQL getReturnOrdet"+e);
    	}
    }
    
    public DBRow[] getReturnOrdetsByPara(String receive_from,String receive_category,
    		String input_st_date,String input_en_date,PageCtrl pc)throws Exception
    {
    	try{
    		StringBuffer sql = new StringBuffer("SELECT * FROM ").
    				append(ConfigBean.getStringValue("return_receiving_view")).append(" WHERE 1=1");
    				
			if(!"".equals(receive_from)){
				sql.append(" AND receive_from='").append(receive_from).append("'");
			}

			if(!"".equals(receive_category)){
				sql.append(" AND receive_category='").append(receive_category).append("'");
			}
			
			if(!"".equals(input_st_date)){
				sql.append(" AND dock_receive_date>='").append(input_st_date).append("'");
			}
			
			if(!"".equals(input_en_date)){
				sql.append(" AND dock_receive_date<='").append(input_en_date).append(" 23:59:59'");
			}

    		
			if(pc==null)
    		{
    			return this.dbUtilAutoTran.selectMutliple(sql.toString());

    		}else{
    			
    			return this.dbUtilAutoTran.selectMutliple(sql.toString(), pc);
    		}
    	}catch(Exception e){
    		throw new Exception("FlooReturnOrderMgrGQL getReturnOrdetsByPara"+e);
    	}
    }
    
    public DBRow returnOrderById(long id)throws Exception
    {
    	try{
    		String sql="select * from "+ConfigBean.getStringValue("return_receiving_view")+" where 1=1 and receive_id="+id;
    		
    		return this.dbUtilAutoTran.selectSingle(sql);
    		
    	}catch(Exception e){
    		throw new Exception("FlooReturnOrderMgrGQL getReturnOrdet"+e);
    	}
    }
    
    public long addReturnOrder(DBRow row) throws Exception{
		try{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("return_receiving_view"), row);
		}catch (Exception e) {
			throw new Exception("FlooReturnOrderMgrGql.addReturnOrder():"+e);
		}
	}
    
    public void updateReturnOrderById(DBRow updateRow,String receive_id) throws Exception{
		try{
			dbUtilAutoTran.update(" where receive_id= "+receive_id, ConfigBean.getStringValue("return_receiving_view"), updateRow);
		}catch (Exception e) {
			throw new Exception("FlooReturnOrderMgrGql.updateReturnOrderById():"+e);
		}
	}
    
    public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) 
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
    
}
