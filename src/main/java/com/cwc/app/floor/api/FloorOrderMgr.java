package com.cwc.app.floor.api;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cwc.app.key.HandStatusleKey;
import com.cwc.app.key.HandleKey;
import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.key.ProductTypeKey;
import com.cwc.app.key.WaybillInternalTrackingKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.TDate;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;


public class FloorOrderMgr
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	public long addPOrder(DBRow row)
		throws Exception
	{
		try
		{
			
			long oid = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("porder"));
			row.add("oid",oid);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("porder"),row);
			return(oid);
		}
		catch (Exception e)
		{
			throw new Exception("addPOrder(row) error:" + e);
		}
	}
	
	public void modPOrder(String txnid,DBRow row)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.update("where txn_id='" + txnid+"'",ConfigBean.getStringValue("porder"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modPOrder(row) error:" + e);
		}
	}
	
	public void modPOrder(long oid,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where oid=" + oid,ConfigBean.getStringValue("porder"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modPOrder(row) error:" + e);
		}
	}
	
	public void modPOrderItem(long iid,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where iid=" + iid,ConfigBean.getStringValue("porder_item"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modPOrderItem(row) error:" + e);
		}
	}

	public void delPOrder(String txnid)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.delete("where txn_id='" + txnid+"'",ConfigBean.getStringValue("porder"));
		}
		catch (Exception e)
		{
			throw new Exception("delPOrder(row) error:" + e);
		}
	}
	
	public void delPOrder(long oid)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.delete("where oid=" + oid,ConfigBean.getStringValue("porder"));
		}
		catch (Exception e)
		{
			throw new Exception("delPOrder(row) error:" + e);
		}
	}
	
	public DBRow getDetailPOrderByTxnid(String txnid)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("txn_id",txnid);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("porder") + " where txn_id=?",para));
//			return(dbUtilAutoTran.selectPreSingleCache("select * from " + ConfigBean.getStringValue("porder") + " where txn_id=?",para,new String[]{ConfigBean.getStringValue("porder")}));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailPOrderByTxnid(row) error:" + e);
		}
	}
	
	public DBRow getDetailPOrderByOid(long oid)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("oid",oid);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("porder") + " where oid=?",para));
//			return(dbUtilAutoTran.selectPreSingleCache("select * from " + ConfigBean.getStringValue("porder") + " where oid=?",para,new String[]{ConfigBean.getStringValue("porder")}));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailPOrderByOid(row) error:" + e);
		}
	}
	
	public DBRow getDetailOrderDeliveryInfoByOid(long oid)
		throws Exception
	{
		try
		{
			String sql = "select address_name,address_street,address_country,address_city,address_state,address_zip,tel,ems_id,ccid,pro_id from " + ConfigBean.getStringValue("porder") + " where oid=?";
			DBRow para = new DBRow();
			para.add("oid",oid);
			return(dbUtilAutoTran.selectPreSingle(sql,para));
//			return(dbUtilAutoTran.selectPreSingleCache("select address_name,address_street,address_city,address_state,address_zip,tel,ems_id,ccid,pro_id from " + ConfigBean.getStringValue("porder") + " where oid=?",para,new String[]{ConfigBean.getStringValue("porder")}));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailOrderDeliveryInfoByOid(row) error:" + e);
		}
	}

	public DBRow[] getAllOrders(PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql = "select * from " + ConfigBean.getStringValue("porder") + " order by post_date desc";
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectMutliple(sql,pc));
//				return(dbUtilAutoTran.selectMutlipleCache(sql,pc,new String[]{ConfigBean.getStringValue("porder")}));
			}
			else
			{
				return(dbUtilAutoTran.selectMutliple(sql));
//				return(dbUtilAutoTran.selectMutlipleCache(sql,new String[]{ConfigBean.getStringValue("porder")}));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getAllOrders(row) error:" + e);
		}
	}
	
//	public DBRow[] getSearchOrdersByField(String field,String value,PageCtrl pc)
//		throws Exception
//	{
//		try
//		{
//			
//			DBRow para = new DBRow();
//			para.add("1",value);
//			
//			String sql = "select * from " + ConfigBean.getStringValue("porder") + " where "+field+"=? order by post_date desc";
//			if ( pc!=null )
//			{
//				return(dbUtilAutoTran.selectPreMutlipleCache(sql,para,pc,new String[]{ConfigBean.getStringValue("porder")}));
//			}
//			else
//			{
//				return(dbUtilAutoTran.selectPreMutlipleCache(sql,para,new String[]{ConfigBean.getStringValue("porder")}));
//			}
//		}
//		catch (Exception e)
//		{
//			throw new Exception("getSearchOrdersByField(row) error:" + e);
//		}
//	}

	public DBRow[] getOrdersByFilter(String st,String en,String business,int handle,int after_service_status,int handle_status,int product_status,int product_type_int,long filter_ps_id,String p_name,long order_note_adid,int trace_type,String order_source,int bad_feedback_flag,PageCtrl pc)
		throws Exception
	{
		
		try
		{
			String groupBy = "";
			
			StringBuffer sb = new StringBuffer();
			StringBuffer sb2 = new StringBuffer();
			
			String cond;

			if (handle_status>=0)
			{
				sb.append(" po.handle_status=");
				sb.append(handle_status);
				sb.append(" and ");	
			}	
			
			if (after_service_status>0)
			{
				sb.append(" po.after_service_status=");
				sb.append(after_service_status);
				sb.append(" and ");	
			}
			
			if (product_type_int>0)
			{
				sb.append(" po.product_type=");
				sb.append(product_type_int);
				sb.append(" and ");	
			}
			
//			if (product_status>=0)
//			{
//				sb.append(" po.product_status=");
//				sb.append(product_status);
//				sb.append(" and ");	
//			}		
			
			if ( business.indexOf("@")==0 )
			{
				sb.append(" ");
				sb.append("po.business");
				sb.append("!='");
				sb.append(business.substring(1));
				sb.append("' and ");
			}
			else if ( business.equals("")==false )
			{
				sb.append(" ");
				sb.append("po.business");
				sb.append("='");
				sb.append(business);
				sb.append("' and ");
			}

			if (filter_ps_id>0)
			{
				sb.append(" po.ps_id=");
				sb.append(filter_ps_id);
				sb.append(" and ");	
			}

			if (order_source.equals("")==false)
			{
				sb.append(" po.order_source='");
				sb.append(order_source);
				sb.append("' and ");	
			}
			
			if (bad_feedback_flag>0)
			{
				sb.append(" po.bad_feedback_flag=");
				sb.append(bad_feedback_flag);
				sb.append(" and ");	
			}
			
			/*
			 * 代抄单 handle=1
			 * 代打印 handle=1 and handle_status=0
			 * 已打印 handle=3
			 * 已上网 handle=4
			 */
			if ( handle>0 )
			{
				sb2.append(" po.handle=");
				sb2.append(handle);
				cond = sb.toString()+ sb2.toString() + " and ";
			}
			else
			{
				cond = sb.toString(); 
			}
			
			/**
			 * 过滤订单跟进人
			 */
			String order_note_table = "";
			String order_note_filter_cond = "";
			
			if ( order_note_adid>0 )
			{
				order_note_table = " "+ConfigBean.getStringValue("porder_note")+" pn ";
				//order_note_filter_cond = " po.oid=pn.oid and pn.adid="+order_note_adid+" and  ";
				order_note_filter_cond = "EXISTS( select pn.on_id from "+ order_note_table + " where po.oid=pn.oid and pn.adid="+order_note_adid+" ) and ";
			}
			//跟进类型
			if ( trace_type>=0 )
			{
				order_note_table = " "+ConfigBean.getStringValue("porder_note")+" pn ";
				if (order_note_filter_cond.equals("")==false)
				{
					order_note_filter_cond = order_note_filter_cond.replaceFirst("EXISTS\\(([^\\)]+)\\)",
							"EXISTS($1 and pn.trace_type="+trace_type+" )");
				}
				else
				{
					order_note_filter_cond = "EXISTS( select pn.on_id from "+ order_note_table + "where po.oid=pn.oid and pn.trace_type="+trace_type+" ) and ";//单独过来 跟进类型
				}
				
				
			}

			
			String sql;
			if (p_name.equals("")==false)
			{
				String sql_old = "select po.pay_type,po.after_service_status,po.seller_id,po.pro_id,po.bad_feedback_flag,po.mc_gross_rmb,po.shipping_name,po.oid,po.txn_id,po.item_name,po.item_number,po.quantity,po.mc_gross,po.mc_currency,po.first_name,po.last_name,po.auction_buyer_id,po.business,po.address_name,po.address_country,po.address_city,po.address_zip,po.address_street,po.address_state,po.address_status,po.payment_status,po.client_id,po.post_date,po.handle,po.handle_status,po.note,po.type,po.tel,po.ems_id,po.print_date,po.print_manager,po.dispute_flag,po.append_name,po.product_type,po.order_source,po.ccid,po.parent_oid,po.ps_id,po.delivery_note,po.product_status,po.delivery_date,po.delivery_account,po.product_cost,po.total_cost,po.total_weight,po.address_validate_status,cc.c_code from order_all_products_view oapv," + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where "+order_note_filter_cond+cond+" oapv.oid=po.oid and po.post_date >= str_to_date('"+st+"','%Y-%m-%d') and po.post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') and ((oapv.p_name='"+p_name+"' and (oapv.product_type=1 or oapv.product_type=4)) or (oapv.set_name='"+p_name+"' and (oapv.product_type=2 or oapv.product_type=3)))  order by po.post_date desc";
				//TODO 先用p_name查出product_id(pc_id) ,然后再用新表结构查
				String t_po = ConfigBean.getStringValue("porder");
				String t_cc = ConfigBean.getStringValue("country_code");
				String t_oi = ConfigBean.getStringValue("porder_item");
				String t_pu = ConfigBean.getStringValue("product_union");
				String t_p =  ConfigBean.getStringValue("product");
				
				DBRow r = dbUtilAutoTran.selectSingle("select pc_id from "+t_p+" where p_name='"+p_name+"'");
				long pc_id = r.get("pc_id", -1);
				if(pc_id<0) 
				throw new Exception("不存在此商品："+p_name);
				
				
				String sql_sel   = "po.internal_tracking_status,po.pay_type,po.after_service_status,po.seller_id,po.pro_id,po.bad_feedback_flag,po.mc_gross_rmb,po.shipping_name,po.oid,po.txn_id,po.item_name,po.item_number,po.quantity,po.mc_gross,po.mc_currency,po.first_name,po.last_name,po.auction_buyer_id,po.business,po.address_name,po.address_country,po.address_city,po.address_zip,po.address_street,po.address_state,po.address_status,po.payment_status,po.client_id,po.post_date,po.handle,po.handle_status,po.note,po.type,po.tel,po.ems_id,po.print_date,po.print_manager,po.dispute_flag,po.append_name,po.product_type,po.order_source,po.ccid,po.parent_oid,po.ps_id,po.delivery_note,po.product_status,po.delivery_date,po.delivery_account,po.product_cost,po.total_cost,po.total_weight,po.address_validate_status,cc.c_code";
				String sql_from  = "(("+t_po+" po left join "+t_cc+" cc on po.ccid=cc.ccid) " +
								  "inner join "+t_oi+" oi on po.oid = oi.oid) "+
								  "left join "+t_pu+" pu on oi.pid = pu.set_pid";
				String sql_where = order_note_filter_cond+cond+
						" po.post_date >= str_to_date('"+st+"','%Y-%m-%d') and po.post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') "+
						" and "+pc_id+" in (pu.pid,oi.pid) ";
				
//				String sql_where = order_note_filter_cond+cond+
//				" po.delivery_date >= str_to_date('"+st+"','%Y-%m-%d') and po.delivery_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') "+
//				" and "+pc_id+" in (pu.pid,oi.pid) ";//临时使用发货时间查询商品
				
				sql = String.format("select %s from %s where %s group by po.oid desc order by delivery_date desc", sql_sel,sql_from,sql_where);
			}
			else
			{
				sql = "select po.internal_tracking_status,po.is_pay_for_order,po.pay_type,po.after_service_status,po.seller_id,po.pro_id,po.bad_feedback_flag,po.mc_gross_rmb,po.shipping_name,po.oid,po.txn_id,po.item_name,po.item_number,po.quantity,po.mc_gross,po.mc_currency,po.first_name,po.last_name,po.auction_buyer_id,po.business,po.address_name,po.address_country,po.address_city,po.address_zip,po.address_street,po.address_state,po.address_status,po.payment_status,po.client_id,po.post_date,po.handle,po.handle_status,po.note,po.type,po.tel,po.ems_id,po.print_date,po.print_manager,po.dispute_flag,po.append_name,po.product_type,po.order_source,po.ccid,po.parent_oid,po.ps_id,po.delivery_note,po.product_status,po.delivery_date,po.delivery_account,po.product_cost,po.total_cost,po.total_weight,po.address_validate_status,cc.c_code from "+ ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where " +order_note_filter_cond+cond+ " po.post_date >= str_to_date('"+st+"','%Y-%m-%d') and po.post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  order by po.post_date desc";
			}
			
			//System.out.println(sql);
			//System.out.println("======="+bad_feedback_flag+"=========");
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectMutliple(sql,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectMutliple(sql));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getOrdersByFilter(row) error:" + e);
		}
	}
	
	public DBRow getDetailCountryByCountryCode(String countryCode)
		throws Exception
	{
		try
		{
			countryCode = countryCode.trim();
			DBRow para = new DBRow();
			para.add("c_code",countryCode);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("country_code") + " where c_code=?",para));
//			return(dbUtilAutoTran.selectPreSingleCache("select * from " + ConfigBean.getStringValue("country_code") + " where c_code=?",para,new String[]{ConfigBean.getStringValue("country_code")}));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailCountryCodeByCcid(row) error:" + e);
		}
	}
	
	public DBRow[] getWait4PrintOrders(long ps_id,String business,long product_type,String st,String en,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("handle_status",HandStatusleKey.NORMAL);
			para.add("product_status",ProductStatusKey.IN_STORE);
			para.add("handle",HandleKey.WAIT4_PRINT);
			para.add("ps_id",ps_id);
			
			String product_type_str = "";
			
			if (product_type>0)
			{
				product_type_str = " and product_type="+product_type+" ";
			}
			
			
			String sql;

			if (business.equals(""))
			{
				sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where po.handle_status=? and po.product_status=? and po.handle=? and po.ps_id=? and (parent_oid=0 or parent_oid=-1) and po.post_date >= str_to_date('"+st+"','%Y-%m-%d') and po.post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') "+product_type_str+" order by oid asc";
			}
			else
			{
				para.add("business",business);
				sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where po.handle_status=? and po.product_status=? and po.handle=? and po.business=? and po.ps_id=? and (parent_oid=0 or parent_oid=-1) and po.post_date >= str_to_date('"+st+"','%Y-%m-%d') and po.post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  "+product_type_str+" order by oid asc";
			}
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getWait4PrintOrders(row) error:" + e);
		}
	}
	
//	public DBRow[] getSearchOrdersByKeyLike(String field,String value,PageCtrl pc)
//		throws Exception
//	{
//		try
//		{
//
//			
//			String sql = "select * from " + ConfigBean.getStringValue("porder") + " where " + field + " like '%"+ value +"%' order by post_date desc";
//			if ( pc!=null )
//			{
//				return(dbUtilAutoTran.selectMutlipleCache(sql,pc,new String[]{ConfigBean.getStringValue("porder")}));
//			}
//			else
//			{
//				return(dbUtilAutoTran.selectMutlipleCache(sql,new String[]{ConfigBean.getStringValue("porder")}));
//			}
//		}
//		catch (Exception e)
//		{
//			throw new Exception("getSearchOrdersByKeyLike(row) error:" + e);
//		}
//	}
	
	
	public void addRelationOrder(DBRow row)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("relation_order"),row);
		}
		catch (Exception e)
		{
			throw new Exception("addRelationOrder(row) error:" + e);
		}
	}
	
	public DBRow[] getRelationOrders(String txn_id)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("txn_id",txn_id);
			
			String sql = "select * from " + ConfigBean.getStringValue("relation_order") + " where txn_id=? order by post_date asc";
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getRelationOrders(row) error:" + e);
		}
	}
	
	public DBRow[] getStatOrdersCount(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("business",business);
			para.add("st",st);
			para.add("en",en);
			
			
			String sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d,count(oid) c FROM "+ConfigBean.getStringValue("porder")+" where  business=? and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) group by date(post_date) order by post_date asc";
			
			if (product_type_int>0)
			{
				para.add("product_type_int",product_type_int);
				sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d,count(oid) c FROM "+ConfigBean.getStringValue("porder")+" where business=? and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) and product_type=? group by date(post_date) order by post_date asc";
			}

			return( dbUtilAutoTran.selectPreMutliple(sql, para) );
		}
		catch (Exception e)
		{
			throw new Exception("getStatOrdersCount(row) error:" + e);
		}
	}

	public DBRow[] getStatNormalOrdersCount(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try
		{
			
			
			DBRow para = new DBRow();
			para.add("business",business);
			para.add("st",st);
			para.add("en",en);
			
			
			String sql = "SELECT  DATE_FORMAT(post_date,'%Y-%m-%d') d,count(oid) c FROM "+ConfigBean.getStringValue("porder")+" where  handle=1 and handle_status=0 and business=? and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) group by date(post_date) order by post_date asc";
			
			if (product_type_int>0)
			{
				para.add("product_type_int",product_type_int);
				sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d,count(oid) c FROM "+ConfigBean.getStringValue("porder")+" where handle=1 and handle_status=0 and business=? and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) and product_type=? group by date(post_date) order by post_date asc";
			}
			
			return( dbUtilAutoTran.selectPreMutliple(sql, para) );
			
		}
		catch (Exception e)
		{
			throw new Exception("getStatNormalOrdersCount(row) error:" + e);
		}
	}

	public DBRow[] getStatPendingOrdersCount(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try
		{
			
			
			DBRow para = new DBRow();
			para.add("business",business);
			para.add("st",st);
			para.add("en",en);
			
			
			String sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d,count(oid) c FROM "+ConfigBean.getStringValue("porder")+" where handle_status=1 and business=? and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) group by date(post_date) order by post_date asc";
			
			if (product_type_int>0)
			{
				para.add("product_type_int",product_type_int);
				sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d,count(oid) c FROM "+ConfigBean.getStringValue("porder")+" where  handle_status=1 and business=? and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) and product_type=? group by date(post_date) order by post_date asc";
			}
			
			return( dbUtilAutoTran.selectPreMutliple(sql, para) );
		}
		catch (Exception e)
		{
			throw new Exception("getStatPendingOrdersCount(row) error:" + e);
		}
	}

	public DBRow[] getStatArchiveOrdersCount(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try
		{
			
			
			DBRow para = new DBRow();
			para.add("business",business);
			para.add("st",st);
			para.add("en",en);
			
			
			String sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d,count(oid) c FROM "+ConfigBean.getStringValue("porder")+" where  handle_status=2 and business=? and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) group by date(post_date) order by post_date asc";
			
			if (product_type_int>0)
			{
				para.add("product_type_int",product_type_int);
				sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d,count(oid) c FROM "+ConfigBean.getStringValue("porder")+" where handle_status=2 and business=? and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) and product_type=? group by date(post_date) order by post_date asc";
			}
			
			return( dbUtilAutoTran.selectPreMutliple(sql, para) );
		}
		catch (Exception e)
		{
			throw new Exception("getStatArchiveOrdersCount(row) error:" + e);
		}
	}

	public DBRow[] getStatPrintedOrdersCount(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try
		{
			
			
			DBRow para = new DBRow();
			para.add("business",business);
			para.add("st",st);
			para.add("en",en);

			String sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d,count(oid) c FROM "+ConfigBean.getStringValue("porder")+" where handle=3 and business=?  and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) group by date(post_date) order by post_date asc";
			
			if (product_type_int>0)
			{
				para.add("product_type_int",product_type_int);
				sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d,count(oid) c FROM "+ConfigBean.getStringValue("porder")+" where handle=3 and business=? and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) and product_type=? group by date(post_date) order by post_date asc";
			}
			
			return( dbUtilAutoTran.selectPreMutliple(sql, para) );
		}
		catch (Exception e)
		{
			throw new Exception("getStatPrintedOrdersCount(row) error:" + e);
		}
	}


	public DBRow[] getStatDeliveryOrdersCount(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try
		{
			
			
			DBRow para = new DBRow();
			para.add("business",business);
			para.add("st",st);
			para.add("en",en);
			
			
			String sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d,count(oid) c FROM "+ConfigBean.getStringValue("porder")+" where handle=4 and business=?  and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) group by date(post_date) order by post_date asc";
			
			if (product_type_int>0)
			{
				para.add("product_type_int",product_type_int);
				sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d,count(oid) c FROM "+ConfigBean.getStringValue("porder")+" where  handle=4 and business=?  and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) and product_type=? group by date(post_date) order by post_date asc ";
			}
			
			return( dbUtilAutoTran.selectPreMutliple(sql, para) );
		}
		catch (Exception e)
		{
			throw new Exception("getStatDeliveryOrdersCount(row) error:" + e);
		}
	}

	public DBRow[] getTimeRangeOrders(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("business",business);
			para.add("st",st);
			para.add("en",en);
			
			String sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d FROM "+ConfigBean.getStringValue("porder")+" where business=? and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) group by date(post_date) order by post_date asc";
			
			if (product_type_int>0)
			{
				para.add("product_type_int",product_type_int);
				sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d FROM "+ConfigBean.getStringValue("porder")+" where business=? and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) and product_type=? group by date(post_date) order by post_date asc";
			}
			
			return(dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e)
		{
			throw new Exception("getTimeRangeOrders(row) error:" + e);
		}
		
	}
	
	public DBRow[] getStatNotPrintOrdersCount(String st,String en,String business,int product_type_int)
		throws Exception
	{
		try
		{
			
			
			DBRow para = new DBRow();
			para.add("business",business);
			para.add("st",st);
			para.add("en",en);
			
			
			String sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d,count(oid) c FROM "+ConfigBean.getStringValue("porder")+" where  handle=2 and handle_status=0 and business=?  and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) group by date(post_date) order by post_date asc";
			
			if (product_type_int>0)
			{
				para.add("product_type_int",product_type_int);
				sql = "SELECT DATE_FORMAT(post_date,'%Y-%m-%d') d,count(oid) c FROM "+ConfigBean.getStringValue("porder")+" where  handle=2 and handle_status=0 and business=?  and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) and product_type=? group by date(post_date) order by post_date asc";
			}
			
			return( dbUtilAutoTran.selectPreMutliple(sql, para) );
		}
		catch (Exception e)
		{
			throw new Exception("getStatNotPrintOrdersCount(row) error:" + e);
		}
	}
	
	public int getOrderCountByClientId(String client_id)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("client_id",client_id);
			
			DBRow data = dbUtilAutoTran.selectPreSingle("select count(oid) c from " + ConfigBean.getStringValue("porder") + " where client_id=?",para);
			
			return(data.get("c", 0));
		}
		catch (Exception e)
		{
			throw new Exception("getOrderCountByClientId(row) error:" + e);
		}
	}
	
	public double getSumMcGrossByClientId(String client_id)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("client_id",client_id);
			
			DBRow data = dbUtilAutoTran.selectPreSingle("select sum(mc_gross) c from " + ConfigBean.getStringValue("porder") + " where client_id=? and handle="+HandleKey.DELIVERIED+" and handle_status="+HandStatusleKey.NORMAL,para);
			
			return(data.get("c", 0d));
		}
		catch (Exception e)
		{
			throw new Exception("getSumMcGrossByPayerEmail(row) error:" + e);
		}
	}
	
	public DBRow[] getAllInvoiceTemplates()
		throws Exception
	{
		try
		{
			
			String sql = "SELECT *  FROM " + ConfigBean.getStringValue("invoice_template");				
			return(dbUtilAutoTran.selectMutliple(sql));
		}
		catch (Exception e)
		{
			throw new Exception("getAllInvoiceTemplates(row) error:" + e);
		}
	}
	
	public long addDelivererInfo(DBRow row)
		throws Exception
	{
		try
		{
			
			long di_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("deliverer_info"));
			row.add("di_id",di_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("deliverer_info"),row);
			return(di_id);
		}
		catch (Exception e)
		{
			throw new Exception("addDelivererInfo(row) error:" + e);
		}
	}

	public void modDelivererInfo(long di_id,DBRow row)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.update("where di_id='" + di_id+"'",ConfigBean.getStringValue("deliverer_info"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modDelivererInfo(row) error:" + e);
		}
	}
		
	public void delDelivererInfo(long di_id)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.delete("where di_id='" + di_id+"'",ConfigBean.getStringValue("deliverer_info"));
		}
		catch (Exception e)
		{
			throw new Exception("delDelivererInfo(row) error:" + e);
		}
	}
		
	public DBRow[] getAllDelivererInfo()
		throws Exception
	{
		try
		{
			
			String sql = "SELECT *  FROM " + ConfigBean.getStringValue("deliverer_info");				
			return(dbUtilAutoTran.selectMutliple(sql));
		}
		catch (Exception e)
		{
			throw new Exception("getAllDelivererInfo(row) error:" + e);
		}
	}
	
	public DBRow[] getAllDelivererInfoByRandGroup(int rand_group)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("rand_group", rand_group);
			
			String sql = "SELECT *  FROM " + ConfigBean.getStringValue("deliverer_info") + " where rand_group=?";				
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getAllDelivererInfo(row) error:" + e);
		}
	}
	
	public DBRow[] getAllDelivererInfoByPsid(long ps_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("ps_id", ps_id);
			
			String sql = "SELECT *  FROM " + ConfigBean.getStringValue("deliverer_info") + " where ps_id=?";				
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getAllDelivererInfoByPsid(row) error:" + e);
		}
	}
	
	public long addInvoiceTemplates(DBRow row)
		throws Exception
	{
		try
		{
			
			long oid = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("invoice_template"));
			dbUtilAutoTran.insert(ConfigBean.getStringValue("invoice_template"),row);
			return(oid);
		}
		catch (Exception e)
		{
			throw new Exception("addPOrder(row) error:" + e);
		}
	}
	
	public void delInvoiceTemplatesByInvoiceId(long invoice_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where invoice_id=" + invoice_id,ConfigBean.getStringValue("invoice_template"));
		}
		catch (Exception e)
		{
			throw new Exception("delInvoiceTemplates(row) error:" + e);
		}
	}
	
	public void modInvoiceTemplates(long invoice_id,DBRow row)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.update("where invoice_id='" + invoice_id+"'",ConfigBean.getStringValue("invoice_template"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modInvoiceTemplates(row) error:" + e);
		}
	}

	public DBRow getDetailDelivererInfo(long di_id)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("di_id",di_id);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("deliverer_info") + " where di_id=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailPOrderByTxnid(row) error:" + e);
		}
	}
	
	public DBRow getDetailInvoiceTemplate(long invoice_id)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("invoice_id",invoice_id);
			
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("invoice_template") + " where invoice_id=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailInvoiceTemplate(row) error:" + e);
		}
	}
		
	public void updateOrderSource()
		throws Exception
	{
		try
		{
			
//			dbUtilAutoTran.updateOrderSource();
		}
		catch (Exception e)
		{
			throw new Exception("updateOrderSource(row) error:" + e);
		}
	}
	
	public DBRow[] getAllCountryCode()
		throws Exception
	{
		try
		{
			String sql = "SELECT *  FROM " + ConfigBean.getStringValue("country_code") + " c," + ConfigBean.getStringValue("storage_country") + " sc where c.ccid=sc.cid order by c_country asc";		
			DBRow country[] = dbUtilAutoTran.selectMutliple(sql);//dbUtilAutoTran.selectMutlipleCache(sql, new String[]{ConfigBean.getStringValue("country_code"),ConfigBean.getStringValue("storage_country")});
			
			return(country);
		}
		catch (Exception e)
		{
			throw new Exception("getAllCountryCode(row) error:" + e);
		}
	}
	
	public DBRow getDetailCountryCodeByCcid(long ccid)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("ccid",ccid);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("country_code") + " where ccid=?",para));
//			return(dbUtilAutoTran.selectPreSingleCache("select * from " + ConfigBean.getStringValue("country_code") + " where ccid=?",para,new String[]{ConfigBean.getStringValue("country_code")}));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailCountryCodeByCcid(row) error:" + e);
		}
	}
	
	public DBRow getDetailCountryCodeByCountry(String country)
		throws Exception
	{
		try
		{
			country = country.trim();
			
			
			DBRow para = new DBRow();
			para.add("country",country);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("country_code") + " where c_country=?",para));
//			return(dbUtilAutoTran.selectPreSingleCache("select * from " + ConfigBean.getStringValue("country_code") + " where c_country=?",para,new String[]{ConfigBean.getStringValue("country_code")}));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailCountryCodeByCcid(row) error:" + e);
		}
	}
	
	public DBRow getDetailCountryCodeByCountryOrCode(String country, String Code)
		throws Exception
	{
		try
		{
			country = country.trim();
			Code = Code.trim();
			DBRow para = new DBRow();
			para.add("country",country);
			
//			para.add("c_code", Code);
			
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("country_code") + " where c_country=?",para));
	}
	
	catch (Exception e)
	{
		throw new Exception("getDetailCountryCodeByCcid(row) error:" + e);
	}
}
	public void convertCountryCode()
		throws Exception
	{
		try
		{
//			dbUtilAutoTran.convertCountryCode();
		}
		catch (Exception e)
		{
			throw new Exception("convertCountryCode(row) error:" + e);
		}
	}

	public long addPOrderNote(DBRow row)
		throws Exception
	{
		try
		{
			
			long on_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("porder_note"));
			row.add("on_id",on_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("porder_note"),row);
			return(on_id);
		}
		catch (Exception e)
		{
			throw new Exception("addPOrderNote(row) error:" + e);
		}
	}

	public void modPOrderNote(long on_id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where on_id=" + on_id,ConfigBean.getStringValue("porder_note"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modPOrderNote(row) error:" + e);
		}
	}

	public DBRow getDetailPOrderNoteByOnid(long on_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("on_id",on_id);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("porder_note") + " where on_id=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailPOrderByTxnid(row) error:" + e);
		}
	}
	
	public DBRow[] getPOrderNoteByOid(long oid,PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql = "select * from " + ConfigBean.getStringValue("porder_note") + " where oid=? order by on_id asc";
			
			DBRow para = new DBRow();
			para.add("oid",oid);
			
			if (pc==null)
			{
				return(dbUtilAutoTran.selectPreMutliple(sql, para));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql, para,pc));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getPOrderNoteByOid(row) error:" + e);
		}
	}
	
	public DBRow[] getOrdersByInvoiceId(long invoice_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("invoice_id", invoice_id);
			
			String sql = "SELECT oid  FROM " + ConfigBean.getStringValue("invoice_template") + " it," + ConfigBean.getStringValue("porder") + " o where o.product_type=it.invoice_id and it.invoice_id=?";
	
			if (pc==null)
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getOrdersByInvoiceId(row) error:" + e);
		}
	}
	
	public DBRow[] getInvoiceTemplatesByDiid(long di_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("di_id", di_id);
			
			String sql = "SELECT *  FROM " + ConfigBean.getStringValue("invoice_template") + "  where di_id=?";
	
			if (pc==null)
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getInvoiceTemplatesByDiid(row) error:" + e);
		}
	}
	
	public long addPOrderItem(DBRow row)
		throws Exception
	{
		try
		{
			long iid = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("porder_item"));
			row.add("iid",iid);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("porder_item"),row);
			return(iid);
		}
		catch (Exception e)
		{
			throw new Exception("addPOrderItem(row) error:" + e);
		}
	}
	
	public long addPOrderItemHasId(DBRow row)
		throws Exception
	{
		try
		{
			long iid = row.get("iid",0l);
			if(iid==0)
			{
				iid = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("porder_item"));
				row.add("iid",iid);
			}
			dbUtilAutoTran.insert(ConfigBean.getStringValue("porder_item"),row);
			return(iid);
		}
		catch (Exception e)
		{
			throw new Exception("addPOrderItem(row) error:" + e);
		}
	}
	
	public void delPOrderItem(long oid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where oid=" + oid,ConfigBean.getStringValue("porder_item"));
		}
		catch (Exception e)
		{
			throw new Exception("delPOrderItem(row) error:" + e);
		}
	}

	public DBRow[] getPOrderItemsByOid(long oid)
		throws Exception
	{
		try
		{
			
			String sql = "select * from " + ConfigBean.getStringValue("porder_item") + " where oid=? order by iid asc";
			
			DBRow para = new DBRow();
			para.add("oid",oid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e)
		{
			throw new Exception("getPOrderItemsByOid(row) error:" + e);
		}
	}
	
	public DBRow getDetailOrderItemByOidPid(long oid,long pid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("oid",oid);
			para.add("pid",pid);
			
			String sql = "select * from " + ConfigBean.getStringValue("porder_item") + " where oid=? and pid=?";
			
			return(dbUtilAutoTran.selectPreSingle(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailOrderItemByOidPid(row) error:" + e);
		}
	}
	
	/**
	 * 特殊用途
	 * 抄单时间先后不同情况下，一个订单可能存在两个相同套装商品
	 * @param oid
	 * @param pid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDetailOrderItemsByOidPid(long oid,long pid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("oid",oid);
			para.add("pid",pid);
			
			String sql = "select * from " + ConfigBean.getStringValue("porder_item") + " where oid=? and pid=?";
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailOrderItemsByOidPid(row) error:" + e);
		}
	}
	
	public DBRow getDetailOrderItemByOidPidProductType(long oid,long pid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("oid",oid);
			para.add("pid",pid);
			
			String sql = "select * from " + ConfigBean.getStringValue("porder_item") + " where oid=? and pid=?";
			
			return(dbUtilAutoTran.selectPreSingle(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailOrderItemByOidPidProductType(row) error:" + e);
		}
	}

	public DBRow[] getSonOrderItemsByParentOid(long parent_oid)
		throws Exception
	{
		try
		{
			String sql = "select o.mc_gross,oi.oid,oi.pid,oi.quantity,oi.catalog_id,oi.unit_price,oi.gross_profit,oi.weight,oi.unit_name,oi.name,oi.product_type,oi.lacking from " + ConfigBean.getStringValue("porder") + " o," + ConfigBean.getStringValue("porder_item") + " oi where o.parent_oid=? and o.oid=oi.oid order by iid asc";
			
			DBRow para = new DBRow();
			para.add("parent_oid",parent_oid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e)
		{
			throw new Exception("getSonOrderItemsByParentOid(row) error:" + e);
		}
	}
	
	public DBRow[] getSonOrderDeliveryNoteByParentOid(long parent_oid)
		throws Exception
	{
		try
		{
			String sql = "select delivery_note from " + ConfigBean.getStringValue("porder") + " where parent_oid=? ";
			
			DBRow para = new DBRow();
			para.add("parent_oid",parent_oid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e)
		{
			throw new Exception("getSonOrderDeliveryNoteByParentOid(row) error:" + e);
		}
	}

	public DBRow[] getSonOrders(long parent_oid,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("porder") + " where parent_oid=? order by post_date desc";
			
			DBRow para = new DBRow();
			para.add("parent_oid",parent_oid);

			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getAllOrders(row) error:" + e);
		}
	}

	public DBRow[] getNormalLackingOrders()
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("porder") + " where  product_status=? and (handle_status=? or handle_status=?) order by oid asc";
			
			DBRow para = new DBRow();
			para.add("product_status",ProductStatusKey.STORE_OUT);
			para.add("handle_status1",HandStatusleKey.NORMAL);
			para.add("handle_status2",HandStatusleKey.ARCHIVE);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getNormalLackingOrders(row) error:" + e);
		}
	}
	
//	public DBRow[] getLackingOrders()
//		throws Exception
//	{
//		try
//		{
//			String sql = "select * from " + ConfigBean.getStringValue("porder") + " where  product_status=?  order by oid asc";
//			
//			DBRow para = new DBRow();
//			para.add("product_status",ProductStatusKey.STORE_OUT);
//			
//			return(dbUtilAutoTran.selectPreMutliple(sql,para));
//		}
//		catch (Exception e)
//		{
//			throw new Exception("getLackingOrders(row) error:" + e);
//		}
//	}

	/**
	 * 订单中的普通商品和标准套装
	 * @param print_date	打印日期
	 * @param psid			仓库
	 * @param pscid			仓库子分类
	 * @param catalog_id	商品货架
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatLeavingStorageOrderProductsByDatePsidPscidCatalogid(String print_date,long psid,long pscid,long catalog_id)
		throws Exception
	{
		try
		{
			String sql = "SELECT oi.name,sum(oi.quantity) quantity,ps.store_station,ps.store_count FROM " + ConfigBean.getStringValue("porder") + " o," + ConfigBean.getStringValue("porder_item") + " oi," + ConfigBean.getStringValue("product_storage") + " ps," + ConfigBean.getStringValue("product") + " p where o.oid=oi.oid  and o.product_status="+ProductStatusKey.IN_STORE+"  and oi.product_type!=3 and oi.product_type!=4 and oi.pid=ps.pc_id and p.pc_id=oi.pid and datediff(print_date,str_to_date('"+print_date+"','%Y-%m-%d'))=0 and o.ps_id=? and ps.cid=? and p.catalog_id=? and p.orignal_pc_id=0 group by oi.pid";
			
			DBRow para = new DBRow();
			para.add("psid",psid);
			para.add("pscid",pscid);
			para.add("catalog_id",catalog_id);
			
//			SELECT oi.name,sum(oi.quantity) FROM porder o,porder_item oi,product_storage ps,product p where o.oid=oi.oid and oi.product_type!=3 and oi.product_type!=4 and oi.pid=ps.pc_id and p.pc_id=oi.pid and datediff(print_date,str_to_date('2010-5-2','%Y-%m-%d'))=0 and o.ps_id=100000 and ps.cid=100001 and p.catalog_id=100147  group by oi.pid
//			SELECT p.catalog_id FROM porder o,porder_item oi,product p,product_storage ps where o.oid=oi.oid and oi.product_type!=3 and oi.product_type!=4 and p.pc_id=oi.pid and ps.pc_id=oi.pid and datediff(print_date,str_to_date('2010-5-2','%Y-%m-%d'))=0 and o.ps_id=100000 and ps.cid=100005   group by p.catalog_id
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getStatLeavingStorageOrderProductsByDatePsidPscidCatalogid(row) error:" + e);
		}
	}

	/**
	 * 订单中的手工套装
	 * @param print_date
	 * @param psid
	 * @param pscid
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatLeavingStorageOrderStandardManualProductsByDatePsid(String print_date,long psid)
		throws Exception
	{
		try
		{
			String sql = "SELECT oi.pid,oi.name,sum(oi.quantity) quantity FROM " + ConfigBean.getStringValue("porder") + " o," + ConfigBean.getStringValue("porder_item") + " oi where o.oid=oi.oid  and o.product_status="+ProductStatusKey.IN_STORE+"  and oi.product_type=3 and datediff(print_date,str_to_date('"+print_date+"','%Y-%m-%d'))=0 and o.ps_id=?  group by oi.pid";
			
			DBRow para = new DBRow();
			para.add("psid",psid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getStatLeavingStorageOrderStandardManualProductsByDatePsid(row) error:" + e);
		}
	}

	/**
	 * 手工套装商品
	 * @param set_pid
	 * @param pscid
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatLeavingStorageOrderStandardManualProductsBySetPidPscidCatalogid(long set_pid,long pscid,long catalog_id)
		throws Exception
	{
		try
		{
			String sql = "SELECT p.p_name,pu.quantity,ps.store_station,ps.store_count FROM " + ConfigBean.getStringValue("product") + " p,"+ConfigBean.getStringValue("product_union")+" pu," + ConfigBean.getStringValue("product_storage") + " ps where pu.pid=p.pc_id and p.pc_id=ps.pc_id and pu.set_pid=? and ps.cid=? and p.catalog_id=? and p.orignal_pc_id=0";
			
			DBRow para = new DBRow();
			para.add("set_pid",set_pid);
			para.add("pscid",pscid);
			para.add("catalog_id",catalog_id);
			
//			System.out.println(sql);
//			System.out.println(set_pid);
//			System.out.println(pscid);
//			System.out.println(catalog_id);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getStatLeavingStorageOrderStandardManualProductsByPscidCatalogid(row) error:" + e);
		}
	}
	
	

	/**
	 * 订单商品分类
	 * @param print_date
	 * @param psid
	 * @param pscid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatLeavingStorageOrderPCByDatePsidPscid(String print_date,long psid,long pscid)
		throws Exception
	{
		try
		{
			String sql = "SELECT p.catalog_id FROM " + ConfigBean.getStringValue("porder") + " o," + ConfigBean.getStringValue("porder_item") + " oi," + ConfigBean.getStringValue("product") + " p," + ConfigBean.getStringValue("product_storage") + " ps where o.oid=oi.oid  and o.product_status="+ProductStatusKey.IN_STORE+"  and oi.product_type!="+ProductTypeKey.UNION_STANDARD_MANUAL+" and oi.product_type!="+ProductTypeKey.UNION_CUSTOM+" and p.pc_id=oi.pid and ps.pc_id=oi.pid and datediff(print_date,str_to_date('"+print_date+"','%Y-%m-%d'))=0 and o.ps_id=? and ps.cid=?  group by p.catalog_id";

			DBRow para = new DBRow();
			para.add("psid",psid);
			para.add("pscid",pscid);
			
//			if (pscid == 100016l)
//			{
//				System.out.println(sql);
//				System.out.println(psid);
//				System.out.println(pscid);
//			}
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getStatLeavingStorageOrderPCByDatePsidPscid(row) error:" + e);
		}
	}
	
	/**
	 * 手工套装商品分类
	 * @param set_pid
	 * @param pscid
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatLeavingStorageOrderStandardManualPCByDatePsidPscid(long set_pid,long pscid)
		throws Exception
	{
		try
		{
			String sql = "SELECT p.catalog_id FROM " + ConfigBean.getStringValue("product") + " p,"+ConfigBean.getStringValue("product_union")+" pu," + ConfigBean.getStringValue("product_storage") + " ps where pu.set_pid=? and ps.cid=? and pu.pid=p.pc_id and p.pc_id=ps.pc_id group by p.catalog_id";
	
			DBRow para = new DBRow();
			para.add("set_pid",set_pid);
			para.add("pscid",pscid);
			
//			if (pscid == 100016l)
//			{
//				System.out.println(sql);
//				System.out.println(set_pid);
//				System.out.println(pscid);
//			}
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getStatLeavingStorageOrderStandardManualPCByDatePsidPscid(row) error:" + e);
		}
	}
	
	/**
	 * 订单中的定制套装
	 * @param print_date
	 * @param psid
	 * @param pscid
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatLeavingStorageOrderCustomProductsByDatePsid(String print_date,long psid)
		throws Exception
	{
		try
		{
			String sql = "SELECT oi.pid,oi.name,sum(oi.quantity) quantity FROM " + ConfigBean.getStringValue("porder") + " o," + ConfigBean.getStringValue("porder_item") + " oi where o.oid=oi.oid and o.product_status="+ProductStatusKey.IN_STORE+" and oi.product_type="+ProductTypeKey.UNION_CUSTOM+" and datediff(print_date,str_to_date('"+print_date+"','%Y-%m-%d'))=0 and o.ps_id=?  group by oi.pid";

			DBRow para = new DBRow();
			para.add("psid",psid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getStatLeavingStorageOrderCustomProductsByDatePsid(row) error:" + e);
		}
	}

	/**
	 * 定制套装商品
	 * @param set_pid
	 * @param pscid
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatLeavingStorageOrderCustomProductsBySetPidPscidCatalogid(long set_pid,long pscid,long catalog_id)
		throws Exception
	{
		try
		{
			String sql = "SELECT p.p_name,pu.quantity,ps.store_station,ps.store_count FROM " + ConfigBean.getStringValue("product") + " p,"+ConfigBean.getStringValue("product_custom_union")+" pu," + ConfigBean.getStringValue("product_storage") + " ps where pu.pid=p.pc_id and p.pc_id=ps.pc_id and pu.set_pid=? and ps.cid=? and p.catalog_id=? and p.orignal_pc_id=0";
			
			DBRow para = new DBRow();
			para.add("set_pid",set_pid);
			para.add("pscid",pscid);
			para.add("catalog_id",catalog_id);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getStatLeavingStorageOrderCustomProductsBySetPidPscidCatalogid(row) error:" + e);
		}
	}

	/**
	 * 定制套装商品分类
	 * @param set_pid
	 * @param pscid
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStatLeavingStorageOrderCustomPCByDatePsidPscid(long set_pid,long pscid)
		throws Exception
	{
		try
		{
			String sql = "SELECT p.catalog_id FROM " + ConfigBean.getStringValue("product") + " p,"+ConfigBean.getStringValue("product_custom_union")+" pu," + ConfigBean.getStringValue("product_storage") + " ps where pu.set_pid=? and ps.cid=? and pu.pid=p.pc_id and p.pc_id=ps.pc_id group by p.catalog_id";
	
			DBRow para = new DBRow();
			para.add("set_pid",set_pid);
			para.add("pscid",pscid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getStatLeavingStorageOrderCustomPCByDatePsidPscid(row) error:" + e);
		}
	}

	public DBRow[] getStatLeavingStorageOrderPCByDatePsidPscidView(String print_date,long psid,long pscid)
		throws Exception
	{
		try
		{
			String sql = "select catalog_id from " + ConfigBean.getStringValue("order_all_products_view") + " oapv," + ConfigBean.getStringValue("product_storage") + " ps where product_status="+ProductStatusKey.IN_STORE+" and datediff(print_date,str_to_date('"+print_date+"','%Y-%m-%d'))=0 and oapv.p_pid=ps.pc_id  and ps_id=? and ps.cid=? group by catalog_id ";
			
			DBRow para = new DBRow();
			para.add("psid",psid);
			para.add("pscid",pscid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getStatLeavingStorageOrderPCByDatePsidPscidView(row) error:" + e);
		}
	}

	public DBRow[] getStatLeavingStorageOrderProductsByDatePsidPscidCatalogidView(String print_date,long psid,long pscid,long catalog_id)
		throws Exception
	{
		try
		{
			String sql = "select oapv.p_name AS name,sum(oapv.p_quantity) quantity,ps.store_count,ps.store_station,lop.p_quantity AS hold_quantity from " + ConfigBean.getStringValue("product_storage") + " ps," + ConfigBean.getStringValue("order_all_products_view") + " oapv LEFT JOIN " + ConfigBean.getStringValue("lacking_order_products_view") + " lop ON oapv.p_pid=lop.p_pid AND lop.ps_id=? where oapv.product_status="+ProductStatusKey.IN_STORE+" and datediff(oapv.print_date,str_to_date('"+print_date+"','%Y-%m-%d'))=0 and oapv.p_pid=ps.pc_id  and oapv.ps_id=? and ps.cid=? and oapv.catalog_id=? group by oapv.p_pid order by oapv.p_name asc";
			
			DBRow para = new DBRow();
			para.add("lop.psid",psid);
			para.add("psid",psid);
			para.add("pscid",pscid);
			para.add("catalog_id",catalog_id);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getStatLeavingStorageOrderProductsByDatePsidPscidCatalogidView(row) error:" + e);
		}
	}
	
	public DBRow[] getOrdersInOids4Search(ArrayList oid)
		throws Exception
	{
		try
		{
			if (oid.size()==0)
			{
				return(new DBRow[0]);
			}

			String oidStr = "";
			int i=0;
			for (; i<oid.size()-1; i++)
			{
				oidStr += oid.get(i)+",";
			}
			oidStr += oid.get(i);
			
			String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid  where po.oid in ("+oidStr+") order by po.oid desc";
			
			return(dbUtilAutoTran.selectMutliple(sql));
		}
		catch (Exception e)
		{
			throw new Exception("getOrdersInOids4Search(row) error:" + e);
		}
	}
	
	public DBRow[] getStatLackingProductsPCByPscid(long pscid)
		throws Exception
	{
		try
		{
			String sql = "select p.catalog_id from " + ConfigBean.getStringValue("product_storage") + " ps," + ConfigBean.getStringValue("product") + " p where ps.store_count<0 and ps.pc_id=p.pc_id and ps.cid=? group by p.catalog_id";
			
			DBRow para = new DBRow();
			para.add("pscid",pscid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getStatLackingProductsPCByPscid(row) error:" + e);
		}
	}
	
	public DBRow[] getStatLackingProductsByPscidCatalogid(long pscid,long catalog_id)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product_storage") + " ps," + ConfigBean.getStringValue("product") + " p where ps.store_count<0 and ps.pc_id=p.pc_id and ps.cid=? and p.catalog_id=? and p.orignal_pc_id=0";
			
			DBRow para = new DBRow();
			para.add("pscid",pscid);
			para.add("catalog_id",catalog_id);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getStatLackingProductsByPscidCatalogid(row) error:" + e);
		}
	}

	public int getWait4PrintOrdersCount(long ps_id,String st,String en)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("handle_status",HandStatusleKey.NORMAL);
			para.add("product_status",ProductStatusKey.IN_STORE);
			para.add("handle",HandleKey.WAIT4_PRINT);
			para.add("ps_id",ps_id);
			
			String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " po where po.handle_status=? and po.product_status=? and po.handle=?  and po.ps_id=? and (parent_oid=0 or parent_oid=-1) and po.post_date >= str_to_date('"+st+"','%Y-%m-%d') and po.post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') ";
			
			return(dbUtilAutoTran.selectPreSingle(sql,para).get("c", 0));
		}
		catch (Exception e)
		{
			throw new Exception("getWait4PrintOrdersCount(row) error:" + e);
		}
	}

	public DBRow[] getInvoiceTemplatesByPsId(long ps_id)
		throws Exception
	{
		try
		{
			String sql = "SELECT *  FROM " + ConfigBean.getStringValue("invoice_template") +" where ps_id=?";

			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getInvoiceTemplatesByPsId(row) error:" + e);
		}
	}

	public DBRow[] getProfileWait4HandleOrders(String st,String en)
		throws Exception
	{
		try
		{
			String sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  handle_status="+HandStatusleKey.NORMAL+" and handle="+HandleKey.WAIT4_RECORD+" group by date(post_date)  order by date desc";

			return(dbUtilAutoTran.selectMutliple(sql));
		}
		catch (Exception e)
		{
			throw new Exception("getProfileWait4HandleOrders(row) error:" + e);
		}
	}

	public DBRow[] getProfileLackingOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			String sql;
			DBRow para = new DBRow();
			
			if (ps_id>0)
			{
				para.add("ps_id", ps_id);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  handle_status=0 and product_status=1   and ps_id=?   group by date(post_date) order by date desc";				
			}
			else
			{
				para.add("ps_id", 1);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  handle_status=0 and product_status=1   and 1=?   group by date(post_date) order by date desc";
			}
	
			
			
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getProfileLackingOrders(row) error:" + e);
		}
	}
	
	public DBRow[] getProfileWait4PrintOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			String sql;
			DBRow para = new DBRow();
			
			if (ps_id>0)
			{
				para.add("ps_id", ps_id);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  handle_status=0 and product_status=2 and handle=2  and ps_id=? and (parent_oid=0 or parent_oid=-1)  group by date(post_date) order by date desc";
			}
			else
			{
				para.add("ps_id", 1);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  handle_status=0 and product_status=2 and handle=2  and 1=? and (parent_oid=0 or parent_oid=-1)  group by date(post_date) order by date desc";
			}
			
			
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getProfileWait4PrintOrders(row) error:" + e);
		}
	}
		
	public DBRow[] getProfileDoubtOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			String sql;
			DBRow para = new DBRow();
			
			if (ps_id>0)
			{
				para.add("ps_id", ps_id);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  handle_status=1  and ps_id=?   group by date(post_date) order by date desc";
			}
			else
			{
				para.add("ps_id", 1);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  handle_status=1  and 1=?   group by date(post_date) order by date desc";
			}
	
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getProfileDoubtOrders(row) error:" + e);
		}
	}
	
	public DBRow[] getProfileDoubtAddressOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			String sql;
			DBRow para = new DBRow();
			
			if (ps_id>0)
			{
				para.add("ps_id", ps_id);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  handle_status=5  and ps_id=?   group by date(post_date) order by date desc";
			}
			else
			{
				para.add("ps_id", 1);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  handle_status=5  and 1=?   group by date(post_date) order by date desc";
			}
		
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getProfileDoubtAddressOrders(row) error:" + e);
		}
	}
	
	public DBRow[] getProfileDoubtCostOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			String sql;
			DBRow para = new DBRow();
			
			if (ps_id>0)
			{
				para.add("ps_id", ps_id);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and handle_status=0 and handle=5  and ps_id=?   group by date(post_date) order by date desc";
			}
			else
			{
				para.add("ps_id", 1);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and handle_status=0 and handle=5  and 1=?   group by date(post_date) order by date desc";
			}
	
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getProfileDoubtCostOrders(row) error:" + e);
		}
	}

	public DBRow[] getProfileDoubtPayOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			String sql;
			DBRow para = new DBRow();
			
			if (ps_id>0)
			{
				para.add("ps_id", ps_id);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  handle_status=7  and ps_id=?   group by date(post_date) order by date desc";
			}
			else
			{
				para.add("ps_id", 1);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  handle_status=7  and 1=?   group by date(post_date) order by date desc";
			}
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getProfileDoubtPayOrders(row) error:" + e);
		}
	}
		
	public DBRow[] getProfilePrintedOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			String sql;
			DBRow para = new DBRow();
			
			if (ps_id>0)
			{
				para.add("ps_id", ps_id);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  handle=3 and handle_status=0  and ps_id=?   group by date(post_date) order by date desc";				
			}
			else
			{
				para.add("ps_id", 1);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  handle=3 and handle_status=0  and 1=?   group by date(post_date) order by date desc";
			}
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getProfilePrintedOrders(row) error:" + e);
		}
	}
		
	public DBRow[] getProfileDate(String st,String en)
		throws Exception
	{
		try
		{
			String sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') group by date(post_date) desc";
			
			return(dbUtilAutoTran.selectMutliple(sql));
		}
		catch (Exception e)
		{
			throw new Exception("getProfileDate(row) error:" + e);
		}
	}

	//获得需要更新递送日期年份、月份的订单
	public DBRow[] getOrdersByYearMonth(int year,int month)
		throws Exception
	{
		try
		{
			String sql = "select oid from " + ConfigBean.getStringValue("porder") + " where  year(post_date)=? and month(post_date)=? ";
			
			DBRow para = new DBRow();
			para.add("year", year);
			para.add("month", month);

			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getOrdersByYearMonth(row) error:" + e);
		}
	}

	/**
	 * 得到所有已经发货的合并订单
	 * @param st
	 * @param en
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllDeliveriedMergeOrders()
		throws Exception
	{
		try
		{
			String sql = "select oid from " + ConfigBean.getStringValue("porder") + " where parent_oid<0 and handle=4";
			
			return(dbUtilAutoTran.selectMutliple(sql));
		}
		catch (Exception e)
		{
			throw new Exception("getAllDeliveriedMergeOrders(row) error:" + e);
		}
	}
	
	public int getTraceDoubtOrderCount(String st,String en,int tracking_day)
		throws Exception
	{
		try
		{
			String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(trace_date,interval "+tracking_day+" day) <= current_date and internal_tracking_status !="+WaybillInternalTrackingKey.DeliveryCompleted+" and handle_status= "+HandStatusleKey.DOUBT+" and post_date>= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')";
				
			return(dbUtilAutoTran.selectSingle(sql).get("c", 0));
		}
		catch (Exception e)
		{
			throw new Exception("getTraceDoubtOrderCount(row) error:" + e);
		}
	}
	
	public DBRow[] getTraceDoubtOrders(String st,String en,int doubt_order_day,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(trace_date,interval "+doubt_order_day+" day) <= current_date and handle_status= "+HandStatusleKey.DOUBT+" and post_date>= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') order by oid asc";
				
			return(dbUtilAutoTran.selectMutliple(sql, pc));
		}
		catch (Exception e)
		{
			throw new Exception("getTraceDoubtOrders(row) error:" + e);
		}
	}

	public int getTraceDoubtAddressOrderCount(String st,String en,int doubt_address_day)
		throws Exception
	{
		try
		{
			
			String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(trace_date,interval "+doubt_address_day+" day) <= current_date and handle_status="+HandStatusleKey.DOUBT_ADDRESS+" and post_date>= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')";
			return(dbUtilAutoTran.selectSingle(sql).get("c", 0));

		}
		catch (Exception e)
		{
			throw new Exception("getTraceDoubtAddressOrderCount(row) error:" + e);
		}
	}
	
	public DBRow[] getTraceDoubtAddressOrders(String st,String en,int doubt_address_day,PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(trace_date,interval "+doubt_address_day+" day) <= current_date and handle_status= "+HandStatusleKey.DOUBT_ADDRESS+" and post_date>= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') order by oid asc";
				
			return(dbUtilAutoTran.selectMutliple(sql, pc));
		}
		catch (Exception e)
		{
			throw new Exception("getTraceDoubtAddressOrders(row) error:" + e);
		}
	}
	
	public int getTraceDoubtPayOrderCount(String st,String en,int doubt_pay_day)
		throws Exception
	{
		try
		{
			String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(trace_date,interval "+doubt_pay_day+" day) <= current_date and handle_status="+HandStatusleKey.DOUBT_PAY+" and post_date>= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')";
			return(dbUtilAutoTran.selectSingle(sql).get("c", 0));
		}
		catch (Exception e)
		{
			throw new Exception("getTraceDoubtPayOrderCount(row) error:" + e);
		}
	}
	
	public DBRow[] getTraceDoubtPayOrders(String st,String en,int doubt_pay_day,PageCtrl pc)
		throws Exception
	{
		try
		{
				String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(trace_date,interval "+doubt_pay_day+" day) <= current_date and handle_status= "+HandStatusleKey.DOUBT_PAY+" and post_date>= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') order by oid asc";
				
				return(dbUtilAutoTran.selectMutliple(sql, pc));
		}
		catch (Exception e)
		{
			throw new Exception("getTraceDoubtPayOrders(row) error:" + e);
		}
	}

	public int getTraceLackingOrderCount(long trace_operator_role_id)
		throws Exception
	{
		try
		{
			if ( trace_operator_role_id==100006l||trace_operator_role_id==100015l )
			{
				DBRow para = new DBRow();
				para.add("trace_operator_role_id", trace_operator_role_id);
				
				String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and product_status=1 and trace_operator_role_id=?";
				
				return(dbUtilAutoTran.selectPreSingle(sql,para).get("c", 0));
			}
			else
			{
				String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and product_status=1 ";
				
				return(dbUtilAutoTran.selectSingle(sql).get("c", 0));
			}
			
		}
		catch (Exception e)
		{
			throw new Exception("getTraceLackingOrderCount(row) error:" + e);
		}
	}

	public DBRow[] getTraceLackingOrders(long trace_operator_role_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			if ( trace_operator_role_id==100006l||trace_operator_role_id==100015l )
			{
				DBRow para = new DBRow();
				para.add("trace_operator_role_id", trace_operator_role_id);
				
				String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and product_status=1 and trace_operator_role_id=? order by trace_date asc,oid asc";
				
				if (pc!=null)
				{
					return(dbUtilAutoTran.selectPreMutliple(sql,para, pc));
				}
				else
				{
					return(dbUtilAutoTran.selectPreMutliple(sql,para));
				}
			}
			else
			{
				
				String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and product_status=1  order by trace_date asc,oid asc";
				
				return(dbUtilAutoTran.selectMutliple(sql, pc));
			}

		}
		catch (Exception e)
		{
			throw new Exception("getTraceLackingOrders(row) error:" + e);
		}
	}
	
	public DBRow[] getTracePrintDoubtOrders(long trace_operator_role_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			if ( trace_operator_role_id==100006l||trace_operator_role_id==100015l )
			{
				DBRow para = new DBRow();
				para.add("trace_operator_role_id", trace_operator_role_id);
				
				String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and handle_status=9  and trace_operator_role_id=? order by trace_date asc,oid asc";
				
				if (pc!=null)
				{
					return(dbUtilAutoTran.selectPreMutliple(sql,para, pc));
				}
				else
				{
					return(dbUtilAutoTran.selectPreMutliple(sql,para));
				}
			}
			else
			{
				
				String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and handle_status=9   order by trace_date asc,oid asc";
				
				return(dbUtilAutoTran.selectMutliple(sql, pc));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getTracePrintDoubtOrders(row) error:" + e);
		}
	}
		
	public int getVerifyCostOrderCount(long trace_operator_role_id)
		throws Exception
	{
		try
		{
			if ( trace_operator_role_id==100006l||trace_operator_role_id==100015l )
			{
				DBRow para = new DBRow();
				para.add("trace_operator_role_id", trace_operator_role_id);
				
				String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and handle_status=0 and handle=5 and trace_operator_role_id=?";
				
				return(dbUtilAutoTran.selectPreSingle(sql,para).get("c", 0));
			}
			else
			{
				String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and handle_status=0 and handle=5 ";
				
				return(dbUtilAutoTran.selectSingle(sql).get("c", 0));
			}
			
		}
		catch (Exception e)
		{
			throw new Exception("getVerifyCostOrderCount(row) error:" + e);
		}
	}
	
	public int getPrintDoubtOrderCount(long trace_operator_role_id)
		throws Exception
	{
		try
		{
			if ( trace_operator_role_id==100006l||trace_operator_role_id==100015l )
			{
				DBRow para = new DBRow();
				para.add("trace_operator_role_id", trace_operator_role_id);
				
				String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and handle_status=9 and trace_operator_role_id=?";
				
				return(dbUtilAutoTran.selectPreSingle(sql,para).get("c", 0));
			}
			else
			{
				String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and handle_status=9 ";
				return(dbUtilAutoTran.selectSingle(sql).get("c", 0));
			}

		}
		catch (Exception e)
		{
			throw new Exception("getPrintDoubtOrderCount(row) error:" + e);
		}
	}
	
	public DBRow[] getVerifyCostOrders(long trace_operator_role_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			if ( trace_operator_role_id==100006l||trace_operator_role_id==100015l )
			{
				DBRow para = new DBRow();
				para.add("trace_operator_role_id", trace_operator_role_id);
				
				String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and handle_status=0 and handle=5 and trace_operator_role_id=? order by trace_date asc,oid asc";
				
				if (pc!=null)
				{
					return(dbUtilAutoTran.selectPreMutliple(sql,para ,pc));
				}
				else
				{
					return(dbUtilAutoTran.selectPreMutliple(sql,para));
				}
			}
			else
			{
				
				String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and handle_status=0 and handle=5  order by trace_date asc,oid asc";
				
				return(dbUtilAutoTran.selectMutliple(sql ,pc));
			}

		}
		catch (Exception e)
		{
			throw new Exception("getVerifyCostOrders(row) error:" + e);
		}
	}
		
	public long addTraceDelay(DBRow row)
		throws Exception
	{
		try
		{
			long td_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("trace_delay"));
			row.add("td_id",td_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("trace_delay"),row);
			return(td_id);
		}
		catch (Exception e)
		{
			throw new Exception("addTraceDelay(row) error:" + e);
		}
	}
	
	public DBRow[] getTraceDelay(PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("trace_delay")+" order by td_id desc";
			
			if (pc!=null)
			{
				return(dbUtilAutoTran.selectMutliple(sql, pc));
			}
			else
			{
				return(dbUtilAutoTran.selectMutliple(sql));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getTraceDelay(row) error:" + e);
		}
	}
	
	public DBRow[] getPOrderNoteByOidTraceType(long oid,int trace_type,PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql = "select * from " + ConfigBean.getStringValue("porder_note") + " where oid=? and trace_type=? order by on_id asc";
			
			DBRow para = new DBRow();
			para.add("oid",oid);
			para.add("trace_type",trace_type);

			if (pc==null)
			{
				return(dbUtilAutoTran.selectPreMutliple(sql, para));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql, para,pc));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getPOrderNoteByOidTraceType(row) error:" + e);
		}
	}
	
	public DBRow[] getOrdersByYearMonthRange(long ps_id,int year,int month,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("porder") + " where ps_id=? and year(post_date)=? and month(post_date)=?";
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("year",year);
			para.add("month",month);
	
			if (pc==null)
			{
				return(dbUtilAutoTran.selectPreMutliple(sql, para));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql, para,pc));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getOrdersByYearMonthRange(row) error:" + e);
		}
	}
	
	public void updateOldOrderCost(int month)
		throws Exception
	{
		try
		{
//			dbUtilAutoTran.updateOldOrderCost(month);
		}
		catch (Exception e)
		{
			throw new Exception("updateOldOrderCost(row) error:" + e);
		}
	}
	
	public void updateOldOrderCostTMP()
		throws Exception
	{
		try
		{
//			dbUtilAutoTran.updateOldOrderCostTMP();
		}
		catch (Exception e)
		{
			throw new Exception("updateOldOrderCost(row) error:" + e);
		}
	}
	
	/**
	 * 增加订单缺货清单
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addOrderLackingList(DBRow row)
		throws Exception
	{
		try
		{
			long oll_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("order_lacking_list"));
			row.add("oll_id",oll_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("order_lacking_list"),row);
			return(oll_id);
		}
		catch (Exception e)
		{
			throw new Exception("addOrderLackingList(row) error:" + e);
		}
	}
	
	/**
	 * 删除订单缺货清单
	 * @param oid
	 * @throws Exception
	 */
	public void delOrderLackingListByOid(long oid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where oid=" + oid,ConfigBean.getStringValue("order_lacking_list"));
		}
		catch (Exception e)
		{
			throw new Exception("delOrderLackingListByOid() error:" + e);
		}
	}		
	
	public DBRow[] getOrderLackingProductListByOid(long oid,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("order_lacking_list") + " where oid=?";
			
			DBRow para = new DBRow();
			para.add("oid",oid);
	
			if (pc==null)
			{
				return(dbUtilAutoTran.selectPreMutliple(sql, para));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql, para,pc));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getOrderLackingProductList(row) error:" + e);
		}
	}

	public int getDisputeRefundCount(long trace_operator_role_id)
		throws Exception
	{
		try
		{
			if ( trace_operator_role_id==100006l||trace_operator_role_id==100015l )
			{
				DBRow para = new DBRow();
				para.add("trace_operator_role_id", trace_operator_role_id);
				
				String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and after_service_status=7 and trace_operator_role_id=?";
				
				return(dbUtilAutoTran.selectPreSingle(sql,para).get("c", 0));
			}
			else
			{
				String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and after_service_status=7 ";
				
				return(dbUtilAutoTran.selectSingle(sql).get("c", 0));
			}

		}
		catch (Exception e)
		{
			throw new Exception("getDisputeRefundCount(row) error:" + e);
		}
	}
	
	public int getDisputeWarrantyCount(long trace_operator_role_id)
		throws Exception
	{
		try
		{
			if ( trace_operator_role_id==100006l||trace_operator_role_id==100015l )
			{
				DBRow para = new DBRow();
				para.add("trace_operator_role_id", trace_operator_role_id);
				
				String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and after_service_status=11 and trace_operator_role_id=?";
				
				return(dbUtilAutoTran.selectPreSingle(sql,para).get("c", 0));
			}
			else
			{
				String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and after_service_status=11 ";
				
				return(dbUtilAutoTran.selectSingle(sql).get("c", 0));
			}

		}
		catch (Exception e)
		{
			throw new Exception("getDisputeWarrantyCount(row) error:" + e);
		}
	}

	public int getOtherDisputeCount(long trace_operator_role_id)
		throws Exception
	{
		try
		{
			if ( trace_operator_role_id==100006l||trace_operator_role_id==100015l )
			{
				DBRow para = new DBRow();
				para.add("trace_operator_role_id", trace_operator_role_id);
				
				String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and after_service_status=13 and trace_operator_role_id=?";
				
				return(dbUtilAutoTran.selectPreSingle(sql,para).get("c", 0));
			}
			else
			{
				
				String sql = "select count(oid) c from " + ConfigBean.getStringValue("porder") + " where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and after_service_status=13 ";
				
				return(dbUtilAutoTran.selectSingle(sql).get("c", 0));
			}

		}
		catch (Exception e)
		{
			throw new Exception("getDisputeWarrantyCount(row) error:" + e);
		}
	}	
			
	
	
	
	
	public DBRow[] getTraceDisputeRefundOrders(long trace_operator_role_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			if ( trace_operator_role_id==100006l||trace_operator_role_id==100015l )
			{
				DBRow para = new DBRow();
				para.add("trace_operator_role_id", trace_operator_role_id);
				
				String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and after_service_status=7 and trace_operator_role_id=? order by trace_date asc,oid asc";
				
				if (pc!=null)
				{
					return(dbUtilAutoTran.selectPreMutliple(sql,para, pc));
				}
				else
				{
					return(dbUtilAutoTran.selectPreMutliple(sql,para));
				}
			}
			else
			{
				
				String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and after_service_status=7  order by trace_date asc,oid asc";
				
				return(dbUtilAutoTran.selectMutliple(sql, pc));
			}

		}
		catch (Exception e)
		{
			throw new Exception("getTraceDisputeRefundOrders(row) error:" + e);
		}
	}

	public DBRow[] getTraceDisputeWarrantyOrders(long trace_operator_role_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			if ( trace_operator_role_id==100006l||trace_operator_role_id==100015l )
			{
				DBRow para = new DBRow();
				para.add("trace_operator_role_id", trace_operator_role_id);
				
				String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and after_service_status=11 and trace_operator_role_id=? order by trace_date asc,oid asc";
				
				if (pc!=null)
				{
					return(dbUtilAutoTran.selectPreMutliple(sql,para, pc));
				}
				else
				{
					return(dbUtilAutoTran.selectPreMutliple(sql,para));
				}
			}
			else
			{
				
				String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and after_service_status=11  order by trace_date asc,oid asc";
				
				return(dbUtilAutoTran.selectMutliple(sql, pc));
			}

		}
		catch (Exception e)
		{
			throw new Exception("getTraceDisputeWarrantyOrders(row) error:" + e);
		}
	}
		
	public DBRow[] getTraceOtherDisputeOrders(long trace_operator_role_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			if ( trace_operator_role_id==100006l||trace_operator_role_id==100015l )
			{
				DBRow para = new DBRow();
				para.add("trace_operator_role_id", trace_operator_role_id);
				
				String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and after_service_status=13 and trace_operator_role_id=? order by trace_date asc,oid asc";
				
				if (pc!=null)
				{
					return(dbUtilAutoTran.selectPreMutliple(sql,para, pc));
				}
				else
				{
					return(dbUtilAutoTran.selectPreMutliple(sql,para));
				}
			}
			else
			{
				
				String sql = "select * from " + ConfigBean.getStringValue("porder") + " po left join " + ConfigBean.getStringValue("country_code") + " cc on po.ccid=cc.ccid where date_add(current_date,interval 1 day) > trace_date and trace_flag=1 and after_service_status=13  order by trace_date asc,oid asc";
				
				return(dbUtilAutoTran.selectMutliple(sql, pc));
			}

		}
		catch (Exception e)
		{
			throw new Exception("getTraceOtherDisputeOrders(row) error:" + e);
		}
	}
		
	
	
	
	
	
	
	public DBRow[] getProfileDisputeRefundOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			String sql;
			DBRow para = new DBRow();
			
			if (ps_id>0)
			{
				para.add("ps_id", ps_id);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  after_service_status=7  and ps_id=?   group by date(post_date) order by date desc";
			}
			else
			{
				para.add("ps_id", 1);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  after_service_status=7  and 1=?   group by date(post_date) order by date desc";
			}
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getProfileDisputeRefundOrders(row) error:" + e);
		}
	}
			
	public DBRow[] getProfileDisputeWarrantyOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			String sql;
			DBRow para = new DBRow();
			
			if (ps_id>0)
			{
				para.add("ps_id", ps_id);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  after_service_status=11  and ps_id=?   group by date(post_date) order by date desc";
			}
			else
			{
				para.add("ps_id", 1);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  after_service_status=11  and 1=?   group by date(post_date) order by date desc";
			}
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getProfileDisputeWarrantyOrders(row) error:" + e);
		}
	}
			
	public DBRow[] getProfileOtherDisputingOrders(String st,String en,long ps_id)
		throws Exception
	{
		try
		{
			String sql;
			DBRow para = new DBRow();
			
			if (ps_id>0)
			{
				para.add("ps_id", ps_id);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  after_service_status=13  and ps_id=?   group by date(post_date) order by date desc";
			}
			else
			{
				para.add("ps_id", 1);
				sql = "select DATE_FORMAT(post_date,'%Y-%m-%d') date,count(oid) c  from " + ConfigBean.getStringValue("porder") + " where str_to_date('"+st+"','%Y-%m-%d') <= post_date and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and  after_service_status=13  and 1=?   group by date(post_date) order by date desc";
			}
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getProfileOtherDisputingOrders(row) error:" + e);
		}
	}
	
	public DBRow[] getAdditionMoneyOrders(long oid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("oid", oid);
			
			String sql = "select * from " + ConfigBean.getStringValue("porder") + " where addition_money_oid=? order by oid desc";
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getAdditionMoneyOrders(row) error:" + e);
		}
	}
	
	public long addReturnInfoByRpId(DBRow row)
		throws Exception
	{
		try
		{
			long rp_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("return_product"));
			row.add("rp_id",rp_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("return_product"),row);
			return(rp_id);
		}
		catch (Exception e)
		{
			throw new Exception("addReturnInfoByRpId(row) error:" + e);
		}
	}
	
	public void delReturnProducts(long rp_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where rp_id=" + rp_id,ConfigBean.getStringValue("return_product"));
		}
		catch (Exception e)
		{
			throw new Exception("delReturnProducts(row) error:" + e);
		}
	}
	
	public long addReturnProductItems(DBRow row)
		throws Exception
	{
		try
		{
			long rpi_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("return_product_items"));
			row.add("rpi_id",rpi_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("return_product_items"),row);
			return(rpi_id);
		}
		catch (Exception e)
		{
			throw new Exception("addReturnProductItems(row) error:" + e);
		}
	}
		
	public void delReturnProductItems(long rpi_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where rpi_id=" + rpi_id,ConfigBean.getStringValue("return_product_items"));
		}
		catch (Exception e)
		{
			throw new Exception("delReturnProductItems(row) error:" + e);
		}
	}
	
	public long addReturnProductSubItems(DBRow row)
		throws Exception
	{
		try
		{
			long rpsi_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("return_product_sub_items"));
			row.add("rpsi_id",rpsi_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("return_product_sub_items"),row);
			return(rpsi_id);
		}
		catch (Exception e)
		{
			throw new Exception("addReturnProductSubItems(row) error:" + e);
		}
	}
		
	public void delReturnProductSubItems(long rpsi_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where rpsi_id=" + rpsi_id,ConfigBean.getStringValue("return_product_sub_items"));
		}
		catch (Exception e)
		{
			throw new Exception("delReturnProductSubItems(row) error:" + e);
		}
	}
	
	public DBRow[] getAllReturnProduct(PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("return_product")+" order by rp_id desc";
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getReturnProductItemsByRpId(row) error:" + e);
		}
	}
		
	public DBRow[] getReturnProductItemsByRpId(long rp_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("rp_id", rp_id);
			
			String sql = "select rpi.* from "+ConfigBean.getStringValue("return_product")+" rp,"+ConfigBean.getStringValue("return_product_items")+" rpi where rp.rp_id=? and rp.rp_id=rpi.rp_id";
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getReturnProductItemsByRpId(row) error:" + e);
		}
	}

	public DBRow[] getReturnProductSubItemsByRpId(long rp_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("rp_id", rp_id);
			
			String sql = "select rpsi.* from "+ConfigBean.getStringValue("return_product")+" rp,"+ConfigBean.getStringValue("return_product_items")+" rpi,"+ConfigBean.getStringValue("return_product_sub_items")+" rpsi where rp.rp_id=? and rp.rp_id=rpi.rp_id and rpi.rpi_id=rpsi.rpi_id";
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getReturnProductSubItemsByRpId(row) error:" + e);
		}
	}
		
	public DBRow getDetailReturnProductByRpId(long rp_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("rp_id",rp_id);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("return_product") + " where rp_id=?",para));
//			return(dbUtilAutoTran.selectPreSingleCache("select * from " + ConfigBean.getStringValue("return_product") + " where rp_id=?",para,new String[]{ConfigBean.getStringValue("return_product")}));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailReturnProductByRpId(row) error:" + e);
		}
	}
		
	public void delReturnProductSubItemsByRpId(long rp_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where rpi_id in (select rpi_id from " + ConfigBean.getStringValue("return_product") + " rp, "+ConfigBean.getStringValue("return_product_items")+"  rpi  where rp.rp_id="+rp_id+" and rp.rp_id=rpi.rp_id )",ConfigBean.getStringValue("return_product_sub_items"));
		}
		catch (Exception e)
		{
			throw new Exception("delReturnProductSubItemsByRpId(row) error:" + e);
		}
	}
	
	public void delReturnProductItemsByRpId(long rp_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where rp_id=" + rp_id,ConfigBean.getStringValue("return_product_items"));
		}
		catch (Exception e)
		{
			throw new Exception("delReturnProductItemsByRpId(row) error:" + e);
		}
	}
			
	public DBRow[] searchReturnProductByRpId(long rp_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("rp_id", rp_id);
			para.add("oid",rp_id);
			
			String sql = "select * from "+ConfigBean.getStringValue("return_product")+" where rp_id=? or oid = ?";
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("searchReturnProductByRpId(row) error:" + e);
		}
	}
				
	public void modReturnProductSubItemsByRpsiId(long rpsi_id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where rpsi_id=" + rpsi_id,ConfigBean.getStringValue("return_product_sub_items"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modReturnProductSubItemsByRpsiId(row) error:" + e);
		}
	}
	
	public void modReturnInfoByRpId(long rp_id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where rp_id=" + rp_id,ConfigBean.getStringValue("return_product"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modReturnInfoByRpId(row) error:" + e);
		}
	}
		
	public void addBadFeedBack(DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.insert(ConfigBean.getStringValue("bad_feedback"),row);
		}
		catch (Exception e)
		{
			throw new Exception("addBadFeedBack(row) error:" + e);
		}
	}
		
	public DBRow[] getBadFeedBacksByOid(long oid)
		throws Exception
	{
		try
		{
			
			String sql = "select * from " + ConfigBean.getStringValue("bad_feedback") + " where oid=? ";
			
			DBRow para = new DBRow();
			para.add("oid",oid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e)
		{
			throw new Exception("getBadFeedBacksByOid(row) error:" + e);
		}
	}
	
	public DBRow[] getExpireReturnInfo(int days)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("return_product")+" where datediff(current_date,create_date) > ? and status=0";
			
			DBRow para = new DBRow();
			para.add("days", days);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getExpireReturnInfo(row) error:" + e);
		}
	}
		
	
	public void updateOrderScIdTMP()
		throws Exception
	{
		try
		{
			dbUtilAutoTran.updateOrderScIdTMP();
		}
		catch (Exception e)
		{
			throw new Exception("updateOrderScIdTMP(row) error:" + e);
		}
	}
	
	public void updateShippingScIdTMP()
		throws Exception
	{
		try
		{
			dbUtilAutoTran.updateShippingScIdTMP();
		}
		catch (Exception e)
		{
			throw new Exception("updateShippingScIdTMP(row) error:" + e);
		}
	}
	
	public void updateSellerID()
		throws Exception
	{
		try
		{
//			dbUtilAutoTran.updateSellerID();
		}
		catch (Exception e)
		{
			throw new Exception("updateSellerID(row) error:" + e);
		}
	}	

	public DBRow[] getAllCountrysRejPsid(long ps_id)
		throws Exception
	{
		try
		{
			String sql = "SELECT *  FROM " + ConfigBean.getStringValue("country_code") + " c," + ConfigBean.getStringValue("storage_country") + " sc," + ConfigBean.getStringValue("product_storage_catalog") + " psc where c.ccid=sc.cid and psc.native!=c.ccid and psc.id=? order by c_country asc";
			DBRow para = new DBRow();
			para.add("ps_id", ps_id);
			
			DBRow country[] = dbUtilAutoTran.selectPreMutliple(sql,para);
			
			return(country);
		}
		catch (Exception e)
		{
			throw new Exception("getAllCountryCode(row) error:" + e);
		}
	}
	
	public void updateAfterServiceStatus()
		throws Exception
	{
		try
		{
//			dbUtilAutoTran.updateAfterServiceStatus();
		}
		catch (Exception e)
		{
			throw new Exception("updateAfterServiceStatus(row) error:" + e);
		}
	}	
		
	public long addWarrantyOrder(DBRow row)
		throws Exception
	{
		try
		{
			long wo_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("warranty_order"));
			row.add("wo_id",wo_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("warranty_order"),row);
			return(wo_id);
		}
		catch (Exception e)
		{
			throw new Exception("addWarrantyOrder(row) error:" + e);
		}
	}
	
	public long addWarrantyOrderItems(DBRow row)
		throws Exception
	{
		try
		{
			long woi_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("warranty_order_items"));
			row.add("woi_id",woi_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("warranty_order_items"),row);
			return(woi_id);
		}
		catch (Exception e)
		{
			throw new Exception("addWarrantyOrderItems(row) error:" + e);
		}
	}
		
	public void modWarrantyOrder(long wo_id,DBRow row)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.update("where wo_id=" + wo_id,ConfigBean.getStringValue("warranty_order"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modWarrantyOrder(row) error:" + e);
		}
	}
		
	public DBRow[] getAllWarrantyOrders(PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql = "select * from " + ConfigBean.getStringValue("warranty_order") + " order by wo_id desc";
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getAllWarrantyOrders(row) error:" + e);
		}
	}
		
	public void delWarrantyOrderItemsByWoId(long wo_id)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.delete("where wo_id=" + wo_id,ConfigBean.getStringValue("warranty_order_items"));
		}
		catch (Exception e)
		{
			throw new Exception("delWarrantyOrder(row) error:" + e);
		}
	}
			
	public DBRow getDetailWarrantyOrderByWoid(long woid)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("woid",woid);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("warranty_order") + " where wo_id=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailWarrantyOrderByWoid(row) error:" + e);
		}
	}	
		
	public DBRow[] getWarrantyOrderItemsByWoId(long woid)
		throws Exception
	{
		try
		{
			
			String sql = "select * from " + ConfigBean.getStringValue("warranty_order_items") + " where wo_id=? order by wo_id asc";
			
			DBRow para = new DBRow();
			para.add("woid",woid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e)
		{
			throw new Exception("getWarrantyOrderItemsByWoId(row) error:" + e);
		}
	}
		
	public DBRow[] getWarrantyOrdersByPayerEmail(String email,PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql = "select * from " + ConfigBean.getStringValue("warranty_order") + " wo,"+ConfigBean.getStringValue("porder")+" o where o.client_id=? and o.oid=wo.oid order by wo_id desc";
			
			DBRow para = new DBRow();
			para.add("email",email);
			
			return(dbUtilAutoTran.selectPreMutliple(sql, para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getWarrantyOrdersByPayerEmail(row) error:" + e);
		}
	}
		
	public DBRow getDetailShipping(long oid)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("oid",oid);
			return(dbUtilAutoTran.selectPreSingle("select * from ship_product where name=?  GROUP BY name",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailShipping(row) error:" + e);
		}
	}
	
	/**
	 * 根据订单明细ID获得订单明细
	 * @param iid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailOrderItem(long iid)
		throws Exception
	{
		try 
		{
			String sql = "select * from porder_item where iid = ?";
			
			DBRow para = new DBRow();
			para.add("iid",iid);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e)
		{
			throw new Exception("FloorOrderMgr getDetailOrderItem error:"+e);
		}
	}
	
	/**
	 * 运单扣库存，减少待发货数量
	 * @param iid
	 * @param count
	 * @throws Exception
	 */
	public void subWaitQuantity(long iid,float count)
		throws Exception
	{
		dbUtilAutoTran.decreaseFieldVal(ConfigBean.getStringValue("porder_item"),"where iid="+iid,"wait_quantity",count);
	}
	
	/**
	 * 运单取消减少待发货数
	 * @param iid
	 * @param count
	 * @throws Exception
	 */
	public void returnWaitQuantity(long iid, float count)
		throws Exception
	{
		dbUtilAutoTran.increaseFieldVal(ConfigBean.getStringValue("porder_item"),"where iid="+iid,"wait_quantity",count);
	}
	
	/**
	 * 返回待发货数量不为0的明细
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] waitQuantityNotZero(long oid)
		throws Exception
	{
		String sql = "select * from porder_item where oid = ? and wait_quantity>0";
		
		DBRow para = new DBRow();
		para.add("oid",oid);
		
		return (dbUtilAutoTran.selectPreMutliple(sql, para));
	}
	
	/**
	 * 返回未上运单的订单明细
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] noShipmentItems(long oid)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("porder_item")+" where oid = ? and wait_quantity=quantity";
		
		DBRow para = new DBRow();
		para.add("oid",oid);
		
		return (dbUtilAutoTran.selectPreMutliple(sql, para));
	}
	
	public DBRow[] getAllOrders()
		throws Exception
	{
		return dbUtilAutoTran.select(ConfigBean.getStringValue("porder"));
	}
	
	/**
	 * 订单统计
	 * @param st
	 * @param en
	 * @param pc_id
	 * @param product_line_id
	 * @param catalog_id
	 * @param ca_id
	 * @param ccid
	 * @param pro_id
	 * @param cost
	 * @param cost_type
	 * @param unfinished
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] statisticsOrder(String st,String en,long pc_id,long product_line_id,long catalog_id,long ca_id,long ccid,long pro_id,int cost,int cost_type,int unfinished,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String costType = "";
			switch (cost_type)
			{
				case 1:
					costType = "outbound_cost";
				break;
				
				case 2:
					costType = "all_cost";
				break;
			}
			
			String whereCost = "";
			if(cost>-1)
			{
				whereCost = " and "+costType+">"+cost*60*60*1000+" ";
			}
			
			String whereUnfinished = "";
			if(unfinished>-1)
			{
				
				TDate tdate = new TDate();
				tdate.addDay(unfinished*-1);
				whereUnfinished = " and post_date<'"+tdate.formatDate("yyyy-MM-dd HH:mm:ss")+"' and handle_status !="+HandleKey.DELIVERIED+" and handle !="+HandStatusleKey.CANCEL+" ";
			}
			
			String whereProductLineOrCatalog = "";
			
			if(catalog_id>0)
			{
				whereProductLineOrCatalog = " join pc_child_list as pcl on pcl.pc_id = pc.id and pcl.search_rootid = "+catalog_id+" ";
			}
			else if(product_line_id>0&&catalog_id==0)
			{
				whereProductLineOrCatalog = " and pc.product_line_id = "+product_line_id+" "; 
			}
			
			
			String whereArea = "";
			if(ca_id>0)
			{
				whereArea = " and ca.ca_id = "+ca_id+" ";	
			}
			
			String whereCountry = "";
			if(ccid>0)
			{
				whereCountry = " and o.ccid = "+ccid+" ";
			}
			
			String whereProvince = "";
			if(pro_id>0||pro_id==-1)
			{
				whereProvince = " and o.pro_id = "+pro_id+" ";
			}
			
			String sql =  "";
			
			
			if(pc_id>0)
			{
				
				/*
				 * 查询商品，单据上商品区分普通商品，套装商品，手动拼装，定制商品
				 * 套装商品，手动拼装，定制商品拆散后用包含输入商品也查询出来
				*/

				sql = "select DISTINCT o.* from porder as o "
					+"join porder_item as oi on o.oid=oi.oid and (oi.product_type = 1 or oi.product_type =2)"
					+"join product as p on oi.pid = p.pc_id and p.pc_id = "+pc_id+" "
					+"join country_area_mapping as cam on cam.ccid = o.ccid "+whereCountry
					+"join country_area as ca on ca.ca_id = cam.ca_id "+whereArea
					+"where o.post_date>'"+st+" 0:00:00' and o.post_date<'"+en+" 23:59:59'"+whereCost+whereUnfinished+whereProvince
					+"union "
					+"select DISTINCT o.* from porder as o "
					+"join porder_item as oi on o.oid = oi.oid and (oi.product_type = 2 or oi.product_type =3 or oi.product_type=4)"
					+"join product_union as pu on oi.pid = pu.set_pid "
					+"join product as p on pu.pid = p.pc_id and p.pc_id = "+pc_id+" "
					+"join country_area_mapping as cam on cam.ccid = o.ccid "+whereCountry
					+"join country_area as ca on ca.ca_id = cam.ca_id "+whereArea
					+"where o.post_date>'"+st+" 0:00:00' and o.post_date<'"+en+" 23:59:59'"+whereCost+whereUnfinished+whereProvince
					+"order by oid desc";
			}
			else
			{
				sql = "select DISTINCT o.* from porder as o "
					+"join porder_item as oi on o.oid=oi.oid "
					+"join product as p on oi.pid = p.pc_id "
					+"join product_catalog as pc on pc.id = p.catalog_id "
					+whereProductLineOrCatalog
					+"join country_area_mapping as cam on cam.ccid = o.ccid "+whereCountry
					+"join country_area as ca on ca.ca_id = cam.ca_id "+whereArea
					+"join country_code as cc on cc.ccid = cam.ccid "+whereCountry
					+"left join country_province as cp on cp.pro_id = o.pro_id "
					+"where o.post_date>'"+st+" 0:00:00' and o.post_date<'"+en+" 23:59:59'"+whereCost+whereUnfinished+whereProvince
					+"order by oid desc";
			}
			
			return (dbUtilAutoTran.selectMutliple(sql,pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorOrderMgr statisticsOrder error:"+e);
		}
	}
	
	public long addCodeMiss(DBRow codeMiss)
		throws Exception
	{
		return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("code_miss"),codeMiss);
	}
	
	/**
	 * 获得所有超期未妥投（正常运输）的订单
	 * @param st
	 * @param en
	 * @param notyet_deliveryed_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNotyetDeliveryedOrders(String st,String en,int notyet_deliveryed_day,int overdue_deliveryed_day,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("porder")+" where date_add(delivery_date,interval "+overdue_deliveryed_day+" day) <= current_date and date_add(trace_date,interval "+notyet_deliveryed_day+" day) <= current_date and internal_tracking_status="+WaybillInternalTrackingKey.NormalTransport+" and post_date >= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') order by oid asc ";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderMgr getNotyetDeliveryedOrders error:"+e);
		}
	}
	
	/**
	 * 获得超期未妥投(正常运输)的订单数量
	 * @param st
	 * @param en
	 * @param notyet_deliveryed_day
	 * @return
	 * @throws Exception
	 */
	public int getNotyetDeliveryedCount(String st,String en,int notyet_deliveryed_day,int overdue_deliveryed_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(oid) as notyet_deliveryed_count from "+ConfigBean.getStringValue("porder")+" where date_add(delivery_date,interval "+overdue_deliveryed_day+" day) <= current_date and date_add(trace_date,interval "+notyet_deliveryed_day+" day) <= current_date and internal_tracking_status="+WaybillInternalTrackingKey.NormalTransport+" and post_date >= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')";
			
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			
			return row.get("notyet_deliveryed_count",0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderMgr getNotyetDeliveryedCount error:"+e);
		}
	}
	
	/**
	 * 清关例外需跟进订单
	 * @param st
	 * @param en
	 * @param customs_exception_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCustomsExceptionOrders(String st,String en,int customs_exception_day,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("porder")+" where date_add(trace_date,interval "+customs_exception_day+" day) <= current_date and internal_tracking_status = "+WaybillInternalTrackingKey.CustomsClearanceError+" and post_date >= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') order by oid asc ";
			
			return dbUtilAutoTran.selectMutliple(sql,pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderMgr getCustomsExceptionOrders error:"+e);
		}
	}
	/**
	 * 获得清关例外订单数量
	 * @param st
	 * @param en
	 * @param customs_exception_day
	 * @return
	 * @throws Exception
	 */
	public int getCustomsExceptionCount(String st,String en,int customs_exception_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(oid) as customs_exception_count from "+ConfigBean.getStringValue("porder")+" where date_add(trace_date,interval "+customs_exception_day+" day) <= current_date and internal_tracking_status = "+WaybillInternalTrackingKey.CustomsClearanceError+" and post_date >= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')";
			
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			
			return row.get("customs_exception_count",0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderMgr getCustomsExceptionCount error:"+e);
		}
	}
	/**
	 * 获得需跟进的派送例外的订单数量
	 * @param st
	 * @param en
	 * @param delivery_exception_day
	 * @return
	 * @throws Exception
	 */
	public int getDeliveryExceptionCount(String st,String en,int delivery_exception_day)
		throws Exception
	{
		try 
		{
			String sql = "select count(oid) as delivery_exception_count from "+ConfigBean.getStringValue("porder")+" where date_add(trace_date,interval "+delivery_exception_day+" day) <= current_date and internal_tracking_status = "+WaybillInternalTrackingKey.AddressError+" and post_date >= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')";
			
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			
			return row.get("delivery_exception_count",0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorderMgr getDeliveryExceptionCount error:"+e);
		}
	}
	
	/**
	 * 需跟进派送例外订单
	 * @param st
	 * @param en
	 * @param delivery_exception_day
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryExceptionOrders(String st,String en,int delivery_exception_day,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("porder")+" where date_add(trace_date,interval "+delivery_exception_day+" day) <= current_date and internal_tracking_status = "+WaybillInternalTrackingKey.AddressError+" and post_date >= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') order by oid asc ";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderMgr getDeliveryExceptionOrders error:"+e);
		}
	}
	/**
	 * 获得正常待抄单数量
	 * @return
	 * @throws Exception
	 */
	public int getWaitRecordOrderCount(String input_st_date,String input_en_date)
		throws Exception
	{
		try 
		{
			String sql = "select count(oid) as wait_count from "+ConfigBean.getStringValue("porder")+" where handle = "+HandleKey.WAIT4_RECORD+" and handle_status = "+HandStatusleKey.NORMAL+" and post_date >= str_to_date('"+input_st_date+"','%Y-%m-%d') and post_date <= str_to_date('"+input_en_date+" 23:59:59','%Y-%m-%d %H:%i:%s')";
			
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			return result.get("wait_count",0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderMgr waitRecordOrderCount error:"+e);
		}
	}
	
	public int getOutboundingCount(String st,String en)
		throws Exception
	{
		try 
		{
			String sql = "select count(*) as outbound_count from "+ConfigBean.getStringValue("porder")+" where handle = "+HandleKey.OUTBOUNDING+" and handle_status = "+HandStatusleKey.NORMAL+" and post_date>= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')";
			
			
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			
			
			return result.get("outbound_count",0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorOrderMgr getOutboundingCount ");
		}
	}
	
	/**
	 * 根据订单ID获得订单的派送例外跟进日志
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryExceptionTraceNoteByOid(long oid)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("porder_note")+" where oid = ? and trace_child_type = ?";
			
			DBRow para = new DBRow();
			para.add("oid",oid);
			para.add("trace_child_type",WaybillInternalTrackingKey.AddressError);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorOrderMgr getDeliveryExceptionTraceNoteByOid error:"+e);
		}
	}
	
	/**
	 * 基于客户和仓库计算sku的调整数量总和
	 * @author Liang Jie
	 * @param companyID
	 * @param customerID
	 * @return
	 * @throws SQLException
	 * 加入 title by wangcr 2015/06/19
	 */
	public DBRow[] getBookedSkuSumCount(String companyID, String customerID) throws SQLException{
		String bookedSql = "SELECT items.item_id ItemID, material_number LotNo, items.title supplierID, SUM(items.b2b_count) BookedQty FROM b2b_order orders "
				+"INNER JOIN b2b_order_item items ON orders.b2b_oid=items.b2b_oid AND orders.b2b_order_status >='2' "
				+"AND orders.b2b_order_status <='7' AND orders.company_id = '"+companyID+"' "
				+"INNER JOIN customer_brand customer ON orders.customer_id=customer.cb_id "
				+"AND orders.customer_id= '"+customerID+"' "
				+"GROUP BY items.item_id,material_number";
		return dbUtilAutoTran.selectMutliple(bookedSql);
	}
	
	/**
	 * 从给定订单列表中根据DN查找已生成BOL的订单
	 * @author Liang Jie
	 * @param companyID
	 * @param customerID
	 * @param orders
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getBookedOrderId(String companyID, String customerID, List<String> dnList) throws SQLException{
		String sql="SELECT b2b_oid FROM b2b_order orders,customer_id customer "
				+"WHERE orders.customer_id=customer.customer_key AND customer.customer_id='"+customerID+"' "
				+"AND company_id='"+companyID+"' AND b2b_order_status>='2' AND b2b_order_status<='7' ";	
		if(dnList!=null && dnList.size()>0){
			sql = sql + "AND orders.customer_dn IN (";
			for(String dn:dnList){
				sql = sql + "'" +dn +"',";
			}
			sql = sql.substring(0, sql.lastIndexOf(',')) + ") ";
		}		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 在给定的订单范围内，获得按优先级排序过的的订单列表
	 * @author Liang Jie
	 * @param companyID
	 * @param customerID
	 * @param orders
	 * @param FreightTerm
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getOrdersListWithPriority(String companyID, String customerID, List<String> orders) throws SQLException{
		String orderNoSql = "";
		String calc_requested_date = "";
		if(orders!=null && orders.size()>0){
			orderNoSql = orderNoSql + "AND orders.customer_dn IN (";
			for(String orderNo:orders){
				orderNoSql = orderNoSql + "'" +orderNo +"',";
			}
			orderNoSql = orderNoSql.substring(0, orderNoSql.lastIndexOf(',')) + ") ";
		}
		//对于Vizio使用mabd-transitTime的时间；其他直接使用order的requestedDate
		if(customerID.equals("4")){
			calc_requested_date = "calc_requested_date";
		}else{
			calc_requested_date = "requested_date";
		}
		String sql = "SELECT customer.cb_id customer_id, company_id,b2b_oid,date_format(mabd,'%m/%d/%Y') mabd,freight_term,b2b_order_status,load_no,customer_dn,"
				+"account_id,receive_psid,transit_time,retailer_rank,date_format("+calc_requested_date+",'%m/%d/%Y') calc_requested_date,requested_date,b2b_order_number,order_type "
				+"FROM b2b_order orders, customer_brand customer "
				+"WHERE  orders.customer_id=customer.cb_id AND customer.cb_id='"+customerID+"' "
				+"AND (b2b_order_status = '1' or b2b_order_status = '10') AND company_id='"+companyID+"' "
				+orderNoSql
				+"ORDER BY retailer_rank, "+calc_requested_date+ ",transit_time desc ";
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 基于仓库和订单获得订单明细
	 * @author Liang Jie
	 * @param order
	 * @param companyId
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getOrderLinesList(DBRow order, String companyId) throws SQLException{
		/*String sql = "SELECT b2b_oid,b2b_detail_id,lot_number, item.b2b_p_name p_name,b2b_count,item.title,catalog.title ship_to_party "
				+"FROM b2b_order_item item where b2b_oid='"+order.getString("b2b_oid")+"' "
				+"left join product_storage_catalog catalog on catalog.id='"+order.getString("receive_psid")+"' ";
		*/
		String sql = "SELECT item.b2b_oid,m.b2b_order_number,m.tag,b2b_detail_id, lot_number, item.b2b_p_name as p_name, b2b_count, item.title, m.ship_to_party_name as ship_to_party, item.retail_po FROM b2b_order_item item join b2b_order m on item. b2b_oid = m.b2b_oid and item.b2b_oid='"+order.getString("b2b_oid")+"'";
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 获取需要同步(Imported，Commited，Open，) wms orderNo， 
	 * @author wangcr
	 * @param companyId
	 * @param customerID
	 * @return
	 * @throws SQLException
	 */
	public List<String> getWMSOrderNOsToCommited(String companyId, String customerID) throws SQLException {
		DBRow para = new DBRow();
		List<String> rt = new ArrayList<String>();
		para.add("companyId", companyId);
		para.add("customerID", customerID);
		String sql = "SELECT b2b_order_number from b2b_order m where company_id =? and customer_id=? and m.b2b_order_status in (1,2,10) and b2b_order_number is not null";
		DBRow[] rows = dbUtilAutoTran.selectPreMutliple(sql, para);
		for (DBRow row: rows) {
			rt.add(row.get("b2b_order_number",""));
		}
		return rt;
	}
	
	/**
	 * 基于仓库、客户和DN列表获得DO列表
	 * @author Liang Jie
	 * @param companyID
	 * @param customerID
	 * @param dnList
	 * @return
	 * @throws SQLException
	 */
	/*
	public DBRow[] getOrdersListBySelection(String companyID, String customerID, List<String> dnList) throws SQLException{
		String sql="SELECT orders.customer_id,company_id,b2b_oid,mabd,freight_term,"
				+"b2b_order_status,load_no,customer_dn,send_psid,receive_psid,account_id,  " 
				+"FROM b2b_order orders, customer_brand customer "
				+"WHERE orders.customer_id=customer.cb_id AND customer.cb_id="+customerID+" AND company_id='"+companyID+"' ";	
		if(dnList!=null && dnList.size()>0){
			sql = sql + "AND orders.customer_dn IN (";
			for(String dn:dnList){
				sql = sql + "'" +dn +"',";
			}
			sql = sql.substring(0, sql.lastIndexOf(',')) + ") ";
		}		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	*/
	
	public DBRow[] getOrdersListBySelection(String companyID, String customerID, List<String> dnList) throws SQLException{
		String sql="select orders.deliver_city, cp.p_code as deliver_state, orders.customer_id, orders.company_id, orders.b2b_oid, orders.mabd,"
				+ "orders.freight_term, orders.b2b_order_status, orders.load_no, orders.customer_dn, orders.send_psid,orders.receive_psid,orders.account_id "
				+ "from b2b_order orders LEFT JOIN country_province cp on cp.nation_id = orders.deliver_ccid and cp.pro_id = orders.deliver_pro_id where orders.company_id='"+companyID+"'";	
		if(dnList!=null && dnList.size()>0){
			sql = sql + "AND orders.customer_dn IN (";
			for(String dn:dnList){
				sql = sql + "'" +dn +"',";
			}
			sql = sql.substring(0, sql.lastIndexOf(',')) + ") ";
		} else {
			sql = sql + "AND orders.customer_dn IN ('')";
		}
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/*
	 * 
	 */
	
	/**
	 * 获得order的发货城市和收货城市
	 * @author Liang Jie
	 * @param orderId
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getCityNameByOrder(String orderId) throws SQLException{
		String sql="select catalog.send_city origin,'' dest from b2b_order orders,product_storage_catalog catalog "
				+"where orders.send_psid=catalog.id and b2b_oid='"+orderId+"' "
				+"union "
				+"select '',catalog.deliver_city from b2b_order orders,product_storage_catalog catalog "
				+"where orders.receive_psid=catalog.id and b2b_oid='"+orderId+"' ";
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 获取仓库所在城市名和州编码
	 * 
	 * @param psid
	 * @return
	 * @throws SQLException
	 */
	public DBRow getStorageCityAndState(long psid) throws SQLException{
		return dbUtilAutoTran.selectSingle("select s.send_city, UPPER(IFNULL(cp.p_code,s.pro_input)) as state from product_storage_catalog s left join country_province cp on cp.nation_id = s.native and s.pro_id = cp.pro_id where s.id="+psid);
	}
	
	/**
	 * 基于零售商和发货/收货城市查询order的货运时间
	 * @author Liang Jie
	 * @param shipToId
	 * @param originCity
	 * @param destCity
	 * @return
	 * @throws SQLException
	 */
	public DBRow getTransitTime(String shipToId, String originCity, String originState, String destCity, String destState) throws SQLException{
		String sql = "select transit_time from transit_times where ship_to_id=? and origin_city=? and origin_state=? and dest_city=? and dest_state=?";
		DBRow para = new DBRow();
		para.add("shipToId", shipToId);
		para.add("originCity", originCity);
		para.add("originState", originState);
		para.add("destCity", destCity);
		para.add("destState", destState);
		return dbUtilAutoTran.selectPreSingle(sql, para);
		
	}
	public DBRow getTransitTime(String shipToId, String originCity, String destCity) throws SQLException{
		String sql = "select transit_time from transit_times where ship_to_id='"+shipToId+"' "
				+"and origin_city='"+originCity+"' and dest_city='"+destCity+"' ";
		return dbUtilAutoTran.selectSingle(sql);
	}

	/**
	 * 获得零售商的优先级
	 * @author Liang Jie
	 * @param freightTerm
	 * @param retailerName
	 * @return
	 * @throws SQLException
	 */
	public DBRow getRetailerRank(String freightTerm, String shipToId) throws SQLException{
		String sql = "select rank from retailer_ranks where ship_to_id='"+shipToId+"' ";
		if(!shipToId.equals("-1")){
			sql = sql + "and freight_term='"+freightTerm+"' ";
		}		
		return dbUtilAutoTran.selectSingle(sql);
	}
	/**
	 * 更新B2B订单
	 * @author Liang Jie
	 * @param b2b_oid
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public int modB2bOrder(String b2b_oid,DBRow row)
		throws Exception
	{
		try
		{
			
			return dbUtilAutoTran.update("where b2b_oid='" + b2b_oid+"'",ConfigBean.getStringValue("b2b_order"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modB2bOrder(row) error:" + e);
		}
	}
	
	/**
	 * 获取映射到WMS中的CompanyID
	 * @author Liang Jie
	 * @param delieveryPlant
	 * @param customerId
	 * @return
	 * @throws SQLException
	 */
	public DBRow getWmsCompanyID(String delieveryPlant, String customerId) throws SQLException{
		String sql = "select company_id,ps_id from wms_company_dic where delivery_plant='" + delieveryPlant+"' and customer_id='" + customerId+"'";
		return dbUtilAutoTran.selectSingle(sql);
	}
	
	/**
	 * 获取映射到WMS中的CustomerID
	 * @author Liang Jie
	 * @param cb_id
	 * @return
	 * @throws SQLException
	 */
	public DBRow getCustomerBrandNumber(String cb_id) throws SQLException{
		String sql = "select brand_number from customer_brand where cb_id='" + cb_id+"'";
		return dbUtilAutoTran.selectSingle(sql);
	}
	
	/**
	 * 批量更新订单状态，用于同步wms数据
	 * 
	 * @author wangcr
	 * @param sqls
	 * @throws Exception
	 */
	public void updateOrderStatus(List<String> sqls) throws Exception {
		dbUtilAutoTran.batchUpdate(sqls.toArray(new String[0]));
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	/**
	 * 到各州运输时间
	 * 
	 * @param originState
	 * @param originCity
	 * @return
	 * @throws Exception
	 */
	public Map<String, Integer> getStateTransTime(String originState, String originCity) throws Exception {
		Map<String, Integer> rt = new HashMap<String,Integer>();
		DBRow params = new DBRow();
		params.add("originCity", originCity.toUpperCase());
		params.add("originState", originState.replaceAll(" ", ""));
		DBRow[] data =  dbUtilAutoTran.selectPreMutliple("select UPPER(dest_state) as dest_state, max(t.transit_time) as transit_time from transit_times t where UPPER(t.origin_city) = ? and t.origin_state = ? group by UPPER(dest_state)",params);
		for (DBRow row :data) {
			rt.put(row.get("dest_state", "").replaceAll(" ", ""), row.get("transit_time", 0));
		}
		return rt;
	}
}
