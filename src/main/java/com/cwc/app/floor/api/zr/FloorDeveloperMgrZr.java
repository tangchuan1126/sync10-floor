package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorDeveloperMgrZr {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	public long addDeveloper(DBRow row) throws Exception {
		try{
			 String tableName =  ConfigBean.getStringValue("developer");
			 return dbUtilAutoTran.insertReturnId(tableName, row);
		}catch(Exception e){
			throw new Exception("FloorDeveloperMgrZr.addDeveloper():"+e);
		}
	}
	public DBRow[] getAllDeveloper(PageCtrl pc) throws Exception {
		try{
			 String tableName =  ConfigBean.getStringValue("developer");
			 return dbUtilAutoTran.selectMutliple("select * from " + tableName, pc);
		}catch(Exception e){
			throw new Exception("FloorDeveloperMgrZr.getAllDeveloper(billId):"+e);
		}
	}
}
