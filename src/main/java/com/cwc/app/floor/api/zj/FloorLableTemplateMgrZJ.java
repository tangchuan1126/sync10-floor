package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorLableTemplateMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加标签模板
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addLableTemplate(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long lable_template_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("lable_template"));
			
			dbrow.add("lable_template_id",lable_template_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("lable_template"),dbrow);
			
			return (lable_template_id);
		}
		catch (Exception e)
		{
			throw new Exception("FloorLableTemplateMgrZJ addLableTemplate error:"+e);
		}
	}
	
	/**
	 * 修改标签模板
	 * @param pring_lable_id
	 * @param para
	 * @throws Exception
	 */
	public void modLableTemplate(long lable_template_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where lable_template_id = "+lable_template_id,ConfigBean.getStringValue("lable_template"),para);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorLableTemplateMgrZJ modLableTemplate error:"+e);
		}
	}
	
	/**
	 * 删除标签模板
	 * @param print_lable_id
	 * @throws Exception
	 */
	public void delLableTemplate(long lable_template_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where lable_template_id = "+lable_template_id,ConfigBean.getStringValue("lable_template"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLableTemplateMgrZJ delLableTemplate error:"+e);
		}
	}
	
	/**
	 * 获得全部商品使用标签呢模板
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllLableTemplate(PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("lable_template")+" where lable_type = 1 or lable_type =4 or lable_type =5 or lable_type = 6 order by lable_type,(print_range_width*print_range_height)";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch (Exception e)
		{
			throw new Exception("FloorLableTemplateMgrZJ getAllLableTemplate error:"+e);
		}
	}
	
	/**
	 * 获得商品或序列号标签
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllProductTemplate(PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("lable_template")+" where lable_type = 1 or lable_type = 5 or lable_type = 6 order by lable_type,(print_range_width*print_range_height)";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLableTemplateMgrZJ getAllProductTemplate error:"+e);
		}
	}
	
	/**
	 * 获得全部模板
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllTemplateLabel(PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("lable_template")+" order by lable_type,(print_range_width*print_range_height)";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLableTemplateMgrZJ getAllTemplateLabel error:"+e);
		}
	}
	
	
	/**
	 * 根据ID获得标签模板信息
	 * @param lable_template_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailLableTemplateById(long lable_template_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("lable_template")+" where lable_template_id = "+lable_template_id;
			
			return dbUtilAutoTran.selectSingle(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorLableTemplateMgrZJ getLableTemplateById error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
