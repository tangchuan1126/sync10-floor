package com.cwc.app.floor.api.zj;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.cwc.app.key.GateCheckLoadingTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.WmsOrderStatusKey;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTranSQLServer;

public class FloorSQLServerMgrZJ 
{
	private DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer;
	public DBRow[] getLoadingSQLServer(String search_number,String ps_name,int mumber_type) 
		throws Exception 
	{
		String sql = "";//"select UPPER(CarrierID) as carrier,AppointmentDate as time,UPPER(StagingAreaID) as area_name,UPPER(SupplierID) as title_name from dbo.Ex_VW_InboundAndOutbound_Schedule where Status = 'Open' and Facility = '"+ps_name+"'";
		
		if(mumber_type==GateCheckLoadingTypeKey.LOAD)
		{
			sql +="select TOP 1 UPPER(CarrierID) as carrier,AppointmentDate as time,UPPER(StagingAreaID) as area_name from dbo.Ex_VW_InboundAndOutbound_Schedule where (Status = 'Open' or Status = 'Imported' or Status = 'Picking' or Status = 'Picked') and Facility = '"+ps_name+"' and type = 'Outbount' ";
			sql +=" and replace(LoadNo,' ', '') = '"+search_number+"'";
			sql +=" GROUP BY StagingAreaID,CarrierID,AppointmentDate ORDER BY COUNT(StagingAreaID) DESC";
		}
		else if (mumber_type==GateCheckLoadingTypeKey.CTNR)
		{
			sql +="select UPPER(CarrierID) as carrier,AppointmentDate as time,UPPER(SupplierID) as title_name from dbo.Ex_VW_InboundAndOutbound_Schedule where (Status = 'Open' or Status = 'Imported') and type = 'Inbound' and Facility = '"+ps_name+"'";			
			sql +=" and ContainerNo = '"+search_number+"'";
		}
		else if(mumber_type==GateCheckLoadingTypeKey.BOL)
		{
			sql +="select UPPER(CarrierID) as carrier,AppointmentDate as time,UPPER(SupplierID) as title_name from dbo.Ex_VW_InboundAndOutbound_Schedule where (Status = 'Open' or Status = 'Imported') and type = 'Inbound' and Facility = '"+ps_name+"'";
			sql +=" and BOLNo = '"+search_number+"'";
	    }
		
		return dbUtilAutoTranSQLServer.selectMutliple(sql);
	}
	
	public DBRow[] getLoadingAll()
	throws Exception
	{
		String sql = "select UPPER(CarrierID) as carrier,AppointmentDate as time,UPPER(StagingAreaID) as area_name,UPPER(SupplierID) as title_name from dbo.Ex_VW_InboundAndOutbound_Schedule where Status = 'Open' ";
		
		return dbUtilAutoTranSQLServer.selectMutliple(sql);
	}
	
	/**
	 * 通过LoadNo查询MasterBols
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolsByLoadNo(String loadNo,String CompanyID,String CustomerID, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select top 1 mbol.*, odp.SupplierID from MasterBOLs  mbol");
			sql.append(" LEFT JOIN MasterBOLLines mboll on mboll.MasterBOLNo = mbol.MasterBOLNo and mboll.CompanyID = mbol.CompanyID");
			sql.append(" LEFT JOIN OrderLinePlates odp on odp.OrderNo = mboll.OrderNo and odp.CompanyID = mboll.CompanyID");
			sql.append(" where replace(mbol.LoadNo,' ', '') = '"+loadNo.replace(" ", "")+"'");
			if(!StrUtil.isBlank(CompanyID))
			{
				sql.append(" AND mbol.CompanyID = '").append(CompanyID).append("'");
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql.append(" AND mbol.CustomerID = '").append(CustomerID).append("'");
			}
			sql.append(sqlStatusMbol(status));
			if(!StrUtil.isBlank(loadNo))
			{
				return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
			}
			return new DBRow[0];
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolsByLoadNo error:"+e);
		}
	}
	
	public String sqlStatus(String[] status)
	{
		String sql = "";
		if(null != status && status.length > 0)
		{
			sql += " AND (";
			sql += " Status = '"+status[0]+"'";
			for (int i = 1; i < status.length; i++) 
			{
				sql += " OR Status = '"+status[i]+"'";
			}
			sql += " ) ";
		}
		return sql;
	}
	public String sqlStatusMbol(String[] status)
	{
		String sql = "";
		if(null != status && status.length > 0)
		{
			sql += " AND (";
			sql += " mbol.Status = '"+status[0]+"'";
			for (int i = 1; i < status.length; i++) 
			{
				sql += " OR mbol.Status = '"+status[i]+"'";
			}
			sql += " ) ";
		}
		return sql;
	}
	public String sqlStatusBol(String[] status)
	{
		String sql = "";
		if(null != status && status.length > 0)
		{
			sql += " AND (";
			sql += " bol.Status = '"+status[0]+"'";
			for (int i = 1; i < status.length; i++) 
			{
				sql += " OR bol.Status = '"+status[i]+"'";
			}
			sql += " ) ";
		}
		return sql;
	}
	public String sqlStatusOrder(String[] status)
	{
		String sql = "";
		if(null != status && status.length > 0)
		{
			sql += " AND (";
			sql += " od.Status = '"+status[0]+"'";
			for (int i = 1; i < status.length; i++) 
			{
				sql += " OR od.Status = '"+status[i]+"'";
			}
			sql += " ) ";
		}
		return sql;
	}
	public String sqlStatusRe(String[] status)
	{
		String sql = "";
		if(null != status && status.length > 0)
		{
			sql += " AND (";
			sql += " re.Status = '"+status[0]+"'";
			for (int i = 1; i < status.length; i++) 
			{
				sql += " OR re.Status = '"+status[i]+"'";
			}
			sql += " ) ";
		}
		return sql;
	}
	
	public DBRow[] findLoadCustomerAccountIdsByLoadNoCompanyIds(String loadNo, String[] companyIds, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT LoadNo,CompanyID,");
			sql.append(" max(bol.AppointmentDate) AppointmentDate,max(bol.Status)Status");
			sql.append(" STUFF(");
			sql.append(" ( SELECT ',' + CustomerID");
			sql.append(" FROM MasterBOLs m");
			sql.append(" WHERE replace(LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			sql.append(" and CustomerID = m.CustomerID ");
			sql.append(" FOR XML PATH ('') ), 1, 1,'') AS CustomerIDs,");
			sql.append(" STUFF(");
			sql.append(" ( SELECT ',' + AccountID");
			sql.append(" FROM MasterBOLs m");
			sql.append(" WHERE replace(LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			sql.append(" and AccountID = m.AccountID ");
			sql.append(" FOR XML PATH ('') ), 1, 1,'') AS AccountIDs,");
			sql.append(" FROM MasterBOLs m");
			sql.append(" WHERE replace(LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlCompanyId(companyIds));
			sql.append(sqlStatusBol(status));
			sql.append(" GROUP BY LoadNo, CompanyID");//, bol.CustomerID, bol.AccountID
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadAllInfosByLoadNoCompanyIds error:"+e);
		}
	}
	
	
	/**
	 * 通过LoadNo查询Order主子表相关数据
	 * @param masterBOLNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findB2BOrderItemPlatesWmsByMasterBolNo(long masterBOLNo, String companyId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select mbol.MasterBOLNo, boll.OrderNo, odl.Pallets, odl.ItemID, od.StagingAreaID, odlp.PlateNo, od.PickingType, od.CustomerID, od.CompanyID, od.PalletTypeID , od.PONo");
			sql.append(",CASE WHEN(odlp.PlateNo is null or odlp.PlateNo = '') then odl.OrderedQty else odlp.ShippedQty end as ShippedQty");
			//, odlp.ShippedQty
			sql.append(" from MasterBOLs mbol ");
			sql.append(" JOIN MasterBOLLines boll ON mbol.MasterBOLNo = boll.MasterBOLNo AND mbol.CompanyID = boll.CompanyID ");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND mbol.CompanyID = od.CompanyID ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND mbol.CompanyID = odl.CompanyID ");
			sql.append(" LEFT JOIN OrderLinePlates odlp ON odl.[LineNo]=odlp.[LineNo] AND odl.OrderNo = odlp.OrderNo AND mbol.CompanyID = odlp.CompanyID ");
			sql.append(" WHERE mbol.MasterBOLNo = ").append(masterBOLNo);
			sql.append(" AND mbol.CompanyID = '").append(companyId).append("'");
			sql.append(sqlStatusMbol(status));
			sql.append(" ORDER BY boll.OrderNo, odl.ItemID, odlp.PlateNo");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findB2BOrderItemPlatesWmsByMasterBolNo error:"+e);
		}
	}
	/**
	 * 通过PlateNo，companyID查询Movements
	 * @param plateNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow findLocationWmsByPlateNo(int plateNo, String companyId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT TOP 1 * FROM Movements WHERE PlateNo = ").append(plateNo);
			sql.append(" AND CompanyID = '").append(companyId).append("' ORDER BY DateCreated DESC ;");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLocationWmsByPlateNo error:"+e);
		}
	}
	
	/**
	 * 通过itemID，companyID，customerID查询CustomerItems
	 * @param ItemID
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findItemInfoByItemId(String ItemID, String CompanyID, String CustomerID) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM CustomerItems WHERE 1=1");
			if(!StrUtil.isBlank(ItemID))
			{
				sql.append(" AND ItemID = '").append(ItemID).append("'");
			}
			if(!StrUtil.isBlank(CompanyID))
			{
				sql.append(" AND CompanyID = '").append(CompanyID).append("'");
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql.append(" AND CustomerID = '").append(CustomerID).append("'");
			}
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findItemInfoByItemId error:"+e);
		}
	}
	
	/**
	 * 通过ContainerNo查询SupplierID,CarrierID,AppointmentDate
	 * @param containerNo
	 * @return
	 * @throws Exception
	 */
	public DBRow findReceiptByContainerNo(String containerNo, String[] status, String[] companyID) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select top 1 SupplierID,CarrierID,AppointmentDate,CompanyID, CustomerID,");
			sql.append(ModuleKey.CHECK_IN_CTN).append(" as order_type,ReferenceNo,PONo,BOLNo,ContainerNo,Status,");
			sql.append(" (select count(*) countDate from (select count(*) countGroupDate from Receipts where rtrim(ltrim(ContainerNo)) = '").append(containerNo).append("'")
			.append(sqlStatus(status))
			.append(" group by AppointmentDate) a) countDate");
			sql.append(" from Receipts where rtrim(ltrim(ContainerNo)) = '").append(containerNo).append("'")
			.append(sqlStatus(status));
			sql.append(sqlCompanyId(companyID));
//			System.out.println("de1:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptByContainerNo error:"+e);
		}
	}
	
	/**
	 * 通过BOL查询SupplierID,CarrierID,AppointmentDate
	 * @param BolNo
	 * @return
	 * @throws Exception
	 */
	public DBRow findReceiptByBolNo(String BolNo, String[] status, String[] companyID) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select top 1 SupplierID,CarrierID,AppointmentDate,CompanyID, CustomerID,");
			sql.append(ModuleKey.CHECK_IN_BOL).append(" as order_type,ReferenceNo,PONo,BOLNo,ContainerNo,Status,");
			sql.append(" (select count(*) countDate from (select count(*) countGroupDate from Receipts where rtrim(ltrim(BOLNo)) = '").append(BolNo).append("' and Status != '").append(WmsOrderStatusKey.CLOSED).append("' and Status != '").append(WmsOrderStatusKey.RECEIVED).append("' group by AppointmentDate) a) countDate");
			sql.append(" from Receipts where rtrim(ltrim(BOLNo)) = '").append(BolNo).append("'")
			.append(sqlStatus(status));
			sql.append(sqlCompanyId(companyID));
//			System.out.println("de:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptByBolNo error:"+e);
		}
	}
	
	/**
	 * 通过loadNo查询SupplierID,CarrierID,AppointmentDate
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public DBRow findOrderInfoByLoadNo(String loadNo, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select top 1 * from (");
			sql.append(" SELECT od.OrderNo, SUM(odl.Pallets) Pallets, MAX(od.StagingAreaID)StagingAreaID, MAX(od.CarrierID)CarrierID, MAX(od.AppointmentDate)AppointmentDate");
			sql.append(" from Orders od ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID");
			sql.append(" where replace(od.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			sql.append(sqlStatusOrder(status));
			sql.append("  GROUP BY od.OrderNo) a ORDER BY a.Pallets desc");
//			System.out.println("se:"+sql);
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderInfoByLoadNo error:"+e);
		}
	}
	
	/**
	 * 通过loadNo查询SupplierID,CarrierID,AppointmentDate
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public DBRow findMasterInfoByLoadNo(String loadNo, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select top 1 * from (");
			sql.append(" SELECT od.OrderNo, SUM(odl.Pallets) Pallets, MAX(od.StagingAreaID)StagingAreaID, MAX(od.CarrierID)CarrierID, MAX(od.AppointmentDate)AppointmentDate");
			sql.append(" FROM MasterBOLs mbol ");
			sql.append(" JOIN MasterBOLLines mboll ON mbol.MasterBOLNo = mboll.MasterBOLNo AND mbol.CompanyID = mboll.CompanyID");
			sql.append(" JOIN Orders od ON mboll.OrderNo = od.OrderNo AND mboll.CompanyID = od.CompanyID");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID");
			sql.append(" where replace(mbol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			sql.append(sqlStatusMbol(status));
			sql.append(sqlStatusOrder(status));
			sql.append("  GROUP BY od.OrderNo) a ORDER BY a.Pallets desc");

			//System.out.println("sq:"+sql);
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterInfoByLoadNo error:"+e);
		}
	}

	
	public DBRow findOrdersByLoadNo(String loadNo, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select boll.OrderNo, odl.Pallets, odl.ItemID, od.StagingAreaID, od.PickingType, od.CustomerID ");
			sql.append(" from MasterBOLs bol ");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND boll.CompanyID = od.CompanyID ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND od.CompanyID = odl.CompanyID ");
			sql.append(" where replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND bol.CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND bol.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatusBol(status));
			sql.append(sqlStatusOrder(status));
			sql.append(" ORDER BY boll.OrderNo");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersByLoadNo error:"+e);
		}
	}
	
	public DBRow findOrdersPlatesByOrderNoLineNoCompany(String CompanyID, int OrderNo, int LineNo) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select  odlp.PlateNo, odlp.ShippedQty");
			sql.append(" from OrderLinePlates odlp ");
			sql.append(" where odlp.OrderNo = ").append(OrderNo);
			sql.append(" AND odlp.[LineNo] = ").append(LineNo);
			sql.append(" AND odlp.CompanyID = '").append(CompanyID).append("'");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersPlatesByOrderNoLineNoCompany error:"+e);
		}
	}
	
	public DBRow findPalletsByOrderNoCompanyId(String CompanyID, long OrderNo) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select sum(Pallets) as palletNum, CAST(CEILING(sum(Pallets)) as int)palletNumInt from OrderLines ");
			sql.append(" where OrderNo = ").append(OrderNo);
			sql.append(" AND CompanyID = '").append(CompanyID).append("'");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findPalletsByOrderNoCompanyId error:"+e);
		}
	}
	
	/**
	 * 可能获取多条地址信息，还应该加什么查询条件
	 * 方法是有问题的，查出来的数据不对，需要再寻求
	 * @param CustomerID
	 * @param AccountID
	 * @return
	 * @throws Exception
	 */
	public DBRow findShipToInfoByCustomerAndAccountAddress(String CustomerID, String AccountID, String address) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select top 1 * from CustomerAccountAddresss where 1=1");
			sql.append(" and CustomerID = '").append(CustomerID).append("'");
			sql.append(" and AccountID = '").append(AccountID).append("'");
			sql.append(" and Address1 = '").append(address).append("'");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findShipToInfoByCustomerAndAccount error:"+e);
		}
	}
	
	public long findMaxOrderNoByCompanyId(String companyID) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select max(OrderNo) maxOrderNo from Orders where CompanyID = '").append(companyID).append("'");
			DBRow row = dbUtilAutoTranSQLServer.selectSingle(sql.toString());
			if(null == row)
			{
				return 0L;
			}
			else
			{
				return row.get("maxOrderNo", 0L);
			}
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMaxOrderNoByCompanyId error:"+e);
		}
	}
	
	
	public void updateMasterBol(DBRow row, String loadNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			String where = " WHERE replace(LoadNo,' ', '') = '"+loadNo.replace(" ", "")+"'";
			where += sqlStatus(status);
			if(!StrUtil.isBlank(companyId))
			{
				where += " and CompanyID = '"+companyId+"'";
			}
			if(!StrUtil.isBlank(customerId))
			{
				where += " and CustomerID = '"+customerId+"'";
			}
			dbUtilAutoTranSQLServer.update(where, "MasterBOLs", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.updateMasterBol error:"+e);
		}
	}
	
	public void updateMasterBol(DBRow row, long MasterBOLNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			String where = " WHERE MasterBOLNo = "+MasterBOLNo;
			where += sqlStatus(status);
			if(!StrUtil.isBlank(companyId))
			{
				where += " and CompanyID = '"+companyId+"'";
			}
			if(!StrUtil.isBlank(customerId))
			{
				where += " and CustomerID = '"+customerId+"'";
			}
			dbUtilAutoTranSQLServer.update(where, "MasterBOLs", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.updateMasterBol error:"+e);
		}
	}
	
	
	public void updateOrder(DBRow row, long orderNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			String where = " WHERE OrderNo = "+orderNo;
			where += sqlStatus(status);
			if(!StrUtil.isBlank(companyId))
			{
				where += " and CompanyID = '"+companyId+"'";
			}
			if(!StrUtil.isBlank(customerId))
			{
				where += " and CustomerID = '"+customerId+"'";
			}
			dbUtilAutoTranSQLServer.update(where, "Orders", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.updateOrder error:"+e);
		}
	}
	
	public DBRow[] findMasterBolLinesByLoadNo(String loadNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			// add ReferenceNo
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT od.ReferenceNo, boll.OrderNo, boll.CompanyID, od.AppointmentDate, bol.AppointmentDate AppointmentDateLoad FROM MasterBOLLines boll");
			sql.append(" JOIN MasterBOLs bol ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ");
			sql.append(" JOIN Orders od on od.OrderNo = boll.OrderNo and od.CompanyID = boll.CompanyID");
			sql.append(" WHERE replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND bol.CompanyID='"+companyId+"'");
			}
			if(!StrUtil.isBlank(customerId))
			{
//				sql.append(" AND bol.CustomerID='"+customerId+"'");
				sql.append(" AND bol.CustomerID in (").append(parseCustomerIDStrToInStr(customerId)).append(")");
			}
			sql.append(sqlStatusBol(status));
			sql.append(" ORDER BY OrderNo DESC");
			
			//System.out.println("s:"+sql.toString());
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolLinesByLoadNo error:"+e);
		}
	}
	
	public DBRow[] findOrdersNoAndCompanyByLoadNo(String loadNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			String sql = "SELECT OrderNo, CompanyID FROM Orders WHERE replace(LoadNo,' ', '') = '"+loadNo.replace(" ", "")+"'";
			if(!StrUtil.isBlank(companyId))
			{
				sql+=" AND CompanyID='"+companyId+"'";
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql+=" AND CustomerID='"+customerId+"'";
			}	
			sql += sqlStatus(status);
			sql+=" ORDER BY OrderNo DESC";
			//System.out.println("s2:"+sql);
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersNoAndCompanyByLoadNo error:"+e);
		}
	}
	
	public DBRow[] findMasterBolByLoadNo(String loadNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from MasterBOLs where replace(LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND CompanyID='"+companyId+"'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND CustomerID in ("+parseCustomerIDStrToInStr(customerId)+")");
			}	
			sql.append(sqlStatus(status));
//			System.out.println("sql2:"+sql.toString()+"------------"+customerId);
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolByLoadNo error:"+e);
		}
	}
	
	public DBRow findOneOfMasterBolsByLoadNo(String loadNo, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select top 1 * from MasterBOLs where replace(LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			sql.append(sqlStatus(status));
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOneOfMasterBolsByLoadNo error:"+e);
		}
	}
	
	/**
	 * 通过LoadNo查询一条MasterBol 某些数据
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow findMasterBolSomeInfoByLoadNo(String loadNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT mbol.CustomerID, whs.Address1, whs.Address2, whs.City, whs.State, whs.ZipCode, whs.Phone, whs.Contact, mbol.CompanyID,");
			sql.append(" mbol.ShipToName, mbol.ShipToAddress1, mbol.ShipToAddress2, mbol.ShipToCity, mbol.ShipToState, mbol.ShipToZipCode, mbol.ShipToID, mbol.ShipToPhone,");
			sql.append(" mbol.BillToName, mbol.BillToAddress1, mbol.BillToAddress2, mbol.BillToCity, mbol.BillToState, mbol.BillToZipCode, mbol.Note,");
			sql.append(" mbol.LoadNo, mbol.AppointmentDate, mbol.ProNo, mbol.FreightTerm");
			sql.append(" FROM  (SELECT TOP 1 * FROM MasterBOLs WHERE replace(LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND CompanyID='"+companyId+"'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND CustomerID='"+customerId+"'");
			}	
			sql.append(sqlStatus(status));
			sql.append(" ) mbol");
			sql.append(" LEFT JOIN Warehouses whs ON whs.CompanyID = mbol.CompanyID AND whs.WarehouseID = mbol.WarehouseID");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolSomeInfoByLoadNo error:"+e);
		}
	}
	
	
	public DBRow[] findLoadNoOrdersItemInfoByLoadNo(String loadNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT b.Pallets palletCount, b.orderLineCase, ");
			sql.append(" CAST(CEILING(b.CustomerPallets* b.PalletWeight+ b.PoundPerPackage* b.orderLineCase)  as int)weightItemAndPallet,");
			sql.append(" b.CommodityDescription,b.NMFC, b.FreightClass");
			sql.append(" FROM (SELECT  CASE ");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and (a.Unit = 'Package' or a.Unit = 'Pallet' or a.Unit = 'Piece'))");
			sql.append(" THEN  (case when (0 = a.OrderedQty%a.UnitsPerPackage) then (a.OrderedQty/a.UnitsPerPackage) else (a.OrderedQty/a.UnitsPerPackage+1) end )");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and a.QtyPerUnit is not null and a.QtyPerUnit != 0 and (a.Unit = 'Inner Case'))");
			sql.append(" THEN (case when (0 = a.OrderedQty%(a.UnitsPerPackage*a.QtyPerUnit)) then (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit))else (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)+1) end )");
			sql.append(" ELSE a.OrderedQty END AS orderLineCase,a.*");
			sql.append(" FROM (select odl.Pallets,odl.OrderedQty,odl.PoundPerPackage,odl.PalletWeight,ci.CommodityDescription, odl.NMFC, ");
			sql.append(" odl.FreightClass, boll.OrderNo, odl.ItemID, ci.Unit, ci.QtyPerUnit, ci.UnitsPerPackage, ISNULL(odl.CustomerPallets, 0)  CustomerPallets");
			sql.append(" FROM MasterBOLs bol ");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND bol.CompanyID = od.CompanyID ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND bol.CompanyID = odl.CompanyID ");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ");
			sql.append(" WHERE replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND bol.CompanyID='"+companyId+"'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND bol.CustomerID='"+customerId+"'");
			}	
			sql.append(sqlStatusBol(status));
			sql.append(sqlStatusOrder(status));
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID");
			sql.append(" ) a )b ORDER BY b.Pallets desc, b.orderLineCase desc;");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadNoOrdersItemInfoByLoadNo error:"+e);
		}
	}
	
	
	public DBRow[] findLoadNoOrdersPONoInfoByLoadNo(String loadNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT c.PONo, SUM(c.orderLineCase) orderLineCaseSum, SUM(c.weightItemAndPallet) weightItemAndPallet, c.ReferenceNo, MAX(c.StoreName)StoreName, MAX(c.ShipToName)ShipToName");
			sql.append(" FROM(SELECT b.PONo, b.orderLineCase, b.ReferenceNo, b.StoreName, b.ShipToName,");
			sql.append(" CAST(CEILING(b.CustomerPallets* b.PalletWeight+ b.PoundPerPackage* b.orderLineCase)  as int)weightItemAndPallet");
			sql.append(" FROM (select a.*, CASE ");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and (a.Unit = 'Package' or a.Unit = 'Pallet' or a.Unit = 'Piece'))");
			sql.append(" THEN  (case when (0 = a.OrderedQty%a.UnitsPerPackage) then (a.OrderedQty/a.UnitsPerPackage) else (a.OrderedQty/a.UnitsPerPackage+1) end )");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and a.QtyPerUnit is not null and a.QtyPerUnit != 0 and (a.Unit = 'Inner Case'))");
			sql.append(" THEN (case when (0 = a.OrderedQty%(a.UnitsPerPackage*a.QtyPerUnit)) then (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)) else (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)+1) end )");
			sql.append(" ELSE a.OrderedQty END AS orderLineCase");
			sql.append(" FROM ( select odl.Pallets,odl.OrderedQty,odl.PoundPerPackage, ci.Unit, ci.QtyPerUnit, ci.UnitsPerPackage,od.PONo");
			sql.append(" , od.ReferenceNo,odl.PalletWeight, ISNULL(odl.CustomerPallets, 0)  CustomerPallets, od.StoreName, od.ShipToName");
			sql.append(" FROM MasterBOLs bol ");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND bol.CompanyID = od.CompanyID ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND bol.CompanyID = odl.CompanyID ");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ");
			sql.append(" WHERE replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND bol.CompanyID='"+companyId+"'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND bol.CustomerID='"+customerId+"'");
			}	
			sql.append(sqlStatusBol(status));
			sql.append(sqlStatusOrder(status));
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID");
			sql.append(" ) a )b )c GROUP BY c.PONo, c.ReferenceNo ORDER BY c.PONo");
			//System.out.println("msql2:"+sql.toString());
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadNoOrdersPONoInfoByLoadNo error:"+e);
		}
	}
	
	public DBRow findOrderSomeInfoByOrderNo(long orderNo, String companyId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT od.CompanyID, od.CustomerID, od.DeptNo, od.RetailerOrderType, whs.Address1, whs.Address2, whs.City, whs.State, whs.ZipCode, whs.Phone,");
			sql.append(" od.ShipToName, od.ShipToAddress1, od.ShipToAddress2, od.ShipToCity, od.ShipToState, od.ShipToZipCode, od.ShipToID,od.ShipToPhone, od.AccountID");
//			sql.append(" od.BillToName, od.BillToAddress1, od.BillToAddress2, od.BillToCity, od.BillToState");
			sql.append("  ,CASE WHEN(od.FreightTerm = 'Third Party') THEN od.BillToName ELSE '' END as BillToName");
			sql.append("  ,CASE WHEN(od.FreightTerm = 'Third Party') THEN od.BillToAddress1 ELSE '' END as BillToAddress1");
			sql.append("  ,CASE WHEN(od.FreightTerm = 'Third Party') THEN od.BillToAddress2 ELSE '' END as BillToAddress2");
			sql.append(" , CASE WHEN(od.FreightTerm = 'Third Party') THEN od.BillToCity ELSE '' END as BillToCity");
			sql.append("  ,CASE WHEN(od.FreightTerm = 'Third Party') THEN od.BillToState ELSE '' END as BillToState");
			sql.append("  ,CASE WHEN(od.FreightTerm = 'Third Party') THEN od.BillToZipCode ELSE '' END as BillToZipCode");
			sql.append(" , od.BillToZipCode,od.BOLNote,od.AccountID");
			sql.append(" ,od.OrderNo, od.ReferenceNo, od.LoadNo, od.AppointmentDate, od.ProNo, v.external_id as ExternalID, od.FreightTerm,od.CarrierID, ca.ShipFromName");
			sql.append(" FROM  Orders od");
			sql.append(" LEFT JOIN Warehouses whs ON whs.CompanyID = od.CompanyID ");
			sql.append(" LEFT JOIN CustomerAccounts ca ON od.CompanyID = ca.CompanyID AND od.CustomerID = ca.CustomerID AND od.AccountID = ca.AccountID");
			sql.append(" left join vizio_extension v on v.vizio_dn = od.ReferenceNo ");
			sql.append(" WHERE od.OrderNo = ").append(orderNo).append(" AND od.CompanyID = '").append(companyId).append("'");
			sql.append(sqlStatusOrder(status));
			sql.append(" AND whs.WarehouseID = od.WarehouseID");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolSomeInfoByLoadNo error:"+e);
		}
	}
	
	
	/*public DBRow[] findOrderItemsInfoByOrderNo(long orderNo, String companyId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT b.Pallets palletCount, b.orderLineCase,b.CommodityDescription , b.OrderedQty,b.Unit");
//			sql.append(" CAST(CEILING(b.CustomerPallets* b.PalletWeight+ b.PoundPerPackage* b.orderLineCase)  as int)weightItemAndPallet,");
//			sql.append(" ,CAST((CEILING(b.palletCounts* b.PalletWeight)+ CEILING(b.PoundPerPackage* b.orderLineCase)) as int)weightItemAndPallet");
			sql.append(" ,CAST((CEILING(b.palletCounts* b.PalletWeight+ b.PoundPerPackage* b.orderLineCase)) as int)weightItemAndPallet");
			sql.append(" ,CASE WHEN(b.NMFC is null or b.NMFC='') THEN b.NMFC1 ELSE NMFC end as NMFC,CASE WHEN(b.FreightClass is null or b.FreightClass='') THEN b.FreightClass1 ELSE FreightClass end as FreightClass");
			sql.append(" FROM (select  CASE ");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and (a.Unit = 'Package' or a.Unit = 'Pallet' or a.Unit = 'Piece'))");
			sql.append(" THEN  (case when (0 = a.OrderedQty%a.UnitsPerPackage) then (a.OrderedQty/a.UnitsPerPackage) else (a.OrderedQty/a.UnitsPerPackage+1) end )");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and a.QtyPerUnit is not null and a.QtyPerUnit != 0 and (a.Unit = 'Inner Case'))");
			sql.append(" THEN (case when (0 = a.OrderedQty%(a.UnitsPerPackage*a.QtyPerUnit)) then (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit))else (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)+1) end )");
			sql.append(" ELSE a.OrderedQty END AS orderLineCase,a.*");
			sql.append(" FROM (");
			
			sql.append(" select f.*, g.ShippedQty OrderedQty from (");
			sql.append(" select sum(e.Pallets)Pallets,max(e.PoundPerPackage)PoundPerPackage,max(e.PalletWeight)PalletWeight,max(e.CommodityDescription)CommodityDescription");
			sql.append(" , max(e.NMFC)NMFC,max(e.FreightClass)FreightClass , sum(e.palletCounts)palletCounts, max(e.UnitsPerPackage)UnitsPerPackage, sum(e.CustomerPallets)CustomerPallets");
			sql.append(" , max(e.NMFC1)NMFC1,max(e.FreightClass1)FreightClass1, max(e.OrderNo)OrderNo, e.ItemID, max(e.Unit)Unit, max(e.QtyPerUnit)QtyPerUnit, max(e.CompanyID)CompanyID");
			
			sql.append("  from (select odl.Pallets,odl.OrderedQty,odl.PoundPerPackage,odl.PalletWeight,ci.CommodityDescription, odl.NMFC,odl.FreightClass , odl.CompanyID");
			sql.append(" , ci.NMFC NMFC1,ci.FreightClass FreightClass1, od.OrderNo, odl.ItemID, ci.Unit, ci.QtyPerUnit, ci.UnitsPerPackage, ISNULL(odl.CustomerPallets, 0)  CustomerPallets");
			sql.append(" ,CASE WHEN(od.CustomerID = 'VIZIO' OR od.CustomerID = 'VZO' OR od.CustomerID = 'VIZIO2' OR od.CustomerID = 'VIZIO3') THEN ISNULL(odl.CustomerPallets, odl.Pallets) ELSE odl.Pallets END AS palletCounts");
			sql.append(" FROM Orders od");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID ");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ");
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID");
			sql.append(" WHERE od.OrderNo = ").append(orderNo).append(" AND od.CompanyID = '").append(companyId).append("'");
			sql.append(sqlStatusOrder(status));
			
			
			sql.append(" ) e GROUP BY e.ItemID ) f");
			sql.append(" join ( select sum(odlp.ShippedQty)ShippedQty, max(odl.ItemID)ItemID, max(odl.CompanyID)CompanyID");
			
			
			sql.append(" FROM Orders od");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID ");
			sql.append("  JOIN OrderLinePlates odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo]");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ");
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID");
			sql.append(" WHERE od.OrderNo = ").append(orderNo).append(" AND od.CompanyID = '").append(companyId).append("'");
			sql.append(sqlStatusOrder(status));
			sql.append("  GROUP BY odl.ItemID) g on g.ItemID = f.ItemID and g.CompanyID = f.CompanyID");
			
			sql.append(" ) a )b ORDER BY b.Pallets desc, b.orderLineCase DESC");
			
			//System.out.println("bsql1:"+sql.toString());
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderItemsInfoByOrderNo error:"+e);
		}
	}	*/
	
	
	
	public DBRow[] findOrderItemsInfoByOrderNo(long orderNo, String companyId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT b.Pallets palletCount, b.orderLineCase,b.CommodityDescription , b.OrderedQty,b.Unit");
//			sql.append(" CAST(CEILING(b.CustomerPallets* b.PalletWeight+ b.PoundPerPackage* b.orderLineCase)  as int)weightItemAndPallet,");
//			sql.append(" ,CAST((CEILING(b.palletCounts* b.PalletWeight)+ CEILING(b.PoundPerPackage* b.orderLineCase)) as int)weightItemAndPallet");
			sql.append(" ,CAST((CEILING(b.palletCounts* b.PalletWeight+ b.PoundPerPackage* b.orderLineCase)) as int)weightItemAndPallet");
			sql.append(" ,CASE WHEN(b.NMFC is null or b.NMFC='') THEN b.NMFC1 ELSE NMFC end as NMFC,CASE WHEN(b.FreightClass is null or b.FreightClass='') THEN b.FreightClass1 ELSE FreightClass end as FreightClass");
			sql.append(" FROM (select  CASE ");
			sql.append(" WHEN (CombineType=1  and a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and (a.Unit = 'Package' or a.Unit = 'Pallet' or a.Unit = 'Piece'))");
			sql.append(" THEN  (case when (0 = a.OrderedQty%a.UnitsPerPackage) then (a.OrderedQty/a.UnitsPerPackage) else (a.OrderedQty/a.UnitsPerPackage+1) end )");
			sql.append(" WHEN (CombineType=1  and a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and a.QtyPerUnit is not null and a.QtyPerUnit != 0 and (a.Unit = 'Inner Case'))");
			sql.append(" THEN (case when (0 = a.OrderedQty%(a.UnitsPerPackage*a.QtyPerUnit)) then (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit))else (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)+1) end )");
			sql.append(" ELSE a.OrderedQty END AS orderLineCase,a.*");
			sql.append(" FROM (");
			
			sql.append(" select CombineType,f.*, g.ShippedQty OrderedQty from (");
			sql.append(" select sum(e.Pallets)Pallets,max(e.PoundPerPackage)PoundPerPackage,max(e.PalletWeight)PalletWeight,max(e.CommodityDescription)CommodityDescription");
			sql.append(" , max(e.NMFC)NMFC,max(e.FreightClass)FreightClass , sum(e.palletCounts)palletCounts, max(e.UnitsPerPackage)UnitsPerPackage, sum(e.CustomerPallets)CustomerPallets");
			sql.append(" , max(e.NMFC1)NMFC1,max(e.FreightClass1)FreightClass1, max(e.OrderNo)OrderNo, e.ItemID, max(e.Unit)Unit, max(e.QtyPerUnit)QtyPerUnit, max(e.CompanyID)CompanyID");
			sql.append("  from (select odl.Pallets,odl.OrderedQty,odl.PoundPerPackage,odl.PalletWeight,ci.CommodityDescription, odl.NMFC,odl.FreightClass , odl.CompanyID");
			sql.append(" , ci.NMFC NMFC1,ci.FreightClass FreightClass1, od.OrderNo, odl.ItemID, ci.Unit, ci.QtyPerUnit, ci.UnitsPerPackage, ISNULL(odl.CustomerPallets, 0)  CustomerPallets");
			sql.append(" ,CASE WHEN(od.CustomerID = 'VIZIO' OR od.CustomerID = 'VZO' OR od.CustomerID = 'VIZIO2' OR od.CustomerID = 'VIZIO3') THEN ISNULL(odl.CustomerPallets, odl.Pallets) ELSE odl.Pallets END AS palletCounts");
			sql.append(" FROM Orders od");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID ");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ");
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID");
			sql.append(" WHERE od.OrderNo = ").append(orderNo).append(" AND od.CompanyID = '").append(companyId).append("'");
			sql.append(sqlStatusOrder(status));
			
			
			sql.append(" ) e GROUP BY e.ItemID ) f");
			sql.append(" join ( select sum(odlp.ShippedQty)ShippedQty, max(odl.ItemID)ItemID, max(odl.CompanyID)CompanyID,1 as CombineType");
			
			
			sql.append(" FROM Orders od");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID ");
			sql.append("  JOIN OrderLinePlates odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo]");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ");
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID");
			sql.append(" WHERE   (od.CompanyID ! ='W17' or od.CustomerID!='Seidig0005' or od.AccountID!='Walmart-ETC') and od.OrderNo = ").append(orderNo).append(" AND od.CompanyID = '").append(companyId).append("'");
			sql.append(sqlStatusOrder(status));
			sql.append("  GROUP BY odl.ItemID");
			
			sql.append(" union all ");
			
			sql.append(" select  CASE WHEN(a.CartonQty>1) THEN a.CartonQty  ELSE b.ShippedQty END AS ShippedQty,  ");
			sql.append("b.ItemID, b.CompanyID  ,CASE WHEN(ISNULL(a.CartonQty,0)>1) THEN 2  ELSE 1 END AS CombineType ");
			sql.append(" from (");
			sql.append(" SELECT SUM(odlp.ShippedQty) as ShippedQty,odl.ItemID, odl.CompanyID FROM Orders od ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID ");
			sql.append(" JOIN OrderLinePlates odlp ON odlp.OrderNo = odl.OrderNo AND odlp.CompanyID = odl.CompanyID AND odlp.[LineNo] = odl.[LineNo] ");
			sql.append(" WHERE od.OrderNo = '"+orderNo+"'  AND od.CompanyID = 'W17' AND od.CustomerID='Seidig0005' AND od.AccountID='Walmart-ETC' ");
			sql.append(" GROUP BY odl.ItemID, odl.CompanyID  ");
			sql.append(" ) b");
			sql.append(" left join (SELECT count(oc.CartonNo) AS CartonQty ,odl.ItemID,odl.CompanyID FROM Orders od ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID ");
			sql.append(" JOIN OrderCartons oc ON oc.OrderNo = od.OrderNo AND oc.CompanyID = od.CompanyID ");
			sql.append(" WHERE od.OrderNo = '"+orderNo+"' AND od.CompanyID = 'W17' AND od.CustomerID='Seidig0005' AND od.AccountID='Walmart-ETC' ");
			sql.append(" GROUP BY odl.ItemID, odl.CompanyID HAVING COUNT(distinct  odl.ItemID) =1 ");
			sql.append(" union all ");
			sql.append(" SELECT count(oc.CartonNo) AS CartonQty ,odl.ItemID, odl.CompanyID FROM Orders od ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID ");
			sql.append(" inner JOIN OrderLineCartons oc ON oc.OrderNo = odl.OrderNo AND oc.CompanyID = odl.CompanyID  and oc.[LineNo] = odl.[LineNo] ");
			sql.append(" WHERE od.OrderNo = '"+orderNo+"' AND od.CompanyID = 'W17' AND od.CustomerID='Seidig0005' AND od.AccountID='Walmart-ETC' ");
			sql.append(" GROUP BY odl.ItemID,odl.CompanyID HAVING COUNT(distinct odl.ItemID) >1 ) a ");
			sql.append(" on a.ItemID=b.ItemID and a.CompanyID=b.CompanyID");
			/*sql.append(" inner join ( ");
			sql.append(" SELECT SUM(odlp.ShippedQty) as ShippedQty,odl.ItemID, odl.CompanyID FROM Orders od ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID ");
			sql.append(" JOIN OrderLinePlates odlp ON odlp.OrderNo = odl.OrderNo AND odlp.CompanyID = odl.CompanyID AND odlp.[LineNo] = odl.[LineNo] ");
			sql.append(" WHERE od.OrderNo = '"+orderNo+"'  AND od.CompanyID = 'W17' AND od.CustomerID='Seidig0005' AND od.AccountID='Walmart-ETC' ");
			sql.append(" GROUP BY odl.ItemID, odl.CompanyID  ");
			sql.append(" ) b on a.ItemID=b.ItemID and a.CompanyID=b.CompanyID ");*/
		
			
			/*sql.append("select  CASE WHEN(sp.CartonQty>1) THEN CartonQty  ELSE sp.ShippedQty END AS ShippedQty,sp.ItemID,sp.CompanyID ");
			sql.append("from ( ");
			sql.append("SELECT sum(odlp.ShippedQty) as ShippedQty  ,count(oc.CartonNo) as CartonQty , odl.ItemID, odl.CompanyID ");
			sql.append(" FROM Orders od ");
			sql.append("JOIN OrderLines odl ON odl.OrderNo = od.OrderNo  AND odl.CompanyID = od.CompanyID ");
			sql.append("JOIN OrderLinePlates odlp ON odlp.OrderNo = odl.OrderNo AND odlp.CompanyID = odl.CompanyID AND odlp.[LineNo] = odl.[LineNo] ");
			sql.append("left join OrderCartons oc on oc.OrderNo = od.OrderNo and oc.CompanyID = od.CompanyID ");
			sql.append("WHERE od.OrderNo = '"+orderNo+"'  AND od.CompanyID = 'W17' and od.CustomerID='Seidig0005' and od.AccountID='Walmart-ETC' ");
			sql.append(sqlStatusOrder(status));
			sql.append("GROUP BY odl.ItemID,odl.CompanyID having COUNT(odl.[LineNo]) =1 ");
			sql.append(" union all ");
			sql.append("SELECT sum(odlp.ShippedQty) as ShippedQty  ,count(oc.CartonNo) as CartonQty , odl.ItemID, odl.CompanyID ");
			sql.append("FROM Orders od ");
			sql.append("JOIN OrderLines odl ON odl.OrderNo = od.OrderNo  AND odl.CompanyID = od.CompanyID ");
			sql.append("JOIN OrderLinePlates odlp ON odlp.OrderNo = odl.OrderNo AND odlp.CompanyID = odl.CompanyID AND odlp.[LineNo] = odl.[LineNo] ");
			sql.append("left join OrderLineCartons oc on oc.OrderNo = od.OrderNo and oc.CompanyID = od.CompanyID ");
			sql.append("WHERE od.OrderNo = '"+orderNo+"'  AND od.CompanyID = 'W17' and od.CustomerID='Seidig0005' and od.AccountID='Walmart-ETC' ");
			sql.append(sqlStatusOrder(status));
			sql.append("GROUP BY odl.ItemID,odl.CompanyID having COUNT(odl.[LineNo]) >1 ) sp ");*/
			
			
			sql.append(" ) g on g.ItemID = f.ItemID and g.CompanyID = f.CompanyID");
			//sql.append("  GROUP BY odl.ItemID) g on g.ItemID = f.ItemID and g.CompanyID = f.CompanyID");
			
			sql.append(" ) a )b ORDER BY b.Pallets desc, b.orderLineCase DESC");
			
			System.out.println("bsql1:"+sql.toString());
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderItemsInfoByOrderNo error:"+e);
		}
	}	
	
	public DBRow findOrderPONoInfoByOrderNo(long orderNo, String companyId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT c.PONo, SUM(c.orderLineCase) orderLineCaseSum, SUM(c.weightItemAndPallet) weightItemAndPallet,MAX(c.DeptNo)DeptNo FROM").append("\n");
			sql.append(" (SELECT b.PONo, b.orderLineCase,b.DeptNo,").append("\n");
//			sql.append(" CAST(CEILING(b.CustomerPallets* b.PalletWeight+ b.PoundPerPackage* b.orderLineCase)  as int)weightItemAndPallet");
//			sql.append(" CAST((CEILING(b.palletCounts* b.PalletWeight)+ CEILING(b.PoundPerPackage* b.orderLineCase)) as int)weightItemAndPallet");
			sql.append(" CAST((CEILING(b.palletCounts* b.PalletWeight+b.PoundPerPackage* b.orderLineCase)) as int)weightItemAndPallet").append("\n");
			sql.append(" FROM (select a.*, CASE ").append("\n");
			//sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and (a.Unit = 'Package' or a.Unit = 'Pallet' or a.Unit = 'Piece'))").append("\n");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and ( a.Unit = 'Piece'))").append("\n");
			sql.append(" THEN  (case when (0 = a.OrderedQty%a.UnitsPerPackage) then (a.OrderedQty/a.UnitsPerPackage) else (a.OrderedQty/a.UnitsPerPackage+1) end )").append("\n");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and a.QtyPerUnit is not null and a.QtyPerUnit != 0 and (a.Unit = 'Inner Case'))").append("\n");
			sql.append(" THEN (case when (0 = a.OrderedQty%(a.UnitsPerPackage*a.QtyPerUnit)) then (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)) else (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)+1) end )").append("\n");
			sql.append(" ELSE a.OrderedQty END AS orderLineCase").append("\n");
			sql.append(" FROM ( ").append("\n");
			
			sql.append("  select f.*, g.ShippedQty OrderedQty  from (").append("\n");
			sql.append(" select sum(e.Pallets)Pallets,max(e.PoundPerPackage)PoundPerPackage, max(e.Unit)Unit, max(e.QtyPerUnit)QtyPerUnit, max(e.UnitsPerPackage)UnitsPerPackage").append("\n");
			sql.append(" ,max(e.PONo)PONo,max(e.PalletWeight)PalletWeight, sum(e.CustomerPallets)CustomerPallets,sum(e.palletCounts)palletCounts,max(e.DeptNo)DeptNo, e.ItemID, max(e.CompanyID)CompanyID").append("\n");
			
			sql.append("  from (select odl.Pallets,odl.OrderedQty,odl.PoundPerPackage, ci.Unit, ci.QtyPerUnit, ci.UnitsPerPackage,od.PONo,odl.PalletWeight, ISNULL(odl.CustomerPallets, 0)  CustomerPallets, odl.ItemID, odl.CompanyID").append("\n");
			sql.append(" ,CASE WHEN(od.CustomerID = 'VIZIO' OR od.CustomerID = 'VZO' OR od.CustomerID = 'VIZIO2' OR od.CustomerID = 'VIZIO3') THEN ISNULL(odl.CustomerPallets, odl.Pallets) ELSE odl.Pallets END AS palletCounts,od.DeptNo").append("\n");
			sql.append(" from Orders od ").append("\n");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID ").append("\n");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ").append("\n");
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID").append("\n");
			sql.append(" WHERE od.OrderNo = ").append(orderNo).append(" AND od.CompanyID = '").append(companyId).append("'").append("\n");
			sql.append(sqlStatusOrder(status)).append("\n");
			
			sql.append(" ) e GROUP BY e.ItemID) f").append("\n");
			sql.append(" join ( select sum(odlp.ShippedQty)ShippedQty, max(odl.ItemID)ItemID, max(odl.CompanyID)CompanyID").append("\n");
			
			sql.append(" from Orders od ").append("\n");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID ").append("\n");
			sql.append(" JOIN OrderLinePlates odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo]").append("\n");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ").append("\n");
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID").append("\n");
			sql.append(" WHERE od.OrderNo = ").append(orderNo).append(" AND od.CompanyID = '").append(companyId).append("'").append("\n");
			sql.append(sqlStatusOrder(status)).append("\n");
			sql.append("  GROUP BY odl.ItemID)  g on g.ItemID = f.ItemID and g.CompanyID = f.CompanyID").append("\n");
			
			
			sql.append(" ) a )b )c GROUP BY c.PONo ORDER BY c.PONo").append("\n");
//			System.out.println("bsql2:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderPONoInfoByOrderNo error:"+e);
		}
	}	
	
	public DBRow findCarrierInfoByCarrierId(String carrierId, String companyId)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select top 1 * from Carriers   where replace(CarrierID,'''', '') = '").append(StrUtil.replacePartStr(carrierId, new String[]{"'"})).append("'").append(" AND CompanyID = '").append(companyId).append("'");
			sql.append(" order by DateUpdated DESC");
			System.out.println(sql.toString());
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findCarrierScacByCarrierId error:"+e);
		}
	}	
	
	public DBRow findCarrierInfoByCarrierName(String carrierName, String companyId)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select top 1 * from Carriers where CarrierName = '").append(carrierName).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND CompanyID = '").append(companyId).append("'");
			}
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findCarrierInfoByCarrierName error:"+e);
		}
	}	
	
	
	
	public DBRow[] findMasterBols(String startTime, String endTime, String companyId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from MasterBOLs where 1=1");
			if(!StrUtil.isBlank(startTime))
			{
				sql.append(" and DateCreated > ").append(startTime);
			}
			if(!StrUtil.isBlank(endTime))
			{
				sql.append(" and DateCreated < ").append(endTime);
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and CompanyID = '").append(companyId).append("'");
			}
			sql.append(sqlStatus(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolLinesByMasterNo error:"+e);
		}
	}
	
	public DBRow[] findMasterBolLinesByMasterNo(long masterBolNo, String companyId)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from MasterBOLLines where MasterBOLNo = ").append(masterBolNo)
			.append(" and CompanyID = '").append(companyId).append("'");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolLinesByMasterNo error:"+e);
		}
	}
	
	public DBRow[] findOrdersByMasterNo(long masterBolNo, String companyId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT od.* FROM MasterBOLLines boll");
			sql.append(" JOIN Orders od ON od.OrderNo = boll.OrderNo AND boll.CompanyID = od.CompanyID");
			sql.append(" WHERE boll.MasterBOLNo = ").append(masterBolNo);
			sql.append(" AND boll.CompanyID = '").append(companyId).append("'");
			sql.append(sqlStatusOrder(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersByMasterNo error:"+e);
		}
	}
	
	public DBRow[] findOrderLinesByOrderNoAndCompanyId(long orderNo, String companyId)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from OrderLines where OrderNo = ").append(orderNo)
			.append(" and CompanyID = '").append(companyId).append("'");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderLinesByOrderNoAndCompanyId error:"+e);
		}
	}
	public DBRow[] findOrderLinePlatesByOrderNoAndCompanyId(long orderNo, String companyId)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from OrderLinePlates where OrderNo = ").append(orderNo)
			.append(" and CompanyID = '").append(companyId).append("'");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderLinePlatesByOrderNoAndCompanyId error:"+e);
		}
	}
	
	public long addMasterBols(DBRow row)throws Exception
	{
		try
		{
			return dbUtilAutoTranSQLServer.insertReturnId("MasterBOLs", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.addMasterBols error:"+e);
		}
	}
	public long addMasterBOLLines(DBRow row)throws Exception
	{
		try
		{
			return dbUtilAutoTranSQLServer.insertReturnId("MasterBOLLines", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.addMasterBols error:"+e);
		}
	}
	public long addOrders(DBRow row)throws Exception
	{
		try
		{
			return dbUtilAutoTranSQLServer.insertReturnId("Orders", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.addOrders error:"+e);
		}
	}
	public long addOrderLines(DBRow row)throws Exception
	{
		try
		{
			return dbUtilAutoTranSQLServer.insertReturnId("OrderLines", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.addOrderLines error:"+e);
		}
	}
	public long addOrderLinePlates(DBRow row)throws Exception
	{
		try
		{
			return dbUtilAutoTranSQLServer.insertReturnId("OrderLinePlates", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.addOrderLinePlates error:"+e);
		}
	}
	/**
	 * 通过bolNo或者CtnNo查询Receipts
	 * @param bolNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptsByBolNoOrContainerNo(String bolNo, String containerNo,String CompanyID,String CustomerID, String[] status) throws Exception
	{
		try
		{
			String sql = "SELECT * FROM Receipts WHERE 1=1 ";
			if(!StrUtil.isBlank(bolNo))
			{
				sql += " and rtrim(ltrim(BOLNo)) = '"+bolNo+"'";
			}
			else if(!StrUtil.isBlank(containerNo))
			{
				sql += " and rtrim(ltrim(ContainerNo)) = '"+containerNo+"'";
			}
			if(!StrUtil.isBlank(CompanyID))
			{
				sql += " and CompanyID = '"+CompanyID+"'";
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql += " and CustomerID = '"+CustomerID+"'";
			}
			sql += sqlStatus(status);
 			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptsByBolNoOrContainerNo error:"+e);
		}
	}
	
	/**
	 * 通过BolNo或者CtnNo，ReceiptsNo获得明细
	 * @param bolNo
	 * @param receiptsNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptLinesPalteByBolNoOrCtnNO(String bolNo, String containerNo, long receiptsNo, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT renp.[LineNo],ren.ItemID,re.SupplierID,ren.LotNo, renp.PlateNo,ren.LocationID,ren.ExpectedQty, re.CompanyID");
			sql.append(" FROM Receipts re ");
			sql.append(" JOIN ReceiptLines ren on ren.CompanyID = re.CompanyID AND ren.ReceiptNo = re.ReceiptNo");
			sql.append(" LEFT JOIN ReceiptLinePlates renp on renp.CompanyID = re.CompanyID AND renp.ReceiptNo = re.ReceiptNo AND renp.[LineNo] = ren.[LineNo]");
			sql.append(" WHERE 1=1");
			if(!StrUtil.isBlank(bolNo))
			{
				sql.append(" AND rtrim(ltrim(re.BOLNo)) = '").append(bolNo).append("'");
			}
			if(!StrUtil.isBlank(containerNo))
			{
				sql.append(" AND rtrim(ltrim(re.ContainerNo)) = '").append(containerNo).append("'");
			}
			sql.append(sqlStatusRe(status));
			sql.append(" AND re.Status != '").append(WmsOrderStatusKey.CLOSED).append("'");
			sql.append(" AND re.ReceiptNo = ").append(receiptsNo);
			sql.append(" ORDER BY ren.[LineNo]");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptLinesPalteByBolNo error:"+e);
		}
	}
	
	/**
	 * 通过LoadNo查询一条MasterBol 某些数据
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolsSomeInfoByLoadNo(String loadNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT mbol.MasterBOLNo ,mbol.CustomerID, whs.Address1, whs.Address2, whs.City, whs.State, whs.ZipCode, whs.Phone, whs.Contact, mbol.CompanyID,");
			sql.append(" mbol.ShipToName, mbol.ShipToAddress1, mbol.ShipToAddress2, mbol.ShipToCity, mbol.ShipToState, mbol.ShipToZipCode, mbol.ShipToID, mbol.ShipToPhone,");
			sql.append(" mbol.BillToName, mbol.BillToAddress1, mbol.BillToAddress2, mbol.BillToCity, mbol.BillToState, mbol.BillToZipCode, mbol.Note,");
			sql.append(" mbol.LoadNo, mbol.AppointmentDate, mbol.ProNo, mbol.FreightTerm,mbol.CarrierID, ca.ShipFromName");
			sql.append(" FROM MasterBOLs mbol");
			sql.append(" LEFT JOIN Warehouses whs ON  whs.WarehouseID = mbol.WarehouseID");
			sql.append(" LEFT JOIN CustomerAccounts ca ON mbol.CompanyID = ca.CompanyID AND mbol.CustomerID = ca.CustomerID AND mbol.AccountID = ca.AccountID");
			sql.append(" WHERE replace(mbol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND mbol.CompanyID='"+companyId+"'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND mbol.CustomerID='"+customerId+"'");
			}	
			sql.append(sqlStatusMbol(status));
			sql.append(" AND whs.CompanyID = mbol.CompanyID");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolsSomeInfoByLoadNo error:"+e);
		}
	}
	
	/**
	 * 通过MasterBol查询商品总和，总重量
	 * @param mbol
	 * @param companyID
	 * @return
	 * @throws Exception
	 */
	public DBRow findItemSumCountWeightByMasterBolCompany(long mbol, String companyID, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select sum(c.palletCount) palletSum, sum(c.orderLineCase) caseSum, sum(c.weightItemAndPallet) weigthSum,MAX(c.CommodityDescription)CommodityDescription,MAX(c.NMFC) NMFC, MAX(c.FreightClass) FreightClass ");
			sql.append(" from (SELECT CAST(round(b.Pallets,2) as numeric(5,2)) palletCount, b.orderLineCase, ");
			sql.append(" CAST(CEILING(b.CustomerPallets* b.PalletWeight+ b.PoundPerPackage* b.orderLineCase)  as int)weightItemAndPallet,");
			sql.append(" b.CommodityDescription,b.NMFC, b.FreightClass,b.OrderNo");
			sql.append(" FROM (select  CASE ");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and (a.Unit = 'Package' or a.Unit = 'Pallet' or a.Unit = 'Piece'))");
			sql.append(" THEN  (case when (0 = a.OrderedQty%a.UnitsPerPackage) then (a.OrderedQty/a.UnitsPerPackage) else (a.OrderedQty/a.UnitsPerPackage+1) end )");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and a.QtyPerUnit is not null and a.QtyPerUnit != 0 and (a.Unit = 'Inner Case'))");
			sql.append(" THEN (case when (0 = a.OrderedQty%(a.UnitsPerPackage*a.QtyPerUnit)) then (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit))else (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)+1) end )");
			sql.append(" ELSE a.OrderedQty END AS orderLineCase,a.*");
			sql.append(" FROM (select odl.Pallets,odl.OrderedQty,odl.PoundPerPackage,odl.PalletWeight,ci.CommodityDescription, odl.NMFC, ");
			sql.append(" odl.FreightClass, boll.OrderNo, odl.ItemID, ci.Unit, ci.QtyPerUnit, ci.UnitsPerPackage, ISNULL(odl.CustomerPallets, 0)  CustomerPallets");
			sql.append(" FROM MasterBOLLines boll");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND boll.CompanyID = od.CompanyID ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND boll.CompanyID = odl.CompanyID ");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ");
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID");
			sql.append(" WHERE 1=1");
			sql.append(" and boll.MasterBOLNo = ").append(mbol);
			sql.append(" AND boll.CompanyID = '").append(companyID).append("'");
			sql.append(sqlStatusOrder(status));
			sql.append(" ) a )b) c");
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findItemSumCountWeightByMasterBolCompany error:"+e);
		}
	}	
	
	
	/**
	 * 通过bolNo查询Receipts
	 * @param bolNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptsByBolNo(String bolNo, String[] status) throws Exception
	{
		try
		{
			String sql = "SELECT * FROM Receipts WHERE rtrim(ltrim(BOLNo)) = '"+bolNo+"'"
			+sqlStatus(status);
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptsByBolNo error:"+e);
		}
	}
	
	/**
	 * 通过CtnNo查询Receipts
	 * @param bolNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptsByContainerNo(String containerNo, String[] status) throws Exception
	{
		try
		{
			String sql = "SELECT * FROM Receipts WHERE rtrim(ltrim(ContainerNo)) = '"+containerNo+"'" 
					+sqlStatus(status);
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptsByContainerNo error:"+e);
		}
	}
	/**
	 * 通过bolNo更新Receipts
	 * @param bolNo
	 * @return
	 * @throws Exception
	 */
	public void updateReceiptsInYardDateByBolNo(String bolNo, String[] status, String SupplierID,String companyID,DBRow row) throws Exception
	{
		try
		{
			String where = " WHERE rtrim(ltrim(BOLNo)) = '"+bolNo+"'"
						+sqlStatus(status)
						+ " AND (InYardDate is null or InYardDate = '')";
			if(!StrUtil.isBlank(SupplierID))
			{
				where += " AND SupplierID = '"+SupplierID+"'";
			}
			if(!StrUtil.isBlank(companyID))
			{
				where += " AND CompanyID = '"+companyID+"'";
			}
			dbUtilAutoTranSQLServer.update(where, "Receipts", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.updateReceiptsInYardDateByBolNo error:"+e);
		}
	}
	/**
	 * 通过ctnNo更新Receipts
	 * @param bolNo
	 * @return
	 * @throws Exception
	 */
	public void updateReceiptsInYardDateByCtnNo(String ctnNo, String[] status, String SupplierID,String companyID, DBRow row) throws Exception
	{
		try
		{
			String where = " WHERE rtrim(ltrim(ContainerNo)) = '"+ctnNo+"'"
					+sqlStatus(status)
					+ " AND (InYardDate is null or InYardDate = '')";
			if(!StrUtil.isBlank(SupplierID))
			{
				where += " AND SupplierID = '"+SupplierID+"'";
			}
			if(!StrUtil.isBlank(companyID))
			{
				where += " AND CompanyID = '"+companyID+"'";
			}
			dbUtilAutoTranSQLServer.update(where, "Receipts", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.updateReceiptsInYardDateByCtnNo error:"+e);
		}
	}
	
	/**
	 * 查询所有卡车公司名称，去重, CarrierName
	 * @throws Exception
	 */
	public DBRow[] findAllCarriersNameNoRepeat() throws Exception
	{
		try
		{
			String sql = "select CarrierName from Carriers GROUP BY CarrierName";
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findAllCarriersNameNoRepeat error:"+e);
		}
	}
	
	
	public DBRow[] findCarrierWarehouseInfo() throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select c.CarrierID, c.CarrierName, w.Facility, c.CompanyID ");
			sql.append(" from (select CarrierID, max(CarrierName) CarrierName, max(CompanyID) CompanyID from Carriers group by CarrierID) c ");
			sql.append(" left join Def_FacilityWarehouse w on c.CompanyID = w.Warehouse");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findCarrierWarehouseInfo error:"+e);
		}
	}
	
	/**
	 * 查询customerAccount,a4打印模版
	 * @param AccountID
	 * @param CompanyID
	 * @param CustomerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterOrBolLabelFormatByAccountCompanyCustomer(String AccountID, String CompanyID, String CustomerID) throws Exception
	{
		try
		{
			String sql = "SELECT MasterBOLFormat,  BOLFormat FROM CustomerAccounts where 1=1"+
					" and AccountID = '"+AccountID+"' and CompanyID = '"+CompanyID+"'  and CustomerID = '"+CustomerID+"'";
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterOrBolLabelFormatByAccountCompanyCustomer error:"+e);
		}
	}
	
	/**
	 * 通过LoadNo查询Order主子表相关数据
	 * @param masterBOLNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersWmsByMasterBolNo(long masterBOLNo, String companyId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select bol.MasterBOLNo, boll.OrderNo, odl.Pallets, odl.ItemID, od.StagingAreaID, odlp.PlateNo, odlp.ShippedQty, od.PickingType, od.CustomerID, od.CompanyID, od.PalletTypeID ");
			sql.append(" from MasterBOLs bol ");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND bol.CompanyID = od.CompanyID ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND bol.CompanyID = odl.CompanyID ");
			sql.append(" JOIN OrderLinePlates odlp ON odl.[LineNo]=odlp.[LineNo] AND odl.OrderNo = odlp.OrderNo AND bol.CompanyID = odlp.CompanyID ");
			sql.append(" WHERE bol.MasterBOLNo = ").append(masterBOLNo);
			sql.append(" AND bol.CompanyID = '").append(companyId).append("'");
			sql.append(sqlStatusBol(status));
			sql.append(sqlStatusOrder(status));
			sql.append(" ORDER BY boll.OrderNo, odl.ItemID, odlp.PlateNo");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterOrdersWmsByMasterBolNo error:"+e);
		}
	}
	
	
	/**
	 * 通过LoadNo查询Order主子表相关数据
	 * @param masterBOLNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersSomeInfoWmsByLoadNo(String loadNo,String CompanyID,String CustomerID, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT od.CustomerID, whs.Address1, whs.Address2, whs.City, whs.State, whs.ZipCode, whs.Phone, whs.Contact, od.CompanyID,");
			sql.append(" od.ShipToName, od.ShipToAddress1, od.ShipToAddress2, od.ShipToCity, od.ShipToState, od.ShipToZipCode, od.ShipToID, od.ShipToPhone,");
			sql.append(" od.BillToName, od.BillToAddress1, od.BillToAddress2, od.BillToCity, od.BillToState, od.BillToZipCode, od.Note,");
			sql.append(" od.LoadNo, od.AppointmentDate, od.ProNo, v.External_ID as ExternalID, od.FreightTerm, od.PalletTypeID, od.PickingType, od.OrderNo,od.StagingAreaID, od.PONo");
			sql.append(" FROM Orders od");
			sql.append(" LEFT JOIN Warehouses whs ON whs.CompanyID = od.CompanyID AND whs.WarehouseID = od.WarehouseID");
			sql.append(" left join vizio_extension v on v.vizio_dn = od.ReferenceNo ");
			sql.append(" WHERE replace(od.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			sql.append(sqlStatusOrder(status));
			if(!StrUtil.isBlank(CompanyID))
			{
				sql.append(" AND od.CompanyID = '").append(CompanyID).append("'");
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql.append(" AND od.CustomerID = '").append(CustomerID).append("'");
			}
			sql.append(" ORDER BY od.OrderNo");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersSomeInfoWmsByLoadNo error:"+e);
		}
	}
	
	
	/**
	 * 通过LoadNo查询Order主子表相关数据
	 * @param OrderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersItemsWmsByOrderNoForConsolidate(long OrderNo, String companyId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select odl.OrderNo, odl.ItemID, odl.LotNo,odl.OrderedQty, odl.[LineNo] ");
			sql.append(" , CAST(CEILING(CEILING(odl.OrderedQty)) as int) OrderedQtyInt, Pallets");
			sql.append(" ,CAST(CEILING(CEILING(odl.Pallets)) as int) PalletsInt");
			//, odlp.PlateNo, odlp.ShippedQty, odlp.[LineNo]
			sql.append(" FROM OrderLines odl");
//			sql.append(" LEFT JOIN OrderLinePlates odlp ON odl.[LineNo]=odlp.[LineNo] AND odl.OrderNo = odlp.OrderNo AND odl.CompanyID = odlp.CompanyID ");
			sql.append(" WHERE odl.OrderNo = ").append(OrderNo);
			sql.append(" AND odl.CompanyID = '").append(companyId).append("'");
			sql.append(" ORDER BY odl.OrderNo, odl.[LineNo], odl.ItemID");
//			sql.append(" ORDER BY odl.OrderNo, odlp.[LineNo], odl.ItemID, odlp.PlateNo");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersItemsWmsByOrderNo error:"+e);
		}
	}
	
	
	
	public DBRow[] findOrdersNoByLoadNo(String loadNo, String[] status)throws Exception
	{
		try
		{
			String sql = "SELECT * FROM Orders WHERE replace(LoadNo,' ', '') = '"+loadNo.replace(" ", "")+"'" +
					sqlStatus(status)+
							" ORDER BY OrderNo DESC";
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersNoAndCompanyByLoadNo error:"+e);
		}
	}
	
	/**
	 * 通过LoadNo得到Master数量
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public int findMasterBolNoCountByLoadNo(String loadNo,String CompanyID,String CustomerID, String[] status)throws Exception
	{
		try
		{
			String sql = "SELECT count(*) countMaster FROM MasterBOLs WHERE replace(LoadNo,' ', '') = '"+loadNo.replace(" ", "")+"'";
			if(!StrUtil.isBlank(CompanyID))
			{
				sql += " AND CompanyID = '"+CompanyID+"'";
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql += " AND CustomerID in ("+parseCustomerIDStrToInStr(CustomerID)+")";
			}
			sql += sqlStatus(status);
//			System.out.println("sql1:"+sql+"------------"+CustomerID);
			DBRow row = dbUtilAutoTranSQLServer.selectSingle(sql);
			if(null != row)
			{
				return row.get("countMaster", 0);
			}
			else
			{
				return 0;
			}
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolNoCountByLoadNo error:"+e);
		}
	}
	
	/**
	 * 通过LoadNo查询companyID和CustomerID
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadCompanyIdCustomerIdByLoadNo(String loadNo, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT bol.CompanyID, bol.CustomerID, bol.LoadNo FROM MasterBols bol");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND bol.CompanyID = od.CompanyID ");
			sql.append(" WHERE replace(bol.LoadNo,' ', '') = '"+loadNo.replace(" ", "")+"'");
			sql.append(sqlStatusBol(status));
			sql.append(sqlStatusOrder(status));
			sql.append(" GROUP BY bol.LoadNo, bol.CompanyID, bol.CustomerID");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadCompanyIdCustomerIdByLoadNo error:"+e);
		}
	}
	
	
	/**
	 * 通过LoadNo查询companyID和CustomerID
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderCompanyIdCustomerIdByLoadNo(String loadNo, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT CompanyID, CustomerID, LoadNo FROM Orders WHERE replace(LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("' ");
			sql.append(sqlStatus(status));
			sql.append(" GROUP BY LoadNo, CompanyID, CustomerID");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderCompanyIdCustomerIdByLoadNo error:"+e);
		}
	}
	
	/**
	 * 通过bolNo或者CtnNo查询Receipts
	 * @param bolNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptsrCompanyIdCustomerIdByBolNoOrContainerNo(String bolNo, String containerNo, String[] status) throws Exception
	{
		try
		{
			String sql = "";
			if(!StrUtil.isBlank(bolNo))
			{
				sql += "SELECT BOLNo, CompanyID, CustomerID FROM Receipts WHERE 1=1 ";
				sql += " and rtrim(ltrim(BOLNo)) = '"+bolNo+"'";
				sql += sqlStatus(status);
				sql += " GROUP BY BOLNo, CompanyID, CustomerID";
			}
			else if(!StrUtil.isBlank(containerNo))
			{
				sql += "SELECT ContainerNo, CompanyID, CustomerID FROM Receipts WHERE 1=1 ";
				sql += " and rtrim(ltrim(ContainerNo)) = '"+containerNo+"'";
				sql += sqlStatus(status);
				sql += " GROUP BY ContainerNo, CompanyID, CustomerID";
			}
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptsrCompanyIdCustomerIdByBolNoOrContainerNo error:"+e);
		}
	}
	
	
	/**
	 * 通过bolNo或者CtnNo查询Receipts
	 * @param bolNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptsrCompanyIdCustomerIdByBolNo(String bolNo, String[] status) throws Exception
	{
		try
		{
			String sql = "SELECT BOLNo, CompanyID, CustomerID FROM Receipts WHERE 1=1 ";
			sql += " and rtrim(ltrim(BOLNo)) = '"+bolNo+"'";
			sql += sqlStatus(status);
			sql += " GROUP BY BOLNo, CompanyID, CustomerID";
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptsrCompanyIdCustomerIdByBolNo error:"+e);
		}
	}
	
	
	/**
	 * 通过bolNo或者CtnNo查询Receipts
	 * @param bolNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptsrCompanyIdCustomerIdByContainerNo(String containerNo, String[] status) throws Exception
	{
		try
		{
			String sql = "SELECT ContainerNo, CompanyID, CustomerID FROM Receipts WHERE 1=1 ";
			sql += " and rtrim(ltrim(ContainerNo)) = '"+containerNo+"'";
			sql += sqlStatus(status);
			sql += " GROUP BY ContainerNo, CompanyID, CustomerID";
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptsrCompanyIdCustomerIdByContainerNo error:"+e);
		}
	}
	
	/**
	 * 查预约到达时间为某天的Loads
	 * @param time
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadNoAppointTimeBetweenTime(String startTime, String endTime, String[] status, String[] companyId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT mbol.LoadNo, mbol.AppointmentDate, mbol.CompanyID, mbol.CustomerID, mbol.CarrierID,mbol.AccountID, od.OrderNo, od.ReferenceNo ,odl.SupplierID, '' SupplierIDs,mbol.Status");
			sql.append(" FROM MasterBOLs mbol");
			sql.append(" join MasterBOLLines mboll on mboll.MasterBOLNo = mbol.MasterBOLNo and mboll.CompanyID = mbol.CompanyID");
			sql.append(" join Orders od on od.OrderNo = mboll.OrderNo and od.CompanyID = mbol.CompanyID");
			sql.append(" join OrderLines odl on odl.OrderNo = od.OrderNo and odl.CompanyID = od.CompanyID");
			sql.append(" WHERE 1=1 ");
			sql.append(" AND mbol.LoadNo IS NOT NULL AND mbol.LoadNo != ''");
//			sql.append(sqlStatusMbol(status));
//			sql.append(sqlStatusOrder(status));
			if(!StrUtil.isBlank(startTime))
			{
				sql.append(" and Convert(varchar(10),mbol.AppointmentDate,120) >= '" + startTime + "'");
			}
			if(!StrUtil.isBlank(endTime))
			{
				sql.append(" AND Convert(varchar(10),mbol.AppointmentDate,120) <= '" + endTime + "'");
			}
//			sql.append(" and (mbol.LoadNo = '150207569'");
//			sql.append(" or mbol.LoadNo = '123637356')");
			sql.append(sqlCompanyIdMbol(companyId));
			sql.append(" ORDER BY mbol.LoadNo,mbol.AppointmentDate");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadNoAppointTimeBetweenTime error:"+e);
		}
	}
	
	/**
	 * 查预约到达时间为某天的Receipts
	 * @param time
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptBolCtnAppointTimeBetweenTime(String startTime, String endTime, String[] status, String[] companyId) throws Exception
	{
		try
		{
			String sql = "SELECT BOLNo, ContainerNo, AppointmentDate, CompanyID, CustomerID, CarrierID, SupplierID,Status,ReferenceNo FROM Receipts where 1=1";
			if(!StrUtil.isBlank(startTime))
			{
				sql += " and Convert(varchar(10),AppointmentDate,120) >= '" + startTime + "'";
			}
			if(!StrUtil.isBlank(endTime))
			{
				sql += " AND Convert(varchar(10),AppointmentDate,120) <= '" + endTime + "'";
			}
//			sql += sqlStatus(status);
			sql += sqlCompanyId(companyId);
			sql += " ORDER BY AppointmentDate";
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptBolCtnAppointTimeBetweenTime error:"+e);
		}
	}
	
	/**
	 * 通过load查询bill of lading模版
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findBillOfLadingTemplateByLoadCompanyCustomerMaster(String loadNo, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT mbol.LoadNo, mbol.CompanyID,mbol.CustomerID,mbol.AccountID,ca.BOLFormat,ca.MasterBOLFormat, mbol.MasterBOLNo,mbol.ShipToID,mbol.ShipToName")
			.append(" FROM MasterBOLs mbol ")
			.append(" LEFT JOIN CustomerAccounts ca ON mbol.CustomerID = ca.CustomerID AND mbol.AccountID = ca.AccountID")
			.append(" WHERE replace(mbol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND mbol.CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND mbol.CustomerID = '").append(customerId).append("'");
			}	
			sql.append(sqlStatusMbol(status));
//			System.out.println(sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findBillOfLadingTemplateByLoadCompanyCustomerMaster error:"+e);
		}
	}
	
	
	/**
	 * 通过load查询bill of lading模版
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findBillOfLadingTemplateByOrderNo(long orderNo, String companyId, String customerId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT od.LoadNo, od.CompanyID,od.CustomerID,od.AccountID,ca.BOLFormat,ca.MasterBOLFormat,od.ShipToID, od.ShipToName,od.cn");
			sql.append("  FROM (SELECT *");
			sql.append("  FROM (SELECT COUNT (*) cn");
			sql.append(" ,MAX(LoadNo)LoadNo,MAX(CompanyID)CompanyID ,MAX(AccountID)AccountID");
			sql.append(" ,MAX(CustomerID)CustomerID,MAX(ShipToID)ShipToID,MAX(ShipToName)ShipToName,MAX(Status)Statusm,OrderNo");
			sql.append("  FROM Orders ");
			sql.append("  WHERE OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND CustomerID = '").append(customerId).append("'");
			}
//			sql.append(" AND Status != '").append(WmsOrderStatusKey.CLOSED).append("'");
			sql.append(" GROUP BY  OrderNo, CompanyID, CustomerID, ShipToID, ShipToName");
			sql.append(" ) a) od");
			sql.append(" LEFT JOIN CustomerAccounts ca ON od.CustomerID = ca.CustomerID AND od.AccountID = ca.AccountID");
			sql.append(" WHERE od.OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND od.CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND od.CustomerID = '").append(customerId).append("'");
			}
//			sql.append(" AND od.Status != '").append(WmsOrderStatusKey.CLOSED).append("'");
//			System.out.println("sqlNoM:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findBillOfLadingTemplateByLoadNoMaster error:"+e);
		}
	}
	

	
	/**
	 * 通过LoadNo查询是否是运到同一地址
	 * 1：同一地址
	 * 2：非同一地址
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public int findMasterBolIsToSameAddressByLoadNo(String loadNo, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			String sql = "select ISNULL(sum(a.cn), 0) cn from ("
						+"select count(*) cn from MasterBOLs where replace(LoadNo,' ', '') = '"+loadNo.replace(" ", "")+"'" +
								sqlStatus(status) +
										" GROUP BY CompanyID,AccountID,CustomerID, ShipToID,ShipToName) a";
			if(!StrUtil.isBlank(companyId))
			{
				
			}
			return dbUtilAutoTranSQLServer.selectSingle(sql).get("cn", 0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolIsToSameAddressByLoadNo error:"+e);
		}
	}
	
	
	/**
	 * 通过load查询bill of lading模版
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findBillOfLadingTemplateByLoadCompanyCustomerNoMaster(String loadNo, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT od.LoadNo, od.CompanyID,od.CustomerID,od.AccountID,ca.BOLFormat,ca.MasterBOLFormat, od.OrderNo,od.ShipToID,od.ShipToName")
			.append(" FROM Orders od ")
			.append(" LEFT JOIN CustomerAccounts ca ON od.CustomerID = ca.CustomerID AND od.AccountID = ca.AccountID")
			.append(" WHERE replace(od.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND od.CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND od.CustomerID = '").append(customerId).append("'");
			}	
			sql.append(sqlStatusOrder(status));
//			System.out.println("o:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findBillOfLadingTemplateByLoadCompanyCustomerNoMaster error:"+e);
		}
	}
	
	/**
	 * 通过LoadNo查询是否是运到同一地址
	 * 1：同一地址
	 * 2：非同一地址
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public int findOrdersIsToSameAddressByLoadNo(String loadNo, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			String sql = "select ISNULL(sum(a.cn), 0) cn from ("
						+"select count(*) cn from Orders where replace(LoadNo,' ', '') = '"+loadNo.replace(" ", "")+"'" +
						 sqlStatus(status) +
						 		" GROUP BY AccountID,CustomerID, ShipToID,ShipToName) a";
			return dbUtilAutoTranSQLServer.selectSingle(sql).get("cn", 0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersIsToSameAddressByLoadNo error:"+e);
		}
	}
	
	
	public DBRow[] findVizioOrderInfoMasterByLoadNo(String loadNo) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
//			sql.append("select icl.SegmentDescription, od.ReferenceNo from Orders od ")
//			.append(" join Interchanges ic on 'DC'+od.ReferenceNo = ic.ControlNo and od.CompanyID = ic.CompanyID and od.CustomerID = ic.ReceiverID")
//			.append(" join InterchangeLines icl on ic.InterchangeNo = icl.InterchangeNo and ic.CompanyID = icl.CompanyID")
//			.append(" WHERE LoadNo = '").append(loadNo).append("'");
//			.append(" where od.ReferenceNo = '8500559003'");
			sql.append("select * from InterchangeLines where InterchangeNo = 708882 and [LineNo] = 2");
//			System.out.println("sql:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findVizioOrderInfoMasterByLoadNo error:"+e);
		}
	}
	
	public DBRow[] findLoadNoOrdersItemInfoGroupByLoadNo(String loadNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT * FROM (");
			sql.append(" SELECT c.CommodityDescription,SUM(c.weightItemAndPallet) weightItemAndPallet, SUM(c.orderLineCase) orderLineCase,");
			sql.append(" SUM(c.palletCount) palletCount, MAX(c.NMFC) NMFC, MAX(c.FreightClass) FreightClass ,SUM(c.CustomerPallets) CustomerPallets");
			sql.append(" FROM (SELECT b.Pallets palletCount, b.orderLineCase, ");
			sql.append(" CAST(CEILING(b.CustomerPallets* b.PalletWeight+ b.PoundPerPackage* b.orderLineCase)  as int)weightItemAndPallet,");
			sql.append(" b.CommodityDescription,b.NMFC, b.FreightClass ,b.CustomerPallets");
			sql.append(" FROM (SELECT  CASE ");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and (a.Unit = 'Package' or a.Unit = 'Pallet' or a.Unit = 'Piece'))");
			sql.append(" THEN  (case when (0 = a.OrderedQty%a.UnitsPerPackage) then (a.OrderedQty/a.UnitsPerPackage) else (a.OrderedQty/a.UnitsPerPackage+1) end )");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and a.QtyPerUnit is not null and a.QtyPerUnit != 0 and (a.Unit = 'Inner Case'))");
			sql.append(" THEN (case when (0 = a.OrderedQty%(a.UnitsPerPackage*a.QtyPerUnit)) then (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit))else (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)+1) end )");
			sql.append(" ELSE a.OrderedQty END AS orderLineCase,a.*");
			sql.append(" FROM (select odl.Pallets,odl.OrderedQty,odl.PoundPerPackage,odl.PalletWeight,ci.CommodityDescription, odl.NMFC, ");
			sql.append(" odl.FreightClass, boll.OrderNo, odl.ItemID, ci.Unit, ci.QtyPerUnit, ci.UnitsPerPackage, ISNULL(odl.CustomerPallets, 0)  CustomerPallets");
			sql.append(" FROM MasterBOLs bol ");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND bol.CompanyID = od.CompanyID ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND bol.CompanyID = odl.CompanyID ");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ");
			sql.append(" WHERE replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND bol.CompanyID='"+companyId+"'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND bol.CustomerID='"+customerId+"'");
			}	
			sql.append(sqlStatusBol(status));
			sql.append(sqlStatusOrder(status));
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID");
			sql.append("  ) a )b ) c");
			sql.append(" GROUP BY c.CommodityDescription");
			sql.append(" ) d ORDER BY d.palletCount DESC, d.orderLineCase DESC");
			
//			System.out.println("msql1:"+sql.toString());
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadNoOrdersItemInfoGroupByLoadNo error:"+e);
		}
	}
	
	/**
	 * Counting sheet 查询
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderItemsInfoForCountingByOrderNo(long orderNo, String companyId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT b.Pallets palletCount, b.orderLineCase, ");
			sql.append(" CAST(CEILING(b.Pallets* b.PalletWeight+ b.PoundPerPackage* b.orderLineCase)  as int)weightItemAndPallet,");
			sql.append(" b.CommodityDescription,b.NMFC, b.FreightClass, b.OrderedQty,b.Unit");
			sql.append(" FROM (select  CASE ");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and (a.Unit = 'Package' or a.Unit = 'Pallet' or a.Unit = 'Piece'))");
			sql.append(" THEN  (case when (0 = a.OrderedQty%a.UnitsPerPackage) then (a.OrderedQty/a.UnitsPerPackage) else (a.OrderedQty/a.UnitsPerPackage+1) end )");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and a.QtyPerUnit is not null and a.QtyPerUnit != 0 and (a.Unit = 'Inner Case'))");
			sql.append(" THEN (case when (0 = a.OrderedQty%(a.UnitsPerPackage*a.QtyPerUnit)) then (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit))else (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)+1) end )");
			sql.append(" ELSE a.OrderedQty END AS orderLineCase,a.*");
			sql.append(" FROM (");
			
			sql.append(" select f.*, g.ShippedQty OrderedQty  from (");
			sql.append(" select sum(e.Pallets)Pallets,max(e.PoundPerPackage)PoundPerPackage,max(e.PalletWeight)PalletWeight,max(e.CommodityDescription)CommodityDescription, max(e.NMFC)NMFC, ");
			sql.append(" max(e.FreightClass)FreightClass, max(e.OrderNo)OrderNo, e.ItemID, max(e.Unit)Unit, max(e.QtyPerUnit)QtyPerUnit, max(e.UnitsPerPackage)UnitsPerPackage, max(e.CompanyID)CompanyID");
			
			sql.append(" from(select odl.Pallets,odl.OrderedQty,odl.PoundPerPackage,odl.PalletWeight,ci.CommodityDescription, odl.NMFC, ");
			sql.append(" odl.FreightClass, od.OrderNo, odl.ItemID, ci.Unit, ci.QtyPerUnit, ci.UnitsPerPackage, odl.CompanyID");
			sql.append(" FROM Orders od");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID ");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ");// and odl.CompanyID = ci.CompanyID
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID");
			sql.append(" WHERE od.OrderNo = ").append(orderNo).append(" AND od.CompanyID = '").append(companyId).append("'");
			sql.append(sqlStatusOrder(status));
			sql.append(" )e GROUP BY e.ItemID ) f ");
			
			sql.append(" join ( select sum(odlp.ShippedQty)ShippedQty, max(odl.ItemID)ItemID, max(odl.CompanyID)CompanyID");
			sql.append(" FROM Orders od");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID ");
			sql.append("  JOIN OrderLinePlates odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo]");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ");// and odl.CompanyID = ci.CompanyID
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID");
			sql.append(" WHERE od.OrderNo = ").append(orderNo).append(" AND od.CompanyID = '").append(companyId).append("'");
			sql.append(sqlStatusOrder(status));
			sql.append("  GROUP BY odl.ItemID)  g on g.ItemID = f.ItemID and g.CompanyID = f.CompanyID");
			
			sql.append(" ) a )b ORDER BY b.Pallets desc, b.orderLineCase DESC");
//			System.out.println("counting:"+sql.toString());
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderItemsInfoForCountingByOrderNo error:"+e);
		}
	}
	
	
	/**
	 * Generic 通过MasterBol查询Items,带分组
	 * @param MasterBOLNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadNoOrdersItemInfoGroupByMasterBolNo(long MasterBOLNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT * FROM (");
			sql.append(" SELECT c.CommodityDescription,SUM(c.weightItemAndPallet) weightItemAndPallet, SUM(c.orderLineCase) orderLineCase,");
			sql.append(" SUM(c.palletCount) palletCount, MAX(c.NMFC) NMFC, MAX(c.FreightClass) FreightClass ,SUM(c.CustomerPallets) CustomerPallets");
			sql.append(" FROM (SELECT b.Pallets palletCount, b.orderLineCase, ");
			sql.append(" CAST(CEILING(b.CustomerPallets* b.PalletWeight+ b.PoundPerPackage* b.orderLineCase)  as int)weightItemAndPallet,");
			sql.append(" b.CommodityDescription,b.NMFC, b.FreightClass ,b.CustomerPallets");
			sql.append(" FROM (SELECT  CASE ");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and (a.Unit = 'Package' or a.Unit = 'Pallet' or a.Unit = 'Piece'))");
			sql.append(" THEN  (case when (0 = a.OrderedQty%a.UnitsPerPackage) then (a.OrderedQty/a.UnitsPerPackage) else (a.OrderedQty/a.UnitsPerPackage+1) end )");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and a.QtyPerUnit is not null and a.QtyPerUnit != 0 and (a.Unit = 'Inner Case'))");
			sql.append(" THEN (case when (0 = a.OrderedQty%(a.UnitsPerPackage*a.QtyPerUnit)) then (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit))else (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)+1) end )");
			sql.append(" ELSE a.OrderedQty END AS orderLineCase,a.*");
			sql.append(" FROM (select odl.Pallets,odl.OrderedQty,odl.PoundPerPackage,odl.PalletWeight,ci.CommodityDescription, odl.NMFC, ");
			sql.append(" odl.FreightClass, boll.OrderNo, odl.ItemID, ci.Unit, ci.QtyPerUnit, ci.UnitsPerPackage, ISNULL(odl.CustomerPallets, 0)  CustomerPallets");
			sql.append(" FROM MasterBOLs bol ");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND bol.CompanyID = od.CompanyID ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND bol.CompanyID = odl.CompanyID ");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ");
			sql.append(" WHERE bol.MasterBOLNo = ").append(MasterBOLNo);
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND bol.CompanyID='"+companyId+"'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND bol.CustomerID='"+customerId+"'");
			}	
			sql.append(sqlStatusBol(status));
			sql.append(sqlStatusOrder(status));
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID");
			sql.append("  ) a )b ) c");
			sql.append(" GROUP BY c.CommodityDescription");
			sql.append(" ) d ORDER BY d.palletCount DESC, d.orderLineCase DESC");
			
//			System.out.println("msql1:"+sql.toString());
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadNoOrdersItemInfoGroupByMasterBolNo error:"+e);
		}
	}
	
	/**暂时无用
	 * Generic 通过MasterBol查询Ponos,带分组
	 * @param MasterBOLNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadNoOrdersPONoInfoByMasterBolNo(long MasterBOLNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT c.PONo, SUM(c.orderLineCase) orderLineCaseSum, SUM(c.weightItemAndPallet) weightItemAndPallet, c.ReferenceNo, MAX(c.StoreName)StoreName, MAX(c.ShipToName)ShipToName, MAX(c.ShipToStoreNo)ShipToStoreNo");
			sql.append(" FROM(SELECT b.PONo, b.orderLineCase, b.ReferenceNo, b.StoreName, b.ShipToName, b.ShipToStoreNo ,");
			sql.append(" CAST(CEILING(b.CustomerPallets* b.PalletWeight+ b.PoundPerPackage* b.orderLineCase)  as int)weightItemAndPallet");
			sql.append(" FROM (select a.*, CASE ");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and (a.Unit = 'Package' or a.Unit = 'Pallet' or a.Unit = 'Piece'))");
			sql.append(" THEN  (case when (0 = a.OrderedQty%a.UnitsPerPackage) then (a.OrderedQty/a.UnitsPerPackage) else (a.OrderedQty/a.UnitsPerPackage+1) end )");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and a.QtyPerUnit is not null and a.QtyPerUnit != 0 and (a.Unit = 'Inner Case'))");
			sql.append(" THEN (case when (0 = a.OrderedQty%(a.UnitsPerPackage*a.QtyPerUnit)) then (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)) else (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)+1) end )");
			sql.append(" ELSE a.OrderedQty END AS orderLineCase");
			sql.append(" FROM ( select odl.Pallets,odl.OrderedQty,odl.PoundPerPackage, ci.Unit, ci.QtyPerUnit, ci.UnitsPerPackage,od.PONo");
			sql.append(" , od.ReferenceNo,odl.PalletWeight, ISNULL(odl.CustomerPallets, 0)  CustomerPallets, od.StoreName, od.ShipToName, od.ShipToStoreNo");
			sql.append(" FROM MasterBOLs bol ");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND bol.CompanyID = od.CompanyID ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND bol.CompanyID = odl.CompanyID ");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ");
			sql.append(" WHERE bol.MasterBOLNo = ").append(MasterBOLNo);
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND bol.CompanyID='"+companyId+"'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND bol.CustomerID='"+customerId+"'");
			}	
			sql.append(sqlStatusBol(status));
			sql.append(sqlStatusOrder(status));
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID");
			sql.append(" ) a )b )c GROUP BY c.PONo, c.ReferenceNo ORDER BY c.PONo");
			//System.out.println("msql2:"+sql.toString());
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadNoOrdersPONoInfoByMasterBolNo error:"+e);
		}
	}
	
	/**
	 * 通过LoadNo查询MasterBols 某些数据
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolsSomeInfoNotOneByLoadNo(String loadNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT mbol.CustomerID, whs.Address1, whs.Address2, whs.City, whs.State, whs.ZipCode, whs.Phone, whs.Contact, mbol.CompanyID,");
			sql.append(" mbol.ShipToName, mbol.ShipToAddress1, mbol.ShipToAddress2, mbol.ShipToCity, mbol.ShipToState, mbol.ShipToZipCode, mbol.ShipToID, mbol.ShipToPhone,");
			sql.append(" mbol.BillToName, mbol.BillToAddress1, mbol.BillToAddress2, mbol.BillToCity, mbol.BillToState, mbol.BillToZipCode, mbol.Note,");
			sql.append(" mbol.LoadNo, mbol.AppointmentDate, mbol.ProNo, mbol.FreightTerm, mbol.MasterBOLNo");
			sql.append(" FROM  MasterBOLs mbol");
			sql.append(" LEFT JOIN Warehouses whs ON whs.CompanyID = mbol.CompanyID AND whs.WarehouseID = mbol.WarehouseID");
			sql.append(" WHERE replace(mbol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND mbol.CompanyID='"+companyId+"'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND mbol.CustomerID='"+customerId+"'");
			}	
			sql.append(sqlStatusMbol(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolsSomeInfoNotOneByLoadNo error:"+e);
		}
	}
	
	/**
	 * 通过load查询bill of lading模版
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findBillOfLadingTemplateByLoadMaster(String loadNo, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT mbl.LoadNo, mbl.CompanyID,mbl.CustomerID,mbl.AccountID,ca.BOLFormat,ca.MasterBOLFormat,mbl.ShipToID, mbl.ShipToName,mbl.cn");
			sql.append(" FROM (SELECT *");
			sql.append(" FROM (SELECT COUNT (*) cn");
			sql.append(" ,MAX(LoadNo)LoadNo,MAX(CompanyID)CompanyID ,MAX(AccountID)AccountID");
			sql.append(" ,MAX(CustomerID)CustomerID,MAX(ShipToID)ShipToID,MAX(ShipToName)ShipToName");
			sql.append(" FROM MasterBOLs ");
			sql.append(" WHERE replace(LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
			//	sql.append(" AND CustomerID = '").append(customerId).append("'");
				sql.append(" AND CustomerID in (").append(parseCustomerIDStrToInStr(customerId)).append(")");
			}
			sql.append(sqlStatus(status));
			sql.append(" GROUP BY  LoadNo, CompanyID, CustomerID, ShipToID, ShipToName");
			sql.append(" ) a) mbl");
			sql.append(" LEFT JOIN CustomerAccounts ca ON mbl.CustomerID = ca.CustomerID AND mbl.AccountID = ca.AccountID");
			sql.append(" WHERE replace(mbl.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND mbl.CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
			//	sql.append(" AND mbl.CustomerID = '").append(customerId).append("'");
				sql.append(" AND mbl.CustomerID in (").append(parseCustomerIDStrToInStr(customerId)).append(")");
			}
//			System.out.println("te:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findBillOfLadingTemplateByLoadMaster error:"+e);
		}
	}
	
	
	/**
	 * 通过load查询bill of lading模版
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findBillOfLadingTemplateByLoadNoMaster(String loadNo, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT od.LoadNo, od.CompanyID,od.CustomerID,od.AccountID,ca.BOLFormat,ca.MasterBOLFormat,od.ShipToID, od.ShipToName,od.cn");
			sql.append(" FROM (SELECT *");
			sql.append(" FROM (SELECT COUNT (*) cn");
			sql.append(" ,MAX(LoadNo)LoadNo,MAX(CompanyID)CompanyID ,MAX(AccountID)AccountID");
			sql.append(" ,MAX(CustomerID)CustomerID,MAX(ShipToID)ShipToID,MAX(ShipToName)ShipToName,MAX(Status)Status");
			sql.append(" FROM Orders ");
			sql.append(" WHERE replace(LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatus(status));
			sql.append(" GROUP BY  LoadNo, CompanyID, CustomerID, ShipToID, ShipToName");
			sql.append(" ) a) od");
			sql.append(" LEFT JOIN CustomerAccounts ca ON od.CustomerID = ca.CustomerID AND od.AccountID = ca.AccountID");
			sql.append(" WHERE replace(od.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND od.CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND od.CustomerID = '").append(customerId).append("'");
			}
//			sql.append(sqlStatusOrder(status));
//			System.out.println("sqlNoM:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findBillOfLadingTemplateByLoadNoMaster error:"+e);
		}
	}
	
	/**
	 * 通过account,customer,companyid,loadno查询MasterBols
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @param accountId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolNosByAccountShiptoAddr(String LoadNo,String CompanyID, String CustomerID, String AccountID,String ShipToID,String ShipToName, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select MasterBOLNo from MasterBOLs");
			sql.append(" where replace(LoadNo,' ', '') = '").append(LoadNo.replace(" ", "")).append("'");
			sql.append(" and CompanyID = '").append(CompanyID).append("'");
			sql.append(" and CustomerID = '").append(CustomerID).append("'");
			sql.append(" and AccountID = '").append(AccountID).append("'");
			sql.append(" and ShipToID = '").append(ShipToID).append("'");
			sql.append(" and ShipToName = '").append(ShipToName).append("'");
			sql.append(sqlStatus(status));
//			System.out.println("sl:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolByAccountShiptoAddr error:"+e);
		}
	}
	
	/**
	 * 通过account,customer,companyid,loadno查询MasterBols
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @param accountId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderNosByAccountShiptoAddr(String LoadNo,String CompanyID, String CustomerID, String AccountID,String ShipToID,String ShipToName, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select OrderNo from Orders");
			sql.append(" where replace(LoadNo,' ', '') = '").append(LoadNo.replace(" ", "")).append("'");
			sql.append(" and CompanyID = '").append(CompanyID).append("'");
			sql.append(" and CustomerID = '").append(CustomerID).append("'");
			sql.append(" and AccountID = '").append(AccountID).append("'");
			sql.append(" and ShipToID = '").append(ShipToID).append("'");
			sql.append(" and ShipToName = '").append(ShipToName).append("'");
			sql.append(sqlStatus(status));
//			System.out.println("orderNoM:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderNosByAccountShiptoAddr error:"+e);
		}
	}
	
	
	/**
	 * 通过LoadNo查询MasterBols 某些数据
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolsSomeInfoOneByMasterBolArr(String loadNo,Long[] masterBolArr, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT mbol.CustomerID, whs.Address1, whs.Address2, whs.City, whs.State, whs.ZipCode, whs.Phone, whs.Contact, mbol.CompanyID,");
			sql.append(" mbol.ShipToName, mbol.ShipToAddress1, mbol.ShipToAddress2, mbol.ShipToCity, mbol.ShipToState, mbol.ShipToZipCode, mbol.ShipToID, mbol.ShipToPhone ,mbol.AccountID");
//			sql.append(" mbol.BillToName, mbol.BillToAddress1, mbol.BillToAddress2, mbol.BillToCity, mbol.BillToState, mbol.BillToZipCode");
			sql.append("  ,CASE WHEN(mbol.FreightTerm = 'Third Party') THEN mbol.BillToName ELSE '' END as BillToName");
			sql.append("  ,CASE WHEN(mbol.FreightTerm = 'Third Party') THEN mbol.BillToAddress1 ELSE '' END as BillToAddress1");
			sql.append("  ,CASE WHEN(mbol.FreightTerm = 'Third Party') THEN mbol.BillToAddress2 ELSE '' END as BillToAddress2");
			sql.append(" , CASE WHEN(mbol.FreightTerm = 'Third Party') THEN mbol.BillToCity ELSE '' END as BillToCity");
			sql.append("  ,CASE WHEN(mbol.FreightTerm = 'Third Party') THEN mbol.BillToState ELSE '' END as BillToState");
			sql.append("  ,CASE WHEN(mbol.FreightTerm = 'Third Party') THEN mbol.BillToZipCode ELSE '' END as BillToZipCode");
			sql.append(" , mbol.Note,mbol.ShipToStoreNo,mbol.AccountID");
			sql.append(" ,mbol.LoadNo, mbol.AppointmentDate, mbol.ProNo, mbol.FreightTerm, mbol.MasterBOLNo,mbol.CarrierID, ca.ShipFromName");
			sql.append(" FROM  MasterBOLs mbol");
			sql.append(" LEFT JOIN Warehouses whs ON whs.CompanyID = mbol.CompanyID AND whs.WarehouseID = mbol.WarehouseID");
			sql.append(" LEFT JOIN CustomerAccounts ca ON mbol.CompanyID = ca.CompanyID AND mbol.CustomerID = ca.CustomerID AND mbol.AccountID = ca.AccountID");
			sql.append(" WHERE replace(mbol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(masterBolArr.length > 0)
			{
				sql.append(" AND mbol.MasterBOLNo = ").append(masterBolArr[0]);
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND mbol.CompanyID='"+companyId+"'");
			}
			if(!StrUtil.isBlank(customerId))
			{
			//	sql.append(" AND mbol.CustomerID='"+customerId+"'");
				sql.append(" AND mbol.CustomerID in (").append(parseCustomerIDStrToInStr(customerId)).append(")");
			}	
			sql.append(sqlStatusMbol(status));
//			System.out.println("ll:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolsSomeInfoOneByMasterBolArr error:"+e);
		}
	}
	
	/**
	 * Generic 通过MasterBol字符串查询Items,带分组
	 * @param MasterBOLNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadNoOrdersItemInfoGroupByMasterBolNoArr(String loadNo, Long[] MasterBOLNos, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT CommodityDescription,weightItemAndPallet,orderLineCase,palletCount, ").append("\n");
			sql.append(" CASE WHEN(NMFC is null or NMFC='') THEN NMFC1 ELSE NMFC end as NMFC,CASE WHEN(FreightClass is null or FreightClass='') THEN FreightClass1 ELSE FreightClass end as FreightClass").append("\n");
			sql.append("  FROM (").append("\n");
			sql.append(" SELECT b.Pallets palletCount, b.orderLineCase, ").append("\n");
			sql.append(" CAST(CEILING(b.palletCounts* b.PalletWeight+ b.PoundPerPackage* b.orderLineCase)  as int)weightItemAndPallet,").append("\n");
			sql.append(" b.CommodityDescription,b.NMFC, b.FreightClass ,b.CustomerPallets,b.NMFC1, b.FreightClass1").append("\n");
			sql.append(" FROM (SELECT  CASE ").append("\n");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and (a.Unit = 'Package' or a.Unit = 'Pallet' or a.Unit = 'Piece'))").append("\n");
			sql.append(" THEN  (case when (0 = a.OrderedQty%a.UnitsPerPackage) then (a.OrderedQty/a.UnitsPerPackage) else (a.OrderedQty/a.UnitsPerPackage+1) end )").append("\n");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and a.QtyPerUnit is not null and a.QtyPerUnit != 0 and (a.Unit = 'Inner Case'))").append("\n");
			sql.append(" THEN (case when (0 = a.OrderedQty%(a.UnitsPerPackage*a.QtyPerUnit)) then (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit))else (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)+1) end )").append("\n");
			sql.append(" ELSE a.OrderedQty END AS orderLineCase,a.*").append("\n");
			
			sql.append(" FROM (select f.*, g.ShippedQty OrderedQty from (").append("\n");
			sql.append(" select sum(e.Pallets)Pallets,max(e.PoundPerPackage)PoundPerPackage,max(e.PalletWeight)PalletWeight,max(e.CommodityDescription)CommodityDescription, max(e.NMFC)NMFC, ").append("\n");
			sql.append(" max(e.FreightClass)FreightClass, max(e.OrderNo)OrderNo, max(e.ItemID)ItemID, max(e.Unit)Unit, max(e.QtyPerUnit)QtyPerUnit, max(e.UnitsPerPackage)UnitsPerPackage,sum(e.CustomerPallets)CustomerPallets").append("\n");
			sql.append("  , max(e.NMFC1)NMFC1, max(e.FreightClass1)FreightClass1 , max(e.CustomerID)CustomerID,sum(e.palletCounts)palletCounts, max(e.CompanyID)CompanyID").append("\n");
			
			sql.append(" FROM (select odl.Pallets,odl.PoundPerPackage,odl.PalletWeight,ci.CommodityDescription, odl.NMFC, ").append("\n");
			sql.append(" odl.FreightClass, boll.OrderNo, odl.ItemID, ci.Unit, ci.QtyPerUnit, ci.UnitsPerPackage, ISNULL(odl.CustomerPallets, 0)  CustomerPallets").append("\n");
			sql.append(" , ci.NMFC NMFC1, ci.FreightClass FreightClass1, bol.CustomerID,bol.CompanyID,").append("\n");
			sql.append(" CASE WHEN(bol.CustomerID = 'VIZIO' OR bol.CustomerID = 'VZO' OR bol.CustomerID = 'VIZIO2' OR bol.CustomerID = 'VIZIO3') THEN ISNULL(odl.CustomerPallets, odl.Pallets) ELSE odl.Pallets END AS palletCounts").append("\n");
			sql.append(" FROM MasterBOLs bol ").append("\n");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ").append("\n");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND bol.CompanyID = od.CompanyID ").append("\n");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND bol.CompanyID = odl.CompanyID ").append("\n");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ").append("\n");
			sql.append(" WHERE replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'").append("\n");
			if(MasterBOLNos.length > 0)
			{
				sql.append(" AND (bol.MasterBOLNo = ").append(MasterBOLNos[0]).append("\n");
				for (int i = 1; i < MasterBOLNos.length; i++)
				{
					sql.append(" OR bol.MasterBOLNo = ").append(MasterBOLNos[i]).append("\n");
				}
				sql.append(")").append("\n");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND bol.CompanyID='"+companyId+"'").append("\n");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND bol.CustomerID='"+customerId+"'").append("\n");
			}	
			sql.append(sqlStatusBol(status)).append("\n");
			sql.append(sqlStatusOrder(status)).append("\n");
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID").append("\n");
			
			sql.append(" ) e GROUP BY e.ItemID) f ").append("\n");
			sql.append(" left join (select sum(odlp.ShippedQty) ShippedQty, max(odl.ItemID)ItemID, max(odl.CompanyID)CompanyID").append("\n");
			sql.append(" FROM MasterBOLs bol ").append("\n");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ").append("\n");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND bol.CompanyID = od.CompanyID ").append("\n");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND bol.CompanyID = odl.CompanyID ").append("\n");
			sql.append(" JOIN OrderLinePlates odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo]").append("\n");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ").append("\n");
			sql.append(" WHERE replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'").append("\n");
			if(MasterBOLNos.length > 0)
			{
				sql.append(" AND (bol.MasterBOLNo = ").append(MasterBOLNos[0]).append("\n");
				for (int i = 1; i < MasterBOLNos.length; i++)
				{
					sql.append(" OR bol.MasterBOLNo = ").append(MasterBOLNos[i]).append("\n");
				}
				sql.append(")").append("\n");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND bol.CompanyID='"+companyId+"'").append("\n");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND bol.CustomerID='"+customerId+"'").append("\n");
			}	
			sql.append(sqlStatusBol(status)).append("\n");
			sql.append(sqlStatusOrder(status)).append("\n");
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID").append("\n");
			sql.append(" GROUP BY odl.ItemID) g on g.ItemID = f.ItemID and g.CompanyID = f.CompanyID").append("\n");
			
			sql.append("  ) a )b").append("\n");
			sql.append(" ) d ORDER BY d.palletCount DESC, d.orderLineCase DESC").append("\n");
			
//			System.out.println("msql1:"+sql.toString());
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadNoOrdersItemInfoGroupByMasterBolNo error:"+e);
		}
	}
	
	/**
	 * Generic 通过MasterBol查询Ponos,带分组
	 * @param MasterBOLNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadNoOrdersPONoInfoByMasterBolNoArr(String loadNo,Long[] MasterBOLNos, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT c.PONo, SUM(c.orderLineCase) orderLineCaseSum, SUM(c.weightItemAndPallet) weightItemAndPallet, c.ReferenceNo, MAX(c.StoreName)StoreName, MAX(c.ShipToName)ShipToName, MAX(c.ShipToStoreNo)ShipToStoreNo,MAX(c.DeptNo)DeptNo").append("\n");
			sql.append(" FROM(SELECT b.PONo, b.orderLineCase, b.ReferenceNo, b.StoreName, b.ShipToName, b.ShipToStoreNo ,").append("\n");
			sql.append(" CAST(CEILING(b.palletCounts* b.PalletWeight+ b.PoundPerPackage* b.orderLineCase)  as int)weightItemAndPallet,b.DeptNo").append("\n");
			sql.append(" FROM (select a.*, CASE ").append("\n");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and (a.Unit = 'Package' or a.Unit = 'Pallet' or a.Unit = 'Piece'))").append("\n");
			sql.append(" THEN  (case when (0 = a.OrderedQty%a.UnitsPerPackage) then (a.OrderedQty/a.UnitsPerPackage) else (a.OrderedQty/a.UnitsPerPackage+1) end )").append("\n");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and a.QtyPerUnit is not null and a.QtyPerUnit != 0 and (a.Unit = 'Inner Case'))").append("\n");
			sql.append(" THEN (case when (0 = a.OrderedQty%(a.UnitsPerPackage*a.QtyPerUnit)) then (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)) else (a.OrderedQty/(a.UnitsPerPackage*a.QtyPerUnit)+1) end )").append("\n");
			sql.append(" ELSE a.OrderedQty END AS orderLineCase").append("\n");
			sql.append(" FROM ( select odl.Pallets,odl.OrderedQty,odl.PoundPerPackage, ci.Unit, ci.QtyPerUnit, ci.UnitsPerPackage,od.PONo").append("\n");
			sql.append(" , od.ReferenceNo,odl.PalletWeight, ISNULL(odl.CustomerPallets, 0)  CustomerPallets, od.StoreName, od.ShipToName, od.ShipToStoreNo,od.DeptNo").append("\n");
			sql.append(" ,CASE WHEN(bol.CustomerID = 'VIZIO' OR bol.CustomerID = 'VZO' OR bol.CustomerID = 'VIZIO2' OR bol.CustomerID = 'VIZIO3') THEN ISNULL(odl.CustomerPallets, odl.Pallets) ELSE odl.Pallets END AS palletCounts").append("\n");
			sql.append(" FROM MasterBOLs bol ").append("\n");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ").append("\n");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND bol.CompanyID = od.CompanyID ").append("\n");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND bol.CompanyID = odl.CompanyID ").append("\n");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ").append("\n");
			sql.append(" WHERE replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'").append("\n");
			if(MasterBOLNos.length > 0)
			{
				sql.append(" AND (bol.MasterBOLNo = ").append(MasterBOLNos[0]).append("\n");
				for (int i = 1; i < MasterBOLNos.length; i++)
				{
					sql.append(" OR bol.MasterBOLNo = ").append(MasterBOLNos[i]).append("\n");
				}
				sql.append(")").append("\n");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND bol.CompanyID='"+companyId+"'").append("\n");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND bol.CustomerID='"+customerId+"'").append("\n");
			}	
			sql.append(sqlStatusBol(status)).append("\n");
			sql.append(sqlStatusOrder(status)).append("\n");
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID").append("\n");
			sql.append(" ) a )b )c GROUP BY c.PONo, c.ReferenceNo ORDER BY c.PONo").append("\n");
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadNoOrdersPONoInfoByMasterBolNo error:"+e);
		}
	}
	
	/**
	 * 通过MasterBol数组查询Orders
	 * @param loadNo
	 * @param masterBolArr
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolLinesByMasterBolArr(String loadNo, Long[] masterBolArr, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT boll.OrderNo, boll.CompanyID,bol.AccountID FROM MasterBOLLines boll");
			sql.append(" JOIN MasterBOLs bol ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ");
			sql.append(" WHERE replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND bol.CompanyID='"+companyId+"'");
			}
			if(!StrUtil.isBlank(customerId))
			{
//				sql.append(" AND bol.CustomerID='"+customerId+"'");
				sql.append(" AND bol.CustomerID in (").append(parseCustomerIDStrToInStr(customerId)).append(")");
			}
			if(masterBolArr != null && masterBolArr.length > 0)
			{
				sql.append(" AND (bol.MasterBOLNo = ").append(masterBolArr[0]);
				for (int i = 1; i < masterBolArr.length; i++)
				{
					sql.append(" OR bol.MasterBOLNo = ").append(masterBolArr[i]);
				}
				sql.append(")");
			}
			sql.append(sqlStatusBol(status));
			sql.append(" ORDER BY OrderNo DESC");
			
		//	System.out.println("s:"+sql.toString());
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolLinesByMasterBolArr error:"+e);
		}
	}
	
	/**
	 * 通过Order数组查询Orders
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersNoAndCompanyByLoadNoOrderArr(String loadNo, Long[] orderArr, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT OrderNo, CompanyID FROM Orders WHERE replace(LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(orderArr.length > 0)
			{
				sql.append(" AND (OrderNo = ").append(orderArr[0]);
				for (int i = 1; i < orderArr.length; i++)
				{
					sql.append(" OR OrderNo = ").append(orderArr[i]);
				}
				sql.append(")");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND CompanyID='").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND CustomerID='").append(customerId).append("'");
			}		
			sql.append(sqlStatus(status));
			sql.append(" ORDER BY OrderNo DESC");
//			System.out.println("s2:"+sql);
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersNoAndCompanyByLoadNo error:"+e);
		}
	}
	
/**
	 * 通过LoadNos、Master、查询supplierID
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findSupplierIDWithMasterByLoadNoArr(String[] loadNo, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT odl.SupplierID FROM MasterBOLs mbol");
			sql.append(" JOIN MasterBOLLines mboll ON mbol.CompanyID = mboll.CompanyID AND mbol.MasterBOLNo = mboll.MasterBOLNo");
			sql.append(" JOIN Orders od ON od.OrderNo = mboll.OrderNo AND mboll.CompanyID = od.CompanyID");
			sql.append(" JOIN OrderLines odl ON od.OrderNo = odl.OrderNo AND od.CompanyID = odl.CompanyID WHERE 1=1 ");
			if(loadNo.length > 0)
			{
				sql.append(" AND (replace(mbol.LoadNo,' ', '') = '").append(loadNo[0].replace(" ", "")).append("'");
				for (int i = 1; i < loadNo.length; i++) 
				{
					sql.append(" OR replace(mbol.LoadNo,' ', '') = '").append(loadNo[i].replace(" ", "")).append("'");
				}
				sql.append(")");
			}
			sql.append(sqlStatusMbol(status)).append(sqlStatusOrder(status));
			sql.append(" GROUP BY odl.SupplierID");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findSupplierIDWithMasterByLoadNoArr error:"+e);
		} 
	}
	
	/**
	 * 通过LoadNos、Order、查询supplierID
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findSupplierIDWithoutMasterByLoadNoArr(String[] loadNo, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select odl.SupplierID from Orders od");
			sql.append(" join OrderLines odl on od.OrderNo = odl.OrderNo and od.CompanyID = odl.CompanyID WHERE 1=1");
			if(loadNo.length > 0)
			{
				sql.append(" AND (replace(od.LoadNo,' ', '') = '").append(loadNo[0].replace(" ", "")).append("'");
				for (int i = 1; i < loadNo.length; i++) 
				{
					sql.append(" OR replace(od.LoadNo,' ', '') = '").append(loadNo[1].replace(" ", "")).append("'");
				}
				sql.append(")");
			}
			sql.append(sqlStatusOrder(status));
			sql.append(" GROUP BY odl.SupplierID");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findSupplierIDWithoutMasterByLoadNoArr error:"+e);
		} 
	}
	
	/**
	 * 通过CtnNos查询Receipts SupplierID
	 * @param bolNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptsSupplierIDByContainerNoArr(String[] containerNo, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT SupplierID FROM Receipts WHERE 1=1");
			if(containerNo.length > 0)
			{
				sql.append(" AND (rtrim(ltrim(ContainerNo)) = '").append(containerNo[0]).append("'");
				for (int i = 1; i < containerNo.length; i++) 
				{
					sql.append(" OR rtrim(ltrim(ContainerNo)) = '").append(containerNo[i]).append("'");
				}
				sql.append(")");
			}
			sql.append(sqlStatus(status));
			sql.append(" GROUP BY SupplierID");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptsSupplierIDByContainerNoArr error:"+e);
		}
	}
	
	/**
	 * 通过bolNos查询Receipts SupplierID
	 * @param bolNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptsSupplierIDByBolNoArr(String[] bolNo, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT SupplierID FROM Receipts WHERE 1=1");
			if(bolNo.length > 0)
			{
				sql.append(" AND (rtrim(ltrim(BOLNo)) = '").append(bolNo[0]).append("'");
				for (int i = 1; i < bolNo.length; i++) 
				{
					sql.append(" OR rtrim(ltrim(BOLNo)) = '").append(bolNo[i]).append("'");
				}
				sql.append(")");
			}
			sql.append(sqlStatus(status));
			sql.append(" GROUP BY SupplierID");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
			
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptsSupplierIDByBolNoArr error:"+e);
		}
	}
	
	/**
	 * shipping label
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * ShipFromName,ShipFromAddress1,City,ShipFromAddress2,
	 * ShipToName,ShipToAddress1,ShipToAddress2,
	 * SCACCode,OrderNo,ProNo,ReferenceNo,PONo,BOLNote,OrderedQty,Pallets
	 * @throws Exception
	 */
	public DBRow[] findOrdersShipPalletByLoadNo(String loadNo,long masterBolNo, String customerId, String companyId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select max(ca.ShipFromName)ShipFromName, (max(whs.Address1)+max(whs.Address2)) as ShipFromAddress1, max(whs.City)City, (max(whs.State)+' '+max(whs.ZipCode))ShipFromAddress2");
			sql.append(" , max(od.ShipToName)ShipToName, (max(od.ShipToAddress1)+' '+max(od.ShipToAddress2)) ShipToAddress1, (max(od.ShipToCity)+','+max(od.ShipToState)+' '+max(od.ShipToZipCode)) ShipToAddress2");
			sql.append(" , max(car.SCACCode)SCACCode,max(od.OrderNo)OrderNo,max(od.ProNo)ProNo,max(v.External_ID)ExternalID,max(od.ReferenceNo)ReferenceNo, max(od.PONo)PONo, max(od.BOLNote)BOLNote, sum(odl.OrderedQty)OrderedQty, CAST(CEILING(sum(odl.Pallets)) as int)Pallets");
			sql.append(" from MasterBOLs mbol ");
			sql.append(" join MasterBOLLines mboll on mboll.MasterBOLNo = mbol.MasterBOLNo and mboll.CompanyID = mbol.CompanyID");
			sql.append(" join Orders od on od.OrderNo = mboll.OrderNo and od.CompanyID = mboll.CompanyID");
			sql.append(" join OrderLines odl on odl.OrderNo = od.OrderNo and odl.CompanyID = od.CompanyID");
			sql.append(" left join Warehouses whs on whs.CompanyID = od.CompanyID and whs.WarehouseID = od.WarehouseID");
			sql.append(" LEFT JOIN CustomerAccounts ca ON ca.CompanyID = od.CompanyID AND ca.CustomerID = od.CustomerID AND ca.AccountID = od.AccountID");
			sql.append(" left join Carriers car on car.CompanyID = od.CompanyID and car.CarrierID = od.CarrierID");
			sql.append(" left join vizio_extension v on v.vizio_dn = od.ReferenceNo ");
			sql.append(" where replace(mbol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(masterBolNo > 0)
			{
				sql.append(" and mbol.MasterBOLNo = ").append(masterBolNo);
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and mbol.CustomerID = '").append(customerId).append("'");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and mbol.CompanyID = '").append(companyId).append("'");
			}
			sql.append(sqlStatusMbol(status)).append(sqlStatusOrder(status));
			sql.append(" GROUP BY od.OrderNo");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersShipPalletByLoadNo error:"+e);
		}
	}
	
	/**
	 * shipping label
	 * @param orderNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * ShipFromName,ShipFromAddress1,City,ShipFromAddress2,
	 * ShipToName,ShipToAddress1,ShipToAddress2,
	 * SCACCode,OrderNo,ProNo,ReferenceNo,PONo,BOLNote,OrderedQty,Pallets
	 * @throws Exception
	 */
	public DBRow[] findOrderShipPalletByOrderNo(long orderNo, String customerId, String companyId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select max(ca.ShipFromName)ShipFromName, (max(whs.Address1)+max(whs.Address2)) as ShipFromAddress1, max(whs.City)City, (max(whs.State)+' '+max(whs.ZipCode))ShipFromAddress2");
			sql.append(" , max(od.ShipToName)ShipToName, (max(od.ShipToAddress1)+' '+max(od.ShipToAddress2)) ShipToAddress1, (max(od.ShipToCity)+','+max(od.ShipToState)+' '+max(od.ShipToZipCode)) ShipToAddress2");
			sql.append(" , max(car.SCACCode)SCACCode,max(od.OrderNo)OrderNo,max(od.ProNo)ProNo,max(v.External_ID)ExternalID,max(od.ReferenceNo)ReferenceNo, max(od.PONo)PONo, max(od.BOLNote)BOLNote, sum(odl.OrderedQty)OrderedQty, CAST(CEILING(sum(odl.Pallets)) as int)Pallets");
			sql.append(" from Orders od");
			sql.append(" join OrderLines odl on odl.OrderNo = od.OrderNo and odl.CompanyID = od.CompanyID");
			sql.append(" left join Warehouses whs on whs.CompanyID = od.CompanyID and whs.WarehouseID = od.WarehouseID");
			sql.append(" LEFT JOIN CustomerAccounts ca ON ca.CompanyID = od.CompanyID AND ca.CustomerID = od.CustomerID AND ca.AccountID = od.AccountID");
			sql.append(" left join Carriers car on car.CompanyID = od.CompanyID and car.CarrierID = od.CarrierID");
			sql.append(" left join vizio_extension v on v.vizio_dn = od.ReferenceNo ");
			sql.append(" where od.OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and od.CustomerID = '").append(customerId).append("'");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and od.CompanyID = '").append(companyId).append("'");
			}
			sql.append(sqlStatusOrder(status));
			sql.append(" GROUP BY od.OrderNo");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderShipPalletByLoadNo error:"+e);
		}
	}
	
	
	/**
	 * shipping label
	 * @param orderNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * ShipFromName,ShipFromAddress1,City,ShipFromAddress2,
	 * ShipToName,ShipToAddress1,ShipToAddress2,
	 * SCACCode,OrderNo,ProNo,ReferenceNo,PONo,BOLNote,OrderedQty,Pallets
	 * @throws Exception
	 */
	public DBRow[] findOrderShipPalletByLoadNo(String loadNo, String customerId, String companyId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select max(ca.ShipFromName)ShipFromName, (max(whs.Address1)+max(whs.Address2)) as ShipFromAddress1, max(whs.City)City, (max(whs.State)+' '+max(whs.ZipCode))ShipFromAddress2");
			sql.append(" , max(od.ShipToName)ShipToName, (max(od.ShipToAddress1)+' '+max(od.ShipToAddress2)) ShipToAddress1, (max(od.ShipToCity)+','+max(od.ShipToState)+' '+max(od.ShipToZipCode)) ShipToAddress2");
			sql.append(" , max(car.SCACCode)SCACCode,max(od.OrderNo)OrderNo,max(od.ProNo)ProNo,max(v.External_ID)ExternalID,max(od.ReferenceNo)ReferenceNo, max(od.PONo)PONo, max(od.BOLNote)BOLNote, sum(odl.OrderedQty)OrderedQty, CAST(CEILING(sum(odl.Pallets)) as int)Pallets");
			sql.append(" from Orders od");
			sql.append(" join OrderLines odl on odl.OrderNo = od.OrderNo and odl.CompanyID = od.CompanyID");
			sql.append(" left join Warehouses whs on whs.CompanyID = od.CompanyID and whs.WarehouseID = od.WarehouseID");
			sql.append(" LEFT JOIN CustomerAccounts ca ON ca.CompanyID = od.CompanyID AND ca.CustomerID = od.CustomerID AND ca.AccountID = od.AccountID");
			sql.append(" left join Carriers car on car.CompanyID = od.CompanyID and car.CarrierID = od.CarrierID");
			sql.append(" left join vizio_extension v on v.vizio_dn = od.ReferenceNo ");
			sql.append(" where replace(od.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and od.CustomerID = '").append(customerId).append("'");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and od.CompanyID = '").append(companyId).append("'");
			}
			sql.append(sqlStatusOrder(status));
			sql.append(" GROUP BY od.OrderNo");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderShipPalletByLoadNo error:"+e);
		}
	}
	
	/**
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return 
	 * OrderNo, Pallets,od_status,ml_status,PalletTypeID, MPalletTypeID
	 * @throws Exception
	 */
	public DBRow[] findOrdersPalletByLoadNo(String loadNo, String customerId, String companyId, long masterBolNo, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select * from ( ");
			sql.append(" select od.OrderNo, CAST(CEILING(sum(CEILING(odl.Pallets))) as int)Pallets, max(od.Status)od_status, max(od.StagingAreaID) StagingAreaID");
			sql.append(" , max(mbol.Status)ml_status, max(od.PalletTypeID)PalletTypeID, max(mbol.PalletTypeID)MPalletTypeID, max(mbol.MasterBOLNo)MasterBOLNo");
			sql.append(" , max(mbol.CompanyID) CompanyID, max(mbol.CustomerID) CustomerID, max(mbol.Note)MNote, max(od.Note)Note,max(od.PONo)PONo,max(od.ReferenceNo)ReferenceNo");
			sql.append(" , max(od.AccountID)AccountID, max(od.ShipToID)ShipToID,max(od.ShipToName)ShipToName,max(od.ShipToAddress1)ShipToAddress1");
			sql.append(" , max(od.ShipToAddress2)ShipToAddress2,max(od.ShipToCity)ShipToCity,max(od.ShipToState)ShipToState,max(od.ShipToZipCode)ShipToZipCode");
			sql.append(" , max(od.ShipToCountry)ShipToCountry,max(od.ShipToContact)ShipToContact,max(od.ShipToPhone)ShipToPhone,max(od.ShipToExtension)ShipToExtension");
			sql.append(" , max(od.ShipToFax)ShipToFax,max(od.ShipToStoreNo)ShipToStoreNo,max(od.ProNo)ProNo,max(v.External_ID)ExternalID");
			sql.append(" from MasterBOLs mbol ");
			sql.append(" join MasterBOLLines mboll on mboll.MasterBOLNo = mbol.MasterBOLNo and mboll.CompanyID = mbol.CompanyID");
			sql.append(" join Orders od on od.OrderNo = mboll.OrderNo and od.CompanyID = mboll.CompanyID");
			sql.append(" join OrderLines odl on odl.OrderNo = od.OrderNo and odl.CompanyID = od.CompanyID");
			sql.append(" left join vizio_extension v on v.vizio_dn = od.ReferenceNo ");
			sql.append(" where replace(mbol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(masterBolNo > 0)
			{
				sql.append(" and mbol.MasterBOLNo = ").append(masterBolNo);
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and mbol.CustomerID in (").append(parseCustomerIDStrToInStr(customerId)).append(")");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and mbol.CompanyID = '").append(companyId).append("'");
			}
			sql.append(sqlStatusMbol(status)).append(sqlStatusOrder(status));
			sql.append(" GROUP BY od.OrderNo");
			sql.append(" ) a ORDER BY a.Pallets DESC");
//			System.out.println("sq:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersShipPalletByLoadNo error:"+e);
		}
	}
	
	/**
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return 
	 * OrderNo, Pallets,od_status,ml_status,PalletTypeID, MPalletTypeID
	 * @throws Exception
	 */
	public DBRow[] findOrderPalletByLoadNo(String loadNo, String customerId, String companyId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select * from ( ");
			sql.append(" select od.OrderNo, CAST(CEILING(sum(CEILING(odl.Pallets))) as int)Pallets, max(od.Status)od_status, max(od.StagingAreaID) StagingAreaID");
			sql.append(" , max(od.Status)ml_status,max(od.PalletTypeID)PalletTypeID, max(od.PalletTypeID)MPalletTypeID, 0 as MasterBOLNo");
			sql.append(" , max(od.CompanyID) CompanyID, max(od.CustomerID) CustomerID, max(od.Note)Note,max(od.PONo)PONo,max(od.ReferenceNo)ReferenceNo");
			sql.append(" , max(od.AccountID)AccountID, max(od.ShipToID)ShipToID,max(od.ShipToName)ShipToName,max(od.ShipToAddress1)ShipToAddress1");
			sql.append(" , max(od.ShipToAddress2)ShipToAddress2,max(od.ShipToCity)ShipToCity,max(od.ShipToState)ShipToState,max(od.ShipToZipCode)ShipToZipCode");
			sql.append(" , max(od.ShipToCountry)ShipToCountry,max(od.ShipToContact)ShipToContact,max(od.ShipToPhone)ShipToPhone,max(od.ShipToExtension)ShipToExtension");
			sql.append(" , max(od.ShipToFax)ShipToFax,max(od.ShipToStoreNo)ShipToStoreNo,max(od.ProNo)ProNo,max(v.External_ID)ExternalID");
			sql.append(" from Orders od left join OrderLines odl on od.OrderNo = odl.OrderNo and od.CompanyID = odl.CompanyID");
			sql.append(" left join vizio_extension v on v.vizio_dn = od.ReferenceNo ");
			sql.append(" where replace(od.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and od.CustomerID in (").append(parseCustomerIDStrToInStr(customerId)).append(")");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and od.CompanyID = '").append(companyId).append("'");
			}
			sql.append(sqlStatusOrder(status));
			sql.append(" GROUP BY od.OrderNo");
			sql.append(" ) a ORDER BY a.Pallets DESC");
//			System.out.println("sq:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderShipPalletByLoadNo error:"+e);
		}
	}
	
	/**
	 * 更新order的托盘数量
	 * @param row
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @throws Exception
	 */
	public void updateOrderLines(DBRow row, long orderNo, String companyId)throws Exception
	{
		try
		{
			String where = " WHERE OrderNo = "+orderNo;
			if(!StrUtil.isBlank(companyId))
			{
				where += " and CompanyID = '"+companyId+"'";
			}
			dbUtilAutoTranSQLServer.update(where, "OrderLines", row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.updateOrder error:"+e);
		}
	}
	
	
	public DBRow[] findOrderLinesByOrderNo(long orderNo, String customerId, String companyId, int pallets)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select ItemID,CAST(CEILING(Pallets) as int) intPallets , Pallets,[LineNo] from OrderLines");
			sql.append(" where OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and CompanyID = '").append(companyId).append("'");
			}
			if(pallets > 0)
			{
				sql.append(" and Pallets > ").append(pallets);
			}
//			System.out.println("findP:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderLinesByOrderNo error:"+e);
		}
	}
	
	
	public DBRow[] findOrderNosByLoadNo(String loadNo, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select boll.OrderNo");
			sql.append(" from MasterBOLs bol ");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID");
			sql.append(" where replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND bol.CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND bol.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatusBol(status));
			sql.append(sqlStatusOrder(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderNosByLoadNo error:"+e);
		}
	}
	
	public DBRow[] findOrderNosSingleByLoadNo(String loadNo, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select OrderNo");
			sql.append(" from Orders ");
			sql.append(" where replace(LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatusOrder(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderNosByLoadNo error:"+e);
		}
	}
	
	/**
	 * 修改order某个pallets
	 * @param orderNo
	 * @param customerId
	 * @param companyId
	 * @param itemId
	 * @param lineNo
	 * @param palletsDiff
	 * @throws Exception
	 */
	public void updateOrderLinePallets(long orderNo, String customerId, String companyId, String itemId, int lineNo, float palletsDiff) throws Exception
	{
		try
		{
			StringBuffer where = new StringBuffer();
			where.append(" where OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(companyId))
			{
				where.append(" AND CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(itemId))
			{
				where.append(" and ItemID = '").append(itemId).append("'");
			}
			if(lineNo > 0)
			{
				where.append(" and [LineNo] = ").append(lineNo);
			}
//			System.out.println("where:"+where);
			dbUtilAutoTranSQLServer.addSubFieldVal("OrderLines", where.toString(), "Pallets", palletsDiff);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderNosByLoadNo error:"+e);
		}
	}
	
	/**
	 * 修改order某个pallets
	 * @param orderNo
	 * @param customerId
	 * @param companyId
	 * @param itemId
	 * @param lineNo
	 * @param palletsDiff
	 * @throws Exception
	 */
	public DBRow[] findOrderByOrderNo(long orderNo, String customerId, String companyId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from Orders where OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND CustomerID = '").append(customerId).append("'");
			}
//			System.out.println("orders:"+sql.toString());
			sql.append(sqlStatus(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderByOrderNo error:"+e);
		}
	}
	
	public DBRow[] findOrderInfoByMasterBol(long mbol, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select bol.MasterBOLNo, boll.OrderNo, od.StagingAreaID, od.PickingType, od.CustomerID, od.CompanyID, od.PalletTypeID , od.PONo");
			sql.append(", od.AccountID, od.ShipToID, od.Status");
			sql.append(" from MasterBOLs bol ");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND bol.CompanyID = od.CompanyID ");
			sql.append(" WHERE bol.MasterBOLNo = ").append(mbol);
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND bol.CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND bol.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatusBol(status));
			sql.append(" ORDER BY boll.OrderNo");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderInfoByMasterBol error:"+e);
		}
	}
	
	
	/**
	 * 通过LoadNo查询MasterBols
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolsSomeByLoadNoForTicket(String loadNo,String CompanyID,String CustomerID, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select LoadNo, MasterBOLNo,0 as OrderNo, CompanyID");
			sql.append(" , CustomerID, ShipToID, AccountID, PalletTypeID, Status, ProNo");
			sql.append(" from MasterBOLs where replace(LoadNo,' ', '') = '"+loadNo.replace(" ", "")+"'");
			if(!StrUtil.isBlank(CompanyID))
			{
				sql.append(" AND CompanyID = '").append(CompanyID).append("'");
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql.append(" AND CustomerID in (").append(parseCustomerIDStrToInStr(CustomerID)).append(")");
			}
//			sql.append(" GROUP BY AccountID");
			sql.append(sqlStatus(status));
//			System.out.println("sql4:"+sql.toString()+"------------"+CustomerID);
			if(!StrUtil.isBlank(loadNo))
			{
				return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
			}
			return new DBRow[0];
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolsSomeByLoadNoForTicket error:"+e);
		}
	}
	
	/**
	 * 通过LoadNo查询Order主子表相关数据
	 * @param masterBOLNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersSomeInfoWmsByLoadNoForTicket(String loadNo,String CompanyID,String CustomerID, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT LoadNo,0 as MasterBOLNo,OrderNo,CompanyID,CustomerID,ShipToID,AccountID,PalletTypeID,StagingAreaID,PickingType,PONo,Status");
			sql.append(" FROM Orders");
			sql.append(" WHERE replace(LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			if(!StrUtil.isBlank(CompanyID))
			{
				sql.append(" AND CompanyID = '").append(CompanyID).append("'");
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql.append(" AND CustomerID = '").append(CustomerID).append("'");
			}
			sql.append(sqlStatus(status));
			sql.append(" ORDER BY OrderNo");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersSomeInfoWmsByLoadNoForTicket error:"+e);
		}
	}
	
	/**
	 * 通过仓库获取托盘类型
	 * @param CompanyID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPalletTypesByCompanyID(String CompanyID) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT CompanyID, PalletTypeID, replace(PalletTypeName,' ', '')PalletTypeName FROM PalletTypes WHERE 1=1");
			if(!StrUtil.isBlank(CompanyID))
			{
				sql.append(" AND CompanyID = '").append(CompanyID).append("'");
			}
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findPalletTypesByCompanyID error:"+e);
		}
	}
		

	/**
	 * 通过LoadNo查询Order主子表相关数据
	 * @param masterBOLNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersSomeInfoWmsByOrderNoForTicket(long orderNo,String CompanyID,String CustomerID, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT LoadNo,0 as MasterBOLNo,OrderNo,CompanyID,CustomerID,ShipToID,AccountID,PalletTypeID,StagingAreaID,PickingType,PONo,Status,ProNo");
			sql.append(" FROM Orders");
			sql.append(" WHERE OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(CompanyID))
			{
				sql.append(" AND CompanyID = '").append(CompanyID).append("'");
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql.append(" AND CustomerID = '").append(CustomerID).append("'");
			}
			sql.append(sqlStatus(status));
			sql.append(" ORDER BY OrderNo");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersSomeInfoWmsByOrderNoForTicket error:"+e);
		}
	}



	/**
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return 
	 * OrderNo, Pallets,od_status,ml_status,PalletTypeID, MPalletTypeID
	 * @throws Exception
	 */
	public DBRow[] findOrderPalletByOrderNo(long orderNo, String customerId, String companyId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select * from ( ");
			sql.append(" select od.OrderNo, CAST(CEILING(sum(CEILING(odl.Pallets))) as int)Pallets, max(od.Status)od_status, max(od.StagingAreaID) StagingAreaID");
			sql.append(" , max(od.Status)ml_status,max(od.PalletTypeID)PalletTypeID, max(od.PalletTypeID)MPalletTypeID, 0 as MasterBOLNo");
			sql.append(" , max(od.CompanyID) CompanyID, max(od.CustomerID) CustomerID, max(od.Note) Note");
			sql.append(" , max(od.AccountID)AccountID, max(od.ShipToID)ShipToID,max(od.ShipToName)ShipToName,max(od.ShipToAddress1)ShipToAddress1");
			sql.append(" , max(od.ShipToAddress2)ShipToAddress2,max(od.ShipToCity)ShipToCity,max(od.ShipToState)ShipToState,max(od.ShipToZipCode)ShipToZipCode");
			sql.append(" , max(od.ShipToCountry)ShipToCountry,max(od.ShipToContact)ShipToContact,max(od.ShipToPhone)ShipToPhone,max(od.ShipToExtension)ShipToExtension");
			sql.append(" , max(od.ShipToFax)ShipToFax,max(od.ShipToStoreNo)ShipToStoreNo,max(od.ProNo)ProNo,max(v.External_ID)ExternalID");
			sql.append(" from Orders od join OrderLines odl on od.OrderNo = odl.OrderNo and od.CompanyID = odl.CompanyID");
			sql.append(" left join vizio_extension v on v.vizio_dn = od.ReferenceNo ");
			sql.append(" where od.OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and od.CustomerID = '").append(customerId).append("'");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and od.CompanyID = '").append(companyId).append("'");
			}
			sql.append(sqlStatusOrder(status));
			sql.append(" GROUP BY od.OrderNo");
			sql.append(" ) a ORDER BY a.Pallets DESC");
//			System.out.println("sq:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderShipPalletByLoadNo error:"+e);
		}
	}

	/**
	 * 通过LoadNo查询MasterBols
	 * @param loadNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolsSomeByMasterBolNoForTicket(long masterBolNo,String CompanyID,String CustomerID, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select LoadNo, MasterBOLNo,0 as OrderNo, CompanyID");
			sql.append(" , CustomerID, ShipToID, AccountID, PalletTypeID,Status,PalletTypeID, ProNo");
			sql.append(" from MasterBOLs where MasterBOLNo = ").append(masterBolNo);
			if(!StrUtil.isBlank(CompanyID))
			{
				sql.append(" AND CompanyID = '").append(CompanyID).append("'");
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql.append(" AND CustomerID in (").append(parseCustomerIDStrToInStr(CustomerID)).append(")");
			}
			sql.append(sqlStatus(status));
//			sql.append(" GROUP BY AccountID");
//			System.out.println("sq5:"+sql.toString()+"---------------------"+CustomerID);
			if(masterBolNo > 0)
			{
				return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
			}
			return new DBRow[0];
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolsSomeByLoadNoForTicket error:"+e);
		}
	}
	
	
	
	/**
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return 
	 * OrderNo, Pallets,od_status,ml_status,PalletTypeID, MPalletTypeID
	 * @throws Exception
	 */
	public DBRow[] findOrdersPalletByMasterBolNo(String customerId, String companyId, long masterBolNo, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select * from ( ");
			sql.append(" select od.OrderNo, CAST(CEILING(sum(CEILING(odl.Pallets))) as int)Pallets, max(od.Status)od_status, max(od.StagingAreaID) StagingAreaID");
			sql.append(" , max(mbol.Status)ml_status, max(od.PalletTypeID)PalletTypeID, max(mbol.PalletTypeID)MPalletTypeID, max(mbol.MasterBOLNo)MasterBOLNo");
			sql.append(" , max(mbol.CompanyID) CompanyID, max(mbol.CustomerID) CustomerID");
			sql.append(" , max(od.AccountID)AccountID, max(od.ShipToID)ShipToID,max(od.ShipToName)ShipToName,max(od.ShipToAddress1)ShipToAddress1");
			sql.append(" , max(od.ShipToAddress2)ShipToAddress2,max(od.ShipToCity)ShipToCity,max(od.ShipToState)ShipToState,max(od.ShipToZipCode)ShipToZipCode");
			sql.append(" , max(od.ShipToCountry)ShipToCountry,max(od.ShipToContact)ShipToContact,max(od.ShipToPhone)ShipToPhone,max(od.ShipToExtension)ShipToExtension");
			sql.append(" , max(od.ShipToFax)ShipToFax,max(od.ShipToStoreNo)ShipToStoreNo,max(od.ProNo)ProNo,max(od.External_ID)ExternalID");
			sql.append(" from MasterBOLs mbol ");
			sql.append(" join MasterBOLLines mboll on mboll.MasterBOLNo = mbol.MasterBOLNo and mboll.CompanyID = mbol.CompanyID");
			sql.append(" join Orders od on od.OrderNo = mboll.OrderNo and od.CompanyID = mboll.CompanyID");
			sql.append(" join OrderLines odl on odl.OrderNo = od.OrderNo and odl.CompanyID = od.CompanyID");
			sql.append(" left join vizio_extension v on v.vizio_dn = od.ReferenceNo ");
			sql.append(" where 1=1");
			if(masterBolNo > 0)
			{
				sql.append(" and mbol.MasterBOLNo = ").append(masterBolNo);
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and mbol.CustomerID in (").append(parseCustomerIDStrToInStr(customerId)).append(")");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and mbol.CompanyID = '").append(companyId).append("'");
			}
			sql.append(sqlStatusMbol(status));
			sql.append(" GROUP BY od.OrderNo");
			sql.append(" ) a ORDER BY a.Pallets DESC");
//			System.out.println("sq:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersShipPalletByLoadNo error:"+e);
		}
	}



	/**
	 * 通过 MasterBolNo获取LoadNo和MasterBOLNo
	 * @param orderNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolOrLoadByMasterBolNo(long MasterBolNo, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select mbol.LoadNo,mbol.MasterBOLNo, mbol.CompanyID, mbol.CustomerID, mbol.AccountID");
			sql.append(" from MasterBOLs mbol");
			sql.append(" where mbol.MasterBOLNo = ").append(MasterBolNo);
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and mbol.CustomerID = '").append(customerId).append("'");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and mbol.CompanyID = '").append(companyId).append("'");
			}
			sql.append(sqlStatusMbol(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolOrLoadByMasterBolNo error:"+e);
		}
	}
	
	

	/**
	 * 通过 OrderNo获取LoadNo和MasterBOLNo
	 * @param orderNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findMasterBolOrLoadByOrderNo(long orderNo, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select mbol.LoadNo,mbol.MasterBOLNo, mbol.CompanyID, mbol.CustomerID, mbol.AccountID  from MasterBOLs mbol ");
			sql.append("  join MasterBOLLines mboll on mboll.MasterBOLNo = mbol.MasterBOLNo and mboll.CompanyID = mbol.CompanyID");
			sql.append("  join Orders od on od.OrderNo = mboll.OrderNo and od.CompanyID = mboll.CompanyID");
			sql.append(" where od.OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and od.CustomerID = '").append(customerId).append("'");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and od.CompanyID = '").append(companyId).append("'");
			}
			sql.append(sqlStatusMbol(status));
			sql.append(sqlStatusOrder(status));
			sql.append(" UNION ");
			sql.append(" select od.LoadNo, 0 as MasterBOLNo, od.CompanyID, od.CustomerID, od.AccountID from Orders od");
			sql.append(" where od.OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and od.CustomerID = '").append(customerId).append("'");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and od.CompanyID = '").append(companyId).append("'");
			}
			sql.append(sqlStatusOrder(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolOrLoadByOrderNo error:"+e);
		}
	}
	
	
	
	public DBRow[] findOrderLinesByOrderNoItem(long orderNo, String itemId, int line_no, String companyId, String customerId)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select ItemID,CAST(CEILING(Pallets) as int) intPallets , Pallets,[LineNo] from OrderLines");
			sql.append(" where OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(itemId))
			{
				sql.append(" and ItemID = '").append(itemId).append("'");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and CompanyID = '").append(companyId).append("'");
			}
			if(line_no > 0)
			{
				sql.append(" and [LineNo] = ").append(line_no);
			}
//			System.out.println("findP:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderLinesByOrderNoItem error:"+e);
		}
	}
	
	public String sqlCompanyId(String[] companyId)
	{
		String sql = "";
		if(null != companyId && companyId.length > 0)
		{
			sql += " and (CompanyID = '" + companyId[0]+"'";
			for (int i = 1; i < companyId.length; i++)
			{
				sql += " or CompanyID = '" + companyId[i]+"'";
			}
			sql += ")";
		}
		return sql;
	}
	
	public String sqlCompanyIdOrder(String[] companyId)
	{
		String sql = "";
		if(null != companyId && companyId.length > 0)
		{
			sql += " and (od.CompanyID = '" + companyId[0]+"'";
			for (int i = 1; i < companyId.length; i++)
			{
				sql += " or od.CompanyID = '" + companyId[i]+"'";
			}
			sql += ")";
		}
		return sql;
	}
	
	public String sqlCompanyIdMbol(String[] companyId)
	{
		String sql = "";
		if(null != companyId && companyId.length > 0)
		{
			sql += " and (mbol.CompanyID = '" + companyId[0]+"'";
			for (int i = 1; i < companyId.length; i++)
			{
				sql += " or mbol.CompanyID = '" + companyId[i]+"'";
			}
			sql += ")";
		}
		return sql;
	}
	
	public String sqlCompanyIdBol(String[] companyId)
	{
		String sql = "";
		if(null != companyId && companyId.length > 0)
		{
			sql += " and (bol.CompanyID = '" + companyId[0]+"'";
			for (int i = 1; i < companyId.length; i++)
			{
				sql += " or bol.CompanyID = '" + companyId[i]+"'";
			}
			sql += ")";
		}
		return sql;
	}
	
	/**
	 * 验证单据类型
	 * @param orderNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkOrderInfoByOrderNo(long orderNo, String[] status, String[] companyId)throws Exception
	{
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT od.LoadNo, od.StagingAreaID,od.CarrierID, od.AppointmentDate, od.ReferenceNo");
		sql.append(" , od.CompanyID, od.CustomerID, od.AccountID, od.OrderNo number, od.PONo, od.FreightTerm");
		sql.append(", od.ShipToID, od.ShipToName");
		sql.append(",").append(ModuleKey.CHECK_IN_ORDER).append(" as order_type");
		sql.append(" from Orders od");
		sql.append("  where od.OrderNo = ").append(orderNo);
		sql.append(sqlStatusOrder(status));
		sql.append(sqlCompanyIdOrder(companyId));
//		System.out.println("sql1:"+sql.toString());
		return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
	}
	
	public DBRow[] checkOrderInfoByPoNo(String pono, String[] status, String[] companyId)throws Exception
	{
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT od.LoadNo, od.StagingAreaID,od.CarrierID, od.AppointmentDate, od.ReferenceNo");
		sql.append(" , od.CompanyID, od.CustomerID, od.AccountID, od.OrderNo, od.PONo as number, od.FreightTerm");
		sql.append(", od.ShipToID, od.ShipToName");
		sql.append(",").append(ModuleKey.CHECK_IN_PONO).append(" as order_type");
		sql.append(" from Orders od ");
		sql.append(" where od.PONo = '").append(pono).append("'");
		sql.append(sqlStatusOrder(status));
		sql.append(sqlCompanyIdOrder(companyId));
//		System.out.println("sql2:"+sql.toString());
		return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
	}
	
	
	public DBRow[] checkOrdersByLoadNo(String loadNo, String[] status, String[] companyId)throws Exception
	{
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT  od.StagingAreaID,od.CarrierID, od.AppointmentDate, od.ReferenceNo");
		sql.append(" , od.CompanyID, od.CustomerID, od.AccountID,od.LoadNo  as number, od.OrderNo, od.PONo, od.FreightTerm");
		sql.append(", od.ShipToID, od.ShipToName");
		sql.append(",").append(ModuleKey.CHECK_IN_LOAD).append(" as order_type");
		sql.append(" from Orders od ");
		sql.append(" where replace(od.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
		sql.append(sqlStatusOrder(status));
		sql.append(sqlCompanyIdOrder(companyId));
//		System.out.println("sql4:"+sql.toString());
		return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
	}
	
	

	/**
	 * 通过load查询bill of lading模版
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findBillOfLadingTemplateByOrderNo(long orderNo, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT od.LoadNo, od.CompanyID,od.CustomerID,od.AccountID,ca.BOLFormat,ca.MasterBOLFormat,od.ShipToID, od.ShipToName,od.cn");
			sql.append("  FROM (SELECT *");
			sql.append("  FROM (SELECT COUNT (*) cn");
			sql.append(" ,MAX(LoadNo)LoadNo,MAX(CompanyID)CompanyID ,MAX(AccountID)AccountID");
			sql.append(" ,MAX(CustomerID)CustomerID,MAX(ShipToID)ShipToID,MAX(ShipToName)ShipToName,MAX(Status)Statusm,OrderNo");
			sql.append("  FROM Orders ");
			sql.append("  WHERE OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatus(status));
			sql.append(" GROUP BY  OrderNo, CompanyID, CustomerID, ShipToID, ShipToName");
			sql.append(" ) a) od");
			sql.append(" LEFT JOIN CustomerAccounts ca ON od.CustomerID = ca.CustomerID AND od.AccountID = ca.AccountID");
			sql.append(" WHERE od.OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND od.CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND od.CustomerID = '").append(customerId).append("'");
			}
//			sql.append(sqlStatusOrder(status));
//			System.out.println("sqlNoM:"+sql.toString());
//			System.out.println("sql5:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findBillOfLadingTemplateByLoadNoMaster error:"+e);
		}
	}
	
	/**
	 * 通过masterbolno数组查询order bolnote
	 * @param masterBols
	 * @param companyID
	 * @param customerID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersNoteByMasterBolNos(Long[] masterBols, String companyID, String customerID) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select od.BOLNote, od.OrderNo");
			sql.append(" from MasterBOLs mbol ");
			sql.append(" join MasterBOLLines mboll on mbol.MasterBOLNo = mboll.MasterBOLNo and mbol.CompanyID = mboll.CompanyID");
			sql.append(" join Orders od on mboll.OrderNo = od.OrderNo and mboll.CompanyID = od.CompanyID");
			sql.append(" where 1=1");
			if(masterBols.length > 0)
			{
				sql.append(" and (mbol.MasterBOLNo = ").append(masterBols[0]);
				for (int i = 1; i < masterBols.length; i++) 
				{
					sql.append(" or mbol.MasterBOLNo = ").append(masterBols[i]);
				}
				sql.append(")");
			}
			sql.append(" and od.BOLNote is not null and od.BOLNote != ''");
			if(!StrUtil.isBlank(companyID))
			{
				sql.append(" and mbol.CompanyID = '").append(companyID).append("'");
			}
			if(!StrUtil.isBlank(customerID))
			{
				sql.append(" and mbol.CustomerID = '").append(customerID).append("'");
			}
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersNoteByMasterBolNos error:"+e);
		}
	}

	/**
	 * 通过LoadNo查询Order主子表相关数据
	 * @param masterBOLNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersSomeInfoWmsByOrderNo(long OrderNo,String CompanyID, String CustomerID, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT od.CustomerID, whs.Address1, whs.Address2, whs.City, whs.State, whs.ZipCode, whs.Phone, whs.Contact, od.CompanyID,");
			sql.append(" od.ShipToName, od.ShipToAddress1, od.ShipToAddress2, od.ShipToCity, od.ShipToState, od.ShipToZipCode, od.ShipToID, od.ShipToPhone,");
			sql.append(" od.BillToName, od.BillToAddress1, od.BillToAddress2, od.BillToCity, od.BillToState, od.BillToZipCode, od.Note,");
			sql.append(" od.LoadNo, od.AppointmentDate, od.ProNo, v.External_ID as ExternalID, od.FreightTerm, od.PalletTypeID, od.PickingType, od.OrderNo,od.StagingAreaID, od.PONo");
			sql.append(" FROM Orders od");
			sql.append(" LEFT JOIN Warehouses whs ON whs.CompanyID = od.CompanyID AND whs.WarehouseID = od.WarehouseID");
			sql.append(" left join vizio_extension v on v.vizio_dn = od.ReferenceNo ");
			sql.append(" WHERE od.OrderNo = ").append(OrderNo);
			if(!StrUtil.isBlank(CompanyID))
			{
				sql.append(" and od.CompanyID = '").append(CompanyID).append("'");
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql.append(" and od.CustomerID = '").append(CustomerID).append("'");
			}
			sql.append(sqlStatusOrder(status));
			sql.append(" ORDER BY od.OrderNo");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersSomeInfoWmsByOrderNo error:"+e);
		}
	}
	
	
	/**
	 * 通过LoadNo查询Order主子表相关数据
	 * @param OrderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrdersItemsWmsByOrderNoForTicket(long OrderNo, String companyId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select odl.OrderNo, odl.ItemID, odl.LotNo,odl.OrderedQty, odl.[LineNo], CAST(CEILING(CEILING(odl.OrderedQty)) as int) OrderedQtyInt ");
			sql.append(" , Pallets,CAST(CEILING(CEILING(odl.Pallets)) as int) PalletsInt, odlp.PlateNo, odlp.[LineNo]");
			sql.append(",CASE WHEN(odlp.PlateNo is null or odlp.PlateNo = '') then odl.OrderedQty else odlp.ShippedQty end as ShippedQty");
			sql.append(" FROM OrderLines odl");
			sql.append(" LEFT JOIN OrderLinePlates odlp ON odl.[LineNo]=odlp.[LineNo] AND odl.OrderNo = odlp.OrderNo AND odl.CompanyID = odlp.CompanyID ");
			sql.append(" WHERE odl.OrderNo = ").append(OrderNo);
			sql.append(" AND odl.CompanyID = '").append(companyId).append("'");
			sql.append(" ORDER BY odl.OrderNo, odlp.[LineNo], odl.ItemID, odlp.PlateNo");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersItemsWmsByOrderNo error:"+e);
		}
	}
	/**
	 * 收费报表
	 * @param startTime
	 * @param endTime
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCustomerServices(String startTime,String endTime,String companyId,String customerId) throws Exception{
		try{
			StringBuffer sql = new StringBuffer("select * from CustomerServices where 1=1");
			if(!companyId.equals("")){
				sql.append(" AND CompanyID='"+companyId+"'");
			}
			if(!customerId.equals("")){
				sql.append(" AND CustomerID='"+customerId+"'");
			}
			if(!startTime.equals("")){
				sql.append(" AND DateCreated > '"+startTime+"'");
			}
			if(!endTime.equals("")){
				sql.append(" AND DateCreated < '"+endTime+"'");
			}
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.getCustomerServices error:"+e);
		}
	}
	
	/**
	 * 通过OrderNo查询load部分信息
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadSomeInfoByOrderNo(long orderNo, String[] status, String[] companyId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select mbol.CompanyID, mbol.LoadNo, max(mbol.CustomerID)CustomerID, max(mbol.MasterBOLNo)MasterBOLNo");
			sql.append(" , max(mbol.AccountID)AccountID, max(mbol.AppointmentDate)AppointmentDate,max(mbol.Status)Status,mbol.LoadNo number,'' StagingAreaID");
			sql.append(",").append(ModuleKey.CHECK_IN_LOAD).append(" as order_type");
			sql.append("  from  MasterBOLs mbol");
			sql.append(" join MasterBOLLines mboll on mboll.MasterBOLNo = mbol.MasterBOLNo and mboll.CompanyID = mbol.CompanyID");
			sql.append(" where mboll.OrderNo = ").append(orderNo);
			sql.append(sqlCompanyIdMbol(companyId));
			sql.append(sqlStatusMbol(status));
			sql.append(" GROUP BY mbol.LoadNo, mbol.CompanyID");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadSomeInfoByOrderNo error:"+e);
		}
	}
	
	/**
	 * 通过OrderNo查询load部分信息
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadSomeInfoByPoNoOrOrderNo(String orders, String[] status, String[] companyId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select mbol.CompanyID, mbol.LoadNo, max(mbol.CustomerID)CustomerID, max(mbol.MasterBOLNo)MasterBOLNo");
			sql.append(" , max(mbol.AccountID)AccountID, max(mbol.AppointmentDate)AppointmentDate,max(mbol.Status)Status,mbol.LoadNo number,'' StagingAreaID, max(mbol.FreightTerm)FreightTerm");
			sql.append(",").append(ModuleKey.CHECK_IN_LOAD).append(" as order_type");
			sql.append("  from  MasterBOLs mbol");
			sql.append(" join MasterBOLLines mboll on mboll.MasterBOLNo = mbol.MasterBOLNo and mboll.CompanyID = mbol.CompanyID");
			sql.append(" join Orders od on od.OrderNo = mboll.OrderNo and od.CompanyID = mboll.CompanyID");
			sql.append(" where od.PONo = '").append(orders).append("'");
			sql.append(sqlCompanyIdMbol(companyId));
			sql.append(sqlStatusMbol(status));
			sql.append(sqlStatusOrder(status));
			sql.append(" GROUP BY mbol.LoadNo, mbol.CompanyID");
			if(!StrUtil.isBlankAndCanParseLong(orders))
			{
				sql.append(" UNION");
				sql.append(" select mbol.CompanyID, mbol.LoadNo, max(mbol.CustomerID)CustomerID, max(mbol.MasterBOLNo)MasterBOLNo");
				sql.append(" , max(mbol.AccountID)AccountID, max(mbol.AppointmentDate)AppointmentDate,max(mbol.Status)Status,mbol.LoadNo number,'' StagingAreaID, max(mbol.FreightTerm)FreightTerm");
				sql.append(",").append(ModuleKey.CHECK_IN_LOAD).append(" as order_type");
				sql.append("  from  MasterBOLs mbol");
				sql.append(" join MasterBOLLines mboll on mboll.MasterBOLNo = mbol.MasterBOLNo and mboll.CompanyID = mbol.CompanyID");
				sql.append("  join Orders od on od.OrderNo = mboll.OrderNo and od.CompanyID = mboll.CompanyID");
				sql.append(" where od.OrderNo = ").append(Long.parseLong(orders));
				sql.append(sqlCompanyIdMbol(companyId));
				sql.append(sqlStatusMbol(status));
				sql.append(sqlStatusOrder(status));
				sql.append(" GROUP BY mbol.LoadNo, mbol.CompanyID");
			}
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadSomeInfoByPoNo error:"+e);
		}
	}
	
	public DBRow[] findLoadSomeInfoByPoNo(String poNo, String[] status, String[] companyId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select mbol.CompanyID, mbol.LoadNo, max(mbol.CustomerID)CustomerID, max(mbol.MasterBOLNo)MasterBOLNo");
			sql.append(" , max(mbol.AccountID)AccountID, max(mbol.AppointmentDate)AppointmentDate,max(mbol.Status)Status,mbol.LoadNo number,'' StagingAreaID, max(mbol.FreightTerm)FreightTerm");
			sql.append(",").append(ModuleKey.CHECK_IN_LOAD).append(" as order_type");
			sql.append("  from  MasterBOLs mbol");
			sql.append(" join MasterBOLLines mboll on mboll.MasterBOLNo = mbol.MasterBOLNo and mboll.CompanyID = mbol.CompanyID");
			sql.append(" join Orders od on od.OrderNo = mboll.OrderNo and od.CompanyID = mboll.CompanyID");
			sql.append(" where od.PONo = '").append(poNo).append("'");
			sql.append(sqlCompanyIdMbol(companyId));
			sql.append(sqlStatusMbol(status));
			sql.append(sqlStatusOrder(status));
			sql.append(" GROUP BY mbol.LoadNo, mbol.CompanyID");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadSomeInfoByPoNo error:"+e);
		}
	}
	
	/**
	 * 通过OrderNo查询load部分信息
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadSomeInfoByLoadNo(String loadNo, String[] status, String[] companyId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select mbol.CompanyID, mbol.LoadNo, max(mbol.CustomerID)CustomerID, max(mbol.MasterBOLNo)MasterBOLNo, max(mbol.AccountID)AccountID");
//			sql.append("select mbol.CompanyID, mbol.LoadNo,  mbol.CustomerID , mbol.AccountID , max(mbol.MasterBOLNo)MasterBOLNo");
			sql.append(" , max(mbol.AppointmentDate)AppointmentDate,max(mbol.Status)Status,mbol.LoadNo number");
			sql.append(",'' StagingAreaID, max(mbol.FreightTerm)FreightTerm, max(mbol.ShipToID)ShipToID, max(mbol.ShipToName)ShipToName");
			sql.append(",").append(ModuleKey.CHECK_IN_LOAD).append(" as order_type");
			sql.append("  from  MasterBOLs mbol");
			sql.append(" join MasterBOLLines mboll on mboll.MasterBOLNo = mbol.MasterBOLNo and mboll.CompanyID = mbol.CompanyID");
			sql.append(" where mbol.LoadNo = '").append(loadNo).append("'");
			sql.append(sqlCompanyIdMbol(companyId));
			sql.append(sqlStatusMbol(status));
			sql.append(" GROUP BY mbol.LoadNo, mbol.CompanyID");
//			sql.append(" GROUP BY mbol.LoadNo, mbol.CompanyID,mbol.CustomerID,mbol.AccountID");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadSomeInfoByLoadNo error:"+e);
		}
	}
	
	/**
	 * 通过load查询load的staging
	 * @param loadNo
	 * @param status
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] checkMasterInfosByLoadNo(String loadNo, String[] status, String companyId, String customerId)throws Exception
	{
		StringBuffer sql = new StringBuffer();
		sql.append(" select top 1 * from (");
		sql.append(" SELECT  MAX(od.OrderNo) OrderNo, SUM(odl.Pallets) Pallets,od.StagingAreaID, MAX(od.CarrierID)CarrierID, MAX(mbol.AppointmentDate)AppointmentDate");
		sql.append(" , MAX(mbol.CompanyID)CompanyID , MAX(mbol.CustomerID)CustomerID, MAX(mbol.AccountID)AccountID, MAX(mbol.LoadNo) number, MAX(mbol.LoadNo)LoadNo");
		sql.append(", MAX(mbol.MasterBOLNo)MasterBOLNo, max(mbol.FreightTerm)FreightTerm, max(mbol.ShipToID)ShipToID, max(mbol.ShipToName)ShipToName,max(mbol.Status)Status");
		sql.append(",").append(ModuleKey.CHECK_IN_LOAD).append(" as order_type");
		sql.append("  FROM MasterBOLs mbol ");
		sql.append("  JOIN MasterBOLLines mboll ON mbol.MasterBOLNo = mboll.MasterBOLNo AND mbol.CompanyID = mboll.CompanyID");
		sql.append("  JOIN Orders od ON mboll.OrderNo = od.OrderNo AND mboll.CompanyID = od.CompanyID");
		sql.append("  JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID");
		sql.append(" where replace(mbol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
		if(!StrUtil.isBlank(companyId))
		{
			sql.append(" and mbol.CompanyID = '").append(companyId).append("'");
		}
		if(!StrUtil.isBlank(customerId))
		{
			sql.append(" and mbol.CustomerID = '").append(customerId).append("'");
		}
		sql.append(sqlStatusMbol(status));
//		sql.append(sqlStatusOrder(status));
		sql.append(" GROUP BY od.StagingAreaID) a ORDER BY a.Pallets desc");
//		System.out.println("sql3:"+sql.toString());
		return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
	}
	
	/**
	 * 查预约到达时间为某天的Orders, pos
	 * @param time
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderNoAppointTimeBetweenTime(String startTime, String endTime, String[] status, String[] companyId) throws Exception
	{
		try
		{
			String sql = "select * from (";
			sql += " SELECT od.OrderNo, od.PONo, od.AccountID,od.AppointmentDate, od.CompanyID, od.CustomerID, od.CarrierID, odl.SupplierID, '' SupplierIDs, od.ReferenceNo,od.Status";
			sql += " FROM Orders od ";
			sql += " JOIN OrderLines odl on odl.OrderNo = od.OrderNo and odl.CompanyID = od.CompanyID";
			sql += " LEFT JOIN MasterBOLLines mboll on mboll.OrderNo = od.OrderNo and mboll.CompanyID = od.CompanyID";
			sql += " where 1=1";
			if(!StrUtil.isBlank(startTime))
			{
				sql += " and Convert(varchar(10),od.AppointmentDate,120) >= '" + startTime + "'";
			}
			if(!StrUtil.isBlank(endTime))
			{
				sql += " AND Convert(varchar(10),od.AppointmentDate,120) <= '" + endTime + "'";

			}
//			sql += " and (od.OrderNo = 75111 or od.OrderNo = 146180)";
//			sql += sqlStatusOrder(status);
			sql += sqlCompanyIdOrder(companyId);
			sql += " and mboll.MasterBOLNo is null";
			sql += "  )a ";
			sql += " GROUP BY a.PONo, a.OrderNo, a.CompanyID, a.CustomerID, a.AccountID, a.AppointmentDate, a.CarrierID, a.SupplierID, a.SupplierIDs, a.ReferenceNo,a.Status";
			sql += " ORDER BY a.PONo, a.OrderNo, a.CompanyID, a.CustomerID, a.AccountID, a.AppointmentDate, a.CarrierID, a.SupplierID, a.SupplierIDs, a.ReferenceNo,a.Status";
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderNoAppointTimeBetweenTime error:"+e);
		}
	}
	

	/**
	 * Parking list
	 * @param OrderNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow findOrderSomeInfosByOrderNo(long OrderNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT od.OrderNo, od.CompanyID, od.CustomerID, od.DeptNo, od.RetailerOrderType, whs.Address1, whs.Address2, whs.City, whs.State, whs.ZipCode, whs.Phone,whs.Fax");
			sql.append(" ,od.ShipToName, od.ShipToAddress1, od.ShipToAddress2, od.ShipToCity, od.ShipToState, od.ShipToZipCode, od.ShipToID,od.ShipToPhone");
			sql.append(" ,CASE WHEN(od.FreightTerm = 'Third Party') THEN od.BillToName ELSE '' END as BillToName");
			sql.append(" ,CASE WHEN(od.FreightTerm = 'Third Party') THEN od.BillToAddress1 ELSE '' END as BillToAddress1");
			sql.append(" ,CASE WHEN(od.FreightTerm = 'Third Party') THEN od.BillToAddress2 ELSE '' END as BillToAddress2");
			sql.append(" ,CASE WHEN(od.FreightTerm = 'Third Party') THEN od.BillToCity ELSE '' END as BillToCity");
			sql.append(" ,CASE WHEN(od.FreightTerm = 'Third Party') THEN od.BillToState ELSE '' END as BillToState");
			sql.append(" ,CASE WHEN(od.FreightTerm = 'Third Party') THEN od.BillToZipCode ELSE '' END as BillToZipCode");
			sql.append(" ,od.BillToZipCode,od.BOLNote");
			sql.append(" ,od.ReferenceNo, od.LoadNo, od.AppointmentDate, od.ProNo, v.External_ID as ExternalID, od.FreightTerm,od.CarrierID, ca.ShipFromName");
			sql.append(" , od.OrderedDate, od.RequestedDate, od.ShippedDate, od.PONo");
			sql.append(" FROM  Orders od");
			sql.append(" LEFT JOIN Warehouses whs ON whs.CompanyID = od.CompanyID ");
			sql.append(" LEFT JOIN CustomerAccounts ca ON od.CompanyID = ca.CompanyID AND od.CustomerID = ca.CustomerID AND od.AccountID = ca.AccountID");
			sql.append(" left join vizio_extension v on v.vizio_dn = od.ReferenceNo ");
			sql.append(" WHERE od.OrderNo = ").append(OrderNo);
			sql.append(" AND whs.WarehouseID = od.WarehouseID");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and od.CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and od.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatus(status));
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderSomeInfosByOrderNo error:"+e);
		}
	}
	
	/**
	 * Parking list items
	 * @param OrderNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderItemsSomeInfoGroupByOrderNo(long OrderNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select  CASE ");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and (a.Unit = 'Package' or a.Unit = 'Pallet' or a.Unit = 'Piece'))");
			sql.append(" THEN  (case when (0 = a.ShippedQty%a.UnitsPerPackage) then (a.ShippedQty/a.UnitsPerPackage) else (a.ShippedQty/a.UnitsPerPackage+1) end )");
			sql.append(" WHEN (a.UnitsPerPackage is not null and 0 != a.UnitsPerPackage and a.QtyPerUnit is not null and a.QtyPerUnit != 0 and (a.Unit = 'Inner Case'))");
			sql.append(" THEN (case when (0 = a.ShippedQty%(a.UnitsPerPackage*a.QtyPerUnit)) then (a.ShippedQty/(a.UnitsPerPackage*a.QtyPerUnit))else (a.ShippedQty/(a.UnitsPerPackage*a.QtyPerUnit)+1) end )");
			sql.append(" ELSE a.ShippedQty END AS orderLineCase,a.*, CAST(round(a.volume,2) as numeric(5,2))*a.ShippedQty volume1");
			sql.append(" FROM(");
			
			sql.append(" select f.*, g.ShippedQty from ");
			sql.append(" (select max(e.ItemID)ItemID, max(e.CommodityDescription)CommodityDescription,max(e.BuyerItemID)BuyerItemID,max(e.UPC)UPC , max(e.StyleNo)StyleNo,max(e.Size)Size");
			sql.append(" ,max(e.Unit)Unit, sum(e.OrderedQty)OrderedQty, max(e.UnitsPerPackage)UnitsPerPackage ,max(e.volume)volume,sum(e.Pallets)Pallets,max(e.PoundPerPackage)PoundPerPackage");
			sql.append(" ,max(e.PalletWeight)PalletWeight, max(e.OrderNo)OrderNo , max(e.Length)Length,max(e.Width)Width,max(e.Height)Height, max(e.QtyPerUnit)QtyPerUnit, max(e.CompanyID)CompanyID");
			
			sql.append("  from (select odl.Pallets,odl.OrderedQty,odl.PoundPerPackage,odl.PalletWeight,odl.BuyerItemID, od.OrderNo, odl.ItemID,odl.Length,odl.Width,odl.Height");
			sql.append(" ,ci.Unit, ci.QtyPerUnit, ci.UnitsPerPackage, ci.StyleNo,ci.Size,ci.UPC ,(odl.Length*odl.Width*odl.Height*0.0005787037037) volume, odl.CompanyID");
			sql.append(" , CASE WHEN(odl.CommodityDescription is null or odl.CommodityDescription = '') THEN ci.CommodityDescription ELSE odl.CommodityDescription end as CommodityDescription");
			sql.append(" FROM Orders od");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID");
//			sql.append(" LEFT JOIN OrderLinePlates odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo]");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ");
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID");
			sql.append(" WHERE od.OrderNo = ").append(OrderNo);
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and od.CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and od.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatus(status));
			
			sql.append(" )e  GROUP BY e.ItemID) f");
			sql.append(" join(select sum(odlp.ShippedQty)ShippedQty, max(odl.ItemID)ItemID, max(odl.CompanyID)CompanyID");
			sql.append(" FROM Orders od");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID");
			sql.append(" LEFT JOIN OrderLinePlates odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo]");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID ");
			sql.append(" AND odl.CompanyID = ci.CompanyID AND ci.CustomerID = od.CustomerID");
			sql.append(" WHERE od.OrderNo = ").append(OrderNo);
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and od.CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and od.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatus(status));
			sql.append(" GROUP BY odl.ItemID)  g on g.ItemID = f.ItemID and g.CompanyID = f.CompanyID");
			
			sql.append(" ) a ");
//			System.out.println("ww:"+sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderItemsSomeInfoGroupByOrderNo error:"+e);
		}
	}
	
	/**
	 * 通过LoadNo，MasterBolNos，查询Order的Ponos
	 * @param loadNo
	 * @param MasterBOLNos
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderPonoInfosByLoadNoMasterBolNo(String loadNo,Long[] MasterBOLNos, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select od.PONo, od.ReferenceNo, od.StoreName, od.ShipToName, od.ShipToStoreNo,od.DeptNo,od.OrderNo, od.CompanyID,od.CustomerID").append("\n");
			sql.append(" FROM MasterBOLs bol ").append("\n");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ").append("\n");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND bol.CompanyID = od.CompanyID ").append("\n");
			sql.append(" WHERE bol.LoadNo = '").append(loadNo).append("'").append("\n");
			//sql.append(" WHERE replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'").append("\n");
			if(MasterBOLNos.length > 0)
			{
				sql.append(" AND (bol.MasterBOLNo = ").append(MasterBOLNos[0]).append("\n");
				for (int i = 1; i < MasterBOLNos.length; i++)
				{
					sql.append(" OR bol.MasterBOLNo = ").append(MasterBOLNos[i]).append("\n");
				}
				sql.append(")").append("\n");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND bol.CompanyID='"+companyId+"'").append("\n");
			}
			if(!StrUtil.isBlank(customerId))
			{
//				sql.append(" AND bol.CustomerID='"+customerId+"'").append("\n");
				sql.append(" AND bol.CustomerID in (").append(parseCustomerIDStrToInStr(customerId)).append(")\n");
			}	
			sql.append(sqlStatusBol(status)).append("\n");
			sql.append(sqlStatusOrder(status)).append("\n");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderPonoInfosByLoadNoMasterBolNo error:"+e);
		}
	}
	
	
	public DBRow[] findOrderPonoInfosByPoNo(String loadNo, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			/*sql.append(" select od.PONo, od.ReferenceNo, od.StoreName, od.ShipToName, od.ShipToStoreNo,od.DeptNo,od.OrderNo, od.CompanyID,od.CustomerID").append("\n");
			sql.append(" FROM Orders od ").append("\n");
			sql.append(" JOIN MasterBOLLines boll ON bol.MasterBOLNo = boll.MasterBOLNo AND bol.CompanyID = boll.CompanyID ").append("\n");
			sql.append(" JOIN Orders od ON boll.OrderNo = od.OrderNo AND bol.CompanyID = od.CompanyID ").append("\n");
			sql.append(" WHERE od.PoNo = '").append(loadNo).append("'").append("\n");*/
			//sql.append(" WHERE replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'").append("\n");
			sql.append(" select od.PONo, od.ReferenceNo, od.StoreName, od.ShipToName, od.ShipToStoreNo,od.DeptNo,od.OrderNo, od.CompanyID,od.CustomerID").append("\n");
			sql.append(" FROM Orders od ").append("\n");
			sql.append(" WHERE od.PoNo = '").append(loadNo).append("'").append("\n");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND od.CompanyID='"+companyId+"'").append("\n");
			}
			if(!StrUtil.isBlank(customerId))
			{
//				sql.append(" AND bol.CustomerID='"+customerId+"'").append("\n");
				sql.append(" AND od.CustomerID in (").append(parseCustomerIDStrToInStr(customerId)).append(")\n");
			}	
			sql.append(sqlStatusBol(status)).append("\n");
			sql.append(sqlStatusOrder(status)).append("\n");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderPonoInfosByLoadNoMasterBolNo error:"+e);
		}
	}
	
	
	public DBRow[] findOrderLinesByOrderNoCompany(long orderNo, String companyID, String customerId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select odl.*, ci.Unit, ci.UnitsPerPackage, ci.QtyPerUnit");
			sql.append(" , CASE WHEN(odl.NMFC is null or odl.NMFC='') THEN ci.NMFC ELSE odl.NMFC end as NMFC2");
			sql.append(" ,CASE WHEN(odl.FreightClass is null or odl.FreightClass='') THEN ci.FreightClass ELSE odl.FreightClass end as FreightClass2");
			sql.append(" ,CASE WHEN(odl.CommodityDescription is null or odl.CommodityDescription='') THEN ci.CommodityDescription ELSE ci.CommodityDescription end as CommodityDescription2");
			sql.append(" from OrderLines odl");
			sql.append(" LEFT JOIN CustomerItems ci on odl.ItemID = ci.ItemID  AND odl.CompanyID = ci.CompanyID");
			sql.append(" where 1=1");
			sql.append(" and odl.OrderNo = ").append(orderNo);
			sql.append(" and odl.CompanyID = '").append(companyID).append("'");
			sql.append(" and ci.CustomerID = '").append(customerId).append("'");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderLinesByOrderNoCompany error:"+e);
		}
	}
	
	public DBRow[] findOrderLinePlatesByOrderNoCompany(long orderNo, String companyID, int lineNo) throws Exception
	{
		
		try
		{
			StringBuffer sql = new StringBuffer();
			
			sql.append("SELECT max(op.CompanyID) as CompanyID,op.OrderNo, [LineNo],MAX(SupplierID)SupplierID, ");
			sql.append("MAX(LotNo)LotNo,CAST(CEILING(SUM(ShippedQty))  as int)  ShippedQty,1 as CombineType  from OrderLinePlates op ");
			sql.append(" inner join Orders od on op.CompanyID=od.CompanyID and op.OrderNo=od.OrderNo");
			sql.append(" where op.OrderNo = '"+orderNo+"' and op.CompanyID = '"+companyID+"' and [LineNo] = "+lineNo);
			sql.append(" and (od.CompanyID ! ='W17' or od.CustomerID!='Seidig0005' or od.AccountID!='Walmart-ETC')");
			sql.append(" GROUP BY op.OrderNo, [LineNo]");
			sql.append(" union all ");
			sql.append(" select b.CompanyID ,b.OrderNo,b.[LineNo],b.SupplierID as SupplierID ,b.LotNo as LotNo,");
			sql.append(" CASE WHEN(IsNull(a.CartonQty,0)>1)  THEN a.CartonQty  ELSE b.ShippedQty END AS ShippedQty,");
			sql.append(" CASE WHEN(ISNULL(a.CartonQty,0)>1) THEN 2  ELSE 1 END AS CombineType");
			
			sql.append(" from ");
			sql.append(" (SELECT max(op.CompanyID) as CompanyID,op.OrderNo, [LineNo],MAX(SupplierID)SupplierID, MAX(LotNo)LotNo,CAST(CEILING(SUM(ShippedQty))  as int) as ShippedQty  ");
			sql.append(" from OrderLinePlates op ");
			sql.append(" inner join Orders od on op.CompanyID=od.CompanyID and op.OrderNo=od.OrderNo");
			sql.append(" where op.OrderNo = '"+orderNo+"' and op.CompanyID = 'W17'  and  od.CompanyID = '"+ companyID +"' and  od.CustomerID='Seidig0005' AND od.AccountID='Walmart-ETC'  and [LineNo] =   "+lineNo);
			sql.append(" GROUP BY op.OrderNo, [LineNo])b");
			sql.append(" left join (SELECT count(oc.CartonNo) AS CartonQty,oc.OrderNo,odl.CompanyID,odl.[LineNo] FROM Orders od  ");
			sql.append(" JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID  ");
			sql.append(" JOIN OrderCartons oc ON oc.OrderNo = od.OrderNo AND oc.CompanyID = od.CompanyID  ");
			sql.append(" WHERE od.OrderNo = '"+orderNo+"' AND od.CompanyID = 'W17' and  od.CompanyID = '"+ companyID +"' AND od.CustomerID='Seidig0005' AND od.AccountID='Walmart-ETC'  ");
			sql.append(" GROUP BY oc.OrderNo, odl.CompanyID,odl.[LineNo] HAVING COUNT(distinct odl.ItemID) =  1");
			sql.append(" union all  ");
			sql.append(" SELECT count(oc.CartonNo) AS CartonQty,oc.OrderNo, odl.CompanyID,odl.[LineNo] ");
			sql.append(" FROM Orders od  JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID  ");
			sql.append(" inner JOIN OrderLineCartons oc ON oc.OrderNo = odl.OrderNo AND oc.CompanyID = odl.CompanyID  and oc.[LineNo] = odl.[LineNo]");
			sql.append(" WHERE od.OrderNo = '"+orderNo+"' AND od.CompanyID = 'W17'  and  od.CompanyID = '"+ companyID +"' AND od.CustomerID='Seidig0005' AND od.AccountID='Walmart-ETC'  ");
			sql.append(" GROUP BY oc.OrderNo,odl.CompanyID,odl.[LineNo] HAVING COUNT(distinct odl.ItemID) >1) a on a.CompanyID =b.CompanyID and a.OrderNo=b.OrderNo and a.[LineNo] =b.[LineNo]");
			/*sql.append(" left  join (");
			sql.append(" SELECT max(op.CompanyID) as CompanyID,op.OrderNo, [LineNo],MAX(SupplierID)SupplierID, MAX(LotNo)LotNo,CAST(CEILING(SUM(ShippedQty))  as int) as ShippedQty  ");
			sql.append(" from OrderLinePlates op ");
			sql.append(" inner join Orders od on op.CompanyID=od.CompanyID and op.OrderNo=od.OrderNo");
			sql.append(" where op.OrderNo = '"+orderNo+"' and op.CompanyID = 'W17' and  od.CustomerID='Seidig0005' AND od.AccountID='Walmart-ETC'  and [LineNo] =   "+lineNo);
			sql.append(" GROUP BY op.OrderNo, [LineNo]");
			sql.append(" )b on a.CompanyID =b.CompanyID and a.OrderNo=b.OrderNo and a.[LineNo] =b.[LineNo]");*/
		
			/*sql.append("select max(CompanyID)CompanyID, OrderNo, [LineNo],MAX(SupplierID)SupplierID, MAX(LotNo)LotNo,CAST(CEILING(SUM(ShippedQty))  as int) ShippedQty");
			sql.append(" from OrderLinePlates where OrderNo = ").append(orderNo);
			sql.append(" and CompanyID = '").append(companyID).append("'");
			sql.append(" and [LineNo] = ").append(lineNo);
			sql.append("  GROUP BY OrderNo, [LineNo]");*/
			System.out.println(sql); 
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderLinePlatesByOrderNoCompany error:"+e);
		}
	}
	
	/**
	 * 查询plate，不group by LineNo
	 * @param orderNo
	 * @param companyID
	 * @param lineNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderLinePlatesByOrderNoCompanyNoGroup(long orderNo, String companyID, int lineNo) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select *");
			sql.append(" from OrderLinePlates where OrderNo = ").append(orderNo);
			sql.append(" and CompanyID = '").append(companyID).append("'");
			sql.append(" and [LineNo] = ").append(lineNo);
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderLinePlatesByOrderNoCompany error:"+e);
		}
	}
	
	/**
	 * 查出所有AccountID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllAccountIDs() throws Exception
	{
		try
		{
			return dbUtilAutoTranSQLServer.selectMutliple("select AccountID from CustomerAccounts GROUP BY AccountID");
	
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findAllAccountIDs error:"+e);
		}
	}
	
	/**
	 * 查出所有SupplierID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllSupplierIDs() throws Exception
	{
		try
		{
			return dbUtilAutoTranSQLServer.selectMutliple("select SupplierID from CustomerSuppliers GROUP BY SupplierID");
	
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findAllSupplierIDs error:"+e);
		}
	}
	
	/**
	 * 查出所有CustomerIDs
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllCustomerIDs() throws Exception
	{
		try
		{
			return dbUtilAutoTranSQLServer.selectMutliple("select CustomerID from CustomerAccounts GROUP BY CustomerID");
	
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findAllCustomerIDs error:"+e);
		}
	}
	
	
	/**
	 * 通过 loadNo获取Orders
	 * @param loadNo
	 * @param customerId
	 * @param companyId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findOrderInfosByLoadNo(String loadNo, String companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select mbol.LoadNo, mbol.CompanyID, mbol.CustomerID, mbol.AccountID, od.OrderNo, od.ReferenceNo ");
			sql.append("  from MasterBOLs mbol ");
			sql.append("  join MasterBOLLines mboll on mboll.MasterBOLNo = mbol.MasterBOLNo and mboll.CompanyID = mbol.CompanyID");
			sql.append("  join Orders od on od.OrderNo = mboll.OrderNo and od.CompanyID = mboll.CompanyID");
			sql.append(" where mbol.LoadNo = '").append(loadNo).append("'");
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and od.CustomerID in (").append(parseCustomerIDStrToInStr(customerId)).append(")");
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and od.CompanyID = '").append(companyId).append("'");
			}
			sql.append(sqlStatusMbol(status));
			sql.append(sqlStatusOrder(status));
//			sql.append(" UNION ");
//			sql.append(" select od.LoadNo, od.CompanyID, od.CustomerID, od.AccountID , od.OrderNo, od.ReferenceNo from Orders od");
//			sql.append(" where od.LoadNo = '").append(loadNo).append("'");
//			if(!StrUtil.isBlank(customerId))
//			{
//				sql.append(" and od.CustomerID = '").append(customerId).append("'");
//			}
//			if(!StrUtil.isBlank(companyId))
//			{
//				sql.append(" and od.CompanyID = '").append(companyId).append("'");
//			}
//			sql.append(sqlStatusOrder(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderInfosByLoadNo error:"+e);
		}
	}
	
	private String parseCustomerIDStrToInStr(String CustomerID)
	{
		String str = "";
		if(!StrUtil.isBlank(CustomerID))
		{
			String[] customerIds = CustomerID.split(",");
			for (int i = 0; i < customerIds.length; i++)
			{
				str += ",'"+customerIds[i]+"'";
			}
			if(str.length() > 0)
			{
				str = str.substring(1);
			}
		}
		return str;
	}


 
	/**
	 * @param pono
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @throws Exception
	 */
	public DBRow[] findOrderInfoByPoNo(String pono, String companyId, String customerId, String status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT  od.ReferenceNo, od.AccountID, od.OrderNo");
			sql.append(" from Orders od ");
			sql.append(" where od.PONo = '").append(pono).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and od.CustomerID = '").append(customerId).append("'");
			}
			if(!StrUtil.isBlank(status))
			{
				sql.append(" and Status = '").append(status).append("'");
			}
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderInfoByPoNo error:"+e);
		}
	}
	
	
	/**
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @throws Exception
	 */
	public DBRow findOrderInfoByOrderNo(long orderNo, String companyId, String customerId, String status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select od.ReferenceNo, od.AccountID, od.OrderNo");
			sql.append(" from Orders od ");
			sql.append(" where od.OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and od.CustomerID = '").append(customerId).append("'");
			}
			if(!StrUtil.isBlank(status))
			{
				sql.append(" and Status = '").append(status).append("'");
			}
			return dbUtilAutoTranSQLServer.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderInfoByOrderNo error:"+e);
		}
	}
	
	
	/**
	 * 查预约到达时间为某天的Loads
	 * @param time
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findLoadOrderNosAppointTimeBetweenTime(String startTime, String endTime, String[] status, String[] companyId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT mbol.LoadNo, mbol.AppointmentDate, mbol.CompanyID, mbol.CustomerID, mbol.CarrierID,mbol.AccountID, od.OrderNo, od.ReferenceNo ,mbol.Status");
			sql.append(" FROM MasterBOLs mbol");
			sql.append(" join MasterBOLLines mboll on mboll.MasterBOLNo = mbol.MasterBOLNo and mboll.CompanyID = mbol.CompanyID");
			sql.append(" join Orders od on od.OrderNo = mboll.OrderNo and od.CompanyID = mbol.CompanyID");
//			sql.append(" join OrderLines odl on odl.OrderNo = od.OrderNo and odl.CompanyID = od.CompanyID");
			sql.append(" WHERE 1=1 ");
			sql.append(" AND mbol.LoadNo IS NOT NULL AND mbol.LoadNo != ''");
//			sql.append(sqlStatusMbol(status));
//			sql.append(sqlStatusOrder(status));
			if(!StrUtil.isBlank(startTime))
			{
				sql.append(" and Convert(varchar(10),mbol.AppointmentDate,120) >= '" + startTime + "'");
			}
			if(!StrUtil.isBlank(endTime))
			{
				sql.append(" AND Convert(varchar(10),mbol.AppointmentDate,120) <= '" + endTime + "'");
			}
//			sql.append(" and (mbol.LoadNo = '150207569'");
//			sql.append(" or mbol.LoadNo = '123637356')");
			sql.append(sqlCompanyIdMbol(companyId));
			sql.append(" ORDER BY mbol.LoadNo,mbol.AppointmentDate");
//			System.out.println(sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadOrderNosAppointTimeBetweenTime error:"+e);
		}
	}
	
	
	/**
	 * 通过MasterBolNo，CompanyId，CustomerId查询MasterBols信息
	 * @param masterBolNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年11月25日 上午10:29:11
	 */
	public DBRow[] findMasterBolsByMasterBolNo(long masterBolNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from MasterBOLs where 1=1");
			if(masterBolNo > 0)
			{
				sql.append(" and MasterBOLNo = ").append(masterBolNo);
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND CustomerID = '").append(customerId).append("'");
			}
//			System.out.println("orders:"+sql.toString());
			sql.append(sqlStatus(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findMasterBolsByMasterBolNo error:"+e);
		}
	}
	
	/**
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @throws Exception
	 */
	public DBRow[] findOrdersByOrderNo(long orderNo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select od.*,odl.ItemID, odp.SupplierID");
			sql.append(" from Orders od ");
			sql.append(" LEFT JOIN OrderLines odl on odl.OrderNo = od.OrderNo and odl.CompanyID = od.CompanyID");
			sql.append(" LEFT JOIN OrderLinePlates odp on odp.OrderNo = od.OrderNo and odp.CompanyID = od.CompanyID");
			sql.append(" where convert(varchar(10),od.OrderNo)");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" + od.CompanyID");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" + od.CustomerID");
			}
			sql.append(" in ('").append(orderNo).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append("+'").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append("+'").append(customerId).append("'");
			}
			sql.append(")");
			sql.append(sqlStatusOrder(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersByOrderNo error:"+e);
		}
	}
	/**
	 * 将OrderNo，CompanyID，CustomerID拼成字符串查询plate
	 * @param orderNoCompanyIdCustomerID
	 * @param status
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年2月4日 下午8:22:44
	 */
	public DBRow[] findOrderLinePlatesByOrderNo(String orderNoCompanyIdCustomerID, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select od.*,odl.ItemID, odp.SupplierID");
			sql.append(" from Orders od ");
			sql.append(" LEFT JOIN OrderLines odl on odl.OrderNo = od.OrderNo and odl.CompanyID = od.CompanyID");
			sql.append(" LEFT JOIN OrderLinePlates odp on odp.OrderNo = od.OrderNo and odp.CompanyID = od.CompanyID and odp.[LineNo]=odl.[LineNo]");
			sql.append(" where convert(varchar(10),od.OrderNo) + od.CompanyID");
			sql.append(" in (").append(orderNoCompanyIdCustomerID).append(")");
			sql.append(sqlStatusOrder(status));
			sql.append(" order by od.OrderNo desc");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderLinePlatesByOrderNo error:"+e);
		}
	}
	/**
	 * 按照order的最后操作时间查询pallet
	 * @param orderNoCompanyIdCustomerID
	 * @param status
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年2月4日 下午8:22:44
	 */
	public DBRow[] findOrderLinePlatesByTime(String startTime, String endTime,String companyId)throws Exception
	{
		try
		{
			String sql = "SELECT DISTINCT odl.ItemID,odp.SupplierID,od.CompanyID,od.OrderNo,od.CustomerID,od.ShipToName,od.ShipToAddress1,od.ShipToAddress2,od.ShipToCity,od.ShipToState"
					+" ,od.ShipToZipCode,od.ShipToCountry,od.DateUpdated,od.AccountID "
					+" FROM Orders od LEFT JOIN OrderLines odl ON odl.OrderNo = od.OrderNo AND odl.CompanyID = od.CompanyID"
					+" LEFT JOIN OrderLinePlates odp ON odp.OrderNo = od.OrderNo AND odp.CompanyID = od.CompanyID AND odp.[LineNo] = odl.[LineNo] WHERE"
					+" od.DateUpdated >= '"+startTime+"'"
					+" AND od.DateUpdated <= '"+endTime+"'"
					+" AND od.CompanyID IN ("+companyId+")"
					+" ORDER BY od.OrderNo DESC";
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderLinePlatesByOrderNo error:"+e);
		}
	}

	
	/**
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @throws Exception
	 */
	public DBRow[] findOrdersByPONo(String PONo, String companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" select od.*, odp.SupplierID");
			sql.append(" from Orders od ");
			sql.append(" LEFT JOIN OrderLinePlates odp on odp.OrderNo = od.OrderNo and odp.CompanyID = od.CompanyID");
			sql.append(" where od.PONo = '").append(PONo).append("'");
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" and od.CompanyID = '").append(companyId).append("'");
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" and od.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatusOrder(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersByPONo error:"+e);
		}
	}
	
	/**
	 * 通过Load查询Order
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月9日 上午9:21:20
	 */
	public DBRow[] findOrderAllInfosByLoadNo(String loadNo, String[] companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select bol.LoadNo, bol.MasterBOLNo, od.*");
			sql.append(" from MasterBOLs bol ");
			sql.append(" JOIN MasterBOLLines boll ON boll.MasterBOLNo = bol.MasterBOLNo AND boll.CompanyID = bol.CompanyID");// AND boll.CustomerID = bol.CustomerID
			sql.append(" join Orders od on od.OrderNo = boll.OrderNo and od.CompanyID = boll.CompanyID");// AND od.CustomerID = boll.CustomerID
			sql.append(" where replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			sql.append(sqlCompanyIdBol(companyId));
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND bol.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatusBol(status));
			sql.append(sqlStatusOrder(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderAllInfosByLoadNo error:"+e);
		}
	}
	
	/**
	 * @param PONo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @throws Exception
	 */
	public DBRow[] findOrdersAllInfoByPONo(String PONo, String[] companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select bol.LoadNo, bol.MasterBOLNo, od.*");
			sql.append(" from Orders od ");// AND od.CustomerID = boll.CustomerID
			sql.append(" left JOIN MasterBOLLines boll ON boll.OrderNo = od.OrderNo AND boll.CompanyID = od.CompanyID");// AND boll.CustomerID = bol.CustomerID
			sql.append(" left join MasterBOLs bol on bol.MasterBOLNo = boll.MasterBOLNo and bol.CompanyID = boll.CompanyID");
			sql.append(" where replace(od.PONo,' ', '') = '").append(PONo.replace(" ", "")).append("'");
			sql.append(sqlCompanyIdOrder(companyId));
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND od.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatusOrder(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersAllInfoByPONo error:"+e);
		}
	}
	
	
	/**
	 * @param orderNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @throws Exception
	 */
	public DBRow[] findOrdersAllInfoByOrderNo(long OrderNo, String[] companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select bol.LoadNo, bol.MasterBOLNo, od.*");
			sql.append(" from Orders od ");// AND od.CustomerID = boll.CustomerID
			sql.append(" left JOIN MasterBOLLines boll ON boll.OrderNo = od.OrderNo AND boll.CompanyID = od.CompanyID");// AND boll.CustomerID = bol.CustomerID
			sql.append(" left join MasterBOLs bol on bol.MasterBOLNo = boll.MasterBOLNo and bol.CompanyID = boll.CompanyID");
			sql.append(" where od.OrderNo = ").append(OrderNo);
			sql.append(sqlCompanyIdOrder(companyId));
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND od.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatusOrder(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersAllInfoByOrderNo error:"+e);
		}
	}
	
	
	
		/**
	 * 通过Load查询Order
	 * @param loadNo
	 * @param companyIds
	 * @param customerId
	 * @param status
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月9日 上午9:21:20
	 */
	public DBRow[] findLoadAllInfosByLoadNoCompanyIds(String loadNo, String[] companyIds, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select bol.LoadNo, bol.CompanyID, max(bol.MasterBOLNo)MasterBOLNo");
			sql.append(" ,max(bol.AppointmentDate) AppointmentDate,max(bol.Status)Status");
			sql.append(" from MasterBOLs bol ");
			sql.append(" where replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			sql.append(sqlCompanyIdBol(companyIds));
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND bol.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatusBol(status));
			sql.append(" GROUP BY bol.LoadNo, bol.CompanyID");//, bol.CustomerID, bol.AccountID
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findLoadAllInfosByLoadNoCompanyIds error:"+e);
		}
	}
	
	/**
	 * @param PONo
	 * @param companyIds
	 * @param customerId
	 * @param status
	 * @throws Exception
	 */
	public DBRow[] findPoAllInfosByPONoCompanyIds(String PONo, String[] companyIds, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select max(bol.LoadNo) m_load, max(bol.MasterBOLNo)MasterBOLNo,max(bol.Status) m_status,max(bol.AppointmentDate) m_appointment,max(bol.CustomerID) m_customer");
			sql.append(" ,max(od.OrderNo) OrderNo,od.PONo, od.CompanyID, max(od.CustomerID)CustomerID,max(od.Status) Status,max(od.AppointmentDate) AppointmentDate, max(od.StagingAreaID) StagingAreaID,max(od.Status)Status,max(od.AccountID)AccountID");
			sql.append(" from Orders od ");// AND od.CustomerID = boll.CustomerID
			sql.append(" left JOIN MasterBOLLines boll ON boll.OrderNo = od.OrderNo AND boll.CompanyID = od.CompanyID");// AND boll.CustomerID = bol.CustomerID
			sql.append(" left join MasterBOLs bol on bol.MasterBOLNo = boll.MasterBOLNo and bol.CompanyID = boll.CompanyID");
			sql.append(" where replace(od.PONo,' ', '') = '").append(PONo.replace(" ", "")).append("'");
			sql.append(sqlCompanyIdOrder(companyIds));
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND od.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatusOrder(status));
			sql.append(" group by od.PONo, od.CompanyID");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findPoAllInfosByPONoCompanyIds error:"+e);
		}
	}
	
	
	/**
	 * @param orderNo
	 * @param companyIds
	 * @param customerId
	 * @param status
	 * @throws Exception
	 */
	public DBRow[] findOrdersAllInfoByOrderNoCompanyIds(long OrderNo, String[] companyIds, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select bol.LoadNo m_load, bol.MasterBOLNo, bol.Status m_status,bol.AppointmentDate m_appointment,bol.CustomerID m_customer");//,od.*
			sql.append(" , od.OrderNo, od.CompanyID, od.Status, od.CustomerID, od.PONo, od.StoreID, od.LoadNo, od.AppointmentDate, od.StagingAreaID,od.AccountID");
			sql.append(" from Orders od ");// AND od.CustomerID = boll.CustomerID
			sql.append(" left JOIN MasterBOLLines boll ON boll.OrderNo = od.OrderNo AND boll.CompanyID = od.CompanyID");// AND boll.CustomerID = bol.CustomerID
			sql.append(" left join MasterBOLs bol on bol.MasterBOLNo = boll.MasterBOLNo and bol.CompanyID = boll.CompanyID");
			sql.append(" where od.OrderNo = ").append(OrderNo);
			sql.append(sqlCompanyIdOrder(companyIds));
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND od.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatusOrder(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersAllInfoByOrderNo error:"+e);
		}
	}
	
	
	/**
	 * 通过OrderNo查询order信息
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月24日 下午2:10:07
	 */
	public DBRow[] findOrderByOrderNo(long orderNo, String companyId) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from Orders where OrderNo = ").append(orderNo);
			if(!StrUtil.isBlank(companyId))
			{
				sql.append(" AND CompanyID = '").append(companyId).append("'");
			}
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderByOrderNo error:"+e);
		}
	}
	
	
	/**
	 * @param PONo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @throws Exception
	 */
	public DBRow[] findOrdersSomeInfoByPONo(String PONo, String[] companyId, String customerId, String[] status)throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select bol.LoadNo, bol.MasterBOLNo");
			sql.append(" , od.OrderNo, od.CompanyID, od.Status, od.CustomerID, od.PONo, od.StoreID, od.LoadNo, od.AppointmentDate");
			sql.append(" from Orders od ");// AND od.CustomerID = boll.CustomerID
			sql.append(" left JOIN MasterBOLLines boll ON boll.OrderNo = od.OrderNo AND boll.CompanyID = od.CompanyID");// AND boll.CustomerID = bol.CustomerID
			sql.append(" left join MasterBOLs bol on bol.MasterBOLNo = boll.MasterBOLNo and bol.CompanyID = boll.CompanyID");
			sql.append(" where replace(od.PONo,' ', '') = '").append(PONo.replace(" ", "")).append("'");
			sql.append(sqlCompanyIdOrder(companyId));
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND od.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatusOrder(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrdersAllInfoByPONo error:"+e);
		}
	}
	
	
	/**
	 * 通过Load查询Order
	 * @param loadNo
	 * @param companyId
	 * @param customerId
	 * @param status
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月9日 上午9:21:20
	 */
	public DBRow[] findOrderSomeInfosByLoadNo(String loadNo, String[] companyId, String customerId, String[] status) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select bol.LoadNo, bol.MasterBOLNo");
			sql.append(" , od.OrderNo, od.CompanyID, od.Status, od.CustomerID, od.PONo, od.StoreID, od.LoadNo, od.AppointmentDate");
			sql.append(" from MasterBOLs bol ");
			sql.append(" JOIN MasterBOLLines boll ON boll.MasterBOLNo = bol.MasterBOLNo AND boll.CompanyID = bol.CompanyID");// AND boll.CustomerID = bol.CustomerID
			sql.append(" join Orders od on od.OrderNo = boll.OrderNo and od.CompanyID = boll.CompanyID");// AND od.CustomerID = boll.CustomerID
			sql.append(" where replace(bol.LoadNo,' ', '') = '").append(loadNo.replace(" ", "")).append("'");
			sql.append(sqlCompanyIdBol(companyId));
			if(!StrUtil.isBlank(customerId))
			{
				sql.append(" AND bol.CustomerID = '").append(customerId).append("'");
			}
			sql.append(sqlStatusBol(status));
			sql.append(sqlStatusOrder(status));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderAllInfosByLoadNo error:"+e);
		}
	}
	
	/**
	 * 获得预约了的MasterBOL
	 * @author zhanjie
	 * @param start_date
	 * @param end_date
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAppointmentMasterBOL(String start_date,String end_date,String companyIDs)
		throws Exception
	{
		try 
		{
			String sql = "SELECT bol.CompanyID as company_id,"+ModuleKey.CHECK_IN_LOAD+" as 'order_type',bol.LoadNo as order_number,null as 'pono',MAX(bol.AppointmentDate) as appointment_time,2 as 'inbound_outbound' FROM MasterBOLs AS bol "
						+"WHERE CONVERT(VARCHAR(19),bol.AppointmentDate,120)>='"+start_date+" 00:00:00' and CONVERT(VARCHAR(19),bol.AppointmentDate,120)<='"+end_date+" 23:59:59' and bol.CompanyID in ("+companyIDs+") "
						+"group by bol.LoadNo, bol.CompanyID "
						+"order by appointment_time asc ";
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ getAppointmentOrder error:"+e);
		}
	}
	
	/**
	 * 基于LoadNumber，CompanID获得MasterBOL
	 * @param loadNumber
	 * @param companyID
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getMasterBOLsByLoadCompany(String loadNumber,String companyID)
		throws Exception
	{
		String sql = "select DISTINCT bol.CustomerID,bol.CompanyID,bol.LoadNo from MasterBOLs AS bol "
					+"where bol.LoadNo = '"+loadNumber+"' and bol.CompanyID = '"+companyID+"' ";
		
		return dbUtilAutoTranSQLServer.selectMutliple(sql);
	}
	
	/**
	 * 获得没上Load的Order的预约
	 * @param start_date
	 * @param end_date
	 * @param companyIDs
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAppointmentOrder(String start_date,String end_date,String companyIDs)
			throws Exception
		{
			try 
			{
				String sql = "select o.CustomerID as customers,o.CompanyID as company_id,"+ModuleKey.CHECK_IN_ORDER+" as 'order_type',CONVERT(VARCHAR(15),o.OrderNo) as order_number,o.PONo as 'pono',o.AppointmentDate as appointment_time,2 as 'inbound_outbound' from Orders as o "
							+"left join MasterBOLLines as boll on o.OrderNo = boll.OrderNo " 
							+"WHERE boll.MasterBOLNo is null and CONVERT(VARCHAR(19),o.AppointmentDate,120)>='"+start_date+" 00:00:00' and CONVERT(VARCHAR(19),o.AppointmentDate,120)<='"+end_date+" 23:59:59' and o.CompanyID in ("+companyIDs+") "
							+"order by appointment_time asc ";
				
				return dbUtilAutoTranSQLServer.selectMutliple(sql);
			}
			catch (Exception e) 
			{
				throw new Exception("FloorSQLServerMgrZJ getAppointmentOrder error:"+e);
			}
		}
	
	
	/**
	 * 通过ContainerNo查询SupplierID,CarrierID,AppointmentDate
	 * @param containerNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptsByContainerNo(String containerNo, String[] status, String[] companyID) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select ReceiptNo,SupplierID,AppointmentDate,CompanyID, CustomerID,Status,");//BOLNo,ContainerNo,
			sql.append(ModuleKey.CHECK_IN_CTN).append(" as order_type");
			sql.append(" from Receipts where rtrim(ltrim(ContainerNo)) = '").append(containerNo.replace(" ", "")).append("'");
			sql.append(sqlStatus(status));
			sql.append(sqlCompanyId(companyID));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptsByContainerNo error:"+e);
		}
	}
	
	/**
	 * 通过BOL查询SupplierID,CarrierID,AppointmentDate
	 * @param BolNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptsByBolNo(String BolNo, String[] status, String[] companyID) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select ReceiptNo,SupplierID,AppointmentDate,CompanyID, CustomerID,Status,");//BOLNo,ContainerNo,
			sql.append(ModuleKey.CHECK_IN_BOL).append(" as order_type");
			sql.append(" from Receipts where rtrim(ltrim(BOLNo)) = '").append(BolNo.replace(" ", "")).append("'");
			sql.append(sqlStatus(status));
			sql.append(sqlCompanyId(companyID));
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptsByBolNo error:"+e);
		}
	}
	
	/**
	 * 基于日期获得出库的Appointment
	 * @author zhanjie
	 * @param start_date
	 * @param end_date
	 * @param companyIDs
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutboundAppointment(String start_date,String end_date,String companyIDs)
		throws Exception
	{
		try 
		{
			String whereBOLCompanyIDs = "";
			String whereOrderCompanyIDs = "";
			if (!companyIDs.equals(""))
			{
				 whereBOLCompanyIDs = " and bol.CompanyID in ("+companyIDs+") ";
				 whereOrderCompanyIDs = " and od.CompanyID in ("+companyIDs+") ";
			}	
						 //有Load的Order,有OrderLinePlates,SupplierID基于OrderLinePlates
			String sql = " select bol.CustomerID as customer_id,bol.CompanyID as company_id,bol.LoadNo as load_no,bol.MasterBOLNo as masterbol_no,od.PONo as pono,od.OrderNo as order_no,odl.[LineNo] as line_no,odlp.PlateNo as plate_no,odlp.SupplierID as supplier_id,bol.AppointmentDate as appointment_time,bol.ShipToName as ship_to,bol.CarrierID as carrier,od.ReferenceNo as reference_no,odl.Pallets as line_pallets"
						+ " , od.ShipToZipCode as ship_to_zip_code, od.ShipToAddress1+' '+od.ShipToAddress2 as ship_to_address, od.ShipToState as ship_to_state "
						+ " from Orders as od "
						+"join OrderLines as odl on odl.OrderNo = od.OrderNo and od.CompanyID = odl.CompanyID "
						+"join OrderLinePlates as odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo] "
						+"join MasterBOLLines as boll on od.OrderNo = boll.OrderNo  and boll.CompanyID = od.CompanyID " 
						+"join MasterBOLs as bol on boll.MasterBOLNo = bol.MasterBOLNo  and boll.CompanyID = bol.CompanyID " 
						+"where Convert(varchar(19),bol.AppointmentDate,120)>= '"+start_date+" 00:00:00' and Convert(varchar(19),bol.AppointmentDate,120)<='"+end_date+" 23:59:59' "
						+whereBOLCompanyIDs
						+"union " 
						//--有Load的Order,没OrderLinePlates,SupplierID基于OrderLine
						+"select bol.CustomerID,bol.CompanyID,bol.LoadNo,bol.MasterBOLNo,od.PONo,od.OrderNo,odl.[LineNo],0 as PlateNo,odl.SupplierID,bol.AppointmentDate,bol.ShipToName,bol.CarrierID,od.ReferenceNo,odl.Pallets as line_pallets"
						+ " , od.ShipToZipCode as ship_to_zip_code, od.ShipToAddress1+' '+od.ShipToAddress2 as ship_to_address, od.ShipToState as ship_to_state "
						+ " from Orders as od "
						+"join OrderLines as odl on odl.OrderNo = od.OrderNo and od.CompanyID = odl.CompanyID "
						+"left join OrderLinePlates as odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo] "
						+"join MasterBOLLines as boll on od.OrderNo = boll.OrderNo  and boll.CompanyID = od.CompanyID " 
						+"join MasterBOLs as bol on boll.MasterBOLNo = bol.MasterBOLNo  and boll.CompanyID = bol.CompanyID "
						+"where odlp.PlateNo is null and Convert(varchar(19),bol.AppointmentDate,120)>= '"+start_date+" 00:00:00' and Convert(varchar(19),bol.AppointmentDate,120)<='"+end_date+" 23:59:59' " 
						+whereBOLCompanyIDs
						+"union " 
						//--没Load的Order,有OrderLinePlates,SupplierID基于OrderLinePlates
						+"select od.CustomerID,od.CompanyID,bol.LoadNo,bol.MasterBOLNo,od.PONo,od.OrderNo,odl.[LineNo],0 as PlateNo,odl.SupplierID,od.AppointmentDate,od.ShipToName,od.CarrierID,od.ReferenceNo,odl.Pallets as line_pallets"
						+ " , od.ShipToZipCode as ship_to_zip_code, od.ShipToAddress1+' '+od.ShipToAddress2 as ship_to_address, od.ShipToState as ship_to_state "
						+ " from Orders as od "
						+"join OrderLines as odl on odl.OrderNo = od.OrderNo and od.CompanyID = odl.CompanyID "
						+"join OrderLinePlates as odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo] "
						+"left join MasterBOLLines as boll on od.OrderNo = boll.OrderNo  and boll.CompanyID = od.CompanyID " 
						+"left join MasterBOLs as bol on boll.MasterBOLNo = bol.MasterBOLNo  and boll.CompanyID = bol.CompanyID "
						+"where boll.MasterBOLNo is null and Convert(varchar(19),od.AppointmentDate,120)>= '"+start_date+" 00:00:00' and Convert(varchar(19),od.AppointmentDate,120)<='"+end_date+" 23:59:59' " 
						+whereOrderCompanyIDs
						+"union " 
						//--没Load的Order,没OrderLinePlates,SupplierID基于OrderLine--
						+"select od.CustomerID,od.CompanyID,bol.LoadNo,bol.MasterBOLNo,od.PONo,od.OrderNo,odl.[LineNo],0 as PlateNo,odl.SupplierID,od.AppointmentDate,od.ShipToName,od.CarrierID,od.ReferenceNo,odl.Pallets as line_pallets"
						+ " , od.ShipToZipCode as ship_to_zip_code, od.ShipToAddress1+' '+od.ShipToAddress2 as ship_to_address, od.ShipToState as ship_to_state "
						+ " from Orders as od "
						+"join OrderLines as odl on odl.OrderNo = od.OrderNo and od.CompanyID = odl.CompanyID "
						+"left join OrderLinePlates as odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo] "
						+"left join MasterBOLLines as boll on od.OrderNo = boll.OrderNo  and boll.CompanyID = od.CompanyID " 
						+"left join MasterBOLs as bol on boll.MasterBOLNo = bol.MasterBOLNo  and boll.CompanyID = bol.CompanyID "
						+"where boll.MasterBOLNo is null and odlp.PlateNo is null and Convert(varchar(19),od.AppointmentDate,120)>= '"+start_date+" 00:00:00' and Convert(varchar(19),od.AppointmentDate,120)<='"+end_date+" 23:59:59' " 
						+whereOrderCompanyIDs
						+"order by order_no,company_id,line_no";
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ getAppointmentOrder error:"+e);
		}
	}
	
	/**
	 * 基于单据类型与单据号查询预约
	 * @author zhanjie
	 * @param number_type
	 * @param number
	 * @param companyIDs
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOutboundAppointment(int number_type,String number,String companyIDs)
			throws Exception
		{
			try 
			{
				String whereBOLCompanyIDs = "";
				String whereOrderCompanyIDs = "";
				if (!companyIDs.equals(""))
				{
					 whereBOLCompanyIDs = " and bol.CompanyID in ("+companyIDs+") ";
					 whereOrderCompanyIDs = " and od.CompanyID in ("+companyIDs+") ";
				}	
				
				String sql = "";
				if (number_type == ModuleKey.CHECK_IN_LOAD)
				{
					sql = " select bol.CustomerID as customer_id,bol.CompanyID as company_id,bol.LoadNo as load_no,bol.MasterBOLNo as masterbol_no,od.PONo as pono,od.OrderNo as order_no,odl.[LineNo] as line_no,odlp.PlateNo as plate_no,odlp.SupplierID as supplier_id,bol.AppointmentDate as appointment_time,bol.ShipToName as ship_to,bol.CarrierID as carrier,od.ReferenceNo as reference_no,odl.Pallets as line_pallets from Orders as od "
							+"join OrderLines as odl on odl.OrderNo = od.OrderNo and od.CompanyID = odl.CompanyID "
							+"join OrderLinePlates as odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo] "
							+"join MasterBOLLines as boll on od.OrderNo = boll.OrderNo  and boll.CompanyID = od.CompanyID " 
							+"join MasterBOLs as bol on boll.MasterBOLNo = bol.MasterBOLNo  and boll.CompanyID = bol.CompanyID " 
							+"where bol.AppointmentDate is not null and bol.LoadNo = '"+number+"' "
							+whereBOLCompanyIDs
							+"union " 
							//--有Load的Order,没OrderLinePlates,SupplierID基于OrderLine
							+"select bol.CustomerID,bol.CompanyID,bol.LoadNo,bol.MasterBOLNo,od.PONo,od.OrderNo,odl.[LineNo],0 as PlateNo,odl.SupplierID,bol.AppointmentDate,bol.ShipToName,bol.CarrierID,od.ReferenceNo,odl.Pallets as line_pallets from Orders as od "
							+"join OrderLines as odl on odl.OrderNo = od.OrderNo and od.CompanyID = odl.CompanyID "
							+"left join OrderLinePlates as odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo] "
							+"join MasterBOLLines as boll on od.OrderNo = boll.OrderNo  and boll.CompanyID = od.CompanyID " 
							+"join MasterBOLs as bol on boll.MasterBOLNo = bol.MasterBOLNo  and boll.CompanyID = bol.CompanyID " 
							+"where bol.AppointmentDate is not null and bol.LoadNo = '"+number+"' " 
							+whereBOLCompanyIDs
							+"order by order_no,company_id,line_no";
				}
				else if (number_type == ModuleKey.CHECK_IN_ORDER || number_type == ModuleKey.CHECK_IN_PONO)
				{
					String whereNumber;
					if (number_type == ModuleKey.CHECK_IN_ORDER)
					{
						whereNumber = " and od.OrderNo = '"+number+"' ";
					}
					else
					{
						whereNumber = " and od.PONo = '"+number+"' ";
					}
					
					 		//--没Load的Order,有OrderLinePlates,SupplierID基于OrderLinePlates
					sql ="select od.CustomerID AS customer_id,od.CompanyID AS company_id,bol.LoadNo AS load_no,bol.MasterBOLNo AS masterbol_no,od.PONo AS pono,od.OrderNo AS order_no,odl.[LineNo] AS line_no,0 AS plate_no,odl.SupplierID AS supplier_id,od.AppointmentDate AS appointment_time,od.ShipToName AS ship_to,od.CarrierID AS carrier,od.ReferenceNo AS reference_no,odl.Pallets AS line_pallets from Orders as od "
						+"join OrderLines as odl on odl.OrderNo = od.OrderNo and od.CompanyID = odl.CompanyID "
						+"join OrderLinePlates as odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo] "
						+"left join MasterBOLLines as boll on od.OrderNo = boll.OrderNo  and boll.CompanyID = od.CompanyID " 
						+"left join MasterBOLs as bol on boll.MasterBOLNo = bol.MasterBOLNo  and boll.CompanyID = bol.CompanyID " 
						+"where od.AppointmentDate is not null and boll.MasterBOLNo is null "+whereNumber 
						+whereOrderCompanyIDs
						+"union " 
						//--没Load的Order,没OrderLinePlates,SupplierID基于OrderLine--
						+"select od.CustomerID,od.CompanyID,bol.LoadNo,bol.MasterBOLNo,od.PONo,od.OrderNo,odl.[LineNo],0 as PlateNo,odl.SupplierID,od.AppointmentDate,od.ShipToName,od.CarrierID,od.ReferenceNo,odl.Pallets as line_pallets from Orders as od "
						+"join OrderLines as odl on odl.OrderNo = od.OrderNo and od.CompanyID = odl.CompanyID "
						+"left join OrderLinePlates as odlp on odlp.OrderNo = odl.OrderNo and odlp.CompanyID = odl.CompanyID and odlp.[LineNo] = odl.[LineNo] "
						+"left join MasterBOLLines as boll on od.OrderNo = boll.OrderNo  and boll.CompanyID = od.CompanyID " 
						+"left join MasterBOLs as bol on boll.MasterBOLNo = bol.MasterBOLNo  and boll.CompanyID = bol.CompanyID " 
						+"where od.AppointmentDate is not null and boll.MasterBOLNo is null and odlp.PlateNo is null "+whereNumber 
						+whereOrderCompanyIDs
						+"order by order_no,company_id,line_no";
				}
				return dbUtilAutoTranSQLServer.selectMutliple(sql);
			}
			catch (Exception e) 
			{
				throw new Exception("FloorSQLServerMgrZJ getAppointmentOrder error:"+e);
			}
		}
	
	/**
	 * 基于时间获得入库的Appointment
	 * @param start_date
	 * @param end_date
	 * @param companyIDs
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getInboundAppointment(String start_date,String end_date,String companyIDs)
		throws Exception
	{
		String whereReceiptCompanyIDs = "";
		if (!companyIDs.equals(""))
		{
			 whereReceiptCompanyIDs = "and rp.CompanyID in ("+companyIDs+") ";
		}	
		String sql = "select rp.CustomerID as customer_id,rp.CompanyID as company_id,rp.ReceiptNo as receipt_no,rp.BOLNo as bol_no,rp.ContainerNo as container_no,rp.PONo as pono,rp.SupplierID as supplier_id,rp.AppointmentDate as appointment_time,rp.CarrierID as carrier,rp.ReferenceNo as reference_no,rpl.Pallets as line_pallets  from Receipts as rp "
					+"join ReceiptLines as rpl on rp.ReceiptNo = rpl.ReceiptNo and rp.CompanyID = rpl.CompanyID "
					+"where Convert(varchar(19),rp.AppointmentDate,120)>= '"+start_date+" 00:00:00' and Convert(varchar(19),rp.AppointmentDate,120)<='"+end_date+" 23:59:59' "
					+whereReceiptCompanyIDs
					+"order by appointment_time,rp.ReceiptNo";
		return dbUtilAutoTranSQLServer.selectMutliple(sql);
	}
	
	/**
	 * 基于ReceiptNo获得入库的Appointment
	 * @author zhanjie
	 * @param receipt_no
	 * @param companyIDs
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getInboundAppointment(String receipt_no,String companyIDs)
		throws Exception
		{
			String whereReceiptCompanyIDs = "";
			if (!companyIDs.equals(""))
			{
				 whereReceiptCompanyIDs = "and rp.CompanyID in ("+companyIDs+") ";
			}	
			String sql = "select rp.CustomerID as customer_id,rp.CompanyID as company_id,rp.ReceiptNo as receipt_no,rp.BOLNo as bol_no,rp.ContainerNo as container_no,rp.PONo as pono,rp.SupplierID as supplier_id,rp.AppointmentDate as appointment_time,rp.CarrierID as carrier,rp.ReferenceNo as reference_no,rpl.Pallets as line_pallets  from Receipts as rp "
						+"join ReceiptLines as rpl on rp.ReceiptNo = rpl.ReceiptNo and rp.CompanyID = rpl.CompanyID "
						+"where rp.AppointmentDate is not null and rp.ReceiptNo = '"+receipt_no+"'"
						+whereReceiptCompanyIDs
						+"order by appointment_time,rp.ReceiptNo";
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}	

	/**
	 * 基于客户和仓库计算sku的入库数量总和
	 * @author Liang Jie
	 * @param companyID
	 * @param customerID
	 * @return
	 * @throws SQLException
	 */
	private DBRow[] getInSkuSumCount(String companyID, String customerID) throws SQLException{
		String inSql = "SELECT ItemID, t2.LotNo, t1.supplierID, SUM (t1.ReceivedQty) ReceivedQty FROM ReceiptLinePlates t1 "
				+"INNER JOIN (SELECT replace( replace(ItemID, char(13), ''), char(10), '') ItemID,CompanyID,ReceiptNo, rtrim(replace(LotNo, 'None', '')) LotNo,[LineNo] FROM ReceiptLines) t2 "
				+"ON t1.CompanyID = t2.CompanyID AND t1.ReceiptNo = t2.ReceiptNo AND t1.[LineNo] = t2.[LineNo] "
				+"INNER JOIN receipts t3 ON t2.CompanyID = t3.CompanyID AND t2.ReceiptNo = t3.receiptNo "
				+"AND t3.companyID = '"+companyID+"' AND  t3.customerID = '"+customerID+"' AND (t3.Status='Recieved' or t3.Status='Closed') "
//				+"GROUP BY ItemID, t1.supplierID";
				+ "GROUP BY ItemID, t2.LotNo, t1.supplierID";
		return dbUtilAutoTranSQLServer.selectMutliple(inSql);
	}
	/**
	 * 基于客户和仓库计算sku的入库数量总和
	 * @author wangcr
	 * @param companyID
	 * @param customerID
	 * @param hasLotno 是否区分lotno
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getInSkuSumCount(String companyID, String customerID, boolean hasLotno) throws SQLException{
		String inSql = "";
		if (hasLotno) {
			inSql = "SELECT ItemID, t2.LotNo, t1.supplierID, SUM (t1.ReceivedQty) ReceivedQty FROM ReceiptLinePlates t1 "
					+"INNER JOIN (SELECT replace( replace(ItemID, char(13), ''), char(10), '') ItemID,CompanyID,ReceiptNo, rtrim(replace(LotNo, 'None', '')) LotNo,[LineNo] FROM ReceiptLines) t2 "
					+"ON t1.CompanyID = t2.CompanyID AND t1.ReceiptNo = t2.ReceiptNo AND t1.[LineNo] = t2.[LineNo] "
					+"INNER JOIN receipts t3 ON t2.CompanyID = t3.CompanyID AND t2.ReceiptNo = t3.receiptNo "
					+"AND t3.companyID = '"+companyID+"' AND  t3.customerID = '"+customerID+"' AND (t3.Status='Recieved' or t3.Status='Closed') "
					+ "GROUP BY ItemID, t2.LotNo, t1.supplierID";
		} else {
			inSql = "SELECT ItemID, t2.LotNo, t1.supplierID, SUM (t1.ReceivedQty) ReceivedQty FROM ReceiptLinePlates t1 "
					+"INNER JOIN (SELECT replace( replace(ItemID, char(13), ''), char(10), '') ItemID,CompanyID,ReceiptNo, '' LotNo,[LineNo] FROM ReceiptLines) t2 "
					+"ON t1.CompanyID = t2.CompanyID AND t1.ReceiptNo = t2.ReceiptNo AND t1.[LineNo] = t2.[LineNo] "
					+"INNER JOIN receipts t3 ON t2.CompanyID = t3.CompanyID AND t2.ReceiptNo = t3.receiptNo "
					+"AND t3.companyID = '"+companyID+"' AND  t3.customerID = '"+customerID+"' AND (t3.Status='Recieved' or t3.Status='Closed') "
					+ "GROUP BY ItemID, t2.LotNo, t1.supplierID";
		}
		return dbUtilAutoTranSQLServer.selectMutliple(inSql);
	}
	
	/**
	 * 基于客户和仓库计算sku的出库数量总和
	 * @author Liang Jie
	 * @param companyID
	 * @param customerID
	 * @return
	 * @throws SQLException
	 */
	private DBRow[] getOutSkuSumCount(String companyID, String customerID) throws SQLException{
		String outSql = "SELECT ItemID, t2.LotNo, t1.SupplierID, SUM (t1.ShippedQty) ShippedQty FROM OrderLinePlates t1 "
				+"INNER JOIN(SELECT replace( replace(ItemID, char(13), ''), char(10), '') ItemID,CompanyID,OrderNo, rtrim(replace(LotNo, 'None', '')) LotNo,[LineNo] FROM OrderLines) t2 "
				+"ON t1.CompanyID = t2.CompanyID AND t1.OrderNo = t2.OrderNo AND t1.[LineNo] = t2.[LineNo] "
				+"INNER JOIN Orders t3 ON t2.CompanyID = t3.CompanyID AND t2.OrderNo = t3.OrderNo "
				+"AND t3.companyID = '"+companyID+"' AND  t3.customerID = '"+customerID+"' AND (t3.Status='Shipped' or t3.Status='Closed') "
//				+ "GROUP BY ItemID, t1.SupplierID";
				+ "GROUP BY ItemID, t2.LotNo, t1.SupplierID";
		return dbUtilAutoTranSQLServer.selectMutliple(outSql);
	}
	
	/**
	 * 基于客户和仓库计算sku的出库数量总和
	 * @author wangcr
	 * @param companyID
	 * @param customerID
	 * @param hasLotno
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getOutSkuSumCount(String companyID, String customerID, boolean hasLotno) throws SQLException {
		String outSql = "";
		if (hasLotno) {
			outSql = "SELECT ItemID, t2.LotNo, t1.SupplierID, SUM (t1.ShippedQty) ShippedQty FROM OrderLinePlates t1 "
					+"INNER JOIN(SELECT replace( replace(ItemID, char(13), ''), char(10), '') ItemID,CompanyID,OrderNo, rtrim(replace(LotNo, 'None', '')) LotNo,[LineNo] FROM OrderLines) t2 "
					+"ON t1.CompanyID = t2.CompanyID AND t1.OrderNo = t2.OrderNo AND t1.[LineNo] = t2.[LineNo] "
					+"INNER JOIN Orders t3 ON t2.CompanyID = t3.CompanyID AND t2.OrderNo = t3.OrderNo "
					+"AND t3.companyID = '"+companyID+"' AND  t3.customerID = '"+customerID+"' AND (t3.Status='Shipped' or t3.Status='Closed') "
					+ "GROUP BY ItemID, t2.LotNo, t1.SupplierID";
		} else {
			outSql = "SELECT ItemID, t2.LotNo, t1.SupplierID, SUM (t1.ShippedQty) ShippedQty FROM OrderLinePlates t1 "
					+"INNER JOIN(SELECT replace( replace(ItemID, char(13), ''), char(10), '') ItemID,CompanyID,OrderNo, '' LotNo,[LineNo] FROM OrderLines) t2 "
					+"ON t1.CompanyID = t2.CompanyID AND t1.OrderNo = t2.OrderNo AND t1.[LineNo] = t2.[LineNo] "
					+"INNER JOIN Orders t3 ON t2.CompanyID = t3.CompanyID AND t2.OrderNo = t3.OrderNo "
					+"AND t3.companyID = '"+companyID+"' AND  t3.customerID = '"+customerID+"' AND (t3.Status='Shipped' or t3.Status='Closed') "
					+ "GROUP BY ItemID, t2.LotNo, t1.SupplierID";
		}
		return dbUtilAutoTranSQLServer.selectMutliple(outSql);
	}
	
	/**
	 * 基于客户和仓库计算sku的调整数量总和
	 * @author Liang Jie
	 * @param companyID
	 * @param customerID
	 * @return
	 * @throws SQLException
	 * 
	 * 2015/05/28 by wangcr : Status in ('Closed' 'Open') 的数据
	 */
	private DBRow[] getAdjSkuSumCount(String companyID, String customerID) throws SQLException{
		String adjSql = "SELECT ItemID, t1.LotNo, t1.SupplierID, SUM (t1.AdjustedQty) AdjustedQty FROM "
				+"(SELECT replace( replace(ItemID, char(13), ''), char(10), '') ItemID, AdjustmentNo,SupplierID,CompanyID, rtrim(replace(LotNo, 'None', '')) LotNo,AdjustedQty FROM AdjustmentLines) t1 "
				+"INNER JOIN Adjustments t2 ON t1.AdjustmentNo = t2.AdjustmentNo AND t1.CompanyID = t2.CompanyID "
				+"AND t2.companyID = '"+companyID+"' AND  t2.customerID = '"+customerID+"' AND (t2.Status ='Closed' or t2.Status ='Open') "
//				+"GROUP BY ItemID, t1.SupplierID";
				+"GROUP BY ItemID, t1.LotNo, t1.SupplierID";
		return dbUtilAutoTranSQLServer.selectMutliple(adjSql);
	}
	
	/**
	 * 基于客户和仓库计算sku的调整数量总和
	 * 
	 * @author wangcr
	 * @param companyID
	 * @param customerID
	 * @param hasLotno
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getAdjSkuSumCount(String companyID, String customerID, boolean hasLotno) throws SQLException{
		String adjSql = "";
		if (hasLotno) {
			adjSql = "SELECT ItemID, t1.LotNo, t1.SupplierID, SUM (t1.AdjustedQty) AdjustedQty FROM "
					+"(SELECT replace( replace(ItemID, char(13), ''), char(10), '') ItemID, AdjustmentNo,SupplierID,CompanyID, rtrim(replace(LotNo, 'None', '')) LotNo,AdjustedQty FROM AdjustmentLines) t1 "
					+"INNER JOIN Adjustments t2 ON t1.AdjustmentNo = t2.AdjustmentNo AND t1.CompanyID = t2.CompanyID "
					+"AND t2.companyID = '"+companyID+"' AND  t2.customerID = '"+customerID+"' AND (t2.Status ='Closed' or t2.Status ='Open') "
					+"GROUP BY ItemID, t1.LotNo, t1.SupplierID";
		} else {
			adjSql = "SELECT ItemID, t1.LotNo, t1.SupplierID, SUM (t1.AdjustedQty) AdjustedQty FROM "
					+"(SELECT replace( replace(ItemID, char(13), ''), char(10), '') ItemID, AdjustmentNo,SupplierID,CompanyID, '' LotNo,AdjustedQty FROM AdjustmentLines) t1 "
					+"INNER JOIN Adjustments t2 ON t1.AdjustmentNo = t2.AdjustmentNo AND t1.CompanyID = t2.CompanyID "
					+"AND t2.companyID = '"+companyID+"' AND  t2.customerID = '"+customerID+"' AND (t2.Status ='Closed' or t2.Status ='Open') "
					+"GROUP BY ItemID, t1.LotNo, t1.SupplierID";
		}
		return dbUtilAutoTranSQLServer.selectMutliple(adjSql);
	}
	
	/**
	 * 基于客户和仓库计算sku的（picked，picking）数量总和
	 * @author wangcr
	 * @param companyID
	 * @param customerID
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getBookedSkuSumCount(String companyID, String customerID, boolean hasLotno) throws SQLException{
		String picksql = "";
		if (hasLotno) {
			picksql = "select l.ItemID, rtrim(replace(l.LotNo, 'None', '')) LotNo, l.SupplierID, sum(l.OrderedQty) wBookedQty from OrderLines l left join Orders t on l.CompanyID = t.CompanyID and l.OrderNo = t.OrderNo where "
				+ "t.Status in ('Picked','Picking') and t.CompanyID='"+companyID+"' and t.CustomerID = '"+customerID+"' group BY itemID, rtrim(replace(l.LotNo, 'None', '')), SupplierID";
		} else {
			picksql = "select l.ItemID, '' LotNo, l.SupplierID, sum(l.OrderedQty) wBookedQty from OrderLines l left join Orders t on l.CompanyID = t.CompanyID and l.OrderNo = t.OrderNo where "
					+ "t.Status in ('Picked','Picking') and t.CompanyID='"+companyID+"' and t.CustomerID = '"+customerID+"' group BY itemID, SupplierID";
		}
		return dbUtilAutoTranSQLServer.selectMutliple(picksql);
	}
	
	/**
	 * 在给定的订单范围内，获得状态是picked或picking且有LoadNo的订单列表
	 * @author Liang Jie
	 * @param companyID
	 * @param customerID
	 * @param orders
	 * @param FreightTerm
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getOrdersListForPickWithLoadNo(String companyID, String customerID, List<String> orders, String FreightTerm) throws SQLException{
		String sql = "select CustomerID,CompanyID,OrderNo,RequestedDate,FreightTerm,Status,LoadNo,ReferenceNo from Orders "
				+"WHERE Orders.CompanyID = '"+companyID+"' AND  Orders.CustomerID = '"+customerID+"' "
				+"AND (status='Picked' or status='Picking') AND LEN(LoadNo)>0 AND FreightTerm='"+FreightTerm+"' ";
		if(orders!=null && orders.size()>0){
			sql = sql + "AND t3.ReferenceNo IN (";
			for(String orderNo:orders){
				sql = sql + "'" +orderNo +"',";
			}
			sql = sql.substring(0, sql.lastIndexOf(',')) + ") ";
		}
		sql = sql + "ORDER BY RequestedDate ";
		return dbUtilAutoTranSQLServer.selectMutliple(sql);
	}
	
	/**
	 * 在给定的订单范围内，获得Collect或Prepaid的订单列表
	 * @author Liang Jie
	 * @param companyID
	 * @param customerID
	 * @param orders
	 * @param FreightTerm
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getOrdersListForCollectAndPrepaid(String companyID, String customerID, List<String> orders, String FreightTerm) throws SQLException{
		String orderNoSql = "";
		if(orders!=null && orders.size()>0){
			orderNoSql = orderNoSql + "AND ReferenceNo IN (";
			for(String orderNo:orders){
				orderNoSql = orderNoSql + "'" +orderNo +"',";
			}
			orderNoSql = orderNoSql.substring(0, orderNoSql.lastIndexOf(',')) + ") ";
		}
		String sql = "SELECT CustomerID,CompanyID,OrderNo,RequestedDate,FreightTerm,Status,LoadNo,ReferenceNo  FROM Orders "
				+"WHERE (Status = 'Open' OR Status = 'Imported') AND FreightTerm='"+FreightTerm+"' AND CustomerID='"+customerID+"' AND CompanyID='"+companyID+"' "
				+orderNoSql
				+"UNION "
				+"SELECT CustomerID,CompanyID,OrderNo,RequestedDate,FreightTerm,Status,LoadNo,ReferenceNo  FROM Orders "
				+"WHERE (Status = 'Picked' OR Status = 'Picking') AND LEN(LoadNo)=0 AND FreightTerm='"+FreightTerm+"' "
				+"AND CustomerID='"+customerID+"' AND CompanyID='"+companyID+"' "
				+orderNoSql
				+"ORDER BY RequestedDate ";
		return dbUtilAutoTranSQLServer.selectMutliple(sql);
	}
	
	/**
	 * 基于仓库和订单获得订单明细
	 * @author Liang Jie
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getOrderLinesList(String orderNo, String companyId) throws SQLException{
		String sql = "SELECT orderNo,[LineNo],ItemID,LotNo,OrderedQty from OrderLines WHERE orderNo='"+orderNo+"' AND CompanyID='"+companyId+"'";
		return dbUtilAutoTranSQLServer.selectMutliple(sql);
	}
	
	/**
	 * 基于仓库和客户获得已预订订单
	 * @author Liang Jie
	 * @param customerId
	 * @param companyId
	 * @param bookedDnList
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getBookedOrderNo(String customerId, String companyId, DBRow[] bookedDnList) throws SQLException{
		String sql = "SELECT DISTINCT OrderNo FROM Orders WHERE CustomerID='"+customerId+"' AND CompanyID='"+companyId+"' ";
		if(bookedDnList!=null && bookedDnList.length>0){
			sql = sql + " AND ReferenceNo in (";
			for(DBRow row:bookedDnList){
				String dn = row.getString("ReferenceNo");
				sql = sql + "'" + dn + "',";
			}
			sql = sql.substring(0, sql.lastIndexOf(',')) + ") ";
		}
		return dbUtilAutoTranSQLServer.selectMutliple(sql);
	}
	
	/**
	 * 通过bolNo或者CtnNo查询Receipts
	 * @param bolNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptsByBolNoOrContainerNo(String bolNo, String containerNo,String CompanyID,String CustomerID,String SupplierID, String[] status) throws Exception
	{
		try
		{
			String sql = "SELECT * FROM Receipts WHERE 1=1 ";
			if(!StrUtil.isBlank(bolNo))
			{
				sql += " and rtrim(ltrim(BOLNo)) = '"+bolNo+"'";
			}
			else if(!StrUtil.isBlank(containerNo))
			{
				sql += " and rtrim(ltrim(ContainerNo)) = '"+containerNo+"'";
			}
			if(!StrUtil.isBlank(CompanyID))
			{
				sql += " and CompanyID = '"+CompanyID+"'";
			}
			if(!StrUtil.isBlank(CustomerID))
			{
				sql += " and CustomerID = '"+CustomerID+"'";
			}
			if(!StrUtil.isBlank(SupplierID))
			{
				sql += " and SupplierID = '"+SupplierID+"'";
			}
			sql += sqlStatus(status);
 			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptsByBolNoOrContainerNo error:"+e);
		}
	}
	
	/**
	 * fn_order_serialno_final_status
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年4月18日 下午4:11:37
	 */
	public String checkFnOrderSerialnoFinalStatus(long orderNo, String companyId) throws Exception
	{
		try
		{
			return dbUtilAutoTranSQLServer.CallFnOrderSerialnoFinalStatus("fn_order_serialno_final_status", companyId, orderNo);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptsByBolNoOrContainerNo error:"+e);
		}
	}

	
	/**
	 * 收货计划
	 * 已经commit的单号,用于同步状态
	 * 
	 * @author wangcr
	 * 
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getPickedOrderPK(String companyId,String customerId) throws SQLException {
		DBRow para = new DBRow();
		para.put("companyId", companyId);
		para.put("customerId", customerId);
		String sql = "select m.OrderNo, CASE WHEN CustomerID in ('SEIDIG0005','SEIDIG0006','SEIDIG0007','SEIDIG0009','TINLLC0001') and ISNULL(MasterReferenceNo, '')!='' THEN MasterReferenceNo else ReferenceNo end ReferenceNo from Orders m where Status in ('Picking','Picked') and CompanyID=? AND CustomerID=?";
		return dbUtilAutoTranSQLServer.selectPreMutliple(sql, para);
	}
	
	/**
	 * 已经Out的单号,用于同步状态
	 * 
	 * @author wangcr
	 * 
	 * @param companyId
	 * @param customerId
	 * @param OrderNoS
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getOverOrderPK(String companyId,String customerId, List<String> OrderNoS) throws SQLException {
		DBRow para = new DBRow();
		para.put("companyId", companyId);
		para.put("customerId", customerId);
		String dnSqL = "";
		
		if (OrderNoS.size()>0) {
			dnSqL+=" AND OrderNo IN (";
			for(String OrderNo: OrderNoS){
				dnSqL+= "'" +OrderNo +"',";
			}
			dnSqL = dnSqL.substring(0, dnSqL.lastIndexOf(',')) + ") ";
		} else {
			return new DBRow[0];
		}
		String sql = "select m.OrderNo, CASE WHEN CustomerID in ('SEIDIG0005','SEIDIG0006','SEIDIG0007','SEIDIG0009','TINLLC0001') and ISNULL(MasterReferenceNo, '')!='' THEN MasterReferenceNo else ReferenceNo end ReferenceNo from Orders m where Status in ('Shipped','Closed') and CompanyID=? AND CustomerID=?";
		return dbUtilAutoTranSQLServer.selectPreMutliple(sql + dnSqL, para);
	}
	
	/**
	 * 获取已经删除的Order
	 * @author wangcr
	 * @param companyId
	 * @param customerId
	 * @param OrderNoS
	 * @return
	 * @throws SQLException
	 */
	public List<String> getCloseOrders(String companyId,String customerId, List<String> OrderNoS) throws SQLException {
		List<String> rts = new ArrayList<String>();
		String dnSqL = "";
		if (OrderNoS.size()>0) {
			dnSqL+=" AND OrderNo IN (";
			for(String OrderNo: OrderNoS){
				dnSqL+= "'" +OrderNo +"',";
			}
			dnSqL = dnSqL.substring(0, dnSqL.lastIndexOf(',')) + ") ";
		} else {
			return new ArrayList<String>();
		}
		for (String OrderNo: OrderNoS) {
			rts.add(OrderNo);
		}
		DBRow para = new DBRow();
		para.put("companyId", companyId);
		para.put("customerId", customerId);
		String sql = "select OrderNo from Orders where CompanyID=? AND CustomerID=?";
		DBRow[] rows = dbUtilAutoTranSQLServer.selectPreMutliple(sql + dnSqL, para);
		for (DBRow row: rows) {
			rts.remove(row.get("OrderNo", ""));
		}
		return rts;
	}
	
	/**
	 * Pending Inbounds
	 * 
	 * @author wangcr
	 * @param customerId
	 * @param companyId
	 * @return
	 * @throws SQLException
	 */
	public DBRow[] getReceivSchedule(String customerId, String companyId) throws SQLException{
		String sql = "select m.CustomerID, m.SupplierID, l.ItemID, l.LotNo, m.ReferenceNo, m.ContainerNo, l.ExpectedQty, CONVERT(VARCHAR(10),m.ScheduledDate,101) as ScheduledDate, m.ReceiptNo, CONVERT(VARCHAR(19), m.AppointmentDate,120) as AppointmentDate, CONVERT(VARCHAR(10),m.InYardDate,101) as InYardDate "+
				" from ReceiptLines l LEFT JOIN Receipts m on l.CompanyID = m.CompanyID and l.ReceiptNo = m.ReceiptNo "+
				" where m.CompanyID =? and m.CustomerID =? and m.Status in ('Imported','Open') order by ItemID, lotNo, m.ScheduledDate";
		DBRow para = new DBRow();
		para.add("companyId", companyId);
		para.add("customerId", customerId);
		return dbUtilAutoTranSQLServer.selectPreMutliple(sql, para);
	}
	
	/**
	 * 通过Load查询DN
	 * @param loadCompany
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年5月10日 下午2:41:41
	 */
	public DBRow[] findDNByLoadCompanyIds(String[] loadCompany) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select od.ReferenceNo,od.CompanyID,max(od.CustomerID)CustomerID,max(od.CarrierID)CarrierID,max(od.ShipToID)ShipToID,max(od.PONo)PONo,max(m.LoadNo)LoadNo ");
			sql.append(" from MasterBOLs m");
			sql.append(" join MasterBOLLines ml on ml.MasterBOLNo = m.MasterBOLNo and ml.CompanyID = m.CompanyID");
			sql.append(" join Orders od on od.OrderNo = ml.OrderNo and od.CompanyID = ml.CompanyID");
			sql.append(" where 1=1");
			sql.append(" and m.LoadNo+m.CompanyID in (");
			sql.append(convertStringArrayToString(loadCompany));
			sql.append(" )");
			
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findDNByLoadCompanyIds error:"+e);
		}
	}
	
	/**
	 * 通过Pono查询DN
	 * @param ponoCompany
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年5月10日 下午3:32:51
	 */
	public DBRow[] findDNByPonoCompanyIds(String[] ponoCompany) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select od.ReferenceNo,od.CompanyID,max(od.CustomerID)CustomerID,max(od.CarrierID)CarrierID,max(od.ShipToID)ShipToID,max(od.PONo)PONo, '' as LoadNo");
			sql.append(" from Orders od");
			sql.append(" left join MasterBOLLines ml on od.OrderNo = ml.OrderNo and od.CompanyID = ml.CompanyID");
			sql.append(" where 1=1");
			sql.append(" and ml.MasterBOLNo is null");
			sql.append(" and od.PONo+od.CompanyID in (");
			sql.append(convertStringArrayToString(ponoCompany));
			sql.append(" )");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			throw new Exception("FloorSQLServerMgrZJ.findDNByPonoCompanyIds error:"+e);
		}
	}
	
	
	/**
	 * 通过Order查询DN
	 * @param orderCompany
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年5月10日 下午3:32:51
	 */
	public DBRow[] findDNByOrderCompanyIds(String[] orderCompany) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select od.ReferenceNo,od.CompanyID,max(od.CustomerID)CustomerID,max(od.CarrierID)CarrierID,max(od.ShipToID)ShipToID,max(od.PONo)PONo, '' as LoadNo");
			sql.append(" from Orders od");
			sql.append(" left join MasterBOLLines ml on od.OrderNo = ml.OrderNo and od.CompanyID = ml.CompanyID");
			sql.append(" where 1=1");
			sql.append(" and ml.MasterBOLNo is null");
			sql.append(" and convert(varchar(10),od.OrderNo)+od.CompanyID in (");
			sql.append(convertStringArrayToString(orderCompany));
			sql.append(" )");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			throw new Exception("FloorSQLServerMgrZJ.findDNByOrderCompanyIds error:"+e);
		}
	}
	
	/**
	 * 通过loadCompany,ponoCompany,orderCompany查询DN
	 * @param loadCompany
	 * @param ponoCompany
	 * @param orderCompany
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年5月10日 下午3:46:09
	 */
	public DBRow[] findDNByLoadCompanyPoCompanyOrderCompany(String[] loadCompany, String[] ponoCompany, String[] orderCompany) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select od.ReferenceNo,od.CompanyID,max(od.CustomerID)CustomerID,max(od.CarrierID)CarrierID,max(od.ShipToID)ShipToID,max(od.PONo) PONo,max(od.OrderNo) OrderNo,max(m.LoadNo)LoadNo ");
			sql.append(" from MasterBOLs m");
			sql.append(" join MasterBOLLines ml on ml.MasterBOLNo = m.MasterBOLNo and ml.CompanyID = m.CompanyID");
			sql.append(" join Orders od on od.OrderNo = ml.OrderNo and od.CompanyID = ml.CompanyID");
			sql.append(" where 1=1");
			sql.append(" and m.LoadNo+m.CompanyID in (");
			sql.append(StrUtil.isBlank(convertStringArrayToString(loadCompany))?"''":convertStringArrayToString(loadCompany));
			sql.append(" ) GROUP BY od.ReferenceNo,od.CompanyID ");
			sql.append(" union");
			sql.append(" select od.ReferenceNo,od.CompanyID,max(od.CustomerID)CustomerID,max(od.CarrierID)CarrierID,max(od.ShipToID)ShipToID,max(od.PONo) PONo,'' OrderNo,'' LoadNo");
			sql.append(" from Orders od");
			sql.append(" left join MasterBOLLines ml on od.OrderNo = ml.OrderNo and od.CompanyID = ml.CompanyID");
			sql.append(" where 1=1");
			sql.append(" and ml.MasterBOLNo is null");
			sql.append(" and od.PONo+od.CompanyID in (");
			sql.append(StrUtil.isBlank(convertStringArrayToString(ponoCompany))?"''":convertStringArrayToString(ponoCompany));
			sql.append(" ) GROUP BY od.ReferenceNo,od.CompanyID ");
			sql.append(" union");
			sql.append(" select od.ReferenceNo,od.CompanyID,max(od.CustomerID)CustomerID,max(od.CarrierID)CarrierID,max(od.ShipToID)ShipToID,'' PONo,max(od.OrderNo) OrderNo, '' LoadNo");
			sql.append(" from Orders od");
			sql.append(" left join MasterBOLLines ml on od.OrderNo = ml.OrderNo and od.CompanyID = ml.CompanyID");
			sql.append(" where 1=1");
			sql.append(" and ml.MasterBOLNo is null");
			sql.append(" and convert(varchar(10),od.OrderNo)+od.CompanyID in (");
			sql.append(StrUtil.isBlank(convertStringArrayToString(orderCompany))?"''":convertStringArrayToString(orderCompany));
			sql.append(" ) GROUP BY od.ReferenceNo,od.CompanyID ");
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			throw new Exception("FloorSQLServerMgrZJ.findDNByLoadCompanyPoCompanyOrderCompany error:"+e);
		}
	}
	
	
	public DBRow[] getMasterBOLNos( DBRow[] filterRows) throws Exception
	{
		DBRow[] rows = null;
		try
		{
			
			if(filterRows!=null && filterRows.length>0){
				String sqlTempTable="IF not EXISTS  (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb..#filter') and type='U')"
						+ "Create table #filter(LoadNo nvarchar(20)  COLLATE SQL_Latin1_General_CP1_CI_AS,CompanyID nvarchar(20)  COLLATE SQL_Latin1_General_CP1_CI_AS,CustomerID nvarchar(20)  COLLATE SQL_Latin1_General_CP1_CI_AS);";
				dbUtilAutoTranSQLServer.executeSQL(sqlTempTable);
				dbUtilAutoTranSQLServer.executeSQL("delete from #filter");
				for(int i=0;i<filterRows.length;i++){
					DBRow row = filterRows[i];
					String sqlInsert="insert into  #filter(LoadNo,CompanyID,CustomerID)values('"+row.getString("load_no")+"','"+row.getString("company_name")+"','"+row.getString("customer_id")+"')";
					//System.out.println(sqlInsert);
					dbUtilAutoTranSQLServer.executeSQL(sqlInsert);
				}
				
				String sql="  select   a.LoadNo, a.CompanyID, a.CustomerID, AccountID, ShipToID, ShipToName, stuff(( select ','+ CAST( MasterBOLNo as nvarchar (20 )) from MasterBOLs b "  
						+" where a.LoadNo= b. LoadNo "
								+" and a. CompanyID= b. CompanyID "
								+" and a. CustomerID= b. CustomerID "
								+" and a. AccountID= b. AccountID "
								+" and a. ShipToID= b. ShipToID "
								+" and a. ShipToName= b. ShipToName "
								+" for xml path( '')   ) , 1, 1, '')   as MasterBols "
								+" from MasterBOLs  a "
								+" inner join #filter b on a.CompanyID=b.CompanyID and a.CustomerID=b.CustomerID and a.LoadNo=b.LoadNo"
								+" group by  a.LoadNo, a.CompanyID, a.CustomerID, AccountID, ShipToID, ShipToName "
								+" order by a.LoadNo, a.CompanyID, a.CustomerID asc ";
				rows = dbUtilAutoTranSQLServer.selectMutliple(sql);
			 }
			
		}
		catch(Exception e)
		{
			throw new Exception("FloorSQLServerMgrZJ.findDNByLoadCompanyPoCompanyOrderCompany error:"+e);
		}
		return rows;
	}
	
	public String createTempTable(String tableName,String[] loadCompany) throws SQLException{
		
		String sql="IF not EXISTS  (select * from tempdb.dbo.sysobjects where id = object_id(N'tempdb.."+tableName+"') and type='U')"  
					+"create table "+tableName+" (LoadNo varchar(50) COLLATE  SQL_Latin1_General_CP1_CI_AS ,Company varchar(50) COLLATE  SQL_Latin1_General_CP1_CI_AS);";
		dbUtilAutoTranSQLServer.executeSQL(sql);
		if(loadCompany!=null ){
			for(int i=0;i<loadCompany.length;i++){
				String [] arr = loadCompany[i].split("-");
				String insertSql="insert into "+tableName+" values('"+arr[0]+"','"+arr[1]+"');";
				dbUtilAutoTranSQLServer.executeSQL(insertSql);
				//System.out.println(insertSql);
				sql+="  "+insertSql;
			}
		}
		return sql;
	}
	
	
	/*public DBRow[] findDNByLoadCompanyPoCompanyOrderCompany(String[] loadCompany, String[] ponoCompany, String[] orderCompany,String referenceNo) throws Exception
	{
		try
		{
			createTempTable("#loadCompany",loadCompany);
			createTempTable("#ponoCompany",ponoCompany);
			createTempTable("#orderCompany",orderCompany);
			
			
			StringBuffer sql = new StringBuffer();
			sql.append("select od.ReferenceNo,od.CompanyID,max(od.CustomerID)CustomerID,max(od.CarrierID)CarrierID,max(od.ShipToID)ShipToID,max(od.PONo) PONo,max(od.OrderNo) OrderNo,max(m.LoadNo)LoadNo ");
			sql.append(" from MasterBOLs m");
			sql.append(" join #loadCompany lc on m.LoadNo=lc.LoadNo and m.CompanyID = lc.Company");
			sql.append(" join MasterBOLLines ml on ml.MasterBOLNo = m.MasterBOLNo and ml.CompanyID = m.CompanyID");
			sql.append(" join Orders od on od.OrderNo = ml.OrderNo and od.CompanyID = ml.CompanyID");
			sql.append(" where 1=1");
			if(!StrUtil.isBlank(referenceNo)){
				sql.append(" and od.ReferenceNo='"+referenceNo+"'");
			}
	
			sql.append(" GROUP BY od.ReferenceNo,od.CompanyID ");
			sql.append(" union");
			sql.append(" select od.ReferenceNo,od.CompanyID,max(od.CustomerID)CustomerID,max(od.CarrierID)CarrierID,max(od.ShipToID)ShipToID,max(od.PONo) PONo,'' OrderNo,'' LoadNo");
			sql.append(" from Orders od");
			sql.append(" inner join #ponoCompany pc on od.PONo=pc.LoadNo and od.CompanyID = pc.Company");
			sql.append(" left join MasterBOLLines ml on od.OrderNo = ml.OrderNo and od.CompanyID = ml.CompanyID");
			
			sql.append(" where 1=1");
			if(!StrUtil.isBlank(referenceNo)){
				sql.append(" and od.ReferenceNo='"+referenceNo+"'");
			}
			sql.append(" and ml.MasterBOLNo is null");
			
			sql.append(" GROUP BY od.ReferenceNo,od.CompanyID ");
			sql.append(" union");
			sql.append(" select od.ReferenceNo,od.CompanyID,max(od.CustomerID)CustomerID,max(od.CarrierID)CarrierID,max(od.ShipToID)ShipToID,'' PONo,max(od.OrderNo) OrderNo, '' LoadNo");
			sql.append(" from Orders od");
			sql.append(" inner join #orderCompany oc on od.OrderNo=oc.LoadNo and od.CompanyID = oc.Company");
			sql.append(" left join MasterBOLLines ml on od.OrderNo = ml.OrderNo and od.CompanyID = ml.CompanyID");
			
			sql.append(" where 1=1");
			if(!StrUtil.isBlank(referenceNo)){
				sql.append(" and od.ReferenceNo='"+referenceNo+"'");
			}
			sql.append(" and ml.MasterBOLNo is null");
		
			sql.append(" GROUP BY od.ReferenceNo,od.CompanyID ");
			System.out.println(sql.toString());
			return dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
		}
		catch(Exception e)
		{
			throw new Exception("FloorSQLServerMgrZJ.findDNByLoadCompanyPoCompanyOrderCompany error:"+e);
		}
	}*/
	
	
	public DBRow[] findDNByLoadCompanyPoCompanyOrderCompany(String[] loadCompany, String[] ponoCompany, String[] orderCompany,String referenceNo) throws Exception
	{
		try
		{
			createTempTable("#loadCompany",loadCompany);
			createTempTable("#ponoCompany",ponoCompany);
			createTempTable("#orderCompany",orderCompany);
			
			
			StringBuffer sql = new StringBuffer();
			sql.append("select od.ReferenceNo,od.CompanyID,od.CustomerID as CustomerID,od.CarrierID as CarrierID,od.ShipToID as ShipToID,od.PONo as PONo,od.OrderNo as  OrderNo,m.LoadNo as LoadNo,m.ShipToID as mShipToID,m.ShipToName as ShipToName,od.WarehouseID ");
			sql.append(" from MasterBOLs m");
			sql.append(" join #loadCompany lc on m.LoadNo=lc.LoadNo and m.CompanyID = lc.Company");
			sql.append(" join MasterBOLLines ml on ml.MasterBOLNo = m.MasterBOLNo and ml.CompanyID = m.CompanyID");
			sql.append(" join Orders od on od.OrderNo = ml.OrderNo and od.CompanyID = ml.CompanyID");
			sql.append(" where 1=1");
			if(!StrUtil.isBlank(referenceNo)){
				sql.append(" and od.ReferenceNo='"+referenceNo+"'");
			}
	
			
			sql.append(" union all");
			sql.append(" select od.ReferenceNo,od.CompanyID,od.CustomerID as CustomerID,od.CarrierID as CarrierID,od.ShipToID as ShipToID,od.PONo as PONo,''  as OrderNo,''  as LoadNo,m.ShipToID as mShipToID,m.ShipToName as ShipToName ,od.WarehouseID ");
			sql.append(" from Orders od");
			sql.append(" inner join #ponoCompany pc on od.PONo=pc.LoadNo and od.CompanyID = pc.Company");
			sql.append(" left join MasterBOLLines ml on od.OrderNo = ml.OrderNo and od.CompanyID = ml.CompanyID");
			sql.append(" left join  MasterBOLs m on m.MasterBOLNo = ml.MasterBOLNo");
			sql.append(" where 1=1");
			if(!StrUtil.isBlank(referenceNo)){
				sql.append(" and od.ReferenceNo='"+referenceNo+"'");
			}
			sql.append(" and ml.MasterBOLNo is null");
			
		
			sql.append(" union all");
			sql.append(" select od.ReferenceNo,od.CompanyID,od.CustomerID as CustomerID,od.CarrierID as CarrierID,od.ShipToID as ShipToID,'' as PONo,od.OrderNo as OrderNo, '' as LoadNo, m.ShipToID as mShipToID,m.ShipToName as ShipToName,od.WarehouseID  ");
			sql.append(" from Orders od");
			sql.append(" inner join #orderCompany oc on od.OrderNo=oc.LoadNo and od.CompanyID = oc.Company");
			sql.append(" left join MasterBOLLines ml on od.OrderNo = ml.OrderNo and od.CompanyID = ml.CompanyID");
			sql.append(" left join  MasterBOLs m on m.MasterBOLNo = ml.MasterBOLNo");
			
			sql.append(" where 1=1");
			if(!StrUtil.isBlank(referenceNo)){
				sql.append(" and od.ReferenceNo='"+referenceNo+"'");
			}
			sql.append(" and ml.MasterBOLNo is null");
			String sql2="select ReferenceNo," 
				       +"CompanyID,"
					   +"max( CustomerID) CustomerID,"
					   +"max( CarrierID) CarrierID,"
				       +"max( ShipToID) ShipToID,"
				       +"max( PONo) PONo ,"
				       +"max( OrderNo) OrderNo ,"
				       +"max( LoadNo) LoadNo , "
				       +"MAX(mShipToID) as mShipToID,MAX(ShipToName) as ShipToName,"
				       +"max( WarehouseID) WarehouseID  "
				       +"from ("+sql.toString() +") tb GROUP BY ReferenceNo,CompanyID";
			
			System.out.println(sql2);
			DBRow[] rows = dbUtilAutoTranSQLServer.selectMutliple(sql.toString());
			dbUtilAutoTranSQLServer.executeSQL("drop table #loadCompany");
			dbUtilAutoTranSQLServer.executeSQL("drop table #ponoCompany");
			dbUtilAutoTranSQLServer.executeSQL("drop table #orderCompany");
			return rows ;
			
		}
		catch(Exception e)
		{
			throw new Exception("FloorSQLServerMgrZJ.findDNByLoadCompanyPoCompanyOrderCompany error:"+e);
		}
	}
	
	
	private String convertStringArrayToString(String[] orderCompany)
	{
		String str = "";
		if(null != orderCompany && orderCompany.length > 0)
		{
			for (int i = 0; i < orderCompany.length; i++)
			{
				str += ",'"+orderCompany[i].replace("'", "")+"'";
			}
		}
		if(!StrUtil.isBlank(str))
		{
			str = str.substring(1);
		}
		return str;
	} 
	/* 通过 companys，customers， carrier，ship_time查询Order
	 * @param companys
	 * @param customers
	 * @param carrier
	 * @param ship_time
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年5月10日 下午4:45:35
	 */
	public DBRow[] findOrderByCompanyCustomerCarrierDate(String[] companys, String[] customers, String[] carrier, String ship_time) throws Exception
	{
		try
		{
			String sql = " select * from Orders where 1=1";
			if(null != companys && companys.length > 0)
			{
				sql += sqlCompanyId(companys);
			}
			if(null != customers && customers.length > 0)
			{
				sql += sqlCustomerId(customers);
			}
			if(null != carrier && carrier.length > 0)
			{
				sql += sqlCarrierId(carrier);
			}
			if(!StrUtil.isBlank(ship_time))
			{
				sql += " and Convert(varchar(19),ShippedDate,120) >= '"+ship_time+" 00:00:00'";
	//			sql += " and Convert(varchar(19),ShippedDate,120) <= '"+ship_time+" 23:59:59'";
			}
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderByCompanyCustomerCarrierDate error:"+e);
		}
	}
	
	
	
	public String sqlCustomerId(String[] customerId)
	{
		String sql = "";
		if(null != customerId && customerId.length > 0)
		{
			sql += " and (CustomerID = '" + customerId[0]+"'";
			for (int i = 1; i < customerId.length; i++)
			{
				sql += " or CustomerID = '" + customerId[i]+"'";
			}
			sql += ")";
		}
		return sql;
	}
	
	
	public String sqlCarrierId(String[] carrierID)
	{
		String sql = "";
		if(null != carrierID && carrierID.length > 0)
		{
			sql += " and (CarrierID = '" + carrierID[0]+"'";
			for (int i = 1; i < carrierID.length; i++)
			{
				sql += " or CarrierID = '" + carrierID[i]+"'";
			}
			sql += ")";
		}
		return sql;
	}
	
	/**
	 * 通过OrderNo，companyId查询TrackingNo
	 * @param orderNo
	 * @param companyId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年5月15日 下午3:03:40
	 */
	public DBRow[] findOrderTrackingNosByOrderNoCompany(long orderNo, String companyId) throws Exception
	{
		try
		{
			String sql = " select * from OrderCartons where OrderNo = "+orderNo +" and CompanyID = '"+companyId+"'";
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findOrderTrackingNosByOrderNoCompany error:"+e);
		}
	}
	

	/**
	 *  通过ReceiptNo、CompanyId，customerId……查询Receipts
	 * @param bolNo
	 * @param containerNo
	 * @param receiptNo
	 * @param companyId
	 * @param customerId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年5月22日 下午3:47:36
	 */
	public DBRow[] findReceiptsByReceiptNoBolCtnr(String bolNo, String containerNo, long receiptNo, String companyId, String customerId,String[] status) throws Exception
	{
		try
		{
			String sql = "SELECT * FROM Receipts WHERE 1=1 ";
			if(!StrUtil.isBlank(bolNo))
			{
				sql += " and rtrim(ltrim(BOLNo)) = '"+bolNo+"'";
			}
			else if(!StrUtil.isBlank(containerNo))
			{
				sql += " and rtrim(ltrim(ContainerNo)) = '"+containerNo+"'";
			}
			if(!StrUtil.isBlank(companyId))
			{
				sql += " and CompanyID = '"+companyId+"'";
			}
			if(!StrUtil.isBlank(customerId))
			{
				sql += " and CustomerID = '"+customerId+"'";
			}
			if(receiptNo > 0)
			{
				sql += " and ReceiptNo = "+receiptNo;
			}
			sql += sqlStatus(status);
//			System.out.println("RN:"+receiptNo+",bolNo:"+bolNo+",ctnr:"+containerNo);
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSQLServerMgrZJ.findReceiptsByReceiptNoBolCtnr error:"+e);
		}
	}

	public void setDbUtilAutoTranSQLServer(
			DBUtilAutoTranSQLServer dbUtilAutoTranSQLServer) {
		this.dbUtilAutoTranSQLServer = dbUtilAutoTranSQLServer;
	}
	
	public DBRow[] findPOTypeOfDN(String companyId) throws Exception{
		try{
			String sql="SELECT od.companyID, od.orderNo,od.loadNo,od.referenceNo,od.accountID,ve.external_id,od.PONo "
					+ " FROM orders od right JOIN vizio_extension ve on od.referenceNo=ve.vizio_dn "
					+ " where od.status<>'Closed' and  od.companyID in (" + companyId + ") order by companyID ";
			return dbUtilAutoTranSQLServer.selectMutliple(sql);
		}catch(Exception ex){
			throw new Exception("FloorSQLServerMgrZJ.findPOTypeOfDN error:"+ex);
		}
	}
}
