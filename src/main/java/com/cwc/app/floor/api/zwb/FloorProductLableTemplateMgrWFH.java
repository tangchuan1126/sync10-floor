package com.cwc.app.floor.api.zwb;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.StrUtil;

public class FloorProductLableTemplateMgrWFH  {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBUtilAutoTran getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public DBRow[] findAllLable(String lableName,String type,int templateType,PageCtrl pc) throws Exception {
		try{
			String sql = "select * from "+ConfigBean.getStringValue("lable_template_base")+" where 1=1 ";
			if(!StrUtil.isBlank(lableName))
			{
				sql += " and lable_name='"+lableName+"' ";
			}
			
			if(!StrUtil.isBlank(type))
			{
				sql += " and type='"+type+"' ";
			}
			if(templateType!=0){
				sql += " and lable_template_type="+templateType;
			}
//			System.out.println(sql);
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}catch(Exception e){
			throw new Exception("FloorProductLableTemplateMgrWFH.findAllLable(row) error:" + e);
		}
	}

	//新建基础标签模板信息
	public long addLableTemp(DBRow row) throws Exception
	{
		try {
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("lable_template_base"), row);
		} catch (Exception e) {
			throw new Exception("FloorProductLableTemplateMgrWFH addLableTemp"+e);
		}
	}
	
	//根据id查询基础标签模板信息
	public DBRow getLableTempById(long id)throws Exception
	{
		try {
			String sql="select * from "+ConfigBean.getStringValue("lable_template_base")+" where lable_id="+id;
			return this.dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorProductLableTemplateMgrWFH getLableTempById"+e);
		}
	}
	
	//根据id修改
	public void modifyLableTemp(long id,DBRow row) throws Exception{
		try {
			dbUtilAutoTran.update(" where lable_id="+id, ConfigBean.getStringValue("lable_template_base"), row);
		} catch (Exception e) {
			throw new Exception("FloorProductLableTemplateMgrWFH modifyLableTemp"+e);
		}
	}
	
	//根据id删除
	public void delLabelTemp(long id)throws Exception{
		try {
			dbUtilAutoTran.delete(" where lable_id="+id, ConfigBean.getStringValue("lable_template_base"));
		} catch (Exception e) {
			throw new Exception("FloorProductLableTemplateMgrWFH delLabelTemp"+e);
		}
	}
}
