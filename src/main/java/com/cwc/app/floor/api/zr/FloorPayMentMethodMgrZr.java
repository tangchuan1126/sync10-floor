package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorPayMentMethodMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public DBRow[] getAllPayMentMethod(PageCtrl page) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("account_payee");
			String sql = "select * from " + tableName ;
			return dbUtilAutoTran.selectMutliple(sql, page);
		}catch(Exception e){
			throw new Exception("FloorPayMentMethodMgrZr.getAllPayMentMethod(page):"+e);
		}
	}
	public DBRow[] getAllPayMentMethodByKey(int key , PageCtrl page , int isTip) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("account_payee");
			StringBuffer sql = new StringBuffer("select * from ").append(tableName).append(" where 1=1 ");
			if(key != -1){
				sql.append(" and key_type=").append(key);
			}
			if(isTip == 1){
				sql.append(" and is_tip = '1' ");
			}else if(isTip == 0){
				sql.append(" and is_tip = '0' ");
			}
			return dbUtilAutoTran.selectMutliple(sql.toString(), page);
		 
		}catch (Exception e) {
			throw new Exception("FloorPayMentMethodMgrZr.getAllPayMentMethodByKey(key,page):"+e);
		}
	}
	
	public void updatePayMentMethodByDBRow(DBRow row,int key) throws Exception{
		try{
			String tablename = ConfigBean.getStringValue("account_payee");
			dbUtilAutoTran.update(" where id="+key, tablename, row);
			
		}catch (Exception e) {
			throw new Exception("FloorPayMentMethodMgrZr.updatePayMentMethodByDBRow():"+e);
		}
	}
	public void addPayMethod(DBRow row) throws Exception{
		try{
			String tablename = ConfigBean.getStringValue("account_payee");
			dbUtilAutoTran.insert(tablename, row);
		}catch (Exception e) {
			throw new Exception("FloorPayMentMethodMgrZr.addPayMethod(row):"+e);
		}
	}
	public boolean isHasTipPayMentMethod(int key) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("account_payee");
			String sql =  "select * from " + tableName + " where key_type="+key + " and is_tip = '1'";
			DBRow row  = dbUtilAutoTran.selectSingle(sql);
			return row != null;
		}catch (Exception e) {
			throw new Exception("FloorPayMentMethodMgrZr.addPayMethod(row):"+e);
		}
		
	}
	public void deletePayMentMethod(long id) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("account_payee");
			dbUtilAutoTran.delete(" where id = " + id, tableName);
			 
		}catch (Exception e) {
			throw new Exception("FloorPayMentMethodMgrZr.deletePayMentMethod(id):"+e);
		}
	}
}
