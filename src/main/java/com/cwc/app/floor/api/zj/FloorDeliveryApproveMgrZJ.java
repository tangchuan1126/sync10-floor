package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorDeliveryApproveMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 添加交货单审核
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addDeliveryApprove(DBRow dbrow)
		throws Exception
	{
		try
		{
			long da_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("delivery_approve"));
			
			dbrow.add("da_id",da_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("delivery_approve"),dbrow);
			
			return (da_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDeliveryApproveMgrZJ addDeliveryApprove error:"+e);
		}
	}
	
	/**
	 * 添加交货单审核具体差异
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addDeliveryApproveDetail(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long dad_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("delivery_approve_detail"));
			
			dbrow.add("dad_id",dad_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("delivery_approve_detail"),dbrow);
			
			return (dad_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDeliveryApproveMgrZJ addDeliveryApproveDetail error:"+e);
		}
	}
	
	/**
	 * 修改交货单审核
	 * @param pa_id
	 * @param dbrow
	 * @throws Exception
	 */
	public void modDeliveryApprove(long da_id,DBRow dbrow)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where da_id ="+da_id,ConfigBean.getStringValue("delivery_approve"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDeliveryApproveMgrZJ modDeliveryApprove error:"+e);
		}
	}
	
	/**
	 * 修改交货审核具体差异
	 * @param pdd_id
	 * @param dbrow
	 * @throws Exception
	 */
	public void modDeliveryApproveDetail(long dad_id,DBRow dbrow)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where dad_id ="+dad_id,ConfigBean.getStringValue("delivery_approve_detail"),dbrow);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorPurchaseApproveMgrZJ modPurchaseApproveDifference error:"+e);
		}
	}
	
	/**
	 * 获得交货单审核，支持提交时间、审核时间排序，可按仓库过滤
	 * @param ps_id
	 * @param pc
	 * @param approve_status
	 * @param sorttype
	 * @return
	 * @throws Exception
	 */
	public DBRow[] allDeliveryApprove(long ps_id,PageCtrl pc,int approve_status,String sorttype)
		throws Exception
	{
		StringBuffer sql = new StringBuffer("select da.*,ps.title as title,do.delivery_order_number as number from "+ConfigBean.getStringValue("delivery_approve")+" as da,"+ConfigBean.getStringValue("delivery_order")+" as do,"+ConfigBean.getStringValue("purchase")+" as p,"+ConfigBean.getStringValue("product_storage_catalog")+" as ps"
					 +" where da.delivery_order_id = do.delivery_order_id and do.delivery_purchase_id = p.purchase_id and ps.id = p.ps_id");
		
		if(ps_id>0)
		{
			sql.append(" and p.ps_id = "+ps_id);
		}
		
		if(!(approve_status<0))
		{
			sql.append(" and da.approve_status = "+approve_status);
		}
		
		if(sorttype.equals("subdate"))
		{
			sql.append(" order by da.commit_date desc");
		}
		else if(sorttype.equals("approvedate"))
		{
			sql.append(" order by da.approve_date desc");
		}
		else
		{
			sql.append(" order by da.da_id desc");
		}
		
		return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
	}
	
	/**
	 * 根据审核单ID获得审核单详细
	 * @param pa_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailDeliveryApproveByDaid(long da_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("delivery_approve")+" where da_id = ?";
		DBRow para = new DBRow();
		para.add("da_id",da_id);
		
		return (dbUtilAutoTran.selectPreSingle(sql, para));
	}
	
	/**
	 * 根据审核单ID获得交货审核单获得审核单明细
	 * @param da_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryApproverDetailsByDaid(long da_id,PageCtrl pc)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("delivery_approve_detail")+" where da_id = ?";
		
		DBRow para = new DBRow();
		para.add("da_id",da_id);
		
		return (dbUtilAutoTran.selectPreMutliple(sql, para,pc));
	}
	
	/**
	 * 根据审核状态查询交货单差异
	 * @param pa_id
	 * @param approve_status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDeliveryApproveDetailsWithStatus(long da_id,int approve_status)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("delivery_approve_detail")+" where da_id = ? and approve_status = ?";
		DBRow para = new DBRow();
		para.add("da_id",da_id);
		para.add("approve_status",approve_status);
		
		return (dbUtilAutoTran.selectPreMutliple(sql, para));
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
