package com.cwc.app.floor.api;

import static us.monoid.web.Resty.content;
import static us.monoid.web.Resty.put;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;
import us.monoid.web.Content;
import us.monoid.web.JSONResource;
import us.monoid.web.Resty;

public class FloorLogMgr implements FloorLogMgrIFace {
	private String restBaseUrl;
	private StringRedisTemplate redisTemplate;
	private SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	private Resty client = new Resty();
	
	private RedisMessageListenerContainer redisMessageListenerContainer;
	
	private MessageListenerAdapter  redisMessageListenerAdapter;

	private boolean mock = false;

	public boolean isMock() {
		return mock;
	}

	public void setMock(boolean mock) {
		this.mock = mock;
	}

	public StringRedisTemplate getRedisTemplate() {
		return redisTemplate;
	}

	public void setRedisTemplate(StringRedisTemplate redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	public String getRestBaseUrl() {
		return restBaseUrl;
	}

	public void setRestBaseUrl(String restBaseUrl) {
		this.restBaseUrl = restBaseUrl;
	}

	
	public void initAsync() {
		ChannelTopic eventTopic = new ChannelTopic("logserv:event");
		//如果被注入了，则进行异步监听初始化
		if(redisMessageListenerContainer != null && redisMessageListenerAdapter !=null){
			redisMessageListenerAdapter.setDelegate(this);
			redisMessageListenerAdapter.setDefaultListenerMethod("asyncLogReceived");
			redisMessageListenerContainer.addMessageListener(
				redisMessageListenerAdapter, eventTopic);
		}
	}

	private String addLog(Object logBean) throws Exception {
		JSONObject logJSON = new JSONObject(logBean);
		// 注意：提交日志因可能有延迟，所以应立即产生时间戳，而不是依赖服务端产生时间戳
		logJSON.put("post_date", dateFormat.format(new Date()));
		int i1 = logBean.getClass().getName().lastIndexOf(".");
		int i2 = logBean.getClass().getName().lastIndexOf("Bean");
		String modelName = logBean.getClass().getName().substring(i1 + 1, i2);
		JSONResource result = client.json(
				restBaseUrl + "/log_mgr/" + modelName, put(content(logJSON)));
		return (String) result.get("id");
	}

	public void addLogAsync(Object logBean) throws Exception {
		addLogAsync(logBean, new Date());
	}

	public void addLogAsync(Object logBean, Date post_date) throws Exception {
		if (mock)
			return;
		JSONObject logJSON = new JSONObject(logBean);
		// 注意：异步提交日志因可能有延迟，所以应立即产生时间戳，而不是依赖服务端产生时间戳
		logJSON.put("post_date", dateFormat.format(post_date));
		int i1 = logBean.getClass().getName().lastIndexOf(".");
		int i2 = logBean.getClass().getName().lastIndexOf("Bean");
		String modelName = logBean.getClass().getName().substring(i1 + 1, i2);
		JSONObject msgJSON = new JSONObject();
		msgJSON.put("modelName", modelName);
		msgJSON.put("logJSON", logJSON);
		// 向队列中写入数据
		redisTemplate.boundListOps("logserv:queue")
				.leftPush(msgJSON.toString());
		// 发出通知，以激活队列的消费者
		redisTemplate.convertAndSend("logserv:event", "addLog");
	}

	public RedisMessageListenerContainer getRedisMessageListenerContainer() {
		return redisMessageListenerContainer;
	}

	public void setRedisMessageListenerContainer(
			RedisMessageListenerContainer redisMessageListenerContainer) {
		this.redisMessageListenerContainer = redisMessageListenerContainer;
	}

	public MessageListenerAdapter getRedisMessageListenerAdapter() {
		return redisMessageListenerAdapter;
	}

	public void setRedisMessageListenerAdapter(
			MessageListenerAdapter redisMessageListenerAdapter) {
		this.redisMessageListenerAdapter = redisMessageListenerAdapter;
	}

	@Override
	public String addLog(Object logBean, boolean async) throws Exception {
		if (async) {
			addLogAsync(logBean);
			return null;
		} else {
			return addLog(logBean);
		}
	}

	@Override
	public String addLog(String modelName, JSONObject logJson, boolean async)
			throws Exception {
		if (async) {
			addLogAsync(modelName, logJson);
			return null;
		} else {
			return addLog(modelName, logJson);
		}
	}

	// 同步提交日志
	private String addLog(String modelName, JSONObject logJson)
			throws Exception {
		// 注意：提交日志因可能有延迟，所以应立即产生时间戳，而不是依赖服务端产生时间戳
		logJson.put("post_date", dateFormat.format(new Date()));
		JSONResource result = client.json(
				restBaseUrl + "/log_mgr/" + modelName, put(content(logJson)));
		return (String) result.get("id");
	}

	// 异步提交日志
	public void addLogAsync(String modelName, JSONObject logJson)
			throws Exception {
		if (mock)
			return;
		// 注意：异步提交日志因可能有延迟，所以应立即产生时间戳，而不是依赖服务端产生时间戳
		logJson.put("post_date", dateFormat.format(new Date()));
		JSONObject msgJSON = new JSONObject();
		msgJSON.put("modelName", modelName);
		msgJSON.put("logJSON", logJson);
		// 向队列中写入数据
		redisTemplate.boundListOps("logserv:queue")
				.leftPush(msgJSON.toString());
		// 发出通知，以激活队列的消费者
		redisTemplate.convertAndSend("logserv:event", "addLog");
	}

	
	public void asyncLogReceived(String event) throws Exception {
		String message = null;
		Map<String, JSONArray> modelName2Log = new HashMap<String, JSONArray>();

		do {

			BoundListOperations<String, String> ops = redisTemplate
					.boundListOps("logserv:queue");
			if ((message = ops.rightPop()) != null) {
				JSONObject msgJSON = new JSONObject(message);
				String modelName = msgJSON.getString("modelName");
				JSONObject logJSON = msgJSON.getJSONObject("logJSON");

				if (!modelName2Log.containsKey(modelName)) {
					modelName2Log.put(modelName, new JSONArray());
				}
				modelName2Log.get(modelName).put(logJSON);
			} else
				break;
		} while (message != null);

		for (String modelName : modelName2Log.keySet()) {
			client.json(restBaseUrl + "/log_mgr/"
					+ modelName, new Content("application/json", modelName2Log
					.get(modelName).toString().getBytes("UTF-8")));
		}

	}

	
}
