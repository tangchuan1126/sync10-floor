package com.cwc.app.floor.api.ccc;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorFreightCostMgrCCC {
	
	private DBUtilAutoTran dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	/**
	 * 添加货运运费
	 * @param dbrow
	 * @throws Exception
	 */
	public void addFreightCost(DBRow dbrow) throws Exception{
		try 
		{
		//	long freight_resources_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("freight_resources"));
			
			//dbrow.add("fr_id",freight_resources_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("freight_cost"),dbrow);
			
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorFreightCostMgrCCC addFreightCost error:"+e);
		}
	}
	
	/**
	 * 根据条件查询所有的货运运费分页列表
	 * @param fr_id 货运资源主键ID
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	 public DBRow[] getFreightCostByCondition(String fr_id, PageCtrl pc)
	    throws Exception
	    {
	    	try
	    	{
	    		StringBuffer sqlCondition=new StringBuffer();
	    		sqlCondition.append(" where 1=1");
	    		if(!fr_id.equals("0"))
	    			sqlCondition.append(" and fr_id = "+fr_id);
	    		String sql = "select * from "
					+ ConfigBean.getStringValue("freight_cost")+" "+sqlCondition.toString()+" order by fc_id DESC ";
	    		
				if(pc!=null)
				{
					return (dbUtilAutoTran.selectMutliple(sql,pc));
				}
				else
				{
					
					return (dbUtilAutoTran.selectMutliple(sql));
				}
	    	}
	    	catch(Exception e)
	    	{
	    		throw new Exception("getFreightCostByCondition(sql)"+e);
	    	}
	    }
	 
	 /**
	  * 此方法暂时不用
	  * @param fr_company
	  * @param fr_undertake_company
	  * @param pc
	  * @return
	  * @throws Exception
	  */
	 public DBRow[] listFreightCost(String fr_company,String fr_undertake_company, PageCtrl pc)
	    throws Exception
	    {
	    	try
	    	{
	    		StringBuffer sqlCondition=new StringBuffer();
		    	sqlCondition.append("where");
		    	if(!fr_company.equals("")) {
		    		sqlCondition.append(" fr_company = '"+fr_company+"'");
		    	}
		    	if(!fr_undertake_company.equals("")) {
		    		sqlCondition.append(" or fr_undertake_company = '"+fr_undertake_company+"'");
		    	}
		    	
	    		String sql = "select * from "
					+ ConfigBean.getStringValue("freight_resources")+" "+sqlCondition.toString()+" order by fr_id DESC ";
	    		
				if(pc!=null)
				{
					return (dbUtilAutoTran.selectMutliple(sql,pc));
				}
				else
				{
					
					return (dbUtilAutoTran.selectMutliple(sql));
				}
	    	}
	    	catch(Exception e)
	    	{
	    		throw new Exception("listFreightCost(sql)"+e);
	    	}
	    }
	 
	 /**
	  * 通过货运运费ID获得该条货运运费
	  * @param fc_id
	  * @return
	  * @throws Exception
	  */
	 public DBRow getFreightCostById(String fc_id) throws Exception{
			String sql = "";
			sql = "select * from freight_cost where fc_id = "+fc_id;
			return dbUtilAutoTran.selectSingle(sql);
	 }
	 
	 /**
	  * 通过货运运费ID更新此条货运运费
	  * @param dbrow
	  * @param fc_id
	  * @throws Exception
	  */
	 public void updateFreightCost(DBRow dbrow,long fc_id) throws Exception{
		 try 
			{
				dbUtilAutoTran.update("where fc_id="+fc_id, ConfigBean.getStringValue("freight_cost"), dbrow);
			} 
			catch (Exception e) 
			{
				throw new Exception("updateFreightCost(row,sid) error: "+e);
			}
		}
	 
	 /**
	  * 通过货运运费ID删除此条货运运费
	  * @param fc_id
	  * @throws Exception
	  */
	 public void deleteFreightCost(long fc_id) throws Exception{
		 try 
			{
				dbUtilAutoTran.delete("where fc_id="+fc_id, ConfigBean.getStringValue("freight_cost"));
			} 
			catch (Exception e) 
			{
				throw new Exception("deleteFreightCost(sid) error: "+e);
			}
		}
	 
	 /**
	  * 通过国家的ID获得国家的名字等信息
	  * @param id
	  * @return
	  * @throws Exception
	  */
	 public DBRow getCountryById(String id) throws Exception{
			String sql = "";
			sql = "select * from country_code where ccid = "+id;
			return dbUtilAutoTran.selectSingle(sql);
	 }
	 
	 public boolean changeFreightIndex(String fc_id,int freight_index)
		throws Exception {
		DBRow row = new DBRow();
		row.add("freight_index", freight_index);
		if(dbUtilAutoTran.update("where fc_id="+fc_id, ConfigBean.getStringValue("freight_cost"),row)>0)
			return true;
		else
			return false;
	 }
}
