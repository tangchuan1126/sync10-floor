package com.cwc.app.floor.api.zyj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorTrackStatusMgrZyj {
	
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 通过物流公司的名称得到其名下的物流编码信息
	 * @param shipCom
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTrackStatusListByShipCom(String shipCom) throws Exception{
		
		try {
			shipCom		= "".equals(shipCom)?"%":shipCom;
			String sql	= "SELECT * FROM waybill_tracking_status_code WHERE shipping_company = '"+shipCom+"' ORDER BY tracking_status";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorTrackStatusMgrZyj.getTrackStatusListByShipCom(shipCom)"+e);
		}
	}

	/**
	 * 得到所有物流公司
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getShipCompanyList() throws Exception{
		try {
			String sql = "SELECT shipping_company FROM waybill_tracking_status_code GROUP BY shipping_company";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorTrackStatusMgrZyj.getShipCompanyList()"+e);
		}
	}
	
	/**
	 * 添加物流编码
	 * @param track
	 * @return
	 * @throws Exception
	 */
	public boolean addTrackStatus(DBRow track) throws Exception{
		try {
			String tableName = ConfigBean.getStringValue("waybill_tracking_status_code");
			return dbUtilAutoTran.insert(tableName, track);
		} catch (Exception e) {
			throw new Exception("FloorTrackStatusMgrZyj.addTrackStatus(track)"+e);
		}
	}
	
	/**
	 * 更新物流编码
	 * @param track
	 * @return
	 * @throws Exception
	 */
	public int updateTrackStatus(int trackId, DBRow track) throws Exception{
		try {
			String tableName = ConfigBean.getStringValue("waybill_tracking_status_code");
			return dbUtilAutoTran.update(" where track_id = "+trackId, tableName, track);
		} catch (Exception e) {
			throw new Exception("FloorTrackStatusMgrZyj.updateTrackStatus(track)"+e);
		}
	}
	
	/**
	 * 通过物流编码和公司查询
	 * @param trackCode
	 * @param shipComp
	 * @return
	 * @throws Exception
	 */
	public DBRow getTrackStatusByCodeComp(String trackCode, String shipComp) throws Exception{
		try {
			String sql = "select * from waybill_tracking_status_code where tracking_status_code = '"+ trackCode +"' and shipping_company = '"+ shipComp +"'";
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorTrackStatusMgrZyj.getTrackStatusByCodeComp(trackCode, shipCom)"+e);
		}
	}
	
	/**
	 * 通过ID查询
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getTrackStatusById(String id) throws Exception{
		try {
			int idInt	= "".equals(id)?0:Integer.parseInt(id);	
			String sql	= "select * from waybill_tracking_status_code where track_id = " + idInt;
			return dbUtilAutoTran.selectSingle(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorTrackStatusMgrZyj.getTrackStatusById(id)"+e);
		}
	}
	

	public void deleteById(int id) throws Exception{
		try {
			String tableName = ConfigBean.getStringValue("waybill_tracking_status_code");
			dbUtilAutoTran.delete(" where track_id = " + id, tableName);
		}catch (Exception e) {
			throw new Exception("FloorTrackStatusMgrZyj.deleteById(id)"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
}
