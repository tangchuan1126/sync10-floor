
package com.cwc.app.floor.api.zr;

import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.cwc.app.key.CheckInChildDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInLiveLoadOrDropOffKey;
import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.ScheduleFinishKey;
import com.cwc.app.key.SpaceRelationTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorCheckInMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
	public DBRow getCompany(String company_id) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("company_id");
			String sql = "select * from " +tableName +" where company_id= ? ";
			DBRow para = new DBRow();
			para.add("company_id", company_id);
			return dbUtilAutoTran.selectPreSingle(sql, para );
		}catch (Exception e) {
			throw new Exception("FloorCheckInMgrZr.getCompany(company_id):"+e);
		}
	}
	public DBRow getCustomer(String customer_id) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("customer_id");
			String sql = "select * from " +tableName +" where customer_id= ? ";
			DBRow para = new DBRow();
			para.add("customer_id", customer_id);
			return dbUtilAutoTran.selectPreSingle(sql, para );
		}catch (Exception e) {
			throw new Exception("FloorCheckInMgrZr.getCustomer(customer_id):"+e);
		}
	}
	public DBRow getTitle(String title) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("title");
			String sql = "select * from " +tableName +" where title_name= ? ";
			DBRow para = new DBRow();
			para.add("title_name", title);
			return dbUtilAutoTran.selectPreSingle(sql, para );
		}catch (Exception e) {
			throw new Exception("FloorCheckInMgrZr.getTitle(customer_id):"+e);
		}
	}
	public DBRow getShipTo(String shipto) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("ship_to");
			String sql = "select * from " +tableName +" where ship_to_name= ? ";
			DBRow para = new DBRow();
			para.add("ship_to_name", shipto);
			return dbUtilAutoTran.selectPreSingle(sql, para );
		}catch (Exception e) {
			throw new Exception("FloorCheckInMgrZr.getShipTo(shipto):"+e);
		}
	}
	public DBRow getFreightTerm(String freight_term) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("freight_term");
			String sql = "select * from " +tableName +" where freight_term= ? ";
			DBRow para = new DBRow();
			para.add("freight_term", freight_term);
			return dbUtilAutoTran.selectPreSingle(sql, para );
		}catch (Exception e) {
			throw new Exception("FloorCheckInMgrZr.getFreightTerm(freight_term):"+e);
		}
	}
	public DBRow getLoadBar(String load_bar) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("load_bar");
			String sql = "select * from " +tableName +" where load_bar_name= ? ";
			DBRow para = new DBRow();
			para.add("load_bar_name", load_bar);
			return dbUtilAutoTran.selectPreSingle(sql, para );
		}catch (Exception e) {
			throw new Exception("FloorCheckInMgrZr.getLoadBar(load_bar_name):"+e);
		}
	}
	/**
	 * title 不是必须的，ship_to_id 可以有也可以没有，其他的参数必须有
	 * @param company_key
	 * @param customer_key
	 * @param title_id
	 * @param freight_term_id
	 * @param ship_to_id
	 * @param load_bar_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月6日
	 */
	public DBRow getLoadSetBy(long company_key,long customer_key , 
			long freight_term_id,long ship_to_id,long load_bar_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("load_bar_setup");
			String sql = "" ;
			if( ship_to_id == 0l){
				sql = "select * from " + tableName + " ,load_bar where company_key="+company_key
					 + " and customer_key ="+ customer_key 
					 + " and freight_term_id="+freight_term_id
					 + " and load_bar_setup.load_bar_id="+load_bar_id  +" and load_bar.load_bar_id = load_bar_setup.load_bar_id" ;
			}else{
				sql = "select * from " + tableName + ",load_bar where company_key="+company_key
						 + " and customer_key ="+ customer_key 
						 + " and freight_term_id="+freight_term_id
						 + " and ship_to_id="+ship_to_id
						 + " and load_bar_setup.load_bar_id="+load_bar_id + " and load_bar.load_bar_id = load_bar_setup.load_bar_id " ;
			}
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getLoadSetBy(load_bar_name):"+e);

		}
	}
	
	/**
	 * 添加关系
	 * @param row
	 * @return
	 * @throws Exception
	 */
	
	public long addLoadBarSetup(DBRow row)throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("load_bar_setup");
			return this.dbUtilAutoTran.insertReturnId(tableName, row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.addLoadBarSetup(DBRow row)"+e);
		}
	}
	
	/**
	 * 修改关系
	 * @param row
	 * @return
	 * @throws Exception
	 */
	
	public void updateLoadBarSetup(long id,DBRow row)throws Exception{
		String tableName = ConfigBean.getStringValue("load_bar_setup");
		try{
			this.dbUtilAutoTran.update("where load_bar_setup_id="+id,tableName, row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.updateLoadBarSetup"+e);
		}
	}
	/**
	 * 查询关系表 join所有关系表
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow[] selectAllLoadBarSetup()throws Exception{
		try{
			String sql="select * from load_bar_setup a LEFT JOIN load_bar b "
					+ "on a.load_bar_id=b.load_bar_id LEFT JOIN customer_id c "
					+ "on a.customer_key=c.customer_key LEFT JOIN company_id d "
					+ "on a.company_key=d.company_key LEFT JOIN title e "
					+ "on a.title_id=e.title_id LEFT JOIN ship_to f "
					+ "on a.ship_to_id=f.ship_to_id LEFT JOIN freight_term g "
					+ "on a.freight_term_id=g.freight_term_id";
		
			 return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr selectAllLoadBarSetup"+e);
		}
	}
	
	/**
	 * title 不是必须的，ship_to_id 可以有也可以没有，其他的参数必须有
	 * @param company_key
	 * @param customer_key
	 * @param title_id
	 * @param freight_term_id
	 * @param ship_to_id
	 * @param load_bar_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月6日
	 */
	public DBRow getBolSetBy(long company_key,long customer_key,long ship_to_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("bol_setup");
			String sql = "select * from "+tableName+" where company_key="+company_key+" and customer_key="+customer_key+" and ship_to_id="+ship_to_id ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getLoadSetBy(load_bar_name):"+e);

		}
	}
	
	/**
	 * 添加标签模版关系
	 * @param row
	 * @return
	 * @throws Exception
	 */
	
	public long addBolSetup(DBRow row)throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("bol_setup");
			return this.dbUtilAutoTran.insertReturnId(tableName, row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.addBolSetup(DBRow row)"+e);
		}
	}
	
	/**
	 * 修改关系
	 * @param row
	 * @return
	 * @throws Exception
	 */
	
	public void updateBolSetup(long id,DBRow row)throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("bol_setup");
			this.dbUtilAutoTran.update("where bol_setup_id="+id,tableName, row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.updateBolSetup"+e);
		}
	}
	
	public DBRow[] selectBolSetup()throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("bol_setup");
			String sql="select * from "+tableName+" a LEFT JOIN customer_id c "
					+ "on a.customer_key=c.customer_key LEFT JOIN company_id d "
					+ "on a.company_key=d.company_key LEFT JOIN title e "
					+ "on a.title_id=e.title_id LEFT JOIN ship_to f "
					+ "on a.ship_to_id=f.ship_to_id LEFT JOIN freight_term g "
					+ "on a.freight_term_id=g.freight_term_id";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.selectBolSetup"+e);
		}
	}
	
	public DBRow[] getEnetryTaskEquipments(long entry_id) throws Exception{
		try{
			
			String sql = " select distinct  equipment.* from door_or_location_occupancy_details as details LEFT JOIN entry_equipment as equipment "
					  +" on equipment.equipment_id = details.equipment_id "
					  +" where IFNULL(details.equipment_id,0)  > 0 and  details.dlo_id =?  GROUP BY details.equipment_id ";
					
			DBRow para = new DBRow();
			para.add("dlo_id", entry_id);
 			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getEnetryTaskEquipments"+e);
		}
	}
	
	public DBRow[] getAllDoor(long ps_id) throws Exception{
		try{
 			String sql = "select  sd_id , doorId ,area.area_id , area.area_name  from storage_door as door  LEFT JOIN  storage_location_area as area on door.area_id = area.area_id where door.ps_id = ? " ;
 			DBRow param = new DBRow();
 			param.add("ps_id", ps_id);
 			return dbUtilAutoTran.selectPreMutliple(sql, param);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getAllDoor(entry_id)"+e);
		}
	}
	/**
	 * 从资源表中去查询当前门 已经被那些资源占用了
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月26日
	 */
	public DBRow[] getNotFreeDoor(long ps_id ) throws Exception{
		try{
			 String sql = " select  srr.relation_type, sd.sd_id, count(*) as total , sd.doorId  , area.area_name   ,area.area_id from  space_resources_relation as srr  "
			 		+ " LEFT  JOIN storage_door as sd on sd.sd_id =  srr.resources_id "
			 		+ " LEFT JOIN storage_location_area as area on sd.area_id = area.area_id"
			 		+ " where srr.resources_type ="+OccupyTypeKey.DOOR+" and sd.ps_id = "+ps_id+" GROUP BY srr.resources_id , srr.relation_type" ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getNotFreeDoor(ps_id)"+e);
		}
	}
	
	/**
	 * 查询那些Spot Not Free了
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月26日
	 */
	public DBRow[] getNotFreeSpot(long ps_id ) throws Exception{
		try{
			 String sql = " select srr.relation_type ,spot.yc_id,spot.yc_no ,count(*) as total , area.area_name   ,area.area_id from  space_resources_relation as srr "
			 		+ " left  JOIN storage_yard_control as spot on spot.yc_id =  srr.resources_id "
			 		+ " left JOIN storage_location_area as area on spot.area_id = area.area_id"
			 		+ " where srr.resources_type ="+OccupyTypeKey.SPOT+" and spot.ps_id = "+ps_id+"  GROUP BY srr.resources_id , srr.relation_type ";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getNotFreeDoor(ps_id)"+e);
		}
	}
	
	public DBRow[] getAllSpot(long ps_id) throws Exception{
		try{
 			String sql = " select  spot.yc_id , spot.yc_no ,area.area_id , area.area_name  from storage_yard_control as spot  LEFT JOIN  storage_location_area as area "
 						+" on spot.area_id = area.area_id where spot.ps_id = ? "; 
 			DBRow param = new DBRow();
 			param.add("ps_id", ps_id);
 			return dbUtilAutoTran.selectPreMutliple(sql, param);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getAllSpot(ps_id)"+e);
		}
	}
	public DBRow getDoorAreaBy(long area_id) throws Exception{
		try{
			String sql = "select * from storage_location_area where area_id="+area_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getDoorAreaName(ps_id)"+e);
		}
	}
	public DBRow getSpotAreaBy(long area_id) throws Exception{
		try{
			String sql = "select * from storage_location_area where area_id="+area_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getSpotAreaName(ps_id)"+e);
		}
	}
	/**
	 * 只要是出于 processing + exception ..都是应该有通知信息的
	 * @param entry_id
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月2日
	 */
	public DBRow[] getAssignTaskEquipmentTask(long entry_id , long equipment_id ) throws Exception{
		try{
			String sql = "select * from door_or_location_occupancy_details where number_status <> "+CheckInChildDocumentsStatusTypeKey.UNPROCESS+" and dlo_id = "+entry_id+" and equipment_id ="+equipment_id; 
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getInEquipmentEntryDetail(entry_id,equipment_id,number_status)"+e);
		}
	}
	
	/**
	 * 需要assign Task的接口
	 * assign任务没有完成的 
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月5日
	 */
	public DBRow[] needWareHouseAssignTask(long entry_id) throws Exception{
		try{
			String sql = "select * from `schedule` as sch  JOIN door_or_location_occupancy_details as details  on details.dlo_detail_id = sch.associate_id  ";
				   sql += " JOIN space_resources_relation as arr  on arr.relation_id = details.dlo_detail_id and arr.relation_type ="+ SpaceRelationTypeKey.Task ;
				   sql += " where sch.associate_main_id ="+entry_id+" and sch.associate_process = "+ProcessKey.CHECK_IN_WINDOW+"   and sch.schedule_state < "+ScheduleFinishKey.ScheduleFinish;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.needWareHouseAssignTask(entry_id)"+e);
		}
	}
	/**
	 * 获取一个某个待分配任务的列表
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月6日
	 */
	public DBRow[] getAssignTaskList(long adid) throws Exception{
		try{
			String sql = " select  eq.rel_type , eq.equipment_id , eq.equipment_number , equipment_type ,equipment_status ,  details.dlo_id  ,count(*) as task_total from `schedule` as  sch " +
				    	"	LEFT JOIN door_or_location_occupancy_details as details "+
				    	"   on sch.associate_id = details.dlo_detail_id  "+
				    	"   LEFT JOIN entry_equipment as eq  "+
				    	" 	on eq.equipment_id = details.equipment_id "+
				    	"   LEFT JOIN schedule_sub as sub " +
				    	"   on sub.schedule_id = sch.schedule_id " +
				    	//"   where sch.schedule_state < "+ScheduleFinishKey.ScheduleFinish+" and (associate_process = "+ProcessKey.CHECK_IN_WINDOW+" or associate_process="+ProcessKey.GateNotifyWareHouse+") and sub.schedule_execute_id = "+adid+"  and eq.equipment_status != "+CheckInMainDocumentsStatusTypeKey.LEFT+"  GROUP BY details.equipment_id";
				    	 "   where sch.schedule_state < "+ScheduleFinishKey.ScheduleFinish+" and (associate_process = "+ProcessKey.CHECK_IN_WINDOW+" or associate_process="+ProcessKey.GateNotifyWareHouse+") and sub.schedule_execute_id = "+adid+"  GROUP BY details.equipment_id";
				//System.out.println(sql);
 			 return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getAssignTaskList(entry_id)"+e);
		}
	}
	
	/**
	 * 获取一个entry 某个设备下的 需要分配的detail
	 * @param entry_id
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月6日
	 */
	public DBRow[] getAssignTaskDetail(long entry_id , long equipment_id) throws Exception{
		try{
			String  sql = "	select * from `schedule` as sch JOIN door_or_location_occupancy_details as details  on sch.associate_id = details.dlo_detail_id " ;
				    sql +=	" LEFT JOIN space_resources_relation as srr on srr.relation_type ="+SpaceRelationTypeKey.Task+"  and srr.relation_id = details.dlo_detail_id " ;
			        sql +="  where sch.schedule_state < "+ScheduleFinishKey.ScheduleFinish+" and associate_main_id ="+entry_id ;
			        sql += " and sch.associate_process ="+ProcessKey.CHECK_IN_WINDOW+" and details.equipment_id ="+equipment_id;
 			       return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getAssignTaskDetail(entry_id，equipment_id)"+e);
		}
	}
	/**
	 * 通过schedule_id去获取，相应WIndowCheckIn的详细记录
	 * @param schedule_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月6日
	 */
	public DBRow getCheckInWindowScheduleDetail(long schedule_id) throws Exception{
		try{
			String sql = "select * from `schedule` as sch "
					+ " LEFT JOIN door_or_location_occupancy_details as details on details.dlo_detail_id = sch.associate_id "
					+ " LEFT JOIN space_resources_relation as srr on srr.relation_id = sch.associate_id and srr.relation_type="+SpaceRelationTypeKey.Task
					+ " LEFT JOIN entry_equipment as equipment on equipment.equipment_id = details.equipment_id "
					+ " where sch.schedule_id = "+schedule_id ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getCheckInWindowScheduleDetail(schedule_id)"+e);
		}
	}
 	
	public DBRow[] getAssignTaskListByEntry(long entry_id) throws Exception{
		try{
			String sql = "select * , count(*) as total_task  from door_or_location_occupancy_details as details  "
					+ "LEFT JOIN entry_equipment as eq ON eq.equipment_id = details.equipment_id  "
 					+ " where eq.equipment_status != "+CheckInMainDocumentsStatusTypeKey.LEFT+" and dlo_id = "+entry_id+"  GROUP BY details.equipment_id";
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getAssignTaskListByEntry(entry_id)"+e);
		}
	}
	/**
	 * 获取当前这个人需要去ExecuteTask 53 processkey 为53的类型
	 * @param adid
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月8日
	 */
	public DBRow[] executeTaskList(long adid) throws Exception{
		try {
			//-- 查询出当前这个人 Load 没有完成 的任务 53  door:53 , equiment : 3434, task_number 
			/*String sql = "select  	IFNULL(srr.resources_id,details.rl_id) as resources_id ,IFNULL(srr.resources_type,details.occupancy_type) as  resources_type , details.equipment_id , eq.equipment_number , eq.equipment_type  , details.dlo_id  as entry_id , "
					+ "		count(DISTINCT sch.associate_id , sch.associate_process , sch.associate_type)  as task_total from schedule_sub as ss "
					+ " LEFT JOIN `schedule` as sch on ss.schedule_id = sch.schedule_id "
					+ " LEFT JOIN door_or_location_occupancy_details as details on details.dlo_detail_id = sch.associate_id"
					+ " LEFT JOIN space_resources_relation as srr on srr.relation_type = "+SpaceRelationTypeKey.Task+" and  details.dlo_detail_id = srr.relation_id"
					+ "	LEFT JOIN entry_equipment as eq on eq.equipment_id = details.equipment_id"
					+ " where ss.schedule_execute_id = "+adid+" and ss.schedule_state < "+ScheduleFinishKey.ScheduleFinish+" and ss.is_task_finish = 0 "
					+ "and sch.associate_process = "+ProcessKey.CHECK_IN_WAREHOUSE+" and (details.number_status = "+CheckInChildDocumentsStatusTypeKey.PROCESSING+" or details.number_status ="+CheckInChildDocumentsStatusTypeKey.UNPROCESS+")  GROUP BY   srr.resources_id , srr.resources_type , details.equipment_id ORDER BY entry_id ";*/
			StringBuffer sqlBuffer = new StringBuffer();
			sqlBuffer.append("select t.resources_id, t.resources_type, t.equipment_id, t.equipment_number, t.equipment_type, t.entry_id, t.rel_type, ")
			 .append("       count( DISTINCT associate_id, associate_process, associate_type ) AS task_total  ")
			 .append("  from  ")
			 .append("   ( ")
			 .append("	   SELECT IFNULL(srr.resources_id, details.rl_id) AS resources_id, ")
			 .append("	   		 IFNULL(srr.resources_type, details.occupancy_type) AS resources_type, ")
			 .append("	   		 details.equipment_id, ")
			 .append("	   		 eq.equipment_number, ")
			 .append("	   		 eq.rel_type, ")
			 .append("	   		 eq.equipment_type, ")
			 .append("	   		 details.dlo_id AS entry_id, ")
			 .append("	   		 sch.associate_id, ")
			 .append("	   		 sch.associate_process, ")
			 .append("	   		 sch.associate_type ")
			 .append("	   FROM schedule_sub AS ss LEFT JOIN `schedule` AS sch ON ss.schedule_id = sch.schedule_id ")
			 .append("	   LEFT JOIN door_or_location_occupancy_details AS details ON details.dlo_detail_id = sch.associate_id ")
			 .append("	   LEFT JOIN space_resources_relation AS srr ON  details.dlo_detail_id = srr.relation_id  AND  srr.relation_type = 2 ")
			 .append("	   LEFT JOIN entry_equipment AS eq ON eq.equipment_id = details.equipment_id ")
			 .append("	   WHERE ss.schedule_execute_id = "+adid)
			 .append("	     AND ss.schedule_state < " + ScheduleFinishKey.ScheduleFinish + " ")
			 .append("	     AND ss.is_task_finish = 0 ")
			 .append("	     AND sch.associate_process = "+ProcessKey.CHECK_IN_WAREHOUSE )
			 .append("	     AND ( details.number_status = "+CheckInChildDocumentsStatusTypeKey.PROCESSING+" OR details.number_status = "+CheckInChildDocumentsStatusTypeKey.UNPROCESS+" )  ")
			 .append("	 ) t ")
			 .append("  GROUP BY resources_id, resources_type, equipment_id ")
			 .append("  ORDER BY entry_id ");

			return dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.executeTaskList(adid)"+e);
		}
	}
	
	public DBRow[] taskProcessingListByEntry(long entry_id , long adid) throws Exception{
		try{
			String sql = "select srr.resources_id ,srr.resources_type , eq.equipment_id , eq.equipment_number ,eq.equipment_type  ,count(*) as total_task from  door_or_location_occupancy_details as details   "
					+ " left JOIN space_resources_relation  as srr  on details.dlo_detail_id = srr.relation_id and srr.relation_type ="+SpaceRelationTypeKey.Task
					+ " left JOIN entry_equipment as eq on eq.equipment_id = details.equipment_id "
  					+ " where details.dlo_id = "+entry_id + "  GROUP BY srr.resources_id , srr.resources_type , eq.equipment_id " ;
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.taskProcessingListByEntry(entry_id,adid)"+e);
		}
	}
	public DBRow[] getTaskProcessingDetails(long equipment_id ,long resources_id , int resources_type , long entry_id) throws Exception{
		try{
			String sql = "select * from door_or_location_occupancy_details as details "
					+ " JOIN space_resources_relation as srr on srr.relation_id = details.dlo_detail_id and srr.relation_type ="+SpaceRelationTypeKey.Task
					+ " JOIN `schedule` as sch on sch.associate_process = "+ProcessKey.CHECK_IN_WAREHOUSE+" and sch.associate_id = details.dlo_detail_id and sch.associate_type ="+ModuleKey.CHECK_IN
					+ " where srr.resources_type = "+resources_type+" and srr.resources_id ="+resources_id+" and details.equipment_id ="+equipment_id +" and  details.dlo_id="+entry_id  + " order by details.dlo_detail_id";
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getTaskProcessingDetails(entry_id,adid)"+e);
		}
	}
	public DBRow getEntryDetailRow(long dlo_detail_id ) throws Exception{
		try{
			String sql ="select * from door_or_location_occupancy_details as details "
					+ " LEFT JOIN entry_equipment as eq on eq.equipment_id = details.equipment_id "
					+ " LEFT JOIN space_resources_relation as srr on srr.relation_id = details.dlo_detail_id and srr.relation_type ="+ SpaceRelationTypeKey.Task 
					+ " where details.dlo_detail_id ="+dlo_detail_id ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getEntryDetailRow(dlo_detail_id)"+e);
		}
	}
	public int countEquipmentAtDoorNotFinishTask(long equipment_id , long entry_id) throws Exception {
		try{
			String sql = " select count(*) as total  from door_or_location_occupancy_details as details "
					 	+"  JOIN  space_resources_relation as srr on srr.relation_id = details.dlo_detail_id and srr.relation_type ="+SpaceRelationTypeKey.Task
					 	+ " where ( number_status = "+CheckInChildDocumentsStatusTypeKey.UNPROCESS+" or number_status= "+CheckInChildDocumentsStatusTypeKey.PROCESSING+") and dlo_id ="+entry_id + "   and equipment_id ="+equipment_id;
				DBRow row = dbUtilAutoTran.selectSingle(sql);
				if(row != null){
					return row.get("total", 0);
				}
				return 0 ;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.countEquipmentAtDoorNotFinishTask(equipment_id)"+e);
		}
	}
	/**
	 * 得到一个门下的Details  detail表中的数据为准
	 * @param entry_id
	 * @param sd_id
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月11日
	 */
	public DBRow[] getDoorDetailsWithCloseBy(long entry_id , long sd_id , long equipment_id , int  occupyType) throws Exception{
		try{
			String sql = "select * from door_or_location_occupancy_details as details  "
					+ "  LEFT JOIN space_resources_relation as srr on details.dlo_detail_id = srr.relation_id and srr.relation_type ="+SpaceRelationTypeKey.Task 
					+ "  where ((srr.resources_id = "+sd_id+" and srr.resources_type = "+occupyType+")  or ( details.occupancy_type = "+occupyType+" and details.rl_id = "+sd_id+"))  "
					+ "	 and details.dlo_id =  "+entry_id+" and details.equipment_id ="+equipment_id + " order by details.dlo_detail_id ";
 				return dbUtilAutoTran.selectMutliple(sql);
 		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getDetailsBy(equipment_id)"+e);
		}
	}
	/**
	 * 得到当前设备在停车位上所有没有完成的
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月12日
	 */
	public DBRow[] getEquipmentAtSpotNotFinishDetails(long equipment_id ) throws Exception {
		try{
			String sql = "select * from door_or_location_occupancy_details  as details "
					+ " JOIN space_resources_relation as srr on srr.relation_id = details.dlo_detail_id and srr.relation_type ="+ SpaceRelationTypeKey.Task
					+ "	where (details.number_status = "+CheckInChildDocumentsStatusTypeKey.UNPROCESS+" or details.number_status ="+CheckInChildDocumentsStatusTypeKey.PROCESSING+") "
					+ " and details.equipment_id ="+equipment_id+" and srr.resources_type ="+OccupyTypeKey.SPOT;
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getEquipmentAtSpotNotFinishDetails(equipment_id)"+e);
		}
	}
	
	
	public DBRow[] getWareHouseAssignScheduleByEntry(long entry_id) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("schedule")  + " where associate_main_id="+entry_id + " and associate_process="+ProcessKey.CHECK_IN_WAREHOUSE + " and associate_type="+ ModuleKey.CHECK_IN ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getWareHouseAssignScheduleByEntry(entry_id)"+e);
		}
	}
	
	public DBRow[] getEntryEquipmentJoinResouces(long entry_id) throws Exception{
		try{
			String sql = " select eq.*,srr.*,dm.company_name as carrier from entry_equipment  as eq LEFT JOIN  space_resources_relation as srr  "
                    + " on srr.relation_id = eq.equipment_id and srr.relation_type ="+SpaceRelationTypeKey.Equipment+" "
                    + " LEFT JOIN  door_or_location_occupancy_main as dm on dm.dlo_id=eq.check_in_entry_id  "
                    + " where eq.check_in_entry_id ="+entry_id ;
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getEntryEquipmentJoinResouces(entry_id)"+e);
		}
	}
	
	public DBRow[] getWareHouseManagerAssignTask(long dlo_detail_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("schedule") ;
			String sql = "select * from " +tableName + " where associate_type="+ModuleKey.CHECK_IN + " and (associate_process=  "+ProcessKey.CHECK_IN_WINDOW+" or associate_process= "+ProcessKey.GateNotifyWareHouse+"  or  associate_process="+ProcessKey.GateHasTaskNotifyWindow+") and associate_id="+dlo_detail_id ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getWareHouseManagerAssignTask(dlo_detail_id)"+e);
		}
	}
	/**
	 * 根据一个Detail_id 去查询  一个Detail 是否已经分配给了 WarehouseWorker
	 * @param dlo_detail_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月22日
	 */
	public boolean isEntryDetailHasAssignWarehouseWorkTask(long dlo_detail_id )throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("schedule") ;
			String sql = "select count(*) as total_count from "+ tableName + " where associate_id="+dlo_detail_id + " and associate_process="+ProcessKey.CHECK_IN_WAREHOUSE  ;
			 	   sql += " and associate_type = " + ModuleKey.CHECK_IN ;
			 	   
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			if(row != null && row.get("total_count", 0) > 0){
				return true ;
			}
			return false ;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.isEntryDetailHasAssignWarehouseWorkTask(dlo_detail_id)"+e);
		}
	}
	
	public DBRow[] getWindowCheckInList(long entry_id , int pageSize) throws Exception{
		try{
			String sql = "select * from door_or_location_occupancy_main where dlo_id > "+entry_id+"  ORDER BY dlo_id desc   LIMIT 0,"+pageSize ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getWindowCheckInList(entry_id,pageSize)"+e);
		}
	}
	
	public DBRow[] getEquipmentByEntryAndTotalTask(long entry_id) throws Exception{
		try{
			String sql = "select eq.* , srr.*, count(details.dlo_detail_id) as total_task from entry_equipment as eq "
					+ " LEFT JOIN  door_or_location_occupancy_details as details  on details.equipment_id = eq.equipment_id"
					+"  LEFT JOIN space_resources_relation as srr on srr.relation_id =  eq.equipment_id and srr.relation_type ="+ SpaceRelationTypeKey.Equipment
					+ " where check_in_entry_id  = "+entry_id+" GROUP BY eq.equipment_id ";
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getEquipmentByEntryAndTotalTask(entry_id)"+e);
		}
	}
	/**
	 * 获取一个Entry PickUp的equipment
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月23日
	 */
	public DBRow[] getEntryPickUpEequipmentAndTotalTask(long entry_id) throws Exception{
		try{
 			String sql = "select eq.* , srr.* , count(details.dlo_detail_id) as total_task from entry_equipment as eq  "
 					+ " LEFT JOIN door_or_location_occupancy_details as details on details.equipment_id = eq.equipment_id "
					+ " LEFT JOIN space_resources_relation as srr on srr.relation_id = eq.equipment_id and srr.relation_type =" + SpaceRelationTypeKey.Equipment
 					+ " where check_out_entry_id ="+entry_id
					+ " and check_in_entry_id != check_out_entry_id group by eq.equipment_id " ;
  			return dbUtilAutoTran.selectMutliple(sql);
 		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getEntryPickUpEequipmentAndTotalTask(entry_id)"+e);
		}
	}
	
	
	public DBRow[] getTaskFinishUser(long dlo_detail_id) throws Exception{
		try{
			String sql = "select  sub.* , admin.employe_name from `schedule` as sch "
					+ " LEFT JOIN schedule_sub as sub on sch.schedule_id = sub.schedule_id "
					+ " LEFT JOIN admin on admin.adid = sub.schedule_finish_adid "
					+ " where sch.associate_id = "+dlo_detail_id+" and sch.associate_type ="+ ModuleKey.CHECK_IN + " and sch.associate_process="+ProcessKey.CHECK_IN_WAREHOUSE 
					+ " GROUP BY schedule_finish_adid " ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getTaskFinishUser(dlo_detail_id)"+e);
		}
	}
	
	/**
	 * 通过TaskID查询Supervizor
	 * @param dlo_detail_id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月25日 下午12:36:35
	 */
	public DBRow[] getTaskSupervizorUsers(long dlo_detail_id) throws Exception{
		try{
			String sql = "select  sub.* , admin.employe_name from `schedule` as sch "
					+ " LEFT JOIN schedule_sub as sub on sch.schedule_id = sub.schedule_id "
					+ " LEFT JOIN admin on admin.adid = sub.schedule_execute_id "
					+ " where sch.associate_id = "+dlo_detail_id+" and sch.associate_type ="+ ModuleKey.CHECK_IN + " and sch.associate_process in("+ProcessKey.CHECK_IN_WINDOW+","+ProcessKey.GateNotifyWareHouse+")"
					+ " GROUP BY schedule_execute_id " ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getTaskFinishUser(dlo_detail_id)"+e);
		}
	}
	/**
	 * 得到设备 以及当前设备占用的位置
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月24日
	 */
	public DBRow getEquipmentWithResources(long equipment_id) throws Exception{
		try{
			String sql ="select eq.* , srr.resources_id , srr.resources_type , srr.occupy_status from entry_equipment as eq"
					+ " LEFT JOIN space_resources_relation as srr on eq.equipment_id =  srr.relation_id and srr.relation_type =" + SpaceRelationTypeKey.Equipment + " "
					+ " where eq.equipment_id ="+equipment_id ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getEquipmentWithResources(equipment_id)"+e);
		}
	}
	
	/**
	 * 查询EntryEquipment (自己带来的 + 自己PickUp的Equipment(window PickUp CTNR))
	 * @param entry_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月24日
	 */
	public DBRow[] getEntryEequipment(long entry_id) throws Exception{
		try{
			String sql = "SELECT * FROM entry_equipment WHERE check_in_entry_id = "+entry_id+" OR ( check_out_entry_id = "+entry_id+" AND check_in_entry_id <> check_out_entry_id)";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getEntryEequipment(entry_id)"+e);
		}
	}
	
	public void updateEquipmentBy(long equipment_id , DBRow updateRow) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("entry_equipment") ;
			dbUtilAutoTran.update(" where  equipment_id="+equipment_id, tableName, updateRow);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.updateEquipmentBy(equipment_id,updateRow)"+e);
		}
	}
	public DBRow[] getAllCarrier() throws Exception{
		try{
			return dbUtilAutoTran.selectMutliple("select * from carrier_scac_mcdot");
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getAllCarrier()"+e);
		}
	}
	public void updateCarrier(long  id, DBRow updateRow ) throws Exception{
		try{
			dbUtilAutoTran.update(" where id="+id, "carrier_scac_mcdot", updateRow);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.updateCarrier(id,updateRow)"+e);
		}
	}
	
	public DBRow[] getCarrierByPhoneNumber(String phoneNumber) throws Exception{
		try{
			String sql = "select * from carrier_scac_mcdot where phone_carrier LIKE '"+phoneNumber+"%'";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getCarrierByPhoneNumber(phoneNumber)"+e);
		}
	}
	public DBRow[] getCarrierByCarrier(String carrier) throws Exception{
		try{
			return dbUtilAutoTran.selectMutliple("select * from carrier_scac_mcdot where carrier LIKE '"+carrier+"%'");
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getCarrierByCarrier(carrier)"+e);
		}
	}
	
	public DBRow[] getEntryByNotLeftEquipment(String equipment_number , long ps_id) throws Exception {
		try{
			String sql = "select entry.*  from entry_equipment  as eq "
					+ " LEFT JOIN door_or_location_occupancy_main as entry on eq.check_in_entry_id = entry.dlo_id  "
					+ " where entry.ps_id = "+ps_id+" and eq.equipment_number = '"+equipment_number+"' and eq.equipment_status <>"+ CheckInMainDocumentsStatusTypeKey.LEFT
					+ " GROUP BY entry.dlo_id ";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getEntryByNotLeftEquipment(equipment_number,ps_id)"+e);
		}
	}
	
	public int getCarrierAndMcDot(String carrier , String mc_dot) throws Exception{
		try{
			String sql = "select count(*) as total_count from carrier_scac_mcdot where carrier= ? and mc_dot=?" ;
			DBRow para = new DBRow();
			para.add("carrier", carrier);
			para.add("mc_dot", mc_dot);
			DBRow result =  dbUtilAutoTran.selectPreSingle(sql, para );
			if(result != null){
				return result.get("total_count", 0);
			}
			return 0 ;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getCarrierAndMcDot(carrier,mc_dot)"+e);
		}
	}
	
	/**
	 * 因为现在的数据库有出现同一 mc_dot 对应不同的  carrier.我返回ID最大(最新的)数据 倒叙
	 * @param carrier
	 * @param mc_dot
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月25日
	 */
	public DBRow[] getMcDotNotCarrier(String carrier , String mc_dot) throws Exception{
		try{
			String sql = "select * from carrier_scac_mcdot where mc_dot = '"+mc_dot+"' and carrier <> '"+carrier+"'  ORDER BY id desc";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getMcDotNotCarrier(carrier,mc_dot)"+e);
		}
	}
	
 	
	/**
	 * 因为现在的数据库有出现同一 carrier 对应不同的  mc_dot.我返回ID(最新的)数据 倒叙
	 * @param carrier
	 * @param mc_dot
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月25日
	 */
	public DBRow[] getCarrierNotMcDot(String carrier , String mc_dot) throws Exception{
		try{
			String sql = "select * from carrier_scac_mcdot where carrier = '"+carrier+"' and mc_dot <> '"+mc_dot+"'  ORDER BY id desc";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getCarrierNotMcDot(carrier,mc_dot)"+e);
		}
	}
	
	public DBRow[] getMcdot(String mc_dot) throws Exception {
		try{
			String sql = "select * from carrier_scac_mcdot where mc_dot like '"+mc_dot+"%'";
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getMcdot(mc_dot)"+e);
		}
	}
	public long addMcDotAndCarrier(DBRow carrier) throws Exception{
		try{
			return dbUtilAutoTran.insertReturnId("carrier_scac_mcdot", carrier);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.addMcDotAndCarrier(carrier)"+e);
		}
	}
	public int updateMcDotAndCarrier(long id , DBRow updateCarrier) throws Exception{
		try{
			return dbUtilAutoTran.update(" where id="+id, "carrier_scac_mcdot", updateCarrier);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.updateMcDotAndCarrier(id,updateCarrier)"+e);
		}
	}
	
	public int countRLIdNotFinishTask(long LR_ID) throws Exception{
		try{
			String sql = "select  count(*) as total_count  from door_or_location_occupancy_details where  (number_status ="+CheckInChildDocumentsStatusTypeKey.UNPROCESS+" or number_status ="+CheckInChildDocumentsStatusTypeKey.PROCESSING+") and LR_ID ="+LR_ID;
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			if(result != null){
				return result.get("total_count", 0);
			}
			return 0 ;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.countRLIdNotFinishTask(rl_id)"+e);
		}
	}
	public int updateOrderSystem(long id , DBRow updateRow) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("order_system")  ; 
			return dbUtilAutoTran.update(" where id="+id, tableName, updateRow);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.updateOrderSystem(id,updateRow)"+e);
		}
	}
	
	public DBRow[] getSetReportScreenParam(long ps_id , int report_screen_key) throws Exception{
		try{
			String sql = "select param.* from set_report_screen as report  LEFT JOIN set_report_screen_param as param on param.set_report_screen_id = report.set_report_screen_id  "
					+ "where ps_id = "+ps_id+" and report.report_screen_key ="+report_screen_key;
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getSetReportScreenParam(ps_id,report_screen_key)"+e);
		}
	}
	
	 
	public DBRow getReportScreen(long ps_id , int report_screen_key) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("set_report_screen");
 			String sql = "select * from " + tableName + " where ps_id="+ ps_id + " and report_screen_key="+report_screen_key;
 			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.setReportScreenSaveOrUpdate(ps_id,report_screen_key)"+e);
		}
	}
	
	public long addReportScreen(DBRow insertRow) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("set_report_screen");
			return dbUtilAutoTran.insertReturnId(tableName, insertRow);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.addReportScreen(insertRow)"+e);
		}
	}
	public long addReportScreenParam(DBRow insertRow) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("set_report_screen_param");
			return dbUtilAutoTran.insertReturnId(tableName, insertRow);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.addReportScreenParam(insertRow)"+e);
		}
	}
	public void updateReportScreenParam(long set_report_screen_param_id , DBRow updateRow ) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("set_report_screen_param");
			dbUtilAutoTran.update(" where set_report_screen_param_id ="+set_report_screen_param_id  , tableName, updateRow);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.updateReportScreenParam(set_report_screen_param_id)"+e);
		}
	}
	 
	public DBRow getReportScreenParam(long set_report_screen_id , int type_key) throws Exception{
		try{
			String tableName =  ConfigBean.getStringValue("set_report_screen_param");
			String sql = "select * from " + tableName + " where set_report_screen_id="+ set_report_screen_id + " and type_key="+type_key ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getReportScreenParam(set_report_screen_id,type_key)"+e);
		}
	}
	
	/**
	 * 查询所有未完成的业务单据号
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月30日
	 */
	public DBRow[] getSetReportScreenLoadReceive(long ps_id ) throws Exception{
		try{
			String sql = "select *, create_time as start_time from order_system where IFNULL(finish_time,0) = 0 and ps_id ="+ps_id; 
			DBRow[] returnArrays =  dbUtilAutoTran.selectMutliple(sql);
			covertStartTimeToTotalTime(returnArrays);
			return returnArrays ;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getSetReportScreenLoadReceive(ps_id)"+e);
		}
	}
	/**
	 * 得到已经分配给SuperVisor 但是Super没有把当前的任务分配出去
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月30日
	 */
	public DBRow[] getSetReportScreenNeedAssignTask(long ps_id) throws Exception{
		try{
			String sql = "select sch.* , IFNULL(dm.window_check_in_time ,dm.gate_check_in_time) as start_time_c from `schedule` as sch  "
					+ "join door_or_location_occupancy_details dd on dd.dlo_detail_id = sch.associate_id "
					+ "join door_or_location_occupancy_main as dm on dm.dlo_id = dd.dlo_id "
					+ "where (sch.associate_process = "+ProcessKey.CHECK_IN_WINDOW+" or sch.associate_process = "+ProcessKey.GateNotifyWareHouse+") and sch.schedule_state < "+ScheduleFinishKey.ScheduleFinish+"and dm.ps_id ="+ps_id;
			
 			DBRow[] returnValue = dbUtilAutoTran.selectMutliple(sql);
			fixNeedAssignTask(returnValue);
			covertStartTimeToTotalTime(returnValue);
			return returnValue ;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getNeedAssignTask(ps_id)"+e);
		}
	}
	/**
	 * remain jobs 任务分配给loader 但是Load没有开始这个任务
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月31日
	 */
	public DBRow[] getSetReportScreenRemainJobs(long ps_id) throws Exception{
		try{
			String sql = "select detail.task_assign_loader_time as start_time  from door_or_location_occupancy_details as detail "
					+ " JOIN door_or_location_occupancy_main as main on main.dlo_id = detail.dlo_id "
					+ " join `schedule` s on s.associate_id = detail.dlo_detail_id and s.associate_process = "+ProcessKey.CHECK_IN_WAREHOUSE
					+" join space_resources_relation as srr on srr.relation_id = detail.dlo_detail_id and srr.relation_type = "+SpaceRelationTypeKey.Task+" "
					+ " where number_status = "+CheckInChildDocumentsStatusTypeKey.UNPROCESS+" and IFNULL(task_assign_loader_time,0) > 0 and main.ps_id ="+ps_id;
			DBRow[] datas = dbUtilAutoTran.selectMutliple(sql);
 			covertStartTimeToTotalTime(datas);
			return datas ;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getRemainJobs(ps_id)"+e);
		}
	}
	/**
	 * underprocessing 在处理中的任务
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月31日
	 */
	public DBRow[] getSetReportScreenUnderProcessing(long ps_id ) throws Exception{
		try{
			String sql = "select  handle_time as start_time from door_or_location_occupancy_details as detail "
					+ " LEFT JOIN door_or_location_occupancy_main as main on main.dlo_id = detail.dlo_id "
					+ " where detail.number_status ="+CheckInChildDocumentsStatusTypeKey.PROCESSING+" and main.ps_id ="+ps_id  + " and IFNULL(detail.is_forget_task,0) = 0";
			DBRow[] datas = dbUtilAutoTran.selectMutliple(sql);
 			 covertStartTimeToTotalTime(datas);
			return datas ;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getSetReportScreenUnderProcessing(ps_id)"+e);
		}
	}
	/**
	 * 得到当前的这个时刻 某个仓库今天关闭的单据
	 * 拿ps_id 转换成UTC的时间
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年12月31日
	 */
	public DBRow[] getSetReportScreenCloseTodayNumber(long ps_id , String startTime ,  String endTime) throws Exception{
		try{
			String sql = "select * , finish_time as start_time from order_system where ps_id="+ps_id + " and finish_time >= '"+startTime +"' and finish_time<'"+endTime +"'";
 			DBRow[] datas = dbUtilAutoTran.selectMutliple(sql);
			covertStartTimeToTotalTime(datas);
			return datas ;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getSetReportScreenCloseTodayNumber(ps_id)"+e);
		}
	}
	
	public DBRow  getSetScreenParam(long ps_id , int screen_type ,  int param_type) throws Exception{
		try{
			String sql = "select * , param.`value` as total_time   from set_report_screen_param as param "
					+ " LEFT JOIN set_report_screen  as screen  on screen.set_report_screen_id = param.set_report_screen_id "
					+ " where screen.ps_id = "+ps_id+" and screen.report_screen_key = "+screen_type+" and param.type_key="+param_type ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getSetScreenParam(long ps_id , int screen_type ,  int param_type)"+e);
		}
	}
	
	public int getForgetCloseTasks(long ps_id) throws Exception{
		try{
			 String sql = "select count(*) as total_task from door_or_location_occupancy_details as details "
			 		+ "JOIN door_or_location_occupancy_main as main on main.dlo_id = details.dlo_id  "
			 		+ "where details.is_forget_task = 1 and ( details.number_status = "+CheckInChildDocumentsStatusTypeKey.UNPROCESS+" or details.number_status = "+CheckInChildDocumentsStatusTypeKey.PROCESSING+") and main.ps_id ="+ps_id;
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			if(result != null){
				return result.get("total_task", 0);
			}
			return 0 ;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getForgetCloseTasks(ps_id)"+e);
		}
	}
	
	
	
	/**
	 * 得到一个LR_ID总共收到多少货
	 * @param lr_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月5日
	 */
	public int getCommonReceivePallet(long lr_id) throws Exception{
		try{
			String sql = "select sum(pallet_type_count) as sum  from wms_receive_pallet_type_count where lr_id ="+lr_id ;
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			if(result != null){
				return result.get("sum", 0);
			}
			return 0;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getCommonReceivePallet(lr_id)"+e);
		}
	}
	/**
	 * 得到一个LR_ID装多少货
	 * @param lr_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月5日
	 */
	public int getCommonLoadPallet(long lr_id) throws Exception{
		try{
			String sql = "select sum(wms_pallet_type_count) as sum  from wms_load_order_pallet_type_count where lr_id ="+lr_id ;
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			if(result != null){
				return result.get("sum", 0);
			}
			return 0;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getCommonLoadPallet(lr_id)"+e);
		}
	}
	public DBRow[] loadReceiveCloseToday(long ps_id , String start_time , String end_time , PageCtrl pc) throws Exception{
		try{
			 /* String sql = "select t.id, t.number, t.system_type, t.company_id, t.customer_id, t.account_id, t.title, t.starttime, t.finishtime, t.number_status, "
			  		+ " (select group_concat(DISTINCT cast(ss.schedule_finish_adid as char)) from schedule_sub ss where ss.schedule_id in (select s.schedule_id from schedule s where s.associate_process = '53' and s.associate_id in (select dlo1.dlo_detail_id from door_or_location_occupancy_details dlo1 where dlo1.finish_time is not null and dlo1.lr_id = t.id) )) closeby  "
			  		+ "from "
			  		+ "( "
			  		+ "			select os.id, os.number, os.system_type, os.company_id, os.customer_id, os.account_id, os.title,  os.number_status, "
			  		+ "	 coalesce(min(dlo.handle_time), os.create_time) starttime, "
			  		+ "	os.finish_time finishtime "
			  		+ " from order_system os , door_or_location_occupancy_details dlo  "
			  		+ " where dlo.lr_id = os.id  "
			  		+ " and os.finish_time is not null "
			  		+ " and os.ps_id = "+ps_id+"  and os.finish_time >= '"+start_time+"' and os.finish_time <'"+end_time+"'"
			  		+ " group by os.id) t order by t.id desc " ;
			  */
			  String sql = "SELECT os.id, os.number, os.system_type, os.company_id, "
			  		+ "			os.customer_id,	os.account_id, os.title, os.number_status, 	 "
			  		+ "			coalesce(min(dlod.handle_time), os.create_time) starttime, 	"
			  		+ "			os.finish_time finishtime, dlo_id entry_id,"
			  		//+ "			-- group_concat(DISTINCT cast(ss.schedule_finish_adid as char)) closeby, "
			  		+ "			group_concat(DISTINCT ad.employe_name) labor "
			  		+ " FROM order_system os "
			  		+ " join door_or_location_occupancy_details dlod on dlod.lr_id = os.id and dlod.finish_time is not null "
			  		+ " left join `schedule` s on  s.associate_id = dlod.dlo_detail_id and s.associate_process = '53' "
			  		+ " left join schedule_sub ss on ss.schedule_id = s.schedule_id "
			  		+ " left join admin ad on ad.adid = ss.schedule_finish_adid "
			  		+ " where os.finish_time is not null  "
			  		+ "	and os.ps_id = "+ps_id
			  		+ "	and os.finish_time >= '"+start_time+"' "
			  		+ "	and os.finish_time <'"+end_time+"' "
			  		+ " group by os.id "
			  		+ " order by os.id desc";
			  
   			return dbUtilAutoTran.selectMutliple(sql, pc);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.loadReceiveCloseToday(ps_id,start_time,end_time)"+e);
		}
	}
	
	public DBRow[] notFinishLoadReceive(long ps_id , PageCtrl pc) throws Exception{
		try{
			String sql = " select DISTINCT  t.id, t.number, t.system_type, t.company_id, t.customer_id, t.account_id, t.title , t.create_time  , dd.dlo_id  entry_id,t.order_type,ee.equipment_purpose "
					+ " from order_system as t  "
					+ " join door_or_location_occupancy_details dd on dd.lr_id = t.id "
					+ " left join entry_equipment ee on dd.equipment_id=ee.equipment_id "
					+ " where t.ps_id = "+ps_id+"  and t.finish_time is null"
					+ " AND if(ee.equipment_purpose ="+CheckInLiveLoadOrDropOffKey.DROP+" ,dd.number_type NOT in("+ModuleKey.CHECK_IN_DELIVERY_ORTHERS+","+ModuleKey.CHECK_IN_PICKUP_ORTHERS+"),1=1) !=0" ;
			return dbUtilAutoTran.selectMutliple(sql,pc);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.notFinishLoadReceive(ps_id,pc)"+e);
		}
	}
	public int countForgetDetails(long entry_id) throws Exception{
		try{
			String sql = "select count(*) as total_tasks from door_or_location_occupancy_details as details where details.dlo_id = "+entry_id+" and is_forget_task = 1";
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			if(result != null){
				return result.get("total_tasks", 0);
			}
			return 0 ;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.countForgetDetails(detail_id)"+e);
		}
	}
	/**
	 * 去开始的时间
	 * @param datas
	 * @author zhangrui
	 * @Date   2014年12月31日
	 */
	private void fixNeedAssignTask(DBRow[] datas){
		if(datas != null && datas.length > 0){
			 for(DBRow temp : datas){
				 temp.remove("start_time");
 				 //选择 scheduleCreate的时间 和 WindowCheckIn的时间比较小的一个
				 String start_time = temp.getString("create_time");
				 String start_time_c = temp.getString("start_time_c");
				 if(start_time.compareTo(start_time_c) == -1){
					 start_time = start_time_c ;
 				 }
				 temp.add("start_time",start_time_c); 
			 }
		}
	}
	
	private void covertStartTimeToTotalTime(DBRow[] datas){
		if(datas != null && datas.length > 0){
			for(DBRow temp : datas){
				String start_time = temp.getString("start_time");
				if(!StringUtils.isBlank(start_time)){
					long total_time = costMinute(start_time);
					temp.add("total_time", total_time);
				}
			}
		}
	}
	private long costMinute(String start_time){
		try{
			//如果start_time 为null ||  或者时间格式错误 那么返回-1
			long currentLongTime =	new Date().getTime() ;
			long startLongTime =  DateUtil.createDateTime(start_time).getTime() ;
			long lastTime = (currentLongTime - startLongTime)/60000;
			return lastTime;
		}catch(Exception e){
			
		}
		return -1l;
	}
	/**
	 * 得到某个Detail Load了多少个Pallet
	 * @param detail_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月8日
	 */
	public int getLoadPalletCountBy(long detail_id) throws Exception{
		try{
			String sql = "select  SUM(pallet_type.wms_pallet_type_count)  as sum_pallet from wms_load_order_pallet_type_count as pallet_type where dlo_detail_id ="+detail_id;
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			if(result != null ){
				return result.get("sum_pallet", 0);
			}
			return 0 ;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getCommonReceivePalletCountBy(detail_id)"+e);
		}
	}
	/**
	 * 得到某个Detail Receive了多少个Pallet
	 * @param detail_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月8日
	 */
	public int getReceivePalletCountBy(long detail_id) throws Exception{
		try{
			String sql = "select  SUM(pallet_type.pallet_type_count)  as sum_pallet from wms_receive_pallet_type_count as pallet_type where dlo_detail_id ="+detail_id;
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			if(result != null){
				return result.get("sum_pallet", 0);
			}
			return 0;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getReceivePalletCountBy(detail_id)"+e);
		}
	}
	/**
	 * 通过一个DetailId获取 当前这个Detail 扫描的Pallet
	 * @param detail_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月16日
	 */
	public DBRow[] getReceiveScanPallet(long detail_id ) throws Exception{
		try{
			String sql = "select  receive.pallet_type , receive.pallet_type_count , receive.pallet_number  ,receive.scan_time , receive.resources_id , receive.resources_type , admin.employe_name  from  "
					+ "wms_receive_pallet_type_count  as receive LEFT JOIN admin  on receive.scan_adid = admin.adid  "
					+ "where dlo_detail_id ="+detail_id ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getReceiveScanPallet(detail_id)"+e);
		}
	}
	/**
	 * 得到Load的Pallet
	 * @param detail_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月16日
	 */
	public DBRow[] getLoadScanPallet(long detail_id ) throws Exception{
		try{
			String sql = "select  wms_pallet_type as pallet_type , wms_pallet_type_count as pallet_type_count , wms_pallet_number as pallet_number , scan_time , resources_id , resources_type , admin.employe_name ,wms_consolidate_pallet_number as consolidate_pallet "
					+ " from wms_load_order_pallet_type_count   LEFT JOIN admin on  scan_adid =  admin.adid where dlo_detail_id="+detail_id ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.getReceiveScanPallet(detail_id)"+e);
		}
	}
	
 
 
	public void deleteReceivePalletBy(long detail_id) throws Exception{
		try{
			dbUtilAutoTran.delete(" where dlo_detail_id="+detail_id, "wms_receive_pallet_type_count") ;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.deleteReceivePalletBy(detail_id)"+e);
		}
	}
	public void deleteLoadPalletBy(long detail_id ) throws Exception{
		try{
			dbUtilAutoTran.delete("where dlo_detail_id="+detail_id, "wms_load_order_pallet_type_count") ;
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZr.deleteLoadPalletBy(detail_id)"+e);
		}
	}
	/**
	 * 
	 * @param ps_id
	 * @param entry_id
	 * @param queryType 1 向上查询 ， 0 向下查询
	 * @param size
	 * @return
	 * @throws ExceptionrefreshLastRefreshTime
	 * @author zhangrui
	 * @Date   2015年1月9日
	 */
	public DBRow[] getUnasignTaskOfEntry(String ps_id, String entry_id, int queryType, int size) throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();
		sqlBuffer.append("select eq.check_in_entry_id, eq.equipment_id, eq.equipment_number, dlo.task_assign_loader_time")
				 .append("  from entry_equipment eq, door_or_location_occupancy_details dlo ")
				 .append(" where eq.equipment_id = dlo.equipment_id ")
				 .append("   and eq.check_in_entry_id in ")
				 .append("			( ")
				 .append("				select eq.check_in_entry_id ")
				 .append("					from entry_equipment eq, door_or_location_occupancy_details dlo ")
				 .append("				 where eq.check_in_entry_id = dlo.dlo_id ")
				 .append("					 and dlo.task_assign_loader_time is null ")
				 .append("					 and eq.ps_id = "+ ps_id + ((entry_id==null || entry_id.trim().length()==0) ? "" : " and eq.check_in_entry_id " + ((queryType == 1) ? ">" : "<") + entry_id))
				 .append("			) ")
				 .append("  order by eq.check_in_entry_id desc limit "+ ((size<1) ? 10 : size ) );
		try {
			return this.dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
		} catch (Exception e) {
			throw new Exception("UnasignTaskOfEntryQ getUnasignTaskOfEntry" + e);
		}
	}
	public DBRow[] countMemberUncompletedTask(String ps_id, String proJsId, String adgid, int[] processKeys) throws Exception{
		try{
			StringBuffer sqlBuffer = new StringBuffer();
			
			String processCondition = "";
			if(processKeys != null){
				for(int processKey : processKeys){
					processCondition += " or s.associate_process = "+ processKey;
				}
				
				if(!StringUtils.isBlank(processCondition)){
					processCondition = processCondition.replaceFirst("or", "");
					processCondition = "("+ processCondition +")";
				}
			}
			
			String proJsIdCondition = "";
			if(!StringUtils.isBlank(proJsId)){
				if("-1".equals(proJsId)){
					
				}else if("15".equals(proJsId)){
					proJsIdCondition = "(ad.post_id = "+ 5 +" or ad.post_id = "+ 10 +")";
				/*}else if("0".equals(proJsId)){
					proJsIdCondition = " a.proJsId is null";*/
				}else {
					proJsIdCondition = "ad.post_id = "+ proJsId;
				}
			}
			
			sqlBuffer.append("select ")
					 .append("a.adid, a.account, ad.department_id adgid, a.employe_name, aw.warehouse_id ps_id, ad.post_id proJsId, a.is_online, af.file_path,   ")
					 .append("       count(DISTINCT ss.schedule_id) tasks ")
					 .append("  from admin a ")
					 .append(" join admin_department ad on ad.adid = a.adid ")
					 .append(" join admin_warehouse aw on aw.adid = a.adid ")
					 .append(" LEFT JOIN admin_file af on af.adid = a.adid and af.activity = 1 ")
					 .append(" LEFT JOIN schedule_sub ss on ss.schedule_execute_id = a.adid ")
					 .append("   and ss.schedule_state < 10 ")
					 .append("   "+ (!StringUtils.isBlank(processCondition) ? "and exists (select 1 from schedule s where s.schedule_id = ss.schedule_id and "+ processCondition +" ) " : "") )
					 .append("   where a.llock=0 "+ (!StringUtils.isBlank(adgid) ? " and ad.department_id = "+ adgid : "") )
					 .append("   "+ (!StringUtils.isBlank(proJsIdCondition) ? "and "+ proJsIdCondition : "") )
					 .append("   "+ (!StringUtils.isBlank(ps_id) ? "and aw.warehouse_id = "+ ps_id : "") )
					 .append("  group by a.adid ");
  			DBRow[] datas = dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
			return datas ;
		}catch(Exception e){
			throw new Exception("UnasignTaskOfEntryQ getUnasignTaskOfEntry" + e);
		}
	}
	
	/*
	 * 
	 */
	public DBRow[] dcountMemberUncompletedTask(String ps_id, String proJsId, String adgid, int[] processKeys) throws Exception{
		try{
			StringBuffer sqlBuffer = new StringBuffer();
			
			String processCondition = "";
			if(processKeys != null){
				for(int processKey : processKeys){
					processCondition += " or s.associate_process = "+ processKey;
				}
				
				if(!StringUtils.isBlank(processCondition)){
					processCondition = processCondition.replaceFirst("or", "");
					processCondition = "("+ processCondition +")";
				}
			}
			
			String proJsIdCondition = "";
			if(!StringUtils.isBlank(proJsId)){
				if("-1".equals(proJsId)){
					
				}else if("15".equals(proJsId)){
					proJsIdCondition = "(ad.post_id = "+ 5 +" or ad.post_id = "+ 10 +")";
				/*}else if("0".equals(proJsId)){
					proJsIdCondition = " a.proJsId is null";*/
				}else {
					proJsIdCondition = "ad.post_id = "+ proJsId;
				}
			}
			
			sqlBuffer.append("select a.adid, a.account,  ad.department_id  as adgid, a.employe_name, aw.warehouse_id as ps_id, a.email, a.skype, a.msn, a.AreaId, a.mobilePhone, a.proQQ, ad.post_id as proJsId, a.is_online, ")
					 .append("       (select ag.name from admin_group ag where ag.adgid = ad.department_id) name, ")
					 .append("       count(ss.schedule_id) tasks ")
					 .append("  from admin a LEFT JOIN schedule_sub ss on ss.schedule_execute_id = a.adid ")
					 .append("   and ss.schedule_state < 10 ")
					 .append("   "+ (!StringUtils.isBlank(processCondition) ? "and exists (select 1 from schedule s where s.schedule_id = ss.schedule_id and "+ processCondition +" ) " : "") )
					 .append("  LEFT JOIN admin_warehouse aw on aw.adid = a.adid")
					 .append("  LEFT JOIN admin_department ad on ad.adid = a.adid")
					 .append("   where "+ (!StringUtils.isBlank(adgid) ? "  ad.department_id = "+ adgid : "") )
					 .append("   "+ (!StringUtils.isBlank(proJsIdCondition) ? "and "+ proJsIdCondition : "") )
					 .append("   "+ (!StringUtils.isBlank(ps_id) ? "and aw.warehouse_id = "+ ps_id : "") )
					 .append("  group by a.adid ");
			
			//System.out.println(sqlBuffer.toString());
 			DBRow[] datas = dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
			return datas ;
		}catch(Exception e){
			throw new Exception("UnasignTaskOfEntryQ getUnasignTaskOfEntry" + e);
		}
	}
	
	public int getEquipmentAssignedTaskCount(long equipment_id) throws Exception{
		try{
			String sql = "select count(*) as sum_count from door_or_location_occupancy_details as details  where equipment_id  ="+equipment_id 
					+ " and IFNULL(details.task_assign_loader_time,0) > 0  ";
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			if(result != null){
				return result.get("sum_count", 0);
			}
			return 0 ;
		}catch(Exception e){
			throw new Exception("UnasignTaskOfEntryQ getEquipmentAssignedTaskCount" + e);
		}
	} 

	public DBRow[] getPresenceOfSuperior(long ps_id , long adgid) throws Exception {
	 		try{
	 			StringBuffer sqlBuffer = new StringBuffer();
	 	 		
	 	 		sqlBuffer.append("select a.adid, a.employe_name,a.is_online ")
	 			 		 .append("  from admin a ")
	 			 		 .append(" where (a.proJsId = 5 or a.proJsId = 10) and adgid="+adgid)
	 			 		 .append("   "+( ps_id > 0l ? "and a.ps_id = "+ ps_id : "") );
	 			
	 	 		
	 	 		DBRow[] superoirs = dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
	 	 		return superoirs ;
	 	 	 
	 		}catch(Exception e){
				throw new Exception("UnasignTaskOfEntryQ getPresenceOfSuperior" + e);
	 		}
	 }
	public DBRow[]  countMemberTaskRelEqptTask(long adid) throws Exception {
		try{
			StringBuffer sqlBuffer = new StringBuffer();
			
			sqlBuffer.append("select eq.equipment_id, eq.equipment_status, eq.equipment_purpose, eq.equipment_number, eq.check_in_entry_id AS entry_id, ")
					 .append("       count(1) AS task_total, ")
					 .append("       count(dlo.task_assign_loader_time) AS task_assigned ")
					 .append("  from schedule_sub ss LEFT JOIN schedule s ON s.schedule_id = ss.schedule_id ")
					 .append("                       LEFT JOIN door_or_location_occupancy_details dlo ON dlo.dlo_detail_id = s.associate_id ")
					 .append("						 LEFT JOIN entry_equipment eq ON eq.equipment_id = dlo.equipment_id ")
					 .append(" where s.schedule_state < 10 ")
					 .append("   and ss.schedule_execute_id = "+ adid)
					 .append("  group by eq.equipment_id ")
					 .append("  having task_assigned < task_total ")
					 .append("  ORDER BY entry_id, equipment_id ");
			
	 		DBRow[] datas = dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
	 		
			return datas;
		}catch(Exception e){
			throw new Exception("UnasignTaskOfEntryQ countMemberTaskRelEqptTask" + e);
		}
 	}
	public DBRow[] searchGateDriverLiscenseByPhoneNumber(String phoneNumber) throws Exception{
		try{
			String sql = "select   gate_driver_liscense , gate_driver_name  from door_or_location_occupancy_main  where phone_gate_liscense_plate LIKE '"+phoneNumber+"%' group by  gate_driver_name ";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("UnasignTaskOfEntryQ searchGateDriverLiscenseByPhoneNumber" + e);
		}
	}
	public DBRow[] searchGateDriverLiscenseBy(String gate_driver_liscense) throws Exception{
		try{
			String sql = "select   gate_driver_liscense , gate_driver_name  from door_or_location_occupancy_main  where gate_driver_liscense LIKE '"+gate_driver_liscense+"%' group by gate_driver_name ";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("UnasignTaskOfEntryQ searchGateDriverLiscenseByPhoneNumber" + e);
		}
	}
	
	public DBRow[] getAdidNeedAssignEntry(long adid) throws Exception{
		try{
			String sql ="select associate_main_id from `schedule` as sch  "
					+ " LEFT JOIN schedule_sub as sub on sub.schedule_id = sch.schedule_id "
					+ " where sch.schedule_state < "+ScheduleFinishKey.ScheduleFinish+" and ( associate_process = "+ProcessKey.CHECK_IN_WINDOW+" or associate_process="+ProcessKey.GateNotifyWareHouse+" ) and associate_type="+ModuleKey.CHECK_IN+"  and sub.schedule_execute_id ="+adid
					+ " GROUP BY sch.associate_main_id " ;
 			
			
			
			
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getAdidNeedAssignEntry" + e);
		}
	}
	public DBRow[] getEntryDetailsByEntryId(long entry_id) throws Exception{
		try{
			 String sql = "select * from door_or_location_occupancy_details where dlo_id="+entry_id ;
			 return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getAdidNeedAssignEntry" + e);
		}
	}
	public DBRow[] getAdidTaskProcessingEntry(long adid) throws Exception{
		try{
			String sql ="select associate_main_id from `schedule` as sch  "
					+ " LEFT JOIN schedule_sub as sub on sub.schedule_id = sch.schedule_id "
					+ " where sch.schedule_state < "+ScheduleFinishKey.ScheduleFinish+" and ( associate_process = "+ProcessKey.CHECK_IN_WAREHOUSE+") and associate_type="+ModuleKey.CHECK_IN+" and sub.schedule_execute_id ="+adid
					+ " GROUP BY sch.associate_main_id" ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getAdidNeedAssignEntry" + e);
		}
	}
	
	public DBRow[] getCheckInCloseTaskUserInfos(long detail_id ) throws Exception{
		try{
			String sql = "select  admin.adid , admin.employe_name  from `schedule` as sch  "
					+ "LEFT JOIN schedule_sub as sub on sch.schedule_id = sub.schedule_id  "
					+ "LEFT JOIN admin on sub.schedule_finish_adid = admin.adid  "
					+ "where sch.associate_process = "+ProcessKey.CHECK_IN_WAREHOUSE+" and sch.associate_type = "+ModuleKey.CHECK_IN+" and sub.schedule_state = "+ScheduleFinishKey.ScheduleFinish+"  and associate_id ="+detail_id ;
				//System.out.println(sql);
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getCloseTaskUserInfos" + e);
		}
	}
	public DBRow[] getEntryEquipmentsNoResouces(long entry_id) throws Exception{
		try{
			String sql = "select equipment_number , equipment_type ,equipment_status ,equipment_purpose  from entry_equipment where check_in_entry_id  ="+entry_id ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getEntryEquipmentsNoResouces" + e);
		}
	}
	/**
	 * 查询当前的设备是否在某个资源上
	 * @param equipment_id
	 * @param resocues_id
	 * @param resouces_type
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年1月21日
	 */
	public int getEquipmentAndResouces(long equipment_id , long resocues_id , int resouces_type) throws Exception{
		try{
			String sql = "select count(*) as total_sum from entry_equipment as eq  "
					+ " JOIN space_resources_relation as srr on  srr.relation_id = eq.equipment_id and srr.relation_type ="+SpaceRelationTypeKey.Equipment
					+ " where eq.equipment_id = "+equipment_id+" and srr.resources_id ="+resocues_id+" and srr.resources_type ="+resouces_type;
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			if(result != null){
				return result.get("total_sum", 0);
			}
			return 0 ;
		}catch(Exception e){
			throw new Exception("getEquipmentAndResouces" + e);
		}
	}
	public DBRow[] getUnLeftEquipmentJoinResouces(String equipment_number , int equipment_type) throws Exception {
		try{
  			String sql = "select check_in_entry_id as entry_id , resources_id , resources_type , equipment_number , equipment_type ,equipment_id  from entry_equipment as eq "
 					+ " LEFT JOIN space_resources_relation as srr on srr.relation_id = eq.equipment_id and srr.relation_type ="+SpaceRelationTypeKey.Equipment
					+ " where eq.equipment_status <> "+CheckInMainDocumentsStatusTypeKey.LEFT+" and eq.equipment_type ="+equipment_type+" and  eq.equipment_number = '"+equipment_number+"' " ;
			
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getUnLeftEquipmentJoinResouces" + e);
		}
	}
	
	
	public DBRow[] getEquipmentNotFinishTasks(long equipment_id) throws Exception{
		try{
			String sql = "select * from door_or_location_occupancy_details as details  where  " 
					+ " and details.equipment_id = "+equipment_id ; 
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getEquipmentNotFinishTasks" + e);
		}
	}
	public void updateALLUserOffLine() throws Exception{
		try{
			String admin =  ConfigBean.getStringValue("admin");
			DBRow updateRow = new DBRow();
			updateRow.add("is_online", 0);
			dbUtilAutoTran.update("where 1=1", admin, updateRow);
		}catch(Exception e){
			throw new Exception("updateALLUserOffLine" + e);
		}
	}
	
	public void updateAdminByAccount(String account, DBRow updateRow) throws Exception{
		try{
			String admin =  ConfigBean.getStringValue("admin");
 			dbUtilAutoTran.update("where account='"+account+"'", admin, updateRow);
		}catch(Exception e){
			throw new Exception("updateALLUserOffLine" + e);
		}
	}
	public DBRow[] selectUsersBy(int ps_id, int proJsId, int adgid, int[] processKeys)  throws Exception {
		 try{
			 StringBuffer sql = new StringBuffer();
			 sql.append(" SELECT a.adid, a.account, ad.department_id AS adgid, a.employe_name, aw.warehouse_id AS ps_id, a.email, a.skype, a.msn, a.AreaId, a.mobilePhone, a.proQQ,ad.post_id AS proJsId,	a.is_online  " );
			 sql.append(" from admin as a  LEFT JOIN admin_warehouse aw ON aw.adid = a.adid LEFT JOIN admin_department ad ON ad.adid = a.adid ");
			 sql.append(" where aw.warehouse_id ="+ps_id);
			 sql.append(" and ad.department_id ="+adgid);
			 if(proJsId == 15){
				 sql.append(" and (ad.post_id="+5 + " or ad.post_id=10)");
			 }else{
				 sql.append(" and ad.post_id ="+proJsId);
			 }
			 sql.append(" GROUP BY a.adid ");
  			 return dbUtilAutoTran.selectMutliple(sql.toString());
		 }catch(Exception e){
			 throw new Exception("selectUsersBy" + e);
		 }
	}
	
	public DBRow[] selectUsersBy(long ps_id,  long adgid)  throws Exception {
		 try{
			 StringBuffer sql = new StringBuffer();
			 sql.append(" SELECT a.adid, a.account, ad.department_id AS adgid, a.employe_name, aw.warehouse_id AS ps_id, a.email, a.skype, a.msn, a.AreaId, a.mobilePhone, a.proQQ,ad.post_id AS proJsId,	a.is_online  " );
			 sql.append(" from admin as a  LEFT JOIN admin_warehouse aw ON aw.adid = a.adid LEFT JOIN admin_department ad ON ad.adid = a.adid ");
			 sql.append(" where aw.warehouse_id ="+ps_id);
			 sql.append(" and ad.department_id ="+adgid);
			 sql.append(" GROUP BY a.adid ");
 			 return dbUtilAutoTran.selectMutliple(sql.toString());
		 }catch(Exception e){
			 throw new Exception("selectUsersBy" + e);
		 }
	}
	public DBRow[] getScheduleDetails(long dlo_detail_id,long entry_id) throws Exception{
		try{
			String sql = "select * from  `schedule` as sch where sch.associate_id = "+dlo_detail_id +" and associate_type="+ModuleKey.CHECK_IN + " and associate_main_id="+entry_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("getScheduleDetails" + e);
		}
	}
	/**
	 * 修改资源表中的数据，通过 relation_id + relation_type
	 * @param relation_id
	 * @param relation_type
	 * @param updateRow
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2015年2月10日
	 */
	public int updateSpaceBy(long relation_id , int relation_type , DBRow updateRow) throws Exception{
		try{
			String space_resources_relation =  ConfigBean.getStringValue("space_resources_relation");
			return dbUtilAutoTran.update(" where relation_id="+relation_id + " and relation_type="+ relation_type , space_resources_relation, updateRow);
		}catch(Exception e){
			throw new Exception("updateSpaceBy" + e);
		}
	}


	public int receiveCountPallet(long detail_id) throws Exception {
		try{
			DBRow result = dbUtilAutoTran.selectSingle("select count(plate_no) as counted_pallet from receipt_rel_container rlc where (damage_qty is not null or normal_qty is not null) and detail_id="+detail_id);
			int count = 0 ;
			if(result != null){
				count = result.get("counted_pallet", 0);
			}
			return count ;
		}catch(Exception e){
			throw new Exception("receiveCountPallet" + e);
		}
	}


	public int receiveAllPallet(long detail_id) throws Exception {
		try{
			DBRow result = dbUtilAutoTran.selectSingle("select count(plate_no) as total_pallet from receipt_rel_container rlc where detail_id="+detail_id);
			int count = 0 ;
			if(result != null){
				count = result.get("total_pallet", 0);
			}
			return count ;
		}catch(Exception e){
			throw new Exception("receiveAllPallet" + e);
		}
	}
 }

