package com.cwc.app.floor.api.zzz;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorQuotedPriceTableMgrZZZ {

	private DBUtilAutoTran dbUtilAutoTran;
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	/**
	 *  根据查询条件得到产品信息
	 * @param productName
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	 
	public DBRow[] getQuotedPrice(String productName,PageCtrl pc)
	throws Exception
	{
		try
		{
			String sql="select * from "+ConfigBean.getStringValue("product")+" p where p.p_name='"+productName+"'  and orignal_pc_id = 0";	
			
			if(pc!=null)
				return dbUtilAutoTran.selectMutliple(sql,pc);
			else
				return dbUtilAutoTran.selectMutliple(sql);
		}
		catch(Exception e)
		{
			throw new Exception("FloorQuotedPriceTableMgrZZZ.getQuotedPrice(catalogIds,catalogIdsByPline,pName,warehouse)"+e);
		}
	}
	/**
	 * 根据parent获取产品分类
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductCategoryByParentId(DBRow row)
	throws Exception
	{
		try
		{
			String sql="select * from "+ConfigBean.getStringValue("product_catalog")+" where parentid=?";
			return dbUtilAutoTran.selectPreMutliple(sql,row);
		}
		catch(Exception e)
		{
			throw new Exception("FloorQuotedPriceTableMgrZZZ.getProductCategory():"+e);
		}
	}
	/**
	 * 获得产品线
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductLine()
	throws Exception
	{
		try
		{
			String sql="select * from "+ConfigBean.getStringValue("product_line_define");
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch(Exception e)
		{
			throw new Exception("FloorQuotedPriceTableMgrZZZ.getProductLine()"+e);
		}
	}
	/**
	 * 获取仓库
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStorageCatalogByParentid(DBRow para)
	throws Exception 
	{
		try
		{
			String sql="select * from "+ConfigBean.getStringValue("product_storage_catalog")+" where parentid=?";
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch(Exception e)
		{
			throw new Exception("FloorQuotedPriceTableMgrZZZ.getProductStorageCatalogById(dbrow)"+e);
		}
	}
	/**
	 * 根据父级的ID查询所属分类
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductCatalogById(long id)
	throws Exception
	{
		try
		{
			String sql="select id from "+ConfigBean.getStringValue("product_catalog")+" where parentid="+id;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch(Exception e)
		{
			throw new Exception("FloorQuotedPriceTableMgrZZZ.getProductCatalogById(id)"+e);
		}
	}
	/**
	 * 根据产品线ID获取产品类型ID
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductCatalogByProductLine(long id)
	throws Exception
	{
		try
		{
			String sql="select * from "+ConfigBean.getStringValue("product_catalog")+" where product_line_id="+id;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch(Exception e)
		{
			throw new Exception("FloorQuotedPriceTableMgrZZZ.getProductCatalogByProductLine(id)"+e);
		}
	}
	/**
	 * 根据产品的ID，查询产品
	 * @param pcIds
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductByPcIds(String pcIds,PageCtrl pc)
	throws Exception
	{
		try
		{
			String sql="select * from "+ConfigBean.getStringValue("product")+" p where p.orignal_pc_id=0 p.pc_id in("+pcIds+") order by catalog_id desc,p_name asc";
			
			if(pc!=null)
				return dbUtilAutoTran.selectMutliple(sql,pc);
			else
				return dbUtilAutoTran.selectMutliple(sql);
		}
		catch(Exception e)
		{
			throw new Exception("FloorQuotedPriceTableMgrZZZ.getQuotedPrice(catalogIds,catalogIdsByPline,pName,warehouse)"+e);
		}
	}
}
