package com.cwc.app.floor.api.zyj.model;

public class CustomerId {
	private Integer customer_key;
	private String customer_id;
	private String customer_name;

	public Integer getCustomer_key() {
		return customer_key;
	}

	public void setCustomer_key(Integer customer_key) {
		this.customer_key = customer_key;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
}
