package com.cwc.app.floor.api.zj;

import com.cwc.app.key.WaybillInternalOperKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorWaybillLogMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	public long addWaybillLog(DBRow dbrow)
		throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("waybill_log"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWaybillLogMgrZJ addWaybillLog error:"+e);
		}
	}
	
	/**
	 * 添加追踪类型
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addWaybillTrackingStatusCode(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("waybill_tracking_status_code"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWaybillLogMgrZJ addWaybillTrackingStatusCode error:"+e);
		}
	}
	
	/**
	 * 根据主键ID修改运单日志
	 * @param waybill_log_id
	 * @param para
	 * @throws Exception
	 */
	public void modWayBillLogByWayBillLogId(long waybill_log_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where waybill_log_id = "+waybill_log_id,ConfigBean.getStringValue("waybill_log"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWaybillLogMgrZJ modWayBillLongByWayBillLogId error:"+e);
		}
	}
	
	/**
	 * 根据运单ID修改日志
	 * @param waybill_id
	 * @param para
	 * @throws Exception
	 */
	public void modWayBIllLogWayBillId(long waybill_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where waybill_id="+waybill_id,ConfigBean.getStringValue("waybill_log"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWaybillLogMgrZJ modWayBIllLogWayBillId error:"+e);
		}
	}
	
	public DBRow[] filterWayBillLog(String trackingNumber,String st,String en,int waybill_internal_status,int waybill_external_status,long adid,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String whereTrackingNumber = "";
			String whereOperTime = "";
			String whereWayBillExternalStatus = "";
			String whereWayBillInternalStatus = "";
			String whereOperator = "";
			
			if(!trackingNumber.equals(""))
			{
				whereTrackingNumber = " and trackingNumber = '"+trackingNumber+"'";
			}
			else
			{
				whereOperTime = " and operator_time>'"+st+" 0:00:00' and '"+en+" 23:59:59' ";
				
				if(waybill_external_status>0)
				{
					whereWayBillExternalStatus = " and waybill_external_status = "+waybill_external_status+" ";
				}
				
				if(waybill_internal_status>0)
				{
					whereWayBillInternalStatus = " and waybill_internal_status = "+waybill_internal_status+" ";
				}
				
				if(adid>0)
				{
					whereOperator = " and operator_adid = "+adid+" ";
				}
			}
			
			String sql = "select * from "+ConfigBean.getStringValue("waybill_log")+" where 1=1"+whereOperTime+whereTrackingNumber+whereWayBillInternalStatus+whereWayBillExternalStatus+whereOperator
						+" order by operator_time desc";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorWaybillLogMgrZJ filterWayBillLog error:"+e);
		}
	}
	
	public DBRow getWayBillTrackingStatusCode(String status_code,String shipping_company)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("waybill_tracking_status_code")+" where tracking_status_code =? and shipping_company = ?";
			
			DBRow para = new DBRow();
			para.add("status_code",status_code);
			para.add("shipping_company",shipping_company);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWaybillLogMgrZJ getWayBillTrackingStatusCode error:"+e);
		}
	}
	
	public DBRow[] getWaybillLogByWaybillId(long waybill_id)
		throws Exception
	{
		try {
			String sql = "select * from "+ConfigBean.getStringValue("waybill_log")+" where waybill_id = ?";
			
			DBRow para = new DBRow();
			para.add("waybill_id",waybill_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorWaybillLogMgrZJ getWaybillLogByWaybillId error:"+e);
		}
	}
	
	public DBRow[] needTrackingWaybillLog(String st,String en)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("waybill_log")+" where operator_time>'"+st+" 0:00:00' and '"+en+" 23:59:59' and waybill_internal_status = "+WaybillInternalOperKey.Send+" and (waybill_external_status != 2 or waybill_external_status is NULL) group by waybill_id ";
			
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWaybillLogMgrZJ needTrackingWaybillLog error:"+e);
		}
	}
	
	public DBRow getDetailWayBillLog(long waybill_log_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("waybill_log")+" where waybill_log_id = ?";
			
			DBRow para = new DBRow();
			para.add("waybill_log_id",waybill_log_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorWaybillLogMgrZJ getDetailWayBillLog error:"+e);
		}
	}
	
	public DBRow[] initWaybillLog(int waybill_status,String st,String en)
		throws Exception
	{
		String sql = "select * from waybill_order where status = "+waybill_status+" and create_date >'"+st+" 0:0:00' and create_date<'"+en+" 23:59:59'";
		return dbUtilAutoTran.selectMutliple(sql);
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
