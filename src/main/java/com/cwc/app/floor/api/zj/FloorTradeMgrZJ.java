package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorTradeMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBRow[] getTradeByTradeId4Search(long[] tradeIds)
		throws Exception
	{
		if(tradeIds.length==0)
		{
			return (new DBRow[0]);
		}
		else
		{
			String whereTrades = "";
			
			for (int i = 0; i < tradeIds.length; i++) 
			{
				if(i==0)
				{
					whereTrades += " where trade_id = "+tradeIds[i];
				}
				
				whereTrades += " or trade_id = "+tradeIds[i];
			}
			
			String sql = "select * from "+ConfigBean.getStringValue("trade")+whereTrades;
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
	}
	
	/**
	 * 根据txn_id查询交易信息
	 * @param txn_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTradesByTxnId(String txn_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("trade")+" where txn_id = ?";
			
			DBRow para = new DBRow();
			para.add("txn_id",txn_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTradeMgrZJ getTradesByTxnId error:"+e);
		}
	}
	
	/**
	 * 根据txn_id获得COI
	 * @param txn_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTradeItemsByTradeId(long trade_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("trade_item")+" where trade_id = ?";
			
			DBRow para = new DBRow();
			para.add("trade_id",trade_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTradeMgrZJ getTradeItemsByTxnId error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
