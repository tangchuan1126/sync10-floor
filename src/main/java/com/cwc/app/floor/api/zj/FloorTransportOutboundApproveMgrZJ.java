package com.cwc.app.floor.api.zj;

import javax.mail.Transport;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorTransportOutboundApproveMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 添加发货审核
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addSendApprove(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long tsa_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("transport_send_approve"));
			
			dbrow.add("tsa_id",tsa_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("transport_send_approve"),dbrow);
			
			return (tsa_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportSendApproveMgrZJ addSendApprove error:"+e);
		}
	}
	
	/**
	 * 发货审核明细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addSendApproveDetail(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long tsad_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("transport_send_approve_detail"));
			
			dbrow.add("tsad_id",tsad_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("transport_send_approve_detail"),dbrow);
			
			return (tsad_id);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportSendApproveMgrZJ addSendApproveDetail error:"+e);
		}
	}
	/**
	 * 修改发货审核
	 * @param tsa_id
	 * @param para
	 * @throws Exception
	 */
	public void modSendApprove(long tsa_id,DBRow para)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where tsa_id="+tsa_id,ConfigBean.getStringValue("transport_send_approve"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportSendApproveMgrZJ modSendApprove error:"+e);
		}
	}
	
	/**
	 * 修改发货审核明细
	 * @param tsad_id
	 * @param para
	 * @throws Exception
	 */
	public void modSendApproveDetail(long tsad_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where tsad_id="+tsad_id,ConfigBean.getStringValue("transport_send_approve_detail"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportSendApproveMgrZJ modSendApproveDetail error:"+e);
		}
	}
	
	/**
	 * 过滤发货审核
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param approve_status
	 * @param sorttype
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterTransportOutboundApprove(long send_psid, long receive_psid,PageCtrl pc, int approve_status, String sorttype)
	throws Exception
	{
		try {
			StringBuffer sql = new StringBuffer("select tsa.*,send.title as sendtitle,receive.title as receivetitle from "+ConfigBean.getStringValue("transport_send_approve")+" as tsa"
					+" left join "+ConfigBean.getStringValue("transport")+" as t on t.transport_id = tsa.transport_id"
					+" left join "+ConfigBean.getStringValue("product_storage_catalog")+" as send on send.id = t.send_psid"
					+" left join "+ConfigBean.getStringValue("product_storage_catalog")+" as receive on receive.id = t.receive_psid"
					+" where 1=1");
					if(send_psid>0)
					{
					sql.append(" and t.send_psid = "+send_psid);
					}
					if(receive_psid>0)
					{
					sql.append(" and t.receive_psid = "+receive_psid);
					}
					
					if(approve_status>0)
					{
					sql.append(" and tsa.approve_status = "+approve_status);
					}
					
					if(sorttype.equals("subdate"))
					{
					sql.append(" order by tsa.commit_date desc");
					}
					else if(sorttype.equals("approvedate"))
					{
					sql.append(" order by tsa.approve_date desc");
					}
					else
					{
					sql.append(" order by tsa.tsa_id desc");
					}
					return dbUtilAutoTran.selectMutliple(sql.toString(),pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportSendApproveMgrZJ fillterTransportOutboundApprove error:"+e);
		}
	}
	
	/**
	 * 根据审核ID获得转运发货审核明细
	 * @param tsa_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApproveTransportDetailByTsa(long tsa_id)
		throws Exception
	{
		try 
		{
			String sql = "select *,(transport_send_count-transport_count) as different_count from "+ConfigBean.getStringValue("transport_send_approve_detail")+" where tsa_id=?";
			
			DBRow para = new DBRow();
			para.add("tsa_id",tsa_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorTransportSendApproveMgrZJ getApproveTransportDetailByTsa error:"+e);
		}
	}
	
	/**
	 * 根据审核状态，过滤审核明细
	 * @param tsa_id
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApproveTransportDetailWihtStatus(long tsa_id,int status)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("transport_send_approve_detail")+" where tsa_id = ? and approve_status=?";
		
		DBRow para = new DBRow();
		para.add("tsa_id",tsa_id);
		para.add("approve_status",status);
		
		return (dbUtilAutoTran.selectPreMutliple(sql, para));
	}
	
	/**
	 * 获得差异数额
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApproveSendCount(long tsa_id)
		throws Exception
	{
		String sql = "select tsa.*,(transport_send_count-transport_count) from "+ConfigBean.getStringValue("transport_send_approve_detail")+" as tsad"
					+" left join "+ConfigBean.getStringValue("transport_send_approve")+" as tsa on tsad.tsa_id = tsa.tsa_id"
					+" where tsa.tsa_id =?"; 
		
		DBRow para = new DBRow();
		para.add("tsa_id",tsa_id);
		
		return (dbUtilAutoTran.selectPreMutliple(sql, para));
	}
	
	/**
	 * 获得发货审核详细
	 * @param tsa_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailTransportSendApprove(long tsa_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_send_approve")+" where tsa_id=?";
			
			DBRow para = new DBRow();
			para.add("tsa_id",tsa_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportSendApproveMgrZJ getDetailTransportSendApprove error:"+e);
		}
	}
	
	/**
	 * 添加转运单发货审核序列号差异
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addTransportSendApproveDetailSN(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("transport_send_approve_detail_sn"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportOutboundMgrZJ addTransportSendApproveDetailSN error:"+e);
		}
	}
	
	/**
	 * 修改发货序列号差异审核
	 * @param tsads_id
	 * @param para
	 * @throws Exception
	 */
	public void modTransportSendApproveDetailSN(long tsads_id,DBRow para )
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where tsads_id="+tsads_id,ConfigBean.getStringValue("transport_send_approve_detail_sn"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportSendApproveMgrZJ modTransportSendApproveDetailSN error:"+e);
		}
	}
	
	/**
	 * 根据审核单ID获得发货序列号差异
	 * @param tsa_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportSendApproveDetailSNByTsaId(long tsa_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("transport_send_approve_detail_sn")+" where tsa_id = ? ";
			
			DBRow para = new DBRow();
			para.add("tsa_id",tsa_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorTransportSendApproveMgrZJ modTransportSendApproveDetailSN error:"+e);
		}
	}
	
	/**
	 * 根据状态获得序列号发货差异审核明细
	 * @param tsa_id
	 * @param approve_status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTransportSendApproveDetailSNWithStatus(long tsa_id,int approve_status)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("transport_send_approve_detail_sn")+" where tsa_id = ? and approve_status = ? ";
		
		DBRow para = new DBRow();
		para.add("tsa_id",tsa_id);
		para.add("approve_status",approve_status);
		
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
