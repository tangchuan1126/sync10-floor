package com.cwc.app.floor.api.zj;

import com.cwc.app.key.ReturnProductKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorReturnProductMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 创建退货单
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addReturnProduct(DBRow dbrow)
		throws Exception
	{
		try
		{
			long rp_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("return_product"));
			dbrow.add("rp_id",rp_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("return_product"),dbrow);
			return(rp_id);
		}
		catch (Exception e)
		{
			throw new Exception("addReturnInfoByRpId(row) error:" + e);
		}
	}
	
	/**
	 * 根据ID获得详细
	 * @param rp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailReturnProduct(long rp_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("return_product")+" where rp_id = ?";
			
			DBRow para = new DBRow();
			para.add("rp_id",rp_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductMgrZJ getDetailReturnProduct error:"+e);
		}
	}
	
	/**
	 * 修改退货单
	 * @param rp_id
	 * @param para
	 * @throws Exception
	 */
	public void modReturnProduct(long rp_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where rp_id="+rp_id,ConfigBean.getStringValue("return_product"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductMgrZJ modReturnProduct error:"+e);
		}
	}
	
	/**
	 * 删除退货单明细
	 * @param rp_id
	 * @throws Exception
	 */
	public void delReturnProductItemsByRpId(long rp_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where rp_id=" + rp_id,ConfigBean.getStringValue("return_product_items"));
		}
		catch (Exception e)
		{
			throw new Exception("delReturnProductItemsByRpId(row) error:" + e);
		}
	}
	
	/**
	 * 删除退货单打散明细
	 * @param rp_id
	 * @throws Exception
	 */
	public void delReturnProductSubItemsByRpId(long rp_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where rpi_id in (select rpi_id from " + ConfigBean.getStringValue("return_product") + " rp, "+ConfigBean.getStringValue("return_product_items")+"  rpi  where rp.rp_id="+rp_id+" and rp.rp_id=rpi.rp_id )",ConfigBean.getStringValue("return_product_sub_items"));
		}
		catch (Exception e)
		{
			throw new Exception("delReturnProductSubItemsByRpId(row) error:" + e);
		}
	}
	
	/**
	 * 添加退货单明细
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addReturnProductItems(DBRow row)
		throws Exception
	{
		try
		{
			long rpi_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("return_product_items"));
			row.add("rpi_id",rpi_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("return_product_items"),row);
			return(rpi_id);
		}
		catch (Exception e)
		{
			throw new Exception("addReturnProductItems(row) error:" + e);
		}
	}
	
	/**
	 * 添加退货单拆散明细
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addReturnProductSubItems(DBRow row)
		throws Exception
	{
		try
		{
			long rpsi_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("return_product_sub_items"));
			row.add("rpsi_id",rpsi_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("return_product_sub_items"),row);
			return(rpsi_id);
		}
		catch (Exception e)
		{
			throw new Exception("addReturnProductSubItems(row) error:" + e);
		}
	}
	
	/**
	 * 添加质保商品原因
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addReturnProductReason(DBRow row)
		throws Exception
	{
		try {
			long rps_id = dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("return_product_reason"),row);
			
			return (rps_id);
		} catch (Exception e) 
		{
			throw new Exception("FloorReturnProductMgrZJ addReturnProductReason error:"+e);
		}
	}
	
	/**
	 * 获得这个退货的全部质保原因
	 * @param rp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllReturnReason(long rp_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("return_product_reason")+" where rp_id=?";
			
			DBRow para = new DBRow();
			para.add("rp_id",rp_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductMgrZJ getAllReturnReason error:"+e);
		}
	}
	
	public DBRow[] filterReturnProduct(int status,int product_status,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("return_product") + " where 1=1 ";
			
			if(status>=0)
			{
				sql += " and status = "+status;
			}
			
			if(product_status>0)
			{
				sql += " and product_status = "+product_status; 
			}
			
			sql += " order by rp_id desc";
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductMgrZJ filterReturnProduct error:" + e);
		}
	}
	
	/**
	 * 过滤客户对商品意见
	 * @param st
	 * @param en
	 * @param warranty_type
	 * @param catalog_id
	 * @param product_line_id
	 * @param pc_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] filterReturnProductReason(String st,String en,int warranty_type,long catalog_id,long product_line_id,long pc_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String whereProductLine = "";
			if(product_line_id>0)
			{
				whereProductLine = " and pc.product_line_id = "+product_line_id+" ";
			}
			
			String whereCatalog = "";
			if(catalog_id>0)
			{
				whereCatalog = " join pc_child_list as pcl on pcl.pc_id = pc.id and pcl.search_rootid = "+catalog_id+" ";
			}
			
			
			String whereProduct = "";
			if(pc_id>0)
			{
				whereProduct = " and rpr.pc_id = "+pc_id+" ";
			}
			
			String sql = "select * from "+ConfigBean.getStringValue("return_product_reason")+" as rpr " 
			+"join product as p on p.pc_id = rpr.pc_id "+whereProduct
			+"join product_catalog as pc on p.catalog_id = pc.id "+whereProductLine
			+whereCatalog
			+" where rpr.create_time between '"+st+" 0:00:00' and '"+en+" 23:59:59'";
			
			if(warranty_type>0)
			{
				sql +=" and rpr.warranty_type = "+warranty_type;
			}
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductMgrZJ filterReturnProductReason error:"+e);
		}
	}
	
	/**
	 * 账单付款创建订单后管理服务
	 * @param bill_id
	 * @param oid
	 * @throws Exception
	 */
	public void warrantyInvoiceHasPay(long bill_id,long oid)
		throws Exception
	{
		DBRow para = new DBRow();
		para.add("warranty_oid",oid);
		para.add("status",ReturnProductKey.CREATEORDER);
		
		dbUtilAutoTran.update(" where bill_id="+bill_id,ConfigBean.getStringValue("return_product"),para);
	}
	
	/**
	 * 根据服务单搜索退货服务理由
	 * @param rp_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchReturnProductReasonByRpid(long rp_id,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("return_product_reason")+" as rpr where rp_id = ? ";
			
			DBRow para = new DBRow();
			para.add("rp_id",rp_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql,para,pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductMgrZJ searchReturnProductReasonByRpid error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
