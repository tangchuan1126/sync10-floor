package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorCommonFileMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public long addFile(DBRow row) throws Exception{
		try{
			 String tableName =  ConfigBean.getStringValue("common_file");
			 long id =  dbUtilAutoTran.insertReturnId(tableName, row);
			 return id ;
		}catch(Exception e){
			throw new Exception("FloorCommonFileMgrZr.addFile(row):"+e);
		}
	}
	public DBRow getCommonFileByFileId(long fileId) throws Exception{
		try{
			 String tableName =  ConfigBean.getStringValue("common_file");
			 String sql = "select * from " + tableName + " where id=" + fileId;
			 return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCommonFileMgrZr.getCommonFileByFileId(fileId):"+e);
		}
	}
	/**
	 * 删除文件与具体义务的关联
	 * @param fileId
	 * @throws Exception
	 */
	public void deleteTheFileBussines(long fileId) throws Exception {
		try{
			 String tableName =  ConfigBean.getStringValue("common_file");
			 DBRow row = new DBRow();
			 row.add("bussines_id", 0);
			 row.add("bussines_table", "");
			 dbUtilAutoTran.update(" where id = " + fileId, tableName, row);
		}catch(Exception e){
			throw new Exception("FloorCommonFileMgrZr.getCommonFileByFileId(fileId):"+e);
		}
	}
}
