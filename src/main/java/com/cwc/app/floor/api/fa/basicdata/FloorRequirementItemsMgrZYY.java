package com.cwc.app.floor.api.fa.basicdata;

import org.springframework.transaction.annotation.Transactional;

import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.cwc.app.floor.api.fa.basicdata.RequirementItem;
import com.cwc.app.floor.api.fa.basicdata.RequirementItemDetails;

/**  
 * 
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.floor.FloorRequirementItemsMgrZYY.java] 
 * @ClassName:    [FloorRequirementItemsMgrZYY]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年4月5日 上午10:16:54]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年4月5日 上午10:16:54]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
public class FloorRequirementItemsMgrZYY {
	private DBUtilAutoTran dbUtilAutoTran;
	private final static String TABLE = "requirement_item";
	private final static String TABLE_DETAILS = "requirement_item_details";
	
	/**
	 * 
	 * @param data
	 * @param children
	 * @return 0：失败 ,1：成功, 2,:已存在
	 * @throws Exception
	 */
	@Transactional
	public int add(DBRow data,DBRow[] children) throws Exception{
		//TODO  check Name IF EXISTS  
		try{
			if(checkExistsByName(data.get(RequirementItem.REQUIREMENT_NAME, ""),0)){
				return 2;
			}
			long rid= dbUtilAutoTran.insertReturnId(TABLE, data);
			if(null!=children &&children.length>0){
				for(DBRow row:children){
					row.add("ri_id", rid);
					//TODO 保证同一个RequirementItem 的值不唯一，要再后台做检查
					dbUtilAutoTran.insert(TABLE_DETAILS, row);
				}
			}
			return 1;
		}catch(Exception e){
			throw new Exception("FloorRequirementItemsMgrZYY->add" + e);
		}
	}
	/**
	 * 修改 
	 * @param row
	 * @param children
	 * @return 0：失败 ,1：成功, 2,:已存在
	 * @throws Exception 
	 */
	@Transactional
	public int mod(DBRow row, DBRow[] children) throws Exception {
		try{
			if(checkExistsByName(row.get(RequirementItem.REQUIREMENT_NAME, ""),row.get(RequirementItem.REQUIREMENT_ID, -1))){
				return 2;
			}else{
				String sql = " where 1=1 and ri_id = "+row.get(RequirementItem.REQUIREMENT_ID);
				dbUtilAutoTran.update(sql, TABLE, row);
				//TODO 修改children
				if(null!=children &&children.length>0){
					for(DBRow _row:children){
						_row.add("ri_id", _row.get(RequirementItem.REQUIREMENT_ID));
						String rid_id = _row.get(RequirementItemDetails.REQUIREMENT_DETAILS_ID,"");
						if(!StrUtil.isBlankNullUndefined(rid_id)){
							//TODO update 
							String _sql = " where 1=1 and "+RequirementItemDetails.REQUIREMENT_DETAILS_ID +"="+rid_id;
							dbUtilAutoTran.update(_sql, TABLE_DETAILS, _row);
						}else{
							//
							_row.remove(RequirementItemDetails.REQUIREMENT_DETAILS_ID);
							dbUtilAutoTran.insert(TABLE_DETAILS, _row);
						}
						
						
					}
				}else{
					String sql1 = " where ri_id = " + row.get(RequirementItem.REQUIREMENT_ID);
					dbUtilAutoTran.delete(sql1, TABLE_DETAILS);
				}
			}
			return 1;
		}catch(Exception e){
			throw new Exception("FloorRequirementItemsMgrZYY->mod" + e);
		}
	}
	/**
	 * 
	 * @param id
	 * @param flag
	 * @return
	 * @throws Exception 
	 */
	public DBRow getById(int id ,boolean flag ) throws Exception{
		try{
			String sql = "select * from "+TABLE+" where 1=1 and ri_id = "+id +" limit 1";
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			if(flag){
				String _sql ="select * from "+TABLE_DETAILS +" where 1=1 and ri_id = "+id;
				DBRow[] values = dbUtilAutoTran.selectMutliple(_sql);
				row.add("children", values);
			}
			return row;
		}catch(Exception e){
			throw new Exception("FloorRequirementItemsMgrZYY->getById"+e);
		}
		
	}
	
	/**
	 * 根据名字检测是否存在
	 * @param name RequirementItem Name 
	 * @param ri_id ID，如果是修改的时候，传 RequirementItem ID，如果是添加 传 0
	 * @return
	 * @throws Exception
	 */
	public boolean checkExistsByName(String name,int ri_id) throws Exception{
		try {
			DBRow para = new DBRow();
			para.add(RequirementItem.REQUIREMENT_NAME, name);
			StringBuffer sql = new StringBuffer("select * from ");
			sql.append( TABLE).append(" WHERE ")
				.append(RequirementItem.REQUIREMENT_NAME).append(" = ? ");
				if(ri_id>0){
					sql.append(" AND ").append(RequirementItem.REQUIREMENT_ID)
					.append(" != ? ");
					para.add(RequirementItem.REQUIREMENT_ID, ri_id);
				}
//			sql.append("limit 1");
			
			DBRow[] result = dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
			return result!=null && result.length>0;
		} catch (Exception e) {
			throw new Exception("FloorRequirementItemsMgrZYY->checkExistsByName" + e);
		}
	}
	
	public boolean checkItemValueExistsByName(String item_name,int ri_id,int rid_id) throws Exception{
		StringBuffer sql = new StringBuffer("SELECT * FROM ");
		sql.append(TABLE_DETAILS).append(" WHERE 1=1")
			.append(" AND item_name=? AND ri_id=? AND rid_id!=?");
		DBRow params = new DBRow();
		params.put("item_name", item_name);
		params.put("ri_id", ri_id);
		params.put("rid_id", rid_id);
		DBRow[] rows = dbUtilAutoTran.selectPreMutliple(sql.toString(), params);
		return rows.length>0;
	}
	/**
	 * @CreateTime 2015年4月10日 15:30:42
	 * 删除RequirementItems,如果在使用时，不能删除，
	 * 如果没有使用，责删除该项，并删除该项下的配置value
	 * @param ri_id
	 * @return -1 使用中，0 删除是吧 ，1删除成功
	 * @throws Exception
	 * @author zhaoyy
	 * 
	 */
	public int delItem(long ri_id) throws Exception{
		//TODO 检测是否应用到该Requirement 
		if(checkUsed(ri_id)){
			return -1;
		}
		try{
			StringBuffer wherePart = new StringBuffer(" WHERE 1=1 ");
			wherePart.append(" AND ri_id = ? ");
			DBRow condparams = new DBRow();
			condparams.add("ri_id", ri_id);
			
			dbUtilAutoTran.deletePre(wherePart.toString(), condparams, TABLE);
			dbUtilAutoTran.deletePre(wherePart.toString(), condparams, TABLE_DETAILS);
			return 1;
		}catch (Exception e){
			throw new Exception("FloorRequirementItemsMgrZYY ->delItem");
		}
	}
	
	public DBRow[] getAllRequirement(boolean flag  ,PageCtrl pc, DBRow row) throws Exception{
		try{
			String sql = "SELECT * FROM " +TABLE + " WHERE 1=1";
			if(row.get("searchDisplaytype", 0l) != 0){
				sql += " and display_type = " + row.get("searchDisplaytype", 0l);
			}
			if(!"".equals(row.getString("searchRname"))){
				sql += " and requirement_name like '%" + row.getString("searchRname")+"%'";
			}
			//searchIsactive
			if(row.get("searchIsactive", 0l) == 2){
				
			}else{
				sql += " and active = " + row.get("searchIsactive", 0l);
			}
			sql+=" order by ri_id desc";

			DBRow[] rows;
			if(pc!=null){
				
				rows=dbUtilAutoTran.selectMutliple(sql, pc);
				
			}else{
				rows=dbUtilAutoTran.selectMutliple(sql);
			}
			
			if(null!=rows && rows.length>0 && flag){
				for(DBRow r :rows){
					sql = "SELECT * FROM "+TABLE_DETAILS+" where ri_id = "+r.get("ri_id", -1)+" order by sort asc";
					r.add("children",dbUtilAutoTran.selectMutliple(sql));
				}
			}
			return rows;
		}catch(Exception e){
			throw new Exception(" FloorRequirementItemsMgrZYY ->getAllRequirement");
		}
	}
	
	/**
	 * @CreateTime 2015年4月10日 15:15:48
	 * 根据ri_id检测Requirement是否应用在配置中，
	 * 如果应用在配置中返回“true”否是返回“false” 
	 * @param ri_id
	 * @return
	 * @throws Exception 
	 */
	public boolean checkUsed(long ri_id) throws Exception{
		
		boolean flag = false;
		try{
			StringBuffer sql = new StringBuffer("SELECT * FROM ");
			sql.append(" title_ship_config_detail WHERE 1=1 AND ri_id = ?");
			DBRow para = new DBRow();
			para.put("ri_id", ri_id);
			DBRow[] rows = dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
			if(rows!=null && rows.length>0){
				flag = true;
			}
		}catch(Exception e){
			throw new Exception("FloorRequirementItemsMgrZYY ->checkUsed");
		}
		return flag;
	}
	
	public int updateActive(DBRow dbRow) throws Exception {
		try {
			String sql = "where ri_id = "
					+ dbRow.get("ri_id", -1L);
			return dbUtilAutoTran.update(sql, TABLE, dbRow);
		} catch (Exception e) {
			throw new Exception("FloorRequirementItemsMgrZYY.updateActive(DBRow dbRow) error : "+ e);
		}
	}
	
	
	public DBUtilAutoTran getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	
	
	
}
