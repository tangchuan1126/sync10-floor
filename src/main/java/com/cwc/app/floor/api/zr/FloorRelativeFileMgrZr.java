package com.cwc.app.floor.api.zr;

import com.cwc.app.key.FileWithTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorRelativeFileMgrZr {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}	
	
	public long addRelativeFile(DBRow row) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("relative_file") ;
			return dbUtilAutoTran.insertReturnId(tableName, row);
		}catch (Exception e) {
			throw new Exception("FloorRelativeFileMgrZr.addRelativeFile(row):"+e);
		}
	}
	 
	/**
	 * @param relative_value
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getRelativeFile(String relative_value, int type) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("relative_file") ;
			String sql = "select * from " + tableName + " where relative_value = ? and relative_type = ? order by relative_id desc";
			DBRow para = new DBRow();
			para.add("relative_value", relative_value);
			para.add("relative_type", type);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch (Exception e) {
			throw new Exception("FloorRelativeFileMgrZr.addRelativeFile(row):"+e);
		}
	}


	/*public DBRow[] getRelativeFile(DBRow filterRows, int type) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("relative_file") ;
			String sqlCreateTempTable = "CREATE TEMPORARY TABLE  IF NOT EXISTS tb_masterbol(relative_value varchar(100))";
			String sql = "select * from " + tableName + " where relative_value = ? and relative_type = ? order by relative_id desc";
			DBRow para = new DBRow();
			para.add("relative_value", relative_value);
			para.add("relative_type", type);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch (Exception e) {
			throw new Exception("FloorRelativeFileMgrZr.addRelativeFile(row):"+e);
		}
	}*/



	/**
	 * @param file_id
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public void updateFileWithType(String file_id) throws Exception{
		try{
			if(!StrUtil.isBlank(file_id)){
				String tableName = ConfigBean.getStringValue("file") ;
				DBRow row=new DBRow();
				row.add("file_with_type", FileWithTypeKey.BOL_SIGNATURE);
				row.add("expire_in_secs", 0);
				dbUtilAutoTran.update(" where file_id="+file_id, tableName, row);	
			}
		}catch (Exception e) {
			throw new Exception("FloorRelativeFileMgrZr.updateFileWithType(file_id):"+e);
		}
	}
	/**
	 * @param file_id
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] batchUploadSignature() throws Exception{
		try{
		    return dbUtilAutoTran.select("relative_file");
		}catch (Exception e) {
			throw new Exception("FloorRelativeFileMgrZr.batchUploadSignature():"+e);
		}
	}
	/**
	 * @param relative_id
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public void updateRelativeFilePath(String relative_id,String file_id) throws Exception{
		try{
		    if(!StrUtil.isBlank(relative_id)){
		    	DBRow row=new DBRow();
		    	row.add("relative_file_path", file_id);
		    	dbUtilAutoTran.update(" where relative_id="+relative_id, "relative_file", row);
		    }
		}catch (Exception e) {
			throw new Exception("FloorRelativeFileMgrZr.updateRelativeFilePath():"+e);
		}
	}
}
