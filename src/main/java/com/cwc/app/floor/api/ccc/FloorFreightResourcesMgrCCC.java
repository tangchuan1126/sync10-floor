package com.cwc.app.floor.api.ccc;


import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorFreightResourcesMgrCCC {
	
	private DBUtilAutoTran dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	/**
	 * 添加货运资源
	 * @param dbrow
	 * @throws Exception
	 */
	public void addFreightResources(DBRow dbrow) throws Exception{
		try 
		{
		//	long freight_resources_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("freight_resources"));
			
			//dbrow.add("fr_id",freight_resources_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("freight_resources"),dbrow);
			
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorFreightResourcesMgrCCC addFreightResources error:"+e);
		}
	}
	
	/**
	 * 通过条件获得货运资源分页列表
	 * @param fr_company
	 * @param fr_undertake_company
	 * @param fr_way
	 * @param fr_from_country
	 * @param fr_from_port
	 * @param fr_to_country
	 * @param fr_to_portString
	 * @param cmd
	 * @param company
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	 public DBRow[] getFreightResourcesByCondition(String fr_company,String fr_undertake_company,String fr_way,String fr_from_country,String fr_from_port,String fr_to_country,String fr_to_portString,String cmd,String company, PageCtrl pc)
	    throws Exception
	    {
	    	try
	    	{
	    		StringBuffer sqlCondition=new StringBuffer();
	    		if(cmd != null && cmd.equals("search")){
	    			sqlCondition.append("where");
	    			if(!company.equals("")) {
			    		sqlCondition.append(" fr_company like '%"+company+"%'");
			    		sqlCondition.append(" or fr_undertake_company like '%"+company+"%'");
			    	}
	    		}else if(cmd != null && cmd.equals("filter")){
	    			sqlCondition.append("where");
	    			if(!fr_company.equals("")){
	    				sqlCondition.append(" fr_company = '"+fr_company+"'");
	    			}
	    			if(!fr_undertake_company.equals("")){
	    				sqlCondition.append(" fr_undertake_company = '"+fr_undertake_company+"'");
	    			}
	    			if(fr_way.equals("0")){
	    				sqlCondition.append(" fr_way is not null");
	    			}else{
	    				sqlCondition.append(" fr_way = '"+fr_way+"'");
	    			}
	    			if(!fr_from_country.equals("0")){
	    				sqlCondition.append(" and fr_from_country = '"+fr_from_country+"'");
	    			}
	    			if(!fr_from_port.equals("")){
	    				sqlCondition.append(" and fr_from_port like '%"+fr_from_port+"%'");
	    			}
	    			if(!fr_to_country.equals("0")){
	    				sqlCondition.append(" and fr_to_country = '"+fr_to_country+"'");
	    			}
	    			
	    			if(!fr_to_portString.equals("")){
	    				sqlCondition.append(" and fr_to_port like '%"+fr_to_portString+"%'");
	    			}
	    			
	    		}
	    		String sql = "select * from "
					+ ConfigBean.getStringValue("freight_resources")+" "+sqlCondition.toString()+" order by fr_id DESC ";
	    		
				if(pc!=null)
				{
					return (dbUtilAutoTran.selectMutliple(sql,pc));
				}
				else
				{
					
					return (dbUtilAutoTran.selectMutliple(sql));
				}
	    	}
	    	catch(Exception e)
	    	{
	    		throw new Exception("FloorApplyMoneyMgrZZZ.getApplyMoneyByCondition(sql)"+e);
	    	}
	    }
	 
	 /**
	  * 
	  * @param fr_company
	  * @param fr_undertake_company
	  * @param pc
	  * @return
	  * @throws Exception
	  */
	 public DBRow[] listFreightResources(String fr_company,String fr_undertake_company, PageCtrl pc)
	    throws Exception
	    {
	    	try
	    	{
	    		StringBuffer sqlCondition=new StringBuffer();
		    	sqlCondition.append("where");
		    	if(!fr_company.equals("")) {
		    		sqlCondition.append(" fr_company = '"+fr_company+"'");
		    	}
		    	if(!fr_undertake_company.equals("")) {
		    		sqlCondition.append(" or fr_undertake_company = '"+fr_undertake_company+"'");
		    	}
		    	
	    		String sql = "select * from "
					+ ConfigBean.getStringValue("freight_resources")+" "+sqlCondition.toString()+" order by fr_id DESC ";
	    		
				if(pc!=null)
				{
					return (dbUtilAutoTran.selectMutliple(sql,pc));
				}
				else
				{
					
					return (dbUtilAutoTran.selectMutliple(sql));
				}
	    	}
	    	catch(Exception e)
	    	{
	    		throw new Exception("listFreightResources(sql)"+e);
	    	}
	    }
	 
	 /**
	  * 通过货运资源ID获得此条货运资源
	  * @param fr_id
	  * @return
	  * @throws Exception
	  */
	 public DBRow getFreightResourcesById(String fr_id) throws Exception{
			String sql = "";
			sql = "select * from freight_resources where fr_id = "+fr_id;
			return dbUtilAutoTran.selectSingle(sql);
	 }
	 
	 /**
	  * 通过货运资源ID更新此条货运资源
	  * @param dbrow
	  * @param fr_id
	  * @throws Exception
	  */
	 public void updateFreightResources(DBRow dbrow,long fr_id) throws Exception{
		 try 
			{
				dbUtilAutoTran.update("where fr_id="+fr_id, ConfigBean.getStringValue("freight_resources"), dbrow);
			} 
			catch (Exception e) 
			{
				throw new Exception("updateFreightResources(row,sid) error: "+e);
			}
		}
	 
	 /**
	  * 通过货运资源ID删除此条货运资源
	  * @param fr_id
	  * @throws Exception
	  */
	 public void deleteFreightResources(long fr_id) throws Exception{
		 try 
			{
				dbUtilAutoTran.delete("where fr_id="+fr_id, ConfigBean.getStringValue("freight_resources"));
			} 
			catch (Exception e) 
			{
				throw new Exception("deleteFreightResources(sid) error: "+e);
			}
		}
	 
	 /**
	  * 
	  * @param id
	  * @return
	  * @throws Exception
	  */
	 public DBRow getCountryById(String id) throws Exception{
			String sql = "";
			sql = "select * from country_code where ccid = "+id;
			return dbUtilAutoTran.selectSingle(sql);
	 }
}
