package com.cwc.app.floor.api.zr;

import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorStorageLocationAreaZr {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	/**
	 * 
	 * @param ps_id
	 * @param area_type(0 未知，1存货区域， 2装卸货区域， 3停车位区域)
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageLocationAreaByPsId(long ps_id , int area_type) throws Exception {
		try{
		//	String tableName =  ConfigBean.getStringValue("storage_location_area");
			DBRow para = new DBRow();
			para.add("area_psid", ps_id);
			para.add("area_type", area_type);
			String sql ="SELECT t.area_id,sla.area_name, SUM(t.z) count,MIN(t.yc_no) min,MAX(t.yc_no) max,sla.patrol_time"+
						" FROM(SELECT y.area_id, CAST(y.yc_no AS SIGNED) yc_no,if(y.yc_status=2,1,0) z from storage_yard_control y where y.ps_id="+ps_id+") t "+
						" LEFT JOIN storage_location_area sla on sla.area_id = t.area_id where area_type="+area_type+" and sla.area_psid="+ps_id+" GROUP BY t.area_id " +
					    " UNION SELECT 0,'ALL', SUM(t.z) count,MIN(t.yc_no) min,MAX(t.yc_no) max,'' FROM(SELECT y.area_id, CAST(y.yc_no AS SIGNED) yc_no,if(y.yc_status=2,1,0) z " +
					    " from storage_yard_control y where y.ps_id="+ps_id+") t ";
			return dbUtilAutoTran.selectMutliple(sql);
  		}catch(Exception e){
			throw new Exception("FloorStorageLocationAreaZr.getStopAreaByPsId(ps_id):"+e);
		}
	}
	/**
	 * 根据area_id 去获取 。这个area下面的stops
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStop(long area_id) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("storage_yard_control");
			DBRow para = new DBRow();
			para.add("area_id", area_id);
			return dbUtilAutoTran.selectPreMutliple("select * from " + tableName +" where area_id = ? ",para );
  		}catch(Exception e){
			throw new Exception("FloorStorageLocationAreaZr.getStop(ps_id):"+e);
		}
	}
	/**
	 * 获取这个stop area 下面所有对应的 stop 系统记录的checkinMain 的单据信息
	 * @param area_id,yc_status(2表示占用状态)
	 * @return	
	 * @throws Exception
	 */
	public DBRow[] getStorageYardControlCheckInMainInfo(long area_id ) throws Exception{
		try{
			String sql = " SELECT syc.*,dlom.dlo_id,dlom.gate_liscense_plate,dlom.gate_container_no FROM "  ;
				  sql += " storage_yard_control AS syc " ;
				  sql += " LEFT JOIN door_or_location_occupancy_main AS dlom ON dlom.yc_id = syc.yc_id where  syc.yc_status = 2 and syc.area_id = ? and dlom.yc_id > 0 ";
				  DBRow	para = new DBRow();
				  para.add("area_id", area_id);
				  return dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch (Exception e) {
			throw new Exception("FloorStorageLocationAreaZr.getStorageYardControlCheckInMainInfo(area_id):"+e);
		}
	}
	public DBRow[] getDoorAreaByPsId(long ps_id, int area_type) throws Exception {
		try{
			//	String tableName =  ConfigBean.getStringValue("storage_location_area");
				DBRow para = new DBRow();
				para.add("area_psid", ps_id);
				para.add("area_type", area_type);
				String sql = "SELECT t.area_id,a.area_name,SUM(t.z) count,min(name) min,MAX(name) max ,a.patrol_time from ("+
								" SELECT s.area_id,CAST(s.doorid AS SIGNED) name,"+
								" IFNULL((SELECT 1 from door_or_location_occupancy_main m,door_or_location_occupancy_details d "+
								"	where d.dlo_id = m.dlo_id and  IFNULL(m.status,m.tractor_status) <>  "+CheckInMainDocumentsStatusTypeKey.LEFT+" and d.rl_id = s.sd_id and d.occupancy_status =2 group by s.sd_id),0) z"+
								"	from storage_door s where s.sd_status=0 and s.ps_id="+ps_id+") t"+
								" LEFT JOIN storage_location_area a on a.area_id = t.area_id where  area_type="+area_type+" and a.area_psid="+ps_id+" GROUP BY t.area_id " +
								" UNION"+
								" SELECT 0,'ALL',SUM(t.z) count,min(name) min,MAX(name) max ,'' from ("+
								" SELECT s.area_id,CAST(s.doorid AS SIGNED) name,"+
								" IFNULL((SELECT 1 from door_or_location_occupancy_main m,door_or_location_occupancy_details d "+
								" where d.dlo_id = m.dlo_id and  IFNULL(m.status,m.tractor_status) <> "+CheckInMainDocumentsStatusTypeKey.LEFT+" and d.rl_id = s.sd_id and d.occupancy_status =2 group by s.sd_id),0) z"+
								" from storage_door s where s.sd_status=0 and s.ps_id="+ps_id+") t";
				return dbUtilAutoTran.selectMutliple(sql);
	  		}catch(Exception e){
				throw new Exception("FloorStorageLocationAreaZr.getDoorAreaByPsId(ps_id):"+e);
			}
	}
	
}
