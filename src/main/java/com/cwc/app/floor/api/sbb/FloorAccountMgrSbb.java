package com.cwc.app.floor.api.sbb;

import java.util.Map;

import com.cwc.app.key.StorageTypeKey;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

/**
 * 账号管理DAO
 * @since Sync10-ui 1.0
 * @author subin
 * */
public class FloorAccountMgrSbb{
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	/**
	 * 获取SEQ
	 * @since Sync10-ui 1.0
	 * @author subin
	 * @throws Exception
	**/
	public long getSequance(String seqName) throws Exception{
		
		return dbUtilAutoTran.getSequance(seqName);
	}
	
	/**
	 * 账号管理 >> 通过ID获得账户信息
	 * @param 账号ID
	 * @return 单个账号信息
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow getAccountById(long id) throws Exception{
		
		try {
			
			String sql = "select * from admin where adid = "+id;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountById(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 通过帐号ID获得帐号信息
	 * @param 账号名(唯一)(用于验证)
	 * @return 单个账号信息
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getAccountByAccount(String parameter) throws Exception{
		
		try {
			
			String sql = "select * from admin where account = '"+parameter+"'";
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountByAccount(String parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取全部账号
	 * @param 分页
	 * @return 所有账号信息
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow[] getAccountList(PageCtrl pc) throws Exception{
		
		try {
			
			String sql = "select adid,account,last_login_date,llock,employe_name,email,Entrytime,mobilePhone,register_token,office_location,ol.office_name,corporation_id, corporation_type from admin LEFT OUTER JOIN office_location as ol on admin.office_location = ol.id";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountList(PageCtrl pc) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取全部账号
	 * @param 查询条件 分页
	 * @return 所有账号信息
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAccountList(Map<String,Object> parameter) throws Exception{
		
		try {
			
			String searchCorporation = (String) parameter.get("searchCorporation");
			String searchCorporationType = (String) parameter.get("searchCorporationType");
			String searchConditions = (String) parameter.get("searchConditions");
			String searchDeptment = (String) parameter.get("searchDeptment");
			String searchPost = (String) parameter.get("searchPost");
			String searchWarehouse = (String) parameter.get("searchWarehouse");
			String searchArea = (String) parameter.get("searchArea");
			String searchLocation = (String) parameter.get("searchLocation");
			
			String searchFingerprint = (String) parameter.get("searchFingerprint");
			String searchToken = (String) parameter.get("searchToken");
			String searchPermission = (String) parameter.get("searchPermission");
			String searchTitle = (String) parameter.get("searchTitle");
			String searchUploadPhoto = (String) parameter.get("searchUploadPhoto");
			String searchShield = (String) parameter.get("searchShield");
			
			String sql = "select adid,account,last_login_date,llock,employe_name,email,Entrytime,mobilePhone,register_token,office_location,ol.office_name,ol.house_number,ol.street,ol.city,ol.zip_code,ol.provinces_name,ol.nation_name,ol.contact,ol.phone,corporation_id, corporation_type from admin LEFT OUTER JOIN office_location as ol on admin.office_location = ol.id where 1=1";
			
			if(!searchConditions.equals("")){
				
				sql += " and (admin.adid = '"+searchConditions+"')";
			}
			
			if(searchCorporation!=null && !searchCorporation.equals("")){
				
				sql += " and (admin.corporation_id = '"+searchCorporation+"')";
				
			}
			
			if(searchCorporationType!=null && !searchCorporationType.equals("")){
				
				sql += " and (admin.corporation_type = '"+searchCorporationType+"')";
			}
			
			if(!searchDeptment.equals("")){
				
				sql +=" and exists(select * from admin_department as adt where adt.adid = admin.adid and adt.department_id = '"+searchDeptment+"')";
			}
			if(!searchPost.equals("")){
				
				sql +=" and exists(select * from admin_department as adt where adt.adid = admin.adid and adt.post_id = '"+searchPost+"')";
			}
			if(!searchWarehouse.equals("")){
				
				sql +=" and exists(select * from admin_warehouse as awe where awe.adid = admin.adid and awe.warehouse_id = '"+searchWarehouse+"')";
			}
			if(!searchArea.equals("")){
				
				sql +=" and exists(select * from admin_warehouse as awe where awe.adid = admin.adid and awe.area_id = '"+searchArea+"')";
			}
			if(!searchLocation.equals("")){
				
				sql += " and get_office_location_belong(if(admin.office_location ='',0,admin.office_location),"+searchLocation+") = '1'";
			}
			
			if(searchFingerprint.equals("true")){
				
				sql +=" and (admin.fingercode is null) ";
			}
			if(searchToken.equals("true")){
				
				sql +=" and (admin.register_token is null or admin.register_token = '') ";
			}
			if(searchPermission.equals("true")){
				
				sql +=" and NOT exists( select * from turboshop_control_tree_map_extend as tctme where tctme.adid = admin.adid ) ";
				
				sql +=" and NOT exists( select * from turboshop_admin_group_auth_extend as tagae where tagae.adid = admin.adid ) ";
				
				sql +=" and NOT exists( select * from admin_group_auth where adgid in (select department_id from admin_department where adid = admin.adid group by department_id)) ";
				
				sql +=" and NOT exists( select * from turboshop_control_tree_map where adgid in (select department_id from admin_department where adid = admin.adid group by department_id) ) ";
			}
			if(searchTitle.equals("true")){
				
				sql +=" and NOT exists(select * from title_admin where title_admin_adid = admin.adid) ";
			}
			if(searchUploadPhoto.equals("true")){
				
				sql +=" and NOT exists(select * from admin_file where admin_file.adid = admin.adid)";
			}
			if(searchShield.equals("true")){
				
				sql +=" and admin.llock = '1'";
			}
			
			sql += " order by admin.adid desc ";
			
			return dbUtilAutoTran.selectMutliple(sql, (PageCtrl)parameter.get("PageCtrl"));
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountList(Map<String,Object> parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 工作地点属于关系
	 * @param 办公地点ID
	 * @return 级联办公地点名
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public boolean getOfficeLocationBelong(long childId,long parentId) throws Exception{
		
		try {
			
			boolean result = false;
			
			if(childId==parentId){
				
				result = true;
			}else{
				
				String sql = "select parentid from office_location where id = "+childId;
				
				DBRow officeRow = dbUtilAutoTran.selectSingle(sql);
				
				if(officeRow.get("parentid", -1L)!=0){
					
					result = getOfficeLocationBelong(officeRow.get("parentid", -1L),parentId);
				}
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getOfficeLocationBelong(long childId,long parentId) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取级联办公地点名
	 * @param 办公地点ID
	 * @return 级联办公地点名
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public String getOfficeLocationCascadeNameById(long id) throws Exception{
		
		try {
			
			String officeName = "";
			
			String sql = " select office_name,parentid from office_location where id ="+id;
			
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			
			if(result != null){
				
				officeName = result.getString("office_name");
				
				if(result.get("parentid", 0) != 0){
					
					officeName = getOfficeLocationCascadeNameById(result.get("parentid", 0)) + "»" + officeName;
					
				}
			}
			
			return officeName;
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getOfficeLocationCascadeNameById(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取全部部门
	 * @param null
	 * @return 全部部门
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow[] getDepartmentList() throws Exception{
		
		try {
			
			String sql = "select adgid,name from admin_group";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getDepartmentList() error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取全部职务
	 * @param 部门ID,账号ID
	 * @return 全部部门(显示账号关联的部门,用于树列表)
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow[] getPostList(long deptId,long id) throws Exception{
		
		try {
			
			String sql = "select admin_post.id,admin_post.name,IF(has_post.post_id is null,'false','true')as checked,IF(has_post.post_id is null,'false','true')as open from (select ap.id,ap.name from admin_group_post agp LEFT JOIN admin_post ap on agp.adp_id = ap.id where agp.adg_id = "+deptId+") as admin_post LEFT OUTER JOIN (select post_id from admin_department where adid= "+ id +" and department_id = "+ deptId +" ) as has_post on admin_post.id = has_post.post_id ORDER BY admin_post.id";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getPostList(long deptId,long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取全部职务
	 * @param null
	 * @return 全部职务
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow[] getPostList() throws Exception{
		
		try {
			
			String sql = "select admin_post.id,admin_post.name from admin_post";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getPostList() error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取全部办公地点
	 * @param 账号ID,办公地点ID
	 * @return 全部办公地点(显示账号关联的办公地点,用于树列表)
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow[] getOfficeLocationList(long id,long locationId) throws Exception{
		
		try {
			
			String sql = "select id,office_name as name ,if(offadm.office_location is null,'false','true') as checked,if(offadm.office_location is null,'false','true') as open from office_location LEFT OUTER JOIN (select office_location from admin where adid = "+id+") as offadm on office_location.id = offadm.office_location where parentid= "+ locationId +" order by id,sort ";
			
			DBRow[] result = dbUtilAutoTran.selectMutliple(sql);
			
			//递归算法
			for(DBRow oneResult:result){
				
				long parentid = oneResult.get("id", -1);
				
				DBRow[] tmpResult = getOfficeLocationList(id,parentid);
				
				if(tmpResult.length!=0){
					oneResult.add("children", tmpResult);
				}else{
					continue;
				}
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getOfficeLocationList() error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取全部仓库
	 * @param null
	 * @return 全部仓库信息
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow[] getWarehouseList() throws Exception{
		
		try {
			
			String sql = "select id,title as name,storage_type from product_storage_catalog order by sort";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getWarehouseList() error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取全部区域
	 * @param 仓库ID,账号ID
	 * @return 全部区域信息(显示账号关联的区域,用于树列表)
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow[] getRegionalList(long warehouseId,long id) throws Exception{
		
		try {
			
			String sql = "select al_area.area_id as id,al_area.area_name as name,if(ac_area.area_id is null,'false','true') as checked,if(ac_area.area_id is null,'false','true') as open from (select area_id,area_name from storage_location_area where area_psid = " +warehouseId+ ") as al_area LEFT OUTER JOIN (select area_id from admin_warehouse where adid = "+id+" and warehouse_id = "+warehouseId+") as ac_area on al_area.area_id = ac_area.area_id ";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getRegionalList() error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 添加账号
	 * @param 账号信息
	 * @return 账号ID
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public long insertAccount(DBRow parameter) throws Exception{
		
		try {
			
			return dbUtilAutoTran.insertReturnId("admin", parameter);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.insertAccount(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 添加部门职务
	 * @param 账户的部门职务
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void insertDepartmentAndPost(DBRow[] parameter) throws Exception{
		
		try {
			for(DBRow oneResult:parameter){
				dbUtilAutoTran.insert("admin_department", oneResult);
			}
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.insertDepartmentAndPost(DBRow[] parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 查询单一账号部门职务
	 * @param 账号ID
	 * @return 账号部门职务
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAccountDepartmentAndPost(long id) throws Exception{
		
		try {
			
			String sql = "select ad.department_id as deptid,ad.post_id as postid,ag.name as deptname,ap.name as postname from admin_department as ad LEFT JOIN admin_group as ag on ad.department_id = ag.adgid LEFT JOIN admin_post as ap on ad.post_id = ap.id where ad.adid = "+id;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountDepartmentAndPost(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 添加仓库区域
	 * @param 账户的仓库区域
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void insertWarehouseAndArea(DBRow[] parameter) throws Exception{
		
		try {
			for(DBRow oneResult:parameter){
				dbUtilAutoTran.insert("admin_warehouse", oneResult);
			}
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.insertWarehouseAndArea(DBRow[] parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 查询单一账号仓库区域
	 * @param 账号ID
	 * @return 账号仓库区域
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAccountWarehouseAndArea(long id) throws Exception{
		
		try {
			
			String sql = "select aw.warehouse_id as wareid,aw.area_id as areaid,psc.title as warename,sla.area_name as areaname from admin_warehouse as aw LEFT JOIN product_storage_catalog as psc on aw.warehouse_id = psc.id LEFT JOIN storage_location_area as sla on aw.area_id = sla.area_id where adid = "+id;
 			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountWarehouseAndArea(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 删除账号
	 * @param 账号ID
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void deleteAccountById(long id) throws Exception{
		
		try {
			String sql = " where adid = "+id;
			
			dbUtilAutoTran.delete(sql, "admin");
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.deleteAccountById(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 删除账号关联的部门
	 * @param 账号ID
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void deleteDepartmentByAccountId(long id) throws Exception{
		
		try {
			
			String sql = " where adid = "+id;
			
			dbUtilAutoTran.delete(sql, "admin_department");
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.deleteDepartmentByAccountId(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 删除账号关联的仓库
	 * @param 账号ID
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void deleteWarehouseByAccountId(long id) throws Exception{
		
		try {
			
			String sql = " where adid = "+id;
			
			dbUtilAutoTran.delete(sql, "admin_warehouse");
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.deleteWarehouseByAccountId(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 修改帐号
	 * @param 账号信息
	 * @return 账号信息
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow updateAccount(DBRow parameter) throws Exception{
		
		try {
			long id = parameter.get("adid", 0L);
			
			String sql = " where adid = "+id;
			
			dbUtilAutoTran.update(sql, "admin", parameter);
			
			sql = "select * from admin where adid = "+id;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.updateAccount(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取令牌信息
	 * @param 令牌ID
	 * @return 令牌信息
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getTokenById(String id) throws Exception{
		
		try {
			
			String sql = "select * from sn_sninfo_token where sn = '"+id+"'";
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getTokenById(String id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 更新令牌SNINFO
	 * @param 令牌信息
	 * @return int
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public int updateSninfo(DBRow parameter) throws Exception{
		
		try {
			
			String sql = " where sn = '"+parameter.getString("sn")+"'";
			
			return dbUtilAutoTran.update(sql, "sn_sninfo_token", parameter);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.updateSninfo(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取权限列表
	 * @param null
	 * @return DBRow[]
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow[] getPermissionList() throws Exception{
		
		try {
			
//			String sql = "SELECT id,parent,text,'menu' as flag,original_id,sort FROM(SELECT	id as original_id,id AS id,IF(parentid = 0,'#',CONVERT(parentid, CHAR))AS parent,title AS text,sort FROM turboshop_control_tree ORDER BY sort, id)AS control_tree UNION ALL	SELECT id, parent, text,'page' as flag,original_id,sort	FROM ( SELECT ataid as original_id ,(( SELECT MAX(id) FROM turboshop_control_tree )+ ataid )AS id,CONVERT(page, CHAR)AS parent, description AS text,10000 as sort FROM authentication_action ORDER BY ataid )AS authentication ORDER BY sort";
			String sql = "SELECT id,parent,text,'menu' as flag,original_id,sort,control_type FROM(SELECT id as original_id,id AS id,IF(parentid = 0,'#',CONVERT(parentid, CHAR))AS parent,title AS text,sort,control_type FROM turboshop_control_tree ORDER BY sort, id)AS control_tree UNION ALL SELECT id, parent, text,'page' as flag,original_id,sort,control_type FROM ( SELECT ataid as original_id ,(( SELECT MAX(id) FROM turboshop_control_tree )+ ataid )AS id,CONVERT(page, CHAR)AS parent, description AS text,10000 as sort,(select ct.control_type from turboshop_control_tree ct where ct.id = page) AS control_type FROM authentication_action where auth=1 ORDER BY ataid )AS authentication ORDER BY sort";
			DBRow[] result = dbUtilAutoTran.selectMutliple(sql);
			
			return result;
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getPermissionList(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取账户菜单单一权限
	 * @param DBRow
	 * @return DBRow[]
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getAccountMenuPermission(DBRow parameter) throws Exception{
		
		try {
			
			String sql = "select * from turboshop_control_tree_map_extend where adid = "+parameter.get("adid", -1)+" and ctid = "+parameter.get("ctid", -1);
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountMenuPermission(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取账户页面单一权限
	 * @param DBRow
	 * @return DBRow[]
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getAccountPagePermission(DBRow parameter) throws Exception{
		
		try {
			
			String sql = "select * from turboshop_admin_group_auth_extend where adid = "+parameter.get("adid", -1)+" and ataid = "+parameter.get("ataid", -1);
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountPagePermission(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取账户所属角色菜单单一权限
	 * @param DBRow
	 * @return DBRow[]
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAccountBelongsRolesMenuPermission(DBRow parameter) throws Exception{
		
		try {
			
			String sql = "select * from turboshop_control_tree_map where adgid in (select department_id from admin_department where adid = "+parameter.get("adid", -1)+") and ctid = "+parameter.get("ctid", -1);
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountBelongsRolesMenuPermission(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取账户所属角色页面单一权限
	 * @param DBRow
	 * @return DBRow[]
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAccountBelongsRolesPagePermission(DBRow parameter) throws Exception{
		
		try {
			
			String sql = "select * from admin_group_auth where adgid in (select department_id from admin_department where adid = "+parameter.get("adid", -1)+") and ataid = "+parameter.get("ataid", -1);
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountBelongsRolesPagePermission(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取权限列表数组套数组
	 * @param long
	 * @return DBRow[]
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public DBRow[] getPermissionArrayList(long id) throws Exception{
		
		try {
			
			String sql = "select id,title as text from turboshop_control_tree where parentid="+id+" order by parentid asc,sort asc,id asc ";
			
			DBRow[] result = dbUtilAutoTran.selectMutliple(sql);
			
			//递归算法
			for(DBRow oneResult:result){
				
				long parentid = oneResult.get("id", -1);
				
				DBRow[] tmpResult = getPermissionArrayList(parentid);
				
				if(tmpResult.length!=0){
					
					oneResult.add("children", tmpResult);
					
				}else{
					
					String subSql = "select ataid id,description text from authentication_action where page = "+parentid+" order by ataid";
					
					DBRow[] subTmpResult = dbUtilAutoTran.selectMutliple(subSql);
					
					if(subTmpResult.length!=0){
						
						oneResult.add("children", subTmpResult);
					}
					
					continue;
				}
			}
			
			return result;
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getPermissionArrayList(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 权限管理 >> 删除权限
	 * @param long
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void deleteAccountPermission(long id) throws Exception{
		
		try {
			
			String sql = " where adid = "+id;
			
			dbUtilAutoTran.delete(sql, "turboshop_control_tree_map_extend");
			dbUtilAutoTran.delete(sql, "turboshop_admin_group_auth_extend");
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.deleteAccountPermission(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 权限管理 >> 添加菜单权限
	 * @param DBRow
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public void insertAccountMenuPermission(DBRow parameter) throws Exception{
		
		try {
			
			dbUtilAutoTran.insert("turboshop_control_tree_map_extend", parameter);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.insertAccountMenuPermission(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 权限管理 >> 添加页面权限
	 * @param DBRow
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void insertAccountPagePermission(DBRow parameter) throws Exception{
		
		try {
			
			dbUtilAutoTran.insert("turboshop_admin_group_auth_extend", parameter);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.insertAccountPagePermission(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> title管理 >> 查询已关联title
	 * @param adid
	 * @return DBRow[]
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAdminHasTitleList(long adid,String has) throws Exception{
		try{
			
			String sql = "SELECT * FROM title t LEFT OUTER JOIN( SELECT * FROM title_admin ta WHERE ta.title_admin_adid = "+adid+" order by ta.title_admin_sort) AS ta ON t.title_id = ta.title_admin_title_id where ta.title_admin_adid ="+adid+" and t.title_name like '%"+has+"%' ORDER BY title_admin_sort";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorAccountMgrSbb.getAdminHasTitleList(long adid) error:"+e);
		}
	}
	
	/**
	 * 账号管理 >> title管理 >> 查询未关联title
	 * @param adid
	 * @return DBRow[]
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAdminUnhasTitleList(long adid,String unHas) throws Exception{
		try{
			
			String sql = "SELECT * FROM title t LEFT OUTER JOIN( SELECT * FROM title_admin ta WHERE ta.title_admin_adid = "+adid+") AS ta ON t.title_id = ta.title_admin_title_id where ta.title_admin_adid IS NULL and t.title_name like '%"+unHas+"%'";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorAccountMgrSbb.getAdminUnhasTitleList(long adid) error:"+e);
		}
	}
	
	/**
	 * 账号管理 >> title管理 >> 查询所有title
	 * @param null
	 * @return DBRow[]
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAllTitleList(String unHas) throws Exception{
		try{
			
			String sql = "SELECT * FROM title where title_name like '%"+unHas+"%'";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorAccountMgrSbb.getAllTitleList() error:"+e);
		}
	}
	
	/**
	 * 账号管理 >> title管理 >> 删除账号Title
	 * @param 账户ID TITLEID
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void deleteAccountTitle(long adid, long titleId) throws Exception{
		try{
			//查询需要更新的列
			String sql ="select title_admin_id " +
					" from title_admin " +
					" where title_admin_adid = "+adid+
					" and title_admin_sort > (" +
						" select title_admin_sort " +
						" from title_admin " +
						" where title_admin_adid = "+adid+
						" and title_admin_title_id ="+titleId+")";
			
			DBRow[] titleAdminId = dbUtilAutoTran.selectMutliple(sql);
			
			//更新查询后的列
			for(DBRow result : titleAdminId){
				//排序大于被删除的字段的时候,结果集减一
				dbUtilAutoTran.addSubFieldVal("title_admin"," where title_admin_id = "+result.getString("title_admin_id"),"title_admin_sort", -1);
			}
			
			//删除
			String where = " where title_admin_adid = "+adid+" and title_admin_title_id = " +titleId;
			dbUtilAutoTran.delete(where,"title_admin");
		}
		catch (Exception e) {
			throw new Exception("FloorAccountMgrSbb.deleteAccountTitle(long adid, String titleId) error"+e);
		}
	}
	
	/**
	 * 账号管理 >> title管理 >> 插入账号title
	 * @param 账户ID TITLEID
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void insertAccountTitle(long adid, long titleId) throws Exception{
		try{
			
			String sql = "select MAX(title_admin_sort) as title_admin_sort from title_admin where title_admin_adid ="+adid;
			DBRow maxSort = dbUtilAutoTran.selectSingle(sql);
			
			DBRow param = new DBRow();
			param.add("title_admin_adid",adid);
			param.add("title_admin_title_id",titleId);
			param.add("title_admin_sort",maxSort.get("title_admin_sort",0)+1);
			//添加用戶與TITLE關係
			dbUtilAutoTran.insert("title_admin", param);
		}
		catch (Exception e) {
			throw new Exception("FloorAccountMgrSbb.insertAccountTitle(long adid, String titleId) error"+e);
		}
	}
	
	/**
	 * 账号管理 >> 添加图像
	 * @param 图像信息
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public long insertAccountImage(DBRow parameter) throws Exception{
		
		try {
			
			return dbUtilAutoTran.insertReturnId("admin_file", parameter);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.insertAccountImage(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 图像管理 >> 获取图像列表
	 * @param 账号ID
	 * @return 此账号下的图像列表
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAccountPhotoList(long id) throws Exception{
		
		try {
			
			String sql = "select * from admin_file where adid="+id+" order by file_flag,id";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountPhotoList(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 图像管理 >> 更新图像信息
	 * @param 图像信息
	 * @return 
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public int updateAccountPhoto(DBRow parameter) throws Exception{
		
		try {
			DBRow updateRow = new DBRow();
			updateRow.put("activity", "1");
			
			String sql = " where id="+parameter.get("id", -1L);
			
			return dbUtilAutoTran.update(sql, "admin_file", updateRow);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.updateAccountPhoto(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 图像管理 >> 清除默认选择
	 * @param 图像信息
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void updateAccountPhotoDefault(DBRow parameter) throws Exception{
		
		try {
			
			DBRow updateRow = new DBRow();
			updateRow.put("activity", "0");
			
			String sql = " where adid="+parameter.get("adid", -1L) + " and file_flag = '"+parameter.get("file_flag", "-1") + "' and activity = 1";
			
			dbUtilAutoTran.update(sql, "admin_file", updateRow);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.updateAccountPhotoDefault(DBRow parameter) error:" + e);
		}
	}
	
	public DBRow[] getAccountPhotoFromType(DBRow parameter) throws Exception{
		
		try {
			
			String sql = " select * from admin_file where adid = "+parameter.get("adid", -1L) + " and file_flag = '"+parameter.get("file_flag", "-1") + "'";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountPhotoFromType(DBRow parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 图像管理 >> 删除图像
	 * @param 图像ID
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void deleteAccountPhoto(long id) throws Exception{
		
		try {
			
			String sql = " where id="+id;
			
			dbUtilAutoTran.delete(sql,"admin_file");
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.deleteAccountPhoto(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 图像管理 >> 查询图像
	 * @param 图像ID
	 * @return 图像信息
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getAccountPhotoById(long id) throws Exception{
		
		try {
			
			String sql = "select * from admin_file where id="+id;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountPhotoById(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 查询导出TITLE
	 * @param 搜索参数
	 * @return 搜索结果
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAccountExportTitleList(Map<String,String> parameter) throws Exception{
		
		try {
			
			//搜索参数
			String searchConditions = parameter.get("searchConditions");
			String searchDeptment = parameter.get("searchDeptment");
			String searchPost = parameter.get("searchPost");
			String searchWarehouse = parameter.get("searchWarehouse");
			String searchArea = parameter.get("searchArea");
			String searchLocation = parameter.get("searchLocation");
			String searchFingerprint = parameter.get("searchFingerprint");
			String searchToken = parameter.get("searchToken");
			String searchPermission = parameter.get("searchPermission");
			String searchTitle = parameter.get("searchTitle");
			String searchUploadPhoto = parameter.get("searchUploadPhoto");
			String searchShield = parameter.get("searchShield");
			
			String sql = "select admin.adid,admin.account,title.title_id,title.title_name,title.title_admin_sort from admin INNER JOIN (select tad.title_admin_adid,title.title_id,title.title_name,tad.title_admin_sort from title_admin as tad LEFT JOIN title on tad.title_admin_title_id = title.title_id) as title ON admin.adid = title.title_admin_adid where 1=1";
			
			if(!searchConditions.equals("")){
				
				sql += " and (admin.adid = '"+searchConditions+"')";
			}
			
			if(!searchDeptment.equals("")){
				
				sql +=" and exists(select * from admin_department as adt where adt.adid = admin.adid and adt.department_id = '"+searchDeptment+"')";
			}
			if(!searchPost.equals("")){
				
				sql +=" and exists(select * from admin_department as adt where adt.adid = admin.adid and adt.post_id = '"+searchPost+"')";
			}
			if(!searchWarehouse.equals("")){
				
				sql +=" and exists(select * from admin_warehouse as awe where awe.adid = admin.adid and awe.warehouse_id = '"+searchWarehouse+"')";
			}
			if(!searchArea.equals("")){
				
				sql +=" and exists(select * from admin_warehouse as awe where awe.adid = admin.adid and awe.area_id = '"+searchArea+"')";
			}
			if(!searchLocation.equals("")){
				
				sql += " and get_office_location_belong(if(admin.office_location ='',0,admin.office_location),"+searchLocation+") = '1'";
			}
			
			if(searchFingerprint.equals("true")){
				
				sql +=" ";
			}
			if(searchToken.equals("true")){
				
				sql +=" and (admin.register_token is null or admin.register_token = '') ";
			}
			if(searchPermission.equals("true")){
				
				sql +=" ";
			}
			if(searchTitle.equals("true")){
				
				sql +=" and NOT exists(select * from title_admin where title_admin_adid = admin.adid) ";
			}
			if(searchUploadPhoto.equals("true")){
				
				sql +=" and NOT exists(select * from admin_file where admin_file.adid = admin.adid)";
			}
			if(searchShield.equals("true")){
				
				sql +=" and admin.llock = '1'";
			}
			
			sql += " order by admin.adid,title.title_admin_sort ";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountExportTitleList(Map<String,String> parameter) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取账号单一title
	 * @param 账号ID titleId
	 * @return title信息
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getAccountTitleByTitle(long adid,long titleId) throws Exception{
		
		try {
			
			String sql = "select * from title_admin where title_admin_adid = "+adid+" and title_admin_title_id ="+titleId;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountTitleByTitle(long adid,long titleId) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取需要改变sort的title
	 * @param 账号ID titleId
	 * @return title信息
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getAccountTitleUp(long adid,long titleId) throws Exception{
		
		try {
			
			String sql = "select * from title_admin where title_admin_adid = "+adid+" and title_admin_sort = ((select title_admin_sort from title_admin where title_admin_adid = "+adid+" and title_admin_title_id = "+titleId+") - 1)";
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountTitleUp(long adid,long titleId) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 获取需要改变sort的title
	 * @param 账号ID titleId
	 * @return title信息
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getAccountTitleDown(long adid,long titleId) throws Exception{
		
		try {
			
			String sql = "select * from title_admin where title_admin_adid = "+adid+" and title_admin_sort = ((select title_admin_sort from title_admin where title_admin_adid = "+adid+" and title_admin_title_id = "+titleId+") + 1)";
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountTitleDown(long adid,long titleId) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 更新title sort
	 * @param title_admin_id,sort
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public void updateAccountTitleSort(long id,DBRow param) throws Exception{
		
		try {
			
			String sql = " where title_admin_id = "+id;
			
			dbUtilAutoTran.update(sql, "title_admin", param);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.updateAccountTitleSort(long id,DBRow param) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 更新指纹CODE
	 * @param title_admin_id,sort
	 * @return null
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow updateAccountFingerCode(long id,DBRow param) throws Exception{
		
		try {
			
			String sql = " where adid = "+id;
			
			dbUtilAutoTran.update(sql, "admin", param);
			
			return this.getAccountById(id);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.updateAccountTitleSort(long id,DBRow param) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 查询仓库和区域平铺列表
	 * @param null
	 * @return 仓库和区域平铺列表
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAccountAllWareAndAreaTileList() throws Exception{
		
		try {
			
			String sql = "select psc.id as warehouse_id,psc.title as warehouse_name,sla.area_id as area_id,sla.area_name as area_name from product_storage_catalog as psc LEFT JOIN storage_location_area as sla on psc.id = sla.area_psid";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountAllWareAndAreaTileList() error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 查询仓库
	 * @param id
	 * @return 仓库
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getWarehouseById(long id) throws Exception{
		
		try {
			
			String sql = " select * from product_storage_catalog where id = "+id;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getWarehouseById(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 查询仓库
	 * @param name
	 * @return 仓库
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getWarehouseByName(String name) throws Exception{
		
		try {
			
			String sql = " select * from product_storage_catalog where title = '"+name+"'";
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getWarehouseByName(String name) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 查询仓库下区域
	 * @param 仓库ID,区域ID
	 * @return 仓库下区域
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getWarehouseAreaById(long did,long aid) throws Exception{
		
		try {
			
			String sql = " select * from storage_location_area where area_psid = "+did+" and area_id = "+aid;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getWarehouseAreaById(long did,long aid) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 查询仓库下区域
	 * @param 仓库ID,区域NAME
	 * @return 仓库下区域
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getWarehouseAreaByName(long id,String name) throws Exception{
		
		try {
			
			String sql = " select * from storage_location_area where area_psid = "+id+" and area_name = '"+name+"'";
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getWarehouseAreaByName(long id,String name) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 查询部门
	 * @param id
	 * @return 部门
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getDepartmentById(long id) throws Exception{
		
		try {
			
			String sql = " select * from admin_group where adgid = "+id;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getDepartmentById(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 查询部门
	 * @param name
	 * @return 部门
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getDepartmentByName(String name) throws Exception{
		
		try {
			
			String sql = " select * from admin_group where name = '"+name+"'";
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getDepartmentByName(String name) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 查询职位
	 * @param id
	 * @return 职位
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getPostById(long id) throws Exception{
		
		try {
			
			String sql = " select * from admin_post where id = "+id;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getPostById(long id) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 查询职位
	 * @param name
	 * @return 职位
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getPostByName(String name) throws Exception{
		
		try {
			
			String sql = " select * from admin_post where name = '"+name+"'";
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getPostByName(String name) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 查询title
	 * @param name
	 * @return title
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getTitleByName(String name) throws Exception{
		
		try {
			
			String sql = " select * from title where title_name = '"+name+"'";
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getTitleByName(String name) error:" + e);
		}
	}
	
	/**
	 * 账号管理 >> 查询账号是否与title已关联
	 * @param account name,title name
	 * @return 关联
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow existAccountTitleRelevance(String account,String title) throws Exception{
		
		try {
			
			String sql = "select * from title_admin where title_admin_adid = (select adid from admin where account = '"+account+"') and title_admin_title_id = (select title_id from title where title_name = '"+title+"')";
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.existAccountTitleRelevance(String account,String title) error:" + e);
		}
	}
	
	/**
	 * 账号登录 >> 左侧菜单
	 * @param 账号ID,根ID
	 * @return 权限树
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAccountNavigationMenu(long adid,long id) throws Exception{
		
		try {
			
			//注：如果角色权限跟节点是未确定的状态,权限表不会存储根节点,只有角色权限是确定状态时才会存储.所以查询的时候会根据子节点把父节点找出来.[只适用于二级结构]
		/*	String sql1 = "select * from (select tct.id,tct.parentid,tct.sort,tct.title,"
					+ "tct.link,tct.control_type from turboshop_control_tree as tct INNER JOIN "
					+ "(select distinct tctm.ctid from turboshop_control_tree_map as tctm"
					+ " INNER JOIN (select * from admin_department where adid = "+adid+" )"
							+ " as ad ON tctm.adgid = ad.department_id UNION ALL select"
							+ " ctid from turboshop_control_tree_map_extend where adid = "+ adid +")"
									+ " as adtct ON tct.id = adtct.ctid UNION select ttt.id,ttt.parentid,"
									+ "ttt.sort,ttt.title,ttt.link,ttt.control_type "
									+ "from turboshop_control_tree as ttt where EXISTS "
									+ "(select * from ( select tct.id,tct.parentid,tct.sort,"
									+ "tct.title,tct.link,tct.control_type from turboshop_control_tree as tct"
									+ " INNER JOIN (select distinct tctm.ctid from turboshop_control_tree_map as tctm"
									+ " INNER JOIN (select * from admin_department where adid = "+adid+" ) as ad"
											+ " ON tctm.adgid = ad.department_id UNION ALL select ctid "
											+ "from turboshop_control_tree_map_extend where adid = "+adid+") as adtct "
													+ "ON tct.id = adtct.ctid) as subtct where ttt.id = subtct.parentid )) as menu"
													+ " where menu.parentid= "+id+" and menu.control_type = 1 order by menu.sort asc";
			*/
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT	* FROM");
			sql.append(" (SELECT tct.id,tct.parentid,tct.sort,tct.title,tct.link,tct.control_type");
			sql.append("  FROM turboshop_control_tree AS tct");
			sql.append(" INNER JOIN  ( SELECT DISTINCT tctm.ctid	FROM turboshop_control_tree_map AS tctm");
			sql.append(" INNER JOIN (SELECT * FROM	admin_department WHERE	adid = ").append(adid).append(") AS ad ON tctm.adgid = ad.department_id");
			sql.append(" UNION ALL SELECT ctid FROM turboshop_control_tree_map_extend WHERE	adid = ").append(adid);
			sql.append(" ) AS adtct ON tct.id = adtct.ctid");
			sql.append(" UNION");
			sql.append(" SELECT ttt.id,ttt.parentid,ttt.sort,ttt.title,ttt.link,ttt.control_type	FROM turboshop_control_tree AS ttt");
			sql.append(" WHERE EXISTS (SELECT * FROM");
			sql.append(" ( SELECT	tct.id,tct.parentid,tct.sort,tct.title,tct.link,tct.control_type FROM	turboshop_control_tree AS tct");
			sql.append(" INNER JOIN ");
			sql.append(" 	(SELECT DISTINCT	tctm.ctid	FROM turboshop_control_tree_map AS tctm");
			sql.append(" INNER JOIN (SELECT * FROM	admin_department WHERE adid = ").append(adid).append(") AS ad ON tctm.adgid = ad.department_id");
			sql.append(" UNION ALL SELECT	ctid FROM	turboshop_control_tree_map_extend	WHERE	adid = ").append(adid);
			sql.append(" ) AS adtct ON tct.id = adtct.ctid");
			sql.append(" 	) AS subtct WHERE	ttt.id = subtct.parentid) ");
			sql.append(" union");
			sql.append(" SELECT tct.id,tct.parentid,tct.sort,tct.title,tct.link,tct.control_type");
			sql.append(" FROM turboshop_control_tree AS tct");
			sql.append(" INNER JOIN ( select DISTINCT aa.page ctid from admin_group_auth aga ");
			sql.append(" join admin_department ad on ad.department_id = aga.adgid");
			sql.append(" 	join authentication_action aa on aa.ataid = aga.ataid");
			sql.append(" 	where ad.adid = ").append(adid);
			sql.append(" 	union all");
			sql.append(" 	select DISTINCT aa.page ctid from turboshop_admin_group_auth_extend tagae ");
			sql.append(" 	join authentication_action aa on aa.ataid = tagae.ataid");
			sql.append(" 	where tagae.adid = ").append(adid);
			sql.append(" ) AS adtct ON tct.id = adtct.ctid");
			sql.append(" UNION");
			sql.append(" 	SELECT ttt.id,ttt.parentid,ttt.sort,ttt.title,ttt.link,ttt.control_type	FROM turboshop_control_tree AS ttt"); 
			sql.append("	WHERE EXISTS (SELECT * FROM");
			sql.append(" 	( SELECT	tct.id,tct.parentid,tct.sort,tct.title,tct.link,tct.control_type FROM	turboshop_control_tree AS tct");
			sql.append("	INNER JOIN ");
			sql.append(" 	(		select DISTINCT aa.page ctid from admin_group_auth aga ");
			sql.append("	join admin_department ad on ad.department_id = aga.adgid");
			sql.append("	join authentication_action aa on aa.ataid = aga.ataid");
			sql.append("	where ad.adid = ").append(adid);
			sql.append("	union all");
			sql.append("	select DISTINCT aa.page ctid from turboshop_admin_group_auth_extend tagae ");
			sql.append("	join authentication_action aa on aa.ataid = tagae.ataid");
			sql.append("	where tagae.adid = ").append(adid);
			sql.append(" ) AS adtct ON tct.id = adtct.ctid");
			sql.append(" ) AS subtct 	WHERE	ttt.id = subtct.parentid )  ");
			sql.append(" ) AS menu");
			sql.append(" WHERE	menu.parentid = ").append(id).append(" AND menu.control_type = 1 ORDER BY	menu.sort ASC");
			
			DBRow[] result = dbUtilAutoTran.selectMutliple(sql.toString());
			
			//递归算法
			for(DBRow oneResult:result){
				
				long parentid = oneResult.get("id", -1);
				
				DBRow[] tmpResult = getAccountNavigationMenu(adid,parentid);
				
				oneResult.add("subMenu", tmpResult);
				
				if(tmpResult.length==0){
					
					continue;
				}
			}
			return result;
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountNavigationMenu(long adid,long id) error:" + e);
		}
	}
	
	/**
	 * 账号登录 >> 右侧主页
	 * @param 账号ID
	 * @return 主页
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow getAccountDefaultHomePage(long adid) throws Exception{
		
		try {
			
			//注:原来跟据部门[部门就是权限]去判定主页,现在要求根据人去判定主页,商定:应该有多个主页,应该有一个默认主页
			String sql = "select * from admin_home_page where adid = "+adid+" and default_page = '1'";
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountDefaultHomePage(long adid) error:" + e);
		}
	}
	
	/**
	 * 通过ID查询菜单
	 * @param id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月20日 上午9:23:52
	 */
	public DBRow getTurboshopControlTreeById(long id) throws Exception
	{
		try
		{
			String sql = "select * from turboshop_control_tree where id="+id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorAccountMgrSbb.getTurboshopControlTreeById error:" + e);
		}
	}
	
	/**
	 * 通过ID查询action
	 * @param id
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月20日 上午9:32:30
	 */
	public DBRow getAuthenticationActionById(long id) throws Exception
	{
		try
		{
			String sql = "select * from authentication_action where ataid="+id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorAccountMgrSbb.getAuthenticationActionById error:" + e);
		}
	}
	
	/**
	 * 账号所属部门及自身的菜单权限
	 * @param adid
	 * @param ctid
	 * @param ctid_parent
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月22日 下午1:18:37
	 */
	public DBRow[] findAccountRoleAndSelfMenuRights(long adid, long ctid, long ctid_parent) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select tct.* from turboshop_control_tree tct join (");
			sql.append(" SELECT DISTINCT tctm.ctid	FROM turboshop_control_tree_map tctm");
			sql.append(" JOIN (SELECT * FROM	admin_department WHERE	adid = ").append(adid).append(") AS ad ON tctm.adgid = ad.department_id");
			sql.append(" union");
			sql.append(" SELECT distinct ctid FROM turboshop_control_tree_map_extend WHERE	adid = ").append(adid);
			sql.append(" ) tctr on tctr.ctid = tct.id");
			sql.append(" where 1=1");
			if(ctid > -1)
			{
				sql.append(" and tct.id = ").append(ctid);
			}
			if(ctid_parent > -1)
			{
				sql.append(" and tct.parentid = ").append(ctid_parent);
			}
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch (Exception e)
		{
			throw new Exception("FloorAccountMgrSbb.findAccountRoleAndSelfMenuRights error:" + e);
		}
	}
	
	/**
	 * 获得账号所属部门及自身的action权限
	 * @param adid
	 * @param ctid
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年1月22日 下午2:16:07
	 */
	public DBRow[] findAccountRoleAndSelfActionRights(long adid, long ctid) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select aa.* from authentication_action aa join (");
			sql.append(" select DISTINCT aga.ataid from admin_group_auth aga ");
			sql.append(" join admin_department ad on ad.department_id = aga.adgid");
			sql.append(" where ad.adid = ").append(adid);
			sql.append(" union");
			sql.append(" select DISTINCT tagae.ataid from turboshop_admin_group_auth_extend tagae");
			sql.append(" where tagae.adid = ").append(adid);
			sql.append(" ) agar on agar.ataid = aa.ataid");
			sql.append(" where 1=1");
			if(ctid > -1)
			{
				sql.append(" and aa.page = ").append(ctid);
			}
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch (Exception e)
		{
			throw new Exception("FloorAccountMgrSbb.findAccountRoleAndSelfMenuRights error:" + e);
		}
	}
	
	/**
	 * 安卓：获取所有帐户默认头像[图片地址]
	 * @param null
	 * @return 所有帐户的默认头像[adid,file_path][100198,/Sync10/upload/account/100000342.jpg]
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAccountPortrait() throws Exception{
		
		try {
			
			String sql = "select admin.adid,admin.account,substring(af.file_path,9) as file_path from admin as admin left join (select * from admin_file where file_flag = 'portrait' and activity = '1') as af on admin.adid = af.adid";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountPortrait() error:" + e);
		}
	}
	
	/**
	 * 安卓：获取个人帐户所有头像
	 * @param null
	 * @return 所有帐户的默认头像[file_path,activity][/Sync10/upload/account/100000342.jpg,1||0]
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getPersonalPortrait(long adid) throws Exception{
		
		try {
			
			String sql = "select id,substring(file_path,9) as file_path,activity from admin_file where file_flag = 'portrait' and adid = "+adid;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getPersonalPortrait(long adid) error:" + e);
		}
	}
	
	/**
	 * 获取部门下职务
	 * @param 部门ID
	 * @return 部门下的职务
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public DBRow[] getAccountPostByDeptId(long deptId) throws Exception{
		
		try {
			
			String sql = "select apt.id,apt.name from admin_group_post agp LEFT JOIN admin_post apt on agp.adp_id = apt.id where adg_id = "+deptId +" order by apt.id";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorAccountMgrSbb.getAccountPostByDeptId(long deptId) error:" + e);
		}
	}
	
	/**
	 * 查询所有customer_id
	 * @return
	 * @throws Exception
	 * @author:yxy
	 * @date: 2015年3月30日 下午5:49:47
	 */
	public DBRow[] getCustomerId()throws Exception {
        
		try {
			 
            String sql = "select * from customer_id";
            return dbUtilAutoTran.selectMutliple(sql);
        }
        catch(Exception e)
        {
            throw new Exception((new StringBuilder()).append("FloorAccountMgrSbb.getCustomerId() error:").append(e).toString());
        }
	}
	 
	public DBRow[] getTitle() throws Exception {
		
		try {
			
			String sql = "select * from title";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception(e);
		}
	}	 
			 
	public DBRow[] getCustomerByTitle(long id) throws Exception {
		
		try {
			
			String sql = "select * from title_product tp LEFT JOIN  customer_id ci on tp.tp_customer_id = ci.customer_key LEFT JOIN product p on tp.tp_pc_id = p.pc_id where tp.tp_title_id = "+id;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception(e);
		}
	}
	
	public DBRow[] getCategoryByTitle(long id) throws Exception {
		
		try {
			
			String sql = "select * from title_product_catalog tpc LEFT JOIN product_catalog pc on tpc.tpc_product_catalog_id = pc.id where tpc_title_id = "+id;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception(e);
		}
	}

	public DBRow[] getLineByTitle(long id) throws Exception {
	
	try {
		
		String sql = "select * from title_product_line tpl LEFT JOIN product_line_define pld on tpl.tpl_product_line_id = pld.id where tpl_title_id = "+id;
		
		return dbUtilAutoTran.selectMutliple(sql);
		
	}catch (Exception e){
		throw new Exception(e);
	}
}
	 
	/**
	 * 查询所有carrier_id
	 * @return
	 * @throws Exception
	 * @author:slj
	 * @date: 2015年4月27日 下午3:39:37
	 */
	 public DBRow[] getCarrierId()throws Exception
	    {
	        try
	        {
	            String sql = "select * from carrier_scac_mcdot";
	            return dbUtilAutoTran.selectMutliple(sql);
	        }
	        catch(Exception e)
	        {
	            throw new Exception((new StringBuilder()).append("FloorAccountMgrSbb.getCarrierId() error:").append(e).toString());
	        }
	    }
	 
	 /**
	 *	根据customer_key查询此id下的地址
	 */
	 public DBRow[] getCustomerAddressByCustomerKey(long customer_key)
		     throws Exception
	   {
	     try
	     {
	       String sql = "SELECT cus.*,psc.* from customer_id cus LEFT JOIN product_storage_catalog psc ON cus.customer_key = psc.storage_type_id and psc.storage_type = " + StorageTypeKey.CUSTOMER + " where cus.customer_key = " + customer_key;
	       
	       return dbUtilAutoTran.selectMutliple(sql);
	     }
	     catch (Exception e) {
	       throw new Exception("FloorAccountMgrSbb.getCustomerAddressByCustomerKey(long customer_key) error:" + e);
	     }
	   }
	 
	 
	 /**
		 * 功能：根据warehouse_id查询仓库下所有的账号
		 * @param warehouse_id
		 * @return DBRow[]
		 * @throws Exception
		 * @author lixf  2015年5月12日
		 */
	 public DBRow[] findAllAccountByWarehouseId(long warehouse_id) throws Exception {
			StringBuffer sqlBuffer = new StringBuffer();
			
			sqlBuffer.append("SELECT * ")
					 .append("  FROM admin_warehouse aw ")
					 .append("  LEFT JOIN admin a ON a.adid = aw.adid ")
					 .append(" WHERE aw.warehouse_id = ?");
			
			DBRow params = new DBRow();
			params.add("warehouse_id", warehouse_id);
			
			try{
				return dbUtilAutoTran.selectPreMutliple(sqlBuffer.toString(), params);
			}catch(Exception e){
				throw new Exception("FloorAccountMgrSbb.findAllAccountByWarehouseId(long warehouse_id) error:" + e);
			}
		}


	 
	/**
	 * 通过Carrier主键查询Carrier，暂时用
	 * @param id
	 * @return
	 * @throws Exception
	 * @author:yxy
	 * @date: 2015年7月17日 上午11:52:34
	 */
	 public DBRow getCarrierById(long id)throws Exception {
	      try
	      {
	        String sql = "select * from carrier_scac_mcdot where id = " + id;
	        return dbUtilAutoTran.selectSingle(sql);
	      }
	      catch (Exception e)
	      {
	        throw new Exception("FloorAccountMgrSbb.getCarrierById(long id) error:" + e);
	      }
	 }
	 
	 
	 /**
	  * 通过Customer主键查询Customer，暂时用
	  * @param id
	  * @return
	  * @throws Exception
	  * @author:yxy
	  * @date: 2015年7月17日 上午11:53:02
	  */
	 public DBRow getCustomerById(long id)throws Exception {
	      try
	      {
	        String sql = "select * from customer_id where customer_key = " + id;
	        return dbUtilAutoTran.selectSingle(sql);
	      }
	      catch (Exception e)
	      {
	        throw new Exception("FloorAccountMgrSbb.getCustomerById(long id) error:" + e);
	      }
	    }

	 
	 
}