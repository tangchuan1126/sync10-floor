package com.cwc.app.floor.api.zj;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cwc.app.beans.jqgrid.FilterBean;
import com.cwc.app.key.DamagedRepairKey;
import com.cwc.app.key.JqGridFilterKey;
import com.cwc.app.key.TransportOrderKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorDamagedRepairMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 添加返修单
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addDamagedRepairOrder(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long repair_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("damaged_repair_order"));
			
			dbrow.add("repair_id",repair_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("damaged_repair_order"),dbrow);
			
			return (repair_id);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorDamagedRepairMgrZJ addDamagedRepairOrder error:"+e);
		}
	}
	
	/**
	 * 修改返修单
	 * @param repair_id
	 * @param para
	 * @throws Exception
	 */
	public void modDamagedRepairOrder(long repair_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update(" where repair_id="+repair_id,ConfigBean.getStringValue("damaged_repair_order"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ modDamagedRepairOrder error:"+e);
		}
	}
	
	/**
	 * 删除返修单
	 * @param repair_id
	 * @throws Exception
	 */
	public void delDamagedRepairOrder(long repair_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete(" where repair_id="+repair_id, ConfigBean.getStringValue("damaged_repair_order"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ delDamagedRepairOrder error:"+e);
		}
	}
	
	/**
	 * 过滤返修单
	 * @param send_psid
	 * @param receive_psid
	 * @param status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] filterDamagedRepairOrder(long send_psid,long receive_psid,int status,PageCtrl pc)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("damaged_repair_order")+" where 1=1");
			
			if(send_psid !=0)//转运仓库
			{
				sql.append(" and send_psid="+send_psid);
			}
			
			if(receive_psid!=0)
			{
				sql.append(" and receive_psid="+receive_psid);
			}
			
			if(status!=0)
			{
				sql.append(" and repair_status="+status);
			}
			
			sql.append(" order by repair_id desc");
			
			return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ filterDamagedRepairOrder error:"+e);
		}
	}
	
	/**
	 * 根据返修ID获得返修单明细
	 * @param repair_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailDamagedRepairById(long repair_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("damaged_repair_order")+" where repair_id=?";
			
			DBRow para = new DBRow();
			para.add("repair_id",repair_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ getDetailDamagedRepairById error:"+e);
		}
	}
	
	/**
	 * 增加返修明细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addDamagedRepairDetail(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long repair_detail_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("damaged_repair_detail"));
			
			dbrow.add("repair_detail_id",repair_detail_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("damaged_repair_detail"),dbrow);
			
			return (repair_detail_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ addDamagedRepairDetail error:"+e);
		}
	}
	
	/**
	 * 修改返修明细
	 * @param repair_detail_id
	 * @param para
	 * @throws Exception
	 */
	public void modDamagedRepairDetail(long repair_detail_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update(" where repair_detail_id="+repair_detail_id,ConfigBean.getStringValue("damaged_repair_detail"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ modDamagedRepairDetail error:"+e);
		}
	}
	
	/**
	 * 删除返修明细
	 * @param repair_detail_id
	 * @throws Exception
	 */
	public void delDamagedRepairDetail(long repair_detail_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where repair_detail_id="+repair_detail_id,ConfigBean.getStringValue("damaged_repair_detail"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ delDamagedRepairDetail error:"+e);
		}
	}
	
	/**
	 * 删除全部返修明细根据返修ID
	 * @param repair_id
	 * @throws Exception
	 */
	public void delAllDamagedRepairDetailByRepairId(long repair_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where repair_id="+repair_id,ConfigBean.getStringValue("damaged_repair_detail"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ delAllDamagedRepairDetailByRepairId error:"+e);
		}
	}
	
	/**
	 * 根据返修单号获得返修明细
	 * @param repair_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDamagedRepairDetailsByRepairId(long repair_id,PageCtrl pc,String sidx,String sord,FilterBean fillterBean)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("damaged_repair_detail")+" as drd left join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = drd.repair_pc_id where drd.repair_id=?");
			
			DBRow para = new DBRow();
			para.add("repair_id",repair_id);
			
			if(fillterBean !=null)
			{
				sql.append(new JqGridFilterKey().filterSQL(fillterBean));
			}
			
			if(sidx==null||sord==null)
			{
				sql.append(" order by repair_detail_id desc");
			}
			else
			{
				sql.append(" order by "+sidx+" "+sord);
			}
			
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(), para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ getDamagedRepairDetailsByRepairId error:"+e);
		}
	}
	
	/**
	 * 根据ID获得返修单明细的详细
	 * @param repair_detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailDamagedRepairDetail(long repair_detail_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("damaged_repair_detail")+" as drd left join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = drd.repair_pc_id where repair_detail_id=?";
			
			DBRow para = new DBRow();
			para.add("repair_detail_id",repair_detail_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ getDetailDamagedRepairDetail error:"+e);
		}
	}
	
	/**
	 * 检查返修单内是否有重复商品
	 * @param repair_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailDamagedRepairDetailByPcid(long repair_id,long pc_id)
		throws Exception
	{
		try {
			String sql = "select * from "+ConfigBean.getStringValue("damaged_repair_detail")+" where repair_id=? and repair_pc_id=?";
			
			DBRow para = new DBRow();
			para.add("repair_id",repair_id);
			para.add("repair_pc_id",pc_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ getDetailDamagedRepairDetailByPcid error:"+e);
		}
	}
	
	/**
	 * 检查返修装箱
	 * @param repair_id
	 * @param ps_id
	 * @return	需装数量大于功能残损与外观残损之和
	 * @throws Exception
	 */
	public DBRow[] checkDamagedRepairStorage(long repair_id,long ps_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from damaged_repair_detail as drd " 
						+" left join product_storage as ps on ps.pc_id = drd.repair_pc_id and ps.cid=?"
						+" where drd.repair_count>(ps.damaged_count+ps.damaged_package_count) and drd.repair_id=?";
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("repair_id",repair_id);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ checkDamagedRepairStorage error:"+e);
		}
	}
	
	/**
	 * 获得返修单明细，支持根据返修仓库，接收仓库，返修单状态过滤
	 * @param send_psid
	 * @param receive_psid
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDamagedRepairDetailByPsWithStatus(long send_psid,long receive_psid,int status)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select drd.* from "+ConfigBean.getStringValue("damaged_repair_detail")+" as drd,"+ConfigBean.getStringValue("damaged_repair_order")+" as dro where drd.repair_id=dro.repair_id");
			
			if(send_psid>0)
			{
				sql.append(" and dro.send_psid="+send_psid);
			}
			
			if(receive_psid>0)
			{
				sql.append(" and dro.receive_psid="+receive_psid);
			}
			
			if(status>0)
			{
				if(status==DamagedRepairKey.ALLINTRANSIT)
				{
					sql.append(" and (dro.repair_status="+DamagedRepairKey.INTRANSIT+")");
				}
				else
				{
					sql.append(" and dro.repair_status="+status);
				}
				
			}
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ getDamagedRepairDetailByPsWithStatus error:"+e);
		}
	}
	
	/**
	 * 返修单起运修改返修明细
	 * @param repair_id
	 * @param pc_id
	 * @param para
	 * @throws Exception
	 */
	public void deliveryDamagedRepairDetail(long repair_id,long pc_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update(" where repair_id="+repair_id+" and repair_pc_id="+pc_id,ConfigBean.getStringValue("damaged_repair_detail"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ deliveryDamagedRepairDetail error:"+e);
		}
	}
	
	/**
	 * 搜索返修单
	 * @param key
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchDamagedRepairById(String key,PageCtrl pc)
		throws Exception
	{
		try 
		{
			String repairId = "";
			if (key.substring(0,1).toUpperCase().equals("R"))
			{
				String regEx = "[^0-9]";
				Pattern p = Pattern.compile(regEx);
				Matcher m = p.matcher(key);
				if (!m.replaceAll("").equals("")) 
				{
					repairId = m.replaceAll("");
				}
				
				if(repairId.equals("")|| (key.length() - repairId.length()) != 1) 
				{
					repairId = key;
				}
			}
			else
			{
				repairId = key;
			}
			
			String sql = "select * from "+ConfigBean.getStringValue("damaged_repair_order")+" where repair_id = ?";
			
			DBRow para = new DBRow();
			para.add("repair",repairId);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para,pc));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairMgrZJ searchDamagedRepairById error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
