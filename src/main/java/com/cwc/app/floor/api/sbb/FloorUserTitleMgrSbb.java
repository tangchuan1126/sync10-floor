package com.cwc.app.floor.api.sbb;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

/**
 * 新框架实施后废除此类
 * 
 * 
 * */
public class FloorUserTitleMgrSbb{
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public void updateUserTitlePriority(String id, DBRow row) throws Exception{
		try{
			dbUtilAutoTran.update(" where title_admin_id = " + id,ConfigBean.getStringValue("title_admin"), row);
		}
		catch (Exception e) {
			throw new Exception(""+e);
		}
	}
	
	public DBRow getAdminIdByTitleSort(String id,int sort) throws Exception{
		try{
			
			String sql = "select title_admin_id,title_admin_sort from title_admin where title_admin_adid = " +
					"(select title_admin_adid from title_admin where title_admin_id = "+id+") and title_admin_sort ="+sort;
			
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) {
			throw new Exception(""+e);
		}
	}
	
	public DBRow[] getAdminHasTitleList(long adid) throws Exception{
		try{
			
			String sql = "SELECT * FROM title t LEFT OUTER JOIN( SELECT * FROM title_admin ta WHERE ta.title_admin_adid = "+adid+") AS ta ON t.title_id = ta.title_admin_title_id where ta.title_admin_adid ="+adid;
			
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception(""+e);
		}
	}
	
	public DBRow[] getAdminUnhasTitleList(long adid) throws Exception{
		try{
			
			String sql = "SELECT * FROM title t LEFT OUTER JOIN( SELECT * FROM title_admin ta WHERE ta.title_admin_adid = "+adid+") AS ta ON t.title_id = ta.title_admin_title_id where ta.title_admin_adid IS NULL";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception(""+e);
		}
	}
}
