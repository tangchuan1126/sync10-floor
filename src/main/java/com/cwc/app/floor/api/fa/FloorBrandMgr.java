package com.cwc.app.floor.api.fa;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;
import com.cwc.db.PageCtrl;
import com.cwc.app.floor.api.fa.customer.FloorBrandMgrIFace;

/**
 * @author Zhangchangjiang
 *
 */
public class FloorBrandMgr implements FloorBrandMgrIFace {
	private DBUtilAutoTran_Optm dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran_Optm dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	/**
	 * 品牌管理--根据storage_type_id,查询
	 * 
	 * @param customer_key
	 * @return customer_key对应的品牌商[]
	 * @throws Exception
	 */
	public DBRow[] getAllBrandList(DBRow dbRow, PageCtrl pc) throws Exception {
		try {
			DBRow rows[];
			String sql = "select * from customer_brand where customer_key = ? order by cb_id desc";
			rows = dbUtilAutoTran.selectPreMutliple(sql, dbRow, pc);
			return rows;
		} catch (Exception e) {
			throw new Exception(
					"FloorBrandMgrZcj.getAllBrandList (DBRow dbRow, PageCtrl pc) error : "
							+ e);
		}
	}

	/**
	 * 品牌商管理-插入品牌
	 * 
	 * @param 品牌商信息
	 * @return 品牌商cb_id
	 * @throws Exception
	 */
	public long insertBrand(DBRow dbRow) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId("customer_brand", dbRow);
		} catch (Exception e) {
			throw new Exception(
					"FloorBrandMgrZcj.insertBrand(DBRow dbRow) error :" + e);
		}
	}

	/**
	 * 品牌商管理-修改brand
	 * 
	 * @param active
	 * @return 是否成功
	 * @throws Exception
	 */
	public int updateActive(DBRow dbRow) throws Exception {
		try {
			String sql = "where cb_id = " + dbRow.get("cb_id", -1L);
			return dbUtilAutoTran.update(sql, "customer_brand", dbRow);
		} catch (Exception e) {
			throw new Exception(
					"FloorBrandMgrZcj.updateActive(DBRow dbRow) error : " + e);
		}
	}

}
