package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorFileMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加文件
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addFile(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("file"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorFileMgrZJ addFile error:"+e);
		}
	}
	
	/**
	 * 删除文件
	 * @param file_id
	 * @throws Exception
	 */
	public void delFile(long file_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where file_id="+file_id,ConfigBean.getStringValue("file"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorFileMgrZJ delFile error:"+e);
		}
	}
	
	/**
	 * 根据单据号与单据类型获得所有文件
	 * @param with_id
	 * @param with_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFilesByWithAndType(long with_id,int with_type)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("file")+" where file_with_id=? and file_with_type=?";
			
			DBRow para = new DBRow();
			para.add("file_with_id",with_id);
			para.add("file_with_type",with_type);
			
			return (dbUtilAutoTran.selectPreMutliple(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorFileMgrZJ getFilesByWithAndType error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
