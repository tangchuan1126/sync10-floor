package com.cwc.app.floor.api.gql;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import us.monoid.json.JSONArray;

import com.cwc.app.key.BillStateKey;
import com.cwc.app.key.InvoiceStatusKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.key.TaskStatusKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorPrintLabelMgrGql {
	private DBUtilAutoTran dbUtilAutoTran;
	String tableName = ConfigBean.getStringValue("label_template_detail");

	public DBRow findLabelTempDetail(String entryId, String loadNo)
			throws Exception {
		try {
			String sql = "select * from " + tableName + " where 1=1 ";

			if (!StrUtil.isBlank(entryId)) {
				sql += " and entry_id='" + entryId + "'";

			}
			if (!StrUtil.isBlank(loadNo)) {
				sql += " and load_no='" + loadNo + "'";
			}
			return this.dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorPrintLabelMgrGql findLabelTempDetail" + e);
		}
	}

	public long addLabelTempDetail(DBRow row) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId(tableName, row);
		} catch (Exception e) {
			throw new Exception("FloorPrintLabelMgrGql.addLabelTempDetail():"
					+ e);
		}
	}

	public long deleteLabelTempDetail(long entryId, String loadNo)
			throws Exception {
		try {
			return this.dbUtilAutoTran.delete(" where entry_id=" + entryId
					+ " and load_no='" + loadNo + "'", tableName);
		} catch (Exception e) {
			throw new Exception("FloorPrintLabelMgrGql.deleteLabelTempDetail"
					+ e);
		}
	}

	/**
	 * 根据参数查询当天的load，批量下载pdf使用 ---zhangtao
	 * 
	 * @param account_id
	 * @param company_name
	 * @param customer_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getNumberBySearchInfo(String account_id,
			String company_name, String start_time, String end_time,
			Long ps_id, Long title_id, String customer_id, String loadno,
			int number_type) throws Exception {
		try {
			String sql = " SELECT distinct detail.number,detail.company_id"
					+ " FROM  door_or_location_occupancy_main main  ";
			if (!StrUtil.isBlank(loadno)) {
				sql += " JOIN door_or_location_occupancy_details detail ON detail.dlo_id = main.dlo_id and detail.number = "
						+ loadno;
			} else {
				sql += " JOIN door_or_location_occupancy_details detail ON detail.dlo_id = main.dlo_id ";
			}
			sql += "  JOIN product_storage_catalog  ps on main.ps_id=ps.id "
					+ "  WHERE detail.number_type in (" + number_type + ") ";
			if (ps_id > 0l) {
				sql = (new StringBuilder()).append(sql)
						.append(" AND main.ps_id = '").append(ps_id)
						.append("'").toString();
			}
			if (!StrUtil.isBlank(account_id))
				sql = (new StringBuilder()).append(sql)
						.append(" AND detail.account_id = '")
						.append(account_id).append("'").toString();
			if (!StrUtil.isBlank(company_name))
				sql = (new StringBuilder()).append(sql)
						.append(" AND main.company_name = '")
						.append(company_name).append("'").toString();
			if (!StrUtil.isBlank(start_time))
				sql = ((new StringBuilder(sql))
						.append(" and  main.gate_check_in_time>='")
						.append(start_time).append("'").toString());
			if (!StrUtil.isBlank(end_time))
				sql = ((new StringBuilder(sql))
						.append(" and main.gate_check_in_time<='")
						.append(end_time).append("'").toString());
			if (!StrUtil.isBlank(customer_id))
				sql = ((new StringBuilder(sql)).append(" and FIND_IN_SET('")
						.append(customer_id).append("',detail.customer_id)")
						.toString());
			DBRow rows[] = dbUtilAutoTran.selectMutliple(sql);
			return rows;
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql getDnNameBySearchInfo")
					.append(e).toString());
		}
	}

	public DBRow[] getNumberBySearchInfo(String account_id,
			String company_name, String start_time, String end_time,
			Long ps_id, Long title_id, String customer_id, String loadno,
			String dn, int number_type, String... customerIds) throws Exception {
		try {
			String sql = " SELECT distinct detail.number,detail.company_id"
					+ " FROM  door_or_location_occupancy_main main  ";
			if (!StrUtil.isBlank(loadno)) {
				sql += " JOIN door_or_location_occupancy_details detail ON detail.dlo_id = main.dlo_id and detail.number = "
						+ loadno;
			} else {
				sql += " JOIN door_or_location_occupancy_details detail ON detail.dlo_id = main.dlo_id ";
			}

			if (!StrUtil.isBlank(dn)) {
				sql += " JOIN printed_label label ON detail.number = CAST(label.load_no AS CHAR) AND main.dlo_id=label.entry_id ";
				sql += " and label.dn='" + dn + "'";
			}
			sql += "  JOIN product_storage_catalog  ps on main.ps_id=ps.id "
					+ "  WHERE detail.number_type in (" + number_type + ") ";
			if (ps_id > 0l) {
				sql = (new StringBuilder()).append(sql)
						.append(" AND main.ps_id = '").append(ps_id)
						.append("'").toString();
			}
			if (!StrUtil.isBlank(account_id))
				sql = (new StringBuilder()).append(sql)
						.append(" AND detail.account_id = '")
						.append(account_id).append("'").toString();
			if (!StrUtil.isBlank(company_name))
				sql = (new StringBuilder()).append(sql)
						.append(" AND main.company_name = '")
						.append(company_name).append("'").toString();
			if (!StrUtil.isBlank(start_time))
				sql = ((new StringBuilder(sql))
						.append(" and  main.gate_check_in_time>='")
						.append(start_time).append("'").toString());
			if (!StrUtil.isBlank(end_time))
				sql = ((new StringBuilder(sql))
						.append(" and main.gate_check_in_time<='")
						.append(end_time).append("'").toString());

			if (!StrUtil.isBlank(customer_id)) {
				sql = ((new StringBuilder(sql)).append(" and FIND_IN_SET('")
						.append(customer_id).append("',detail.customer_id)")
						.toString());
			} else if (customerIds != null) {
				String customerFilter = "";
				for (String customerId : customerIds) {
					String spit = "".equals(customerFilter) ? "" : " OR ";
					customerFilter += spit + " FIND_IN_SET('" + customerId
							+ "',detail.customer_id)";
				}
				if (!"".equals(customerFilter)) {
					sql += " and (" + customerFilter + ")";
				}
			}

			System.out.println(sql);
			DBRow rows[] = dbUtilAutoTran.selectMutliple(sql);
			return rows;
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql getDnNameBySearchInfo")
					.append(e).toString());
		}
	}

	/**
	 * 批量下载pdf使用---zhangtao
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	/*
	 * public DBRow[] getDnNameByParam(String account_id, String company_name,
	 * String start_time,String end_time,Long ps_id,Long title_id,String
	 * customer_id,String loadno) throws Exception { try { String sql =
	 * " SELECT DISTINCT label.id" +
	 * " FROM  door_or_location_occupancy_main main  ";
	 * if(!StrUtil.isBlank(loadno)){ sql+=
	 * " JOIN door_or_location_occupancy_details detail ON detail.dlo_id = main.dlo_id and detail.number = "
	 * +loadno; }else{ sql+=
	 * " JOIN door_or_location_occupancy_details detail ON detail.dlo_id = main.dlo_id "
	 * ; } sql+=
	 * " JOIN printed_label label ON detail.number = CAST(label.load_no AS CHAR) AND main.dlo_id=label.entry_id "
	 * + "  JOIN product_storage_catalog  ps on main.ps_id=ps.id " +
	 * "  WHERE detail.number_type in (10,11,17) "; if(ps_id>0l){ sql=(new
	 * StringBuilder
	 * ()).append(sql).append(" AND main.ps_id = '").append(ps_id).append
	 * ("'").toString(); } if(!StrUtil.isBlank(account_id)) sql = (new
	 * StringBuilder
	 * ()).append(sql).append(" AND detail.account_id = '").append(account_id
	 * ).append("'").toString(); if(!StrUtil.isBlank(company_name)) sql = (new
	 * StringBuilder
	 * ()).append(sql).append(" AND main.company_name = '").append(company_name
	 * ).append("'").toString(); if(!StrUtil.isBlank(start_time)) sql=((new
	 * StringBuilder
	 * (sql)).append(" and   main.gate_check_in_time>='").append(start_time
	 * ).append("'").toString()); if(!StrUtil.isBlank(end_time)) sql=((new
	 * StringBuilder
	 * (sql)).append(" and   main.gate_check_in_time<='").append(end_time
	 * ).append("'").toString()); if(!StrUtil.isBlank(customer_id)) sql=((new
	 * StringBuilder
	 * (sql)).append(" and FIND_IN_SET('").append(customer_id).append
	 * ("',detail.customer_id)").toString()); DBRow[]
	 * ids=dbUtilAutoTran.selectMutliple(sql);
	 * 
	 * String create_temp_table=
	 * " CREATE TEMPORARY TABLE IF NOT EXISTS table_printed_label_ids (id int) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci "
	 * ; dbUtilAutoTran.executeSQL(create_temp_table);
	 * this.insertDBrow(create_temp_table, ids);
	 * 
	 * 
	 * StringBuffer sql_result=new StringBuffer(); sql_result.append(
	 * " SELECT DISTINCT IFNULL(label.entry_id,main.dlo_id) entry_id,main.ps_id,ps.title wsname,label_temp.ShipToID account_id,label_temp.CompanyID company_name,label_temp.CustomerID customer_id,IFNUll(label.load_no,CONVERT(label_temp.LoadNo  USING utf8) COLLATE utf8_unicode_ci) load_no,label_temp.orderno orderno,label_temp.pono pono,label.file_id,IFNUll(label.dn,label_temp.ReferenceNo) dn "
	 * ); sql_result.append(" FROM  printed_label_temp label_temp");
	 * sql_result.append(
	 * " LEFT JOIN printed_label label ON CONVERT(label_temp.ReferenceNo USING utf8) COLLATE utf8_unicode_ci=label.dn "
	 * +
	 * "AND ( CONVERT(label_temp.loadno USING utf8) COLLATE utf8_unicode_ci=label.load_no   ) "
	 * +
	 * "AND (label.id in ("+(StrUtil.isBlank(convertStringArrayToString(ids))?"''"
	 * :convertStringArrayToString(ids))+") )"); sql_result.append(
	 * " left JOIN door_or_location_occupancy_main main ON ( main.dlo_id=label.entry_id)"
	 * ); sql_result.append(
	 * " JOIN door_or_location_occupancy_details detail ON detail.dlo_id = main.dlo_id AND (  CONVERT(label_temp.loadno USING utf8) COLLATE utf8_unicode_ci=detail.number )  "
	 * ); sql_result.append(
	 * " JOIN product_storage_catalog  ps ON ( main.ps_id=ps.id  )");
	 * sql_result.append(" order by  label.file_id desc"); DBRow[]
	 * row=dbUtilAutoTran.selectMutliple(sql_result.toString());
	 * 
	 * return row; } catch(Exception e) { throw new Exception((new
	 * StringBuilder(
	 * )).append("FloorPrintLabelMgrGql getDnNameByParam").append(e
	 * ).toString()); } }
	 */

	public DBRow[] getDnNameByParam(String account_id, String company_name,
			String start_time, String end_time, Long ps_id, Long title_id,
			String customer_id, String loadno) throws Exception {
		try {
			String create_temp_table = " CREATE TEMPORARY TABLE IF NOT EXISTS table_printed_label_ids (id int(6)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ";
			dbUtilAutoTran.executeSQL(create_temp_table);
			String sql = " insert into table_printed_label_ids(id) SELECT DISTINCT label.id"
					+ " FROM  door_or_location_occupancy_main main  ";
			if (!StrUtil.isBlank(loadno)) {
				sql += " JOIN door_or_location_occupancy_details detail ON detail.dlo_id = main.dlo_id and detail.number = '"
						+ loadno + "'";
			} else {
				sql += " JOIN door_or_location_occupancy_details detail ON detail.dlo_id = main.dlo_id ";
			}
			sql += " JOIN printed_label label ON detail.number = CAST(label.load_no AS CHAR) AND main.dlo_id=label.entry_id "
					+ "  JOIN product_storage_catalog  ps on main.ps_id=ps.id "
					+ "  WHERE detail.number_type in (10,11,17) ";
			if (ps_id > 0l) {
				sql = (new StringBuilder()).append(sql)
						.append(" AND main.ps_id = '").append(ps_id)
						.append("'").toString();
			}
			if (!StrUtil.isBlank(account_id))
				sql = (new StringBuilder()).append(sql)
						.append(" AND detail.account_id = '")
						.append(account_id).append("'").toString();
			if (!StrUtil.isBlank(company_name))
				sql = (new StringBuilder()).append(sql)
						.append(" AND main.company_name = '")
						.append(company_name).append("'").toString();
			if (!StrUtil.isBlank(start_time))
				sql = ((new StringBuilder(sql))
						.append(" and   main.gate_check_in_time>='")
						.append(start_time).append("'").toString());
			if (!StrUtil.isBlank(end_time))
				sql = ((new StringBuilder(sql))
						.append(" and   main.gate_check_in_time<='")
						.append(end_time).append("'").toString());
			if (!StrUtil.isBlank(customer_id))
				sql = ((new StringBuilder(sql)).append(" and FIND_IN_SET('")
						.append(customer_id).append("',detail.customer_id)")
						.toString());
			dbUtilAutoTran.executeSQL(sql);

			// this.insertDBrow("table_printed_label_ids", ids);

			StringBuffer sql_result = new StringBuffer();
			sql_result
					//.append(" SELECT DISTINCT label.entry_id entry_id,main.ps_id, ps.title as  wsname,label_temp.ShipToID account_id,label_temp.CompanyID company_name,label_temp.CustomerID customer_id,label_temp.LoadNo as  load_no,label_temp.orderno orderno,label_temp.pono pono,label.file_id,IFNUll(label.dn,label_temp.ReferenceNo) dn ");
			.append(" SELECT DISTINCT label.entry_id entry_id,main.ps_id, ps.title as  wsname,label_temp.ShipToID account_id,label_temp.CompanyID company_name,label_temp.CustomerID customer_id,label_temp.LoadNo as  load_no,label_temp.orderno orderno,label_temp.pono pono,label.file_id, ");
			sql_result.append(" CASE label.dn WHEN  NOT NULL THEN label.dn WHEN  NULL THEN label_temp.ReferenceNo END AS dn ");
			sql_result.append(" FROM  printed_label_temp label_temp");
			sql_result
					.append(" LEFT JOIN printed_label label ON CONVERT(label_temp.ReferenceNo USING utf8) COLLATE utf8_unicode_ci=label.dn "
							+ "AND ( CONVERT(label_temp.loadno USING utf8) COLLATE utf8_unicode_ci=label.load_no "
							+ "	or CONVERT(label_temp.pono USING utf8) COLLATE utf8_unicode_ci=label.load_no "
							+ "	or  CONVERT(label_temp.orderno USING utf8) COLLATE utf8_unicode_ci=label.load_no ) ");

			sql_result
					.append(" Inner JOIN  table_printed_label_ids ids on label.id=ids.id ");
			sql_result
					.append(" LEFT JOIN door_or_location_occupancy_main main ON ( main.dlo_id=label.entry_id  )");
			sql_result
					.append(" LEFT JOIN product_storage_catalog  ps ON ( main.ps_id=ps.id  )");
			sql_result.append(" order by  label.file_id desc");
			DBRow[] row = dbUtilAutoTran.selectMutliple(sql_result.toString());
			this.deleteTempTable("table_printed_label_ids");
			return row;
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql getDnNameByParam").append(e)
					.toString());
		}
	}

	public DBRow[] getCheckinAndOrderInfo(String start_time, String end_time,
			Long ps_id, String customer_id, String tempTableName)
			throws Exception {

		String sql = "SELECT label_temp.ReferenceNo AS reference_no,  DATE_FORMAT(main.gate_check_in_time,'%Y-%m-%d %H:%i') AS gate_check_in_time, DATE_FORMAT(main.check_out_time,'%Y-%m-%d %H:%i') AS check_out_time,equipment_number "
				+ "    FROM  door_or_location_occupancy_main main "
				+ " INNER JOIN entry_equipment equipment ON main.dlo_id = equipment.check_in_entry_id AND equipment.equipment_type=2 "
				+ " INNER JOIN door_or_location_occupancy_details detail ON detail.dlo_id=main.dlo_id AND detail.number_type IN (10,11,17) "
				+ "JOIN order_info_temp label_temp  "
				+ " ON ifnull(ReferenceNo,'')!='' and ( CONVERT(label_temp.loadno USING utf8) COLLATE utf8_unicode_ci=detail.number "
				+ " 	 OR  CONVERT(label_temp.pono USING utf8) COLLATE utf8_unicode_ci=detail.number "
				+ " 	 OR  CONVERT(label_temp.orderno USING utf8) COLLATE utf8_unicode_ci=detail.number) where 1=1";

		if (ps_id > 0l) {
			sql = (new StringBuilder()).append(sql)
					.append(" AND main.ps_id = '").append(ps_id).append("'")
					.toString();
		}

		if (!StrUtil.isBlank(start_time))
			sql = ((new StringBuilder(sql))
					.append(" and   main.gate_check_in_time>='")
					.append(start_time).append("'").toString());
		if (!StrUtil.isBlank(end_time))
			sql = ((new StringBuilder(sql))
					.append(" and   main.gate_check_in_time<='")
					.append(end_time).append("'").toString());
		if (!StrUtil.isBlank(customer_id))
			sql = ((new StringBuilder(sql)).append(" and FIND_IN_SET('")
					.append(customer_id).append("',detail.customer_id)")
					.toString());
		DBRow[] row = dbUtilAutoTran.selectMutliple(sql);
		return row;
	}

	public void createMissedPdfTempTable(String tableName) throws Exception {
		try {
			String sqlCreateTempTable = "CREATE TEMPORARY TABLE  IF NOT EXISTS "
					+ tableName
					+ "( ps_id INT,entry_id INT,"
					+ " wsname VARCHAR(50), "
					+ " shipto_id VARCHAR(50), "
					+ " shipto_name VARCHAR(50), "
					+ " company_name VARCHAR(50),"
					+ " customer_id VARCHAR(50),"
					+ " load_no VARCHAR(50),order_no VARCHAR(50),po_no VARCHAR(50)"
					+ " ) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ";
			dbUtilAutoTran.executeSQL(sqlCreateTempTable);

			String sql = "INSERT INTO tb_missed_pdf(entry_id,wsname,shipto_id,shipto_name,company_name,customer_id,load_no,order_no,po_no)";
			sql += " SELECT distinct entry_id,wsname,shipto_id,shipto_name,company_name,customer_id,load_no,order_no,po_no"
					+ " FROM (SELECT  entry_id ,file_id,"
					+ " label_temp.WarehouseID as wsname,"
					+ " label_temp.ShipToID as account_id,"
					+ " label_temp.mShipToID shipto_id,"
					+ " label_temp.ShipToName shipto_name,"
					+ " label_temp.CompanyID company_name,"
					+ " label_temp.CustomerID customer_id,"
					+ " label_temp.LoadNo as  load_no, OrderNo as order_no,PONo as po_no"
					+ " FROM printed_label_temp label_temp"
					+ " LEFT JOIN printed_label label ON label_temp.ReferenceNo COLLATE utf8_unicode_ci=label.dn "
					+ " AND (label_temp.loadno  COLLATE utf8_unicode_ci=label.load_no "
					+ " OR label_temp.pono COLLATE utf8_unicode_ci=label.load_no "
					+ " OR label_temp.orderno COLLATE utf8_unicode_ci=label.load_no) "
					+ " )a WHERE a.entry_id IS NULL";

			dbUtilAutoTran.executeSQL(sql);
			DBRow rows[] = dbUtilAutoTran.selectMutliple("select * from tb_missed_pdf");
			if (rows != null) {
				System.out.println("===========tb_missed_pdf  size is "+ rows.length + "=============");
			} else {
				System.out.println("===========tb_missed_pdf is null=============");
			}

		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql getDnNameByParam").append(e)
					.toString());
		}
	}

	public DBRow[] getloadNoCompanyCustomerIDGroup(String tableName)
			throws Exception {
		String sql = "SELECT DISTINCT load_no,customer_id,company_name FROM  "
				+ tableName;
		return dbUtilAutoTran.selectMutliple(sql);
	}

	private String convertStringArrayToString(DBRow[] ids) {
		String str = "";
		if (null != ids && ids.length > 0) {
			for (int i = 0; i < ids.length; i++) {
				str += ",'" + ids[i].get("id", "") + "'";
			}
		}
		if (!StrUtil.isBlank(str)) {
			str = str.substring(1);
		}
		return str;
	}

	/**
	 * 得出print_lable表id再查询出要得到的信息，批量下载pdf使用---zhangtao
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailByEntryId(long entry_id, String load_no)
			throws Exception {
		try {
			String number_type = ModuleKey.CHECK_IN_LOAD + ","
					+ ModuleKey.CHECK_IN_ORDER + "," + ModuleKey.CHECK_IN_PONO;
			String sql = "SELECT * "
					+ "FROM door_or_location_occupancy_details t "
					+ "WHERE t.number = '" + load_no + "' AND t.dlo_id ="
					+ entry_id + " AND t.number_type IN (" + number_type + ") "
					+ "LIMIT 1";
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorPrintLabelMgrGql getDetailByEntryId" + e);
		}
	}

	/**
	 * 查询所有的customer
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllCustomerId() throws Exception {
		try {
			// String
			// sql="select custom.customer_key,custom.customer_id from  customer_id custom ";
			String sql = "SELECT DISTINCT dd.customer_id,cc.customer_key FROM door_or_location_occupancy_details dd inner join customer_id cc on dd.customer_id=cc.customer_id  WHERE dd.customer_id != ''";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorPrintLabelMgrGql getAllCustomerId" + e);
		}
	}

	/**
	 * 查询所有的Carrier
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllCarrier() throws Exception {
		try {

			String sql = " select * from carrier_scac_mcdot ";

			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorPrintLabelMgrGql getAllCarrier" + e);
		}
	}

	/**
	 * @Title: getSearchPdfLoadJSON
	 * @Description: loadno 自动提示
	 * @param carrier
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchPdfLoadJSON(String loadno) throws Exception {
		return this.getSearchPdfLoadJSON(loadno, new String[] {});
		/*
		 * try { String sql = ""; if (loadno.length() > 0) sql = (new
		 * StringBuilder()) .append(
		 * " SELECT DISTINCT  detail.number AS loadno FROM  door_or_location_occupancy_details detail  WHERE detail.number_type IN(10, 11, 17)  AND detail.number like '"
		 * ) .append(loadno).append("%'").toString(); return
		 * dbUtilAutoTran.selectMutliple(sql); } catch (Exception e) { throw new
		 * Exception((new StringBuilder())
		 * .append("FloorPrintLabelMgrGql getSearchPdfLoadJSON")
		 * .append(e).toString()); }
		 */
	}

	public DBRow[] getSearchPdfDnameJSON(String dn) throws Exception {
		return this.getSearchPdfDnameJSON(dn, new String[] {});
		/*
		 * try { String sql = ""; if (dn.length() > 0) sql = (new
		 * StringBuilder()) .append(
		 * " SELECT   DISTINCT IFNULL(dn,'') AS dn FROM  printed_label WHERE  dn like '"
		 * ) .append(dn).append("%'").toString(); sql += " limit 20"; return
		 * dbUtilAutoTran.selectMutliple(sql); } catch (Exception e) { throw new
		 * Exception((new StringBuilder())
		 * .append("FloorPrintLabelMgrGql getSearchPdfLoadJSON")
		 * .append(e).toString()); }
		 */
	}

	public DBRow[] getSearchPdfLoadJSON(String loadno, String... customerIds)
			throws Exception {
		try {
			String sql = "";
			if (loadno.length() > 0) {
				sql = (new StringBuilder())
						.append(" SELECT DISTINCT  detail.number AS loadno FROM  door_or_location_occupancy_details detail  WHERE detail.number_type IN(10, 11, 17)  AND detail.number like '")
						.append(loadno).append("%'").toString();
			}

			if (customerIds != null) {
				String customerFilter = "";
				for (String customerId : customerIds) {
					String spit = "".equals(customerFilter) ? "" : " OR ";
					customerFilter += spit + " FIND_IN_SET('" + customerId
							+ "',customer_id)";
				}
				if (!"".equals(customerFilter)) {
					sql += " and (" + customerFilter + ")";
				}
			}
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql getSearchPdfLoadJSON")
					.append(e).toString());
		}
	}

	public DBRow[] getSearchPdfDnameJSON(String dn, String... customerIds)
			throws Exception {
		try {
			String sql = "";
			if (dn.length() > 0) {
				sql = (new StringBuilder())
						.append(" SELECT   DISTINCT IFNULL(dn,'') AS dn FROM  printed_label WHERE  dn like '")
						.append(dn).append("%'").toString();
			}

			if (customerIds != null) {
				String customerFilter = "";
				for (String customerId : customerIds) {
					String spit = "".equals(customerFilter) ? "" : " OR ";
					customerFilter += spit + " FIND_IN_SET('" + customerId
							+ "',customer_id)";
				}
				if (!"".equals(customerFilter)) {
					sql += " and (" + customerFilter + ")";
				}
			}
			sql += " limit 20";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql getSearchPdfLoadJSON")
					.append(e).toString());
		}
	}

	/**
	 * 创建临时表
	 * 
	 * @param dbUtilAutoTran
	 * @throws Exception
	 */
	public void createTempTable(String table_name) throws Exception {
		try {

			String create_temp_table = " CREATE TEMPORARY TABLE IF NOT EXISTS "
					+ table_name
					+ " (ReferenceNo varchar(255) ,CompanyID varchar(255) ,CustomerID varchar(255) ,CarrierID varchar(255) ,ShipToID varchar(255) ,PONo varchar(255) ,LoadNo varchar(255),OrderNo varchar(255),ShipToName varchar(255),mShipToID varchar(255),WarehouseID varchar(255)) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ";
			dbUtilAutoTran.executeSQL(create_temp_table);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql createTempTabel").append(e)
					.toString());
		}
	}

	public void createTempTable2(String table_name) throws Exception {
		try {

			String create_temp_table = " CREATE TEMPORARY TABLE IF NOT EXISTS "
					+ table_name
					+ " (ReferenceNo varchar(255) ,CompanyID varchar(255) ,CustomerID varchar(255) ,CarrierID varchar(255) ,ShipToID varchar(255) ,PONo varchar(255) ,LoadNo varchar(255),OrderNo varchar(255)) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ";
			dbUtilAutoTran.executeSQL(create_temp_table);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql createTempTabel").append(e)
					.toString());
		}
	}

	/**
	 * 删除临时表
	 * 
	 * @param dbUtilAutoTran
	 */
	public void deleteTempTable(String table_name) throws Exception {
		try {
			String delete_temp_table = "  TRUNCATE TABLE " + table_name;
			dbUtilAutoTran.executeSQL(delete_temp_table);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql deleteTempTable").append(e)
					.toString());
		}
	}

	/**
	 * 向表中插入数据
	 * 
	 * @param dbUtilAutoTran
	 */
	public Boolean insertDBrow(String table_name, DBRow[] row) throws Exception {
		boolean success = false;
		try {
			if (row != null && row.length > 0) {
				for (int i = 0; i < row.length; i++) {
					success = dbUtilAutoTran.insert(table_name, row[i]);
				}
			}
			return success;
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql insertDBRow").append(e)
					.toString());
		}
	}

	/* 以下部分为invoice task 操作 开始 */
	/**
	 * 根据schedule_id得到schedule
	 * 
	 * @throws Exception
	 */
	public DBRow getScheduleByScheduleId(long schedule_id) throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					" SELECT GROUP_CONCAT( cast( `task_join`.task_join_execute_id AS CHAR charset utf8 )SEPARATOR ',') schedule_join_execute_id, ");
			sql.append(" GROUP_CONCAT( cast(`joinadmin`.`employe_name` AS CHAR charset utf8 )SEPARATOR ',') joinEmployName, ");
			sql.append(" part.schedule_execute_id schedule_execute_id, part.exeEmployName exeEmployName,");
			sql.append(" task.end_time AS end_time,task.start_time AS start_time,");
			sql.append(" task.is_additional_task AS is_additional_task,task.task_overview AS schedule_overview,");
			sql.append(" task.task_detail AS schedule_detail,task.is_main_task AS is_main_schedule,task.create_time AS create_time,task.task_number AS task_number,");
			sql.append(" task.task_status AS task_status,task.finish_time AS finish_time, ");
			sql.append(" task.task_id AS schedule_id,	task.assign_user_id AS assign_user_id,	assignadmin.employe_name AS assEmployName ");
			sql.append(" FROM ( SELECT GROUP_CONCAT( cast( task_sub.task_execute_id AS CHAR charset utf8 ) SEPARATOR ',' ) schedule_execute_id, GROUP_CONCAT( cast( admin.employe_name AS CHAR charset utf8 ) SEPARATOR ',' ) exeEmployName,");
			sql.append(" task .task_id schedule_id ");
			sql.append(" FROM task LEFT JOIN task_sub ON task.task_id = task_sub.task_id JOIN admin ON task_sub.task_execute_id = admin.adid GROUP BY task.task_id ) part ");
			sql.append(" LEFT JOIN task_join ON part.schedule_id = task_join.task_id JOIN task ON task_join.task_id = task.task_id JOIN admin joinadmin ON task_join.task_join_execute_id = joinadmin.adid JOIN admin assignadmin ON task.assign_user_id = assignadmin.adid ");
			sql.append(" WHERE task.is_main_task IS NOT NULL AND task.task_id="
					+ schedule_id);
			return dbUtilAutoTran.selectSingle(sql.toString());
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql getScheduleByScheduleId")
					.append(e).toString());
		}
	}

	/**
	 * 得到关联的task
	 * 
	 * @throws Exception
	 */
	public DBRow[] getRelationTaskByScheduleId(String parent_schedule_id,
			String adid) throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					" SELECT GROUP_CONCAT( cast( `task_join`.task_join_execute_id AS CHAR charset utf8 )SEPARATOR ',') schedule_join_execute_id, ");
			sql.append(" GROUP_CONCAT( cast(`joinadmin`.`employe_name` AS CHAR charset utf8 )SEPARATOR ',') joinEmployName, ");
			sql.append(" part.schedule_execute_id schedule_execute_id, part.exeEmployName exeEmployName,");
			sql.append(" task.end_time AS end_time,task.start_time AS start_time,");
			sql.append(" task.is_additional_task AS is_additional_task,task.task_overview AS schedule_overview,");
			sql.append(" task.task_detail AS schedule_detail,task.is_main_task AS is_main_schedule,task.create_time AS create_time,task.task_number AS task_number,");
			sql.append(" task.task_status AS task_status,task.finish_time AS finish_time, ");
			sql.append(" task.task_id AS schedule_id,task.assign_user_id AS assign_user_id,assignadmin.employe_name AS assEmployName   ");
			sql.append(" FROM ( SELECT GROUP_CONCAT( cast( task_sub.task_execute_id AS CHAR charset utf8 ) SEPARATOR ',' ) schedule_execute_id, GROUP_CONCAT( cast( admin.employe_name AS CHAR charset utf8 ) SEPARATOR ',' ) exeEmployName,");
			sql.append(" task.task_id schedule_id ");
			sql.append(" FROM task LEFT JOIN task_sub ON task.task_id = task_sub.task_id LEFT JOIN admin ON task_sub.task_execute_id = admin.adid WHERE task.is_main_task IS NOT NULL GROUP BY task.task_id ) part ");
			sql.append(" LEFT JOIN task_relationship ON part.schedule_id = task_relationship.task_id JOIN task ON task_relationship.task_id = task.task_id LEFT JOIN task_join ON task_relationship.task_id = task_join.task_id LEFT JOIN admin joinadmin ON task_join.task_join_execute_id = joinadmin.adid JOIN admin assignadmin ON task.assign_user_id = assignadmin.adid ");
			sql.append(" WHERE task_relationship.parent_task_id="
					+ parent_schedule_id);
			if (!StrUtil.isBlank(adid)) {
				sql.append(" AND FIND_IN_SET('" + adid
						+ "',part.schedule_execute_id)");
			}
			sql.append(" GROUP BY task.task_id ORDER BY task.task_number  ");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql getRelationTaskByScheduleId")
							.append(e).toString());
		}
	}

	/**
	 * invoice关联task task关联invoice
	 * 
	 * @param 查询的search_id
	 * @param 是否根据search_id查询invoice
	 *            是 查询 invoice 否 查询 task
	 * @throws Exception
	 */
	public DBRow[] getRelationTaskByInvoiceIdOrInvoiceByTaskId(
			String search_id, boolean is) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			if (is) {
				sql.append("select distinct bill.* from task_invoice_relationship relation LEFT JOIN  bill_order bill on relation.invoice_id=bill.bill_id where relation.task_id="
						+ search_id + "");
			} else {
				sql.append("select distinct task.* from task_invoice_relationship relation LEFT JOIN  task  on relation.task_id=task.task_id where relation.invoice_id="
						+ search_id + "");
			}
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql getRelationTaskByInvoiceIdOrInvoiceByTaskId")
							.append(e).toString());
		}
	}

	/**
	 * 根据project_id查询invocies
	 */
	public DBRow[] getRelationInvoiceByProjectId(String project_id)
			throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					" SELECT distinct bill_order.* FROM task_invoice_relationship JOIN task_relationship ON task_invoice_relationship.task_id = task_relationship.task_id ");
			sql.append(" JOIN bill_order ON task_invoice_relationship.invoice_id=bill_order.bill_id ");
			sql.append(" JOIN bill_order_item ON bill_order.bill_id=bill_order_item.bill_id ");
			sql.append(" where task_relationship.parent_task_id=" + project_id);
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql getRelationTaskByInvoiceIdOrInvoiceByTaskId")
							.append(e).toString());
		}
	}

	/**
	 * 根据bill_id查询到project信息
	 */
	public DBRow[] getRelationScheduleByBillId(String bill_id,
			boolean isProjectSearch) throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					" select DISTINCT task.assign_user_id,task.task_id schedule_id,task.start_time,task.end_time,task.task_detail schedule_detail,task.task_overview schedule_overview,task.create_time,task.is_main_task,task.is_additional_task,task.task_status,task.finish_time,task.task_number FROM task_invoice_relationship LEFT JOIN task_relationship ON task_invoice_relationship.task_id=task_relationship.task_id ");
			if (isProjectSearch) {
				sql.append(" INNER JOIN task on  task_relationship.parent_task_id=task.task_id");
			} else {
				sql.append(" INNER JOIN task on  task_relationship.task_id=task.task_id");
			}
			sql.append(" WHERE task_invoice_relationship.invoice_id=" + bill_id);
			sql.append(" ORDER BY task.create_time ");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql getRelationTaskByInvoiceIdOrInvoiceByTaskId")
							.append(e).toString());
		}
	}

	/**
	 * 根据create_adid得到用户信息
	 * 
	 * @throws Exception
	 */
	public DBRow getAdminInformationByAdid(Long adid) throws Exception {
		try {
			String sql = " select * from admin where admin.adid=" + adid;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql getAdminInformationByAdid")
					.append(e).toString());
		}
	}

	/**
	 * 查询
	 * 
	 * @param id
	 * @param tableName
	 * @param column
	 * @return
	 * @throws Exception
	 */
	public DBRow getDBRowByIdAndTableName(long id, String tableName,
			String column) throws Exception {
		try {
			String sql = "select * from "
					+ tableName
					+ " left join customer_id on bill_order.client_id=customer_id.customer_key where "
					+ column + " =" + id;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql getDBRowByIdAndTableName")
					.append(e).toString());
		}
	}

	/**
	 * 根据bill_id查询billitem
	 * 
	 * @param billId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBillItemByBillOrderId(long billId) throws Exception {
		try {
			String tableName = ConfigBean.getStringValue("bill_order_item");
			String sql = "select * from " + tableName + " where bill_id ="
					+ billId;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql getBillItemByBillOrderId")
					.append(e).toString());
		}
	}

	/**
	 * 分页查询task
	 * 
	 * @param row
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getScheduleByExecuteUserIdAndDetailAndPc(DBRow row,
			PageCtrl pc) throws Exception {
		try {
			String adid = row.getString("executeUserId");
			DBRow data[] = null;
			if (!StrUtil.isBlank(adid)) {
				StringBuffer sql = new StringBuffer(
						"SELECT part.schedule_join_execute_id AS schedule_join_execute_id , part.joinEmployName AS joinEmployName, GROUP_CONCAT(cast(task_sub.task_execute_id AS CHAR charset utf8)SEPARATOR ',')AS schedule_execute_id,GROUP_CONCAT(cast(exeadmin.employe_name AS CHAR charset utf8)SEPARATOR ',')AS exeEmployName, ");
				sql.append(" task.task_id AS schedule_id, task.end_time AS end_time, task.start_time AS start_time ,task.is_additional_task AS is_additional_task,task.task_overview AS schedule_overview,task.task_detail AS schedule_detail,task.is_main_task AS is_main_schedule,task.create_time AS create_time,task.task_number AS task_number,task.task_status AS task_status,task.finish_time AS finish_time,task.assign_user_id AS assign_user_id,assignadmin.employe_name AS assEmployName ");
				sql.append(" FROM task_sub RIGHT JOIN (SELECT GROUP_CONCAT(cast(task_join.task_join_execute_id AS CHAR charset utf8 )SEPARATOR ',' )AS schedule_join_execute_id,GROUP_CONCAT(cast(joinadmin.employe_name AS CHAR charset utf8 )SEPARATOR ',')AS joinEmployName,task.task_id FROM task LEFT JOIN task_join ON task.task_id = task_join.task_id LEFT JOIN admin joinadmin ON task_join.task_join_execute_id = joinadmin.adid WHERE task.is_main_task = 1 GROUP BY task.task_id ) part ON task_sub.task_id=part.task_id ");
				sql.append(" LEFT JOIN task ON part.task_id=task.task_id LEFT JOIN admin assignadmin ON task.assign_user_id = assignadmin.adid LEFT JOIN admin exeadmin ON task_sub.task_execute_id = exeadmin.adid ");
				sql.append(" where task_status!="
						+ TaskStatusKey.CLOSE
						+ " GROUP BY task.task_id ORDER BY task.create_time DESC ");
				data = dbUtilAutoTran.selectMutliple(sql.toString(), pc);

			}
			return data;
		} catch (Exception e) {
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql.getScheduleByExecuteUserIdAndDetailAndPc(row):")
							.append(e).toString());
		}
	}

	/**
	 * 得到INVOICE
	 * 
	 * @throws Exception
	 */
	public DBRow[] getAllBill(PageCtrl pc) throws Exception {
		try {
			String tableName = ConfigBean.getStringValue("bill_order");
			String sql = (new StringBuilder())
					.append("select * from ")
					.append(tableName)
					.append(" left join customer_id on bill_order.client_id=customer_id.customer_key where invoice_status<>3 and bill_status<>"
							+ BillStateKey.cancel
							+ " order by create_date desc").toString();
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql.getAllBill(pc):").append(e)
					.toString());
		}
	}

	/**
	 * 删除主任务和invoice关联
	 * 
	 * @throws Exception
	 */
	public int deleteScheduleAndInvoiceRelationship(DBRow data)
			throws Exception {
		try {
			String tablename = ConfigBean
					.getStringValue("task_invoice_relationship");
			String whereCond = "";
			if (data.containsKey("task_id")) {
				whereCond = "where task_id= ? and invoice_id = ?";
			} else {
				whereCond = "where invoice_id = ?";
			}
			return dbUtilAutoTran.deletePre(whereCond, data, tablename);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql.deleteScheduleAndInvoiceRelationship:")
							.append(e).toString());
		}
	}

	/**
	 * 删除主任务及关联数据
	 * 
	 * @throws Exception
	 */
	public void deleteMainSchedule(Long schedule_id) throws Exception {
		try {
			String v = (new StringBuilder())
					.append("call delete_main_schedule_or_sub_schedule(")
					.append(schedule_id).append(")").toString();
			dbUtilAutoTran.CallStringSingleResultProcedure(v);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql.deleteMainSchedule:")
					.append(e).toString());
		}
	}

	/**
	 * 根据client_id得到所有的address信息
	 * 
	 * @param client_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAddressInfoListByClientId(String client_id)
			throws Exception {
		try {
			String sql = "select psc.* from  customer_id custom left join product_storage_catalog psc on custom.customer_key = psc.storage_type_id and psc.storage_type = "
					+ StorageTypeKey.CUSTOMER
					+ "  AND psc.active = 1 "
					+ " LEFT JOIN country_province cp ON psc.pro_id = cp.pro_id  "
					+ " LEFT JOIN country_code cc ON psc.native = cc.ccid "
					+ " where  1=1 AND custom.customer_key=" + client_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql.getAddressInfoListByClientId:")
							.append(e).toString());
		}
	}

	/**
	 * 得到付款账户
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAccountInfoList(long key) throws Exception {
		try {
			String tableName = ConfigBean.getStringValue("account_payee");
			String sql = (new StringBuilder()).append("select * from ")
					.append(tableName).append(" where key_type =").append(key)
					.toString();
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql.getAccountInfoList(key):")
					.append(e).toString());
		}
	}

	/**
	 * 得到所有的国家
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllCountryCode() throws Exception {
		try {
			String sql = (new StringBuilder()).append("SELECT *  FROM ")
					.append(ConfigBean.getStringValue("country_code"))
					.append(" c,")
					.append(ConfigBean.getStringValue("storage_country"))
					.append(" sc where c.ccid=sc.cid order by c_country asc")
					.toString();
			DBRow country[] = dbUtilAutoTran.selectMutliple(sql);
			return country;
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql.getAllCountryCode()")
					.append(e).toString());
		}
	}

	/**
	 * 根据country_id 得到省份
	 * 
	 * @param ccid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageProvinceByCcid(long ccid) throws Exception {
		try {
			String sql = (new StringBuilder()).append("select * from ")
					.append(ConfigBean.getStringValue("country_province"))
					.append(" where nation_id=? order by pro_name asc")
					.toString();
			DBRow para = new DBRow();
			para.add("ccid", ccid);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql.getStorageProvinceByCcid(ccid)")
							.append(e).toString());
		}
	}

	/**
	 * 得到所有的人与部门
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllUserInfoAndDept() throws Exception {
		try {
			return dbUtilAutoTran
					.selectMutliple(" select  admin.adid,admin.employe_name , admin_group.adgid , admin_group.name from admin , admin_group where admin.adgid = admin_group.adgid ");
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql.getAllUserInfoAndDept()")
					.append(e).toString());
		}
	}

	/**
	 * 排序
	 * 
	 * @param floorCheckInMgrZwb
	 * @throws Exception
	 */
	public void updateTaskNumberById(String schedule_id, String task_number)
			throws Exception {
		try {
			DBRow row = null;
			if (!StrUtil.isBlank(schedule_id) && !StrUtil.isBlank(task_number)) {
				row = new DBRow();
				row.add("task_number", task_number);
				dbUtilAutoTran.update(" where task_id=" + schedule_id,
						ConfigBean.getStringValue("task"), row);
			}
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql.updateTaskNumberById()")
					.append(e).toString());
		}
	}

	/**
	 * 修改schedule
	 * 
	 * @param id
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow updateSchedule(long id, DBRow row) throws Exception {
		DBRow resultRow = new DBRow();
		try {
			String deleteusers = row.getString("deleteusers");
			String addusers = row.getString("addusers");
			row.remove("deleteusers");
			row.remove("addusers");
			row.remove("notchange");
			resultRow.add("deleteusers", deleteusers);
			resultRow.add("addusers", addusers);
			if (null != deleteusers && deleteusers.length() > 0) {
				String deleteArray[] = deleteusers.split(",");
				String arr$[] = deleteArray;
				int len$ = arr$.length;
				for (int i$ = 0; i$ < len$; i$++) {
					String deleteId = arr$[i$];
					dbUtilAutoTran.delete(
							(new StringBuilder())
									.append("where task_execute_id = ")
									.append(deleteId).append(" and task_id=")
									.append(id).toString(),
							ConfigBean.getStringValue("task_sub"));
				}

			}
			if (null != addusers && addusers.length() > 0) {
				String addUserArray[] = addusers.split(",");
				String arr$[] = addUserArray;
				int len$ = arr$.length;
				for (int i$ = 0; i$ < len$; i$++) {
					String addUserId = arr$[i$];
					DBRow scheduleSubRow = new DBRow();
					scheduleSubRow.add("task_id", id);
					scheduleSubRow.add("task_execute_id", addUserId);
					dbUtilAutoTran.insertReturnId(
							ConfigBean.getStringValue("task_sub"),
							scheduleSubRow);
				}

			}
			String add_join_user_id = row.getString("add_join_user_id");
			String del_join_user_id = row.getString("del_join_user_id");
			row.remove("add_join_user_id");
			row.remove("del_join_user_id");
			if (del_join_user_id != null && del_join_user_id.length() > 0)
				dbUtilAutoTran
						.delete((new StringBuilder())
								.append("where task_join.task_join_execute_id in (")
								.append(del_join_user_id)
								.append(") and task_join.task_id = ")
								.append(id).toString(),
								ConfigBean.getStringValue("task_join"));
			if (add_join_user_id != null && add_join_user_id.length() > 0) {
				String scheduleAddUser[] = add_join_user_id.split(",");
				if (scheduleAddUser.length > 0) {
					int index = 0;
					for (int count = scheduleAddUser.length; index < count; index++) {
						DBRow insertRow = new DBRow();
						insertRow.add("task_id", id);
						insertRow.add("task_join_execute_id",
								scheduleAddUser[index]);
						dbUtilAutoTran.insertReturnId(
								ConfigBean.getStringValue("task_join"),
								insertRow);
					}

				}
			}
			resultRow.add("del_join_user_id", del_join_user_id);
			resultRow.add("add_join_user_id", add_join_user_id);

			// 2014-04-16 修改时处理与invoice的关系表 schedule_invoice_relationship
			String delsubinvoice = row.getString("delsubinvoice");
			String subinvoice = row.getString("subinvoice");
			row.remove("delsubinvoice");
			row.remove("subinvoice");
			if (delsubinvoice != null && delsubinvoice.length() > 0)
				dbUtilAutoTran
						.delete((new StringBuilder())
								.append("where task_invoice_relationship.invoice_id in (")
								.append(delsubinvoice)
								.append(") and task_invoice_relationship.task_id = ")
								.append(id).toString(), ConfigBean
								.getStringValue("task_invoice_relationship"));
			if (subinvoice != null && subinvoice.length() > 0) {
				String invoiceAddUser[] = subinvoice.split(",");
				if (invoiceAddUser.length > 0) {
					int index = 0;
					for (int count = invoiceAddUser.length; index < count; index++) {
						DBRow insertRow = new DBRow();
						insertRow.add("task_id", id);
						insertRow.add("invoice_id", invoiceAddUser[index]);
						dbUtilAutoTran.insertReturnId(ConfigBean
								.getStringValue("task_invoice_relationship"),
								insertRow);
					}
				}
			}
			resultRow.add("delsubinvoice", delsubinvoice);
			resultRow.add("subinvoice", subinvoice);

			// 2014-05-18 修改时处理与file的关系表 schedule_file_relationship
			String delsubfile = row.getString("delsubfile");
			String subfile = row.getString("subfile");
			row.remove("delsubfile");
			row.remove("subfile");
			if (delsubfile != null && delsubfile.length() > 0)
				dbUtilAutoTran
						.delete((new StringBuilder())
								.append("where task_file_relationship.file_id in (")
								.append(delsubfile)
								.append(") and task_file_relationship.task_id = ")
								.append(id).toString(), ConfigBean
								.getStringValue("task_file_relationship"));
			if (subfile != null && subfile.length() > 0) {
				JSONArray fileAdd = new JSONArray(subfile);
				if (fileAdd != null && fileAdd.length() > 0) {
					int index = 0;
					for (int count = fileAdd.length(); index < count; index++) {
						DBRow insertRow = new DBRow();
						insertRow.add("task_id", id);
						insertRow.add("file_id", fileAdd.getJSONObject(index)
								.get("file_id".toUpperCase()));
						insertRow.add(
								"original_file_name",
								fileAdd.getJSONObject(index).get(
										"original_file_name".toUpperCase()));
						dbUtilAutoTran.insertReturnId(ConfigBean
								.getStringValue("task_file_relationship"),
								insertRow);
					}
				}
			}
			resultRow.add("delsubfile", delsubfile);
			resultRow.add("subfile", subfile);

			dbUtilAutoTran.update((new StringBuilder())
					.append("where task_id=").append(id).toString(),
					ConfigBean.getStringValue("task"), row);
			return resultRow;
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql.updateSchedule()").append(e)
					.toString());
		}
	}

	/**
	 * 关闭invoice
	 * 
	 * @throws Exception
	 * 
	 */
	public void closeInvoice(Long bill_id) throws Exception {
		try {
			if (bill_id != null) {
				// 关闭schedule
				DBRow data = new DBRow();
				data.add("invoice_status", InvoiceStatusKey.CLOSED);
				data.add("close_time", DateUtil.NowStr());
				dbUtilAutoTran.update(" where bill_id=" + bill_id,
						ConfigBean.getStringValue("bill_order"), data);
			}
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql.closeTask()").append(e)
					.toString());
		}
	}

	/**
	 * 关闭task
	 * 
	 * @throws Exception
	 * 
	 */
	public int closeTask(Long schedule_id) throws Exception {
		try {
			// 关闭schedule
			DBRow row = new DBRow();
			row.add("finish_time", DateUtil.NowStr());
			row.add("task_status", TaskStatusKey.CLOSE);
			return dbUtilAutoTran.update(" where task_id=" + schedule_id,
					ConfigBean.getStringValue("task"), row);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql.closeTask(schedule_id)")
					.append(e).toString());
		}
	}

	/**
	 * 判断子关系是否全部关闭
	 * 
	 * @throws Exception
	 * 
	 */
	public DBRow isChildTaskAllClosed(Long task_id,
			boolean isNeedGroupByInvoiceId) throws Exception {
		try {
			// 关闭schedule
			DBRow row = new DBRow();
			row.add("finish_time", DateUtil.NowStr());
			row.add("task_status", TaskStatusKey.CLOSE);
			dbUtilAutoTran.update(" where task_id=" + task_id,
					ConfigBean.getStringValue("task"), row);
			// 如果是0 的话 就证明其子关系已经全部关闭 num是关闭的数量 schedule_id是其主子的id
			StringBuffer sql = new StringBuffer(
					" SELECT * FROM ( SELECT GROUP_CONCAT( cast(task_relationship.task_id AS CHAR charset utf8 ) SEPARATOR ',' ) AS relation_id, SUM(IF(task.task_status=3,0,1)) num, task_relationship.parent_task_id  task_id ");
			if (isNeedGroupByInvoiceId) {
				sql.append(" ,task_invoice_relationship.invoice_id invoice_id FROM  task_relationship  LEFT JOIN task ON task_relationship.task_id=task.task_id LEFT JOIN task_invoice_relationship ON  task.task_id=task_invoice_relationship.task_id WHERE task.is_main_task is NOT NULL GROUP BY task_invoice_relationship.invoice_id,task_relationship.parent_task_id ");
			} else {
				sql.append(" FROM  task_relationship  LEFT JOIN task ON task_relationship.task_id=task.task_id LEFT JOIN task_invoice_relationship ON  task.task_id=task_invoice_relationship.task_id WHERE task.is_main_task is NOT NULL GROUP BY task_relationship.parent_task_id ");
			}

			sql.append(" ) part WHERE FIND_IN_SET(" + task_id
					+ ",part.relation_id)");
			return dbUtilAutoTran.selectSingle(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql.isChildTaskAllClosed(task_id)")
							.append(e).toString());
		}
	}

	/**
	 * 主任务和子任务关联
	 * 
	 * @throws Exception
	 */
	public Long addScheduleRelationship(DBRow data) throws Exception {
		try {
			String tablename = ConfigBean.getStringValue("task_relationship");
			return dbUtilAutoTran.insertReturnId(tablename, data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql.addScheduleRelationship()")
					.append(e).toString());
		}
	}

	/**
	 * 主任务和子任务关联
	 * 
	 * @throws Exception
	 */
	public Long addScheduleAndInvoiceRelationship(DBRow data) throws Exception {
		try {
			String tablename = ConfigBean
					.getStringValue("task_invoice_relationship");
			return dbUtilAutoTran.insertReturnId(tablename, data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql.addScheduleAndInvoiceRelationship()")
							.append(e).toString());
		}
	}

	/**
	 * floor增加
	 * 
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addSchedule(DBRow row) throws Exception {
		try {
			String execute_user_ids[] = row.getString("execute_user_id").split(
					",");
			row.remove("execute_user_id");
			String schedule_join_execute_id = row
					.getString("schedule_join_execute_id");
			row.remove("schedule_join_execute_id");

			long id = dbUtilAutoTran.insertReturnId(
					ConfigBean.getStringValue("task"), row);
			if (null != execute_user_ids && execute_user_ids.length > 0) {
				int index = 0;
				for (int count = execute_user_ids.length; index < count; index++) {
					if (!StrUtil.isBlank(execute_user_ids[index])) {
						DBRow scheduleSubRow = new DBRow();
						scheduleSubRow.add("task_id", id);
						scheduleSubRow.add("task_execute_id",
								execute_user_ids[index]);
						dbUtilAutoTran.insertReturnId(
								ConfigBean.getStringValue("task_sub"),
								scheduleSubRow);
					}
				}

			}
			if (null != schedule_join_execute_id
					&& schedule_join_execute_id.length() > 0) {
				String scheduleJoinUserIdArray[] = schedule_join_execute_id
						.split(",");
				int index = 0;
				for (int count = scheduleJoinUserIdArray.length; index < count; index++) {
					DBRow scheduleJoin = new DBRow();
					scheduleJoin.add("task_id", id);
					scheduleJoin.add("task_join_execute_id",
							scheduleJoinUserIdArray[index]);
					dbUtilAutoTran.insertReturnId(
							ConfigBean.getStringValue("task_join"),
							scheduleJoin);
				}

			}
			return id;
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql.addSchedule(row):")
					.append(e).toString());
		}
	}

	/**
	 * 增加账单
	 * 
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addBill(DBRow row) throws Exception {
		try {
			String tableName = ConfigBean.getStringValue("bill_order");
			return dbUtilAutoTran.insertReturnId(tableName, row);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql.addBill(row):").append(e)
					.toString());
		}
	}

	/**
	 * 
	 * @param idList
	 * @param quantity
	 * @param billId
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void deleteItemsByNotInIdsAndUpdateBillQuantity(List idList,
			double quantity, long billId) throws Exception {
		try {
			String tableName = ConfigBean.getStringValue("bill_order_item");
			String billName = ConfigBean.getStringValue("bill_order");
			String idsSql = (new StringBuilder())
					.append("select bill_order_item.bill_item_id from bill_order_item where bill_order_item.bill_id = ")
					.append(billId).toString();
			DBRow idRows[] = dbUtilAutoTran.selectMutliple(idsSql);
			Set set = new HashSet();
			DBRow arr$[] = idRows;
			int len$ = arr$.length;
			for (int i$ = 0; i$ < len$; i$++) {
				DBRow row = arr$[i$];
				set.add(Long.valueOf(row.get("bill_item_id", 0L)));
			}

			Iterator i$ = idList.iterator();
			do {
				if (!i$.hasNext())
					break;
				long id = ((Long) i$.next()).longValue();
				if (id != 0L && set.contains(Long.valueOf(id)))
					set.remove(Long.valueOf(id));
			} while (true);
			if (set.size() > 0) {
				String idstr = "";
				for (Iterator it = set.iterator(); it.hasNext();)
					idstr = (new StringBuilder()).append(idstr).append(",")
							.append(it.next()).toString();

				dbUtilAutoTran.delete(
						(new StringBuilder()).append(" where bill_item_id in(")
								.append(idstr.substring(1)).append(") ")
								.toString(), tableName);
			}
			DBRow data = new DBRow();
			data.add("total_quantity", quantity);
			dbUtilAutoTran.update(
					(new StringBuilder()).append(" where bill_id=")
							.append(billId).toString(), billName, data);
		} catch (Exception e) {
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql.deleteItemsByNotInIds(ids):")
							.append(e).toString());
		}
	}

	/**
	 * 修改bill
	 * 
	 * @param billId
	 * @param billRow
	 * @throws Exception
	 */
	public void updateBill(long billId, DBRow billRow) throws Exception {
		try {
			String tableName = ConfigBean.getStringValue("bill_order");
			dbUtilAutoTran.update(
					(new StringBuilder()).append(" where bill_id =")
							.append(billId).toString(), tableName, billRow);
		} catch (Exception e) {
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql.updateBill(billId,billRow):")
							.append(e).toString());
		}
	}

	/**
	 * 修改bill_item
	 * 
	 * @param billItemId
	 * @param billItem
	 * @param billId
	 * @return
	 * @throws Exception
	 */
	public long saveOrUpdateBillItem(long billItemId, DBRow billItem,
			long billId) throws Exception {
		String tableName;
		try {
			tableName = ConfigBean.getStringValue("bill_order_item");
			String sqlSql = (new StringBuilder())
					.append("select count(*) as s from ").append(tableName)
					.append(" where bill_item_id=").append(billItemId)
					.toString();
			DBRow item = dbUtilAutoTran.selectSingle(sqlSql);
			if (item.get("s", 0) <= 0) {
				billItem.add("bill_id", billId);
				return addBillItem(billItem);
			}
		} catch (Exception e) {
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql.updateBill(billId,billRow):")
							.append(e).toString());
		}
		dbUtilAutoTran.update(
				(new StringBuilder()).append(" where bill_item_id=")
						.append(billItemId).toString(), tableName, billItem);
		return billItemId;
	}

	/**
	 * 增加bill_item
	 * 
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addBillItem(DBRow row) throws Exception {
		try {
			String tableName = ConfigBean.getStringValue("bill_order_item");
			return dbUtilAutoTran.insertReturnId(tableName, row);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql.addBillItem(row):")
					.append(e).toString());
		}
	}

	/**
	 * 删除bill
	 * 
	 * @param billId
	 * @throws Exception
	 */
	public void deleteBillById(long billId) throws Exception {
		try {
			String tableName = ConfigBean.getStringValue("bill_order");
			DBRow data = new DBRow();
			data.add("bill_status", BillStateKey.cancel);
			dbUtilAutoTran.update(
					(new StringBuilder()).append(" where bill_id=")
							.append(billId).toString(), tableName, data);
		} catch (Exception e) {
			throw new Exception((new StringBuilder())
					.append("FloorBillMgrZr.deleteBillById(billId):").append(e)
					.toString());
		}
	}

	/**
	 * 主任务和文件关系
	 * 
	 * @throws Exception
	 */
	public Long addScheduleAndFileRelationship(DBRow data) throws Exception {
		try {
			String tablename = ConfigBean
					.getStringValue("task_file_relationship");
			return dbUtilAutoTran.insertReturnId(tablename, data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql.addScheduleAndFileRelationship(data)")
							.append(e).toString());
		}
	}

	/**
	 * 得到sop
	 * 
	 * @throws Exception
	 */
	public DBRow[] getSopByScheduleId(String schedule_id) throws Exception {
		try {
			String tablename = ConfigBean
					.getStringValue("task_file_relationship");
			String sql = " select * from " + tablename + " where task_id=?";
			DBRow para = new DBRow();
			para.add("schedule", schedule_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql.addScheduleAndFileRelationship(data)")
							.append(e).toString());
		}
	}

	/**
	 * 增加task回复
	 * 
	 * @throws Exception
	 */
	public Long insertScheduleReplay(DBRow data) throws Exception {
		try {
			String tablename = ConfigBean.getStringValue("schedule_replay");
			return dbUtilAutoTran.insertReturnId(tablename, data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception((new StringBuilder())
					.append("FloorPrintLabelMgrGql.insertScheduleReplay(data)")
					.append(e).toString());
		}
	}

	/**
	 * 查询task的回复
	 * 
	 * @throws Exception
	 */
	public DBRow[] getReplayByScheduleId(String schedule_id) throws Exception {
		try {
			String tablename = ConfigBean.getStringValue("schedule_replay");
			String sql = "select replay.sch_replay_context , date_format(replay.sch_replay_time,'%m/%d/%y %H:%i') sch_replay_time, replay.sch_replay_type , replay.schedule_id , replay.adid , replay.employe_name , replay.sch_state  from "
					+ tablename + " as replay where schedule_id=" + schedule_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception(
					(new StringBuilder())
							.append("FloorPrintLabelMgrGql.getReplayByScheduleId(data)")
							.append(e).toString());
		}
	}

	/**
	 * 通过部门，仓库，职位查询
	 * 
	 * @param proJsId
	 * @param ps_id
	 * @param group
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllUserAndDept(long proJsId, long ps_id, long group)
			throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					"select DISTINCT ad.department_id adgid,ag.name name,a.adid adid,a.employe_name employe_name from admin a ");
			sql.append(" inner join admin_department ad on a.adid = ad.adid inner join admin_group ag on ad.department_id = ag.adgid ");
			sql.append(" INNER JOIN admin_warehouse aw on aw.adid = a.adid");
			sql.append("  where llock = 0 and LENGTH(employe_name)>0");
			if (ps_id > 0L) {
				sql.append(" AND aw.warehouse_id = ").append(ps_id);
			}
			if (group > 0L) {
				sql.append(" and ag.adgid=").append(group);
			}
			if ((proJsId >= 0L) && (proJsId != 15L)) {
				sql.append(" and ad.post_id=").append(proJsId);
			}
			if (proJsId == 15L) {
				sql.append(" and ad.post_id >= ").append(5);
			}
			sql.append(" order by ag.adgid, a.adid ");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception("FloorPrintLabelMgrGql.getAllUserAndDept():"
					+ e);
		}
	}

	/**
	 * 通过schedule_id得到父节点schedule_id
	 * 
	 * @param proJsId
	 * @param ps_id
	 * @param group
	 * @return
	 * @throws Exception
	 */
	public DBRow getParentIdByScheduleId(String schedule_id) throws Exception {
		try {
			String sql = " select max(parent_task_id) parent_task_id from task_relationship where task_id="
					+ schedule_id;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorPrintLabelMgrGql.getParentIdByScheduleId():" + e);
		}
	}

	/**
	 * 通过pro_id得到province name
	 * 
	 * @param proJsId
	 * @param ps_id
	 * @param group
	 * @return
	 * @throws Exception
	 */
	public DBRow getProvinceNameByProId(Long pro_id) throws Exception {
		try {
			String sql = " select p_code from country_province where pro_id="
					+ pro_id;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorPrintLabelMgrGql.getProvinceNameByProId():" + e);
		}
	}

	/**
	 * 通过参数查询project
	 * 
	 * @param proJsId
	 * @param ps_id
	 * @param group
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProjectBySearchParam(DBRow bean, PageCtrl pc)
			throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					"SELECT part.schedule_join_execute_id AS schedule_join_execute_id , part.joinEmployName AS joinEmployName, GROUP_CONCAT(cast(task_sub.task_execute_id AS CHAR charset utf8)SEPARATOR ',')AS schedule_execute_id,GROUP_CONCAT(cast(exeadmin.employe_name AS CHAR charset utf8)SEPARATOR ',')AS exeEmployName, ");
			sql.append(" task.task_id AS schedule_id, task.end_time AS end_time, task.start_time AS start_time ,task.is_additional_task AS is_additional_task,task.task_overview AS schedule_overview,task.task_detail AS schedule_detail,task.is_main_task AS is_main_schedule,task.create_time AS create_time,task.task_number AS task_number,task.task_status AS task_status,task.finish_time AS finish_time,task.assign_user_id AS assign_user_id,assignadmin.employe_name AS assEmployName ");
			sql.append(" FROM task_sub RIGHT JOIN (SELECT GROUP_CONCAT(cast(task_join.task_join_execute_id AS CHAR charset utf8 )SEPARATOR ',' )AS schedule_join_execute_id,GROUP_CONCAT(cast(joinadmin.employe_name AS CHAR charset utf8 )SEPARATOR ',')AS joinEmployName,task.task_id FROM task LEFT JOIN task_join ON task.task_id = task_join.task_id LEFT JOIN admin joinadmin ON task_join.task_join_execute_id = joinadmin.adid WHERE task.is_main_task = 1 GROUP BY task.task_id ) part ON task_sub.task_id=part.task_id ");
			sql.append(" LEFT JOIN task ON part.task_id=task.task_id LEFT JOIN admin assignadmin ON task.assign_user_id = assignadmin.adid LEFT JOIN admin exeadmin ON task_sub.task_execute_id = exeadmin.adid where 1=1 ");
			if (!bean.isEmpty()) {
				// 处理查询条件
				if (bean.containsKey("task_status")) {
					sql.append(" and task_status="
							+ bean.getString("task_status"));
				}
				if (bean.containsKey("start_time")) {
					sql.append(" and start_time>='"
							+ bean.getString("start_time") + "'");
				}
				if (bean.containsKey("end_time")) {
					sql.append(" and start_time<='"
							+ bean.getString("end_time") + "'");
				}
			}
			sql.append(" GROUP BY task.task_id ");
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		} catch (Exception e) {
			throw new Exception(
					"FloorPrintLabelMgrGql.getProjectBySearchParam():" + e);
		}
	}

	/**
	 * 通过project_id
	 * 
	 * @param proJsId
	 * @param ps_id
	 * @param group
	 * @return
	 * @throws Exception
	 */
	public DBRow getSingleTaskInfomationByTaskId(Long task_id) throws Exception {
		try {
			String sql = " select * from task where task_id=" + task_id;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorPrintLabelMgrGql.getSingleTaskInfomationByTaskId(task_id):"
							+ e);
		}
	}

	/* android 开始 */
	/**
	 * getproject by adid
	 * 
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] androidGetProjectByAdid(String adid) throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					" select * from ( SELECT GROUP_CONCAT(cast(task_sub.task_execute_id AS CHAR charset utf8)SEPARATOR ',')schedule_execute_id, ");
			sql.append("  GROUP_CONCAT(cast(exeadmin.employe_name AS CHAR charset utf8 )SEPARATOR ',') exeEmployName,");
			sql.append("  task.task_id schedule_id, task.task_detail description,task.task_overview subject,task.start_time,task.end_time,assignadmin.employe_name fromname FROM ");
			sql.append("  task LEFT JOIN task_sub ON task.task_id = task_sub.task_id ");
			sql.append("  LEFT JOIN admin exeadmin on task_sub.task_execute_id=exeadmin.adid ");
			sql.append("  LEFT JOIN admin assignadmin ON task.assign_user_id = assignadmin.adid ");
			sql.append("  WHERE task.is_main_task = 1  AND task.task_status <> 3 ");
			sql.append("  GROUP BY task.task_id ) project WHERE FIND_IN_SET('"
					+ adid + "',project.schedule_execute_id) ");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorPrintLabelMgrGql.androidGetMainTask(adid):" + e);
		}
	}

	/**
	 * add schedule_sub_id
	 * 
	 * @param
	 * @throws Exception
	 */
	public void androidAddScheduleSub(DBRow row) throws Exception {
		try {
			if (!row.isEmpty()) {
				String tablename = ConfigBean.getStringValue("task_sub");
				dbUtilAutoTran.insert(tablename, row);
			}
		} catch (Exception e) {
			throw new Exception(
					"FloorPrintLabelMgrGql.androidAddScheduleSub(row):" + e);
		}
	}

	/**
	 * add schedule_sub_id
	 * 
	 * @param
	 * @throws Exception
	 */
	public void androidDelScheduleSub(DBRow row) throws Exception {
		try {
			if (!row.isEmpty()) {
				String tablename = ConfigBean.getStringValue("task_sub");
				dbUtilAutoTran.deletePre(
						" where task_id=? and task_execute_id=?", row,
						tablename);
			}
		} catch (Exception e) {
			throw new Exception(
					"FloorPrintLabelMgrGql.androidAddScheduleSub(row):" + e);
		}
	}

	/**
	 * 
	 * @param
	 * @throws Exception
	 */
	public DBRow androidGetCloseInformationByMainId(String schedule_id)
			throws Exception {
		try {
			StringBuffer sql = new StringBuffer(
					" SELECT IFNULL(SUM(IF(task.task_status=3,1,0)),0) closedcount, count(1) totalcount FROM  ");
			sql.append(" task_relationship  LEFT JOIN task ON task_relationship.task_id=task.task_id WHERE task.is_main_task is NOT NULL");
			sql.append(" AND task_relationship.parent_task_id=" + schedule_id);
			return dbUtilAutoTran.selectSingle(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorPrintLabelMgrGql.androidAddScheduleSub(row):" + e);
		}
	}

	/**
	 * 
	 * @param
	 * @throws Exception
	 */
	public void androidUpdateTaskStatus(String schedule_id, int status)
			throws Exception {
		try {
			String tablename = ConfigBean.getStringValue("task");
			DBRow row = new DBRow();
			row.add("task_status", status);
			dbUtilAutoTran.update(" where task_id=" + schedule_id, tablename,
					row);
		} catch (Exception e) {
			throw new Exception(
					"FloorPrintLabelMgrGql.androidUpdateTaskStatus(schedule_id,status):"
							+ e);
		}
	}

	/**
	 * 
	 * @param
	 * @throws Exception
	 */
	public DBRow androidGetRoleByAdid(String adid) throws Exception {
		try {
			String sql = " select max(post_id) role from admin a inner join admin_department ad on a.adid = ad.adid where a.adid="
					+ adid;
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorPrintLabelMgrGql.androidGetRoleByAdid(adid):" + e);
		}
	}

	/**
	 * 
	 * @param
	 * @throws Exception
	 */
	public DBRow[] androidGetNoticeByExecuteUserIdAndAssociateTypeAndAssociateId(
			DBRow row) throws Exception {
		try {
			String executeUserId = row.getString("executeUserId");
			int associate_type = row.get("associate_type", 0);
			int associate_process = row.get("associate_process", 0);

			StringBuffer sql = new StringBuffer(
					" SELECT DISTINCT `schedule`.schedule_id,`schedule`.create_time notice_time,`schedule`.associate_main_id ,task.* ");
			sql.append(" FROM `schedule` INNER JOIN task on `schedule`.associate_id=task.task_id AND schedule.is_schedule = 1 ");
			sql.append(" JOIN schedule_sub ON `schedule`.schedule_id=schedule_sub.schedule_id  AND schedule_sub.schedule_execute_id="
					+ executeUserId);
			sql.append(" WHERE `schedule`.associate_type=" + associate_type
					+ " AND `schedule`.associate_process=" + associate_process);
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorScheduleMgrZr.getScheduleByExecuteUserIdAndDetail(row):"
							+ e);
		}
	}

	/* android 结束 */
	/* 以上部分为invoice task 操作 结束 */
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public DBRow[] getMissedPdfList(DBRow[] masterBolRows) throws Exception {
		DBRow[] rows = null;
		String sqlCreateTemplateTable = "CREATE TEMPORARY TABLE  IF NOT EXISTS tb_masterbol(LoadNo varchar(50),CompanyID varchar(50),CustomerID varchar(50),AccountID varchar(50),ShipToID varchar(50),ShipToName varchar(50),MasterBols varchar(50)) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci";
		dbUtilAutoTran.executeSQL(sqlCreateTemplateTable);

		sqlCreateTemplateTable = "CREATE TEMPORARY TABLE  IF NOT EXISTS tb_masterbol(LoadNo varchar(50),CompanyID varchar(50),CustomerID varchar(50),AccountID varchar(50),ShipToID varchar(50),ShipToName varchar(50),MasterBols varchar(50)) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci";
		this.insertDBrow("tb_masterbol", masterBolRows);

		sqlCreateTemplateTable = "CREATE TEMPORARY TABLE IF NOT EXISTS tb_missed_pdf_with_entryid( ps_id INT,entry_id INT,dlo_detail_id int,"
				+ " wsname VARCHAR(50), "
				+ " shipto_id VARCHAR(50), "
				+ " shipto_name VARCHAR(50), "
				+ " company_name VARCHAR(50),"
				+ " customer_id VARCHAR(50),"
				+ " load_no VARCHAR(50)"
				+ " ) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ";
		dbUtilAutoTran.executeSQL(sqlCreateTemplateTable);
		sqlCreateTemplateTable = "insert into tb_missed_pdf_with_entryid(ps_id,entry_id,dlo_detail_id,wsname,shipto_id,shipto_name,company_name,customer_id,load_no)"
				+ " select main.ps_id,detail.dlo_id as entry_id,detail.dlo_detail_id,ps.title as warename,shipto_id,shipto_name,a.company_name,a.customer_id,load_no "
				+ " from tb_missed_pdf a"
				+ " INNER JOIN door_or_location_occupancy_details detail ON  a.load_no =detail.number  "
				+ " INNER JOIN door_or_location_occupancy_main main ON (main.dlo_id=detail.dlo_id) "
				+ " INNER JOIN product_storage_catalog ps ON (main.ps_id=ps.id)  ";
		dbUtilAutoTran.executeSQL(sqlCreateTemplateTable);

		sqlCreateTemplateTable = "CREATE TEMPORARY TABLE  IF NOT EXISTS tb_pdf_missed_stat("
				+ " wsname varchar(50),load_no varchar(50), relative_id int"
				+ ") DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci";
		dbUtilAutoTran.executeSQL(sqlCreateTemplateTable);

		sqlCreateTemplateTable = "CREATE TEMPORARY TABLE  IF NOT EXISTS tb_pdf_missed_without_sign_stat("
				+ " wsname varchar(50), qty int"
				+ ") DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci";
		dbUtilAutoTran.executeSQL(sqlCreateTemplateTable);

		sqlCreateTemplateTable = "CREATE TEMPORARY TABLE  IF NOT EXISTS tb_pdf_missed_detail("
				+ " wsname varchar(50),entry_id int,dlo_detail_id int ,load_no varchar(50),company_id varchar(50),customer_id varchar(50),account_id varchar(50),relative_id int"
				+ ") DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci";
		dbUtilAutoTran.executeSQL(sqlCreateTemplateTable);

		String sql = "insert into tb_pdf_missed_stat(wsname,load_no,relative_id)";
		sql += " select  distinct a.wsname,a.load_no,ifnull(relative_id,0) as relative_id "
				+ " from (SELECT wsname,load_no,a.company_name AS company_id,a.customer_id,CONCAT(a.entry_id,'_',a.load_no,'_',b.MasterBols)  COLLATE utf8_unicode_ci AS `key` "
				+ " FROM tb_missed_pdf_with_entryid  a "
				+ " LEFT JOIN  tb_masterbol b on a.load_no=b.LoadNo "
				+ " and a.company_name =b.CompanyID"
				+ " and a.customer_id=b.CustomerID"
				+ " and a.shipto_id=b.ShipToID"
				+ " and a.shipto_name = b.ShipToName"
				+ ") a "
				+ " LEFT JOIN (SELECT relative_id ,relative_value FROM relative_file  WHERE  relative_type = 2 )b ON a.`key` = b.relative_value";
		dbUtilAutoTran.executeSQL(sql);

		sql = "insert into tb_pdf_missed_detail(wsname,entry_id,dlo_detail_id,load_no,company_id,customer_id,account_id)";
		sql += " select  distinct a.wsname,a.entry_id,dlo_detail_id,a.load_no,company_name as company_id,customer_id,shipto_id"
				+ " FROM tb_missed_pdf_with_entryid a ";
		dbUtilAutoTran.executeSQL(sql);

		sql = "insert into tb_pdf_missed_without_sign_stat(wsname,qty)";
		sql += "select  wsname,count(*) as qty  from tb_pdf_missed_stat where  ifnull(relative_id,0)=0 group by wsname";
		dbUtilAutoTran.executeSQL(sql);

		sql = "select a.wsname,a.qty,ifnull(b.qty_without_sign,0) as qty_without_sign"
				+ " from  (select wsname,count(*) as qty from tb_pdf_missed_stat group by wsname) a "
				+ " left join (select wsname,qty  as qty_without_sign from tb_pdf_missed_without_sign_stat )b  on a.wsname = b.wsname";
		rows = dbUtilAutoTran.selectMutliple(sql);
		if (rows != null) {
			for (int i = 0; i < rows.length; i++) {
				DBRow row = rows[i];
				sql = "select * from tb_pdf_missed_detail a where a.wsname='"
						+ row.getString("wsname") + "' order by entry_id";
				DBRow[] details = dbUtilAutoTran.selectMutliple(sql);
				row.add("detail", details);

			}
		}

		this.deleteTempTable("tb_missed_pdf_with_entryid");
		this.deleteTempTable("tb_pdf_missed_stat");

		this.deleteTempTable("tb_pdf_missed_without_sign_stat");
		this.deleteTempTable("tb_pdf_missed_detail");
		// String sql="select * from tb_missed_pdf a  ";
		return rows;
	}

	public DBRow[] getMissedPdfStat(String start_time, String end_time,Long ps_id,String number,String ...customerIds) throws Exception {
		DBRow[] rows = null;
		try {
			String sqlCreateTempTable = "CREATE TEMPORARY TABLE  IF NOT EXISTS tb_occupancy_table ( ps_id INT,entry_id int,"
					+ " wsname VARCHAR(50), "
					+ " number VARCHAR(50),"
					+ "customer_id  VARCHAR(50)," + " detail_id int )  ";
			dbUtilAutoTran.executeSQL(sqlCreateTempTable);

			sqlCreateTempTable = "CREATE TEMPORARY TABLE  IF NOT EXISTS tb_stat_table ( ps_id INT,"
					+ " wsname VARCHAR(50), "
					+ " number int,"
					+ " entry_id int" + " )  ";
			// dbUtilAutoTran.executeSQL(sqlCreateTempTable);

			String sql = "INSERT INTO tb_occupancy_table(number,ps_id,wsname,customer_id,entry_id,detail_id)"
					+ " SELECT DISTINCT number,ps_id,title,customer_id,entry_id,detail_id   FROM ( "
					+ "SELECT  DISTINCT detail.number,main.ps_id,ps.title,detail.customer_id,detail.dlo_detail_id AS detail_id "
					+ ",main.dlo_id AS entry_id,label.entry_id AS exists_entry_id,label.flag  "
					+ "    FROM  door_or_location_occupancy_main main"
					+ "  JOIN door_or_location_occupancy_details detail ON detail.dlo_id = main.dlo_id"
					+ "  JOIN product_storage_catalog  ps ON main.ps_id=ps.id "
					+ " LEFT JOIN printed_label label ON main.dlo_id = label.entry_id"
					+ " WHERE detail.number_type IN (10,11,17)";
			if (ps_id > 0l) {
				sql += "AND main.ps_id = '" + ps_id + "'";
			}
			if (!StrUtil.isBlank(start_time)){
				sql += "and  main.gate_check_in_time>='" + start_time + "'";
			}
			if (!StrUtil.isBlank(end_time)){
				sql += "and  main.gate_check_in_time<='" + end_time + "'";
			}
			
			if (!StrUtil.isBlank(number)){
				sql += "and  detail.number='" + number + "'";
			}
			if (customerIds != null ) {
				String customerFilter = "";
				for (String customerId : customerIds) {
					String spit = "".equals(customerFilter) ? "" : " OR ";
					customerFilter += spit + " FIND_IN_SET('" + customerId + "',detail.customer_id)";
				}
				if (!"".equals(customerFilter)) {
					sql += " and (" + customerFilter + ")";
				}
			}
			sql += ") a where (exists_entry_id is null OR flag=2)";
			
			System.out.println(sql);
			
			dbUtilAutoTran.executeSQL(sql);

			sql = " SELECT ps_id,wsname,COUNT(*) as qty FROM tb_occupancy_table GROUP BY  ps_id,wsname";
			rows = dbUtilAutoTran.selectMutliple(sql);
			if (rows != null) {
				for (int i = 0; i < rows.length; i++) {
					DBRow row = rows[i];
					String sqlDetail = "select distinct entry_id,number,customer_id,detail_id from tb_occupancy_table where ps_id="
							+ row.getString("ps_id");
					DBRow[] detailRows = dbUtilAutoTran
							.selectMutliple(sqlDetail);
					row.add("detail", detailRows);
				}

			}

			this.deleteTempTable("tb_occupancy_table");
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return rows;
	}

	public void createMissedPdfStatTempTable(String tableName,
			String start_time, String end_time, Long ps_id, Long title)
			throws SQLException {
		String sqlCreateTempTable = "CREATE TEMPORARY TABLE  IF NOT EXISTS "
				+ tableName + "( ps_id INT," + "entry_id INT,"
				+ " wsname VARCHAR(50), " + " number VARCHAR(50) "
				+ " ) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ";
		dbUtilAutoTran.executeSQL(sqlCreateTempTable);
		String sql = "";
	}

}
