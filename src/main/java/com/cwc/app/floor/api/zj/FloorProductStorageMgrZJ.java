package com.cwc.app.floor.api.zj;

import java.sql.SQLException;

import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorProductStorageMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 根据仓库ID获得商品库存
	 * 
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllProductStorageByPsid(String[] ps_ids, long catalog_id, long pro_line_id) throws Exception {
		try {
			String psid = "";
			// 仓库分类
			if (ps_ids == null || ps_ids.length == 0) {
				return (new DBRow[0]);
			}
			
			int i = 0;
			for (; i < ps_ids.length - 1; i++) {
				psid += ps_ids[i] + ",";
			}
			psid += ps_ids[i];
			
			String whereProductLine = "";
			if (pro_line_id > 0) {
				whereProductLine = " and pc.product_line_id = " + pro_line_id + " ";
			}
			
			String whereCatalog = "";
			if (catalog_id > 0) {
				whereCatalog = " join pc_child_list as pcl on pcl.pc_id = p.catalog_id and pcl.search_rootid = "
						+ catalog_id + " ";
				
			}
			
			StringBuffer sql = new StringBuffer("select ps.* from " + ConfigBean.getStringValue("product_storage")
					+ " as ps " + "join product as p on ps.pc_id = p.pc_id and p.orignal_pc_id=0 "
					+ "join product_catalog as pc on p.catalog_id = pc.id" + whereProductLine + whereCatalog);
			
			sql.append(" where ps.cid = ? ");
			
			DBRow para = new DBRow();
			para.add("cid", psid);
			
			sql.append(" group by ps.pc_id");
			
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(), para));
		} catch (Exception e) {
			throw new Exception("FloorProductStorageMgrZJ getAllProductStorageByPsid error:" + e);
		}
	}
	
	/**
	 * 获得全部仓库库存数
	 * 
	 * @param catalog_id
	 * @param pro_line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllProductStorage(long catalog_id, long pro_line_id) throws Exception {
		try {
			String whereProductLine = "";
			if (pro_line_id > 0) {
				whereProductLine = " and pc.product_line_id = " + pro_line_id + " ";
			}
			
			String whereCatalog = "";
			if (catalog_id > 0) {
				whereCatalog = " join pc_child_list as pcl on pcl.pc_id = p.catalog_id and pcl.search_rootid = "
						+ catalog_id + " ";
				
			}
			
			StringBuffer sql = new StringBuffer("select p.*,sum(ps.store_count) as store_count from "
					+ ConfigBean.getStringValue("product_storage")
					+ " as ps "
					+ "join product_storage_catalog psc on psc.id = ps.cid and psc.dev_flag = 1 "// 只统计允许发货的
					+ "join product as p on ps.pc_id = p.pc_id and p.orignal_pc_id=0 "
					+ "join product_catalog as pc on p.catalog_id = pc.id" + whereProductLine + whereCatalog);
			
			sql.append(" group by ps.pc_id");
			
			return (dbUtilAutoTran.selectMutliple(sql.toString()));
		} catch (Exception e) {
			throw new Exception("FloorProductStorageMgrZJ getAllProductStorage error:" + e);
		}
	}
	
	public DBRow[] getAllAliveProductStorage(long catalog_id, long pro_line_id) throws Exception {
		try {
			String whereProductLine = "";
			if (pro_line_id > 0) {
				whereProductLine = " and pc.product_line_id = " + pro_line_id + " ";
			}
			
			String whereCatalog = "";
			if (catalog_id > 0) {
				whereCatalog = " join pc_child_list as pcl on pcl.pc_id = p.catalog_id and pcl.search_rootid = "
						+ catalog_id + " ";
				
			}
			
			StringBuffer sql = new StringBuffer("select p.*,sum(ps.store_count) as store_count from "
					+ ConfigBean.getStringValue("product_storage")
					+ " as ps "
					+ "join product_storage_catalog psc on psc.id = ps.cid and psc.dev_flag = 1 "// 只统计允许发货的
					+ "join product as p on ps.pc_id = p.pc_id and p.orignal_pc_id = 0 and p.alive = 1 "
					+ "join product_catalog as pc on p.catalog_id = pc.id" + whereProductLine + whereCatalog);
			
			sql.append(" group by ps.pc_id");
			
			return (dbUtilAutoTran.selectMutliple(sql.toString()));
		} catch (Exception e) {
			throw new Exception("FloorProductStorageMgrZJ getAllProductStorage error:" + e);
		}
	}
	
	/**
	 * 获得仓库信息
	 * 
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getProductStorageCatalogByPsid(long ps_id) throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where id = ?";
			
			DBRow para = new DBRow();
			para.add("id", ps_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} catch (Exception e) {
			throw new Exception("FloorProductStorageMgrZJ getProductStorageCatalogByPsid error:" + e);
		}
	}
	
	/**
	 * 根据仓库标题获得仓库名称
	 * 
	 * @param title
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductStorageCatalogByTitle(String title) throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where title = ?";
			
			DBRow para = new DBRow();
			para.add("title", title);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} catch (Exception e) {
			throw new Exception("FloorProductStorageMgrZJ getDetailProductStorageCatalogByTitle error:" + e);
		}
	}
	
	public DBRow[] getProductStorageCatalogsByTypeShipTo(int storage_type, long storage_type_id, PageCtrl pc)
			throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where 1 = ? ";
			
			DBRow para = new DBRow();
			para.add("1", 1);
			
			if (storage_type != 0) {
				sql += " and storage_type = ? ";
				para.add("storage_type", storage_type);
			}
			
			if (storage_type_id != 0) {
				sql += " and storage_type_id = ? ";
				para.add("storage_type_id", storage_type_id);
			}
			
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		} catch (Exception e) {
			throw new Exception("FloorProductStorage getProductStorageCatalogsByTypeShipTo error:" + e);
		}
	}
	
	public DBRow[] getProductStorageCatalogsByTypeShipTo0(int storage_type, long storage_type_id, PageCtrl pc)
			throws Exception {
		try {
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where is_bill_dc=1 ";
			
			DBRow para = new DBRow();
			
			if (storage_type != 0) {
				sql += " and storage_type = ? ";
				para.add("storage_type", storage_type);
			}
			
			if (storage_type_id != 0) {
				sql += " and storage_type_id = ? ";
				para.add("storage_type_id", storage_type_id);
			}
			
			return dbUtilAutoTran.selectPreMutliple(sql, para, pc);
		} catch (Exception e) {
			throw new Exception("FloorProductStorage getProductStorageCatalogsByTypeShipTo error:" + e);
		}
	}
	
	/**
	 * 修改商品库存警戒值（为只修改单项，特意使用非DBRow）
	 * 
	 * @param pc_id
	 * @param ps_id
	 * @param store_alert_count
	 * @throws Exception
	 */
	public void modProductStorageAlert(long pc_id, long ps_id, float store_alert_count) throws Exception {
		try {
			DBRow para = new DBRow();
			para.add("store_count_alert", store_alert_count);
			
			dbUtilAutoTran.update(" where pc_id=" + pc_id + " and cid =" + ps_id,
					ConfigBean.getStringValue("product_storage"), para);
		} catch (Exception e) {
			throw new Exception("FloorProductStorageMgrZJ modProductStorageAlert error:" + e);
		}
	}
	
	public void modProductStorageCost(long pc_id, long ps_id, float storage_unit_freight, float storage_unit_price)
			throws Exception {
		try {
			DBRow para = new DBRow();
			para.add("storage_unit_freight", storage_unit_freight);
			para.add("storage_unit_price", storage_unit_price);
			dbUtilAutoTran.update(" where pc_id=" + pc_id + " and cid =" + ps_id,
					ConfigBean.getStringValue("product_storage"), para);
		} catch (Exception e) {
			throw new Exception("FloorProductStorageMgrZJ modProductStorageAlert error:" + e);
		}
	}
	
	public DBRow[] getProductStorageInCids(long pro_line_id, long catalog_id, String[] psids, int type, int union_flag,
			PageCtrl pc) throws Exception {
		try {
			
			String whereProductLine = "";
			if (pro_line_id > 0) {
				whereProductLine = " and pc.product_line_id = " + pro_line_id + " ";
			}
			
			String whereCatalog = "";
			if (catalog_id > 0) {
				whereCatalog = " join pc_child_list as pcl on pcl.pc_id = p.catalog_id and pcl.search_rootid = "
						+ catalog_id + " ";
				
			}
			
			String psid = "";
			
			// 仓库分类
			if (psids == null || psids.length == 0) {
				return (new DBRow[0]);
			}
			int i = 0;
			for (; i < psids.length - 1; i++) {
				psid += psids[i] + ",";
			}
			psid += psids[i];
			
			String whereUnionFlag = "";
			
			if (union_flag > -1) {
				whereUnionFlag = " and p.union_flag=" + union_flag;
			}
			
			StringBuffer sql = new StringBuffer("SELECT * FROM	product_storage AS ps "
					+ "join product_storage_catalog AS psc on ps.cid = psc.id and psc.id in (" + psid + ")"
					+ "join product AS p on p.pc_id = ps.pc_id "
					+ "join product_catalog as pc on p.catalog_id = pc.id " + whereProductLine + whereCatalog
					+ whereUnionFlag);
			
			if (type == ProductStatusKey.STORE_OUT) {
				sql.append(" and store_count<0");
			} else if (type == ProductStatusKey.IN_STORE) {
				sql.append(" and store_count>0");
			} else if (type == ProductStatusKey.NO_STORE) {
				sql.append(" and store_count =0");
			}
			
			sql.append(" order by pid desc");
			
			if (pc != null) {
				return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
			} else {
				return (dbUtilAutoTran.selectMutliple(sql.toString()));
			}
		} catch (Exception e) {
			throw new Exception("FloorProductStorageMgrZJ.getProductStorageInCids(row) error:" + e);
		}
	}
	
	public DBRow[] getProductStorageByName(String catalogid, long ps_id, String code, int union_flag, PageCtrl pc)
			throws Exception {
		try {
			
			// 获得跟仓库分类下所有子分类
			// Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
			// ArrayList al = tree.getAllSonNode(ps_id);
			// al.add(ps_id);
			
			String cid = ps_id + "";
			// int i=0;
			// for (; i<al.size()-1; i++)
			// {
			// cid += al.get(i)+",";
			// }
			// cid += al.get(i);
			
			String catalogWhere = "";
			if (!catalogid.equals("")) {
				catalogWhere = " and p.catalog_id in (" + catalogid + ")";
			}
			
			String unionFlagWhere = "";
			if (union_flag > -1) {
				unionFlagWhere = " and p.union_flag=" + union_flag + " ";
			}
			
			StringBuffer sql = new StringBuffer(
					"SELECT ps.*,c.*,p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,p.pc_id,p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width FROM	product_storage ps "
							+ "join product_storage_catalog c on ps.cid = c.id "
							+ "join product p on ps.pc_id = p.pc_id and  p.p_name like '%"
							+ code
							+ "%' "
							+ "join product_code pcode on p.pc_id = pcode.pc_id "
							+ "where ps.cid in ("
							+ cid
							+ ") "
							+ catalogWhere
							+ unionFlagWhere
							+ "union "
							+ "SELECT ps.*,c.*,p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,p.pc_id,p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width from product_storage ps "
							+ "join product_storage_catalog c on ps.cid = c.id "
							+ "join product p on ps.pc_id = p.pc_id "
							+ "join product_code pcode on p.pc_id = pcode.pc_id and pcode.p_code like '%"
							+ code
							+ "%' " + "where ps.cid in (" + cid + ") " + catalogWhere + unionFlagWhere);
			
			sql.append(" order by pid desc");
			
			if (pc != null) {
				return (dbUtilAutoTran.selectMutliple(sql.toString(), pc));
			} else {
				return (dbUtilAutoTran.selectMutliple(sql.toString()));
			}
		} catch (Exception e) {
			throw new Exception("FloorProductMgr.getProductStorageByName(row) error:" + e);
		}
	}
	
	/**
	 * 获得某仓库下某商品的采购数(此方法为库存页面调用，故写在Storage类里)
	 * 
	 * @param ps_id
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPurchaseCount(long ps_id, long pc_id, int[] purchase_status, boolean do_group) throws Exception {
		try {
			String wherePurchaseStatus = "";
			for (int i = 0; i < purchase_status.length; i++) {
				if (i == 0) {
					wherePurchaseStatus += " and(pu.purchase_status = " + purchase_status[i] + " ";
				} else {
					wherePurchaseStatus += " or pu.purchase_status = " + purchase_status[i] + " ";
				}
				
				if (i == purchase_status.length - 1) {
					wherePurchaseStatus += " ) ";
				}
			}
			
			String sql = "";
			if (do_group) {
				sql = "select p.p_name as p_name,pu.purchase_id as bill_id,pd.product_id as pc_id,sum(pd.purchase_count) as count,pd.eta,'采购单' as type from "
						+ ConfigBean.getStringValue("purchase_detail")
						+ " as pd "
						+ "join "
						+ ConfigBean.getStringValue("purchase")
						+ " as pu on pd.purchase_id = pu.purchase_id "
						+ "join "
						+ ConfigBean.getStringValue("product")
						+ " as p on pd.product_id = p.pc_id "
						+ "where pu.ps_id="
						+ ps_id
						+ " and pd.product_id="
						+ pc_id
						+ wherePurchaseStatus
						+ "group by pd.product_id";
			} else {
				sql = "select p.p_name as p_name,pu.purchase_id as bill_id,pd.product_id as pc_id,pd.purchase_count as count,pd.eta,'采购单' as type from "
						+ ConfigBean.getStringValue("purchase_detail")
						+ " as pd "
						+ "join "
						+ ConfigBean.getStringValue("purchase")
						+ " as pu on pd.purchase_id = pu.purchase_id "
						+ "join "
						+ ConfigBean.getStringValue("product")
						+ " as p on pd.product_id = p.pc_id "
						+ "where pu.ps_id=" + ps_id + " and pd.product_id=" + pc_id + wherePurchaseStatus;
			}
			
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorProductStoreageMgrZJ getPurchaseCount error:" + e);
		}
	}
	
	public DBRow[] getDeliveryingCount(long ps_id, long[] pcids, int[] delivery_status, int[] transport_status,
			boolean do_group) throws Exception {
		try {
			String whereDelivery = "";
			
			for (int i = 0; i < delivery_status.length; i++) {
				if (i == 0) {
					whereDelivery += " and(`do`.delivery_order_status = " + delivery_status[i] + " ";
				} else {
					whereDelivery += " or `do`.delivery_order_status = " + delivery_status[i] + " ";
				}
				
				if (i == delivery_status.length - 1) {
					whereDelivery += " ) ";
				}
			}
			
			String whereTransport = "";
			for (int i = 0; i < transport_status.length; i++) {
				if (i == 0) {
					whereTransport += " and(t.transport_status= " + transport_status[i] + " ";
				} else {
					whereTransport += " or t.transport_status = " + transport_status[i] + " ";
				}
				
				if (i == transport_status.length - 1) {
					whereTransport += " ) ";
				}
			}
			
			String whereDeliveryProduct = " and(";
			String whereTransportProduct = " and(";
			for (int i = 0; i < pcids.length; i++) {
				whereDeliveryProduct += " dod.product_id=" + pcids[i];
				whereTransportProduct += " td.transport_pc_id=" + pcids[i];
				
				if (i < pcids.length - 1) {
					whereDeliveryProduct += " or ";
					whereTransportProduct += " or ";
				}
			}
			
			whereDeliveryProduct += ") ";
			whereTransportProduct += ") ";
			
			String sql = "";
			if (do_group) {
				sql = " select bill_id,pc_id,sum(count) as count,eta,type from("
						+ " select p.p_name as p_name,dod.delivery_order_id as bill_id,dod.product_id as pc_id,delivery_count as count, `do`.arrival_eta as eta,'交货单' as type from "
						+ ConfigBean.getStringValue("delivery_order_detail")
						+ " as dod "
						+ " join "
						+ ConfigBean.getStringValue("delivery_order")
						+ " as `do` on `do`.delivery_order_id = dod.delivery_order_id "
						+ " join "
						+ ConfigBean.getStringValue("purchase")
						+ " as pu on pu.purchase_id = `do`.delivery_purchase_id "
						+ " join "
						+ ConfigBean.getStringValue("product")
						+ " as p on p.pc_id = dod.product_id "
						+ " where pu.ps_id="
						+ ps_id
						+ whereDeliveryProduct
						+ whereDelivery
						+ " union all "
						+ " select p.p_name as p_name,td.transport_id as bill_id,td.transport_pc_id as pc_id,td.transport_send_count as count,t.transport_receive_date as eta,'转运单' as type from "
						+ ConfigBean.getStringValue("transport_detail")
						+ " as td "
						+ " join "
						+ ConfigBean.getStringValue("transport")
						+ " as t on t.transport_id = td.transport_id "
						+ " join "
						+ ConfigBean.getStringValue("product")
						+ " as p on td.transport_pc_id = p.pc_id "
						+ " where t.receive_psid="
						+ ps_id
						+ whereTransportProduct
						+ whereTransport
						+ ") as delivery group by pc_id";
			} else {
				sql = "select p.p_name as p_name,dod.delivery_order_id as bill_id,dod.product_id as pc_id,delivery_count as count, `do`.arrival_eta as eta,'交货单' as type from "
						+ ConfigBean.getStringValue("delivery_order_detail")
						+ " as dod "
						+ " join "
						+ ConfigBean.getStringValue("delivery_order")
						+ " as `do` on `do`.delivery_order_id = dod.delivery_order_id "
						+ " join "
						+ ConfigBean.getStringValue("purchase")
						+ " as pu on pu.purchase_id = `do`.delivery_purchase_id "
						+ " join "
						+ ConfigBean.getStringValue("product")
						+ " as p on p.pc_id = dod.product_id "
						+ " where pu.ps_id="
						+ ps_id
						+ whereDeliveryProduct
						+ whereDelivery
						+ " union all "
						+ " select p.p_name as p_name,td.transport_id as bill_id,td.transport_pc_id as pc_id,td.transport_send_count as count,t.transport_receive_date as eta,'转运单' as type from "
						+ ConfigBean.getStringValue("transport_detail")
						+ " as td "
						+ " join "
						+ ConfigBean.getStringValue("transport")
						+ " as t on t.transport_id = td.transport_id "
						+ " join "
						+ ConfigBean.getStringValue("product")
						+ " as p on td.transport_pc_id = p.pc_id "
						+ " where t.receive_psid=" + ps_id + whereTransportProduct + whereTransport + " order by eta";
			}
			
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorProductStorageMgrZJ getDeliveryingCount error:" + e);
		}
	}
	
	/**
	 * 查询仓库
	 * 
	 * @param def_flag 是否可发乎
	 * @param return_flag 是否可退货
	 * @param sample_flag 是否可存放样品
	 * @param storage_type 仓库类型
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchProductStorageCatalog(int def_flag, int return_flag, int sample_flag, int storage_type,
			PageCtrl pc) throws Exception {
		try {
			String whereDef = "";
			if (def_flag == 1) {
				whereDef = " and def_flag = " + def_flag;
			}
			
			String whereReturn = "";
			if (return_flag == 1)
			
			{
				whereReturn = " and return_flag = " + return_flag;
			}
			
			String whereSample = "";
			if (sample_flag == 1) {
				whereSample = " and sample_flag = " + sample_flag;
			}
			
			String whereStorageType = "";
			if (storage_type != 0) {
				whereStorageType = " and storage_type = " + storage_type;
			}
			
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where 1=1 "
					+ whereDef + whereReturn + whereSample + whereStorageType;
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} catch (Exception e) {
			throw new Exception("FloorProductStorageMgrZJ searchProductStorageCatalog error:" + e);
		}
	}
	
	/**
	 * 
	 * 根据title模糊查询
	 * 
	 * @param title
	 * @return <b>Date:</b>2015年2月4日下午3:47:10<br>
	 * @author: cuicong
	 * @throws Exception
	 * @throws SQLException
	 */
	public DBRow[] getProductStorageCatalogLikeTitle(String title) throws Exception {
		String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where title like '%"
				+ title + "%'";
		try {
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (SQLException e) {
			throw new Exception("FloorProductStorageMgrZJ getProductStorageCatalogLikeTitle error:" + e);
		}
		
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
