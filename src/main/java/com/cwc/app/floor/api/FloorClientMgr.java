package com.cwc.app.floor.api;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.*;

public class FloorClientMgr
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBRow getDetailClientByCid(long cid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("cid",cid);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("clients") + " where cid=?",para));
//			return(dbUtilAutoTran.selectPreSingleCache("select * from " + ConfigBean.getStringValue("clients") + " where cid=?",para,new String[]{ConfigBean.getStringValue("clients")}));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailClientByCid(row) error:" + e);
		}
	}
	
	public DBRow getDetailClientByEmail(String email)
		throws Exception
	{
		try
		{
			
			
			DBRow para = new DBRow();
			para.add("email",email);
			
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("clients") + " where email=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailClientByEmail(row) error:" + e);
		}
	}
		
	public void modClient(long cid,DBRow row)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.update("where cid=" + cid,ConfigBean.getStringValue("clients"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modPOrder(row) error:" + e);
		}
	}
	
	public DBRow[] getClientTraceByCount(long cid,int c)
		throws Exception
	{
		try
		{
			
			String sql = "select * from " + ConfigBean.getStringValue("clients_trace") + " where cid=? order by ct_id desc";
			
			DBRow para = new DBRow();
			para.add("cid",cid);
			
			return(dbUtilAutoTran.selectPreMutlipleByCount(sql, para, c));
		}
		catch (Exception e)
		{
			throw new Exception("getClientTraceByCount(row) error:" + e);
		}
	}
	
	public long addClientTrace(DBRow row)
		throws Exception
	{
		try
		{
			
			
			long ctid = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("clients_trace"));			
			row.add("ct_id",ctid);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("clients_trace"),row);
			
			return(ctid);
		}
		catch (Exception e)
		{
			throw new Exception("addClientTrace(row) error:" + e);
		}
	}
	
	public int getClientTraceCountByCid(long cid)
		throws Exception
	{
		try
		{
			
			
			DBRow para = new DBRow();
			para.add("cid",cid);
			
			DBRow row = dbUtilAutoTran.selectPreSingle("select count(cid) c from " + ConfigBean.getStringValue("clients_trace") + " where cid=?",para);
			
			return(row.get("c", 0));
		}
		catch (Exception e)
		{
			throw new Exception("getClientTraceCountByCid(row) error:" + e);
		}
	}
	
	public DBRow[] getClientTracesByCid(long cid,PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql = "select * from " + ConfigBean.getStringValue("clients_trace") + " where cid=? order by ct_id desc";
			
			DBRow para = new DBRow();
			para.add("cid",cid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql, para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getClientTraceByCount(row) error:" + e);
		}
	}
	
	
	public void modClientByCid(long cid,DBRow row)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.update("where cid=" + cid,ConfigBean.getStringValue("clients"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modClientByCid(row) error:" + e);
		}
	}
	
	public DBRow[] getClientsSortByMcGross(PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select cid ,(select count(oid) c from porder where client_id=cl.email) order_count,(select sum(mc_gross) c from porder where client_id=cl.email) sum_mcgross from clients cl order by cid desc";
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectMutliple(sql,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectMutliple(sql));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getClientsSortByMcGross(row) error:" + e);
		}
	}
	
	public DBRow[] getClientsSortBySumMcGross(String st,String en,String account,PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql;
			
			DBRow para = new DBRow();
			
			if (account.equals(""))
			{
				para.add("st",st);
				para.add("en",en);
				sql = "select *,(select sum(mc_gross) c from " + ConfigBean.getStringValue("porder") + " where client_id=cl.email) sumMcGross from " + ConfigBean.getStringValue("clients") + " cl  where cl.mod_date >= str_to_date( ? ,'%Y-%m-%d') and cl.mod_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) order by sumMcGross desc";
			}
			else if (account.equals("-1"))
			{
				para.add("1",1);
				sql = "SELECT *,(select sum(mc_gross) c from " + ConfigBean.getStringValue("porder") + " where client_id=cl.email) sumMcGross FROM " + ConfigBean.getStringValue("clients") + " cl left join " + ConfigBean.getStringValue("clients_trace") + " ct on cl.cid=ct.cid where ct.note is null and 1=? order by sumMcGross desc";
			}
			else
			{
				para.add("st",st);
				para.add("en",en);
				para.add("account",account);
				sql = "select * ,(select sum(mc_gross) c from " + ConfigBean.getStringValue("porder") + " where client_id=cl.email) sumMcGross from " + ConfigBean.getStringValue("clients") + " cl," + ConfigBean.getStringValue("clients_trace") + " ct where cl.cid=ct.cid and  cl.mod_date >= str_to_date( ? ,'%Y-%m-%d') and cl.mod_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) and ct.account=? group by cl.cid  order by sumMcGross desc";				
			}
			//System.out.println(sql);
			//System.out.println("--------------------");
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getClientsSortBySumMcGross(row) error:" + e);
		}
	}
	
	public DBRow[] getClientsSortBySumMcGrossDown(String st,String en,String account,PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql;
			
			DBRow para = new DBRow();
			
			if (account.equals(""))
			{
				para.add("st",st);
				para.add("en",en);
				sql = "select *,(select sum(mc_gross) c from " + ConfigBean.getStringValue("porder") + " where client_id=cl.email) sumMcGross from " + ConfigBean.getStringValue("clients") + " cl  where cl.mod_date >= str_to_date( ? ,'%Y-%m-%d') and cl.mod_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) order by sumMcGross asc";
			}
			else if (account.equals("-1"))
			{
				para.add("1",1);
				sql = "SELECT *,(select sum(mc_gross) c from " + ConfigBean.getStringValue("porder") + " where client_id=cl.email) sumMcGross FROM " + ConfigBean.getStringValue("clients") + " cl left join " + ConfigBean.getStringValue("clients_trace") + " ct on cl.cid=ct.cid where ct.note is null and 1=? order by sumMcGross asc";
			}
			else
			{
				para.add("st",st);
				para.add("en",en);
				para.add("account",account);
				sql = "select * ,(select sum(mc_gross) c from " + ConfigBean.getStringValue("porder") + " where client_id=cl.email) sumMcGross from " + ConfigBean.getStringValue("clients") + " cl," + ConfigBean.getStringValue("clients_trace") + " ct where cl.cid=ct.cid and  cl.mod_date >= str_to_date( ? ,'%Y-%m-%d') and cl.mod_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) and ct.account=? group by cl.cid  order by sumMcGross asc";				
			}
			
			//System.out.println(sql);
			//System.out.println("--------------------------");
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getClientsSortBySumMcGrossDown(row) error:" + e);
		}
	}
	
	public DBRow[] getClientsSortByModDate(String st,String en,String account,PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql = "";
			
			DBRow para = new DBRow();
			para.add("st",st);
			para.add("en",en);
			
			if (account.equals(""))
			{
				sql = "select * from " + ConfigBean.getStringValue("clients") + " cl  where cl.mod_date >= str_to_date( ? ,'%Y-%m-%d') and cl.mod_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) order by mod_date desc";
			}
			else
			{
				para.add("account",account);
				sql = "select * ,(select count(oid) c from " + ConfigBean.getStringValue("porder") + " where client_id=cl.email) orderCount,(select sum(mc_gross) c from " + ConfigBean.getStringValue("porder") + " where client_id=cl.email) sumMcGross from " + ConfigBean.getStringValue("clients") + " cl ," + ConfigBean.getStringValue("clients_trace") + " ct where cl.cid=ct.cid and cl.mod_date >= str_to_date( ? ,'%Y-%m-%d') and cl.mod_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) and ct.account=? group by cl.cid order by mod_date desc";				
			}
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getClientsSortByModDate(row) error:" + e);
		}
	}
	
	public DBRow[] getClientsSortByModDateDown(String st,String en,String account,PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql = "";
			
			DBRow para = new DBRow();
			para.add("st",st);
			para.add("en",en);
			
			if (account.equals(""))
			{
				sql = "select * from " + ConfigBean.getStringValue("clients") + " cl  where cl.mod_date >= str_to_date( ? ,'%Y-%m-%d') and cl.mod_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) order by mod_date asc";
			}
			else
			{
				para.add("account",account);
				sql = "select * ,(select count(oid) c from " + ConfigBean.getStringValue("porder") + " where client_id=cl.email) orderCount,(select sum(mc_gross) c from " + ConfigBean.getStringValue("porder") + " where client_id=cl.email) sumMcGross from " + ConfigBean.getStringValue("clients") + " cl ," + ConfigBean.getStringValue("clients_trace") + " ct where cl.cid=ct.cid and cl.mod_date >= str_to_date( ? ,'%Y-%m-%d') and cl.mod_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) and ct.account=? group by cl.cid order by mod_date asc";				
			}
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getClientsSortByModDateDown(row) error:" + e);
		}
	}
	
	public DBRow[] getClientsByCidRange(long st_cid,long en_cid,PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql = "select * ,(select count(oid) c from " + ConfigBean.getStringValue("porder") + " where client_id=cl.email) orderCount,(select sum(mc_gross) c from " + ConfigBean.getStringValue("porder") + " where client_id=cl.email) sumMcGross from " + ConfigBean.getStringValue("clients") + " cl where cid>=? and ?>=cid order by cid desc";
			DBRow para = new DBRow();
			para.add("st_cid", st_cid);
			para.add("en_cid", en_cid);
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getClientsByCidRange(row) error:" + e);
		}
	}

	public DBRow[] getSearchServiceByFilter(String inService[],String st,String en,String account,PageCtrl pc)
		throws Exception
	{
	try
	{
		String sql;
		DBRow para = new DBRow();
	
		//全部帐号
		if (account.equals(""))
		{
			//包含过滤条件
			if (inService!=null&&inService.length>0)
			{
				StringBuffer mainSql = new StringBuffer("");
				StringBuffer condSql = new StringBuffer(""); 
				mainSql.append("select c.email,c.summary,ct0.cid from " + ConfigBean.getStringValue("clients") + " c,");
	
				int i=0;
				for (; i<inService.length-1; i++)
				{
					mainSql.append("(select cid from " + ConfigBean.getStringValue("clients_trace") + " where clients_service=");
					mainSql.append(inService[i]);
					mainSql.append("  and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day)  group by cid) ct");
					mainSql.append(i);
					mainSql.append(",");
					
					if (i==0)
					{
						continue;
					}
					//where 条件
					condSql.append(" and ct0.cid=ct");
					condSql.append(i);
					condSql.append(".cid ");
				}
				mainSql.append("(select cid from " + ConfigBean.getStringValue("clients_trace") + " where clients_service=");
				mainSql.append(inService[i]);
				mainSql.append("  and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day)  group by cid) ct");
				mainSql.append(i);
				
				condSql.append(" and ct0.cid=ct");
				condSql.append(i);
				condSql.append(".cid ");
				
				mainSql.append(" where c.cid=ct0.cid  ");
				mainSql.append(condSql.toString());
				mainSql.append(" group by c.cid order by mod_date desc ");
				
				for (int paraI=0; paraI<inService.length; paraI++)
				{
					para.add(paraI+"st",st);
					para.add(paraI+"en",en);
				}
				
				sql = mainSql.toString();
			}
			else
			{
				para.add("st",st);
				para.add("en",en);
				sql = "select * from " + ConfigBean.getStringValue("clients") + " c," + ConfigBean.getStringValue("clients_trace") + " ct  where c.cid=ct.cid and ct.post_date >= str_to_date( ? ,'%Y-%m-%d') and ct.post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) group by c.cid order by mod_date desc";
			}
		}
		else if (account.equals("-1"))
		{
			para.add("1",1);
			sql = "SELECT * FROM " + ConfigBean.getStringValue("clients") + " c left join " + ConfigBean.getStringValue("clients_trace") + " ct on c.cid=ct.cid where ct.note is null and 1=? order by c.cid desc";
		}
		else
		{
			//包含过滤条件的情况
			
			/**
			 * 包含查询思路
			 * 
			 * 先查主表clients
			 * 然后分别对包含条件查询出子表，然后主表和子表通过cid关联，做交集 
			 * 
			 * 
	
			 * 
	
			 * 
			 * 
			 */
			
			if (inService!=null&&inService.length>0)
			{
				StringBuffer mainSql = new StringBuffer("");
				StringBuffer condSql = new StringBuffer(""); 
				mainSql.append("select c.cid,c.email,c.summary,c.mod_date from " + ConfigBean.getStringValue("clients") + " c," + ConfigBean.getStringValue("clients_trace") + " ct,");
	
				int i=0;
				for (; i<inService.length-1; i++)
				{
					mainSql.append("(select cid from " + ConfigBean.getStringValue("clients_trace") + " where clients_service=");
					mainSql.append(inService[i]);
					mainSql.append("  and account=?  and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day)   group by cid) ct");
					mainSql.append(i);
					mainSql.append(",");
					
					if (i==0)
					{
						continue;
					}
					
					//where 条件
					condSql.append(" and ct0.cid=ct");
					condSql.append(i);
					condSql.append(".cid ");
				}
				mainSql.append("(select cid from " + ConfigBean.getStringValue("clients_trace") + " where clients_service=");
				mainSql.append(inService[i]);
				mainSql.append("  and account=?  and post_date >= str_to_date( ? ,'%Y-%m-%d') and post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day)  group by cid) ct");
				mainSql.append(i);
				
				//把子查询做交集
				condSql.append(" and ct0.cid=ct");
				condSql.append(i);
				condSql.append(".cid ");
				
				mainSql.append(" where c.cid=ct.cid and c.cid=ct0.cid ");
				mainSql.append(condSql.toString());
				mainSql.append(" group by c.cid order by c.mod_date desc ");
				
				for (int paraI=0; paraI<inService.length; paraI++)
				{
					para.add(paraI+"account",account);
					para.add(paraI+"st",st);
					para.add(paraI+"en",en);
				}
				
				sql = mainSql.toString();
			}
			else
			{
				para.add("account",account);
				para.add("st",st);
				para.add("en",en);
	
				sql = "select c.cid,c.email,c.summary,c.mod_date from " + ConfigBean.getStringValue("clients") + " c," + ConfigBean.getStringValue("clients_trace") + " ct where c.cid=ct.cid and ct.account=? and ct.post_date >= str_to_date( ? ,'%Y-%m-%d') and ct.post_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day)  group by c.cid order by c.mod_date desc";
			}
			
		}
		
		return(dbUtilAutoTran.selectPreMutliple(sql, para,pc));
	}
	catch (Exception e)
	{
		throw new Exception("getSearchServiceByFilter(row) error:" + e);
	}
	}
	
	public DBRow[] getAllClientsEmail()
		throws Exception
	{
		try
		{
			
			String sql = "SELECT client_id FROM " + ConfigBean.getStringValue("porder") + " group by client_id";
			
			return(dbUtilAutoTran.selectMutliple(sql));
		}
		catch (Exception e)
		{
			throw new Exception("getAllClientsEmail(row) error:" + e);
		}
	}
	
	public void delClientByCid(long cid)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.delete("where cid=" + cid,ConfigBean.getStringValue("clients"));
		}
		catch (Exception e)
		{
			throw new Exception("delClientByCid(row) error:" + e);
		}
	}
	
	public void delClientTraceByCid(long cid)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.delete("where cid=" + cid,ConfigBean.getStringValue("clients_trace"));
		}
		catch (Exception e)
		{
			throw new Exception("delClientTraceByCid(row) error:" + e);
		}
	}
	
	public DBRow[] getAllClientsByDate(String st,String en,PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql;
			
			DBRow para = new DBRow();
			para.add("st",st);
			para.add("en",en);
			
			sql = "select * from " + ConfigBean.getStringValue("clients") + " cl  where cl.mod_date >= str_to_date( ? ,'%Y-%m-%d') and cl.mod_date < date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) order by mod_date desc";
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getAllClientsByDate(row) error:" + e);
		}
	}
	
	public DBRow[] getClientsServiceStat(String st,String en,String account,int clients_service)
		throws Exception
	{
		try
		{
			
			String sql;
			
			DBRow para = new DBRow();
			para.add("st",st);
			para.add("en",en);
			para.add("clients_service",clients_service);
			
			if (account.equals(""))
			{
				sql = "SELECT ct.post_date post_date,count(cid) c  FROM " + ConfigBean.getStringValue("clients_trace") + " ct where str_to_date( ? ,'%Y-%m-%d') <= ct.post_date and date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) > ct.post_date and ct.clients_service=? group by date(ct.post_date) order by ct.post_date asc";				
			}
			else
			{
				para.add("account",account);
				sql = "SELECT ct.post_date post_date,count(cid) c  FROM " + ConfigBean.getStringValue("clients_trace") + " ct where str_to_date( ? ,'%Y-%m-%d') <= ct.post_date and date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) > ct.post_date and ct.clients_service=? and ct.account=?  group by date(ct.post_date) order by ct.post_date asc";
			}
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getClientsServiceStat(row) error:" + e);
		}
	}
	
	public DBRow[] getClientsServiceStatDate(String st,String en,String account)
		throws Exception
	{
		try
		{
			
			String sql;
			
			DBRow para = new DBRow();
			para.add("st",st);
			para.add("en",en);
			
			if (account.equals(""))
			{
				sql = "SELECT ct.post_date post_date  FROM " + ConfigBean.getStringValue("clients_trace") + " ct where  str_to_date( ? ,'%Y-%m-%d') <= ct.post_date and date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) > ct.post_date  group by date(ct.post_date) order by ct.post_date asc";				
			}
			else
			{
				para.add("account",account);
				sql = "SELECT ct.post_date post_date  FROM " + ConfigBean.getStringValue("clients_trace") + " ct where  str_to_date( ? ,'%Y-%m-%d') <= ct.post_date and date_add(str_to_date( ? ,'%Y-%m-%d'), interval 1 day) > ct.post_date and ct.account=? group by date(ct.post_date) order by ct.post_date asc";
			}
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getClientsServiceStatDate(row) error:" + e);
		}
	}
	
	public double getClientsServiceOrderMcGrossStat(String date,String account)
		throws Exception
	{
		try
		{
			
			String sql;
			
			DBRow para = new DBRow();
			para.add("date",date);
	
			if (account.equals(""))
			{
				sql = "SELECT sum(mc_gross) c FROM " + ConfigBean.getStringValue("clients_trace") + " ct where datediff(str_to_date( ? ,'%Y-%m-%d'), ct.post_date)=0 and ct.clients_service=88";
			}
			else
			{
				para.add("account",account);
				sql = "SELECT sum(mc_gross) c FROM " + ConfigBean.getStringValue("clients_trace") + " ct where datediff(str_to_date( ? ,'%Y-%m-%d'), ct.post_date)=0 and ct.clients_service=88 and ct.account=?";				
			}
	
			
			return(dbUtilAutoTran.selectPreSingle(sql,para).get("c", 0d));
		}
		catch (Exception e)
		{
			throw new Exception("getClientsServiceOrderMcGrossStat(row) error:" + e);
		}
	}
	
	public long addClients(DBRow row)
		throws Exception
	{
		try
		{
			
			
			long cid = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("clients"));			
			row.add("cid",cid);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("clients"),row);
			
			return(cid);
		}
		catch (Exception e)
		{
			throw new Exception("addClients(row) error:" + e);
		}
	}
	
	public DBRow[] getClients(PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql = "select * ,(select count(oid) c from " + ConfigBean.getStringValue("porder") + " where client_id=cl.email) orderCount,(select sum(mc_gross) c from " + ConfigBean.getStringValue("porder") + " where client_id=cl.email) sumMcGross from " + ConfigBean.getStringValue("clients") + " cl order by cid desc";
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectMutliple(sql,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectMutliple(sql));
			}
		}
		catch (Exception e)
		{
			throw new Exception("getClients(row) error:" + e);
		}
	}
	
	public DBRow[] getClientsTraceByOid(long oid)
		throws Exception
	{
		try
		{
			String sql;
			
			DBRow para = new DBRow();
			para.add("oid",oid);
			
			sql = "SELECT *  FROM " + ConfigBean.getStringValue("clients_trace") + " where oid=? order by post_date desc";
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getClientsTraceByOid(row) error:" + e);
		}
	}
	
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}








