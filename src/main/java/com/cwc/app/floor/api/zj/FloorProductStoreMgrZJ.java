package com.cwc.app.floor.api.zj;

import com.cwc.app.beans.log.ProductStoreAllocateLogBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.beans.log.ProductStorePhysicalLogBean;
import com.cwc.app.key.ContainerProductState;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;


/**
 * 库存操作独立类。4张库存表操作，3张日志表操作全部集中
 * @author Administrator
 *
 */
public class FloorProductStoreMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	//添加部分
	/**
	 * 添加商品总库存(不分位置，不分批次，不分所有人。只记录数字)
	 */
	public long addProductStorage(DBRow row)
		throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_storage"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addProductStorage(row) error:" + e);
		}
	}
	
	/**
	 * 添加Title库存
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addProductStorageTitle(DBRow row)
		throws Exception
	{
		return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_storage_title"),row);
	}
	
	/**
	 * 添加位置库存
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addProductStoreLocation(DBRow dbrow)
		throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_store_location"), dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ addProductStoreLocation error:"+e);
		}
	}
	
	/**
	 * 添加容器库存
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addProductStorageContainer(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_storage_container"), dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreLocationMgrZJ addProductStoragePlate error:"+e);
		}
	}
	
	/**
	 * 记录库存可用值日志
	 * 直接传log日志bean方法内构建DBRow,使用表自增方式
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addProductStoreLogs(ProductStoreLogBean productStoreLogBean)
		throws Exception
	{
		try
		{
			DBRow psLog = new DBRow();
			
			psLog.add("oid",productStoreLogBean.getOid());
			psLog.add("operation",productStoreLogBean.getOperation());
			psLog.add("quantity",productStoreLogBean.getQuantity());
			psLog.add("ps_id",productStoreLogBean.getPs_id());
			psLog.add("pc_id",productStoreLogBean.getPc_id());
			psLog.add("account",productStoreLogBean.getAccount());
			
			psLog.add("post_date", DateUtil.NowStr());
			psLog.add("bill_type",productStoreLogBean.getBill_type());
			psLog.add("adid",productStoreLogBean.getAdid());
			psLog.add("cancel_psl_id",productStoreLogBean.getCancel_psl_id());
			psLog.add("post_date",DateUtil.NowStr());
			psLog.add("title_id",productStoreLogBean.getTitle_id());
			psLog.add("lot_number",productStoreLogBean.getLot_number());
			
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_store_logs"),psLog);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductStoreMgrZJ addProductStoreLogs error:" + e);
		}
	}
	
	/**
	 * 添加分配记录
	 * @param productStoreAllocateLogBean
	 * @return
	 * @throws Exception
	 */
	public long addProductStoreAllocateLogs(ProductStoreAllocateLogBean productStoreAllocateLogBean)
		throws Exception
	{
		try 
		{
			DBRow productStoreAllocateLog = new DBRow();
			productStoreAllocateLog.add("pc_id",productStoreAllocateLogBean.getPc_id());
			productStoreAllocateLog.add("slc_id",productStoreAllocateLogBean.getSlc_id());
			productStoreAllocateLog.add("allocate_piece_count",productStoreAllocateLogBean.getAllocate_piece_count());
			productStoreAllocateLog.add("allocate_adid",productStoreAllocateLogBean.getAllocate_adid());
			productStoreAllocateLog.add("allocate_time",DateUtil.NowStr());
			productStoreAllocateLog.add("system_bill_id",productStoreAllocateLogBean.getSystem_bill_id());
			productStoreAllocateLog.add("system_bill_type",productStoreAllocateLogBean.getSystem_bill_type());
			
			productStoreAllocateLog.add("con_id",productStoreAllocateLogBean.getCon_id());
			productStoreAllocateLog.add("container_type",productStoreAllocateLogBean.getContainer_type());
			productStoreAllocateLog.add("container_type_id",productStoreAllocateLogBean.getContainer_type_id());
			productStoreAllocateLog.add("allocate_container_count",productStoreAllocateLogBean.getAllocate_container_count());
			productStoreAllocateLog.add("allocate_lot_number",productStoreAllocateLogBean.getLot_number());
			productStoreAllocateLog.add("allocate_title_id",productStoreAllocateLogBean.getTitle_id());
			
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_store_allocate_log"),productStoreAllocateLog);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ addProductStoreAllocateLogs error:" + e);
		}
	}
	
	/**
	 * 添加物理操作日志
	 * @param productStorePhysicalLogBean
	 * @return
	 * @throws Exception
	 */
	public long addProductStorePhysicalLog(ProductStorePhysicalLogBean productStorePhysicalLogBean)
		throws Exception
	{
		try 
		{
			DBRow productStorePhysicalLog = new DBRow();
			productStorePhysicalLog.add("system_bill_id",productStorePhysicalLogBean.getSystem_bill_id());
			productStorePhysicalLog.add("system_bill_type",productStorePhysicalLogBean.getSystem_bill_type());
			productStorePhysicalLog.add("operation_slc_id",productStorePhysicalLogBean.getOperation_type());
			productStorePhysicalLog.add("quantity",productStorePhysicalLogBean.getQuantity());
			productStorePhysicalLog.add("ps_id",productStorePhysicalLogBean.getPs_id());
			productStorePhysicalLog.add("pc_id",productStorePhysicalLogBean.getPc_id());
			productStorePhysicalLog.add("operation_adid",productStorePhysicalLogBean.getOperation_adid());
			productStorePhysicalLog.add("operation_type",productStorePhysicalLogBean.getOperation_type());
			productStorePhysicalLog.add("operation_time",DateUtil.NowStr());
			productStorePhysicalLog.add("operation_machine_id",productStorePhysicalLogBean.getMachine());
			
			if(productStorePhysicalLogBean.getSerial_number()!=null&&!productStorePhysicalLogBean.getSerial_number().equals("")) 
			{
				productStorePhysicalLog.add("serial_number",productStorePhysicalLogBean.getSerial_number());
			}
			if(productStorePhysicalLogBean.getLp_id()!= 0)
			{
				productStorePhysicalLog.add("lp_id",productStorePhysicalLogBean.getLp_id());
			}
			
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_store_physical_log"),productStorePhysicalLog);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStorePhysicalLogMgrZJ addProductStorePhysicalLog error:"+e);
		}
	}
	
	
	
	//可用值库存加减操作
	/**
	 * 总数库存加减可用值,使用日志bean操作
	 */
	public void addSubProductStorageStoreCount(long pid,float quantity)
		throws Exception
	{
		try
		{	
			dbUtilAutoTran.addSubFieldVal(ConfigBean.getStringValue("product_storage"),"where pid = "+pid,"store_count",quantity);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr addSubProductStorageStoreCount error:" + e);
		}
	}
	
	/**
	 * title库存可用值加减，记录库存保留日志
	 * @param productStoreLogBean
	 * @throws Exception
	 */
	public void addSubProductStorageTitleAvailableCount(long pst_id,float quantity)
		throws Exception
	{
		try 
		{
			String where = "where pst_id = "+pst_id;
			
			dbUtilAutoTran.addSubFieldVal(ConfigBean.getStringValue("product_storage_title"),where,"available_count",quantity);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ addSubProductStorageTitleAvailableCount error:"+e);
		}
	}
	
	/**
	 * 位置库存可用值加减
	 * @param ps_location_id
	 * @param quantity
	 * @throws Exception
	 */
	public void addSubProductStoreLocationTheoreticalQuantity(long ps_location_id,float quantity)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.addSubFieldVal(ConfigBean.getStringValue("product_store_location"),"where ps_location_id="+ps_location_id,"theoretical_quantity",quantity);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ addSubProductStoreLocationTheoreticalQuantity error:"+e);
		}
	}
	
	public void cleanProductStoreLocation(long ps_location_id,String count_type)
		throws Exception
	{
		DBRow para = new DBRow();
		para.add(count_type,0);
		
		dbUtilAutoTran.update("where ps_location_id = "+ps_location_id, ConfigBean.getStringValue("product_store_location"), para);
	}
	
	/**
	 * 容器库存理论值加减
	 * @param psc_id
	 * @param quantity
	 * @throws Exception
	 */
	public void addSubProductStorageContainerAvailableCount(long psc_id,float quantity)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.addSubFieldVal(ConfigBean.getStringValue("product_storage_container"),"where psc_id = "+psc_id,"available_count",quantity);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ addSubProductStoragePlateAvailableCount error:"+e);
		}
	}
	
	public void cleanProductStoreContainer(long psc_id,String count_type)
		throws Exception
	{
		DBRow para = new DBRow();
		para.add(count_type,0);
		
		dbUtilAutoTran.update("where psc_id = "+psc_id, ConfigBean.getStringValue("product_storage_container"), para);
	}
	//物理值库存加减
	/**
	 * 总库存物理值加减
	 */
	public void addSubProductStoragePhysicalCount(long pid,float quantity)
		throws Exception
	{
		try
		{	
			dbUtilAutoTran.addSubFieldVal(ConfigBean.getStringValue("product_storage"),"where pid = "+pid,"physical_count",quantity);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductStoreMgrZJ addSubProductStoragePhysicalCount error:" + e);
		}
	}
	
	/**
	 * title库存物理值加减
	 * @param pst_id
	 * @param quantity
	 * @throws Exception
	 */
	public void addSubProductStorageTitlePhysicalCount(long pst_id,float quantity)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.addSubFieldVal(ConfigBean.getStringValue("product_storage_title"),"where pst_id = "+pst_id,"physical_count",quantity);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ addSubProductStorageTitlePhysicalCount error:" + e);
		}
	}
	
	/**
	 * 位置库存物理值加减
	 * @param ps_location_id
	 * @param quantity
	 * @throws Exception
	 */
	public void addSubProductStoreLocationPhysicalQuantity(long ps_location_id,float quantity)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.addSubFieldVal(ConfigBean.getStringValue("product_store_location"),"where ps_location_id="+ps_location_id,"physical_quantity",quantity);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ addOrSubProductStoreLocationTheoreticalQuantity error:"+e);
		}
	}
	
	/**
	 * 容器库存物理值加减
	 * @param psc_id
	 * @param quantity
	 * @throws Exception
	 */
	public void addSubProductStorageContainerPhysicalQuantity(long psc_id,float quantity)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.addSubFieldVal(ConfigBean.getStringValue("product_storage_container"),"where psc_id="+psc_id,"physical_count",quantity);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ addSubProductStorageContainerPhysicalQuantity error:"+e);
		}
	}
	
	//查询
	
	/**
	 * 获得CLP在仓库内库存
	 * @param container_type
	 * @param container_type_id
	 * @return
	 * @throws Exception
	 */
	public int getCLPStoreCount(long ps_id,long clp_type_id,String lot_number)
		throws Exception
	{
		try
		{
			String whereLotNumber = "";
			if (!lot_number.equals("")) 
			{
				whereLotNumber = " and lot_number = "+lot_number;
			}
			
			
//			String sql = "select count(*) as store_count from "+ConfigBean.getStringValue("product_storage_container")+" as psc "
//						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on slc.slc_id = psc.slc_id and slc.slc_psid = ? "
//						+"join "+ConfigBean.getStringValue("container")+" as con on psc.con_id = con.con_id and con.is_full = "+ContainerProductState.FULL+" "
//						+" where psc.container_type = ? and psc.container_type_id = ? and psc.available_count>0 ";
//			
//			DBRow para = new DBRow();
//			para.add("ps_id",ps_id);
//			para.add("container_type",ContainerTypeKey.CLP);
//			para.add("container_type_id",clp_type_id);
//			
//			if (!lot_number.equals("")) 
//			{
//				sql +=" and lot_number = ? ";
//				para.add("lot_number",lot_number);
//			}
//			
//			
//			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			
			String sql ="select count(*) as store_count from (" 
			+"select psc.* from "+ConfigBean.getStringValue("product_storage_container")+" as psc "
			+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on slc.slc_id = psc.slc_id and slc.slc_psid = "+ps_id+" and slc.is_three_dimensional = 1 "
			+"join "+ConfigBean.getStringValue("container")+" as con on psc.con_id = con.con_id and con.is_full = "+ContainerProductState.FULL+" "
			+" where psc.container_type = "+ContainerTypeKey.CLP+" and psc.container_type_id = "+clp_type_id+" and psc.available_count>0 "+whereLotNumber
			+"union all "
			+"select psc.* from "+ConfigBean.getStringValue("product_storage_container")+" as psc "
			+"join "+ConfigBean.getStringValue("product_store_location")+" as psl on psc.slc_id = psl.slc_id and psc.pc_id = psl.pc_id "
			+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on slc.slc_id = psc.slc_id and slc.slc_psid = "+ps_id+" and slc.is_three_dimensional = 0 "
			+"join "+ConfigBean.getStringValue("container")+" as con on psc.con_id = con.con_id and con.is_full = "+ContainerProductState.FULL+" "
			+" where psc.container_type = "+ContainerTypeKey.CLP+" and psc.container_type_id = "+clp_type_id+" and psc.available_count>0 "+whereLotNumber
			+") as clpStore ";
			
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			return result.get("store_count",0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLpTypeMgrZJ getLPStoreCount error:"+e);
		}
	}
	
	/**
	 * 获得BLP在仓库内库存（位置上的BLP与TLP内的BLP都算可使用的BLP库存）
	 * @param container_type_id
	 * @param lot_number
	 * @return
	 * @throws Exception
	 */
	public int getBLPStoreCount(long ps_id,long blp_type_id,long title_id,String lot_number)
		throws Exception
	{
		try
		{
			String whereLotNumber = "";
			
			if (lot_number!=null && !lot_number.equals("")) 
			{
				whereLotNumber = " and psc.lot_number = '"+lot_number+"' ";
			}
			
			
			String sql = "select sum(blp_store_count) as blp_store_count from " 
						+"("
						+"select count(psc.con_id) as blp_store_count from "+ConfigBean.getStringValue("product_storage_container")+"  psc "
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on psc.slc_id = slc.slc_id and slc.slc_psid = ? "
						+"join "+ConfigBean.getStringValue("container")+" as con on psc.con_id = con.con_id and con.is_full = "+ContainerProductState.FULL+" "
						+"where psc.container_type = ? and psc.container_type_id = ? and psc.title_id = ? and psc.available_count>0 "+whereLotNumber
						+"union all "
						+"select count(cl.con_id) as blp_sotre_count from "+ConfigBean.getStringValue("product_storage_container")+" as psc "
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on psc.slc_id = slc.slc_id and slc.slc_psid = ? "
						+"join "+ConfigBean.getStringValue("container_loading")+" as cl on psc.con_id = cl.parent_con_id and cl.container_type_id = ? "
						+"where psc.container_type = ? and psc.title_id = ? "+whereLotNumber
						+") as blpStoreCountView";

			DBRow para = new DBRow();
			para.add("container_psid",ps_id);
//			para.add("container_type",ContainerTypeKey.BLP);//去掉ILP和BLP
			para.add("container_type_id",blp_type_id);
			para.add("title_id",title_id);
			para.add("inner_container_psid",ps_id);
			para.add("inner_container_type",blp_type_id);
			para.add("inner_title_id",title_id);
			para.add("parent_container_type",ContainerTypeKey.TLP);
			
			
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			return result.get("blp_store_count",0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getBLPStoreCount error:"+e);
		}
	}
	
	/**
	 * 获得ILP在仓库内库存
	 * @param ps_id
	 * @param ilp_type_id
	 * @param title_id
	 * @param lot_number
	 * @return
	 * @throws Exception
	 */
	public int getILPProductStore(long ps_id,long ilp_type_id,long title_id,String lot_number)
		throws Exception
	{
		try
		{
			String sql = "select count(cl.con_id) as ilp_store_count from "+ConfigBean.getStringValue("product_storage_container")+" as psc "
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on psc.slc_id = slc.slc_id and slc.slc_psid = ? "
						+"join "+ConfigBean.getStringValue("container_loading")+" as cl on psc.con_id = cl.parent_con_id and cl.container_type_id = ? "
						+"join "+ConfigBean.getStringValue("container")+" as con on psc.con_id = con.con_id and con.is_full = "+ContainerProductState.FULL+" "
						+"where psc.container_type = ? and psc.title_id = ? and psc.lot_number = ? ";
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("inner_container_type_id",ilp_type_id);
			para.add("container_type",ContainerTypeKey.TLP);
			para.add("title_id",title_id);
			para.add("lot_number",lot_number);
			
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			
			return result.get("ilp_store_count",0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getILPProductStore error:"+e);
		}
	}
	
	/**
	 * 获得商品可用散装数量（在TLP上的商品认为是散装的）
	 * @param pc_id
	 * @param title_id
	 * @param lot_number
	 * @return
	 * @throws Exception
	 */
	public int getTLPProductAvailableCount(long ps_id,long pc_id,long title_id,String lot_number)
		throws Exception
	{
		try 
		{
			String sql = "select sum(psc.available_count) as available_count from "+ConfigBean.getStringValue("product_storage_container")+" as psc "
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on slc.slc_id = psc.slc_id and slc.slc_psid = ? "
						+"where psc.pc_id = ? and psc.title_id = ? and container_type = ?  and psc.available_count>0 ";
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("pc_id",pc_id);
			para.add("title_id",title_id);
			para.add("container_type",ContainerTypeKey.TLP);
			
			if (!lot_number.equals(""))
			{
				sql +=" and psc.lot_number = ? ";
				para.add("lot_number",lot_number);
			}
			
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			return (int)result.get("available_count",0f);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getTLPProductAvailableCount error:"+e);
		}
	}
	
	/**
	 * 根据Title仓库商品LopNumber获得库存数
	 * @param ps_id
	 * @param pc_id
	 * @param title_id
	 * @param lot_number
	 * @return
	 * @throws Exception
	 */
	public int getProductStoreCountForPPTL(long ps_id,long pc_id,long title_id,String lot_number)
		throws Exception
	{
		try
		{
			String sql = "select sum(available_count) as available_count from "+ConfigBean.getStringValue("product_storage_title")
						+" where ps_id = ? and pc_id = ? and title_id = ? ";
			
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("pc_id",pc_id);
			para.add("title_id",title_id);
			
			if (!lot_number.equals(""))
			{
				sql += "and lot_number = ? ";
				para.add("lot_number",lot_number);
			}
			
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para); 
			return (int)result.get("available_count",0f);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getProductStoreCountForPPTL error:"+e);
		}
	}
	
	/**
	 * 获得所有人下这个商品的所有批次库存
	 * @param pc_id
	 * @param title_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoresTilte(long pc_id,long title_id,String lot_number)
		throws Exception
	{
		try 
		{
			String whereLotNumber = "";
			if (lot_number!=null&&!lot_number.equals("")) 
			{
				whereLotNumber = " and lot_number = ? ";
			}
			
			String sql = "select * from "+ConfigBean.getStringValue("product_store_title")+" pst where pc_id = ? and title_id = ? "+whereLotNumber;
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("title_id",title_id);
			if (lot_number!=null&&!lot_number.equals("")) 
			{
				para.add("lot_number",lot_number);
			}
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getProductStoreTilte error:"+e);
		}
	}
	
	/**
	 * 根据商品ID，仓库ID获得总库存
	 * @param pc_id
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductStorage(long pc_id,long ps_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_storage")+" where pc_id = ? and cid = ? ";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("ps_id",ps_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getProductStorage error:"+e);
		}
	}
	
	/**
	 * 根据主键ID获得仓库总库存
	 * @param pid
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductStorage(long pid)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_storage")+" where pid = ? ";
			
			DBRow para = new DBRow();
			para.add("pid",pid);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getProductStorage error:"+e);
		}
		
	}
	
	/**
	 * 根据商品ID，仓库ID，titleID，批次获得title库存
	 * @param pc_id
	 * @param ps_id
	 * @param title_id
	 * @param lot_number
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductStorageTitle(long pc_id,long ps_id,long title_id,String lot_number)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_storage_title")+" where pc_id = ? and ps_id = ? and title_id = ? and lot_number = ? ";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("ps_id",ps_id);
			para.add("title_id",title_id);
			para.add("lot_number",lot_number);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getDetailProductStoreTitle error:"+e);
		}
	}
	
	/**
	 * 根据主键ID获得Title库存
	 * @param pst_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductStorageTitle(long pst_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_storage_title")+" where pst_id = ?";
			
			DBRow para = new DBRow();
			para.add("pst_id",pst_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductProductStoreMgrZJ getDetailProductStorageTitle error:"+e);
		}
	}
	
	/**
	 * 根据主键ID获得容器库存
	 * @param psc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductStoreContainer(long psc_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_storage_container")+" where psc_id = ? ";
			
			DBRow para = new DBRow();
			para.add("psc_id",psc_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getDetailProductStoreContainer error:"+e);
		}
	}
	
	/**
	 * 根据商品ID，位置ID，titleID，容器ID，批次获得容器库存
	 * @param pc_id
	 * @param slc_id
	 * @param title_id
	 * @param con_id
	 * @param lot_number
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductStoreContainer(long pc_id,long slc_id,long title_id,long con_id,String lot_number)
		throws Exception
	{
		try
		{
			
				
			StringBuffer sql =  new StringBuffer("select * from "+ConfigBean.getStringValue("product_storage_container")+" where pc_id = ? and slc_id = ?  and con_id = ?");
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("slc_id",slc_id);
			para.add("con_id",con_id);
			
			
			String whereTitle = "";
			if (title_id!=0)
			{
				whereTitle = " and title_id = ? ";
				para.add("title_id",title_id);
			}
			sql.append(whereTitle);
			
			String whereLotNumber = "";
			if(!lot_number.equals(""))
			{
				whereLotNumber = " and lot_number = ? ";
				para.add("lot_number",lot_number);
			}
			sql.append(whereLotNumber);
			
			
			return dbUtilAutoTran.selectPreSingle(sql.toString(), para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getDetailProductStoreContainer error:"+e);
		}
	}
	
	public DBRow[] getDetailProductStoreContainerForPickUpException(long pc_id ,long slc_id,int containter_type,long containter_type_id,long con_id)
		throws Exception
	{
		try 
		{
			StringBuffer sql =  new StringBuffer("select * from "+ConfigBean.getStringValue("product_storage_container")+" where physical_count>0 and pc_id = ? and slc_id = ? and container_type = ? and container_type_id = ? ");
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("slc_id",slc_id);
			para.add("container_type",containter_type);
			para.add("container_type_id",containter_type_id);
			
			String whereCon = "";
			if (con_id !=0)
			{
				whereCon = " and con_id = ? ";
				para.add("con_id",con_id);
			}
			
			sql.append(whereCon);
			
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getDetailProductStoreContainerForPickUpException error:"+e);
		}
	}
	/**
	 * 根据主键ID获得位置库存
	 * @param ps_location_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductStoreLocation(long ps_location_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_location")+" where ps_location_id = ? ";
			
			DBRow para = new DBRow();
			para.add("ps_location_id",ps_location_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getDetailProductStoreLocation error:"+e);
		}
	}
	
	/**
	 * 根据商品，位置，所属人，批次获得位置库存
	 * @param pc_id
	 * @param slc_id
	 * @param title_id
	 * @param lot_number
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProductStoreLocation(long pc_id,long slc_id,long title_id,String lot_number)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_location")+" where pc_id = ? and slc_id = ? and title_id = ? and lot_number = ? ";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("slc_id",slc_id);
			para.add("title_id",title_id);
			para.add("lot_number",lot_number);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getDetailProductStoreLocation error:"+e);
		}
	}
	
	
	
	/**
	 * 获得所需容器的先进先出顺序
	 * @param pc_id
	 * @param ps_id
	 * @param title_id
	 * @param container_type
	 * @param container_type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLPProductStoreOrderByTime(long pc_id,long ps_id,long title_id,int container_type,long container_type_id,String lot_number)
		throws Exception
	{
		try 
		{
			String whereLotNumber = " ";
			if (lot_number!=null&&!lot_number.equals(""))
			{
				whereLotNumber = " and psc.lot_number = '"+lot_number+"' ";
			}
			
			String sql = //3D位置落地容器
						"select psc_id as id,time_number,'Container' as type,0 as from_container_type,0 as from_container_type_id,0 as from_con_id,psc.container_type as pick_container_type,psc.container_type_id as pick_container_type_id,psc.con_id as pick_con_id from product_storage_container as psc "
						+"join storage_location_catalog as slc on psc.slc_id = slc.slc_id and slc.is_three_dimensional = 1 "
						+"join container as con on psc.con_id = con.con_id and con.is_full = "+ContainerProductState.FULL+" "
						+"where psc.available_count>0 and psc.pc_id = "+pc_id+" and slc.slc_psid = "+ps_id+" and psc.title_id = "+title_id+" and psc.container_type = "+container_type+" and psc.container_type_id = "+container_type_id+whereLotNumber
						+" union all "
						//2D位置落地容器，不做distinct,使用distinct后同一位置多个无法表现出来，多个让循环次数来控制位置上拿几个容器
						+"select psl.ps_location_id as id,psl.time_number,'Location' as type,0 as from_container_type,0 as from_container_type_id,0 as form_con_id,psc.container_type as pick_container_type,psc.container_type_id as pick_container_type_id,0 as pick_con_id from product_store_location as psl " 
						+"join storage_location_catalog as slc on psl.slc_id = slc.slc_id and slc.is_three_dimensional = 0 "
						+"join product_storage_container as psc on psl.slc_id = psc.slc_id "
						+"join container as con on psc.con_id = con.con_id and con.is_full = "+ContainerProductState.FULL+" "
						+"where psl.theoretical_quantity>0 and psc.pc_id = "+pc_id+" and slc.slc_psid = "+ps_id+" and psc.title_id = "+title_id+" and psc.container_type = "+container_type+" and psc.container_type_id = "+container_type_id+whereLotNumber
						+" union all "
						//3D位置落地容器TLP内装载的容器，不做distinct,使用distinct后同一位置多个无法表现出来，多个让循环次数来控制位置上拿几个容器
						+"select psc_id as id,psc.time_number,'ContainerLoading' as type,psc.container_type as from_container_type,psc.container_type_id as from_container_type_id,psc.con_id as from_con_id,cl.container_type as pick_container_type,cl.container_type_id as pick_container_type_id,0 as pick_con_id from product_storage_container as psc " 
						+"join storage_location_catalog as slc on psc.slc_id = slc.slc_id and slc.is_three_dimensional = 1 " 
						+"join container_loading as cl on psc.con_id = cl.con_id and cl.container_type = "+container_type+" and cl.container_type_id = "+container_type_id+" "
						+"join container as con on cl.con_id = con.con_id and con.is_full = "+ContainerProductState.FULL+" "
						+"where psc.available_count>0 and psc.pc_id = "+pc_id+" and slc.slc_psid = "+ps_id+" and psc.title_id = "+title_id+" and psc.container_type = "+ContainerTypeKey.TLP+whereLotNumber 
						+" union all "
						//2D位置落地容器TLP内装载的容器，不做distinct,使用distinct后同一位置多个无法表现出来，多个让循环次数来控制位置上拿几个容器
						+"select psl.ps_location_id as id,psl.time_number,'LocationContainerLoading' as type,psc.container_type as from_container_type,psc.container_type_id as from_container_type_id,0 as con_id,cl.container_type as pick_container_type,cl.container_type_id as pick_container_type_id,0 as pick_con_id from product_store_location as psl " 
						+"join storage_location_catalog as slc on psl.slc_id = slc.slc_id and slc.is_three_dimensional = 0 "
						+"join product_storage_container as psc on psl.slc_id = psc.slc_id "
						+"join container_loading as cl on psc.con_id = cl.parent_con_id and cl.parent_container_type = "+container_type+" and cl.parent_container_type_id = "+container_type_id+" "
						+"join container as con on cl.con_id = con.con_id and con.is_full = "+ContainerProductState.FULL+" "
						+"where psc.available_count>0 and psc.pc_id = "+pc_id+" and slc.slc_psid = "+ps_id+" and psc.title_id = "+title_id+" and psc.container_type_id = "+ContainerTypeKey.TLP+whereLotNumber
						+" union all "
						//3D位置落地不满的容器
						+"select psc_id as id,time_number,'ContainerLoading' as type,psc.container_type as from_container_type,psc.container_type_id as from_container_type_id,psc.con_id as from_con_id,cl.container_type as pick_container_type,cl.container_type_id as pick_container_type_id,0 as pick_con_id from product_storage_container as psc " 
						+"join storage_location_catalog as slc on psc.slc_id = slc.slc_id and slc.is_three_dimensional = 1 " 
						+"join container as con on psc.con_id = con.con_id and con.is_full = "+ContainerProductState.DEPART+" "
						+"join container_loading as cl on psc.con_id = cl.con_id and  cl.container_type = "+container_type+" and cl.container_type_id = "+container_type_id+" " 
						+"where psc.available_count>0 and psc.pc_id = "+pc_id+" and slc.slc_psid = "+ps_id+" and psc.title_id = "+title_id+whereLotNumber
						+" union all " 
						//2D位置落地不满的容器
						+"select psl.ps_location_id as id,psl.time_number,'LocationContainerLoading' as type,psc.container_type as from_container_type,psc.container_type_id as from_container_type_id,psc.con_id as from_con_id,cl.container_type as pick_container_type,cl.container_type_id as pikc_contaiern_type_id,0 as pick_con_id from product_store_location as psl " 
						+"join storage_location_catalog as slc on psl.slc_id = slc.slc_id and slc.is_three_dimensional = 0 "
						+"join product_storage_container as psc on psl.slc_id = psc.slc_id "
						+"join container as con on con.con_id = psc.con_id and con.is_full = "+ContainerProductState.DEPART+" "
						+"join container_loading as cl on psc.con_id = cl.con_id  and  cl.container_type = "+container_type+" and cl.container_type_id = "+container_type_id+" "
						+"where psc.available_count>0 and psc.pc_id = "+pc_id+" and slc.slc_psid = "+ps_id+" and psc.title_id = "+title_id+whereLotNumber
						+"order by time_number,type,id" ;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getLPProductStoreOrderByTime error:"+e);
		}
	}
	
	/**
	 * 获得散件商品的捡货顺序
	 * @param pc_id
	 * @param ps_id
	 * @param title_id
	 * @param lot_number
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoreOrderByTime(long pc_id,long ps_id,long title_id,String lot_number)
		throws Exception
	{
		try 
		{
			String whereLotNumber = "";
			if (lot_number!=null&&!lot_number.equals(""))
			{
				whereLotNumber = " and psc.lot_number = '"+lot_number+"' ";
			}
						//3D位置上的TLP
			String sql = "select psc_id as id,psc.time_number,psc.available_count,'ContainerLoading' as type,psc.container_type as from_container_type,psc.container_type_id as from_container_type_id,psc.con_id as from_con_id from product_storage_container as psc " 
						+"join storage_location_catalog as slc on psc.slc_id = slc.slc_id and slc.is_three_dimensional = 1 "
						+"where psc.available_count>0 and psc.pc_id = "+pc_id+" and slc.slc_psid = "+ps_id+" and psc.title_id = "+title_id+" and psc.container_type = "+ContainerTypeKey.TLP+whereLotNumber
						//2D位置上TLP psl.theoretical_quantity
						+" union all " 
						+"select psl.ps_location_id as id,psl.time_number,psc.available_count,'LocationContainerLoading' as type,psc.container_type as from_container_type,0 as from_container_type_id,0 as from_con_id from product_store_location as psl " 
						+"join storage_location_catalog as slc on psl.slc_id = slc.slc_id and slc.is_three_dimensional = 0 "
						+"join product_storage_container as psc on psl.slc_id = psc.slc_id "
						+"where psl.theoretical_quantity>0 and psc.pc_id = "+pc_id+" and slc.slc_psid = "+ps_id+" and psc.title_id = "+title_id+" and psc.container_type = "+ContainerTypeKey.TLP+whereLotNumber
						//3D位置上的CLP					
						+" union all "
						+"select psc_id as id,psc.time_number,psc.available_count,'ContainerLoading' as type,psc.container_type as from_container_type,psc.container_type_id as from_container_type_id,psc.con_id as from_con_id from product_storage_container as psc " 
						+"join storage_location_catalog as slc on psc.slc_id = slc.slc_id and slc.is_three_dimensional = 1 and slc.slc_psid = "+ps_id+" " 
						+"join clp_type as ctype on psc.container_type_id = ctype.lp_type_id and ctype.sku_lp_box_type = 0 "
						+"join container as con on psc.con_id = con.con_id and con.is_full = "+ContainerProductState.DEPART+" "
						+"where psc.available_count>0 and psc.container_type = "+ContainerTypeKey.CLP+" and psc.pc_id = "+pc_id+" and psc.title_id =  "+title_id+whereLotNumber
						//3D位置上的BLP
						/*
						+" union all " 
						+"select psc_id as id,psc.time_number,psc.available_count,'ContainerLoading' as type,psc.container_type as from_container_type,psc.container_type_id as from_container_type_id,psc.con_id as from_con_id from product_storage_container as psc " 
						+"join storage_location_catalog as slc on psc.slc_id = slc.slc_id and slc.is_three_dimensional = 1 and slc.slc_psid = "+ps_id+" "
						+"join box_type as btype on btype.box_inner_type = 0 and btype.box_type_id = psc.container_type_id " 
						+"join container as con on psc.con_id = con.con_id and con.is_full = "+ContainerProductState.DEPART+" "
						+"where psc.available_count>0 and psc.container_type = "+ContainerTypeKey.BLP+" and psc.pc_id = "+pc_id+" and psc.title_id = "+title_id
						*///去掉ILP和BLP
						+" union all "
						//2D位置上的CLP
						+"select psl.ps_location_id as id,psl.time_number,psc.available_count,'LocationContainerLoading' as type,psc.container_type as from_container_type,psc.container_type_id as from_container_type_id,psc.con_id as from_con_id from product_store_location as psl " 
						+"join storage_location_catalog as slc on psl.slc_id = slc.slc_id and slc.is_three_dimensional = 0 " 
						+"join product_storage_container as psc on psl.slc_id = psc.slc_id " 
						+"join clp_type as ctype on psc.container_type_id = ctype.lp_type_id and ctype.sku_lp_box_type = 0 " 
						+"join container as con on psc.con_id = con.con_id and con.is_full = "+ContainerProductState.DEPART+" "
						+"where psc.available_count>0 and psc.pc_id = "+pc_id+" and slc.slc_psid = "+ps_id+" and psc.title_id = "+title_id+" and psc.container_type = "+ContainerTypeKey.CLP+whereLotNumber;
					/*
						+" union all "
						//2D位置上的BLP
						+"select psl.ps_location_id as id,psl.time_number,psc.available_count,'LocationContainerLoading' as type,psc.container_type as from_container_type,psc.container_type_id as from_container_type_id,psc.con_id as from_con_id from product_store_location as psl " 
						+"join storage_location_catalog as slc on psl.slc_id = slc.slc_id and slc.is_three_dimensional = 0 " 
						+"join product_storage_container as psc on psl.slc_id = psc.slc_id " 
						+"join box_type as btype on btype.box_type_id = psc.container_type_id and btype.box_inner_type = 0 "
						+"join container as con on psc.con_id = con.con_id and con.is_full = "+ContainerProductState.DEPART+" "
						+"where psc.available_count>0 and psc.pc_id = "+pc_id+" and slc.slc_psid = "+ps_id+" and psc.title_id = "+title_id+" and psc.container_type = "+ContainerTypeKey.BLP+whereLotNumber
						+" order by time_number,type,id";
					*///去掉ILP和BLP
				return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getProductStoreOrderByTime error:"+e);
		}
	}
	
	/**
	 * 根据区域获得容器库存
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoreContainerByArea(long area_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_storage_container")+" as psc "
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on psc.slc_id = slc.slc_id and slc_area = ? "
						+"order by slc.slc_position_all ";
			
			DBRow para = new DBRow();
			para.add("slc_area",area_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getProductStoreContainerByArea error:"+e);
		}
	}
	
	public DBRow[] getProductStoreContainerBySlc(long slc_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_storage_container")+" as psc "
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on psc.slc_id = slc.slc_id "
						+"where psc.slc_id = ? "
						+"order by slc.slc_position_all ";
			
			DBRow para = new DBRow();
			para.add("slc_id",slc_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStoreMgrZJ getProductStoreContainerBySlc error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
