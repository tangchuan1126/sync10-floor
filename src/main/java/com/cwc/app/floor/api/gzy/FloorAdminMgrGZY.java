package com.cwc.app.floor.api.gzy;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;


public class FloorAdminMgrGZY
{
	private DBUtilAutoTran dbUtilAutoTran;
	//添加管理员
	public long addAdmin(DBRow row)
		throws Exception
	{
		try
		{		
//			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("admin"));
//			row.add("adid",id);
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("admin"), row);
//					dbUtilAutoTran.insert(ConfigBean.getStringValue("admin"),row);					
//			return(id);
		}
		catch (Exception e)
		{
			throw new Exception("addAdmin(row) error:" + e);
		}
	}
	//添加管理员关系
	public long addAdminGRL(DBRow drow) throws Exception{
		        	  				 
			try 
			{
				return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("admin_group_relation"), drow);
			} 
			catch (Exception e) 
			{
				throw new Exception("addAdminGRL(drow) error:" + e);
			}
				  	
	}
	
	//根据账号查询
	public DBRow[] getActionsByPage(PageCtrl pc,String account)
	throws Exception
{
	try
	{
		DBRow para = new DBRow();
		para.add("account",account);
		
		DBRow row[];
		
		if (pc!=null)
		{
			row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("admin")+" where account=? order by adid desc" ,para,pc);
		}
		else
		{
			row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("admin")+" where account=? order by adid desc",para);
		}
		
		return(row);
	}
	catch (Exception e)
	{
		throw new Exception("getActionsByPage() error:" + e);
	}
}
	
	
	//高级查询
	
	public DBRow[] getadvanSearch(long proPsId,String proJsId,long proAdgid,long filter_lid,int lock_state,PageCtrl pc)
	throws Exception{
		try
		{
			DBRow para = new DBRow();
			
			DBRow row[] = null;
			String sql = "select * from "+ConfigBean.getStringValue("admin")+" where 1=1 ";
			if(proPsId == 0 && proJsId.equals("0") && proAdgid == 0 && filter_lid == 0 && -1 == lock_state){
				sql = "select * from "+ConfigBean.getStringValue("admin")+" order by adid desc";
				row = dbUtilAutoTran.selectMutliple(sql, pc);
				return row;
			}
			if(proPsId != 0){
				para.add("ps_id",proPsId);
				sql += "and ps_id=? ";
			}
			if(!proJsId.equals("0")){
				para.add("proJsId",proJsId);
				sql += "and adid in (select adid from " + ConfigBean.getStringValue("admin_group_relation") + " where proJsId = ?)";
			}
			if(1 == lock_state){
				sql += "and llock=? ";
				para.add("llock", lock_state);
			}else if(2 == lock_state){
				sql += "and llock=? ";
				para.add("llock", 0);
			}
			//如果不是从页面上以超链接形式查询(根据办公地点id)说明传来的proJsId默认值是空，那么查询条件就只有areaId一个。
			if (proJsId.equals("")) {
				para.remove("proJsId");// 移除上面添加的proJsId字段
				para.add("parentid", filter_lid);
				para.add("id", filter_lid);
				para.add("lid", filter_lid);
				sql = "select * from "+ConfigBean.getStringValue("admin")+" where adid in(select adid from " + ConfigBean.getStringValue("admin_group_relation") + " a join (select * from "+ ConfigBean.getStringValue("office_location") +" where parentid in(select id from "+ ConfigBean.getStringValue("office_location") +" where parentid = ? or id= ? ) or id= ? ) o on a.AreaId = o.id )order by adid desc";
				row = dbUtilAutoTran.selectPreMutliple(sql, para, pc);
				return row;
			}
			if(proAdgid != 0){
				para.add("adgid", proAdgid);
				sql += "and adid in (SELECT adid from "+ConfigBean.getStringValue("admin_group_relation")+" where adgid = ?)";
			}
			if(filter_lid != 0){
				para.add("parentid", filter_lid);
				para.add("id", filter_lid);
				para.add("lid", filter_lid);
				sql += "and adid in(select adid from " + ConfigBean.getStringValue("admin_group_relation") + " a join (select * from "+ ConfigBean.getStringValue("office_location") +" where parentid in(select id from "+ ConfigBean.getStringValue("office_location") +" where parentid = ? or id= ? ) or id= ? ) o on a.AreaId = o.id )";
			}
			sql += "order by adid desc";
			if (null != pc){
				row = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			}
			else{
				row = dbUtilAutoTran.selectPreMutliple(sql,para);
			}
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getActionsByPage() error:" + e);
		}	
	}

	public void delAdmin(long adid)
		throws Exception
	{
		try
		{
			
			dbUtilAutoTran.delete("where adid=" + adid,ConfigBean.getStringValue("admin"));
		}
		catch (Exception e)
		{
			throw new Exception("delAdmin(row) error:" + e);
		}
	}
	//修改管理员信息
	public void modifyAdmin(long adid,DBRow row)
	throws Exception
	{
		try
		{		
			dbUtilAutoTran.update("where adid=" + adid,ConfigBean.getStringValue("admin"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modifyAdmin(row) error:" + e);
		}
	}
	//根据员工id删除关系表信息
	public void deleteAdminRelation(long adid) throws Exception{
		try
		{		
			dbUtilAutoTran.delete("where adid="+adid, ConfigBean.getStringValue("admin_group_relation"));
		}
		catch (Exception e)
		{
			throw new Exception("deleteAdminRelation() error:" + e);
		}
	}
	
	public DBRow getDetailAdmin(long adid)
		throws Exception
	{
		try
		{
			
			DBRow row = dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("admin") + " where adid=" + adid);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailAdmin(row) error:" + e);
		}
	}

	public DBRow[] getAllAdmin(PageCtrl pc)throws Exception{
		try
		{
			
			DBRow row[];
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("admin") + " order by adid desc",pc);
			}
			else
			{
				row = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("admin") + " order by adid desc");
			}
			
			return(row);
		}
		catch (Exception e){
			throw new Exception("getAllAdmin() error:" + e);
		}
	}
	//获得所有部门信息
	public DBRow[] getAllAdminGroup(PageCtrl pc)
		throws Exception
	{
		try
		{
			
			DBRow row[];
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("admin_group") + " order by adgid desc",pc);
			}
			else
			{
				row = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("admin_group") + " order by adgid desc");
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getAllAdminGroup() error:" + e);
		}
	}
	//根据用户ID获取部门角色关系信息
	public DBRow[] getAdminGroupRelation(long adid)throws Exception{
		try {
			DBRow[] row = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("admin_group_relation") + " where adid="+adid);
			return row;
		} catch (Exception e) {
			throw new Exception("getAdminGroupRelation() error:" + e);
		}
	}
	//通过部门ID获得所有部门
	public DBRow getDetailAdminGroup(long adgid)
		throws Exception
	{
		try
		{
			
			DBRow row = dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("admin_group") + " where adgid =" + adgid);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailAdminGroup() error:" + e);
		}
	}
	
	public DBRow getDetailAdminByAccount(String account)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("account",account);
			DBRow row = dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("admin") + " where account=?",para);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailAdminByAccount() error:" + e);
		}
	}
	
	public DBRow[] getPermitActionByAdgid(long adgid)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("adgid",adgid);
			DBRow row[] = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("admin_group_auth") + " admin_group_auth," + ConfigBean.getStringValue("authentication_action") + " authentication_action where admin_group_auth.adgid=? and admin_group_auth.ataid=authentication_action.ataid",para);		
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getPermitActionByAdgid() error:" + e);
		}
	}

	public DBRow[] getControlTreeByParentid(long parentid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("parentid",parentid);

			String sql = "select * from " + ConfigBean.getStringValue("control_tree") + " where parentid=? order by id asc";
			
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql,para);
		
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getControlTreeByParentid() error:" + e);
		}
	}
	
	public DBRow[] getControlTreeByParentidOrderBySort(long parentid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("parentid",parentid);
	
			String sql = "select * from " + ConfigBean.getStringValue("control_tree") + " where parentid=? order by sort asc";
			
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql,para);
		
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getControlTreeByParentidOrderBySort() error:" + e);
		}
	}
	
	public DBRow getDetailAuthenticationActionByAction(String action)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("action",action);
			
			DBRow row = dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("authentication_action") + " where action=?",para);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailAuthenticationActionByAction(row) error:" + e);
		}
	}
	
	public DBRow[] getControlTreeByAdgid(long adgid)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("adgid",adgid);
	
			String sql = "select * from " + ConfigBean.getStringValue("control_tree") + "," + ConfigBean.getStringValue("control_tree_map") + " where adgid=? and ctid=id order by id asc";
			
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql,para);
		
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getControlTreeByAdgid() error:" + e);
		}
	}
	
	public DBRow getDetailControlTreeByLink(String link)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("link",link);
			
			DBRow row = dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("control_tree") + " where link=?",para);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailControlTreeByLink(row) error:" + e);
		}
	}

	public DBRow getDetailControlTreeById(long id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("id",id);
			
			DBRow row = dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("control_tree") + " where id=?",para);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailControlTreeById(row) error:" + e);
		}
	}

	public DBRow[] getPermitPageAdgid(String page)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("page",page);
	
			String sql = "select ctm.adgid adgid from " + ConfigBean.getStringValue("control_tree") + " ct," + ConfigBean.getStringValue("control_tree_map") + " ctm where ct.link=? and ct.id=ctm.ctid";
			
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql,para);
		
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getPermitPageAdgid() error:" + e);
		}
	}

	public DBRow[] getPermitPageAdid(String page)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("page",page);
	
			String sql = "select ctme.adid adid from " + ConfigBean.getStringValue("control_tree") + " ct," + ConfigBean.getStringValue("control_tree_map_extend") + " ctme where ct.link=? and ct.id=ctme.ctid";
			
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql,para);
		
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getPermitPageAdid() error:" + e);
		}
	}
		
	public DBRow[] getPermitActionAdgid(String action)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("action",action);
	
			String sql = "select aga.adgid adgid from " + ConfigBean.getStringValue("authentication_action") + " aua," + ConfigBean.getStringValue("admin_group_auth") + " aga where aua.action=? and aua.ataid=aga.ataid";
		
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql,para);
		
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getPermitActionAdgid() error:" + e);
		}
	}

	public DBRow[] getPermitActionAdid(String action)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("action",action);
	
			String sql = "select agae.adid adid from " + ConfigBean.getStringValue("authentication_action") + " aua," + ConfigBean.getStringValue("admin_group_auth_extend") + " agae where aua.action=? and aua.ataid=agae.ataid";
		
			DBRow row[] = dbUtilAutoTran.selectPreMutliple(sql,para);
		
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getPermitActionAdid() error:" + e);
		}
	}

	public long addRole(DBRow row)
		throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("admin_group"));
			
			row.add("adgid",id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("admin_group"),row);
			return(id);
		}
		catch (Exception e)
		{
			throw new Exception("addRole(row) error:" + e);
		}
	}
	
	public void modifyRole(long adgid,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where adgid=" + adgid,ConfigBean.getStringValue("admin_group"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modifyRole(row) error:" + e);
		}
	}
	
	public long addRoleControlPageMap(DBRow row)
		throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("control_tree_map"));
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("control_tree_map"),row);
			return(id);
		}
		catch (Exception e)
		{
			throw new Exception("addRoleControlPageMap(row) error:" + e);
		}
	}

	public long addAdminControlPageMap(DBRow row)
		throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("control_tree_map_extend"));
			dbUtilAutoTran.insert(ConfigBean.getStringValue("control_tree_map_extend"),row);
			return(id);
		}
		catch (Exception e)
		{
			throw new Exception("addAdminControlPageMap(row) error:" + e);
		}
	}
	
	public long addRoleActionMap(DBRow row)
		throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("admin_group_auth"));
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("admin_group_auth"),row);
			return(id);
		}
		catch (Exception e)
		{
			throw new Exception("addRoleActionMap(row) error:" + e);
		}
	}
	
	public long addAdminActionMap(DBRow row)
		throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("admin_group_auth_extend"));
			dbUtilAutoTran.insert(ConfigBean.getStringValue("admin_group_auth_extend"),row);
			return(id);
		}
		catch (Exception e)
		{
			throw new Exception("addAdminActionMap(row) error:" + e);
		}
	}

	public void delRole(long adgid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where adgid=" + adgid,ConfigBean.getStringValue("admin_group"));
		}
		catch (Exception e)
		{
			throw new Exception("delRole(row) error:" + e);
		}
	}
	
	public void delRoleControlPageMapByAdgid(long adgid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where adgid=" + adgid,ConfigBean.getStringValue("control_tree_map"));
		}
		catch (Exception e)
		{
			throw new Exception("delRoleControlPageMapByAdgid(row) error:" + e);
		}
	}
	
	public void delAdminControlPageMapByAdid(long adid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where adid=" + adid,ConfigBean.getStringValue("control_tree_map_extend"));
		}
		catch (Exception e)
		{
			throw new Exception("delAdminControlPageMapByAdid(row) error:" + e);
		}
	}
	
	public void delRoleActionMapByAdgid(long adgid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where adgid=" + adgid,ConfigBean.getStringValue("admin_group_auth"));
		}
		catch (Exception e)
		{
			throw new Exception("delRoleActionMapByAdgid(row) error:" + e);
		}
	}
	
	public void delAdminActionMapByAdid(long adid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where adid=" + adid,ConfigBean.getStringValue("admin_group_auth_extend"));
		}
		catch (Exception e)
		{
			throw new Exception("delAdminActionMapByAdid(row) error:" + e);
		}
	}
	
	public DBRow getDetailRoleByName(String name)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("name",name);
			DBRow row = dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("admin_group") + " where name=?",para);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailRoleByName() error:" + e);
		}
	}
	
	public DBRow getDetailRoleByAdgid(long adgid)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("adgid",adgid);
			DBRow row = dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("admin_group") + " where adgid=?",para);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailRoleByAdgid() error:" + e);
		}
	}
		
	public DBRow[] getActionsByPage(PageCtrl pc,long id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("page",id);
			
			DBRow row[];
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("authentication_action")+" where page=?" ,para,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("authentication_action")+" where page=?",para);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getActionsByPage() error:" + e);
		}
	}
	
	
	
	
	public DBRow[] getAdminByAdgid(long adgid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("adgid",adgid);
			
			DBRow row[];
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("admin") + " where adgid=? order by adid desc",para,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("admin") + " where adgid=? order by adid desc",para);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getAdminByAdgid() error:" + e);
		}
	}
	public void updateAdminRegisterToken(DBRow row, long adid) throws Exception{
		try {
			dbUtilAutoTran.update(" where adid = " + adid, ConfigBean.getStringValue("admin"), row);
		} catch (Exception e) {
			throw new Exception("updateAdminRegisterToken(row) error:" + e);
		}
		
	}
	public void updateSnSninfoToken(DBRow row, String snNo) throws Exception{
		try {
			dbUtilAutoTran.update(" where sn = '" + snNo+"'", ConfigBean.getStringValue("sn_sninfo_token"), row);
		} catch (Exception e) {
			throw new Exception("updateSnSninfoToken(row) error:" + e);
		}
	}
	public DBRow checkTokenSn(String snNo) throws Exception{
		try {
			return dbUtilAutoTran.selectSingle("select * from sn_sninfo_token where sn = '"+snNo+"'");
		} catch (Exception e) {
			throw new Exception("checkTokenSn(sn) error:" + e);
		}
	}
	public DBRow getAdminBySn(String snNo) throws Exception{
		try {
			return dbUtilAutoTran.selectSingle("select * from admin where register_token = '"+snNo+"'");
		} catch (Exception e) {
			throw new Exception("getAdminBySn(sn) error:" + e);
		}
	}
	/**
	 * 根据sn获取所有用户信息
	 * @param snNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAdminsBySn(String snNo) throws Exception{
		try {
			return dbUtilAutoTran.selectMutliple("select * from admin where register_token = '"+snNo+"'");
		} catch (Exception e) {
			throw new Exception("getAdminBySn(sn) error:" + e);
		}
	}
	public void updateTokenSnInfo(DBRow row, String snNo) throws Exception{
		try {
			dbUtilAutoTran.update(" where sn = '"+snNo+"'", ConfigBean.getStringValue("sn_sninfo_token"), row);
		} catch (Exception e) {
			throw new Exception("updateTokenSnInfo(sn) error:" + e);
		}
	}
	
	//根据员工名获取管理员信息
	public  DBRow getAdminByEmployeName(String Employe_name) throws Exception{
		try {
			String sql = "select * from "+ConfigBean.getStringValue("admin")+" where employe_name='"+Employe_name+"'";
			return (dbUtilAutoTran.selectSingle(sql));
		} catch (Exception e) {
			throw new Exception("getAdminByEmployeName(Employe_name) error:"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}








