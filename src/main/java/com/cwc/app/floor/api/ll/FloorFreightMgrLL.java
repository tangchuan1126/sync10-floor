package com.cwc.app.floor.api.ll;

import com.cwc.app.beans.ll.CountryCodeBeanLL;
import com.cwc.app.beans.ll.DeliveryFreightCost;
import com.cwc.app.beans.ll.FreightCost;
import com.cwc.app.beans.ll.FreightResources;
import com.cwc.app.beans.ll.TransportFreightCost;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class FloorFreightMgrLL {
	private FreightResources freightResources;
	private CountryCodeBeanLL countryCodeBeanLL;
	private DeliveryFreightCost deliveryFreightCost;
	private TransportFreightCost transportFreightCost;
	private FreightCost freightCost;
	
	public DBRow[] getAllFreightCost() throws Exception {
		return freightCost.getAll("freight_index asc");
	}
	
	public TransportFreightCost getTransportFreightCost() {
		return transportFreightCost;
	}

	public void setTransportFreightCost(TransportFreightCost transportFreightCost) {
		this.transportFreightCost = transportFreightCost;
	}

	public FreightCost getFreightCost() {
		return freightCost;
	}

	public void setFreightCost(FreightCost freightCost) {
		this.freightCost = freightCost;
	}

	public DeliveryFreightCost getDeliveryFreightCost() {
		return deliveryFreightCost;
	}

	public void setDeliveryFreightCost(DeliveryFreightCost deliveryFreightCost) {
		this.deliveryFreightCost = deliveryFreightCost;
	}

	public CountryCodeBeanLL getCountryCodeBeanLL() {
		return countryCodeBeanLL;
	}

	public void setCountryCodeBeanLL(CountryCodeBeanLL countryCodeBeanLL) {
		this.countryCodeBeanLL = countryCodeBeanLL;
	}

	public FreightResources getFreightResources() {
		return freightResources;
	}

	public void setFreightResources(FreightResources freightResources) {
		this.freightResources = freightResources;
	}
	
	public DBRow[] getAllFreightResources() throws Exception {
		return freightResources.getAll();
	}
	
	public DBRow[] getFreightResourcesByCompany(String company,PageCtrl pc) throws Exception {
		DBRow row = new DBRow();
		row.add("fr_company", company);
		return freightResources.getRowsByPara(row, null, null, "fr_id desc", pc);
	}
	
	public DBRow getCountryById(String id) throws Exception {
		return countryCodeBeanLL.getRowById(id);
	}
	
	public DBRow[] getCompanys() throws Exception {
		String sql = "select distinct fr_company from " + freightResources.getTableName() ;
		return freightResources.getDbUtilAutoTran().selectMutliple(sql);
	}
	
	public DBRow[] getDeliveryFreightCostByDeliveryId(String delivery_id) throws Exception {
		DBRow row = new DBRow();
		row.add("delivery_id", delivery_id);
		return deliveryFreightCost.getRowsByPara(row, null, null, "", null);
	}
	
	public DBRow[] getTransportFreightCostByTransportId(String transport_id) throws Exception {
		DBRow row = new DBRow();
		row.add("transport_id", transport_id);
		return transportFreightCost.getRowsByPara(row, null, null, "", null);
		
	}
	
	public DBRow[] getFreightCostByfrId(String ft_id) throws Exception {
		DBRow row = new DBRow();
		row.add("fr_id", ft_id);
		return freightCost.getRowsByPara(row, null, null, "", null);
	}
	
	public void insertDeliveryFreightCost(DBRow row) throws Exception {
		deliveryFreightCost.insertRowInc(row);
	}
	
	public void updateDeliveryFreightCost(DBRow row) throws Exception {
		deliveryFreightCost.updateRow(row);
	}
	
	public void deleteDeliveryFreightCostByDeliveryId(String delivery_id) throws Exception {
		String cond = "where delivery_id = " + delivery_id;
		deliveryFreightCost.deleteByCond(cond);
	}
	
	public void insertTransportFreightCost(DBRow row) throws Exception {
		transportFreightCost.insertRowInc(row);
	}
	
	public void updateTransportFreightCost(DBRow row) throws Exception {
		transportFreightCost.updateRow(row);
	}
	
	public void deleteTransportFreightCostByDeliveryId(String transport_id) throws Exception {
		String cond = "where transport_id = " + transport_id;
		transportFreightCost.deleteByCond(cond);
	}
}
