package com.cwc.app.floor.api.xj;

import java.sql.SQLException;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.app.key.CheckInChildDocumentsStatusTypeKey;
import com.cwc.app.key.EquipmentTypeKey;
import com.cwc.app.key.OccupyStatusTypeKey;
import com.cwc.app.key.CheckInLiveLoadOrDropOffKey;
import com.cwc.app.key.CheckInMainDocumentsRelTypeKey;
import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInSpotStatusTypeKey;
import com.cwc.app.key.CheckInTractorOrTrailerTypeKey;
import com.cwc.app.key.GateCheckLoadingTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.SpaceRelationTypeKey;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorCheckInMgrXj {
	
	private DBUtilAutoTran dbUtilAutoTran;

	
	
	//根据门id 查询门信息
	public DBRow findDoorById(long id)throws Exception{
		try{
			String sql="select * from storage_door where sd_id="+id;
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb findDoorById"+e);
		}
	}
	
	 //查询可用门列表
	 public DBRow[] selectAllDoor(long ps_id,String doorName,long dlo_id)throws Exception{
		 try{
			String sql="select sd.* from storage_door sd  where sd.sd_status=0 and (sd.occupied_status="+OccupyStatusTypeKey.RELEASED+" or "
					 + " (sd.occupied_status <>"+OccupyStatusTypeKey.RELEASED+" and sd.associate_id ="+dlo_id+" and sd.associate_type="+ModuleKey.CHECK_IN+")) and sd.ps_id = "+ps_id;
			if(doorName!=""){
				sql+=" and doorId='"+doorName+"'";
			}
				sql+=" group by CAST(sd.doorId as SIGNED)";
				
	   // System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql);
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb selectAllDoor"+e);
		 }
	 }
	 //查询可用门spot
	 public DBRow[] selectUnavailableSpot(long ps_id,long area_id,long dlo_id)throws Exception{
		 try{
			String sql="select * from storage_yard_control syc left join door_or_location_occupancy_main dm on syc.associate_id=dm.dlo_id where  syc.yc_status="+CheckInSpotStatusTypeKey.OCUPIED+" and syc.ps_id = "+ps_id;
			
			if(area_id>0){
				sql +=" and syc.area_id="+area_id;
			}
	   // System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql);
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb selectUnavailableSpot"+e);
		 }
	 }
	 //查询被占用的门 
	 public DBRow[] selectUseDoor(long ps_id,long area_id)throws Exception{
		 try{
			String sql="select * from door_or_location_occupancy_details dod " +
					" left join storage_door sd on dod.rl_id = sd.sd_id where (occupancy_status=1 or occupancy_status=2) " +
					" and sd.sd_status=0 AND sd.ps_id = "+ps_id;
			if(area_id>0){
				sql+=" and sd.area_id="+area_id ;
			}
			sql+=" GROUP BY dod.rl_id order by CAST(sd.doorId as SIGNED)";
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql);
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb selectUseDoor"+e);
		 }
	 }
	 
	 // 查询被占用门的详细信息
	 public DBRow[] selectUseDoorDetail(long doorId)throws Exception{
		 try{
			 String sql="select * from door_or_location_occupancy_details dd " +
			 		"join door_or_location_occupancy_main dm on dd.dlo_id=dm.dlo_id " +
			 		"where dd.rl_id="+doorId+" and (dd.occupancy_status=1 or dd.occupancy_status=2)";
		//	 System.out.println(sql);
			 return this.dbUtilAutoTran.selectMutliple(sql);
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb selectUseDoorDetail"+e);
		 }
	 }
	 
	 //查询门位置关系
	 public DBRow[] findDoorAndLocation(long doorId,long psId)throws Exception{
		 try{
			 String sql="select distinct * from storage_load_unload_location sl join storage_door sd " +
			 		"on sl.sd_id=sd.sd_id where sd.sd_id="+doorId+" and ps_id="+psId;	
			 //System.out.println(sql);
			 return this.dbUtilAutoTran.selectMutliple(sql);
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb findDoorAndLocation"+e);
		 }
	 }
	 //根据住单据查询 load ---
	 public DBRow[] findloadingByInfoId(long dlo_id)throws Exception{
		 try{
			 String sql="select * from door_or_location_occupancy_details where dlo_id="+dlo_id;
			 return this.dbUtilAutoTran.selectMutliple(sql);
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb findloadingByInfoId"+e);
		 }
	 }
	 
	 //添加数据到detail表
	 public long addOccupancyDetails(DBRow row)throws Exception{
		 try{
			 return this.dbUtilAutoTran.insertReturnId("door_or_location_occupancy_details", row);
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb addOccupancyDetails"+e);
		 }
	 }
	 
	 //查询详细表去重 门
	public DBRow[] selectDoorByInfoId(long infoId)throws Exception{
		try{
			String sql="select DISTINCT(rl_id),doorId from door_or_location_occupancy_details dd join storage_door sd on dd.rl_id=sd_id " +
					"where dlo_id="+infoId;
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb selectDoorByInfoId"+e);
		}
	}
	
	//根据门查询load数据和主单据id
	public DBRow[] selectLotNumberByDoorId(long doorId,long infoId)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_details where dlo_id="+infoId+" and occupancy_type="+OccupyTypeKey.DOOR;
			//System.out.println(sql);
			if(doorId>0){
				sql += " and rl_id="+doorId;
			}
			return this.dbUtilAutoTran.selectMutliple(sql);
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb selectLotNumberByDoorId"+e);
		 }
	}
	
	//查询主单据功能 带分页
	public DBRow[] selectAllMain(long id,PageCtrl pc)throws Exception{
		try{
			
			String sql="SELECT * FROM door_or_location_occupancy_main  dm LEFT JOIN  t_asset ta on ta.id=dm.gps_tracker";
			if(id>0){
				sql += " where dlo_id ="+id;
			}
				sql += " ORDER BY dm.dlo_id DESC";
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql, pc);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb selectAllMain"+e);
		}
	}
	
	//根据id查询主单据信息--打印用
	public DBRow findGateCheckInById(long id)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_main  where dlo_id="+id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findGateCheckInById error:"+e);
		}
	}
	
	//向主表添加数据
	public long gateCheckInAdd(DBRow row) throws Exception {
		try {
			
			return  dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("door_or_location_occupancy_main"), row);

		} catch (Exception e) {
			throw new Exception("FloorCheckInMgrZwb gateCheckInAdd error:"+e);
		}
	}
	//查询GPS号是否存在
	public DBRow findAssetByGPSNumber(String gps_tracker) throws Exception {
		try {
			
			String sql ="select * from t_asset where  imei='"+gps_tracker+"'";
			
			return dbUtilAutoTran.selectSingle(sql);

		} catch (Exception e) {
			throw new Exception("FloorCheckInMgrZwb findAssetByGPSNumber error:"+e);
		}
	}	
	
	//查询GPS根据id
	public DBRow findAssetByGpsId(long gps_id) throws Exception {
		try {
			String sql ="select * from t_asset where id="+gps_id;
			//System.out.println(sql);
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorCheckInMgrZwb findAssetByGPSNumber error:"+e);
		}
	}	
	
	//添加GPS设备
	public long addAsset(DBRow asset) throws Exception {
		try {
			
			return  dbUtilAutoTran.insertReturnId("t_asset", asset);

		} catch (Exception e) {
			throw new Exception("FloorCheckInMgrZwb gateCheckInAdd error:"+e);
		}
	}
	//更新停车位状态
	public void updateParkingStatus(long ycId,int status) throws Exception {
		String sql = "update storage_yard_control s set s.yc_status = "+ status +" where s.yc_id = " +ycId;
		try {
			dbUtilAutoTran.executeSQL(sql);
		} catch (Exception e) {
			throw new Exception("FloorCheckInMgrZwb updateParkingStatus error:"+e);
		}
	}
	
	//根据停车位id 查询停车位
	public DBRow findStorageYardControl(long yc_id)throws Exception{
		try{
			String sql="select * from  storage_yard_control where yc_id="+yc_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findStorageYardControl"+e);
		}
	}
	

	//根据停车位查询主表信息
	public DBRow findMainBySpot(long yc_id,String ctnr,long infoId)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_main where yc_id="+yc_id;
			if(!StrUtil.isNull(ctnr)){
				sql += " AND gate_container_no = '"+ctnr+"'";
			}else{
				sql+=" and dlo_id = "+infoId;
			}
			sql += " order by create_time desc LIMIT 0,1";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findMainBySpot"+e);
		}
	}

	
	//根据load#查询loading表  --仓库id
	/**
	 pickup:首先根据staging查询出对应的门,如果没有，再根据loaction关联到area,根据area指定此区域下的门, 优先指定单数,再指定双数
	 delivery:首先根据title关联到area,根据area指定此区域下的门  , 优先指定单数,再指定双数
	 */
	public DBRow[] getLoading(String area_name,String title_name,long type,long mainId,int mark,long ps_id) throws Exception {
		try {   


			String sql = null;
			   if(type==1){
				   if(mark==1){
					   sql ="select  * from storage_load_unload_location sul join storage_door sd  on sul.sd_id = sd.sd_id AND sd.sd_status=0 "+
		                        " where sul.location_name='"+area_name+"' and sul.psc_id="+ps_id; 
				   }else{
					   sql ="select sd.* ,slc.* ,sla.* from storage_location_catalog slc join storage_area_door szd on slc.slc_area=szd.area_id " +
					   		" join storage_location_area sla on slc.slc_area=sla.area_id"+
					   		" join storage_door sd on sd.sd_id=szd.sd_id    " +
			/**		   		+ "and sd.sd_id not in " +
                            " (select rl_id from door_or_location_occupancy_details where (occupancy_status = 1 or occupancy_status = 2 ) and dlo_id!="+mainId+")"*/
                            " where  sd.sd_status=0 and sd.occupied_status="+OccupyStatusTypeKey.RELEASED+" AND slc.slc_position='"+area_name+"' and slc.slc_psid="+ps_id; 

					   if(mark==2){
						   sql+= " and CAST(sd.doorId as SIGNED)%2=1 ";
					   }
					   else if(mark==3){
						   sql+=" and CAST(sd.doorId as SIGNED)%2=0";
					   }
				   }
				   
				   
			   }else{
				   sql ="select DISTINCT sd.sd_id,sd.doorId,sla.area_id,sla.area_name,t.title_id from title t join check_in_zone_with_title zt on zt.title_id=t.title_id " +
				   		" join storage_area_door sad on zt.area_id=sad.area_id"+
                        " join storage_door sd on sd.sd_id=sad.sd_id  "+
                   /**      " AND sd.sd_id not in   "+
                        "  (select rl_id from door_or_location_occupancy_details where (occupancy_status = 1 or occupancy_status = 2 ) and dlo_id!=0)"+*/
                        " left join storage_location_area sla on sd.area_id=sla.area_id where  sd.sd_status=0 and sd.occupied_status="+OccupyStatusTypeKey.RELEASED+" and t.title_name= '"+title_name+"' and sd.ps_id="+ps_id;

				   if(mark==1){
					   sql+=" and CAST(sd.doorId as SIGNED)%2=1";
				   }else if(mark==2){
					   sql+=" and CAST(sd.doorId as SIGNED)%2=0";
				   }
				
			   }
			 //  System.out.println(sql);
			return dbUtilAutoTran.selectMutliple(sql);  
		}
		catch (Exception e) 
		{
			throw new Exception("FloorCheckInMgrZwb getLoading error:"+e);
		}
	}

	//查询仓库
	public DBRow findPsDetail(long ps_id)throws Exception{
		try{
			String sql="select * from product_storage_catalog where id="+ps_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findPsDetail error:"+e);
		}
	}
	
	//查询load与 order关系
	public DBRow[] findOrderByLoadId(String loadno)throws Exception{
		try{
			String sql="select * from check_in_pickup_staging where loadno='"+loadno+"'";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findOrderByLoadId error:"+e);
		}
	}
	
	//根据doorId和entryId查询详细表     --xiugai
	public DBRow[] selectDetails(long doorId,long main_id) throws Exception{
		try{
			String sql="select DISTINCT(rl_id),doorId ,dd.number,dd.number_status,dd.occupancy_status,dd.dlo_id from door_or_location_occupancy_details dd " +
					"left join storage_door sd on dd.rl_id=sd_id " +
					"left join door_or_location_occupancy_main dm on dd.dlo_id=dm.dlo_id where 1=1" ;
			if(doorId>0){
				sql+=" and dd.rl_id="+doorId;
			}
			if(main_id>0){
				sql+=" and dd.dlo_id="+main_id;
			}
			
//			if(numberStatus>0){
//				sql+=" and number_status="+numberStatus;
//			}
//			if(doorStatus>0){
//				sql+=" and occupancy_status="+doorStatus;
//			}
			sql+=" order by  rl_id desc";
//			System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb selectDetails"+e);
		}
	}	
	//根据doorId和entryId查询详细表
	public DBRow[] selectDetailsInfo(long doorId,long main_id) throws Exception{
			try{


				String sql="select rl_id ,doorId ,dd.number,dd.number_status,dd.occupancy_status,dd.dlo_id,dd.number_type,dd.dlo_detail_id,dd.supplier_id,dd.customer_id from door_or_location_occupancy_details dd " +
							"left join storage_door sd on dd.rl_id=sd_id where 1=1" ;

						
				if(doorId>0 || doorId==-1){
					sql+=" and dd.rl_id="+doorId;
				}
				if(main_id>0){
					sql+=" and dd.dlo_id="+main_id;
				}
				if(main_id>0 && doorId==0){
					sql+=" group by rl_id";
				}
				sql+=" order by  rl_id desc";
//				System.out.println(sql);
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrZwb selectDetailsInfo"+e);
			}
	}	
	//根据doorId和entryId查询详细表
		public DBRow[] selectTaskBy(long rl_id,long main_id,int occupancy_type) throws Exception{
				try{

					String sql="select dd.rl_id ,sd.doorId, syc.yc_no ,dd.number,dd.number_status,dd.occupancy_status,dd.dlo_id,dd.number_type,dd.dlo_detail_id,dd.supplier_id,dd.customer_id,dd.occupancy_type from door_or_location_occupancy_details dd " +
								"left join storage_door sd on dd.rl_id=sd.sd_id and dd.occupancy_type="+OccupyTypeKey.DOOR+" left join storage_yard_control syc on dd.rl_id=syc.yc_id and dd.occupancy_type="+OccupyTypeKey.SPOT+"  where 1=1 "  ;
							
					if(rl_id>0 ){
						sql+=" and dd.rl_id="+rl_id+" and dd.occupancy_type="+occupancy_type;
					}
					if(main_id>0){
						sql+=" and dd.dlo_id="+main_id;
					}
					if(main_id>0 && rl_id==0){
						sql+=" group by dd.occupancy_type,rl_id ";
					}
					sql+=" order by  rl_id desc";
//					System.out.println(sql);
					return this.dbUtilAutoTran.selectMutliple(sql);
				}catch(Exception e){
					 throw new Exception("FloorCheckInMgrZwb selectTaskBy"+e);
				}
		}	
	//更新主单据 数据
	public long modCheckIn(long main_id, DBRow row) throws Exception{
		try{
			 return dbUtilAutoTran.update("where dlo_id="+main_id, "door_or_location_occupancy_main", row);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb modCheckIn"+e);
		}
	}
	//根据id查询主表
	public DBRow selectMainByEntryId(long id)throws Exception{
		try{
			
			String sql="select * from door_or_location_occupancy_main dm"+
			           "  left join t_asset ta on ta.id=dm.gps_tracker where dlo_id="+id;
			
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb selectMainByEntryId"+e);
		}
	}
	//根据id查询主表
	public DBRow selectMainByEntryId(long id,long adgid , long ps_id)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_main dm "
			           + " left join t_asset ta on ta.id=dm.gps_tracker "
			           + " left join product_storage_catalog psc on dm.ps_id=psc.id "
			           + " left join load_bar lb on dm.load_bar_id=lb.load_bar_id "
			           + " where dlo_id="+id;
			if(adgid!=10000){
				sql +=" and dm.ps_id="+ps_id;
			}
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb selectMainByEntryId"+e);
		}
	}
	//查询停车位信息
	public DBRow[] getParkingOccupancy(String yc,long ps_id,long flag,long spotArea,long mainId) throws Exception {
		try {
			
			String sql =" select yc.yc_id ,yc.yc_no ,yc.yc_status,dm.gate_container_no,yc.area_id, dm.gate_liscense_plate,yc.associate_id" +
						"	from " +ConfigBean.getStringValue("storage_yard_control") + " yc" +
						"	LEFT JOIN " +ConfigBean.getStringValue("door_or_location_occupancy_main") + " dm ON yc.associate_id = dm.dlo_id " +
						"	where yc.ps_id =  " +ps_id +" and (yc.yc_status=1 or( yc.yc_status ="+CheckInSpotStatusTypeKey.OCUPIED+" and yc.associate_id ="+mainId+" and yc.associate_type="+ModuleKey.CHECK_IN+"))";
			if(!yc.equals("") && yc!=null && flag==2){
				sql += " and yc.yc_no like "+"\'"+yc+"%\'";
			}	
			if(!yc.equals("") && yc!=null && flag ==0 || flag==1){
				sql += " and yc.yc_no = "+"\'"+yc+"\'";
				
			}	
			if(spotArea>0 && flag==3){
				sql +=" and yc.area_id="+spotArea+"  and CAST(yc.yc_no as SIGNED)%2=1";
			}
			if(spotArea>0 && flag==4){
				sql +=" and yc.area_id="+spotArea+"  and CAST(yc.yc_no as SIGNED)%2=0";
			}
			if(spotArea>0){
				sql +=" and yc.area_id="+spotArea;
			}
/**			if(!spotArea.equals("") && spotArea!=null && flag==6){
				sql +=" and yc.area_id='"+spotArea+"' and yc.yc_status=2";
			}*/
			
			
			sql += "  order by CAST(yc.yc_no as SIGNED)";
//			System.out.println(sql);
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (SQLException e) {
			throw new Exception("FloorCheckInMgrZwb getParkingOccupancy():" + e);
		}
	}
	
	public DBRow[] findOccupancyDetails(long main_id) throws Exception {
		try{
			
			String sql="select dm.*, dd.*, IFNULL(sd.doorId, sy.yc_no) as occupy_name from door_or_location_occupancy_main  dm join door_or_location_occupancy_details dd " +
						" on dm.dlo_id = dd.dlo_id "
						+ " left join storage_door sd on sd.sd_id=dd.rl_id and dd.occupancy_type="+OccupyTypeKey.DOOR
						+ " left join storage_yard_control sy on sy.yc_id=dd.rl_id and dd.occupancy_type="+OccupyTypeKey.SPOT
						+" where dd.dlo_id="+main_id;
						
			//System.out.println(sql);
			return  dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findOccupancyDetails"+e);
		}
	}
		
	//根据主单据id 和子单据id 查询子单据 是否存在  
	public DBRow[] findDetailIsExist(long dlo_id,String load_number,String ctn_number,String bol_number,String others_number)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_details where dlo_id="+dlo_id;
			if(!load_number.equals("")){
				sql+=" and load_number='"+load_number+"'";
			}
			if(!ctn_number.equals("")){
				sql+=" and ctn_number='"+ctn_number+"'";
			}
			if(!bol_number.equals("")){
				sql+=" and bol_number='"+bol_number+"'";
			}
			if(!others_number.equals("")){
				sql+=" and others_number='"+others_number+"'";
			}
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findDetailIsExist"+e);
		}
	}
	
	//查询子单据是否存在  --仓库id
	public DBRow[] findBillIsExist(String order,String type,long ps_id,String company_id)throws Exception{
		try{

			String sql="select * from door_or_location_occupancy_details dds join door_or_location_occupancy_main ddm " +
					   "on dds.dlo_id=ddm.dlo_id where ps_id="+ps_id+" and number="+"\'"+order+"\'  and number_status!=4 and number_status!=5"+
					   " and ddm.status <>  "+CheckInMainDocumentsStatusTypeKey.LEFT;
			if(Long.parseLong(type)==GateCheckLoadingTypeKey.LOAD){
				sql+=" and number_type in (10,11,17,18)";
			}else{
				sql+=" and number_type in (12,13,14)";
			}
			if(!StrUtil.isBlank(company_id)){
				sql+=" and company_id='"+company_id+"'";
			}
//			if(type.equals("load")){
//				sql+=" and load_number="+"\'"+order+"\'  and number_status!=4 and number_status!=5";
//			}
//			if(type.equals("ctn")){
//				sql+=" and ctn_number="+"\'"+order+"\' and number_status!=4 and number_status!=5";			
//			}
//			if(type.equals("other")){
//				sql+=" and others_number="+"\'"+order+"\' and number_status!=4 and number_status!=5";
//			}
//			if(type.equals("bol")){
//				sql+=" and bol_number="+"\'"+order+"\' and number_status!=4 and number_status!=5";
//			}
//			sql+=" and ddm.status <>  "+CheckInMainDocumentsStatusTypeKey.LEFT;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findBillIsExist"+e);
		}
	}
	//查询子单据是否是partially
		public DBRow[] findBillIsPartially(String order,long ps_id)throws Exception{
			try{

				String sql="select * from door_or_location_occupancy_details dd join door_or_location_occupancy_main dm on dd.dlo_id=dm.dlo_id " +
						" where number="+"\'"+order+"\' and number_status=5 and dm.ps_id="+ps_id;

				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb findBillIsPartially"+e);
			}
		}
	//更新子表数据
	public long updateDetailByIsExist(long detail_id,DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.update("where dlo_detail_id="+detail_id, "door_or_location_occupancy_details", row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findDetailIsExist"+e);
		}
	}
	
	//warehouse 操作一个门时释放占用门
	public long updataWarehouseDoor(long dlo_id,DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.update("where occupancy_status=2 and dlo_id="+dlo_id,"door_or_location_occupancy_details", row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb updataWarehouseDoor"+e);
		}
	}
	//warehouse 查询被占用的门
	public DBRow[] wareHouseUseDoor(long dlo_id)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_details where dlo_id="+dlo_id+" and occupancy_status=2";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb wareHouseUseDoor"+e);
		}
	}
	
	//warehouse查询门下是否单子都被关闭
	public DBRow[] wareHouseSearchOrderIsClose(long dlo_id,long door_id)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_details where rl_id="+door_id+" and dlo_id="+dlo_id+"  and number_status!=3";
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb wareHouseSearchOrderIsClose"+e);
		}
	}
	
	//android 巡逻释放门用
	public long updateDetailByDoorIdAndDloId(long info_id,long door_id,DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.update("where dlo_id="+info_id+" and rl_id="+door_id,"door_or_location_occupancy_details", row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findDetailIsExist"+e);
		}
	}
	
	//根据 number 查询详细
	public DBRow findDetailByBill(long dlo_id,String load_number,String ctn_number,String bol_number,String others_number)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_details where dlo_id="+dlo_id;
			if(!load_number.equals("")){
				sql+=" and load_number='"+load_number+"'";
			}
			if(!ctn_number.equals("")){
				sql+=" and ctn_number='"+ctn_number+"'";
			}
			if(!bol_number.equals("")){
				sql+=" and bol_number='"+bol_number+"'";
			}
			if(!others_number.equals("")){
				sql+=" and others_number='"+others_number+"'";
			}
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findDetail"+e);
		}
	}
	 
    //根据mainId  查询该单据下所有load 
    public DBRow[] findAllLoadByMainId(long mainId)throws Exception{
    	try{
    		String sql="select * from door_or_location_occupancy_details dd  left join storage_location_area sla on dd.zone_id = sla.area_id" +
    				   " left join storage_door sd on dd.rl_id=sd.sd_id and dd.occupancy_type="+OccupyTypeKey.DOOR+
    				   " left join storage_yard_control syc on dd.rl_id=syc.yc_id and dd.occupancy_type="+OccupyTypeKey.SPOT+
    				   " where dlo_id="+mainId+" order by dd.dlo_detail_id";
    		//System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findAllLoadByMainId"+e);
    	}
    }
    
    //根据主单据号 查询 子单据 被占用门
    public DBRow[] findUseDoorByMainId(long maind_id)throws Exception{
    	try{
    		String sql="select * from door_or_location_occupancy_details where dlo_id="+maind_id+" and  (occupancy_status=1 or occupancy_status=2)";
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findAllLoadByMainId"+e);
    	}
    }
    
    //门卫修改子单据
    public long updateDetail(long dlo_detail_id,DBRow row)throws Exception{
    	try{
    		return this.dbUtilAutoTran.update("where dlo_detail_id="+dlo_detail_id, "door_or_location_occupancy_details ", row);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb updateDetail"+e);
    	}
    }
    
    //根据mainId 查询该单据下 去重门
    public DBRow[] findDoorNoRepeat(long mainId)throws Exception{
    	try{
    		String sql="select DISTINCT(rl_id) from door_or_location_occupancy_details where dlo_id="+mainId+" and rl_id is not null";
    		//System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findDoorNoRepeat"+e);
    	}
    }
    
    //window 签 查询门 不要load
    public DBRow[] findDoorNoRepeatNotPickUp(long mainId)throws Exception{
    	try{
    		String sql="select DISTINCT(rl_id) from door_or_location_occupancy_details where dlo_id="+mainId+" and rl_id is not null and load_number is null";
    		//System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findDoorNoRepeat"+e);
    	}
    }
    
    //load生成标签    
    public DBRow[] createLoadBiaoQian(long mainId)throws Exception{
    	try{
    		String sql="select * from door_or_location_occupancy_details  dd " +
    				"join storage_door sd on dd.rl_id=sd.sd_id " +
    				"where dlo_id="+mainId+" and load_number is not null and rl_id is not null";
    		//System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findDoorNoRepeat"+e);
    	}
    }	
    /**
     * 通过entryID查询Bol不能为空的子单据
     * @param mainId
     * @return
     * @throws Exception
     * zyj
     */
    public DBRow[] findBolNoDetailsByEntryId(long EntryId)throws Exception
    {
    	try{
    		String sql="select * from door_or_location_occupancy_details  dd " +
    				" join storage_door sd on dd.rl_id=sd.sd_id " +
    				" where dlo_id="+EntryId+" and bol_number is not null and rl_id is not null";
    		//System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findBolNoDetailsByEntryId"+e);
    	}
    }
    /**
     * 通过entryID查询Container不能为空的子单据
     * @param mainId
     * @return
     * @throws Exception
     * zyj
     */
    public DBRow[] findContainerNoDetailsByEntryId(long EntryId)throws Exception
    {
    	try{
    		String sql="select * from door_or_location_occupancy_details  dd " +
    				" join storage_door sd on dd.rl_id=sd.sd_id " +
    				" where dlo_id="+EntryId+" and ctn_number is not null and rl_id is not null";
    		//System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findContainerNoDetailsByEntryId"+e);
    	}
    }
    /**
     * 通过entryID查询Containera或者bol不能为空的子单据
     * @param EntryId
     * @return
     * @throws Exception
     * zyj
     */
    public DBRow[] findBolNoContainerNoNotDetailsByEntryId(long EntryId)throws Exception
    {
    	try{
    		String sql="select * from door_or_location_occupancy_details  dd " +
    				" join storage_door sd on dd.rl_id=sd.sd_id " +
    				" where dlo_id="+EntryId+" and (bol_number is not null or ctn_number is not null)and rl_id is not null" +
    				" order by bol_number, ctn_number";
    		//System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findBolNoContainerNoNotDetailsByEntryId"+e);
    	}
    }
    
    
    //根据单据类型 ，单据id 查询通知的详细信息
    public DBRow[] findNoticeDedetail(long id,long moduleKey,long processKey)throws Exception{
    	try{
    		
    		String sql="select admin.employe_name,s.schedule_id,s.associate_id,s.schedule_detail,s.sms_short_notify,s.sms_email_notify,s.schedule_is_note from schedule s join admin a on s.assign_user_id=a.adid " +
    				"join schedule_sub ss on s.schedule_id=ss.schedule_id " +
    				"join admin on admin.adid= ss.schedule_execute_id " +
    				"where associate_id="+id+" and associate_type="+moduleKey+" and associate_process="+processKey;
   		//System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findNoticeDedetail"+e);
    	}
    }
    
    //根据单据类型 ，单据id 查询通知的详细信息
    public DBRow[] findAllNoticeDedetail(long id,long moduleKey,long windowKey,long warehouseKey)throws Exception{
    	try{
    		
    		String sql="select admin.employe_name,s.schedule_id,s.associate_id,s.schedule_detail,s.sms_short_notify,s.sms_email_notify,s.schedule_is_note from schedule s join admin a on s.assign_user_id=a.adid " +
    				"join schedule_sub ss on s.schedule_id=ss.schedule_id " +
    				"join admin on admin.adid= ss.schedule_execute_id " +
    				"where associate_id="+id+" and associate_type="+moduleKey+" and (associate_process="+windowKey+" or associate_process="+warehouseKey+")";
   		//System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findNoticeDedetail"+e);
    	}
    }
    
   //根据单据类型 ，单据id 查询通知的详细信息
    public DBRow[] findAllWindowNoticeDedetail(long id,long windowKey,long warehouseKey)throws Exception{
    	try{
    		
    		String sql="select admin.employe_name,s.schedule_id,s.associate_id,s.schedule_detail,s.sms_short_notify,s.sms_email_notify,s.schedule_is_note,a.adid from schedule s join admin a on s.assign_user_id=a.adid " +
    				"join schedule_sub ss on s.schedule_id=ss.schedule_id " +
    				"join admin on admin.adid= ss.schedule_execute_id " +
    				"where associate_id="+id+" and (associate_process="+windowKey+" or associate_process="+warehouseKey+")";
   		//System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findNoticeDedetail"+e);
    	}
    }
    
    
    
    //添加checkin等待信息
    public DBRow[] selectAllCheckInWait(long zoneId)throws Exception{
    	try{
    		String sql="select * from check_in_waiting where 1=1";
    		if(zoneId!=0){
    			sql+=" and zone_id="+zoneId;
    		}
    		//System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb selectAllCheckInWait"+e);
    	}
    }
    
    //添加check in等待信息
    public long addCheckInWait(DBRow row)throws Exception{
    	try{
    		return this.dbUtilAutoTran.insertReturnId("check_in_waiting", row);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb addCheckInWait"+e);
    	}
    }
    
    //查询等待的zone 根据 门id
    public DBRow findZoneByDoorId(long door_id)throws Exception{
    	try{
    		String sql="select * from storage_door sd left join check_in_waiting cw on sd.area_id=cw.zone_id " +
    				"where sd_id="+door_id+" LIMIT 1";
    		return this.dbUtilAutoTran.selectSingle(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb FloorCheckInMgrZwb"+e);
    	}
    }
    
    //根据 id 查询等待信息
    public DBRow findWaitById(long id)throws Exception{
    	try{
    		String sql="select * from check_in_waiting where waiting_id="+id;
    		return this.dbUtilAutoTran.selectSingle(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findWaitById"+e);
    	}
    }
    
    //区域可用门
    public DBRow[] zoneReadyDoor(long zone_id,long dlo_id,String doorName)throws Exception{ //要改张睿
    	try{	
    		String sql="select * from storage_door sr LEFT JOIN " +
			        "(select * from door_or_location_occupancy_details where (occupancy_status=1 or occupancy_status=2)";
    		if(dlo_id!=0){
    			sql+=" and dlo_id!="+dlo_id;
    		}
    		    sql+=") as dd on sr.sd_id=dd.rl_id where sr.area_id="+zone_id+"  AND sr.sd_status=0 and rl_id is null";
    		    
    		if(!doorName.equals("")){
    			sql +=" and doorId="+"\'"+doorName+"\'";
    		}
   // 		System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb zoneReadyDoor"+e);
    	}
    }
	//修改子单据和门的状态
	public DBRow modOccupancyDetailsNumberStatus(long main_id, String number,int flag,int status,int occupancy_status,long doorId,long ps_id,String note) throws Exception {
		
		try {
			DBRow[] partially = null;
			DBRow row = new DBRow();
			int record = 0;
			
			if(occupancy_status>0){
				row.add("occupancy_status",occupancy_status);
				record = dbUtilAutoTran.update("where dlo_id="+main_id+" and rl_id="+doorId , "door_or_location_occupancy_details", row);
			}
			if(!number.equals("")){
				row.add("number_status",status);
				row.add("finish_time",DateUtil.NowStr());
				row.add("note",note);

//				if(flag==1){
					record = dbUtilAutoTran.update("where dlo_id="+main_id+" and number='"+number+"'", "door_or_location_occupancy_details", row);
					partially=this.findBillIsPartially(number,ps_id);
					if(partially!=null && partially.length>0){
						for (int j = 0; j < partially.length; j++) {
								dbUtilAutoTran.update("where dlo_id="+partially[j].get("dlo_id", 0)+" and number='"+number+"'", "door_or_location_occupancy_details", row);
						}
					}
					
//				}else if(flag==2){
//					record = dbUtilAutoTran.update("where dlo_id="+main_id+" and ctn_number='"+number+"'", "door_or_location_occupancy_details", row);
//				}else if(flag==3){
//					record = dbUtilAutoTran.update("where dlo_id="+main_id+" and bol_number='"+number+"'", "door_or_location_occupancy_details", row);
//				}else{
//					record = dbUtilAutoTran.update("where dlo_id="+main_id+" and others_number='"+number+"'", "door_or_location_occupancy_details", row);
//				}
			}
			DBRow result = new DBRow();
			if(record>0){
				result.add("number", number);
				result.add("flag", flag);
				result.add("status", status);
				result.add("occupancy_status", occupancy_status);
			}
			return result;
			
		} catch (Exception e) {
			throw new Exception("FloorCheckInMgrZwb modOccupancyDetailsNumberStatus error:"+e);
		}
	}
	
	public void modOccupancyDetails(DBRow row) throws Exception{
		try{
			long main_id  =row.get("dlo_id", 01);
			dbUtilAutoTran.update("where dlo_id="+main_id, "door_or_location_occupancy_details", row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findOccupancyDetails"+e);
		}
		
	}
	public void modOccupancyDetailsDoor(DBRow row) throws Exception{
		try{
			long main_id  =row.get("dlo_id", 0l);
			long door_id  =row.get("door_id", 0l);
			row.remove("door_id");
			String condition="";
			if(door_id>0 || door_id==-1){
				condition= " and rl_id="+door_id;
			}
			dbUtilAutoTran.update("where dlo_id="+main_id+condition, "door_or_location_occupancy_details", row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb modOccupancyDetailsDoor"+e);
		}
		
	}
	//跟新门从保留状态改为占用状态
	public long updateDoorUse(long detail_id,DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.update("where dlo_detail_id="+detail_id+" and occupancy_status=1", "door_or_location_occupancy_details",row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb updateDoorUse"+e);
		}
	}
	
	//warehouse 更新 子单据门状态 同门下 子单据一起变
	public long updateDetailDoor(long dlo_id,long door_id,DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.update("where dlo_id="+dlo_id+" and rl_id="+door_id+" and occupancy_status=1", "door_or_location_occupancy_details",row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb updateDetailDoor"+e);
		}
	}
    
    //区域占用门
    public DBRow[] zoneUseDoor(long zone_id)throws Exception{
    	try{		
    		String sql="select * from storage_door sr join door_or_location_occupancy_details ds " +
    				"on sr.sd_id=ds.rl_id " +
    				"join door_or_location_occupancy_main main " +
    				"on ds.dlo_id=main.dlo_id " +
    				"where sr.area_id="+zone_id+" and (occupancy_status = 1 or occupancy_status = 2 ) AND sr.sd_status=0";
    		//System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb zoneUseDoor"+e);
    	}
    }
    
    //根据主单据id 查询单据
    public DBRow findMainById(long dlo_id)throws Exception{
    	try{
    		String sql="select * from door_or_location_occupancy_main where dlo_id="+dlo_id;
    		//System.out.println(sql);
    		return this.dbUtilAutoTran.selectSingle(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findMainById"+e);
    	}
    }
	
	//释放门
    public void openDoorByInfoId(long infoId,String door)throws Exception{
    	try{
    		String sql = "update door_or_location_occupancy_details set occupancy_status = "+OccupyStatusTypeKey.RELEASED+" where dlo_id="+infoId+
    				     " and rl_id in ("+door+")";
    		//System.out.println(sql);
    		dbUtilAutoTran.executeSQL(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb openDoorByInfoId"+e);
    	}
    }
    
    //根据主单据和门id查询 子单据号
    public DBRow[] findOrderByEntryIdAndDoorId(long entry_id,long door_id)throws Exception{
    	try{
    		String sql="select * from door_or_location_occupancy_details where rl_id="+door_id+" and dlo_id="+entry_id;
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findOrderByEntryIdAndDoorId"+e);
    	}
    }
    
    //window签用
   
    
    //释放门
    public void openDoorByInfoId(long infoId,DBRow row)throws Exception{
    	try{
    		
    		dbUtilAutoTran.update("where dlo_id="+infoId, "door_or_location_occupancy_details", row);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb openDoorByInfoId"+e);
    	}
    }
	//条件过滤
	public DBRow[] filterCheckIn(String entryType,String start_time,String end_time,String mainStatus,String tractorStatus,String numberStatus,
			String doorStatus,long ps_id,long loadBar, long priority , PageCtrl pc) throws Exception {
		try{
			
			String sql="select DISTINCT dm.* ,ta.*,psc.* ,lb.* from door_or_location_occupancy_main dm "
//					+ " LEFT JOIN storage_yard_control yc  on yc.associate_id = dm.dlo_id"+
//			           " left join storage_door sd on sd.associate_id=dm.dlo_id "
			           + " left join t_asset ta on ta.id=dm.gps_tracker" +
					   " left join door_or_location_occupancy_details dd on dm.dlo_id=dd.dlo_id"
					   + " left join product_storage_catalog psc on dm.ps_id=psc.id "+ 
					   " left join load_bar lb on dm.load_bar_id=lb.load_bar_id where 1=1" ;
						
			
			if(!entryType.equals("")&!entryType.equals("0")){
				if(entryType.contains("1")){
					sql += "  and dm.rel_type in ("+entryType+",3)";
				}else{
					sql += "  and dm.rel_type in ("+entryType+")";
				}
				
			}
			if(!mainStatus.equals("")&!mainStatus.equals("0")){
				sql += "  and dm.status in ("+mainStatus+")";
			}
			if(!tractorStatus.equals("")&!tractorStatus.equals("0")){
				sql += "  and dm.tractor_status in("+tractorStatus+")";
			}
			if(!numberStatus.equals("")&!numberStatus.equals("0")){
				sql += "  and dd.number_status in("+numberStatus+")";
			}
			if(!doorStatus.equals("")&!doorStatus.equals("0")){
				sql += "  and dd.occupancy_status in("+doorStatus+") and dd.rl_id!=-1";
			}
			if(ps_id>0){
				sql += "  and  dm.ps_id ="+ps_id;
			}
			if(!start_time.equals("")){
				sql += "  and  dm.create_time >='"+start_time+"'";
			}
			if(!end_time.equals("")){
				sql += "  and  dm.create_time <='"+end_time+"'";
			}
			if(loadBar!=0){
				if(loadBar==-1){
					sql +=" and (dm.load_bar_id =0 or dm.load_bar_id is null)";
				}else{
					sql +=" and dm.load_bar_id ="+loadBar;
				}
				
			}
			if(priority>0){
				sql += "  and  priority ="+priority;
			}
			sql += "    ORDER BY dm.dlo_id DESC";
		//	System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql, pc);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb filterCheckIn"+e);
		}
	}
	//导出过滤
	public DBRow[] filterCheckIn(String entryType,String start_time,String end_time,String mainStatus,String tractorStatus,String numberStatus,
			String doorStatus,long loadBar,long priority,long ps_id) throws Exception {
			try{			

				String sql="select DISTINCT dm.* , ta.imei ,lb.* ,psc.title "//, yc.yc_no , yc.yc_id , yc.yc_status
						+ " from door_or_location_occupancy_main dm  "
//						+ " LEFT JOIN storage_yard_control yc  on yc.yc_id = dm.yc_id "
				          + " left join t_asset ta on ta.id=dm.gps_tracker" +
						   " left join door_or_location_occupancy_details dd on dm.dlo_id=dd.dlo_id"
						   + " left join product_storage_catalog psc on dm.ps_id=psc.id "
						   + " left join load_bar lb on dm.load_bar_id=lb.load_bar_id where 1=1" ;

				
				if(!entryType.equals("")&!entryType.equals("0")){
					if(entryType.contains("1")){
						sql += "  and dm.rel_type in ("+entryType+",3)";
					}else{
						sql += "  and dm.rel_type in ("+entryType+")";
					}
					
				}
				if(!mainStatus.equals("")&!mainStatus.equals("0")){
					sql += "  and dm.status in ("+mainStatus+")";
				}
				if(!tractorStatus.equals("")&!tractorStatus.equals("0")){
					sql += "  and dm.tractor_status in("+tractorStatus+")";
				}
				if(!numberStatus.equals("")&!numberStatus.equals("0")){
					sql += "  and dd.number_status in("+numberStatus+")";
				}
				if(!doorStatus.equals("")&!doorStatus.equals("0")){
					sql += "  and dd.occupancy_status in("+doorStatus+") and dd.rl_id!=-1";
				}
				if(ps_id>0){
					sql += "  and  dm.ps_id ="+ps_id;
				}
				if(!start_time.equals("")){
					sql += "  and  dm.create_time >='"+start_time+"'";
				}
				if(!end_time.equals("")){
					sql += "  and  dm.create_time <='"+end_time+"'";
				}
				if(loadBar!=0){
					if(loadBar==-1){
						sql +=" and (dm.load_bar_id =0 or dm.load_bar_id is null)";
					}else{
						sql +=" and dm.load_bar_id ="+loadBar;
					}
					
				}
				if(priority>0){
					sql += "  and  priority ="+priority;
				}
				sql += "    ORDER BY dm.dlo_id DESC";
				//System.out.println(sql);
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb filterCheckIn"+e);
			}
		}
	//导出过滤
		public DBRow[] excelFilterCheckIn(String start_time,String end_time,long ps_id) throws Exception {
				try{			
				
					String sql="select  *  from door_or_location_occupancy_main dm  LEFT JOIN storage_yard_control yc "+
					           " on yc.yc_id = dm.yc_id  left join t_asset ta on ta.id=dm.gps_tracker left join door_or_location_occupancy_details dd on dm.dlo_id=dd.dlo_id "+ 
					           " left join storage_door sd on sd.sd_id=dd.rl_id left join product_storage_catalog psc on dm.ps_id=psc.id "+
							   " left join load_bar lb on dm.load_bar_id=lb.load_bar_id where 1=1" ;

					
					
					if(ps_id>0){
						sql += "  and  dm.ps_id ="+ps_id;
					}
					if(!start_time.equals("")){
						sql += "  and  dm.create_time >='"+start_time+"'";
					}
					if(!end_time.equals("")){
						sql += "  and  dm.create_time <='"+end_time+"'";
					}
					
					sql += "    ORDER BY dm.dlo_id DESC";
					//System.out.println(sql);
					return this.dbUtilAutoTran.selectMutliple(sql);
				}catch(Exception e){
					throw new Exception("FloorCheckInMgrZwb filterCheckIn"+e);
				}
			}
	//导出 当天迟到的load
	public DBRow[] selectCreateTimeForLoadNo(String time,long ps_id) throws Exception {
		try{	
			String sql="select DISTINCT dm.*,dd.number,dd.number_type from door_or_location_occupancy_details dd  " +
					"LEFT JOIN door_or_location_occupancy_main dm "+
			           " ON dm.dlo_id=dd.dlo_id where 1=1 " ;
//			if(!"".equals(load_number)){
//				sql += "  and dd.load_number ="+load_number;
//			}
			
			sql += "  and  dm.create_time >='"+time+"'";
			sql += "  and  dm.create_time <='"+time+" 23:59:59' and dm.ps_id="+ps_id;
			sql += "    ORDER BY dm.dlo_id DESC";
		//	System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb filterCheckIn"+e);
		}
	}
	
    //释放门时查看是否有人等待
	public DBRow findWaitingListByDoorId(long doorId) throws Exception {
		try{
			
			String sql="select * from door_or_location_occupancy_details dd join check_in_waiting cw on dd.zone_id=cw.zone_id"+
					" where dd.rl_id="+doorId+" and cw.wait_status=1"+ 
					" and dd.occupancy_status = 3"+
					" LIMIT 1";
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findWaitingListByDoorId"+e);
		}
	}
	public DBRow[] findParkAndDoorByEntryId(long mainId) throws Exception {
		try{		
    		String sql="select * from door_or_location_occupancy_main dm left join  door_or_location_occupancy_details dd "+
    				   "on dm.dlo_id = dd.dlo_id and dd.occupancy_status!=3 left join storage_door sd on dd.rl_id = sd.sd_id "+
    				   "left join storage_yard_control syc on  dm.yc_id = syc.yc_id  where dm.dlo_id="+mainId+" group by dd.rl_id order by dd.rl_id desc";
    
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findParkAndDoorByEntryId"+e);
    	}
	}
	public DBRow[] findAllZone(long psId) throws Exception {
		try{		
    		String sql="select sla.area_id, sla.area_name,sla.patrol_time  from storage_door sd join (select * from storage_location_area sla where sla.area_psid="+psId+" and sla.area_type=2) sla on sd.area_id=sla.area_id group by sd.area_id order by sla.area_name";
					   
    				  
           // System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findAllZone"+e);
    	}
	}
	public DBRow[] findDoorsByZoneId(long zoneId,long mainId,long ps_id,long flag) throws Exception {
		try{		
    		String sql="select sd.* from storage_door sd left join " +
					"(select * from door_or_location_occupancy_details where (occupancy_status = 1 or occupancy_status = 2 ) and dlo_id!="+mainId+") dod " +
					"on sd.sd_id = dod.rl_id where dod.dlo_detail_id is null and sd.ps_id="+ps_id;
    		if(zoneId>0 && flag==0){
    			sql +=" and area_id="+zoneId;
    		}	
    		if(zoneId>0 && flag==1){
    			sql +=" and area_id="+zoneId+" and CAST(sd.doorId as SIGNED)%2=1";
    		}	
    		if(zoneId>0 && flag==2){
    			sql +=" and area_id="+zoneId+" and CAST(sd.doorId as SIGNED)%2=0";
    		}	
    		sql+=" group by CAST(sd.doorId as SIGNED)";
    		//System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findDoorsByZoneId"+e);
    	}
	}
	public DBRow[] findSpotArea(long ps_id) throws Exception {
		try{		
    		String sql="select * from storage_location_area sla where sla.area_psid="+ps_id+" and sla.area_type=3 order by sla.area_name";
    				  
            //System.out.println(sql);
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findSpotArea"+e);
    	}
	}
	public long addCheckInLog(DBRow row) throws Exception {
		try {
 			return  dbUtilAutoTran.insertReturnId("check_in_log", row);

		} catch (Exception e) {
			throw new Exception("FloorCheckInMgrZwb addCheckInLog error:"+e);
		}
	}
	public DBRow[] findCheckInLog(long dlo_id) throws Exception {
		try{		
    		String sql="select *  from check_in_log cil left join admin a on cil.operator_id=a.adid where dlo_id="+dlo_id+"  ORDER BY check_in_log_id DESC";
    		return this.dbUtilAutoTran.selectMutliple(sql);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb findCheckInLog"+e);
    	}
	}

	//张睿添加
	public DBRow getDoorOrLocationOccupancyMainBy(long dlo_id) throws Exception{
		try{
			DBRow para = new DBRow();
			para.add("dlo_id",dlo_id);
			
			DBRow result = dbUtilAutoTran.selectPreSingle("select *   from door_or_location_occupancy_main where dlo_id= ? " , para);
			 
			return result ;
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb countDoorOrLocationOccupancyMainBy"+e);
		}
	}
	
	//张睿添加
	public int countDoorOrLocationOccupancyMainBy(long dlo_id) throws Exception{
		try{
			DBRow para = new DBRow();
			para.add("dlo_id",dlo_id);
			
			DBRow result = dbUtilAutoTran.selectSingle("select count(*) as count_sum from door_or_location_occupancy_main where dlo_id= "+ dlo_id);
			int count = 0 ;
			if(result != null){
				count = result.get("count_sum", 0);
			}
			return count ;
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb countDoorOrLocationOccupancyMainBy"+e);
		}
	}
	/**
	 * 张睿添加
	 * @param dlo_id
	 * @return
	 * @throws Exception
	 */
    public DBRow[] getDetailByMainRecordId(long dlo_id) throws Exception{
    	try{
    		StringBuffer sql = new StringBuffer(); 
    		sql.append("select *  from door_or_location_occupancy_details as detail left join storage_door as sd on detail.rl_id = sd.sd_id "
    			//	+ " detail.number_type in("+ModuleKey.CHECK_IN_LOAD+","+ModuleKey.CHECK_IN_ORDER+","+ModuleKey.CHECK_IN_PONO+","+ModuleKey.CHECK_IN_PICKUP_ORTHERS+") "
    						+ "where detail.dlo_id = " + dlo_id+" and rl_id is not null");
    		//System.out.println(sql.toString());
    		return dbUtilAutoTran.selectMutliple(sql.toString());
    	}catch (Exception e) {
			 throw new Exception("FloorCheckInMgrZwb getDetailByMainRecordId"+e);
    	}
    }
    
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public DBRow findGroup(long groupId) throws Exception {
		try{
    		String sql ="select * from t_group where id="+groupId;
    		
    		return dbUtilAutoTran.selectSingle(sql);
    	}catch (Exception e) {
			 throw new Exception("FloorCheckInMgrZwb getDetailByMainRecordId"+e);
    	}
	}

	public long addGroup(DBRow group) throws Exception {
		try{
    		return this.dbUtilAutoTran.insertReturnId("t_group", group);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb addCheckInWait"+e);
    	}
		
	}

	public long addGroupAssetRelation(DBRow group_asset) throws Exception {
		try{
    		return this.dbUtilAutoTran.insertReturnId("t_group_asset", group_asset);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb addCheckInWait"+e);
    	}
		
	}

	public void updateAsset(long gpsId,DBRow updateRow) throws Exception {
		try{
    		
    		dbUtilAutoTran.update("where id="+gpsId, "t_asset",updateRow);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb updateAsset"+e);
    	}
		
	}
	
	//扫描门
	public DBRow selectDetailByDoorId(long door_id)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_details where rl_id="+door_id+" and (occupancy_status=1 or occupancy_status=2) LIMIT 1";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb selectDetailByDoorId"+e);
		}
	}
	
	public DBRow[] selectDoorOrLocationOccupancyDetails(long entry_id)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_details where dlo_id="+entry_id;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb selectDoorOrLocationOccupancyDetails"+e);
		}
	}
	
	//更新停车位
	public long updateYcIdByDloId(long dlo_id,DBRow row)throws Exception{
		try{
			
			return this.dbUtilAutoTran.update("where dlo_id="+dlo_id, "door_or_location_occupancy_main", row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb updateYcIdByDloId"+e);
		}
	}
	//扫描门 时关闭
	public long updateCloseDoor(long door_id,DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.update("where rl_id="+door_id, "door_or_location_occupancy_details",row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb updateCloseDoor"+e);
		}
	}
	//查询门是否存在或占用
	public DBRow selectDoorisExistByDoorName(long ps_id, String doorName) throws Exception {
		try{
			String sql="select max(dd.dlo_id) ,sd.doorId,sd.sd_id,dd.occupancy_status from storage_door sd left join  door_or_location_occupancy_details dd " +
					   "on sd.sd_id = dd.rl_id  where  sd.doorId= "+"\'"+doorName+"\'"+" AND sd.sd_status=0 and sd.ps_id="+ps_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb selectDoorisExistByDoorName"+e);
		}
	}
	
	//删除子单据
	public long detOccupancyDetails(long detail_id)throws Exception{
		try{
			return this.dbUtilAutoTran.delete("where dlo_detail_id="+detail_id, "door_or_location_occupancy_details");
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb detOccupancyDetails"+e);
		}
	}
	
	//根据货柜号查询停车位
	public DBRow findSpotByCtnNo(String container_no,long ps_id)throws Exception{
		try{

			String sql="select * from door_or_location_occupancy_main where gate_container_no='"+container_no+"' and ps_id="+ps_id+" and status <> "+CheckInMainDocumentsStatusTypeKey.LEFT+" order by create_time desc LIMIT 0,1";

			//String sql="select * from door_or_location_occupancy_main where gate_container_no='"+container_no+"'";
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findSpotByCtnNo"+e);
		}
	}
	//根据货柜号查询所有单据
	public DBRow[] findAllSpotByCtnNo(String container_no,long ps_id)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_main where gate_container_no='"+container_no+"' and ps_id="+ps_id+" and status <> "+CheckInMainDocumentsStatusTypeKey.LEFT+" order by create_time desc";
			//String sql="select * from door_or_location_occupancy_main where gate_container_no='"+container_no+"'";
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findAllSpotByCtnNo"+e);
		}
	}
	//根据货柜号查询多余的单据
	public DBRow[] findSpotByCtnNo(String container_no,long ps_id,long dlo_id)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_main where gate_container_no='"+container_no+"' and ps_id="+ps_id+" and status <> "+CheckInMainDocumentsStatusTypeKey.LEFT+" and dlo_id!='"+dlo_id+"' order by create_time desc";
			//String sql="select * from door_or_location_occupancy_main where gate_container_no='"+container_no+"'";
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findSpotByCtnNo"+e);
		}
	}
	
	//根据主单据号查询被占用的门
	public DBRow[] findUseDoorByDloId(long dlo_id)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_details ds " +
					"join storage_door sr on ds.rl_id=sr.sd_id " +
					"where dlo_id="+dlo_id+" and (occupancy_status=1 or occupancy_status=2)";
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findUseDoorByDloId"+e);
		}
	}
	

	
	//查询是否有通知
	public DBRow[] findSchedule(long associate_id,long typeKey)throws Exception{
		try{
			String sql="select * from schedule where associate_id="+associate_id+" and associate_process="+typeKey;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findSchedule"+e);
		}
	}
	
	//查询通知详细
	public DBRow[] selectSchedule(long associate_id,long typeKey)throws Exception{
		try{
			String sql="select admin.employe_name,admin.adid,s.schedule_id,s.associate_id,s.schedule_detail,s.sms_short_notify,s.sms_email_notify,s.schedule_is_note " +
					"from schedule s join admin a on s.assign_user_id=a.adid " +
					"join schedule_sub ss on s.schedule_id=ss.schedule_id " +
					"join admin on admin.adid= ss.schedule_execute_id " +
					"where associate_id="+associate_id+" and associate_process="+typeKey;
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb selectSchedule"+e);
		}
	}
	
	//更新子单据状态
	public long updateOccupancyDetails(String ids,DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.update("where dlo_detail_id in("+ids+")", "door_or_location_occupancy_details", row);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb updateOccupancyDetails"+e);
		}
	}


	public DBRow[] findMainByCondition(long ps_id, long entryId,
			String license_plate, String trailerNo) throws Exception {
		try{
			String sql="select dm.dlo_id,dm.gate_liscense_plate,dm.gate_container_no,dm.company_name,dm.warehouse_check_in_time ,"+
					   " dm.gate_check_in_time , dm.status as trailer_status,dm.tractor_status ,dm.rel_type  " +
					   " from door_or_location_occupancy_main dm "+
		/**			   + "LEFT JOIN storage_yard_control syc ON dm.dlo_id = syc.associate_id "+
					   " LEFT JOIN storage_location_area sla ON sla.area_id = syc.area_id " +
					   " LEFT JOIN storage_door sd ON dm.dlo_id = sd.associate_id "+*/
					   " where dm.ps_id="+ps_id;
			if(entryId>0){
				sql+=" and dm.dlo_id="+entryId;
			}
			if(!license_plate.equals("")){
				sql+=" and dm.gate_liscense_plate='"+license_plate+"' and dm.tractor_status <> "+CheckInMainDocumentsStatusTypeKey.LEFT;
			}
			if(!trailerNo.equals("")){
				sql+=" and dm.gate_container_no='"+trailerNo+"' and dm.status <> "+CheckInMainDocumentsStatusTypeKey.LEFT;
			}
			sql+=" order by dm.dlo_id desc";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findMainByEntryId"+e);
		}
	}
	public DBRow[] findDoorByEntryId(long entryId) throws Exception {
		try{
			String sql="select * from  storage_door where associate_id="+entryId;
		
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findDoorByEntryId"+e);
		}
	}
	public DBRow[] findSpotByEntryId(long entryId) throws Exception {
		try{
			String sql="select * from storage_yard_control where associate_id ="+entryId;
					 
			
			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findSpotByEntryId"+e);
		}
	}
/**	public DBRow[] findDoubtDockMainMesByCondition(long ps_id, long entryId,
			String license_plate, String trailerNo) throws Exception {
		try{
			String sql ="select sd.doorId,sd.sd_id,dm.dlo_id ,sla.area_name,dm.gate_liscense_plate,dm.gate_container_no,dm.company_name,dm.warehouse_check_in_time as warehouse_check_in_operate_time,dm.status,dm.tractor_status,dd.occupancy_status ,dm.rel_type ,dd.rl_id as yc_id , syc.yc_no ,dd.rl_id as door_id " +
						   " from door_or_location_occupancy_main dm left join door_or_location_occupancy_details dd on dd.dlo_id=dm.dlo_id and dm.doubt_door_id=dd.rl_id "+
						   " left join storage_door sd on sd.sd_id=dd.rl_id and occupancy_type= " +OccupyTypeKey.DOOR+" left join storage_yard_control syc on syc.yc_id=dd.rl_id and occupancy_type= " +OccupyTypeKey.SPOT+
						   " left join storage_location_area sla on dd.zone_id=sla.area_id  where dm.ps_id= "+ps_id+"  and IFNULL(dm.status,dm.tractor_status) <> "+CheckInMainDocumentsStatusTypeKey.LEFT;
		    if(entryId>0){
				sql+=" and dm.dlo_id="+entryId;
			}
			if(!license_plate.equals("")){
				sql+=" and dm.gate_liscense_plate='"+license_plate+"' ";
			}
			if(!trailerNo.equals("")){
				sql+=" and dm.gate_container_no='"+trailerNo+"' ";
			}
			sql +=" group by dd.rl_id";
		
			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findDockMainMesByCondition"+e);
		}
	}*/
	public DBRow[] rtOccupiedSpot(long ps_id, long spot_area, long spotStatus, PageCtrl pc) throws Exception {
		try{
			String sql="select * from storage_yard_control syc left join door_or_location_occupancy_main dm on syc.associate_id=dm.dlo_id" +
					   " join storage_location_area sla on sla.area_id=syc.area_id"+
					   " where syc.patrol_time is null";
			if(ps_id>0){
				sql+=" and syc.ps_id= "+ps_id;
			}
			if(spotStatus>0){
				sql+=" and syc.yc_status= "+spotStatus;
			}
			if(spot_area>0){
				sql+=" and syc.area_id= "+spot_area;
			}
			sql+=" order by CAST(syc.yc_no as SIGNED) ";
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql, pc);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb rtOccupiedSpot"+e);
		}
	}
	public DBRow[] rtOccupiedDoor(long ps_id, long zone_area,long doorStatus, PageCtrl pc) throws Exception {
		try{
			String sql="select * from storage_door sd left join door_or_location_occupancy_main dm on sd.associate_id=dm.dlo_id  "+
                       " LEFT JOIN door_or_location_occupancy_details dd on dd.dlo_id=dm.dlo_id"+
                       " left join storage_location_area sla on sla.area_id=sd.area_id where sd.patrol_time is null ";
			if(ps_id>0){
				sql += " and sd.ps_id ="+ps_id;
			}
			if(doorStatus==1){
				sql += " and sd.occupied_status="+OccupyStatusTypeKey.RESERVERED;
			}
			if(doorStatus==2){
				sql += " and sd.occupied_status="+OccupyStatusTypeKey.OCUPIED;
			}
			if(doorStatus==3){
				sql += " and sd.occupied_status="+OccupyStatusTypeKey.RELEASED;
			}
			if(zone_area>0){
				sql+=" and sd.area_id= "+zone_area;
			}
			sql+=" group by sd.sd_id order by CAST(sd.doorId as SIGNED) ";
			return this.dbUtilAutoTran.selectMutliple(sql, pc);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb rtOccupiedSpot"+e);
		}
	}

	public DBRow[] findSpotOrDoorByCtnr(String ctnr,long ps_id) throws Exception {
		try{
			String sql="select sd.doorId,syc.yc_no,sd.area_id as zone_id,syc.area_id as spot_area,dm.gate_driver_liscense,dm.gate_liscense_plate,dm.dlo_id from door_or_location_occupancy_main dm " +
				//	   " left join door_or_location_occupancy_details dd on dd.dlo_id=dm.dlo_id and dd.occupancy_status in(1,2)" +
						" left join storage_door sd on sd.sd_id=dm.door_id left join storage_yard_control syc on syc.associate_id=dm.dlo_id"+
					    "  where dm.status <> "+CheckInMainDocumentsStatusTypeKey.LEFT +" and dm.ps_id="+ps_id;
			if(!("").equals(ctnr)){
				sql +=" and  dm.gate_container_no='"+ctnr+"'";
			}
//			if(entryId>0){
//				sql +=" and  dm.dlo_id="+entryId;
//			}
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findSpotOrDoorByCtnr"+e);
		}
	}

	public DBRow[] findLoadNumberByEntryId(long entryId) throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_details dd where dd.load_number is not null and dd.dlo_id="+entryId;

			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findLoadNumberByEntryId"+e);
		}
	}
	
	//根据子单据号查询子单据
	public DBRow selectDetailByDetailId(long detail_id)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_details where dlo_detail_id="+detail_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb selectDetailByDetailId"+e);
		}
	}

	public DBRow[] findZoneByTitleName(String title_name,long ps_id) throws Exception {
		try{
			String sql="select * from title t left join check_in_zone_with_title zt on t.title_id=zt.title_id  "+
                       "left join storage_location_area sla on sla.area_id=zt.area_id where t.title_name='"+title_name+"' and sla.area_psid="+ps_id;

			
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findZoneByTitleName"+e);
		}
	}

	//查找 打印标签title
	public DBRow[] findTitleByAdminId(long adminId)throws Exception{
		try{
			String sql="select * from title t join title_admin tad on t.title_id=tad.title_admin_title_id " +
					"where tad.title_admin_adid="+adminId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findTitle"+e);
		}
	}
	
	//根据 titleid 查询 title 与 模版关系表
	public DBRow[] selectTitleLableTemplate(long title_id)throws Exception{
		try{
			String sql="select * from title_lable_template tt join lable_template ltl " +
					"on tt.lable_template_id=ltl.lable_template_id where tt.title_id="+title_id;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb selectTitleLableTemplate"+e);
		}
	}

	public DBRow[] getSearchCheckInCTNRJSON(long ps_id, String trailerNo,int flag) throws Exception {
		try{
			String sql="";
			
			if(flag==1 && trailerNo.length()>5){  //1的修改 张睿
				sql += "select equipment_number from entry_equipment where equipment_number like '"+trailerNo+"%' and equipment_type="+EquipmentTypeKey.Container+" and ps_id="+ps_id+" and  equipment_status <> "+CheckInMainDocumentsStatusTypeKey.LEFT;//yard里	
			}
			else if(flag==2 && trailerNo.length()>5){
				sql += "SELECT DISTINCT gate_container_no FROM door_or_location_occupancy_main WHERE gate_container_no LIKE '"+trailerNo+"%' AND ps_id = "+ps_id+" AND gate_container_no NOT IN ("+
					   " SELECT DISTINCT gate_container_no	FROM door_or_location_occupancy_main WHERE STATUS <> "+CheckInMainDocumentsStatusTypeKey.LEFT +" AND ps_id = "+ps_id+")";//已离开
			}
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrXj getSearchCheckInCTNRJSON"+e);
		}
	}

	public DBRow[] getSearchCheckInLicensePlateJSON(long ps_id,
			String license_plate,int flag) throws Exception {
		try{
			String sql="";
			
			if(flag==1  && license_plate.length()>3){
				sql += "select gate_liscense_plate from door_or_location_occupancy_main where gate_liscense_plate like '"+license_plate+"%' and ps_id="+ps_id+" and tractor_status <> "+CheckInMainDocumentsStatusTypeKey.LEFT;//yard里	
			}
			else if(flag==2 && license_plate.length()>3){
				sql += "SELECT DISTINCT gate_liscense_plate FROM door_or_location_occupancy_main WHERE gate_liscense_plate LIKE '"+license_plate+"%' AND ps_id = "+ps_id+" AND gate_liscense_plate NOT IN ("+
						   " SELECT DISTINCT gate_liscense_plate FROM door_or_location_occupancy_main WHERE  IFNULL(status,tractor_status) <> "+CheckInMainDocumentsStatusTypeKey.LEFT+" AND ps_id = "+ps_id+")";//已离开
			}
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb getSearchCheckInLicensePlateJSON"+e);
		}
	}
	
	//停车位审核
	public DBRow[] verifySpot(long ps_id,PageCtrl pc) throws Exception {
		try{
			String sql="select  dm.*, syc.yc_no,sd.doorId  from door_or_location_occupancy_main dm  left join storage_door sd on dm.doubt_door_id=sd.sd_id " +
					" left join  storage_yard_control syc on syc.yc_id=dm.doubt_yc_id where (dm.doubt_door_id is not null or dm.doubt_yc_id is not null)  " +
					" and (IFNULL(dm.status,dm.tractor_status) <> "+CheckInMainDocumentsStatusTypeKey.LEFT+" or dm.tractor_status <> "+CheckInMainDocumentsStatusTypeKey.LEFT+") and dm.dlo_id not in (select entry_id from check_in_patrol_approve_details " +
					" where approve_status=1) and dm.ps_id= "+ps_id+" order by dm.dlo_id";
			
//			if(spotStatus>0){
//				sql+=" and syc.yc_status= "+spotStatus;
//			}
//			if(spot_area>0){
//				sql+=" and syc.area_id= "+spot_area;
//			}
//			sql+=" order by CAST(syc.yc_no as SIGNED) ";
			//System.out.println(sql);
			return this.dbUtilAutoTran.selectMutliple(sql,pc);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb verifySpot"+e);
		}
	}

	public long addCheckInPatrolApprove(DBRow row) throws Exception {
		try{
			 return this.dbUtilAutoTran.insertReturnId("check_in_patrol_approve", row);
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb addCheckInPatrolApprove"+e);
		 }
		
	}

	public long addCheckInPatrolApproveDetails(DBRow row) throws Exception {
		try{
			 return this.dbUtilAutoTran.insertReturnId("check_in_patrol_approve_details", row);
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb addCheckInPatrolApproveDetails"+e);
		 }
	}

	public DBRow[] findCheckInPatrolApprove(long ps_id,long approve_status,PageCtrl pc) throws Exception {
		try{
			String sql ="select pa.* ,ad.employe_name as poster,ad1.employe_name as verifier from check_in_patrol_approve pa join admin ad on pa.commit_adid=ad.adid left join admin ad1 on  pa.approve_adid=ad1.adid  where pa.ps_id="+ps_id+" order by pa.post_date desc";
			if(approve_status>0){
				sql +=" and approve_status="+approve_status;
			}
			return this.dbUtilAutoTran.selectMutliple(sql, pc);
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb findCheckInPatrolApprove"+e);
		 }
	}

	public DBRow[] findCheckInPatrolApproveDetails(long ps_id, long pa_id,
			PageCtrl pc) throws Exception {
		try{
			String sql ="select * from check_in_patrol_approve_details pad left join entry_equipment ee on pad.equipment_id=ee.equipment_id  where pad.ps_id="+ps_id;
				//	+" and pa_id="+pa_id;
			return this.dbUtilAutoTran.selectMutliple(sql, pc);
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb findCheckInPatrolApproveDetails"+e);
		 }
	}
	

	public DBRow findCheckInPatrolApproveDetailsByPadId(long pad_id) throws Exception {
		try{
			String sql ="select * from check_in_patrol_approve_details where  pad_id="+pad_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb findCheckInPatrolApproveDetailsByPadId"+e);
		 }
	}

	public long modCheckInPatrolApproveDetails(long pad_id, DBRow note) throws Exception {
		try{
			 return dbUtilAutoTran.update("where pad_id="+pad_id, "check_in_patrol_approve_details", note);
		}catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb modCheckIn"+e);
		}
		
	}
	public int getApproveCountByPaId(long pa_id)
			throws Exception
		{
			try
			{
				
				DBRow para = new DBRow();
				para.add("pa_id",pa_id);
				
				String sql = "select count(pad_id) c from  check_in_patrol_approve_details where pa_id=? and approve_status=2";
				DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
				
				return(rowo.get("c", 0));
			}
			catch (Exception e)
			{
				throw new Exception("FloorCheckInMgrZwb getApproveCountByPaId error:" + e);
			}
		}
		
		public int getNotApproveCountByPaId(long pa_id)
			throws Exception
		{
			try
			{
				
				DBRow para = new DBRow();
				para.add("pa_id",pa_id);
				
				String sql = "select count(pad_id) c from  check_in_patrol_approve_details where pa_id=? and approve_status=1";
				DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
				
				return(rowo.get("c", 0));
			}
			catch (Exception e)
			{
				throw new Exception("FloorCheckInMgrZwb getNotApproveCountByPaId:" + e);
			}
		}

		public long modCheckInPatrolApprove(long pa_id, DBRow approve) throws Exception {
			try{
				 return dbUtilAutoTran.update("where pa_id="+pa_id, "check_in_patrol_approve", approve);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrZwb modCheckInPatrolApprove"+e);
			}
			
		}

	public DBRow findLableByName(DBRow lable,String masterBolformat, String Bolformat,String AccountID,long number_type) throws Exception{
		try{
			String customerId=lable.getString("CUSTOMERID").toUpperCase();

			if(customerId.equals("SEIDIG0007")){
				masterBolformat="generic_not_izio";
				if(number_type==10){		
					Bolformat="Vics2";
				}else{
					Bolformat="Vics2 Order";
				}
			}else{
				masterBolformat="Generic";
				if(number_type==10){		
					Bolformat="Vics4";
				}else{
					Bolformat="Vics4 Order";
				}
				
			}
			
			lable.add("MasterBOLFormat","Generic");
			lable.add("BOLFormat",Bolformat);
		
			String sql="select * from lable_template where template_name = '"+masterBolformat+"' and lable_type=10";
			String BOLsql="select * from lable_template where template_name = '"+Bolformat+"' and lable_type=9";
//			System.out.println(sql);
			lable.add("MasterBOLFormatBAK", this.dbUtilAutoTran.selectSingle(sql));
			lable.add("BOLFormatBAK", this.dbUtilAutoTran.selectSingle(BOLsql));
			
			return lable;
			
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findLableByName"+e);
		}
	}

	public long createEntryId(DBRow row) throws Exception {
		try{
    		return this.dbUtilAutoTran.insertReturnId("door_or_location_occupancy_main", row);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb createEntryId"+e);
    	}
	}
	
	public long modStorageDoor(long sd_id,DBRow row) throws Exception {
		try{
			 return dbUtilAutoTran.update("where sd_id="+sd_id, "storage_door", row);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb modStorageDoor"+e);
    	}
	}
	

	public long modStorageParking(long yc_id,DBRow row) throws Exception {
		try{
			 return dbUtilAutoTran.update("where yc_id="+yc_id, "storage_yard_control", row);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb modStorageParking"+e);
    	}
	}
	public long modStorageArea(long area_id,DBRow row) throws Exception {
		try{
			 return dbUtilAutoTran.update("where area_id="+area_id, "storage_location_area", row);
    	}catch(Exception e){
    		throw new Exception("FloorCheckInMgrZwb modStorageArea"+e);
    	}
	}
	public long AgainPatrolSpot(long ps_id, long area_id, DBRow row) throws Exception {
		try{
			 String conditon = "";
			 if(area_id>0){
				 conditon="where area_id="+area_id;
			 }else{
				 conditon="where ps_id="+ps_id;
			 }
			 return dbUtilAutoTran.update(conditon, "storage_yard_control", row);
	   	}catch(Exception e){
	   		throw new Exception("FloorCheckInMgrZwb AgainPatrolSpot"+e);
	   	}
	}

	public long AgainPatrolDock(long ps_id, long area_id, DBRow row) throws Exception {
		try{
			String conditon = "";
			 if(area_id>0){
				 conditon="where area_id="+area_id;
			 }else{
				 conditon="where ps_id="+ps_id;
			 }
			 return dbUtilAutoTran.update(conditon, "storage_door", row);
	   	}catch(Exception e){
	   		throw new Exception("FloorCheckInMgrZwb AgainPatrolDock"+e);
	   	}
	}

	public long ClearAreaPatrolDoneTime(long ps_id,long area_type ,long area_id, DBRow row) throws Exception {
		try{
			 String conditon = "";
			 if(area_id>0){
				 conditon="where area_id="+area_id;
			 }else{
				 conditon="where area_psid="+ps_id+" and area_type="+area_type;
			 }
			 return dbUtilAutoTran.update(conditon, "storage_location_area", row);
	   	}catch(Exception e){
	   		throw new Exception("FloorCheckInMgrZwb ClearAreaPatrolDoneTime"+e);
	   	}
	}
	public int findStorageDoorByAreaId(long area_id) throws Exception {
		try{
			String sql ="select count(sd_id)c from storage_door where area_id="+area_id+" and patrol_time is null";
			DBRow row = this.dbUtilAutoTran.selectSingle(sql);
			int count = row.get("c", 0);
			return count;
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb findStorageDoorByAreaId"+e);
		 }
	}
	public int findStorageParkingByAreaId(long area_id) throws Exception {
		try{
			String sql ="select count(yc_id)c from storage_yard_control where area_id="+area_id+" and patrol_time is null";
			DBRow row = this.dbUtilAutoTran.selectSingle(sql);
			int count = row.get("c", 0);
			return count;
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb findStorageParkingByAreaId"+e);
		 }
	}
	public int findStoragePatrolTimeByPsId(long ps_id) throws Exception {
		try{
			String sql ="select count(area_id)c from storage_location_area where area_psid="+ps_id+" and area_type in(2,3) and patrol_time is null "+
						" and (area_id in(SELECT DISTINCT(area_id) from storage_door where ps_id="+ps_id+" and area_id !=0) or "+
						" area_id in(SELECT DISTINCT(area_id) from storage_yard_control where ps_id="+ps_id+" and area_id !=0))";
			DBRow row = this.dbUtilAutoTran.selectSingle(sql);
			int count = row.get("c", 0);
			return count;
		 }catch(Exception e){
			 throw new Exception("FloorCheckInMgrZwb findStoragePatrolTimeByPsId"+e);
		 }
	}
	
	public DBRow[] queryContainerNoByPhoneNumber(long ps_id, String containerNo) throws Exception{
		try{
			String sql = "select DISTINCT gate_container_no  from door_or_location_occupancy_main where phone_gate_container_no like '"+containerNo+"%' and status <> "+CheckInMainDocumentsStatusTypeKey.LEFT+" and ps_id =" + ps_id; 
 			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
    		throw new Exception("FloorCheckInMgrZwb queryContainerNoByPhoneNumber"+e);
		}
	}
	public DBRow[] queryLicensePlateByPhoneNumber(long ps_id, String licensePlate) throws Exception{
		try{
			String sql = "select DISTINCT gate_liscense_plate  from door_or_location_occupancy_main where phone_gate_liscense_plate like '"+licensePlate+"%' and tractor_status <> "+CheckInMainDocumentsStatusTypeKey.LEFT+" and ps_id =" + ps_id; 
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
    		throw new Exception("FloorCheckInMgrZwb queryLicensePlateByPhoneNumber"+e);
		}
	}

	//根据DropOffkey查询停车位
		public DBRow[] selectSprotByType(long ps_id, int dropOffkey,int type) throws Exception{
			
			try{
				String sql="select sla.area_id,sla.area_name,sla.area_psid,sla.area_type,sla.area_subtype from storage_location_area sla where area_psid="+ps_id;
				if(dropOffkey==3|dropOffkey==2){
					sql+=" and area_subtype in (3,4)";
				}else if(dropOffkey==1){
					sql+=" and area_subtype in(2)";
				}
				sql+=" and area_type=3";
				
				if(type==1){
					sql+=" order by sla.area_subtype desc";
				}else if(type==2){
					sql+=" order by sla.area_subtype ";
				}
				//System.out.println(sql);
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrZwb selectSprotByType"+e);
			}
			
		}
		
		
		public DBRow[] getYardEntry() throws Exception{
			try{
				String sql = "select * from door_or_location_occupancy_main where status not in (5,10) or tractor_status not in (5,10)" ;
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrZwb getYardEntry"+e);
			}
		}
	
		/**
		 * 得到在yard 里面最大的EntryId
		 * @return
		 * @throws Exception
		 */
		public long getMaxEntryIdInYard() throws Exception{
			try{
				String sql = "select max(dlo_id) as max_dlo_id from door_or_location_occupancy_main where IFNULL(status,tractor_status) <> "+CheckInMainDocumentsStatusTypeKey.LEFT ;
				DBRow returnRow = dbUtilAutoTran.selectSingle(sql);
				if(returnRow != null){
					return returnRow.get("max_dlo_id", 0l);
				}
				return 0l;
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrZwb getMaxEntryIdInYard"+e);
			}	
		}
	
		
		public DBRow getEntryIdById(long entry_id) throws Exception {
			try{
				String sql = "select * from door_or_location_occupancy_main  where dlo_id=" + entry_id ; 
				return dbUtilAutoTran.selectSingle(sql);
			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb getEntryIdById"+e);
			}
		}
		 /**
		  * 查询子单据 会包含有门的数据
		  * 子单据都是包含门的数据
		  * 张睿 添加门的条件 2014-11-18
		  * @param entry_id
		  * @return
		  * @throws Exception
		  */
		public DBRow[] getEntryDetailHasDoor(long entry_id) throws Exception{
			try{
				StringBuilder sql = new StringBuilder("SELECT dlod.*, sd.doorId FROM ");
				sql.append(" door_or_location_occupancy_details AS dlod ");
				sql.append(" LEFT JOIN storage_door AS sd ON sd.sd_id = dlod.rl_id ");
				sql.append(" where ").append("  dlod.occupancy_type="+OccupyTypeKey.DOOR+" and dlod.dlo_id ="+entry_id);
				return dbUtilAutoTran.selectMutliple(sql.toString());
			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb getEntryDetailHasDoor"+e);
			}
		}
		/**
		 * 通过LoadNumber 去查询主单据信息，有可能有多条的情况
		 * @param loadNumber
		 * @return
		 * @throws Exception
		 */
		public DBRow[] getEntryByLoadNumber(String loadNumber) throws Exception{
			try{
				String sql = "select  DISTINCT dm.dlo_id ,dm.*,dd.rl_id , dd.number_status ,dm.out_seal from door_or_location_occupancy_details as dd LEFT JOIN door_or_location_occupancy_main as dm  on dd.dlo_id = dm.dlo_id where dd.load_number ='"+loadNumber+"' order by dm.dlo_id desc ";
		//	 System.out.println(sql);
				return dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb getEntryByLoadNumber"+e);
			}
		}
		public void updateLoadDetailBy(long entry_id , String load_number,DBRow updateRow) throws Exception{
			try{
				dbUtilAutoTran.update(" where dlo_id="+ entry_id + " and load_number='"+load_number+"'", "door_or_location_occupancy_details", updateRow);
			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb updateLoadDetailBy"+e);
			}
		}

		
		/**
		 * 在yard里,没到达门
		 * @return
		 * @throws Exception
		 */
		public DBRow[] selectGateCheckIn(long ps_id,PageCtrl pc) throws Exception{
			try{
				String sql = "select dm.dlo_id,dm.gate_liscense_plate,syc.yc_no ,dm.gate_check_in_time as gate_check_in_operate_time,dm.isLive,sd.doorId,dm.rel_type from door_or_location_occupancy_main dm 	"+		       
							 " LEFT JOIN storage_yard_control syc ON dm.yc_id = syc.yc_id LEFT JOIN door_or_location_occupancy_details dd on dm.dlo_id=dd.dlo_id LEFT JOIN storage_door sd on dd.rl_id=sd.sd_id where gate_check_in_time is not null "+
							 " and warehouse_check_in_time is null  and window_check_in_time is null AND check_out_time IS NULL AND (sd.doorId is not null or syc.yc_no is not null) and patrol_last_update_time is null" +
							 " and (`status` = "+CheckInMainDocumentsStatusTypeKey.UNPROCESS+" or tractor_status="+CheckInMainDocumentsStatusTypeKey.UNPROCESS+") and dm.ps_id="+ps_id+" and dm.isLive="+CheckInLiveLoadOrDropOffKey.LIVE+" order by gate_check_in_time";

				return dbUtilAutoTran.selectMutliple(sql, pc);
			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb selectGateCheckIn"+e);
			}
		}
		
		
		/**
		 * 在yard里,没到达门
		 * @return
		 * @throws Exception
		 */
		public DBRow[] inYardNoDoorTask(long ps_id,PageCtrl pc)
			throws Exception
		{
			try
			{
				String sql = "select DISTINCT dlo.dlo_id,dlo.gate_check_in_time,dlo.gate_liscense_plate,dlo.rel_type from door_or_location_occupancy_main as dlo "
							+"left join door_or_location_occupancy_details as dlod on dlo.dlo_id = dlod.dlo_detail_id and dlo_detail_id is NULL "
							+"where dlo.gate_check_in_time is not null and dlo.check_out_time is null and ps_id = ? and dlo.isLive in ("+CheckInLiveLoadOrDropOffKey.LIVE+","+CheckInLiveLoadOrDropOffKey.DROP+","+CheckInLiveLoadOrDropOffKey.SWAP+","+CheckInLiveLoadOrDropOffKey.PICK_UP+") "
							+"union "
							+"select DISTINCT dlo.dlo_id,dlo.gate_check_in_time,dlo.gate_liscense_plate,dlo.rel_type from door_or_location_occupancy_main as dlo " 
							+"left JOIN storage_door as sd on sd.associate_id = dlo.dlo_id and sd.associate_type = "+ModuleKey.CHECK_IN+" and sd.sd_id is null "
							+"where dlo.gate_check_in_time is not null and dlo.check_out_time is null and ps_id = ? and dlo.isLive in ("+CheckInLiveLoadOrDropOffKey.LIVE+","+CheckInLiveLoadOrDropOffKey.DROP+","+CheckInLiveLoadOrDropOffKey.SWAP+","+CheckInLiveLoadOrDropOffKey.PICK_UP+") ";
				
				DBRow para = new DBRow();
				para.add("1_ps_id",ps_id);
				para.add("2_ps_id",ps_id);

				return dbUtilAutoTran.selectPreMutliple(sql,para, pc);
			}
			catch (Exception e) 
			{
				 throw new Exception("FloorCheckInMgrZwb selectGateCheckIn"+e);
			}
		}
		
		


//根据停车位查询主表信息
/**	public DBRow findMainBySpot(long yc_id,String ctnr,long infoId)throws Exception{
		try{
			String sql="select * from door_or_location_occupancy_main where yc_id="+yc_id;
			if(!StrUtil.isNull(ctnr)){
				sql += " AND gate_container_no = '"+ctnr+"'";
			}else{
				sql+=" and dlo_id = "+infoId;
			}
			sql += " order by create_time desc LIMIT 0,1";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorCheckInMgrZwb findMainBySpot"+e);
		}
	}*/

	/**
		 * 在yard里,到达门
		 * @return
		 * @throws Exception
		 */
		public DBRow[] selectWareHouseCheckIn(long ps_id,PageCtrl pc) throws Exception{
			try{
				String sql = "select allUnProcessing.dlo_id,gate_liscense_plate,window_check_in_time,unprocessingCount,allCount,doorId from ( "
							+"	select " 
							+"	  dlo_id,gate_liscense_plate,window_check_in_time,"
							+"	  MAX(CASE number_status WHEN 'unprocessing' THEN status_count END ) unprocessingCount,"
							+"	  MAX(CASE number_status WHEN 'all' THEN status_count END ) allCount " 
							+"	from ( "
							+"	select dlod.dlo_id,dlo.gate_liscense_plate,dlo.window_check_in_time,count(dlo_detail_id) as status_count,'unprocessing' as number_status from door_or_location_occupancy_details as dlod "
							+"	join door_or_location_occupancy_main as dlo on dlod.dlo_id = dlo.dlo_id and dlo.ps_id = "+ps_id+" "
							+"	where number_status = "+CheckInChildDocumentsStatusTypeKey.UNPROCESS+" and dlo.gate_check_in_time is not null and dlo.window_check_in_time is not NULL and dlo.check_out_time is null and dlo.isLive = "+CheckInLiveLoadOrDropOffKey.LIVE+" "
							+"	GROUP BY dlo_id "
							+"	union ALL "
							+"	select dlod.dlo_id,dlo.gate_liscense_plate,dlo.window_check_in_time,count(dlo_detail_id) as status_count,'all' as number_status from door_or_location_occupancy_details as dlod "
							+"	join door_or_location_occupancy_main as dlo on dlod.dlo_id = dlo.dlo_id and dlo.ps_id ="+ps_id+ " "
							+"	where dlo.gate_check_in_time is not null and dlo.window_check_in_time is not NULL and dlo.check_out_time is null " 
							+"	GROUP BY dlo_id "
							+"	)as allStatusView GROUP BY dlo_id HAVING unprocessingCount = allCount"
							+"	)as allUnProcessing "
							+"	left join ("
							+"	SELECT "
							+"			dd.dlo_id,GROUP_CONCAT(DISTINCT sd.doorId)doorId "
							+"		FROM "
							+"			door_or_location_occupancy_details dd "
							+"			join storage_door sd on dd.rl_id = sd.sd_id "
							+"			join door_or_location_occupancy_main dlo on dd.dlo_id = dlo.dlo_id and dlo.ps_id = "+ps_id+" "
							+"		GROUP BY dlo_id "
							+"	) as doorList on allUnProcessing.dlo_id = doorList.dlo_id";
				return dbUtilAutoTran.selectMutliple(sql, pc);
			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb selectWareHouseCheckIn"+e);
			}
		}

		/**
		 * 在yard里,到达门并处理单据
		 * @return
		 * @throws Exception
		 */
		public DBRow[] selectWareHouseNumberProcessing(long ps_id,PageCtrl pc) throws Exception{
			try{
				String sql = "SELECT DISTINCT dm.dlo_id , dd.flag_help, sd.doorId ,dd.number  ,ad.employe_name as account,dd.handle_time,dd.dlo_detail_id ,dd.number_type FROM door_or_location_occupancy_main dm "+ 
						     " join door_or_location_occupancy_details dd  on dm.dlo_id=dd.dlo_id "+
						     " left join storage_door sd on sd.sd_id=dd.rl_id "+
						     " left join `schedule` s on s.associate_id=dd.dlo_detail_id "
						     + "left join admin ad on ad.adid=s.assign_user_id "+
						     " WHERE dm.`status` <> "+CheckInMainDocumentsStatusTypeKey.LEFT+" and dm.ps_id = "+ps_id+"  and dd.number_status =2 and (s.associate_process = 54 or s.associate_process = 53) "+
						     " and ORDER BY dd.handle_time";
				return dbUtilAutoTran.selectMutliple(sql, pc);
			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb selectWareHouseNumberProcessing"+e);
			} 
		}
		/**
		 * spot占用情况
		 * @return
		 * @throws Exception
		 */
		public DBRow[] selectSpotSituation(long ps_id,PageCtrl pc) throws Exception{
			try{
				String sql = "select syc.yc_no,dm.dlo_id,dm.gate_liscense_plate,dm.gate_container_no,dm.gate_container_no,dm.gate_check_in_time as gate_check_in_operate_time "+
                             " from storage_yard_control syc  join door_or_location_occupancy_main dm on syc.associate_id=dm.dlo_id "+
                             " where syc.yc_status=2 and syc.ps_id="+ps_id+" order by CAST(syc.yc_no as SIGNED)";
				return dbUtilAutoTran.selectMutliple(sql, pc);
			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb selectSpotSituation"+e);
			}
		}
		/**
		 * dock占用情况
		 * @return
		 * @throws Exception
		 */
		public DBRow[] selectDockSituation(long ps_id,PageCtrl pc) throws Exception{
			try{
				String sql = "select  DISTINCT sd.doorId,dd.dlo_id,h.gate_liscense_plate,h.gate_container_no,h.warehouse_check_in_time as warehouse_check_in_operate_time, "+
							" h.window_check_in_time as window_check_in_operate_time,h.gate_check_in_time as gate_check_in_operate_time ,h.number,h.num_type from storage_door sd  join door_or_location_occupancy_details dd "+
							" on sd.sd_id=dd.rl_id  join (select GROUP_CONCAT(dd.number) number ,GROUP_CONCAT(dd.number_type) num_type ,dd.dlo_id ,dm.gate_container_no, "+
							" dm.gate_liscense_plate,dm.gate_check_in_time,dm.warehouse_check_in_time,dm.window_check_in_time from "+
							" door_or_location_occupancy_details dd ,door_or_location_occupancy_main dm where dm.dlo_id=dd.dlo_id  group by dd.dlo_id) h "+
							" on dd.dlo_id=h.dlo_id  where   dd.occupancy_status in(1,2) and sd.ps_id="+ps_id+" order by CAST(sd.doorId as SIGNED)";
				return dbUtilAutoTran.selectMutliple(sql, pc);
			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb selectDockSituation"+e);
			}
		}
		/**
		 * spot占用情况
		 * @return
		 * @throws Exception
		 */
		public DBRow[] selectSpotSituationReport(long ps_id) throws Exception{
			try{
				String sql = "SELECT main.dlo_id,yard.yc_id,yard.yc_no,yard.yc_status,main.gate_liscense_plate,main.gate_check_in_time,main.gate_container_no , detail.number_type, detail.number, detail.company_id FROM storage_yard_control yard "+
							" LEFT JOIN door_or_location_occupancy_main main ON yard.associate_id = main.dlo_id LEFT JOIN door_or_location_occupancy_details detail on main.dlo_id = detail.dlo_id "+
						    "  WHERE yard.yc_status = 2 and yard.ps_id="+ps_id+" order by CAST(yard.yc_no as SIGNED)";
				return dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb selectSpotSituationReport"+e);
			}
		}
		/**
		 * dock占用情况
		 * @return
		 * @throws Exception
		 */
		public DBRow[] selectDockSituationReport(long ps_id) throws Exception{
			try{
				String sql = "SELECT  main.dlo_id,door.sd_id,door.doorId,main.gate_liscense_plate,main.warehouse_check_in_time,main.gate_container_no,main.reL_type, detail.number_type, detail.number, detail.company_id FROM storage_door door "+
							" LEFT JOIN door_or_location_occupancy_details detail ON door.sd_id = detail.rl_id "+
							" LEFT JOIN door_or_location_occupancy_main main on main.dlo_id = detail.dlo_id "+
							" WHERE detail.occupancy_status in(1,2) and door.ps_id="+ps_id+" order by CAST(door.doorId as SIGNED)";
				return dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb selectDockSituationReport"+e);
			}
		}
		/**
		 * load完的数量
		 * @return
		 * @throws Exception
		 */
		public int loadDoneConut(long detail_id) throws Exception{
			try{
				
				
				DBRow result = dbUtilAutoTran.selectSingle("select sum(pc.wms_pallet_type_count) load_count from wms_load_order_pallet_type_count pc  where dlo_detail_id="+ detail_id);
				int count = 0 ;
				if(result != null){
					count = result.get("load_count", 0);
				}
				return count ;
			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb loadDoneConut"+e);
			}
		}
		/**
		 * load总数
		 * @return
		 * @throws Exception
		 */
		public int totalConut(long detail_id) throws Exception{
			try{
				
				
				DBRow result = dbUtilAutoTran.selectSingle("select sum(wlo.pallets) total_count from wms_load_order wlo where wlo.dlo_detail_id ="+detail_id);
				int count = 0 ;
				if(result != null){
					count = result.get("total_count", 0);
				}
				return count ;
			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb totalConut"+e);
			}
		}
		public DBRow[] selectLoadBar() 
			throws Exception
		{
			try
			{
				String sql = "select * from load_bar";
				return dbUtilAutoTran.selectMutliple(sql);
			}
			catch (Exception e) 
			{
				 throw new Exception("FloorCheckInMgrZwb selectLoadBar"+e);
			}
		}
		/**
		 * 
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findNearestDoor() 
				throws Exception
			{
				try
				{
					String sql = "select * from load_bar";
					return dbUtilAutoTran.selectMutliple(sql);
				}
				catch (Exception e) 
				{
					 throw new Exception("FloorCheckInMgrZwb selectLoadBar"+e);
				}
		}
		/**
		 * 仓库优先级查询local
		 * @return
		 * @throws Exception
		 */
		public DBRow[] storageSearchLocal(long ps_id) 
				throws Exception
		{
				try
				{
					String sql = "select * from config_search_warehouse_location where ps_id="+ps_id+" and search_location="+1;
					return dbUtilAutoTran.selectMutliple(sql);
				}
				catch (Exception e) 
				{
					 throw new Exception("FloorCheckInMgrZwb storageSearchPriority"+e);
				}
		}
		/**
		 * 仓库优先级查询wms
		 * @return
		 * @throws Exception
		 */
		public DBRow[] storageSearchWms(long ps_id) 
				throws Exception
		{
				try
				{
					String sql = "select * from config_search_warehouse_location where ps_id="+ps_id+" and search_location="+2;
					return dbUtilAutoTran.selectMutliple(sql);
				}
				catch (Exception e) 
				{
					 throw new Exception("FloorCheckInMgrZwb storageSearchPriority"+e);
				}
		}

		/**
		 * wms仓库查询
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findWmsStorage(long ps_id) 
				throws Exception
		{
				try
				{
					String sql = "select * from config_search_warehouse_location where ps_id="+ps_id+" and search_location="+2;
					return dbUtilAutoTran.selectMutliple(sql);
				}
				catch (Exception e) 
				{
					 throw new Exception("FloorCheckInMgrZwb storageSearchPriority"+e);
				}
		}
		/**
		 * 报表loadbar
		 * @return
		 * @throws Exception
		 */

		public DBRow[] getLoadBarReport(String startTime,String endTime,long ps_id) 

				throws Exception
		{
				try
				{

					String sql = "SELECT main.out_seal,main.dlo_id, main.company_name, main.load_bar_id, bar.load_bar_name, main.check_out_time ,detail.number_type,detail.number,detail.customer_id,detail.company_id ,detail.freight_term "
							+ " FROM door_or_location_occupancy_main main  JOIN load_bar bar  ON main.load_bar_id = bar.load_bar_id LEFT JOIN door_or_location_occupancy_details detail on main.dlo_id = detail.dlo_id "
							+  " WHERE main.check_out_time > '"+startTime+"' AND main.check_out_time < '"+endTime+"' and main.ps_id="+ps_id+" order by main.dlo_id";

					return dbUtilAutoTran.selectMutliple(sql);
				}
				catch (Exception e) 
				{
					 throw new Exception("FloorCheckInMgrZwb getLoadBarOrSealReport"+e);
				}
		}
		/**
		 * 报表seal
		 * @return
		 * @throws Exception
		 */

		public DBRow[] getSealReport(String startTime,String endTime,long ps_id) 

				throws Exception
		{
				try
				{

					String sql = "SELECT main.out_seal,main.dlo_id, main.company_name, main.check_out_time,detail.number_type,detail.number,detail.customer_id,detail.company_id FROM door_or_location_occupancy_main main   "
							+    " LEFT JOIN door_or_location_occupancy_details detail on main.dlo_id = detail.dlo_id WHERE main.check_out_time > '"+startTime+"' AND main.check_out_time < '"+endTime+"' and main.ps_id="+ps_id+" and  main.out_seal is not null and main.out_seal!='NA' and main.out_seal!='' order by main.dlo_id";

					return dbUtilAutoTran.selectMutliple(sql);
				}
				catch (Exception e) 
				{
					 throw new Exception("FloorCheckInMgrZwb getLoadBarOrSealReport"+e);
				}
		}
		/**
		 * 报表task
		 * @return
		 * @throws Exception
		 */
		public DBRow[] getTaskReportByCheckOutTime(String startTime,String endTime,long ps_id) 
				throws Exception
		{
				try
				{

					String sql = "SELECT details.dlo_id, details.number, details.number_type, a.employe_name, door.doorId, details.customer_id, details.company_id, details.supplier_id, details.finish_time,details.handle_time "
							+ " FROM door_or_location_occupancy_details details LEFT JOIN `schedule` sc ON sc.associate_id = details.dlo_detail_id LEFT JOIN admin a ON a.adid = sc.assign_user_id "
							+ " LEFT JOIN storage_door door ON door.sd_id = details.rl_id left join door_or_location_occupancy_main dm on details.dlo_id=dm.dlo_id "
							+ " WHERE (sc.associate_process = 54 or sc.associate_process = 53 )AND details.finish_time >= '"+startTime+"' AND details.finish_time <= '"+endTime+"' and dm.ps_id="+ps_id;
							   

			
					return dbUtilAutoTran.selectMutliple(sql);
				}
				catch (Exception e) 
				{
					 throw new Exception("FloorCheckInMgrZwb getTaskReportByCheckOutTime"+e);
				}
		}
		/**
		 * 报表task
		 * @return
		 * @throws Exception
		 */

		public DBRow[] getLoadNoByGateCheckInTime(String startTime,String endTime,long ps_id) 

				throws Exception
		{
				try
				{
					String sql = "SELECT detail.dlo_id,detail.customer_id,detail.number,detail.number_type,main.company_name,detail.supplier_id,product.title,main.gate_check_in_time ,detail.handle_time FROM "+
								 " door_or_location_occupancy_details detail LEFT JOIN door_or_location_occupancy_main main ON detail.dlo_id = main.dlo_id INNER JOIN product_storage_catalog product ON main.ps_id = product.id "+
		                         " WHERE main.gate_check_in_time >= '"+startTime+"' AND main.gate_check_in_time <= '"+endTime+" 23:59:59 ' and   detail.number!='' and main.ps_id="+ps_id ;

		   
					return dbUtilAutoTran.selectMutliple(sql);
				}
				catch (Exception e) 
				{
					 throw new Exception("FloorCheckInMgrZwb getLoadNoByGateCheckInTime"+e);
				}
		}
		/**
		 * 通过ContainerNo，查询container信息
		 * @param containerNo
		 * @param pc
		 * @return
		 * @throws Exception
		 */
		public DBRow findContainerInfoByNo(String containerNo,String poNo,int[] status)throws Exception
		{
			try
			{
				String date=DateUtil.NowStr();
				String sql = "select * from import_container_info where import_time<='"+date+"' ";
				if(!StrUtil.isBlank(containerNo))
				{
					sql += " and container_no = '"+ containerNo+"'";
				}
				if(!StrUtil.isBlank(poNo))
				{
					sql += " and po_no = '"+ poNo+"'";
				}
				if(status != null && status.length > 0)
				{
					sql += " and (status = "+status[0];
					for (int i = 1; i < status.length; i++) 
					{
						sql += " or status = "+status[i];
					}
					sql += ")";
				}
				sql +=" order by import_time desc  limit 1";
				return dbUtilAutoTran.selectSingle(sql);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZyj findContainerInfoByNo"+e);
			}
		}

		
		
		/**
		 * 添加导入seal信息
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public long addImportSeals(DBRow row)throws Exception
		{
			try
			{
				return dbUtilAutoTran.insertReturnId("import_seal", row);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb addImportSeals"+e);
			}
		}
		/**
		 * 未使用的seal退回
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public void unusedSealsReturn(long seal_id ,DBRow updateRow) throws Exception{
			try{
				dbUtilAutoTran.update(" where seal_id="+ seal_id, "import_seal", updateRow);
			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb updateLoadDetailBy"+e);
			}
		}
		/**
		 * 未使用的seal退回
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public void updateSealUsed(String seal_no ,DBRow updateRow) throws Exception{
			try{

				dbUtilAutoTran.update(" where seal_no='"+ seal_no+"' and seal_status=2", "import_seal", updateRow);


			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb updateSealUsed"+e);
			}
		}
		/**
		 * 查询seal信息
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public DBRow[] selectSealsByTime(String start_time,String end_time,long ps_id)throws Exception
		{
			try
			{
				String sql ="select * from import_seal ims left join admin ad on ims.import_user=ad.adid left join product_storage_catalog slc on slc.id=ims.ps_id where 1=1";
/**				if(!StrUtil.isBlank(start_time))
				{
					sql += " and import_time >= '" + start_time + " 00:00:00'";
				}
				if(!StrUtil.isBlank(end_time))
				{
					sql += " and import_time <= '" + end_time + " 23:59:59'";
				}*/
				if(ps_id>0){
					sql +=" and ps_id="+ps_id;
				}
				
				sql +=" order by seal_status desc,seal_no asc";
				return dbUtilAutoTran.selectMutliple(sql);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb selectSealsByTime"+e);
			}
		}
		/**
		 * 查询seal信息
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public DBRow[] selectBySealNo(String seal_no,long ps_id)throws Exception
		{
			try
			{
				String sql ="select * from import_seal where seal_no='"+seal_no+"' and ps_id="+ps_id;
				
				return dbUtilAutoTran.selectMutliple(sql);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb selectBySealNo"+e);
			}
		}
		/**
		 *releasedDoor 更具主单据号码 + door_id去更新  ,那么就是把这个门下面的所有的子单据都是释放
		 * @param entry_id
		 * @param door_id
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年9月19日
		 */
		public int releasedDoor(long entry_id , long door_id ,DBRow updateRow) throws Exception{
			try{
				return dbUtilAutoTran.update("where dlo_id="+entry_id+" and rl_id="+door_id +" and occupancy_type="+OccupyTypeKey.DOOR, "door_or_location_occupancy_details", updateRow);
			}catch(Exception e){
				 throw new Exception("FloorCheckInMgrZyj releasedDoor"+e);
			}
			
		}  
		
		/**
		 * DockClose但checkout
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public DBRow[] selectDockCloseNoCheckOut(long ps_id, PageCtrl pc)throws Exception
		{
			try
			{
				String sql ="select dm.dlo_id,dm.gate_liscense_plate,dm.gate_container_no from door_or_location_occupancy_main dm where `status`= 4 and "
						  + " (dm.check_out_time!='' or dm.check_out_time is null) and ps_id="+ps_id+" order by dm.dlo_id";
				
				return dbUtilAutoTran.selectMutliple(sql,pc);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb selectDockCloseNoCheckOut"+e);
			}
		}
		/**
		 * DockClose但checkout
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public DBRow selectNearestDoor(long ps_id,long title_id)throws Exception
		{
			try
			{
				String sql ="select r_nearest_door("+ps_id+","+title_id+") as door";
				
				return dbUtilAutoTran.selectSingle(sql);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb selectNearestDoor"+e);
			}
		}

		/**
		 * 返回titleID
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public DBRow selectTitleIdByTitleName(String title_name)throws Exception
		{
			try
			{
				String sql ="select * from title where title_name='"+title_name+"'";
				
				return dbUtilAutoTran.selectSingle(sql);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb selectTitleIdByTitleName"+e);
			}
		}
		
		/**
		 * pallet Inventory
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public DBRow[]  getAllPalletInventory(long ps_id,int invenStatus,int title_id,PageCtrl pc)throws Exception
		{
			try
			{
				String sql = "SELECT pallet.pallet_id, pallet.pallet_type, pallet.pallet_count, pallet.damaged_count, pallet.ps_id, `storage`.title "
						+ " FROM pallet_inventroy pallet LEFT JOIN product_storage_catalog STORAGE ON pallet.ps_id = `storage`.id ";
				
				if(title_id!=0){
					sql += "INNER JOIN pallet_inventroy_title palletTitle ON palletTitle.pallet_id = pallet.pallet_id ";
				}
				
				if(invenStatus!=0){
					sql += " , (SELECT sum(pt.pallet_count) pallet_count, pt.pallet_id ,pt.title_id FROM pallet_inventroy_title pt INNER JOIN pallet_inventroy pallet ON pt.pallet_id = pallet.pallet_id  ";
					sql += " WHERE 1=1";
					if(ps_id!=0){
						sql += " AND pallet.ps_id = "+ps_id;
					}
					if(title_id!=0){
						sql += " AND pt.title_id = "+title_id;
					}
					sql += " GROUP BY pt.pallet_id ) p";
				}
				
				sql += " WHERE 1=1";
				
				if(invenStatus==1){
					sql += " and p.pallet_count > 0 AND pallet.pallet_id = p.pallet_id";
				}else if(invenStatus==2){
					sql += " and p.pallet_count = 0 AND pallet.pallet_id = p.pallet_id";
				}
				if( ps_id > 0 ){
					sql += " AND pallet.ps_id = "+ps_id;
				}
				if( title_id > 0 ){
					sql += " AND palletTitle.title_id = "+title_id;
				}
				sql +=" order by pallet.post_date desc";

				return dbUtilAutoTran.selectMutliple(sql,pc);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb getAllPalletInventory"+e);
			}
		}
		/**
		 * add pallet Inventory
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public long addPalletInventory(DBRow row) throws Exception {
			try {
				
				return  dbUtilAutoTran.insertReturnId("pallet_inventroy", row);

			} catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb addPalletInventory error:"+e);
			}
		}
		/**
		 * update pallet Inventory
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public long updatePalletInventory(long pallet_id,DBRow row)throws Exception{
			try{
				return this.dbUtilAutoTran.update("where pallet_id="+pallet_id, "pallet_inventroy", row);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb updatePalletInventory"+e);
			}
		}
		/**
		 * 查询pallet
		 * @param palletId
		 * @return
		 * @throws Exception
		 */
		public DBRow findPalletInventoryById(int palletId)throws Exception
		{
			try
			{
				String sql ="SELECT pallet.pallet_id,pallet.pallet_type,pallet.pallet_count,pallet.damaged_count,pallet.ps_id,`storage`.title from pallet_inventroy pallet LEFT JOIN product_storage_catalog storage on pallet.ps_id = `storage`.id where pallet_id ="+palletId;
				
				return dbUtilAutoTran.selectSingle(sql);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb findPalletInventoryById"+e);
			}
		}
		/**
		 * add pallet Inventory log
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public long addPalletInventoryLogs(DBRow row) throws Exception {
			try {
				
				return  dbUtilAutoTran.insertReturnId("pallet_inventroy_logs", row);

			} catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb addPalletInventoryLogs error:"+e);
			}
		}
		public long addPalletInventroyDetail(DBRow row) throws Exception {
			try {
				
				return  dbUtilAutoTran.insertReturnId("pallet_inventroy_title", row);

			} catch (Exception e) {
				throw new Exception("FloorCheckInMgrZwb addPalletInventroyDetail error:"+e);
			}
		}
		public long updatePalletInventoryDetail(long pallet_id,long titleId,DBRow row)throws Exception{
			try{
				return this.dbUtilAutoTran.update("where pallet_id="+pallet_id+" and title_id="+titleId, "pallet_inventroy_title", row);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb updatePalletInventory"+e);
			}
		}
		/**
		 * 查询pallet操作日志
		 * @param start_time
		 * @param end_time
		 * @param pallet_type
		 * @param ps_id
		 * @param pc
		 * @return
		 * @throws Exception
		 */
		public DBRow[] findPalletInventoryLog(String start_time,String end_time,String pallet_type,long ps_id,PageCtrl pc)throws Exception
		{
			try
			{
				String sql ="SELECT log.pal_id, log.operation, log.post_date, log.damaged_quantity, log.quantity, log.ps_id, log.pallet_id, log.adid, pallet.pallet_type, `STORAGE`.title, t.title_id,t.title_name"
						+ " ,ad.employe_name FROM pallet_inventroy_logs log LEFT JOIN pallet_inventroy pallet ON log.pallet_id = pallet.pallet_id "
						+ " LEFT JOIN product_storage_catalog STORAGE ON `storage`.id = log.ps_id LEFT JOIN admin ad ON ad.adid = log.adid "
						+ " left join title t on t.title_id=log.title_id where 1=1 ";
				if(!StrUtil.isBlank(start_time)){
					sql +=" and log.post_date >='"+start_time+"'" ;
				}
				if(!StrUtil.isBlank(end_time)){
					sql +=" and log.post_date <='"+end_time+"'" ;
				}
				if(!StrUtil.isBlank(pallet_type)){
					sql +=" and pallet.pallet_type ="+pallet_type ;
				}
				if(ps_id>0){
					sql +=" and log.ps_id ="+ps_id;
				}
				sql +=" order by log.post_date desc";
				return dbUtilAutoTran.selectMutliple(sql, pc);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb findPalletInventoryLog"+e);
			}
		}
		public DBRow[] findPalletType()throws Exception
		{
			try
			{
				String sql ="select distinct pallet_type from pallet_inventroy";
				
				return dbUtilAutoTran.selectMutliple(sql);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb findPalletType"+e);
			}
		}
		public DBRow findPalletInventoryByPsIdAndPalletType(long ps_id, String palletType )throws Exception
		{
			try
			{
				String sql ="SELECT pallet_id,pallet_type,pallet_count,damaged_count,ps_id FROM pallet_inventroy where pallet_type = "+palletType+" AND ps_id = "+ps_id;
				
				return dbUtilAutoTran.selectSingle(sql);
			}
			catch(Exception e)
			{
				 throw new Exception("FloorCheckInMgrZwb findPalletInventoryByPsIdAndPalletType"+e);
			}
		}
		
		public DBRow[] findPalletForTitleByStorageAndType(int palletId, long ps_id, int titleId,int invenStatus) throws Exception{ 
			
			try{
				String sql = "SELECT pallet.pit_id, pallet.title_id, t.title_name, pallet.pallet_count, pallet.damaged_count ,pallet.post_date "
						+  " FROM pallet_inventroy_title pallet LEFT JOIN title t ON t.title_id = pallet.title_id WHERE 1=1";
				if(palletId!=0){
					sql += " AND pallet.pallet_id = '"+palletId+"'";
				}
				if(titleId!=0){
					sql += " AND t.title_id = "+titleId;
				}
				
				if(invenStatus==1){
					sql += " AND pallet.pallet_count >"+0;
				}
				if(invenStatus == 2){
					sql += " AND pallet.pallet_count ="+0;
				}
				
				return  dbUtilAutoTran.selectMutliple(sql);
			}catch (Exception e){
				throw new Exception("findPalletForTitleByStorageAndType"+e);
			}
		}
		public DBRow findPalletCountByIdTypeTitle(long ps_id, String palletType, long titleId) throws Exception{
			try{
				String sql = "SELECT pallet.pallet_id, pallet.pallet_type,pallet.ps_id, pallet.pallet_count pallet_totality, pallet.damaged_count damaged_totality,p_title.title_id,p_title.pallet_count ,p_title.damaged_count "
						+ "FROM pallet_inventroy pallet INNER JOIN pallet_inventroy_title p_title ON p_title.pallet_id = pallet.pallet_id "
						+ "WHERE 1=1 ";
				if(ps_id!=0){
					sql += " And pallet.ps_id = "+ps_id;
				}
				if(!StrUtil.isBlank(palletType)){
					sql += " AND  pallet.pallet_type = '"+palletType+"'";
				}
				if(titleId!=0){
					sql += " AND p_title.title_id = "+titleId;
				}
				return  dbUtilAutoTran.selectSingle(sql);
			}catch (Exception e){
				throw new Exception("findPalletCountByIdTypeTitle"+e);
			}
			
		}
		public DBRow[] findAllTitle(String palletType, long ps_id) throws Exception {
			
				
			try{
				String sql = "SELECT t.title_id, t.title_name "
						+ "FROM title t ";
				    if(!(StrUtil.isBlank(palletType))||ps_id!=0){
					sql += " INNER JOIN pallet_inventroy_title pt ON pt.title_id = t.title_id";
					sql += " INNER JOIN pallet_inventroy pallet ON pt.pallet_id = pallet.pallet_id";
					if(!(StrUtil.isBlank(palletType))){
						sql += " AND pallet.pallet_type = '"+palletType+"'";
					}
					if(ps_id!=0){
						sql += "AND pallet.ps_id = "+ps_id;
					}
				}

					
					return  dbUtilAutoTran.selectMutliple(sql);
				}catch (Exception e){
					throw new Exception("findAllTitle"+e);
				}
				
		 }
		
		public DBRow findLoadBar(long load_bar_id)throws Exception{
			try{
				String sql="select * from load_bar where load_bar_id="+load_bar_id;
				return this.dbUtilAutoTran.selectSingle(sql);
			}catch(Exception e){
				throw new Exception("findLoadBar"+e);
			}
		}
		public DBRow[] getAuthenticationByAdgid(long adgid) throws Exception{
			try{
				String sql = "SELECT admin.adgid,action.* FROM admin_group_auth admin JOIN authentication_action action ON admin.ataid = action.ataid WHERE action.page = 182 AND ";
				if(adgid>0){
					sql += " admin.adgid = "+adgid;
				}
				return  dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("getAuthenticationByAdgid"+e);
			}
			
		}
		
		/**
		 * 添加 已经打印的标签记录
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public long addPrintedLabel(DBRow row)throws Exception{
			try{
				return this.dbUtilAutoTran.insertReturnId("printed_label", row);
				
			}catch(Exception e){
				throw new Exception("addPrintedLabel"+e);
			}
		}
		
		public DBRow selectCountPrintedLabel(long entry_id,String number,long master_bol_no,long type)throws Exception{
			try{
				String sql="select * from printed_label where entry_id="+entry_id+" and number="+number+" and master_bol_no="+master_bol_no+" and type="+type;				
				return this.dbUtilAutoTran.selectSingle(sql);
			}catch(Exception e){
				throw new Exception("selectCountPrintedLabel"+e);
			}
		}
		public DBRow[] getUseSpot(long ps_id) throws Exception {
			try {
				
				String sql =" select yc.yc_id ,yc.yc_no ,yc.yc_status from " +ConfigBean.getStringValue("storage_yard_control") + " yc" +
							" where yc.ps_id =  " +ps_id +" and yc.yc_status=1" ;
				
				//System.out.println(sql);
				return dbUtilAutoTran.selectMutliple(sql);
			} catch (SQLException e) {
				throw new Exception("FloorCheckInMgrZwb getParkingOccupancy():" + e);
			}
		}
		
		/**
		 * 根据EntryID和ModuleKey查询Entry子单据
		 * @param moduleKey
		 * @param entry_id
		 * @return
		 * @throws Exception
		 * @author:zyj
		 * @date: 2014年11月12日 下午6:51:42
		 */
		public DBRow[] findEntryDetailByEntryIdType(long entry_id ,int moduleKey) throws Exception
		{
			try
			{
				String sql = "select * from door_or_location_occupancy_details where dlo_id = " + entry_id
							+ " and number_type = " + moduleKey;
				return dbUtilAutoTran.selectMutliple(sql);
			} catch (SQLException e) {
				throw new Exception("FloorCheckInMgrZwb findEntryDetailByEntryIdType:" + e);
			}
		}
		public DBRow selectPsTimeSet(long ps_id) throws Exception{
			try{
				String sql = "select * from set_storage_timeout where ps_id="+ ps_id ;
				return dbUtilAutoTran.selectSingle(sql);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb selectPsTimeSet:" + e);
			}
		}

		public DBRow[] selectCtnrInYard(long ps_id) throws Exception{
			try{
				String sql = "select * from door_or_location_occupancy_main where `status` <> 5  and gate_check_in_time is not null and ps_id="+ ps_id;
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb selectCtnrInYard:" + e);
			}
		}
/**
		 * 批量释放门
		 * @param row
		 * @return
		 * @throws Exception
		 */
		public void batchReleaseDoor(String door ,DBRow updateRow) throws Exception{
			try{

				dbUtilAutoTran.update(" where sd_id in ("+door+")", "storage_door", updateRow);


			}catch (Exception e) {
				 throw new Exception("FloorCheckInMgrZwb batchReleaseDoor"+e);
			}
		}
		
		//释放门
	    public long updateDetailsDoorOrSpot(long entryId,int occupancy_type,long rl_id,DBRow row)throws Exception{
	    	try{
	    		return this.dbUtilAutoTran.update("where dlo_id="+entryId+" and occupancy_type= "+occupancy_type+" and rl_id="+rl_id, "door_or_location_occupancy_details", row);
	    		//System.out.println(sql);
	    		
	    	}catch(Exception e){
	    		throw new Exception("FloorCheckInMgrZwb updateDetailsDoorOrSpot"+e);
	    	}
	    }


		/**
		 * 判断task与其他未处理或处理中的task是否重
		 * @param order
		 * @param number_type
		 * @param ps_id
		 * @param company_id
		 * @param dlo_id
		 * @return
		 * @throws Exception
		 * @author:zyj
		 * @date: 2014年11月17日 下午6:56:05
		 */
		public DBRow[] findTaskByNumberTypePsCompanyNotDloId(String order,int number_type,long ps_id,String company_id, long dlo_id) throws Exception
		{
			try{

				String sql="select * from door_or_location_occupancy_details dds join door_or_location_occupancy_main ddm " +
						   "on dds.dlo_id=ddm.dlo_id where ps_id="+ps_id+" and number="+"'"+order
						   +"' and (number_status="+CheckInChildDocumentsStatusTypeKey.UNPROCESS 
						   +" or number_status="+CheckInChildDocumentsStatusTypeKey.PROCESSING+")"
						   +" and ddm.status <>  "+CheckInMainDocumentsStatusTypeKey.LEFT;
				if(number_type > 0){
					sql+=" and number_type ="+number_type;
				}
				if(!StrUtil.isBlank(company_id)){
					sql+=" and company_id='"+company_id+"'";
				}
				sql += " and dds.dlo_id != " + dlo_id;
				return this.dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb findTaskByNumberTypePsCompanyNotDloId"+e);
			}
		}
		
		/**
		 * ctn_number与其他entry的ctn_nubmer不能重
		 * @param ctn_number
		 * @param dlo_id
		 * @param ps_id
		 * @return
		 * @throws Exception
		 * @author:zyj
		 * @date: 2014年11月17日 下午7:14:37
		 */
		public DBRow[] findCtnNumberNotDloId(String ctn_number, long dlo_id, long ps_id) throws Exception
		{
			try
			{
				String sql = "select * from door_or_location_occupancy_details dds join door_or_location_occupancy_main ddm "
						 +" on dds.dlo_id=ddm.dlo_id where ps_id="+ps_id;
				sql += " and ctn_nubmer = '"+ctn_number+"'";
				sql += " and (number_status="+CheckInChildDocumentsStatusTypeKey.UNPROCESS 
						+" or number_status="+CheckInChildDocumentsStatusTypeKey.PROCESSING+")"
						   +" and ddm.status <>  "+CheckInMainDocumentsStatusTypeKey.LEFT;
				sql += " and dds.dlo_id != " + dlo_id;
				return dbUtilAutoTran.selectMutliple(sql);
			}
			catch(Exception e)
			{
				throw new Exception("FloorCheckInMgrZwb findCtnNumberNotDloId"+e);
			}
		}
		
		/**
		 * 通过entryID更新所有子表的ctn_number
		 * @param dlo_id
		 * @param ctn_number
		 * @throws Exception
		 * @author:zyj
		 * @date: 2014年11月17日 下午7:49:29
		 */
		public void updateDetailsBlankCntNumberByDloId(long dlo_id, DBRow row) throws Exception
		{
			try
			{
				dbUtilAutoTran.update(" where dlo_id = " + dlo_id+" and (ctn_number is null or ctn_number = '')", "door_or_location_occupancy_details", row);
			}
			catch(Exception e)
			{
				throw new Exception("FloorCheckInMgrZwb updateDetailsCntNumberByDloId"+e);
			}
		}
		/**
		 * 得到当前门下面的数据
		 * @param dlo_id
		 * @param sd_id
		 * @return
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年11月18日
		 */
		public DBRow[] getDetailsBy(long dlo_id , long sd_id) throws Exception{
			try{
				String sql = "select * from door_or_location_occupancy_details as detail where  occupancy_type="+OccupyTypeKey.DOOR+" and rl_id="+sd_id+" and dlo_id="+dlo_id;
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb getDetailsBy"+e);

			}
		}
		
		public void updateUnProcessAndProcessingDetail(long dlo_id ,  DBRow updateRow) throws Exception{
			try{
				dbUtilAutoTran.update("where dlo_id="+dlo_id, "door_or_location_occupancy_details", updateRow);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb updateUnProcessAndProcessingDetail"+e);
			}
		} 
		
		/**
		 * entryId关联门或停车位
		 * @param dlo_id
		 * @return
		 * @throws Exception
		 */
		public DBRow[] selectLocationByEntryId(long dlo_id) throws Exception{
			try{
				String sql = "select * from door_or_location_occupancy_main dm "
						+ "left join storage_yard_control syc on dm.dlo_id=syc.associate_id "
						+ "left join storage_door sd on dm.dlo_id=sd.associate_id "
						+ "where  dm.dlo_id="+dlo_id;
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb getDetailsBy"+e);

			}
		}
		public DBRow[] forgetCheckOut(long ps_id,PageCtrl pc)
				throws Exception
			{
			String sql = "select forgetView.* from (" 
					+"select noDoorView.dlo_id,noDoorView.gate_container_no,noDoorView.patrol_create,noDoorView.patrol_lost from " 
					+"( "
					+"select dlo.* from door_or_location_occupancy_main as dlo "
					+"left join door_or_location_occupancy_details as dlod on dlo.dlo_id = dlod.dlo_id "
					+"left join storage_door as sd on dlo.dlo_id = sd.associate_id and sd.associate_type = "+ModuleKey.CHECK_IN+" "
					+"where dlo.`status` in ("+CheckInMainDocumentsStatusTypeKey.UNPROCESS+","+CheckInMainDocumentsStatusTypeKey.PROCESSING+","+CheckInMainDocumentsStatusTypeKey.INYARD+") and dlo.ps_id = ?  and sd.sd_id is null and dlo.rel_type in ("+CheckInMainDocumentsRelTypeKey.DELIVERY+","+CheckInMainDocumentsRelTypeKey.PICK_UP+","+CheckInMainDocumentsRelTypeKey.BOTH+","+CheckInMainDocumentsRelTypeKey.CTNR+","+CheckInMainDocumentsRelTypeKey.PATROL+") "
					+"group by dlo.dlo_id "
					+") as noDoorView "
					+"join ( "
					+"select dlo.* from door_or_location_occupancy_main as dlo "
					+"left join door_or_location_occupancy_details as dlod on dlo.dlo_id = dlod.dlo_id "
					+"left join storage_yard_control as syc on dlo.dlo_id = syc.associate_id and syc.associate_type = "+ModuleKey.CHECK_IN+" "
					+"where dlo.`status` in ("+CheckInMainDocumentsStatusTypeKey.UNPROCESS+","+CheckInMainDocumentsStatusTypeKey.PROCESSING+","+CheckInMainDocumentsStatusTypeKey.INYARD+") and dlo.ps_id = ?  and syc.yc_id is null and dlo.rel_type in ("+CheckInMainDocumentsRelTypeKey.DELIVERY+","+CheckInMainDocumentsRelTypeKey.PICK_UP+","+CheckInMainDocumentsRelTypeKey.BOTH+","+CheckInMainDocumentsRelTypeKey.CTNR+","+CheckInMainDocumentsRelTypeKey.PATROL+") "
					+") as noSpotView on noDoorView.dlo_id = noSpotView.dlo_id"
					+") as forgetView "
					+"join "+ConfigBean.getStringValue("check_in_log")+" as cil on cil.dlo_id = forgetView.dlo_id "
					+"GROUP BY forgetView.dlo_id";
				DBRow para = new DBRow();
				para.add("1_ps_id",ps_id);
				para.add("2_ps_id",ps_id);
				
				DBRow[] result = dbUtilAutoTran.selectPreMutliple(sql, para, pc);
				return result;
			}
		 
		public DBRow findStorageLocationAreaByAreaId(long area_id) throws Exception
		{
			try{
				String sql = "select * from storage_location_area where area_id = "+area_id;
				return dbUtilAutoTran.selectSingle(sql);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb findStorageLocationAreaByAreaId"+e);

			}
		}
		public DBRow[] getSealInventory(DBRow para, PageCtrl pc) throws Exception{
			try{
			String sql = "SELECT seal.seal_id, seal.seal_no, seal.seal_status, seal.import_time, "
					+ "(SELECT admin.account FROM admin WHERE seal.import_user = admin.adid ) import_user, "
					+ "seal.used_time, seal.assign_time, (SELECT admin.account FROM admin WHERE seal.assign_user = admin.adid ) assign_user,"
					+ " seal.return_time, (SELECT admin.account FROM admin WHERE seal.return_user = admin.adid ) return_user, ps.title, seal.ps_id "
					+ "FROM import_seal seal LEFT JOIN product_storage_catalog ps ON ps.id = seal.ps_id WHERE 1=1";
			
			int seal_status = para.get("seal_status", 0);
			long ps_id = para.get("ps_id", 0l);
			String import_start_time = para.get("import_start_time", "");
			String import_end_time = para.get("import_end_time", "");
			String used_start_time = para.get("used_start_time", "");
			String used_end_time = para.get("used_end_time", "");
			String return_start_time = para.get("return_start_time", "");
			String return_end_time = para.get("return_end_time", "");
			if(seal_status>0){
				sql += " AND seal.seal_status = "+seal_status;
			}
			if(ps_id>0){
				sql += " AND seal.ps_id = "+ps_id;
			}
			if(!StrUtil.isBlank(import_start_time)){
				sql += " AND seal.import_time > "+import_start_time;
			}
			if(!StrUtil.isBlank(import_end_time)){
				sql += " AND seal.import_time < "+import_end_time;
			}
			if(!StrUtil.isBlank(used_start_time)){
				sql += " AND seal.used_time > "+used_start_time;
			}
			if(!StrUtil.isBlank(used_end_time)){
				sql += " AND seal.used_time < "+used_end_time;
			}
			if(!StrUtil.isBlank(return_start_time)){
				sql += " AND seal.return_time > "+return_start_time;
			}
			if(!StrUtil.isBlank(return_end_time)){
				sql += " AND seal.return _time < "+return_end_time;
			}
			return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb getSealInventory"+e);
			}
		}
		public boolean isEntryDetailHasProcessing(long entry_id) throws Exception{
			try{
				
				String sql = "select count(*) as sum_count from door_or_location_occupancy_details where number_status="+CheckInChildDocumentsStatusTypeKey.PROCESSING + " and dlo_id="+entry_id ;
 				DBRow row = dbUtilAutoTran.selectSingle(sql);
				int count = 0 ;
				if(row != null){
					count = row.get("sum_count", 0);
				}
				return count > 0 ;
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb isEntryDetailHasProcessing"+e);
			}
		}
		
		public void moveSpotDetail(long entry_id , DBRow updateRow) throws Exception {
			try{
				dbUtilAutoTran.update(" where occupancy_type="+ OccupyTypeKey.SPOT + " and dlo_id="+entry_id , "door_or_location_occupancy_details", updateRow);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb handSpotDetail"+e);
			}
		}
		//个Entry下面所有门是Occupied && 是Un + processing 的任务移动到当前选择的这个门上
		public void moveDoorDetail(long entry_id ,long sd_id , DBRow updateRow) throws Exception{
			try{
				dbUtilAutoTran.update(" where  rl_id="+sd_id+" and occupancy_type="+OccupyTypeKey.DOOR + " and occupancy_status="+OccupyStatusTypeKey.OCUPIED+" and (number_status="+CheckInChildDocumentsStatusTypeKey.UNPROCESS+" or number_status="+CheckInChildDocumentsStatusTypeKey.PROCESSING+" ) and dlo_id="+ entry_id, "door_or_location_occupancy_details", updateRow);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb moveDoorDetail"+e);
			}
		}
		
		/**
		 * 更新updateDoorUnProcessAndProcessingDetail 
		 * 更新当前门下 UnProcessAndProcessing 处于某个状态
		 * @param sd_id
		 * @param dlo_id
		 * @param updateRow
		 * @throws Exception
		 * @author zhangrui
		 * @Date   2014年11月21日
		 */
		public void updateDoorUnProcessAndProcessingDetail(long sd_id , long dlo_id , DBRow updateRow) throws Exception{
			try{
				dbUtilAutoTran.update(" where occupancy_status="+OccupyTypeKey.DOOR+" and rl_id="+sd_id + " and( number_status = "+CheckInChildDocumentsStatusTypeKey.PROCESSING +""
						+ " or  number_status="+CheckInChildDocumentsStatusTypeKey.UNPROCESS+")and dlo_id="+dlo_id , "door_or_location_occupancy_details", updateRow);
				
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb updateDoorUnProcessAndProcessingDetail"+e);
			}
		}
		
		public void updateSpotUnProcessAndProcessingDetail(long yc_id , long dlo_id , DBRow updateRow) throws Exception{
			try{
				dbUtilAutoTran.update(" where occupancy_status="+OccupyTypeKey.SPOT+" and rl_id="+yc_id + " and ( number_status = "+CheckInChildDocumentsStatusTypeKey.PROCESSING +""
						+ " or  number_status="+CheckInChildDocumentsStatusTypeKey.UNPROCESS+") and dlo_id="+dlo_id , "door_or_location_occupancy_details", updateRow);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb updateSpotUnProcessAndProcessingDetail"+e);
			}
		}
		public DBRow[] getSpotDetail(long yc_id ,  long dlo_id ) throws Exception{
			try{
				String sql = "select * from door_or_location_occupancy_details where rl_id="+yc_id + "  and occupancy_type="+OccupyTypeKey.SPOT+" and dlo_id="+dlo_id ;
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb getSpotDetail"+e);
			}
		}
		public DBRow[] getEquipmentBySpotId(long yc_id) throws Exception{
			try{
				String sql = "select ee.equipment_number,ee.equipment_id ,ee.equipment_type , srr.occupy_status, ee.check_in_entry_id from "+ConfigBean.getStringValue("space_resources_relation")+" as srr "
							+"left join "+ConfigBean.getStringValue("entry_equipment")+" as ee on ee.equipment_id = srr.relation_id "
							+"where srr.resources_id="+yc_id+" and srr.occupy_status="+OccupyStatusTypeKey.OCUPIED+" "
							+"and srr.relation_type="+SpaceRelationTypeKey.Equipment+" and srr.resources_type = "+OccupyTypeKey.SPOT;
				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb getEquipmentBySpotId"+e);
			}
		}
		public DBRow[] getEquipmentByDoorId(long doorId) throws Exception{
			try{
				String sql ="select ee.equipment_number,ee.equipment_id ,ee.equipment_type , srr.occupy_status, ee.check_in_entry_id from "+ConfigBean.getStringValue("space_resources_relation")+" as srr  "
					       +"left join "+ConfigBean.getStringValue("entry_equipment")+" as ee on ee.equipment_id = srr.relation_id "
						   +"where srr.resources_id="+doorId+" and srr.occupy_status="+OccupyStatusTypeKey.OCUPIED+" "
						   +"and srr.relation_type="+SpaceRelationTypeKey.Equipment+" and srr.resources_type = "+OccupyTypeKey.DOOR;

				return dbUtilAutoTran.selectMutliple(sql);
			}catch(Exception e){
				throw new Exception("FloorCheckInMgrZwb getEquipmentByDoorId"+e);
			}
		}
		/**
		 * 返回车位
		 * @xujia
		 * @param ps_id
		 * @param area_id
		 * @param spot_status
		 * @param pc
		 * @return
		 * @throws Exception
		 */
		public DBRow[] returnSpot(long ps_id,long area_id,int spot_status,int isFinish,PageCtrl pc)
			throws Exception
		{
			String whereWarehouse = "";
			String whereArea = "";
			String whereSpotStatus = "";
			String whereisFinish = "";
			if (ps_id !=0)
			{
				whereWarehouse = " and syc.ps_id = "+ps_id+" ";
			}
			
			if (area_id != 0)
			{
				whereArea = " and syc.area_id = "+area_id+" ";
			}
			
			if (spot_status == OccupyStatusTypeKey.OCUPIED)
			{
				whereSpotStatus = " and srr.occupy_status = "+spot_status+" ";
			}
			if(spot_status == OccupyStatusTypeKey.RELEASED){
				
				whereSpotStatus = " and srr.occupy_status is null ";
			}
			if(isFinish == 1){ //未patrol
				
				whereisFinish = " and syc.patrol_time is null ";
			}
			if(isFinish == 2){ //已patrol
	
				whereisFinish = " and syc.patrol_time is not null ";
			}
			String sql = "select syc.yc_no as resource_name,syc.yc_id as resource_id,syc.area_id ,sla.area_name from "+ConfigBean.getStringValue("storage_yard_control")+" as syc "
						+"left join "+ConfigBean.getStringValue("space_resources_relation")+" as srr on  srr.resources_type = "+OccupyTypeKey.SPOT+" and syc.yc_id = srr.resources_id "
						+"left join "+ConfigBean.getStringValue("storage_location_area")+" as sla on  sla.area_id = syc.area_id "
						+"where  1=1  "+whereisFinish+whereWarehouse+whereArea+whereSpotStatus
						+"group by syc.yc_id ORDER BY CAST(syc.yc_no AS SIGNED)";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}
		/**
		 * 返回门
		 * @xujia
		 * @param ps_id
		 * @param area_id
		 * @param spot_status
		 * @param pc
		 * @return
		 * @throws Exception
		 */
		
		public DBRow[] returnDoor(long ps_id,long area_id,int door_status,int isFinish,PageCtrl pc)
				throws Exception
		{
			String whereWarehouse = "";
			String whereArea = "";
			String whereDoorStatus = "";
			String whereisFinish = "";
			if (ps_id !=0)
			{
				whereWarehouse = " and sd.ps_id = "+ps_id+" ";
			}
			
			if (area_id != 0)
			{
				whereArea = " and sd.area_id = "+area_id+" ";
			}
				
			if (door_status == OccupyStatusTypeKey.RELEASED)
			{
				whereDoorStatus = " and srr.occupy_status is null ";
			}
			if(door_status == OccupyStatusTypeKey.RESERVERED || door_status == OccupyStatusTypeKey.OCUPIED){
				
				whereDoorStatus = " and srr.occupy_status= "+door_status+" ";
			}
			if(isFinish == 1){ //未patrol
				
				whereisFinish = " and sd.patrol_time is null ";
			}
			if(isFinish == 2){ //已patrol
	
				whereisFinish = " and sd.patrol_time is not null ";
			}	
			String sql = "select sd.doorId as resource_name,sd.sd_id as resource_id,sd.area_id,sla.area_name  from "+ConfigBean.getStringValue("storage_door")+" as sd "
						+"left join "+ConfigBean.getStringValue("space_resources_relation")+" as srr on  srr.resources_type = "+OccupyTypeKey.DOOR+" and sd.sd_id = srr.resources_id "
						+"left join "+ConfigBean.getStringValue("storage_location_area")+" as sla on  sla.area_id = sd.area_id "
						+"where 1=1  "+whereisFinish+whereWarehouse+whereArea+whereDoorStatus
						+"group by sd.sd_id ORDER BY CAST(sd.doorId AS SIGNED)";
				
			return dbUtilAutoTran.selectMutliple(sql, pc);
		}	
		
		public DBRow[] findTaskByEquipmentId(long equipment_id)
				throws Exception
		{
			String sql ="select * from door_or_location_occupancy_details where equipment_id="+equipment_id;
			
			return dbUtilAutoTran.selectMutliple(sql);
		}	
		public void updateCtnrStatus(long equipment_id,DBRow row)
				throws Exception
		{
			 dbUtilAutoTran.update("where equipment_id="+equipment_id, "entry_equipment", row);

		}	
		public DBRow[] ghostCtnrSecondConfirm(String patrol_start_time)
				throws Exception
		{
			String sql ="select * from entry_equipment ee left join door_or_location_occupancy_details dd on ee.check_in_entry_id=dd.dlo_id where patrol_lost=2  and  (number_status is null or number_status <>2)";
			if(!StrUtil.isBlank(patrol_start_time)){
				sql += " and check_in_time <= '"+patrol_start_time+"'";
			}
			
			return dbUtilAutoTran.selectMutliple(sql);
		}	
		public DBRow[] getPatrolStartTime(long ps_id)
				throws Exception
		{
			
			return dbUtilAutoTran.selectMutliple("select patrol_time from storage_door  where patrol_time is not null and ps_id="+ps_id+
                                                 " UNION "+
                                                 " select patrol_time from storage_yard_control where patrol_time is not null and ps_id="+ps_id+
												 " order by patrol_time asc");
		}	
		
}

