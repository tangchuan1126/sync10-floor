package com.cwc.app.floor.api.zj;

import com.cwc.app.key.ProductStoreBillKey;
import com.cwc.app.key.ProductStorePhysicalOperationKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorProductStorePhysicalLogMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加物理操作日志
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addProductStorePhysicalLog(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_store_physical_log"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductStorePhysicalLogMgrZJ addProductStorePhysicalLog error:"+e);
		}
	}
	
	/**
	 * 获得转运单的放货日志
	 * @param transport_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStorePhysicaLogLayUpForTransport(long transport_id)
		throws Exception
	{
		try 
		{
			String sql = "select pspl.*,slc.slc_position_all from "+ConfigBean.getStringValue("product_store_physical_log")+" as pspl "
						+"join "+ConfigBean.getStringValue("storage_location_catalog")+" as slc on pspl.operation_slc_id = slc.slc_id "
						+"where system_bill_id = ? and operation_type = ? and (system_bill_type ="+ProductStoreBillKey.DELIVERY_ORDER+" or system_bill_type = "+ProductStoreBillKey.TRANSPORT_ORDER+")";
			
			DBRow para = new DBRow();
			para.add("system_bill_id",transport_id);
			para.add("operation_type",ProductStorePhysicalOperationKey.LayUp);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductStorePhysicalLogMgrZJ getProductStorePhysicaLogForTransport error:"+e);
		}
	}
	
	/**
	 * 根据转运单号查询序列号是否放置
	 * @param transport_id
	 * @param sn
	 * @return
	 * @throws Exception
	 */
	public DBRow getProductStorePhysicalLogLayUpBySN(long system_bill_id,int system_bill_type,String sn)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_physical_log")+" where system_bill_id = ? and operation_type = ? and serial_number = ? and system_bill_type =? ";
			
			DBRow para = new DBRow();
			para.add("system_bill_id",system_bill_id);
			para.add("operation_type",ProductStorePhysicalOperationKey.LayUp);
			para.add("serial_number",sn);
			para.add("system_bill_type",system_bill_type);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorProductStorePhysicalLogMgrZJ getProductStorePhysicalLogLayUpBySN error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
