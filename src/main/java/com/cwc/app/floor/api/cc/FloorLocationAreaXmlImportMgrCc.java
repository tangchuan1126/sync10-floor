package com.cwc.app.floor.api.cc;

import java.sql.SQLException;
import java.util.List;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorLocationAreaXmlImportMgrCc {
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	/**
	 * 保存KML数据
	 * @param rows
	 * @throws Exception
	 */
	public void savePraseRusult(List<DBRow> rows) throws Exception{
		String tableName = ConfigBean.getStringValue("folder_from_kml");
		try {
			for (DBRow row : rows) {
				dbUtilAutoTran.insert(tableName, row);
			}
		} catch (Exception e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc.savePraseRusult(List<DBRow> rows):"+e);
		}
	}
	public void deleteDataByid(String ps_id) throws Exception{
		String wherecond =" where ps_id ="+ps_id;
		String tablename = ConfigBean.getStringValue("folder_from_kml");
		try {
			dbUtilAutoTran.delete(wherecond, tablename);
		} catch (Exception e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc.deleteDataByid(String ps_id):"+e);
		}
	}
	
	/**
	 * 截断folder_from_kml表
	 * @throws Exception
	 */
	public void truncateTableFolderFromKml() throws Exception{
		String sql = "TRUNCATE TABLE "+ConfigBean.getStringValue("folder_from_kml");
		try {
			dbUtilAutoTran.executeSQL(sql);
		} catch (SQLException e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc.truncateTableFolderFromKml():"+e);
		}
	}
	/**
	 * 删除folder_from_kml临时数据
	 * @param source
	 * @throws Exception
	 */
	public void deleteFolderFromKml(String source) throws Exception{
		String wherecond = " where source = '"+source+"' ";
		try {
			dbUtilAutoTran.delete(wherecond, ConfigBean.getStringValue("folder_from_kml"));
		} catch (Exception e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc.deleteFolderFromKml():"+e);
		}
	}
	
	/**
	 * 添加storage_kml数据
	 * @return
	 * @throws Exception
	 */
	public long addOrUpdateStorageKml(DBRow storageKml) throws Exception{
		try {
			String psId = storageKml.getString("ps_id");
			String storage_kml = ConfigBean.getStringValue("storage_kml");
			String sql = "SELECT * FROM "+storage_kml+" s WHERE s.ps_id = ? ";
			DBRow para = new DBRow();
			para.add("ps_id", psId);
			DBRow row = dbUtilAutoTran.selectPreSingle(sql, para);
			if(row == null){
				return dbUtilAutoTran.insertReturnId(storage_kml, storageKml);
			}else{
				return dbUtilAutoTran.update(" where ps_id = " + psId, storage_kml, storageKml);
			}
		} catch (Exception e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc.addOrUpdateStorageKml():"+e);
		}
	}
	/**
	 * 获取当前仓库现有和KML导入数据
	 * @param storage
	 * @return
	 * @throws Exception
	 */
	public DBRow getAllAreaInfoForStorage(DBRow storage) throws Exception{
		DBRow row = new DBRow();
		try {
			row.add("warehouseBase", getWarehouseBase(storage));
			row.add("zones", getLocationName(storage));
			row.add("location",getLocation(storage));
			row.add("docks", getDocks(storage));
			row.add("staging",getStaging(storage));
			row.add("parking",getParking(storage));
			row.add("webcam",getWebcam(storage));
			return row;	
		} catch (SQLException e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc.getAllAreaInfoForStorage():"+e);
		}
	}
	/**
	 * 获取KML导入的仓库基础数据
	 * @param storage
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getWarehouseBase(DBRow storage) throws Exception{
		try {
			String sql = "select f.area_name type, f.placemark_name param, f.x lng, f.y lat, f.ps_id" +
						" from " + ConfigBean.getStringValue("folder_from_kml") + " f " +
						" where f.folder_name='base' and f.ps_id=" + storage.get("id",0);
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc.getWarehouseBase():"+e);
		}
	}
	/**
	 * 获取KML导入的区域分组信息
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocationName(DBRow storage) throws Exception{
		try{
			String name = "SELECT DISTINCT t.folder_name,t.area_name,t.placemark_name title," +
						" IFNULL((SELECT title_id from title where title_name=t.placemark_name),0) title_id," +
						"t.is_three_dimensional docks, " +
						"t.x, t.y, t.width, t.height, t.angle, t.latlng," +
						"			IFNULL((SELECT s1.area_id from "+ConfigBean.getStringValue("storage_location_area")+" s1 where s1.area_psid = ? and s1.area_name = t.area_name and t.folder_name='area' ),0) area_id, " +
						" IFNULL((SELECT DISTINCT k.folder_name from "+ConfigBean.getStringValue("folder_from_kml")+" k WHERE k.ps_id = t.ps_id and t.area_name = k.area_name AND k.folder_name in('docks','location','parking')),'') area_type"+
						"			from "+ConfigBean.getStringValue("folder_from_kml")+" t " +
						"			where t.folder_name = 'area' " ;
						
			DBRow para  = new DBRow();
			para.add("area_psid1",storage.get("id", 0));
			return dbUtilAutoTran.selectPreMutliple(name, para);
		} catch (SQLException e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc.getAllAreaInfoForStorage():"+e);
		}
	}
	/**
	 * 获取location
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLocation(DBRow storage) throws Exception{
		try{
			String location = "SELECT f.area_name area_name ,f.placemark_name, f.source area_kml , f.is_three_dimensional is3D, f.ip, f.port, f.user, f.password, " +
					" f.x, f.y, f.width, f.height, f.angle, f.latlng," +
					"IFNULL((SELECT s1.area_id from "+ConfigBean.getStringValue("storage_location_area")+" s1 where s1.area_psid = ? and s1.area_name = f.area_name and folder_name = 'Location'),0) area_id, "+
					" IFNULL(( SELECT s.slc_id " +
					"			FROM "+ConfigBean.getStringValue("storage_location_catalog")+" s,"+ConfigBean.getStringValue("storage_location_area")+" sla " +
					"			WHERE s.slc_area = sla.area_id " +
					"			AND sla.area_name = f.area_name " +
					"			AND f.placemark_name = s.slc_position " +
					"			AND sla.area_psid = ?),0) slc_id "+
					"	from "+ConfigBean.getStringValue("folder_from_kml")+" f  " +
					"	where f.folder_name = 'Location'  " ;
			DBRow para  = new DBRow();
			para.add("area_psid",storage.get("id", 0));
			para.add("area_psid1",storage.get("id", 0));
			return dbUtilAutoTran.selectPreMutliple(location, para);
		} catch (SQLException e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc.getLocation():"+e);
		}
	}
	/**
	 * 获取Docks
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDocks(DBRow storage) throws Exception{
		try{
			String docks =  "SELECT t.placemark_name , t.area_name , t.source area_kml, t.ip, t.port, t.user, t.password, " +
							"t.x, t.y, t.width, t.height, t.angle, t.latlng," +
							"IFNULL((SELECT s1.area_id from "+ConfigBean.getStringValue("storage_location_area")+" s1 where s1.area_psid = ? and s1.area_name = t.area_name and t.folder_name = 'Docks'),0) area_id, " +
							" IFNULL((SELECT s2.sd_id from storage_door s2 where s2.doorId = t.placemark_name and s2.ps_id = ?),0) door_id "+
							"	from "+ConfigBean.getStringValue("folder_from_kml")+" t  " +
							"	where t.folder_name = 'Docks'";
			DBRow para  = new DBRow();
			para.add("ps_id",storage.get("id", 0));
			para.add("ps_id1",storage.get("id", 0));
			return dbUtilAutoTran.selectPreMutliple(docks, para);
		} catch (SQLException e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc.getDocks():"+e);
		}
	}
	/**
	 * 获取Staging
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStaging(DBRow storage) throws Exception{
		try{	
			String staging ="SELECT t.placemark_name , t.area_name door_name, t.source area_kml, t.ip, t.port, t.user, t.password," +
							"t.x, t.y, t.width, t.height, t.angle, t.latlng," +
							" IFNULL((SELECT s1.sd_id from "+ConfigBean.getStringValue("storage_door")+" s1 where s1.ps_id = ? and s1.doorId = t.area_name and t.folder_name = 'Staging'),0) door_id,"+
							" IFNULL((SELECT s2.id FROM storage_load_unload_location s2  " +
							"						where s2.location_name = t.placemark_name  " +
							"						and s2.psc_id =? ),0) staging_id " +
							"  from "+ConfigBean.getStringValue("folder_from_kml")+" t  " +
							" where t.folder_name = 'Staging'" ;
			DBRow para  = new DBRow();
			para.add("psc_id",storage.get("id", 0));
			para.add("psc_id1",storage.get("id", 0));
			return dbUtilAutoTran.selectPreMutliple(staging, para);
		} catch (SQLException e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc.getStaging():"+e);
		}
	}
	/**
	 * 获取parking
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getParking(DBRow storage) throws Exception{
		try{	
			String parking ="SELECT t.placemark_name , t.area_name , t.source area_kml, t.ip, t.port, t.user, t.password," +
							"	t.x, t.y, t.width, t.height, t.angle, t.latlng," +
							"					IFNULL((SELECT s1.area_id from storage_location_area s1 where s1.area_psid = ? and s1.area_name = t.area_name and t.folder_name = 'Parking'),0) area_id ," +
							"					IFNULL((SELECT s.yc_id FROM storage_yard_control s" +
							"											where s.yc_no = t.placemark_name  " +
							"											and s.ps_id =? ),0) parking_id" +
							"	from "+ConfigBean.getStringValue("folder_from_kml") + " t  " +
							"	where t.folder_name = 'parking'";
			DBRow para  = new DBRow();
			para.add("ps_id1",storage.get("id", 0));
			para.add("ps_id2",storage.get("id", 0));
			return dbUtilAutoTran.selectPreMutliple(parking, para);
		} catch (SQLException e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc.getParking():"+e);
		}
	}
	/**
	 * 
	 * @param storage
	 * @return
	 * @throws Exception
	 */
	private DBRow[]  getWebcam(DBRow storage) throws Exception {
		
		
		String webcam =
				"Select t.placemark_name , t.area_name ," +
				" t.source area_kml, t.ip, t.port, t.user, t.password," +
				" t.x, t.y, t.width, t.height, t.ps_id,t.latlng, " +
				"IFNULL((select w.id from webcam w where w.port=t.port and w.ip = t.ip and w.ps_id =? and t.folder_name = 'webcam'),0) id  from " +
				ConfigBean.getStringValue("folder_from_kml") +
				" t where t.folder_name = 'webcam' " +
				"and t.ps_id =?  ";
		DBRow para  = new DBRow();
		para.add("ps_id1",storage.get("id", 0));
		para.add("ps_id2",storage.get("id", 0));
		try {
			return dbUtilAutoTran.selectPreMutliple(webcam, para);
		} catch (SQLException e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc.getWebcam():"+e);
		} 
	}
	/**
	 * 保存停车位
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addStorageYardControl(DBRow row) throws Exception{
		try{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("storage_yard_control"), row);
		}catch (SQLException e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc addStorageYardControl error:" +e);
		}
	}
	/**
	 * 保存area和door关系
	 * @param areaId
	 * @param sdId
	 * @return
	 * @throws Exception
	 */
	public long addStorageAreaDoor(long areaId, long sdId) throws Exception{
		try {
			long id = 0l;
			String table = ConfigBean.getStringValue("storage_area_door");
			String sql = "select 1 from "+table+" s where s.area_id="+areaId+" and s.sd_id="+sdId;
			DBRow[] rows = dbUtilAutoTran.selectMutliple(sql);
			if(rows.length==0){
				DBRow r = new DBRow();
				r.add("area_id", areaId);
				r.add("sd_id", sdId);
				id = dbUtilAutoTran.insertReturnId(table, r);
			}
			return id;
		} catch (Exception e) {
			throw new Exception("FloorLocationAreaXmlImportMgrCc addStorageAreaDoor error:" +e);
		}
	}
}
