package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorContainerProductMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加托盘商品
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addContainerProduct(DBRow dbrow)
		throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("container_product"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerProductMgrZJ addContainerProduct error:"+e);
		}
	}
	
	/**
	 * 根据托盘ID删除托盘商品
	 * @param lp_id
	 * @throws Exception
	 */
	public void delContainerProductByLP(long lp_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where cp_lp_id ="+lp_id,ConfigBean.getStringValue("container_product"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerProductMgrZJ delContainerProduct error:"+e);
		}
	}
	
	public void delContainerProduct(long cp_id)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where cp_id="+cp_id,ConfigBean.getStringValue("container_product"));
		}
		catch (Exception e) 
		{
			throw new Exception();
		}
		
	}
	
	/**
	 * 根据序列号获得托盘商品
	 * @param sn
	 * @return
	 * @throws Exception
	 */
	public DBRow getContainerProductBySN(String sn)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("container_product")+" where cp_sn = ?";
			
			DBRow para = new DBRow();
			para.add("cp_sn",sn);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerProductMgrZJ getContainerProductBySN error:"+e);
		}
	}
	
	public DBRow getDetailContainerByContainer(String container)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("container")+" where container = ?";
			
			DBRow para = new DBRow();
			para.add("container",container);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerProducutMgrZJ getDetailContainerProduct error:"+e);
		}
	}
	
	public DBRow getDetailContai(String searchKey)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("container")+" where container = ? or con_id = ?";
		
		DBRow para = new DBRow();
		para.add("container",searchKey);
		para.add("con_id",searchKey);
		
		return dbUtilAutoTran.selectPreSingle(sql, para);
	}
	
	/**
	 * 根据托盘ID获得托盘上的商品
	 * @param lp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getContainerProductByContainer(long lp_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("container_product")+" as cp "
						+"join "+ConfigBean.getStringValue("product")+" as p on cp.cp_pc_id = p.pc_id "
						+" where cp.cp_lp_id = ? ";
			
			DBRow para  = new DBRow();
			para.add("lp_id",lp_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerProductMgrZJ getContainerProductByContainer error:"+e);
		}
	}
	
	/**
	 * 获得托盘商品信息
	 * @param lp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductForContainer(long lp_id)
		throws Exception
	{
		try 
		{
			String sql = "select p.* from "+ConfigBean.getStringValue("product")+" as p "
						+"join "+ConfigBean.getStringValue("container_product")+" as cp on cp.cp_pc_id = p.pc_id and cp.cp_lp_id = ? ";
			
			DBRow para = new DBRow();
			para.add("cp_lp_id",lp_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorContainerProductMgrZJ getProductForContainer error:"+e);
		}
	}
	
	/**
	 * 获得托盘商品信息
	 * @param lp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductCodeForContainer(long lp_id)
		throws Exception
	{
		try
		{
			String sql = "select pc.* from "+ConfigBean.getStringValue("product_code")+" pc "
						+"join "+ConfigBean.getStringValue("product")+" as p on pc.pc_id = p.pc_id "
						+"join "+ConfigBean.getStringValue("container_product")+" as cp on cp.cp_pc_id = p.pc_id and cp.cp_lp_id = ? ";
			
			DBRow para = new DBRow();
			para.add("cp_lp_id",lp_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerProductMgrZJ getProductCodeForContainer error:"+e);
		}
	}
	
	/**
	 * 获得托盘上的商品的序列号
	 * @param lp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSerialProductForContainer(long lp_id)
		throws Exception
	{
		try 
		{
			String sql = "select sp.* from "+ConfigBean.getStringValue("serial_product")+" as sp "
						+"join "+ConfigBean.getStringValue("product")+" as p on p.pc_id = sp.pc_id "
						+"join "+ConfigBean.getStringValue("container_product")+" as cp on p.pc_id = cp.cp_pc_id and cp.cp_lp_id = ? "
//						+"where sp.in_time is is null and out_time is null "
						+"order by p.pc_id";
			
			DBRow para = new DBRow();
			para.add("cp_lp_id",lp_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorContainerProductMgrZJ getSerialProductForContainer error:"+e);
		}
	}
	
	public DBRow getDetailContainerByContainerId(long lpId)
	throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("container")+" where con_id = ?";
			
			DBRow para = new DBRow();
			para.add("con_id",lpId);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorContainerProducutMgrZJ getDetailContainerProductById error:"+e);
		}
	}
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
