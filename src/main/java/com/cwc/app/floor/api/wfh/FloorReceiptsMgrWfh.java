package com.cwc.app.floor.api.wfh;

import com.cwc.app.key.LineStatueKey;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

/**
 * @author 	Wfh
 * 2015年3月4日
 *
 */
public class FloorReceiptsMgrWfh{
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	//添加pallet type
	public long addContainer(DBRow row) throws Exception{
		try {
			return dbUtilAutoTran.insertReturnId("container", row);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.addContainer(DBRow param) error:" + e);
		}
	}
	/**
	 * 添加container config信息
	 * 
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public long addContainerConfig(DBRow data) throws Exception{
		try {
			return dbUtilAutoTran.insertReturnId("container_config", data);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.addContainer(DBRow param) error:" + e);
		}
	}
	
	//添加pallet 与line的关系
	public long addReceiptRelContainer(DBRow row) throws Exception{
		try {
			return dbUtilAutoTran.insertReturnId("receipt_rel_container", row);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.addContainerLine(DBRow param) error:" + e);
		}
	}
	/**
	 * 更新detial 状态
	 * @param receipt_line_id
	 * @param upReceipt
	 * @return
	 */
	public long updateDetail(long detail_id, DBRow upRow) throws Exception{
		try {
			return dbUtilAutoTran.update("where dlo_detail_id ="+detail_id, "door_or_location_occupancy_details", upRow);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.updateDetail(long receipt_line_id, DBRow upReceipt) error:" + e);
		}
	}
	/**
	 * 删除 detailResources表
	 * @param receipt_line_id
	 * @param upReceipt
	 * @return
	 */
	public long deleteDetailResources(long detail_res_id) throws Exception{
		try {
			return dbUtilAutoTran.delete("where srr_id="+detail_res_id, "space_resources_relation");
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.deleteDetailResources(long receipt_line_id, DBRow upReceipt) error:" + e);
		}
	}
	/**
	 * 更新receipt_line
	 * @param receipt_line_id
	 * @param upReceipt
	 * @return
	 */
	public long updateReceiptLine(long receipt_line_id, DBRow upReceipt) throws Exception{
		try {
			return dbUtilAutoTran.update("where receipt_line_id = "+receipt_line_id, "receipt_lines", upReceipt);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.updateReceiptLine(long receipt_line_id, DBRow upReceipt) error:" + e);
		}
	}
	/**
	 * 更新receipt_line
	 * @param receipt_line_id
	 * @param upReceipt
	 * @return
	 */
	public long updateReceiptRelContainerByConID(long con_id, DBRow upReceipt) throws Exception{
		try {
			return dbUtilAutoTran.update("where plate_no = "+con_id, "receipt_rel_container", upReceipt);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.updateReceiptRelContainerByConID(long receipt_line_id, DBRow upReceipt) error:" + e);
		}
	}
	/**
	 * edit by zhengziqi at 11th April, 2015
	 * 返回一个数组而不是单个DBRow
	 * 
	 * 通过line_id查询pallets  传入3查询全部
	 * @param receipt_line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findNoStandardConfigByLineID(long receipt_line_id) throws Exception{
		try {
			DBRow row = null;
			String sql = "SELECT cc.* FROM receipt_rel_container rrc JOIN container_config cc ON cc.container_config_id = rrc.container_config_id "
					+ " WHERE cc.goods_type !=2 AND cc.width*cc.height*cc.length =0 AND rrc.receipt_line_id ="+receipt_line_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findNoStandardConfigByLineID error:" + e);
		}
	}
	/**
	 * 通过company_id 查询ps_id
	 * @param receipt_line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findPSIDByCompanyId(String company_id) throws Exception{
		try {
			String sql = "SELECT * FROM config_search_warehouse_location cl WHERE cl.company_id ='"+company_id+"'";
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findPSIDByCompanyId error:" + e);
		}
	}
	
	/**
	 * 通过line_id查询pallets  传入3查询全部
	 * @param receipt_line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPalletByLineId(long receipt_line_id,long cc_id,int goods_type) throws Exception{
		try {
			String sql = "SELECT DISTINCT con.status,ifnull(ci.customer_name,ci.customer_id) AS customer_id,con.detail_id as con_detail_id"
					+ " ,con.con_id, con.location,IFNULL(con.location_type,1) as location_type , con.container as container_no ,con.item_id"
					+ " ,con.lot_number AS lot_no,con.container_type ,cc.config_type,cc.length"
					+ " ,cc.width,cc.height,cc.pallet_type,cc.note,a.adid,cc.create_date,cc.goods_type"
					+ " ,a.employe_name,(cc.length*cc.height*cc.width) AS config_qty "
					+ " , cp.cp_quantity, rrc.*,p_user.employe_name as print_user_name"
					+ " FROM receipt_rel_container rrc "
					+ " JOIN container con ON rrc.plate_no = con.con_id AND con.is_delete=0"
					+ " JOIN container_config cc ON cc.container_config_id = rrc.container_config_id "
					+ " LEFT JOIN container_product cp ON cp.cp_lp_id = con.con_id "
					+ " LEFT JOIN admin a ON cc.create_user = a.adid"
					+ " LEFT JOIN admin p_user ON rrc.print_user = p_user.adid"
					+ " LEFT JOIN customer_id ci ON ci.customer_key = con.customer_id"
					+ " WHERE rrc.receipt_line_id="+receipt_line_id;
			if(goods_type != 3){
				sql += " AND cc.goods_type="+goods_type;
			}
			if(cc_id>0){
				sql += " AND rrc.container_config_id="+cc_id;
			}
			sql += " ORDER BY con.`status` ASC ,con.con_id desc";
			System.out.println(sql);
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findPalletByLineId error:" + e);
		}
	}
	/**
	 * 通过con_ids查询pallets  传入3查询全部
	 * @param receipt_line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPalletByConIDS(String con_ids) throws Exception{
		try {
			String sql = "SELECT DISTINCT lpt.lp_name ,con.con_id,con.container as container_no,con.container_type,con.item_id,con.lot_number AS lot_no,con.location "
					+ " ,cc.container_config_id,cc.config_type,cc.receipt_line_id,cc.length"
					+ " ,cc.width,cc.height,cc.pallet_type,cc.note,a.adid,cc.create_date,cc.goods_type"
					+ " ,a.employe_name,(cc.length*cc.height*cc.width) AS config_qty "
					+ " , cp.cp_quantity, rrc.*,p_user.employe_name as print_user_name"
					+ " FROM receipt_rel_container rrc "
					+ " JOIN container con ON rrc.plate_no = con.con_id "
					+ " JOIN container_config cc ON cc.container_config_id = rrc.container_config_id "
					+ " LEFT JOIN license_plate_type lpt ON cc.license_plate_type_id = lpt.lpt_id "
					+ " LEFT JOIN container_product cp ON cp.cp_lp_id = con.con_id "
					+ " LEFT JOIN admin a ON cc.create_user = a.adid"
					+ " LEFT JOIN admin p_user ON rrc.print_user = p_user.adid"
					+ " WHERE rrc.plate_no in("+con_ids+")";
			sql += " ORDER BY con.con_id desc";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findPalletByConIDS error:" + e);
		}
	}
	/**
	 * 通过palletId查找line
	 * @param pallet_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findSNByPalletId(long pallet_id) throws Exception{
		try {
			String sql = "SELECT sp.* FROM serial_product sp WHERE sp.at_lp_id ="+pallet_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findSNByPalletId error:" + e);
		}
	}
	/**
	 * 通过lineId查找sn
	 * @param pallet_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findSNByLineId(long line_id,int goods_type,int is_damage) throws Exception{
		try {
			String sql = "SELECT sp.*,cc.goods_type FROM receipt_rel_container rrc JOIN container_config cc ON cc.container_config_id = rrc.container_config_id AND cc.is_delete = 0 "
					+ " JOIN serial_product sp ON sp.at_lp_id = rrc.plate_no JOIN container c ON rrc.plate_no = c.con_id AND c.is_delete = 0 WHERE rrc.receipt_line_id ="+line_id;
			if(is_damage!=3){
				sql += " AND sp.is_damage="+is_damage;
			}
			if(goods_type!=3){
				sql += " AND cc.goods_type="+goods_type;
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findSNByLineId error:" + e);
		}
	}
	/**
	 * 通过lineId查找location
	 * @param pallet_id
	 * @return
	 * @throws Exception
	 * @update by zhaoyy ,only search tlp  location  AND con.container_type = 3
	 */
	public DBRow findLocationByLineId(long line_id) throws Exception{
		try {
			
			String sql = "SELECT GROUP_CONCAT(DISTINCT con.location separator ', ') AS location FROM receipt_rel_container rrc"
					+ " JOIN container con on con.con_id = rrc.plate_no AND con.container_type = 3 WHERE rrc.receipt_line_id ="+line_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findLocationByLineId error:" + e);
		}
	}
	
	/**
	 *  验证sn是否在receipt下重复
	 * @param receiptId
	 * @param sn
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findSameSNBySNNumber(long receiptId, String sn,String lot_no,String item_id) throws Exception{
		try {
			String sql = "SELECT sp.* FROM receipt_lines rl "
					+ " JOIN receipt_rel_container rrc ON rrc.receipt_line_id = rl.receipt_line_id JOIN serial_product sp ON sp.at_lp_id = rrc.plate_no"
					+ " WHERE rl.lot_no = '"+lot_no+"' AND rl.item_id  ='"+item_id+"' AND sp.serial_number = '"+sn+"' AND rl.receipt_id ="+receiptId +" limit 0,1";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findSameSNBySNNumber error:" + e);
		}
		
	}
	/**
	 *  验证sn是否在receipt下重复
	 * @param receiptId
	 * @param sn
	 * @return
	 * @throws Exception
	 */
	public DBRow findSameSN(long receiptId, String sn,String lot_no,String item_id) throws Exception{
		try {
			String sql = "SELECT sp.* FROM receipt_lines rl "
					+ " JOIN receipt_rel_container rrc ON rrc.receipt_line_id = rl.receipt_line_id JOIN serial_product sp ON sp.at_lp_id = rrc.plate_no"
					+ " WHERE rl.lot_no = '"+lot_no+"' AND rl.item_id  ='"+item_id+"' AND sp.serial_number = '"+sn+"' AND rl.receipt_id ="+receiptId +" limit 0,1";
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findSameSN error:" + e);
		}
		
	}
	/**
	 *  验证sn是否在line下重复
	 * @param receiptId
	 * @param sn
	 * @return
	 * @throws Exception
	 */
	public DBRow findSameSNByLine(long line_id) throws Exception{
		try {
			String sql = "SELECT sp.*,rrc.receipt_line_id FROM receipt_rel_container rrc JOIN serial_product sp ON sp.at_lp_id = rrc.plate_no JOIN container c ON c.con_id = rrc.plate_no AND c.is_delete = 0 "
					+ " WHERE sp.is_repeat =1 AND rrc.receipt_line_id  = "+line_id +" limit 0,1";
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findSameSNByLine error:" + e);
		}
		
	}
	
	/**
	 *
	 * find Receipt_Lines And Title By LineId
	 * 2015年5月14日
	 * @param line_id
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow findReceiptLineAndTitleByLineId(long line_id) throws Exception{
		try{
			StringBuffer sb = new StringBuffer(" select rl.*, r.supplier_id, t.title_id, c.customer_key from receipt_lines rl ");
			sb.append(" left join receipts r on rl.receipt_id = r.receipt_id ")
			.append(" left join title t on t.title_name = r.supplier_id ")
			.append(" left join customer_id c on c.customer_id = r.customer_id ")
			.append(" where rl.receipt_line_id= ")
			.append(line_id)
			.append(" group by rl.receipt_line_id ");
			return dbUtilAutoTran.selectSingle(sb.toString());
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findReceiptLineByLineId error:" + e);
		}
	}
	
	public DBRow findReceiptLineByLineId(long line_id) throws Exception{
		try{
			String sql = "select rl.* from receipt_lines rl where rl.receipt_line_id="+line_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findReceiptLineByLineId error:" + e);
		}
	}
	
	/**
	 * 绑定qty与pallet
	 * @param con_id
	 * @param para
	 * @return
	 * @throws Exception
	 */
	public long bindQTYToPallet(DBRow para) throws Exception{
		try {
			return dbUtilAutoTran.insertReturnId("container_product", para);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.bindQTYToPallet error:" + e);
		}
	}
	/**
	 * 绑定SN与pallet
	 * @param con_id
	 * @param para
	 * @return
	 * @throws Exception
	 */
	public long bindSNToPallet(DBRow para) throws Exception{
		try {
			return dbUtilAutoTran.insertReturnId("serial_product", para);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.bindSNToPallet error:" + e);
		}
	}
	/**
	 * 更新serial_product
	 * @param con_id
	 * @param para
	 * @return
	 * @throws Exception
	 */
	public long updateSerialProduct(long sp_id,DBRow para) throws Exception{
		try {
			return dbUtilAutoTran.update("where serial_product_id = "+sp_id, "serial_product", para);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.updateSerialProduct error:" + e);
		}
	}
	/**
	 * 按照palletId查找line
	 * @param con_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findReceiptLineByPalletId(long con_id) throws Exception{
		try{
			String sql = "SELECT rl.* FROM container con JOIN receipt_rel_container rrc ON con.con_id = rrc.plate_no JOIN receipt_lines rl ON rl.receipt_line_id = rrc.receipt_line_id WHERE con.con_id = "+con_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findReceiptLineByPalletId error:" + e);
		}
	}
	/**
	 * 删除某一条sn
	 * @param cp_id
	 * @return
	 * @throws Exception
	 */
	public long deleteSNBySPId(long sp_id) throws Exception{
		try{
			return dbUtilAutoTran.delete("where serial_product_id="+sp_id, "serial_product");
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.deleteSNBySPId error:" + e);
		}
	}
	/**
	 * 删除line下的sn
	 * @param cp_id
	 * @return
	 * @throws Exception
	 */
	public void deleteSNByLineId(long line_id) throws Exception{
		try{
			String sql = "DELETE sp.* FROM receipt_rel_container rrc JOIN serial_product sp ON sp.at_lp_id = rrc.plate_no WHERE rrc.receipt_line_id  ="+line_id;
			dbUtilAutoTran.executeSQL(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.deleteSNByLineId error:" + e);
		}
	}
	/**
	 * 删除pallet下的sn
	 * @param cp_id
	 * @return
	 * @throws Exception
	 */
	public long deleteSNByConID(long con_id) throws Exception{
		try{
			return dbUtilAutoTran.delete("where at_lp_id = "+con_id, "serial_product");
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.deleteSNByConID error:" + e);
		}
	}
	
	/**
	 * 删除qty
	 * @param cp_id
	 * @return
	 * @throws Exception
	 */
	public long deleteContainerProductByCpId(long cp_id) throws Exception{
		try{
			return dbUtilAutoTran.delete("where cp_id="+cp_id, "container_product");
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.deleteContainerProductByCpId error:" + e);
		}
	}
	/**
	 * 删除qty
	 * @param cp_id
	 * @return
	 * @throws Exception
	 */
	public long deleteContainerProductByConIDs(String con_ids) throws Exception{
		try{
			return dbUtilAutoTran.delete("where cp_lp_id in("+con_ids+")", "container_product");
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.deleteContainerProductByConIDs error:" + e);
		}
	}
	/**
	 * 删除pallet
	 * @param cp_id
	 * @return
	 * @throws Exception
	 */
	public long deleteContainerByConId(long con_id) throws Exception{
		try{
			return dbUtilAutoTran.delete("where con_id="+con_id, "container");
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.deleteContainerByConId error:" + e);
		}
	}
	/**
	 * 删除pallet_config
	 * @param cp_id
	 * @return
	 * @throws Exception
	 */
	public long deleteContainerConfigByConConfigId(long container_config_id) throws Exception{
		try{
			return dbUtilAutoTran.delete("where container_config_id="+container_config_id, "container_config");
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.deleteContainerConfigByConConfigId error:" + e);
		}
	}
	/**
	 * 删除与line的关系
	 * @param cp_id
	 * @return
	 * @throws Exception
	 */
	public long deleteReceRelConByConConfigId(long container_config_id) throws Exception{
		try{
			return dbUtilAutoTran.delete("where container_config_id="+container_config_id, "receipt_rel_container");
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.deleteReceRelConByConConfigId error:" + e);
		}
	}
	
	
	/**
	 * 查找这种配置下所有托盘的sn
	 * @param cp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findSNByConConfigId(long container_config_id) throws Exception{
		try{
			String sql = "SELECT sp.*,con.process FROM receipt_rel_container rrc"
					+ " JOIN container con ON con.con_id=rrc.plate_no AND con.is_delete=0"
					+ " JOIN serial_product sp ON sp.at_lp_id = con.con_id"
			 		+ " WHERE rrc.container_config_id =" + container_config_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findSNByConConfigId error:" + e);
		}
	}
	/**
	 * 查找sn
	 * @param cp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findSNBySpID(long sp_id) throws Exception{
		try{
			String sql = "SELECT sp.* FROM serial_product sp WHERE sp.serial_product_id =" + sp_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findSNBySpID error:" + e);
		}
	}
	/**
	 * 查找sn
	 * @param cp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findSNBySN(String sn) throws Exception{
		try{
			String sql = "SELECT sp.* FROM serial_product sp WHERE sp.serial_number =" + sn;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findSNBySN error:" + e);
		}
	}
	/**
	 * 查找这种配置下所有托盘 count qty
	 * @param cp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findCountQTYByConConfigId(long container_config_id) throws Exception{
		try{
			String sql = "SELECT cp.*,con.process FROM receipt_rel_container rrc"
					+ " JOIN container con ON con.con_id = rrc.plate_no AND con.is_delete=0"
					+ " JOIN container_product cp ON con.con_id = cp.cp_lp_id WHERE rrc.container_config_id = "+container_config_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findCountQTYByConConfigId error:" + e);
		}
	}
	/**
	 * 查找这种配置下所有托盘 
	 * @param cp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findContainerByConConfigId(long container_config_id) throws Exception{
		try{
			String sql = "SELECT con.* FROM receipt_rel_container rrc"
					+ " JOIN container con ON con.con_id = rrc.plate_no AND con.is_delete=0"
					+ " WHERE rrc.container_config_id = "+container_config_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findContainerByConConfigId error:" + e);
		}
	}
	
	/**
	 * 查找container_product  palletId
	 * @param palletId
	 * @return
	 * @throws Exception
	 */
	public DBRow findContainerProductByPalletId(long palletId) throws Exception{
		try{
			String sql ="SELECT * FROM container_product cp where cp.cp_lp_id = "+palletId;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findContainerProductByPalletId error:" + e);
		}
	}
	
	/**
	 * 按照palletId查找line下面已扫描的qty
	 * @param con_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findCPQuantityByConId(long con_id) throws Exception{
		try{
			String sql = " SELECT sum(cp.cp_quantity) cp_quantity FROM container con JOIN receipt_rel_container rrc ON rrc.plate_no = con.con_id JOIN container_product cp ON cp.cp_lp_id = con.con_id"
					+ " WHERE rrc.receipt_line_id = (SELECT rrc.receipt_line_id FROM receipt_rel_container rrc WHERE rrc.plate_no = "+con_id+") AND cp.cp_sn IS NULL";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findCPQuantityByConId error:" + e);
		}
	}
	/**
	 * 按照line_id查找line下面已扫描的qty
	 * @param con_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findCPQuantityByLineId(long line_id) throws Exception{
		try{
			String sql = " SELECT sum(cp.cp_quantity) cp_quantity FROM container con JOIN receipt_rel_container rrc ON rrc.plate_no = con.con_id"
					+ " JOIN container_product cp ON cp.cp_lp_id = con.con_id"
					+ " WHERE rrc.receipt_line_id = "+line_id+" AND cp.cp_sn IS NULL";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findCPQuantityByConId error:" + e);
		}
	}
	/**
	 * 查找line下面config过的pallet total
	 * @param con_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findSUMConfigByLineId(long line_id,int goods_type) throws Exception{
		try{
			String sql = " select  sum(config_total) as number from ( SELECT cc.length*cc.width*cc.height AS config_total"
					+ " FROM receipt_rel_container rrc , container_config cc, container c"
					+ " WHERE rrc.container_config_id = cc.container_config_id and rrc.plate_no = c.con_id  and rrc.receipt_line_id =  "+line_id;
			if(goods_type!=3){
				sql += " AND cc.goods_type="+goods_type;
			}
			sql+= " group by plate_no ) as qty_numb";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findCPQuantityByConId error:" + e);
		}
	}
	
	/**
	 *
	 * 查找line下面config过的pallet total
	 * 2015年5月12日
	 * @param line_id
	 * @param goods_type
	 * @param is_clp
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow findSUMConfigByLineId(long line_id,int goods_type, Boolean is_clp) throws Exception{
		try{
			String sql = " select  sum(config_total) as number from ( SELECT cc.length*cc.width*cc.height AS config_total"
					+ " FROM receipt_rel_container rrc , container_config cc, container c"
					+ " WHERE rrc.container_config_id = cc.container_config_id and rrc.plate_no = c.con_id  and rrc.receipt_line_id =  "+line_id;
			if(goods_type!=3){
				sql += " AND cc.goods_type="+goods_type;
			}
			if(is_clp){
				sql += " AND c.container_type= 1 ";
			}else{
				sql += " AND c.container_type= 3 ";
			}
			sql+= " group by plate_no ) as qty_numb";
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findCPQuantityByConId error:" + e);
		}
	}
	
	/**
	 * 查找pallet的congi信息
	 * @param con_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findContainerConfigByConID(long con_id) throws Exception{
		try{
			String sql = " SELECT cc.* FROM container con JOIN receipt_rel_container rrc ON con.con_id = rrc.plate_no"
					+ " JOIN container_config cc ON cc.container_config_id =  rrc.container_config_id"
					+ " WHERE con.con_id="+con_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findContainerConfigByConID error:" + e);
		}
	}
	/**
	 * 查找pallet的receiptRelContainer
	 * @param con_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findReceiptRelContainerByConID(long con_id) throws Exception{
		try{
			String sql = " SELECT rrc.* FROM receipt_rel_container rrc WHERE rrc.plate_no ="+con_id;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findReceiptRelContainerByConID error:" + e);
		}
	}
	
	/**
	 * 添加通知
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addSchedule(DBRow row) throws Exception{
		try{
			return dbUtilAutoTran.insertReturnId("schedule", row);
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.addSchedule error:"+e);
		}
	}
	/**
	 * 添加详细通知人
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addScheduleSub(DBRow row) throws Exception{
		try{
			return dbUtilAutoTran.insertReturnId("schedule_sub", row);
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.addScheduleSub error:"+e);
		}
	}
			
	/**
	 * android查找task列表
	 * @param row
	 * @return
	 * @throws Exception
	 * 去掉 and rl.status !="+LineStatueKey.STATUS_CLOSE 
	 */
	public DBRow[] findTaskAndroid(int associate_type,int associate_process,long adid) throws Exception{
		try{
			String sql = "SELECT rl.*,assignUser.employe_name AS create_user,sc.create_time FROM `schedule` sc JOIN schedule_sub ss ON ss.schedule_id = sc.schedule_id "
					+ " JOIN receipt_lines rl ON rl.receipt_line_id = sc.associate_id "
					+ "	LEFT JOIN admin assignUser ON assignUser.adid = sc.assign_user_id"
					+ " WHERE sc.schedule_state != 10 AND sc.associate_type = "+associate_type+" AND sc.associate_process = "+associate_process
					+ " AND ss.schedule_execute_id = "+adid +" ORDER BY sc.create_time";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findTaskAndroid error:"+e);
		}
	}
	
	/**
	 * 查找superVisoer的line任务 用来关闭这个任务
	 * @param adid 
	 * @param line_id
	 * @param associate_type
	 * @param associate_process
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findSheduleByLineID(long line_id,int associate_type,int associate_process) throws Exception{
		try{
			String sql = "SELECT sc.*,ss.schedule_sub_id,assignUser.employe_name,sc.create_time FROM `schedule` sc JOIN schedule_sub ss ON ss.schedule_id = sc.schedule_id "
					+ " JOIN receipt_lines rl ON rl.receipt_line_id = sc.associate_id"
					+ "	LEFT JOIN admin assignUser ON assignUser.adid = sc.assign_user_id"
					+ " WHERE sc.schedule_state != 10 AND sc.associate_type = "+associate_type+" AND sc.associate_process = "+associate_process
					+ " AND sc.associate_id="+line_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findSheduleByLineID error:"+e);
		}
	}
	/**
	 * 查找superVisoer的line任务 用来关闭这个任务
	 * @param adid 
	 * @param line_id
	 * @param associate_type
	 * @param associate_process
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findSheduleSubByScheduleID(long schedule_id) throws Exception{
		try{
			String sql = "SELECT ss.* FROM schedule_sub ss "
					+ " WHERE ss.schedule_id="+schedule_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findSheduleSubByScheduleID error:"+e);
		}
	}
	
	/**
	 * 关闭supervisor的schedule任务
	 * @param schedule_id
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long closeSchedule(long schedule_id,DBRow row) throws Exception{
		try{
			return dbUtilAutoTran.update("where schedule_id ="+schedule_id, "schedule", row);
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.closeSchedule error:"+e);
		}
	}
	/**
	 * 关闭supervisor的schedule_sub任务
	 * @param schedule_id
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long closeScheduleSub(long schedule_sub_id,DBRow row) throws Exception{
		try{
			return dbUtilAutoTran.update("where schedule_sub_id ="+schedule_sub_id, "schedule_sub", row);
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.closeScheduleSub error:"+e);
		}
	}
	
	/**
	 * 查找palletConfig  传入3查询全部
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPalletConfigByLineId(long line_id,int goods_type, int is_clp) throws Exception{
		try{
			String sql = "SELECT DISTINCT cc.*, count(rrc.receipt_rel_container_id) AS plate_number"
					+ " FROM receipt_rel_container rrc JOIN container_config cc ON cc.container_config_id = rrc.container_config_id"
					+ " JOIN container c ON c.con_id = rrc.plate_no"
					+ " WHERE c.is_delete = 0 AND cc.is_delete = 0 AND rrc.receipt_line_id = "+line_id;
			if(goods_type!=3){
				sql += "  AND cc.goods_type = "+goods_type;
			}
			//added by zhengziqi at May.12th 2015
			//for cc task
			if(is_clp == 0){
				sql += "  AND cc.license_plate_type_id is NULL";
			}else{
				sql += "  AND cc.license_plate_type_id > 0";
			}
			sql +=  " GROUP BY cc.container_config_id ORDER BY cc.create_date desc";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findPalletConfigByLineId error:"+e);
		}
	}
	/**
	 * 查找serialNumber
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findSNByConIds(String con_ids) throws Exception{
		try{
			String sql = "SELECT * FROM serial_product sp WHERE sp.at_lp_id in("+con_ids+")";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findSNByConIds error:"+e);
		}
	}
	/**
	 * 查找已经扫描的sn个数
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow findSNCountByLineId(long line_id) throws Exception{
		try{
			String sql = "SELECT count(sp.serial_product_id) AS sn_count FROM receipt_rel_container rrc "
					+ " JOIN serial_product sp ON sp.at_lp_id = rrc.plate_no WHERE rrc.receipt_line_id ="+line_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findSNCountByLineId error:"+e);
		}
	}
	/**
	 * 查找已经扫描的pallet
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findScanPalletByLineId(long line_id) throws Exception{
		try{
			String sql = "SELECT sp.* FROM receipt_rel_container rrc JOIN serial_product sp ON sp.at_lp_id = rrc.plate_no"
					+ " WHERE rrc.receipt_line_id = "+line_id+" GROUP BY rrc.plate_no";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findScanPalletByLineId error:"+e);
		}
	}
	
	/**
	 * 查找countQty
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findCountQTYByConIds(String con_ids) throws Exception{
		try{
			String sql = "SELECT DISTINCT * FROM container_product cp WHERE cp.cp_lp_id IN("+con_ids+")";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findCountQTYByConIds error:"+e);
		}
	}
	
	/**
	 * 删除一些container
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long deleteContainerByConIds(String con_ids) throws Exception{
		try{
			return dbUtilAutoTran.delete("where con_id in("+con_ids+")", "container");
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.deleteContainerByConIds error:"+e);
		}
	}
	
	/**
	 * 删除一些container
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long deleteReceiptContainerByConIds(String con_ids) throws Exception{
		try{
			return dbUtilAutoTran.delete("where plate_no in("+con_ids+")", "receipt_rel_container");
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.deleteReceiptContainerByConIds error:"+e);
		}
	}
	
	
	/**
	 * 按id查托盘 配置
	 * added by zhengziqi
	 * @param cp_id
	 * @return
	 * @throws Exception
	 * add ship_to_name by zhaoyy 2015年6月2日 15:03:01
	 */
	public DBRow findContainerConfigByConConfigId(long container_config_id) throws Exception{
		try{
			String sql = "SELECT cc.*,ifnull(st.ship_to_name,'') as ship_to_name , ifnull(lpt.lp_name,'') as lp_name  FROM container_config  cc LEFT JOIN ship_to st on cc.ship_to_id = st.ship_to_id LEFT JOIN license_plate_type lpt on lpt.lpt_id = cc.license_plate_type_id   WHERE container_config_id = "+container_config_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findContainerConfigByConConfigId error:" + e);
		}
	}
	/**
	 * 查找admin
	 * added by zhengziqi
	 * @param cp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findAdminByAdid(long adid) throws Exception{
		try{
			String sql = "SELECT * FROM admin WHERE adid = "+adid;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findAdminByAdid error:" + e);
		}
	}
	
	/**
	 * 添加receipt_lines_log 日志
	 * @param operator
	 * @param operator_id
	 * @param operator_time
	 * @param receipt_line_id
	 * @param receipt_line_status
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public long addTaskLog(DBRow data) throws Exception{
		try{
			return dbUtilAutoTran.insertReturnId("receipt_lines_log", data);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.addTaskLog error:" + e);
		}
	}
	/**
	 * 查找pallet 以及config
	 * @param con_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findContainerByConId(long con_id) throws Exception{
		try{
			String sql = "SELECT  con.process, con.status,con.customer_con_id,con.detail_id as con_detail_id,con.con_id,con.container as container_no,con.container_type,con.item_id,con.lot_number AS lot_no,cc.container_config_id,con.location"
					+ " ,cc.config_type,cc.receipt_line_id,cc.length,cc.width,cc.height,cc.pallet_type,cc.note,a.adid,cc.create_date"
					+ " ,a.employe_name as create_user_name,(cc.length*cc.height*cc.width) AS config_qty, cp.cp_quantity,rrc.*"
					+ " ,cc.goods_type,p_user.employe_name as print_user_name"
					+ " FROM receipt_rel_container rrc JOIN container con on con.con_id = rrc.plate_no"
					+ " JOIN container_config cc ON cc.container_config_id = rrc.container_config_id"
					+ " LEFT JOIN container_product cp ON cp.cp_lp_id = con.con_id"
					+ " LEFT JOIN admin a ON cc.create_user = a.adid"
					+ " LEFT JOIN admin p_user ON rrc.print_user = p_user.adid"
					+ " where con.con_id="+con_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findContainerByConId error:" + e);
		}
	}
	
	/**
	 *
	 * 2015年4月6日
	 * @param pallets
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] findContainerByConIds(String pallets) throws Exception{
		try{
			String sql = "SELECT   con.status,con.customer_con_id,con.detail_id as con_detail_id,con.con_id,con.container as container_no,con.container_type,con.item_id,con.lot_number AS lot_no,cc.container_config_id"
					+ " ,cc.config_type,cc.receipt_line_id,cc.length,cc.width,cc.height,cc.pallet_type,cc.note,a.adid,cc.create_date"
					+ " ,a.employe_name as create_user_name,(cc.length*cc.height*cc.width) AS config_qty, cp.cp_quantity,rrc.*"
					+ " ,cc.goods_type,p_user.employe_name as print_user_name"
					+ " FROM receipt_rel_container rrc JOIN container con on con.con_id = rrc.plate_no"
					+ " JOIN container_config cc ON cc.container_config_id = rrc.container_config_id"
					+ " LEFT JOIN container_product cp ON cp.cp_lp_id = con.con_id"
					+ " LEFT JOIN admin a ON cc.create_user = a.adid"
					+ " LEFT JOIN admin p_user ON rrc.print_user = p_user.adid"
					+ " where con.con_id in ("+ pallets + ") order by container_no asc";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("flooReceiptsMgrWfh.findContainerByConIds error:" + e);
		}
	}
	/**
	 * 查找line config qty total
	 * @param con_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findContainerConfigQTYByLineId(long line_id,int goods_type) throws Exception{
		try{
			String sql = "SELECT sum(ccc.config_qty) AS config_qty FROM receipt_rel_container rrc "
					+ " ,(SELECT cc.length*cc.width*cc.height AS config_qty,cc.* FROM container_config cc WHERE cc.is_delete = 0 AND cc.receipt_line_id = "+line_id;
			if(goods_type!=3){
				sql += " AND cc.goods_type="+goods_type;
			}
			sql += ")  ccc";
			sql += " WHERE rrc.container_config_id = ccc.container_config_id AND rrc.receipt_line_id = "+line_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findContainerConfigQTYByLineId error:" + e);
		}
	}
	
	/**
	 * 更新container prodect
	 * @param cp_id
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long updateContainerProduct(long cp_id, DBRow row) throws Exception{
		try{
			return dbUtilAutoTran.update("where cp_id="+cp_id, "container_product", row);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.updateContainerProduct error:" + e);
		}
	}
	/**
	 * 更新container
	 * @param cp_id
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long updateContainer(long con_id, DBRow upRow) throws Exception{
		try{
			return dbUtilAutoTran.update("where con_id="+con_id, "container", upRow);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.updateContainer error:" + e);
		}
	}
	/**
	 * 更新container
	 * @param cp_id
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public void updateContainerStatusByLineID(long line_id,int status) throws Exception{
		try{
			status = status==2?2:1;
			String sql = "UPDATE container con,receipt_rel_container rrc SET con.`status` = "+status+" WHERE  con.con_id=rrc.plate_no AND rrc.receipt_line_id = " +line_id;
//			String sql = "UPDATE container con SET con.`status` = "+status+" WHERE con.con_id in (SELECT rrc.plate_no FROM receipt_rel_container rrc WHERE rrc.receipt_line_id ="+line_id+");";
			dbUtilAutoTran.executeSQL(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.updateContainerStatusByLineID error:" + e);
		}
	}
	
	/**
	 * 更新container
	 * @param cp_id
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long updateContainers(String con_ids, DBRow upRow) throws Exception{
		try{
			return dbUtilAutoTran.update("where con_id in("+con_ids+")", "container", upRow);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.updateContainers error:" + e);
		}
	}
	
	/**
	 * 找到schedule未关闭的任务
	 * @param line_id
	 * @param associate_type
	 * @param associate_process
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findSchedule(int associate_type,int associate_process,long line_id) throws Exception{
		try{
			String sql = "SELECT sc.* FROM `schedule` sc WHERE sc.associate_type = "+associate_type+" AND sc.associate_process = "+associate_process
					+ " AND sc.associate_id ="+line_id+" AND sc.schedule_state != 10";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findSchedule error:" + e);
		}
	}
	/**
	 * 找到schedule未关闭的任务
	 * @param line_id
	 * @param associate_type
	 * @param associate_process
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findScheduleByDetail(int associate_type,int associate_process,long detail_id) throws Exception{
		try{
			String sql = "SELECT sc.* FROM `schedule` sc WHERE sc.associate_id ="+detail_id+" AND sc.schedule_state != 10";
			if(associate_type!=0){
				 sql += " AND sc.associate_type = "+associate_type;
			}
			if(associate_process!=0){
				sql += " AND sc.associate_process = "+associate_process;
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findScheduleByDetail error:" + e);
		}
	}
	
	/**
	 * 找到schedule未关闭的任务
	 * @param line_id
	 * @param associate_type
	 * @param associate_process
	 * @return
	 * @throws Exception
	 */
	public long addException(DBRow row) throws Exception{
		try{
			return dbUtilAutoTran.insertReturnId("receipt_lines_exception", row);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.addException error:" + e);
		}
	}
	/**
	 * 查找count 的 normalqty的总数
	 * @param line_id
	 * @param associate_type
	 * @param associate_process
	 * @return
	 * @throws Exception
	 */
	public DBRow findSUMReceivedQTY(long receipt_line_id,int goods_type) throws Exception{
		try{
			String sql = "SELECT SUM(rrc.normal_qty) AS normal_qty,SUM(rrc.damage_qty) AS damage_qty"
					+ " FROM receipt_rel_container rrc "
					+ " JOIN container_config cc ON cc.container_config_id = rrc.container_config_id"
					+ " WHERE rrc.receipt_line_id ="+receipt_line_id;
			if(goods_type!=3){
				sql += " AND cc.goods_type="+goods_type;
			}
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findSUMReceivedQTY error:" + e);
		}
	}
	
	/**
	 *
	 * 查找count 的 normalqty的总数
	 * 2015年5月12日
	 * @param receipt_line_id
	 * @param goods_type
	 * @param is_clp
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow findSUMReceivedQTY(long receipt_line_id,int goods_type, Boolean is_clp) throws Exception{
		try{
			String sql = "SELECT SUM(rrc.normal_qty) AS normal_qty,SUM(rrc.damage_qty) AS damage_qty"
					+ " FROM receipt_rel_container rrc "
					+ " JOIN container_config cc ON cc.container_config_id = rrc.container_config_id and cc.is_delete = 0"
					+ " JOIN container c ON c.con_id = rrc.plate_no and c.is_delete = 0 "
					+ " WHERE rrc.receipt_line_id ="+receipt_line_id;
			if(goods_type!=3){
				sql += " AND cc.goods_type="+goods_type;
			}
			if(is_clp){
				sql += " AND cc.license_plate_type_id > 0 ";
			}else{
				sql += " AND cc.license_plate_type_id is NULL ";
			}
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findSUMReceivedQTY error:" + e);
		}
	}
	/**
	 * 更新receipt_rel_container
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public long updateReceiptContainerByConID(long con_id, DBRow upRow) throws Exception{
		try{
			return dbUtilAutoTran.update("where plate_no = "+con_id, "receipt_rel_container", upRow);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.updateReceiptContainerByConID error:" + e);
		}
	}
	/**
	 * 查找这种配置下的托盘是否打印过
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptRelContainer(long container_config_id,int findDeletePallet) throws Exception{
		try{
			String sql = "SELECT rrc.*,print_user.employe_name AS print_user_name,con.process,con.is_delete"
					+ " FROM receipt_rel_container rrc"
					+ " JOIN container con ON con.con_id = rrc.plate_no";
					if(findDeletePallet!=1){
						sql += " AND con.is_delete=0";
					}
					sql += " LEFT JOIN admin print_user ON print_user.adid = rrc.print_user"
					+ " WHERE rrc.container_config_id  ="+container_config_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findReceiptRelContainer error:" + e);
		}
	}
	/**
	 * 查找这种配置下的托盘的个数
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findContainerRelReceipt(String con_id) throws Exception{
		try{
			String sql = "SELECT * FROM receipt_rel_container WHERE container_config_id ="
					+ " (SELECT DISTINCT rrc.container_config_id FROM receipt_rel_container rrc WHERE rrc.plate_no in("+con_id+"))";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findContainerRelReceipt error:" + e);
		}
	}
	/**
	 * 查找这个托盘的照片
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findPalletPhotoByDetailId(long detail_id,long con_id) throws Exception{
		try{
			String sql = "SELECT f.file_id,f.file_name FROM file f"
					+ " WHERE f.file_with_class =8  AND f.file_with_type = 52"
					+ " AND f.file_with_detail_id  = "+detail_id+" AND f.option_file_param = "+con_id
					+ " ORDER BY f.upload_time DESC";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findPalletPhotoByDetailId error:" + e);
		}
	}
	/**
	 * 查找这个line下count的数量
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findContainerProductByLineId(long line_id,int goods_type) throws Exception{
		try{
			String sql = "SELECT cp.*,cc.goods_type FROM receipt_rel_container rrc"
					+ " JOIN container_product cp ON cp.cp_lp_id = rrc.plate_no"
					+ " JOIN container_config cc ON cc.container_config_id = rrc.container_config_id"
					+ " WHERE rrc.receipt_line_id = "+line_id;
			if(goods_type!=3){
				sql += " AND cc.goods_type="+goods_type;
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findContainerProductByLineId error:" + e);
		}
	}
	
	/**
	 * 查找这个line下container 传入3查询全部 
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findContainerByLineId(long line_id,int goods_type) throws Exception{
		try{
			String sql = "SELECT con.*,con.container as container_no,rrc.print_date,rrc.print_user,print_admin.employe_name as print_user_name"
					+ " FROM receipt_rel_container rrc"
					+ " JOIN container con ON con.con_id = rrc.plate_no AND con.is_delete=0"
					+ " JOIN container_config cc ON cc.container_config_id=rrc.container_config_id left"
					+ " join admin print_admin on print_admin.adid = rrc.print_user"
					+ " WHERE rrc.receipt_line_id ="+line_id;
			if(goods_type!=3){
				sql += " AND cc.goods_type="+goods_type;
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findContainerByLineId error:" + e);
		}
	}
	/**
	 * 查找这个RN下container 传入3查询全部 
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findContainerByRNId(long rn_id,int goods_type,int status) throws Exception{
		try{
			String sql = "SELECT con.* FROM receipt_rel_container rrc JOIN container con ON con.con_id = rrc.plate_no AND con.is_delete = 0 "
					+ " JOIN receipt_lines rl on rl.receipt_line_id = rrc.receipt_line_id"
					+ " JOIN container_config cc ON cc.container_config_id=rrc.container_config_id WHERE rl.receipt_id ="+rn_id;
			if(goods_type!=3){
				sql += " AND cc.goods_type="+goods_type;
			}
			if(status!=0){
				sql += " AND con.status="+status;
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findContainerByRNId error:" + e);
		}
	}
	/**
	 * 查找这个line下container  传入3查询全部
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow findDamageContainerConfigByLineId(long line_id,int goods_type) throws Exception{
		try{
			String sql = "SELECT cc.* FROM container_config cc WHERE cc.receipt_line_id ="+line_id;
			if(goods_type!=3){
				sql += " AND cc.goods_type="+goods_type;
			}
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findDamageContainerConfigByLineId error:" + e);
		}
	}
	
	/**
	 * 更新上传完成的图片
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public long updateFileByFileIds(String fileIds,DBRow upRow) throws Exception{
		try{
			return dbUtilAutoTran.update("where file_id in("+fileIds+")", "file", upRow);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.updateFileByFileIds error:" + e);
		}
	}

	/**
	 * 查找这个detail
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow findDetailByDetailId(long detail_id) throws Exception{
		try{
			String sql = "SELECT * from door_or_location_occupancy_details where dlo_detail_id = "+detail_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findDetailByDetailId error:" + e);
		}
	}
	/**
	 * 查找这个detail与资源的关系 
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow findDetailResourcesByDetailId(long detail_id) throws Exception{
		try{
			String sql = "SELECT * FROM space_resources_relation srr WHERE srr.relation_type =2 AND srr.relation_id ="+detail_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findDetailResourcesByDetailId error:" + e);
		}
	}
	/**
	 * 查找line下的detail_id
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow findDetailByLineId(long line_id) throws Exception{
		try{
			String sql = "SELECT GROUP_CONCAT(DISTINCT rrc.detail_id) AS detail_id FROM receipt_rel_container rrc WHERE rrc.receipt_line_id ="+line_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findDetailByLineId error:" + e);
		}
	}
	/**
	 * 查找这个receipt
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow findReceiptByLineId(long line_id) throws Exception{
		try{
			String sql = "SELECT re.*,t.title_id FROM receipt_lines rl JOIN receipts re ON re.receipt_id = rl.receipt_id left join title t on t.title_name = re.supplier_id WHERE rl.receipt_line_id = "+line_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findReceiptByLineId error:" + e);
		}
	}
	/**
	 * 查找这个receipt
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow findReceiptById(long receipt_id) throws Exception{
		try{
			String sql = "SELECT re.* FROM  receipts re where re.receipt_id = "+receipt_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findReceiptById error:" + e);
		}
	}
	/**
	 * 查找这个receipt
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow findReceiptByNo(String receipt_no) throws Exception{
		try{
			String sql = "SELECT re.* FROM  receipts re where re.receipt_no = "+receipt_no;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findReceiptByNo error:" + e);
		}
	}
	/**
	 * 查找这个receipt 下所有的line
	 * @param line_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findReceiptLineByReceiptId(long receipt_id) throws Exception{
		try{
			String sql = "SELECT rl.* FROM receipt_lines rl WHERE rl.receipt_id = "+receipt_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findReceiptLineByReceiptId error:" + e);
		}
	}
	
	
	/**
	 *
	 * 查找这个receipt 下所有的line,以及line上的货物数
	 * 2015年6月26日
	 * @param receipt_id
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow[] findReceiptLineByReceiptIdAndQuantity(long receipt_id) throws Exception{
		try{
			StringBuffer sb = new StringBuffer(" SELECT  rl.*, COUNT(sp.serial_number) as quantity ");
			sb.append(" FROM receipt_lines rl  ")
			.append(" JOIN receipt_rel_container rrc ON rrc.receipt_line_id = rl.receipt_line_id ")
			.append(" JOIN container c on c.con_id = rrc.plate_no and c.is_delete = 0 ")
			.append(" JOIN serial_product sp on sp.at_lp_id = c.con_id ")
			.append(" WHERE rl.receipt_id = ")
			.append(receipt_id);
			return dbUtilAutoTran.selectMutliple(sb.toString());
		}catch (Exception e){
			throw new Exception("findAdminByAdid.findReceiptLineByReceiptId error:" + e);
		}
	}
	
	//查找rn
	public DBRow[] queryReceiptInfoByRN(String receipt_no)throws Exception{
		try{
			DBRow para = new DBRow();
			para.add("receipt_no", receipt_no);
			String sql = "select * from receipts where receipt_no=?";
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}catch (Exception e){
			throw new Exception("findAdminByAdid.queryReceiptInfoByRN error:" + e);
		}
	}
	//查找customer 
	public DBRow findCustomerByCustomer(String customer) throws Exception{
		try{
			String sql = "SELECT * FROM customer_id ci WHERE ci.customer_name = '"+customer+"'";
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("floorReceiptsMgrWfh.findCustomerByCustomer error:" + e);
		}
	}
	
	/**
	 * query Actual Qty By Line_Id
	 * added by zhengziqi
	 * 2015年4月10日
	 * @param line_id
	 * @return
	 * @throws Exception
	 *
	 */
	public DBRow queryActualQtyByLineId(long line_id) throws Exception{
		try{
			String sql = " select SUM(normal_qty) as goods_total, SUM(damage_qty) as damage_total from receipt_rel_container where receipt_line_id = " + line_id;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.queryActualQtyByLineId error:" + e);
		}
	}
	/**
	 * 更新containerConfig
	 * @param config_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public long updateContainerConfig(long config_id,DBRow upRow) throws Exception{
		try{
			return dbUtilAutoTran.update("where container_config_id="+config_id, "container_config", upRow);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.queryActualQtyByLineId error:" + e);
		}
	}
	
	/**
	 * 查询task扫描的其他的order
	 * @param order_id
	 * @param upRow
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findTaskOtherCarrierOrder(String carrier_sub,long detail_id) throws Exception{
		try{
			String sql = "SELECT DISTINCT wlo.* FROM wms_load_order wlo"
					+ " JOIN wms_load_order_pallet_type_count wlc ON wlo.wms_order_id = wlc.wms_order_id"
					+ " WHERE wlo.carrier_id NOT in('"+carrier_sub+"') AND wlc.dlo_detail_id= "+detail_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findTaskOtherCarrierOrder error:" + e);
		}
	}
	
	/**
	 * 查找line的receive的pallet数量
	 * @param line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow findReceivePalletCount(long line_id) throws Exception{
		try{
			String sql = " select count(cp.cp_lp_id) as receive_pallet "
					+ " from receipt_lines rl"
					+ " join receipt_rel_container rrc on  rl.receipt_line_id = rrc.receipt_line_id"
					+ " JOIN container_config cc ON cc.container_config_id = rrc.container_config_id AND cc.is_delete = 0 "
					+ " join container c on rrc.plate_no = c.con_id and c.is_delete = 0"
					+ " join container_product cp on cp.cp_lp_id = c.con_id"
					+ " where rl.receipt_line_id = "+line_id
					+ " and expected_qty > 0"
					+ " group by rl.receipt_line_id ";
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrWfh.findReceivePalletCount error:" + e);
		}
	}

}
	
