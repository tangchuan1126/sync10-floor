package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorDoubtGoodsMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	public long addDoubtGoods(DBRow dbrow)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("doubt_goods"),dbrow);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDoubtGoodsMgrZJ addDoubtGoods error:"+e);
		}
	}
	
	public void modDoubtGoods(long dg_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where dg_id = "+dg_id,ConfigBean.getStringValue("doubt_goods"),para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDoubtGoodsMgrZJ modDoubtGoods error:"+e);
		}
	}
	
	/**
	 * 过滤疑问商品
	 * @param product_line_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] filterDoubtGoods(long product_line_id,int is_answerd,PageCtrl pc)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("doubt_goods")+" where 1=1 ";
		
		if(product_line_id!=0)
		{
			sql += " and product_line_id = "+product_line_id;
		}
		
		if (is_answerd==1)
		{
			sql += " and answer_adid is NULL";
		}
		else if(is_answerd==2)
		{
			sql += " and answer_adid is not null";
		}
		
		sql += " order by dg_id desc ";
		
		return dbUtilAutoTran.selectMutliple(sql, pc);
	}
	
	/**
	 * 根据Id获得疑问商品详细
	 * @param dg_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailDoubtGoods(long dg_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("doubt_goods")+" where dg_id = ?";
			
			DBRow para = new DBRow();
			para.add("dg_id",dg_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDoubtGoodsMgrZJ getDetailDoubtGoods error:"+e);
		}
	}
	
	/**
	 * 待回答疑问商品
	 * @return
	 * @throws Exception
	 */
	public int waitAnswerDoubtGoodCount()
		throws Exception
	{
		try 
		{
			String sql = "select count(dg_id)as need_answer_doubt from doubt_goods where answer_adid is NULL";
			
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			
			int need_answer_doubt = row.get("need_answer_doubt",0);
			
			return need_answer_doubt;
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDoubtGoodsMgrZJ waitAnswerDoubtGoodCount ");
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
