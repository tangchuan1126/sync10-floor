package com.cwc.app.floor.api.zzz;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorApplyTransferMgrZZZ 
{
	private DBUtilAutoTran dbUtilAutoTran;
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
	/**
	 * 排序 最迟转款时间
	 */
    public DBRow[] getApplyTransferLastTimeSort(PageCtrl pc)throws Exception{
    	try{
			String sql="select * from apply_transfer ORDER BY last_time DESC";
			return this.dbUtilAutoTran.selectMutliple(sql, pc);
		}catch(Exception e){
			throw new Exception("FloorApplyMoneyMgrZZZ.getApplyMoneySort(pc)"+e);
		}
    }
    /**
	 * 转账完成时间排序
	 */
    public DBRow[] getApplyTransferEndTimeSort(PageCtrl pc)throws Exception{
    	try{
			String sql="select * from apply_transfer ORDER BY voucher_createTime DESC";
			return this.dbUtilAutoTran.selectMutliple(sql, pc);
		}catch(Exception e){
			throw new Exception("FloorApplyMoneyMgrZZZ.getApplyMoneySort(pc)"+e);
		}
    }
	
	/**
	 * 新增转账申请
	 * @param row
	 * @throws Exception
	 */
	public long addApplyTransfer(DBRow row)
	throws Exception
	{
		try
		{
			long id=dbUtilAutoTran.getSequance(ConfigBean.getStringValue("apply_transfer"));
			row.add("transfer_id",id);
		
			dbUtilAutoTran.insert(ConfigBean.getStringValue("apply_transfer"),row);
			return (id);
		}
		catch(Exception e)
		{
			throw new Exception("FloorApplyTransfer.addApplyTransfer(row) error:" + e);
		}
	}
	/**
	 * 获取所有的转账申请记录
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllApplyTransfer(PageCtrl pc)
	throws Exception
	{
		try
		{
			String sql = "select * from "
				+ ConfigBean.getStringValue("apply_transfer")+" order by transfer_id DESC ";			
			if(pc!=null)
			{
				return (dbUtilAutoTran.selectMutliple(sql,pc));
			}
			else
			{
				
				return (dbUtilAutoTran.selectMutliple(sql));
			}
		}
		catch(Exception e)
		{
		    throw new Exception("FloorApplyTransfer.getAllApplyTransfer()error:re"+e);	
		}
	}
	/**
	 * 根据关联资金申请单的ID查询关于该ID的所有转账申请
	 * @param id
	 * @return
	 */
	public DBRow[] getApplyTransferByApplyMoneyId(long id,PageCtrl pc)
	throws Exception
	{
	     try
	     {
	    	 String sql = "select * from "+ConfigBean.getStringValue("apply_transfer")+
	    	 " where apply_money_id="+id+" order by transfer_id DESC ";
	    	 
	    	 if(pc!=null)
	    	 {
	    		 return (dbUtilAutoTran.selectMutliple(sql, pc));
	    	 }
	    	 
	    	 return (dbUtilAutoTran.selectMutliple(sql));
	    	 
	     }	
	     catch(Exception e)
	     {
	    	 throw new Exception("FloorApplyTransfer.getApplyTransferById"+e);
	     }
	}
	
	/**
	 * 根据申请转账的ID修改
	 * @param id
	 * @param data
	 * @throws Exception
	 */
	public void updateApplyTransferById(long id,DBRow data)
	throws Exception
	{
		try
		{
		
			dbUtilAutoTran.update("where transfer_id="+id, ConfigBean.getStringValue("apply_transfer"), data);
		}
		catch(Exception e)
		{
			throw new Exception("updateApplyTransferById.FloorApplyTransfer()"+e);
		}
		
	}
	/**
	 * 根据申请转账ID查询申请转账记录
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApplyTransferById(long id)
		throws Exception
	{
	     try
	     {
	    	 String sql = "select * from "+ConfigBean.getStringValue("apply_transfer")+
	    	 " where transfer_id="+id;
	    	 	    		    	 
	    	 return (dbUtilAutoTran.selectMutliple(sql));
	    	 
	     }	
	     catch(Exception e)
	     {
	    	 throw new Exception("FloorApplyTransfer.getApplyTransferById"+e);
	     }
	}
	
	public DBRow getDetailApplyTransferById(long apply_transfer_id)
		throws Exception
	{
	     try
	     {
	    	 String sql = "select * from "+ConfigBean.getStringValue("apply_transfer")+" where transfer_id=?";
	    	 
	    	 DBRow para = new DBRow();
	    	 para.add("apply_transfer_id",apply_transfer_id);
	    	 	    		    	
	    	 return (dbUtilAutoTran.selectPreSingle(sql, para));
	     }	
	     catch(Exception e)
	     {
	    	 throw new Exception("FloorApplyTransfer.getApplyTransferById"+e);
	     }
	}
	
	
	/**
	 * 根据条件过滤转账申请
	 * @param startTime
	 * @param endTime
	 * @param status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
    public DBRow[] getApplyTransferByCondition(String selectSortTime,String startTime,String endTime,int status,PageCtrl pc)
    throws Exception
    {
    	try
    	{
    		StringBuffer sqlCondition=new StringBuffer();
	    	 sqlCondition.append("where create_time between '"+startTime+" 00:00:00' and '"+endTime+" 23:59:59'");
	    	
	    	if(status!=-1)
	    	{
	    		sqlCondition.append("and status="+status);
	    	}
    		String sql = "select * from "
				+ ConfigBean.getStringValue("apply_transfer")+" "+sqlCondition.toString()+" order by "+selectSortTime+" DESC ";			
			if(pc!=null)
			{
				return (dbUtilAutoTran.selectMutliple(sql,pc));
			}
			else
			{
				
				return (dbUtilAutoTran.selectMutliple(sql));
			}    
    	}
    	catch(Exception e)
    	{
    		throw new Exception("FloorApplyMoneyMgrZZZ.getApplyMoneyByCondition(startTime,endTime,status,pc)"+e);
	    }
     }
    /**
     * 根据ID删除转账申请
     * @param id
     * @throws Exception
     */
     public  void deleteApplyTransferById(long id)
     throws Exception
     {
    	 try
    	 {
    		 dbUtilAutoTran.delete("where transfer_id="+id, ConfigBean.getStringValue("apply_transfer"));
    	 }
    	 catch(Exception e)
    	 {
    		 throw new Exception("FloorApplyMoneyMgrZZZ.deleteApplyTransferById(id)"+e);
    	 }
     }
     public DBRow[] getApplyTransferArrayByApplyMoneyId(long applyMoneyId, int[] transferStatus)
     throws Exception{
    	 try {
    		 String sql = "select * from apply_transfer where apply_money_id =" + applyMoneyId;
    		 String sqlWhere = "";
    		 if(null == transferStatus || transferStatus.length > 0){
    			if(transferStatus.length == 1){
    				sqlWhere = " and status = " + transferStatus[0];
    			}else{
    				sqlWhere = " and ( status = " + transferStatus[0];
    				for (int i = 1; i < transferStatus.length; i++) {
    					sqlWhere += " or status = " + transferStatus[i];
					}
    				sqlWhere += ")";
    			}
    		 }
    		 sql += sqlWhere;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			 throw new Exception("FloorApplyMoneyMgrZZZ.getApplyTransferByApplyMoneyId(long applyMoneyId, int[] transferStatus)"+e);
		}
     }
}
