package com.cwc.app.floor.api.zyj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorServiceOrderMgrZyj {

	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 添加服务单
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addServiceOrder(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("service_order"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorServiceOrderMgrZyj.addServiceOrder(row) error:" + e);
		}
	}
	
	/**
	 * 更新服务单
	 * @param row
	 * @param sid
	 * @throws Exception
	 */
	public void updateServiceOrder(DBRow row, long sid) throws Exception
	{
		try 
		{
			dbUtilAutoTran.update(" where sid = "+sid, ConfigBean.getStringValue("service_order"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorServiceOrderMgrZyj.updateServiceOrder(row，sid) error:" + e);
		}
	}
	
	/**
	 * 通过服务单ID获取服务单信息
	 * @param sid
	 * @return
	 * @throws Exception
	 */
	public DBRow getServiceOrderDetailBySid(long sid) throws Exception
	{
		try 
		{
			String sql = "select * from service_order where sid = " + sid;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorServiceOrderMgrZyj.getServiceOrderDetailBySid(long sid) error:" + e);
		}
	}
	
	/**
	 * 添加服务单明细
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addServiceOrderDetails(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("service_order_items"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorServiceOrderMgrZyj.addServiceOrderDetails(DBRow row) error:" + e);
		}
	}
	
	/**
	 * 通过服务单明细ID获取服务单明细信息
	 * @param sid
	 * @return
	 * @throws Exception
	 */
	public DBRow getServiceOrderItemDetail(long si_id) throws Exception
	{
		try 
		{
			String sql = "select * from service_order_items where si_id = " + si_id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorServiceOrderMgrZyj.getServiceOrderItemDetail(long si_id) error:" + e);
		}
	}
	
	/**
	 * 通过服务单ID获取所有服务单明细
	 * @param sid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getServiceOrderItemsByServiceId(long sid) throws Exception
	{
		try
		{
			String sql = "select * from service_order_items where sid = " +sid + " order by oi_id,wi_id";
			return dbUtilAutoTran.selectMutliple(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorServiceOrderMgrZyj.getServiceOrderItemsByServiceId(long sid) error:" + e);
		}
	}
	
	/**
	 * 通过订单明细ID查询订单明细信息
	 * @param oiid
	 * @return
	 * @throws Exception
	 */
	public DBRow getPorderItemDetailByOiid(long oiid) throws Exception
	{
		try 
		{
			String sql = "select * from porder_item where iid = " + oiid;
			return dbUtilAutoTran.selectSingle(sql);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorServiceOrderMgrZyj.getPorderItemDetailByOiid(long oiid) error:" + e);
		}
	}
	
	/**
	 * 更新订单明细
	 * @param row
	 * @param oiid
	 * @throws Exception
	 */
	public void updatePorderItemDetail(DBRow row, long oiid) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where iid = " + oiid, ConfigBean.getStringValue("porder_item"), row);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorServiceOrderMgrZyj.updatePorderItemDetail(DBRow row, long oiid) error:" + e);
		}
	}
	
	/**
	 * 保存服务单的服务原因及处理结果
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long saveServiceOrderReasonSolution(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("service_reason_solution"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorServiceOrderMgrZyj.saveServiceOrderReasonSolution(row) error:" + e);
		}
	}
	
	/**
	 * 通过订单ID得到所有服务单
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getServiceOrdersByPOrderId(long oid) throws Exception
	{
		try
		{
			String sql = "select * from service_order where oid = " + oid ;//+ " and wid = 0";
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorServiceOrderMgrZyj.getServiceOrdersByPOrderId(long oid) error:" + e);
		}
	}
	
	/**
	 * 通过订单ID得到服务单总数
	 * @param oid
	 * @return
	 * @throws Exception
	 */
	public int getServiceOrderCountByPOrderId(long oid) throws Exception
	{
		try
		{
			String sql = "select count(sid) as serviceCountByOid from service_order where oid = " + oid ;//+ " and wid = 0";
			return dbUtilAutoTran.selectSingle(sql).get("serviceCountByOid", 0);
		}
		catch (Exception e)
		{
			throw new Exception("FloorServiceOrderMgrZyj.getServiceOrderCountByPOrderId(long oid) error:" + e);
		}
	}
	
	/**
	 * 通过运单ID查询服务单列表
	 * @param wid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getServiceOrdersByWaybillId(long wid) throws Exception
	{
		try
		{
			String sql = "select * from service_order where wid = " + wid;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorServiceOrderMgrZyj.getServiceOrdersByWaybillId(long wid) error:" + e);
		}
	}
	
	/**
	 * 通过运单ID查询服务单列表
	 * @param wid
	 * @return
	 * @throws Exception
	 */
	public int getServiceOrderCountByWaybillId(long wid) throws Exception
	{
		try
		{
			String sql = "select count(sid) as serviceOrderCountByOid from service_order where wid = " + wid;
			return dbUtilAutoTran.selectSingle(sql).get("serviceOrderCountByOid", 0);
		}
		catch (Exception e)
		{
			throw new Exception("FloorServiceOrderMgrZyj.getServiceOrderCountByWaybillId(long wid) error:" + e);
		}
	}
	
	/**
	 * 通过运单ID查询服务单处理列表
	 * @param wid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getServiceOrderReasonSolutionsBySid(long sid) throws Exception
	{
		try
		{
			String sql = "select * from service_reason_solution where sid = " + sid;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorServiceOrderMgrZyj.getServiceOrderReasonSolutionsBySid(long sid) error:" + e);
		}
	}
	
	/**
	 * 通过运单ID查询服务单处理列表
	 * @param wid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getServiceOrderReasonSolutionsBySid(long sid, int num) throws Exception
	{
		try
		{
			String sql = "select * from service_reason_solution where sid = " + sid +" limit 0,2";
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorServiceOrderMgrZyj.getServiceOrderReasonSolutionsBySid(long sid) error:" + e);
		}
	}
	
	/**
	 * 通过运单ID查询服务单处理数量
	 * @param wid
	 * @return
	 * @throws Exception
	 */
	public int getServiceOrderReasonSolutionCountBySid(long sid) throws Exception
	{
		try
		{
			String sql = "select count(*) as reasonSolutionCount from service_reason_solution where sid = " + sid;
			return dbUtilAutoTran.selectSingle(sql).get("reasonSolutionCount", 0);
		}
		catch (Exception e)
		{
			throw new Exception("FloorServiceOrderMgrZyj.getServiceOrderReasonSolutionsBySid(long sid) error:" + e);
		}
	}
	
	/**
	 * 通过服务单ID获取服务单的账单
	 * @param sid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getServiceBillOrdersBySid(long sid) throws Exception
	{
		try
		{
			String sql = "select * from bill_order where sid = " + sid;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorServiceOrderMgrZyj.getServiceBillOrdersBySid(long sid) error:" + e);
		}
	}
	
	/**
	 * 通过服务单ID删除服务单明细
	 * @param sid
	 * @throws Exception
	 */
	public void deleteServiceItemsBySid(long sid) throws Exception
	{
		try
		{
			String sql = " where sid = " + sid;
			dbUtilAutoTran.delete(sql, ConfigBean.getStringValue("service_order_items"));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorServiceOrderMgrZyj.deleteServiceItemsBySid(long sid) error:" + e);
		}
	}
	
	/**
	 * 分页查询服务单列表
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getServiceOrderByPage(PageCtrl pc) throws Exception
	{
		try
		{
			String sql = " select * from service_order order by sid desc ";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorServiceOrderMgrZyj.getServiceOrderByPage(PageCtrl pc) error:" + e);
		}
	}
	
	/**
	 * 过滤服务单列表
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getServiceOrderByPage(PageCtrl pc, int status, int is_need_return, String st, String end, long create_adid) throws Exception
	{
		try
		{
			String sql = " select * from service_order where 1=1";
			if(0 != status)
			{
				sql += " and status = " + status;
			}
			if(0 != is_need_return)
			{
				sql += " and is_need_return = "+is_need_return;
			}
			if(st != null && st.length() > 0 ){
				sql += " and create_date >='" + st + " 00:00:00'";
			}
			if(end != null && end.length() > 0 ){
				sql += " and create_date <='" + end + " 23:59:59'";
			}
			if(-1 != create_adid)
			{
				sql += " and create_user = " + create_adid;
			}
			sql += " order by sid desc";
			return dbUtilAutoTran.selectMutliple(sql, pc);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorServiceOrderMgrZyj.getServiceOrderByPage(PageCtrl pc, int status) error:" + e);
		}
	}
	
	/**
	 * 通过服务处理ID查询详细
	 * @param rs_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getServiceReasonSolutionBySrsid(long srs_id) throws Exception
	{
		try
		{
			String sql = "select * from service_reason_solution where srs_id = " + srs_id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorServiceOrderMgrZyj.getServiceReasonSolutionBySrsid(long srs_id) error:" + e);
		}
	}
	
	/**
	 * 更新服务处理
	 * @param row
	 * @param srs_id
	 * @throws Exception
	 */
	public void updateServiceReasonSolutionBySrsid(DBRow row, long srs_id) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where srs_id = "+srs_id, ConfigBean.getStringValue("service_reason_solution"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorServiceOrderMgrZyj.updateServiceReasonSolutionBySrsid(DBRow row, long srs_id) error:" + e);
		}
	}
	
	//获取所有的退货单
	public DBRow[] getAllReturnOrder()throws Exception
	{
		try 
		{
			String sql = "select * from return_product";
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorServiceOrderMgrZyj.getAllReturnOrder() error:" + e);
		}
	}
	
	/**
	 * 通过oid，trace_type,rel_id获取日志，修改日志上的申请服务时的ID
	 * @param oid
	 * @param trace_type
	 * @param rel_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPorderNoteByOidTraceTypeRelId(long oid, int trace_type, long rel_id) throws Exception
	{
		try
		{
			String sql = "select * from porder_note where oid = " + oid + " and trace_type = " + trace_type + " and rel_id = " + rel_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorServiceOrderMgrZyj.getPorderNoteByOidTraceTypeRelId(long oid, int trace_type, long rel_id) error:" + e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
}
