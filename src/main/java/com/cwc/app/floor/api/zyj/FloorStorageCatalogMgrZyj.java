package com.cwc.app.floor.api.zyj;


import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorStorageCatalogMgrZyj {
	
	private DBUtilAutoTran dbUtilAutoTran;

	public DBRow getStorageCatalogById(long storageId) throws Exception{
		try{
			 String sql = "select * from product_storage_catalog where id = " + storageId;
			 return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorStorageCatalogMgrZyj.getStorageCatalogById(id):"+e);
		}
	}
	
	public long addProductStorageCatalog(DBRow row)throws Exception{
		try{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_storage_catalog"), row);
		}catch (Exception e){
			throw new Exception("FloorStorageCatalogMgrZyj.addProductStorageCatalog(row) error:" + e);
		}
	}

	public long updateProductStorageCatalogBasic(DBRow row, long rowId)throws Exception{
		try{
			return dbUtilAutoTran.update(" WHERE id = "+rowId, ConfigBean.getStringValue("product_storage_catalog"), row);
		}catch (Exception e){
			throw new Exception("FloorStorageCatalogMgrZyj.addProductStorageCatalog(row) error:" + e);
		}
		
	}
	
	public DBRow getProviceInfoById(long proId) throws Exception{
		try{
			 String sql = "SELECT * FROM country_province WHERE pro_id = " + proId;
			 return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorStorageCatalogMgrZyj.getProviceInfoById(id):"+e);
		}
	}
	/**
	 * 
	 * @param proName
	 * @return
	 * @throws Exception
	 */
	public DBRow getProviceInfoByName(String proName) throws Exception{
			
		try {
			String sql = "select * from "+ConfigBean.getStringValue("country_province")+" where pro_name='"+proName+"'";
			return dbUtilAutoTran.selectSingle(sql);
		} catch (Exception e) {
			throw new Exception("FloorStorageCatalogMgrZyj.getProviceInfoByName(id):"+e);
		}
	}
	
	/**
	 * 获取所有省份
	 * @author Administrator
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageAllProvinces() throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("country_province") + " order by pro_name asc";
			
			return(dbUtilAutoTran.selectMutliple(sql));
		}
		catch (Exception e)
		{
			throw new Exception("FloorStorageCatalogMgrZyj.getStorageAllProvinces() error:" + e);
		}
	}
	
	public DBRow[] getProductStorageCatalogByType(int storageType,PageCtrl pc) throws Exception{
		try 
		{
			String sql = "select * from product_storage_catalog";
			if(0 != storageType && -1 != storageType)
			{
				sql += " where storage_type = " + storageType;
			}
			if(null == pc)
			{
				return dbUtilAutoTran.selectMutliple(sql);
			}
			else
			{
				return dbUtilAutoTran.selectMutliple(sql, pc);
			}
			
		} 
		catch (Exception e)
		{
			throw new Exception("FloorStorageCatalogMgrZyj.getShortMessageList(pc):"+e);
		}
	}
	
	/**
	 * 通过仓库ID获得商品信息
	 * @author Administrator
	 * @param storage_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductInfoByStorageId() throws Exception
	{
		try
		{
			String sql = "select * from product";
			return dbUtilAutoTran.selectMutliple(sql);

		}
		catch (Exception e)
		{
			throw new Exception(
					"FloorStorageCatalogMgrZyj.getProductInfoByStorageId():"+ e);
		}
	}
	
	/**
	 * 通过仓库ID获取商品条码信息
	 * @author Administrator
	 * @param storage_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductCodeInfoByStorageId() throws Exception
	{
		try
		{
//			String sql  = "select distinct c.pc_id, c.pcode_id, c.p_code, c.code_type, p.p_name " +
//					"from  product_code c left join product p on c.pc_id = p.pc_id order by c.pc_id";
			String sql = "select * from  ((select c.pc_id,  c.p_code, c.code_type, p.p_name  from  product_code c left join product p on c.pc_id = p.pc_id order by c.pc_id)"+
					" UNION (select p.pc_id, CONVERT(p.pc_id,char) AS p_code, 0 as code_type, p.p_name from product p where p.orignal_pc_id = 0 ORDER BY p.pc_id)) u ORDER BY u.pc_id, u.code_type"; 
//			System.out.println(sql);
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception(
					"FloorStorageCatalogMgrZyj.getProductCodeInfoByStorageId():"+ e);
		}
	}
	
	
	/**
	 * 通过仓库Id获得商品序列号
	 * @author Administrator
	 * @param storage_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSerialProductByStorageId() throws Exception {
		try
		{
			String sql = "select distinct * from " + ConfigBean.getStringValue("serial_product") + " order by pc_id";
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception(
					"FloorStorageCatalogMgrZyj.getSerialProductByStorageId():"+ e);
		}
	}
	/**
	 * 
	 * @return
	 * @throws Exception
	 * 
	 */
	public DBRow[] getProductCodeInfoByBarCode(String barCode) throws Exception
	{
		try
		{
 
			String sql  = "select distinct c.pc_id, c.pcode_id, c.p_code, c.code_type, p.p_name from  product_code c left join product p on c.pc_id = p.pc_id "
				+"where c.p_code = '"+barCode.trim()+"' order by c.pc_id" ;
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception(
					"FloorStorageCatalogMgrZyj.getProductCodeInfoByBarCode(String barCode):"+ e);
		}
	}
	public DBRow getProductByPcid(long pcid) throws Exception {
		try
		{
			return dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("product") + " where pc_id ="+pcid);
		}
		catch (Exception e) 
		{
			throw new Exception( "FloorStorageCatalogMgrZyj.getProductByPcid(pcid):"+ e);
		}
	}
	
	/**
	 * 通过用户id获取批次
	 * @param adid
	 * @param title_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProductLotNumberByTitlePcIdAdmin(long adid, Long[] title_ids, long pc_id, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pst.lot_number FROM product_storage_title pst ");
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON pst.title_id = ta.title_admin_title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			sql.append(" WHERE 1=1");
			if(null != title_ids && title_ids.length > 0)
			{
				sql.append(" AND (pst.title_id = " + title_ids[0]);
				for (int i = 1; i < title_ids.length; i++)
				{
					sql.append(" OR pst.title_id = " + title_ids[i]);
				}
				sql.append(" )");
			}
			if(pc_id > 0)
			{
				sql.append(" AND pst.pc_id = "+ pc_id);
			}
			sql.append(" GROUP BY pst.lot_number");
			
//			System.out.println("findProductLotNumberByTitlePcIdAdmin:"+sql.toString());
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}
		catch (Exception e) 
		{
			throw new Exception( "FloorStorageCatalogMgrZyj.findProductLotNumberByTitlePcIdAdmin(long adid, String[] title_ids, long pc_id, PageCtrl pc):"+ e);
		}
	}
	
	
	/**
	 * 库存查询页面
	 * @param ps_id
	 * @param type
	 * @param adid
	 * @param title_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProductStorages(Long[] ps_id, int type, long adid, Long[] title_id, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM");
			sql.append(" (SELECT p.* FROM product p");
			sql.append("	LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
			sql.append("	LEFT JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id");
			sql.append("	WHERE 1=1");
			if(adid > 0)
			{
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			if(null != title_id && title_id.length > 0)
			{
				sql.append(" AND (tp.tp_title_id = " + title_id[0]);
				for (int i = 1; i < title_id.length; i++)
				{
					sql.append(" OR tp.tp_title_id = " + title_id[i]);
				}
				sql.append(" )");
			}
			sql.append("	GROUP BY p.pc_id");
			sql.append(" )q");
			sql.append(" LEFT JOIN product_storage ps ON q.pc_id = ps.pc_id ");
			sql.append(" WHERE 1=1");
			if(ProductStatusKey.IN_STORE == type)//有货
			{
				sql.append(" AND IFNULL(ps.store_count,0) > 0 ");
			}
			else if(ProductStatusKey.NO_STORE == type)//无库存，即库存为0
			{
				sql.append(" AND IFNULL(ps.store_count,0) = 0");
			}
			if(null != ps_id && ps_id.length > 0)
			{
				sql.append(" AND (ps.cid = " + ps_id[0]);
				for (int i = 1; i < ps_id.length; i++) 
				{
					sql.append(" OR ps.cid = " + ps_id[i]);
				}
				sql.append(" )");
			}
			sql.append(" GROUP BY q.pc_id");
			sql.append(" ORDER BY q.pc_id");
			
//			System.out.println("getProductStorages:");
//			System.out.println(sql);
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}
		catch (Exception e)
		{
			throw new Exception("FloorStorageCatalogMgrZyj.findProductStorages(long ps_id, int type, long adid, long title_id, PageCtrl pc) error:" + e);
		}
	}
	
	
	public DBRow[] findProductStorageByName(Long[] pc_line_id, Long[] catalog_id,Long[] ps_id,String code,int union_flag,long adid, Long[] title_id, int type, String[] lot_number_id, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM");
			sql.append(" (SELECT p.* FROM product p");
			sql.append("	JOIN product_catalog c ON p.catalog_id = c.id");
			sql.append("	JOIN pc_child_list pcl ON pcl.pc_id = p.catalog_id");
			sql.append("	JOIN product_line_define pld ON pld.id = c.product_line_id ");
			sql.append("	LEFT JOIN title_product tp ON p.pc_id = tp.tp_pc_id");
//			sql.append("	JOIN title_product_line tpl ON tpl.tpl_title_id = tp.tp_title_id");
//			sql.append("	JOIN title_product_catalog tpc ON tpc.tpc_title_id= tp.tp_title_id");
			sql.append("	LEFT JOIN title_admin ta ON ta.title_admin_title_id = tp.tp_title_id");
			sql.append("	LEFT JOIN product_code pc ON p.pc_id = pc.pc_id");
			sql.append("	WHERE 1=1");
			if(adid > 0)
			{
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			if(null != code && !"".equals(code.trim()))
			{
				sql.append(" AND (p.p_name LIKE '%"+code+"%' OR pc.p_code LIKE '%"+code+"%')");
			}
			if(null != title_id && title_id.length > 0)
			{
				sql.append(" AND (tp.tp_title_id = " + title_id[0]);
				for (int i = 1; i < title_id.length; i++)
				{
					sql.append(" OR tp.tp_title_id = " + title_id[i]);
				}
				sql.append(" )");
			}
			if(union_flag > -1)
			{
				sql.append(" AND p.union_flag = " + union_flag);
			}
			if(null != pc_line_id && pc_line_id.length > 0)
			{
				sql.append(" AND (pld.id = " + pc_line_id[0]);
				for (int i = 1; i < pc_line_id.length; i++) 
				{
					sql.append(" OR pld.id = " + pc_line_id[i]);
				}
				sql.append(" )");
			}
			if(null != catalog_id && catalog_id.length > 0)
			{
				sql.append(" AND (pcl.search_rootid = " + catalog_id[0]);
				for (int i = 1; i < catalog_id.length; i++) {
					sql.append(" OR pcl.search_rootid = " + catalog_id[i]);
				}
				sql.append(" )");
			}
			sql.append("	GROUP BY p.pc_id) q");
			sql.append(" LEFT JOIN product_storage ps ON q.pc_id = ps.pc_id ");
			sql.append(" LEFT JOIN product_storage_title pst ON pst.pc_id = ps.pc_id AND pst.ps_id = ps.cid ");
			sql.append(" WHERE 1=1");
			if(null != ps_id && ps_id.length > 0)
			{
				sql.append(" AND (ps.cid = " + ps_id[0]);
				for (int i = 1; i < ps_id.length; i++) 
				{
					sql.append(" OR ps.cid = " + ps_id[i]);
				}
				sql.append(" )");
			}
			if(ProductStatusKey.IN_STORE == type)//有货
			{
				sql.append(" AND IFNULL(ps.store_count,0) > 0 ");
			}
			else if(ProductStatusKey.NO_STORE == type)//无库存，即库存为0
			{
				sql.append(" AND IFNULL(ps.store_count,0) = 0");
			}
			if(null != lot_number_id && lot_number_id.length > 0)
			{
				sql.append(" AND (pst.lot_number = '"+lot_number_id[0]+"'");
				for (int i = 1; i < lot_number_id.length; i++)
				{
					sql.append(" OR pst.lot_number = '"+lot_number_id[i]+"'");
				}
				sql.append(" )");
			}
			sql.append(" GROUP BY q.pc_id");
			sql.append(" ORDER BY q.pc_id");
	
//			System.out.println("getProductStorageByName:");
//			System.out.println(sql.toString());
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectMutliple(sql.toString(),pc));
			}
			else
			{
				return(dbUtilAutoTran.selectMutliple(sql.toString()));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.findProductStorageByName(long pc_line_id, long catalog_id,long ps_id,String code,int union_flag,long adid, long title_id, int type, String lot_number_id, PageCtrl pc) error:" + e);
		}
	}
	
	/**
	 * 通过用户id获取批次
	 * @param adid
	 * @param title_id
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findProductLotNumberIdAndNameByTitlePcIdAdmin(long adid, Long[] title_ids, long pc_id, PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT pst.lot_number id, pst.lot_number name FROM product_storage_title pst ");
			if(adid > 0)
			{
				sql.append(" JOIN title_admin ta ON pst.title_id = ta.title_admin_title_id ");
				sql.append(" AND ta.title_admin_adid = " + adid);
			}
			sql.append(" WHERE 1=1");
			if(null != title_ids && title_ids.length > 0)
			{
				sql.append(" AND (pst.title_id = " + title_ids[0]);
				for (int i = 1; i < title_ids.length; i++)
				{
					sql.append(" OR pst.title_id = " + title_ids[i]);
				}
				sql.append(" )");
			}
			if(pc_id > 0)
			{
				sql.append(" AND pst.pc_id = "+ pc_id);
			}
			sql.append(" GROUP BY pst.lot_number");
			
//			System.out.println("findProductLotNumberByTitlePcIdAdmin:"+sql.toString());
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}
		catch (Exception e) 
		{
			throw new Exception( "FloorStorageCatalogMgrZyj.findProductLotNumberIdAndNameByTitlePcIdAdmin(long adid, String[] title_ids, long pc_id, PageCtrl pc):"+ e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
