package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorQuoteMgrZJ {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 根据商品ID获得所有分区毛利
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductProfitByPcid(long pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_profit")+" where pid=?";
			
			DBRow para = new DBRow();
			para.add("pid",pc_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e)
		{
			throw new Exception("FloorQutyMgrZJ getProductProfitByPcid error:"+e);
		}
	}
	
	/**
	 * 根据rpiId获得调价记录与调价明细
	 * @param rpi_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getRetailPriceAndItemByRpiId(long rpi_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("retail_price_items")+" as rpi"
					   +" left join "+ConfigBean.getStringValue("retail_price")+" as rp on rp.rp_id = rpi.rp_id"
					   +" where rpi.rpi_id=?";
			
			DBRow para = new DBRow();
			para.add("rpi_id",rpi_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorQutyMgrZJ getRetailPriceAndItemByRpiId error:"+e);
		}
	}
	
	public DBRow[] getAllRpiFromProductProfit()
		throws Exception
	{
		String sql = "select DISTINCT rpi_id from product_profit where rpi_id>0";
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	public void delAllProductProfitByRpiId(long rpi_id) 
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete(" where rpi_id="+rpi_id,ConfigBean.getStringValue("product_profit"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorQuoteMgrZJ delAllProductProfitByRpiId error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
