package com.cwc.app.floor.api.ll;

import com.cwc.app.beans.ll.ApplyMoneyAssociationTypeBeanLL;
import com.cwc.app.beans.ll.ApplyMoneyBeanLL;
import com.cwc.app.beans.ll.ApplyMoneyCategoryBeanLL;
import com.cwc.app.beans.ll.ApplyTransferBeanLL;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class FloorApplyMoneyMgrLL {
	private ApplyMoneyBeanLL applyMoneyBeanLL;
	private ApplyMoneyAssociationTypeBeanLL applyMoneyAssociationTypeBeanLL;
	private ApplyTransferBeanLL applyTransferBeanLL;
	private ApplyMoneyCategoryBeanLL applyMoneyCategoryBeanLL;
	
	public ApplyMoneyCategoryBeanLL getApplyMoneyCategoryBeanLL() {
		return applyMoneyCategoryBeanLL;
	}

	public void setApplyMoneyCategoryBeanLL(
			ApplyMoneyCategoryBeanLL applyMoneyCategoryBeanLL) {
		this.applyMoneyCategoryBeanLL = applyMoneyCategoryBeanLL;
	}

	public ApplyTransferBeanLL getApplyTransferBeanLL() {
		return applyTransferBeanLL;
	}

	public void setApplyTransferBeanLL(ApplyTransferBeanLL applyTransferBeanLL) {
		this.applyTransferBeanLL = applyTransferBeanLL;
	}

	public ApplyMoneyBeanLL getApplyMoneyBeanLL() {
		return applyMoneyBeanLL;
	}

	public void setApplyMoneyBeanLL(ApplyMoneyBeanLL applyMoneyBeanLL) {
		this.applyMoneyBeanLL = applyMoneyBeanLL;
	}

	public ApplyMoneyAssociationTypeBeanLL getApplyMoneyAssociationTypeBeanLL() {
		return applyMoneyAssociationTypeBeanLL;
	}

	public void setApplyMoneyAssociationTypeBeanLL(
			ApplyMoneyAssociationTypeBeanLL applyMoneyAssociationTypeBeanLL) {
		this.applyMoneyAssociationTypeBeanLL = applyMoneyAssociationTypeBeanLL;
	}

	public DBRow insertRow(DBRow row) throws Exception {
		return applyMoneyBeanLL.insertRow(row);
	}
	
	public DBRow[] getMoneyAssociationTypeAll() throws Exception {
		return applyMoneyAssociationTypeBeanLL.getAll();
	}
	
	public DBRow getMoneyAssociationTypeById(String id)  throws Exception {
		return applyMoneyAssociationTypeBeanLL.getRowById(id);
	}
	
	public DBRow getRowById(String id) throws Exception {
		return applyMoneyBeanLL.getRowById(id);
	}
	
	
	public DBRow[] getApplyTransferByBusiness(String businessId,int associationTypeId) throws Exception {
		DBRow para = new DBRow();
		para.add("association_type_id", associationTypeId);
		para.add("association_id", businessId);
		DBRow[] applyMoneyRows = applyMoneyBeanLL.getRowsByPara(para, null, null, "", null);
		if(applyMoneyRows.length>0) 
		{
			String ids = "1";
			for(int i=0;i<applyMoneyRows.length;i++) 
			{
				ids += ","+applyMoneyRows[i].get("apply_id", 0);
			}
			
			return applyTransferBeanLL.getDBRowsByFKIds("apply_money_id",ids, null);
		}
		else
			return null;
	}
	
	public DBRow[] getApplyMoneyByBusiness(String businessId,int associationTypeId) throws Exception {
		DBRow para = new DBRow();
		para.add("association_type_id", associationTypeId);
		para.add("association_id", businessId);
		DBRow[] applyMoneyRows = applyMoneyBeanLL.getRowsByPara(para, null, null, "", null);
		return applyMoneyRows;
	}
	
	public DBRow[] getApplyTransferByApplyMoney(String id) throws Exception {
		DBRow para = new DBRow();
		para.add("apply_money_id", id);
		return applyTransferBeanLL.getRowsByPara(para, null, null, "", null);
	}
	
	public DBRow getApplyMoneyCategoryById(String id) throws Exception {
		return applyMoneyCategoryBeanLL.getRowById(id);
	}
	
	public DBRow[] getApplyMoneyByIds(String ids, PageCtrl pc) throws Exception {
		return applyMoneyBeanLL.getDBRowsByIds(ids, pc);
	}
	
	public DBRow[] getApplyTransferByIds(String ids, PageCtrl pc) throws Exception {
		return applyTransferBeanLL.getDBRowsByIds(ids, pc);
	}
}
