package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorWmsLoadMgrZr {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}	
	
	public long addWmsLoad(DBRow insertLoad) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("wms_load_load") ;
 			return dbUtilAutoTran.insertReturnId(tableName, insertLoad);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.addWmsLoad(row):"+e);
 		}
	}
	
	public long addWmsLoadOrder(DBRow insertOrder) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("wms_load_order") ;
			return dbUtilAutoTran.insertReturnId(tableName, insertOrder);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.addWmsLoad(row):"+e);
 		}
	}
	 
	public void updateLoadOrder(DBRow updateRow , long order_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("wms_load_order") ;
			dbUtilAutoTran.update(" where wms_order_id ="+ order_id, tableName, updateRow);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.updateLoadOrder(row,order_id):"+e);
 		}
	}
	public void updateLoad(DBRow updateRow, long wms_load_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("wms_load_load") ;
			dbUtilAutoTran.update(" where wms_load_id ="+ wms_load_id, tableName, updateRow);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.updateLoad(updateRow,load_id):"+e);
		}
	}
	
	public DBRow getLoadBy(String loadNumber , String company_id , String customer_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("wms_load_load") ;
			String sql = "select * from "+ tableName + " where customer_id='"+customer_id+"' and company_id='"+company_id+"' and load_number='"+loadNumber+"'"; 
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.getLoadBy(loadNumber,company_id,customer_id):"+e);
		}
	}
	
	public DBRow[] getOrderByLoadId(long load_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("wms_load_order") ;
			String sql = "select * from " + tableName + " where  wms_load_id=" + load_id ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.getOrderByLoadId(load_id):"+e);
		}
	}
	public long addWmsMastBolRow(DBRow insertRow) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("wms_load_master_bol") ;
			return dbUtilAutoTran.insertReturnId(tableName, insertRow);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.addWmsMastBolRow(row):"+e);
		}
	}
	public DBRow[] getWmsMastBol(long load_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("wms_load_master_bol") ;
			String sql  = "select * from " + tableName + " where wms_load_id = " +load_id ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.getWmsMastBol(load_id):"+e);
		}
	}
	//>>>>>>>>>>>>>>>>>>8.27添加<<<<<<<<<<<<<<<
	public void deleteWmsMastBol(long wms_master_bol_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("wms_load_master_bol") ;
			dbUtilAutoTran.delete(" where wms_master_bol_id=" + wms_master_bol_id, tableName);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.deleteWmsMastBol(wms_master_bol_id):"+e);
		}
	}
	public void deleteWmsOrderByMasterBolId(long wms_master_bol_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("wms_load_order") ;
			dbUtilAutoTran.delete(" where master_bol_id=" + wms_master_bol_id, tableName);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.deleteOWmsOrderByMasterBolId(wms_master_bol_id):"+e);
		}
	}
	public DBRow getLoadInfoByLoadId(long wms_load_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("wms_load_load") ;
			String sql = "select  * from " + tableName +" where wms_load_id=" +wms_load_id ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.getLoadInfoByLoadId(wms_load_id):"+e);
		}
	}
	public DBRow getMasterBolBy(String master_bol_no , String customer_id , String company_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("wms_load_master_bol") ;
			String sql = "select * from " + tableName + " where master_bol_no='"+master_bol_no+"'";
					sql +=" and company_id='"+company_id+"'";
					sql +=" and customer_id='"+customer_id+"'";
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.getLoadInfoByLoadId(wms_load_id):"+e);
		}
	}
	public DBRow getOrderBy(long wms_master_bol_id , String order_numbers) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("wms_load_order") ;
			String sql = "select * from " + tableName + " where master_bol_id="+wms_master_bol_id;
			sql +=" and order_numbers='"+order_numbers+"'";
 			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.getOrderBy(wms_master_bol_id,order_numbers):"+e);
		}
	}
	public long addPalletTypeCount(DBRow  palletTypeCountRow ) throws Exception{
		try{
			String tablename = ConfigBean.getStringValue("wms_load_order_pallet_type_count") ;
			return dbUtilAutoTran.insertReturnId(tablename, palletTypeCountRow);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.getOrderBy(addPalletTypeCount):"+e);
		}
	}
	public DBRow[] getLoadByEntryIdAndLoadNumber(String load_no , long entry_id ) throws Exception{
		try{
			String tablename = ConfigBean.getStringValue("wms_load_load") ;
			String sql = "select * from " + tablename + " where load_number='"+load_no+"'";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorWmsLoadMgrZr.getOrderBy(addPalletTypeCount):"+e);
		}
	}
	/*************************************09-09**************************************************/
	 
	public void deleteMasterBolByDetailId(long detail_id) throws Exception{
		try{
  			dbUtilAutoTran.delete(" where dlo_detail_id=" + detail_id, ConfigBean.getStringValue("wms_load_master_bol"));
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.deleteMasterBolByDetailId(detail_id):"+e);
		}
	}
 
/*	public void deleteOrderByDetailId(long detail_id) throws Exception{
		try{
 			dbUtilAutoTran.delete(" where dlo_detail_id=" + detail_id, ConfigBean.getStringValue("wms_load_order"));
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.deleteOrderDetailId(detail_id):"+e);
		}
	}*/
/*	public void deleteOrderByDloDetailIdAndMasterBolNo(long dlo_detail_id , String master_bol_no ) throws Exception{
		try{
			dbUtilAutoTran.delete(" where  dlo_detail_id="+dlo_detail_id + " and master_bol_no='"+master_bol_no+"'", ConfigBean.getStringValue("wms_load_order"));
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.deleteOrderByDloDetailIdAndMasterBolNo(dlo_detail_id,master_bol_no):"+e);
		}
	}*/
	public DBRow[] getOrderInfoByDetailIdAndMasterBol(long lr_id , String master_bol_no) throws Exception{
		try{
			if(StrUtil.isNull(master_bol_no)){
				String sql = "select * from " + ConfigBean.getStringValue("wms_load_order") + " where lr_id = " + lr_id ;
				return dbUtilAutoTran.selectMutliple(sql);
 			}else{
 				String sql = "select * from " + ConfigBean.getStringValue("wms_load_order") + " where lr_id = " + lr_id + " and master_bol_no='"+master_bol_no+"'";
				return dbUtilAutoTran.selectMutliple(sql);
			}
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getOrderInfoByDetailIdAndMasterBol(lr_id):"+e);
		}
	}
	public void deleteOrderPalletTypeBy(long order_id) throws Exception{
		try{
 			dbUtilAutoTran.delete(" where wms_order_id=" + order_id, ConfigBean.getStringValue("wms_load_order_pallet_type_count"));
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.deleteOrderPalletTypeByF(order_id):"+e);
		}
	}
	public DBRow getOrderByLRIDAndOrderNumbers(long lr_id  , String order_number) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("wms_load_order")  + " where lr_id="+lr_id ;
			sql += " and order_numbers='"+order_number+"'";
			//System.out.println(sql);
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getOrderByDetailIdAndOrderNumbers(lr_id , order_number):"+e);
		}
	}
	public void updateWmsLoadOrderPalletTypeBy(long wms_order_id, String pallet_number, DBRow udpateRow) throws Exception{
		try{
			dbUtilAutoTran.update(" where wms_order_id="+wms_order_id + " and wms_pallet_number='"+pallet_number+"'", ConfigBean.getStringValue("wms_load_order_pallet_type_count"), udpateRow);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.updateWmsLoadOrderPalletTypeBy(wms_order_id ,pallet_number, udpateRow):"+e);
		}
	}
	public void updateWmsLoadOrderPalletTypeBy(long wms_order_type_id, DBRow  udpateRow) throws Exception{
		try{
			dbUtilAutoTran.update(" where wms_order_type_id="+wms_order_type_id  , ConfigBean.getStringValue("wms_load_order_pallet_type_count"), udpateRow);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.updateWmsLoadOrderPalletTypeBy(wms_order_id ,pallet_number, udpateRow):"+e);
		}
	}
	
	public DBRow[] getOrderInfoByLrId(long lr_id ,  long wms_master_bol_id) throws Exception {
		try{
			String sql = "select * from " + ConfigBean.getStringValue("wms_load_order") + " where master_bol_id =" + wms_master_bol_id + " and lr_id="+ lr_id ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getOrderInfoByEntryDetailId(lr_id ,wms_master_bol_id):"+e);
		}
	}
	public DBRow getMasterBolByEntryDetailIdAndMasterBolNo(long lr_id , String master_bol_no) throws Exception{
		try{
			String sql = "select * from " +  ConfigBean.getStringValue("wms_load_master_bol") + " where lr_id ="+ lr_id + " and master_bol_no='"+master_bol_no+"'";
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getMasterBolByEntryDetailIdAndMasterBolNo(lr_id ,master_bol_no):"+e);
		}
	}
	public int updateMasterBol(long wms_master_bol_id , DBRow updateMasterRow) throws Exception{
		try{
			return dbUtilAutoTran.update(" where wms_master_bol_id="+ wms_master_bol_id, ConfigBean.getStringValue("wms_load_master_bol"), updateMasterRow);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getMasterBolByEntryDetailIdAndMasterBolNo(dlo_detail_id ,master_bol_no):"+e);
		}
	}
	public void deleteWmsOrderBy(long wms_order_id) throws Exception{
		try{
			dbUtilAutoTran.delete(" where wms_order_id="+ wms_order_id, ConfigBean.getStringValue("wms_load_order"));
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.deleteWmsOrderBy(wms_order_id ):"+e);
		}
	}
	public DBRow[] getOrderPalletInfoBy(long dlo_detail_id,long wms_order_id) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("wms_load_order_pallet_type_count") + " where wms_order_id="+wms_order_id; 
			//显示主页面loadInfo 因为结构变了所以加上这个参数    wfh
			if(dlo_detail_id>0){
				sql +=" and dlo_detail_id = "+dlo_detail_id;
			}
			sql +="  group by wms_pallet_number";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getOrderPalletInfo(wms_order_id ):"+e);
		}
	}
	
	
	public void updateOrderStaticByLrId(long lr_id , DBRow udpateRow) throws Exception {
		try{
			dbUtilAutoTran.update(" where lr_id="+lr_id, ConfigBean.getStringValue("wms_load_order") , udpateRow);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.updateOrderStaticByDetailId(lr_id,udpateRow ):"+e);
		}
	}
	public void updateOrderStaticByLrIdAndMasterBol(long lr_id , String master_bol_no , DBRow udpateRow) throws Exception {
		try{
			dbUtilAutoTran.update("where lr_id="+lr_id + " and master_bol_no='"+master_bol_no+"'", ConfigBean.getStringValue("wms_load_order") , udpateRow);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.updateOrderStaticByDetailIdAndMasterBol(lr_id,udpateRow ):"+e);
		}
	}
	public void udpateMaseterBolByNoAndLrId(long lr_id ,  String master_bol_no ,DBRow udpateRow) throws Exception{
		try{
			dbUtilAutoTran.update("where lr_id="+lr_id + " and master_bol_no='"+master_bol_no+"'" , ConfigBean.getStringValue("wms_load_master_bol"), udpateRow);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.udpateMaseterBolByNoAndDetailId(lr_id,master_bol_no ):"+e);
		}
	}
	public void updateOrderProNoByLrId(long lr_id , String master_bol_no , String pro_no ) throws Exception{
		try{
			DBRow updateRow = new DBRow();
			updateRow.add("pro_no", pro_no);
			dbUtilAutoTran.update(" where    IFNULL(wms_load_order.pro_no,'') =''  and  master_bol_no='"+master_bol_no+"' and lr_id="+lr_id, ConfigBean.getStringValue("wms_load_order"), updateRow);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.updateOrderProNoByEntryDetailId(lr_id,master_bol_no,pro_no ):"+e);
		}
	}
	public DBRow[] getMasterBolBy(long lr_id)throws Exception {
		try{
			String sql = "select * from " +ConfigBean.getStringValue("wms_load_master_bol") +" where lr_id="+lr_id ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getMasterBolBy(lr_id,master_bol_no,pro_no ):"+e);
		}
	}
	public DBRow getPalletTypeBy(long wms_order_id , String pallet_number) throws Exception{
		try{
			String sql = "select * from " + ConfigBean.getStringValue("wms_load_order_pallet_type_count") +" where wms_consolidate_pallet_number='"+pallet_number+"' and wms_order_id="+wms_order_id ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getPalletTypeBy(wms_order_id,pallet_number ):"+e);
		}
	}
	public int updatePalletTypeBy(long wms_order_type_id , DBRow updateRow) throws Exception{
		try{
			return dbUtilAutoTran.update(" where wms_order_type_id = "+ wms_order_type_id, ConfigBean.getStringValue("wms_load_order_pallet_type_count"), updateRow);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.updatePalletTypeBy(wms_order_type_id,updateRow):"+e);
		}
	}
	 
	public DBRow getMasterBolByLrIdAndMasterBolNo(long lr_id, String master_bol_no) throws Exception{
		try{
			String sql = "select * from " +   ConfigBean.getStringValue("wms_load_master_bol") + " where master_bol_no='"+master_bol_no+"' and lr_id="+lr_id ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getMasterBolByLrIdAndMasterBolNo(lr_id,master_bol_no):"+e);
		}
	}
	public int updateOrder(long wms_order_id , DBRow udpateRow) throws Exception{
		try{
			return dbUtilAutoTran.update(" where wms_order_id ="+wms_order_id, ConfigBean.getStringValue("wms_load_order"), udpateRow);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.updateOrder(wms_order_id,udpateRow):"+e);
		}
	}
	public DBRow getMasterBolByWmsMasterBolId(long wms_master_bol_id) throws Exception{
		try{
			String sql = "select * from " +  ConfigBean.getStringValue("wms_load_master_bol") + " where wms_master_bol_id="+wms_master_bol_id ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getMasterBolByWmsMasterBolId(wms_master_bol_id):"+e);
		}
	}
	public void updateMasterBolBy(long wms_master_bol_id , DBRow updateRow) throws Exception{
		try{
			dbUtilAutoTran.update(" where wms_master_bol_id="+wms_master_bol_id, ConfigBean.getStringValue("wms_load_master_bol"), updateRow);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.updateMasterBolBy(wms_master_bol_id,updateRow):"+e);
		}
	}
  
	public DBRow getOrderConsolidateRow(long wms_order_id) throws Exception{
		try{
			String sql = "select count(*) as sum from wms_load_order_pallet_type_count where  IFNULL(wms_consolidate_reason,0) > 0 and  wms_order_id ="+wms_order_id ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getOrderConsolidateRow(wms_order_id):"+e);
		}
	}
	
	public DBRow getPalletBy(long dlo_detail_id , String order_number , String pallet_no) throws Exception{
		try{
			String sql = "SELECT * FROM	wms_load_order "
					+ " JOIN wms_load_order_pallet_type_count AS pallet ON pallet.wms_order_id = wms_load_order.wms_order_id  "
					+ " WHERE pallet.dlo_detail_id = "+dlo_detail_id+" AND order_numbers = '"+order_number+"' and pallet.wms_pallet_number = '"+pallet_no+"'" ;
 			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getPalletBy():"+e);
		}
	}
	public int deletePallet(long wms_order_type_id) throws Exception{
		try{
			return dbUtilAutoTran.delete(" where  wms_order_type_id="+wms_order_type_id, ConfigBean.getStringValue("wms_load_order_pallet_type_count"));
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.deletePallet():"+e);
		}
	}
	
	/**
	 * 查询所有的WmsLoadOrders
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2014年12月24日 下午12:45:01
	 */
	public DBRow[] findWmsLoadOrders(long start, long end) throws Exception
	{
		try{
			return dbUtilAutoTran.selectMutliple("select * from wms_load_order where wms_order_id > "+start +" and wms_order_id <="+end+" order by wms_order_id desc");
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.findWmsLoadOrders():"+e);
		}
	}
	public void deleteTaskLoadPallet(long dlo_detail_id) throws Exception{
		try{
			dbUtilAutoTran.delete(" where dlo_detail_id="+dlo_detail_id, "wms_load_order_pallet_type_count");
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.deleteTaskLoadPallet(dlo_detail_id):"+e);
		}
	}
	
	public DBRow[] getCommonLoadRefresh(long lr_id) throws Exception{
		try{ 
			String tableName = ConfigBean.getStringValue("wms_load_order_pallet_type_count") ;
			String sql = "select * from " + tableName + " where  lr_id="+lr_id  + " and IFNULL(wms_pallet_type_count,0) > 0";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getCommonLoadRefresh(lr_id):"+e);
		}
	}
	public long commonLoadPallet(DBRow insertRow) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("wms_load_order_pallet_type_count") ;
			return dbUtilAutoTran.insertReturnId(tableName, insertRow);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.commonLoadPallet(insertRow):"+e);
		}
	}
	public int commonLoadDelete(long wms_order_type_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("wms_load_order_pallet_type_count") ;
			return dbUtilAutoTran.delete(" where wms_order_type_id="+wms_order_type_id, tableName);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.commonLoadDelete(wms_order_type_id):"+e);
		}
	}
	public int commonLoadModify(long wms_order_type_id , DBRow updateRow) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("wms_load_order_pallet_type_count") ;
			return dbUtilAutoTran.update(" where wms_order_type_id="+wms_order_type_id, tableName, updateRow);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.commonLoadModify(wms_order_type_id):"+e);
		}
	}
	public void commonLoadDeletePalletBy(long lr_id) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("wms_load_order_pallet_type_count") ;
			dbUtilAutoTran.delete(" where lr_id="+lr_id, tableName);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.commonLoadDeletePallet():"+e);
		}
	}
	//heckInIndex查询pallet用的方法  wfh  按照detailId查询masterBol
	public DBRow[] getMasterBolInfoByDetailId(long dlo_detail_id) throws Exception{
		try{
			String sql = "SELECT count(DISTINCT wlop.wms_pallet_number) as total_pallets, wlm.* "
					+ " FROM wms_load_master_bol wlm "
					+ " JOIN wms_load_order wlo ON  wlm.wms_master_bol_id = wlo.master_bol_id"
					+ " JOIN wms_load_order_pallet_type_count wlop ON wlop.wms_order_id = wlo.wms_order_id"
					+ " AND wlop.dlo_detail_id = "+dlo_detail_id+" GROUP BY wlm.master_bol_no";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getMasterBolInfoByDetailId():"+e);
		}
	}
	
	//checkInIndex查询pallet用的方法  wfh 按照detailId，masterbolNo查询order
	public DBRow[] getOrderByDetailAndMasterBol(long dlo_detail_id, String masterBolNo) throws Exception{
		try{
			String sql = "SELECT count(DISTINCT wlop.wms_pallet_number) as total_pallets, wlo.order_numbers, wlo.reference_no, wlo.po_no, wlo.wms_order_id, wlo.pro_no "
					+ " FROM wms_load_order wlo "
					+ " JOIN wms_load_order_pallet_type_count wlop ON wlop.wms_order_id = wlo.wms_order_id AND wlop.dlo_detail_id ="+dlo_detail_id
					+ " WHERE wlo.master_bol_no ='"+masterBolNo+"' GROUP BY wlo.wms_order_id";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getOrderByDetailAndMasterBol():"+e);
		}
	}
	//checkInIndex查询pallet用的方法  wfh 按照detailId，masterbolNo查询order,包括没有Pallet的Order
	public DBRow[] getOrderByDetailAndMasterBolIncludeZeroPallet(long dlo_detail_id, String masterBolNo) throws Exception{
		try{
			String sql = "SELECT count(DISTINCT wlop.wms_pallet_number) as total_pallets, wlo.order_numbers, wlo.reference_no, wlo.po_no, wlo.wms_order_id, wlo.pro_no "
					+ " FROM wms_load_order wlo "
					+ " LEFT JOIN wms_load_order_pallet_type_count wlop ON wlop.wms_order_id = wlo.wms_order_id Where wlop.dlo_detail_id ="+dlo_detail_id
					+ " And wlo.master_bol_no ='"+masterBolNo+"' GROUP BY wlo.wms_order_id";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorWmsLoadMgrZr.getOrderByDetailAndMasterBolIncludeZeroPallet():"+e);
		}
	}
	
	
	
}
