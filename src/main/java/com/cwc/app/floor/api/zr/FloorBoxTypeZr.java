package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorBoxTypeZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	public DBRow[] getAllBoxTypeByPage(PageCtrl pageCtrl) throws Exception{
		try{
			 String boxType =  ConfigBean.getStringValue("box_type");	 
			 String containerType = ConfigBean.getStringValue("container_type");
			 
			 String sql = "select  bt.* , ct.type_name from  "+boxType+"  as bt left join "+containerType+" as ct on bt.container_type_id = ct.type_id  ORDER BY box_type_id desc ";
			 return  dbUtilAutoTran.selectMutliple(sql, pageCtrl) ;
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr.getAllBoxTypeByPage(pageCtrl):"+e);
		}
	}
	public long addBoxType(DBRow data) throws Exception {
		try{
			 String tableName =  ConfigBean.getStringValue("license_plate_type");	 

			 return  dbUtilAutoTran.insertReturnId(tableName, data);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr.addBoxType(row):"+e);
		}
	}
	public void updateBoxType(long type_id ,DBRow row) throws Exception {
		try{
			 String tableName =  ConfigBean.getStringValue("license_plate_type");	 

			 dbUtilAutoTran.update(" where lpt_id ="+type_id, tableName, row);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr.updateBoxType(type_id,row):"+e);
		}
	}
	public void deleteBoxType(long type_id) throws Exception {
		try{
			 String tableName =  ConfigBean.getStringValue("box_type");	 
			 dbUtilAutoTran.delete(" where box_type_id = " + type_id, tableName);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr.deleteBoxType(type_id):"+e);
		}
	}
	public DBRow getBoxTypeById(long type_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("license_plate_type");
			DBRow para = new DBRow();
			para.add("lpt_id",type_id);
			return dbUtilAutoTran.selectPreSingle("select * from " + tableName + " where lpt_id = ? " ,para);
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.getBoxTypeById(type_id):"+e);
		}
	}
	public DBRow[] getBoxContainerType(int container_type) throws Exception{
		try{
			String tableName = ConfigBean.getStringValue("container_type");
			return dbUtilAutoTran.selectMutliple("select * from " + tableName + " order by type_id desc ");
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.getBoxContainerType():"+e);
		}
	}
	public DBRow getContainerByName(String contaimer_type_Name) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("container_type");
			DBRow para = new DBRow();
			para.add("type_name", contaimer_type_Name);
//			para.add("container_type", ContainerTypeKey.BLP);//去掉ILP和BLP
			
//			return dbUtilAutoTran.selectPreSingle("select * from " + tableName + " where type_name = ? and container_type = ? ", para);
			return dbUtilAutoTran.selectPreSingle("select * from " + tableName + " where type_name = ?", para);//去掉ILP和BLP
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.getBoxContainerType():"+e);
		}
	}
	/**
	 * 
	 * @param search_key
	 * @param container_type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBoxTypeByNameAndContainerType(String search_key, long container_type_id , PageCtrl pc ) throws Exception{
		try{
			 String tableName =  ConfigBean.getStringValue("box_type");	
 			 StringBuffer sql = new StringBuffer("select * from ").append(tableName);
			 if(search_key != null && search_key.trim().length() > 0){
				 
				 sql.append("  right join ("+
						 	" select pc_id from product where p_name like '%"+search_key+"%'" +
				 			" ) as  p on   p.pc_id = box_type.box_pc_id " );
			 }
			 sql.append(" left join container_type  as ct on ct.type_id = box_type.container_type_id  ");
			 sql.append(" where 1=1 and box_type_id > 0 ");
			 if(container_type_id != 0l){
 				 sql.append(" and container_type_id = ").append(container_type_id);
			 }			
			 sql.append(" order by box_type_id desc ");
			return  dbUtilAutoTran.selectMutliple(sql.toString(), pc);
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.getBoxTypeByNameAndContainerType():"+e);

 		}
	}
	public DBRow[] getBoxTypesByPcId(long pcid , int limit) throws Exception  {
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT bt.*, ct.type_name, bp.ship_to_id");
			sql.append(" FROM license_plate_type bt");
			sql.append(" LEFT JOIN container_type AS ct ON ct.type_id = bt.basic_type_id");
			sql.append(" LEFT JOIN lp_title_ship_to bp ON bp.lp_type_id = bt.lpt_id");
			sql.append(" WHERE bt.pc_id = ?");
			sql.append(" AND bt.container_type = 2");
			sql.append(" AND bp.ship_to_id is null");
			sql.append(" ORDER BY sort  asc ");
			DBRow para = new DBRow();
			para.add("pc_id", pcid);
			if(limit != 0){
				sql.append(" limit 0,"+limit);
			}
			//System.out.println("=================="+"ss:"+pcid+","+limit+","+sql);
 			return dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.getBoxTypesByPcId(pcid,limit):"+e);
		}
	}
	public DBRow[] getBoxTypesByPcIdAndContainerTypeId(long pcid , long container_type_id) throws Exception{
		try{
			DBRow para = new DBRow();
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT bt.*, ct.type_name, bp.ship_to_id");
			sql.append(" FROM license_plate_type bt");
			sql.append(" LEFT JOIN container_type AS ct ON ct.type_id = bt.basic_type_id");
			sql.append(" LEFT JOIN lp_title_ship_to bp ON bp.lp_type_id = bt.lpt_id ");
			sql.append(" WHERE bt.pc_id = ?");
			sql.append(" AND bt.container_type = 2");
			sql.append(" AND bp.ship_to_id is null");
			para.add("pc_id", pcid);
			if(container_type_id != 0l){
				sql.append(" and bt.inner_pc_or_lp = ? ");
				para.add("inner_pc_or_lp", container_type_id);
			}
			sql.append(" ORDER BY bt.sort asc ");
		//System.out.println("****************1"+"s:"+pcid+","+container_type_id+","+sql);
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), para) ;
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.getBoxTypesByPcIdAndContainerTypeId(pcid,container_type_id):"+e);
 		}
	}
	//根据pc_id , container_type_id 查询PName
	public DBRow[] getBoxTypesGetPNameAndContainerName(long pc_id , long container_type_id) throws Exception{
		try{
			StringBuffer sql = new StringBuffer("select bt.* , ct.type_name ,p.p_name from license_plate_type as bt left join container_type as ct on ct.type_id = bt.basic_type_id " );
			sql.append(" left join product as p on bt.pc_id = p.pc_id  where bt.pc_id =? ");
			DBRow para = new DBRow();
			para.add("pc_id", pc_id);
			if(container_type_id != 0l){
				para.add("basic_type_id", container_type_id);
				sql.append(" and basic_type_id = ? ");
			}
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
 		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.getBoxTypesGetPNameAndContainerName(pc_id,container_type_id):"+e);
		}
	}
	public DBRow[] getBoxSkuByPcId(long pc_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("license_plate_type");	
			StringBuffer sql = new StringBuffer("select * from ");
			sql.append(tableName);
			sql.append(" where pc_id = ? ");
			DBRow para = new DBRow();
			para.add("pc_id", pc_id);
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
			
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.getBoxSkuByPcId(pc_id):"+e);

 		}
	}
	public long addBoxContainer(DBRow row) throws Exception {
		try{
			//改成获取sequence
			String tableName = ConfigBean.getStringValue("container");
//			long containerId = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("container"));
//			row.add("con_id", containerId);
			return dbUtilAutoTran.insertReturnId(tableName, row);
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.addBoxContainer(row):"+e);
 		}
	}
	public boolean isExitsHardwareId(String hardwareId ) throws Exception {
		boolean flag = false ;
		try{
			String tableName = ConfigBean.getStringValue("container");
			StringBuffer sql = new StringBuffer();
			sql.append("select count(*) as count_sum from ").append(tableName).append(" where hardwareId= ? ");
			DBRow para = new DBRow();
			para.add("hardwareId", hardwareId);
			DBRow data = dbUtilAutoTran.selectPreSingle(sql.toString(), para);
			if(data != null && data.get("count_sum", 0) > 0){
				flag = true ;
			}
			return flag;
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.isExitsHardwareId(hardwareId):"+e);
		}
	}
	public boolean isExitsHardwareId(String hardwareId , long con_id) throws Exception {
		boolean flag = false ;
		try{
			String tableName = ConfigBean.getStringValue("container");
			StringBuffer sql = new StringBuffer();
			sql.append("select count(*) as count_sum from ").append(tableName).append(" where hardwareId= ? ");
			sql.append(" and con_id != ").append(con_id);
			DBRow para = new DBRow();
			para.add("hardwareId", hardwareId);
			DBRow data = dbUtilAutoTran.selectPreSingle(sql.toString(), para);
			if(data != null && data.get("count_sum", 0) > 0){
				flag = true ;
			}
			return flag;
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.isExitsHardwareId(hardwareId):"+e);
		}
	}
	public DBRow[] getBoxContainer(PageCtrl page , long box_type_id,long container_type) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("container");

			StringBuffer sql = new StringBuffer();
			sql.append("select * from ").append(tableName).append(" where type_id =").append(box_type_id).append(" and container_type=").append(container_type)
			.append(" order by con_id desc ");
			return dbUtilAutoTran.selectMutliple(sql.toString(), page);
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.getBoxContainer(page,box_type_id):"+e);
		}
	}
	public void deleteBoxContainer(long con_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("container");
			dbUtilAutoTran.delete(" where con_id ="+ con_id, tableName);
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.deleteBoxContainer(con_id):"+e);
		}
	}
	public void updateBoxContainer(long con_id ,DBRow row) throws Exception {
		try{
			 String tableName =  ConfigBean.getStringValue("container");	 

			 dbUtilAutoTran.update(" where con_id ="+con_id, tableName, row);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr.updateBoxContainer(con_id,row):"+e);
		}
	}
	public DBRow[] getBoxContainerBySearchKey(PageCtrl page, long box_type_id,
			String search_key,long containerType) throws Exception{
		
		try{
			StringBuffer sql = new StringBuffer("select * from container where  1=1 ") ;
			if(search_key != null && search_key.trim().length() > 0){
				sql.append(" and (container like '%"+search_key+"%' or hardwareId like '%"+search_key+"%')");
			}
			sql.append("and type_id ="+box_type_id+" and container_type ="+containerType + " order by con_id desc ");
			
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), page);
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.getBoxContainerBySearchKey(page,box_type_id,search_key):"+e);
		}
	}
	public DBRow getBoxTypeDetailById(long type_id) throws Exception {
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("select  bt.* , p.p_name from license_plate_type as bt " );
			sql.append(" left join product as p on p.pc_id = bt.pc_id ");
			sql.append(" where bt.lpt_id ="+type_id );

 
			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.getBoxTypeDetailById(type_id):"+e);
		}
	}
	public int countNumBy(long pc_id) throws Exception {
		try{
			String tableName =  ConfigBean.getStringValue("license_plate_type");	 
			String sql = "select max(sort) as count_sum from "+tableName +" where pc_id = "+ pc_id;
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			return result.get("count_sum", 0);
		}catch (Exception e) {
			throw new Exception("FloorBoxTypeZr.countNumBy(pc_id):"+e);
		}
	}
	//查询ilp根据商品
	public DBRow[] selectIlp(long productId)throws Exception{
		try{
			String sql="select ibt.*,ct.type_name from license_plate_type ibt join container_type ct on ibt.basic_type_id=ct.type_id where ibt.pc_id="+productId;
			sql += " AND ibt.container_type= 4 ";
			sql += " order by ct.type_id, ibt.stack_length_qty, ibt.stack_width_qty, ibt.stack_height_qty";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr selectIlp()"+e);
		}
	}
	//查询ilp类型
	public DBRow[] selectIlpType(long productId)throws Exception{
		try{
			String sql="select * from license_plate_type ibt " +
					"join container_type ct on ibt.basic_type_id=ct.type_id " +
					"where ct.container_type=4 and pc_id="+productId;
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr selectIlpType()"+e);
		}
	}
	//查询ilp容器类型
	public DBRow[] selectContainerType()throws Exception{
		try{
			String sql="select * from container_type where container_type=4";
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr selectContainerType()"+e);
		}
	}
	//根据id查询ilp
	public DBRow findIlpByIlpId(long ilpId)throws Exception{
		try{
			String sql="select * from license_plate_type where lpt_id="+ilpId;
			return this.dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr findIlpByIlpId()"+e);
		}
	}
	//添加ilp 类型
	public long addBoxIlp(DBRow row)throws Exception{
		try{
			return this.dbUtilAutoTran.insertReturnId("license_plate_type",row);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr addBoxIlp()"+e);
		}
	}
	
	//ilp 类型查询
	public DBRow[] seachIlpContainerType(long productId,long ilp_container_type)throws Exception{
		try{
			String sql="select * from container_type ct " +
					"join license_plate_type ibt on ct.type_id=ibt.basic_type_id " +
					"where container_type=4 and pc_id="+productId+" and type_id="+ilp_container_type;
					
			return this.dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr seachIlpContainerType"+e);
		}
	}
	
	//更新ilp
	public void updateIlpType(long ilp_id ,DBRow row) throws Exception {
		try{
			 String tableName = "inner_box_type ";	 
			 dbUtilAutoTran.update(" where ibt_id ="+ilp_id, tableName, row);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr.updateIlpType(type_id,row):"+e);
		}
	}
	//删除ilp
	public void deleteIlpType(long id) throws Exception {
		try{
			 String tableName =  "inner_box_type ";	 
			 dbUtilAutoTran.delete(" where ibt_id = " + id, tableName);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr.deleteIlpType(type_id):"+e);
		}
	}
	
	/**zyj
	 * 验证添加的ILP类型是否已经存在
	 * @param pc_id：商品ID
	 * @param basic_type：基础容器类型ID
	 * @param len：长
	 * @param wid：宽
	 * @param hei：高
	 * @return
	 * @throws Exception
	 */
	public int findIlpTypeByBasicLWW(long pc_id,long basic_type, int len, int wid, int hei)throws Exception
	{
		try{
			 String tableName =  ConfigBean.getStringValue("license_plate_type");	
			 String sql = "select count(*) cn from "+tableName+" where pc_id = "+pc_id+" and stack_length_qty="+len
					 +" and stack_width_qty="+wid+" and stack_height_qty="+hei+" and basic_type_id="+basic_type;
			 DBRow row = dbUtilAutoTran.selectSingle(sql);
			 if(null != row)
			 {
				 return row.get("cn", 0);
			 }
			 return 0;
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr.findIlpTypeByBasicLWW:"+e);
		}
	}
	
	/**zyj
	 * 验证添加的BLP类型是否已经存在
	 * @param pc_id：商品ID
	 * @param basic_type：基础容器类型ID
	 * @param len：长
	 * @param wid：宽
	 * @param hei：高
	 * @return
	 * @throws Exception
	 */
	public int findBlpTypeByBasicLWW(long pc_id,long basic_type, long innerType, int len, int wid, int hei)throws Exception
	{
		try{
			 String tableName =  ConfigBean.getStringValue("license_plate_type");	
			 String sql = "select count(*) cn from "+tableName+" where pc_id = "+pc_id+" and stack_length_qty="+len
					 +" and stack_width_qty="+wid+" and stack_height_qty="+hei+" and inner_total_lp="+innerType+" and basic_type_id="+basic_type;
			 DBRow row = dbUtilAutoTran.selectSingle(sql);
			 if(null != row)
			 {
				 return row.get("cn", 0);
			 }
			 return 0;
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr.findBlpTypeByBasicLWW:"+e);
		}
	}
	
	/**
	 * 通过商品ID和容器名称查询容器类型
	 * @param pc_id
	 * @param lpName
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年3月11日 下午6:41:26
	 */
	public DBRow findLpTypeByPcidLpTypeName(long pc_id, String lpName) throws Exception
	{

		try{
			String sql = "select * from license_plate_type where pc_id = "+pc_id+" and lp_name = '"+lpName+"'";
			return dbUtilAutoTran.selectSingle(sql);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr.findLpTypeByPcidLpTypeName:"+e);
		}
	
}
	/*
	 * ILP锁定，解锁
	 * BLP锁定，解锁
	 * CLP锁定，解锁
	 * */
	public void updateLock(long id ,DBRow row) throws Exception {
		try{
			 String tableName =  ConfigBean.getStringValue("license_plate_type");	 

			 dbUtilAutoTran.update(" where lpt_id ="+id, tableName, row);
		}catch(Exception e){
			throw new Exception("FloorBoxTypeZr.updateBoxContainer(con_id,row):"+e);
		}
	}
	
	public DBRow[] getBoxTypesInfo(long pc_id, int container_type_id, int title_id, int ship_to_id) throws Exception {
		return this.getBoxTypesInfo(pc_id, container_type_id, title_id, ship_to_id, 0L);
	}
	
	/**
	 * 查询商品容器类型信息
	 * @param pc_id
	 * @param container_type_id
	 * @param title_id
	 * @param ship_to_id
	 * zhanglingfeng
	 * @throws Exception
	 */
	public DBRow[] getBoxTypesInfo(long pc_id, int container_type_id,int title_id, int ship_to_id, long active) throws Exception {
		try {
			DBRow para = new DBRow();
			StringBuffer sql = new StringBuffer();
			sql.append("select lpt.*,ct.type_name,newItst.ship_to_ids,newItst.title_ids");
			sql.append(" from license_plate_type lpt");
			sql.append(" join container_type ct on lpt.basic_type_id=ct.type_id");
			sql.append(" left join (select ltst.lp_type_id,GROUP_CONCAT(convert(ltst.ship_to_id,char) separator',') as ship_to_ids,GROUP_CONCAT(convert(ltst.title_id,char) separator',') as title_ids from lp_title_ship_to ltst group by ltst.lp_type_id) newItst on lpt.lpt_id=newItst.lp_type_id");
			sql.append(" where lpt.pc_id = ?");
			para.add("pc_id", pc_id);
			if(container_type_id > 0){
				sql.append(" and lpt.basic_type_id = ?");
				para.add("basic_type_id", container_type_id);
			}
			if(ship_to_id > 0){
				sql.append(" and FIND_IN_SET(?,newItst.ship_to_ids)");
				para.add("ship_to_id", ship_to_id);
			}
			if(title_id > 0){
				sql.append(" and FIND_IN_SET(?,newItst.title_ids)");
				para.add("title_id", title_id);
			}
			
			if(active > 0){
				sql.append(" AND lpt.active = ?");
				para.add("active", active);
			}
			
			sql.append(" order by newItst.ship_to_ids asc,newItst.title_ids asc,lpt.lpt_id desc");
			return this.dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
		} catch (Exception e) {
			throw new Exception("getBoxTypesInfo(long pc_id, int container_type_id,int title_id, int ship_to_id, long active):" + e);
		}
	}

	public DBRow[] getBoxTypesInfo(long pc_id, int container_type_id,
			int title_id, int ship_to_id, int customer_id, long active)
			throws Exception {
		try {
			DBRow para = new DBRow();
			StringBuffer sql = new StringBuffer();
			sql.append("select lpt.*,ct.type_name,newItst.ship_to_ids,newItst.title_ids,newItst.customer_ids ");
			sql.append(" from license_plate_type lpt");
			sql.append(" join container_type ct on lpt.basic_type_id=ct.type_id");
			sql.append(" left join (select ltst.lp_type_id,GROUP_CONCAT(convert(ltst.ship_to_id,char) separator',') as ship_to_ids,GROUP_CONCAT(convert(ltst.title_id,char) separator',') as title_ids,GROUP_CONCAT(convert(ltst.customer_id,char) separator',') as customer_ids from lp_title_ship_to ltst group by ltst.lp_type_id) newItst on lpt.lpt_id=newItst.lp_type_id");
			sql.append(" where lpt.pc_id = ?");
			para.add("pc_id", pc_id);
			if (container_type_id > 0) {
				sql.append(" and lpt.basic_type_id = ?");
				para.add("basic_type_id", container_type_id);
			}
			if (ship_to_id > 0) {
				sql.append(" and (FIND_IN_SET(?,newItst.ship_to_ids) or FIND_IN_SET(0,newItst.ship_to_ids))");
				para.add("ship_to_id", ship_to_id);
			}
			if (title_id > 0) {
				sql.append(" and (FIND_IN_SET(?,newItst.title_ids) or FIND_IN_SET(0,newItst.title_ids))");
				para.add("title_id", title_id);
			}
			if (customer_id > 0) {
				sql.append(" and (FIND_IN_SET(?,newItst.customer_ids) or FIND_IN_SET(0,newItst.customer_ids))");
				para.add("customer_id", customer_id);
			}
			if (active > 0L) {
				sql.append(" AND lpt.active = ?");
				para.add("active", active);
			}
			sql.append(" union(");
			sql.append(" SELECT lpt.*, ct.type_name,NULL as	ship_to_ids,NULL  as	title_ids,NULL as	customer_ids FROM license_plate_type lpt");
			sql.append(" JOIN container_type ct ON lpt.basic_type_id = ct.type_id");
			sql.append(" LEFT JOIN lp_title_ship_to  ltst  on lpt.lpt_id = ltst.lp_type_id ");
			sql.append(" WHERE	lpt.pc_id =" + pc_id + " AND ltst.customer_id is null");
			if (container_type_id > 0) {
				sql.append(" and lpt.basic_type_id =" + container_type_id);
			}			
			if (active > 0L) {
				sql.append(" AND lpt.active =" + active);
			}
			sql.append(") ORDER BY ship_to_ids DESC,title_ids DESC,lpt_id DESC");
			return this.dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
		} catch (Exception e) {
			throw new Exception(
					"getBoxTypesInfo(long pc_id, int container_type_id,int title_id, int ship_to_id, long active):"
							+ e);
		}
	}	
	
	public DBRow[] getClpTypesByTitleAndShipToFloor(long pc_id, long title_id, long ship_to_id) throws Exception {
		return getClpTypesByTitleAndShipToFloor(pc_id, title_id, 0, ship_to_id, 0);
	}
	
	/**
	 * 根据title和shipTo查询商品容器类型信息
	 * @param pc_id
	 * @param title_id
	 * @param ship_to_id
	 * zhanglingfeng
	 * @throws Exception
	 */
	public DBRow[] getClpTypesByTitleAndShipToFloor(long pc_id, long title_id, long customer_id, long ship_to_id, long active) throws Exception {
		
		try {
			
			DBRow para = new DBRow();
			para.add("pc_id", pc_id);
			
			StringBuffer sql = new StringBuffer();
			sql.append(" select lpt.*,ct.type_name, ltst.lp_title_ship_id, ltst.ship_to_sort ");
			sql.append(" from license_plate_type lpt");
			sql.append(" join container_type ct on lpt.basic_type_id=ct.type_id");
			sql.append(" join lp_title_ship_to ltst on lpt.lpt_id=ltst.lp_type_id");
			sql.append(" where lpt.pc_id = ?");
			
			if(title_id != 0){
				sql.append(" and ltst.title_id = ?");
				para.add("title_id", title_id);
			}
			
			if(customer_id != 0){
				sql.append(" and ltst.customer_id = ?");
				para.add("customer_id", customer_id);
			}
			
			if(ship_to_id != 0){
				sql.append(" and ltst.ship_to_id = ?");
				para.add("ship_to_id", ship_to_id);
			}
			
			if(active > 0L){
				sql.append(" AND lpt.active = ?");
				para.add("active", active);
			}
			
			sql.append(" ORDER BY ltst.ship_to_sort asc");

			return this.dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
		} catch (Exception e) {
			throw new Exception("getClpTypesByTitleAndShipToFloor(long pc_id, long title_id, long ship_to_id, long active):" + e);
		}
	}
}
