package com.cwc.app.floor.api;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorTaskMgr
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	public long createOrderTask(DBRow row)
		throws Exception
	{
		try
		{
			long ot_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("order_task"));
			row.add("ot_id", ot_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("order_task"),row);
			
			return(ot_id);
		}
		catch (Exception e)
		{
			throw new Exception("createOrderTask(row) error:" + e);
		}
	}

	public DBRow[] getOrderTasksByTargetAdmin(long adid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			para.add("adid",adid);
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("order_task") + " where t_target_admin=? order by ot_id desc",para,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("order_task") + "  where t_target_admin=? order by  ot_id desc",para);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getOrderTasksByTargetAdmin() error:" + e);
		}
	}
	
	public DBRow[] getOrderTasksByTargetGroup(long adgid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			para.add("adgid",adgid);
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("order_task") + " where t_target_group=? order by ot_id desc",para,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("order_task") + "  where t_target_group=? order by  ot_id desc",para);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getOrderTasksByTargetGroup() error:" + e);
		}
	}

	public DBRow[] getOrderTasks(PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("order_task") + " order by ot_id desc",pc);
			}
			else
			{
				row = dbUtilAutoTran.selectMutliple("select * from " + ConfigBean.getStringValue("order_task") + " order by  ot_id desc");
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getOrderTasksByTargetGroup() error:" + e);
		}
	}
	
	public DBRow[] getOrderTasksByPsId(long ps_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			
			DBRow para = new DBRow();
			para.add("ps_id", ps_id);
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("order_task") + " where ps_id=? order by ot_id desc",para,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("order_task") + "  where ps_id=?  order by  ot_id desc",para);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getOrderTasksByTargetGroup() error:" + e);
		}
	}
	
	public DBRow[] getOrderTasksByOid(long oid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			para.add("oid", oid);
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("order_task") + " where oid=? order by ot_id desc",para,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectPreMutliple("select * from " + ConfigBean.getStringValue("order_task") + " where oid=? order by  ot_id desc",para);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getOrderTasksByOid() error:" + e);
		}
	}
		
	public DBRow getDetailOrderTaskByOtid(long ot_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("ot_id",ot_id);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("order_task") + " where ot_id=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailOrderTaskByOtid(row) error:" + e);
		}
	}
		
	public void modOrderTaskByOtid(long ot_id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where ot_id=" + ot_id,ConfigBean.getStringValue("order_task"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modOrderTaskByOtid(row) error:" + e);
		}
	}

	public DBRow getTasksCountByOidStatus(long oid,int status)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("oid",oid);
			para.add("status",status);
			
			DBRow row = dbUtilAutoTran.selectPreSingle("select count(ot_id) c from " + ConfigBean.getStringValue("order_task") + "  where oid=? and t_status=? ",para);
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getTasksCountByOidStatus() error:" + e);
		}
	}
		
	public int getOrderTasksCount()
		throws Exception
	{
		try
		{
			DBRow row = dbUtilAutoTran.selectSingle("select  count(ot_id) c  from " + ConfigBean.getStringValue("order_task") + " where t_status=1");
			
			return(row.get("c", 0));
		}
		catch (Exception e)
		{
			throw new Exception("getOrderTasksCount() error:" + e);
		}
	}
	
	public DBRow[] getTodayUnDoOrderTasksByOid(long oid,long ps_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			
			para.add("oid", oid);
			para.add("ps_id", ps_id);
			
			String sql = "select * from " + ConfigBean.getStringValue("order_task") + " where oid=? and ps_id=? and t_status=1 and datediff(current_date,create_date)=0 order by ot_id desc";
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getTodayUnDoOrderTasksByOid() error:" + e);
		}
	}

	public void delOrderTaskByOid(long oid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where ot_id=" + oid,ConfigBean.getStringValue("order_task"));
		}
		catch (Exception e)
		{
			throw new Exception("delOrderTaskByOid() error:" + e);
		}
	}		
	
	
	
	
	
	

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
