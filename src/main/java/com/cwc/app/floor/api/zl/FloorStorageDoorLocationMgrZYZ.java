package com.cwc.app.floor.api.zl;


import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorStorageDoorLocationMgrZYZ {
	
	private DBUtilAutoTran dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	/**
	 * 获取所有卸货门
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllStorageDoor(PageCtrl pc)throws Exception{
		
		try {
			String sql="select sd.*,psc.title from "+ConfigBean.getStringValue("storage_door")+" as sd" +
			" join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on sd.ps_id = psc.id order by sd.sd_id desc";
			if(pc != null)
			   return dbUtilAutoTran.selectMutliple(sql, pc);
			else
				return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorContainerMgrZYZ getAllStorageDoor error:" +e);
		}
	}
	/**
	 * 卸货门高级查询
	 * @param doorId
	 * @param psId
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchStorageDoor(String doorId,long psId,PageCtrl pc)throws Exception{
		
			try {
				String sql="select sd.*,psc.title from "+ConfigBean.getStringValue("storage_door")+" as sd" +
				" join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on sd.ps_id = psc.id where 0=0";
				if(doorId != null){
					sql += " and doorId like '"+doorId+"%'";
				}
				if(psId != 0){
					sql += " and ps_id ="+psId;
				}
				sql += " order by sd_id desc";
				return dbUtilAutoTran.selectMutliple(sql, pc);
				
			} catch (Exception e) {
				throw new Exception("FloorContainerMgrZYZ getSearchStorageDoor error:" +e);
			}
			
	}
	/**
	 * 获取某个装卸门信息
	 * @param sdId
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailStorageDoor(long sdId)throws Exception{
		
		try {
			String sql = "select * from "+ConfigBean.getStringValue("storage_door")+" where sd_id="+sdId;
			return dbUtilAutoTran.selectSingle(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorContainerMgrZYZ getDetailStorageDoor error:" +e);
		}
	}
	/**
	 * 增加卸货门
	 * @param drow
	 * @return
	 * @throws Exception
	 */
	public long addStorageDoor(DBRow drow)throws Exception{
		
			try
			{
				return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("storage_door"), drow);
			}
			catch (Exception e) 
			{
				throw new Exception("FloorContainerMgrZYZ addStorageDoor error:" +e);
			}
	}
	/**
	 * 修改装卸门
	 * @param sdId
	 * @param row
	 * @throws Exception
	 */
	public void modStorageDoor(long sdId, DBRow row)throws Exception{
		
			try {
				dbUtilAutoTran.update("where sd_id="+sdId, ConfigBean.getStringValue("storage_door"), row);
				
			} catch (Exception e) {
				throw new Exception("FloorContainerMgrZYZ modStorageDoor error:" +e);
			}
	}
	/**
	 * 删除装卸门
	 * @param sdId
	 * @throws Exception
	 */
	public void deleteStorageDoor(long sdId)throws Exception{
		
			try {
				dbUtilAutoTran.delete("where sd_id="+sdId, ConfigBean.getStringValue("storage_door"));
				
			} catch (Exception e) {
				throw new Exception("FloorContainerMgrZYZ deleteStorageDoor error:" +e);
			}
	}
	/**
	 * 获取所有装卸位置
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllLoadUnloadLocation(PageCtrl pc)throws Exception{
		
			try {
				String sql = "select * from "+ConfigBean.getStringValue("storage_load_unload_location")+" as sll"+
				" join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on sll.psc_id = psc.id order by sll.id desc";
				if(pc != null)
					return dbUtilAutoTran.selectMutliple(sql, pc);
				else 
					return dbUtilAutoTran.selectMutliple(sql);
				
			} catch (Exception e) {
				throw new Exception("FloorContainerMgrZYZ getAllLoadUnloadLocation error:" +e);
			}
	}
	/**
	 * 装卸位置高级查询
	 * @param locationName
	 * @param pscId
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchLoadUnloadLocation(String locationName,long pscId,PageCtrl pc)throws Exception{
			
			try {
				String sql = "select * from "+ConfigBean.getStringValue("storage_load_unload_location")+" as sll"+
				" join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on sll.psc_id = psc.id where 0=0";
				if(locationName != null){
					sql += " and sll.location_name like '"+locationName+"%'";
				}
				if(pscId != 0){
					sql += " and sll.psc_id ="+pscId;
				}
				sql += " order by sll.id desc";
				return dbUtilAutoTran.selectMutliple(sql, pc);
				
			} catch (Exception e) {
				throw new Exception("FloorContainerMgrZYZ getSearchLoadUnloadLocation error:" +e);
			}
			
	}
	/**
	 * 获取某个装卸位置
	 * @param locationId
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailLoadUnloadLocation(long locationId)throws Exception{
			
			try {
				String sql = "select * from "+ConfigBean.getStringValue("storage_load_unload_location")+" where id ="+locationId;
				return dbUtilAutoTran.selectSingle(sql);
				
			} catch (Exception e) {
				throw new Exception("FloorContainerMgrZYZ getDetailLoadUnloadLocation error:" +e);
			}
	}
	/**
	 * 增加装卸位置
	 * @param rows
	 * @return
	 * @throws Exception
	 */
	public long addLoadUnloadLocation(DBRow rows)throws Exception{
		
		try 
		{
			
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("storage_load_unload_location"), rows);
			
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorContainerMgrZYZ addLoadUnloadLocation error:" +e);
		}
	}
	/**
	 * 修改装卸位置
	 * @param locationId
	 * @param drow
	 * @throws Exception
	 */
	public void modLoadUnloadLocation(long locationId,DBRow drow)throws Exception{
		
			try {
				dbUtilAutoTran.update(" where id ="+locationId, ConfigBean.getStringValue("storage_load_unload_location"), drow);
			
			} catch (Exception e) {
				throw new Exception("FloorContainerMgrZYZ modLoadUnloadLocation error:" +e);
			}
	}
	/**
	 * 删除装卸位置
	 * @param locationId
	 * @throws Exception
	 */
	public void deleteLoadUnloadLocation(long locationId)throws Exception{
		
		try {
			dbUtilAutoTran.delete("where id="+locationId, ConfigBean.getStringValue("storage_load_unload_location"));
			
		} catch (Exception e) {
			throw new Exception("FloorContainerMgrZYZ modLoadUnloadLocation error:" +e);
		}
	}
	/**
	 * 根据仓库ID获取装卸门信息
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageDoorByPsid(long psId)throws Exception{
		 
		try {
			String sql="select * from "+ConfigBean.getStringValue("storage_door")+" where ps_id="+psId;
			return dbUtilAutoTran.selectMutliple(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorContainerMgrZYZ getStorageDoorByPsid error:" +e);
		}
	}
	/**
	 * 根据仓库ID获取装卸位置信息
	 * @param psId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLoadUnloadLocationByPsid(long psId)throws Exception{
			
			try {
				String sql="select * from "+ConfigBean.getStringValue("storage_load_unload_location")+" where psc_id="+psId;
				return dbUtilAutoTran.selectMutliple(sql);
				
			} catch (Exception e) {
				throw new Exception("FloorContainerMgrZYZ getLoadUnloadLocationByPsid error:" +e);
			}
	}
}
