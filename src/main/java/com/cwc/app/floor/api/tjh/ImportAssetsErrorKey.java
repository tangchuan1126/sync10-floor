package com.cwc.app.floor.api.tjh;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ImportAssetsErrorKey {
	
	DBRow row;
	public static int A_NAME = 1;
	public static int A_CATEGORY = 2;
	public static int CATEGORYTYPEERROR = 3;
	public static int A_CODE = 4;
	public static int A_PRICE = 5;
	public static int A_PRICENERROR = 6;
	public static int PRICENULL = 7;
	public static int CATEGORYNULL = 8;
	
	public ImportAssetsErrorKey()
	{
		row = new DBRow();
		row.add(String.valueOf(ImportAssetsErrorKey.A_NAME), "<font color=red>此资产名称不能为空</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.A_CATEGORY), "<font color=red>该资产类别不存在，请您核实资产类别表再导入</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.CATEGORYTYPEERROR),"<font color=red>资产类别ID格式不正确，只可填写数字</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.A_CODE), "<font color=red>资产的条码不能为空</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.A_PRICENERROR),"<font color=red>资产的单价格式不正确</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.A_PRICE),"<font color=red>资产的单价不大于0</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.PRICENULL), "<font color=red>资产的单价不能为空</font>");
		row.add(String.valueOf(ImportAssetsErrorKey.CATEGORYTYPEERROR),"<font color=red>资产类别不能为空</font>");
	}
	
	public ArrayList getErrorMessage()
	{
		return(row.getFieldNames());
	}

	public String getErrorMessageById(String id)
	{
		return(row.getString(id));
	}
}
