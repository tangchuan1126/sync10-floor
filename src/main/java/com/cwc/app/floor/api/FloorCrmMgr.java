package com.cwc.app.floor.api;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorCrmMgr
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	public long addClients(DBRow row)
		throws Exception
	{
		try
		{
			long cc_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("crm_clients"));
			row.add("cc_id",cc_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("crm_clients"),row);
			return(cc_id);
		}
		catch (Exception e)
		{
			throw new Exception("addClients(row) error:" + e);
		}
	}
		
	public DBRow[] getNewOrderClients(PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select client_id from " + ConfigBean.getStringValue("porder") + " where handle=4 and client_id!='' group by client_id";
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getNewOrderClients(row) error:" + e);
		}
	}
	
	public DBRow[] getAllCrmClients(PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("crm_clients") + " order by cc_id desc";
			return (dbUtilAutoTran.selectMutliple(sql,pc));
//			return(dbUtilAutoTran.selectMutlipleCache(sql,pc,new String[]{ConfigBean.getStringValue("porder")}));
		}
		catch (Exception e)
		{
			throw new Exception("getAllCrmClients(row) error:" + e);
		}
	}
		
	public DBRow[] getFrequentlyReadyClients(long adid,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select cp.*,ctc.c_country,cc.*,COUNT(oid) c from " + ConfigBean.getStringValue("country_province") + " cp," + ConfigBean.getStringValue("country_code") + " ctc," + ConfigBean.getStringValue("porder") + " o," + ConfigBean.getStringValue("crm_clients") + " cc LEFT JOIN "+ConfigBean.getStringValue("crm_special_clients")+" csc ON csc.cc_id=cc.cc_id where cp.pro_id=cc.pro_id and ctc.ccid=cc.ccid and csc_id IS NULL AND cc.email=o.client_id and cc.adid=? and post_date>=DATE_SUB(DATE_FORMAT(NOW(),\"%Y-%c-%e\"), INTERVAL 3 MONTH) and (trace_count=0 or trace_count is null) GROUP BY o.client_id HAVING c>5 ORDER BY c DESC";
			DBRow para = new DBRow();
			para.add("adid", adid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getFrequentlyReadyClients(row) error:" + e);
		}
	}
	
	public DBRow[] getFrequentlyMustTraceClients(long adid,int period,int target_type,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select cp.*,ctc.c_country,cc.*,COUNT(oid) c from " + ConfigBean.getStringValue("country_province") + " cp," + ConfigBean.getStringValue("country_code") + " ctc," + ConfigBean.getStringValue("porder") + " o," + ConfigBean.getStringValue("crm_clients") + " cc LEFT JOIN "+ConfigBean.getStringValue("crm_special_clients")+" csc ON csc.cc_id=cc.cc_id where cp.pro_id=cc.pro_id and ctc.ccid=cc.ccid and  csc_id IS NULL AND cc.email=o.client_id and cc.adid=? and NOW()>=DATE_ADD(DATE_FORMAT(last_trace_date,\"%Y-%c-%e\"), INTERVAL ? DAY ) and cc.target_type=?  GROUP BY o.client_id  ORDER BY c DESC";
			DBRow para = new DBRow();
			para.add("adid", adid);
			para.add("period", period);
			para.add("target_type", target_type);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getFrequentlyMustTraceClients(row) error:" + e);
		}
	}
			
	public DBRow[] getTraceByCcid(long ccid,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("crm_trace_clients") + " where cc_id=? order by ctc_id desc";
			DBRow para = new DBRow();
			para.add("ccid", ccid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("getAllCrmClients(row) error:" + e);
		}
	}	
	
	public long addTrace(DBRow row)
		throws Exception
	{
		try
		{
			long ctc_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("crm_trace_clients"));
			row.add("ctc_id",ctc_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("crm_trace_clients"),row);
			return(ctc_id);
		}
		catch (Exception e)
		{
			throw new Exception("addTrace(row) error:" + e);
		}
	}
		
	public DBRow getDetailClientByCcid(long ccid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("ccid",ccid);
			
			DBRow row = dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("crm_clients") + " where cc_id=?",para);
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getDetailClientByCcid(row) error:" + e);
		}
	}
	
	public void modClientByCcid(long ccid,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where cc_id=" + ccid,ConfigBean.getStringValue("crm_clients"),row);
		}
		catch (Exception e)
		{
			throw new Exception("modClientByCcid(row) error:" + e);
		}
	}
		
	public long addSpecialClients(DBRow row)
		throws Exception
	{
		try
		{
			long csc_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("crm_special_clients"));
			row.add("csc_id",csc_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("crm_special_clients"),row);
			return(csc_id);
		}
		catch (Exception e)
		{
			throw new Exception("addSpecialClients(row) error:" + e);
		}
	}
			
	public DBRow[] getSpecialClients(long adid,int period,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("crm_clients")+" cc,"+ConfigBean.getStringValue("crm_special_clients")+" csc WHERE cc.cc_id=csc.cc_id and date(NOW())>=DATE_ADD(date(last_trace_date), INTERVAL ? DAY ) and adid=?";
			DBRow para = new DBRow();
			para.add("period", period);
			para.add("adid", adid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("getSpecialClients(row) error:" + e);
		}
	}
		
	public void delSpecialClientByCscId(long csc_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where csc_id=" + csc_id,ConfigBean.getStringValue("crm_special_clients"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.delSpecialClientByCscId(row) error:" + e);
		}
	}
	
	public void delClientByCcId(long cc_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where cc_id=" + cc_id,ConfigBean.getStringValue("crm_clients"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.delClientByCcId(row) error:" + e);
		}
	}
			
			
		
		
		
	

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
