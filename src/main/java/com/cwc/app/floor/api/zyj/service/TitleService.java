package com.cwc.app.floor.api.zyj.service;

import java.util.List;
import com.cwc.app.floor.api.zyj.model.Title;



/**
 * 定义针对生产商的数据访问接口
 * @author lujintao
 *
 */
public interface TitleService {
	/**
	 * 获得所有的生产商
	 * @return 如果有，返回所有生产商;如果没有，返回空集合
	 */
	public List<Title> getAll();
	
	/**
	 * 获取与特定品牌商相关的所有Title
	 * @param customerId 品牌商ID
	 * @return
	 */
	public List<Title>  getTitlesByCustomerId(int customerId);
	
	/**
	 * 获取与特定产品相关的所有Title
	 * @param productId 产品ID
	 * @return
	 */
	public List<Title>  getTitles(int productId);
	
	/**
	 * 获取与特定产品相关的所有Title
	 * @param productId 产品ID
	 * @return
	 */
	public List<Title>  getTitles(int productId,int customerId);
	
	/**
	 * 获取与指定产品和CLP Type 的所有Title
	 * @param productId
	 * @param clpTypeId
	 * @return
	 */
	public List<Title> getTitlesByProdAndCLP(int productId,int clpTypeId);
	
	/**
	 * 获取与指定产品和CLP Type以及 customer 相关 的所有Title
	 * @param productId
	 * @param clpTypeId
	 * @param customerId  
	 * @return
	 */
	public List<Title> getTitlesByProdAndCLP(int productId,int clpTypeId,int customerId);
	
	
	
	
}
