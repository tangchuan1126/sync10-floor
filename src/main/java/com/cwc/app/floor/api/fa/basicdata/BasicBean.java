package com.cwc.app.floor.api.fa.basicdata;

import com.cwc.db.DBRow;

/**  
 * 
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.beans.BasicBean.java] 
 * @ClassName:    [BasicBean]  
 * @Description:  [为所有bean提供toDBRow]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年3月24日 上午10:40:42]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年3月24日 上午10:40:42]  
 * @UpdateRemark: [创建] 
 * @Version:      [v1.0]
 *   
 */
public interface BasicBean {
	/**
	 * bean  转换成 DBRow 的接口
	 * @param flag  flag 为true是，表示把null转换成空格添加到DBRow ，
	 * 否则不添加值为null的属性字段到DBRow
	 * 其中如果是数据库的主键字段且为自动增长类型，那么该字段属性值如果是0责不添加
	 * @return
	 */
	public DBRow toDBRow(boolean flag);
}
