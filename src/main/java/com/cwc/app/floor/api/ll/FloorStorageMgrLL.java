package com.cwc.app.floor.api.ll;

import com.cwc.app.beans.ll.ProductStorageBeanLL;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

public class FloorStorageMgrLL {
	private ProductStorageBeanLL productStorageBeanLL;
	
	public DBRow[] getCostLog(long ps_id, String p_name, PageCtrl pc) throws Exception {
		String sql = "select c.title,b.p_name, a.store_count,a.storage_unit_freight, a.storage_unit_price, d.employe_name, a.post_date from product_store_edit_log a"; 
				sql += " INNER JOIN product b on a.pc_id = b.pc_id";
				sql += " INNER JOIN product_storage_catalog c on a.cid = c.id";
				sql += " INNER JOIN admin d on a.edit_adid = d.adid";
				sql += " where 1=1";
				
				if(ps_id != 0) {
					sql += " and a.cid="+ps_id;
				}
				
				if(!p_name.equals("")) {
					sql += " and b.p_name like '%"+p_name+"%'";
				}
		return productStorageBeanLL.getDbUtilAutoTran().selectMutliple(sql, pc);
	}
	
	public ProductStorageBeanLL getProductStorageBeanLL() {
		return productStorageBeanLL;
	}

	public void setProductStorageBeanLL(ProductStorageBeanLL productStorageBeanLL) {
		this.productStorageBeanLL = productStorageBeanLL;
	}
	
}
