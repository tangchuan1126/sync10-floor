package com.cwc.app.floor.api.zj;

import com.cwc.app.key.CheckInLiveLoadOrDropOffKey;
import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.EquipmentTypeKey;
import com.cwc.app.key.ModuleKey;
import com.cwc.app.key.OccupyStatusTypeKey;
import com.cwc.app.key.OccupyTypeKey;
import com.cwc.app.key.SpaceRelationTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorSpaceResourcesRelationMgr {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加空间资源关联关系
	 * @author zhanjie
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addSpaceResourcesRelation(DBRow row)
		throws Exception
	{
		return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("space_resources_relation"),row);
	}
	
	/**
	 * 基于ID删除空间资源关联关系
	 * @author zhanjie
	 * @param srr_id
	 * @throws Exception
	 */
	public void delSpaceResourcesRelation(long srr_id)
		throws Exception
	{
		dbUtilAutoTran.delete("where srr_id = "+srr_id,ConfigBean.getStringValue("space_resources_relation"));
	}
	
	/**
	 * @author zhanjie
	 * @param relation_type
	 * @param relation_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getSpaceResorceRelation(int relation_type,long relation_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("space_resources_relation")+" where relation_type = ? and relation_id = ? ";
		
		DBRow para = new DBRow();
		para.add("relation_type",relation_type);
		para.add("relation_id",relation_id);
		
		return dbUtilAutoTran.selectPreSingle(sql, para);
	}
	
	/**
	 * 基于关联删除空间关系
	 * @param relation_type
	 * @param relation_id
	 * @throws Exception
	 */
	public void delSpaceResourcesRelation(int relation_type,long relation_id)
		throws Exception
	{
		dbUtilAutoTran.delete("where relation_type = "+relation_type+" and relation_id = "+relation_id,ConfigBean.getStringValue("space_resources_relation"));
	}
	
	
	/**
	 * 根据关联id 关联类型  删除空间资源关联关系
	 * @param relation_id
	 * @throws Exception
	 */
	public void delSpceResourcesByRelation_id(long module_id,int module_type,int relation_type)
		throws Exception
	{
		this.dbUtilAutoTran.delete("where module_id="+module_id+" and module_type="+module_type+" and relation_type="+relation_type, ConfigBean.getStringValue("space_resources_relation"));
	}
	
	/**
	 * 删除空间资源关系
	 * @param resources_type
	 * @param resources_id
	 * @param relation_type
	 * @param relation_id
	 * @param module_type
	 * @param module_id
	 * @throws Exception
	 */
	public void delSpaceResourcesRelation(int resources_type,long resources_id,int relation_type,long relation_id,int module_type,long module_id)
		throws Exception
	{
		DBRow para = new DBRow();
		para.add("resources_type",resources_type);
		para.add("resources_id",resources_id);
		para.add("relation_type",relation_type);
		para.add("relation_id",relation_id);
		para.add("module_type",module_type);
		para.add("module_id",module_id);
		
		dbUtilAutoTran.deletePre("where resources_type = ? and resources_id = ? and relation_type = ? and relation_id = ? and module_type = ? and module_id = ?",para,ConfigBean.getStringValue("space_resources_relation"));
	}
	
		/**
	 * 根据yc_no 等条件获得可用车位(自己已使用的和实际可用的)
	 * @author geqingling
	 * @param entry_id
	 * @param area_id
	 * @param yc_no
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCanUseSpotByYcno(long entry_id,long ps_id,long area_id,String yc_no)
		throws Exception
	{
		String whereWareHouse = "";
		if (ps_id !=0)
		{
			whereWareHouse = " and syc.ps_id ="+ps_id+" ";
		}
		String whereArea = "";
		if (area_id !=0)
		{
			whereArea = " and syc.area_id = "+area_id+" ";
		}	
		String whereYc = "";
		if(!"".equals(yc_no)){
			whereYc = " and t.yc_no like '%"+yc_no+"%'";
		}
		String subSql = "select syc.* from storage_yard_control as syc "
					+"join space_resources_relation as srr on syc.yc_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.SPOT+" "
					+"where  srr.module_type = "+ModuleKey.CHECK_IN+" and srr.module_id = "+entry_id+" "
					+"union " 
					+"select syc.* from storage_yard_control as syc " 
					+"left join space_resources_relation as srr on syc.yc_id = resources_id and srr.resources_type = "+OccupyTypeKey.SPOT+" "
					+"where  srr.srr_id is null "+whereWareHouse+whereArea+" "
					+"order by yc_no ";
		String sql = "select t.* "
			+"from ("+subSql+") t "
			+" where 1=1 "+whereYc; 
		//System.out.println(sql);
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 根据yc_no 等条件获得可用车位(自己已使用的和实际可用的)
	 * @author zwb
	 * @param entry_id
	 * @param area_id
	 * @param yc_no
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCanUseSpotByYcnoAccurate(long entry_id,long ps_id,long area_id,String yc_no)
		throws Exception
	{
		String whereWareHouse = "";
		if (ps_id !=0)
		{
			whereWareHouse = " and syc.ps_id ="+ps_id+" ";
		}
		String whereArea = "";
		if (area_id !=0)
		{
			whereArea = " and syc.area_id = "+area_id+" ";
		}	
		String whereYc = "";
		if(!"".equals(yc_no)){
			whereYc = " and t.yc_no = "+yc_no+" ";
		}
		String subSql = "select syc.* from storage_yard_control as syc "
					+"join space_resources_relation as srr on syc.yc_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.SPOT+" "
					+"where  srr.module_type = "+ModuleKey.CHECK_IN+" and srr.module_id = "+entry_id+" "+whereArea+" "
					+"union " 
					+"select syc.* from storage_yard_control as syc " 
					+"left join space_resources_relation as srr on syc.yc_id = resources_id and srr.resources_type = "+OccupyTypeKey.SPOT+" "
					+"where  srr.srr_id is null "+whereWareHouse+whereArea;
		
		String sql = "select t.* "
			+"from ("+subSql+") t "
			+" where 1=1 "+whereYc; 
		//System.out.println(sql);
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 返回车位
	 * @zhanjie
	 * @param ps_id
	 * @param area_id
	 * @param spot_status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] returnSpot(long ps_id,long area_id,int spot_status,PageCtrl pc)
		throws Exception
	{
		String whereWarehouse = "";
		String whereArea = "";
		String whereSpotStatus = "";
		if (ps_id !=0)
		{
			whereWarehouse = " and syc.ps_id = "+ps_id+" ";
		}
		
		if (area_id != 0)
		{
			whereArea = " and syc.area_id = "+area_id+" ";
		}
		
		if (spot_status != 0)
		{
			whereSpotStatus = " and syc.occupy_status = "+spot_status+" ";
		}
		
		String sql = "select syc.yc_no,syc.yc_id as resource_id,srr.occupy_status,syc.area_id ,sla.area_name from "+ConfigBean.getStringValue("storage_yard_control")+" as syc "
					+"left join "+ConfigBean.getStringValue("space_resources_relation")+" as srr on  srr.resources_type = "+OccupyTypeKey.SPOT+" and syc.yc_id = srr.resources_id "
					+"left join "+ConfigBean.getStringValue("storage_location_area")+" as sla on  sla.area_id = syc.area_id "
					+"where  syc.patrol_time is null  "+whereWarehouse+whereArea+whereSpotStatus
					+"group by syc.yc_id ORDER BY CAST(syc.yc_no AS SIGNED)";
		
		return dbUtilAutoTran.selectMutliple(sql, pc);
	}
	
	
	public DBRow[] returnDoor(long ps_id,long area_id,int door_status,PageCtrl pc)
			throws Exception
	{
		String whereWarehouse = "";
		String whereArea = "";
		String whereDoorStatus = "";
		if (ps_id !=0)
		{
			whereWarehouse = " and sd.ps_id = "+ps_id+" ";
		}
		
		if (area_id != 0)
		{
			whereArea = " and sd.area_id = "+area_id+" ";
		}
			
		if (door_status != 0)
		{
			whereDoorStatus = " and syc.occupy_status = "+door_status+" ";
		}	
			
		String sql = "select sd.doorId as door_name,sd.sd_id as resource_id,srr.occupy_status,sd.area_id,sla.area_name  from "+ConfigBean.getStringValue("storage_door")+" as sd "
					+"left join "+ConfigBean.getStringValue("space_resources_relation")+" as srr on  srr.resources_type = "+OccupyTypeKey.DOOR+" and sd.sd_id = srr.resources_id "
					+"left join "+ConfigBean.getStringValue("storage_location_area")+" as sla on  sla.area_id = sd.area_id "
					+"where sd.patrol_time is null  "+whereWarehouse+whereArea+whereDoorStatus
					+"group by sd.sd_id ORDER BY CAST(sd.doorId AS SIGNED)";
			
		return dbUtilAutoTran.selectMutliple(sql, pc);
	}			
	

	public DBRow[] findResourceByEquipment (long ps_id, String license_plate, String trailerNo)
		throws Exception
	{
		String sql = "SELECT CONCAT('DOOR',sd.doorId) as location,srr.resources_type,srr.resources_id,ee.check_in_entry_id,ee.equipment_type,ee.equipment_id,ee.equipment_number FROM "+ConfigBean.getStringValue("entry_equipment")+" AS ee "
					+"join "+ConfigBean.getStringValue("space_resources_relation")+" AS srr ON ee.equipment_id = srr.relation_id AND srr.relation_type = "+SpaceRelationTypeKey.Equipment+" "
					+"join "+ConfigBean.getStringValue("storage_door")+" as sd on srr.resources_type = "+OccupyTypeKey.DOOR+" and srr.resources_id = sd.sd_id "
					+"where ee.equipment_type = "+EquipmentTypeKey.Container+" and ee.equipment_number = '"+trailerNo+"' "
					+"union "
					+"select CONCAT('DOOR',sd.doorId) as location,srr.resources_type,srr.resources_id,ee.check_in_entry_id,ee.equipment_type,ee.equipment_id ,ee.equipment_number from "+ConfigBean.getStringValue("entry_equipment")+" AS ee "
					+"JOIN "+ConfigBean.getStringValue("space_resources_relation")+" AS srr ON ee.equipment_id = srr.relation_id AND srr.relation_type ="+SpaceRelationTypeKey.Equipment+" "
					+"join "+ConfigBean.getStringValue("storage_door")+" as sd on srr.resources_type = "+OccupyTypeKey.DOOR+" and srr.resources_id = sd.sd_id "
					+"where ee.equipment_type = "+EquipmentTypeKey.Tractor+" and ee.equipment_number = '"+license_plate+"' "
					+"union "
					+"SELECT CONCAT('SPOT',syc.yc_no) as location,srr.resources_type,srr.resources_id,ee.check_in_entry_id,ee.equipment_type,ee.equipment_id, ee.equipment_number FROM "+ConfigBean.getStringValue("entry_equipment")+" AS ee "
					+"join "+ConfigBean.getStringValue("space_resources_relation")+" AS srr ON ee.equipment_id = srr.relation_id AND srr.relation_type = "+SpaceRelationTypeKey.Equipment+" "
					+"join "+ConfigBean.getStringValue("storage_yard_control")+" as syc on srr.resources_type = "+OccupyTypeKey.SPOT+" and srr.resources_id = syc.yc_id "
					+"where ee.equipment_type = "+EquipmentTypeKey.Container+" and ee.equipment_number = '"+trailerNo+"' "
					+"union " 
					+"select CONCAT('SPOT',syc.yc_no) as location,srr.resources_type,srr.resources_id,ee.check_in_entry_id,ee.equipment_type,ee.equipment_id, ee.equipment_number from "+ConfigBean.getStringValue("entry_equipment")+" AS ee "
					+"JOIN "+ConfigBean.getStringValue("space_resources_relation")+" AS srr ON ee.equipment_id = srr.relation_id AND srr.relation_type = "+SpaceRelationTypeKey.Equipment+" "
					+"join "+ConfigBean.getStringValue("storage_yard_control")+" as syc on srr.resources_type = "+OccupyTypeKey.SPOT+" and srr.resources_id = syc.yc_id "
					+"where ee.equipment_type = "+EquipmentTypeKey.Tractor+" and ee.equipment_number = '"+license_plate+"' "
					+"order by equipment_id desc";
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 查询设备，带出占用的资源信息
	 * @author zhanjie
	 * @param ps_id
	 * @param license_plate
	 * @param trailerNo
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findEquipmentWithResource (long ps_id, String license_plate, String trailerNo)
			throws Exception
		{
			String sql = "SELECT CONCAT('DOOR',sd.doorId) as location,srr.resources_type,srr.resources_id,ee.check_in_entry_id,ee.equipment_type,ee.equipment_id,ee.equipment_number FROM "+ConfigBean.getStringValue("entry_equipment")+" AS ee "
						+"left join "+ConfigBean.getStringValue("space_resources_relation")+" AS srr ON ee.equipment_id = srr.relation_id AND srr.relation_type = "+SpaceRelationTypeKey.Equipment+" "
						+"left join "+ConfigBean.getStringValue("storage_door")+" as sd on srr.resources_type = "+OccupyTypeKey.DOOR+" and srr.resources_id = sd.sd_id "
						+"where ee.equipment_type = "+EquipmentTypeKey.Container+" and ee.equipment_number = '"+trailerNo+"' "
						+"union "
						+"select CONCAT('DOOR',sd.doorId) as location,srr.resources_type,srr.resources_id,ee.check_in_entry_id,ee.equipment_type,ee.equipment_id ,ee.equipment_number from "+ConfigBean.getStringValue("entry_equipment")+" AS ee "
						+"left JOIN "+ConfigBean.getStringValue("space_resources_relation")+" AS srr ON ee.equipment_id = srr.relation_id AND srr.relation_type ="+SpaceRelationTypeKey.Equipment+" "
						+"left join "+ConfigBean.getStringValue("storage_door")+" as sd on srr.resources_type = "+OccupyTypeKey.DOOR+" and srr.resources_id = sd.sd_id "
						+"where ee.equipment_type = "+EquipmentTypeKey.Tractor+" and ee.equipment_number = '"+license_plate+"' "
						+"union "
						+"SELECT CONCAT('SPOT',syc.yc_no) as location,srr.resources_type,srr.resources_id,ee.check_in_entry_id,ee.equipment_type,ee.equipment_id, ee.equipment_number FROM "+ConfigBean.getStringValue("entry_equipment")+" AS ee "
						+"left join "+ConfigBean.getStringValue("space_resources_relation")+" AS srr ON ee.equipment_id = srr.relation_id AND srr.relation_type = "+SpaceRelationTypeKey.Equipment+" "
						+"left join "+ConfigBean.getStringValue("storage_yard_control")+" as syc on srr.resources_type = "+OccupyTypeKey.SPOT+" and srr.resources_id = syc.yc_id "
						+"where ee.equipment_type = "+EquipmentTypeKey.Container+" and ee.equipment_number = '"+trailerNo+"' "
						+"union " 
						+"select CONCAT('SPOT',syc.yc_no) as location,srr.resources_type,srr.resources_id,ee.check_in_entry_id,ee.equipment_type,ee.equipment_id, ee.equipment_number from "+ConfigBean.getStringValue("entry_equipment")+" AS ee "
						+"left JOIN "+ConfigBean.getStringValue("space_resources_relation")+" AS srr ON ee.equipment_id = srr.relation_id AND srr.relation_type = "+SpaceRelationTypeKey.Equipment+" "
						+"left join "+ConfigBean.getStringValue("storage_yard_control")+" as syc on srr.resources_type = "+OccupyTypeKey.SPOT+" and srr.resources_id = syc.yc_id "
						+"where ee.equipment_type = "+EquipmentTypeKey.Tractor+" and ee.equipment_number = '"+license_plate+"' "
						+"order by equipment_id desc";
			
			//System.out.println(sql);
			return dbUtilAutoTran.selectMutliple(sql);
		}
	
	/**
	 * 获得任务,及其关联的资源
	 * @author zhanjie
	 * @param entry_id
	 * @param occpuy_status
	 * @param equipment_id 返回当前设备下面的的任务
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTaskByEntryIdWithOccpuyStatus(long entry_id,int occpuy_status ,long equipment_id)
		throws Exception
	{
		String whereOccpuyStatus = "";
		DBRow para = new DBRow();
		para.add("entry_id",entry_id);
		
		if (occpuy_status !=0)
		{
			whereOccpuyStatus = " and srr.occupy_status = ? ";
			para.add("occpuy_status",occpuy_status);
		}
		if(equipment_id != 0l){ //append by zhangrui
			whereOccpuyStatus += " and equipment_id = ? ";
			para.add("equipment_id", equipment_id);
		}
		
		String sql = "select * from door_or_location_occupancy_details as dlod "
					+"left join space_resources_relation as srr on srr.relation_type = "+SpaceRelationTypeKey.Task+" and srr.relation_id = dlod.dlo_detail_id "
					+"where dlod.dlo_id = ? "+whereOccpuyStatus;
 		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	/**
	 * 获得任务,及其关联的资源
	 * @author gql
	 * @param entry_id
	 * @param detail_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTaskByEntryIdWithDetailId(long entry_id,long detail_id)
			throws Exception
	{
		String whereOccpuyStatus = "";
		DBRow para = new DBRow();
		para.add("entry_id",entry_id);
		
		if (detail_id !=0)
		{
			whereOccpuyStatus = " and dlod.dlo_detail_id = ? ";
			para.add("detail_id",detail_id);
		}
		
		String sql = "select * from door_or_location_occupancy_details as dlod "
				+"left join space_resources_relation as srr on srr.relation_type = "+SpaceRelationTypeKey.Task+" and srr.relation_id = dlod.dlo_detail_id "
				+"left join "+ConfigBean.getStringValue("storage_yard_control")+" as syc on srr.resources_type = "+OccupyTypeKey.SPOT+" and srr.resources_id = syc.yc_id "
				+"left join "+ConfigBean.getStringValue("storage_door")+" as sd on srr.resources_type = "+OccupyTypeKey.DOOR+" and srr.resources_id = sd.sd_id "
				+"where dlod.dlo_id = ? "+whereOccpuyStatus;
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	/**
	 * 获得load,及其关联的资源，按时间段创建pdf时使用
	 * @author gql
	 * @param startTime
	 * @param endTime
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTaskByTime(String startTime, String endTime,Long ps_id)
			throws Exception
	{
		StringBuffer sql = new StringBuffer();
        sql.append("select distinct dlod.dlo_id as entry_id,dlod.dlo_detail_id as detail_id, dlod.number, dlod.number_type, dlod.company_id,dlod.equipment_id,dlod.customer_id,dlod.order_no,srr.resources_type,syc.yc_no,sd.doorId ").append(" from door_or_location_occupancy_details as dlod LEFT JOIN door_or_location_occupancy_main main ON dlod.dlo_id=main.dlo_id ").append((new StringBuilder()).append(" left join space_resources_relation as srr on srr.relation_type = ").append(SpaceRelationTypeKey.Task).append(" and srr.relation_id = dlod.dlo_detail_id ").toString()).append((new StringBuilder()).append(" left join ").append(ConfigBean.getStringValue("storage_yard_control")).append(" as syc on srr.resources_type = ").append(OccupyTypeKey.SPOT).append(" and srr.resources_id = syc.yc_id ").toString()).append((new StringBuilder()).append(" left join ").append(ConfigBean.getStringValue("storage_door")).append(" as sd on srr.resources_type = ").append(OccupyTypeKey.DOOR).append(" and srr.resources_id = sd.sd_id ").toString()).append((new StringBuilder()).append(" where number_type in(").append(ModuleKey.CHECK_IN_LOAD).append(",").append(ModuleKey.CHECK_IN_ORDER).append(",").append(ModuleKey.CHECK_IN_PONO).append(")").toString());
        if(!StrUtil.isBlank(startTime))
            sql.append((new StringBuilder()).append(" and dlod.create_time>='").append(startTime).append("'").toString());
        if(!StrUtil.isBlank(endTime))
            sql.append((new StringBuilder()).append(" and dlod.create_time<='").append(endTime).append("'").toString());
        if(ps_id>0l)
            sql.append((new StringBuilder()).append(" and main.ps_id=").append(ps_id).toString());
        return dbUtilAutoTran.selectMutliple(sql.toString());
	}
	
	
	/**
	 * 获得可用门(自己已使用的和实际可用的)
	 * @author zhanjie
	 * @param entry_id
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCanUseDoor(long entry_id,long ps_id,long area_id)
		throws Exception
	{
		String whereWareHouse = "";
		if (ps_id !=0)
		{
			whereWareHouse = " and sd.ps_id ="+ps_id+" ";
		}
		
		String whereArea = "";
		if (area_id !=0)
		{
			whereArea = " and sd.area_id = "+area_id+" ";
		}
		
		
		
		String sql = "select DISTINCT sd.* from storage_door as sd "
					+"join space_resources_relation as srr on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"where  srr.module_type = "+ModuleKey.CHECK_IN+" and srr.module_id = "+entry_id+" "+whereArea+" "
					+"union "
					+"select DISTINCT sd.* from storage_door as sd "
					+"left join space_resources_relation as srr on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"where srr.srr_id is null "+whereWareHouse+whereArea
					+"order by doorId asc";
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 获得所有不可用门
	 * @param ps_id
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getUnavailableDoor(long ps_id,long area_id)
		throws Exception
	{
		String whereWareHouse = "";
		if (ps_id !=0)
		{
			whereWareHouse = " and sd.ps_id ="+ps_id+" ";
		}
		
		String whereArea = "";
		if (area_id !=0)
		{
			whereArea = " and sd.area_id = "+area_id+" ";
		}
		
		String sql = "select DISTINCT count(srrTask.srr_id)as task_count,sd.* from storage_door as sd "
					+"join space_resources_relation as srr on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"left join space_resources_relation as srrTask on srrTask.resources_id = sd.sd_id and srrTask.resources_type = "+OccupyTypeKey.DOOR+" and srrTask.relation_type = "+SpaceRelationTypeKey.Task+" "
					+"where 1=1 "+whereWareHouse+whereArea
					+"group by sd.sd_id "
					+"order by task_count asc";
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 获得可用车位(自己已使用的和实际可用的)
	 * @author zhanjie
	 * @param entry_id
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCanUseSpot(long entry_id,long ps_id,long area_id)
		throws Exception
	{
		String whereWareHouse = "";
		if (ps_id !=0)
		{
			whereWareHouse = " and syc.ps_id ="+ps_id+" ";
		}
		
		String whereArea = "";
		if (area_id !=0)
		{
			whereArea = " and syc.area_id = "+area_id+" ";
		}
		
		String sql = "select syc.* from storage_yard_control as syc "
					+"join space_resources_relation as srr on syc.yc_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.SPOT+" "
					+"where  srr.module_type = "+ModuleKey.CHECK_IN+" and srr.module_id = "+entry_id+whereArea+" "
					+"union " 
					+"select syc.* from storage_yard_control as syc " 
					+"left join space_resources_relation as srr on syc.yc_id = resources_id and srr.resources_type = "+OccupyTypeKey.SPOT+" "
					+"where  srr.srr_id is null "+whereWareHouse+whereArea
					+"order by yc_no asc ";
		//System.out.println(sql);
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	
	/**
	 * 获得可用门(自己已使用的和实际可用的)根据doorName等条件精确查询可用门
	 * @author geqingling
	 * @param entry_id
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDoorByDoorName(long entry_id,long ps_id,long area_id,String door_name)
		throws Exception
	{
		String whereWareHouse = "";
		if (ps_id !=0)
		{
			whereWareHouse = " and sd.ps_id ="+ps_id+" ";
		}
		
		String whereArea = "";
		if (area_id !=0)
		{
			whereArea = " and sd.area_id = "+area_id+" ";
		}
		
		String whereDoorName = "";
		if (!"".equals(door_name))
		{
			whereDoorName = " and t.doorid ='"+door_name.trim()+"'  ";
		}
		
		String subSql = "select sd.* from storage_door as sd "
					+"join space_resources_relation as srr on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"where  srr.module_type = "+ModuleKey.CHECK_IN+" and srr.module_id = "+entry_id+" "
					+"union "
					+"select sd.* from storage_door as sd "
					+"left join space_resources_relation as srr on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"where srr.srr_id is null "+whereWareHouse+whereArea;
		
		String sql = "select t.* "
					+ "from ( "+subSql+ " ) t "
					+ "where 1=1 "+whereDoorName
					+ " order by t.doorid ";
		
//		System.out.println(sql);
		
		return dbUtilAutoTran.selectMutliple(sql);
	}

	
	/**
	 * 所有不可用车位
	 * @param ps_id
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getUnavailableSpot(long ps_id,long area_id,int relation_type)
		throws Exception
	{
		String whereWareHouse = "";
		if (ps_id !=0)
		{
			whereWareHouse = " and syc.ps_id ="+ps_id+" ";
		}
		
		String whereArea = "";
		if (area_id !=0)
		{
			whereArea = " and syc.area_id = "+area_id+" ";
		}
		
		String whereRelationType = "";
		if (relation_type !=0)
		{
			whereRelationType = "and srr.relation_type="+relation_type+" ";
		}
		
		String sql = "select DISTINCT count(srrTask.srr_id)as task_count,syc.* from storage_yard_control as syc "
					+"join space_resources_relation as srr on syc.yc_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.SPOT+" "
					+"left join space_resources_relation as srrTask on srrTask.resources_id = syc.yc_id and srrTask.resources_type = "+OccupyTypeKey.SPOT+" and srrTask.relation_type = "+SpaceRelationTypeKey.Task+" "
					+"where 1=1 "+whereRelationType+whereWareHouse+whereArea
					+"group by syc.yc_id "
					+"order by task_count asc ";
		//System.out.println(sql);
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 获得entry使用中的门
	 * @author zhanjie
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getEntryUseDoor(long entry_id)
		throws Exception
	{
		String sql = "select DISTINCT sd.* from space_resources_relation as srr "
					+"join storage_door as sd on srr.resources_type = "+OccupyTypeKey.DOOR+" and srr.resources_id = sd.sd_id "
					+"where srr.module_id = ? and srr.module_type ="+ModuleKey.CHECK_IN+" ";
		
		DBRow para = new DBRow();
		para.add("entry_id",entry_id);
		
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	public DBRow[] getEntryTaskUseDoor(long entry_id)
		throws Exception
	{
		String sql = "select DISTINCT sd.* from space_resources_relation as srr "
					+"join storage_door as sd on srr.resources_type = "+OccupyTypeKey.DOOR+" and srr.resources_id = sd.sd_id "
					+"where srr.module_id = ? and srr.module_type ="+ModuleKey.CHECK_IN+" and srr.relation_type = "+SpaceRelationTypeKey.Task+" ";
		
		DBRow para = new DBRow();
		para.add("entry_id",entry_id);
			
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	/**
	 * 获得entry使用中的
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getEntryUseSpot(long entry_id)
		throws Exception
	{
		String sql = "select DISTINCT syc.* from space_resources_relation as srr "
					+"join storage_yard_control as syc on srr.resources_type = "+OccupyTypeKey.SPOT+" and srr.resources_id = syc.yc_id "
					+"where srr.module_id = ? and srr.module_type ="+ModuleKey.CHECK_IN+" ";
		
		DBRow para = new DBRow();
		para.add("entry_id",entry_id);
		
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	/**
	 * 获得所有没占用资源未离开的设备
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] noSpaceRelationEquipment(long ps_id,PageCtrl pc)
		throws Exception
	{
		String whereWareHouse = "";
		
		if (ps_id!=0)
		{
			whereWareHouse = " and dlo.ps_id = "+ps_id+" ";
		}
		
		String sql = "select ee.* ,CONCAT(CASE ee.original_resource_type WHEN "+OccupyTypeKey.DOOR+" THEN 'DOOR  ' WHEN "+OccupyTypeKey.SPOT+" THEN 'SPOT  ' END,IFNULL(syc.yc_no,sd.doorId)) as location from entry_equipment as ee "
					+"join door_or_location_occupancy_main as dlo on ee.check_in_entry_id = dlo.dlo_id "
					+"left join storage_yard_control syc on ee.original_resource_id=syc.yc_id and ee.original_resource_type="+OccupyTypeKey.SPOT+" "
					+"left join storage_door sd on ee.original_resource_id=sd.sd_id and ee.original_resource_type="+OccupyTypeKey.DOOR+" "
					+"left join space_resources_relation as srr on srr.relation_id = ee.equipment_id and srr.relation_type = "+SpaceRelationTypeKey.Equipment+" "
					+"where ee.equipment_status in ("+CheckInMainDocumentsStatusTypeKey.UNPROCESS+","+CheckInMainDocumentsStatusTypeKey.PROCESSING+","+CheckInMainDocumentsStatusTypeKey.INYARD+","+CheckInMainDocumentsStatusTypeKey.LEAVING+") "
					+"and ee.equipment_purpose in (1,2,4) and srr.srr_id is NULL "+whereWareHouse;
		
		return dbUtilAutoTran.selectMutliple(sql,pc);
	}
	
	public DBRow[] getEntryEquipmentUseResource(long entry_id,int equipment_status)
		throws Exception
	{
		String sql = "select * from entry_equipment as ee "
					+"left join space_resources_relation srr on ee.equipment_id = srr.relation_id  and srr.relation_type = "+SpaceRelationTypeKey.Equipment+" "
					+"where ee.check_in_entry_id = "+entry_id+" ";
		
		if(equipment_status>0){
			sql += " and ee.equipment_status <> "+equipment_status ;
		}
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 获得设备，及其关联的资源
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getEquipmentByEntryIdOccpuyResource(long entry_id)
		throws Exception
	{
		String sql = "select * from entry_equipment as ee " 
					+"left join space_resources_relation as srr on ee.equipment_id = srr.relation_id and "
					+"where ee.check_in_entry_id = "+entry_id+" or ee.check_out_entry_id = "+entry_id;
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * CheckOutEntryId关联的设备与对应的资源
	 * @author zhanjie
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCheckOutEntryUseResource(long entry_id)
		throws Exception
	{
		String sql = "select ee.*,srr.*,dm.company_name as carrier from entry_equipment as ee "
					+"left join space_resources_relation as srr on ee.equipment_id = srr.relation_id and srr.relation_type = "+SpaceRelationTypeKey.Equipment+" " 
					+"left join door_or_location_occupancy_main as dm on dm.dlo_id = ee.check_in_entry_id " 
					+"where ee.check_in_entry_id = "+entry_id+" and ee.equipment_purpose in ("+CheckInLiveLoadOrDropOffKey.LIVE+","+CheckInLiveLoadOrDropOffKey.PICK_UP+","+CheckInLiveLoadOrDropOffKey.TMS+")  and ee.equipment_status != 5 "
					+"union " 
					+"select ee.*,srr.*,dm.company_name as carrier from entry_equipment as ee "
					+"left join space_resources_relation as srr on ee.equipment_id = srr.relation_id and srr.relation_type = "+SpaceRelationTypeKey.Equipment+" "
					+"left join door_or_location_occupancy_main as dm on dm.dlo_id = ee.check_in_entry_id "
					+"where ee.check_out_entry_id = "+entry_id+" and ee.equipment_status != 5";
		
		return dbUtilAutoTran.selectMutliple(sql);
	}

	/**
	 * 根据资源类型与资源ID获得资源当前被谁使用
	 * @param resource_type
	 * @param resource_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSpaceResourceUse(int resource_type,long resource_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("space_resources_relation")+" where resources_type =? and resources_id = ? ";
		
		DBRow para = new DBRow();
		para.add("resources_type",resource_type);
		para.add("resources_id",resource_id);
		
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	/**
	 * 
	 * @param module_type
	 * @param module_id
	 * @param resource_type
	 * @param resource_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getMoedUseResource(int module_type,long module_id,int resource_type,long resource_id)
		throws Exception
	{
		DBRow para = new DBRow();
		para.add("module_type",module_type);
		para.add("module_id",module_id);
		
		String whereResourceType = "";
		String whereResourceId = "";
		
		if (resource_type !=0)
		{
			whereResourceType = " and resources_type = ? ";
			para.add("resources_type",resource_type);
		}
		
		if (resource_id !=0)
		{
			whereResourceId = " and resources_id = ? ";
			para.add("resources_id",resource_id);
		}
		
		String sql = "select * from "+ConfigBean.getStringValue("space_resources_relation")+"  where module_type = ? and module_id = ? "+whereResourceType+whereResourceId;
		
		
		
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	
	/**
	 * 获取某个Entry的某个设备下在某个资源的Task
	 * @param entry_id
	 * @param resouces_id
	 * @param resouces_type
	 * @param equipment_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月29日
	 */
	public DBRow[] getRelationByResouceAndEntryIdAndEquipmentId(long entry_id , long resources_id , int resources_type , long equipment_id,int relation_type)
			throws Exception
	{
		 
		String sql = "select * from door_or_location_occupancy_details as details "
					+ "LEFT JOIN space_resources_relation as srr  on srr.relation_id =  details.dlo_detail_id and srr.relation_type = "+relation_type+" "
					+" where details.equipment_id =? and srr.resources_id =?  and srr.resources_type =? and srr.module_type="+ModuleKey.CHECK_IN+" and srr.module_id = ? ";
			
		DBRow para = new DBRow();
		para.add("equipment_id", equipment_id);
		para.add("resources_id", resources_id);
		para.add("resources_type", resources_type);
		para.add("module_id", entry_id);
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	/**
	 * @param srr_id
	 * @param updateRow
	 * @throws Exception
	 * @author zhangrui
	 * @Date   2014年11月29日
	 */
	public int updateSpaceResourcesRelation(long srr_id , DBRow updateRow) throws Exception{
		return dbUtilAutoTran.update(" where srr_id="+srr_id, ConfigBean.getStringValue("space_resources_relation"), updateRow);
	}
	
	
	/**
	 * 根据资源id  查询出占用的设备
	 * @param relation_id
	 * @param relation_type
	 * @return
	 * @throws Exception 
	 * @author zwb
	 */
	public DBRow[] getEquipmentRelation(int resources_type,long resources_id,int relation_type)
			throws Exception
	{
		String sql="select * from space_resources_relation as sr "
				+ "join entry_equipment as ee  on sr.relation_id=ee.equipment_id "
				+ "where resources_type = "+resources_type+" and resources_id="+resources_id+" and relation_type="+relation_type;
		return this.dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 根据资源id 查询出 占用的任务的数量
	 * @param relation_id
	 * @param relation_type
	 * @return
	 * @throws Exception 
	 * @author zwb
	 */
	public DBRow getCountRelation(int resources_type,long resources_id,int relation_type)
			throws Exception
	{
		String sql="select count(*) as task from space_resources_relation "
				+ "where  resources_type = "+resources_type+" and resources_id="+resources_id+" and relation_type= "+relation_type;
		return this.dbUtilAutoTran.selectSingle(sql);
	}
	
	/**
	 * 根据资源id 查询资源信息
	 * @param srr_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getSpaceResourcesRelation(long srr_id)
		throws Exception
	{
		String sql="select * from space_resources_relation where srr_id="+srr_id;
		return this.dbUtilAutoTran.selectSingle(sql);
	}
	
	
	
	
	/**
	 * 基于区域显示占用的门的数量
	 * @author zhanjie
	 * @param ps_id
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] occupyDoorForAreaView(long ps_id,long area_id)
		throws Exception
	{
		String whereWareHouse = "";
		String whereArea = "";
		if(ps_id !=0)
		{
			whereWareHouse = " and sla.area_psid = "+ps_id+" " ;
		}
		if (area_id !=0)
		{
			whereArea = " and sla.area_id = "+area_id+" ";
		}
		
		String sql = "select sla.area_id,sla.area_name,count(sd.sd_id) AS allCount,count(sd.sd_id)-count(sd.patrol_time) as needPatrol, max(sd.patrol_time) as last_time,sla.patrol_time from "+ConfigBean.getStringValue("storage_location_area")+" as sla " 
					+"join "+ConfigBean.getStringValue("storage_door")+" as sd on sla.area_id = sd.area_id "
					+"where 1 = 1 "+whereWareHouse+whereArea
					+"group by sla.area_id " 
					+"UNION "
					+"select 0,'ALL',count(sd.sd_id) AS allCount,count(sd.sd_id)-count(sd.patrol_time) as needPatrol, max(sd.patrol_time) as last_time,sla.patrol_time from "+ConfigBean.getStringValue("storage_location_area")+" as sla " 
					+"join "+ConfigBean.getStringValue("storage_door")+" as sd on sla.area_id = sd.area_id "
					+"where 1 = 1 "+whereWareHouse+whereArea ;
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 基于区域显示占用的车位的数量
	 * @author zhanjie
	 * @param ps_id
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] occupySpotForAreaView(long ps_id,long area_id)
		throws Exception
	{
		String whereWareHouse = "";
		String whereArea = "";
		if(ps_id !=0)
		{
			whereWareHouse = " and sla.area_psid = "+ps_id+" " ;
		}
		if (area_id !=0)
		{
			whereArea = " and sla.area_id = "+area_id+" ";
		}
		
		String sql = "select sla.area_id,sla.area_name,count(syc.yc_id) AS allCount,count(syc.yc_id)-count(syc.patrol_time) as needPatrol, max(syc.patrol_time) as last_time,sla.patrol_time from storage_location_area as sla "
					+"join storage_yard_control as syc on sla.area_id = syc.area_id "
					+"where 1 = 1 "+whereWareHouse+whereArea
					+"group by sla.area_id "
					+"UNION "
					+"select 0,'ALL',count(syc.yc_id) AS allCount,count(syc.yc_id)-count(syc.patrol_time) as needPatrol, max(syc.patrol_time) as last_time,sla.patrol_time from storage_location_area as sla "
					+"join storage_yard_control as syc on sla.area_id = syc.area_id "
					+"where 1 = 1 "+whereWareHouse+whereArea;
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	
	/**
	 * 获得可用门(自己已使用的和实际可用的)根据doorname
	 * @author geqingling
	 * @param entry_id
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCanUseDoorByDoorName(long entry_id,long ps_id,long area_id,String door_name)
		throws Exception
	{
		String whereWareHouse = "";
		if (ps_id !=0)
		{
			whereWareHouse = " and sd.ps_id ="+ps_id+" ";
		}
		
		String whereArea = "";
		if (area_id !=0)
		{
			whereArea = " and sd.area_id = "+area_id+" ";
		}
		
		String whereDoorName = "";
		if (!"".equals(door_name))
		{
			//whereDoorName = " and t.doorid = "+door_name.trim()+" ";
			whereDoorName = " and t.doorid like '%"+door_name.trim()+"%'";
		}
		
		String subSql = "select sd.* from storage_door as sd "
					+"join space_resources_relation as srr on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"where  srr.module_type = "+ModuleKey.CHECK_IN+" and srr.module_id = "+entry_id+" "
					+"union "
					+"select sd.* from storage_door as sd "
					+"left join space_resources_relation as srr on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"where srr.srr_id is null "+whereWareHouse+whereArea;
		
		String sql = "select t.* "
					+ "from ( "+subSql+ " ) t "
					+ "where 1=1 "+whereDoorName+" order by t.doorid ";
		
		
//		System.out.println(sql);
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	
	/**
	 * gatecheckin根据主单据号查询占用的信息
	 * @author zwb
	 * @param module_id
	 * @param module_type
	 * @return
	 * @throws Exception
	 */
	public DBRow getResourcesByModuleId(long module_id,int module_type)
			throws Exception
	{
		String sql="select a.*, area.area_name from(select sr.*, IFNULL(sd.doorId,sy.yc_no) resource_name, "
				+ "IFNULL(sd.area_id,sy.area_id) area_id  FROM space_resources_relation sr "
				+ "LEFT JOIN storage_door sd on sd.sd_id = sr.resources_id and sr.resources_type = "+OccupyTypeKey.DOOR+" "
				+ "LEFT JOIN storage_yard_control sy on sy.yc_id = sr.resources_id and sr.resources_type ="+OccupyTypeKey.SPOT+" "
				+ "where sr.module_type = "+module_type+" and sr.module_id = "+module_id+" LIMIT 1) a join storage_location_area area on a.area_id = area.area_id;";
		
		return this.dbUtilAutoTran.selectSingle(sql);
	}
	
	/**
	 * 获得所有不可用门   gql
	 * @param ps_id
	 * @param area_id
	 * @param doorId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findUnavailableDoorByDoorName(long ps_id,long area_id,String door_name)
		throws Exception
	{
		String whereWareHouse = "";
		if (ps_id !=0)
		{
			whereWareHouse = " and sd.ps_id ="+ps_id+" ";
		}
		
		String whereArea = "";
		if (area_id !=0)
		{
			whereArea = " and sd.area_id = "+area_id+" ";
		}
		String whereDoorName = "";
		if (!"".equals(door_name))
		{
			whereDoorName = " and t.doorid ="+door_name.trim()+" ";
		}
		
		String subSql = "select DISTINCT sd.* from storage_door as sd "
					+"join space_resources_relation as srr on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"where 1=1 "+whereWareHouse+whereArea;
		
		String sql = "select t.* "
				+ "from ( "+subSql+ " ) t "
				+ "where 1=1 "+whereDoorName;
	
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * window check in 时查找被占用的门
	 * @author gql
	 * @param ps_id
	 * @param area_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getOccupiedDoorOfWindow(long ps_id,long area_id)
		throws Exception
	{	
		String whereWarehouse = "";
		String whereArea = "";
		if (ps_id !=0)
		{
			whereWarehouse = " AND sd.ps_id = "+ps_id+" ";
		}
		
		if (area_id != 0)
		{
			whereArea = " AND sd.area_id = "+area_id+" ";
		}
		
		String sql = "SELECT a.*, sd.doorId FROM ("
						+"SELECT resources_id, resources_type,occupy_status, count(*) task_count  FROM space_resources_relation "
						+"WHERE resources_type = "+OccupyTypeKey.DOOR+" "
						+"AND relation_type = "+SpaceRelationTypeKey.Task+" "
						+"GROUP BY resources_id) a "
					+"JOIN storage_door sd ON sd.sd_id = a.resources_id "
					+"WHERE a.resources_id NOT IN( "
						+"SELECT resources_id FROM space_resources_relation "
						+"WHERE resources_type = "+OccupyTypeKey.DOOR+" "
						+"AND (occupy_status = "+OccupyStatusTypeKey.RESERVERED+" OR (occupy_status = "+OccupyStatusTypeKey.RESERVERED+" AND relation_type = "+SpaceRelationTypeKey.Equipment+")) GROUP BY resources_id ) "
						+ whereWarehouse + whereArea
						+"ORDER BY a.task_count, a.resources_id ";
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	
	/**
	 * 根据主单据id查询 设备占用的停车位   gatecheckin 标签用  zwb
	 * @param module_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getEquipmentUseSpot(long module_id)
			throws Exception
	{
		String sql="select * from space_resources_relation as sr "
				+ "LEFT JOIN storage_yard_control sy on sy.yc_id = sr.resources_id "
				+ "where module_id="+module_id+" and module_type="+ModuleKey.CHECK_IN+" and relation_type="+SpaceRelationTypeKey.Equipment+"  and resources_type = "+OccupyTypeKey.SPOT+" "
				+ "GROUP BY yc_no";
		return this.dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 根据主单据id查询 task占用的门 gatecheckin 标签用 zwb
	 * @param module_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getTaskUseDoor(long module_id)
		throws Exception
	{
		String sql="select * from space_resources_relation as sr "
				+ "left join storage_door as sd on sd.sd_id = sr.resources_id "
				+ "left join door_or_location_occupancy_details as ds on ds.dlo_detail_id=sr.relation_id "
				+ "where module_id="+module_id+" and module_type="+ModuleKey.CHECK_IN+" and relation_type="+SpaceRelationTypeKey.Task+" and resources_type = "+OccupyTypeKey.DOOR+" order by sd.doorId";
		return this.dbUtilAutoTran.selectMutliple(sql);
	}

	
	/**
	 * 获得所有不可用门   geql
	 * @param ps_id
	 * @param area_id
	 * @param doorId
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getUnavailableDoorByDoorName(long ps_id,long area_id,String door_name)
		throws Exception
	{
		String whereWareHouse = "";
		if (ps_id !=0)
		{
			whereWareHouse = " and sd.ps_id ="+ps_id+" ";
		}
		
		String whereArea = "";
		if (area_id !=0)
		{
			whereArea = " and sd.area_id = "+area_id+" ";
		}
		String whereDoorName = "";
		if (!"".equals(door_name))
		{
			whereDoorName = " and t.doorid like '"+door_name.trim()+"%' ";
		}
		
		String subSql = "select DISTINCT sd.* from storage_door as sd "
					+"join space_resources_relation as srr on sd.sd_id = srr.resources_id and srr.resources_type = "+OccupyTypeKey.DOOR+" "
					+"where 1=1 "+whereWareHouse+whereArea;
		
		String sql = "select t.* "
				+ "from ( "+subSql+ " ) t "
				+ "where 1=1 "+whereDoorName+" order by t.doorid ";
	
		return dbUtilAutoTran.selectMutliple(sql);
	}


	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
