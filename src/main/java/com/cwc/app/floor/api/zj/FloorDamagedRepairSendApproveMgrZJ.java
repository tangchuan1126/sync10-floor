package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorDamagedRepairSendApproveMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 添加返修发货审核
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addDamagedRepairSendApprove(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long rsa_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("repair_send_approve"));
			
			dbrow.add("rsa_id",rsa_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("repair_send_approve"),dbrow);
			
			return (rsa_id);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairSendApproveMgrZJ addDamagedRepairSendApprove error:"+e);
		}
	}
	
	/**
	 * 修改返修单审核
	 * @param rsa_id
	 * @param para
	 * @throws Exception
	 */
	public void modDamagedRepairSendApprove(long rsa_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update(" where rsa_id="+rsa_id,ConfigBean.getStringValue("repair_send_approve"),para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairSendApproveMgrZJ modDamagedRepairSendApprove error:"+e);
		}
	}
	
	/**
	 * 根据ID获得返修审核详细
	 * @param rsa_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailDamagedRepairSendApprove(long rsa_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("repair_send_approve")+" where rsa_id=?";
			
			DBRow para = new DBRow();
			para.add("rsa_id",rsa_id);
			
			return (dbUtilAutoTran.selectPreSingle(sql, para));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairSendApproveMgrZJ getDetailDamagedRepairSendApprove error:"+e);
		}
	}
	
	/**
	 * 添加返修明细
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addDamagedRepairSendApproveDetail(DBRow dbrow)
		throws Exception
	{
		try 
		{
			long rsad_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("repair_send_approve_detail"));
			
			dbrow.add("rsad_id",rsad_id);
			
			dbUtilAutoTran.insert(ConfigBean.getStringValue("repair_send_approve_detail"),dbrow);
			
			return (rsad_id);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairSendApproveMgrZJ addDamagedRepairSendApproveDetail error:"+e);
		}
	}
	
	/**
	 * 修改返修单明细
	 * @param rsad_id
	 * @param para
	 * @throws Exception
	 */
	public void modDamagedRepairSendApproveDetail(long rsad_id,DBRow para)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update(" where rsad_id="+rsad_id,ConfigBean.getStringValue("repair_send_approve_detail"),para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairSendApproveMgrZJ modDamagedRepairSendApproveDetail error:"+e);
		}
	}
	
	/**
	 * 过滤发货审核
	 * @param send_psid
	 * @param receive_psid
	 * @param pc
	 * @param approve_status
	 * @param sorttype
	 * @return
	 * @throws Exception
	 */
	public DBRow[] fillterDamagedRepairOutboundApprove(long send_psid, long receive_psid,PageCtrl pc, int approve_status, String sorttype)
	throws Exception
	{
		try {
			StringBuffer sql = new StringBuffer("select rsa.*,send.title as sendtitle,receive.title as receivetitle from "+ConfigBean.getStringValue("repair_send_approve")+" as rsa"
					+" left join "+ConfigBean.getStringValue("damaged_repair_order")+" as dro on dro.repair_id = rsa.repair_id"
					+" left join "+ConfigBean.getStringValue("product_storage_catalog")+" as send on send.id = dro.send_psid"
					+" left join "+ConfigBean.getStringValue("product_storage_catalog")+" as receive on receive.id = dro.receive_psid"
					+" where 1=1");
					if(send_psid>0)
					{
					sql.append(" and t.send_psid = "+send_psid);
					}
					if(receive_psid>0)
					{
					sql.append(" and t.receive_psid = "+receive_psid);
					}
					
					if(approve_status>0)
					{
					sql.append(" and rsa.approve_status = "+approve_status);
					}
					
					if(sorttype.equals("subdate"))
					{
					sql.append(" order by rsa.rsa_id desc");
					}
					else if(sorttype.equals("approvedate"))
					{
					sql.append(" order by rsa.approve_date desc");
					}
					else
					{
					sql.append(" order by rsa.rsa_id desc");
					}
					return dbUtilAutoTran.selectMutliple(sql.toString(),pc);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairSendApproveMgrZJ fillterDamagedRepairOutboundApprove error:"+e);
		}
	}
	
	/**
	 * 根据返修审核ID获得返修审核明细，可区分状态
	 * @param rsa_id
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDamagedRepairDetailsByRsa_id(long rsa_id,int status)
		throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer("select *,(repair_send_count-repair_count) as different_count from "+ConfigBean.getStringValue("repair_send_approve_detail")+" where rsa_id=? ");
			
			DBRow para = new DBRow();
			para.add("rsa_id",rsa_id);
			
			if(status!=0)
			{
				sql.append(" and approve_status = ?");
				para.add("approve_status",status);
			}
			
			return (dbUtilAutoTran.selectPreMutliple(sql.toString(), para));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairSendApproveMgrZJ getDamagedRepairDetailsByRsa_id error:"+e);
		}
	}
	
	/**
	 * 审核差异后回退或再次扣掉库存
	 * @param ps_id
	 * @param pc_id
	 * @param count
	 * @throws Exception
	 */
	public void returnDamagedCount(long ps_id,long pc_id,float count)
		throws Exception
	{
		try 
		{
			if(count>0)
			{
				dbUtilAutoTran.increaseFieldVal(ConfigBean.getStringValue("product_storage")," where cid="+ps_id+" and pc_id="+pc_id,"damaged_count",count);
			}
			else
			{
				dbUtilAutoTran.decreaseFieldVal(ConfigBean.getStringValue("product_storage")," where cid="+ps_id+" and pc_id="+pc_id,"damaged_count",count);
			}
			
		}
		catch (Exception e) 
		{
			throw new Exception("FloorDamagedRepairSendApproveMgrZJ returnDamagedCount error:"+e);
		}
	}
	

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
