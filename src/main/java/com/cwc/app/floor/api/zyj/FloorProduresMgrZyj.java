package com.cwc.app.floor.api.zyj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorProduresMgrZyj {

	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 通过关联类型查询所有流程
	 * @param order_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProduresByOrderType(int order_type) throws Exception
	{
		try
		{
			StringBuilder sb = new StringBuilder();
			sb.append("select * from produres");
			if(order_type > 0)
			{
				sb.append(" where associate_order_type = ").append(order_type);
			}
			sb.append(" order by activity_is_need_display desc, produres_id asc");
			return dbUtilAutoTran.selectMutliple(sb.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.getProduresByOrderType(order_type) error:" + e);
		}
	}
	
	/**
	 * 通过流程ID，阶段值数组，阶段状态数组查询阶段
	 * @param produres_id
	 * @param activities
	 * @param activitiesStatus
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProduresDetailsByProduresId(long produres_id, int[] activities, int[] activitiesStatus) throws Exception
	{
		try
		{
			StringBuilder sb = new StringBuilder();
			sb.append("select * from produres_details where 1=1 ");
			if(0 != produres_id)
			{
				sb.append(" and produres_id = " + produres_id);
			}
			if(null != activities && activities.length > 0)
			{
				sb.append(" and (activity_val = " + activities[0]);
				for (int i = 1; i < activities.length; i++) 
				{
					sb.append(" or activity_val = " + activities[i]);
				}
				
				sb.append(" )");
			}
			if(null != activitiesStatus && activitiesStatus.length > 0)
			{
				sb.append(" and (val_status = " + activitiesStatus[0]);
				for (int i = 1; i < activitiesStatus.length; i++) 
				{
					sb.append(" or val_status = " + activitiesStatus[i]);
				}
				
				sb.append(" )");
			}
			
			return dbUtilAutoTran.selectMutliple(sb.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.getProduresDetailsByProduresId(produres_id, int[] activities) error:" + e);
		}
	}
	
	/**
	 * 通过流程ID，是否显示查询通知信息
	 * @param produres_id
	 * @param is_display
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProduresNoticesByProduresId(long produres_id, int is_display) throws Exception
	{
		try
		{
			StringBuilder sb = new StringBuilder();
			sb.append("select * from produres_notice where 1=1 ");
			if(0 != produres_id)
			{
				sb.append(" and produres_id = "+ produres_id);
			}
			if(0 != is_display)
			{
				sb.append(" and notice_is_display = " + is_display);
			}
			return dbUtilAutoTran.selectMutliple(sb.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.getProduresNoticesByProduresId(produres_id, int is_display) error:" + e);
		}
	}

	/**
	 * 通过流程ID获取流程信息
	 * @param produres_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getProdureByProdureId(long produres_id) throws Exception
	{
		try
		{
			StringBuilder sb = new StringBuilder();
			sb.append("select * from produres where produres_id = ").append(produres_id);
			return dbUtilAutoTran.selectSingle(sb.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.getProdureByProdureId(long produre_id) error:" + e);
		}
	}
	
	/**
	 * 通过流程阶段ID详细详细
	 * @param produres_details_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getProduresDetailsByProduresDetailId(long produres_details_id) throws Exception
	{
		try
		{
			StringBuilder sb = new StringBuilder();
			sb.append("select * from produres_details where id = ").append(produres_details_id);
			return dbUtilAutoTran.selectSingle(sb.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.getProduresDetailsByProduresDetailId(long produres_details_id) error:" + e);
		}
	}
	
	/**
	 * 通过通知ID获取通知详细
	 * @param notice_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getProduresNoticeByNoticeId(long notice_id) throws Exception
	{
		try
		{
			StringBuilder sb = new StringBuilder();
			sb.append("select * from produres_notice where id = ").append(notice_id);
			return dbUtilAutoTran.selectSingle(sb.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.getProduresNoticeByNoticeId(long notice_id) error:" + e);
		}
	}
	
	/**
	 * 通过流程ID，是否显示，通知类型查询
	 * @param produres_id
	 * @param is_display
	 * @param notice_type
	 * @return
	 * @throws Exception
	 */
	public DBRow getProduresNoticeByProduresIdIsDisplayType(long produres_id, int is_display, int notice_type) throws Exception
	{
		try
		{
			StringBuilder sb = new StringBuilder();
			sb.append("select * from produres_notice where 1=1");
			if(0 != produres_id)
			{
				sb.append(" and produres_id = ").append(produres_id);
			}
			if(0 != is_display)
			{
				sb.append(" and notice_is_display = ").append(is_display);
			}
			if(0 != notice_type)
			{
				sb.append(" and notice_type = ").append(notice_type);
			}
			return dbUtilAutoTran.selectSingle(sb.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.getProduresNoticeByProduresIdIsDisplayType(long produres_id, int is_display, int notice_type) error:" + e);
		}
	}
	
	
	/**
	 * 通过流程阶段ID详细详细
	 * @param produres_details_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getProduresDetailsByProduresIdActivity(long produres_id, int activity) throws Exception
	{
		try
		{
			StringBuilder sb = new StringBuilder();
			sb.append("select * from produres_details where produres_id = ").append(produres_id);
			sb.append(" and activity_val = ").append(activity);
			return dbUtilAutoTran.selectSingle(sb.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.getProduresDetailsByProduresDetailId(long produres_id, int activity) error:" + e);
		}
	}
	
	/**
	 * 添加流程
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addProdure(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("produres"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.addProdure(DBRow row) error:" + e);
		}
	}
	
	/**
	 * 添加流程阶段
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addProdureDetail(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("produres_details"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.addProdureDetail(DBRow row) error:" + e);
		}
	}
	
	/**
	 * 添加流程通知
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addProdureNotice(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("produres_notice"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.addProdureNotice(DBRow row) error:" + e);
		}
	}
	
	/**
	 * 更新流程
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long updateProdure(DBRow row, long produre_id) throws Exception
	{
		try
		{
			return dbUtilAutoTran.update(" where produres_id = " + produre_id, ConfigBean.getStringValue("produres"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.updateProdure(DBRow row, long produre_id) error:" + e);
		}
	}
	
	/**
	 * 更新流程阶段
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long updateProdureDetail(DBRow row, long id) throws Exception
	{
		try
		{
			return dbUtilAutoTran.update(" where id="+id, ConfigBean.getStringValue("produres_details"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.updateProdureDetail(DBRow row, long id) error:" + e);
		}
	}
	
	/**
	 * 更新流程通知
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long updateProdureNotice(DBRow row, long id) throws Exception
	{
		try
		{
			return dbUtilAutoTran.update(" where id = "+id,ConfigBean.getStringValue("produres_notice"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.updateProdureNotice(DBRow row, long id)error:" + e);
		}
	}

	public int findProdureByOrderTypeKeyColumn(int order_type, int key, String process_name, String column, String column_page) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select count(*) as produreCount from produres where 1=1");
			if(order_type > 0)
			{
				sql.append(" and associate_order_type = ").append(order_type);
			}
			if(key > 0)
			{
				sql.append(" and process_key = ").append(key);
			}
			if(null != process_name && !"".equals(process_name))
			{
				sql.append(" and process_name = '").append(process_name).append("'");
			}
			if(null != column && !"".equals(column))
			{
				sql.append(" and process_coloum = '").append(column).append("'");
			}
			if(null != column_page && !"".equals(column_page))
			{
				sql.append(" and process_coloum_page = '").append(column_page).append("'");
			}
			return dbUtilAutoTran.selectSingle(sql.toString()).get("produreCount", 0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.findProdureByOrderTypeKeyColumn(int order_type, int key, String process_name, String column, String column_page)error:" + e);
		}
	}
	
	public DBRow findProdureRowsByOrderTypeKeyColumn(int order_type, int key, String process_name, String column, String column_page) throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select * from produres where 1=1");
			if(order_type > 0)
			{
				sql.append(" and associate_order_type = ").append(order_type);
			}
			if(key > 0)
			{
				sql.append(" and process_key = ").append(key);
			}
			if(null != process_name && !"".equals(process_name))
			{
				sql.append(" and process_name = '").append(process_name).append("'");
			}
			if(null != column && !"".equals(column))
			{
				sql.append(" and process_coloum = '").append(column).append("'");
			}
			if(null != column_page && !"".equals(column_page))
			{
				sql.append(" and process_coloum_page = '").append(column_page).append("'");
			}
			return dbUtilAutoTran.selectSingle(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.findProdureRowsByOrderTypeKeyColumn(int order_type, int key, String process_name, String column, String column_page)error:" + e);
		}
	}
	
	public void deleteProdureActivitysByProdureId(long produre_id) throws Exception
	{
		try
		{
			dbUtilAutoTran.delete(" where produres_id = " + produre_id, ConfigBean.getStringValue("produres_details"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.deleteProdureActivitysByProdureId(long produre_id)error:" + e);
		}
	}
	
	public void deleteProdureNoticesByProdureId(long produre_id) throws Exception
	{
		try
		{
			dbUtilAutoTran.delete(" where produres_id = " + produre_id, ConfigBean.getStringValue("produres_notice"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProduresMgrZyj.deleteProdureNoticesByProdureId(long produre_id)error:" + e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
	
}
