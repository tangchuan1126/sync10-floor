package com.cwc.app.floor.api.zj;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorSupplierMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBRow SupplierLogin(long id,String password)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("supplier")+" where id = ? and password = ?";
			
			DBRow para = new DBRow();
			para.add("id",id);
			para.add("password",password);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorSupplierMgrZJ SupplierLogin error:"+e);
		}
	}
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
