/**
 * 基于图数据库的库存模型操作接口 主要负责产品分类万能属性的接口
 */
package com.cwc.app.floor.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;
import us.monoid.web.Content;
import us.monoid.web.JSONResource;
import us.monoid.web.Resty;
import us.monoid.web.Resty.Option;
import us.monoid.web.TextResource;

import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.Neo4jTransaction;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorProductCategoryMgr  {
	static Logger log = Logger.getLogger("JAVAPS");
	
	/**
	 * 
	 * 节点类型的枚举值，作为有关方法的参数！
	 * 
	 */
	public enum NodeType {
		 Category, Product,Attribute
	};

	/**
	 * 
	 * 关系类型的枚举值，对调用方暂时不会用到
	 * 
	 */
	public enum RelationType {
		CONTAINS, ATTR
	};
	
	public interface ProductStoreMgrCallbackIFace {
		/**
		 * 
		 * @param props
		 * 			当前容器属性
		 * @param children
		 * 			当前节点的下级子容器数组
		 * @param products
		 * 			当前节点直接放置的产品数组
		 * @return
		 * 			如果需要在遍历过程中构造一颗树，则应返回生成出的当前容器树节点对象，遍历机制在进入下级时，会自动把这个生成的节点压栈
		 * 			如果不转换节点，则返回null，则遍历机制会将参数中的props压栈
		 * @throws Exception
		 */
		public Object containerNode(DBRow props, DBRow[] children, DBRow[] products) throws Exception;
		
		public Stack<Object> getTraverseStack();
		
		public void setTraverseStack(Stack<Object> stack);
		
		public void containerNodePoped(Object node) throws Exception;
		
	}

	private DBUtilAutoTran dbUtilAutoTran;

	private String restBaseUrl;

	private Resty client;

	private static long CACHE_EXPIRES = 3600 * 1000;

	private ThreadLocal<Map<String, Integer>> containerCapacityCache = new ThreadLocal<Map<String, Integer>>();
	private ThreadLocal<Map<String, Long>> containerCapacityCacheTm = new ThreadLocal<Map<String, Long>>();

	private Map<NodeType, Set<String>> nodePropNames = new HashMap<NodeType, Set<String>>();
	private Map<RelationType, Set<String>> relPropNames = new HashMap<RelationType, Set<String>>();
	private Map<String, RelationType> relNames = new HashMap<String, RelationType>();
	
	public void setProductPropNames(Set<String> propNames){
		nodePropNames.put(NodeType.Product,propNames);
	}
	

	public FloorProductCategoryMgr() {
		client = new Resty(Option.timeout(1000000));

		relNames.put(
				NodeType.Category + "-" + NodeType.Product,
				RelationType.CONTAINS);
		relNames.put(NodeType.Category + "-" + NodeType.Attribute,
				RelationType.ATTR);
	}

	public DBUtilAutoTran getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	public String getRestBaseUrl() {
		return restBaseUrl;
	}

	public void setRestBaseUrl(String restBaseUrl) {
		this.restBaseUrl = restBaseUrl;
	}

	public void addNodes(DBRow[] nodes, String nodeLabel, JSONArray statements)
			throws Exception {
		for (DBRow node : nodes) {

			JSONObject parameters = new JSONObject();
			List<String> props = new ArrayList<String>();

			for (Object fn : node.getFieldNames()) {
				String k = ((String) fn).toLowerCase();
				parameters.put(k, node.getValue((String) fn));
				props.add(k + ":{" + k + "}");
			}

			JSONObject statement = new JSONObject();
			statement.put("statement", "MERGE (n:" + nodeLabel + " { "
					+ StringUtils.join(props, ",") + " }) return n");
			statement.put("parameters", parameters);
			statements.put(statement);
		}
	}






	 /**
     * 将long数组返回mysql in () 的string类型
     * @param params
     * @param bracket () {} [] 
     * @return
     * @throws Exception
     */
	public String LongArraytoString(long[] params,String 
			sbracket,String 
			ebracket)throws Exception{
		if(params.length<=0){
			return sbracket+ebracket ;
		}else{
			String ret=sbracket;
			for(int i=0;i<params.length;i++){
				ret+=Long.toString(params[i]);
				ret+=",";
			}
			ret =ret.substring(0, ret.lastIndexOf(","));
		    ret+=ebracket;	
		    return ret ;
		}
	}
	
	/**
	 * 清空指定仓库内所有东西！注意！！！这个方法一般不要使用！！！通常只是用于单元测试
	 * 
	 * @param ps_id
	 * @param reIndex
	 * 		是否清除索引定义并重建
	 * @throws Exception
	 */
	public void clearGraphDB(long ps_id, boolean reIndex) throws Exception {
		JSONArray statements = new JSONArray();
		List<String> cmds = new ArrayList<String>();
		cmds.add("match ()-[r]->() delete r");
		cmds.add("match (n) delete n");

		for (String cmd : cmds) {
			JSONObject statement = new JSONObject();
			statement.put("statement", cmd);
			statements.put(statement);
		}

		JSONObject request = new JSONObject();
		request.put("statements", statements);
		JSONResource response = client.json(restBaseUrl + "/" + ps_id
				+ "/db/data/transaction/commit", new Content(
				"application/json", request.toString().getBytes("UTF-8")));
		if (response.object().getJSONArray("errors").length() > 0)
			throw new Exception(response.object().getJSONArray("errors")
					.toString(4));

		if (reIndex) {
			cmds.clear();
			cmds.add("DROP CONSTRAINT ON (c:Catagroy) ASSERT c.con_id IS UNIQUE");
			cmds.add("CREATE CONSTRAINT ON (c:Container) ASSERT c.con_id IS UNIQUE");
			cmds.add("drop index on :Container(container_type)");
			cmds.add("create index on :Container(container_type)");

			cmds.add("drop index on :StorageLocationCatalog(slc_id)");
			cmds.add("create index on :StorageLocationCatalog(slc_id)");

			cmds.add("drop index on :StorageLocationCatalog(ps_id)");
			cmds.add("create index on :StorageLocationCatalog(ps_id)");
			cmds.add("drop index on :StorageLocationCatalog(slc_area)");
			cmds.add("create index on :StorageLocationCatalog(slc_area)");

			cmds.add("drop index on :Product(pc_id)");
			cmds.add("create index on :Product(pc_id)");
			cmds.add("drop index on :Product(title_id)");
			cmds.add("create index on :Product(title_id)");
			cmds.add("drop index on :Product(product_line)");
			cmds.add("create index on :Product(product_line)");
			cmds.add("drop index on :Product(catalogs)");
			cmds.add("create index on :Product(catalogs)");

			for (String cmd : cmds) {
				try {
					client.json(
							restBaseUrl + "/" + ps_id + "/db/data/cypher",
							new Content("application/json", new JSONObject()
									.put("query", cmd).toString()
									.getBytes("UTF-8")));
				} catch (Exception e) {
					log.error(e.getMessage());
					System.err.println(e.getMessage());
				}
			}
		}

	}

	/**
	 * 优化图数据库，清除两类空的关系：
	 * 			 清除规则待定
	 * @param ps_id
	 * @throws Exception
	 */
	public void optGraphDB(long ps_id) throws Exception {
		JSONArray statements = new JSONArray();
		// 清理空的Category与产品关系（CONTAINS） 删除规则待定
		statements
				.put(new JSONObject()
						.put("statement",
								"match (c:Category)-[r:CONTAINS]->(p:Product) delete r"));

		// 清理空的位置Category和Attribute 关系
		statements
				.put(new JSONObject()
						.put("statement",
								"match (:Category)-[]->(p:Attribute) delete alock"));

		JSONResource response = client.json(
				restBaseUrl + "/" + ps_id + "/db/data/transaction/commit",
				new Content("application/json", new JSONObject()
						.put("statements", statements).toString()
						.getBytes("UTF-8")));
		if (response.object().getJSONArray("errors").length() > 0)
			throw new Exception(response.object().getJSONArray("errors")
					.toString(4));
	}

	/****************************************************************************
	 * 以下为ProductStoreMgrIFace接口的实现方法所依赖的工具函数 * *
	 ****************************************************************************/

	private JSONObject tx(long ps_id, JSONObject... statements)
			throws Exception {
		return transaction(ps_id, new JSONArray(statements));
	}

	private JSONObject transaction(long ps_id, JSONArray statements)
			throws Exception {
		return new Neo4jTransaction(this.restBaseUrl + "/" + ps_id, this.client)
				.commit(statements);
	}

	private JSONArray query(long ps_id, JSONObject params, String... queryLines)
			throws Exception {
		JSONResource response = client.json(
				restBaseUrl + "/" + ps_id + "/db/data/cypher",
				new Content("application/json", new JSONObject()
						.put("query", StringUtils.join(queryLines, " \n"))
						.put("params", params).toString().getBytes("UTF-8")));
		if (response.object().has("exception"))
			throw new Exception(response.object().toString(4));
		

		return response.object().getJSONArray("data");

	}

	private Pattern parameterPattern = Pattern.compile("\\{\\s*(\\w+)\\s*\\}");

	private JSONObject statement(Map<String, Object> params, String... lines)
			throws Exception {
		return new JSONObject()
				.put("statement", StringUtils.join(lines, " \n")).put(
						"parameters", params);
	}

	private JSONObject statement(JSONObject params, String... lines)
			throws Exception {
		return new JSONObject()
				.put("statement", StringUtils.join(lines, " \n")).put(
						"parameters", params);
	}

	private JSONObject statement(Object[] params, String... lines)
			throws Exception {
		String s = StringUtils.join(lines, " \n");
		JSONObject paras = new JSONObject();
		// regular expression for query parameters, like {xxx} or { xxx }:
		// \{\s*(\w+)\*\}

		Matcher m = parameterPattern.matcher(s);
		List<String> paraKeys = new ArrayList<String>();
		while (m.find()) {
			String paraKey = m.group(1);
			if (!paraKeys.contains(paraKey))
				paraKeys.add(paraKey);
		}
		for (int i = 0; i < paraKeys.size(); i++) {
			if (i >= params.length)
				throw new Exception("提供的查询形式参数数目多于实际参数");
			paras.put(paraKeys.get(i), params[i]);
		}
		return new JSONObject().put("statement", s).put("parameters", paras);
	}

	private void validateProps(NodeType nt, Map<String, Object> props)
			throws Exception {
		//props中必须包含必须的属性
		if(props.keySet().containsAll(nodePropNames.get(nt))) return;
		
		throw new Exception("缺少必要的节点属性！");
	}

	private void validateProps(RelationType rt, Map<String, Object> props)
			throws Exception {
		if (relPropNames.get(rt).equals(props.keySet())) return;
		//throw new Exception("缺少必要的关系属性！");
	}

	private String paramPlaceholders(Map<String, Object> props) {
		if(props.isEmpty())return "";
		return paramPlaceholders(props, "{", "", ":", "", ",", "}");
	}
	//用于set
	private String paramPlaceequal (Map<String, Object> props,String fieldPrefix) {
		return paramPlaceholders(props, "", fieldPrefix, "=", "", ",", "");
	}

	private String paramPlaceholders(Map<String, Object> props,
			String bracketBegin, String fieldPrefix, String assign,
			String paramPrefix, String joinBy, String bracketEnd) {
		List<String> propKeys = new ArrayList<String>();
		for (String k : props.keySet()) {
			propKeys.add(fieldPrefix + k + assign + "{" + paramPrefix + k + "}");
		}
		return bracketBegin + StringUtils.join(propKeys, joinBy) + bracketEnd;
	}

	public String paramSetClause(String nodeVar, Map<String, Object> props) {
		List<String> settings = new ArrayList<String>();
		for (String k : props.keySet()) {
			settings.add(nodeVar + "." + k + " = " + "{" + k + "}");
		}
		return StringUtils.join(settings, ",");
	}

	/**
	 * 向某仓库中创建单个节点的方法
	 * 
	 * @param ps_id
	 *            向哪个仓库添加节点，0意味着临时仓库（未归属具体仓库的东西），一下所有其他方法同此含义！
	 * @param nodeType
	 *            节点类型
	 * @param nodeProps
	 *            节点属性，创建不同类型节点时，给的属性不同，如： 
	 *            category: 
	 *            product:  
	 *            attribute:
	 * @throws Exception
	 */

	public void addNode(long ps_id, NodeType nodeType,
			Map<String, Object> nodeProps) throws Exception {
		// 校验属性是否齐全
		validateProps(nodeType, nodeProps);
		tx(ps_id,
				statement(nodeProps, "MERGE (n:" + nodeType
						+ paramPlaceholders(nodeProps) + " ) return n"));

	}

/**
 * 方法需要重写
 * @param ps_id
 * @param fromNodeType
 * @param toNodeType
 * @param fromNode
 * @param toNode
 * @param relProps
 * @throws Exception
 */
	public void addRelation(long ps_id, NodeType fromNodeType,
			NodeType toNodeType, Map<String, Object> fromNode,
			Map<String, Object> toNode, Map<String, Object> relProps)
			throws Exception {
		
		// 校验关系属性是否齐全
		RelationType relType = relNames.get(fromNodeType + "-" + toNodeType);
		if (!(fromNodeType == NodeType.Category && toNodeType == NodeType.Category)
				&& relType == null)
			throw new Exception("不存在此种关系：" + fromNodeType + "-" + toNodeType);

		if (!(fromNodeType == NodeType.Category && toNodeType == NodeType.Category))
			validateProps(relType, relProps);

		String stat = "";
		

		if (fromNodeType.equals(NodeType.Category)
				&& toNodeType.equals(NodeType.Product)) {
			StringBuilder createSetters = new StringBuilder();
			StringBuilder matchSetters  = new StringBuilder();
			int i = 0;
			for (String k: relProps.keySet()) {
				createSetters.append("r." + k + " = {" + k +"}");
				
				if(k.equals("quantity") || k.equals("locked_quantity")) 
					matchSetters.append( "r." + k + " = r." + k + " + {" + k +"}");
				else
					matchSetters.append("r." + k + " = {" + k +"}");
				
				i++;
				if(i<relProps.size()) {
					createSetters.append(", ");
					matchSetters.append(", ");
				}
			}
			// 如果是CP属性，MERGE时需要合并数量
			stat = String
					.format("MATCH (n_from:Container %s),(n_to:Product %s) "
							+ "MERGE (n_from)-[r:CONTAINS]->(n_to) "
							+ " ON CREATE SET " + createSetters
							+ " ON MATCH  SET " + matchSetters
							+ " RETURN r",
							paramPlaceholders(fromNode, "{", "", ":",
									"n_from_", ",", "}"),
							paramPlaceholders(toNode, "{", "", ":", "n_to_",
									",", "}"));
		} else {
			stat = String
					.format("MATCH (n_from:%s %s),(n_to:%s %s) MERGE (n_from)-[r:%s %s]->(n_to) RETURN r",
							fromNodeType,
							paramPlaceholders(fromNode, "{", "", ":",
									"n_from_", ",", "}"),
							toNodeType,
							paramPlaceholders(toNode, "{", "", ":", "n_to_",
									",", "}"), relType,
							paramPlaceholders(relProps));
		}
		Map<String, Object> params = new HashMap<String, Object>();
		for (Map.Entry<String, Object> e : fromNode.entrySet()) {
			params.put("n_from_" + e.getKey(), e.getValue());
		}
		for (Map.Entry<String, Object> e : toNode.entrySet()) {
			params.put("n_to_" + e.getKey(), e.getValue());
		}
		for (Map.Entry<String, Object> e : relProps.entrySet()) {
			params.put(e.getKey(), e.getValue());
		}

		JSONObject result = tx(ps_id, statement(params, stat));

		JSONArray data = result.getJSONArray("results").getJSONObject(0)
				.getJSONArray("data");

		if (data.length() == 0)
			throw new Exception(
					"Cannot Create Relation [try to make relation between nonexistent nodes?]");

	}
   /**
    * 方法需要重写
    * @param ps_id
    * @param con_id
    * @param statements
    * @throws Exception
    */
	public void removeTree(long ps_id, long con_id, JSONArray statements)
			throws Exception {
		

	}

	/**
	 * 删除一颗容器树，连同根容器及其所有下级容器全部删除
	 * @param ps_id
	 * @param con_id
	 * @throws Exception
	 */
	public void removeTree(long ps_id, long con_id) throws Exception {
		
	}


	/**
	 * 
	 * 方法需要重写
	 * 更新节点的方法，满足searchFor条件的所有节点会中nodeProps指出的那些属性会被替换新值
	 * 
	 * @param ps_id
	 * @param nodeType
	 * @param searchFor 
	 * 				作为查询条件，查询需要更新的节点的属性，注意：此中的keys必须是节点属性的子集，参见addNode方法注释（会校验的）
	 * 				注意：产品节点的catalogs属性是个多值类型（类似数组），但在此查询条件中不能是数组，而应该是一个Long（多值中某个值）！
	 * @param nodeProps
	 * 				更新的属性值，此中的keys必须是节点属性的子集，参见addNode方法注释（会校验的）
	 * @throws Exception
	 */
	public void updateNode(long ps_id, NodeType nodeType,
			Map<String, Object> searchFor, Map<String, Object> nodeProps)
			throws Exception {
		
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.putAll(nodeProps);
		for (Map.Entry<String, Object> e : searchFor.entrySet()) {
			params.put("search_" + e.getKey(), e.getValue());
		}
		String catalogsCondition = "";
		if (searchFor.remove("catalogs") != null) {
			catalogsCondition = "where {search_catalogs} in n.catalogs";
		}
		tx(ps_id,
				statement(
						params,
						"match (n:"
								+ nodeType
								+ paramPlaceholders(searchFor, "{", "", ":",
										"search_", ",", "}") + " ) ",
						catalogsCondition,
						"set " + paramSetClause("n", nodeProps), "return n"));

	}

	/**
	 * 搜索满足条件的某一类节点
	 * @param ps_id
	 * @param nodeType
	 * @param searchFor  参见全文
	 * @return
	 * @throws Exception
	 */
	public DBRow[] searchNodes(long ps_id, NodeType nodeType,
			Map<String, Object> searchFor) throws Exception {
		
		JSONObject params = new JSONObject(searchFor);

		String catalogsCondition = "";
		/*if (searchFor.remove("catalogs") != null) {
			catalogsCondition = "where {catalogs} in n.catalogs";
		}*/
		JSONArray data = query(ps_id, params, "match (n:" + nodeType
				+ paramPlaceholders(searchFor) + " ) ", catalogsCondition,
				"return n");

		DBRow[] result = new DBRow[data.length()];
		for (int i = 0; i < data.length(); i++) {
			JSONObject r = data.getJSONArray(i).getJSONObject(0)
					.getJSONObject("data");
			result[i] = DBRowUtils.convertToDBRow(r);
		}

		return result;
	}

	/**
	 * 批量添加同一类型多个节点
	 * @param ps_id
	 * @param nodeType
	 * @param nodeProps
	 * @throws Exception
	 */
	public void addNodes(long ps_id, NodeType nodeType, DBRow[] nodeProps)
			throws Exception {
		// 校验属性是否齐全
		JSONArray statements = new JSONArray();
		for (DBRow props : nodeProps) {
			Map<String, Object> params = DBRowUtils.dbRowAsMap(props);
			validateProps(nodeType, params);
			statements.put(statement(params, "MERGE (n:" + nodeType
					+ paramPlaceholders(params) + " ) return n"));
		}

		transaction(ps_id, statements);

	}


	/**
	 * 删除符合条件的关系
	 * @param ps_id
	 * @param fromNodeType  指定关系起始节点类型
	 * @param toNodeType	指定关系终止节点类型
	 * @param fromNodeProps 指定关系起始节点的查询条件（如不指定，则为空Map）
	 * @param toNodeProps	指定关系终止节点的查询条件（如不指定，则为空Map）
	 * @param relProps		指定关系上的查询条件（如不指定，则为空Map）
	 * @throws Exception
	 */
	public void removeRelations(long ps_id, NodeType fromNodeType,
			NodeType toNodeType, Map<String, Object> fromNodeProps,
			Map<String, Object> toNodeProps, Map<String, Object> relProps)
			throws Exception {
		
		// 校验关系属性是否齐全
		RelationType relType = relNames.get(fromNodeType + "-" + toNodeType);
		if (!(fromNodeType == NodeType.Category && toNodeType == NodeType.Category)
				&& relType == null)
			throw new Exception("不存在此种关系：" + fromNodeType + "-" + toNodeType);

		if (!(fromNodeType == NodeType.Category && toNodeType == NodeType.Category)
				&& !relPropNames.get(relType).containsAll(relProps.keySet()))
			throw new Exception("关系特征属性必须是关系所有属性的子集");

		Map<String, Object> params = new HashMap<String, Object>();
		for (Map.Entry<String, Object> e : fromNodeProps.entrySet()) {
			params.put("n_from_" + e.getKey(), e.getValue());
		}
		for (Map.Entry<String, Object> e : toNodeProps.entrySet()) {
			params.put("n_to_" + e.getKey(), e.getValue());
		}
		for (Map.Entry<String, Object> e : relProps.entrySet()) {
			params.put(e.getKey(), e.getValue());
		}
		String catalogsCondition = "";
		if (toNodeProps.remove("catalogs") != null) {
			catalogsCondition = "where {n_to_catalogs} in n_to.catalogs";
		}
		String stat = String
				.format("MATCH (n_from:%s %s)-[r:%s %s]->(n_to:%s %s)  %s delete r",
						fromNodeType,
						paramPlaceholders(fromNodeProps, "{", "", ":",
								"n_from_", ",", "}"),
						relType,
						paramPlaceholders(relProps),
						toNodeType,
						paramPlaceholders(toNodeProps, "{", "", ":", "n_to_",
								",", "}"), catalogsCondition);

		tx(ps_id, statement(params, stat));

	}

	
	/**
	 * 删除所有符合条件的节点
	 * 
	 * @param ps_id
	 * @param nodeType
	 * @param searchFor 节点搜索条件
	 * @param force 如果是true，则即使节点有关系线存在，也要删（自动先删关系线）；如果是false，则只删除符合条件节点中那些没有关系线的节点
	 * @return 删除的数量
	 * @throws Exception
	 */
	public int removeNodes(long ps_id, NodeType nodeType,
			Map<String, Object> searchFor, boolean force) throws Exception {
		
		JSONObject params = new JSONObject(searchFor);

		String catalogsCondition = "";
		/*if (searchFor.remove("catalogs") != null) {
			catalogsCondition = "and ({catalogs} in n.catalogs)";
		}*/
		JSONArray statements = new JSONArray();
		if (force) {
			// 删除符合条件并带有关系线的节点（关系线同时删除）
			statements.put(statement(params, "match (n:" + nodeType
					+ paramPlaceholders(searchFor)
					+ " )-[r]-(other) where true ", catalogsCondition,
					"delete r"));
		}
		// 删除孤立节点
		statements.put(statement(params, "match (n:" + nodeType
						+ paramPlaceholders(searchFor) + " ) where true ",
						catalogsCondition, "and not ( (n)-[]-() )", "delete n return count(n) as deletedCount"));
		
		JSONArray results = transaction(ps_id, statements).getJSONArray("results");
		
		int deletedCount = results.getJSONObject(results.length()-1)
							.getJSONArray("data")
							.getJSONObject(0)
							.getJSONArray("row")
							.getInt(0);
		return deletedCount;
	}
	
	/**
	 * 给特定类型，拥有特定属性的关系添加新属性
	 * @param ps_id	                 指定存放在哪个仓库
	 * @param relationType	     关系类型
	 * @param relationProps   用于过滤关系的属性条件
	 * @param otherProps  要添加的属性
	 */
	public void addProperties(long ps_id, RelationType relationType, Map<String, Object> relationProps,Map<String,Object> otherProps) throws Exception{
		if(otherProps == null || otherProps.isEmpty()){
			return;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		for (Map.Entry<String, Object> e : otherProps.entrySet()) {
			params.put("add_" + e.getKey(), e.getValue());
		}
		String rel = "";
		if(relationProps != null){
			rel = paramPlaceholders(relationProps, "{", "", ":", "search_", ",", "}");
			for (Map.Entry<String, Object> e : relationProps.entrySet()) {
				params.put("search_" + e.getKey(), e.getValue());
			}
		}
		
		String set_stat = paramPlaceholders(otherProps,"", "r.", "=", "add_", ", ", "");
		String stat = "match ()-[r:"+ relationType + rel + "]-() set " + set_stat;
		tx(ps_id, statement(params, stat));
	}

	/**
	 * 给特定类型的关系添加新属性
	 * @param ps_id	                 指定存放在哪个仓库
	 * @param relationType	     关系类型
	 * @param otherProps  要添加的属性
	 */
	public void addProperties(long ps_id, RelationType relationType,Map<String,Object> otherProps) throws Exception{
		this.addProperties(ps_id, relationType, null, otherProps);
	}

	/**
	 * 根据节点类型和特定条件，找到与之有特定关系的所有节点
	 * @param ps_id         要查找的仓库ID
	 * @param nodeType      节点类型
	 * @param searchFor     节点属性应满足的条件
	 * @param relationType  关系类型
	 * @param isStartFromNode  关系方向，是否 指向要查找的节点；
	 * @return 返回匹配的节点
	 */
	public DBRow[] searchRelationNodes(long ps_id, NodeType nodeType, Map<String, Object> searchFor,RelationType relationType, boolean isStartFromNode) throws Exception{
		
		String stat = null;
		if(isStartFromNode){
			stat = "match (:"+nodeType+ paramPlaceholders(searchFor) +")-[:"+ relationType +"]->(s) return distinct s";
		}else{
			stat = "match (:"+nodeType+ paramPlaceholders(searchFor) +")<-[:"+ relationType +"]-(s) return distinct s";
		}
		
		JSONArray statements = new JSONArray();
		statements.put(statement(searchFor, stat));
		JSONObject json = transaction(ps_id, statements);
		JSONArray data = json.getJSONArray("results").getJSONObject(0).getJSONArray("data");
		DBRow[] rows = new DBRow[data.length()];
		for(int i=0; i<data.length(); i++){
			JSONObject r = data.getJSONObject(i).getJSONArray("row").getJSONObject(0);
			rows[i] = DBRowUtils.convertToDBRow(r);
		}
		return rows;
	}

	/**
	 * 删除与满足特定条件的节点有特定关系的,并满足特定条件的所有节点
	 * @param ps_id     指定存放在哪个仓库
	 * @param nodeType  节点类型
	 * @param searchFor 指定检索条件
	 * @param relationType  指定关系
	 * @param isStartFromNode  关系方向，是否 指向要删除的节点；
	 * @param anotherNodeType   要删除的节点的节点类型
	 * @param another_search 要删除的节点属性需要具备的条件
	 */
	public void removeRelationNode(long ps_id, NodeType nodeType, Map<String, Object> searchFor,RelationType relationType, boolean isStartFromNode, NodeType anotherNodeType,Map<String, Object> another_search) throws Exception{
		
		Map<String, Object> params = new HashMap<String, Object>();
		for (Map.Entry<String, Object> e : searchFor.entrySet()) {
			params.put("search_" + e.getKey(), e.getValue());
		}
		for (Map.Entry<String, Object> e : another_search.entrySet()) {
			params.put("delete_" + e.getKey(), e.getValue());
		}
		String query = null;
		if(isStartFromNode){
			query = "MATCH (:%s %s)-[r:%s]->(n:%s %s)  delete r, n";
		}else{
			query = "MATCH (:%s %s)<-[r:%s]-(n:%s %s)  delete r, n";
		}
		query = String.format(query,
						nodeType,
						paramPlaceholders(searchFor, "{", "", ":", "search_", ",", "}"),
						relationType,
						anotherNodeType, 
						paramPlaceholders(another_search, "{", "", ":", "delete_", ",", "}"));
		tx(ps_id, statement(params, query));
	}
	
	/**
	 *  刷新路由表
	 */
	public String refreshRouter() throws Exception {
		TextResource resp = client.text(restBaseUrl + "/ps_id2rest_port/refresh");
		return resp.toString();
	}
	
}
