package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorReturnProductMgrZr
{

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public long addReturnProduct(DBRow data) throws Exception{
		try{
			long rp_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("return_product"));
			data.add("rp_id",rp_id);
			dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("return_product"), data);
			return rp_id;
		}catch (Exception e) {
			throw new Exception("FloorReturnProductMgrZr.addReturnProduct(DBRow):"+e); 
		}
	}
	public DBRow[] getReturnProductBypage(PageCtrl pc ) throws Exception {
		try{
			StringBuffer sql = new StringBuffer("select * from ").append(ConfigBean.getStringValue("return_product"));
			sql.append(" where (is_need_return is null or is_need_return = 1)");//只有需要退货才显示
			sql.append(" order by rp_id desc");
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			 
		}catch (Exception e) {
			throw new Exception("FloorReturnProductMgrZr.getReturnProductBypage(status,pc):"+e); 
		}
	}
	public void updateReturnProduct(DBRow row , long rp_id) throws Exception {
		try{
			dbUtilAutoTran.update(" where rp_id=" + rp_id, ConfigBean.getStringValue("return_product"), row);
		}catch (Exception e) {
			throw new Exception("FloorReturnProductMgrZr.updateReturnProduct(row):"+e); 
		}
	}
	public DBRow getRetrunProductByRpId(long rp_id) throws Exception {
		try{
			return dbUtilAutoTran.selectSingle("select * from " + ConfigBean.getStringValue("return_product")+" where rp_id =" + rp_id);
		}catch (Exception e) {
			throw new Exception("FloorReturnProductMgrZr.updateReturnProduct(row):"+e); 
		}
	}
	public DBRow[] filterReturnProductRows(String st , String end , long create_adid ,int return_product_source
			,int status ,int is_print_ove ,int return_product_type ,PageCtrl pc,int picture_recognition , int check_item_type,int gather_picture_status, int is_need_return) throws Exception {
		try{
			StringBuffer sb = new StringBuffer();
			sb.append("select * from ").append(ConfigBean.getStringValue("return_product")).append(" where 1=1 ");
			if(st != null && st.length() > 0 ){
				sb.append(" and create_date >='").append(st + " 00:00:00'");
			}
			if(end != null && end.length() > 0 ){
				sb.append(" and create_date <='").append(end + " 23:59:59'");
			}
			if(create_adid > 0l){
				sb.append(" and create_user =").append(create_adid);
			}
			if(return_product_source  > -1){
				sb.append(" and return_product_source =").append(return_product_source);
			}
			if(status > 0){
				sb.append(" and status =").append(status);
			}
			else if(status < 0)
			{
				sb.append(" and status !=").append(-status);
			}
			if(is_print_ove  > -1){
				sb.append(" and is_print_over =").append(is_print_ove);
			}
			if(return_product_type  > -1){
				sb.append(" and return_product_type =").append(return_product_type);
			}
			if(picture_recognition > -1){
				sb.append(" and picture_recognition =").append(picture_recognition);	
			}
			if(check_item_type > -1){
				sb.append(" and return_product_check =").append(check_item_type);	
			}
			if(gather_picture_status > -1){
				sb.append(" and gather_picture_status=").append(gather_picture_status);
			}
			if(is_need_return > 0)
			{
				sb.append(" and is_need_return = ").append(is_need_return);
			}
			//sb.append(" and (is_need_return is null or is_need_return = 1)");//只有需要退货才显示
			sb.append(" order by rp_id desc");
 			return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
 		}catch (Exception e) {
			throw new Exception("FloorReturnProductMgrZr.filterReturnProductRows():"+e); 
		}
	}
	public DBRow getReturnProductByIdAndIsCreateByServiceOrder(long rp_id) throws Exception{
		try{
			StringBuffer sql = new StringBuffer("select * from ").append(ConfigBean.getStringValue("return_product"));
			sql.append(" where rp_id=").append(rp_id).append(" and create_type=").append("2");
			return dbUtilAutoTran.selectSingle(sql.toString());
		}catch (Exception e) {
			throw new Exception("FloorReturnProductMgrZr.updateReturnProduct(row):"+e); 
		}
	}
}
