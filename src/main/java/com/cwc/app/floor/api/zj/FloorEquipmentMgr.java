package com.cwc.app.floor.api.zj;

import com.cwc.app.key.CheckInMainDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInTractorOrTrailerTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorEquipmentMgr {
	
	private DBUtilAutoTran dbUtilAutoTran;

	
	/**
	 * 添加设备
	 * @zhanjie
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addEquipment(DBRow row)
		throws Exception
	{
		return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("entry_equipment"), row);
	}
	
	/**
	 * 修改设备
	 * @zhanjie
	 * @param equipment_id
	 * @param para
	 * @throws Exception
	 */
	public void updateEquipment(long equipment_id,DBRow para)
		throws Exception
	{
		dbUtilAutoTran.update("where equipment_id = "+equipment_id,ConfigBean.getStringValue("entry_equipment"),para);
	}
	
	/**
	 * 删除设备
	 * 
	 * @param equipment_id
	 * @throws Exception
	 */
	public void delEquipment(long equipment_id)
		throws Exception
	{
		dbUtilAutoTran.delete("where equipment_id", ConfigBean.getStringValue("entry_equipment"));
	}
	
	/**
	 * 根据进来的entry_id 删除
	 * @param check_in_entry_id
	 * @throws Exception
	 */
	public void delateEquipmentByCheckInEntryId(long check_in_entry_id)
			throws Exception
	{
		this.dbUtilAutoTran.delete("where check_in_entry_id="+check_in_entry_id, ConfigBean.getStringValue("entry_equipment"));
	}
	
	/**
	 * 基于设备号查询未离开的设备
	 * @zhanjie
	 * @param equipment_number
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getEquipmentByEquipmentNumberNoLeft(String equipment_number)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("entry_equipment")+" where equipment_number = ? and equipment_status in ("+CheckInMainDocumentsStatusTypeKey.UNPROCESS+","+CheckInMainDocumentsStatusTypeKey.PROCESSING+","+CheckInMainDocumentsStatusTypeKey.INYARD+") order by check_in_time desc";
		
		DBRow para = new DBRow();
		para.add("equipment_number",equipment_number);
		
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	public DBRow getEquipmentByEquipmentId(long equipment_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("entry_equipment")+" where equipment_id = ? ";
		
		DBRow para = new DBRow();
		para.add("equipment_id",equipment_id);
		
		return dbUtilAutoTran.selectPreSingle(sql, para);
	}
	
	/**
	 * 根据设备号查询最近一次的设备
	 * @param equipment_number
	 * @return
	 * @throws Exception
	 */
	public DBRow getInYardEquipmentByEquipmentNumberLast(long ps_id, String equipment_number)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("entry_equipment")+" where equipment_number = ? and ps_id = ? and equipment_status in ("+CheckInMainDocumentsStatusTypeKey.UNPROCESS+","+CheckInMainDocumentsStatusTypeKey.PROCESSING+","+CheckInMainDocumentsStatusTypeKey.INYARD+","+CheckInMainDocumentsStatusTypeKey.LEAVING+") order by equipment_id desc limit 1";
		
		DBRow para = new DBRow();
		para.add("equipment_number",equipment_number);
		para.add("ps_id",ps_id);
		return dbUtilAutoTran.selectPreSingle(sql, para);
	}
	
	public DBRow getEntryTractor(long entry_id) throws Exception{
		String sql = "select * from " +ConfigBean.getStringValue("entry_equipment") +" where equipment_type ="+CheckInTractorOrTrailerTypeKey.TRACTOR+" and check_in_entry_id="+entry_id ;
		return dbUtilAutoTran.selectSingle(sql);
	}
	
	/**
	 * 基于设备号查询未离开的设备 车尾的设备
	 * @zwb
	 * @param equipment_number
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getEquipmentByEquipmentNumberByTrailer(String equipment_number)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("entry_equipment")+" where equipment_number = ? and equipment_type="+CheckInTractorOrTrailerTypeKey.TRAILER+" and equipment_status in ("+CheckInMainDocumentsStatusTypeKey.UNPROCESS+","+CheckInMainDocumentsStatusTypeKey.PROCESSING+","+CheckInMainDocumentsStatusTypeKey.INYARD+") order by check_in_time desc";
		
		DBRow para = new DBRow();
		para.add("equipment_number",equipment_number);
		
		return dbUtilAutoTran.selectPreMutliple(sql, para);
	}
	
	/**
	 * 根据 entry id 查询设备 zwb
	 * @param entry_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getEquipmentByEntryId(long entry_id) throws Exception{
		String sql = "select * from " +ConfigBean.getStringValue("entry_equipment") +" where check_in_entry_id="+entry_id ;
		return this.dbUtilAutoTran.selectMutliple(sql);
	}
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
