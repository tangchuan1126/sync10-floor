package com.cwc.app.floor.api.tjh;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FlooInvoiceTypeMgr {
	private DBUtilAutoTran dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	/**
	 * 获得所有发票类型
	 * 
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getDetailInvoiceType() throws Exception {
		try {
			String sql = "select * from "
					+ ConfigBean.getStringValue("invoice_Type");
			return (dbUtilAutoTran.selectMutliple(sql));
		} catch (Exception e) {
			throw new Exception("getDetailInvoiceType error:" + e);

		}
	}

	/**
	 * 添加发票模板信息
	 * 
	 * @param dbRow
	 * @throws Exception
	 */
	public void addInvoiceInfo(DBRow dbRow) throws Exception {
		try {
			dbUtilAutoTran.insert(
					ConfigBean.getStringValue("invoice_template"), dbRow);
		} catch (Exception e) {
			throw new Exception("addInvoiceInfo error:" + e);
		}
	}
/**
 * 根据仓库ID 查找该仓库下面的数据
 * @param ps_id 仓库ID
 * @return
 * @throws Exception
 */
	public DBRow[] getDetailInvoiceInfo(int ps_id) throws Exception {
		try {
			String sql = "select * from "
					+ ConfigBean.getStringValue("invoice_template")
					+ " where ps_id=" + ps_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("getDetailInvoiceInfo error:" + e);
		}
	}
	
	public DBRow getDetailInvoiceCompanyNameByDiId(int diid) throws Exception{
		try{
			String sql = "select CompanyName from "
				+ ConfigBean.getStringValue("deliverer_info")
				+ " where ps_id=" + diid;
		return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("getDetailInvoiceCompanyNameByDiId error:" + e);		}
	}
/*	public DBRow getDetailInvoiceTypeByConfigId(int configId){
	return null;	
	}*/
}
