package com.cwc.app.floor.api.zr;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorRefundMgrZr {

	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	public long addRefund(DBRow refundRow) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("refund");
			return dbUtilAutoTran.insertReturnId(tableName, refundRow);
		}catch (Exception e) {
			throw new Exception("FloorRefundMgrZr.addRefund(refundRow):"+e);
		}
	}
	public DBRow[] getServiceMangnerIds() throws Exception{
		try{
			StringBuffer sql =new StringBuffer( "select adid from " ).append( ConfigBean.getStringValue("admin") );
			sql.append(" where proJsId>=5").append(" and adgid=100006");
			
			return dbUtilAutoTran.selectMutliple(sql.toString()) ;
		}catch (Exception e) {
			throw new Exception("FloorRefundMgrZr.getServiceMangnerIds():"+e);
		}
	}
	public DBRow getRefundRowById(long refund_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("refund");
			String sql = "select * from " + tableName + " where refund_id="+ refund_id ;
			return dbUtilAutoTran.selectSingle(sql);
		}catch (Exception e) {
			throw new Exception("FloorRefundMgrZr.getRefundRowById():"+e);
		}
	}
	public void updateRefund(DBRow updateRow , long refund_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("refund");
			dbUtilAutoTran.update(" where refund_id="+ refund_id , tableName, updateRow);
		}catch (Exception e) {
			throw new Exception("FloorRefundMgrZr.updateRefund():"+e);
		}
	}
	public void addRefundReject(DBRow refundRejectRow ) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("refund_reject");
			dbUtilAutoTran.insertReturnId(tableName, refundRejectRow);
		}catch (Exception e) {
			throw new Exception("FloorRefundMgrZr.addRefundReject():"+e);
		}
	}
	public DBRow[] getRefundRejuectByRefundId(long refund_id) throws Exception {
		try{
			String tableName = ConfigBean.getStringValue("refund_reject");
			String sql =  "select * from "  + tableName + " where refund_id="+refund_id ;
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e) {
			throw new Exception("FloorRefundMgrZr.getRefundRejuectByRefundId(refund_id):"+e);
		}
		
	}
	
	
}
