package com.cwc.app.floor.api;

import java.util.ArrayList;
import java.util.List;

import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.sbb.FloorAccountMgrSbb;
import com.cwc.app.key.LogServModelKey;
import com.cwc.app.key.ProductDeviceTypeKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

/**
 * 记录数据库表的更新内容
 * 
 * 
 * @author subin
 */
public class FloorSetupLogMgr {
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	private FloorLogMgr logMgr;
	
	private FloorAccountMgrSbb floorAccountMgr;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	public void setLogMgr(FloorLogMgr logMgr) {
		this.logMgr = logMgr;
	}

	public void setFloorAccountMgr(FloorAccountMgrSbb floorAccountMgr) {
		
		this.floorAccountMgr = floorAccountMgr;
	}
	
	public DBRow getRowForTableById(DBRow row) throws Exception{
		
		try {
			
			String sql = "select * from "+row.get("table", "")+" where "+row.get("column", "")+" = "+row.get("key", "");
			
			return dbUtilAutoTran.selectSingle(sql);
			
		} catch (Exception e) {
			
			throw new Exception(e);
		}
	}
	
	/**
	 * 比较数据差异 
	 * 
	 */
	public DBRow[] compareContent(DBRow row) throws Exception{
		
		DBRow[] result = null;
		
		if(row.get("event","").equals("create")){
			
			if(row.get("content") != null && row.get("content") instanceof DBRow[]){
				
				result = (DBRow[])row.get("content");
			}
			
		} else {
			
			DBRow tabRow = new DBRow();
			tabRow.put("table", row.get("table",""));
			tabRow.put("column", row.get("column",""));
			tabRow.put("key", row.get("pri_key"));
			
			DBRow content = this.getRowForTableById(tabRow);
			
			List<DBRow> list = new ArrayList<DBRow>();
			
			if(content == null ){
				
				result = (DBRow[])row.get("content");
				
			} else {
				
				for(DBRow one : (DBRow[])row.get("content")){
					
					if(!content.get(one.get("column",""),"").equals(one.get("col_key"))){
						
						list.add(one);
					}
				}
				
				result = new DBRow[list.size()];
				list.toArray(result);
			}
		}
		
		return result;
	}
	
	/**
	 * 数据验证 
	 * 
	 */
	public DBRow validateContent(DBRow row) throws Exception{
		
		DBRow result = new DBRow();
		
		boolean flag = true;
		String error = "";
		
		//空值验证
		if(flag && row.get("event", "").equals("")){
			
			flag = false;
			error = "event is null";
		}
		
		if(flag && row.get("table", "").equals("")){
			
			flag = false;
			error = "table is null";
		}
		
		if(flag && row.get("pri_key", "").equals("")){
			
			flag = false;
			error = "primary key is null";
		}
		
		if(flag && row.get("create_by", 0) == 0){
			
			flag = false;
			error = "create by is null";
		}
		
		//事件验证
		if(flag){
			
			switch(row.get("event","")){
			
				case "create": break;
				case "update": break;
				case "delete": break;
				default:
					flag = false;
					error = "event not exists";
			}
		}
		
		result.put("flag", flag);
		result.put("error", error);
		
		return result;
	}
	
	//重构添加日志方法
	public void addSetupLog (
			String db
			, String event
			, String table
			, String column
			, long pri_key
			, String ref_table
			, String ref_column
			, long ref_pri_key
			, long create_by
			, int device
			, String describe
			, DBRow[] content
		) throws Exception{
		
		DBRow logRow = new DBRow();
		logRow.put("db", db);
		logRow.put("event", event);
		logRow.put("table", table);
		logRow.put("column", column);
		logRow.put("pri_key", pri_key);
		logRow.put("ref_table", ref_table);
		logRow.put("ref_column", ref_column);
		logRow.put("ref_pri_key", ref_pri_key);
		logRow.put("create_by", create_by);
		logRow.put("device", device);
		logRow.put("describe", describe);
		logRow.put("content", content);
		
		this.addSetupLog(logRow);
	}
	
	/**
	 * 添加日志 
	 * 
	 */
	public DBRow addSetupLog(DBRow row) throws Exception{
		
		try {
			
			String collection = LogServModelKey.BASIC_DATA_SETUP;
			
			DBRow result = this.validateContent(row);
			
			if((Boolean)result.get("flag")){
				
				//差分比较
				DBRow[] content = this.compareContent(row);
				
				//参数处理
				DBRow account = floorAccountMgr.getAccountById(row.get("create_by",0L));
				
				DBRow logRow = new DBRow();
				logRow.put("event", row.get("event",""));
				logRow.put("table", row.get("table",""));
				logRow.put("pri_key", row.get("pri_key",0));
				logRow.put("ref_table", row.get("ref_table",""));
				logRow.put("ref_pri_key", row.get("ref_pri_key",0));
				logRow.put("content", content);
				
				DBRow creRow = new DBRow();
				creRow.put("id", row.get("create_by",0L));
				creRow.put("name", account.get("account",""));
				
				logRow.put("create_by", creRow);
				
				//设备类型//默认web
				ProductDeviceTypeKey deviceKey = new ProductDeviceTypeKey();
				if(deviceKey.getProductTypeKeyValue(row.get("device",1)).equals("")){
					
					row.put("device", 1);
				}
				
				DBRow devRow = new DBRow();
				devRow.put("id", row.get("device",1));
				devRow.put("name", deviceKey.getProductTypeKeyValue(row.get("device",1)));
				
				logRow.put("device", devRow);
				logRow.put("describe", row.get("describe",""));
				
				//数据转换
				JSONObject json = new JSONObject(DBRowUtils.multipleDBRowAsMap(logRow));
				
				//插入数据
				String id = logMgr.addLog(collection, json, false);
				
				result.put("id", id);
			}
			
			return result;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
	}
}
