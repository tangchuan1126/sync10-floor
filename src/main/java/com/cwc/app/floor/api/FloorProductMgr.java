package com.cwc.app.floor.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;
import us.monoid.web.Resty;

import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.floor.api.FloorProductCategoryMgr.NodeType;
import com.cwc.app.floor.api.FloorProductCategoryMgr.RelationType;
import com.cwc.app.floor.api.zyj.model.Title;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.CountOperationKey;
import com.cwc.app.key.ProductStatusKey;
import com.cwc.app.key.ProductStoreOperationKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.Neo4jTransaction;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorProductMgr
{
	private DBUtilAutoTran dbUtilAutoTran;
	//图数据库访问URL
	private String restBaseUrl = "";
    private Resty client;
    //图数据库仓库ID
    private long psId;
	
	public FloorProductMgr(){
		this.client = new Resty(new Resty.Option[] { Resty.Option.timeout(1000000) });
	}
	
	public long addProductStorage(DBRow row)
		throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("product_storage"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addProductStorage(row) error:" + e);
		}
	}

	public void modProductStorage(long pid,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where pid=" + pid,ConfigBean.getStringValue("product_storage"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.modProductStorage(row) error:" + e);
		}
	}

//	public void incProductStorageField(long pid,String field,double value)
//		throws Exception
//	{
//		try
//		{
//			dbUtilAutoTran.increaseFieldVal(ConfigBean.getStringValue("product_storage"),"where pid="+pid,field,value);
//		}
//		catch (Exception e)
//		{
//			throw new Exception("FloorProductMgr.incProductStorageField(row) error:" + e);
//		}
//	}


//	public void decreaseProductStorageField(long pid,String field,double value)
//		throws Exception
//	{
//		try
//		{
//			dbUtilAutoTran.decreaseFieldVal(ConfigBean.getStringValue("product_storage"),"where pid="+pid,field,value);
//		}
//		catch (Exception e)
//		{
//			throw new Exception("FloorProductMgr.decreaseProductStorageField(row) error:" + e);
//		}
//	}
	
	public DBRow[] getProductStorageInCids(String[] catalogids,String[] psids,int type,PageCtrl pc)
		throws Exception
	{
		try
		{
			String psid = "";
			String catalogid = "";
			//商品分类
			if (catalogids==null||catalogids.length==0)
			{
				return( new DBRow[0] );
			}
			int i=0;
			for (; i<catalogids.length-1; i++)
			{
				catalogid += catalogids[i]+",";
			}
			catalogid += catalogids[i];
			//仓库分类
			if (psids==null||psids.length==0)
			{
				return( new DBRow[0] );
			}
			i=0;
			for (; i<psids.length-1; i++)
			{
				psid += psids[i]+",";
			}
			psid += psids[i];
			
			
			String sql = "select * from " + ConfigBean.getStringValue("product_storage") + " p," + ConfigBean.getStringValue("product_storage_catalog") + " c,"+ConfigBean.getStringValue("product")+" pc where pc.pc_id=p.pc_id and p.cid=c.id and cid in ("+psid+") and pc.catalog_id in ("+catalogid+") and pc.orignal_pc_id=0 order by pid desc";
			
			if (type==1)
			{
				sql = "select * from " + ConfigBean.getStringValue("product_storage") + " p," + ConfigBean.getStringValue("product_storage_catalog") + " c,"+ConfigBean.getStringValue("product")+" pc where pc.pc_id=p.pc_id and p.cid=c.id and cid in ("+psid+")  and pc.catalog_id in ("+catalogid+") and pc.orignal_pc_id=0  and store_count<store_count_alert order by pid desc";
			}
			else if (type==2)
			{
				sql = "select * from " + ConfigBean.getStringValue("product_storage") + " p," + ConfigBean.getStringValue("product_storage_catalog") + " c,"+ConfigBean.getStringValue("product")+" pc where pc.pc_id=p.pc_id and p.cid=c.id and cid in ("+psid+")  and pc.catalog_id in ("+catalogid+") and pc.orignal_pc_id=0 and store_count>store_count_alert order by pid desc";
			}
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectMutliple(sql,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectMutliple(sql));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductStorageInCids(row) error:" + e);
		}
	}

//	public long addOutProduct(DBRow row)
//		throws Exception
//	{
//		try
//		{
//			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("out_product"));
//			row.add("op_id",id);
//			dbUtilAutoTran.insert(ConfigBean.getStringValue("out_product"),row);
//			return(id);
//		}
//		catch (Exception e)
//		{
//			throw new Exception("FloorProductMgr.addOutProduct(row) error:" + e);
//		}
//	}
	
	public long addShipProduct(DBRow row)
		throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("ship_product"));
			row.add("sp_id",id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("ship_product"),row);
			return(id);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addShipProduct(row) error:" + e);
		}
	}
		
	
	public DBRow[] getShipProductsByStEnPc(long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select sc_id,delivery_code,delivery_operator,product_weight,deliveryto,name,title,quantity,ip.post_date post_date,account from " + ConfigBean.getStringValue("product_storage_catalog") + " c," + ConfigBean.getStringValue("ship_product") + " ip LEFT JOIN " + ConfigBean.getStringValue("admin") + " ad ON ip.adid=ad.adid where ip.cid=c.id  and datemark>=? and datemark<=? order by sp_id desc";
			DBRow para = new DBRow();
			para.add("st_datemark",st_datemark);
			para.add("en_datemark",en_datemark);
			
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getShipProductsByStEnPc(row) error:" + e);
		}
	}


	public DBRow[] getShipProductsByStEnPcCid(long cid,long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select sc_id,delivery_code,delivery_operator,product_weight,deliveryto,name,title,quantity,ip.post_date post_date,account from " + ConfigBean.getStringValue("product_storage_catalog") + " c," + ConfigBean.getStringValue("ship_product") + " ip LEFT JOIN " + ConfigBean.getStringValue("admin") + " ad ON ip.adid=ad.adid  where ip.cid=c.id and ip.cid=? and datemark>=? and datemark<=? order by sp_id desc";
			DBRow para = new DBRow();
			para.add("cid",cid);
			para.add("st_datemark",st_datemark);
			para.add("en_datemark",en_datemark);
			
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getShipProductsByStEnPcCid(row) error:" + e);
		}
	}
	
	public DBRow[] getShipProductsByStEnPcCidScid(long cid,long scid,long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			String cond = "";

			DBRow para = new DBRow();
			para.add("st_datemark",st_datemark);
			para.add("en_datemark",en_datemark);
			
			if (cid>0)
			{
				cond += " and ip.cid=? ";
				para.add("cid",cid);
			}
			
			if (scid>0)
			{
				cond += " and ip.sc_id=? ";
				para.add("scid",scid);
			}
			
			String sql = "select sc_id,delivery_code,delivery_operator,product_weight,deliveryto,name,title,quantity,ip.post_date post_date,account from " + ConfigBean.getStringValue("product_storage_catalog") + " c," + ConfigBean.getStringValue("ship_product") + " ip LEFT JOIN " + ConfigBean.getStringValue("admin") + " ad ON ip.adid=ad.adid  where ip.cid=c.id and datemark>=? and datemark<=? "+cond+" order by sp_id desc";

			//System.out.println(sql);
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getShipProductsByStEnPcCidScid(row) error:" + e);
		}
	}

	
	public DBRow[] getAmountShipProductsByStEnPcCid(long cid,long st_datemark,long en_datemark,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select sc_id,delivery_code,delivery_operator,product_weight,deliveryto,name,title,sum(quantity) quantity,ip.post_date post_date,account from " + ConfigBean.getStringValue("product_storage_catalog") + " c," + ConfigBean.getStringValue("ship_product") + " ip LEFT JOIN " + ConfigBean.getStringValue("admin") + " ad ON ip.adid=ad.adid where ip.cid=c.id  and ip.cid=? and datemark>=? and datemark<=? group by name order by sp_id desc";
			DBRow para = new DBRow();
			para.add("cid",cid);
			para.add("st_datemark",st_datemark);
			para.add("en_datemark",en_datemark);
			
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getAmountShipProductsByStEnPcCid(row) error:" + e);
		}
	}
		
	
	public DBRow[] getStatShipProductsByDatePcid(long st_datemark,long en_datemark,long pcid,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql;
			
			if (st_datemark==0&&en_datemark==0&&pcid==0)	//查询当前日志中所有
			{
				sql = "SELECT op_id,name,cid,sum(quantity) quantity FROM " + ConfigBean.getStringValue("ship_product") + " where stat_flag=0 group by name";
				
				return(dbUtilAutoTran.selectMutliple(sql));
			}
			else
			{
				sql = "SELECT op_id,name,title,sum(quantity) quantity FROM " + ConfigBean.getStringValue("ship_product") + " ip," + ConfigBean.getStringValue("product_storage_catalog") + " pc  where stat_flag=0 and ip.cid=pc.id and datemark>=? and datemark<=? and cid=? group by name";
	
				DBRow para = new DBRow();
				para.add("st_datemark",st_datemark);
				para.add("en_datemark",en_datemark);
				para.add("pcid",pcid);
				
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
	
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStatShipProductsByDatePcid(row) error:" + e);
		}
	}
			
	public DBRow[] getShipProductsFromSearchName(String name,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select sc_id,delivery_code,delivery_operator,product_weight,deliveryto,name,title,sum(quantity) quantity,ip.post_date post_date,account from " + ConfigBean.getStringValue("product_storage_catalog") + " c," + ConfigBean.getStringValue("ship_product") + " ip LEFT JOIN " + ConfigBean.getStringValue("admin") + " ad ON ip.adid=ad.adid where ip.cid=c.id  and (name like '%"+name+"%' or delivery_code like '%"+name+"%' ) group by name order by sp_id desc";
			
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getShipProductsFromSearchName(row) error:" + e);
		}
	}
	
	public long addProduct(DBRow row)
		throws Exception
	{
		try
		{
			
//			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("product"));
//			row.add("pc_id",id);
//			dbUtilAutoTran.insert(ConfigBean.getStringValue("product"),row);
			//lujintao 修改 2015-08-05 添加的时候，将产品保存到图数据库，并且保存main_code属性
			String mainCode = row.get("main_code", "");
			row.remove("main_code");
			long id = dbUtilAutoTran.insertReturnId("product", row);

			DBRow another_row = new DBRow();
			another_row.putAll(row);
			another_row.put("id",id);
			another_row.put("main_cde", mainCode);
			this.addNode(psId, NodeType.Product, this.convertToLower(another_row));
			return id;
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addProduct(row) error:" + e);
		}
	}
	
	public DBRow getDetailProductByPcid(long pcid)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("pcid",pcid);
			
			//定制商品无商品条码，不能强制管理条码
			String checkSql = " select * from "+ConfigBean.getStringValue("product") +" where pc_id = ?";
			DBRow row = dbUtilAutoTran.selectPreSingle(checkSql, para);
			
			if(row ==null)
			{
				return row;
			}
			
			String mastJoin = " join ";
			if(row.get("orignal_pc_id",0l)>0)
			{
				mastJoin = " left join ";
			}
			
			String sql = "select p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,p.pc_id,pcode_main.p_code as p_code," +
					"pcode_amazon.p_code as p_code2,pcode_upc.p_code as upc,p.p_name,p.union_flag,p.unit_name,p.unit_price" +
					",p.volume,p.weight,p.width,p.length_uom,p.weight_uom,p.price_uom from product as p " 
						+mastJoin+"product_code as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type ="+CodeTypeKey.Main+" "
						+"left join product_code as pcode_amazon on p.pc_id = pcode_amazon.pc_id and pcode_amazon.code_type ="+CodeTypeKey.Amazon+" "
						+"left join product_code as pcode_upc on p.pc_id = pcode_upc.pc_id and pcode_upc.code_type ="+CodeTypeKey.UPC+" "
						+"where p.pc_id=?";
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
//			DBRow rowo = dbUtilAutoTran.selectPreSingleCache(sql,para,new String[]{ConfigBean.getStringValue("product")});
			return(rowo);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProductByPcid error:" + e);
		}
	}
	

	public DBRow getDetailProductInCids(long pc_id,String cids[])
		throws Exception
	{
		try
		{
			String cidStr = "";
			int i=0;
			for (; i<cids.length-1; i++)
			{
				cidStr += cids[i]+",";
			}
			cidStr += cids[i];

			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			

			String sql = "select * from " + ConfigBean.getStringValue("product") + " where pc_id=? and catalog_id in ("+cidStr+") and orignal_pc_id=0";
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			return(rowo);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProductInCids error:" + e);
		}
	}
	
	public DBRow[] getProductsByPcids(String pc_ids[],PageCtrl pc)
		throws Exception
	{
		try
		{
			if (pc_ids.length>0)
			{
				String pcidStr = "";
				int i=0;
				for (; i<pc_ids.length-1; i++)
				{
					pcidStr += pc_ids[i]+",";
				}
				pcidStr += pc_ids[i];
				
				String sql = "select * from " + ConfigBean.getStringValue("product") + " where pc_id in ("+pcidStr+") order by pc_id desc";
				
				if ( pc!=null )
				{
					return(dbUtilAutoTran.selectMutliple(sql,pc));
				}
				else
				{
					return(dbUtilAutoTran.selectMutliple(sql));
				}
			}
			else
			{
				return(new DBRow[0]);
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductsByPcids(row) error:" + e);
		}
	}

	public DBRow getDetailProductByPname(String pname)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("pname",pname);
			
			String sql = "select p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,p.pc_id,pcode_main.p_code as p_code,pcode_upc.p_code as p_code_upc" +
					",p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width,p.length_uom,p.weight_uom,p.price_uom,p.freight_class,p.nmfc_code,p.sku from "+ConfigBean.getStringValue("product")+" as p " 
						+"join "+ConfigBean.getStringValue("product_code")+" as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type ="+CodeTypeKey.Main+" "
						+"left join "+ConfigBean.getStringValue("product_code")+" as pcode_upc on p.pc_id = pcode_upc.pc_id and pcode_upc.code_type ="+CodeTypeKey.UPC+" "
						+ " where p.p_name=?  and orignal_pc_id = 0";
			
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			return(rowo);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProductByPname error:" + e);
		}
	}
	
	public DBRow isProductExistByPname(String pname)
			throws Exception
		{
			try
			{
				
				DBRow para = new DBRow();
				para.add("pname",pname);
				
				String sql = "select count(pc_id) as total_products from "+ConfigBean.getStringValue("product")+" as p " 
							
							+ " where p.p_name=?  and orignal_pc_id = 0 and p.alive=1";
				
				DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
				return(rowo);
			}
			catch (Exception e)
			{
				throw new Exception("FloorProductMgr.getDetailProductByPname error:" + e);
			}
		}
	
	public DBRow[] getDetailProductByPcode(String pcode)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("pcode",pcode);
			
			String sql = "select p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,p.pc_id,pcode_main.p_code as p_code,p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width from product as p " 
						+"join product_code as pcode on p.pc_id = pcode.pc_id and pcode.p_code = '"+pcode+"'"
						+"join product_code as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type ="+CodeTypeKey.Main;
			
//			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			DBRow[] rowo = dbUtilAutoTran.selectMutliple(sql);
			return(rowo);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProductByPcode error:" + e);
		}
	}
	
	public DBRow getProductCode(String pcode) throws Exception {
		try {
			
			DBRow para = new DBRow();
			para.add("pcode",pcode);
			
			String sql = " select * from product_code WHERE code_type = "+CodeTypeKey.Main+" and p_code = ? ";
			 
			return dbUtilAutoTran.selectPreSingle(sql,para);
			
		} catch (Exception e) {
			throw new Exception("FloorProductMgr.getProductCode error:" + e);
		}
	}

	
	public DBRow[] getAllProductsNoChinese()
		throws Exception
	{
		String sql = "select * from product where LENGTH(p_name)=CHARACTER_LENGTH(p_name) and orignal_pc_id =0";
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	public DBRow[] getAllProducts(PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select p.alive,p.catalog_id,p.heigth,p.length,p.orignal_pc_id,p.pc_id,pcode_main.p_code as p_code,pcode_amazon.p_code as p_code2,pcode_upc.p_code as upc,p.p_name,p.union_flag,p.unit_name,p.unit_price,p.volume,p.weight,p.width from product as p " 
			+"join product_code as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type ="+CodeTypeKey.Main+" "
			+"left join product_code as pcode_amazon on p.pc_id = pcode_amazon.pc_id and pcode_amazon.code_type ="+CodeTypeKey.Amazon+" "
			+"left join product_code as pcode_upc on p.pc_id = pcode_upc.pc_id and pcode_upc.code_type ="+CodeTypeKey.UPC+" "
			+"where orignal_pc_id = 0 order by catalog_id desc,p_name asc";
			
			if ( pc!=null )
			{
				return (dbUtilAutoTran.selectMutliple(sql, pc));
				//return(dbUtilAutoTran.selectMutlipleCache(sql,pc,new String[]{ConfigBean.getStringValue("product")}));
			}
			else
			{
				return (dbUtilAutoTran.selectMutliple(sql));
//				return(dbUtilAutoTran.selectMutlipleCache(sql,new String[]{ConfigBean.getStringValue("product")}));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getAllProducts(row) error:" + e);
		}
	}
	
	public DBRow[] getAllProductsOld(PageCtrl pc)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("product");
		if ( pc!=null )
		{
			return (dbUtilAutoTran.selectMutliple(sql, pc));
		}
		else
		{
			return (dbUtilAutoTran.selectMutliple(sql));
		}
	}

	public void modProduct(long pcid,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where pc_id=" + pcid,ConfigBean.getStringValue("product"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.modProduct(row) error:" + e);
		}
	}
	
	public void modProductCatalog(long pc_id,long catalog_id)
		throws Exception
	{
		DBRow para = new DBRow();
		para.add("catalog_id",catalog_id);
		
		dbUtilAutoTran.update("where pc_id="+pc_id+" or orignal_pc_id ="+pc_id,ConfigBean.getStringValue("product"),para);//修改商品本身
		
		dbUtilAutoTran.update("where pid="+pc_id,ConfigBean.getStringValue("porder_item"),para);//修改订单明细内的商品分类
		
		dbUtilAutoTran.update("where pid="+pc_id,ConfigBean.getStringValue("quote_order_item"),para);//修改报价单明细内商品分类
		
		dbUtilAutoTran.update("where product_id="+pc_id,ConfigBean.getStringValue("actual_storage_sales"),para);
		
		dbUtilAutoTran.update("where product_id="+pc_id,ConfigBean.getStringValue("theory_storage_sales"),para);
		
		dbUtilAutoTran.update("where product_id="+pc_id,ConfigBean.getStringValue("original_storage_sales"),para);
		
		dbUtilAutoTran.update("where product_id="+pc_id,ConfigBean.getStringValue("plan_storage_sales"),para);
		
	}

	public DBRow getDetailProductStorageByRscIdPcid(long root_storage_catalog_id,long pcid)
		throws Exception
	{
		try
		{
			//获得跟仓库分类下所有子分类
//			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
//			ArrayList al = tree.getAllSonNode(root_storage_catalog_id);
//			al.add(root_storage_catalog_id);
//
			String cid = String.valueOf(root_storage_catalog_id);
//			int i=0;
//
//			for (; i<al.size()-1; i++)
//			{
//				cid += al.get(i)+",";
//			}
//			cid += al.get(i);

			DBRow para = new DBRow();
			para.add("pcid",pcid);
			
			//一个跟仓库下，某个商品只允许存放唯一一个
			String sql = "select * from " + ConfigBean.getStringValue("product_storage") + " where pc_id=? and cid in ("+cid+")";
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			return(rowo);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProductStorageByRscIdPcid error:" + e);
		}
	}

	public DBRow[] getProductInCids(String[] cids,int union_flag,PageCtrl pc)
		throws Exception
	{
		try
		{
			String cid = "";
			int i=0;
			String sql;
			String unionFlagCond = "";
			
			DBRow para = new DBRow();
			para.add("orignal_pc_id",0);
			
			if (union_flag>0)
			{
				unionFlagCond = "and union_flag=?";
				para.add("union_flag",union_flag);
			}
			
			//选择了一个分类
			if (cids!=null)
			{
				for (; i<cids.length-1; i++)
				{
					cid += cids[i]+",";
				}
				cid += cids[i];
				
				sql = "select * from "+ConfigBean.getStringValue("product")+" pc where catalog_id in ("+cid+") and orignal_pc_id=?" + unionFlagCond + " order by catalog_id desc,p_name asc";
			}
			else
			{
				sql = "select * from "+ConfigBean.getStringValue("product")+" pc where orignal_pc_id=? " + unionFlagCond + " order by catalog_id desc, p_name asc";
			}
			
			//System.out.println(sql);
			//System.out.println("----------------------------------");
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
			
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductInCids(row) error:" + e);
		}
	}
	
	
	public DBRow[] getProductStorages(long ps_id,int type,PageCtrl pc)
		throws Exception
	{
		try
		{
			//获得跟仓库分类下所有子分类
//			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
//			ArrayList al = tree.getAllSonNode(ps_id);
//			al.add(ps_id);
//
			String cid = String.valueOf(ps_id);
//			int i=0;
//
//			for (; i<al.size()-1; i++)
//			{
//				cid += al.get(i)+",";
//			}
//			cid += al.get(i);
			
			
			String sql = "select * from " + ConfigBean.getStringValue("product_storage") + " p," + ConfigBean.getStringValue("product_storage_catalog") + " c," + ConfigBean.getStringValue("product") + " pc where pc.pc_id=p.pc_id and p.cid=c.id and p.cid in ("+cid+") order by pc.p_name asc";
			
			if (type>0)
			{
				sql = "select * from " + ConfigBean.getStringValue("product_storage") + " p," + ConfigBean.getStringValue("product_storage_catalog") + " c," + ConfigBean.getStringValue("product") + " pc where pc.pc_id=p.pc_id and p.cid=c.id and store_count<0 and p.cid in ("+cid+") order by pc.p_name asc";
			}
			
			DBRow rows[];
			
			if (pc==null)
			{
				rows = dbUtilAutoTran.selectMutliple(sql);
//				rows = dbUtilAutoTran.selectMutlipleCache(sql,new String[]{ConfigBean.getStringValue("product_storage"),ConfigBean.getStringValue("product_storage_catalog"),ConfigBean.getStringValue("product")});
			}
			else
			{
				rows = dbUtilAutoTran.selectMutliple(sql, pc);
//				rows = dbUtilAutoTran.selectMutlipleCache(sql,pc,new String[]{ConfigBean.getStringValue("product_storage"),ConfigBean.getStringValue("product_storage_catalog"),ConfigBean.getStringValue("product")});
			}
			
			return(rows);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductStorages(row) error:" + e);
		}
	}

	public DBRow[] getNewProductByCid(long cid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("cid",cid);
			String sql = "select * from " + ConfigBean.getStringValue("product_storage") + " p," + ConfigBean.getStringValue("product_storage_catalog") + " c where p.cid=c.id  and cid=? order by pid desc";
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
//				return(dbUtilAutoTran.selectPreMutlipleCache(sql,para,pc,new String[]{ConfigBean.getStringValue("product_storage"),ConfigBean.getStringValue("product_storage_catalog")}));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
//				return(dbUtilAutoTran.selectPreMutlipleCache(sql,para,new String[]{ConfigBean.getStringValue("product_storage"),ConfigBean.getStringValue("product_storage_catalog")}));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getNewProductByCid(row) error:" + e);
		}
	}

	public DBRow[] getProductByCid(long catalog_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("catalog_id",catalog_id);
			String sql = "select * from " + ConfigBean.getStringValue("product") + " p," + ConfigBean.getStringValue("product_catalog") + " c where p.catalog_id=c.id  and catalog_id=? and p.orignal_pc_id=0 order by pc_id desc";
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductByCid(row) error:" + e);
		}
	}

	public void delProduct(long pcid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("pcid", pcid);
			//dbUtilAutoTran.delete("where pc_id=" + pcid,ConfigBean.getStringValue("product"));
			dbUtilAutoTran.deletePre("where pc_id=?", para, ConfigBean.getStringValue("product"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.delProduct(row) error:" + e);
		}
	}
	
	public DBRow getDetailProductProductStorageByPcid(long root_storage_catalog_id,long pcid)
		throws Exception
	{
		try
		{
			//获得跟仓库分类下所有子分类
//			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
//			ArrayList al = tree.getAllSonNode(root_storage_catalog_id);
//			al.add(root_storage_catalog_id);
//
			String cid = String.valueOf(root_storage_catalog_id);
//			int i=0;
//
//			for (; i<al.size()-1; i++)
//			{
//				cid += al.get(i)+",";
//			}
//			cid += al.get(i);
			
			DBRow para = new DBRow();
			para.add("pcid",pcid);
			String sql = "select * from " + ConfigBean.getStringValue("product") + " pc," + ConfigBean.getStringValue("product_storage") + " p where pc.pc_id=p.pc_id  and pc.pc_id=? and cid in ("+cid+") ";
			
			return(dbUtilAutoTran.selectPreSingle(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProductProductStorageByPcid(row) error:" + e);
		}
	}
	
	public DBRow getDetailProductProductStorageByPName(long root_storage_catalog_id,String p_name)
		throws Exception
	{
		try
		{
			//获得跟仓库分类下所有子分类
//			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
//			ArrayList al = tree.getAllSonNode(root_storage_catalog_id);
//			al.add(root_storage_catalog_id);
//	
			String cid = String.valueOf(root_storage_catalog_id);
//			int i=0;
//	
//			for (; i<al.size()-1; i++)
//			{
//				cid += al.get(i)+",";
//			}
//			cid += al.get(i);
			
			DBRow para = new DBRow();
			para.add("p_name",p_name);
			String sql = "select * from " + ConfigBean.getStringValue("product") + " pc," + ConfigBean.getStringValue("product_storage") + " p where pc.pc_id=p.pc_id  and pc.p_name=? and cid in ("+cid+") ";
			
			return(dbUtilAutoTran.selectPreSingle(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProductProductStorageByPName(row) error:" + e);
		}
	}

	public DBRow getDetailProductProductStorageByPCode(long root_storage_catalog_id,String p_code)
		throws Exception
	{
		try
		{
			//获得跟仓库分类下所有子分类
//			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
//			ArrayList al = tree.getAllSonNode(root_storage_catalog_id);
//			al.add(root_storage_catalog_id);
//			
			String cid = String.valueOf(root_storage_catalog_id);
//			int i=0;
//	
//			for (; i<al.size()-1; i++)
//			{
//				cid += al.get(i)+",";
//			}
//			cid += al.get(i);
			
			DBRow para = new DBRow();
			para.add("p_code",p_code);
			
			String sql = "SELECT ps.* FROM "+ConfigBean.getStringValue("product_storage")+" ps "
						+"join "+ConfigBean.getStringValue("product_code")+" pcode on pcode.pc_id = ps.pc_id and pcode.code_type = "+CodeTypeKey.Main+" and pcode.p_code = ? "
						+"join "+ConfigBean.getStringValue("product")+" p on ps.pc_id = p.pc_id " 
						+"where ps.cid in("+cid+")";
			
			return(dbUtilAutoTran.selectPreSingle(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProductProductStorageByPCode(row) error:" + e);
		}
	}

	public void delProductStorage(long pid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where pid=" + pid,ConfigBean.getStringValue("product_storage"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.delProductStorage(row) error:" + e);
		}
	}
	
	public void delProductStorageByPcId(long pc_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where pc_id=" + pc_id,ConfigBean.getStringValue("product_storage"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.delProductStorageByPcId(row) error:" + e);
		}
	}
	
	public DBRow[] getProductsInCids(String[] cids,PageCtrl pc)
		throws Exception
	{
		try
		{
			String cid = "";
			int i=0;
	
			
			if (cids==null||cids.length==0)
			{
				return( new DBRow[0] );
			}
			
			for (; i<cids.length-1; i++)
			{
				cid += cids[i]+",";
			}
			cid += cids[i];
			
			String sql = "select * from "+ConfigBean.getStringValue("product_storage")+" pc where cid in ("+cid+") order by pid desc";
			
			
			if ( pc!=null )
			{
				return(dbUtilAutoTran.selectMutliple(sql,pc));
			}
			else
			{
				return(dbUtilAutoTran.selectMutliple(sql));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductsInCids(row) error:" + e);
		}
	}
		
	public DBRow[] getShipProductsByName(String name)
		throws Exception
	{
		try
		{
			String sql = "select post_date,delivery_operator from " + ConfigBean.getStringValue("ship_product") + "  where name=? order by post_date desc";
			DBRow para = new DBRow();
			para.add("name",name);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getShipProductsByName(row) error:" + e);
		}
	}
	
	public DBRow getProductUnion(long id, long pid) throws Exception {
		
		try{
			
			String sql = " select * from product_union where set_pid = "+id+" and pid = "+pid;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		} catch(Exception e){
			throw new Exception("FloorProductMgr.getProductUnion(long id, long pid) error:"+e);
		}
	}

	public void addProductUnion(DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.insert(ConfigBean.getStringValue("product_union"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addProductUnion(row) error:" + e);
		}
	}

	public void delProductUnion(long set_pid,long pid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where set_pid=" + set_pid+" and pid="+pid,ConfigBean.getStringValue("product_union"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.delProductUnion(row) error:" + e);
		}
	}

	public DBRow getDetailProductInSet(long set_pid,long pid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("set_pid",set_pid);
			para.add("pid",pid);
			
			String sql = "select * from " + ConfigBean.getStringValue("product_union") + " pu "
						+"join "+ConfigBean.getStringValue("product") + " p  on p.pc_id = pu.pid " 
						+"where pu.set_pid=? and pu.pid=?";
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			return(rowo);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProductInSet error:" + e);
		}
	}

	public void modProductUnion(long set_pid,long pid,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where set_pid=" + set_pid+" and pid="+pid,ConfigBean.getStringValue("product_union"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.modProductUnion(row) error:" + e);
		}
	}

	public DBRow[] getProductsInSetBySetPid(long set_pid)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product_union") + " pu," + ConfigBean.getStringValue("product") + " p where p.pc_id=pu.pid and pu.set_pid=? order by p.pc_id desc";
			DBRow para = new DBRow();
			para.add("set_pid",set_pid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductsInSetBySetPid(row) error:" + e);
		}
	}

	public DBRow[] getProductUnionsByPid(long pid)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product_union") + " where pid=? ";
			DBRow para = new DBRow();
			para.add("pid",pid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductUnionsByPid(row) error:" + e);
		}
	}
	
	public DBRow[] getProductInfoUnionsByPid(long pid)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product_union") + " pu," + ConfigBean.getStringValue("product") + " p where pu.pid=? and pu.set_pid=p.pc_id ";
			DBRow para = new DBRow();
			para.add("pid",pid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductInfoUnionsByPid(row) error:" + e);
		}
	}
	
	public DBRow[] getProductUnionsBySetPid(long set_pid)
		throws Exception
	{
		try
		{
			String sql = "SELECT pu.*, p.p_name,pcode.p_code,p.unit_name, p.catalog_id FROM "+ConfigBean.getStringValue("product_union")+" pu "
						+"join "+ConfigBean.getStringValue("product")+" p on pu.pid = p.pc_id "
						+"join "+ConfigBean.getStringValue("product_code")+" pcode on pcode.pc_id = pu.pid and pcode.code_type ="+CodeTypeKey.Main+" "
						+"WHERE	pu.set_pid =? order by p.pc_id desc";
			
			DBRow para = new DBRow();
			para.add("set_pid",set_pid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductUnionsBySetPid(row) error:" + e);
		}
	}
	
	public long incProductStoreCountByPcid(ProductStoreLogBean inProductStoreLogBean,long root_storage_catalog_id,long pcid,double value,long from_psl_id)
		throws Exception
	{
		try
		{		
			//获得跟仓库分类下所有子分类
//			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
//			ArrayList al = tree.getAllSonNode(root_storage_catalog_id);
//			al.add(root_storage_catalog_id);
//			
			String cid = String.valueOf(root_storage_catalog_id);
//			int i=0;
//
//			for (; i<al.size()-1; i++)
//			{
//				cid += al.get(i)+",";
//			}
//			cid += al.get(i);
			
			dbUtilAutoTran.increaseFieldVal(ConfigBean.getStringValue("product_storage"),"where pc_id="+pcid+" and cid in ("+cid+")","store_count",value);
			
			//库存进出日志
//			com.cwc.app.api.ProductMgr pm = (com.cwc.app.api.ProductMgr)MvcUtil.getBeanFromContainer("productMgr");
//			inProductStoreLogBean.setQuantity(value);
//			inProductStoreLogBean.setQuantity_type(ProductStoreCountTypeKey.STORECOUNT);
//			inProductStoreLogBean.setPc_id(pcid);
//			inProductStoreLogBean.setPs_id(root_storage_catalog_id);
//			
//			long psl_id = pm.addProductStoreLogs(inProductStoreLogBean);
			
			//记录入库批次(屏蔽)
//			com.cwc.app.api.zj.ProductStoreLogsDetailMgrZJ productStoreLogsDetailMgrZJ = (com.cwc.app.api.zj.ProductStoreLogsDetailMgrZJ)MvcUtil.getBeanFromContainer("productStoreLogDetailMgrZJ");
//			productStoreLogsDetailMgrZJ.inStoreProdcutLogsDetail(psl_id,inProductStoreLogBean,from_psl_id);
			
			return 0;
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.incProductStoreCountByPcid(row) error:" + e);
		}
	}
	
	/**
	 * 业务回退
	 * @param inProductStoreLogBean
	 * @param root_storage_catalog_id
	 * @param pcid
	 * @param value
	 * @param	in_or_out 1代表库存加2代表库存减
	 * @return
	 * @throws Exception
	 */
	public long rebackProductStoreCountByPcid(ProductStoreLogBean reProductStoreLogBean,long root_storage_catalog_id,long pcid,double value,int in_or_de)
		throws Exception
	{
		try
		{
			//获得跟仓库分类下所有子分类
//			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
//			ArrayList al = tree.getAllSonNode(root_storage_catalog_id);
//			al.add(root_storage_catalog_id);
//			
			String cid = String.valueOf(root_storage_catalog_id);
//			int i=0;
//	
//			for (; i<al.size()-1; i++)
//			{
//				cid += al.get(i)+",";
//			}
//			cid += al.get(i);
			
			if(in_or_de==CountOperationKey.Increase)
			{
				dbUtilAutoTran.increaseFieldVal(ConfigBean.getStringValue("product_storage"),"where pc_id="+pcid+" and cid in ("+cid+")","store_count",value);
			}
			else if(in_or_de==CountOperationKey.Decrease)
			{
				
				dbUtilAutoTran.decreaseFieldVal(ConfigBean.getStringValue("product_storage"),"where pc_id="+pcid+" and cid in ("+cid+")","store_count",value);
				
				value = value*-1;
			}
			
			
			
//			//库存进出日志
//			com.cwc.app.api.ProductMgr pm = (com.cwc.app.api.ProductMgr)MvcUtil.getBeanFromContainer("productMgr");
//			reProductStoreLogBean.setQuantity(value);
//			reProductStoreLogBean.setQuantity_type(ProductStoreCountTypeKey.STORECOUNT);
//			reProductStoreLogBean.setPc_id(pcid);
//			reProductStoreLogBean.setPs_id(root_storage_catalog_id);
//			
//			long psl_id = pm.addProductStoreLogs(reProductStoreLogBean);
			
			return 0;
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.incProductStoreCountByPcid(row) error:" + e);
		}
	}
	
	public void damageProductCountInStore(long storage_catalog_id,long pcid,double value)
	throws Exception
	{
		try
		{
			dbUtilAutoTran.increaseFieldVal(ConfigBean.getStringValue("product_storage"),
					"where pc_id="+pcid+" and cid = "+storage_catalog_id+"","damaged_count",value);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.damageProductCountInStore(row) error:" + e);
		}
	}
	
	public void damageProductCountChangeNormal(long storage_catalog_id,long pcid,double value)
	throws Exception
	{
		try
		{
			String sql1 = "select damaged_count from product_storage where pc_id="+pcid+" and cid="+storage_catalog_id;
			double damaged_count = dbUtilAutoTran.selectSingle(sql1).get("damaged_count", 0.0);
			//如果库存中的残损数不够，减外观残损件
			if(damaged_count >= value)
			{
				dbUtilAutoTran.decreaseFieldVal(ConfigBean.getStringValue("product_storage"),
						"where pc_id="+pcid+" and cid = "+storage_catalog_id+"","damaged_count",value);
			}
			else
			{
				dbUtilAutoTran.decreaseFieldVal(ConfigBean.getStringValue("product_storage"),
						"where pc_id="+pcid+" and cid = "+storage_catalog_id+"","damaged_count",damaged_count);
				dbUtilAutoTran.decreaseFieldVal(ConfigBean.getStringValue("product_storage"),
						"where pc_id="+pcid+" and cid = "+storage_catalog_id+"","damaged_package_count",Math.abs(value-damaged_count));
			}
			
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.damageProductCountChangeNormal(long storage_catalog_id,long pcid,double value) error:" + e);
		}
	}

	public void productRepairCountInStore(long storage_catalog_id,long pcid,double value)
	throws Exception
	{
		try
		{
			dbUtilAutoTran.increaseFieldVal(ConfigBean.getStringValue("product_storage"),
					"where pc_id="+pcid+" and cid = "+storage_catalog_id+"","store_count",value);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.productRepairCountInStore(row) error:" + e);
		}
	}
	
	public double getProductDamageByStorageAndProductId(long storage_catalog_id,long pcid) throws Exception
	{
		try
		{
			String sql = "select (damaged_count+damaged_package_count) as damaged_count_tatal from product_storage where cid ="+storage_catalog_id+" and pc_id = "+pcid;
			return dbUtilAutoTran.selectSingle(sql).get("damaged_count_tatal", 0.0);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductDamageByStorageAndProductId(long storage_catalog_id,long pcid) error:" + e);
		}
	}
	
	public DBRow getDamageProductByStorageAndProductId(long storage_catalog_id,long pcid) throws Exception
	{
		try
		{
			String sql = "select (damaged_count+damaged_package_count) as damaged_count_tatal from product_storage where cid ="+storage_catalog_id+" and pc_id = "+pcid;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDamageProductByStorageAndProductId(long storage_catalog_id,long pcid) error:" + e);
		}
	}
	
	/**
	 * 商品出库
	 * @param deInProductStoreLogBean
	 * @param root_storage_catalog_id
	 * @param pcid
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public long deIncProductStoreCountByPcid(ProductStoreLogBean deInProductStoreLogBean,long root_storage_catalog_id,long pcid,double value)
		throws Exception
	{
		try
		{
			//long st = System.currentTimeMillis();
			
			//获得跟仓库分类下所有子分类
//			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
//			ArrayList al = tree.getAllSonNode(root_storage_catalog_id);
//			al.add(root_storage_catalog_id);
//			
			String cid = String.valueOf(root_storage_catalog_id);
//			int i=0;
//
//			for (; i<al.size()-1; i++)
//			{
//				cid += al.get(i)+",";
//			}
//			cid += al.get(i);
			dbUtilAutoTran.decreaseFieldVal(ConfigBean.getStringValue("product_storage"),"where pc_id="+pcid+" and cid in ("+cid+")","store_count",value);
			
			//库存进出日志
//			com.cwc.app.api.ProductMgr pm = (com.cwc.app.api.ProductMgr)MvcUtil.getBeanFromContainer("productMgr");
//			deInProductStoreLogBean.setQuantity(-value);
//			deInProductStoreLogBean.setQuantity_type(ProductStoreCountTypeKey.STORECOUNT);
//			deInProductStoreLogBean.setPc_id(pcid);
//			deInProductStoreLogBean.setPs_id(root_storage_catalog_id);
			
//			synchronized(String.valueOf(pcid)+"_"+String.valueOf(root_storage_catalog_id))
//			{
//				//获得当前商品实际库存
//				DBRow productActualStorage = getProductActualStoreByPidPsId(pcid, root_storage_catalog_id);
//				deInProductStoreLogBean.setMerge_count(productActualStorage.get("merge_count",0f));
//				deInProductStoreLogBean.setLacking_quantity(productActualStorage.get("lacking_quantity",0f));
//				deInProductStoreLogBean.setStore_count(productActualStorage.get("store_count",0f));
//			}
			
//			long psl_id = pm.addProductStoreLogs(deInProductStoreLogBean);
			
			//记录出库批次(屏蔽)
//			com.cwc.app.api.zj.ProductStoreLogsDetailMgrZJ productStoreLogsDetailMgrZJ = (com.cwc.app.api.zj.ProductStoreLogsDetailMgrZJ)MvcUtil.getBeanFromContainer("productStoreLogDetailMgrZJ");
//			productStoreLogsDetailMgrZJ.outStoreProductLogsDetail(psl_id, deInProductStoreLogBean);
			
			return 0;
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.deIncProductStoreCountByPcid(row) error:" + e);
		}
	}
	
	public void incProductDamagedCountByPid(long pid,double value)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.increaseFieldVal(ConfigBean.getStringValue("product_storage"),"where pid="+pid+" ","damaged_count",value);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.incProductDamagedCountByPid(row) error:" + e);
		}
	}

	public void deIncProductDamagedCountByPid(long pid,double value)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.decreaseFieldVal(ConfigBean.getStringValue("product_storage"),"where pid="+pid+" ","damaged_count",value);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.deIncProductDamagedCountByPid(row) error:" + e);
		}
	}
	
	public void incProductPackageDamagedCountByPid(long pid,double value)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.increaseFieldVal(ConfigBean.getStringValue("product_storage"),"where pid="+pid+" ","damaged_package_count",value);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.incProductPackageDamagedCountByPid(row) error:" + e);
		}
	}

	public void deIncProductPackageDamagedCountByPid(long pid,double value)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.decreaseFieldVal(ConfigBean.getStringValue("product_storage"),"where pid="+pid+" ","damaged_package_count",value);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.deIncProductPackageDamagedCountByPid(row) error:" + e);
		}
	}
	
	public void incProductDamagedCountByPcid(long root_storage_catalog_id,long pcid,double value)
		throws Exception
	{
		try
		{
			//获得跟仓库分类下所有子分类
//			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
//			ArrayList al = tree.getAllSonNode(root_storage_catalog_id);
//			al.add(root_storage_catalog_id);
//			
			String cid = String.valueOf(root_storage_catalog_id);
//			int i=0;
//	
//			for (; i<al.size()-1; i++)
//			{
//				cid += al.get(i)+",";
//			}
//			cid += al.get(i);
			
			dbUtilAutoTran.increaseFieldVal(ConfigBean.getStringValue("product_storage"),"where pc_id="+pcid+" and cid in ("+cid+")","damaged_count",value);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.incProductDamagedCountByPcid(row) error:" + e);
		}
	}

	public void incProductPackageDamagedCountByPcid(long root_storage_catalog_id,long pcid,double value)
		throws Exception
	{
		try
		{
			//获得跟仓库分类下所有子分类
//			Tree tree = new Tree(ConfigBean.getStringValue("product_storage_catalog"));
//			ArrayList al = tree.getAllSonNode(root_storage_catalog_id);
//			al.add(root_storage_catalog_id);
//			
			String cid = String.valueOf(root_storage_catalog_id);
//			int i=0;
//	
//			for (; i<al.size()-1; i++)
//			{
//				cid += al.get(i)+",";
//			}
//			cid += al.get(i);
			
			dbUtilAutoTran.increaseFieldVal(ConfigBean.getStringValue("product_storage"),"where pc_id="+pcid+" and cid in ("+cid+")","damaged_package_count",value);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.incProductPackageDamagedCountByPcid(row) error:" + e);
		}
	}
		
	
	

	/**
	 * 检测该ID是否存在于商品组合表中
	 * @param pid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductUnionsById(long pid)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product_union") + " where pid=? or set_pid=? ";
			DBRow para = new DBRow();
			para.add("pid",pid);
			para.add("set_pid",pid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductUnionsById(row) error:" + e);
		}
	}

	public long addProductCustom(DBRow row)
		throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("product_custom"));
			row.add("pc_id",id);
			row.add("union_flag",1);
			row.add("alive",0);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("product_custom"),row);
			return(id);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addProductCustom(row) error:" + e);
		}
	}
	
	public long addProductCustomUnion(DBRow row)
		throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("product_custom_union"));
			dbUtilAutoTran.insert(ConfigBean.getStringValue("product_custom_union"),row);
			return(id);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addProductCustomUnion(row) error:" + e);
		}
	}	
	
	public void delProductCustomUnionBySetPid(long set_pid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where set_pid=" + set_pid,ConfigBean.getStringValue("product_custom_union"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.delProductCustomUnionBySetPid(row) error:" + e);
		}
	}	
	
	public DBRow getDetailProductCustomByPcPcid(long pcpcid)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("pc_id",pcpcid);

			String sql = "select * from " + ConfigBean.getStringValue("product_custom") + " where pc_id=?";
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			return(rowo);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProductCustomByPcPcid error:" + e);
		}
	}

	public DBRow getDetailProductCustomUnionBySetPidPid(long set_pid,long pid)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("set_pid",set_pid);
			para.add("pid",pid);
	
			String sql = "select * from " + ConfigBean.getStringValue("product_custom_union") + " where set_pid=? and pid=?";
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			return(rowo);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProductCustomUnionByPcPcid error:" + e);
		}
	}
	
	public DBRow[] getProductsCustomInSetBySetPid(long set_pid)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product_custom_union") + " pu," + ConfigBean.getStringValue("product") + " p where p.pc_id=pu.pid and pu.set_pid=? order by p.pc_id desc";
			DBRow para = new DBRow();
			para.add("set_pid",set_pid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductsCustomInSetBySetPid(row) error:" + e);
		}
	}

	public DBRow[] getProductCustomUnionsBySetId(long set_pid)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product_custom_union") + " where set_pid=? ";
			DBRow para = new DBRow();
			para.add("set_pid",set_pid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductCustomUnionsBySetId(row) error:" + e);
		}
	}

	public void addStorageCountry(DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.insert(ConfigBean.getStringValue("storage_country"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addStorageCountry(row) error:" + e);
		}
	}
	
	public void addStorageProvince(DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.insert(ConfigBean.getStringValue("storage_province"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addStorageProvince(row) error:" + e);
		}
	}
		
	public void delStorageProvinceBySid(long sid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where sid=" + sid,ConfigBean.getStringValue("storage_province"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.delStorageProvinceBySid(row) error:" + e);
		}
	}
	
	public void delStorageCountryByCid(long cid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where cid=" + cid,ConfigBean.getStringValue("storage_country"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.delStorageCountryByCid(row) error:" + e);
		}
	}

	public void delStorageCountryBySid(long sid)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where sid=" + sid,ConfigBean.getStringValue("storage_country"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.delStorageCountryBySid(row) error:" + e);
		}
	}
	
	/**
	 * 通过仓库ID获得其下所有国家
	 * @param sid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageCountryBySid(long sid)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("storage_country") + " sc," + ConfigBean.getStringValue("country_code") + " c where sc.cid=c.ccid and sc.sid=? order by c.c_country asc";
			DBRow para = new DBRow();
			para.add("sid",sid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStorageCountryBySid(row) error:" + e);
		}
	}
	
	public DBRow[] getFreeStorageCountrys(long psid)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("country_code") + " c LEFT JOIN " + ConfigBean.getStringValue("storage_country") + " sc on sc.cid=c.ccid where sc.sid=? or sc.sid is null   order by c.c_country asc";
									
			DBRow para = new DBRow();
			para.add("psid",psid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getFreeStorageCountrys(row) error:" + e);
		}
	}

	/**
	 * 通过商品ID，获得所有仓库下的库存信息
	 * @param pcid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getProductStoragesByPcid(long pcid)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product_storage") + "  where pc_id=?";
			DBRow para = new DBRow();
			para.add("pcid",pcid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductStoragesByPcid(row) error:" + e);
		}
	}
	
	public DBRow getDetailStorageCountryByCid(long cid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("cid",cid);
	
			String sql = "select * from " + ConfigBean.getStringValue("storage_country") + " where cid=?";
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			return(rowo);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailStorageCountryByCid error:" + e);
		}
	}

	public DBRow getDetailProductStorageByPid(long pid)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("pid",pid);
			
			String sql = "select * from " + ConfigBean.getStringValue("product_storage") + " where pid=? ";
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			return(rowo);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProductStorageByPid error:" + e);
		}
	}

	public DBRow[] getStatProductStorage(long ps_id,long pscid,long catalog_id)
		throws Exception
	{
		try
		{
			String sql = "select *,lop.p_quantity lacking_quantity from " + ConfigBean.getStringValue("product_storage") + " ps," + ConfigBean.getStringValue("product") + " p LEFT JOIN " + ConfigBean.getStringValue("lacking_order_products_view") + " lop ON lop.p_pid=p.pc_id AND lop.ps_id=?   where ps.cid=? and ps.pc_id=p.pc_id and p.catalog_id=? and p.orignal_pc_id=0 order by p.pc_id asc";
			//String sql = "select * from " + ConfigBean.getStringValue("product_storage") + " ps," + ConfigBean.getStringValue("product") + " p   where ps.cid=? and ps.pc_id=p.pc_id and p.catalog_id=? order by p.pc_id asc";
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("pscid",pscid);
			para.add("catalog_id",catalog_id);

//			System.out.println(sql);
//			System.out.println(ps_id);
//			System.out.println(pscid);
//			System.out.println(catalog_id);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStatProductStorage(row) error:" + e);
		}
	}

	public DBRow[] getStatProductStoragePC(long pscid)
		throws Exception
	{
		try 
		{
			String sql = "select catalog_id from " + ConfigBean.getStringValue("product_storage") + " ps," + ConfigBean.getStringValue("product") + " p  where ps.cid=? and ps.pc_id=p.pc_id group by p.catalog_id order by p.catalog_id asc";
			DBRow para = new DBRow();
			para.add("pscid",pscid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStatProductStoragePC(row) error:" + e);
		}
	}

	public DBRow[] getStatProductPC(long pscid)
		throws Exception
	{
		try
		{
			//获得该分类下所有子分类
//			Tree tree = new Tree(ConfigBean.getStringValue("product_catalog"));
//			ArrayList al = tree.getAllSonNode(pscid);
//			al.add(pscid);
//
			String cid = String.valueOf(pscid);
//			int i=0;
//
//			for (; i<al.size()-1; i++)
//			{
//				cid += al.get(i)+",";
//			}
//			cid += al.get(i);
			
			String sql = "select catalog_id from " + ConfigBean.getStringValue("product") + " p  where p.catalog_id in ("+cid+") and p.orignal_pc_id=0 group by p.catalog_id order by p.catalog_id asc";
			
			//System.out.println("getStatProductPC:"+sql);
			
			return(dbUtilAutoTran.selectMutliple(sql));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStatProductPC(row) error:" + e);
		}
	}
	
	
	public DBRow[] getStatProduct(long catalog_id)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product") + " p  where p.catalog_id=? and p.orignal_pc_id = 0 order by p.pc_id asc";
			DBRow para = new DBRow();
			para.add("catalog_id",catalog_id);

			//System.out.println("getStatProduct "+catalog_id+" :"+sql);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStatProduct(row) error:" + e);
		}
	}

	public long addProductStoreLogs(DBRow row)
		throws Exception
	{
		try
		{
			
			long psl_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("product_store_logs"));
			row.add("psl_id",psl_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("product_store_logs"),row);
			return(psl_id);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addProductStoreLogs(row) error:" + e);
		}
	}

	public DBRow[] getBookProducts(String st,String en,int keep_days,long ps_id,long pscid,long catalog_id)
		throws Exception
	{
		try
		{
			String sql = "select * from (select ps.cid,sdps.catalog_id,sdps.p_pid,sdps.p_name,(sum(sdps.p_quantity)/(to_days('"+en+"') -to_days('"+st+"')+1)) AS avg_quantity,(sum(sdps.p_quantity)/(to_days('"+en+"') -to_days('"+st+"')+1))*"+keep_days+" AS hold_quantity,(sum(sdps.p_quantity)/(to_days('"+en+"') -to_days('"+st+"')+1))*"+keep_days+"-ps.store_count AS book_quantity,ps.store_count   from " + ConfigBean.getStringValue("storage_day_products_sell_view") + " sdps," + ConfigBean.getStringValue("product_storage") + " ps," + ConfigBean.getStringValue("product_storage_catalog") + " psc  where ps.pc_id=sdps.p_pid and ps.cid=psc.id and psc.parentid=sdps.ps_id and  sdps.post_date >= str_to_date('"+st+"','%Y-%m-%d') and sdps.post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and sdps.ps_id=? and ps.cid=? and sdps.catalog_id=? group by sdps.p_pid ) book_product where book_quantity>0";
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			para.add("pscid",pscid);
			para.add("catalog_id",catalog_id);
	
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getBookProducts(row) error:" + e);
		}
	}

	public DBRow[] getBookProductsPC(String st,String en,int keep_days,long pscid)
		throws Exception
	{
		try 
		{
			String sql = "select catalog_id from (select ps.cid,sdps.catalog_id,sdps.p_pid,sdps.p_name,(sum(sdps.p_quantity)/(to_days('"+en+"') -to_days('"+st+"')+1)) AS avg_quantity,(sum(sdps.p_quantity)/(to_days('"+en+"') -to_days('"+st+"')+1))*"+keep_days+" AS hold_quantity,(sum(sdps.p_quantity)/(to_days('"+en+"') -to_days('"+st+"')+1))*"+keep_days+"-ps.store_count AS book_quantity,ps.store_count   from " + ConfigBean.getStringValue("storage_day_products_sell_view") + " sdps," + ConfigBean.getStringValue("product_storage") + " ps," + ConfigBean.getStringValue("product_storage_catalog") + " psc  where ps.pc_id=sdps.p_pid and ps.cid=psc.id and psc.parentid=sdps.ps_id and  sdps.post_date >= str_to_date('"+st+"','%Y-%m-%d') and sdps.post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')  and ps.cid=? group by sdps.p_pid ) book_product where book_quantity>0 group by catalog_id";
			DBRow para = new DBRow();
			para.add("pscid",pscid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getBookProductsPC(row) error:" + e);
		}
	}

	public DBRow[] getStatDamagedProducts(long pscid,long catalog_id)
		throws Exception
	{
		try
		{
			String sql = "select catalog_id,ps.pc_id,p_name,store_station,store_count,damaged_count,unit_name  from " + ConfigBean.getStringValue("product_storage") + " ps," + ConfigBean.getStringValue("product") + " p where ps.pc_id=p.pc_id and damaged_count>0 and cid=? and catalog_id=? and p.orignal_pc_id=0";
			
			DBRow para = new DBRow();
			para.add("pscid",pscid);
			para.add("catalog_id",catalog_id);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStatDamagedProducts(row) error:" + e);
		}
	}

	public DBRow[] getStatDamagedProductsPC(long pscid)
		throws Exception
	{
		try
		{
			String sql = "select catalog_id  from product_storage ps,product p where ps.pc_id=p.pc_id and  damaged_count>0 and cid=? group by catalog_id";
			
			DBRow para = new DBRow();
			para.add("pscid",pscid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStatDamagedProducts(row) error:" + e);
		}
	}
	
	public DBRow[] getProductStoreSysLogs(PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select psc.title,p_name,psl.* from " + ConfigBean.getStringValue("product_store_logs") + " psl," + ConfigBean.getStringValue("product") + " p," + ConfigBean.getStringValue("product_storage_catalog") + " psc where psl.pc_id=p.pc_id and psl.ps_id=psc.id order by psl_id desc";
			
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductStoreSysLogs(row) error:" + e);
		}
	}

	/**
	 * 通过商品ID和仓库ID，获得商品实际库存数
	 * @param pid
	 * @param ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getProductActualStoreByPidPsId(long pid,long ps_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("pid",pid);
			para.add("ps_id",ps_id);
			
			String sql = "select IFNULL(lop.p_quantity,0)+store_count  merge_count,lop.p_quantity lacking_quantity,store_count from (select store_count,pc_id,psc2.id ps_id  from product_storage ps,product_storage_catalog psc1,product_storage_catalog psc2 where ps.cid=psc1.id and psc1.parentid=psc2.id) AS s LEFT JOIN lacking_order_products_view lop on lop.p_pid=s.pc_id and s.ps_id=lop.ps_id where pc_id=? and s.ps_id=?";
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			return(rowo);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductActualStoreByPidPsId error:" + e);
		}
	}

	
	/**
	 * 检索商品库存
	 * @param st
	 * @param en
	 * @param product_line_id
	 * @param catalog_id
	 * @param pc_id
	 * @param bill_id
	 * @param bill_type
	 * @param operotion
	 * @param quantity_type
	 * @param ps_id
	 * @param adid
	 * @param onGroup
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchProductStoreSysLogsNew(String st,String en,long product_line_id,long catalog_id,long pc_id,long bill_id,int bill_type,int operotion,int quantity_type,long ps_id,long adid,int onGroup,int in_or_out,PageCtrl pc)
		throws Exception
	{
		try
		{
			String cond = "";
			
			if (bill_id>0)
			{
				cond += " and oid="+bill_id+" ";
				
				if(bill_type>0)
				{
					cond += " and bill_type="+bill_type+" ";
				}
			}
			
			String whereProductLine = "";
			if(product_line_id>0&&!(catalog_id>0))
			{
				whereProductLine = " join "+ConfigBean.getStringValue("product_catalog")+" as pc on pc.id = p.catalog_id"
								  +" and pc.product_line_id = "+product_line_id+" ";
			}
			else if(product_line_id>0&&catalog_id>0)
			{
				whereProductLine = " and pc.product_line_id = "+product_line_id+" ";
			}
			
			String whereInOrOut = "";
			if(in_or_out==1)
			{
				whereInOrOut = " and psl.quantity>0 ";
			}
			else if(in_or_out==2)
			{
				whereInOrOut = " and psl.quantity<0 ";
			}
			
			String whereProductCatalog = "";
			if(catalog_id>0)//需要根据分类过滤
			{
				whereProductCatalog = " join "+ConfigBean.getStringValue("product_catalog")+" as pc on pc.id = p.catalog_id "+whereProductLine//尝试新方式产品线与商品分类在需要时再join
									 +" join pc_child_list as pcl on pcl.pc_id = pc.id and pcl.search_rootid = "+catalog_id+" ";
			}
			else//不需要根据分类过滤，但有可能需要根据产品线过滤
			{
				whereProductCatalog += whereProductLine;
			}
			
			String whereProdcut = "";
			if(pc_id>0)
			{
				whereProdcut = " and p.pc_id = "+pc_id+" ";
			}
			
			String whereOperotion = "";
			if(operotion>0)
			{
				whereOperotion = " and psl.operation = "+operotion+" ";
			}
			
			String whereQuatityType = "";
			if(quantity_type>0)
			{
				whereQuatityType = " and psl.quantity_type = "+quantity_type+" ";
			}
			
			String whereStore = "";
			if(ps_id>0)
			{
				whereStore = " and psl.ps_id = "+ps_id+" ";
			}
			
			String whereAdmin = "";
			if(adid>0)
			{
				whereAdmin = " and psl.adid = "+adid+" ";
			}
			
			String sql;
			if(onGroup ==0)//列表显示，不汇总，排序按时间顺序
			{
				 sql = "select psc.title,p.p_name,psl.* from "+ConfigBean.getStringValue("product_store_logs")+" as psl "
					+"join "+ConfigBean.getStringValue("product")+" as p on psl.pc_id = p.pc_id "+whereProdcut
					+whereProductCatalog//新方式
					+"join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on psl.ps_id = psc.id "+whereStore
					+"where psl.post_date >= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')"
					+cond+whereAdmin+whereOperotion+whereQuatityType+whereInOrOut
					+"order by psl_id desc";
			}
			else//汇总形式，排序按照商品名
			{
				sql = "select psc.title,p.p_name,psl.psl_id,psl.oid,psl.operation,psl.post_date,sum(psl.quantity) as quantity,psl.ps_id,psl.pc_id,psl.account,psl.bill_type,psl.adid,psl.quantity_type from "+ConfigBean.getStringValue("product_store_logs")+" as psl "
				+"join "+ConfigBean.getStringValue("product")+" as p on psl.pc_id = p.pc_id "+whereProdcut
				+whereProductCatalog//新方式
				+"join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on psl.ps_id = psc.id "+whereStore
				+"where psl.post_date >= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')"
				+cond+whereAdmin+whereOperotion+whereQuatityType+whereInOrOut
				+" group by p.pc_id order by p_name desc";
			}
			
			
	
//			String sql = "select psc.title,p_name,psl.* from " + ConfigBean.getStringValue("product_store_logs") + " psl," + ConfigBean.getStringValue("product") + " p," + ConfigBean.getStringValue("product_storage_catalog") + " psc where psl.pc_id=p.pc_id and psl.ps_id=psc.id and post_date >= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') "+cond+" order by psl_id desc";
//			System.out.println("sql:"+sql);
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getSearchProductStoreSysLogsNew(row) error:" + e);
		}
	}
	
	public DBRow[] getSearchReturnProductStoreSysLogsNew(String st,String en,long product_line_id,long catalog_id,long pc_id,long bill_id,int bill_type,int quantity_type,long ps_id,long adid,int onGroup,PageCtrl pc)
		throws Exception
	{
		try
		{
			String cond = "";
			
			if (bill_id>0)
			{
				cond += " and oid="+bill_id+" ";
				
				if(bill_type>0)
				{
					cond += " and bill_type="+bill_type+" ";
				}
			}
			
			String whereProductLine = "";
			if(product_line_id>0&&!(catalog_id>0))
			{
				whereProductLine = " join "+ConfigBean.getStringValue("product_catalog")+" as pc on pc.id = p.catalog_id"
								  +" and pc.product_line_id = "+product_line_id+" ";
			}
			else if(product_line_id>0&&catalog_id>0)
			{
				whereProductLine = " and pc.product_line_id = "+product_line_id+" ";
			}
			
			String whereProductCatalog = "";
			if(catalog_id>0)//需要根据分类过滤
			{
				whereProductCatalog = " join "+ConfigBean.getStringValue("product_catalog")+" as pc on pc.id = p.catalog_id "+whereProductLine//尝试新方式产品线与商品分类在需要时再join
									 +" join pc_child_list as pcl on pcl.pc_id = pc.id and pcl.search_rootid = "+catalog_id+" ";
			}
			else//不需要根据分类过滤，但有可能需要根据产品线过滤
			{
				whereProductCatalog += whereProductLine;
			}
			
			String whereProdcut = "";
			if(pc_id>0)
			{
				whereProdcut = " and p.pc_id = "+pc_id+" ";
			}
			
			String whereOperotion = "and(psl.operation="+ProductStoreOperationKey.IN_STORE_RETURN+" or psl.operation="+ProductStoreOperationKey.IN_STORE_DAMAGED_RETURN+" )";
			
			String whereQuatityType = "";
			if(quantity_type>0)
			{
				whereQuatityType = " and psl.quantity_type = "+quantity_type+" ";
			}
			
			String whereStore = "";
			if(ps_id>0)
			{
				whereStore = " and psl.ps_id = "+ps_id+" ";
			}
			
			String whereAdmin = "";
			if(adid>0)
			{
				whereAdmin = " and psl.adid = "+adid+" ";
			}
			
			String sql;
			if(onGroup ==0)//列表显示，不汇总，排序按时间顺序
			{
				 sql = "select psc.title,p.p_name,psl.* from "+ConfigBean.getStringValue("product_store_logs")+" as psl "
					+"join "+ConfigBean.getStringValue("product")+" as p on psl.pc_id = p.pc_id "+whereProdcut
					+whereProductCatalog//新方式
					+"join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on psl.ps_id = psc.id "+whereStore
					+"where psl.post_date >= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')"
					+cond+whereAdmin+whereOperotion+whereQuatityType
					+"order by psl_id desc";
			}
			else//汇总形式，排序按照商品名
			{
				sql = "select psc.title,p.p_name,psl.psl_id,psl.oid,psl.operation,psl.post_date,sum(psl.quantity) as quantity,psl.ps_id,psl.pc_id,psl.account,psl.bill_type,psl.adid,psl.quantity_type from "+ConfigBean.getStringValue("product_store_logs")+" as psl "
				+"join "+ConfigBean.getStringValue("product")+" as p on psl.pc_id = p.pc_id "+whereProdcut
				+whereProductCatalog//新方式
				+"join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on psl.ps_id = psc.id "+whereStore
				+"where psl.post_date >= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s')"
				+cond+whereAdmin+whereOperotion+whereQuatityType
				+" group by p.pc_id,quantity_type order by p_name desc";
			}
			
			
	
	//		String sql = "select psc.title,p_name,psl.* from " + ConfigBean.getStringValue("product_store_logs") + " psl," + ConfigBean.getStringValue("product") + " p," + ConfigBean.getStringValue("product_storage_catalog") + " psc where psl.pc_id=p.pc_id and psl.ps_id=psc.id and post_date >= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') "+cond+" order by psl_id desc";
//			System.out.println("sql:"+sql);
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getSearchProductStoreSysLogsNew(row) error:" + e);
		}
	}
	
	
	public DBRow[] getSearchProductStoreSysLogs(String st,String en,long oid,String p_name,String account,int do_group_by,PageCtrl pc)
		throws Exception
	{
		try
		{
			String cond = "";
			
			if (oid>0)
			{
				cond += " and oid="+oid;
			}
				
			if (p_name.equals("")==false)
			{
				cond += " and p_name='"+p_name+"'";
			}
			
			if (account.equals("")==false)
			{
				cond += " and account='"+account+"'";
			}

			String sql = "select psc.title,p_name,psl.* from " + ConfigBean.getStringValue("product_store_logs") + " psl," + ConfigBean.getStringValue("product") + " p," + ConfigBean.getStringValue("product_storage_catalog") + " psc where psl.pc_id=p.pc_id and psl.ps_id=psc.id and post_date >= str_to_date('"+st+"','%Y-%m-%d') and post_date <= str_to_date('"+en+" 23:59:59','%Y-%m-%d %H:%i:%s') "+cond+" order by psl_id desc";
			//System.out.println("sql:"+sql);
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getSearchProductStoreSysLogs(row) error:" + e);
		}
	}

	public long addStorageLocation(DBRow row)
		throws Exception
	{
		try
		{
			
			long sl_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("storage_location"));
			row.add("sl_id",sl_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("storage_location"),row);
			return(sl_id);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addStorageLocation(row) error:" + e);
		}
	}
	
	/**
	 * 删除当前仓库下的商品定位信息
	 * @param ps_id
	 * @throws Exception
	 */
	public void cleanStorageLocationByPsidMachineId(long ps_id,String machine_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where ps_id="+ps_id + " and machine_id='"+machine_id+"'", ConfigBean.getStringValue("storage_location"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.cleanStorageLocationByPsidMachineId(row) error:" + e);
		}
	}
	
	public void cleanStorageLocationBySlc(long slc_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where slc_id ="+slc_id, ConfigBean.getStringValue("storage_location"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.cleanStorageLocationByPsidMachineId(row) error:" + e);
		}
	}
		
	public DBRow[] getAllStorageLocation(PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select sl.machine_id,sl.sl_id,sl.barcode,sl.position,sum(sl.quantity) quantity,psc.title from "+ConfigBean.getStringValue("storage_location")+" sl,"+ConfigBean.getStringValue("product_storage_catalog")+" psc where psc.id=sl.ps_id group by barcode order by barcode asc";
			
			if (pc==null)
			{
				return(dbUtilAutoTran.selectMutliple(sql));
			}
			else
			{
				return(dbUtilAutoTran.selectMutliple(sql,pc));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getAllStorageLocation(row) error:" + e);
		}
	}
		
	public DBRow[] getStorageLocationByPsId(long ps_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("ps_id", ps_id);
			
			String sql = "select sl.sl_id,sl.barcode,sl.position,sum(sl.quantity) quantity,psc.title from "+ConfigBean.getStringValue("storage_location")+" sl,"+ConfigBean.getStringValue("product_storage_catalog")+" psc where psc.id=sl.ps_id and ps_id=? group by barcode order by barcode asc";
			
			if (pc==null)
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStorageLocationByPsId(row) error:" + e);
		}
	}

	public DBRow[] getIncorrectStorageByCidPsid(long cid,long ps_id,PageCtrl pc,int[] status)
		throws Exception
	{
		try
		{
			String whereStatus = "";
			if(status.length>0)
			{
				whereStatus += " and (";
				for (int i = 0; i < status.length; i++) 
				{
					whereStatus += "wo.status="+status[i];
					
					if(i<status.length-1)
					{
						whereStatus += " or ";
					}
				}
				whereStatus +=") ";
			}
			
			String whereCatalog = "";
			if(cid>0)
			{
				whereCatalog = " join pc_child_list as pcl on pcl.pc_id = ps.pc_id and pcl.search_rootid = "+cid+" ";
			}
			
			
			//定位商品数据表跟经过补偿后的库存对比
			String sql;
			
			
			sql="select psc.title,ps.ps_id,ps.pc_id,sl.quantity,ps.barcode,(ps.store_count+ps.compensationValue)as merge_count,ps.store_count,ps.compensationValue from "
			   +"("
			   +"	select cid as ps_id,ps.pc_id,pcode_main.p_code as barcode,sum(store_count) as store_count,COALESCE(compensation.compensationValue,0) as compensationValue from product_storage as ps "
			   +whereCatalog
			   +"left join " 
			   +"("
			   +"select ps_id,pc_id,p_name,p_code as barcode,sum(quantity) as compensationValue FROM "
			   +"(select wo.ps_id,p.pc_id,p.p_name,pcode.p_code,sum(woi.quantity) as quantity from waybill_order as wo "
			   +"join waybill_order_item as woi on wo.waybill_id = woi.waybill_order_id and (woi.product_type =1 or woi.product_type=2) and woi.product_status = "+ProductStatusKey.IN_STORE+" "
			   +"join product as p on woi.pc_id = p.pc_id "
			   +"join product_code pcode on pcode.pc_id = p.pc_id and pcode.code_type ="+CodeTypeKey.Main+" " 
			   +"where wo.ps_id="+ps_id+whereStatus
			   +"group by p.pc_id "
			   +"UNION ALL "
			   +"SELECT wo.ps_id,p.pc_id, p.p_name,pcode.p_code,sum(woi.quantity*pu.quantity) as quantity FROM waybill_order as wo " 
			   +"JOIN waybill_order_item woi  on wo.waybill_id = woi.waybill_order_id and (woi.product_type=3 or woi.product_type=4) and woi.product_status = "+ProductStatusKey.IN_STORE+" "
			   +"JOIN product_union as pu ON woi.pc_id = pu.set_pid "
			   +"JOIN product as p ON pu.pid = p.pc_id "
			   +"join product_code pcode on pcode.pc_id = p.pc_id and pcode.code_type ="+CodeTypeKey.Main+" "
			   +"where wo.ps_id="+ps_id+whereStatus
			   +"group by p.pc_id "
			   +") as compensationValue GROUP BY pc_id "
			   +") as compensation on compensation.ps_id = ps.cid and compensation.pc_id=ps.pc_id "
			   +"join product as p on ps.pc_id=p.pc_id "
			   +"join product_code as pcode_main on p.pc_id = pcode_main.pc_id and pcode_main.code_type="+CodeTypeKey.Main+" "
			   +"where ps.cid="+ps_id+" "
			   +"GROUP BY cid, ps.pc_id "
			   +")as ps "
			   +"left JOIN "
//			   +"(SELECT ps_id,p.pc_id,sum(quantity) as quantity FROM	storage_location as sl join product as p on p.p_code=sl.barcode GROUP BY ps_id,barcode) as sl "//原先是用条码关联
			   +"(SELECT ps_id,p.pc_id,sum(quantity) as quantity FROM	storage_location as sl join product as p on p.pc_id = sl.pc_id GROUP BY ps_id,barcode) as sl "
			   +"on ps.ps_id = sl.ps_id and ps.pc_id = sl.pc_id " 
			   +"join product_storage_catalog as psc on psc.id =ps.ps_id "
			   +"where ((ps.store_count+ps.compensationValue)!=sl.quantity) or ((ps.store_count+ps.compensationValue)>0 and sl.quantity=NULL) ";
			
//			System.out.println(sql);
			if (pc==null)
			{
				return (dbUtilAutoTran.selectMutliple(sql, pc));
			}
			else
			{
				return(dbUtilAutoTran.selectMutliple(sql, pc));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getIncorrectStorageByCidPsid(row) error:" + e);
		}
	}
		
	public DBRow[] getStorageLocationByBarcode(long ps_id,long pc_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			long topFather = ps_id;
			
			
			DBRow para = new DBRow();
			para.add("pc_id", pc_id);
			para.add("ps_id", topFather);
			
			String sql = "select sl.pc_id,sl.sl_id,sl.barcode,sl.position,sum(sl.quantity) quantity,psc.title from  "+ConfigBean.getStringValue("storage_location")+"  sl, "+ConfigBean.getStringValue("product_storage_catalog")+"  psc where psc.id=sl.ps_id and sl.pc_id=? and ps_id=? group by position order by barcode asc";
			//System.out.println(sql);
			if (pc==null)
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStorageLocationByBarcode(row) error:" + e);
		}
	}
	
	public DBRow[] getStorageLocationByBarcodeMachineID(long ps_id,long pc_id,String machine_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			long topFather = ps_id;
			
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("ps_id", topFather);
			para.add("machine_id", machine_id);
			
			String sql = "select sl.pc_id,sl.sl_id,sl.barcode,sl.position,sum(sl.quantity) quantity,psc.title from  "+ConfigBean.getStringValue("storage_location")+"  sl, "+ConfigBean.getStringValue("product_storage_catalog")+"  psc where psc.id=sl.ps_id and sl.pc_id=? and ps_id=? and machine_id=? group by position order by barcode asc";
			//System.out.println(sql);
			if (pc==null)
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStorageLocationByBarcodeMachineID(row) error:" + e);
		}
	}
		
	public long addStorageLocationDifference(DBRow row)
		throws Exception
	{
		try
		{
			long sld_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("storage_location_difference"));
			row.add("sld_id",sld_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("storage_location_difference"),row);
			return(sld_id);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addStorageLocationDifference(row) error:" + e);
		}
	}
		
	public long addStorageApprove(DBRow row)
		throws Exception
	{
		try
		{
			long sa_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("storage_approve"));
			row.add("sa_id",sa_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("storage_approve"),row);
			return(sa_id);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addStorageApprove(row) error:" + e);
		}
	}
			
	public void delStorageLocationDifferenceBySaId(long sa_id)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where sa_id=" + sa_id,ConfigBean.getStringValue("storage_location_difference"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.delStorageLocationDifferenceBySaId(row) error:" + e);
		}
	}
	
	public void modStorageApprove(long sa_id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where sa_id=" + sa_id,ConfigBean.getStringValue("storage_approve"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.modStorageApprove(row) error:" + e);
		}
	}

	public void modStorageLocationDifference(long sld_id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where sld_id=" + sld_id,ConfigBean.getStringValue("storage_location_difference"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.modStorageLocationDifference(row) error:" + e);
		}
	}

	public DBRow[] getStorageApproveByPostDatePsId(String post_date,long ps_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("post_date",post_date);
			para.add("ps_id",ps_id);
			
			String sql = "select * from " + ConfigBean.getStringValue("storage_approve") + " where datediff(post_date, str_to_date( ? ,'%Y-%m-%d'))=0 and ps_id=? order by sa_id desc";
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStorageApproveByPostDatePsId(row) error:" + e);
		}
	}
	
	public DBRow[] getStorageApproveByPsId(long ps_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			
			String sql = "select * from " + ConfigBean.getStringValue("storage_approve") + " where ps_id=? order by sa_id desc";
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStorageApproveByPsId(row) error:" + e);
		}
	}
	
	public DBRow[] getAllStorageApproves(PageCtrl pc)
		throws Exception
	{
		try
		{
			
			String sql = "select * from "+ConfigBean.getStringValue("storage_approve")+"  order by sa_id desc";
			
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getAllStorageApproves(row) error:" + e);
		}
	}
	
	public DBRow[] getAllStorageApprovesSortByPostDate(long ps_id,int status,boolean sort,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql;

			String cond = "";
			
			if (ps_id>0)
			{
				cond += " and ps_id=" + ps_id;
			}
			
			if (status==1)
			{
				cond += " and difference_count=0";
			}
			else if (status==2)
			{
				cond += " and approve_status=0";
			}
			else if (status==3)
			{
				cond += " and approve_status=1";
			}
			
			if (sort)
			{
				sql = "select * from "+ConfigBean.getStringValue("storage_approve")+" where 1=1 "+cond+" order by post_date desc";
			}
			else
			{
				sql = "select * from "+ConfigBean.getStringValue("storage_approve")+" where 1=1 "+cond+" order by post_date asc";
			}
			
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getAllStorageApproves(row) error:" + e);
		}
	}
	
	public DBRow[] getAllStorageApprovesSortByApproveDate(long ps_id,int status,boolean sort,PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql;

			String cond = "";
			
			if (ps_id>0)
			{
				cond += " and ps_id=" + ps_id;
			}
			
			if (status==1)
			{
				cond += " and difference_count=0";
			}
			else if (status==2)
			{
				cond += " and approve_status=0";
			}
			else if (status==3)
			{
				cond += " and approve_status=1";
			}
			
			if (sort)
			{
				sql = "select * from "+ConfigBean.getStringValue("storage_approve")+" where 1=1 "+cond+" order by approve_date desc";
			}
			else
			{
				sql = "select * from "+ConfigBean.getStringValue("storage_approve")+" where 1=1 "+cond+" order by approve_date asc";
			}
			
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getAllStorageApprovesSortByApproveDate(row) error:" + e);
		}
	}
	
	/**
	 * 根据仓库和状态，过滤
	 * @param ps_id
	 * @param status
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getStorageApprovesByPsidStatus(long ps_id,int status,PageCtrl pc)
		throws Exception
	{
		try
		{
			String cond = "";
			
			if (ps_id>0)
			{
				cond += " and ps_id=" + ps_id;
			}
			
			if (status==1)
			{
				cond += " and difference_count=0";
			}
			else if (status==2)
			{
				cond += " and approve_status=0";
			}
			else if (status==3)
			{
				cond += " and approve_status=1";
			}
			
			String sql = "select * from "+ConfigBean.getStringValue("storage_approve")+" where 1=1 "+cond+" order by sa_id desc";
			
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStorageApprovesByPsidStatus(row) error:" + e);
		}
	}

	public DBRow[] getStorageLocationDifferencesBySaId(long sa_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("sa_id",sa_id);
			
			String sql = "SELECT sld.* FROM "+ConfigBean.getStringValue("storage_location_difference")+" sld "
						+"join "+ConfigBean.getStringValue("storage_approve")+" sa on sld.sa_id = sa.sa_id "
						+"join "+ConfigBean.getStringValue("product_storage")+" ps on  ps.cid = sa.ps_id and ps.pc_id = sld.pc_id "
						+"WHERE	sld.sa_id =? "
						+"ORDER BY approve_status DESC,sld_id DESC";
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStorageLocationDifferencesBySaId(row) error:" + e);
		}
	}
		
	public DBRow[] getStorageLocationScannerByPsId(long ps_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("ps_id",ps_id);
			String sql = "select machine_id from "+ConfigBean.getStringValue("storage_location")+" where ps_id=? group by ps_id,machine_id";
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStorageLocationScannerByPsId(row) error:" + e);
		}
	}
	
	public DBRow[] getStorageLocationByPsIdMachineID(long ps_id,String machine_id,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("ps_id", ps_id);
			
			String sql;
			if (machine_id.equals(""))
			{
				sql = "select sl.machine_id,sl.sl_id,sl.barcode,sl.position,sum(sl.quantity) quantity,psc.title from "+ConfigBean.getStringValue("storage_location")+" sl,"+ConfigBean.getStringValue("product_storage_catalog")+" psc where psc.id=sl.ps_id and ps_id=?  group by machine_id,barcode order by barcode asc";	
			}
			else
			{
				para.add("machine_id", machine_id);
				sql = "select sl.machine_id,sl.sl_id,sl.barcode,sl.position,sum(sl.quantity) quantity,psc.title from "+ConfigBean.getStringValue("storage_location")+" sl,"+ConfigBean.getStringValue("product_storage_catalog")+" psc where psc.id=sl.ps_id and ps_id=? and machine_id=? group by barcode order by barcode asc";				
			}
			//System.out.println(sql);
			if (pc==null)
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para));
			}
			else
			{
				return(dbUtilAutoTran.selectPreMutliple(sql,para,pc));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStorageLocationByPsIdMachineID(row) error:" + e);
		}
	}
	
	public void delStorageLocation(String sl_ids[])
		throws Exception
	{
		try
		{
			if (sl_ids!=null&&sl_ids.length>0)
			{
				int i=0;
				StringBuffer sb = new StringBuffer();
				for (;i<sl_ids.length-1; i++)
				{
					sb.append(sl_ids[i]);
					sb.append(",");
				}
				sb.append(sl_ids[i]);
				
				dbUtilAutoTran.delete("where sl_id in ("+sb.toString()+") " ,ConfigBean.getStringValue("storage_location"));
			}
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.delStorageLocation(row) error:" + e);
		}
	}
		
	public int getApproveCountBySaid(long sa_id)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("sa_id",sa_id);
			
			String sql = "select count(sld_id) c from "+ConfigBean.getStringValue("storage_location_difference")+" where sa_id=? and approve_status=1";
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			
			return(rowo.get("c", 0));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getApproveCountBySaid error:" + e);
		}
	}
	
	public int getNotApproveCountBySaid(long sa_id)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("sa_id",sa_id);
			
			String sql = "select count(sld_id) c from "+ConfigBean.getStringValue("storage_location_difference")+" where sa_id=? and approve_status=0";
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			
			return(rowo.get("c", 0));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getNotApproveCountBySaid error:" + e);
		}
	}
	
	public DBRow[] getFreeStorageProvinces(long psid)
		throws Exception
	{
		try
		{
			//select * from (select cp.* from product_storage_catalog psc,country_code cc,country_province cp where psc.id=100000 and psc.native=cc.ccid and cc.ccid=cp.nation_id ) ncp LEFT JOIN storage_province sp on sp.pro_id=ncp.pro_id where sp.sid=100000  or sp.sid is null   order by ncp.pro_name asc
			String sql = "select * from (select cp.* from " + ConfigBean.getStringValue("product_storage_catalog") + " psc," + ConfigBean.getStringValue("country_code") + " cc," + ConfigBean.getStringValue("country_province") + " cp where psc.id=? and psc.native=cc.ccid and cc.ccid=cp.nation_id ) ncp LEFT JOIN " + ConfigBean.getStringValue("storage_province") + " sp on sp.pro_id=ncp.pro_id where sp.sid=?  or sp.sid is null   order by ncp.pro_name asc";
			
			DBRow para = new DBRow();
			para.add("psid1",psid);
			para.add("psid2",psid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getFreeStorageProvinces(row) error:" + e);
		}
	}
		
	public DBRow[] getStorageProvinceBySid(long sid)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("storage_province") + " sc," + ConfigBean.getStringValue("country_province") + " c where sc.pro_id=c.pro_id and sc.sid=? order by c.pro_name asc";
			DBRow para = new DBRow();
			para.add("sid",sid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStorageProvinceBySid(row) error:" + e);
		}
	}
		
	public DBRow[] getStorageProvinceByCcid(long ccid)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("country_province") + " where nation_id=? order by pro_name asc";
			DBRow para = new DBRow();
			para.add("ccid",ccid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStorageProvinceByCcid(row) error:" + e);
		}
	}
		
	public DBRow[] getDevProductStorageByNative(long ccid)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " where native=? and dev_flag=1";
			DBRow para = new DBRow();
			para.add("ccid",ccid);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getProductStorageByNative(row) error:" + e);
		}
	}
		
	public DBRow getDetailDevProductStorageByProvince(long ccid,long pro_id)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("product_storage_catalog") + " psc," + ConfigBean.getStringValue("storage_province") + " sp where psc.dev_flag=1 and psc.native=? and psc.id=sp.sid and sp.pro_id=?";

			DBRow para = new DBRow();
			para.add("ccid",ccid);
			para.add("pro_id",pro_id);
			
			return(dbUtilAutoTran.selectPreSingle(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailDevProductStorageByProvince(row) error:" + e);
		}
	}
	
	/**
	 * 通过省份代码，获得省份详细信息 
	 * @param p_code
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProvinceByPCode(String p_code)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("country_province") + " where p_code=?";
	
			DBRow para = new DBRow();
			para.add("p_code",p_code);
			
			return(dbUtilAutoTran.selectPreSingle(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProvinceByPCode(row) error:" + e);
		}
	}
	
	public DBRow getDetailProvinceByNationIDPCode(long nation_id,String p_code)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("country_province") + " where nation_id=? and p_code=?";
	
			DBRow para = new DBRow();
			para.add("nation_id",nation_id);
			para.add("p_code",p_code);
			
			return(dbUtilAutoTran.selectPreSingle(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProvinceByNationIDPCode(row) error:" + e);
		}
	}
	
	/**
	 * 根据国家ID、区域名称搜索区域
	 * @param nation_id
	 * @param pro_name
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailProvinceByNationIDPName(long nation_id,String pro_name)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("country_province") + " where nation_id=? and pro_name=?";
	
			DBRow para = new DBRow();
			para.add("nation_id",nation_id);
			para.add("pro_name",pro_name);
			
			return(dbUtilAutoTran.selectPreSingle(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProvinceByNationIDPCode(row) error:" + e);
		}
	}
	
	public DBRow getDetailProvinceByProId(long pro_id)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("country_province") + " where pro_id=?";
	
			DBRow para = new DBRow();
			para.add("pro_id",pro_id);
			
			return(dbUtilAutoTran.selectPreSingle(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailProvinceByPCode(row) error:" + e);
		}
	}
	
	public long addCountryArea(DBRow row)
		throws Exception
	{
		try
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("country_area"));
			row.add("ca_id",id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("country_area"),row);
			return(id);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addCountryArea(row) error:" + e);
		}
	}
		
	public void modCountryArea(long ca_id,DBRow row)
		throws Exception
	{
		try
		{
			dbUtilAutoTran.update("where ca_id=" + ca_id,ConfigBean.getStringValue("country_area"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.modCountryArea(row) error:" + e);
		}
	}
		
	public void delCountryAreaByCaId(long ca_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("ca_id", ca_id);
			dbUtilAutoTran.deletePre("where ca_id=?", para, ConfigBean.getStringValue("country_area"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.delCountryAreaByCaId(row) error:" + e);
		}
	}
	
	public void addCountryAreaMapping(long ca_id,long ccid)
		throws Exception
	{
		try
		{
			DBRow row = new DBRow();
			row.add("ca_id",ca_id);
			row.add("ccid",ccid);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("country_area_mapping"),row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.addCountryAreaMapping(row) error:" + e);
		}
	}
	
	public void delCountryAreaMappingByCaId(long ca_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("ca_id", ca_id);
			dbUtilAutoTran.deletePre("where ca_id=?", para, ConfigBean.getStringValue("country_area_mapping"));
		}
		catch (Exception e)
		{
			throw new Exception("FloorCatalogMgr.delCountryAreaMappingByCaId(row) error:" + e);
		}
	}

	public DBRow[] getCountryAreaMappingByCaId(long ca_id)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("country_area") + " ca," + ConfigBean.getStringValue("country_area_mapping") + " cam," + ConfigBean.getStringValue("country_code") + " c where ca.ca_id=cam.ca_id and cam.ccid=c.ccid and cam.ca_id=? order by ca.ca_id desc";
			DBRow para = new DBRow();
			para.add("ca_id",ca_id);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getCountryAreaMappingByCaId(row) error:" + e);
		}
	}
		
	public DBRow[] getFreeAreaCountrys(long ca_id)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("country_code") + " c LEFT JOIN "+ConfigBean.getStringValue("country_area_mapping")+" sc2 on sc2.ccid=c.ccid where sc2.ca_id=? or sc2.ca_id is null   order by c.c_country asc";
									
			DBRow para = new DBRow();
			para.add("ca_id",ca_id);
			
			return(dbUtilAutoTran.selectPreMutliple(sql,para));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getFreeAreaCountrys(row) error:" + e);
		}
	}
	
	public DBRow[] getAllCountryAreas(PageCtrl pc)
		throws Exception
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("country_area") + "   order by ca_id desc";
			return(dbUtilAutoTran.selectMutliple(sql,pc));
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getAllCountryAreas(row) error:" + e);
		}
	}

	public DBRow getDetailCountryAreaByCaId(long ca_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("ca_id",ca_id);
			
			String sql = "select * from " + ConfigBean.getStringValue("country_area") + " where ca_id=?";
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
//			DBRow rowo = dbUtilAutoTran.selectPreSingleCache(sql,para,new String[]{ConfigBean.getStringValue("country_area")});
			return(rowo);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getDetailCountryAreaByCaId error:" + e);
		}
	}
	
	/**
	 * 获得代发关系
	 * @param ps_id
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getStorageTransd(long ps_id,long catalog_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from storage_transpond as st "
						+"join pc_child_list as pcl on st.catalog_id = pcl.search_rootid and pcl.pc_id=? and y_sid=?";
			
			DBRow para = new DBRow();
			para.add("catalog_id",catalog_id);
			para.add("ps_id",ps_id);
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e)
		{
			throw new Exception("FloorProductMgr.getStorageTransd error:"+e);
		}
	}
	
	public DBRow[] getAllShippingCompanyByPsId(long psId)throws Exception{
		try
		{
			String sql =  "select  * from " + ConfigBean.getStringValue("shipping_company") + " where ps_id="+psId;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgr.getAllShippingCompanyByPsId error:"+e);
		}
	}
	
	/**
	 * 获得商品详细，除商品详细外包括分类名，产品线名，产品线ID
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getAllDetailProductByPcid(long pc_id)
		throws Exception
	{
		String sql = "select p.*,pc.product_line_id,pc.title as catalog_name,pld.name as product_line_name from product as p "
					+"left join product_catalog as pc on p.catalog_id = pc.id "
					+"left join product_line_define as pld on pc.product_line_id = pld.id "
					+"where p.pc_id =? ";
		
		DBRow para = new DBRow();
		para.add("pc_id",pc_id);
		
		return (dbUtilAutoTran.selectPreSingle(sql, para));
	}
	
	/**
	 * 根据业务单据查询所有非取消日志
	 * @param oid
	 * @param bill_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getAllProductStoreLogsByOidAndType(long oid,int bill_type)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("product_store_logs")+" where oid = ? and bill_type = ? ";
			
			DBRow para = new DBRow();
			para.add("oid",oid);
			para.add("bill_type",bill_type);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgr getAllProductStoreLogsByOidAndType error:"+e);
		}
		
	}
	
	public DBRow[] todayReback()
	throws Exception
	{
		String sql = "select * from product_store_logs where post_date >'2013-04-07 21:00:00' and quantity_type =1 order by psl_id";
		
		return dbUtilAutoTran.selectMutliple(sql);
	}
	
	/**
	 * 通过条码类型及条码查询条码个数
	 */
	public DBRow[] getProductCodeByTypeAndCode(int codeType, String pcCode) throws Exception
	{
		try
		{
			String sql = "select * from " +ConfigBean.getStringValue("product_code")+" pcode where 1=1 ";
			if(codeType > 0)
			{
				sql += " and pcode.code_type = "+codeType;
			}
			if(null != pcCode && !"".equals(pcCode))
			{
				sql += " and pcode.p_code = '" + pcCode+"'";
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorProductMgr.getProductCodeByTypeAndCode(int codeType, String code) error:"+e);
		}
		
	}
	
	
	/**
	 * 返回简单的ProductInfo ,left Join ProductCode(Main Code)
	 * @param pcid
	 * @return
	 * @throws Exception
	 */
	public DBRow getSimpleProductInfoByPcId(Long pcid ) throws Exception{
		try{
			String sql = "select p.* , pc.p_code from " + ConfigBean.getStringValue("product") + " as p left join ";
				  sql += ConfigBean.getStringValue("product_code") + " as pc on pc.pc_id = p.pc_id " ;
				  sql += "where p.pc_id="+pcid+"  and pc.code_type = 1";
  				return dbUtilAutoTran.selectSingle(sql );
		}catch (Exception e) {
			throw new Exception("FloorProductMgr.getSimpleProductInfoByPcId(pcid) error:"+e);
		}
	}
	/**
	 * 1.利用商品名来查询
	 * @param keyword
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSimpleProductInfoByPName(String keyword) throws Exception{
		try{
			String sql = "select p.* , pc.p_code from " + ConfigBean.getStringValue("product") + " as p left join ";
			  sql += ConfigBean.getStringValue("product_code") + " as pc  on pc.pc_id = p.pc_id " ;
			  sql += "where p.p_name like '%"+keyword.trim()+"%' and pc.code_type = 1" ;
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorProductMgr.getSimpleProductInfoByPName(keyword) error:"+e);
 		}
	}
	/**
	 * 1.利用product_code
	 * @param keyWord
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSimpleProductInfoByPCode(String keyWord) throws Exception{
		try{
			String sql = "select  pc.p_code ,p.*  from product_code as pc LEFT JOIN product as p  on pc.pc_id = p.pc_id where pc.p_code like '%"+keyWord.trim()+"%'";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorProductMgr.getSimpleProductInfoByPCode(keyword) error:"+e);
 		}
	}
	
		/*
	    * 根据pc_id 删除product_sn表中记录
	    * Yuanxinyu
	    * */
	    public void delProductSn(long pc_id)
	            throws Exception
	    {
	        try
	        {
	            dbUtilAutoTran.delete("where pc_id=" + pc_id,ConfigBean.getStringValue("product_sn"));
	        }
	        catch (Exception e)
	        {
	            throw new Exception("FloorProductMgr.delProductSn(long pc_id) error:" + e);
	        }
	    }
	    /*
	    * 插入product_sn表
	    * Yuanxinyu
	    * */
	    public long addProductSn(DBRow row)
	            throws Exception
	    {
	        try
	        {
	            return dbUtilAutoTran.insertReturnId("product_sn",row);
	        }
	        catch (Exception e)
	        {
	            throw new Exception("FloorProductMgr.addProductSn(DBRow row) error:" + e);
	        }
	    }
	    /*
	   * 根据pc_id 查询product_sn表
	   * Yuanxinyu
	   * */
	    public DBRow[] getProductSnByPcId(long pc_id)throws Exception{
	        try
	        {
	            String sql =  "select  * from " + ConfigBean.getStringValue("product_sn") + " where pc_id="+pc_id;
	            return dbUtilAutoTran.selectMutliple(sql);
	        }
	        catch (Exception e)
	        {
	            throw new Exception("FloorProductMgr.getProductSnByPcId(long pc_id) error:"+e);
	        }
	    }

	    /**
	     * 查询套装商品所含的子商品数量
	     * @param set_pid
	     * @return
	     * @throws Exception
	     * @author:zyj
	     * @date: 2015年4月7日 下午12:07:37
	     */
	    public int getProductUnionCountByPid(long set_pid) throws Exception
    	{
    		try
    		{
    			String sql = "select count(*) cn from " + ConfigBean.getStringValue("product_union") + " where set_pid= "+set_pid;
    			DBRow row = dbUtilAutoTran.selectSingle(sql);
    			if(null != row)
    			{
    				return row.get("cn", 0);
    			}
    			return 0;
    		}
    		catch (Exception e)
    		{
    			throw new Exception("FloorProductMgr.getProductUnionCountByPid(long) error:" + e);
    		}
    	}
	    
	 // adid 查询 customer_id
		public DBRow getCustomerId(long adid)
				throws Exception
			{
				try
				{
					String sql = "select customer_id from admin where adid ="+adid;
					
					return(dbUtilAutoTran.selectSingle(sql));
				}
				catch (Exception e)
				{
					throw new Exception("FloorProductMgr.getCustomerId(sql) error:" + e);
				}
			}
		
		//adid 下的第一个title id
		public DBRow getTitleAdmin(long adid)
				throws Exception
			{
				try
				{
					String sql = "select * from title_admin where title_admin_adid ="+adid+" And title_admin_sort= 1";
					
					return(dbUtilAutoTran.selectSingle(sql));
				}
				catch (Exception e)
				{
					throw new Exception("FloorProductMgr.getTitleAdmin(sql) error:" + e);
				}
			}
		//Customer 的title 插入 title_product
		public long addProductTitle(DBRow row) throws Exception
			  {
			    try
			    {
			      return this.dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("title_product"), row);
			    }
			    catch (Exception e) {
			    	 throw new Exception("FloorProductMgr.addProductSn(DBRow row) error:" + e);
			    }
			   
			  }

		
	public long addProductFile(DBRow row) throws Exception{
		
		try {
			
			return dbUtilAutoTran.insertReturnId("product_file", row);
	      
	    } catch (Exception e) {
	    	
	    	e.printStackTrace();
	    	
	    	throw new Exception("FloorProductMgr.addProductFile(DBRow row) error:" + e);
	    }
	}
	

	
	/**
	 * 根据字符串生成相应的in查询条件字符串
	 * @param str
	 * @return
	 */
	private String getInStr(String str){
		String result = "";
		String[] items = str.split(",");
		for(String item : items){
			result += "'" + item + "'" + ",";
		}
		if(result.length() > 0){
			result = result.substring(0, result.length() - 1);
		}
		return result;
	}
	
	
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	//------操作图数据库--------
	/**
	 * 添加节点，如果在nodeProps中指定了名为 id_attribute 的 键,则会给 节点添加一个名为id_attribute指定值的属性，属性值为节点在图数据库中的ID。如id_attribute对应的值为categoryId,
	 * 则会给节点新添一个属性  categoryId
	 * @param ps_id	                 指定存放在哪个仓库
	 * @param nodeType	     节点类型
	 * @param nodeProps   节点属性
	 */
	public long addNode(long ps_id, NodeType nodeType, Map<String, Object> nodeProps){
		long id = 0L;
		if(nodeProps == null){
			nodeProps = new HashMap<String,Object>();
		}
		
		try {
			 //检查属性中是否包括名为ID_ATTRIBUTE 的键；如果有，则需要在添加节点时，额外添加 ID属性 ,id由图数据库自动生成
			 boolean  has_id_attribute = false;
			 JSONObject  result = null;

			 result = transaction(ps_id, new JSONArray(new JSONObject[] { statement(nodeProps, new String[] { new StringBuilder().append("CREATE (n:").append(nodeType).append(paramPlaceholders(nodeProps)).append(" )  return id(n)").toString() }) }));
			 
		     id = result.getJSONArray("results").getJSONObject(0).getJSONArray("data").getJSONObject(0).getJSONArray("row").getLong(0);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	/**
	 * 
	 * @param ps_id         仓库ID
	 * @param statements    要提交的语句及值
	 * @return    执行语句返回的结果
	 * @throws Exception
	 */
    private  JSONObject transaction(long ps_id, JSONArray statements)
			throws Exception {
		return new Neo4jTransaction(this.restBaseUrl + "/" + ps_id, this.client)
				.commit(statements);
	}
    
	private JSONObject statement(Map<String, Object> params, String... lines)
			throws Exception {
		return new JSONObject()
				.put("statement", StringUtils.join(lines, " \n")).put(
						"parameters", params);
	}
	
	private String paramPlaceholders(Map<String, Object> props) {
		if(props.isEmpty())return "{}";
		return paramPlaceholders(props, "{", "", ":", "", ",", "}");
	}
	
	private String paramPlaceholders(Map<String, Object> props,
			String bracketBegin, String fieldPrefix, String assign,
			String paramPrefix, String joinBy, String bracketEnd) {
		List<String> propKeys = new ArrayList<String>();
		for (String k : props.keySet()) {
			propKeys.add(fieldPrefix + k + assign + "{" + paramPrefix + k + "}");
		}
		return bracketBegin + StringUtils.join(propKeys, joinBy) + bracketEnd;
	}
    

	public void setRestBaseUrl(String restBaseUrl) {
		this.restBaseUrl = restBaseUrl;
	}

	public void setPsId(long psId) {
		this.psId = psId;
	}

	/**
	 * 构造一个与原对象拥有相同数据的新对象,只是将指定的DBRow 对象中的key值转变为小写，
	 * @param row
	 * @return
	 */
	private Map<String,Object>  convertToLower(DBRow row){
		Map result = new HashMap();
		for(String key : row.keySet()){
			result.put(key.toLowerCase(), row.get(key));
		}
		return result;
	}
    
    
    //------操作图数据库--------
}









