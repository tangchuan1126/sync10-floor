package com.cwc.app.floor.api.zj;

import com.cwc.app.key.FinanceApplyTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorApplyMoneyMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBRow[] getApplyMoneyByAssociationIdAndType(long association_id,int association_type)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("apply_money")+" where association_id = ? and association_type_id = ?";
			
			DBRow para = new DBRow();
			para.add("association_id",association_id);
			if(association_type==11)
			{
				sql = "select * from "+ConfigBean.getStringValue("apply_money")+" where association_id = ? and(association_type_id = ? or association_type_id = ?)";
				
				para.add("association_type1",FinanceApplyTypeKey.TRANSPORT_ORDER);
				para.add("association_type2",FinanceApplyTypeKey.DELIVERY_ORDER);
			}
			else
			{
				para.add("association_type",association_type);
			}
			
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorApplyMoneyMgrZJ getApplyMoneyByAssociationIdAndType error:"+e);
		}
	}
	
	/**
	 * 根据资金申请ID获得资金申请
	 * @param apply_money_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getApplyMoneyById(long apply_money_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("apply_money")+" where apply_id = ?";
			
			DBRow para = new DBRow();
			para.add("apply_id",apply_money_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorApplyMoneyMgrZJ getApplyMoneyById error:"+e);
		}
	}
	
	/**
	 * 根据apply_money_id获得转账申请
	 * @param apply_money_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getApplyTransferForApplyMoneyId(long apply_money_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("apply_transfer")+" where apply_money_id =?";
			
			DBRow para = new DBRow();
			para.add("apply_money_id",apply_money_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorApplyMoneyMgrZJ getApplyTransferForApplyMoneyId error:"+e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
}
