package com.cwc.app.floor.api;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorHelpCenterMgr
{
	private DBUtilAutoTran dbUtilAutoTran;
	
	public DBRow[] getTopicsByCid(long cid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			para.add("cid",cid);
			
			String sql = "select * from " + ConfigBean.getStringValue("help_center_topic") + " where cid=? order by hct_id desc";
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getTopicsByCid() error:" + e);
		}
	}
	
	public DBRow[] getTopicsByCidSortAsc(long cid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			para.add("cid",cid);
			
			String sql = "select * from " + ConfigBean.getStringValue("help_center_topic") + " where cid=? and issue=1 order by hct_id asc";
			
			if (pc!=null)
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getTopicsByCidSortAsc() error:" + e);
		}
	}
		
	public DBRow getDetailTopicsByHTCID(long hct_id)
		throws Exception
	{
		try
		{
			DBRow para = new DBRow();
			para.add("hct_id",hct_id);
			return(dbUtilAutoTran.selectPreSingle("select * from " + ConfigBean.getStringValue("help_center_topic") + " where hct_id=?",para));
		}
		catch (Exception e)
		{
			throw new Exception("getDetailTopicsByHTCID(row) error:" + e);
		}
	}

	public DBRow[] getCatalogByParentid(long parentid,PageCtrl pc)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			para.add("parentid",parentid);

			String sql = "select * from " + ConfigBean.getStringValue("help_center_catalog") + " where parentid=? order by id asc";

			if (pc!=null)
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para,pc);
			}
			else
			{
				row = dbUtilAutoTran.selectPreMutliple(sql,para);
			}
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("getCatalogByParentid() error:" + e);
		}
	}

	/**
	 * 添加帮助中心分类
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addCatalog(DBRow dbrow) 
		throws Exception{
		try 
		{
			long id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("help_center_catalog"));
			dbrow.add("id",id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("help_center_catalog"),dbrow);
			
			return (id);
		} 
		catch (RuntimeException e) 
		{
			// TODO Auto-generated catch block
			throw new Exception("FloorHelpCenterMgr.addCatalog error:"+e);
		}
	}

	/**
	 * 根据标题名寻找分类
	 * @param title
	 * @return
	 * @throws Exception
	 */
	public DBRow getCatalogByTitle(String title,long parentid)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("title",title);
			para.add("parentid",parentid);
			
			String sql = "select * from " + ConfigBean.getStringValue("help_center_catalog") + " where title=? and parentid=?";
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			return(rowo);
		}
		catch (Exception e)
		{
			throw new Exception("FloorHelpCenterMgr.getCatalogByTitle error:" + e);
		}
	}

	/**
	 * 根据Id获得帮助中心分类
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailCatalogById(long id)
		throws Exception{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("help_center_catalog")+" where id=?";
			DBRow para = new DBRow();
			para.add("id",id);
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			
			return (rowo);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorHelpCenterMgr.getCatalogById error:"+e);
		}
	}

	/**
	 * 修改帮助中心分类
	 * @param id
	 * @param dbrow
	 * @throws Exception
	 */
	public void modCatalog(long id,DBRow dbrow)
		throws Exception{
		try 
		{
			dbUtilAutoTran.update("where id="+id,ConfigBean.getStringValue("help_center_catalog"),dbrow);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorHelpCenterMgr.modCatalog error:"+e);
		}
	}
	

	
	/**
	 * 删除帮助中心分类
	 * @param id
	 * @throws Exception
	 */
	public void delCatalog(long id) 
		throws Exception
	{
		try
		{
			dbUtilAutoTran.delete("where id="+id,ConfigBean.getStringValue("help_center_catalog"));
		}
		catch (RuntimeException e)
		{
			throw new Exception("FloorHelpCenterMgr.delCatalog error:"+e);
		}
	}
	
	/**
	 * 根据父类ID查询子类按索引排序
	 * @param parentid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCatalogByParentidOrderSort(long parentid)
		throws Exception
	{
		try
		{
			DBRow row[];
			DBRow para = new DBRow();
			para.add("parentid",parentid);
	
			String sql = "select * from " + ConfigBean.getStringValue("help_center_catalog") + " where parentid=? order by sort asc";
			row = dbUtilAutoTran.selectPreMutliple(sql,para);
			
			
			return(row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorHelpCenterMgr.getCatalogByParentidOrderSort error:" + e);
		}
	}
	
	/**
	 * 判断该类别是否是底级分类
	 * @param catalog_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCatalogIsParent(long catalog_id)
		throws Exception
	{
		String sql="select * from "+ConfigBean.getStringValue("help_center_catalog")+" where parentid=0 and id= ?";
		
		DBRow para=new DBRow();
		para.add("id",catalog_id);
		
		return (dbUtilAutoTran.selectPreMutliple(sql,para));
	}
	
	/**
	 * 文章列表，可根据分类，也可查看全部分类
	 * @param parentid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	
	public DBRow[] getTopics(long cid,PageCtrl pc)
		throws Exception
	{
		try
		{
			StringBuffer sql=new StringBuffer("select * from "+ConfigBean.getStringValue("help_center_topic")+" where 1=1");
			if(cid!=0)
			{
				sql.append(" and cid in (select id from "+ConfigBean.getStringValue("help_center_catalog")+" where parentid="+cid+" or id="+cid+")");
			}
			sql.append(" order by hct_id desc");
			
			if(pc==null)
			{
				return (dbUtilAutoTran.selectMutliple(sql.toString()));
			}
			else
			{
				return (dbUtilAutoTran.selectMutliple(sql.toString(),pc));
			}
			
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorHelpCenterMgr.getTopics error"+e);
		}
	}
	
	/**
	 * 添加文章
	 * @param dbrow
	 * @return
	 * @throws Exception
	 */
	public long addTopic(DBRow dbrow)
		throws Exception
	{
		try
		{
			long hct_id = dbUtilAutoTran.getSequance(ConfigBean.getStringValue("help_center_topic"));
			dbrow.add("hct_id",hct_id);
			dbUtilAutoTran.insert(ConfigBean.getStringValue("help_center_topic"),dbrow);
			return (hct_id);
		}
		catch (Exception e)
		{
			throw new Exception("FloorHelpCenterMgr.addTopic error:"+e);
		}
	}
	
	
	/**
	 * 根据标题名寻找文章
	 * @param title
	 * @return
	 * @throws Exception
	 */
	public DBRow getTopicByTitle(String title,long parentid)
		throws Exception
	{
		try
		{
			
			DBRow para = new DBRow();
			para.add("title",title);
			para.add("parentid",parentid);
			
			String sql = "select * from " + ConfigBean.getStringValue("help_center_topic") + " where title=? and cid=?";
			DBRow rowo = dbUtilAutoTran.selectPreSingle(sql,para);
			return(rowo);
		}
		catch (Exception e)
		{
			throw new Exception("FloorHelpCenterMgr.getTopicByTitle error:" + e);
		}
	}
	
	/**
	 * 发布，取消发布
	 * @param ids
	 * @param para
	 * @throws Exception
	 */
	public void modTopicIssue(String[] ids,DBRow para)
		throws Exception
	{
		try 
		{
			StringBuffer id=new StringBuffer();
			for(int i=0;i<ids.length;i++)
			{
				id.append(ids[i]);
				if(i<ids.length-1)
				{
					id.append(",");
				}
			}
			dbUtilAutoTran.update("where hct_id in("+id+")",ConfigBean.getStringValue("help_center_topic"),para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorHelpCenterMgr.modTopicIssue error:"+e);
		}
	}
	
	/**
	 * 删除文章
	 * @param id
	 * @throws Exception
	 */
	public void delTopic(long id) 
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete("where hct_id="+id,ConfigBean.getStringValue("help_center_topic"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorHelpCenterMgr.delTopic error"+e);
		}
	}
	
	/**
	 * 修改文章
	 * @param id
	 * @param param
	 * @throws Exception
	 */
	public void modTopic(long id,DBRow param)
		throws Exception
	{
		try 
		{
			dbUtilAutoTran.update("where hct_id="+id,ConfigBean.getStringValue("help_center_topic"),param);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorHelpCenterMgr.modTopic error"+e);
		}
	}
	
	/**
	 * 修改文章分类（分类批量合并）
	 * @param ids
	 * @param para
	 * @throws Exception
	 */
	public void modTopicCatalog(String[] ids,DBRow para)
		throws Exception
	{
		StringBuffer id=new StringBuffer();
		for(int i=0;i<ids.length;i++)
		{
			id.append(ids[i]);
			if(i<ids.length-1)
			{
				id.append(",");
			}
		}
		dbUtilAutoTran.update("where hct_id in ("+id+")",ConfigBean.getStringValue("help_center_topic"),para);
	}
	
	/**
	 * 文章列表，可根据分类，也可查看全部分类，已发布的
	 * @param parentid
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	
	public DBRow[] getTopicsUse(long cid,PageCtrl pc)
		throws Exception
	{
		try
		{
			StringBuffer sql = new StringBuffer("select * from "+ConfigBean.getStringValue("help_center_topic")+" where issue=1 ");
			if(cid!=0)
			{
				sql.append(" and cid in (select id from "+ConfigBean.getStringValue("help_center_catalog")+" where parentid= "+cid+" or id= "+cid+")");
			}
			sql.append(" order by hct_id desc");
			return (dbUtilAutoTran.selectMutliple(sql.toString(),pc));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorHelpCenterMgr.getTopicsUse error"+e);
		}
	}
	
	/**
	 * 根据索引查询文章，返回已发布的
	 * @param hct_ids
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getSearchTopics4CTUse(String[] hctids,PageCtrl pc)
		throws Exception
	{
		StringBuffer paramhctids = new StringBuffer();
		for(int i=0;i<hctids.length;i++)
		{
			paramhctids.append(hctids[i]);
			if(i<hctids.length-1)
			{
				paramhctids.append(",");
			}
		}
		
		String sql="select * from "+ConfigBean.getStringValue("help_center_topic")+" where issue=1 and hct_id in("+paramhctids.toString()+") order by hct_id desc";
		
		return (dbUtilAutoTran.selectMutliple(sql,pc));
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran)
	{
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
