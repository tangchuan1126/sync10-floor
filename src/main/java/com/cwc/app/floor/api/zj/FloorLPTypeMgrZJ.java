package com.cwc.app.floor.api.zj;

import com.cwc.app.key.MoreLessOrEqualKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorLPTypeMgrZJ {
	private DBUtilAutoTran dbUtilAutoTran;
	
	public long addBoxType(DBRow row)
		throws Exception
	{
		try 
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("license_plate_type"),row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ addBoxType error:"+e);
		}
	}
	
	public long addClpType(DBRow row)
		throws Exception  
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("clp_type"), row);
 		}
		catch(Exception e)
		{
			throw new Exception("FloorLPTypeMgrZJ addClpType error:"+e);
		}
	}
	
	public long addCLPPs(DBRow clp_ps)
		throws Exception
	{
		return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("clp_ps"),clp_ps);
	}
	/**
	 * 获得SKU到卖场的可使用CLP
	 * @param pc_id
	 * @param title_id
	 * @param to_ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCLPTypeForSkuToShip(long pc_id,long title_id,long to_ps_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("clp_type")+" as ct " 
						+"join "+ConfigBean.getStringValue("clp_ps")+" as cs on ct.sku_lp_type_id = cs.clp_type_id " 
						+"join "+ConfigBean.getStringValue("ship_to")+" as st on cs.clp_ship_to_id = st.ship_to_id "
						+"join "+ConfigBean.getStringValue("product_storage_catalog")+" as psc on psc.ship_to_id = st.ship_to_id "
						+" where  psc.id = ? and ct.sku_lp_pc_id = ? and ct.sku_lp_title_id = ? order by cs.clp_sort asc";
			
			DBRow para = new DBRow();
			para.add("to_ps_id",to_ps_id);
			para.add("sku_lp_pc_id",pc_id);
			para.add("sku_lp_title_id",title_id);
			
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ getCLPTypeForSkuToShip error:"+e);
		}
	}
	
	public DBRow[] getCLPTypeForSKUShipTo(long pc_id,long ship_to_id)
		throws Exception
	{
		try 
		{
			String sql;
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			
			if (ship_to_id!=-1)
			{
						
				sql = "select distinct ct.*,containerType.*,cs.*,st.*,tt.title_name from "+ConfigBean.getStringValue("license_plate_type")+" as ct "
						 +" join "+ConfigBean.getStringValue("container_type")+" as containerType on containerType.type_id = ct.basic_type_id "
						 +" left join "+ConfigBean.getStringValue("lp_title_ship_to")+" as cs on ct.lpt_id = cs.lp_type_id "
						 +" left join "+ConfigBean.getStringValue("ship_to")+" as st on cs.ship_to_id = st.ship_to_id "
						 +" left join title as tt on tt.title_id =ct.lp_title_id"
						 +" where ct.pc_id = ? and cs.ship_to_id = ? AND ct.container_type= 1  order by cs.ship_to_id,cs.ship_to_sort";
				para.add("ship_to_id",ship_to_id);
			}
			else
			{
				sql = "select distinct ct.*,containerType.*,cs.*,st.*,tt.title_name from "+ConfigBean.getStringValue("license_plate_type")+" as ct "
					 +" join "+ConfigBean.getStringValue("container_type")+" as containerType on containerType.type_id = ct.basic_type_id "
					 +" left join "+ConfigBean.getStringValue("lp_title_ship_to")+" as cs on ct.lpt_id = cs.lp_type_id "
					 +" left join "+ConfigBean.getStringValue("ship_to")+" as st on cs.ship_to_id = st.ship_to_id "
					 +" left join title as tt on tt.title_id =ct.lp_title_id"
					 +" where ct.pc_id = ?  AND ct.container_type= 1  order by cs.ship_to_id,cs.ship_to_sort";
			}
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ getCLPTypeForSKUShipTo error:"+e);
		}
				
	}
	
	public DBRow[] getCLPTypeForSku(long pc_id,long title_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("clp_type")+" where sku_lp_pc_id = ? and sku_lp_title_id = ? order by sku_lp_sort asc";
			
			DBRow para = new DBRow();
			para.add("sku_lp_pc_id",pc_id);
			para.add("sku_lp_title_id",title_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ getCLPTypeForSkuToShip error:"+e);
		}
	}
	
	/**
	 * 获得SKU到卖场可以使用的BLP
	 * @param pc_id
	 * @param to_ps_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBLPTypeForSkuToShip(long pc_id,long to_ps_id)
		throws Exception
	{
		try 
		{
			String sql = "select bt.* from "+ConfigBean.getStringValue("box_ps")+" as bp "
						+"join "+ConfigBean.getStringValue("box_type")+" bt on bp.box_type_id = bt.box_type_id "
						+"where bp.box_pc_id = ? and bp.to_ps_id = ? order by bp.box_sort asc ";
			
			DBRow para = new DBRow();
			para.add("box_pc_id",pc_id);
			para.add("to_ps_id",to_ps_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ getBLPTypeForSkuToShip error:"+e);
		}
	}
	
	/**
	 * 根据CLPTypeId查询是否与这个卖场有关联
	 * @param clp_type_id
	 * @param ship_to_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getCLPTypeForShipTo(long clp_type_id,long ship_to_id, long title_id, long customer_id) throws Exception {
		
		try {
			
			String sql = " select * from lp_title_ship_to where lp_type_id = ? and ship_to_id = ? and title_id = ? and customer_id = ? ";
			
			DBRow para = new DBRow();
			para.add("lp_type_id",clp_type_id);
			para.add("ship_to_id",ship_to_id);
			para.add("title_id", title_id);
			para.add("customer_id", customer_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
			
		} catch (Exception e) {
			
			throw new Exception("FloorLPTypeMgrZJ getCLPTypeForShipTo error:"+e);
		}
	}
	
	public DBRow getCLPTypeForShipTo(long clp_type_id,long ship_to_id, long title_id) throws Exception {
		
		try {
			
			String sql = " select * from lp_title_ship_to where lp_type_id = ? and ship_to_id = ? and title_id = ? ";
			
			DBRow para = new DBRow();
			para.add("lp_type_id",clp_type_id);
			para.add("ship_to_id",ship_to_id);
			para.add("title_id", title_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
			
		} catch (Exception e) {
			
			throw new Exception("FloorLPTypeMgrZJ getCLPTypeForShipTo error:"+e);
		}
	}

	
	public int findMaxSort(long clp_pc_id,long clp_ship_to_id)
		throws Exception
	{
		try 
		{
			String sql = "select max(clp_sort) sort from clp_ps where clp_pc_id = ? and clp_ship_to_id = ?";
			
			DBRow para = new DBRow();
			para.add("clp_pc_id",clp_pc_id);
			para.add("clp_ship_to_id",clp_ship_to_id);
			
			DBRow result = dbUtilAutoTran.selectPreSingle(sql, para);
			
			return result.get("sort",0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ ");
		}
	}
	
	/**
	 * 获得SKU可以使用的盒子
	 * @param pc_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBLPTypeForSku(long pc_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("box_type")+" where box_pc_id = ? order by box_type_sort asc ";
			
			DBRow para = new DBRow();
			para.add("box_pc_id",pc_id);
			
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ getBLPTypeForSku error:"+e);
		}
	}
	
	/**
	 * 获得盒子类型
	 * @param box_type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailBLPType(long box_type_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("box_type")+" where box_type_id = ?";
			
			DBRow para = new DBRow();
			para.add("box_type_id",box_type_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ getDetailBLPType error:"+e);
		}
	}
	
	public DBRow getDetailBLPType(long pc_id,int length,int width,int height,long box_inner_type,long container_type_id)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("license_plate_type")+" where pc_id = ? and stack_length_qty = ? and stack_width_qty = ? and stack_height_qty = ? and inner_pc_or_lp = ? and basic_type_id = ? ";
		
		DBRow para = new DBRow();
		para.add("pc_id",pc_id);
		para.add("stack_length_qty",length);
		para.add("stack_width_qty",width);
		para.add("stack_height_qty",height);
		para.add("inner_pc_or_lp",box_inner_type);
		para.add("basic_type_id",container_type_id);
		
		return dbUtilAutoTran.selectPreSingle(sql, para);
	}
	
	public DBRow getDetailBLPTypeByBLPName(String box_name)
		throws Exception
	{
		String sql = "select * from "+ConfigBean.getStringValue("box_type")+" where box_name = ?";
		
		DBRow para = new DBRow();
		para.add("box_name",box_name);
		
		return dbUtilAutoTran.selectPreSingle(sql, para);
	}
	
	public DBRow getDetailBLPType(long pc_id,int length,int width,int height)
			throws Exception
		{
			String sql = "select * from "+ConfigBean.getStringValue("box_type")+" where box_pc_id = ? and box_total_length = ? and box_total_width = ? and box_total_height = ? ";
			
			DBRow para = new DBRow();
			para.add("pc_id",pc_id);
			para.add("length",length);
			para.add("width",width);
			para.add("height",height);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
	
	/**
	 * 获得托盘类型
	 * @param sku_lp_type_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailCLP(long sku_lp_type_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("clp_type")+" where sku_lp_type_id = ? ";
			
			DBRow para = new DBRow();
			para.add("sku_lp_type_id",sku_lp_type_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ getDetailCLP error:"+e);
		}
	}
	
	public DBRow getDetaiCLP(long pc_id,long lp_type_id,int length,int width,int height,long box_type_id,long title_id)
		throws Exception
	{
		try
		{
			String sql = "select * from "+ConfigBean.getStringValue("clp_type")
						+" where sku_lp_pc_id = ? and sku_lp_title_id = ? and lp_type_id = ? " 
						+"and sku_lp_box_type = ? and sku_lp_box_length= ? and sku_lp_box_width = ? and sku_lp_box_height = ?";
			
			DBRow para = new DBRow();
			para.add("sku_lp_pc_id",pc_id);
			para.add("sku_lp_title_id",title_id);
			para.add("lp_type_id",lp_type_id);
			para.add("sku_lp_box_type",box_type_id);
			para.add("sku_lp_box_length",length);
			para.add("sku_lp_box_width",width);
			para.add("sku_lp_box_height",height);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ getDetaiCLP error:"+e);
		}
	}

	
	/**
	 * 获得内箱类型
	 * @param ibt_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getDetailILPType(long ibt_id)
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("inner_box_type")+" where ibt_id = ? ";
			
			DBRow para = new DBRow();
			para.add("ibt_id",ibt_id);
			
			return dbUtilAutoTran.selectPreSingle(sql, para);
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ getDetailILP error:"+e);
		}
	}
	
	public DBRow[] getILPTypeForSku(long pc_id) 
		throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("inner_box_type")+" where ibt_pc_id = ? ";
			
			DBRow para = new DBRow();
			para.add("ibt_pc_id",pc_id);
			return dbUtilAutoTran.selectPreMutliple(sql, para);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ getILPTypeForSku error:"+e);
		}
	}
	
	/**zyj
	 * 通过pc_id查询特供CLP Type
	 * @param pc_id
	 * @param ship_to_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findClpTypeWithoutShipTo(long pc_id, long ship_to_id)throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select distinct ct.*,containerType.*,st.*,cs.* from  license_plate_type as ct ");
			sql.append(" join container_type as containerType on containerType.type_id = ct.basic_type_id ");
			sql.append(" left join  lp_title_ship_to as cs on ct.lpt_id = cs.lp_type_id ");
			sql.append(" left join ship_to as st on cs.ship_to_id = st.ship_to_id ");
			sql.append(" where ct.pc_id = ").append(pc_id);
			sql.append(" and ct.container_type = 1 "); 
			sql.append(" and cs.ship_to_id is null");
			sql.append(" order by ct.sort");
			

			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ findClpTypeWithoutShipTo error:"+e);
		}
	}
	
	/**zyj
	 * 通过pc_id查询非特供CLP Type
	 * @param pc_id
	 * @param ship_to_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findClpTypeWithShipTo(long pc_id, long ship_to_id)throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer();
			sql.append("select distinct ct.*,containerType.*,st.*,bt.box_total , bt.box_total_piece, bt.box_inner_type from  clp_type as ct ");
			sql.append(" join container_type as containerType on containerType.type_id = ct.lp_type_id ");
			sql.append(" left join  clp_ps as cs on ct.sku_lp_type_id = cs.clp_type_id ");
			sql.append(" left join ship_to as st on cs.clp_ship_to_id = st.ship_to_id ");
			sql.append(" left join box_type as bt on bt.box_type_id =  ct.sku_lp_box_type");
			sql.append(" where ct.sku_lp_pc_id = ").append(pc_id);
//			sql.append(" and cs.clp_ship_to_id = ").append(ship_to_id); 
			sql.append(" and cs.clp_ship_to_id is not null");
			sql.append(" order by ct.sku_lp_sort");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ findClpTypeWithoutShipTo error:"+e);
		}
	}
	
	
	public DBRow[] findClpShipToInfo(long pc_id, int limit, int type)throws Exception
	{
		try 
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT a.*, st.ship_to_name FROM (");
			sql.append(" SELECT COUNT(*) cn, ship_to_id FROM lp_title_ship_to ");
			sql.append(" WHERE lp_pc_id = ").append(pc_id);
			sql.append(" GROUP BY ship_to_id) a");
			sql.append(" JOIN ship_to st ON a.ship_to_id = st.ship_to_id");
			if(limit > 0 && type > 0)
 			{
 				if(MoreLessOrEqualKey.MORE == type)
 				{
 					sql.append(" AND a.cn > ").append(limit);
 				}
 				else if(MoreLessOrEqualKey.LESS == type)
 				{
 					sql.append(" AND a.cn < ").append(limit);
 				}
 				else if(MoreLessOrEqualKey.EQUAL == type)
 				{
 					sql.append(" AND a.cn = ").append(limit);
 				}
 			}
			sql.append(" ORDER BY a.cn DESC");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorLPTypeMgrZJ findClpTypeWithoutShipTo error:"+e);
		}
	}
	
	/**
	 * 获取当前clp的title和shipTo信息
	 * @param clp_type_id
	 * @param ship_to_id
	 * @param title_id
	 * @return
	 * @throws Exception
	 */
	public boolean getCLPTitleShipToInfo(long clp_type_id, long ship_to_id, long title_id, long customer_id)throws Exception{
		
		try{
			
			boolean result = false;
			
			//是否被机构包含
			String sqlCtn = " select count(*) count from lp_title_ship_to where lp_type_id = "+clp_type_id;
			
			if(ship_to_id == 0){
				
				sqlCtn += " and ship_to_id = " + ship_to_id;
				
			} else {
				
				sqlCtn += " and ( ship_to_id / " + ship_to_id + " = 1 or ship_to_id / " + ship_to_id + " = 0 ) ";
			}
			
			if(title_id == 0){
				
				sqlCtn += " and title_id = " + title_id;
				
			} else {
				
				sqlCtn += " and ( title_id / " + title_id + " = 1 or title_id / " + title_id + " = 0 ) ";
			}
			
			if(customer_id == 0){
				
				sqlCtn += " and customer_id = " + customer_id;
				
			} else {
				
				sqlCtn += " and ( customer_id / " + customer_id + " = 1 or customer_id / " + customer_id + " = 0 )";
			}
			
			DBRow row = dbUtilAutoTran.selectSingle(sqlCtn);
			
			if(row.get("count",0) > 0){
				
				result= true;
				
			}
			
			if( !result ){
				
				//是否包含其他结构
				if(ship_to_id == 0 || title_id == 0 || customer_id == 0){
					
					String sql = " where lp_type_id = "+clp_type_id;
					
					if(ship_to_id != 0){
						
						sql += " and ship_to_id = "+ship_to_id;
					}
					
					if(title_id != 0){
						
						sql += " and title_id = "+title_id;
					}
					
					if(customer_id != 0){
						
						sql += " and customer_id = "+customer_id;
					}
					
					dbUtilAutoTran.delete(sql, ConfigBean.getStringValue("lp_title_ship_to"));
				}
			}
			
			return result;
				
		}catch (Exception e){
			throw new Exception("FloorLPTypeMgrZJ.getCLPTitleShipToInfo error:" + e);
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
