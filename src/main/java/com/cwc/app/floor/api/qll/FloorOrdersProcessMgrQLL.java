package com.cwc.app.floor.api.qll;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FloorOrdersProcessMgrQLL {

		private DBUtilAutoTran dbUtilAutoTran;
		 
		/**
		 * 获取产品线的子类 通过function
		 * @param catalogID
		 * @return
		 * @throws Exception
		 */
		public DBRow getLineStr(long lineID)
			throws Exception
		{
			try
			{
				String sql="select  product_line_child_list("+lineID+") as idList";
				return (dbUtilAutoTran.selectSingle(sql));
			} 
			catch (Exception e)
			{
				throw new Exception("getLineStr(long catalogID)error："+e);
			}
		}	
		
		/**
		 * 获取类别的子类 通过function
		 * @param catalogID
		 * @return
		 * @throws Exception
		 */
		public DBRow getIdStr(long catalogID)
			throws Exception
		{
			try
			{
				String sql="select  product_catalog_child_list("+catalogID+") as idList";
				return (dbUtilAutoTran.selectSingle(sql));
			} 
			catch (Exception e)
			{
				throw new Exception("DBRow getStr(long catalogID)error："+e);
			}
		}
	 
		/**
		 * 通过产品名称获取ID
		 * @param pName
		 * @return
		 * @throws Exception
		 */
		public DBRow  getPid(String pName)
			throws Exception 
		{
			try
			{
				DBRow param = new DBRow();
				String sql = "select pc_id from "+ConfigBean.getStringValue("product")+" where p_name = ? and orignal_pc_id = 0";
				param.add("p_name", pName);
				return (dbUtilAutoTran.selectPreSingle(sql, param));
			} 
			catch (Exception e)
			{
				throw new Exception("getPid(String pName)error："+e);
			}
		}
		
		 
		
		/**
		 * 通过商品ID 统计计划发货
		 * @param pid
		 * @param startDate
		 * @param endDate
		 * @param storageID
		 * @return
		 * @throws Exception
		 */
		public DBRow[] buildPidPlanAndStock(long pID, long storage_id,String startDate,String endDate)
			throws Exception
		{
			try 
			{
				DBRow[] dateStr =getDate(startDate,endDate);
				String sql = " SELECT p.product_id, p.product_title,  p.store_count ,p.storage_title,   ";
				
				for(int i=0; i<dateStr.length; i++)
				{
					sql +="  MAX(CASE p.delivery_date  WHEN '"+dateStr[i].getString("date")+"' THEN p.quantity ELSE 0  END)  date_"+i;
					if(i!=(dateStr.length-1))
					{
						sql +="    , ";
					}
				}
				sql +=" FROM ";
				sql +=" (    select t.product_id  ,p.p_name as product_title  ,t.plan_storage_id  ,s.title as storage_title ,storage.store_count  , left(t.delivery_date,10)  as delivery_date, t.quantity  "+
					  " from "+ConfigBean.getStringValue("plan_storage_sales")+" t,"+ConfigBean.getStringValue("product")+" p,"+ConfigBean.getStringValue("product_storage_catalog")+" s,"+ConfigBean.getStringValue("product_storage")+" as storage "+
					  " where t.product_id = p.pc_id and t.plan_storage_id = s.id and t.product_id = storage.pc_id and t.plan_storage_id =storage.cid  and   t.product_id = "+pID+
					  " and  t.plan_storage_id="+storage_id+"  and t.delivery_date between '"+startDate+" 0:00:00' and '"+endDate+" 23:59:59'  )  as p      ORDER BY  p.product_id ";

				return (dbUtilAutoTran.selectMutliple(sql));
			} 
			catch (Exception e)
			{
				throw new Exception("getAllProductLineCatalog(String productLine)error："+e);
			}
		}
		 
		
		/**
		 * 查询产品线的子类
		 * @param productLine
		 * @return
		 * @throws Exception
		 */
		public DBRow[] getAllProductLineCatalog(long productLine)
			throws Exception
		{
			try 
			{
				String sql = " select  distinct id  from "+ConfigBean.getStringValue("product_catalog")+" where product_line_id = "+productLine;
				return (dbUtilAutoTran.selectMutliple(sql));
			} 
			catch (Exception e)
			{
				throw new Exception("getAllProductLineCatalog(String productLine)error："+e);
			}
		}
		
		 
		/**
		 * 通过日期重组plan_and_stock 导出不带分页
		 * @param rows
		 * @param pc
		 * @return
		 * @throws Exception
		 */
		public DBRow[] getPlanAndStockAllExport(String storage_id_list, long storage_id,String startDate,String endDate  ) 
			throws Exception
		{
			try 
			{
				DBRow[] dateStr =getDate(startDate,endDate);
					String sql = " SELECT p.product_id, p.product_title,  p.store_count,p.storage_title,  ";
					
					for(int i=0; i<dateStr.length; i++)
					{
						sql +="  MAX(CASE p.delivery_date  WHEN '"+dateStr[i].getString("date")+"' THEN p.quantity ELSE 0  END)  date_"+i;
						if(i!=(dateStr.length-1))
						{
							sql +="    , ";
						}
					}
					sql +=" FROM ";
					sql +=" ( select p.product_id ,c.p_name as product_title  ,p.plan_storage_id, s.title as storage_title, left(p.delivery_date,10) as delivery_date ,p.quantity as quantity  ,storage.store_count "+
						  " from "+ConfigBean.getStringValue("plan_storage_sales")+" p,"+ConfigBean.getStringValue("product")+" c,"+ConfigBean.getStringValue("product_storage_catalog")+" s,"+ConfigBean.getStringValue("product_storage")+" as storage "+
						  " where  p.catalog_id in  ("+storage_id_list+") "+
						  " and   p.product_id = c.pc_id  and p.plan_storage_id = s.id and p.plan_storage_id="+storage_id+" and  storage.pc_id = p.product_id and     storage.cid = p.plan_storage_id and " +
						  " p.delivery_date between '"+startDate+" 0:00:00' and '"+endDate+" 23:59:59') as p ";
					sql +=" GROUP BY  p.product_id   ";
					sql +=" ORDER BY  p.product_id   ";
					
					return (dbUtilAutoTran.selectMutliple(sql));
				
			} 
			catch (Exception e)
			{
				throw new Exception(" getPlanAndStockAll(DBRow[] rows ,PageCtrl pc)error："+e);
			}
		}
		 
		/**
		 * 根据类别进行查询
		 * @param storage_id_list
		 * @param storage_id
		 * @param startDate
		 * @param endDate
		 * @return
		 * @throws Exception
		 */
	public DBRow[] buildPlanDateIN(String storage_id_list, long storage_id,String startDate,String endDate,PageCtrl pc) 
		throws Exception 
	{
		try 
		{
			DBRow[] dateStr =getDate(startDate,endDate);
			String sql = " SELECT p.product_id, p.product_title,  p.store_count, p.storage_title ,";
			
			for(int i=0; i<dateStr.length; i++)
			{
				sql +="  MAX(CASE p.delivery_date  WHEN '"+dateStr[i].getString("date")+"' THEN p.quantity ELSE 0  END)  date_"+i;
				if(i!=(dateStr.length-1))
				{
					sql +="    , ";
				}
			}
			sql +=" FROM ";
			sql +=" ( select p.product_id ,c.p_name as product_title  ,p.plan_storage_id, s.title as storage_title, left(p.delivery_date,10) as delivery_date ,p.quantity as quantity  ,storage.store_count "+
				  " from "+ConfigBean.getStringValue("plan_storage_sales")+" p,"+ConfigBean.getStringValue("product")+" c,"+ConfigBean.getStringValue("product_storage_catalog")+" s,"+ConfigBean.getStringValue("product_storage")+" as storage "+
				  " where  p.catalog_id in  ("+storage_id_list+") "+
				  " and   p.product_id = c.pc_id  and p.plan_storage_id = s.id and p.plan_storage_id="+storage_id+" and  storage.pc_id = p.product_id and     storage.cid = p.plan_storage_id and " +
				  " p.delivery_date between '"+startDate+" 0:00:00' and '"+endDate+" 23:59:59') as p ";
			sql +=" GROUP BY  p.product_id   ";
			sql +=" ORDER BY  p.product_id   ";
			
			
			return (dbUtilAutoTran.selectMutliple(sql, pc));
		} 
		catch (Exception e)
		{
			throw new Exception("buildPlanDate(long storage_id, long catalog_id,String startDate,String endDate,PageCtrl pc)error："+e);
		}
	}
	
		public DBRow[]  getDate(String startDate,String endDate)
			throws Exception 
		{
		
			//缺少后面天大于 等于前面天的判断
			 long  DAY = 24L * 60L * 60L * 1000L;   
			 Date dateTemp = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
		     Date dateTemp2 = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
			 long n =  (dateTemp2.getTime() - dateTemp.getTime() ) / DAY   ;
			 int num = Long.valueOf(n).intValue();
			 DBRow[] row = new DBRow[num+1];
			 Calendar calendarTemp = Calendar.getInstance();
			 calendarTemp.setTime(dateTemp);
		     int i= 0;
		     while (calendarTemp.getTime().getTime()!= dateTemp2.getTime())
		     {
		            String temp = new SimpleDateFormat("yyyy-MM-dd").format(calendarTemp.getTime()) ;
		            row[i] = new DBRow();
		            row[i].add("date",temp );
		            calendarTemp.add(Calendar.DAY_OF_YEAR, 1);
		            i++;
		     }
		     row[i] = new DBRow();
		     row[i].add("date", new SimpleDateFormat("yyyy-MM-dd").format(dateTemp2.getTime()) );
			 return row;
		}
		
		public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
			this.dbUtilAutoTran = dbUtilAutoTran;
		}
	}
