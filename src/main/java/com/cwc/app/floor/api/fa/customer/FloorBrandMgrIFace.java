/**
 * 
 */
package com.cwc.app.floor.api.fa.customer;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

/**
 * @author Zhangchangjiang
 *
 */
public interface FloorBrandMgrIFace {
	public DBRow[] getAllBrandList(DBRow dbRow, PageCtrl pc) throws Exception;

	public long insertBrand(DBRow dbRow) throws Exception;

	public int updateActive(DBRow dbRow) throws Exception;
}
