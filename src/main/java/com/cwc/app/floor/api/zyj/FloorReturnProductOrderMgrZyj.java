package com.cwc.app.floor.api.zyj;

import com.cwc.app.key.ReturnProductKey;
import com.cwc.app.key.ReturnProductPictureRecognitionkey;
import com.cwc.app.key.ReturnTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorReturnProductOrderMgrZyj {

	private DBUtilAutoTran dbUtilAutoTran;

	/**
	 * 通过服务单ID获取退货单
	 * @param sid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnOrderBySid(long sid) throws Exception
	{
		try 
		{
			String sql = "select * from return_product where sid = " + sid;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnOrderBySid(sid) error:" + e);
		}
	}
	
	/**
	 * 通过退货单明细ID获取子明细
	 * @param rpi_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnSubItemsByRpiId(long rpi_id) throws Exception
	{
		try 
		{
			String sql = "select * from return_product_sub_items where rpi_id = " + rpi_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnSubItemsByRpiId(rpi_id) error:" + e);
		}
	}
	
	/**
	 * 通过退货明细ID删除子明细
	 * @param rpi_id
	 * @throws Exception
	 */
	public void deleteReturnSubItemsByRpiId(long rpi_id) throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete(" where rpi_id = " + rpi_id, ConfigBean.getStringValue("return_product_sub_items"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.deleteReturnSubItemsByRpiId(rpi_id) error:" + e);
		}
	}
	
	/**
	 * 通过退货单ID删除明细
	 * @param rp_id
	 * @throws Exception
	 */
	public void deleteReturnItemsByRpId(long rp_id) throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete(" where rp_id = " + rp_id, ConfigBean.getStringValue("return_product_items"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.deleteReturnItemsByRpId(rp_id) error:" + e);
		}
	}
	
	/**
	 * 通过退货单ID删除明细
	 * @param rp_id
	 * @throws Exception
	 */
	public void deleteReturnItemsByRpiId(long rpi_id) throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete(" where rpi_id = " + rpi_id, ConfigBean.getStringValue("return_product_items"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.deleteReturnItemsByRpiId(rp_id) error:" + e);
		}
	}
	
	/**
	 * 通过退货单删除退货关联文件
	 * @param rp_id
	 * @throws Exception
	 */
	public void deleteReturnItemsFixByRpId(long rp_id) throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete(" where rp_id = " + rp_id, ConfigBean.getStringValue("return_product_items_fix"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.deleteReturnItemsFixByRpId(rp_id) error:" + e);
		}
	}
	
	/**
	 * 通过退货单删除退货检查
	 * @param rp_id
	 * @throws Exception
	 */
	public void deleteReturnItemsCheckByRpId(long rp_id) throws Exception
	{
		try 
		{
			dbUtilAutoTran.delete(" where rp_id = " + rp_id, ConfigBean.getStringValue("return_product_items_zr_check"));
		} 
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.deleteReturnItemsFixByRpId(rp_id) error:" + e);
		}
	}
	
	/**
	 * 删除退货单
	 * @param rp_id
	 * @throws Exception
	 */
	public void deleteReturnOrderByRpId(long rp_id) throws Exception
	{
		try
		{
			dbUtilAutoTran.delete(" where rp_id = " + rp_id, ConfigBean.getStringValue("return_product"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.deleteReturnItemsFixByRpId(rp_id) error:" + e);
		}
	}
	
	/**
	 * 通过退货单ID获取退货单子明细
	 * @param rp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnSubItemsByRpid(long rp_id) throws Exception
	{
		try
		{
			String sql = "select rpsi.* from return_product rp,return_product_items rpi,return_product_sub_items rpsi where rp.rp_id="+rp_id+" and rp.rp_id=rpi.rp_id and rpi.rpi_id=rpsi.rpi_id";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnSubItemsByRpid(rp_id) error:" + e);
		}
	}
	
	/**
	 * 通过退货单ID获取得到退货的退货单子明细
	 * @param rp_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnSubItemsByRpidAndReceived(long rp_id) throws Exception
	{
		try
		{
			String sql = "SELECT rpsi.* FROM return_product_items rpi, return_product_sub_items rpsi WHERE rpi.rp_id = "+rp_id+" AND rpi.rpi_id = rpsi.rpi_id AND rpi.is_receive_item != -1";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnSubItemsByRpid(rp_id) error:" + e);
		}
	}
	
	/**
	 * 通过退货单ID，商品ID获取退货原因
	 * @param rp_id
	 * @param pid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnReasonByRpidAndPid(long rp_id, long pid) throws Exception
	{
		try 
		{
			String sql = "select * from return_product_reason where rp_id = " + rp_id + " and pc_id = " + pid;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnReasonByRpidAndPid(long rp_id, long pid) error:" + e);
		}
	}
	
	/**
	 * 统计退货数量，需要采集图片
	 * 采集图片未完成，需要退货，刚创建（仓库）或等待退货（客服）
	 * @return
	 * @throws Exception
	 */
	public int statNeedGatherPicReturnOrderCount() throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select count(*) as need_gather_pic_count from return_product where 1=1");
			sb.append(" and gather_picture_status = 0");
			sb.append(" and is_need_return = " + ReturnTypeKey.NEED);
			sb.append(" and (status = " + ReturnProductKey.WAITING + " or status = " + ReturnProductKey.WAITINGPRODUCT + ")");
			return dbUtilAutoTran.selectSingle(sb.toString()).get("need_gather_pic_count", 0);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.statNeedGatherPicReturnOrderCount() error:" + e);
		}
	}
	
	/**
	 * 统计退货，需要采集图片
	 * 采集图片未完成，需要退货，刚创建（仓库）或等待退货（客服）
	 * @return
	 * @throws Exception
	 */
	public DBRow[] statNeedGatherPicReturnOrders(PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select * from return_product where 1=1");
			sb.append(" and gather_picture_status = 0");
			sb.append(" and is_need_return = " + ReturnTypeKey.NEED);
			sb.append(" and (status = " + ReturnProductKey.WAITING + " or status = " + ReturnProductKey.WAITINGPRODUCT + ")");
			return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.statNeedGatherPicReturnOrders(PageCtrl pc) error:" + e);
		}
	}
	
	/**
	 * 统计退货，需要识别商品
	 * 未识别，图片采集完成，需要退货，刚创建（仓库）或等待退货（客服）或部分退货
	 * @return
	 * @throws Exception
	 */
	public int statNeedRecognitionReturnOrderCount() throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select count(*) as need_recog_count from return_product where 1=1");
			sb.append(" and gather_picture_status = 1");
			sb.append(" and picture_recognition = " + ReturnProductPictureRecognitionkey.RecognitionNoneFinish);
			sb.append(" and is_need_return = " + ReturnTypeKey.NEED);
			sb.append(" and (status = " + ReturnProductKey.WAITING + " or status = " + ReturnProductKey.WAITINGPRODUCT );
			sb.append(" or status = " + ReturnProductKey.LITTLE_PRODUCT + ")");
			return dbUtilAutoTran.selectSingle(sb.toString()).get("need_recog_count", 0);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.statNeedRecognitionReturnOrderCount() error:" + e);
		}
	}
	
	/**
	 * 统计退货，需要识别商品
	 * 未识别，图片采集完成，需要退货，刚创建（仓库）或等待退货（客服）或部分退货
	 * @return
	 * @throws Exception
	 */
	public DBRow[] statNeedRecognitionReturnOrders(PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select * from return_product where 1=1");
			sb.append(" and gather_picture_status = 1");
			sb.append(" and picture_recognition = " + ReturnProductPictureRecognitionkey.RecognitionNoneFinish);
			sb.append(" and is_need_return = " + ReturnTypeKey.NEED);
			sb.append(" and (status = " + ReturnProductKey.WAITING + " or status = " + ReturnProductKey.WAITINGPRODUCT );
			sb.append(" or status = " + ReturnProductKey.LITTLE_PRODUCT + ")");
			return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.statNeedRecognitionReturnOrders(PageCtrl pc) error:" + e);
		}
	}
	
	/**
	 * 统计退货，需要登记商品
	 * 图片采集完成，识别完成，需要退货，刚创建（仓库）或等待退货（客服）或部分退货
	 * @return
	 * @throws Exception
	 */
	public int statNeedRegisterReturnOrderCount() throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select count(*) as need_register_count from return_product where 1=1");
			sb.append(" and gather_picture_status = 1");
			sb.append(" and picture_recognition = " + ReturnProductPictureRecognitionkey.RecognitionFinish);
			sb.append(" and is_need_return = " + ReturnTypeKey.NEED);
			sb.append(" and (status = " + ReturnProductKey.WAITING + " or status = " + ReturnProductKey.WAITINGPRODUCT );
			sb.append(" or status = " + ReturnProductKey.LITTLE_PRODUCT + ")");
			return dbUtilAutoTran.selectSingle(sb.toString()).get("need_register_count", 0);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.statNeedRegisterReturnOrderCount() error:" + e);
		}
	}
	
	/**
	 * 统计退货，需要登记商品
	 * 图片采集完成，识别完成，需要退货，刚创建（仓库）或等待退货（客服）或部分退货
	 * @return
	 * @throws Exception
	 */
	public DBRow[] statNeedRegisterReturnOrders(PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select * from return_product where 1=1");
			sb.append(" and gather_picture_status = 1");
			sb.append(" and picture_recognition = " + ReturnProductPictureRecognitionkey.RecognitionFinish);
			sb.append(" and is_need_return = " + ReturnTypeKey.NEED);
			sb.append(" and (status = " + ReturnProductKey.WAITING + " or status = " + ReturnProductKey.WAITINGPRODUCT );
			sb.append(" or status = " + ReturnProductKey.LITTLE_PRODUCT + ")");
			return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.statNeedRegisterReturnOrders(PageCtrl pc) error:" + e);
		}
	}
	
	/**
	 * 统计退货，登记完成
	 * 图片采集完成，识别完成，需要退货，登记完成，未创建账单
	 * @return
	 * @throws Exception
	 */
	public int statRegisterFinishReturnOrderCount() throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select count(*) as need_register_finish from return_product r where 1=1");
			sb.append(" and r.gather_picture_status = 1");
			sb.append(" and r.picture_recognition = " + ReturnProductPictureRecognitionkey.RecognitionFinish);
			sb.append(" and r.is_need_return = " + ReturnTypeKey.NEED);
			sb.append(" and (r.status = " + ReturnProductKey.FINISHALL +" or r.status = " + ReturnProductKey.FINISH + ")");
			sb.append(" and ((select count(*) from bill_order b where b.sid = r.sid) = 0");
			sb.append(" 		AND (r.is_need_create_bill is null or r.is_need_create_bill != 2))");
			return dbUtilAutoTran.selectSingle(sb.toString()).get("need_register_finish", 0);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.statRegisterFinishReturnOrderCount() error:" + e);
		}
	}
	
	/**
	 * 统计退货，登记完成
	 * 图片采集完成，识别完成，需要退货，登记完成，未创建账单
	 * @return
	 * @throws Exception
	 */
	public DBRow[] statRegisterFinishReturnOrders(PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select * from return_product r where 1=1");
			sb.append(" and r.gather_picture_status = 1");
			sb.append(" and r.picture_recognition = " + ReturnProductPictureRecognitionkey.RecognitionFinish);
			sb.append(" and r.is_need_return = " + ReturnTypeKey.NEED);
			sb.append(" and (r.status = " + ReturnProductKey.FINISHALL +" or r.status = " + ReturnProductKey.FINISH + ")");
			sb.append(" and ((select count(*) from bill_order b where b.sid = r.sid) = 0");
			sb.append(" 		AND (r.is_need_create_bill is null or r.is_need_create_bill != 2))");
			return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.statRegisterFinishReturnOrders(PageCtrl pc) error:" + e);
		}
	}
	
	/**
	 * 统计退货，登记完成
	 * 图片采集完成，识别完成，需要退货，登记完成
	 * @return
	 * @throws Exception
	 */
	public int statReturnOrderNotRelationOtherOrderCount() throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select count(*) as need_relation_other_order from return_product where 1=1");
			sb.append(" and (oid = 0 or oid is null)");
			sb.append(" and (wid = 0 or wid is null)");
			sb.append(" and (sid = 0 or sid is null)");
			sb.append(" and status != " + ReturnProductKey.FINISHALL );
			return dbUtilAutoTran.selectSingle(sb.toString()).get("need_relation_other_order", 0);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.statReturnOrderNotRelationOtherOrderCount() error:" + e);
		}
	}
	
	/**
	 * 统计退货，登记完成
	 * 图片采集完成，识别完成，需要退货，登记完成
	 * @return
	 * @throws Exception
	 */
	public DBRow[] statReturnOrderNotRelationOtherOrders(PageCtrl pc) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select * from return_product where 1=1");
			sb.append(" and (oid = 0 or oid is null)");
			sb.append(" and (wid = 0 or wid is null)");
			sb.append(" and (sid = 0 or sid is null)");
			sb.append(" and status != " + ReturnProductKey.FINISHALL );
			return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.statReturnOrderNotRelationOtherOrderCount(PageCtrl pc) error:" + e);
		}
	}
	
	/**
	 * 更改退货单明细
	 * @param request
	 * @throws Exception
	 */
	public void modReturnItemCount(DBRow row, long rpi_id) throws Exception
	{
		try
		{
			dbUtilAutoTran.update(" where rpi_id = "+rpi_id, ConfigBean.getStringValue("return_product_items"), row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.modReturnItemCount(request) error:" + e);
		}
	}
	
	/**
	 * 得到收到货且处理完成的退货明细
	 * @param is_receive_item
	 * @param isreturn_handle
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnProductItemByReceiveHandleInfo(int is_receive_item, long rp_id) throws Exception
	{
		try
		{
			String sql = "select * from return_product_items where rp_id = "+rp_id+" and (is_receive_item != "+is_receive_item+" or is_receive_item is null)";
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnProductItemByReceiveHandleInfo(int is_receive_item, long rp_id) error:" + e);
		}
	}
	
	/**
	 * 更新收到货的退货明细
	 * @param row
	 * @param rpi_id
	 * @throws Exception
	 */
	public void updateReceivedReturnProductItem(DBRow row , long rp_id)throws Exception {
		try
		{
 			 dbUtilAutoTran.update(" where rp_id=" + rp_id + " and is_receive_item != -1", ConfigBean.getStringValue("return_product_items"), row);
		}
		catch (Exception e)
		{
			throw new Exception("FloorReturnProductItemsFix.updateReturnProductItem(row, rp_id):"+e);
		}
	}
	
	/**
	 * 通过退货单明细ID获取子明细
	 * @param rpi_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnSubItemsByRpiIdAndPid(long rpi_id, long pid) throws Exception
	{
		try 
		{
			String sql = "select * from return_product_sub_items where rpi_id = " + rpi_id + " and pid = " + pid;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnSubItemsByRpiIdAndPid(rpi_id,pid) error:" + e);
		}
	}
	
	/**
	 * 更新退货子明细
	 * @param row
	 * @param rpsi_id
	 * @throws Exception
	 */
	public void updateReturnProductSubItem(DBRow row , long rpsi_id)throws Exception {
		try
		{
 			 dbUtilAutoTran.update(" where rpsi_id=" + rpsi_id, ConfigBean.getStringValue("return_product_sub_items"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductItemsFix.updateReturnProductSubItem(row, rpsi_id):"+e);
		}
	}
	
	/**
	 * 通过退货单子明细ID获取子明细
	 * @param rpi_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getReturnSubItemsByRpsiId(long rpsi_id) throws Exception
	{
		try 
		{
			String sql = "select * from return_product_sub_items where rpsi_id = " + rpsi_id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnSubItemsByRpsiId(rpsi_id) error:" + e);
		}
	}
	
	/**
	 * 通过退货单子明细ID获取子明细
	 * @param rpi_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnItemsSiidAndPidByRpId(long rp_id) throws Exception
	{
		try 
		{
			String sql = "select IFNULL(siid, 0) as siid, pid, rpi_id from return_product_items where rp_id = " + rp_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnItemsSiidAndPidByRpId(rp_id) error:" + e);
		}
	}
	
	
	/**
	 * 通过退货单子明细ID获取子明细
	 * @param rpi_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getReturnItemByRpiId(long rpi_id) throws Exception
	{
		try 
		{
			String sql = "select * from return_product_items where rpi_id = " + rpi_id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnItemByRpiId(rpi_id) error:" + e);
		}
	}
	
	/**
	 * 通过退货单子明细ID获取子明细
	 * @param rpi_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getReturnSubItemByRpsiId(long rpsi_id) throws Exception
	{
		try 
		{
			String sql = "select * from return_product_sub_items where rpsi_id = " + rpsi_id;
			return dbUtilAutoTran.selectSingle(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnSubItemByRpiId(rpsi_id) error:" + e);
		}
	}
	
	/**
	 * 通过退货单ID、商品ID、服务单明细ID查询明细
	 * @param rpi_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnItemsBySiidPidRpId(long rp_id, long pid, long siid) throws Exception
	{
		try 
		{
			String sql = "select * from return_product_items where rp_id = " + rp_id  + " and pid = " + pid;
			if(0 != siid)
			{
				sql +=  " and siid = " + siid;
			}
			else
			{
				sql +=  " and (siid = " + siid + " or siid is null)";
			}
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnItemsBySiidPidRpId(long rp_id, long pid, long siid) error:" + e);
		}
	}
	
	/**
	 * 通过退货明细ID查询检查
	 * @param rpi_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnProductCheckByRpiId(long rpi_id) throws Exception 
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("return_product_items_zr_check") + " where rpi_id=" + rpi_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnProductCheckByRpiId(rpi_id):"+e);
		}
	}
	
	/**
	 * 通过退货ID查询检查
	 * @param rpi_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnProductCheckByRpId(long rp_id) throws Exception 
	{
		try
		{
			String sql = "select * from " + ConfigBean.getStringValue("return_product_items_zr_check") + " where rp_id=" + rp_id;
			return dbUtilAutoTran.selectMutliple(sql);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnProductCheckByRpId(rpi_id):"+e);
		}
	}
	
	/**
	 * 更新检查
	 * @param checkId
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public void updateReturnProductCheckByCheckId(DBRow row, long checkId) throws Exception 
	{
		try
		{
			dbUtilAutoTran.update(" where check_return_product_id = " + checkId, ConfigBean.getStringValue("return_product_items_zr_check"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.updateReturnProductCheckByCheckId(row,checkId):"+e);
		}
	}
	
	/**
	 * 删除检查
	 * @param checkId
	 * @return
	 * @throws Exception
	 */
	public void deleteReturnProductCheckByCheckId(long checkId) throws Exception 
	{
		try
		{
			dbUtilAutoTran.delete(" where check_return_product_id = " + checkId, ConfigBean.getStringValue("return_product_items_zr_check"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.deleteReturnProductCheckByCheckId(checkId):"+e);
		}
	}
	
	/**
	 * 删除检查
	 * @param checkId
	 * @return
	 * @throws Exception
	 */
	public void deleteReturnProductCheckByRpiId(long rpi_id) throws Exception 
	{
		try
		{
			dbUtilAutoTran.delete(" where rpi_id = " + rpi_id, ConfigBean.getStringValue("return_product_items_zr_check"));
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.deleteReturnProductCheckByRpiId(rpi_id):"+e);
		}
	}
	
	/**
	 * 更新检查
	 * @param checkId
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public void updateReturnSubItemByRpsiId(DBRow row, long rpsi_id) throws Exception 
	{
		try
		{
			dbUtilAutoTran.update(" where rpsi_id = " + rpsi_id, ConfigBean.getStringValue("return_product_sub_items"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.updateReturnSubItemByRpsiId(row,rpsi_id):"+e);
		}
	}
	
	/**
	 * 添加日志
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addReturnPorductLog(DBRow row) throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("return_product_logs"), row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.addReturnPorductLog(DBRow row):"+e);
		}
	}
	
	/**
	 * 添加退货日志
	 * @return
	 * @throws Exception
	 */
	public long addReturnProductLog(long rp_id, long rpi_id, long rpsi_id, long fix_rpi_id,
			int rp_status, String content, int follow_type, long operator, String operate_time) throws Exception
	{
		try
		{
			DBRow row = new DBRow();
			row.add("rp_id", rp_id);
			row.add("rpi_id", rpi_id);
			row.add("rpsi_id", rpsi_id);
			row.add("fix_rpi_id", fix_rpi_id);
			row.add("rp_status", rp_status);
			row.add("content", content);
			row.add("follow_type", follow_type);
			row.add("operator", operator);
			row.add("operate_time", operate_time);
			return this.addReturnPorductLog(row);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.addReturnPorductLog(DBRow row):"+e);
		}
	}
	
	/**
	 * 通过退货单ID，跟进类型，退货单状态查询退货单日志
	 * @param rp_id
	 * @param follow_type
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReturnProductLogsByIdTypeStatus(long rp_id, int follow_type, int status) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select r.*, a.employe_name from return_product_logs r left join admin a on r.operator = a.adid where 1=1");
			if(0 != rp_id)
			{
				sb.append(" and r.rp_id = " + rp_id);
			}
			if(0 != follow_type && -1 != follow_type)
			{
				sb.append(" and r.follow_type = " + follow_type);
			}
			if(0 != status && -1 != status)
			{
				sb.append(" and r.rp_status = " + status);
			}
			sb.append(" order by r.operate_time desc, r.follow_type desc, r.rp_id desc, r.rp_log_id desc");
			return dbUtilAutoTran.selectMutliple(sb.toString());
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnProductLogsByIdTypeStatus(long rp_id, int type, int status):"+e);
		}
	}
	
	/**
	 * 得到日志的数量
	 * @param rp_id
	 * @param follow_type
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public int getReturnProductLogsCountByIdTypeStatus(long rp_id, int follow_type, int status) throws Exception
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("select count(r.rp_log_id) as returnProductLogsCount from return_product_logs r left join admin a on r.operator = a.adid where 1=1");
			if(0 != rp_id)
			{
				sb.append(" and r.rp_id = " + rp_id);
			}
			if(0 != follow_type && -1 != follow_type)
			{
				sb.append(" and r.follow_type = " + follow_type);
			}
			if(0 != status && -1 != status)
			{
				sb.append(" and r.rp_status = " + status);
			}
			sb.append(" order by r.rp_id desc, r.rp_log_id desc");
			return dbUtilAutoTran.selectSingle(sb.toString()).get("returnProductLogsCount", 0);
		}
		catch (Exception e) 
		{
			throw new Exception("FloorReturnProductOrderMgrZyj.getReturnProductLogsCountByIdTypeStatus(long rp_id, int type, int status):"+e);
		}
	}
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

}
