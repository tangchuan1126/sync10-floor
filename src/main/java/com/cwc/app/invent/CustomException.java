package com.cwc.app.invent;

import java.util.HashMap;
import java.util.Map;

import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;

/**
 * Title: CustomException
 * Description: CustomException.java
 * Company: SOS
 * @author liuyi
 * @time:2015年3月25日
 * 功能说明：自定义异常，让捕获的异常以正常的response返回给前台
 */
public class CustomException extends RuntimeException
{
	private Map<String,Object> result = null;
	private CustomException()
	{
		
	}
	
	public CustomException(Map<String,Object> msg) 
	{
		this.result = msg;
	}
	public CustomException(DBRow  row)
	{
		try {
			this.result = DBRowUtils.dbRowAsMap(row);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public CustomException(String  st)
	{
		try {
			Map<String,Object> rt=new HashMap<String,Object>();
			rt.put("e", st);
			this.result =rt;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Map<String,Object> getResult()
	{
		return this.result;
	}
}
