package com.cwc.app.invent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.db.DBRow;
public class BaseController {
	@Autowired
	private FloorProductStoreMgr productStoreMgr;
	@Autowired
    private FloorGoogleMapsMgrCc googleMapsMgr;
	

	public DBRow copyTree(long ps_id ,List<Map<String,Object>> list)throws Exception{
		System.out.println("===========copyTree==============");
		DBRow ret =new DBRow();
		try {
			for(int i=0;i<list.size();i++){
				JSONObject con =new JSONObject();
				JSONArray products =new JSONArray();
				JSONObject product =new JSONObject();
				Map<String,Object>	map =list.get(i);
				Map<String,Object> con_props=(Map<String, Object>) map.get("con_props");
				Map<String,Object> product_props=(Map<String, Object>)(map.get("product_props"));
				Map<String,Object> rel_props=(Map<String, Object>)(map.get("rel_props"));
				//slc 可以为空 为空时放在ghost 位置上
				Map<String,Object> slc_props=map.get("slc_props")==null?null:(Map<String, Object>)(map.get("slc_props"));
				Map<String,Object> locates_props=(Map<String, Object>)(map.get("locates_props"));
				Map<String,Object>	locProps =new HashMap<String,Object>();
				if(slc_props!=null){
					System.out.println("===========start searchNodes==============");
					DBRow[] rows=productStoreMgr.searchNodes(ps_id, NodeType.StorageLocationCatalog, slc_props);
					System.out.println("===========finish searchNodes==============");
					if(rows.length==0){
						//表示ghost位置
						locProps.put("slc_id", 1);
						locProps.put("slc_type", 1);
					}else{
						locProps.put("slc_id", slc_props.get("slc_id"));
						locProps.put("slc_type",slc_props.get("slc_type"));
					}
				}else{
					locProps.put("slc_id", 1);
					locProps.put("slc_type", 1);
				}
				int[] catalogs =getCatalogs(Long.parseLong(product_props.get("pc_id").toString()));
				con.put("con_id", con_props.get("con_id"));
				con.put("container", con_props.get("container"));
				con.put("container_type", con_props.get("container_type"));
				con.put("is_full", con_props.get("is_full"));
				con.put("is_has_sn", con_props.get("is_has_sn"));
				con.put("type_id", con_props.get("type_id"));
				con.put("damage", con_props.get("damage"));
				product.put("catalogs",catalogs);
				product.put("p_name", product_props.get("p_name"));
				product.put("pc_id", product_props.get("pc_id"));
				product.put("product_line", product_props.get("product_line"));
				product.put("quantity", rel_props.get("quantity"));
				product.put("title_id", product_props.get("title_id"));
				product.put("customer_id", product_props.get("customer_id"));
				products.put(product);
				con.put("products", products);
				//System.out.println(con.toString(4));
				locProps.put("pc_id", locates_props.get("pc_id"));
				locProps.put("title_id", locates_props.get("title_id"));
				locProps.put("lot_number", locates_props.get("lot_number"));
				locProps.put("time_number",locates_props.get("time_number"));
				productStoreMgr.copyTree(con, ps_id, locProps);	
			}
			ret.add("err", 0);
			ret.add("ret", 1);
			return ret;
		} catch (Exception e) {
			ret.add("err", 1);
			ret.add("ret", 0);
			ret.add("e", e.getMessage());
			e.printStackTrace();
			throw new CustomException(ret);
		}
	}	
	
	public DBRow deleteContainer( long ps_id,long con_id) throws Exception{
		DBRow row =new DBRow();
		try {
			Map<String,Object> searchFor =new HashMap<String, Object>();
			searchFor.put("con_id", con_id);
			int counts =productStoreMgr.removeNodes(ps_id, NodeType.Container, searchFor, true);
			row.add("err", "0");
			row.add("ret", "1");
			row.add("counts", counts);
			return row;
		} catch (Exception e) {
			row.add("err", "1");
			row.add("ret", "0");
			row.add("e", e.getMessage());
			throw new CustomException(row);
		}
		
	}
	public DBRow putAway(long ps_id,long slc_id,int slc_type,Long...con_ids)throws Exception{
		DBRow rt =new DBRow();
		try {
			Map<String,Object> slc_props=new HashMap<String, Object>();
			slc_props.put("slc_id", slc_id);
			slc_props.put("slc_type", slc_type);
			productStoreMgr.putAway(ps_id, slc_props, con_ids);
			rt.add("ret", "1");
			rt.add("err", "0");
			return rt;
		} catch (Exception e) {
			rt.add("ret", "0");
			rt.add("e", e.getMessage());
			rt.add("err", "1");
			e.printStackTrace();
			throw new CustomException(rt);
		}
	}
		public int[] getCatalogs(long pc_id)throws Exception{
			int[] catalogs =null;
			DBRow row=googleMapsMgr.queryCatalogs(pc_id);
			if(row==null){
			  catalogs= new int[0];
			}else{
				String ids =row.getString("catalog_id");
				if(!ids.equals("")){
					String[] c_ids=	ids.split(",");
					catalogs=new int[c_ids.length];
					for(int j=0;j<c_ids.length;j++){
						catalogs[j]=Integer.parseInt(c_ids[j]);	
					}	
				}
			}
			return catalogs;
		}
}
