package com.cwc.app.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

public class Neo4jPathAnalyser {
	protected Set<String> cpkeys = new HashSet<String>();
	protected Set<String> cckeys = new HashSet<String>();
	protected Set<String> pathkeys = new HashSet<String>();
	protected JSONArray paths = new JSONArray();
	
	protected Neo4jPathAnalyser(){
		
	}
	
	public Neo4jPathAnalyser(JSONArray data) throws Exception {
		/** the data is the result of the following query:
		 match path=(c:Container)-[r:CONTAINS*]->(p:Product)
		 where c.con_id={con_id}
		 return startnode(r[-1]).con_id+'-'+p.pc_id+':'+p.title_id as cpkey, nodes(path),relationships(path)
		 */
		analyse(data);
	}
	
	protected void analyse(JSONArray rawData) throws Exception {
		for (int i = 0; i < rawData.length(); i++) {
			JSONArray row = rawData.getJSONArray(i);
			String cpkey = row.getString(0);
			cpkeys.add(cpkey);
			JSONArray nodes = row.getJSONArray(1);
			JSONArray rels = row.getJSONArray(2);
			JSONArray p = new JSONArray();
			StringBuilder pk = new StringBuilder();
			JSONObject startNode = nodes.getJSONObject(0).getJSONObject("data");
			p.put(startNode);
			pk.append(startNode.get("con_id"));
			for (int r = 0; r < rels.length(); r++) {
				JSONObject rel = rels.getJSONObject(r).getJSONObject("data");
				JSONObject toNode = nodes.getJSONObject(r + 1).getJSONObject("data");
				p.put(rel);
				pk.append("-");
				
				p.put(toNode);
				
				if(toNode.has("con_id")){ //说明这是一个(c1:Container)-[:CONTAINS]->(c2:Container)的CC关系
					JSONObject fromNode = nodes.getJSONObject(r).getJSONObject("data");
					cckeys.add( fromNode.get("con_id") + "-" + toNode.get("con_id") );
					pk.append(toNode.get("con_id"));
				}
				else {
					pk.append(toNode.get("pc_id")).append(":").append(toNode.get("title_id"));
				}
			}
			paths.put(p);
			pathkeys.add(pk.toString());
		}
	}
	
	public JSONArray getPaths(){
		return paths;
	}
	
	public String getPathkeys(){
		List<String> buf = new ArrayList<String>();
		for(String k : pathkeys){
			buf.add("'"+k+"'");
		}
		return "[" + StringUtils.join(buf,",") +"]";
	}
	
	public String getPathkeys(long con_id){
		List<String> buf = new ArrayList<String>();
		for(String k : pathkeys){
			if(k.startsWith(con_id + "-"))
				buf.add("'"+k+"'");
		}
		Collections.sort(buf);
		return "[" + StringUtils.join(buf,",") +"]";
	}
	
	public String getCpkeys(){
		List<String> buf = new ArrayList<String>();
		for(String k : cpkeys){
			buf.add("'"+k+"'");
		}
		return "[" + StringUtils.join(buf,",") +"]";
	}
	
	public String getCpkeys(long con_id){
		List<String> buf = new ArrayList<String>();
		for(String k : cpkeys){
			if(k.startsWith(con_id + "-"))
				buf.add("'"+k+"'");
		}
		Collections.sort(buf);
		return "[" + StringUtils.join(buf,",") +"]";
	}
	
	public String getCckeys(){
		List<String> buf = new ArrayList<String>();
		for(String k : cckeys){
			buf.add("'"+k+"'");
		}
		return "[" + StringUtils.join(buf,",") +"]";
	}
	
	public String getCckeys(long con_id){
		List<String> buf = new ArrayList<String>();
		for(String k : cckeys){
			if(k.startsWith(con_id + "-"))
				buf.add("'"+k+"'");
		}
		Collections.sort(buf);
		return "[" + StringUtils.join(buf,",") +"]";
	}

	@Override
	public String toString() {
		try {
			return new JSONObject().put("cpkeys", this.cpkeys)
							.put("cckeys", this.cckeys)
							.put("pathkeys", this.pathkeys)
							.put("paths", this.paths).toString(4);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
}
