package com.cwc.app.util;

/**
 * 实现人性化翻页按钮计算
 * 5 6 7 8 9 10 11 12 13 14
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class TurnStep
{
	private int p=0;					//当前页
	private int stepRange=10;			//步长
	private int right=0;				//右边距
	private int total=0;				//总页数
	private int st,en;					//起点、终点坐标

	/**
	 * 设置当前页
	 * @param p
	 */
	public void setP(int p)
	{
		this.p = p;
	}
	
	/**
	 * 设置翻页步长
	 * @param stepRange
	 */
	public void setStepRange(int stepRange)
	{
		this.stepRange = stepRange;
	}
	
	/**
	 * 设置右偏移量
	 * @param right
	 */
	public void setRight(int right)
	{
		this.right = right;
	}
	
	/**
	 * 设置总页数
	 * @param total
	 */
	public void setTotal(int total)
	{
		this.total = total;
	}
	
	/**
	 * 计算
	 */
	public void init()
	{
		/**
		 * 计算终点坐标
		 * 
		 * 终点坐标基本计算思路：当前页+右边距
		 * 但需要考虑有些特殊情况边界问题
		 */
		en = p+right;
		
		if ( en>total )//翻到最后几页，终点坐标超过总页数
		{
			en = total;
		}
		else if ( en<stepRange )	//因为翻页范围是等于步长的，所以如果终点坐标小于步长，需要做进一步的判断
		{
			en = stepRange>total?total:stepRange;
		}

		/**
		 * 计算起点坐标
		 * 
		 * 起点坐标基本计算思路：当前页-（步长-右边距）
		 * 但是需要考虑实际右边距是否超过总记录数 
		 * 
		 * (stepRange- (total-p>=right?right:(total-p)) ) 实际上是偏移量的计算，只有p-(stepRange- (total-p>=right?right:(total-p)) )+1才得到st的坐标
		 */
		
		st = p-(stepRange- (total-p>=right?right:(total-p)) )+1;
		if (st<1)
		{
			st=1;
		}		
	}
	
	/**
	 * 获得开始偏移量
	 * @return
	 */
	public int getST()
	{	
		return(st);
	}
	
	/**
	 * 获得终点偏移量
	 * @return
	 */
	public int getEN()
	{	
		return(en);
	}
}
