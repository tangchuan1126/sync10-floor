package com.cwc.app.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

/**
 * 常用字符串处理工具包
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class StrUtil implements Handler
{
	private static String PICTRUEFILE = "jpg,bmp,gif,jpeg,png";
	private static String OFFICEFILE = "pdf,xls,ppt,doc";
	private static Pattern NumberPattern = Pattern.compile("\\d+"); 
	/**
	 * 字符串替换
	 * @param s				源字符串
	 * @param strb			被替换字符串
	 * @param strh			替换内容
	 * @return
	 */
   public static final  String replaceString(String s,String strb,String strh)
   {
      if( (s == null) || (s.length() == 0))
      {
          return s;
      }

      StringBuffer tmp = new StringBuffer();
      int k;
      while((k=s.indexOf(strb))>=0){
         tmp.append(s.substring(0,k));
         tmp.append(strh);
         s = s.substring(k+strb.length());
      }
      if(s.length()>0) tmp.append(s);
      return tmp.toString();
   }

   /**
    * 把回车、换行替换成<br>
    * @param s
    * @return
    */
   public static final String replaceEnter(String s)
   {
    	if (( s == null || s.length() ==0))
        {
             return s;
        }
        else
        {
             s = replaceString(s,"\r\n","<br>");
             s = replaceString(s,"\n","<br>");
             return s;
        }
  }
   
   /**
    * 把asc字符转换为HTML字符
    * @param s
    * @return
    */
    public static final  String ascii2Html(String s)
    {
        if( (s==null) || (s.length() == 0))
        {
            return s;
        }
        else
        {
             s = replaceString(s,"&","&amp;");
             s = replaceString(s,"\"","&quot;");
             s = replaceString(s,"<","&lt;");
             s = replaceString(s,">","&gt;");
             s = replaceString(s," ","&nbsp;");
             s = replaceString(s,"'","&#39");
             s = replaceString(s,"\n","<br>");
             return s;
        }
   }

    /**
     * 把HTML字符转换为ASC
     * @param s
     * @return
     */
    public static final  String html2Ascii(String s)
    {
        if( (s==null) || (s.length() == 0))
        {
            return s;
        }
        else
        {
             s = replaceString(s,"&amp;","&");
             s = replaceString(s,"&quot;","\"");
             s = replaceString(s,"&lt;","<");
             s = replaceString(s,"&gt;",">");
             s = replaceString(s,"&nbsp;"," ");
             s = replaceString(s,"&#39","'");
             s = replaceString(s,"<br>","\r\n");
             return s;
        }
   }

    /**
     * 内码转换：gbk->iso
     * @param InputStr
     * @return
     */
    public static final String GBK2ISO(String InputStr)
    {
		try
        {
			return(new String(InputStr.getBytes("GBK"),"ISO8859_1"));
		}
        catch(Exception e)
        {
			System.err.println ("GBK2ISO����ת��ʧ��!");
			return null;
		}
	}

    /**
     * 内码转换：bg2312->utf8
     * @param InputStr
     * @return
     */
    public static final String GB23122UTF8(String InputStr)
    {
		try
        {
			return(new String(InputStr.getBytes("GB2312"),"UTF-8"));
		}
        catch(Exception e)
        {
        	System.err.println ("GBK2ISO����ת��ʧ��!");
			return null;
		}
	}
    
    /**
     * 内码转换：iso->utf8
     * @param InputStr
     * @return
     */
    public static final String ISO2UTF8(String InputStr)
    {
		try
        {
			return(new String(InputStr.getBytes("ISO8859-1"),"UTF-8"));
		}
        catch(Exception e)
        {
			//System.out.println ("ISO2UTF8");
			return null;
		}
	}
    
    /**
     * 内码转换：window1252->utf8
     * @param InputStr
     * @return
     */
    public static final String Windows1252UTF8(String InputStr)
    {
		try
        {
			return(new String(InputStr.getBytes("Windows-1252"),"UTF-8"));
		}
        catch(Exception e)
        {
			//System.out.println ("Windows1252UTF8");
			return null;
		}
	}
    
    /**
     * 内码转换：utf8->iso
     * @param InputStr
     * @return
     */
    public static final String UTF82ISO(String InputStr)
    {
		try
        {
			return(new String(InputStr.getBytes("UTF-8"),"ISO8859-1"));
		}
        catch(Exception e)
        {
			System.err.println ("GBK2ISO����ת��ʧ��!");
			return null;
		}
	}
    
    /**
     * 内码转换：utf8->gbk
     * @param InputStr
     * @return
     */
    public static final String UTF82GBK(String InputStr)
    {
		try
        {
			return(new String(InputStr.getBytes("UTF-8"),"GBK"));
		}
        catch(Exception e)
        {
			return null;
		}
	}
    
    /**
     * 内码转换：utf8->bg2312
     * @param InputStr
     * @return
     */
    public static final String UTF82GB2312(String InputStr)
    {
		try
        {
			return(new String(InputStr.getBytes("UTF-8"),"gb2312"));
		}
        catch(Exception e)
        {
			return null;
		}
	}

    /**
     * 内码转换：iso->gbk
     * @param InputStr
     * @return
     */
	public static String ISO2GBK(String InputStr)
    {
		try
        {
			return(new String(InputStr.getBytes("ISO8859_1"),"GBK"));
		}
        catch(Exception e)
        {
			System.err.println ("ISO2GBK����ת��ʧ��!");
			return null;
		}
	}

	/**
	 * 处理NULL值（返回空）
	 * @param str
	 * @return
	 */
    public static final  String dealNull(String str)
    {
        if(str == null)
        {
            return "";
        }
        else
        {
            return str;
        }
    }

    /**
     * 处理SQL
     * @param sql
     * @return
     */
    public static final  String dealSql(String sql)
    {
        if( (sql==null) || (sql.trim().length() == 0))
        {
            return sql;
        }
        else
        {
            String s = replaceString(sql.trim().replaceAll("\r",""),"'","''");
            s = replaceString(s,"\\","\\\\");
            if(s.charAt(0)!='\'')
            {
                s = "'"+s;
            }
            if(!s.endsWith("'"))
            {
                s = s + "'";
            }
            return s;
        }
    }

    /**
     * 把一个参数数组组织成,号分隔字符串
     * @param paraName
     * @return
     */
    public static final String getMultiString(String[] paraName)
    {
          return getMultiString(paraName,",");
    }
    
    /**
     * 把一个参数数组组织成某个号分隔字符串
     * @param paraName
     * @param spChar
     * @return
     */
    public static final String getMultiString(String[] paraName,String spChar)
    {

        if ( (paraName == null) || (paraName.length == 0 ) )
        {
            return "";
        }
        else
        {
            StringBuffer valuesTemp = new StringBuffer();
            String specialChar = spChar;
            String[] values;
            values=paraName;

            int i = 0;
            for (i = 0; i < (values.length - 1); i ++ )
            {
                 valuesTemp.append( values[i] + specialChar );
            }

            valuesTemp.append(values[i]);

            return valuesTemp.toString();

           }
      }


    /**
     * 判断字符串是否为空
     * @param str
     * @return
     */
    public static final boolean isBlank(String str){
        return (str == null)||(str.trim().length() == 0);
    }

    /**
     * 获得字符串
     * @param strName
     * @return
     */
    public static final String getString(String strName){
        return getString(strName,null);
    }

    /**
     * 获得字符串
     * @param strName
     * @param def
     * @return
     */
    public static final String getString(String strName,String def)
    {
        if(strName == null)
        {
            return def;
        }
        else
        {
            return strName;
        }
    }

    /**
     * 把string转换成int
     * @param strName
     * @return
     */
    public static final int getInt(String strName){
        return getInt(strName,0);
    }

    /**
     * 把string转换成int，可设置默认值
     * @param strName
     * @param defaultvalue
     * @return
     */
    public static final int getInt(String strName,int defaultvalue){
        if(strName == null)
        {
            return defaultvalue;
        }
        else
        {
            try{
                return Integer.parseInt(strName.trim());
            }catch(Exception e){
                    return defaultvalue;
            }
        }
    }
    
    /**
     * 把string转换成long
     * @param strName
     * @return
     * @throws Exception
     * @throws NumberFormatException
     */
    public static final long getLong(String strName)
    	throws Exception,NumberFormatException
    {
    	if ( strName==null )
    	{
    		throw new Exception("getLong(String strName):Input value is NULL!");
    	}
    	else
    	{
    		try
			{
				return(Long.parseLong(strName.trim()));
			} 
    		catch (NumberFormatException e) 
			{
    			return(0);
			}
    	}
    }

    /**
     * 把string转换成double
     * @param strName
     * @return
     * @throws Exception
     * @throws NumberFormatException
     */
    public static final double getDouble(String strName)
		throws Exception,NumberFormatException
	{
		if ( strName==null )
		{
			throw new Exception("getDouble(String strName):Input value is NULL!");
		}
		else
		{
			try
			{
				return(Double.parseDouble(strName));
			} 
			catch (NumberFormatException e) 
			{
				return(0);
			}
		}
	}
    
    

    /**
     * 把string转换为float
     * @param strName
     * @return
     * @throws Exception
     * @throws NumberFormatException
     */
    public static final float getFloat(String strName)
		throws Exception,NumberFormatException
	{
		if ( strName==null )
		{
			throw new Exception("Input value is NULL!");
		}
		else
		{
			try
			{
				return(Float.parseFloat(strName.trim()));
			} 
			catch (NumberFormatException e) 
			{
				throw new NumberFormatException("getFloat(String) NumberFormatException for input string:" + strName);
			}
		}
	}
    
    public static final float getFloat(String strName,float def)
		throws Exception,NumberFormatException
	{
		if ( strName==null )
		{
			return(def);
		}
		else
		{
			try
			{
				return(Float.parseFloat(strName.trim()));
			} 
			catch (NumberFormatException e) 
			{
				return(def);
			}
		}
	}

   
    
    /**
     * 把string转换成long
     * @param strName
     * @param defaultvalue
     * @return
     */
    public static final long getLong(String strName,long defaultvalue){
        if(strName == null)
        {
            return defaultvalue;
        }
        else
        {
            try{
                return Long.parseLong(strName);
            }catch(Exception e){
                    return defaultvalue;
            }
        }
    }

    /**
     * 截取字符串一定长度
     * @param s
     * @param length
     * @return
     */
    public static final String getString(String s, int length)
    {
        if( s == null)
        {
            return null;
        }
        else
        {
            if(getNumofByte(s)<=length)
            {
                return s;
            }
            else
            {
                byte[] bb = s.getBytes();
                byte[] temp = new byte[length];
                for(int i = 0; i< length; i ++ )
                {
                    temp[i] = bb[i];
                }
                return getString(temp);
            }
        }


    }
    
    /**
     * 获得字符串字节长度
     * @param s
     * @return
     */
    private static int getNumofByte(String s)
    {
        if( s == null )
        {
            return 0;
        }
        else
        {
           return s.getBytes().length;
        }
    }

    /**
     * 把byte转换成string
     * @param bb
     * @return
     */
    private static final String getString(byte[] bb)
    {
        String s = new String(bb);
        if( s.length() == 0)
        {
            int length = bb.length;
            if(length > 1 )
            {
                byte[] temp = new byte[length - 1];
                for(int i = 0; i < length - 1; i ++)
                {
                    temp[i] = bb[i];
                }
                return getString(temp);
            }
            else
            {
                return "";
            }

        }
        else
        {
            return s;
        }
    }

    /**
     * 对字符串进行md5加密
     * @param s
     * @return
     */
    public static final String getMD5(String s)
    {
        if( s == null )
        {
            return null;
        }
        if( s.trim().length() == 0)
        {
            return s;
        }

        Md5 md5 = new Md5();

        return md5.getMD5ofStr(s);
    }

	
	/**
	 * 按一定长度剪裁字符串，在末尾追加自定义符号
	 * @param str
	 * @param len
	 * @param tail
	 * @return
	 */
	public static final String cutString(String str,int len,String tail)
	{
		int slen = str.length(); 
		
		if ( slen>len )
		{
			str = str.substring(0,len) + tail;
		}
		return(str);
	}
	
	/**
	 * 按一定格式格式化double(#0.00)
	 * @param formatstr
	 * @param number
	 * @return
	 */
	public static String formatNumber(String formatstr,double number)
	{
		java.text.DecimalFormat df = new java.text.DecimalFormat(formatstr);
		return(df.format(number));
	}
	
	/**
	 * 按一定格式格式化long(#0.00)
	 * @param formatstr
	 * @param number
	 * @return
	 */
	public static String formatNumber(String formatstr,long number)
	{
		java.text.DecimalFormat df = new java.text.DecimalFormat(formatstr);
		return(df.format(number));
	}
	
	
	
	/**
	 * 编码backurl（主要对&进行编码）
	 * @param backurl
	 * @return
	 */
	public static String enCodeBackURL(String backurl)
	{
		return(backurl.replace('&','!'));
	}
	
	/**
	 * 解码backurl
	 * @param backurl
	 * @return
	 */
	public static String deCodeBackURL(String backurl)
	{
		return(backurl.replace('!','&'));	
	}
	
	/**
	 * 把二进制转换成long
	 * @param bin
	 * @return
	 */
	public static long bin2Dec(String bin)
	{
		int binLen = bin.length();
		int bit;
		long sum = 0;
		
		for (int i=0; i<binLen; i++)
		{
			bit = getInt( bin.substring(binLen-i-1,binLen-i) );
			sum += bit * Math.pow(2,i);
		}
		
		return(sum);
	}
	
	/**
	 * 获得表单checkbox状态
	 * @param i
	 * @return
	 */
	public static String getStatusForCheckBoxAndRadio(int i)
	{
		if ( i==0 )
		{
			return("");
		}
		else
		{
			return("checked");
		}
	}

	

	
	/**
	 * 去除字符串中的回车换行
	 * @param str
	 * @return
	 */
	public static String removeHH(String str)
	{
		return(StrUtil.replaceString(StrUtil.replaceString(str,"\n",""),"\r",""));
	}
	
	/**
	 * 读取一个URL页面内容
	 * @param Inputurl
	 * @return
	 * @throws Exception
	 */
	public static String readURL(String Inputurl) 
		throws Exception
	{
		String sCurrentLine=""; 
		String sTotalString=""; 
		
		try {
			InputStream urlInputStream;
			URL url;
			HttpURLConnection urlConnection;
			BufferedReader BufferReader;
			//定点、打开、连接
			url = new URL(Inputurl);
			urlConnection = (HttpURLConnection)url.openConnection();
			urlConnection.connect(); 
			urlInputStream = urlConnection.getInputStream();
			//缓冲后可以以characters, arrays, and lines 方式读取数据。
			BufferReader = new BufferedReader(new InputStreamReader(urlInputStream)); 
			while ((sCurrentLine = BufferReader.readLine()) != null) sTotalString += sCurrentLine + "\n";
			BufferReader.close();
		} catch (MalformedURLException e) {
			// TODO 自动生成 catch 块
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自动生成 catch 块
			e.printStackTrace();
		}

		return(sTotalString);
	}
	
	/**
	 * 着色
	 * @param osStr
	 * @param color
	 * @param flag
	 * @return
	 */
	public static String withColor(String osStr,String color,boolean flag)
	{
		if (flag)
		{
			return("<font color='".concat(color).concat("'>").concat(osStr).concat("</font>"));			
		}
		else
		{
			return(osStr);
		}
	}
	
	
	
	
	/**
	 * 模拟浏览器访问一个URL，并获得页面内容
	 * @param wurl
	 * @return
	 * @throws Exception
	 */
	public static final String loginWebSite(String wurl)
		throws Exception
	{	
		java.io.InputStream  in;
		String str;
		StringBuffer sb = new StringBuffer("");
		
		java.net.URL url = new java.net.URL(wurl);
		java.net.HttpURLConnection connection = (java.net.HttpURLConnection)url.openConnection();
		connection = (java.net.HttpURLConnection) url.openConnection();
		connection.setRequestProperty("User-Agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows 2000)");
		connection.connect();
		in = connection.getInputStream();
		java.io.BufferedReader breader = new BufferedReader(new InputStreamReader(in,"UTF-8"));
		
		while((str=breader.readLine())!= null)
		{
			sb.append(str);
			sb.append("\n");
		}
		
		return(sb.toString());
	}
	
	/**
	 * 通过正则表达式替换字符串
	 * @param str				源字符串
	 * @param cp				正则表达式
	 * @param mc				替换内容
	 * @return
	 */
    public static String regReplace(String str, String cp, String mc)
    {
    	String txt = new String();
    	txt = str;

    	if(str!=null && !str.equals(""))
    	{
    		txt = str;
    		Pattern p = Pattern.compile(cp,2); //参数2表示大小写不区分
    		Matcher m = p.matcher(txt);
    		StringBuffer sb = new StringBuffer();
    		int i=0;
    		boolean result = m.find();
            //使用循环将句子里所有匹配的内容找出并替换再将内容加到sb里
    		while(result)
    		{
    			i++;
    			m.appendReplacement(sb, mc);
    			//继续查找下一个匹配对象
    			result = m.find();
    		}
            //最后调用appendTail()方法将最后一次匹配后的剩余字符串加到sb里；
    		m.appendTail(sb);
    		txt = sb.toString();
    	}
    	else
    	{
    		txt = "";
    	}

        return(txt);       
    }
    
    public static String[] regMatchers(String s,String reg,int pos)
    {
    	ArrayList al = new ArrayList();
    	
    	if(s!=null && !s.equals(""))
    	{
    		Pattern p = Pattern.compile(reg,2); //参数2表示大小写不区分
    		Matcher m = p.matcher(s);
    		int i=0;
            //使用循环将句子里所有匹配的内容找出并替换再将内容加到sb里
    		while(m.find())
    		{
    			i++;
    			al.add( m.group(pos) );
    		}
    	}
    	else
    	{
    		return(null);
    	}

        return((String[])al.toArray(new String[0]));       
    }
    
    public static String[][] regMatchers(String s,String reg,int pos[])
    {
    	ArrayList al = new ArrayList();
    	
    	if(s!=null && !s.equals(""))
    	{
    		Pattern p = Pattern.compile(reg,2); //参数2表示大小写不区分
    		Matcher m = p.matcher(s);
    		int i=0;
            //使用循环将句子里所有匹配的内容找出并替换再将内容加到sb里
    		while(m.find())
    		{
    			i++;
    			
    			String row[] = new String[pos.length];
    			for (int ii=0; ii<pos.length; ii++)
    			{
    				row[ii] = m.group(pos[ii]);
    				//System.out.println(pos[ii]+" - "+row[ii]);
    			}
    			al.add( row );
    		}
    	}
    	else
    	{
    		return(null);
    	}

        return((String[][])al.toArray(new String[0][0]));       
    }
    
    /**
     * 获得匹配正则表达式字符串
     * @param s
     * @param reg
     * @param i
     * @return
     */
    public static String regMatcher(String s,String reg,int i)
    {
    	Pattern pattern = Pattern.compile(reg);
    	Matcher matcher = pattern.matcher(s);
    	
    	if(matcher.find())
    	{
    		return(matcher.group(i));
    	}
	    else
	    {
	    	return(null);
	    }
    }
    
    /**
     * 
     * @param row
     * @return
     */
    public static DBRow handleNullRow(DBRow row)
    {
    	if (row==null)
    	{
    		return(new DBRow());
    	}
    	else
    	{
    		return(row);
    	}
    }
    
    public static String handleNull(String val,String def)
	{
		if (val==null)
		{
			return(def);
		}
		else
		{
			return(val);
		}
	}
    
    /**
     * 
     * @param s
     * @return
     */
	public static final String cuteNR(String s)
	{
		s = replaceString(s,"\n","");
		s = replaceString(s,"\r","");
		return(s);
	}
	
	/**
	 * 
	 * @param r
	 * @return
	 */
	public static final int getRowLength(DBRow r[])
	{
		if (r==null)
		{
			return(0);
		}
		else
		{
			return(r.length);			
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public static final double getDoubleType()
	{
		return(0d);
	}
	
	/**
	 * 
	 * @return
	 */
	public static final float getFloatType()
	{
		return(0f);
	}
	
	/**
	 * 
	 * @return
	 */
	public static final long getLongType()
	{
		return(0l);
	}
	
	
	/**
	 * 
	 * @param x
	 * @return
	 */
	public static final long getLongType(String x)
	{
		return(Long.parseLong(x));
	}
	
	/**
	 * 
	 * @return
	 */
	public static final boolean getTrueType()
	{
		return(true);
	}
	
	/**
	 * 
	 * @return
	 */
	public static final boolean getFalseType()
	{
		return(false);
	}
	
	/**
	 * 
	 * @return
	 */
	public static final Object getObjectType()
	{
		return(new Object());
	}
	
	/**
	 * 
	 * @param x
	 * @return
	 */
	public static final boolean getBooleanType(int x)
	{
		if (x==1)
		{
			return(true);			
		}
		else
		{
			return(false);
		}

	}
	
	/**
	 * 
	 * @return
	 */
	public static final Object getNullType()
	{
		return(null);
	}

	

	

	/**
	 * 获得表单select状态 
	 * @param a
	 * @param b
	 * @return
	 */
	public static final String selectIt(long a,long b)
	{
		return( a==b?"selected":"" );
	}
	
	/**
	 * 获得表单checkbox状态
	 * @param a
	 * @param b
	 * @return
	 */
	public static final String checkedIt(long a,long b)
	{
		return( a==b?"checked":"" );
	}
	
	/**
	 * 获得表单checkbox状态
	 * @param a
	 * @param b
	 * @return
	 */
	public static final String checkedIt(String a,String b)
	{
		return( a.equals(b)?"checked":"" );
	}
	
	/**
	 * 
	 * @param c
	 * @return
	 */
	public float getPayCostPercent(float c)
	{
		return( c*100 );
	}
	
	/**
	 * 
	 * @param o
	 * @return
	 */
	public static final int isNull(Object o)
	{
		if (o==null)
		{
			return(1);
		}
		else
		{
			return(0);
		}
	}
	
	public static final String mergeSpace(String key)
	{
		if (key.trim().equals(""))
		{
			return("");
		}
		
		StringBuffer sb = new StringBuffer("");
		int strLen = key.length();
		String tmpStr="",currStr;
		int px;
		
		//先合并连续空格
		for (int i=0; i<strLen; i++)
		{
			px = i+1;
			currStr = key.substring(i,px);
			
			if (currStr.equals(" "))
			{
				if (!currStr.equals(tmpStr))
				{
					sb.append(currStr);
				}
			}
			else
			{
				sb.append(currStr);
			}
			
			tmpStr = currStr;
		}
		
		return(sb.toString().trim());
	}
	
	public static final String mergeChars(String key,String chars)
	{
		if (key.trim().equals(""))
		{
			return("");
		}
		
		StringBuffer sb = new StringBuffer("");
		int strLen = key.length();
		String tmpStr="",currStr;
		int px;
		
		//先合并连续空格
		for (int i=0; i<strLen; i++)
		{
			px = i+1;
			currStr = key.substring(i,px);
			
			if (currStr.equals(chars))
			{
				if (!currStr.equals(tmpStr))
				{
					sb.append(currStr);
				}
			}
			else
			{
				sb.append(currStr);
			}
			
			tmpStr = currStr;
		}
		
		return(sb.toString().trim());
	}

	/**
	 * 过滤掉HTML中所有HTML标签
	 * @param x
	 * @return
	 */
	public static final String getASCFromHTML(String x)
	{
		String htmlReg1 = "<([^>]*)>";
		String htmlReg2 = "&nbsp;";		
		
		x = StrUtil.regReplace(x, htmlReg1, "");
		x = StrUtil.regReplace(x, htmlReg2, "");
		x = x.trim();
		x = StrUtil.replaceString(x, "\t", "");
		
		return(x);
	}
	
	/**
	 * 获得客户端MAC地址
	 * @return
	 */
	public static String getMACAddress() 
	{
		String address = null; 
		String os = System.getProperty("os.name"); 
		//System.out.println(os); 
		
		if (os != null && os.startsWith("Windows")) 
		{
			try 
			{
				ProcessBuilder pb = new ProcessBuilder("ipconfig", "/all"); 
				Process p = pb.start(); 
				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream())); 
				String line; 
				
				while ((line = br.readLine()) != null) 
				{
					if (line.indexOf("Physical Address") != -1) 
					{
						int index = line.indexOf(":"); 
						address = line.substring(index+1); 
						break; 
					}
				}
				br.close(); 
				return address.trim(); 
			}
			catch (IOException e)
			{
			}
		}
		return address; 
	}

	
	
	 
	public static boolean isNull(String value){
		if(value == null){
			return true;
		}else{
			return value.trim().length() < 1; 
		}
	}
	
	public static String convertStringArrayToString(String[] array){
		if(array != null){
			StringBuffer sb = new StringBuffer();
			for(String s : array){
				sb.append(",").append(s);
			}
			return  sb.length() > 0 ? sb.substring(1) :"";
		}
		return "";
	}
	
	public static String[] convertStringToStringArray(String value){
		return value != null && value.trim().length() > 0 ? value.split(","):null;
	}
	
	/**
	 * 全角转半角
	 * @param QJstr
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static final String full2HalfChange(String QJstr)  
    	throws UnsupportedEncodingException 
    {  
		StringBuffer outStrBuf = new StringBuffer(""); 
		String Tstr = "";  
		byte[] b = null;  
		for (int i = 0; i < QJstr.length(); i++) 
		{  
		    Tstr = QJstr.substring(i, i + 1);  
		    // 全角空格转换成半角空格  
		    if (Tstr.equals("　")) 
		    {  
		        outStrBuf.append(" ");  
		        continue;  
		    }  
		    b = Tstr.getBytes("unicode");  
		    // 得到 unicode 字节数据  
		    if (b[2] == -1) 
		    {  
		        // 表示全角？  
		        b[3] = (byte) (b[3] + 32);  
		        b[2] = 0;  
		        outStrBuf.append(new String(b, "unicode"));  
		    } 
		    else 
		    {  
		        outStrBuf.append(Tstr);  
		    }  
		} // end for.  
		
		return outStrBuf.toString();  
		
	}  

// 半角转全角  

	public static final String half2Fullchange(String QJstr)  
	    throws UnsupportedEncodingException
	{  
		StringBuffer outStrBuf = new StringBuffer("");  
		String Tstr = "";  
		byte[] b = null;  
		for (int i = 0; i < QJstr.length(); i++) 
		{  
		    Tstr = QJstr.substring(i, i + 1);  
		    if (Tstr.equals(" ")) 
		    {  
		        // 半角空格  
		        outStrBuf.append(Tstr);  
		        continue;  
		    }  
		    b = Tstr.getBytes("unicode");  
		    if (b[2] == 0) 
		    {  
		        // 半角?  
		        b[3] = (byte) (b[3] - 32);  
		        b[2] = -1;  
		        outStrBuf.append(new String(b, "unicode"));  
		    } 
		    else 
		    {  
		        outStrBuf.append(Tstr);  
		    }  
		}  
		return outStrBuf.toString();  
	}  
	
	public static String getSuffix(String filename) 
	{
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1)
        {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
    }
	
	public static boolean isPictureFile(String fileName) {
		String suffix = getSuffix(fileName).toLowerCase();
		return PICTRUEFILE.indexOf(suffix) != -1 ; 
	}
	
	public static boolean isOfficeFile(String fileName) {
		String suffix = getSuffix(fileName).toLowerCase();
		return OFFICEFILE.indexOf(suffix) != -1;
	}
	
	/**
	 * 处理系统配置(数字=中文描述【回车换行】)组织成Map
	 * @param value
	 * @return
	 */
	public static Map<Integer,String> getConfigMap(String value) {
		Map<Integer,String> tempMap = new HashMap<Integer, String>();
		 if(value != null && value.trim().length() > 0 ){
			 String[] arraySelected = value.split("\n");
			 if(arraySelected != null && arraySelected.length > 0 ){
				 for(int index = 0 , count = arraySelected.length ; index < count ; index++ ){
					 String tempStr =  arraySelected[index];
					 if(tempStr.indexOf("=") != -1){
						 String[] tempValues = tempStr.split("=");
						 if(tempValues != null && tempValues.length == 2){
							 tempMap.put(Integer.parseInt(tempValues[0]), tempValues[1]);
						 } 
					 }
				 }
			 }
 
		 }
		 return tempMap ;
	}
	
	/**
	 * 获得节点数据（只适合唯一名称节点）
	 * @return
	 */
	public static String getSampleNode(String xml,String name)
		throws Exception
	{
			String xmlSplit1[] = xml.split("<"+name+">");
			String xmlSplit2[] = xmlSplit1[1].split("</"+name+">");
			if(0 != xmlSplit2.length)
			{
				return(xmlSplit2[0]);
			}
			else
			{
				return "";
			}
	}
	
	/**
	 * 加密算法
	 * @param str
	 * @return
	 */
	public static  String HashBase64(String str)
		throws Exception
	{
		String ret="";
		 try 
		 {
		    //Hash算法
		   MessageDigest sha = MessageDigest.getInstance("SHA-1");
		   sha.update(str.getBytes());  
		   ret = new String(new org.apache.commons.codec.binary.Base64().encode(sha.digest()));
		 }
		 catch (Exception e) 
		 {
		  throw e;
		 }
		 return ret;			
	}
	
 
	
	
	/**
	 * 判断字条串是否为空，且是否可以转换成long型
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public static boolean isBlankAndCanParseLong(String str) throws Exception
	{
		if(str != null && str.trim().length() != 0 && !"undefined".equals(str))
		{
			long parseLong = 0L;
			try
			{
				parseLong = Long.parseLong(str);
			}
			catch (NumberFormatException e)
			{
				parseLong = 0L;
			}
			if(0 == parseLong)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	}
	
	public static String getFileName(String fileName){
		int indexLastDoll =  fileName.lastIndexOf(".") ;
		if(indexLastDoll != -1){
			return fileName.substring(0,indexLastDoll);
		}
 		return fileName ;
	}
	 
	
		
		public static String parseIndexTo4Str(int i)
		{
			String iStr = String.valueOf(i);
			String l = "";
			if(iStr.length() < 4)
			{
				int c = 4 - iStr.length();
				for (int j = 0; j < c; j++)
				{
					l += "0";
				}
			}
			return l + iStr;
		}
		
		
		public static String fromStringGetNumber(String str)
		{
			String reStr = "";
			if(!isBlank(str))
			{
				String regEx="[^0-9]";   
				Pattern p = Pattern.compile(regEx);   
				Matcher m = p.matcher(str);   
				reStr = m.replaceAll("").trim();
			}
			return reStr;
		}
		
		
		public static final String replaceEnterToBlank(String s)
		   {
		    	if (( s == null || s.length() ==0))
		        {
		             return s;
		        }
		        else
		        {
		             s = replaceString(s,"\r\n","");
		             s = replaceString(s,"\n","");
		             return s;
		        }
		  }
		
		
		/**
		 * 判断字条串是否为空，且是否可以转换成long型
		 * @param str
		 * @return
		 * @throws Exception
		 */
		public static boolean isBlankAndCanParseLongOriginal(String str) throws Exception
		{
			if(str != null && str.trim().length() != 0 && !"undefined".equals(str))
			{
				boolean isCan = true;
				try
				{
					Long.parseLong(str);
				}
				catch (NumberFormatException e)
				{
					isCan = false;
				}
				return isCan;
			}
			else
			{
				return true;
			}
		}
		
		public static boolean isNumber(String str){
 			Matcher matcher = NumberPattern.matcher(str);
			return matcher.matches();
		}
		
		private static int covertChar2PhotoNumber(char c){
			int covertInt = (int)c ;
			int returnInt =  0 ;
			if(covertInt <= (int)'C' && covertInt >= (int)'A'){
				return 2 ;
			} 
			if(covertInt <= (int)'F' && covertInt >= (int)'D'){
				return 3 ;
			} 
			if(covertInt <= (int)'I' && covertInt >= (int)'G'){
				return 4 ;
			} 
			if(covertInt <= (int)'L' && covertInt >= (int)'J'){
				return 5 ;
			} 
			if(covertInt <= (int)'O' && covertInt >= (int)'M'){
				return 6 ;
			} 
			if(covertInt <= (int)'S' && covertInt >= (int)'P'){
				return 7 ;
			} 
			if(covertInt <= (int)'V' && covertInt >= (int)'T'){
				return 8 ;
			} 
			if(covertInt <= (int)'Z' && covertInt >= (int)'W'){
				return 9 ;
			}
			return  returnInt ;
		}
		
		public static String convertStr2PhotoNumber(String value){
			if(value != null && value.length() > 0 ){
				value = value.toUpperCase();
				StringBuilder arraysChars = new StringBuilder();
				for(int index = 0 , count = value.length() ; index < count ;index++ ){
					char a = value.charAt(index);
					if(isNumber(String.valueOf(a))){
						arraysChars.append(a) ;
					}else{
						arraysChars.append(covertChar2PhotoNumber(a));
	 				}
	 			}
				return arraysChars.toString();
 			}
			return "";
		}
		 
		
		 /**
	     * 判断字符串是否为空
	     * @param str
	     * @return
	     */
	    public static final boolean isBlankNullUndefined(String str){
	        return (str == null)||(str.trim().length() == 0)||"NULL".equalsIgnoreCase(str)||"undefined".equalsIgnoreCase(str);
	    }
	    
	    /**
	     * 替换字符串中一些字符
	     * @param str
	     * @param rFrom
	     * @return
	     * @throws Exception
	     * @author:zyj
	     * @date: 2015年6月5日 上午10:07:38
	     */
	    public static final String replacePartStr(String str, String... rFrom) throws Exception
	    {
	    	if(isBlank(str))
	    		return str;
	    	if(null != rFrom)
	    	{
	    		for (int i = 0; i < rFrom.length; i++) 
	    		{
					str = str.replaceAll(rFrom[i], "");
				}
	    	}
	    	return str;
	    }
}










