package com.cwc.app.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

public class Neo4jTreeBuilder {
	private JSONArray paths;
	private Map<Integer,Map<String,Object>> nodeData = new HashMap<Integer,Map<String,Object>>();
	private Map<Integer,Set<Integer>> nodeChildren = new HashMap<Integer,Set<Integer>>();
	private Map<String,Map<String,Object>> relProps = new HashMap<String,Map<String,Object>>(); //key is <parentId,childId>
	
	public Neo4jTreeBuilder(JSONArray paths){
		this.paths = paths;
	}
	
	public JSONObject build() throws Exception {
		for(int p=0; p<paths.length(); p++){ //循环所有的路径
			JSONArray nodes = paths.getJSONArray(p).getJSONArray(0);
			JSONArray rels  = paths.getJSONArray(p).getJSONArray(1);
			
			for(int n=nodes.length()-1; n>=0; n--){
				JSONObject node = nodes.getJSONObject(n);
				Integer id = nodeId(node);
				Map<String,Object> props = props(node);
				
				Integer parentId = n == 0 ? 0 : nodeId(nodes.getJSONObject(n-1));
				if(parentId !=0 ) relProps.put(parentId+"-"+id, props(rels.getJSONObject(n-1)));
				if(! nodeData.containsKey(id)){
					nodeData.put(id, props);
				}
				if(! nodeChildren.containsKey(parentId)){
					nodeChildren.put(parentId, new HashSet<Integer>());
				}
				nodeChildren.get(parentId).add(id);
				
			}
		}
		
		return buildTree(0);
	}
	
	private JSONObject buildTree(Integer id) throws Exception {
		JSONObject node = new JSONObject(nodeData.get(id));
		Set<Integer> childrenIds = nodeChildren.get(id);
		if(childrenIds == null) return node;
		JSONArray children = new JSONArray();
		for(Integer cid:childrenIds){
			JSONObject c = buildTree(cid);
			if(relProps.containsKey(id+"-"+cid)){
				Map<String,Object> rp = relProps.get(id+"-"+cid);
				for(String k:rp.keySet()){
					c.put(k, rp.get(k));
				}
			}
			children.put(c);
		}
		node.put("children", children);
		return node;
	}
	
	private Integer nodeId(JSONObject n) throws Exception {
		String[] ss =  n.getString("self").split("/");
		return Integer.parseInt(ss[ss.length-1]);
	}
	
	private Map<String,Object> props(JSONObject n) throws Exception {
		JSONObject d = n.getJSONObject("data");
		Map<String,Object> m = new HashMap<String,Object>();
		for(Iterator<String> kitr = d.keys(); kitr.hasNext(); ){
			String k = kitr.next();
			m.put(k, d.get(k));
		}
		return m;
	}
	
}
