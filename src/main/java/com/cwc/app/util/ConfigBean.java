package com.cwc.app.util;

import java.util.HashMap;


/**
 * 封装系统配置的bean
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ConfigBean 
{
	private static HashMap beans = new HashMap();
	
	public static String getStringValue(String name) 
	{
		return((String)beans.get(name));
	}
	
	public static int getIntValue(String name) 
	{
		return( StrUtil.getInt((String)beans.get(name)) );
	}

	public static void putBeans(String key,String classname)
	{
		beans.put(key,classname);
	}

	public static void desctroy()
	{
		beans.clear();
		beans = null;
	}
	
	public static void main(String args[])
	{
		HashMap hm = new HashMap();
		
		hm.put("a","b");
		//hm.clear();
		
	}
}
