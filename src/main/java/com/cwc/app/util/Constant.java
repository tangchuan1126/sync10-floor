package com.cwc.app.util;

public interface Constant {
	
	public String SessionUser="UserSession";
	
	public int Language=0;   // 0是中文  ，1 是英文
	
	public String Is="是";
	public String IsEn="Yes";
	
	public String No="不是";
	public String NoEn="No";
	
	public String Opera="资料权限";
	public String OperaEn="Oper";
	
	public String Menu="菜单权限";
	public String MenuEn="Menu";
	
	public String Comm="指令权限";
	public String CommEn="Command";
	
	public String ALL="全部";
	public String ALLEN="ALL";
	
	public   String historyName="his";
	public   String lasHistoryName="lasthis";
	
	public     String command="cmd";
	public     String log="log";
	public     String login="login";
	
	public String cmdback="cmdback";
	public String message="mess";
	public String alarm="alarm";
	
	public   String online="online";
	
	public int LASTHALFHOUR=1800000;
     
    /*
     * 发送指令ID
     */
	 public int LOC=1;
	 public int TRACE=2;
	 public int TRACECANCEL=3;
	 public int OVERSPEED=4;
	 public int OVERSPEEDCANCEL=5;
	 
	 public int KEYPOINT=6;
	 public int KEYPOINTCANCEL=7;
	 public int PATHSET=8;
	 public int PATHSETCANCEL=9;
	 
	 public int INFENCE=10;
	 public int OUTFENCE=11;
	 public int INOUTFENCE=12;
	 public int FENCECANCAL=13;
	 
	 public int STOPOIL=14;
	 public int CONPOIL=15;
	 
	 public int MESS=16; //发送短信
	 
	 public int ADDDEPART=100;
	 public int UPDDEPART=101;
	 public int DELDEPART=102;
	 
	 public int ADDUSER=120;
	 public int UPDUSER=121;
	 public int DELUSER=122;
	 
	 
	 public int ADDASSET=140;
	 public int UPDASSET=141;
	 public int DELASSET=142;
	 
	 public int ADDDEPRASSET=144;
	 public int DELDEPRASSET=145;
	 
	 public int ADDUSRRASSET=148;
	 public int DELUSRRASSET=149;
	 
	 
	 
	 public int ADDLABEL=150;
	 public int UPDLABEL=151;
	 public int DELLABEL=152;
	 
	 public int ADDALARMLABEL=160;
	 public int UPDALARMABEL=161;
	 public int DELALARMLABEL=162;
	 
	 public int DELOVERSPEEDSET=170;
	 public int DELKEYPOINTSET=171;
	 
	 
	 public int ADDDEPARTPRESS=180;
	 public int DELDEPARTPRESS=181;
	 
	 public int ADDUSERPRESS=190;
	 public int DELUSERPRESS=191;
	 
	 
	 
	 
	 /*
	  * geotype 
	  */
	 public int MAPPOINT=1;
	 public int MAPPATH=2;
	 public int MAPPOLYGON=3;
	 public int MAPCIRCLE=4;
	 public int MAPRECT=5;
	 
	 /*
	  * alarmtype
	  */
	 public int KEYPOINTALARM=1;
	 public int OVERSPEEDALARM=2;
	 public int PATHALARM=3;
	 public int INALARM=4;
	 public int OUTALARM=5;
	 public int INOUTALARM=6;
	 
	 
}
