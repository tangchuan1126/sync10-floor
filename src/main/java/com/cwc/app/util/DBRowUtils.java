package com.cwc.app.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.app.beans.AdminLoginBean;
import com.cwc.db.DBRow;

public class DBRowUtils {
	public static String dbRowAsString(DBRow para) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("{\n");
		String indent = "    ";
		for (Object field : para.getFieldNames()) {
			sb.append(indent).append(field).append("=")
					.append(para.getValue(field.toString())).append("\n");
		}
		sb.append("}");
		return sb.toString();
	}
	
	public static Map<String, Object> dbRowAsMap(DBRow para) throws Exception {
		
		Map<String, Object> result = new HashMap<String, Object>();
		for (Object field : para.getFieldNames()) {
			result.put(field.toString().toLowerCase(),
					para.getValue(field.toString()));
		}

		return result;
	}
	
	public static DBRow convertToDBRow(JSONObject jo) throws Exception {
		DBRow r = new DBRow();
		for (Iterator<String> itr = jo.keys(); itr.hasNext();) {
			String k = itr.next();
			r.add(k, jo.get(k));
		}
		return r;
	}
	
	public static DBRow convertToDBRow(JSONArray ja, String... keys) throws Exception {
		DBRow r = new DBRow();
		for(int i=0; i<keys.length && i<ja.length(); i++){
			if(keys[i] == null) continue; //skip field
			r.add(keys[i], ja.get(i));
		}
		return r;
	}

	public static JSONArray dbRowArrayAsJSON(DBRow[] rows) throws Exception {
		JSONArray a = new JSONArray();
		for(DBRow row:rows){
			a.put(dbRowAsMap(row));
		}
		return a;
	}
	
	public static Map<String,Object>  jsonObjectAsMap(JSONObject jo) throws Exception{
		Map<String,Object> m = new HashMap<String,Object>();
		for(Iterator<String> i=jo.keys(); i.hasNext(); ){
			String k = i.next();
			m.put(k, jo.get(k));
		}
		return m;
	}

	public static DBRow[] jsonArrayAsDBRowArray(JSONArray data, String... fieldNames) throws Exception {
		DBRow[] rows = new DBRow[data.length()];
		for(int i=0; i<data.length(); i++){
			rows[i] = convertToDBRow(data.getJSONArray(i), fieldNames);
		}
		return rows;
	}
	
	/**
	 * JSON 转换   DBRow内包含 DBRow[] 
	 * @param JSONObject
	 * @return DBRow
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public static DBRow convertToMultipleDBRow(JSONObject parameter) throws Exception {
		
		DBRow row = new DBRow();
		
		for (Iterator<String> result = parameter.keys(); result.hasNext();) {
			
			String key = result.next();
			
			if(parameter.get(key) instanceof JSONArray){
				
				row.add(key,jsonArrayAsDBRowArray((JSONArray)parameter.get(key)));
				continue;
			}
			
			row.add(key,parameter.get(key));
			
		}
		return row;
	}
	
	public static DBRow[] jsonArrayAsDBRowArray(JSONArray parameter) throws Exception {
		
		DBRow[] rows = new DBRow[parameter.length()];
		
		for(int i=0; i<parameter.length(); i++){
			
			rows[i] = convertToMultipleDBRow(parameter.getJSONObject(i));
			
		}
		return rows;
	}
	
	/**
	 * DBRow[]内包含无限DBRow,DBRow[] 转换为 JSON
	 * @param DBRow[]
	 * @return JSONArray
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public static JSONArray multipleDBRowArrayAsJSON(DBRow[] rows) throws Exception {
		
		JSONArray a = new JSONArray();
		
		if(rows != null){
			
			for(DBRow row:rows){
				
				a.put(multipleDBRowAsMap(row));
			}
		}
		
		return a;
	}
	
	public static Map<String, Object> multipleDBRowAsMap(DBRow para) throws Exception {
		
		Map<String, Object> result = new LinkedHashMap<String, Object>();
		
		for (Object field : para.getFieldNames()) {
			
			//包含DBRow[]
			if(para.getValue(field.toString()) instanceof DBRow[]){
				//递归方法
				result.put(field.toString().toLowerCase(), DBRowUtils.multipleDBRowArrayAsJSON((DBRow[])para.getValue(field.toString())));
			//包含DBRow
			}else if(para.getValue(field.toString()) instanceof DBRow){
				
				result.put(field.toString().toLowerCase(), DBRowUtils.dbRowAsMap((DBRow)para.getValue(field.toString())));
			//普通值
			}else{
				result.put(field.toString().toLowerCase(),para.getValue(field.toString()));
			}
		}
		return result;
	}
	
	 public static DBRow mapConvertToDBRow(Map<String,Object> map)
		throws Exception
	 {
	   	DBRow dbrow = new DBRow();
	   	
//	   	dbrow.putAll(map);
	   	Set<String> mapKeys = map.keySet();
	   	for (String key : mapKeys) 
	   	{
			dbrow.add(key,map.get(key));
		}
	   	return dbrow;
	}
	 

	/**
	 * AdminLoginBean类型转换
	 * @param session.getAttribute(Config.adminSesion)
	 * @return AdminLoginBean
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public static AdminLoginBean getAdminLoginBean(Object param){
		
		AdminLoginBean adminBean = new AdminLoginBean();
		
		if(param != null){
			
			if(param instanceof AdminLoginBean){
				
				adminBean = (AdminLoginBean)param;
				
			}else if(param instanceof Map){
				
				@SuppressWarnings("unchecked")
				Map<String,Object> adminMap = (Map<String,Object>)param;
				
				adminBean.setAccount(adminMap.get("account") == null ? null : adminMap.get("account").toString());
				adminBean.setAdid(adminMap.get("adid") == null ? null : Long.valueOf(adminMap.get("adid").toString()));
				adminBean.setCity(adminMap.get("city") == null ? null : Long.valueOf(adminMap.get("city").toString()));
				adminBean.setEmail(adminMap.get("email") == null ? null : adminMap.get("email").toString());
				adminBean.setEmploye_name(adminMap.get("employe_name") == null ? null : adminMap.get("employe_name").toString());
				adminBean.setLoginDate(adminMap.get("loginDate") == null ? null : adminMap.get("loginDate").toString());
				adminBean.setProvince(adminMap.get("province") == null ? null : Long.valueOf(adminMap.get("province").toString()));
				adminBean.setPs_id(adminMap.get("ps_id") == null ? null : Long.valueOf(adminMap.get("ps_id").toString()));
				adminBean.setAdministrator(adminMap.get("administrator") == null ? null : Boolean.valueOf(adminMap.get("administrator").toString()));
				
				if(Boolean.valueOf(adminMap.get("login") == null ? null : adminMap.get("login").toString())){
					
					adminBean.setIsLogin();
				}
				
				if(Boolean.valueOf(adminMap.get("loginRightPath") == null ? null : adminMap.get("loginRightPath").toString())){
					
					adminBean.setIsLoginRightPath();
				}
				
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> titles = adminMap.containsKey("titles") ? (List<Map<String,Object>>)adminMap.get("titles") : null;
				
				if(titles != null){
					
					DBRow[] titlesArray = new DBRow[titles.size()];
					
					for(int i=0; i<titlesArray.length; i++){
						
						titlesArray[i] = new DBRow();
						
						for(@SuppressWarnings("rawtypes") Map.Entry e: titles.get(i).entrySet()){
							
							titlesArray[i].addObject((String)e.getKey(), e.getValue());
						}
					}
					
					adminBean.setTitles(titlesArray);
				}
				
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> depts = adminMap.containsKey("department") ? (List<Map<String,Object>>)adminMap.get("department") : null;
				
				if(depts != null){
					
					DBRow[] deptsArray = new DBRow[depts.size()];
					
					for(int i=0; i<deptsArray.length; i++){
						
						deptsArray[i] = new DBRow();
						
						for(@SuppressWarnings("rawtypes") Map.Entry e: depts.get(i).entrySet()){
							
							deptsArray[i].addObject((String)e.getKey(), e.getValue());
						}
					}
					
					adminBean.setDepartment(deptsArray);
				}
				
				@SuppressWarnings("unchecked")
				List<Map<String,Object>> wares = adminMap.containsKey("warehouse") ? (List<Map<String,Object>>)adminMap.get("warehouse") : null;
				
				if(wares != null){
					
					DBRow[] waresArray = new DBRow[wares.size()];
					
					for(int i=0; i<waresArray.length; i++){
						
						waresArray[i] = new DBRow();
						
						for(@SuppressWarnings("rawtypes") Map.Entry e: wares.get(i).entrySet()){
							
							waresArray[i].addObject((String)e.getKey(), e.getValue());
						}
					}
					
					adminBean.setWarehouse(waresArray);
				}
			}
		}
		
		return adminBean;
	}
	
	/**
	 * 判断AdminLoginBean里是否有此部门ID和职务ID
	 * @param 部门ID,职务ID,AdminLoginBean
	 * @return true/false
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public static boolean existDeptAndPostInLoginBean(long deptId,long postId,AdminLoginBean adminBean){
		
		boolean result = false;
		
		for(DBRow oneResult : adminBean.getDepartment()){
			
			if(Long.valueOf(oneResult.get("deptid", "-1"))==deptId && Long.valueOf(oneResult.get("postid", "-1"))==postId){
				
				result = true;
				break;
			}
		}
		
		return result;
	}
	
	/**
	 * 判断部门数组里是否有此部门ID和职务ID
	 * @param 部门ID,职务ID,部门数组
	 * @return true/false
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public static boolean existDeptAndPostInDeptArray(long deptId,long postId,DBRow[] dept){
		
		boolean result = false;
		
		for(DBRow oneResult : dept){
			
			if(Long.valueOf(oneResult.get("deptid", "-1"))==deptId && Long.valueOf(oneResult.get("postid", "-1"))==postId){
				
				result = true;
				break;
			}
		}
		
		return result;
	}
	
	/**
	 * 判断AdminLoginBean里是否有此部门ID和职务ID
	 * @param 部门ID,职务ID,AdminLoginBean
	 * @return true/false
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	public static boolean existDeptAndPostInLoginBean(long deptId,AdminLoginBean adminBean){
		
		boolean result = false;
		
		for(DBRow oneResult : adminBean.getDepartment()){
			
			if(Long.valueOf(oneResult.get("deptid", "-1"))==deptId){
				
				result = true;
				break;
			}
		}
		
		return result;
	}
	
	/**
	 * 记录日志：DBRow 转换为特定的  DBRow[]
	 * @param DBRow
	 * @return DBRow[]
	 * @author subin
	 **/
	public static DBRow[] dbRowConvertToArray(DBRow row){
		
		DBRow[] result = null;
		
		if(row != null){
			
			@SuppressWarnings("unchecked")
			ArrayList<String> list = row.getFieldNames();
			
			List<DBRow> tmp = new ArrayList<DBRow>();
			
			if(list != null && list.size() > 0){
				
				for(String one : list){
					
					DBRow dbRow = new DBRow();
					dbRow.put("column", one);
					dbRow.put("col_key", row.get(one));
					dbRow.put("col_val", "");
					
					tmp.add(dbRow);
				}
				
				result = new DBRow[tmp.size()];
				
				tmp.toArray(result);
			}
		}
		
		return result;
	}
}
