package com.cwc.app.util;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConstantC {
	public static final int CH=0;
	public static final int EN=1;
	
	public static final int QUICKMAP=0;
	public static final int GOOGLEMAP=1;
	public static final int BAIDUMAP=2;
	
	public static int Lang=0;
	
    public String mongodbHost;
	public String mongodbData;
	public String mongodbUser;
	public String mongodbPassword;
	
	public static SimpleDateFormat dataFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
	public static SimpleDateFormat dataFormat2=new SimpleDateFormat("yyyy-MM-dd"); 
	
   public static Map<String,String> statusMap=new HashMap<String,String>();
   public static Map<String,Boolean> statusAlarmMap=new HashMap<String,Boolean>();
   
   public static Collection mapCol;
   public static List mapType=new ArrayList();
   
   public static Map<String,Integer> mapTypeMap=new HashMap<String,Integer>();
   public static Map<Integer,String> mapTypeMap2=new HashMap<Integer,String>();
   
   public static List alarmType=new ArrayList(); 
   public static Map<String,String> alarmMap=new HashMap<String,String>();
   
   public static double ONEM=1000000.0;
   private static int MAXCOUNT=10000;
   private static int MAXCOUNT2=100;
   private static int SEQ=1;
	
	public String longToTime(long l){
		Date d=new Date(l);
		return dataFormat.format(d);
	}
	public String longToDate(long l){
		Date d=new Date(l);
		return dataFormat2.format(d);
	}
	public long timeToLong(String t){
		try {
			return dataFormat.parse(t).getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return new Date().getTime();
	}
	
	public String getUId(){
		String id=System.currentTimeMillis()+""+SEQ++;
		if(SEQ>MAXCOUNT){
			SEQ=1;
		}
		return id;
	}
	
	
	public long getUIdL(){
		long id=System.currentTimeMillis()*MAXCOUNT2+SEQ++;
		if(SEQ>=MAXCOUNT2){
			SEQ=1;
		}
		return id;
	}
/**  
     *  用getBytes(encoding)：返回字符串的一个byte数组  
     *  当b[0]为  63时，应该是转码错误  
     *  A、不乱码的汉字字符串：  
     *  1、encoding用GB2312时，每byte是负数；  
     *  2、encoding用ISO8859_1时，b[i]全是63。  
     *  B、乱码的汉字字符串：  
     *  1、encoding用ISO8859_1时，每byte也是负数；  
     *  2、encoding用GB2312时，b[i]大部分是63。  
     *  C、英文字符串  
     *  1、encoding用ISO8859_1和GB2312时，每byte都大于0；  
     *  <p/>  
     *  总结：给定一个字符串，用getBytes("iso8859_1")  
     *  1、如果b[i]有63，不用转码；  A-2  
     *  2、如果b[i]全大于0，那么为英文字符串，不用转码；  B-1  
     *  3、如果b[i]有小于0的，那么已经乱码，要转码。  C-1  
     */  
   public String  changeEncode(String  str)  {  
           if  (str  ==  null)  return  null;  
           String  retStr  =  str;  
           byte  b[];  
           try  {  
                   b  =  str.getBytes("ISO8859_1");  

                   for  (int  i  =  0;  i  <  b.length;  i++)  {  
                           byte  b1  =  b[i];  
                           if  (b1  ==  63)  
                                   break;    //1  
                           else  if  (b1  >  0)  
                                   continue;//2  
                           else  if  (b1  <  0)  {        //不可能为0，0为字符串结束符 
                                   retStr  =  new  String(b,  "UTF-8");  
                                   break;  
                           }  
                   }  
           }  catch  (UnsupportedEncodingException  e)  {  
                   //  e.printStackTrace();    //To  change  body  of  catch  statement  use  File    |  Settings    |  File  Templates.  
           }  
           return  retStr;  
   } 
   
   
   
   public String getIs(int i){
	   if(i>0){
		   if(Constant.Language==0){
			   return Constant.Is;
		   }else{
			   return Constant.IsEn;
		   }
	   }else{
		   if(Constant.Language==0){
			   return Constant.No;
		   }else{
			   return Constant.NoEn;
		   } 
	   }
   }
   
   public String getPremmis(int i){
	   if(i==1){
		   if(Constant.Language==0){
			   return Constant.Opera;
		   }else{
			   return Constant.OperaEn;
		   }
	   }else if(i==2){
		   if(Constant.Language==0){
			   return Constant.Menu;
		   }else{
			   return Constant.MenuEn;
		   } 
	   }else if(i==3){
		   if(Constant.Language==0){
			   return Constant.Comm;
		   }else{
			   return Constant.CommEn;
		   } 
	   }
	   return "";
   }
   
   public boolean isNull(String s){
	   return s==null||"".equals(s);
   }
   
   
   public PointD changeMap(double lon,double lat,int mapType){
	   if(mapType==QUICKMAP){
		   return new PointD(lon,lat);
	   }else if(mapType==GOOGLEMAP){
		 return  MarsTitleSystem.getGoogle(null).latLongToMars(lon, lat);
	   }else if(mapType==BAIDUMAP){
		   return MarsTitleSystem.getBaidu(null).latLongToMars(lon, lat);
	   }
	   return new PointD(lon,lat);
   }
   
   public PointD reverseMap(double lon,double lat,int mapType){
	   if(mapType==QUICKMAP){
		   return new PointD(lon,lat);
	   }else if(mapType==GOOGLEMAP){
		 return  MarsTitleSystem.getGoogle(null).marsToLatLong(lon, lat);
	   }else if(mapType==BAIDUMAP){
		   return MarsTitleSystem.getBaidu(null).marsToLatLong(lon, lat);
	   }
	   return new PointD(0,0);
   }
   public String getStatus(String s){
	   if(isNull(s)){
		   return "";
	   }
	   String[] ss=s.split(",");
	   StringBuffer sb=new StringBuffer();
	   for(int i=0;i<ss.length;i++){
		   String sss=statusMap.get(ss[i]);
		   if(sss!=null){
		     sb.append(sss).append(",");
		   }
	   }
	   if(sb.length()>1){
		   sb.deleteCharAt(sb.length()-1);
	   }
	   return sb.toString();
   }
public void setMongodbHost(String mongodbHost) {
	this.mongodbHost = mongodbHost;
}
public void setMongodbData(String mongodbData) {
	this.mongodbData = mongodbData;
}
public void setMongodbUser(String mongodbUser) {
	this.mongodbUser = mongodbUser;
}
public void setMongodbPassword(String mongodbPassword) {
	this.mongodbPassword = mongodbPassword;
}
}
