package com.cwc.app.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

public class Neo4jContainerTreeBuilder {
	private JSONArray data;
	private JSONObject tree;
	private long root_con_id=0;
	
	private Map<Long,JSONObject> nodes = new HashMap<Long,JSONObject>();
	
	public Neo4jContainerTreeBuilder(JSONArray data) throws Exception {
		if(data==null || 
				data.length() == 0 
				// || data.getJSONArray(0).length() == 0 ||  data.getJSONArray(0).getJSONArray(0).length() == 0 
			) //空数据集
			tree = new JSONObject();
		else 
			this.data = data;
	}
	public Neo4jContainerTreeBuilder(JSONArray data,long root_con_id) throws Exception {
		this(data);
		this.root_con_id = root_con_id;
	}
	
	
	public void build() throws Exception {
		if(tree != null) return;
		for(int i=0; i<data.length(); i++){
			//获得路径数据
			JSONArray row = data.getJSONArray(i);
			JSONArray col = row.getJSONArray(0); //返回数据只有一列，所以取0
			//对关系数据做循环
			for(int r=0; r<col.length(); r++){
				//每个关系上有三段
				JSONArray rel = col.getJSONArray(r);
				JSONObject startNode = rel.getJSONObject(0).getJSONObject("data");
				JSONObject relation = rel.getJSONObject(1).getJSONObject("data");
				JSONObject endNode = rel.getJSONObject(2).getJSONObject("data");
				addTreeNode(startNode,relation,endNode);
			}
		}
		
		if(root_con_id == 0) root_con_id = data.getJSONArray(0).getJSONArray(0).getJSONArray(0).getJSONObject(0).getJSONObject("data").getLong("con_id");
		
		tree = buildTree(nodes.get(root_con_id));
	}
	
	public JSONObject buildTree(JSONObject node) throws Exception {
		JSONArray children = new JSONArray();
		Set<Long> childrenIds = new HashSet<Long>();
		JSONArray products = new JSONArray();
		Set<Long> productsIds = new HashSet<Long>();
		
		JSONArray cs = node.has("children") ? node.getJSONArray("children") : new JSONArray();
		for(int i=0; i<cs.length(); i++){
			JSONObject c = new JSONObject(cs.getJSONObject(i).toString());
			if(c.has("pc_id")){
				//先判断这个产品节点是否已经添加过
				if(productsIds.contains(c.getLong("pc_id"))) continue;
				accumulate(products, new JSONArray().put(c), true);
				node.put("title_id", c.get("title_id"));
				productsIds.add(c.getLong("pc_id"));
			}
			else {
				//先判断这个子节点是否已经添加过
				long conId = c.getLong("con_id");
				if(childrenIds.contains(conId)) continue;
				
				JSONObject child = buildTree(c);
				//累计数量
				accumulate(products,child.getJSONArray("products"), false);
				children.put(child);
				if(child.has("title_id")) node.put("title_id", child.get("title_id"));
				childrenIds.add(conId);
			}
			
		}
		
		if(node.has("con_id")) {
			if(children.length() > 0)  node.put("children", children);
			else node.remove("children");
			node.put("products", products);
		}
		
		return node;
	}
	
	public void accumulate(JSONArray ps1, JSONArray ps2, boolean directContains) throws Exception {
		for(int i=0; i<ps2.length(); i++){
			JSONObject p2 = ps2.getJSONObject(i);
			boolean found = false;
			for(int j=0; j<ps1.length(); j++ ){
				JSONObject p1 = ps1.getJSONObject(j);
				
				if(p1.getLong("pc_id") == p2.getLong("pc_id") && p1.getLong("title_id") == p2.getLong("title_id")){
					p1.put("total_quantity", p1.getInt("total_quantity") + p2.getInt("quantity"));
					p1.put("total_locked_quantity", p1.getInt("total_locked_quantity") + p2.getInt("locked_quantity"));
					found = true;
					break;
				}
			}//for
			if(!found){
				JSONObject p = new JSONObject(p2.toString());
				p.put("total_quantity", p.get("quantity"));
				p.put("total_locked_quantity", p.get("locked_quantity"));
				if(!directContains) { //因为暂时上层容器未直接包含此产品，所以是0
					p.put("quantity", 0); 
					p.put("locked_quantity", 0);
				}
				ps1.put(p);
			}
		}//for
	}
	
	public void addTreeNode(JSONObject from, JSONObject rel, JSONObject to) throws Exception {
		//如果to节点是产品节点，则首先把关系上的所有属性复制到to节点上
		to = new JSONObject(to.toString());
		if(to.has("pc_id")){
			for(Iterator<String> itr = rel.keys(); itr.hasNext(); ){
				String k = itr.next();
				to.put(k, rel.get(k));
			}
		}
		
		//查找节点并建立关系
		JSONObject fromNode = nodes.get(from.getLong("con_id"));
		if(fromNode == null){
			fromNode = new JSONObject(from.toString());
			nodes.put(from.getLong("con_id"), fromNode);
		}
		JSONObject toNode = null;
		if(to.has("pc_id")) {
			toNode = to;
		}
		else {
			toNode = nodes.get(to.getLong("con_id"));
			if(toNode == null) {
				toNode = to;
				nodes.put(to.getLong("con_id"), toNode);
			}
		}
		
		if(!fromNode.has("children")) fromNode.put("children", new JSONArray());
			
		fromNode.getJSONArray("children").put(toNode);
		
	}
	
	public JSONObject getTree(){
		return tree;
	}
	
}
