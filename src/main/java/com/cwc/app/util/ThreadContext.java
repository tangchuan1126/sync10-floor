package com.cwc.app.util;

import java.util.HashMap;

/**
 * 局部线程变量
 * 用来保存线程中的临时数据，非常有用
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class ThreadContext
{
	private static class ThreadContextImp extends  ThreadLocal
	{
		protected synchronized Object initialValue()
		{
			return( new HashMap() );
		}
	}
	
	private static ThreadContextImp tci = new ThreadContextImp();
	
	public static void put(Object key,Object val)
	{
		((HashMap)tci.get()).put(key,val);
	}
	
	public static Object get(String key)
	{
		if (tci!=null&&tci.get()!=null)
		{
			return( ((HashMap)tci.get()).get(key));			
		}
		else
		{
			return(null);
		}
	}
	
	public static HashMap getMap()
	{
		return((HashMap)tci.get());
	}
	
	public static void remove(String key)
	{
		((HashMap)tci.get()).remove(key);
	}
	
	public static boolean containsKey(String key)
	{
		return (  ((HashMap)tci.get()).containsKey(key)  );
	}

}
