package com.cwc.app.util;

import org.apache.log4j.Logger;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

public class Neo4jLocationTreeBuilder {
	static Logger log = Logger.getLogger("JAVAPS");
	
	private JSONArray data;
	private JSONObject slcs = new JSONObject();
	
	public Neo4jLocationTreeBuilder(JSONArray data) throws Exception {
			this.data = (data == null) ? new JSONArray() : data;
	}
	
	public void build() throws Exception {
		for(int i=0; i<data.length(); i++){
			JSONArray row = data.getJSONArray(i);
			JSONObject slc = row.getJSONObject(0).getJSONObject("data");
			Long slc_id = slc.getLong("slc_id");
			if(slcs.has(slc_id.toString())) slc = slcs.getJSONObject(slc_id.toString());
			else {
				slc.put("containers", new JSONArray());
				slcs.put(slc_id.toString(), slc);
			}
			JSONObject rootCon = row.getJSONObject(1).getJSONObject("data");
			JSONArray cdata = row.getJSONArray(2);
			cdata = new JSONArray().put(new JSONArray().put(cdata));
			//log.info(cdata.toString(4));
			Neo4jContainerTreeBuilder builder = new Neo4jContainerTreeBuilder(cdata,rootCon.getLong("con_id"));
			builder.build();
			slc.getJSONArray("containers").put(builder.getTree());
			
		}
	}
	
	public JSONArray getTree() throws Exception {
		JSONArray names = slcs.names();
		if(names == null) return new JSONArray();
		return slcs.toJSONArray(names);
	}
	
}
