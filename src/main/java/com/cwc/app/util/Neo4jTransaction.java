package com.cwc.app.util;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;
import us.monoid.web.Content;
import us.monoid.web.JSONResource;
import us.monoid.web.Resty;

public class Neo4jTransaction {
	private String restBaseUrl = null;
	private Resty client = null;
	private String txAction = null;
	private String commitAction = "/db/data/transaction/commit";
	
	public Neo4jTransaction(String restBaseUrl, Resty client){
		this.restBaseUrl = restBaseUrl;
		this.client = client;
	}
	

	public JSONObject commit(JSONArray statements) throws Exception{
		JSONResource response = client.json(
				restBaseUrl + commitAction,
				new Content("application/json", new JSONObject()
						.put("statements", statements).toString()
						.getBytes("UTF-8")));
		
		JSONObject result = response.object();
		if (result.getJSONArray("errors").length() > 0)
			throw new Exception(response.object().getJSONArray("errors")
					.toString());
		//restore commitAction
		commitAction = "/db/data/transaction/commit";
		txAction = null;
		return result;
	}
	
	public void begin(JSONArray statements) throws Exception{
		JSONResource responseWrapper = client.json(
				restBaseUrl + "/db/data/transaction",
				new Content("application/json", new JSONObject()
						.put("statements", statements).toString()
						.getBytes("UTF-8")));
		JSONObject response = responseWrapper.object();
		if (response.getJSONArray("errors").length() > 0)
			throw new Exception(response.getJSONArray("errors")
					.toString());
		commitAction = response.getString("commit");
		int i1 = commitAction.indexOf("/db/");
		int i2 = commitAction.lastIndexOf("/commit");
		txAction = commitAction.substring(i1, i2);
		commitAction = commitAction.substring(i1);
	}
	
	public void tx(JSONArray statements) throws Exception {
		if(txAction == null){
			this.begin(statements);
			return;
		}
		JSONResource responseWrapper = client.json(
				restBaseUrl + txAction,
				new Content("application/json", new JSONObject()
						.put("statements", statements).toString()
						.getBytes("UTF-8")));
		JSONObject response = responseWrapper.object();
		if (response.getJSONArray("errors").length() > 0)
			throw new Exception(response.getJSONArray("errors")
					.toString());
	}
	
	public void rollback() throws Exception {
		if(txAction == null){
			return;
		}
		JSONResource responseWrapper = client.json(restBaseUrl + txAction, Resty.delete());
		JSONObject response = responseWrapper.object();
		if (response.getJSONArray("errors").length() > 0)
			throw new Exception(response.getJSONArray("errors")
					.toString());
		//restore commitAction
		commitAction = "/db/data/transaction/commit";
		txAction = null;
	}
}
