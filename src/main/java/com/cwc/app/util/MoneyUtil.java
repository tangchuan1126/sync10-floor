package com.cwc.app.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import com.cwc.app.key.LengthUOMKey;
import com.cwc.app.key.WeightUOMKey;

/**
 * 货币精确计算操作包
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class MoneyUtil
{
	 //默认除法运算精度
	 private static final int DEF_DIV_SCALE = 2;

	 /** 
	  * 提供精确的加法运算。
	  * @param v1 被加数
	 
	  * @param v2 加数
	 
	  * @return 两个参数的和
	 
	  */


	 public static double add(double v1, double v2) 
	 {
	 	BigDecimal b1 = new BigDecimal(Double.toString(v1));
	 	BigDecimal b2 = new BigDecimal(Double.toString(v2));
	 	return b1.add(b2).doubleValue();
	 }
	 
	 public static double add(String v1,String v2) 
	 {
	 	BigDecimal b1 = new BigDecimal(v1);
	 	BigDecimal b2 = new BigDecimal(v2);
	 	return b1.add(b2).doubleValue();
	 }

	 
	 /**
	  * 提供精确的减法运算。
	  * @param v1 被减数
	  * @param v2 减数
	  * @return 两个参数的差
	  */
	 public static double sub(double v1, double v2)
	 {
	 	BigDecimal b1 = new BigDecimal(Double.toString(v1));
	 	BigDecimal b2 = new BigDecimal(Double.toString(v2));
	 	return b1.subtract(b2).doubleValue();
	 }
	 
	 public static double sub(String v1, String v2)
	 {
	 	BigDecimal b1 = new BigDecimal(v1);
	 	BigDecimal b2 = new BigDecimal(v2);
	 	return b1.subtract(b2).doubleValue();
	 }


	 /**
	 
	  * 提供精确的乘法运算。
	 
	  * @param v1 被乘数
	 
	  * @param v2 乘数
	 
	  * @return 两个参数的积
	 
	  */
	 public static double mul(double v1, double v2) 
	 {
	 	BigDecimal b1 = new BigDecimal(Double.toString(v1));
	 	BigDecimal b2 = new BigDecimal(Double.toString(v2));
	 	return b1.multiply(b2).doubleValue();
	 }

	 public static double mul(String v1, String v2) 
	 {
	 	BigDecimal b1 = new BigDecimal(v1);
	 	BigDecimal b2 = new BigDecimal(v2);
	 	return b1.multiply(b2).doubleValue();
	 }

	 /**
	 
	  * 提供（相对）精确的除法运算，当发生除不尽的情况时，精确到
	 
	  * 小数点以后10位，以后的数字四舍五入。
	 
	  * @param v1 被除数
	 
	  * @param v2 除数
	 
	  * @return 两个参数的商
	 
	  */
	 public static double div(double v1, double v2) 
	 {
	 	return div(v1, v2, DEF_DIV_SCALE);
	 }
	 
	 public static double div(String v1, String v2) 
	 {
	 	return div(Double.parseDouble(v1), Double.parseDouble(v2), DEF_DIV_SCALE);
	 }


	 /**
	 
	  * 提供（相对）精确的除法运算。当发生除不尽的情况时，由scale参数指
	 
	  * 定精度，以后的数字四舍五入。
	 
	  * @param v1 被除数
	 
	  * @param v2 除数
	 
	  * @param scale 表示表示需要精确到小数点以后几位。
	 
	  * @return 两个参数的商
	 
	  */
	 public static double div(double v1, double v2, int scale)
	 {
	 	if (scale < 0) 
	 	{
	 		throw new IllegalArgumentException("The scale must be a positive integer or zero");
	 	}

	 	BigDecimal b1 = new BigDecimal(Double.toString(v1));
	 	BigDecimal b2 = new BigDecimal(Double.toString(v2));
	 	return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	 }
	 
	 public static double div(String v1, String v2, int scale)
	 {
	 	if (scale < 0) 
	 	{
	 		throw new IllegalArgumentException("The scale must be a positive integer or zero");
	 	}

	 	BigDecimal b1 = new BigDecimal(v1);
	 	BigDecimal b2 = new BigDecimal(v2);
	 	return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	 }
	 
	 public static double round(double v, int scale) 
	 {
	 	if (scale < 0)
	 	{
	 		throw new IllegalArgumentException("The scale must be a positive integer or zero");
	 	}

	 	BigDecimal b = new BigDecimal(Double.toString(v));
	 	BigDecimal one = new BigDecimal("1");
	 	return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	 }
	 
	 public static float round(float v,int scale)
	 {
		 if (scale < 0)
		 	{
		 		throw new IllegalArgumentException("The scale must be a positive integer or zero");
		 	}

		 	BigDecimal b = new BigDecimal(Float.toString(v));
		 	BigDecimal one = new BigDecimal("1");
		 	return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).floatValue();
	 }
	 
	 public static double roundUP(double v,int scale)
	 {
		 	if (scale < 0)
		 	{
		 		throw new IllegalArgumentException("The scale must be a positive integer or zero");
		 	}

		 	BigDecimal b = new BigDecimal(Double.toString(v));
		 	BigDecimal one = new BigDecimal("1");
		 	return b.divide(one, scale, BigDecimal.ROUND_UP).floatValue();
	 }
	 
	 public static double roundUP(String v,int scale)
	 {
		 	if (scale < 0)
		 	{
		 		throw new IllegalArgumentException("The scale must be a positive integer or zero");
		 	}

		 	BigDecimal b = new BigDecimal(v);
		 	BigDecimal one = new BigDecimal("1");
		 	return b.divide(one, scale, BigDecimal.ROUND_UP).floatValue();
	 }
	 
	 public static double round(String v, int scale) 
	 {
	 	if (scale < 0)
	 	{
	 		throw new IllegalArgumentException("The scale must be a positive integer or zero");
	 	}

	 	BigDecimal b = new BigDecimal(v);
	 	BigDecimal one = new BigDecimal("1");
	 	return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	 }
	 
	 //得到接近的较大值
	 public static int formatDoubleUpInt(double d)
	 {
		 BigDecimal bg = new BigDecimal(d).setScale(0, RoundingMode.UP);
	     return (int)bg.doubleValue();
	 }
	 
	 //保留两位小数
	 public static String formatDoubleDecimal(double d)
	 {
		 DecimalFormat df = new DecimalFormat("###0.00");
		 return df.format(d);
	 }
	 
	 //不保留小数
	 public static String formatDoubleIntUp(double d)
	 {
		 DecimalFormat df = new DecimalFormat("###0");
		 return df.format(d);
	 }
	 
	 //a 需要格式化的值，length：位数
	 public static String fillZeroByRequire(long a, int length)
	 {
		 String format = "";
		 for (int i = 0; i < length; i++)
		 {
			 format += "0";
		 }
		 DecimalFormat df = new DecimalFormat(format);
		 return df.format(a);
	 }
	 
	 /**暂时只适用于LBS和Kg
	  * 1千克=2.2046226218488磅
		1磅=0.45359237千克
	  */
	 public static float weightUnitConverter(float weight, int fromUom, int toUom)
	 {
		 float result = 0F;
		 switch (fromUom)
		 {
			case WeightUOMKey.LBS:
				result = widthLbsConverterOthers(weight, toUom);
				break;
			case WeightUOMKey.KG:
				result = widthKgConverterOthers(weight, toUom);
				break;
			default:
				break;
		 }
		 return result;
	 }
	 
	 public static float widthLbsConverterOthers(float weight, int toUom)
	 {
		 float result = 0F;
		 switch (toUom) {
			case WeightUOMKey.LBS:
				result = weight;
				break;
			case WeightUOMKey.KG:
				result = 0.4536F * weight;
				break;
			default:
				break;
		}
		return result;
	 }
	 
	 
	 public static float widthKgConverterOthers(float weight, int toUom)
	 {
		 float result = 0F;
		 switch (toUom) {
		 	case WeightUOMKey.KG:
				result = weight;
				break;
			case WeightUOMKey.LBS:
				result = 2.2046F * weight;
				break;
			default:
				break;
		}
		return result;
	 }
	 
	 
	 public static float lengthUnitConverter(float length, int fromUom, int toUom)
	 {
		 float result = 0F;
		 switch (fromUom)
		 {
		 	case LengthUOMKey.DM:
		 		result = lengthDmConverterOthers(length, toUom);
		 		break;
		 	case LengthUOMKey.CM:
		 		result = lengthCmConverterOthers(length, toUom);
		 		break;
		 	case LengthUOMKey.MM:
		 		result = lengthMmConverterOthers(length, toUom);
		 		break;
		 	case LengthUOMKey.INCH:
		 		result = lengthInchConverterOthers(length, toUom);
		 		break;
		 	default:
		 		break;
		}
		return result;
	 }
	
	 public static float lengthDmConverterOthers(float length, int toUom)
	 {
		 float result = 0F;
		 switch (toUom) {
		 	case LengthUOMKey.DM:
				result = length;
				break;
			case LengthUOMKey.CM:
				result = length * 10;
				break;
			case LengthUOMKey.MM:
				result = length * 100;
				break;
			case LengthUOMKey.INCH:
				result = length * 10 / 2.54F;
				break;
			default:
				break;
		}
		return result;
	 }
	 
	 public static float lengthCmConverterOthers(float length, int toUom)
	 {
		 float result = 0F;
		 switch (toUom) {
		 	case LengthUOMKey.CM:
				result = length;
				break;
			case LengthUOMKey.DM:
				result = length / 10;
				break;
			case LengthUOMKey.MM:
				result = length * 10;
				break;
			case LengthUOMKey.INCH:
				result = length / 2.54F;
				break;
			default:
				break;
		}
		return result;
	 }
	 
	 public static float lengthMmConverterOthers(float length, int toUom)
	 {
		 float result = 0F;
		 switch (toUom) {
		 	case LengthUOMKey.MM:
				result = length;
				break;
			case LengthUOMKey.DM:
				result = length / 100;
				break;
			case LengthUOMKey.CM:
				result = length / 10;
				break;
			case LengthUOMKey.INCH:
				result = length / 10 / 2.54F;
				break;
			default:
				break;
		}
		return result;
	 }
	 
	 public static float lengthInchConverterOthers(float length, int toUom)
	 {
		 float result = 0F;
		 switch (toUom) {
		 	case LengthUOMKey.INCH:
				result = length;
				break;
			case LengthUOMKey.DM:
				result = length / 10 * 2.54F;
				break;
			case LengthUOMKey.CM:
				result = length * 2.54F;
				break;
			case LengthUOMKey.MM:
				result = length * 10 * 2.54F;
				break;
			default:
				break;
		}
		return result;
	 }
	 
	 
	 public static void main(String args[])
	 {
	 	//System.out.println(MoneyUtil.roundUP("0.000003",0));
	 }
}


