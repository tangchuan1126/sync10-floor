package com.cwc.app.util;

import java.util.Map;

import com.cwc.app.util.DBRowUtils;

import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

public class RelationBean {
	private Map<String,Object> fromNodeProps;
	private Map<String,Object> toNodeProps;
	private Map<String,Object> relProps;
	
	public RelationBean() {
	}
	
	public RelationBean(Map<String,Object> fromNodeProps, Map<String,Object> toNodeProps, Map<String,Object> relProps){
		this.fromNodeProps = fromNodeProps;
		this.toNodeProps = toNodeProps;
		this.relProps = relProps;
	}
	
	public RelationBean(JSONObject fromNodeProps, JSONObject toNodeProps, JSONObject relProps) throws Exception{
		this.fromNodeProps = DBRowUtils.jsonObjectAsMap(fromNodeProps);
		this.toNodeProps = DBRowUtils.jsonObjectAsMap(toNodeProps);
		this.relProps = DBRowUtils.jsonObjectAsMap(relProps);
	}
	
	public Map<String, Object> getFromNodeProps() {
		return fromNodeProps;
	}
	public void setFromNodeProps(Map<String, Object> fromNodeProps) {
		this.fromNodeProps = fromNodeProps;
	}
	public Map<String, Object> getToNodeProps() {
		return toNodeProps;
	}
	public void setToNodeProps(Map<String, Object> toNodeProps) {
		this.toNodeProps = toNodeProps;
	}
	public Map<String, Object> getRelProps() {
		return relProps;
	}
	public void setRelProps(Map<String, Object> relProps) {
		this.relProps = relProps;
	}
	
	public String toString(){
		try {
			return new JSONObject()
				.put("fromNodeProps", this.fromNodeProps)
				.put("toNodeProps", this.toNodeProps)
				.put("relProps", this.relProps)
				.toString(4);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
