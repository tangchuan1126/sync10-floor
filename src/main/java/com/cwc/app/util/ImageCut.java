package com.cwc.app.util;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageCut {

	/**
	 * 图像剪辑
	 * @param 输出路径,输入路径,x,y,w,h
	 * @return void
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public void cutImage(String outImagePath,String imagePath,int x,int y,int w,int h){
		
		try {
			BufferedImage bufferedImage = ImageIO.read(new File(imagePath));
			
			int srcWidth = bufferedImage.getWidth();
			int srcHeight = bufferedImage.getHeight();
			
			if(srcWidth>=w&&srcHeight>=h){
				
				Image image = bufferedImage.getScaledInstance(srcWidth, srcHeight, Image.SCALE_DEFAULT);
	    
				ImageFilter cropFilter=new CropImageFilter(x,y,w,h);
			    
				Image img = Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(image.getSource(),cropFilter));
				
			    BufferedImage tag = new BufferedImage(w,h,BufferedImage.TYPE_INT_BGR);
			    
			    Graphics g = tag.getGraphics();
			    g.drawImage(img, 0, 0, null);
			    g.dispose();
	    
			    ImageIO.write(tag,"JPEG",new File(outImagePath));
			}
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
}
