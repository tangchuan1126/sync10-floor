package com.cwc.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.quartz.CronExpression;

import com.cwc.db.DBRow;

/**
 * 
 * @author Administrator
 * 利用Corn计算在一个时间段的执行任务的时间点
 */
public class ScheduleTime {
	private static final String dateExp = "yyyy-MM-dd HH:mm:ss";
	private static final SimpleDateFormat format = new SimpleDateFormat(dateExp);
	 
	
	//计算时间点 如果是抛异常了就放回Null
	//这个实际上是进行日期的计算。就是计算出任务的执行的日期。
	//然后在把任务的具体的执行时间加上拼成新的任务的执行时间和结束时间
	
	public List<String> getScheduleTime(DBRow row,DBRow scheduleRow,DBRow comeFromRequest)
		throws Exception
	{
		 
		try {
			CronExpression cronExp = new CronExpression(row.getString("repeat_express"));
			Date startDate = format.parse(row.getString("repeat_start_time")); 
			String[] startArray = scheduleRow.getString("start_time").split(" ");
			String[] endArray = scheduleRow.getString("end_time").split(" ");
			String fixedStartTime =  startArray[1];
			String fixedEndTime = endArray[1];
			int length = DateUtil.getSubDay(scheduleRow.getString("end_time"),scheduleRow.getString("start_time")); 
			long sum = length * 24 * 60 * 60 *1000;
			// 判断应该是用那种计算任务的执行时间和结束的时间
			String repeat_end_time = row.getString("repeat_end_time") ;
			if(null != repeat_end_time && repeat_end_time.length() > 0){
				Date endDate = format.parse(row.getString("repeat_end_time"));
				return hasEndTime(cronExp,startDate,endDate,fixedStartTime,fixedEndTime, sum);
			}else{
				String count = row.getString("repeat_times");
				if(count != null && count.length() > 0){
					int times = Integer.parseInt(count);
					return hasCountTimes(times,cronExp,startDate,fixedStartTime,fixedEndTime ,  sum );
				}else{
					if(null != comeFromRequest.getString("end") && comeFromRequest.getString("end").length() > 0){
						Date lastDate = format.parse(comeFromRequest.getString("end"));
						return hasNerverStopFlag(lastDate,cronExp,startDate,fixedStartTime,fixedEndTime ,  sum);
					}
					return null;
				}	
			}
			 
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}   
		 
	}
	
	public List<String> hasEndTime(CronExpression cronExp,Date startDate ,Date endDate,String fixedStartTime ,String fixedEndTime,long sum){
		List<String> scheduleTimeList = new ArrayList<String>();
		while(true){
			Date triggerDate = cronExp.getNextValidTimeAfter(startDate);
			if(triggerDate==null){   
				break;
			}   
			if(triggerDate.after(endDate)){   
			    break;   
			}  
			startDate = triggerDate;
			Date other = new Date(triggerDate.getTime() + sum);
			String date = format.format(triggerDate).split(" ")[0]+" "; 
			String end = format.format(other).split(" ")[0]+" ";
			scheduleTimeList.add(date + fixedStartTime + ","+end+fixedEndTime);		
			
		}	//
		return scheduleTimeList;
	}
	public List<String> hasCountTimes(int count,CronExpression cronExp,Date startDate,String fixedStartTime ,String fixedEndTime,long sum){
		List<String> scheduleTimeList = new ArrayList<String>();
		while(true){
			Date triggerDate = cronExp.getNextValidTimeAfter(startDate);
			if(triggerDate==null){   
				break;
			}   
			if(count == 0){
				break;
			}
			startDate = triggerDate;
			Date other = new Date(triggerDate.getTime() + sum);
			String date = format.format(triggerDate).split(" ")[0]+" "; 
			String end = format.format(other).split(" ")[0]+" ";
			scheduleTimeList.add(date + fixedStartTime + ","+end+fixedEndTime);		 
			count-- ;
		}
	 
		return scheduleTimeList;
	 
	}
	public List<String> hasNerverStopFlag(Date lastDate,CronExpression cronExp,Date startDate,String fixedStartTime ,String fixedEndTime,long sum){
		List<String> scheduleTimeList = new ArrayList<String>();
		while(true){
			Date triggerDate = cronExp.getNextValidTimeAfter(startDate);
			//System.out.println("triggerDate : " + format.format(triggerDate) + "\n  startDate : " + format.format(startDate) );
			if(triggerDate==null){   
				break;
			}
			if(triggerDate.after(lastDate)){   
			    break;   
			} 
			startDate = triggerDate;
			Date other = new Date(triggerDate.getTime() + sum);
			String date = format.format(triggerDate).split(" ")[0]+" "; 
			String end = format.format(other).split(" ")[0]+" ";
			scheduleTimeList.add(date + fixedStartTime + ","+end+fixedEndTime);		
			
		}
		return scheduleTimeList;
	}
	 

}
