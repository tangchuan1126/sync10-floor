package com.cwc.app.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.Resource;

public class ExPropertyPlaceholderConfigurer extends
		PropertyPlaceholderConfigurer {
	@Override
	public void setLocations(Resource[] locations){
		List<Resource> locExists = new ArrayList<Resource>();
		for(Resource loc:locations){
			if(loc.exists()) locExists.add(loc);
		}
		super.setLocations(locExists.toArray(new Resource[locExists.size()]));
	}
}
