package com.cwc.app.util;

import java.util.Locale;

import org.apache.log4j.Logger;

/**
 * 环境变量
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class Environment 
{
	static Logger log = Logger.getLogger("PLATFORM");
	
	private static String home = null;
	private static int DATABASE_TYPE=1;			//数据库类型
			
	public static int DATABASE_MYSQL = 1;			
	public static int DATABASE_SQLSERVER = 2;		
				
	private static String CURRENT_ACTION = null;	//当前线程执行的事件(例如：com.cwc.app.api.addPro)该变量只在通过代理执行的事件才有值，因为是通过AOP设置
	
	private static Locale locale = Locale.CHINA;				//默认语言地区
	
	
	
	/**
	 * 获得系统安装绝对路径
	 * @return
	 */
	public static String getHome() 
	{
		return(home);
	}

	/**
	 * 设置系统安装绝对路径
	 * @param home
	 */
	public static void setHome(String home)
	{
        
		Environment.home = home;
	}
	
	/**
	 * 
	 * 销毁路径
	 */
	public static void desctroy()
	{
		Environment.home = null;
		log.info("Clean home!");
	}
	
	/**
	 * 设置数据库类型
	 * @param i
	 */
	public static void setDatabaseType(int i)
	{
		Environment.DATABASE_TYPE = i;
	}
	
	/**
	 * 获得数据库类型
	 * @return
	 */
	public static int getDatabaseType() 
	{
		return(Environment.DATABASE_TYPE);
	}

	/**
	 * 设置当前执行事件(通过AOP设置)
	 * @param action
	 */
	public static void setCurrentAction(String action)
	{
		CURRENT_ACTION = action;
	}
	
	/**
	 * 获得当前执行事件
	 * @return
	 */
	public static String getCurrentAction()
	{
		return(CURRENT_ACTION);
	}

	/**
	 * 获得语言地区
	 * @return
	 */
	public static Locale getLocale()
	{
		return locale;
	}

	/**
	 * 设置语言地区
	 * @param locale
	 */
	public static void setLocale(Locale locale)
	{
		Environment.locale = locale;
	}


	
	
}


