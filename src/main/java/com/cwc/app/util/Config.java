package com.cwc.app.util;

public class Config
{
	public final static String confFileFolder = "WEB-INF/conf";
	public final static String loginlicenceSesion = "loginlicenceSesion";
	public final static String adminSesion = "adminSesion";
	public final static String memberSesion = "memberSesion";
	public final static String carSesion = "cartSession";
	public final static String customUnionCartSesion = "customUnionCartSession";
	public final static String finalCustomUnionCartSesion = "finalCustomUnionCartSesion";
	
	public final static String cartQuoteSession = "cartQuoteSession";
	public final static String customUnionCartQuoteSession = "customUnionCartQuoteSession";
	public final static String finalCustomUnionCartQuoteSesion = "finalCustomUnionCartQuoteSesion";
	
	public final static String cartReturnSession = "cartReturnSession";
	
	public final static String cartWarrantySession = "cartWarrantySession";
	public final static String customUnionCartWarrantySession = "customUnionCartWarrantySession";
	public final static String finalCustomUnionCartWarrantySesion = "finalCustomUnionCartWarrantySesion";
	
	public final static String wayBillSession = "wayBillCartSession";
	public final static String cartReturnProductReason = "cartReturnProductReason";
	
	public final static String transportSession = "transportSession";
	
	public final static String waybillServiceOrder = "waybillServiceOrder" ;
	public final static String returnProductItem = "returnProductItem" ;
	
	public final static String cartWaybillB2B = "cartWaybillB2B";
}





