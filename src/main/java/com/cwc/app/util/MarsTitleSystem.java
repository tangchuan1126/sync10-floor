package com.cwc.app.util;

import java.io.*;
import java.math.BigDecimal;



/**
 * 目前实现采用10分度可以精确到3-5米
 * 
 * @author Administrator
 * 
 */
public class MarsTitleSystem {
	private PointD[] p;
	private PointD[] pd;//
	private int fendu;// 分度，目前有10,100
	private float[] stat;
	private int minx, miny, maxx, maxy;
	private static MarsTitleSystem google = null;
	private static MarsTitleSystem baidu = null;
	private MarsTitleSystem(String path) {
		DataInputStream in = null;
		java.net.URL url = null;
		try {
			url = MarsTitleSystem.class.getResource(path);
			InputStream base = new BufferedInputStream(url.openStream());
			in = new DataInputStream(base);

			fendu = in.readInt();
			long count = in.readLong();
			minx = in.readInt();
			maxx = in.readInt();
			miny = in.readInt();
			maxy = in.readInt();
			in.skip(100);// 跳过文件头
			p = new PointD[(int) count];
			pd = new PointD[(int) count];
			stat = new float[fendu];
			for (int i = 0; i < count; i++) {
				//System.out.println(i+","+count);
				float lng = in.readFloat();
				float lat = in.readFloat();
				double dx = in.readDouble() - lng;
				double dy = in.readDouble() - lat;
				if (i < fendu)
					stat[i] = new BigDecimal(Float.toString(lng)).subtract(
							new BigDecimal(Integer.toString((int) lng)))
							.floatValue();// 不丢失精度的减法
				p[i] = new PointD(
						new BigDecimal(Float.toString(lng)).doubleValue(),
						new BigDecimal(Float.toString(lat)).doubleValue());
				pd[i] = new PointD(dx, dy);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("无法打开数据文件" + url);
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

	public static MarsTitleSystem getGoogle(String key) {
		// synchronized(instance){
		if (google == null)
			google = new MarsTitleSystem("/Offset10.bin");
		return google;
		// }
	}

	public static MarsTitleSystem getBaidu(String key) {
		// synchronized(instance){
		if (baidu == null)
			baidu = new MarsTitleSystem("/Offsetbd.bin");
		return baidu;
		// }
	}
	private static double dotFix(double d, int num) {
		double aa = Math.pow(10, num);
		return (double) (Math.round(d * aa) / aa);
	}

	private static float dotFix(float d, int num) {
		double aa = Math.pow(10, num);
		return (float) (Math.round(d * aa) / aa);
	}

	private int binarySearch(double value) {
		Double v = new BigDecimal(Double.toString(value)).subtract(
				new BigDecimal(Double.toString((int) value))).doubleValue();// 不丢失精度的减法
		int startPos = 0; // 查找范围的起始下标
		int endPos = stat.length - 1; // 查找范围的结束下标
		int m = (startPos + endPos) / 2; // m为起始与结束下标中间的下标
		while (startPos <= endPos) {
			if (v == stat[m]) {
			//	System.out.print("num在数组中的下标为：");
				return m;
			} else {
				if (v > stat[m]) {
					startPos = m + 1;
					// 如果要查找的数比中间这个数大，那么下次查找的起始位置移至m之后
				} else {
					endPos = m - 1;
					// 如果要查找的数比中间这个数小，下次查找的结束位置移至m之前
				}
			}
			m = (startPos + endPos) / 2;
		}
		return endPos;// 返回最接近的坐标位置
	}

	/**
	 * 将GPS的经纬度坐标转换为中国地区的偏移坐标
	 * 
	 * @param lng
	 * @param lat
	 * @return
	 */
	public PointD latLongToMars(double lng, double lat) {
		 if (lng < minx || lng + 1 > maxx + stat[fendu - 1] || lat < miny || lat + 1 > maxy + stat[fendu - 1])
            return new PointD(lng, lat);
		int idx = this.binarySearch(lng);
		int idy = this.binarySearch(lat);
		int s = fendu * (maxx - minx);// 经度一个统计单位的偏移量
		int basePos = (((int) lat - miny) * s + ((int) lng - minx)) * fendu;
		int i1 = idy * s + idx;
		int i2 = idy * s + idx + 1;
		int i3 = (idy + 1) * s + idx;
		int i4 = (idy + 1) * s + idx + 1;
		PointD p1 = p[basePos + i1];
		PointD p2 = p[basePos + i2];
		PointD p3 = p[basePos + i3];
		PointD p4 = p[basePos + i4];

		PointD d1 = pd[basePos + i1];
		PointD d2 = pd[basePos + i2];
		PointD d3 = pd[basePos + i3];
		PointD d4 = pd[basePos + i4];
//		System.out.println(p1 + "," + d1);
//		System.out.println(p2 + "," + d2);
//		System.out.println(p3 + "," + d3);
//		System.out.println(p4 + "," + d4);

		double u = (lng - p1.getX())
				/ (idx == stat.length - 1 ? 1-stat[idx] : stat[idx + 1] - stat[idx]);
		double v = (lat - p1.getY())
				/ (idy == stat.length - 1 ? 1-stat[idy] : stat[idy + 1] - stat[idy]);
		// 二次非线性内插值
		double x = (1 - u) * (1 - v) * d1.x + (1 - u) * v * d3.x + u * (1 - v)
				* d2.x + u * v * d4.x;
		double y = (1 - u) * (1 - v) * d1.y + (1 - u) * v * d3.y + u * (1 - v)
				* d2.y + u * v * d4.y;
		// System.out.println("x:" + x + " y:" + y + " u:" + u + " v:" + v);
		return new PointD(lng + x, lat + y);
	}

	/**
	 * 将GPS的经纬度坐标转换为中国地区的偏移坐标
	 * 
	 * @param lng
	 * @param lat
	 * @return
	 */
	public PointD marsToLatLong(double lng, double lat) {
		 if (lng < minx || lng + 1 > maxx + stat[fendu - 1] || lat < miny || lat + 1 > maxy + stat[fendu - 1])
            return new PointD(lng, lat);
		int idx = this.binarySearch(lng);
		int idy = this.binarySearch(lat);
		int s = fendu * (maxx - minx);// 经度一个统计单位的偏移量
		int basePos = (((int) lat - miny) * s + ((int) lng - minx)) * fendu;
		int i1 = idy * s + idx;
		int i2 = idy * s + idx + 1;
		int i3 = (idy + 1) * s + idx;
		int i4 = (idy + 1) * s + idx + 1;
		PointD p1 = p[basePos + i1];
		PointD p2 = p[basePos + i2];
		PointD p3 = p[basePos + i3];
		PointD p4 = p[basePos + i4];

		PointD d1 = pd[basePos + i1];
		PointD d2 = pd[basePos + i2];
		PointD d3 = pd[basePos + i3];
		PointD d4 = pd[basePos + i4];
//		 System.out.println(p1 + "," + d1);
//		 System.out.println(p2 + "," + d2);
//		 System.out.println(p3 + "," + d3);
//		 System.out.println(p4 + "," + d4);

		double u = (lng - p1.getX()) / (idx == stat.length - 1 ? 1-stat[idx] : stat[idx + 1] - stat[idx]);
		double v = (lat - p1.getY()) /(idy == stat.length - 1 ? 1-stat[idy] : stat[idy + 1] - stat[idy]);
		// 二次非线性内插值
		double x = (1 - u) * (1 - v) * d1.x + (1 - u) * v * d3.x + u * (1 - v)
				* d2.x + u * v * d4.x;
		double y = (1 - u) * (1 - v) * d1.y + (1 - u) * v * d3.y + u * (1 - v)
				* d2.y + u * v * d4.y;
		// System.out.println("x:" + x + " y:" + y + " u:" + u + " v:" + v);
		return new PointD(lng - x, lat - y);
	}

	public static void main(String[] args) {
	 
		
		
		PointD aa = MarsTitleSystem.getBaidu(null).latLongToMars(121.558449,31.227341); 
		//System.out.println("gps to baidu 坐标" + aa.x + "," + aa.y );


		PointD bb = MarsTitleSystem.getGoogle(null).latLongToMars(114.032458,22.399024) ;

		//System.out.println("gps to google 坐标" + bb.x + "," + bb.y );
		
		//System.out.println("google :113.214111328125,23.12520549860231");
		  bb = MarsTitleSystem.getGoogle(null).marsToLatLong(113.214111328125,23.12520549860231) ;
         // System.out.println("gps :"+bb.x+"  "+bb.y);
          
          bb = MarsTitleSystem.getGoogle(null).latLongToMars(bb.x,bb.y) ;
         // System.out.println("gps to google :"+bb.x+"  "+bb.y);
          
		 
	}
}
