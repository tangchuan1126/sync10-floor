package com.cwc.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 日期操作工具包
 * 
 * @author TurboShop
 *
 *         TurboShop.cn all rights reserved.
 */
public class DateUtil implements Handler {
	public static final String defaultDateFormat = "yyyy-MM-dd";
	public static final String defaultDateTimeFormat = "yyyy-MM-dd HH:mm:ss";
	public static final String defaultDateTimeFormatDHL = "yyyy-MM-ddTHH:mm:ss";
	public static final String defaultDate24Formate = "MM/dd/yy HH:mm";
	
	private static final SimpleDateFormat defaultFormage = new SimpleDateFormat(defaultDateTimeFormat);
	private static SimpleDateFormat sdf = new SimpleDateFormat();
	//private static Calendar calendar = Calendar.getInstance(); //放到方法内实例化，避免多处同时使用时时间错乱
	//private static Map<Long, Long> stroageJetLeg;		//此方法无法解决夏令时的问题，改用stroageTimeZoneId
	private static Map<Long, String> stroageTimeZoneId;

	
	public static final String showLocalTime(String FormatStr, long ps_id) throws Exception {
		// try{
		Date date = createDateTime(FormatStr);
		return showLocationTime(date, ps_id);
		/*
		 * }catch(Exception e){
		 * 
		 * } return FormatStr ;
		 */
		
	}
	
	public static final String showLocationTime(Date date, long ps_id) throws Exception {
		return showLocationTime(date,ps_id,defaultDateTimeFormat);
	}
	
	public static final String showLocationTime(Date date, long ps_id,String format) throws Exception {
		//Calendar calendar = Calendar.getInstance();
		
		SimpleDateFormat sdf = new SimpleDateFormat(defaultDateTimeFormat);
		sdf.setTimeZone(getTimeZone(ps_id));
		return sdf.format(date);
	}
	
	public static final String showLocationTimeFormat(Date date, long ps_id,String format) throws Exception {
		//Calendar calendar = Calendar.getInstance();
		
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setTimeZone(getTimeZone(ps_id));
		return sdf.format(date);
	}
	
	public static final Date locationZoneTime(String formatStr, long ps_id) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(defaultDateTimeFormat);
		
		return sdf.parse(showLocalTime(formatStr, ps_id));
	}
	
	public static final String locationZoneTimeString(String formatStr, long ps_id) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(defaultDateTimeFormat);
		
		String result;
		try {
			Date date = sdf.parse(showLocalTime(formatStr, ps_id));
			
			result = DateUtil.FormatDatetime(DateUtil.defaultDateTimeFormat, date);
		} catch (Exception e) {
			result = "";
		}
		
		return result;
	}
	
	public static final String showUTCTime(String FormatStr, long ps_id) throws Exception {
		Date date = createDateTime(FormatStr);
		
		return showUTCTime(date, ps_id);
	}
	
	public static final String showUTCTime(Date date, long ps_id) throws Exception {
		//Calendar calendar = Calendar.getInstance();
		//calendar.setTime(date);
		
		//long time = calendar.getTimeInMillis();
		
		//long jetLeg = stroageJetLeg.get(ps_id);
		
		//long localShow = time - jetLeg;
		TimeZone tz = getTimeZone(ps_id);
		long localShow = date.getTime() - tz.getOffset(date.getTime());
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(localShow);
		
		SimpleDateFormat sdf = new SimpleDateFormat(defaultDateTimeFormat);
		
		return sdf.format(calendar.getTime());
	}
	
	public static final Date utcTime(String formatStr, long ps_id) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(defaultDateTimeFormat);
		
		return sdf.parse(showUTCTime(formatStr, ps_id));
	}
	
	public static final String getTimezoneId(long psId){
		return stroageTimeZoneId.get(psId);
	}
	
	public static final TimeZone getTimeZone(long psId){
		TimeZone tz = TimeZone.getDefault();
		String id = stroageTimeZoneId.get(psId);
		if(id != null){
			tz = TimeZone.getTimeZone(id);
		}
		return tz;
	}
	/**
	 * 获取仓库时区int数
	 * @param psId
	 * @return
	 */
	public static final int getStorageTimeZoneInt(long psId){
		TimeZone tz = getTimeZone(psId);
		int gmt = tz.getRawOffset()/1000/60/60;
		return gmt;
	}
	/**
	 * 获取仓库所在时区和UTC时间差毫秒数，不计算夏令时的影响
	 * @param psId
	 * @return
	 */
	public static final int getStorageRawOffset(long psId){
		TimeZone tz = getTimeZone(psId);
		return tz.getRawOffset();
	}
	
	/**
	 * 格式化日期
	 * 
	 * @param FormatStr 格式化格式
	 * @param date Date
	 * @return
	 */
	public static final String FormatDatetime(String FormatStr, Date date) {
		
		SimpleDateFormat bartDateFormat = new SimpleDateFormat(FormatStr);
		return bartDateFormat.format(date);
	}
	
	/**
	 * 格式化当前日期
	 * 
	 * @param FormatStr 格式化格式
	 * @return
	 */
	public static final String FormatDatetime(String FormatStr) {
		SimpleDateFormat bartDateFormat = new SimpleDateFormat(FormatStr);
		return bartDateFormat.format(new Date());
	}
	
	/**
	 * 获取当前月份的上一个月日期
	 * 
	 * @param FormatStr 格式化格式
	 * @return
	 */
	public static final String LastMonthDatetime(String FormatStr) {
		Date date = new Date();// 当前日期
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");// 默认为此格式
		if (FormatStr != null) {
			sdf = new SimpleDateFormat(FormatStr);// 格式化对象
		}
		Calendar calendar = Calendar.getInstance();// 日历对象
		calendar.setTime(date);// 设置当前日期
		calendar.add(Calendar.MONTH, -1);// 月份减一
		return sdf.format(calendar.getTime());// 输出格式化的日期
	}
	
	/**
	 * 把Date转换为字符串
	 * 
	 * @param date Date
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static final String DatetimetoStr(Date date) {
		sdf.applyPattern(defaultDateTimeFormat);
		return sdf.format(date);
	}
	
	/**
	 * 把字符串日期转换为Date
	 * 
	 * @param ADate yyyy-MM-dd
	 * @return Date
	 * @throws Exception
	 */
	public static final Date StrtoDate(String ADate) throws Exception {
		return StrtoDate(ADate, "-");
	}
	
	public static final String StrtoYYMMDD(String ADate) throws Exception {
		SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		SimpleDateFormat to = new SimpleDateFormat("yy-MM-dd");
		
		return to.format(from.parse(ADate));
	}
	
	public static final String StrtoYYMMDDHHMMSS(String ADate) throws Exception {
		SimpleDateFormat from = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		
		SimpleDateFormat to = new SimpleDateFormat(defaultDateTimeFormat);
		
		return to.format(from.parse(ADate));
	}
	
	/**
	 * 把字符串日期转换为Date
	 * 
	 * @param ADate yyyy-MM-dd
	 * @param ListSeparator 分隔符
	 * @return Date
	 * @throws Exception
	 */
	public static final Date StrtoDate(String ADate, String ListSeparator) throws Exception {
		if ((ADate == null) || (ADate.length() == 0)) {
			return new java.util.Date();
		} else {
			StringTokenizer st = new StringTokenizer(ADate, ListSeparator);
			if (st.countTokens() != 3) {
				throw new Exception("date format error!");
			} else {
				
				int Year = Integer.parseInt(st.nextToken());
				int Month = Integer.parseInt(st.nextToken());
				int Day = Integer.parseInt(st.nextToken());
				
				Calendar cale = Calendar.getInstance();
				cale.set(Calendar.YEAR, Year);
				cale.set(Calendar.MONTH, Month - 1);
				cale.set(Calendar.DATE, Day);
				return cale.getTime();
			}
		}
	}
	
	/**
	 * 把Date转换为字符串
	 * 
	 * @param date Date
	 * @return yyyy-MM-dd
	 */
	public static final String DatetoStr(Date date) {
		sdf = new SimpleDateFormat(defaultDateFormat);
		return sdf.format(date);
	}
	
	/**
	 * 获得当前日期
	 * 
	 * @return
	 */
	public static final Date Now() {
		return new Date();
	}
	
	/**
	 * 获得当前日期字符串
	 * 
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static final String NowStr() {
		return DatetimetoStr(new Date());
	}
	
	public static final String NowStrDHL() {
		String dhlTime = "";
		Date date = new Date();
		
		SimpleDateFormat bartDateFormat = new SimpleDateFormat(defaultDateFormat);
		SimpleDateFormat bartDateFormatTime = new SimpleDateFormat("HH:mm:ss");
		
		dhlTime = bartDateFormat.format(date) + "T" + bartDateFormatTime.format(date) + "-08:00";
		
		return dhlTime;
	}
	
	public static final String NowStrEPacket() {
		String dhlTime = "";
		Date date = new Date();
		
		SimpleDateFormat bartDateFormat = new SimpleDateFormat(defaultDateFormat);
		SimpleDateFormat bartDateFormatTime = new SimpleDateFormat("HH:mm:ss");
		
		dhlTime = bartDateFormat.format(date) + "T" + bartDateFormatTime.format(date);
		
		return dhlTime;
	}
	
	/**
	 * 英文格式当前日期
	 * 
	 * @return
	 * @throws ParseException
	 */
	public static final String NowStrGoble() throws ParseException {
		String date = DateUtil.DatetoStr(new Date());
		date = date.replaceAll("-", "");
		SimpleDateFormat sd3 = new SimpleDateFormat("yyyyMMdd");
		String dateGobleStr = sd3.parse(date).toString();
		
		return (dateGobleStr.replaceAll("00:00:00 ", ""));
	}
	
	public static final String gobleDate(String date) throws ParseException {
		if (date.length() > 10) {
			date = date.substring(0, 10);
		}
		date = date.replaceAll("-", "");
		SimpleDateFormat sd3 = new SimpleDateFormat("yyyyMMdd");
		String dateGobleStr = sd3.parse(date).toString();
		
		return (dateGobleStr.replaceAll("00:00:00 ", ""));
	}
	
	/**
	 * 获得当前日期字符串
	 * 
	 * @return yyyy-MM-dd
	 */
	public static final String DateStr() {
		return DatetoStr(new Date());
	}
	
	/**
	 * 获得当前日期long time
	 * 
	 * @return
	 */
	public static final String getTime() {
		return String.valueOf(new Date().getTime());
	}
	
	/**
	 * 获得当前秒数
	 * 
	 * @return
	 */
	public static final String getSecond() {
		return getTime().substring(0, 10);
	}
	
	/**
	 * 获得当前年份
	 * 
	 * @return
	 */
	public static final int getYear() {
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.YEAR);
	}
	
	public static final String getStrCurrYear() {
		return String.valueOf(getYear());
	}
	
	/**
	 * 获得当前string月份
	 * 
	 * @return
	 */
	public static final int getMonth() {
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.MONTH);
	}
	
	public static final String getStrCurrMonth() {
		return String.valueOf(getMonth());
	}
	
	/**
	 * 获得当前string天
	 * 
	 * @return
	 */
	public static final int getMonthDay() {
		Calendar calendar = Calendar.getInstance();
		return (calendar.get(Calendar.DAY_OF_MONTH));
	}
	
	public static final String getStrCurrDay() {
		return (String.valueOf(getMonthDay()));
	}
	
	/**
	 * 获得当前string小时
	 * 
	 * @return
	 */
	public static final int getHour() {
		Calendar calendar = Calendar.getInstance();
		return (calendar.get(Calendar.HOUR_OF_DAY));
	}
	
	public static final String getStrCurrHour() {
		return (String.valueOf(getHour()));
	}
	
	/**
	 * 获得当前string分钟
	 * 
	 * @return
	 */
	public static final int getMinute() {
		Calendar calendar = Calendar.getInstance();
		return (calendar.get(Calendar.MINUTE));
	}
	
	public static final String getStrCurrMinute() {
		return (String.valueOf(getMinute()));
	}
	
	/**
	 * 获得当前int年份
	 * 
	 * @return
	 */
	public static final int getIntCurrYear() {
		return (getYear());
	}
	
	/**
	 * 获得当前int月份
	 * 
	 * @return
	 */
	public static final int getIntCurrMonth() {
		return (getMonth());
	}
	
	/**
	 * 获得当前int天
	 * 
	 * @return
	 */
	public static final int getIntCurrDay() {
		return (getMonthDay());
	}
	
	/**
	 * 获得当前int小时
	 * 
	 * @return
	 */
	public static final int getIntCurrHour() {
		return (getHour());
	}
	
	/**
	 * 获得当前int分钟
	 * 
	 * @return
	 */
	public static final int getIntCurrMinute() {
		return (getMinute());
	}
	
	/**
	 * 获得当前季度
	 * 
	 * @return
	 */
	public static final int getSeason() {
		int month = getIntCurrMonth();
		
		if (1 <= month && month <= 3) {
			return (1);
		} else if (4 <= month && month <= 6) {
			return (2);
		} else if (7 <= month && month <= 9) {
			return (3);
		} else {
			return (4);
		}
	}
	
	public static long getDate2LongTime(String date) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long millionSeconds = sdf.parse(date).getTime();// 毫秒
		return (millionSeconds);
	}
	
	public static String parseDateTo12Hours(String date) throws Exception {
		SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat to = new SimpleDateFormat("MM/dd/yyyy KK:mm a");
		return to.format(from.parse(date));
	}
	
	/**
	 * 转变成Local的时间 6+4 24H
	 * 
	 * @param FormatStr
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年11月12日
	 */
	public static String showLocalparseDateTo24Hours(String FormatStr , long ps_id) throws Exception{
		 if(!FormatStr.isEmpty()){
			Date date = createDateTime(FormatStr);
			String returnTime = showLocationTime(date, ps_id);
			SimpleDateFormat to = new SimpleDateFormat("MM/dd/yy HH:mm");
			return to.format(defaultFormage.parse(returnTime));
		}
		return "";
	}
	
	/**
	 * 转变成Local的时间 4+4 24H
	 * 
	 * @param FormatStr
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年11月12日
	 */
	public static String showLocalparseDateToNoYear24Hours(String FormatStr , long ps_id) throws Exception{
		if(!FormatStr.isEmpty()){
			Date date = createDateTime(FormatStr);
			String returnTime = showLocationTime(date, ps_id);
			SimpleDateFormat to = new SimpleDateFormat("MM/dd HH:mm");
			return to.format(defaultFormage.parse(returnTime));
		}
		return "";
	}
	
	/**
	 * 传入ps_id返回当前的Local Time
	 * 
	 * @param ps_id
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年12月4日
	 */
	public static String getCurrentLocalTimeNoYear(long ps_id) throws Exception {
		try {
			String returnTime = showLocationTime(new Date(), ps_id);
			SimpleDateFormat to = new SimpleDateFormat("MM/dd HH:mm");
			return to.format(defaultFormage.parse(returnTime));
		} catch (Exception e) {
		}
		return "";
	}
	
	public static String parseDateTo12Hour(String date) throws Exception {
		try {
			if (!StrUtil.isBlank(date)) {
				SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String hour = DateUtil.FormatDatetime("HH", from.parse(date));
				int hourInt = Integer.parseInt(hour);
				SimpleDateFormat to = new SimpleDateFormat("MM/dd/yyyy hh:mm");
				String re12HourDate = to.format(from.parse(date));
				if (hourInt > 12) {
					re12HourDate += "PM";
				} else {
					re12HourDate += "AM";
				}
				return re12HourDate;
			}
		} catch (Exception e) {
			
		}
		return "";
	}
	
	public static String parseDateFormat(String date, String format) throws Exception {
		if (!StrUtil.isBlank(date)) {
			SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (StrUtil.isBlank(format)) {
				format = "yyyy-MM-dd";
			}
			SimpleDateFormat to = new SimpleDateFormat(format);
			return to.format(from.parse(date));
		} else {
			return "";
		}
	}
	
	public static String parseDateTo12HourNoYear(String date) {
		//Calendar calendar = Calendar.getInstance();
		try {
			if (!StrUtil.isBlank(date)) {
				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				calendar.setTime(from.parse(date));
				int am_pm = calendar.get(Calendar.AM_PM);
				
				// String hour = DateUtil.FormatDatetime("HH", from.parse(date));
				// int hourInt = Integer.parseInt(hour);
				SimpleDateFormat to = new SimpleDateFormat("MM/dd hh:mm");
				String re12HourDate = to.format(from.parse(date));
				if (am_pm == 1) {
					re12HourDate += " PM";
				} else {
					re12HourDate += " AM";
				}
				return re12HourDate;
			}
			return "";
		} catch (Exception e) {
		}
		return "";
	}
	
	public static Date createDate(String time, String pattern) throws Exception {
		sdf = new SimpleDateFormat(pattern);
		return sdf.parse(time);
	}
	
	public static Date createDate(String time) throws Exception {
		sdf.applyPattern(defaultDateFormat);
		
		return sdf.parse(time);
	}
	
	public static Date createDateTime(String time) throws Exception {
		
		/*
		 * System.out.println(sdf.equals(time));
		 * 
		 * sdf.applyPattern(defaultDateTimeFormat);
		 * 
		 * 
		 * 
		 * return sdf.parse(time);
		 */
		return formateDate(time);
	}
	
	/**
	 * 转换时间字符串到时间 支持 yyyy-MM-dd HH:mm:ss yyyy-MM-dd MM/dd/YYY MM/dd/YYY HH:mm:ss
	 * 
	 * @param d
	 * @return
	 * @throws Exception
	 * @author zhangrui
	 * @Date 2014年9月30日
	 */
	public static Date formateDate(String d) throws Exception {
		Pattern dateFormats = Pattern
				.compile("(\\d\\d\\d\\d-\\d\\d-\\d\\d|\\d\\d/\\d\\d/\\d\\d\\d\\d)( \\d\\d:\\d\\d:\\d\\d)?");
		
		Matcher m = dateFormats.matcher(d);
		if (!m.find()) {
			throw new Exception("date Error" + d);
		}
		Date date = null;
		if (m.groupCount() == 2) {
			// with time part
			if (m.group(2) != null)
				date = new SimpleDateFormat(m.group(1).indexOf("-") > 0 ? "yyyy-MM-dd HH:mm:ss" : "MM/dd/yyyy HH:mm:ss")
						.parse(d);
			else
				date = new SimpleDateFormat(m.group(1).indexOf("-") > 0 ? "yyyy-MM-dd" : "MM/dd/yyyy").parse(d);
		}
		if (date == null) {
			throw new Exception("date Error" + d);
		}
		return date;
	}
	
	public static int getSecond(Date date) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		return calendar.get(Calendar.SECOND);
	}
	
	public static int getSecond(String time) throws Exception {
		return getSecond(createDateTime(time));
	}
	
	public static int getMinute(Date date) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		return calendar.get(Calendar.MINUTE);
	}
	
	public static int getMinute(String time) throws Exception {
		return getDayHour(createDateTime(time));
	}
	
	public static int getDayHour(Date date) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		return calendar.get(Calendar.HOUR_OF_DAY);
	}
	
	public static int getDayHour(String time) throws Exception {
		return getDayHour(createDateTime(time));
	}
	
	public static int getMonthDay(Date date) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		return (calendar.get(Calendar.DAY_OF_MONTH));
	}
	
	public static int getMonthDay(String time) throws Exception {
		return (getMonthDay(createDateTime(time)));
	}
	
	public static int getMonth(Date date) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		return (calendar.get(Calendar.MONTH) + 1);
	}
	
	public static int getYear(Date date) throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}
	
	public static int getYear(String time) throws Exception {
		return (getYear(createDateTime(time)));
	}
	
	private static int getSubDay(Date startDate, Date endDate) throws Exception {
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		
		SimpleDateFormat from = new SimpleDateFormat(defaultDateTimeFormat);
		SimpleDateFormat to = new SimpleDateFormat(defaultDateFormat);
		sdf.applyPattern(defaultDateFormat);
		
		startDate = sdf.parse(sdf.format(to.parse((from.format(startDate)))));
		endDate = sdf.parse(sdf.format(to.parse((from.format(endDate)))));
		
		start.setTime(startDate);
		end.setTime(endDate);
		
		long startTime = start.getTimeInMillis();
		long endTime = end.getTimeInMillis();
		
		long betweenSecond = (endTime - startTime) / 1000;
		
		int day_count = (int) (betweenSecond / 3600 / 24);
		
		return day_count;
	}
	
	/**
	 * 获得两个日期之间日期差 endDate-startDate
	 * 
	 * @param startDateString 格式:yyyy-MM-dd/yyyy-MM-dd HH:mm:ss
	 * @param endDateString 格式:yyyy-MM-dd/yyyy-MM-dd HH:mm:ss
	 * @return endDate-startDate差距天数
	 * @throws Exception
	 */
	public static int getSubDay(String startDateString, String endDateString) throws Exception {
		Date startDate = createDate(startDateString);
		Date endDate = createDate(endDateString);
		
		return getSubDay(startDate, endDate);
	}
	
	private static int getSubHour(Date startDate, Date endDate) throws Exception {
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		
		start.setTime(startDate);
		end.setTime(endDate);
		
		long startTime = start.getTimeInMillis();
		long endTime = end.getTimeInMillis();
		
		long betweenSecond = (endTime - startTime) / 1000;
		
		int hour = (int) (betweenSecond / 3600);
		
		return (hour);
	}
	
	/**
	 * 两个时间点的小时差
	 * 
	 * @param startDateTimeString 格式:yyyy-MM-dd HH:mm:ss
	 * @param endDateTimeString 格式:yyyy-MM-dd HH:mm:ss
	 * @return endDateTime-startDateTime 的小时差
	 * @throws Exception
	 */
	public static int getSubHour(String startDateTimeString, String endDateTimeString) throws Exception {
		Date startDateTime = createDateTime(startDateTimeString);
		Date endDateTime = createDateTime(endDateTimeString);
		
		return (getSubHour(startDateTime, endDateTime));
	}
	
	public static void main(String args[]) throws Exception {
		
		System.out.println(calcDate("2015-02-12 08:02:50",30, true));
	/*	System.out.println(StringToDate("2014-02-03"));
		System.out.println(StringToDate("2014-02-03 11:20:33"));
		System.out.println(StringToDate("2014-02-03 11:20"));
		System.out.println(StringToDate("02/03/2014"));
		System.out.println(StringToDate("02/03/2014 11:20"));*/
		/*
		Map<Long, String> map = new HashMap<Long, String>();
		
		map.put(1000005l, "America/Los_Angeles");
		map.put(1000007l, "America/Los_Angeles");
		map.put(1000009l, "America/Los_Angeles");
		map.put(1000018l, "America/Chicago");
		
		setStroageTimeZoneId(map);
		
		Date date = createDateTime("2015-03-08 00:00:00");

		
		System.out.println(showLocationTime(date, 1000005l));
		System.out.println(showLocationTime(date, 1000018l));
		System.out.println(showUTCTime(date, 1000005l));
		System.out.println(showUTCTime(date, 1000018l));
		System.out.println(showLocationTime(date, 1000008l));
		System.out.println(showUTCTime(date, 1000008l));
		
		
		//TimeZone tz = TimeZone.getTimeZone("GMT-08");
		TimeZone tz = TimeZone.getTimeZone("America/Chicago");
		
		Locale loc = Locale.US;
		//Calendar c = Calendar.getInstance(tz, loc);
		Calendar c = Calendar.getInstance(tz);
		//Calendar c = Calendar.getInstance();
		//c.setTime(date);
		c.setTimeInMillis(date.getTime());
		//c.setTimeInMillis(date.getTime() - tz.getOffset(date.getTime()));
		
		System.out.println("use daylight : " + tz.useDaylightTime());
		System.out.println("in daylight : " + tz.inDaylightTime(date));
		System.out.println(tz.getDisplayName());
		System.out.println(tz.getRawOffset());
		System.out.println(tz.getOffset(date.getTime()));
		System.out.println(tz.getDSTSavings());
		System.out.println(tz.getOffset(date.getTime())/1000/60/60);
		
		SimpleDateFormat sdf = new SimpleDateFormat(defaultDateTimeFormat);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+00"));
		System.out.println(sdf.format(c.getTime()));
		sdf.setTimeZone(tz);
		System.out.println(sdf.format(date));
		
		
		System.out.println("===================");
		
		
		System.out.println(DatetimetoStr(c.getTime()));
		System.out.println(c.get(Calendar.ZONE_OFFSET));
		System.out.println(c.toString());
		
		System.out.println("===================");
		*/
		/*
		System.out.println("===================");
		String[] ids = TimeZone.getAvailableIDs(-21600000);
		for (String s : ids){
			System.out.println(s);
		}
		*/
	}
	/*
	public static void setStroageJetLeg(Map<Long, Long> stroageJetLeg) {
		DateUtil.stroageJetLeg = stroageJetLeg;
	}
	*/
	public static void setStroageTimeZoneId(Map<Long, String> stroageTimeZoneId) {
		DateUtil.stroageTimeZoneId = stroageTimeZoneId;
	}

	public static Date StringToDate(String str) {
		Date date = null;
		SimpleDateFormat dateFormat = null;
		try {
			dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			date = dateFormat.parse(str);
		} catch (ParseException e) {
			try {
				dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				date = dateFormat.parse(str);
			} catch (ParseException e1) {
				try {
					dateFormat = new SimpleDateFormat("MM/dd/yy HH:mm");
					date = dateFormat.parse(str);
				} catch (ParseException e2) {
					try {
						dateFormat = new SimpleDateFormat("MM/dd/yy");
						date = dateFormat.parse(str);
					} catch (ParseException e3) {
						try {
							dateFormat = new SimpleDateFormat("yyyy-MM-dd");
							date = dateFormat.parse(str);
						} catch (ParseException e4) {
							
						}
						
					}
					
				}
				
			}
		}
		
		return date;
	}
	/**
	 * 日期加减计算
	 * @author Liang Jie
	 * @param dateString
	 * @param days
	 * @param isAdd
	 * @return
	 * @throws Exception
	 */
	public static String calcDate(String dateString,int days, boolean isAdd) throws Exception{
		Date date = StringToDate(dateString);
		return calcDate(date,days,isAdd);
	}
	/**
	 * 日期加减计算
	 * @author Liang Jie
	 * @param date
	 * @param days
	 * @param isAdd
	 * @return
	 * @throws Exception
	 */
	public static String calcDate(Date date, int days, boolean isAdd) throws Exception{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		if(isAdd){
			calendar.add(Calendar.DATE, days);
		}else{
			calendar.add(Calendar.DATE, days*(-1));
		}	
		Date resultDate = calendar.getTime();
		return DatetimetoStr(resultDate);
	}
	
}
