package com.cwc.app.util;

import java.util.Iterator;
import java.util.Stack;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

public class Neo4jPathAnalyser2 extends Neo4jPathAnalyser {
	protected Stack<JSONObject>  pathStack = new Stack<JSONObject>();

	public Neo4jPathAnalyser2(JSONObject containerTree) throws Exception {
		super();
		analyse(containerTree);
	}

	protected void analyse(JSONObject c) throws Exception {
		JSONObject current = new JSONObject();
		for(Iterator<String> i = c.keys(); i.hasNext();){
			String k = i.next();
			if(k.equals("sn") || k.equals("products") || k.equals("children")) continue;
			current.put(k, c.get(k));
		}
		
		if(c.has("products")){
			JSONArray products = c.getJSONArray("products");
			for(int i=0; i<products.length(); i++){
				JSONObject p = new JSONObject(products.getJSONObject(i).toString());  //clone the product object
				JSONObject cp = new JSONObject();
				cp.put("quantity", p.optInt("quantity",0) );
				cp.put("locked_quantity", p.optInt("locked_quantity",0));
				p.remove("quantity");
				p.remove("locked_quantity");
				p.remove("total_locked_quantity");
				p.remove("total_quantity");
				if( cp.getInt("quantity") > 0){  //current container contains this product directly, so we find a cpkey! 
					//add a cpkey
					String cpkey = current.get("con_id")+"-"+p.get("pc_id")+":"+p.get("title_id");
					this.cpkeys.add(cpkey);
					//we also find a pathkey and a path, so add a pathkey and a path
					JSONArray path = new JSONArray();
					StringBuilder pathkey = new StringBuilder();
					for(int l=0; l<pathStack.size(); l++){
						JSONObject ps = pathStack.get(l);
						path.put(ps);  //container node in stack
						path.put(new JSONObject()); //CONTAINS rel, that no properties
						pathkey.append(ps.get("con_id")).append("-");
					}
					
					path.put(current).put(cp).put(p);
					this.paths.put(path);
					
					pathkey.append(cpkey);
					this.pathkeys.add(pathkey.toString());
				}
			}//for
		}
		if(c.has("children")){
			JSONArray children = c.getJSONArray("children");
			pathStack.push(current);
			for(int i=0; i<children.length(); i++){
				//find a sub-container, so we find a cckey
				JSONObject child = children.getJSONObject(i);
				this.cckeys.add(current.get("con_id")+"-"+child.get("con_id"));
				analyse(child);
			}
			pathStack.pop();
		}
		
	}
	
	

}
