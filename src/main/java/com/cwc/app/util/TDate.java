package com.cwc.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 * 日期操作包
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */
public class TDate 
{
	private GregorianCalendar firstDay; 
	//final int dateArraySize = 6; 
	private int year,month,day,week,hour,weekyear,minute,second;
	String[] weekDays={ "星期日","星期一","星期二","星期三","星期四","星期五","星期六" };
	//{Resource.getStringValue("","class.visitormgr_weekdays_7","null"),Resource.getStringValue("","class.visitormgr_weekdays_1","null"),Resource.getStringValue("","class.visitormgr_weekdays_2","null"),Resource.getStringValue("","class.visitormgr_weekdays_3","null"),Resource.getStringValue("","class.visitormgr_weekdays_4","null"),Resource.getStringValue("","class.visitormgr_weekdays_5","null"),Resource.getStringValue("","class.visitormgr_weekdays_6","null")};
	private String[] dates;
	

	/**
	 * 根据GregorianCalendar初始化一些参数
	 * @param gcDate
	 */
	private TDate(GregorianCalendar gcDate)
	{ 
		year = gcDate.get(GregorianCalendar.YEAR); 
		month = gcDate.get(GregorianCalendar.MONTH);
		day = gcDate.get(GregorianCalendar.DATE); 
		week = gcDate.get(GregorianCalendar.DAY_OF_WEEK)-1; 
		hour = gcDate.get(GregorianCalendar.HOUR_OF_DAY);
		gcDate.setFirstDayOfWeek(GregorianCalendar.MONDAY);
		weekyear = gcDate.get(GregorianCalendar.WEEK_OF_YEAR)-1;
		minute = gcDate.get(GregorianCalendar.MINUTE);
		second = gcDate.get(GregorianCalendar.SECOND);
		firstDay = new GregorianCalendar(year, month, day,hour,minute,second); 			//用年月日初始化一个日历
		
		
//		oneDay.add(GregorianCalendar.DATE, 1); 
//		oneWeek.add(GregorianCalendar.DATE, 7); 
//		oneMonth.add(GregorianCalendar.MONTH, 1); 
//		oneQuarter.add(GregorianCalendar.MONTH, 3); 
//		oneYear.add(GregorianCalendar.YEAR, 1); 
	}

	/**
	 * 
	 *
	 */
	public TDate()
	{ 
		this(new GregorianCalendar()); 
	} 
	
	public TDate(String dateString) 
		throws ParseException
	{	
		
		this(tDate(dateString));
	}
	
	private static GregorianCalendar tDate(String dateString) 
		throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = sdf.parse(dateString);
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		
		return (gc);
	}
	
	/**
	 * 把日期加一小时
	 * @param v
	 */
	public void addHour(int v)
	{
		firstDay.add(GregorianCalendar.HOUR_OF_DAY,v);
	}

	/**
	 * 把日期加一天
	 * @param v
	 */
	public void addDay(int v)
	{
		firstDay.add(GregorianCalendar.DATE,v);
	}
	
	/**
	 * 把日期加一周
	 * @param v
	 */
	public void addWeek(int v)
	{
		firstDay.add(GregorianCalendar.DATE,7*v);
	}

	/**
	 * 把日期加一月
	 * @param v
	 */
	public void addMonth(int v)
	{
		firstDay.add(GregorianCalendar.MONTH,v);
	}
	
	/**
	 * 把日期加一季度
	 * @param v
	 */
	public void addQuarter(int v)
	{
		firstDay.add(GregorianCalendar.MONTH, 3*v);
	}
	
	/**
	 * 把日期加一年
	 * @param v
	 */
	public void addYear(int v)
	{
		firstDay.add(GregorianCalendar.YEAR,v);
	}
	
	/**
	 * 把分钟加一
	 * @param v
	 */
	public void addMinute(int v)
	{
		firstDay.add(GregorianCalendar.MINUTE,v);
	}
	/**
	 * 把秒加一
	 * @param v
	 */
	public void addSecond(int v)
	{
		firstDay.add(GregorianCalendar.SECOND,v);
	}

	/**
	 * 格式化日期
	 * @param formatStr
	 * @return
	 */
	public String formatDate(String formatStr)
	{
		//"yyyy-MM-dd"
        SimpleDateFormat bartDateFormat = new SimpleDateFormat(formatStr);
        return(bartDateFormat.format(firstDay.getTime()));
	}
	
//	public GregorianCalendar[] getDates() 
//	{
//		GregorianCalendar[] memoryDates = new GregorianCalendar[dateArraySize]; 
//		memoryDates[0] = firstDay; 
//		memoryDates[1] = oneDay; 
//		memoryDates[2] = oneWeek; 
//		memoryDates[3] = oneMonth; 
//		memoryDates[4] = oneQuarter; 
//		memoryDates[5] = oneYear; 
//		return memoryDates; 
//	} 

	/**
	 * 获得int年
	 */
	public int getIntYear() 
	{ 
		return(StrUtil.getInt(getStringYear()));
	}
	
	/**
	 * 获得int月
	 * @return
	 */
	public int getIntMonth() 
	{ 
		return(StrUtil.getInt(getStringMonth()));
	}
	
	/**
	 * 获得int日
	 * @return
	 */
	public int getIntDay()
	{ 
		return(StrUtil.getInt(getStringDay()));
	} 
	
	/**
	 * 获得string年
	 * @return
	 */
	public String getStringYear()
	{ 
		return( formatDate("yyyy") );
	}
	
	/**
	 * 获得string月
	 * @return
	 */
	public String getStringMonth() 
	{ 
		return( formatDate("M") );
	}
	
	/**
	 * 获得string月
	 * @return
	 */
	public String getStringMonthDouble() 
	{ 
		return( formatDate("MM") );
	}
	
	/**
	 * 获得string日
	 * @return
	 */
	public String getStringDay()
	{ 
		return( formatDate("d") );
	} 
	
	/**
	 * 获得string日
	 * @return
	 */
	public String getStringDayDouble()
	{ 
		return( formatDate("dd") );
	} 
	
	/**
	 * 获得string小时
	 * @return
	 */
	public String getStringHour()
	{ 
		return( String.valueOf(getIntHour()) );
	} 
	
	/**
	 * 获得string小时
	 * @return
	 */
	public String getStringHourDouble()
	{ 
		return( formatDate("HH"));
	} 
	
	/**
	 * 获得string分钟
	 * @return
	 */
	public String getStringMinuteDouble()
	{ 
		return(formatDate("mm"));
	} 
	
	/**
	 * 获得int周几
	 * @return
	 */
	public int getIntWeek() 
	{ 
		return(week);
	}
	
	/**
	 * 获得int当前日期属于一年第几周（美式周日为第一天）
	 * @return
	 */
	public int getIntWeekYear()
	{
		return (weekyear);
	}
	/**
	 * 通过日期获得int属于一年第几周（可设定一周从周几开始）
	 * @param date
	 * @param Start
	 * @return
	 */
	public int getIntWeekYear(String date,int calendar_DAY_OF_WEEK)
	{
		GregorianCalendar t = new GregorianCalendar(StrUtil.getInt(date.split("-")[0]),StrUtil.getInt(date.split("-")[1])-1,StrUtil.getInt(date.split("-")[2]),1,0,0);
		t.setFirstDayOfWeek(calendar_DAY_OF_WEEK);
		return (t.get(GregorianCalendar.WEEK_OF_YEAR)-1);
	}
	
	/**
	 * 通过日期获得int周
	 * @param date
	 * @return
	 */
	public int getIntWeek(String date) 
	{
		GregorianCalendar t = new GregorianCalendar(StrUtil.getInt(date.split("-")[0]),StrUtil.getInt(date.split("-")[1])-1,StrUtil.getInt(date.split("-")[2]),1,0,0);
		
		return(t.get(GregorianCalendar.DAY_OF_WEEK)-1);
	}
	
	/**
	 * 获得string周
	 * @return
	 */
	public String getStringWeek() 
	{ 
		int w = getIntWeek();
		if (w<0)
		{
			w = 0;
		}
		
		//星期有错，先屏蔽
		return(weekDays[w]);
		
		//return("");
	}
	
	/**
	 * 
	 * @param date
	 * @return
	 */
	public String getStringWeek(String date) 
	{ 
		int w = getIntWeek(date);
		if (w<0)
		{
			w = 0;
		}
		return(weekDays[w]);
	}
	
	/**
	 * 
	 * @return
	 */
	public int getIntHour() 
	{ 
		return(firstDay.get(Calendar.HOUR_OF_DAY));
	}
	
	public int getIntMinute()
	{
		return (firstDay.get(Calendar.MINUTE));
	}
	
	public int getIntSecond()
	{
		return (firstDay.get(Calendar.SECOND));
	}

	/**
	 * 把两个日期相减
	 * @param sy			起点年
	 * @param sm			起点月
	 * @param sd			起点日
	 * @param sh			起点小时
	 * @param smm			起点分钟
	 * @param ss			起点秒
	 * @param ey			终点年
	 * @param em			终点月
	 * @param ed			终点日
	 * @param eh			终点小时
	 * @param emm			终点分钟
	 * @param es			终点秒
	 * @return
	 */
	public long getSubTime(int sy,int sm,int sd,int sh,int smm,int ss,int ey,int em,int ed,int eh,int emm,int es)
	{
		GregorianCalendar t1 = new GregorianCalendar(sy,sm,sd,sh,smm,ss); 
		GregorianCalendar t2 = new GregorianCalendar(ey,em,ed,eh,emm,es); 
		
		long st = t2.getTimeInMillis()-t1.getTimeInMillis();
		
		return(st/1000);
	}
	
	/**
	 * 从日期字符串中获得int年
	 * @param date			yyyy-MM-dd HH:MM:SS
	 * @return
	 */
	public int getIntYearFromDateString(String date)
	{
		return( StrUtil.getInt(date.split(" ")[0].split("-")[0]) );
	}
	
	/**
	 * 从日期字符串中获得int月
	 * @param date			yyyy-MM-dd HH:MM:SS
	 * @return
	 */
	public int getIntMonthFromDateString(String date)
	{
		return( StrUtil.getInt(date.split(" ")[0].split("-")[1]) );
	}
	
	/**
	 * 从日期字符串中获得int日
	 * @param date			yyyy-MM-dd HH:MM:SS
	 * @return
	 */
	public int getIntDayFromDateString(String date)
	{
		return( StrUtil.getInt(date.split(" ")[0].split("-")[2]) );
	}
	
	/**
	 * 从日期字符串中获得int小时
	 * @param date			yyyy-MM-dd HH:MM:SS
	 * @return
	 */
	public int getIntHourFromDateString(String date)
	{
		return( StrUtil.getInt(date.split(" ")[1].split(":")[0]) );
	}
	
	/**
	 * 从日期字符串中获得int分钟
	 * @param date			yyyy-MM-dd HH:MM:SS
	 * @return
	 */
	public int getIntMinFromDateString(String date)
	{
		return( StrUtil.getInt(date.split(" ")[1].split(":")[1]) );
	}
	
	/**
	 * 从日期字符串中获得int秒
	 * @param date			yyyy-MM-dd HH:MM:SS
	 * @return
	 */
	public int getIntSecFromDateString(String date)
	{
		return( StrUtil.getInt(date.split(" ")[1].split(":")[2]) );
	}
	
	/**
	 * 把字符串型日期转换为long time
	 * @param date
	 * @return
	 * @throws Exception
	 */
	public long getDateTime(String date)
		throws Exception
	{
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
		return( s.parse(date).getTime() );
	}
	
	public long getDateTimeFullTime(String date)
		throws Exception
	{
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return( s.parse(date).getTime() );
	}
	
	/**
	 * 获得当前日期long time
	 * @return
	 * @throws Exception
	 */
	public long getDateTime()
		throws Exception
	{
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = getStringYear()+"-"+getStringMonth()+"-"+getStringDay()+" "+getStringHour()+":"+getIntMinute()+":"+getIntSecond();
		return( s.parse(time).getTime() );
	}
	
	
	public String getThisMonthFirstDay()
	{
		 int _month = month + 1; 
		 return this.year+"-"+(_month < 10 ? "0"+_month:_month)+"-01 00:00:00";
	}
	
	public String getCurrentTime(){
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		return ""+s.format(date)+"";
	}
	public String getFormateTime(String dateString)
	{
		if(dateString.length() < 1)
		{
			return "&nbsp;";
		}
		SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat to = new SimpleDateFormat("MM-dd HH:mm");
		try 
		{
			return to.format(from.parse(dateString)).toString();
		}
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		return dateString;
	}
	/**
	 * format date for English  eg. 30/07/2014 09:00
	 * 
	 * create by wangcr
	 * 
	 * @param dateString {yyyy-MM-dd HH:mm:ss}
	 * @return {dd/MM/yyyy HH:mm}
	 */
	public String getEnglishFormateTime(String dateString)
	{
		if(dateString.length() < 1)
		{
			return "&nbsp;";
		}
		SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat to = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		try 
		{
			return to.format(from.parse(dateString)).toString();
		}
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		return dateString;
	}
	/**
	 * short format date for English eg. 30/07 09:00
	 * create by wangcr
	 * 
	 * @param dateString {yyyy-MM-dd HH:mm:ss}
	 * @return {dd/MM HH:mm}
	 */
	public String getEnglishShortFormateTime(String dateString) {
		if(dateString.length() < 1)
		{
			return "&nbsp;";
		}
		SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat to = new SimpleDateFormat("dd/MM HH:mm");
		try 
		{
			return to.format(from.parse(dateString)).toString();
		}
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		return dateString;
	}
	public String getYYMMDDOfNow(){
		SimpleDateFormat from = new SimpleDateFormat("yy-MM-dd");
		return from.format(new Date());
	}
	public String getFormateTime(String dateString,String formate) {
		if(dateString.length() < 1){
			return "";
		}
		SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat to = new SimpleDateFormat(formate);
		try {
			return to.format(from.parse(dateString)).toString();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateString;
	}
	public String getThisMonthLastDay(){
		 int _month = month + 1; 
		 int days = 0;
		 if(_month!=2){
		  switch(_month){
			  case 1:
			  case 3:
			  case 5:
			  case 7:
			  case 8:
			  case 10:
			  case 12:days = 31 ;break;
			  case 4:
			  case 6:
			  case 9:
			  case 11:days = 30;
		  }
		 }
		 else{
		  if(year%4==0 && year%100!=0 || year%400==0){
			  days = 29;
		  } else{
			  days = 28;
		 }
		}
		  
		return this.year+"-"+(_month < 10 ? "0"+_month:_month) +"-"+days+" 23:59:59";
	}
	
	public static void main(String args[])
	throws Exception
	{
		TDate r = new TDate(new GregorianCalendar());
		r.getThisMonthFirstDay();
		TDate tDate = new TDate();
		//tDate.addYear(1);
		
		//System.out.println(tDate.getStringYear()+" "+tDate.getStringMonth()+" "+tDate.getStringDay());
//		String old_date = "2007-01-05 18:05:03";
//		old_date = old_date.substring(0,19);
//		String post_date = "2007-01-05 18:13:40";
//		
//		int sy = tDate.getIntYearFromDateString(old_date);
//		int sm = tDate.getIntMonthFromDateString(old_date);
//		int sd = tDate.getIntDayFromDateString(old_date);
//		int sh = tDate.getIntHourFromDateString(old_date);
//		int smm = tDate.getIntMinFromDateString(old_date);
//		int ss = tDate.getIntSecFromDateString(old_date);
//		int ey = tDate.getIntYearFromDateString(post_date);
//		int em = tDate.getIntMonthFromDateString(post_date);
//		int ed = tDate.getIntDayFromDateString(post_date);
//		int eh = tDate.getIntHourFromDateString(post_date);
//		int emm = tDate.getIntMinFromDateString(post_date);
//		int es = tDate.getIntSecFromDateString(post_date);
		
		//System.out.println( tDate.getSubTime(sy,sm,sd,sh,smm,ss,ey,em,ed,eh,emm,es) );
		
		//System.out.println(tDate.getSubTime(2007,1,1,17,11,32,2007,1,2,17,11,32)/1000 );
//		System.out.println(tDate.getDateTime(DateUtil.NowStr()));
		//System.out.println(tDate.getStringWeek("2007-1-21"));
		//System.out.println(tDate.getStringWeek());
//		System.out.println(tDate.getDateTime());
//		tDate.addDay(-1);
//		System.out.println(tDate.getDateTime());
		//System.out.println(tDate.getDateTime("2007-1-2 11:11:11"));
		//System.out.println(tDate.getDateTime("2007-05-01"));
		//System.out.println(tDate.getDateTime("2007-05-11"));
		//System.out.println(tDate.getDateTime());
		
		//System.out.println( (tDate.getDateTime("2007-05-01")<=tDate.getDateTime()&&tDate.getDateTime("2007-05-11")>=tDate.getDateTime()));
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}
	private long fromDateStringToLong(String strDate) { // 此方法计算时间毫秒
		Date date = null; // 定义时间类型
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try 
		{
			date = inputFormat.parse(strDate); // 将字符型转换成日期型
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return date.getTime(); // 返回毫秒数
	}
	public double getDiffDate(String strEndDate,String rtnType)
		throws ParseException 
	{
		  long startT = fromDateStringToLong(getCurrentTime()); //定义上机时间
		  TDate dateEn = new TDate(strEndDate);
		  long endT = fromDateStringToLong(dateEn.formatDate("yyyy-MM-dd HH:mm:ss"));  //定义下机时间
		  double ss=(startT-endT)/(1000); //共计秒数
		  double mm = ss/60;   //共计分钟数
		  double hh= ss/3600;  //共计小时数
		  double dd= hh/24;   //共计天数
		  if(rtnType.equals("ss"))
		  {
			  return ss;
		  }
		  else if(rtnType.equals("hh"))
		  {
			  return hh;
		  }
		  else if(rtnType.equals("mm"))
		  {
			  return mm;
		  }
		  else if(rtnType.equals("dd"))
		  {
			  return dd;
		  }
		  else
		  {
			  return dd;
		  }
			  
	}
}




