package com.cwc.app.exception.transport;
/**
 * 缺货检查后库存改变且缺货
 * @author Administrator
 *
 */
public class TransportLackException extends Exception {
	public TransportLackException() 
	{
		super();
	}
	
	public TransportLackException(String inMessage)
	{
		super(inMessage);
	}
}
