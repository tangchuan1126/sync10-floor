package com.cwc.app.exception.transport;
/**
 * 无法填写序列号
 * @author Administrator
 *
 */
public class HadSerialNumberException extends Exception {
	public HadSerialNumberException() 
	{
		super();
	}
	
	public HadSerialNumberException(String inMessage)
	{
		super(inMessage);
	}
}
