package com.cwc.app.exception.transport;
/**
 * 无法填写序列号
 * @author Administrator
 *
 */
public class TransportSerialNumberRepeatException extends Exception {
	public TransportSerialNumberRepeatException() 
	{
		super();
	}
	
	public TransportSerialNumberRepeatException(String inMessage)
	{
		super(inMessage);
	}
}
