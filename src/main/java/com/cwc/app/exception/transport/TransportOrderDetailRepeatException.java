package com.cwc.app.exception.transport;
/**
 * 转运单详细重复
 * @author Administrator
 *
 */
public class TransportOrderDetailRepeatException extends Exception {
	public TransportOrderDetailRepeatException() 
	{
		super();
	}
	
	public TransportOrderDetailRepeatException(String inMessage)
	{
		super(inMessage);
	}
}
