package com.cwc.app.exception.transport;
/**
 * 无法填写序列号(序列号只可标记一件商品)
 * @author Administrator
 *
 */
public class CanNotFillSerialNumberException extends Exception {
	public CanNotFillSerialNumberException() 
	{
		super();
	}
	
	public CanNotFillSerialNumberException(String inMessage)
	{
		super(inMessage);
	}
}
