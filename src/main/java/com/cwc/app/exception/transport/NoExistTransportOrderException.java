package com.cwc.app.exception.transport;
/**
 * 转运单不存在
 * @author Administrator
 *
 */
public class NoExistTransportOrderException extends Exception {
	public NoExistTransportOrderException() 
	{
		super();
	}
	
	public NoExistTransportOrderException(String inMessage)
	{
		super(inMessage);
	}
}
