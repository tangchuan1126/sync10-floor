package com.cwc.app.exception.transport;
/**
 * 转运单明细不存在
 * @author Administrator
 *
 */
public class NoExistTransportDetailException extends Exception {
	public NoExistTransportDetailException() 
	{
		super();
	}
	
	public NoExistTransportDetailException(String inMessage)
	{
		super(inMessage);
	}
}
