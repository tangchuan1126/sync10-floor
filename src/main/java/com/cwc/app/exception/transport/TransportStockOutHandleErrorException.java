package com.cwc.app.exception.transport;


/**
 * 订单并非已打印
 * @author Administrator
 *
 */
public class TransportStockOutHandleErrorException extends Exception {
	
	public TransportStockOutHandleErrorException() 
	{
		super();
	}
	
	public TransportStockOutHandleErrorException(String inMessage)
	{
		super(inMessage);
	}
}
