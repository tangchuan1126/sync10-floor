package com.cwc.app.exception.transport;
/**
 * 转运单不可入库
 * @author Administrator
 *
 */
public class TransportOrderCanNotWareHouseException extends Exception 
{
	public TransportOrderCanNotWareHouseException() 
	{
		super();
	}
	
	public TransportOrderCanNotWareHouseException(String inMessage)
	{
		super(inMessage);
	}
}
