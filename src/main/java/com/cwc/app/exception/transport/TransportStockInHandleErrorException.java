package com.cwc.app.exception.transport;


/**
 * 订单并非已打印
 * @author Administrator
 *
 */
public class TransportStockInHandleErrorException extends Exception {
	
	public TransportStockInHandleErrorException() 
	{
		super();
	}
	
	public TransportStockInHandleErrorException(String inMessage)
	{
		super(inMessage);
	}
}
