package com.cwc.app.exception.transport;
/**
 * 转运单不可出库
 * @author Administrator
 *
 */
public class TransportOrderCanNotOutboundException extends Exception 
{
	public TransportOrderCanNotOutboundException() 
	{
		super();
	}
	
	public TransportOrderCanNotOutboundException(String inMessage)
	{
		super(inMessage);
	}
}
