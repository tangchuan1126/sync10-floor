package com.cwc.app.exception.special;

public class SpecialAddException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SpecialAddException() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public SpecialAddException(String message, Throwable cause) {
		super(message, cause);
 	}

	
}
