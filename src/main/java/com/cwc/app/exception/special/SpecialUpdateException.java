package com.cwc.app.exception.special;

public class SpecialUpdateException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SpecialUpdateException() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public SpecialUpdateException(String message, Throwable cause) {
		super(message, cause);
 	}

	
}
