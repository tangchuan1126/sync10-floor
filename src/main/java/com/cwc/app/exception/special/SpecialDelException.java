package com.cwc.app.exception.special;

public class SpecialDelException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SpecialDelException() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public SpecialDelException(String message, Throwable cause) {
		super(message, cause);
 	}
	
	public SpecialDelException(String message) {
		
		super(message);
 	}
	
	
}
