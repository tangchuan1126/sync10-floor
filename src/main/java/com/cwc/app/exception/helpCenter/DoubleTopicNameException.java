package com.cwc.app.exception.helpCenter;

/**
 * 同级分类下有相同标题的文章
 * @author Administrator
 *
 */
public class DoubleTopicNameException extends Exception 
{
	public DoubleTopicNameException() 
	{
		super();
	}
	
	public DoubleTopicNameException(String inMessage)
	{
		super(inMessage);
	}
}
