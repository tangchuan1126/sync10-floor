package com.cwc.app.exception.helpCenter;
/**
 * 不是作者，不能删除
 * @author Administrator
 *
 */
public class DelHelpCenterTopicNotOwnerException extends Exception 
{
	public DelHelpCenterTopicNotOwnerException() 
	{
		super();
	}
	
	public DelHelpCenterTopicNotOwnerException(String inMessage)
	{
		super(inMessage);
	}
}
