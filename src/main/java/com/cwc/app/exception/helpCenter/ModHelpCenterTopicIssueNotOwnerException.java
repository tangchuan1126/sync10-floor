package com.cwc.app.exception.helpCenter;
/**
 * 不是作者，不能发布
 * @author Administrator
 *
 */
public class ModHelpCenterTopicIssueNotOwnerException extends Exception 
{
	public ModHelpCenterTopicIssueNotOwnerException() 
	{
		super();
	}
	
	public ModHelpCenterTopicIssueNotOwnerException(String inMessage)
	{
		super(inMessage);
	}
}
