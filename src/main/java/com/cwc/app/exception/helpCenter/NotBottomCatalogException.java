package com.cwc.app.exception.helpCenter;
/**
 * 并非底级分类
 * @author Administrator
 *
 */
public class NotBottomCatalogException extends Exception
{
	public NotBottomCatalogException() 
	{
		super();
	}
	
	public NotBottomCatalogException(String inMessage)
	{
		super(inMessage);
	}
}
