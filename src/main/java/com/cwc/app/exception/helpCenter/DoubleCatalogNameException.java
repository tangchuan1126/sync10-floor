package com.cwc.app.exception.helpCenter;
/**
 * 分类名相同
 * @author Administrator
 *
 */
public class DoubleCatalogNameException extends Exception 
{
	public DoubleCatalogNameException() 
	{
		super();
	}
	
	public DoubleCatalogNameException(String inMessage)
	{
		super(inMessage);
	}
}
