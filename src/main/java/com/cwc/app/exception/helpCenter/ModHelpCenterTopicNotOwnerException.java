package com.cwc.app.exception.helpCenter;
/**
 * 不是作者，不能修改
 * @author Administrator
 *
 */
public class ModHelpCenterTopicNotOwnerException extends Exception 
{
	public ModHelpCenterTopicNotOwnerException() 
	{
		super();
	}
	
	public ModHelpCenterTopicNotOwnerException(String inMessage)
	{
		super(inMessage);
	}
}
