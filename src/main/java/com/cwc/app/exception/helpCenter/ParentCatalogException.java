package com.cwc.app.exception.helpCenter;
/**
 * 父级分类，有子级分类
 * @author Administrator
 *
 */
public class ParentCatalogException extends Exception 
{
	public ParentCatalogException() 
	{
		super();
	}
	
	public ParentCatalogException(String inMessage)
	{
		super(inMessage);
	}
}
