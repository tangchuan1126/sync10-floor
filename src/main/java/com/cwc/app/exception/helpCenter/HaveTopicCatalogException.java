package com.cwc.app.exception.helpCenter;
/**
 * 分类下有文章
 * @author Administrator
 *
 */
public class HaveTopicCatalogException extends Exception {
	public HaveTopicCatalogException() 
	{
		super();
	}
	
	public HaveTopicCatalogException(String inMessage)
	{
		super(inMessage);
	}
}
