package com.cwc.app.exception.catalog;

/**
 * 仓库分类已经跟库存关联
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class HaveRelationStorageException extends Exception 
{
	public HaveRelationStorageException() 
	{
		super();
	}
	
	public HaveRelationStorageException(String inMessage)
	{
		super(inMessage);
	}
}
