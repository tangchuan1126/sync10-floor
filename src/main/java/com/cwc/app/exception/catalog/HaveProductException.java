package com.cwc.app.exception.catalog;

/**
 * 货架下有商品
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class HaveProductException extends Exception 
{
	public HaveProductException() 
	{
		super();
	}
	
	public HaveProductException(String inMessage)
	{
		super(inMessage);
	}
}
