package com.cwc.app.exception.LPType;

public class InnerContainerTypeNotFoundException extends Exception 
{
	public InnerContainerTypeNotFoundException() 
	{
		super();
	}
	
	public InnerContainerTypeNotFoundException(String inMessage)
	{
		super(inMessage);
	}
}
