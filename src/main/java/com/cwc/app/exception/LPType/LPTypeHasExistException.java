package com.cwc.app.exception.LPType;

/**
 * 汇率不存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class LPTypeHasExistException extends Exception 
{
	public LPTypeHasExistException() 
	{
		super();
	}
	
	public LPTypeHasExistException(String inMessage)
	{
		super(inMessage);
	}
}
