package com.cwc.app.exception.LPType;

/**
 * 汇率不存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class CLPHasExistException extends Exception 
{
	public CLPHasExistException() 
	{
		super();
	}
	
	public CLPHasExistException(String inMessage)
	{
		super(inMessage);
	}
}
