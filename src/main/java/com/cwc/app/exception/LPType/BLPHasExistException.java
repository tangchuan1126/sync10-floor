package com.cwc.app.exception.LPType;

/**
 * 汇率不存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class BLPHasExistException extends Exception 
{
	public BLPHasExistException() 
	{
		super();
	}
	
	public BLPHasExistException(String inMessage)
	{
		super(inMessage);
	}
}
