package com.cwc.app.exception.paypal;

/**
 * SetExpressCheckOut成功
 * @author Administrator
 *
 */
public class SetExpressCheckOutSufException extends Exception{
	
	public SetExpressCheckOutSufException()
	{
		super();
	}
	public SetExpressCheckOutSufException(String inMessage) {
		super(inMessage);
	}
}