package com.cwc.app.exception.paypal;

public class SetExpressCheckOutFalException extends Exception{
	
	public SetExpressCheckOutFalException()
	{
		super();
	}
	public SetExpressCheckOutFalException(String inMessage) {
		super(inMessage);
	}
}