package com.cwc.app.exception.productLine;

public class CatalogHasProductLineException extends Exception {
	
	public CatalogHasProductLineException() 
	{
		super();
	}
	
	public CatalogHasProductLineException(String inMessage)
	{
		super(inMessage);
	}
}
