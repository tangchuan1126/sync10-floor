package com.cwc.app.exception.damagedRepair;
/**
 * 转运单不可出库
 * @author Administrator
 *
 */
public class DamagedRepairOrderCanNotOutboundException extends Exception 
{
	public DamagedRepairOrderCanNotOutboundException() 
	{
		super();
	}
	
	public DamagedRepairOrderCanNotOutboundException(String inMessage)
	{
		super(inMessage);
	}
}
