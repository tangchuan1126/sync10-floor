package com.cwc.app.exception.damagedRepair;

/**
 * 返修单不存在
 * @author Administrator
 *
 */
public class NoExistDamagedRepairOrderException extends Exception {
	
	public NoExistDamagedRepairOrderException() 
	{
		super();
	}
	
	public NoExistDamagedRepairOrderException(String inMessage)
	{
		super(inMessage);
	}
}
