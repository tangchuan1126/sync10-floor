package com.cwc.app.exception.damagedRepair;

public class NoExistDamagedRepairOrderDetailsException extends Exception {
	
	public NoExistDamagedRepairOrderDetailsException() 
	{
		super();
	}
	
	public NoExistDamagedRepairOrderDetailsException(String inMessage)
	{
		super(inMessage);
	}
}
