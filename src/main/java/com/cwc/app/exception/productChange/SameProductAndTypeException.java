package com.cwc.app.exception.productChange;

/**
 *同一商品和同一类型操作
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class SameProductAndTypeException extends Exception 
{
	public SameProductAndTypeException() 
	{
		super();
	}
	
	public SameProductAndTypeException(String inMessage)
	{
		super(inMessage);
	}
}
