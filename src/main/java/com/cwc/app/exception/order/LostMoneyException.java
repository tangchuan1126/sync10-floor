package com.cwc.app.exception.order;

/**
 * 计算订单成本：亏本
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class LostMoneyException extends Exception 
{
	public LostMoneyException() 
	{
		super();
	}
	
	public LostMoneyException(String inMessage)
	{
		super(inMessage);
	}
}
