package com.cwc.app.exception.order;

/**
 * 合并订单，订单状态不正确
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class MergeOrderStatusIncorrectException extends Exception 
{
	public MergeOrderStatusIncorrectException() 
	{
		super();
	}
	
	public MergeOrderStatusIncorrectException(String inMessage)
	{
		super(inMessage);
	}
}
