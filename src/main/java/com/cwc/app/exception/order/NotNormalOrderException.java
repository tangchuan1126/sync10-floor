package com.cwc.app.exception.order;

/**
 * 非正常状态订单
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class NotNormalOrderException extends Exception 
{
	public NotNormalOrderException() 
	{
		super();
	}
	
	public NotNormalOrderException(String inMessage)
	{
		super(inMessage);
	}
}
