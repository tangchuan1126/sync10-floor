package com.cwc.app.exception.order;

/**
 * 合并订单，不能取消
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class MergeOrderNotCancelException extends Exception 
{
	public MergeOrderNotCancelException() 
	{
		super();
	}
	
	public MergeOrderNotCancelException(String inMessage)
	{
		super(inMessage);
	}
}
