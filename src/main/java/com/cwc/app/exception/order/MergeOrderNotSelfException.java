package com.cwc.app.exception.order;

/**
 * 合并订单，父订单不能是自己
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class MergeOrderNotSelfException extends Exception 
{
	public MergeOrderNotSelfException() 
	{
		super();
	}
	
	public MergeOrderNotSelfException(String inMessage)
	{
		super(inMessage);
	}
}
