package com.cwc.app.exception.order;

/**
 * 汇率不存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class CurrencyNotExistException extends Exception 
{
	public CurrencyNotExistException() 
	{
		super();
	}
	
	public CurrencyNotExistException(String inMessage)
	{
		super(inMessage);
	}
}
