package com.cwc.app.exception.order;

/**
 * 递送人信息已经被使用
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class DeliveryInfoBeUsedException extends Exception 
{
	public DeliveryInfoBeUsedException() 
	{
		super();
	}
	
	public DeliveryInfoBeUsedException(String inMessage)
	{
		super(inMessage);
	}
}
