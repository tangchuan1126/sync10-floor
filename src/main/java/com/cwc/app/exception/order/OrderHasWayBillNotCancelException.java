package com.cwc.app.exception.order;

/**
 * 合并订单，不能取消
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class OrderHasWayBillNotCancelException extends Exception 
{
	public OrderHasWayBillNotCancelException() 
	{
		super();
	}
	
	public OrderHasWayBillNotCancelException(String inMessage)
	{
		super(inMessage);
	}
}
