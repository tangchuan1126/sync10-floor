package com.cwc.app.exception.order;

/**
 * 发货仓库不同，不能合并
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class MergeOrderStorageNotMatchException extends Exception 
{
	public MergeOrderStorageNotMatchException() 
	{
		super();
	}
	
	public MergeOrderStorageNotMatchException(String inMessage)
	{
		super(inMessage);
	}
}
