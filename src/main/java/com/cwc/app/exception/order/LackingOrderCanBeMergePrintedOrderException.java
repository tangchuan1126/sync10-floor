package com.cwc.app.exception.order;

/**
 * 不能把缺货订单合并到已打印订单上
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class LackingOrderCanBeMergePrintedOrderException extends Exception 
{
	public LackingOrderCanBeMergePrintedOrderException() 
	{
		super();
	}
	
	public LackingOrderCanBeMergePrintedOrderException(String inMessage)
	{
		super(inMessage);
	}
}
