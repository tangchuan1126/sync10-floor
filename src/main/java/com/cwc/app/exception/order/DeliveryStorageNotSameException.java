package com.cwc.app.exception.order;

/**
 * 发货仓库不一致
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class DeliveryStorageNotSameException extends Exception 
{
	public DeliveryStorageNotSameException() 
	{
		super();
	}
	
	public DeliveryStorageNotSameException(String inMessage)
	{
		super(inMessage);
	}
}
