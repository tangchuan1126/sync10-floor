package com.cwc.app.exception.order;

/**
 * 合并订单，父订单不存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class TxnIdExistException extends Exception 
{
	public TxnIdExistException() 
	{
		super();
	}
	
	public TxnIdExistException(String inMessage)
	{
		super(inMessage);
	}
}
