package com.cwc.app.exception.order;

/**
 * 合并订单，订单状态冲突
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class MergeOrderStatusConflictException extends Exception 
{
	public MergeOrderStatusConflictException() 
	{
		super();
	}
	
	public MergeOrderStatusConflictException(String inMessage)
	{
		super(inMessage);
	}
}
