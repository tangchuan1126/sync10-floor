package com.cwc.app.exception.order;

/**
 * 合并订单，父订单不存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class MergeOrderNotExistException extends Exception 
{
	public MergeOrderNotExistException() 
	{
		super();
	}
	
	public MergeOrderNotExistException(String inMessage)
	{
		super(inMessage);
	}
}
