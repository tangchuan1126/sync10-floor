package com.cwc.app.exception.order;

/**
 * 发票模板已经被订单使用
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class InvoiceTemplateBeUsedException extends Exception 
{
	public InvoiceTemplateBeUsedException() 
	{
		super();
	}
	
	public InvoiceTemplateBeUsedException(String inMessage)
	{
		super(inMessage);
	}
}
