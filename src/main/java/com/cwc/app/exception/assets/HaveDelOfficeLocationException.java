package com.cwc.app.exception.assets;

public class HaveDelOfficeLocationException extends Exception{
	
	public HaveDelOfficeLocationException()
	{
		super();
	}
	public HaveDelOfficeLocationException(String inMessage) {
		super(inMessage);
	}
}
