package com.cwc.app.exception.assets;

public class HaveAssetsCategoryException extends Exception{
	
	public HaveAssetsCategoryException(){
		super();
	}

	public HaveAssetsCategoryException(String inMessage){
		super(inMessage);
	}
}
