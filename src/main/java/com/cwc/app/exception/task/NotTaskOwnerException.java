package com.cwc.app.exception.task;

/**
 * 不是任务所有人
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class NotTaskOwnerException extends Exception 
{
	public NotTaskOwnerException() 
	{
		super();
	}
	
	public NotTaskOwnerException(String inMessage)
	{
		super(inMessage);
	}
}
