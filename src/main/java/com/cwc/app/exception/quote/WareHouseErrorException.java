package com.cwc.app.exception.quote;

/**
 * 导出的零售价发货仓库不正确
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class WareHouseErrorException extends Exception 
{
	public WareHouseErrorException() 
	{
		super();
	}
	
	public WareHouseErrorException(String inMessage)
	{
		super(inMessage);
	}
}
