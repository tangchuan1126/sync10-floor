package com.cwc.app.exception.quote;

/**
 * 成单订单不存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class OrderNotFoundException extends Exception 
{
	public OrderNotFoundException() 
	{
		super();
	}
	
	public OrderNotFoundException(String inMessage)
	{
		super(inMessage);
	}
}
