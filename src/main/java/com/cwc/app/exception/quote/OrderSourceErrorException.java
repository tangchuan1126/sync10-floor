package com.cwc.app.exception.quote;

/**
 * 成单订单来源不对，只允许directpay
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class OrderSourceErrorException extends Exception 
{
	public OrderSourceErrorException() 
	{
		super();
	}
	
	public OrderSourceErrorException(String inMessage)
	{
		super(inMessage);
	}
}
