package com.cwc.app.exception.quote;

/**
 * 报价单购物车异常
 * @author Administrator
 *
 */
public class CartQuoteErrorException extends Exception {
	
	public CartQuoteErrorException() 
	{
		super();
	}
	
	public CartQuoteErrorException(String inMessage)
	{
		super(inMessage);
	}
}
