package com.cwc.app.exception.quote;

/**
 * 
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ErrorWholeSellDiscountRuleException extends Exception 
{
	public ErrorWholeSellDiscountRuleException() 
	{
		super();
	}
	
	public ErrorWholeSellDiscountRuleException(String inMessage)
	{
		super(inMessage);
	}
}
