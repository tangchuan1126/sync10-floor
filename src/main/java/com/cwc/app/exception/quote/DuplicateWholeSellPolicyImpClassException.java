package com.cwc.app.exception.quote;

/**
 * 批发策略实现类重名
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class DuplicateWholeSellPolicyImpClassException extends Exception 
{
	public DuplicateWholeSellPolicyImpClassException() 
	{
		super();
	}
	
	public DuplicateWholeSellPolicyImpClassException(String inMessage)
	{
		super(inMessage);
	}
}
