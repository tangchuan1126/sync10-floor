package com.cwc.app.exception.quote;

/**
 * 成单订单已经被使用
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class DuplicateOrderException extends Exception 
{
	public DuplicateOrderException() 
	{
		super();
	}
	
	public DuplicateOrderException(String inMessage)
	{
		super(inMessage);
	}
}
