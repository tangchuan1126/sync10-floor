package com.cwc.app.exception.quote;

/**
 * 订单不是待抄单状态
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class OrderNotWait4RecordException extends Exception 
{
	public OrderNotWait4RecordException() 
	{
		super();
	}
	
	public OrderNotWait4RecordException(String inMessage)
	{
		super(inMessage);
	}
}
