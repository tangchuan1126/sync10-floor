package com.cwc.app.exception.quote;

/**
 * 成单订单总金额小于报价单
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class TotalMcGrossErrorException extends Exception 
{
	public TotalMcGrossErrorException() 
	{
		super();
	}
	
	public TotalMcGrossErrorException(String inMessage)
	{
		super(inMessage);
	}
}
