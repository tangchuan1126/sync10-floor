package com.cwc.app.exception.quote;

/**
 * 批发策略重名
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class DuplicateWholeSellPolicyNameException extends Exception 
{
	public DuplicateWholeSellPolicyNameException() 
	{
		super();
	}
	
	public DuplicateWholeSellPolicyNameException(String inMessage)
	{
		super(inMessage);
	}
}
