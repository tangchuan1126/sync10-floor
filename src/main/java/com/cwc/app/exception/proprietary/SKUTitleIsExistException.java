package com.cwc.app.exception.proprietary;

/**
 * 无法序列号预保留
 * @author Administrator
 *
 */
public class SKUTitleIsExistException extends Exception {
	
	public SKUTitleIsExistException() 
	{
		super();
	}
	
	public SKUTitleIsExistException(String inMessage)
	{
		super(inMessage);
	}
}
