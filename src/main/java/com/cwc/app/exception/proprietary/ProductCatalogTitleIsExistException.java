package com.cwc.app.exception.proprietary;

/**
 * 无法序列号预保留
 * @author Administrator
 *
 */
public class ProductCatalogTitleIsExistException extends Exception {
	
	public ProductCatalogTitleIsExistException() 
	{
		super();
	}
	
	public ProductCatalogTitleIsExistException(String inMessage)
	{
		super(inMessage);
	}
}
