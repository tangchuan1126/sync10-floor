package com.cwc.app.exception.proprietary;

/**
 * 无法序列号预保留
 * @author Administrator
 *
 */
public class TitleHasExistException extends Exception {
	
	public TitleHasExistException() 
	{
		super();
	}
	
	public TitleHasExistException(String inMessage)
	{
		super(inMessage);
	}
}
