package com.cwc.app.exception.admin;

/**
 * 角色已经存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class RoleIsExistException extends Exception 
{
	public RoleIsExistException() 
	{
		super();
	}
	
	public RoleIsExistException(String inMessage)
	{
		super(inMessage);
	}
}
