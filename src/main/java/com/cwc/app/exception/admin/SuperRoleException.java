package com.cwc.app.exception.admin;

/**
 * 超级管理员角色
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class SuperRoleException extends Exception 
{
	public SuperRoleException() 
	{
		super();
	}
	
	public SuperRoleException(String inMessage)
	{
		super(inMessage);
	}
}
