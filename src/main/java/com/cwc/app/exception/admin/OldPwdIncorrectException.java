package com.cwc.app.exception.admin;

/**
 * 旧密码不正确
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class OldPwdIncorrectException extends Exception 
{
	public OldPwdIncorrectException() 
	{
		super();
	}
	
	public OldPwdIncorrectException(String inMessage)
	{
		super(inMessage);
	}
}
