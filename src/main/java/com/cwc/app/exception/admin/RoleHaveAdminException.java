package com.cwc.app.exception.admin;

/**
 * 角色下有管理员帐号
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class RoleHaveAdminException extends Exception 
{
	public RoleHaveAdminException() 
	{
		super();
	}
	
	public RoleHaveAdminException(String inMessage)
	{
		super(inMessage);
	}
}
