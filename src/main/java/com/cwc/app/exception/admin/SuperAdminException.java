package com.cwc.app.exception.admin;

/**
 * 超级管理员帐号
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class SuperAdminException extends Exception 
{
	public SuperAdminException() 
	{
		super();
	}
	
	public SuperAdminException(String inMessage)
	{
		super(inMessage);
	}
}
