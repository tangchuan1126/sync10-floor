package com.cwc.app.exception.admin;

/**
 * 管理员帐号不允许登录
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class AccountNotPermitLoginException extends Exception 
{
	public AccountNotPermitLoginException() 
	{
		super();
	}
	
	public AccountNotPermitLoginException(String inMessage)
	{
		super(inMessage);
	}
}
