package com.cwc.app.exception.admin;

public class AdminNotExistException extends Exception{

	public AdminNotExistException(){
		super();
	}
	public AdminNotExistException(String msg){
		super(msg);
	}
	
}
