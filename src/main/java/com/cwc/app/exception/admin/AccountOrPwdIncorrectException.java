package com.cwc.app.exception.admin;

/**
 * 帐号或密码不对
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class AccountOrPwdIncorrectException extends Exception 
{
	public AccountOrPwdIncorrectException() 
	{
		super();
	}
	
	public AccountOrPwdIncorrectException(String inMessage)
	{
		super(inMessage);
	}
}
