package com.cwc.app.exception.admin;

/**
 * 验证码不正确
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class VerifyCodeIncorrectException extends Exception 
{
	public VerifyCodeIncorrectException() 
	{
		super();
	}
	
	public VerifyCodeIncorrectException(String inMessage)
	{
		super(inMessage);
	}
}
