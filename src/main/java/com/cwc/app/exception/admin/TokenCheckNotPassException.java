package com.cwc.app.exception.admin;

/**
 * 令牌验证未通过
 * @author Administrator
 *
 */
public class TokenCheckNotPassException extends Exception{
	
	public TokenCheckNotPassException(){
		super();
	}
	
	public TokenCheckNotPassException(String msg){
		super(msg);
	}

}
