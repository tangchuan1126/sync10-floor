package com.cwc.app.exception.admin;

/**
 * 管理员帐号已经存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class AdminIsExistException extends Exception 
{
	public AdminIsExistException() 
	{
		super();
	}
	
	public AdminIsExistException(String inMessage)
	{
		super(inMessage);
	}
}
