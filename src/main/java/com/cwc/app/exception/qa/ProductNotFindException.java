package com.cwc.app.exception.qa;

public class ProductNotFindException extends Exception {
	
	public ProductNotFindException() 
	{
		super();
	}
	
	public ProductNotFindException(String inMessage)
	{
		super(inMessage);
	}
}
