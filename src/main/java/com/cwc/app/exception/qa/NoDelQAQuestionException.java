package com.cwc.app.exception.qa;
/**
 * 无权限删除问题
 * @author Administrator
 *
 */
public class NoDelQAQuestionException extends Exception 
{
	public NoDelQAQuestionException() 
	{
		super();
	}
	
	public NoDelQAQuestionException(String inMessage)
	{
		super(inMessage);
	}
}
