package com.cwc.app.exception.qa;
/**
 * 不是发问人，不能修改
 * @author Administrator
 *
 */
public class ModQAQuestionNotQuestionerException extends Exception 
{
	public ModQAQuestionNotQuestionerException() 
	{
		super();
	}
	
	public ModQAQuestionNotQuestionerException(String inMessage)
	{
		super(inMessage);
	}
}
