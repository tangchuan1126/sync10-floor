package com.cwc.app.exception.serialnumber;

/**
 * 该序列号在仓库（序列号重复）
 * @author Administrator
 *
 */
public class SerialNumberAlreadyOutStoreException extends Exception {
	public SerialNumberAlreadyOutStoreException() 
	{
		super();
	}
	
	public SerialNumberAlreadyOutStoreException(String inMessage)
	{
		super(inMessage);
	}
}
