package com.cwc.app.exception.serialnumber;

/**
 * 该序列号在仓库（序列号重复）
 * @author Administrator
 *
 */
public class SerialNumberAlreadyInStoreException extends Exception {
	public SerialNumberAlreadyInStoreException() 
	{
		super();
	}
	
	public SerialNumberAlreadyInStoreException(String inMessage)
	{
		super(inMessage);
	}
}
