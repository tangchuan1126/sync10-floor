package com.cwc.app.exception.serialnumber;

/**
 * 序列号超过最大限额
 * @author Administrator
 *
 */
public class NumberIndexOfException extends Exception {
	public NumberIndexOfException() 
	{
		super();
	}
	
	public NumberIndexOfException(String inMessage)
	{
		super(inMessage);
	}
}
