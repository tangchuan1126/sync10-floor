package com.cwc.app.exception.storageApprove;

public class HasWaitApproveAreaException extends Exception 
{
	public HasWaitApproveAreaException()
	{
		super();
	}

	public HasWaitApproveAreaException(String inMessage)
	{
		super(inMessage);
	}
}
