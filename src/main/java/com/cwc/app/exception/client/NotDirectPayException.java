package com.cwc.app.exception.client;

/**
 * 不是直接支付
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class NotDirectPayException extends Exception 
{
	public NotDirectPayException() 
	{
		super();
	}
	
	public NotDirectPayException(String inMessage)
	{
		super(inMessage);
	}
}
