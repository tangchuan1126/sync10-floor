package com.cwc.app.exception.client;

/**
 * 订单已经被成单
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class OrderNotExistException extends Exception 
{
	public OrderNotExistException() 
	{
		super();
	}
	
	public OrderNotExistException(String inMessage)
	{
		super(inMessage);
	}
}
