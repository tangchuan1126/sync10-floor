package com.cwc.app.exception.client;

import com.cwc.db.DBRow;

/**
 * 被成单的订单不存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class MarkOrderIsExistException extends Exception 
{
	private DBRow result;
	
	public MarkOrderIsExistException() 
	{
		super();
	}
	
	public MarkOrderIsExistException(String inMessage)
	{
		super(inMessage);
	}
	
	public MarkOrderIsExistException(DBRow result) 
	{
		super();
		
		this.result = result;
	}
	
	public DBRow getResult()
	{
		return(result);
	}
}
