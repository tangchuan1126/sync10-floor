package com.cwc.app.exception.client;

/**
 * 客户已经存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ClientIsExistException extends Exception 
{
	public ClientIsExistException() 
	{
		super();
	}
	
	public ClientIsExistException(String inMessage)
	{
		super(inMessage);
	}
}
