package com.cwc.app.exception.product;

/**
 * 品名已经存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ProductNameIsExistException extends Exception 
{
	public ProductNameIsExistException() 
	{
		super();
	}
	
	public ProductNameIsExistException(String inMessage)
	{
		super(inMessage);
	}
}
