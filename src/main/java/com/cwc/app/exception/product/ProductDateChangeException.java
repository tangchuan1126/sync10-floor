package com.cwc.app.exception.product;

/**
 * 商品数据变动导致导入审核错误
 * @author Administrator
 *
 */
public class ProductDateChangeException extends Exception {
	
	public ProductDateChangeException() 
	{
		super();
	}
	
	public ProductDateChangeException(String inMessage)
	{
		super(inMessage);
	}
}
