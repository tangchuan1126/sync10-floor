package com.cwc.app.exception.product;

/**
 * 条码已经存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ProductCodeIsExistException extends Exception 
{
	public ProductCodeIsExistException() 
	{
		super();
	}
	
	public ProductCodeIsExistException(String inMessage)
	{
		super(inMessage);
	}
}
