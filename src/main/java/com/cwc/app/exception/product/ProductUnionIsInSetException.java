package com.cwc.app.exception.product;

/**
 * 商品已经存在于组合套装（同一个父亲下，商品不能重复）
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ProductUnionIsInSetException extends Exception 
{
	public ProductUnionIsInSetException() 
	{
		super();
	}
	
	public ProductUnionIsInSetException(String inMessage)
	{
		super(inMessage);
	}
}
