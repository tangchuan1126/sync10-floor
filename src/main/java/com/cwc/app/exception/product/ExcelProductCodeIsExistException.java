package com.cwc.app.exception.product;

public class ExcelProductCodeIsExistException extends Exception {
	
	public ExcelProductCodeIsExistException() 
	{
		super();
	}
	
	public ExcelProductCodeIsExistException(String inMessage)
	{
		super(inMessage);
	}
}
