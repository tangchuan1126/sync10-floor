package com.cwc.app.exception.product;

/**
 * 组合套装不能作为商品
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ProductUnionSetCanBeProductException extends Exception 
{
	public ProductUnionSetCanBeProductException() 
	{
		super();
	}
	
	public ProductUnionSetCanBeProductException(String inMessage)
	{
		super(inMessage);
	}
}
