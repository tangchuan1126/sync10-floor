package com.cwc.app.exception.product;

/**
 * 商品类型不同，不能转换
 * 比如：不能把套装转换成普通商品，反之也不行
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ConvertNotSameTypeException extends Exception 
{
	public ConvertNotSameTypeException() 
	{
		super();
	}
	
	public ConvertNotSameTypeException(String inMessage)
	{
		super(inMessage);
	}
}
