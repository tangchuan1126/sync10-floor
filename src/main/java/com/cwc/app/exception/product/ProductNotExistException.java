package com.cwc.app.exception.product;

/**
 * 商品不存在（创建组合关系使用）
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ProductNotExistException extends Exception 
{
	public ProductNotExistException() 
	{
		super();
	}
	
	public ProductNotExistException(String inMessage)
	{
		super(inMessage);
	}
}
