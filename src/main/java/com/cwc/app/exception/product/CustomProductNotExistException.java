package com.cwc.app.exception.product;

/**
 * 定制商品不存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class CustomProductNotExistException extends Exception 
{
	public CustomProductNotExistException() 
	{
		super();
	}
	
	public CustomProductNotExistException(String inMessage)
	{
		super(inMessage);
	}
}
