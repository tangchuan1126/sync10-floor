package com.cwc.app.exception.product;

/**
 * 转换商品不能转给自己
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ConvertNotBeSelfException extends Exception 
{
	public ConvertNotBeSelfException() 
	{
		super();
	}
	
	public ConvertNotBeSelfException(String inMessage)
	{
		super(inMessage);
	}
}
