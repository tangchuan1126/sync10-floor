package com.cwc.app.exception.product;
/**
 * 上传商品文件内，商品名重复
 * @author Administrator
 *
 */
public class ExcelProductNameIsExistException extends Exception 
{
	public ExcelProductNameIsExistException() 
	{
		super();
	}
	
	public ExcelProductNameIsExistException(String inMessage)
	{
		super(inMessage);
	}
}
