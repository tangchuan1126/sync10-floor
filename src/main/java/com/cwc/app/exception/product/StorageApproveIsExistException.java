package com.cwc.app.exception.product;

/**
 * 存在未审核库存差异
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class StorageApproveIsExistException extends Exception 
{
	public StorageApproveIsExistException() 
	{
		super();
	}
	
	public StorageApproveIsExistException(String inMessage)
	{
		super(inMessage);
	}
}
