package com.cwc.app.exception.product;

/**
 * 商品已经进库
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ProductIsInStorageException extends Exception 
{
	public ProductIsInStorageException() 
	{
		super();
	}

	public ProductIsInStorageException(String inMessage)
	{
		super(inMessage);
	}
}
