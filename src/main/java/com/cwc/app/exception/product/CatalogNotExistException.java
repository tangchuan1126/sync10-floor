package com.cwc.app.exception.product;

public class CatalogNotExistException extends Exception {
	
	public CatalogNotExistException() 
	{
		super();
	}
	
	public CatalogNotExistException(String inMessage)
	{
		super(inMessage);
	}
}
