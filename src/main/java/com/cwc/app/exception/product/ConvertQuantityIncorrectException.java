package com.cwc.app.exception.product;

/**
 * 转换商品库存数不正确
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ConvertQuantityIncorrectException extends Exception 
{
	public ConvertQuantityIncorrectException() 
	{
		super();
	}
	
	public ConvertQuantityIncorrectException(String inMessage)
	{
		super(inMessage);
	}
}
