package com.cwc.app.exception.product;

public class LEVEL2ToLEVEL1ParentCanNotDefineException extends Exception {
	
	public LEVEL2ToLEVEL1ParentCanNotDefineException() 
	{
		super();
	}
	
	public LEVEL2ToLEVEL1ParentCanNotDefineException(String inMessage)
	{
		super(inMessage);
	}
}
