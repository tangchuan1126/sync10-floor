package com.cwc.app.exception.product;
/**
 * 商品数据错误（长宽高为0）
 * @author Administrator
 *
 */
public class ProductDataErrorException extends Exception {
	
	public ProductDataErrorException() 
	{
		super();
	}
	
	public ProductDataErrorException(String inMessage)
	{
		super(inMessage);
	}
}
