package com.cwc.app.exception.product;

/**
 * UPC与商品条码重复
 * @author Administrator
 *
 */
public class UPCExistException extends Exception {
	
	public UPCExistException() 
	{
		super();
	}
	
	public UPCExistException(String inMessage)
	{
		super(inMessage);
	}
}
