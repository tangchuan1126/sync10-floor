package com.cwc.app.exception.product;

/**
 * 商品一件跟库存关联
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ProductHasRelationStorageException extends Exception 
{
	public ProductHasRelationStorageException() 
	{
		super();
	}
	
	public ProductHasRelationStorageException(String inMessage)
	{
		super(inMessage);
	}
}
