package com.cwc.app.exception.product;

public class CatalogNotExistProductException extends Exception {
	
	public CatalogNotExistProductException() 
	{
		super();
	}
	
	public CatalogNotExistProductException(String inMessage)
	{
		super(inMessage);
	}
}
