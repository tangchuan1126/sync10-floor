package com.cwc.app.exception.product;

/**
 * 商品没有建库
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ProductNotCreateStorageException extends Exception 
{
	public ProductNotCreateStorageException() 
	{
		super();
	}
	
	public ProductNotCreateStorageException(String inMessage)
	{
		super(inMessage);
	}
}
