package com.cwc.app.exception.product;

/**
 * 商品没有设置毛利
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ProductNotProfitException extends Exception 
{
	public ProductNotProfitException() 
	{
		super();
	}
	
	public ProductNotProfitException(String inMessage)
	{
		super(inMessage);
	}
}
