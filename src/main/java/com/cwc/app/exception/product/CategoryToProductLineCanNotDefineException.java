package com.cwc.app.exception.product;

public class CategoryToProductLineCanNotDefineException extends Exception {
	
	public CategoryToProductLineCanNotDefineException() 
	{
		super();
	}
	
	public CategoryToProductLineCanNotDefineException(String inMessage)
	{
		super(inMessage);
	}
}
