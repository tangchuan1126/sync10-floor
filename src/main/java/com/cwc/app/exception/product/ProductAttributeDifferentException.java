package com.cwc.app.exception.product;

public class ProductAttributeDifferentException extends Exception {
	
	public ProductAttributeDifferentException() 
	{
		super();
	}
	
	public ProductAttributeDifferentException(String inMessage)
	{
		super(inMessage);
	}
}
