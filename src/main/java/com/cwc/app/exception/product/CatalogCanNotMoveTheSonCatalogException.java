package com.cwc.app.exception.product;

public class CatalogCanNotMoveTheSonCatalogException extends Exception {
	
	public CatalogCanNotMoveTheSonCatalogException() 
	{
		super();
	}
	
	public CatalogCanNotMoveTheSonCatalogException(String inMessage)
	{
		super(inMessage);
	}
}
