package com.cwc.app.exception.product;

public class LEVEL3ToLEVEL2ParentCanNotDefineException extends Exception {
	
	public LEVEL3ToLEVEL2ParentCanNotDefineException() 
	{
		super();
	}
	
	public LEVEL3ToLEVEL2ParentCanNotDefineException(String inMessage)
	{
		super(inMessage);
	}
}
