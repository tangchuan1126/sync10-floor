package com.cwc.app.exception.product;

/**
 * 已经是组合中的商品，不能在其下添加商品，变成组合
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ProductCantBeSetException extends Exception 
{
	public ProductCantBeSetException() 
	{
		super();
	}
	
	public ProductCantBeSetException(String inMessage)
	{
		super(inMessage);
	}
}
