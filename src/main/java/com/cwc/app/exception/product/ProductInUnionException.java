package com.cwc.app.exception.product;

/**
 * 商品有组合关系，不能删除
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ProductInUnionException extends Exception 
{
	public ProductInUnionException() 
	{
		super();
	}
	
	public ProductInUnionException(String inMessage)
	{
		super(inMessage);
	}
}
