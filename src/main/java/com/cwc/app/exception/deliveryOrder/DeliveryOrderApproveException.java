package com.cwc.app.exception.deliveryOrder;
/**
 * 交货单已入库了
 * @author Administrator
 *
 */
public class DeliveryOrderApproveException extends Exception 
{
	public DeliveryOrderApproveException() 
	{
		super();
	}
	
	public DeliveryOrderApproveException(String inMessage)
	{
		super(inMessage);
	}
}
