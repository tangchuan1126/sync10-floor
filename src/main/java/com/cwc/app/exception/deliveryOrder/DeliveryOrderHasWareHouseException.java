package com.cwc.app.exception.deliveryOrder;
/**
 * 交货单已入库了
 * @author Administrator
 *
 */
public class DeliveryOrderHasWareHouseException extends Exception 
{
	public DeliveryOrderHasWareHouseException() 
	{
		super();
	}
	
	public DeliveryOrderHasWareHouseException(String inMessage)
	{
		super(inMessage);
	}
}
