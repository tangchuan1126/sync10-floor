package com.cwc.app.exception.deliveryOrder;

/**
 * 交货单商品重复
 * @author Administrator
 *
 */
public class RepeatProductException extends Exception 
{
	public RepeatProductException() 
	{
		super();
	}
	
	public RepeatProductException(String inMessage)
	{
		super(inMessage);
	}
}
