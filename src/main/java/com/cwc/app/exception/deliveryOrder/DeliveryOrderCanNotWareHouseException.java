package com.cwc.app.exception.deliveryOrder;
/**
 * 交货单不可入库
 * @author Administrator
 *
 */
public class DeliveryOrderCanNotWareHouseException extends Exception 
{
	public DeliveryOrderCanNotWareHouseException() 
	{
		super();
	}
	
	public DeliveryOrderCanNotWareHouseException(String inMessage)
	{
		super(inMessage);
	}
}
