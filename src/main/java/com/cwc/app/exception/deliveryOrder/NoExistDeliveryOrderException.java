package com.cwc.app.exception.deliveryOrder;

/**
 * 交货单不存在
 * @author Administrator
 *
 */
public class NoExistDeliveryOrderException extends Exception 
{
	public NoExistDeliveryOrderException() 
	{
		super();
	}
	
	public NoExistDeliveryOrderException(String inMessage)
	{
		super(inMessage);
	}
}
