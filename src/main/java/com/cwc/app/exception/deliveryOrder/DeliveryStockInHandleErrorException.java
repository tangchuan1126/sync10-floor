package com.cwc.app.exception.deliveryOrder;

/**
 * 订单并非已打印
 * @author Administrator
 *
 */
public class DeliveryStockInHandleErrorException extends Exception {
	
	public DeliveryStockInHandleErrorException() 
	{
		super();
	}
	
	public DeliveryStockInHandleErrorException(String inMessage)
	{
		super(inMessage);
	}
}
