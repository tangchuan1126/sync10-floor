package com.cwc.app.exception.express;

/**
 * 不匹配重量段
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class WeightOutSizeException extends Exception 
{
	public WeightOutSizeException() 
	{
		super();
	}
	
	public WeightOutSizeException(String inMessage)
	{
		super(inMessage);
	}
}
