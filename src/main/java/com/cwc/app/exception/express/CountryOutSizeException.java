package com.cwc.app.exception.express;

/**
 * 国家不再发货范围
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class CountryOutSizeException extends Exception 
{
	public CountryOutSizeException() 
	{
		super();
	}
	
	public CountryOutSizeException(String inMessage)
	{
		super(inMessage);
	}
}
