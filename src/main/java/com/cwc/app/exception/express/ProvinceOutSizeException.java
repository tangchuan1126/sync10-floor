package com.cwc.app.exception.express;

/**
 * 省份不再发货范围
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class ProvinceOutSizeException extends Exception 
{
	public ProvinceOutSizeException() 
	{
		super();
	}
	
	public ProvinceOutSizeException(String inMessage)
	{
		super(inMessage);
	}
}
