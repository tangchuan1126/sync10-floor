package com.cwc.app.exception.express;

/**
 * 重量段交叉
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class WeightCrossException extends Exception 
{
	public WeightCrossException() 
	{
		super();
	}
	
	public WeightCrossException(String inMessage)
	{
		super(inMessage);
	}
}
