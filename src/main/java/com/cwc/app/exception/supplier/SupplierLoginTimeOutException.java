package com.cwc.app.exception.supplier;

/**
 * 供应商登录超时（登录bean中附加信息为空）
 * @author Administrator
 *
 */
public class SupplierLoginTimeOutException extends Exception 
{
	public SupplierLoginTimeOutException()
	{
		super();
	}

	public SupplierLoginTimeOutException(String inMessage)
	{
		super(inMessage);
	}
}
