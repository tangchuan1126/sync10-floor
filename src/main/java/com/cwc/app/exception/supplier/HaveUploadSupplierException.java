package com.cwc.app.exception.supplier;

public class HaveUploadSupplierException extends Exception{
	
	public HaveUploadSupplierException()
	{
		super();
	}

	public HaveUploadSupplierException(String inMessage){
		super(inMessage);
	}
}
