package com.cwc.app.exception.email;

/**
 * 同名和同收款人模板已经存在
 * @author Administrator
 *
 */
public class SameEmailTemplateException extends Exception{
	
	public SameEmailTemplateException(){
		super();
	}

	public SameEmailTemplateException(String inMessage){
		super(inMessage);
	}
}
