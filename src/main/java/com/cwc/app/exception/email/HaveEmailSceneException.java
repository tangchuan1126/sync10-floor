package com.cwc.app.exception.email;

public class HaveEmailSceneException extends Exception{
	
	public HaveEmailSceneException(){
		super();
	}

	public HaveEmailSceneException(String inMessage){
		super(inMessage);
	}
}
