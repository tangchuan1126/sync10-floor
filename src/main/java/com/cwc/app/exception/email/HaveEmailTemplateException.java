package com.cwc.app.exception.email;

/**
 * 
 * @author Administrator
 *
 */
public class HaveEmailTemplateException extends Exception{
	
	public HaveEmailTemplateException(){
		super();
	}

	public HaveEmailTemplateException(String inMessage){
		super(inMessage);
	}
}
