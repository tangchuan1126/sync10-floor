package com.cwc.app.exception.waybill;

public class WayBillOrderPrintedException extends Exception {
	
	public WayBillOrderPrintedException()
	{
		super();
	}

	public WayBillOrderPrintedException(String inMessage){
		super(inMessage);
	}
}
