package com.cwc.app.exception.waybill;

/**
 * 订单明细数量错误
 * @author Administrator
 *
 */
public class OrderItemWaitErrorException extends Exception {
	
	public OrderItemWaitErrorException()
	{
		super();
	}

	public OrderItemWaitErrorException(String inMessage){
		super(inMessage);
	}
}
