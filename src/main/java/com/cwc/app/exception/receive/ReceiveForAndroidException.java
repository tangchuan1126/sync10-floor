package com.cwc.app.exception.receive;


/**
 * @author 	Zhengziqi
 * 2015年4月11日
 *
 */
public class ReceiveForAndroidException extends RuntimeException{
	
	private static final long serialVersionUID = 7291855411593904320L;

	public ReceiveForAndroidException(String message){
	        super(message);
    }

}
