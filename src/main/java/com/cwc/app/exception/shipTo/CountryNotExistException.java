package com.cwc.app.exception.shipTo;

public class CountryNotExistException extends Exception 
{
	public CountryNotExistException()
	{
		super();
	}

	public CountryNotExistException(String inMessage)
	{
		super(inMessage);
	}
}
