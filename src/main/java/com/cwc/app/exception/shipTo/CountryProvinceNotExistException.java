package com.cwc.app.exception.shipTo;

public class CountryProvinceNotExistException extends Exception 
{
	public CountryProvinceNotExistException()
	{
		super();
	}

	public CountryProvinceNotExistException(String inMessage)
	{
		super(inMessage);
	}
}
