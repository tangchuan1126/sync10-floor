package com.cwc.app.exception.shipTo;

public class ShipToNotExistException extends Exception 
{
	public ShipToNotExistException()
	{
		super();
	}

	public ShipToNotExistException(String inMessage)
	{
		super(inMessage);
	}
}
