package com.cwc.app.exception.shipTo;

public class ShipToHasExistException extends Exception 
{
	public ShipToHasExistException()
	{
		super();
	}

	public ShipToHasExistException(String inMessage)
	{
		super(inMessage);
	}
}
