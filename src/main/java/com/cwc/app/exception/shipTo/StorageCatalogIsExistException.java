package com.cwc.app.exception.shipTo;

public class StorageCatalogIsExistException extends Exception 
{
	public StorageCatalogIsExistException()
	{
		super();
	}

	public StorageCatalogIsExistException(String inMessage)
	{
		super(inMessage);
	}
}
