package com.cwc.app.exception.schedule;

public class ScheduleGenerateException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ScheduleGenerateException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ScheduleGenerateException(String message, Throwable cause) {
		super(message, cause);
 	}
}
