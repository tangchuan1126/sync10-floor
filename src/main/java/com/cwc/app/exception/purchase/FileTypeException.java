package com.cwc.app.exception.purchase;

/**
 * 采购单文件上传格式不对
 * @author Administrator
 *
 */
public class FileTypeException extends Exception {
	public FileTypeException() 
	{
		super();
	}
	
	public FileTypeException(String inMessage)
	{
		super(inMessage);
	}
}
