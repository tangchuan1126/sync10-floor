package com.cwc.app.exception.purchase;

/**
 * 采购单内已有商品
 * @author Administrator
 *
 */
public class PurchaseDetailRepartException extends Exception {
	public PurchaseDetailRepartException() 
	{
		super();
	}
	
	public PurchaseDetailRepartException(String inMessage)
	{
		super(inMessage);
	}
}
