package com.cwc.app.exception.purchase;

/**
 * 采购单内无商品
 * @author Administrator
 *
 */
public class NullPurchaseException extends Exception {
	public NullPurchaseException() 
	{
		super();
	}
	
	public NullPurchaseException(String inMessage)
	{
		super(inMessage);
	}
}
