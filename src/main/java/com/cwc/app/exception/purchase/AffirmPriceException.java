package com.cwc.app.exception.purchase;

/**
 * 采购单详细价格无法确认
 * @author Administrator
 *
 */
public class AffirmPriceException extends Exception {
	public AffirmPriceException() 
	{
		super();
	}
	
	public AffirmPriceException(String inMessage)
	{
		super(inMessage);
	}
}
