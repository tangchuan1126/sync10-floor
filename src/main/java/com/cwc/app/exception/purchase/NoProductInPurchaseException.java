package com.cwc.app.exception.purchase;

/**
 * 有采购单内没有商品
 * @author Administrator
 *
 */
public class NoProductInPurchaseException extends Exception {
	public NoProductInPurchaseException() 
	{
		super();
	}
	
	public NoProductInPurchaseException(String inMessage)
	{
		super(inMessage);
	}
}
