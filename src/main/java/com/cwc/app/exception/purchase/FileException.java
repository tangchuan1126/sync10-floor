package com.cwc.app.exception.purchase;

/**
 * 采购单上传文件不对
 * @author Administrator
 *
 */
public class FileException extends Exception {
	public FileException() 
	{
		super();
	}
	
	public FileException(String inMessage)
	{
		super(inMessage);
	}
}
