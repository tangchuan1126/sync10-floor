package com.cwc.app.exception.purchase;

/**
 * 条码机输入采购单不存在
 * @author Administrator
 *
 */
public class NoPurchaseException extends Exception {
	public NoPurchaseException() 
	{
		super();
	}
	
	public NoPurchaseException(String inMessage)
	{
		super(inMessage);
	}
}
