package com.cwc.app.exception.purchase;

/**
 * 采购单不可转账
 * @author Administrator
 *
 */
public class TransferException extends Exception {
	public TransferException() 
	{
		super();
	}
	
	public TransferException(String inMessage)
	{
		super(inMessage);
	}
}
