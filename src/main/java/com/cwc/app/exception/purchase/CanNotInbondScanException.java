package com.cwc.app.exception.purchase;

/**
 * 采购不是可入库状态
 * @author Administrator
 *
 */
public class CanNotInbondScanException extends Exception {
	public CanNotInbondScanException() 
	{
		super();
	}
	
	public CanNotInbondScanException(String inMessage)
	{
		super(inMessage);
	}
}
