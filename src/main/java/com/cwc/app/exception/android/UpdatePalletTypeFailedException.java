package com.cwc.app.exception.android;

public class UpdatePalletTypeFailedException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UpdatePalletTypeFailedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UpdatePalletTypeFailedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	
}
