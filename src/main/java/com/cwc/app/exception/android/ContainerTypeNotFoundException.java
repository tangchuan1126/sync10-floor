package com.cwc.app.exception.android;

public class ContainerTypeNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ContainerTypeNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ContainerTypeNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	
}
