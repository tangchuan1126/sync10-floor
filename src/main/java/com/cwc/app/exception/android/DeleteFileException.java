package com.cwc.app.exception.android;

public class DeleteFileException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DeleteFileException() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public DeleteFileException(String message, Throwable cause) {
		super(message, cause);
 	}

	
}
