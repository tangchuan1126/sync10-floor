package com.cwc.app.exception.android;

public class PrintServerNoPrintException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PrintServerNoPrintException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PrintServerNoPrintException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	
}
