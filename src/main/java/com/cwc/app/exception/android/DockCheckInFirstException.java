package com.cwc.app.exception.android;

public class DockCheckInFirstException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DockCheckInFirstException() {
		super();
 	}

	public DockCheckInFirstException(String message, Throwable cause) {
		super(message, cause);
 	}

	
}
