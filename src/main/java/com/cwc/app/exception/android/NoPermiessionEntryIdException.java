package com.cwc.app.exception.android;

public class NoPermiessionEntryIdException extends Exception{

	private static final long serialVersionUID = 1L;
	 
	 
	public NoPermiessionEntryIdException() {
		super();
 	}

	public NoPermiessionEntryIdException(String message, Throwable cause) {
		super(message, cause);
 	}

}
