package com.cwc.app.exception.android;

public class LoadIsCloseException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LoadIsCloseException() {
		super();
 	}

	public LoadIsCloseException(String message, Throwable cause) {
		super(message, cause);
 	}

	
}
