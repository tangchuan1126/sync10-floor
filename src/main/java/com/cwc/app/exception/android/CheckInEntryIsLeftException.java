package com.cwc.app.exception.android;

public class CheckInEntryIsLeftException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CheckInEntryIsLeftException() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public CheckInEntryIsLeftException(String message, Throwable cause) {
		super(message, cause);
 	}

	
}
