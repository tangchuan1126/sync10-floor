package com.cwc.app.exception.android;

public class ProductPictureDeleteException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProductPictureDeleteException() {
		super();
 	}

	public ProductPictureDeleteException(String message, Throwable cause) {
		super(message, cause);
 	}

	
}
