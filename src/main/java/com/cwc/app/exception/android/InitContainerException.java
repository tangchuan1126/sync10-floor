package com.cwc.app.exception.android;

public class InitContainerException extends RuntimeException{
 
	private static final long serialVersionUID = 1L;
 
 
	public InitContainerException() {
		super();
 	}

	public InitContainerException(String message, Throwable cause) {
		super(message, cause);
 	}

}
