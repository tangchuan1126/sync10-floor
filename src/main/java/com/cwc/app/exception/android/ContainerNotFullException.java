package com.cwc.app.exception.android;

public class ContainerNotFullException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ContainerNotFullException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ContainerNotFullException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	
}
