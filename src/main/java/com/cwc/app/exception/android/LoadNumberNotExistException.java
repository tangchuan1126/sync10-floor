package com.cwc.app.exception.android;

public class LoadNumberNotExistException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LoadNumberNotExistException() {
		super();
 	}

	public LoadNumberNotExistException(String message, Throwable cause) {
		super(message, cause);
 	}

	
}
