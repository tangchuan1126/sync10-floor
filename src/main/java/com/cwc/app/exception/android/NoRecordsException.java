package com.cwc.app.exception.android;

public class NoRecordsException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoRecordsException() {
		super();
 	}

	public NoRecordsException(String message, Throwable cause) {
		super(message, cause);
 	}

	
}
