package com.cwc.app.exception.android;

public class TempContainerCreateFailedException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TempContainerCreateFailedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TempContainerCreateFailedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	
}
