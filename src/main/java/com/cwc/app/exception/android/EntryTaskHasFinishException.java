package com.cwc.app.exception.android;

public class EntryTaskHasFinishException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EntryTaskHasFinishException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EntryTaskHasFinishException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	
}
