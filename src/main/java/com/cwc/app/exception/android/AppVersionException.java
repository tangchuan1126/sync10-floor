package com.cwc.app.exception.android;

public class AppVersionException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AppVersionException() {
		super();
 	}

	public AppVersionException(String message, Throwable cause) {
		super(message, cause);
 	}

	
}
