package com.cwc.app.exception.android;

public class DeletePalletNoFailedException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DeletePalletNoFailedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DeletePalletNoFailedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	
}
