package com.cwc.app.exception.android;

public class UserIsOnLineCantLoginException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserIsOnLineCantLoginException() {
		super();
 	}

	public UserIsOnLineCantLoginException(String message, Throwable cause) {
		super(message, cause);
 	}

	
}
