package com.cwc.app.exception.printTask;

public class AndroidPrintServerNameExitsException extends RuntimeException{

	public AndroidPrintServerNameExitsException() {
		super();
	}

	public AndroidPrintServerNameExitsException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
