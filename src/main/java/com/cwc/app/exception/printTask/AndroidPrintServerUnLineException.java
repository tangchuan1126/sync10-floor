package com.cwc.app.exception.printTask;

public class AndroidPrintServerUnLineException extends RuntimeException{

	public AndroidPrintServerUnLineException() {
		super();
	}

	public AndroidPrintServerUnLineException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
