package com.cwc.app.exception.bcs;

//序列号重复
public class ReceiveProductSNRepeatException extends Exception {
	
	public ReceiveProductSNRepeatException() 
	{
		super();
	}
	
	public ReceiveProductSNRepeatException(String inMessage)
	{
		super(inMessage);
	}
}
