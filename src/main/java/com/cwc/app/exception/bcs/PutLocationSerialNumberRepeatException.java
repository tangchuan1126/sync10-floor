package com.cwc.app.exception.bcs;

/**
 * 放货时，同张单据序列号重复
 * @author Administrator
 *
 */
public class PutLocationSerialNumberRepeatException extends Exception {
	
	public PutLocationSerialNumberRepeatException() 
	{
		super();
	}
	
	public PutLocationSerialNumberRepeatException(String inMessage)
	{
		super(inMessage);
	}
}
