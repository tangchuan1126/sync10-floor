package com.cwc.app.exception.bcs;

public class PickUpCountMoreException extends Exception {
	
	public PickUpCountMoreException() 
	{
		super();
	}
	
	public PickUpCountMoreException(String inMessage)
	{
		super(inMessage);
	}
}
