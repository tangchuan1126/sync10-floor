package com.cwc.app.exception.bcs;

public class XMLDataErrorException extends Exception {
	
	public XMLDataErrorException() 
	{
		super();
	}
	
	public XMLDataErrorException(String inMessage)
	{
		super(inMessage);
	}
}
