package com.cwc.app.exception.bcs;

public class NetWorkException extends Exception {
	
	public NetWorkException() 
	{
		super();
	}
	
	public NetWorkException(String inMessage)
	{
		super(inMessage);
	}
}
