package com.cwc.app.exception.bcs;

/**
 * 无法放货完成
 * @author Administrator
 *
 */
public class CanNotPutOverException extends Exception {
	
	public CanNotPutOverException() 
	{
		super();
	}
	
	public CanNotPutOverException(String inMessage)
	{
		super(inMessage);
	}
}
