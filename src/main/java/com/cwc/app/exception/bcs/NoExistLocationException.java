package com.cwc.app.exception.bcs;

public class NoExistLocationException extends Exception {
	
	public NoExistLocationException() 
	{
		super();
	}
	
	public NoExistLocationException(String inMessage)
	{
		super(inMessage);
	}
}
