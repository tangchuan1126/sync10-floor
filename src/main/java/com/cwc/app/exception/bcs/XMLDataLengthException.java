package com.cwc.app.exception.bcs;

public class XMLDataLengthException extends Exception {
	
	public XMLDataLengthException() 
	{
		super();
	}
	
	public XMLDataLengthException(String inMessage)
	{
		super(inMessage);
	}
}
