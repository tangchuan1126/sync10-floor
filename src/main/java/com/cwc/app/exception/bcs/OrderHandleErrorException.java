package com.cwc.app.exception.bcs;

/**
 * 订单并非已打印
 * @author Administrator
 *
 */
public class OrderHandleErrorException extends Exception {
	
	public OrderHandleErrorException() 
	{
		super();
	}
	
	public OrderHandleErrorException(String inMessage)
	{
		super(inMessage);
	}
}
