package com.cwc.app.exception.bcs;

public class LpNotFindException extends Exception {
	
	public LpNotFindException() 
	{
		super();
	}
	
	public LpNotFindException(String inMessage)
	{
		super(inMessage);
	}
}
