package com.cwc.app.exception.bcs;

public class CanNotPutProductException extends Exception {
	
	public CanNotPutProductException() 
	{
		super();
	}
	
	public CanNotPutProductException(String inMessage)
	{
		super(inMessage);
	}
}
