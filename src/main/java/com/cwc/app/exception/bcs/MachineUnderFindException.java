package com.cwc.app.exception.bcs;

public class MachineUnderFindException extends RuntimeException {
	
	public MachineUnderFindException() 
	{
		super();
	}
	
	public MachineUnderFindException(String inMessage)
	{
		super(inMessage);
	}
}
