package com.cwc.app.exception.b2b;
/**
 * 转运单详细重复
 * @author Administrator
 *
 */
public class B2BOrderDetailWaitCountNotEnoughException extends Exception {
	public B2BOrderDetailWaitCountNotEnoughException() 
	{
		super();
	}
	
	public B2BOrderDetailWaitCountNotEnoughException(String inMessage)
	{
		super(inMessage);
	}
}
