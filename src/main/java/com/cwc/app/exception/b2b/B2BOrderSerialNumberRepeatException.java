package com.cwc.app.exception.b2b;
/**
 * 无法填写序列号
 * @author Administrator
 *
 */
public class B2BOrderSerialNumberRepeatException extends Exception {
	public B2BOrderSerialNumberRepeatException() 
	{
		super();
	}
	
	public B2BOrderSerialNumberRepeatException(String inMessage)
	{
		super(inMessage);
	}
}
