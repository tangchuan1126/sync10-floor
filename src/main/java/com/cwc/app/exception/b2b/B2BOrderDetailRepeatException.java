package com.cwc.app.exception.b2b;
/**
 * 转运单详细重复
 * @author Administrator
 *
 */
public class B2BOrderDetailRepeatException extends Exception {
	public B2BOrderDetailRepeatException() 
	{
		super();
	}
	
	public B2BOrderDetailRepeatException(String inMessage)
	{
		super(inMessage);
	}
}
