package com.cwc.app.exception.b2b;
/**
 * 转运单不存在
 * @author Administrator
 *
 */
public class B2BOrderNoExistException extends Exception {
	public B2BOrderNoExistException() 
	{
		super();
	}
	
	public B2BOrderNoExistException(String inMessage)
	{
		super(inMessage);
	}
}
