package com.cwc.app.exception.b2b;
/**
 * 转运单详细重复
 * @author Administrator
 *
 */
public class B2BOrderDetailSamePcDiffLotNumberException extends Exception {
	public B2BOrderDetailSamePcDiffLotNumberException() 
	{
		super();
	}
	
	public B2BOrderDetailSamePcDiffLotNumberException(String inMessage)
	{
		super(inMessage);
	}
}
