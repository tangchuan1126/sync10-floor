package com.cwc.app.exception.b2b;
/**
 * 转运单明细不存在
 * @author Administrator
 *
 */
public class B2BOrderNoExistItemException extends Exception {
	public B2BOrderNoExistItemException() 
	{
		super();
	}
	
	public B2BOrderNoExistItemException(String inMessage)
	{
		super(inMessage);
	}
}
