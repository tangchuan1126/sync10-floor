package com.cwc.app.exception.productStorage;

/**
 * 无法序列号预保留
 * @author Administrator
 *
 */
public class CanNotSerialNumberReserveException extends Exception {
	
	public CanNotSerialNumberReserveException() 
	{
		super();
	}
	
	public CanNotSerialNumberReserveException(String inMessage)
	{
		super(inMessage);
	}
}
