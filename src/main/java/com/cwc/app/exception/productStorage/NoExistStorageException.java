package com.cwc.app.exception.productStorage;

/**
 * 仓库不存在
 * @author Administrator
 *
 */
public class NoExistStorageException extends Exception {
	
	public NoExistStorageException() 
	{
		super();
	}
	
	public NoExistStorageException(String inMessage)
	{
		super(inMessage);
	}
}
