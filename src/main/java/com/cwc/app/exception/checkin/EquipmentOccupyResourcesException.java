package com.cwc.app.exception.checkin;

/**
 * 设备已经占用资源
 * @author zyj
 */
public class EquipmentOccupyResourcesException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public EquipmentOccupyResourcesException() 
	{
		super();
	}
	
	public EquipmentOccupyResourcesException(String inMessage)
	{
		super(inMessage);
	}
	public EquipmentOccupyResourcesException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
