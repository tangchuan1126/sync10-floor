package com.cwc.app.exception.checkin;

/**
 * entry pickUp设备必须是其他entry带进来的
 * @author zyj
 */
public class EquipmentPickUpNotInThisEntryException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public EquipmentPickUpNotInThisEntryException() 
	{
		super();
	}
	
	public EquipmentPickUpNotInThisEntryException(String inMessage)
	{
		super(inMessage);
	}
	public EquipmentPickUpNotInThisEntryException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
