package com.cwc.app.exception.checkin;

/**
 * 设备未找到
 * @author zyj
 */
public class EquipmentNotFindException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public EquipmentNotFindException() 
	{
		super();
	}
	
	public EquipmentNotFindException(String inMessage)
	{
		super(inMessage);
	}
	public EquipmentNotFindException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
