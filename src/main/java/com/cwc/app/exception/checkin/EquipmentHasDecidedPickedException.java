package com.cwc.app.exception.checkin;

/**
 *设备已经预计或者已经被其他entry带走
 * @author zyj
 */
public class EquipmentHasDecidedPickedException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public EquipmentHasDecidedPickedException() 
	{
		super();
	}
	
	public EquipmentHasDecidedPickedException(String inMessage)
	{
		super(inMessage);
	}
	public EquipmentHasDecidedPickedException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
