package com.cwc.app.exception.checkin;

/**
 * entry内task重复
 * @author zyj
 */
public class CheckInTaskRepeatToEntryException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public CheckInTaskRepeatToEntryException() 
	{
		super();
	}
	
	public CheckInTaskRepeatToEntryException(String inMessage)
	{
		super(inMessage);
	}
	public CheckInTaskRepeatToEntryException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
