package com.cwc.app.exception.checkin;

/**
 * 设备InYard
 * @author zyj
 */
public class EquipmentInYardException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public EquipmentInYardException() {
		super();
	}
	
	public EquipmentInYardException(String inMessage)
	{
		super(inMessage);
	}
	
	public EquipmentInYardException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
