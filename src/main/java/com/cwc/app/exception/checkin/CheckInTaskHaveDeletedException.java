package com.cwc.app.exception.checkin;

/**
 * task已经删除了
 * @author zyj
 */
public class CheckInTaskHaveDeletedException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public CheckInTaskHaveDeletedException() 
	{
		super();
	}
	
	public CheckInTaskHaveDeletedException(String inMessage)
	{
		super(inMessage);
	}
	public CheckInTaskHaveDeletedException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
