package com.cwc.app.exception.checkin;

/**
 * 设备已经开始装货
 * @author zyj
 */
public class EquipmentHasLoadedOrdersException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public EquipmentHasLoadedOrdersException() 
	{
		super();
	}
	
	public EquipmentHasLoadedOrdersException(String inMessage)
	{
		super(inMessage);
	}
	public EquipmentHasLoadedOrdersException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
