package com.cwc.app.exception.checkin;

/**
 * 门已经被占用：Reserved或Occupid
 * @author zyj
 */
public class DoorHasUsedException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public DoorHasUsedException() 
	{
		super();
	}
	
	public DoorHasUsedException(String inMessage)
	{
		super(inMessage);
	}
	public DoorHasUsedException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
