package com.cwc.app.exception.checkin;

/**
 * 子单据不能被删除
 * @author zyj
 */
public class CheckInTaskCanntDeleteException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public CheckInTaskCanntDeleteException() 
	{
		super();
	}
	
	public CheckInTaskCanntDeleteException(String inMessage)
	{
		super(inMessage);
	}
	public CheckInTaskCanntDeleteException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
