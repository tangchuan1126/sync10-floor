package com.cwc.app.exception.checkin;

/**
 * 设备已经带走，不能修改
 * @author zyj
 */
public class EquipmentHadOutException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public EquipmentHadOutException() {
		super();
	}
	
	public EquipmentHadOutException(String inMessage)
	{
		super(inMessage);
	}
	public EquipmentHadOutException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
