package com.cwc.app.exception.checkin;

/**
 * entry必须有子单据
 * @author zyj
 */
public class CheckInMustHasTasksException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public CheckInMustHasTasksException() 
	{
		super();
	}
	
	public CheckInMustHasTasksException(String inMessage)
	{
		super(inMessage);
	}
	public CheckInMustHasTasksException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
