package com.cwc.app.exception.checkin;

/**
 * 门未找到
 * @author zyj
 */
public class DoorNotFindException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public DoorNotFindException() 
	{
		super();
	}
	
	public DoorNotFindException(String inMessage)
	{
		super(inMessage);
	}
	public DoorNotFindException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
