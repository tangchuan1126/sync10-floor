package com.cwc.app.exception.checkin;

/**
 * small parcel不能关闭order/task
 * @author xj
 */
public class SmallParcelNotCloseOrderException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public SmallParcelNotCloseOrderException() 
	{
		super();
	}
	
	public SmallParcelNotCloseOrderException(String inMessage)
	{
		super(inMessage);
	}
	public SmallParcelNotCloseOrderException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
