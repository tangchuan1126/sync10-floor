package com.cwc.app.exception.checkin;

/**
 * Task未找到
 * @author zyj
 */
public class CheckinTaskNotFoundException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public CheckinTaskNotFoundException() 
	{
		super();
	}
	
	public CheckinTaskNotFoundException(String inMessage)
	{
		super(inMessage);
	}
	public CheckinTaskNotFoundException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
