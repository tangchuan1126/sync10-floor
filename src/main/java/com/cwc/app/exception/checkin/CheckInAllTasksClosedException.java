package com.cwc.app.exception.checkin;

/**
 * entry所有子单据都关了
 * @author zyj
 */
public class CheckInAllTasksClosedException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public CheckInAllTasksClosedException() 
	{
		super();
	}
	
	public CheckInAllTasksClosedException(String inMessage)
	{
		super(inMessage);
	}
	public CheckInAllTasksClosedException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
