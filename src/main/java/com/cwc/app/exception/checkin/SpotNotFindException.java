package com.cwc.app.exception.checkin;

/**
 * 停车位未找到
 * @author zyj
 */
public class SpotNotFindException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public SpotNotFindException() 
	{
		super();
	}
	
	public SpotNotFindException(String inMessage)
	{
		super(inMessage);
	}
	public SpotNotFindException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
