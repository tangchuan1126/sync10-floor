package com.cwc.app.exception.checkin;

/**
 * ctnr同其他单据重复
 * @author zyj
 */
public class CheckInCtnRepeatToOthersException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public CheckInCtnRepeatToOthersException() 
	{
		super();
	}
	
	public CheckInCtnRepeatToOthersException(String inMessage)
	{
		super(inMessage);
	}
	public CheckInCtnRepeatToOthersException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
