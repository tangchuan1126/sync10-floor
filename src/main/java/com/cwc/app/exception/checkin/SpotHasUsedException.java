package com.cwc.app.exception.checkin;

/**
 * 停车位已经被占用：Reserved或Occupid
 * @author zyj
 */
public class SpotHasUsedException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public SpotHasUsedException() 
	{
		super();
	}
	
	public SpotHasUsedException(String inMessage)
	{
		super(inMessage);
	}
	public SpotHasUsedException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
