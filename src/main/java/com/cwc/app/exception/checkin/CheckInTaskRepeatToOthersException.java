package com.cwc.app.exception.checkin;

/**
 * task与其他entry重复
 * @author zyj
 */
public class CheckInTaskRepeatToOthersException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public CheckInTaskRepeatToOthersException() 
	{
		super();
	}
	
	public CheckInTaskRepeatToOthersException(String inMessage)
	{
		super(inMessage);
	}
	public CheckInTaskRepeatToOthersException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
