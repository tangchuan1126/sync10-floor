package com.cwc.app.exception.checkin;

/**
 * 设备对于本entry重复
 * @author zyj
 */
public class EquipmentSameToEntryException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public EquipmentSameToEntryException() {
		super();
	}
	
	public EquipmentSameToEntryException(String inMessage)
	{
		super(inMessage);
	}
	public EquipmentSameToEntryException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
