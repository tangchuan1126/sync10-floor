package com.cwc.app.exception.checkin;

/**
 * Task没有指派给warehouse supervisor
 * @author xj
 */
public class TaskNoAssignWarehouseSupervisorException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public TaskNoAssignWarehouseSupervisorException() 
	{
		super();
	}
	
	public TaskNoAssignWarehouseSupervisorException(String inMessage)
	{
		super(inMessage);
	}
	public TaskNoAssignWarehouseSupervisorException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
