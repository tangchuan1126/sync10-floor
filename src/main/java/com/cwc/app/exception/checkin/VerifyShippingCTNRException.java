package com.cwc.app.exception.checkin;

/**
 * 设备已经有tasks
 * @author zyj
 */
public class VerifyShippingCTNRException extends RuntimeException{

	private int ServerErrorCode = 0;
	private String ServerErrorMessage;
	
	public VerifyShippingCTNRException() 
	{
		super();
	}
	
	public VerifyShippingCTNRException(String inMessage)
	{
		super(inMessage);
	}
	public VerifyShippingCTNRException(int ServerErrorCode,String msg)
	{
		super();
		this.ServerErrorCode = ServerErrorCode;
		this.ServerErrorMessage = msg;
	}
	
	/**
	 * 错误代码
	 * @return
	 */
	public int getServerErrorCode()
	{
		return(ServerErrorCode);
	}
	
	/**
	 * 错误原因
	 * @return
	 */
	public String getServerErrorMessage()
	{
		return ServerErrorMessage;
	}
	
}
