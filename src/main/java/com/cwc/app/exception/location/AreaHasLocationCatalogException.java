package com.cwc.app.exception.location;

/**
 * 区域下有位置
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class AreaHasLocationCatalogException extends Exception 
{
	public AreaHasLocationCatalogException() 
	{
		super();
	}
	
	public AreaHasLocationCatalogException(String inMessage)
	{
		super(inMessage);
	}
}
