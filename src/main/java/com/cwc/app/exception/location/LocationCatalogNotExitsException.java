package com.cwc.app.exception.location;

/**
 * 位置不存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class LocationCatalogNotExitsException extends Exception 
{
	public LocationCatalogNotExitsException() 
	{
		super();
	}
	
	public LocationCatalogNotExitsException(String inMessage)
	{
		super(inMessage);
	}
}
