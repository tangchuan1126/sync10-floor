package com.cwc.app.exception.location;

/**
 * 区域下已有此编号位置
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class LocationCatalogHasExitsException extends Exception 
{
	public LocationCatalogHasExitsException() 
	{
		super();
	}
	
	public LocationCatalogHasExitsException(String inMessage)
	{
		super(inMessage);
	}
}
