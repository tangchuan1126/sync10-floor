package com.cwc.app.exception.location;

/**
 * 同仓库下已有此区域
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class AreaHasExitsException extends Exception 
{
	public AreaHasExitsException() 
	{
		super();
	}
	
	public AreaHasExitsException(String inMessage)
	{
		super(inMessage);
	}
}
