package com.cwc.app.exception.location;

/**
 * 区域不存在
 * @author TurboShop
 *
 * TurboShop.cn all rights reserved.
 */

public class AreaNotExitsException extends Exception 
{
	public AreaNotExitsException() 
	{
		super();
	}
	
	public AreaNotExitsException(String inMessage)
	{
		super(inMessage);
	}
}
